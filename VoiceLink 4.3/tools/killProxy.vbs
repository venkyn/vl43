Const HIDDEN_WINDOW = 0

strComputer = "." 

Set objWMIService = GetObject( _
    "winmgmts:\\" & strComputer & "\root\CIMV2") 

Set colItems = objWMIService.ExecQuery( _
    "SELECT * FROM Win32_Process" & _
    " WHERE Name = 'java.exe'",,48) 

Set objShell = Wscript.createObject("wscript.shell")

Dim proxyProcessIds(1)

For Each objItem in colItems 
    Wscript.Echo objItem.ProcessId & "|" & objItem.Name & "|" & objItem.CommandLine

	if InStr(objItem.CommandLine,"com.vocollect.voicelink.proxy.watchdog.ProxyWatchDog") > 0 Then
		proxyProcessIds(0) = objItem.ProcessId 
	end If 

	if InStr(objItem.CommandLine,"com.vocollect.voicelink.proxy.ProxyMain") > 0 Then
		proxyProcessIds(1) = objItem.ProcessId 
	end If
Next

For Each proxyProcessId in proxyProcessIds
	if Len(proxyProcessId) > 0 Then 
		objShell.Run("TASKKILL /F /PID " & proxyProcessId)
		Wscript.Echo "Proxy Process : " & proxyProcessId & " killed!"
	end If
Next