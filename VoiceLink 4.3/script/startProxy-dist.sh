#!/bin/bash

base_dir=/root/Desktop/proxy-dist-install
proxy_config=$base_dir/config
proxy_lib=$base_dir/lib
java_cmd=JAVA_HOMEia/bin/java
java_ext_dirs=JAVA_HOMEia/jre/ext
java_opt_proxy=-Xrs

if [ ! -d "$proxy_config" ]
then
    echo ERROR
    echo Unable to find proxy config directory: $proxy_config
    echo Please check the path specified in proxy_config
    echo correctly pointing to the proxy config directory.
    exit
fi

if [ ! -d "$proxy_lib" ]
then
    echo ERROR
    echo Unable to find proxy library files: $proxy_files
    echo Please check the path specified in proxy_lib
    echo correctly pointing to the Voicelink WEB-INF/classes
    echo directory.
    exit
fi

$java_cmd -server -Djava.ext.dirs="$proxy_lib:$java_ext_dirs" -Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=localhost -Dlog4j.configuration=file:///"$proxy_config/log4j.proxy.properties" -Dproxy.dir=$base_dir -classpath $proxy_config $java_opt_proxy com.vocollect.voicelink.proxy.ProxyMain

