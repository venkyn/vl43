#!/bin/bash
base_dir=/root/Desktop/proxy-install
proxy_config=$base_dir/config
proxy_lib=$base_dir/lib
java_cmd=/usr/java/jdk1.5.0_11/bin/java

watch_dog_class_path="$proxy_config":"$proxy_lib/proxy.jar"


$java_cmd -server -classpath $watch_dog_class_path com.vocollect.voicelink.proxy.watchdog.ProxyWatchDog -shutdown localhost
