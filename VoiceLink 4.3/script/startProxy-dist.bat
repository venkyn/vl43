@echo Starting VoiceLink Proxy Server
@echo off

set base_dir=PROXY_HOMEia
set proxy_config=%base_dir%\config
set proxy_lib=%base_dir%\lib
set java_cmd=JAVA_HOMEia\bin\java.exe
set java_ext_dirs=JAVA_HOMEia\jre\lib\ext
set java_opt_proxy=-Xrs

if not exist "%proxy_config%" goto config_not_found
if not exist "%proxy_lib%" goto lib_not_found
if not exist "%java_ext_dirs%" goto java_ext_not_found

"%java_cmd%" -Djava.ext.dirs="%proxy_lib%;%java_ext_dirs%" -Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=localhost -Dlog4j.configuration=file:///"%proxy_config%\log4j.proxy.properties" -Dproxy.dir="%base_dir%" -classpath "%proxy_config%" %java_opts_proxy% com.vocollect.voicelink.proxy.ProxyMain

goto end

:config_not_found
echo ERROR
echo Unable to find proxy config directory.
echo Please check the path specified in proxy_config
echo correctly pointing to the proxy config directory.
goto end

:lib_not_found
echo ERROR
echo Unable to find proxy library files.
echo Please check the path specified in proxy_lib
echo correctly pointing to the Voicelink WEB-INF\classes
echo directory.
goto end

:java_ext_not_found
echo ERROR
echo Unable to find java.ext.dirs.
echo Please check to ensure that the jdk was installed
echo and that java_ext_dirs is correctly pointing to it.
goto end

:end
REM exit
