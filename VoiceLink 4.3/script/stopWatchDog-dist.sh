#!/bin/bash

base_dir=/root/Desktop/proxy-dist-install
proxy_config=$base_dir/config
proxy_lib=$base_dir/lib
servlet_api=$base_dir/lib/servlet-api.jar
voice_link_path=$base_dir/lib/proxy_dist.jar
java_cmd=/usr/java/jdk1.5.0_11/bin/java

watch_dog_class_path=$base_dir/config:$voice_link_path
echo $watch_dog_class_path

for i in "$proxy_lib"/*
do
	watch_dog_class_path="$watch_dog_class_path:$i"
done


$java_cmd -classpath $watch_dog_class_path com.vocollect.voicelink.proxy.watchdog.ProxyWatchDog -shutdown localhost
