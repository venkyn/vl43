#!/bin/bash
base_dir=/root/Desktop/proxy-dist-install
proxy_config=$base_dir/config
proxy_lib=$base_dir/lib
java_cmd=/usr/java/jdk1.5.0_11/bin/java
java_opt_proxy=-Xrs

watch_dog_class_path="$proxy_config":"$proxy_lib/proxy_dist.jar"


$java_cmd -server -classpath $watch_dog_class_path $java_opt_proxy com.vocollect.voicelink.proxy.watchdog.ProxyWatchDog ./startProxy-dist.sh &

exit 0

