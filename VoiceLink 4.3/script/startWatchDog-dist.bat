@echo off

set base_dir=C:\Documents and Settings\khazra\Desktop\proxy-dist-install
set proxy_config=%base_dir%\config
set proxy_lib=%base_dir%\lib
set java_cmd=C:\Program Files\Java\jdk1.5.0_08\bin\java.exe
set java_opt_proxy=-Xrs

set watch_dog_class_path="%proxy_config%";"%proxy_lib%\proxy_dist.jar"

"%java_cmd%" -classpath %watch_dog_class_path% %java_opt_proxy% com.vocollect.voicelink.proxy.watchdog.ProxyWatchDog "%base_dir%\bin\startProxy-dist.bat" 