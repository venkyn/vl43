/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import com.vocollect.voicelink.proxy.NIOChannelHelper;
import com.vocollect.voicelink.proxy.exceptions.VLProxyIOException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SocketChannel;

import junit.framework.TestCase;

import org.testng.annotations.Test;

/**
 * 
 * 
 * @author khazra
 */
@Test(sequential = true)
public class NIOChannelHelperTest extends TestCase {

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#getChannel()}.
     */

    public void testGetChannel() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        try {
            NIOChannelHelper channelHelper = new NIOChannelHelper(
                "UTF-8", "\r\n");
            channelHelper.setChannel(testChannel);
            SocketChannel resultChannel = channelHelper.getChannel();

            if (!testChannel.equals(resultChannel)) {
                fail("Expected channel : " + testChannel + "\n Got channel : "
                    + resultChannel);
            }

            channelHelper.close();

        } catch (VLProxyIOException ex) {
            fail("Error creating NIO Channel Helper");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#getChannelsOpenedCount()}.
     */

    public void testGetChannelsOpenedCount() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        try {
            NIOChannelHelper testChannelHelper1 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper1.setChannel(testChannel);

            long numberOfChannelOpened1 = NIOChannelHelper
                .getChannelsOpenedCount();

            NIOChannelHelper testChannelHelper2 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper2.setChannel(testChannel);

            long numberOfChannelOpened2 = NIOChannelHelper
                .getChannelsOpenedCount();

            if ((numberOfChannelOpened2 - numberOfChannelOpened1) != 1) {
                fail("Expected channelOpened : 1 \n Actuall channelOpened : "
                    + (numberOfChannelOpened2 - numberOfChannelOpened2));
            }

            testChannelHelper1.close();
            testChannelHelper2.close();

        } catch (VLProxyIOException ex) {
            fail("Error creating NIO Channel Helper");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#getChannelsClosedCount()}.
     */

    public void testGetChannelsClosedCount() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        try {
            NIOChannelHelper testChannelHelper1 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper1.setChannel(testChannel);

            long numberOfChannelOpened1 = NIOChannelHelper
                .getChannelsOpenedCount();
            long numberOfChannelClosed1 = NIOChannelHelper
                .getChannelsClosedCount();

            NIOChannelHelper testChannelHelper2 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper2.setChannel(testChannel);

            testChannelHelper2.close();

            long numberOfChannelOpened2 = NIOChannelHelper
                .getChannelsOpenedCount();
            long numberOfChannelClosed2 = NIOChannelHelper
                .getChannelsClosedCount();

            if ((numberOfChannelOpened2 - numberOfChannelOpened1) != 1) {
                fail("Expected channelOpened : 1 \n Actuall channelOpened : "
                    + (numberOfChannelOpened2 - numberOfChannelOpened2));
            }

            if ((numberOfChannelClosed2 - numberOfChannelClosed1) != 1) {
                fail("Expected channelOpened : 2 \n Actuall channelOpened : "
                    + (numberOfChannelClosed2 - numberOfChannelClosed1));
            }

            testChannelHelper1.close();

        } catch (VLProxyIOException ex) {
            fail("Error creating NIO Channel Helper");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#getChannelsActiveCount()}.
     */

    public void testGetChannelsActiveCount() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        try {
            NIOChannelHelper testChannelHelper1 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper1.setChannel(testChannel);

            NIOChannelHelper testChannelHelper2 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper2.setChannel(testChannel);

            testChannelHelper2.close();

            long numberOfActiveChannel = NIOChannelHelper
                .getChannelsActiveCount();

            if (numberOfActiveChannel != 1) {
                fail("Excepted active channel : 1 \n Got : "
                    + numberOfActiveChannel);
            }

            testChannelHelper1.close();
        } catch (VLProxyIOException ex) {
            fail("Error creating NIO Channel Helper");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#getAcceptDate()}.
     */

    public void testGetAcceptDate() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        try {
            NIOChannelHelper testChannelHelper1 = new NIOChannelHelper(
                "UTF-8", "\r\n");
            testChannelHelper1.setChannel(testChannel);

            if (testChannelHelper1.getAcceptDate() == null) {
                fail("Accept date is not initialized properly");
            }
            testChannelHelper1.close();

        } catch (VLProxyIOException ex) {
            fail("Error creating NIO Channel Helper");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#checkForRemoteClose()}.
     */

//    public void testCheckForRemoteClose() {
//        try {
//            MiniServer server = new MiniServer(15003);
//            server.start();
//
//            InetSocketAddress address = new InetSocketAddress(
//                "localhost", 15003);
//            Socket client = new Socket();
//            client.connect(address, 5000);
//
//            OutputStream out = client.getOutputStream();
//            out.write("Hello".getBytes());
//            long channelCountBefore = NIOChannelHelper.getChannelsActiveCount();
//            client.close();
//            long channelCountAfter = NIOChannelHelper.getChannelsActiveCount();
//
//            if (channelCountBefore != channelCountAfter) {
//                fail("Channel Count before : " + channelCountBefore
//                    + " and after : " + channelCountAfter
//                    + "NIOChannelHelper not detecting client close");
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            fail("NIOChannelHelper Remote close failed due to IOException");
//        }
//
//    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#selectForRead(long)}.
     */

    public void testSelectForReadWrite() {
        try {
            MiniServer server = new MiniServer(15004);
            server.start();

            InetSocketAddress address = new InetSocketAddress(
                "localhost", 15004);
            Socket client = new Socket();
            client.connect(address, 5000);

            OutputStream out = client.getOutputStream();
            out.write("Hello\r\n".getBytes());

            InputStream in = client.getInputStream();
            byte[] data = new byte[7];
            in.read(data);

            String dataString = new String(data);
            if (!"Hello\r\n".equals(dataString)) {
                fail("Expected data : Hello \n Got : " + dataString);
            }

        } catch (IOException ex) {
            fail("NIOChannelHelper Select for Read Write failed due to IOException");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#selectForRead(long)}.
     */

    public void testDelayedRead() {
        try {
            MiniServer server = new MiniServer(15005);
            server.start();

            InetSocketAddress address = new InetSocketAddress(
                "localhost", 15005);
            Socket client = new Socket();
            client.connect(address, 5000);

            OutputStream out = client.getOutputStream();
            out.write("H".getBytes());
            Thread.sleep(5000);
            out.write("e".getBytes());
            out.write("llo\r\n".getBytes());

            InputStream in = client.getInputStream();
            byte[] data = new byte[7];
            in.read(data);

            String dataString = new String(data);
            if (!"Hello\r\n".equals(dataString)) {
                fail("Expected data : Hello \n Got : " + dataString);
            }

        } catch (IOException ex) {
            fail("NIOChannelHelper Delayed Read test failed due to IOException");
        } catch (InterruptedException ex) {
            fail("NIOChannelHelper Delayed Read test failed due to InterruptedException");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.NIOChannelHelper#selectForRead(long)}.
     */

    public void testBufferOverflow() {
        try {
            MiniServer server = new MiniServer(15006);
            server.start();

            InetSocketAddress address = new InetSocketAddress(
                "localhost", 15006);
            Socket client = new Socket();
            client.connect(address, 5000);

            OutputStream out = client.getOutputStream();

            for (int i = 0; i < 1030; i++) {
                out.write("H".getBytes());
            }

            InputStream in = client.getInputStream();
            byte[] dataByte = new byte[5000];
            in.read(dataByte);

            fail("Not detecting oversized message : " + dataByte);

        } catch (Throwable ex) {
            return;
        }
    }

    /**
     * Tests whether NIOChannelHelper is interruptable or not.
     * 
     */

    public void testInterruptablility() {
        try {
            MiniServer server = new MiniServer(15007);
            server.start();

            

            InetSocketAddress address = new InetSocketAddress(
                "localhost", 15007);
            Socket client = new Socket();
            client.connect(address, 5000);

            OutputStream out = client.getOutputStream();
            out.write("Hello".getBytes());
            long channelCountBefore = NIOChannelHelper.getChannelsActiveCount();
            server.interrupt();

            long channelCountAfter = NIOChannelHelper.getChannelsActiveCount();

            if (channelCountAfter != channelCountBefore) {
                fail("Channel Count before : " + channelCountBefore
                    + " and after : " + channelCountAfter
                    + " NIOChannelHelper is not interruptable");
            }

        } catch (Throwable ex) {
            System.out.println(ex);
            fail("NIOChannelHelper is not interruptable");
        }
    }
}
