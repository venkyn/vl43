/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.fail;

import com.vocollect.voicelink.proxy.Connection;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

import org.testng.annotations.Test;

/**
 * Tests the Connection class.
 * 
 * @author khazra
 */
@Test(groups = { "unit" }, sequential = true)
public class TestConnection extends BaseProxyTestCase {

    /**
     * Tests Connection object is created properly.
     * 
     */
    
    public void testConnectionObject() {
        try {
            Connection connection = new Connection();
            connection.close();
        } catch (IOException ex) {
            fail();
        }
    }

    /**
     * Tests that connection can register channels in Accept mode.
     * 
     */
    
    public void testRegisterChannelInAcceptMode() {
        Connection connection = null;
        try {
            connection = new Connection();
        } catch (IOException ex) {
            fail("Cannot create Connection object");
        }

        ServerSocketChannel channel = null;
        try {
            channel = ServerSocketChannel.open();
            channel.configureBlocking(false);

            InetAddress localHost = InetAddress.getLocalHost();
            InetSocketAddress localHostSocket = new InetSocketAddress(
                localHost, 1000);

            channel.socket().bind(localHostSocket);
        } catch (IOException ex) {
            fail("Cannot create the server socket");
        }

        try {
            connection.register(channel);
        } catch (IOException ex) {
            fail("Cannot register channel");
        }

        try {
            connection.close();
            channel.close();
        } catch (IOException ex) {
            fail("Failed closing connection");
        }
    }

    /**
     * Tests that connection can register channels in Read/Write mode.
     * 
     */
    
    public void testRegisterChannelInReadWriteMode() {
        Connection connection = null;
        try {
            connection = new Connection();
        } catch (IOException ex) {
            fail("Cannot create Connection object");
        }

        SocketChannel channel = null;
        try {
            channel = SocketChannel.open();
            channel.configureBlocking(false);

            InetAddress localHost = InetAddress.getLocalHost();
            InetSocketAddress localHostSocket = new InetSocketAddress(
                localHost, 1000);

            channel.socket().bind(localHostSocket);
        } catch (IOException ex) {
            fail("Cannot create the server socket");
        }

        try {
            SelectionKey selection = connection.register(
                channel, SelectionKey.OP_READ);
        } catch (IOException ex) {
            fail("Cannot register channel");
        }

        try {
            connection.close();
        } catch (IOException ex) {
            fail("Failed closing connection");
        }
    }
}
