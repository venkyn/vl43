/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.fail;
import static org.testng.AssertJUnit.assertEquals;

import com.vocollect.voicelink.proxy.Connection;
import com.vocollect.voicelink.proxy.ProcessResponseTask;
import com.vocollect.voicelink.proxy.RequestResponseObject;
import com.vocollect.voicelink.proxy.VocollectRRO;
import com.vocollect.voicelink.proxy.VocollectRROFactory;

import org.testng.annotations.Test;

/**
 * Tests the ProcessResponseTask class.
 * 
 * @author sthomas
 */
@Test(groups = { "unit" }, sequential = true)
public class ResponseProcessorTests {

    final String VALUE_TESTING = "testing";

    final String VALUE_CONVERTED_BOOL_TRUE = "1";

    final String VALUE_CONVERTED_BOOL_FALSE = "0";

    public void testConvertTypes() {
        VocollectRROFactory factory = new VocollectRROFactory(15);
        RequestResponseObject rro = factory.createRRO("test", null);
        ProcessResponseTask task = new ProcessResponseTask();
        task.setRRO(rro);

        Object temp = new String(VALUE_TESTING);
        temp = task.convertTypes(temp);
        int result = VALUE_TESTING.compareTo((String) temp);
        assertEquals(0, result);

        temp = new Boolean(true);
        temp = task.convertTypes(temp);
        assertEquals(1, temp);

        temp = new Boolean(false);
        temp = task.convertTypes(temp);
        assertEquals(0, temp);
    }

    public void testFormatValue() {
        VocollectRROFactory factory = new VocollectRROFactory(15);
        RequestResponseObject rro = factory.createRRO("test", null);
        ProcessResponseTask task = new ProcessResponseTask();
        task.setRRO(rro);

        Object temp = new Integer(5);
        temp = task.formatValue(temp, "%d is a number");
        assertEquals("5 is a number", temp);

        temp = new Integer(5);
        temp = task.formatValue(temp, "'%-20d'");
        assertEquals("'5                   '", temp);

        temp = new Integer(5);
        temp = task.formatValue(temp, "'%20d'");
        assertEquals("'                   5'", temp);

        temp = new Integer(5);
        temp = task.formatValue(temp, "'%020d'");
        assertEquals("'00000000000000000005'", temp);
    }

    public void testCleanString() {
        VocollectRROFactory factory = new VocollectRROFactory(15);
        RequestResponseObject rro = factory.createRRO("test", null);
        ProcessResponseTask task = new ProcessResponseTask();
        task.setRRO(rro);

        Object temp = new String("Go to \"Area A\", then \"Area B\"");
        temp = task.cleanString(temp);
        assertEquals("\"Go to Area A, then Area B\"", temp);
    }
}
