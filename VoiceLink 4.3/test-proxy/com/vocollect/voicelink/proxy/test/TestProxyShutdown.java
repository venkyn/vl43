/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.fail;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.testng.annotations.Test;

/**
 * Tests that proxy server is shutting down properly.
 * 
 * @author khazra
 */

public class TestProxyShutdown {

    /**
     * This test tests that proxy is shutting down properly. Need to run proxy
     * server before starting this test case
     * 
     */
    
    public void testShutdown() {
        String host = "10.22.2.105";
        int port = 15004;

        Socket proxyClientSocket = null;
        try {
            proxyClientSocket = new Socket(host, port);
        } catch (IOException ex) {
            fail("Cannot establish connection with server");
        }

        OutputStream socketOut = null;
        try {
            socketOut = proxyClientSocket.getOutputStream();
        } catch (IOException ex) {
            fail("Cannot open stream to server");
        }

        String data = "prTaskODRShutdownProxyServer()\r\n";
        try {
            socketOut.write(data.getBytes());
            socketOut.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
            fail("Cannot send data to server");
        } finally {
            try {
                proxyClientSocket.close();
            } catch (IOException ex) {
                fail("Cannot close connection");
            }
        }

        try {
            Thread.sleep(3 * 1000);
        } catch (InterruptedException ex) {
            fail("Sleep thread interrupted");
        }

        try {
            proxyClientSocket = new Socket(host, port);
        } catch (IOException ex) {
            return;
        }

        fail("Server is not shutting down");
    }
}
