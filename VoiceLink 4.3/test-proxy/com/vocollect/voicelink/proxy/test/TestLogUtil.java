/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.fail;

import com.vocollect.voicelink.proxy.LogUtil;
import com.vocollect.voicelink.proxy.NIOChannelHelper;
import com.vocollect.voicelink.proxy.RequestResponseObject;
import com.vocollect.voicelink.proxy.VocollectRRO;
import com.vocollect.voicelink.proxy.VocollectRROFactory;

import java.io.IOException;
import java.nio.channels.SocketChannel;

import org.testng.annotations.Test;

/**
 * Tests LogUtil utility class
 * 
 * @author khazra
 */
@Test(groups = { "unit" }, sequential = true)
public class TestLogUtil {

    /**
     * Test method for <code>format()</code>
     * {@link com.vocollect.voicelink.proxy.LogUtil#format(java.lang.String, java.lang.Object[])}.
     */
    
    public void testFormat() {
        StringBuilder resultString = LogUtil.format(
            "Testing", "name1", "value1");

        String testerString = "Testing [name1:value1]";

        if (!testerString.equals(resultString.toString())) {
            fail("String format expected : " + testerString + "\n"
                + "String format got : " + resultString.toString());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.LogUtil#formatValues(java.lang.Object[])}.
     */
    
    public void testFormatValues() {
        StringBuilder resultString = LogUtil.formatValues("name1", "value1");

        String testerString = "[name1:value1]";

        if (!testerString.equals(resultString.toString())) {
            fail("String format expected : " + testerString + "\n"
                + "String format got : " + resultString.toString());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.LogUtil#formatArray(java.lang.Object[])}.
     */
    
    public void testFormatArray() {
        try {
            StringBuilder resultString = LogUtil.formatArray(null);
        } catch (AssertionError ex) {
        } catch (Exception ex) {
            fail("formatArray is not detecting the argument is null");
        }

        String[] data = { "arg1" };
        StringBuilder resultString = LogUtil.formatArray(data);
        String testerString = "[arg1]";
        if (!testerString.equals(resultString.toString())) {
            fail("Expected : " + testerString + "\nGot : "
                + resultString.toString());
        }

        String[] data1 = { "arg1", "arg2" };
        resultString = LogUtil.formatArray(data1);
        testerString = "[arg1,arg2]";
        if (!testerString.equals(resultString.toString())) {
            fail("Expected : " + testerString + "\nGot : "
                + resultString.toString());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.LogUtil#formatRRO(com.vocollect.voicelink.proxy.RequestResponseObject)}.
     */
    
    public void testFormatRRO() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        NIOChannelHelper helper = new NIOChannelHelper("UTF-8", "\r\n");
        helper.setChannel(testChannel);        
        
        VocollectRROFactory factory = new VocollectRROFactory(15);
        RequestResponseObject rro = factory.createRRO("TestScript", helper);

        StringBuilder resultString = LogUtil.formatRRO(rro);

        String testerString = "\\[request id:[0-9]\\[data:TestScript\\]\\[ip address:0.0.0.0/0.0.0.0\\]\\[port:0\\]\\[remote:null\\]";

        if (resultString.toString().matches(testerString)) {
            fail("Expected : " + testerString + "\nGot : "
                + resultString.toString());
        }

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.proxy.LogUtil#formatChannel(java.nio.channels.SocketChannel)}.
     */
    
    public void testFormatChannel() {
        SocketChannel testChannel = null;
        try {
            testChannel = SocketChannel.open();
        } catch (IOException ex) {
            fail("Cannot create Socket Channel. There is an IO exception");
        }

        StringBuilder resultString = LogUtil.formatChannel(testChannel);
        String testerString = "[ip address:0.0.0.0/0.0.0.0][port:0][remote:null]";
        
        if (!testerString.equals(resultString.toString())) {
            fail("Expected : " + testerString + "\nGot : "
                + resultString.toString());
        }

    }

}
