/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;


/**
 * Base test case class brings up 
 * the proxy app context.
 *
 * @author khazra
 */
public abstract class BaseProxyTestCase {
    
    /**
     * The spring configuration file.
     */
    private static final String CONFIG_FILE = "proxyServerContext.xml";
    
    /**
     * Spring application context for proxy.
     */
    private static ApplicationContext appContext;
    
    /**
     * Spring application context for host/ VL3 business layer.
     */
    private static XmlWebApplicationContext hostContext;
    
    static {
        String[] hostContextPaths = { "classpath*:/applicationContext-epp-*.xml", "classpath*:/applicationContext-voicelink-*.xml" };
        hostContext = new XmlWebApplicationContext();
        hostContext.setConfigLocations(hostContextPaths);
        hostContext.refresh();

        appContext = new ClassPathXmlApplicationContext(CONFIG_FILE);
    }

    
    /**
     * Getter for the appContext property.
     * @return ApplicationContext value of the property
     */
    protected static ApplicationContext getAppContext() {
        return appContext;
    }

    
    /**
     * Setter for the appContext property.
     * @param appContext the new appContext value
     */
    protected static void setAppContext(ApplicationContext appContext) {
        BaseProxyTestCase.appContext = appContext;
    }

    
    /**
     * Getter for the hostContext property.
     * @return XmlWebApplicationContext value of the property
     */
    protected static XmlWebApplicationContext getHostContext() {
        return hostContext;
    }

    
    /**
     * Setter for the hostContext property.
     * @param hostContext the new hostContext value
     */
    protected static void setHostContext(XmlWebApplicationContext hostContext) {
        BaseProxyTestCase.hostContext = hostContext;
    }
    
    /**
     * Constructor.
     */
    protected BaseProxyTestCase() {
        
    }
}
