/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.fail;

import com.vocollect.voicelink.proxy.TimeKeeper;
import com.vocollect.voicelink.proxy.utils.ProxyUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Properties;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.testng.annotations.Test;

/**
 * Issues transactions under different conditions to the proxy server
 * and displays the result.
 *
 * NOTE:
 * In config/log4j.proxy.properties, log4j.rootCategory must have
 * stdout appended for the tests to work.
 * 
 * The basePath variable in startProxy() function should be the correct path.
 * It can be set by an Environment variable: PROXY_BASE_PATH.
 *
 * @author daich
 */

@Test(sequential  = true)
public class TestProxyRunTransaction {

    /**
     * The value of the hostname where server is running.
     */
    private final String hostname = "localhost";

    /**
     * The value of port to connect to.
     */
    private final int port = 15004;

    /**
     * Wait time after issuing server shutdown command.
     */
    private final long waitTime = 5000;

    /**
     * The JMX connection url.
     */
    private final String connectorAddress =
        "service:jmx:rmi://localhost/jndi/rmi://localhost:5000/jmxrmi";

    /**
     * Key for the control object in proxy server.
     */
    private final String controlObjKey =
        "VL3 Proxy Server:type=control,name=Control";

    /**
     * Key for the statistics  object in proxy server.
     */
    private final String statObjKey =
        "VL3 Proxy Server:type=monitoring,name=Statistics";

    /**
     * Server Process instance handle.
     */
    private Process p = null;


    /**
     * This function starts up the proxy server, tests that the proxy responding
     * properly to a Transaction Request and then shuts the server down.
     * Launches the server in a separate process.
     */
    @Test(enabled = true)
    public final void testRunTransactionExec() {
        System.out.println("** Running Test testRunTransactionExec...");
        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
             + "'9999999', 'Opertor A', 'en_US','Default', 'CT-30-1-011')\r\n";

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            startProxy();
            System.out.println("** Server started");

            // Send message
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(message.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server.");

            // Read Response
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();

            System.out.println("** Server Response: "
                                                + totalMessage.toString());
            
            // Let the OS clean up the resources
            Thread.sleep(waitTime);

        } catch (Exception ex) {
            ex.printStackTrace();
            fail("** Test testRunTransactionExec failed. " + ex.toString());
        }

        System.out.println("** Test testRunTransactionExec "
                                                + "passed successfully.");
    }



    /**
     * This function starts up the proxy server, tests that the proxy
     * responding properly to a Transaction Request, uses jmx to retrieve
     * the attributes, and then shuts the server down.
     *
     * After sending one transaction, the REQ attribute should be 1
     *
     */
    @Test(enabled = true)
    public final void testRunTransactionJmx() {
        System.out.println("** Running Test testRunTransactionJmx...");
        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'Opertor A', 'en_US','Default', 'CT-30-1-011')\r\n";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            // ProxyMain.main(new String[] {""});
            startProxy();
            System.out.println("** Server started");

            // Send message
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(message.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server: " + message);

            // Read Response
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();

            System.out.println("** Server Response: "
                                                + totalMessage.toString());
            
            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP"});

            if (result[0] == 1 && result[1] == 1) {
                System.out.println("** Test testRunTransactionJmx "
                                                    + "passed successfully.");

            } else {
                fail("** Test testRunTransactionJmx failed. RESP is "
                      + result[1] + ". It Should be 1.");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** test testRunTransactionExec failed. " + ex.toString());
        }
        
        System.out.println("** Test testRunTransactionJmx "
            + "passed successfully.");

    }
    
    
    /**
     * This function starts up the proxy server, tests that the proxy
     * responding properly to a Transaction Request which has mutibyte uniode 
     * characters in it (> x00FF).
     *
     */
    @Test(enabled = true)
    public final void testMultibyte() {
        String name = "\u0453" + "\u0454" + "\u0455" + "\u0456" + "\u0457";

        System.out.println("** Running Test testMultibyte...");
        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'NAME', 'en_US','Default', 'CT-30-1-011')\r\n";

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            startProxy();
            System.out.println("** Server started");

            System.out.println("\n****************************************");
            System.out.println("** Name used for testing -> ");
            printWithEncoding(name);

            // Send message
            String msg = message.replaceFirst("NAME", name);
            System.out.println("****************************************");
            System.out.println("** Message to Server ->");
            printWithEncoding(msg);
            
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(msg.getBytes("UTF-8"));
            
            socketOut.flush();
            System.out.println("** Message sent to Server.");

            // Read Response
            strmFromSrvr = new InputStreamReader(
                                proxyClientSocket.getInputStream(), "UTF-8");
            
            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();

            System.out.println("\n****************************************");
            System.out.println("** Server Response ->");
            message = totalMessage.toString().replaceFirst("\n\n", "");
            printWithEncoding(message);

            // Let the OS clean up the resources
            Thread.sleep(waitTime);

        } catch (Exception ex) {
            ex.printStackTrace();
            fail("** Test testMultibyte failed. " + ex.toString());
        }

        if(message.indexOf(name) > 0) {
            System.out.println("** Test testMultibyte passed "
                     + "successfully. (index at " + message.indexOf(name) + ")");
        } else {
            fail("** Test testMultibyte failed: " + name + " not in " + message);
        }

        
    }


    /**
     * This function starts up the proxy server, tests that the proxy
     * responding properly to a Transaction Request. Uses Socket Channel.
     *
     */
    @Test(enabled = true)
    public final void testRunTransactionSC() {
        System.out.println("** Running Test testRunTransactionSC...");
        SocketChannel channel = null;
        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'Opertor A', 'en_US','Default', 'CT-30-1-011')\r\n";

        final int bufferSize = 1024;

        // Connect to the server using Socket Channel
        try {
            // ProxyMain.main(new String[] {""});
            startProxy();
            System.out.println("** Server started");

            // Create a channel
            InetSocketAddress socketAddress = new InetSocketAddress(
                                                            hostname, port);
            channel = SocketChannel.open();
            channel.configureBlocking(true);
            channel.connect(socketAddress);

            // Use blocking I/O
            Charset charset = Charset.forName("ISO-8859-1");
            CharsetEncoder encoder = charset.newEncoder();
            CharsetDecoder decoder = charset.newDecoder();

            ByteBuffer buffer = ByteBuffer.allocateDirect(bufferSize);
            CharBuffer charBuffer = CharBuffer.allocate(bufferSize);

            channel.write(encoder.encode(CharBuffer.wrap(message)));
            System.out.println("** Message sent to Server");

            while ((channel.read(buffer)) != -1) {
                buffer.flip();
                decoder.decode(buffer, charBuffer, false);
                charBuffer.flip();
                System.out.println("** Reading... " + charBuffer);
                buffer.clear();
                charBuffer.clear();
            }


            /* Code for Non Blocking I/O, not used now.
            Selector selector = Selector.open();
            channel.register(selector, SelectionKey.OP_CONNECT |
                        SelectionKey.OP_READ | SelectionKey.OP_WRITE);

            while (selector.select(timeOutValue * 1000) > 0) {
                Set readyKeys = selector.selectedKeys();
                Iterator readyItor = readyKeys.iterator();

                ByteBuffer buffer = ByteBuffer.allocateDirect(1024);
                CharBuffer charBuffer = CharBuffer.allocate(1024);

                Charset charset = Charset.forName("ISO-8859-1");
                CharsetDecoder decoder = charset.newDecoder();
                CharsetEncoder encoder = charset.newEncoder();

                while (readyItor.hasNext()) {
                    SelectionKey key = (SelectionKey) readyItor.next();
                    readyItor.remove();
                    SocketChannel keyChannel = (SocketChannel) key.channel();

                    if (keyChannel.isConnectionPending()) {
                        keyChannel.finishConnect();

                    } else if (key.isWritable()) {
                        channel.write(encoder.encode(CharBuffer.wrap(message)));
                        System.out.print("Message sent to server");

                    } else if(key.isReadable()) {
                        keyChannel.read(buffer);
                        buffer.flip();

                        decoder.decode(buffer, charBuffer, false);
                        charBuffer.flip();
                        System.out.print("Response = " + charBuffer);

                        buffer.clear();
                        charBuffer.clear();

                    }
                }
            } */

            System.out.println("** Done Reading.");
            stopProxy();

        } catch (Exception ex) {
            ex.printStackTrace();
            fail("** Test testRunTransactionSC failed. " + ex.toString());

        } finally {
            if (channel != null) {
                try {
                    channel.close();

                } catch (IOException ignored) {
                    // IOException ignored
                }
            }

            // ProxyMain.getApp().shutdown();
        }

        System.out.println("** Test testRunTransactionSC "
                                                + "passed successfully.");
    }


    /**
     * This function opens a socket to the server, idles for
     * "Request.readTimeout.seconds" and then tries to read from the socket. The
     * server should close the socket by then.
     *
     */
    @Test(enabled = true)
    public final void testProxyIdleTimeout() {
        System.out.println("** Running Test testProxyIdleTimeout...");
        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        long timeOutValue = 0;
        final long defaultExtraTimeout = 5;
        final long defaultTimeout = 60;
        final long convFactor = 1000;

        try {
            // Read "Request.readTimeout.seconds" from
            // VoiceLink-proxyServer.properties
            Properties prop = new Properties();
            prop.load(this.getClass().getResourceAsStream(
                "/VoiceLink-proxyServer.properties"));

            timeOutValue = Long.parseLong(prop
                .getProperty("Request.readTimeout.seconds"));
            timeOutValue = timeOutValue + defaultExtraTimeout;

        } catch (Exception i) {
            timeOutValue = defaultTimeout;
        }

        try {
            startProxy();
            System.out.println("** Server started");

            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();

            System.out.println("** Socket Opened. Idling for " + timeOutValue
                + " seconds...");
            Thread.sleep(timeOutValue * convFactor);

            // Read Response
            System.out.println("** Tracking socket availability...");
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            StringBuilder totalMessage = new StringBuilder(1024);
            
            TimeKeeper timer = new TimeKeeper();
            timer.start();

            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            System.out.println("** Server Response: "
                                            + totalMessage.toString());

            timer.stop();
            System.out.println("** Final read() time = " + timer.timeExpiredMs()
                                                         + " ms.");

            stopProxy();

            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP"});

            if (result[0] == 0 && result[1] == 0) {
                System.out.println("** Test testProxyIdleTimeout "
                                        + "passed successfully.");

            } else {
                fail("** Test testProxyIdleTimeout failed. "
                       + "REQ = " + result[0] + " (should be 0) "
                       + "RESP = " + result[1] + " (should be 0).");
            } */


        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyIdleTimeout failed. " + ex.toString());

        } finally {
            // Clean up
            try {
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Throwable t) {
                // Ignore any errors at this point
            }
        }
        
        System.out.println("** Test testProxyIdleTimeout "
            + "passed successfully.");

    }

    /**
     * This function opens a socket to the server, sends an incomplete message
     * and then tries to read from the socket.
     *
     */
    @Test(enabled = true)
    public final void testProxyIncompleteMessage() {
        System.out.println("** Running Test testProxyIncompleteMessage...");
        // Note: no \r\n at the end of string. Server will be waiting for
        // these characters to be read.
        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'Opertor A', 'en_US','Default', 'CT-30-1-011')";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            startProxy();
            System.out.println("** Server started");

            // Send message
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(message.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server");

            // Read Response
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();
            
            System.out.println("** Server Response: "
                                                    + totalMessage.toString());
            
            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP"});
            
            if (result[0] == 0 && result[1] == 0) {
                System.out.println("** Test testProxyIncompleteMessage "
                                        + "passed successfully.");

            } else {
                fail("** Test testProxyIncompleteMessage failed. "
                       + "REQ = " + result[0] + " (should be 0) "
                       + "RESP = " + result[1] + " (should be 0).");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyIncompleteMessage failed. " + ex.toString());

        } finally {
            // Clean up
            try {
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Exception e) {
                // Do nothing
            }

        }
        
        System.out.println("** Test testProxyIncompleteMessage "
            + "passed successfully.");

    }



    /**
     * This function opens a socket to the server, sends a non-supported
     * message and then tries to read from the socket.
     *
     */
    @Test(enabled = true)
    public final void testProxyBogusMessage() {
        System.out.println("** Running Test testProxyBogusMessage...");
        String confMessage = "prTaskLUTBogusMsg('12-05-06 07:55:56', "
            + "" + "'9999999', 'Operator A', 'en_US', 'Default' )\r\n";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            startProxy();
            System.out.println("** Server started");

            // Send message
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(confMessage.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server");

            // Read Response
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();
            
            System.out.println("** Server Response: "
                                               + totalMessage.toString());

            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP",
                                                               "ERRCMD"});

            if (result[0] == 1 && result[1] == 0 && result[2] == 1) {
                System.out.println("** Test testProxyBogusMessage "
                                                    + "passed successfully.");

            } else {
                fail("** Test testProxyBogusMessage failed. "
                    + "REQ = " + result[0] + " (should be 1) "
                    + "RESP = " + result[1] + " (should be 0) "
                    + "ERRCMD = " + result[2] + " (should be 1).");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyBogusMessage failed. " + ex.toString());

        } finally {
            // Clean up
            try {
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Exception e) {
                // Do nothing
            }

        }
        
        System.out.println("** Test testProxyBogusMessage "
            + "passed successfully.");

    }



    /**
     * This function opens a socket to the server, sends an message with
     * wrong number of arguments and then tries to read from the socket.
     *
     */
    @Test(enabled = true)
    public final void testProxyIncorrectArguments() {
        System.out.println("** Running Test testProxyIncorrectArguments...");
        String confMessage = "prTaskLUTCoreConfiguration("
                              + "'testProxyIncorrectArguments')\r\n";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        StringBuilder totalMessage = new StringBuilder(1024);

        try {
            startProxy();
            System.out.println("** Server started");

            // Send message
            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(confMessage.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server");

            // Read Response
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            
            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();

            System.out.println("** Server Response: "
                                             + totalMessage.toString());

            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP",
                                                               "ERRFMT1"});

            if (result[0] == 1 && result[1] == 0 && result[2] == 1) {
                System.out.println("** Test testProxyIncorrectArguments "
                                                + "passed successfully.");

            } else {
                fail("** Test testProxyIncorrectArguments failed. "
                + "REQ = " + result[0] + " (should be 1) "
                + "RESP = " + result[1] + " (should be 0) "
                + "ERRFMT1 = " + result[2] + " (should be 1).");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyIncorrectArguments failed. "
                                                           + ex.toString());

        } finally {
            // Clean up
            try {
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Exception e) {
                // Do nothing
            }

        }
        
        System.out.println("** Test testProxyIncorrectArguments "
            + "passed successfully.");

    }


    /**
     * This function opens a socket to the server, sends message and then reads
     * the response in a delayed manner from the socket.
     *
     */
    @Test(enabled = true)
    public final void testProxyDelayedRead() {
        System.out.println("** Running Test testProxyDelayedRead...");
        String message = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'Opertor A', 'en_US','Default', 'CT-30-1-011')\r\n";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;
        BufferedReader stdInput = null;

        final long sleepBetweenReads = 1000;

        try {
            startProxy();
            System.out.println("** Server started");

            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(message.getBytes());
            socketOut.flush();
            System.out.println("** Message sent to Server");

            // Read Response
            System.out.println("** Tracking socket availability...");
            strmFromSrvr = new InputStreamReader(proxyClientSocket
                .getInputStream());
            stdInput = new BufferedReader(strmFromSrvr);
            StringBuilder totalMessage = new StringBuilder();
            char[] buffer = new char[1];

            TimeKeeper timer = new TimeKeeper();
            timer.start();

            while (stdInput.read(buffer) != -1) {
                totalMessage.append(buffer);
                System.out.println(totalMessage.toString());
                Thread.sleep(sleepBetweenReads);
            }

            timer.stop();
            System.out.println("** Final read() time = " + timer.timeExpiredMs()
                                                       + " ms.");

            stopProxy();

            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP",
                                                                 "SUB"});

            if (result[0] == 1 && result[1] == 1 && result[2] == 1) {
                System.out.println("** Test testProxyDelayedRead "
                                + "passed successfully.");

            } else {
                fail("** Test testProxyDelayedRead failed. "
                    + "REQ = " + result[0] + " (should be 1) "
                    + "RESP = " + result[1] + " (should be 1) "
                    + "SUB = " + result[2] + " (should be 1).");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyDelayedRead failed. " + ex.toString());

        } finally {
            // Clean up
            try {
                stdInput.close();
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Throwable t) {
                // Do nothing
            }

        }

        System.out.println("** Test testProxyDelayedRead "
            + "passed successfully.");
    }

    /**
     * This function opens a socket to the server, sends two messages one after
     * another and tries to read the response from the server.
     *
     */
    @Test(enabled = true)
    public final void testProxyMultiTrx() {
        System.out.println("** Running Test testProxyMultiTrx...");
        String message1 = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999998', 'Opertor A', 'en_US','Default', 'CT-30-1-011')\r\n";

        String message2 = "prTaskLUTCoreConfiguration('12-05-06 07:55:56',"
            + "'9999999', 'Opertor B', 'en_US','Default', 'CT-30-1-011')\r\n";

        Socket proxyClientSocket = null;
        OutputStream socketOut = null;
        InputStreamReader strmFromSrvr = null;

        try {
            startProxy();
            System.out.println("** Server started");

            proxyClientSocket = new Socket(hostname, port);
            socketOut = proxyClientSocket.getOutputStream();

            socketOut.write(message1.getBytes());
            socketOut.flush();
            System.out.println("** Message1 sent to Server");

            Thread.sleep(waitTime);

            socketOut.write(message2.getBytes());
            socketOut.flush();
            System.out.println("** Message2 sent to Server");

            // Read Response
            System.out.println("** Tracking socket availability...");
            strmFromSrvr = new InputStreamReader(
                proxyClientSocket.getInputStream(), "UTF-8");

            final char[] buf = new char[32];
            int read = 0;
            StringBuilder totalMessage = new StringBuilder(1024);
            
            TimeKeeper timer = new TimeKeeper();
            timer.start();

            while ((read = strmFromSrvr.read(buf)) > 0) {                        
                totalMessage.append(buf, 0, read);
                if (totalMessage.lastIndexOf("\n\n") >= 0) {
                    break;
                }
            }

            System.out.println("** Server Response: "
                                            + totalMessage.toString());

            timer.stop();
            System.out.println("** Final read() time = " + timer.timeExpiredMs()
                                                              + " ms.");
            
            // Clean up
            strmFromSrvr.close();
            socketOut.close();
            proxyClientSocket.close();
            stopProxy();

            /* JMX Access Code
            int [] result =  checkResult(new String[] {"REQ", "RESP",
                                                                  "SUB"});
                                                                  
            if (result[0] == 1 && result[1] == 1 && result[2] == 1) {
                System.out.println("** Test testProxyMultiTrx "
                                                + "passed successfully.");

            } else {
                fail("** Test testProxyMultiTrx failed. "
                    + "REQ = " + result[0] + " (should be 1) "
                    + "RESP = " + result[1] + " (should be 1) "
                    + "SUB = " + result[2] + " (should be 1).");
            } */

        } catch (Throwable ex) {
            ex.printStackTrace();
            fail("** Test testProxyMultiTrx failed. " + ex.toString());

        } finally {
            // Clean up
            try {
                strmFromSrvr.close();
                socketOut.close();
                proxyClientSocket.close();

            } catch (Throwable t) {
                // Do nothing
            }

        }

        System.out.println("** Test testProxyMultiTrx "
            + "passed successfully.");

    }


    /**
     * This function tests whether the quote (') character can become part of the
     * excrypted string when shutdown is issued. Quotes are delimeters for
     * the command arguments.
     *
     */
    @Test(enabled = true)
    public final void testXorExcryption() {
        String s = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char [] chars = s.toCharArray();
        
        System.out.println("** Running Test testXorExcryption...");
        
        for(int i = 0; i<=9; i++) {
            for(int j=0; j<s.length(); j++) {
                String res = ProxyUtils.codeDecode(""+i, ""+chars[j]);
                
                int intValue = (int) res.toCharArray()[0];
                if(intValue < 32 || intValue > 126) { res = "Unprintable (" + intValue + ")."; }
                System.out.print(chars[j] + "^" + i + " = " + res);
                
                if("'".equals(res)) {
                    System.out.print("   <==\n");
                    fail("** Test testXorExcryption failed. Quote(') found as a result of XOR.");
                }
                                
                System.out.println("");
            }
        }
        
        System.out.println("\n** Test testXorExcryption passed successfully.");
    }


    /** This Private function checks the value of the Attributes passed in
      * argsToCheck and returns them in the result array.
      *
      * It also shuts down the server if SHUTDOWN is passed as argsToCheck[0].
      *
      * The result array is initialized with a default value of -1
      * to prevent the default array initialization values being returned
      * in case of an exception.
      *
      * @param argsToCheck []
      * @return int []
      */
    @Test(enabled = false)
    private int [] checkResult(final String [] argsToCheck) {
        int [] result = new int[argsToCheck.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = -1;
        }

        try {
            // Set up JMX connection
            JMXServiceURL url = new JMXServiceURL(connectorAddress);
            JMXConnector conn = JMXConnectorFactory.connect(url);
            MBeanServerConnection server = conn.getMBeanServerConnection();

            if ("SHUTDOWN".equals(argsToCheck[0])) {
                server.invoke(new ObjectName(controlObjKey), "shutdown",
                                                              null, null);
                conn.close();
                return null;
            }

            // Read the attribute(s), if not shutdown
            ObjectName statObj = new ObjectName(statObjKey);
            MBeanInfo mBeanInfo = server.getMBeanInfo(statObj);
            MBeanAttributeInfo[] attribsInfo = mBeanInfo.getAttributes();
            for (int i = 0; i < attribsInfo.length; i++) {
                MBeanAttributeInfo mbeanAttrib = attribsInfo[i];
                Object value = server.getAttribute(statObj, mbeanAttrib
                    .getName());

                System.out.println("** Attrib Name = "
                    + mbeanAttrib.getName() + "  " + value.toString());

                for (int j = 0; j < argsToCheck.length; j++) {
                    if (argsToCheck[j].equals(mbeanAttrib.getName())) {
                        result[j] = Integer.parseInt(value.toString());
                    }
                }
            }

            conn.close();

        } catch (Exception e) {
            System.out.println("** Exception in checkResult : " + e.toString());
            return result;
        }

        return result;
    }

    /**
     * This function prints a string in unicode 16 format.
     * @param s String to print
     */
    private void printWithEncoding(String s) {
        byte[] byteArray = null;
        
        try {
            byteArray = s.getBytes("UTF-16");
          
            System.out.print("String as UTF-16 (" + byteArray.length + "): " );
            for( int ndx=0; ndx<byteArray.length; ndx++ ) {
                // Print the current byte AND the next byte
                System.out.print( Integer.toHexString(byteArray[ndx++] ) 
                               + Integer.toHexString(byteArray[ndx] ) + "_");
            }        
            System.out.print("\n");

            String tmp = new String(s.getBytes("UTF-8"), "UTF-8");
            byteArray = tmp.getBytes("UTF-8");

            System.out.print("String as UTF-8 (" + byteArray.length + "): " );
            for( int ndx = 0; ndx < byteArray.length; ndx++ ) {
                System.out.print(Integer.toHexString(byteArray[ndx]) + "_" );
            }

            System.out.println("\n****************************************\n");
        
        } catch( UnsupportedEncodingException  e ) {
            System.out.println( "Exception: "  + e.toString());
        }
        
        return;
    }


    /**
     * This function starts up the server.
     * @throws Exception if an error occurs
     */
    @Test(enabled = false)
    private void startProxy() throws Exception {
        String basePath = System.getProperty("PROXY_BASE_PATH");
        if(basePath == null || basePath.length() <= 0) {
            basePath = "C:/DAich/ProxyServer/bin";
            System.out.println("** PROXY_BASE_PATH is not set. Using \"" + basePath + "\".");
        }

        InputStreamReader strmFromSrvr = null;
        BufferedReader stdInput = null;

        // Determine the startup script based on OS
        String tempStr = "";
        if (System.getProperty("os.name").toLowerCase().startsWith("windows")) {
            tempStr += basePath + "/startProxy.bat";

        } else {
            tempStr = "/startProxy.sh";
        }

        // Startup Proxy Server
        p = Runtime.getRuntime().exec(tempStr);
        strmFromSrvr = new InputStreamReader(p.getInputStream());
        stdInput = new BufferedReader(strmFromSrvr);

        while ((tempStr = stdInput.readLine()) != null) {
            System.out.println(tempStr);
            if (tempStr.indexOf("ERROR") > 0) {
                System.out.println("** Error starting up server.");
                System.exit(-1);
            }

            // Without this line, the program will wait infinitely
            if (tempStr.indexOf("Ready to receive client requests") > 0) {
                break;
            }
        }
        stdInput.close();
        strmFromSrvr.close();

        return;
    }


    /**
     * This function Stops the server.
     * @throws Exception if an error occurs
     */
    @Test(enabled = false)
    private void stopProxy() throws Exception {
        // checkResult(new String[] {"SHUTDOWN"});
        
        String trxName = "prTaskODRShutdownProxyServer";
        String currTime = "" + System.currentTimeMillis();
                
        String shutDowmMsg = trxName + "('"
                             + ProxyUtils.codeDecode(currTime, trxName)
                             + "')\r\n";
        try {
            Socket proxyClientSocket = new Socket(hostname, port);
            OutputStream socketOut = proxyClientSocket.getOutputStream();
            socketOut.write(shutDowmMsg.getBytes());
            socketOut.flush();
        } catch (Exception e) {
            // Do Nothing
        }
        if(p != null) {
            p.destroy();
            p = null;
        }
        
        
        Thread.sleep(waitTime);
        System.out.println("** Server Shutdown Complete.");
    }
}
