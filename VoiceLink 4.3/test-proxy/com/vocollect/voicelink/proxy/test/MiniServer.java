/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import com.vocollect.voicelink.proxy.NIOChannelHelper;
import com.vocollect.voicelink.proxy.exceptions.VLProxyIOException;
import com.vocollect.voicelink.proxy.exceptions.VLProxyInterruptionException;

import java.io.IOException;
import java.io.OutputStream;
import java.net.BindException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.spi.SelectorProvider;

/**
 * Tests NIOChannel Helper.
 * 
 * @author khazra
 */
public class MiniServer extends Thread {

    private NIOChannelHelper channelHelper;

    private Selector serverSelector;

    private ServerSocketChannel serverChannel;

    private SelectionKey acceptSelectionKey;

    private int serverPort = 15004;

    private final String encoding = "UTF-8";

    private final long timeOutForRead = 50000;

    private final long timeOutForWrite = 50000;

    /**
     * 
     * Constructor.
     * @param port in which this server will listen.
     */
    public MiniServer(int port) {
        this.serverPort = port;
    }

    /**
     * Getter for the serverChannel property.
     * @return Channel value of the property
     */
    public ServerSocketChannel getServerChannel() {
        return serverChannel;
    }

    /**
     * Setter for the serverChannel property.
     * @param serverChannel the new serverChannel value
     */
    public void setServerChannel(ServerSocketChannel serverChannel) {
        this.serverChannel = serverChannel;
    }

    /**
     * Getter for the channelHelper property.
     * @return NIOChannelHelper value of the property
     */
    public NIOChannelHelper getChannelHelper() {
        return channelHelper;
    }

    /**
     * Setter for the channelHelper property.
     * @param channelHelper the new channelHelper value
     */
    public void setChannelHelper(NIOChannelHelper channelHelper) {
        this.channelHelper = channelHelper;
    }

    /**
     * Getter for the serverSelector property.
     * @return Selector value of the property
     */
    public Selector getServerSelector() {
        return serverSelector;
    }

    /**
     * Setter for the serverSelector property.
     * @param serverSelector the new serverSelector value
     */
    public void setServerSelector(Selector serverSelector) {
        this.serverSelector = serverSelector;
    }

    /**
     * Getter for the acceptSelectionKey property.
     * @return SelectionKey value of the property
     */
    public SelectionKey getAcceptSelectionKey() {
        return acceptSelectionKey;
    }

    /**
     * Setter for the acceptSelectionKey property.
     * @param acceptSelectionKey the new acceptSelectionKey value
     */
    public void setAcceptSelectionKey(SelectionKey acceptSelectionKey) {
        this.acceptSelectionKey = acceptSelectionKey;
    }

    /**
     * Starts the Mini server in port defined in serverPort constant.
     * 
     * @throws IOException when there is any IO problem with serverChannel
     */
    public void startMiniServer() throws IOException {
        InetSocketAddress localMechineAddress = new InetSocketAddress(
            this.serverPort);

        this.setServerChannel(ServerSocketChannel.open());
        this.getServerChannel().configureBlocking(false);

        while (true) {
            try {
                this.getServerChannel().socket().bind(localMechineAddress);
                break;
            } catch (BindException ex) {
                try {
                    this.sleep(50);
                } catch (InterruptedException ex1) {
                    throw new IOException("Error starting server");
                }
            }
        }

        this.setServerSelector(SelectorProvider.provider().openSelector());
        this.setAcceptSelectionKey(this.getServerChannel().register(
            this.getServerSelector(), SelectionKey.OP_ACCEPT));
    }

    /**
     * Responds to the selection from client.
     * 
     * @throws IOException when there is any error in communication with client
     */
    public void respondToSelection()
        throws IOException, VLProxyInterruptionException, VLProxyIOException {
        try {
            if (this.getServerSelector().select() > 0
                && this.getAcceptSelectionKey().isAcceptable()) {
                this.setChannelHelper(new NIOChannelHelper(encoding, "\r\n"));
                this.getChannelHelper().setChannel(
                    this.getServerChannel().accept());
            }
        } catch (ClosedByInterruptException ex) {
            throw new VLProxyInterruptionException();
        }
    }

    /**
     * Reads data from client with the help of NIOChannelHelper.
     * @return Data in StringBuffer
     * @throws IOException if there is any communication error when it reads
     *             data from client
     */
    public StringBuilder readDataFromClient()
        throws IOException, VLProxyInterruptionException, VLProxyIOException {
        ByteBuffer dataBuffer = ByteBuffer.allocateDirect(1024);
        return this.getChannelHelper().readRequest(dataBuffer, timeOutForRead);
    }

    /**
     * Writes data to client with the help of NIOChannelHelper.
     * @param data To send to client.
     * @throws IOException if there is any communication error with client.
     */
    public void writeDataToClient(StringBuilder data)
        throws IOException, VLProxyInterruptionException, VLProxyIOException {
        ByteBuffer dataBuffer = ByteBuffer.allocateDirect(1024);
        this.getChannelHelper().writeData(data, dataBuffer, timeOutForWrite);
    }

    public void stopMiniServer()
        throws IOException, VLProxyInterruptionException, VLProxyIOException {
        this.getChannelHelper().close();
    }

    /**
     * Run method.
     */
    public void run() {
        try {
            startMiniServer();

            System.out.println("server started");
            respondToSelection();

            // writeDataToClient(new StringBuilder("Connected :"));

            StringBuilder echoString = new StringBuilder();

            System.out.println("reading data");
            StringBuilder clientData = readDataFromClient();

            System.out.println(clientData);
            echoString.append(clientData);

            System.out.println("writing data to client");
            writeDataToClient(echoString);

            System.out.println("stopping miniserver");
            stopMiniServer();

            System.out.println("should be stopped");
        } catch (VLProxyInterruptionException ex) {
            try {
                if (this.getChannelHelper() != null) {
                    this.getChannelHelper().close();
                }
            } catch (VLProxyIOException ex1) {
                throw new Error("Cannot close the channel helper");
            }
        } catch (VLProxyIOException ex) {
            try {
                if (this.getChannelHelper() != null) {
                    this.getChannelHelper().close();
                }
            } catch (VLProxyIOException ex1) {
                throw new Error("Cannot close the channel helper");
            }
        } catch (IOException ex) {
            System.out.println("[SDT] Caught IOException: cause="
                + ex.getCause() + ", msg=" + ex.getMessage());
            ex.printStackTrace();
            throw new Error("IOException occured while running mini server");
        }
    }

    /**
     * Main method to start the program
     * 
     * @param args Command line arguments
     * @throws Exception if there is any problem starting the server or
     *             responding to client
     */
    public static void main(String[] args) throws Exception {

        // MiniServer miniServer = new MiniServer(15004);
        // miniServer.start();
        //
        // long connectionCount = NIOChannelHelper.getChannelsActiveCount();
        //
        // InetSocketAddress address = new InetSocketAddress("localhost",
        // 15004);
        // Socket client = new Socket();
        // client.connect(address, 5000);
        //
        // OutputStream out = client.getOutputStream();
        // for (int i = 0; i < 1030; i++) {
        // out.write("H".getBytes());
        // }
        // out.write("\r\n".getBytes());
        //
        // InputStream in = client.getInputStream();
        // byte[] dataBuffer = new byte[1024];
        // in.read(dataBuffer);
        // System.out.println(new String(dataBuffer));

//        MiniServer miniServer1 = new MiniServer(15004);
//        miniServer1.start();
//
//        InetSocketAddress address1 = new InetSocketAddress("localhost", 15004);
//        Socket client1 = new Socket();
//        client1.connect(address1, 5000);
//
//        OutputStream out = client1.getOutputStream();
//        out.write("Hello".getBytes());
//
//        miniServer1.interrupt();

        // MiniServer miniServer = new MiniServer(15004); miniServer.start();
        //         

        /*
         * MiniServer server = new MiniServer(15003); server.start();
         * 
         * InetSocketAddress address = new InetSocketAddress("localhost",
         * 15003); Socket client = new Socket(); client.connect(address, 5000);
         * 
         * OutputStream out = client.getOutputStream();
         * out.write("Hello".getBytes()); long channelCountBefore =
         * NIOChannelHelper.getChannelsActiveCount(); client.close(); long
         * channelCountAfter = NIOChannelHelper.getChannelsActiveCount();
         * 
         * if (channelCountBefore != channelCountAfter) {
         * System.out.println("Channel Count before : " + channelCountBefore + "
         * and after : " + channelCountAfter + "NIOChannelHelper not detecting
         * client close"); }
         */

        /*
         * MiniServer miniServer = new MiniServer(15004); miniServer.start();
         * 
         * InetSocketAddress address1 = new InetSocketAddress("localhost",
         * 15004); Socket client1 = new Socket(); client1.connect(address1,
         * 5000);
         * 
         * OutputStream out = client1.getOutputStream();
         * 
         * out.write("Hello".getBytes()); Thread.sleep(1000);
         * out.write("\r\n".getBytes());
         */
        
        try {
            MiniServer server = new MiniServer(15007);
            server.start();

            long channelCountBefore = NIOChannelHelper.getChannelsActiveCount();

            InetSocketAddress address = new InetSocketAddress(
                "localhost", 15007);
            Socket client = new Socket();
            client.connect(address, 5000);

            OutputStream out = client.getOutputStream();
            out.write("Hello".getBytes());
            server.interrupt();

            long channelCountAfter = NIOChannelHelper.getChannelsActiveCount();

            if (channelCountAfter != channelCountBefore) {
                System.out.println("Channel Count before : " + channelCountBefore
                    + " and after : " + channelCountAfter
                    + " NIOChannelHelper is not interruptable");
            }

        } catch (Throwable ex) {
            System.out.println(ex);
            System.out.println("NIOChannelHelper is not interruptable");
        }
    }
}
