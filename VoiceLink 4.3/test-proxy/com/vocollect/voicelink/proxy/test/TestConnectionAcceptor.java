/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.proxy.test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.fail;

import com.vocollect.voicelink.proxy.ConnectionAcceptor;
import com.vocollect.voicelink.proxy.ProxyMain;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;

import org.testng.annotations.Test;

/**
 * Tests the connection acceptor.
 * 
 * @author khazra
 */
@Test(groups = { "unit" }, sequential = true)
public class TestConnectionAcceptor extends BaseProxyTestCase {

    /**
     * Test case to test that port setter method is working properly.
     * 
     */
    
    public void testPortSetter() {
        ConnectionAcceptor connectionAcceptor = (ConnectionAcceptor) getAppContext()
            .getBean("acceptor");

        String[] thePorts = { "1880", "1881", "1882" };
        connectionAcceptor.setPortsViaStringArray(thePorts);

        Integer[] returnedPorts = connectionAcceptor.getPorts();
        assertEquals(returnedPorts[0], new Integer(1880));
        assertEquals(returnedPorts[1], new Integer(1881));
        assertEquals(returnedPorts[2], new Integer(1882));
    }

    /**
     * Tests that createConnectionListener is creating the listener properly.
     * 
     */
    
    public void testCreateConnectionListener() {
        ConnectionAcceptor connectionAcceptor = (ConnectionAcceptor) getAppContext()
            .getBean("acceptor");

        try {
            ServerSocketChannel serverSocketChannel = connectionAcceptor
                .createConnectionListener(1881);

            if (!(serverSocketChannel instanceof ServerSocketChannel)) {
                fail();
            }

            serverSocketChannel.close();
        } catch (IOException ex) {
            fail();
        }
    }

    /**
     * Tests that createConnectionListener throwing IOException when the port is
     * already blocked.
     * 
     */
    
    public void testCreateConnectionListenerFailed() {
        ConnectionAcceptor connectionAcceptor = (ConnectionAcceptor) getAppContext()
            .getBean("acceptor");

        ServerSocketChannel serverSocketChannel = null;
        try {
            serverSocketChannel = connectionAcceptor
                .createConnectionListener(1881);

            serverSocketChannel = connectionAcceptor
                .createConnectionListener(1881);

            serverSocketChannel.close();
        } catch (IOException ex) {
            return;
        } catch (Exception ex) {
            fail();
        } finally {
            try {
                serverSocketChannel.close();
            } catch (IOException ex) {
                fail();
            }
        }

        fail();
    }
}
