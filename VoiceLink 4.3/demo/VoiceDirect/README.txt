Instructions to extend this tool to applications such as Selection, Replenishment, PutAway and Lineloading.

STEP 1:
Create a new folder in this file's parent directory with the application you will be demoing.

STEP 2:
Copy the .template files of this file's parent directory to a the folder created in STEP 1.

STEP 3:  
Rename the files removing the .template so that you have two .txt files and 1 .ppt file.  

STEP 4:
Build your data Queues and PPT presentation using renamed ppt file and TaskMate.

STEP 5:
If you have not already, register the dll using the Batch file contained in this file;s parent directory and name your slides accordingly

STEP 6:
Add your slides to TaskMateSlideParams.txt (if needed) otherwise rename slides to match procedure calls.

STEP 7:
Add your requirements if any to TaskMateDataParams.txt (ie required password or parameter matching).
