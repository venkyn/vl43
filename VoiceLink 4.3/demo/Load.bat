@Echo off
cls

REM ****************************************
REM *Save original PATH
REM ****************************************
SET OLDPATH=%PATH%

REM ****************************************
REM Intialize Enviroment
REM ****************************************
SET ANT_HOME=%CD%\ant
SET JAVA_HOME=%CD%\java
SET PATH=%ANT_HOME%\bin;%JAVA_HOME%\bin

SET DemoFile=%1
SET Specified=Y

REM ****************************************
REM *Check if filename was specified
REM ****************************************
IF "%1"=="" GOTO FileNotSpecified
GOTO CheckExist

:FileNotSpecified
SET Specified=N
Echo Files Available....
Echo ===================
dir data /B /A-D
Echo ===================

SET /P DemoFile=Enter File Name (Enter=DemoData.xml): 
IF "%DemoFile%"=="" SET DemoFile=DemoData.xml

REM ****************************************
REM *Check if file exists if not goto error
REM ****************************************
:CheckExist
IF NOT EXIST DATA\%DemoFile% GOTO NotFound

REM ****************************************
REM *File exists so load it
REM ****************************************
ECHO Loading File "Data\%DemoFile%", Please Wait...
CALL ANT -q -f=build-database.xml db-demo-import-complete -Ddbunit.export.file=%DemoFile% 
GOTO End

REM ****************************************
REM *File didn't exists tell user
REM ****************************************
:NotFound
ECHO File Data\%1 not found, try again
GOTO End

:End
SET DemoFile=

IF "%Specified%"=="N" Pause

SET Specified=

REM ****************************************
REM *Restore original PATH
REM ****************************************
PATH %OLDPATH%
