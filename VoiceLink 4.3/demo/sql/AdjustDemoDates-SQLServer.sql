DECLARE @dnow AS DATETIME
DECLARE @dmax AS DATETIME
DECLARE @dmax1 As DATETIME
DECLARE @dmax2 AS DATETIME
DECLARE @dmax3 AS DATETIME
DECLARE @dDiff AS INTEGER
DECLARE @dDiff1 AS INTEGER
DECLARE @dDiff2 as INTEGER
DECLARE @dDiff3 AS INTEGER

--
--This will force the import and export to regenerate
--the site directories upon service restart.
--

UPDATE voc_system_properties
	SET [value] = 1 
	WHERE systemPropertyId = -1005

--
--Start of adjust dates
--

SET @dnow = getDate()

SELECT @dmax = MAX(createdDate)
FROM arch_pts_container_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_pts_container_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM arch_pts_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_pts_containers
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM arch_pts_license_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE arch_pts_license_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_pts_license_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE arch_pts_license_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM arch_pts_licenses
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE arch_pts_licenses
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_pts_licenses
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE arch_pts_licenses
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(putTime)
FROM arch_pts_put_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE arch_pts_put_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_pts_put_details
	SET putTime = DATEADD(HOUR, @dDiff1, putTime)
	WHERE putTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(putTime)
FROM arch_pts_puts
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE arch_pts_puts
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_pts_puts
	SET putTime = DATEADD(HOUR, @dDiff1, putTime)
	WHERE putTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM arch_sel_assignment_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE arch_sel_assignment_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_sel_assignment_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE arch_sel_assignment_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(deliveryDate)
FROM arch_sel_assignments
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE arch_sel_assignments
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_sel_assignments
	SET deliveryDate = DATEADD(HOUR, @dDiff1, deliveryDate)
	WHERE deliveryDate IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM arch_sel_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_sel_containers
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(pickTime)
FROM arch_sel_pick_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE arch_sel_pick_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_sel_pick_details
	SET pickTime = DATEADD(HOUR, @dDiff1, pickTime)
	WHERE pickTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(pickTime), @dmax2 = MAX(shortedDate)
FROM arch_sel_picks
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE arch_sel_picks
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE arch_sel_picks
	SET pickTime = DATEADD(HOUR, @dDiff1, pickTime)
	WHERE pickTime IS NOT NULL
UPDATE arch_sel_picks
	SET shortedDate = DATEADD(HOUR, @dDiff2, shortedDate)
	WHERE shortedDate IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM core_break_types
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_break_types
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_items
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_items
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_location_items
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_location_items
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_locations
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_locations
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(expirationDate)
FROM core_lot_import_data
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_lot_import_data
	SET expirationDate = DATEADD(HOUR, @dDiff, expirationDate)


SELECT @dmax = MAX(createdDate)
FROM core_lots
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)


UPDATE core_lots
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE core_lots
	SET expirationDate = @dnow + 7



SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM core_operator_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE core_operator_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE core_operator_labor
	SET startTime = DATEADD(Hour, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE core_operator_labor
	SET endTime = DATEADD(Hour, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(signOnTime), @dmax2 = MAX(signOffTime)
FROM core_operators
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE core_operators
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE core_operators
	SET signOnTime = DATEADD(HOUR, @dDiff1, signOnTime)
	WHERE signOnTime IS NOT NULL
UPDATE core_operators
	SET signOffTime = DATEADD(HOUR, @dDiff2, signOffTime)
	WHERE signOffTime IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM core_printers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_printers
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_reason_codes
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_reason_codes
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_regions
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_regions
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_work_group_functions
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_work_group_functions
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM core_work_groups
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE core_work_groups
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
	
	
SELECT @dmax = MAX(createdDate)
FROM arch_loading_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_loading_containers
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate) 	

    
SELECT @dmax = MAX(createdDate)
FROM arch_loading_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_loading_labor
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate) 

    
SELECT @dmax = MAX(createdDate)
FROM arch_loading_routes
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_loading_routes
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)     

    
SELECT @dmax = MAX(createdDate)
FROM arch_loading_stops
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_loading_stops
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)      

    
SELECT @dmax = MAX(createdDate)
FROM loading_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE loading_containers
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)

    
SELECT @dmax = MAX(createdDate)
FROM loading_routes
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE loading_routes
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)    
    
    
SELECT @dmax = MAX(createdDate)
FROM loading_stops
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE loading_stops
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)     


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM loading_routes_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE loading_routes_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE loading_routes_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE loading_routes_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL
	    
	
SELECT @dmax = MAX(createdDate)
FROM arch_cc_assignment_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_cc_assignment_details
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate) 	
	

SELECT @dmax = MAX(createdDate)
FROM arch_cc_assignments
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_cc_assignments
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate) 	

    
SELECT @dmax = MAX(createdDate)
FROM arch_cc_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE arch_cc_labor
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate) 
    
SELECT @dmax = MAX(createdDate)
FROM cc_assignments
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE cc_assignments
    SET createdDate = @dmax  

UPDATE cc_assignments
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)     
    
SELECT @dmax = MAX(createdDate)
FROM cc_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE cc_details
    SET createdDate = @dmax  
    
UPDATE cc_details
    SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
    
SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM cc_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE cc_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE cc_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE cc_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL    
    
  
SELECT @dmax = MAX(createdDate), @dmax1 = MAX(actionTime)
FROM lineload_carton_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE lineload_carton_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE lineload_carton_details
	SET actionTime = DATEADD(HOUR, @dDiff1, actionTime)
	WHERE actionTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(loadTime)
FROM lineload_cartons
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE lineload_cartons
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE lineload_cartons
	SET loadTime = DATEADD(HOUR, @dDiff1, loadTime)
	WHERE loadTime IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM lineload_pallets
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE lineload_pallets
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(dateOpened), @dmax2 = MAX(dateClosed)
FROM lineload_route_stops
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE lineload_route_stops
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE lineload_route_stops
	SET dateOpened = DATEADD(HOUR, @dDiff1, dateOpened)
	WHERE dateOpened IS NOT NULL
UPDATE lineload_route_stops
	SET dateClosed = DATEADD(HOUR, @dDiff2, dateClosed)
	WHERE dateClosed IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(putTime)
FROM pts_container_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE pts_container_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE pts_container_details
	SET putTime = DATEADD(HOUR, @dDiff1, putTime)
	WHERE putTime IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM pts_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE pts_containers
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM pts_license_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE pts_license_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE pts_license_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE pts_license_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM pts_licenses
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE pts_licenses
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE pts_licenses
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE pts_licenses
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(putTime)
FROM pts_put_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE pts_put_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE pts_put_details
	SET putTime = DATEADD(HOUR, @dDiff1, putTime)
	WHERE putTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(putTime)
FROM pts_puts
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE pts_puts
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE pts_puts
	SET putTime = DATEADD(HOUR, @dDiff1, putTime)
	WHERE putTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(putTime)
FROM putaway_license_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE putaway_license_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE putaway_license_details
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE putaway_license_details
	SET putTime = DATEADD(HOUR, @dDiff2, putTime)
	WHERE putTime IS NOT NULL

SELECT @dmax = MAX(dateReceived)
FROM putaway_license_import_data
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE putaway_license_import_data
	SET dateReceived = DATEADD(HOUR, @dDiff, dateReceived)
	WHERE dateReceived IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(putTime), @dmax3 = MAX(dateReceived)
FROM putaway_licenses
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)
SET @dDiff3 = DATEDIFF(HOUR, @dmax3, @dnow)

UPDATE putaway_licenses
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE putaway_licenses
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE putaway_licenses
	SET putTime = DATEADD(HOUR, @dDiff2, putTime)
	WHERE putTime IS NOT NULL
UPDATE putaway_licenses
	SET dateReceived = DATEADD(HOUR, @dDiff3, dateReceived)
	WHERE dateReceived IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(replenishTime)
FROM replen_replenishment_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE replen_replenishment_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE replen_replenishment_details
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE replen_replenishment_details
	SET replenishTime = DATEADD(HOUR, @dDiff2, replenishTime)
	WHERE replenishTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime), @dmax3 = MAX(dateReceived)
FROM replen_replenishments
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)
SET @dDiff3 = DATEDIFF(HOUR, @dmax3, @dnow)

UPDATE replen_replenishments
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE replen_replenishments
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE replen_replenishments
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL
UPDATE replen_replenishments
	SET dateReceived = DATEADD(HOUR, @dDiff3, dateReceived)
	WHERE dateReceived IS NOT NULL


SELECT @dmax = MAX(deliveryDate)
FROM sel_assignment_import_data
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE sel_assignment_import_data
	SET deliveryDate = DATEADD(HOUR, @dDiff, deliveryDate)
	WHERE deliveryDate IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(endTime)
FROM sel_assignment_labor
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE sel_assignment_labor
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE sel_assignment_labor
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE sel_assignment_labor
	SET endTime = DATEADD(HOUR, @dDiff2, endTime)
	WHERE endTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(startTime), @dmax2 = MAX(deliveryDate), @dmax3 = MAX(endTime)
FROM sel_assignments
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)
SET @dDiff3 = DATEDIFF(HOUR, @dmax3, @dnow)

UPDATE sel_assignments
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE sel_assignments
	SET startTime = DATEADD(HOUR, @dDiff1, startTime)
	WHERE startTime IS NOT NULL
UPDATE sel_assignments
	SET endTime = DATEADD(HOUR, @dDiff3, endTime)
	WHERE endTime IS NOT NULL
UPDATE sel_assignments
	SET deliveryDate = DATEADD(HOUR, @dDiff2, deliveryDate)
	WHERE deliveryDate IS NOT NULL

DECLARE cur CURSOR FOR 
SELECT route  FROM sel_assignments GROUP BY route ORDER BY MIN(assignmentNumber)
OPEN cur
DECLARE @rout VARCHAR(50),@dt AS DATETIME,@ct AS INT, @minVal AS INT
SET @minVal = 0
FETCH NEXT FROM cur INTO @rout
WHILE (@@FETCH_STATUS = 0)
BEGIN
SET @minVal = @minVal + 45
       UPDATE sel_assignments SET departuredatetime =DATEADD(MI,@minVal,GETDATE()) WHERE route=@rout
    FETCH NEXT FROM cur INTO @rout
END
CLOSE cur
DEALLOCATE cur

	


SELECT @dmax = MAX(createdDate)
FROM sel_containers
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE sel_containers
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(pickTime)
FROM sel_pick_details
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE sel_pick_details
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE sel_pick_details
	SET pickTime = DATEADD(HOUR, @dDiff1, pickTime)
	WHERE pickTime IS NOT NULL


SELECT @dmax = MAX(createdDate), @dmax1 = MAX(pickTime), @dmax2 = MAX(shortedDate)
FROM sel_picks
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)
SET @dDiff2 = DATEDIFF(HOUR, @dmax2, @dnow)

UPDATE sel_picks
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)
UPDATE sel_picks
	SET pickTime = DATEADD(HOUR, @dDiff1, pickTime)
	WHERE pickTime IS NOT NULL
UPDATE sel_picks
	SET shortedDate = DATEADD(HOUR, @dDiff2, shortedDate)
	WHERE shortedDate IS NOT NULL


SELECT @dmax = MAX(createdDate)
FROM sel_sum_prompt_def
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE sel_sum_prompt_def
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(createdDate)
FROM sel_sum_prompt_loc
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE sel_sum_prompt_loc
	SET createdDate = DATEADD(HOUR, @dDiff, createdDate)


SELECT @dmax = MAX(creationDateTime)
FROM voc_notification
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE voc_notification
	SET creationDateTime = DATEADD(HOUR, @dDiff, creationDateTime)


SELECT @dmax = MAX(start_time), @dmax1 = MAX(finish_time)
FROM voc_job_history
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)
SET @dDiff1 = DATEDIFF(HOUR, @dmax1, @dnow)

UPDATE voc_job_history
	SET start_time = DATEADD(HOUR, @dDiff, start_time)
	WHERE start_time IS NOT NULL
UPDATE voc_job_history
	SET finish_time = DATEADD(HOUR, @dDiff1, finish_time)
	WHERE finish_time IS NOT NULL


SELECT @dmax = MAX(last_run)
FROM voc_jobs
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE voc_jobs
	SET last_run = DATEADD(HOUR, @dDiff, last_run)
	WHERE last_run IS NOT NULL


SELECT @dmax = MAX(last_login)
FROM voc_user
SET @dDiff = DATEDIFF(HOUR, @dmax, @dnow)

UPDATE dbo.voc_user
	SET last_login = DATEADD(HOUR, @dDiff, last_login)
	WHERE last_login IS NOT NULL
