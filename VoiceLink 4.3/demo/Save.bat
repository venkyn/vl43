@Echo Off
cls

REM ****************************************
REM *Save original PATH
REM ****************************************
SET OLDPATH=%PATH%

REM ****************************************
REM Intialize Enviroment
REM ****************************************
SET ANT_HOME=%CD%\Ant
SET JAVA_HOME=%CD%\Java
SET PATH=%ANT_HOME%\bin;%JAVA_HOME%\bin

SET Answer=
SET DemoFile=%1
SET Specified=Y

REM ****************************************
REM *Check if filename was specified
REM ****************************************
IF "%DemoFile%"=="" GOTO FileNotSpecified
GOTO CheckExist

:FileNotSpecified
SET Specified=N
SET /P DemoFile=Enter File Name: 

REM ****************************************
REM *Check if file exists
REM ****************************************
:CheckExist
IF NOT EXIST Data\%DemoFile% GOTO ExportData

REM ****************************************
REM *File exists check if we should overwrite
REM ****************************************
SET /P Answer=File "%DemoFile%" already exist do you want to overwrite it (Y/N)?
IF "%Answer%"=="" GOTO DoNotExport
IF "%Answer%"=="Y" GOTO ExportData
IF "%Answer%"=="y" GOTO ExportData

GOTO DoNotExport

REM ****************************************
REM *Export/Save data to file specified
REM ****************************************
:ExportData
ECHO Saving File "Data\%DemoFile%", Please Wait...
CALL ANT -q -f=build-database.xml db-demo-export-complete -Ddbunit.export.file=%DemoFile%
GOTO End

REM ****************************************
REM *Prompt operation cancled and exit
REM ****************************************
:DoNotExport
ECHO Operation Canceled
GOTO End

:End

SET Answer=
SET DemoFile=

IF "%Specified%"=="N" Pause

SET Specified=

REM ****************************************
REM *Restore original PATH
REM ****************************************
PATH %OLDPATH%
