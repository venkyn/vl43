create sequence ARCH_CONTAINER_KEY_SEQUENCE start with 8;
create sequence VOC_CONTAINER_KEY_SEQUENCE start with 2;
create sequence VOC_GROUPNUM_KEY_SEQUENCE start with 2;
create sequence VOC_PRIMARY_KEY_SEQUENCE start with 2;
create sequence VOC_PTS_CONTAINER_KEY_SEQUENCE start with 2;

