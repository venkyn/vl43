/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;

import java.util.Date;

/**
 * Manager used to determine which PTS Licenses should be purged and or
 * archived.
 *
 * @author mlashinsky
 */
public interface PtsContainerToArchiveManagerRoot {

    /**
     * Mark PTS License records for purging(2) or archiving(1).
     * @param status - status to look for
     * @param olderThan - how old to look for
     * @param archive - whether or not to mark for archive
     * @return - number archived
     */
    public Integer executeMarkForPurge(PtsContainerStatus status,
                                       Date olderThan,
                                       boolean archive);

    /**
     * Archive specified number of PTS Licenses.
     *
     * @param decorator - number of PTS Licenses to list and archive
     * @return - number archived.
     */
    public int executeArchive(QueryDecorator decorator);

    /**
     * Purges PTS Licenses based on date and status.
     *
     * @param decorator - number of PTS Licenses to list and archive
     * @return - number archived.
     */
    public int executePurgePtsContainer(QueryDecorator decorator);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 