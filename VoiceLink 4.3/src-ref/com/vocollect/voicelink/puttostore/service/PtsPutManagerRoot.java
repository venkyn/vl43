/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.puttostore.dao.PtsPutDAO;
import com.vocollect.voicelink.puttostore.model.PtsPut;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Put-related operations.
 *
 * @author mnichols
 */
public interface PtsPutManagerRoot extends
    GenericManager<PtsPut, PtsPutDAO>, DataProvider {

    /**
     * Used for reseting state during unit tests.
     */
    public void initializeState();



    /**
     * List all puts for the given group number.
     *
     * @param groupNumber - group number on license object.
     * @return - list of puts for the group
     * @throws DataAccessException - on db exception
     */
    public List<PtsPut> listPutsForGroup(Long groupNumber)
                                    throws DataAccessException;

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 