/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.puttostore.dao.PtsCustomerLocationDAO;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for PtsCustomerLocation-related operations.
 *
 * @author mnichols
 */
public interface PtsCustomerLocationManagerRoot extends
    GenericManager<PtsCustomerLocation, PtsCustomerLocationDAO>, DataProvider {

    /**
     * Used for reseting state during unit tests.
     */
    public void initializeState();


    /**
     * Find customer location for customer number and location.
     *
     * @param customerNumber - number to find.
     * @param locationId - location where stored
     * @return - customer location if found.
     */
    PtsCustomerLocation findCustomerLocationByNumberLocation(String customerNumber,
                String locationId);


    /**
     * Find customer location by the core location scanned verification value.
     *
     * @param scannedVerification - the scanned value to search by.
     * @return - customer location object.
     */
    PtsCustomerLocation findCustomerLocationByScanned(String scannedVerification);


    /**
     * Find customer location by the core location spoken verification
     * and check digits value.
     *
     * @param spokenVerification - the spoken value to search by.
     * @param checkDigits - the check digits to search by.
     * @return - customer location object.
     */
    PtsCustomerLocation findCustomerLocationBySpoken(String spokenVerification,
                                                     String checkDigits);

    /**
     * Find if customer location exists for customer number and location.
     *
     * @param customerNumber - number to find.
     * @param locationId - location where stored
     * @return - customer location if found.
     */
    PtsCustomerLocation findImportBlockingCondition(String customerNumber,
                String locationId);

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 