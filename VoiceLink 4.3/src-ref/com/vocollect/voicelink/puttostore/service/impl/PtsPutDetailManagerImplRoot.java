/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsPutDetailDAO;
import com.vocollect.voicelink.puttostore.model.PtsPutDetail;
import com.vocollect.voicelink.puttostore.service.PtsPutDetailManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>PtsPutDetail</code> model object.
 *
 * @author svoruganti
 */
public abstract class PtsPutDetailManagerImplRoot extends
    GenericManagerImpl<PtsPutDetail, PtsPutDetailDAO> implements PtsPutDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutDetailManagerImplRoot(PtsPutDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
            return this.getPrimaryDAO().
                listPutDetails(new QueryDecorator(rdi));
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsPutDetailManagerRoot#sumQuantityPut(long, java.util.Date, java.util.Date)
     */
    public int sumQuantityPut(long licenseId, Date startTime, Date endTime) throws DataAccessException {
        return convertNumberToInt(
            getPrimaryDAO().sumQuantityPut(licenseId, startTime, endTime));
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsPutDetailManagerRoot#sumLicenseProrateCount(long, java.util.Date, java.util.Date)
     */
    public int sumLicenseProrateCount(long licenseId, Date startTime, Date endTime) throws DataAccessException {
        return convertNumberToInt(
            getPrimaryDAO().sumLicenseProrateCount(licenseId, startTime, endTime));
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsPutDetailManagerRoot#sumGroupProrateCount(long, java.util.Date, java.util.Date)
     */
    public int sumGroupProrateCount(long operatorId , Date startTime, Date endTime) throws DataAccessException {
        return convertNumberToInt(
            getPrimaryDAO().sumGroupProrateCount(operatorId, startTime, endTime));
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsPutDetailManagerRoot#maxEndTime(long, java.util.Date)
     */
      public Date maxEndTime(long operatorId, Date minTime, Date maxTime) throws DataAccessException {
          return this.getPrimaryDAO().maxEndTime(operatorId, minTime, maxTime);
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 