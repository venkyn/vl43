/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsCustomerLocationDAO;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;



/**
 * Additional service methods for the <code>PtsCustomerLocation</code> model object.
 *
 * @author mnichols
 */
public class PtsCustomerLocationManagerImplRoot extends
    GenericManagerImpl<PtsCustomerLocation, PtsCustomerLocationDAO> implements PtsCustomerLocationManager {

    // Declare and instantiate a logger
    private static final Logger log = new Logger(PtsCustomerLocationManagerImpl.class);

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsCustomerLocationManagerImplRoot(PtsCustomerLocationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listCustomerLocations(new QueryDecorator(rdi));
    }

    /**
     *
     */
    public void initializeState() {
        // TODO Auto-generated method stub

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManagerRoot#findCustomerLocationByNumberLocation(java.lang.String, java.lang.String)
     */
    public PtsCustomerLocation findCustomerLocationByNumberLocation(
                                           String customerNumber,
                                           String locationId) {

        return getPrimaryDAO().findCustomerLocationByNumberLocation(
            customerNumber, locationId);
    }

    /**
     * Find customer location by the core location scanned verification value.
     *
     * @param scannedVerification - the scanned value to search by.
     * @return - customer location object.
     */
    public PtsCustomerLocation findCustomerLocationByScanned(String scannedVerification) {

        return (this.getPrimaryDAO().findCustomerLocationByScanned(scannedVerification));
    }



    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(PtsCustomerLocation instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("puttostore.customerLocation.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
     }

    /**
     * Find customer location by the core location spoken verification
     * and check digits value.
     *
     * @param spokenVerification - the spoken value to search by.
     * @param checkDigits - the check digits to search by.
     * @return - customer location object.
     */
    public PtsCustomerLocation findCustomerLocationBySpoken(String spokenVerification,
                                                            String checkDigits) {
        return (this.getPrimaryDAO().findCustomerLocationBySpoken(spokenVerification, checkDigits));

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManagerRoot#findImportBlockingCondition(java.lang.String, java.lang.String)
     */
    public PtsCustomerLocation findImportBlockingCondition(
                                           String customerNumber,
                                           String locationId) {

        return getPrimaryDAO().findImportBlockingCondition(
            customerNumber, locationId);
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 