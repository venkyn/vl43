/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.puttostore.dao.PtsPutImportDataDAO;
import com.vocollect.voicelink.puttostore.model.PtsPutImportData;
import com.vocollect.voicelink.puttostore.service.PtsPutImportDataManager;

/**
 * Standard implementation of the Importable PtsPut data manager implementation.
 *
 * @author svoruganti
 *
 */
public abstract class PtsPutImportDataManagerImplRoot
     extends GenericManagerImpl<PtsPutImportData, PtsPutImportDataDAO>
        implements PtsPutImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutImportDataManagerImplRoot(PtsPutImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 