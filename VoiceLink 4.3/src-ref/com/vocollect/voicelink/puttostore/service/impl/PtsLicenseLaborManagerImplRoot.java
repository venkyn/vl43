/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.puttostore.dao.PtsLicenseLaborDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.puttostore.service.PtsPutDetailManager;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author treed
 */
public abstract class PtsLicenseLaborManagerImplRoot extends GenericManagerImpl <PtsLicenseLabor, PtsLicenseLaborDAO>
    implements PtsLicenseLaborManager {

    private static final Double ONE_HUNDRED = 100.0;

    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.0;

    private PtsPutDetailManager    ptsPutDetailManager;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsLicenseLaborManagerImplRoot(PtsLicenseLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#recalculateAggregatesAndSave(com.vocollect.voicelink.puttostore.model.PtsLicenseLabor)
     */
    public void recalculateAggregatesAndSave(PtsLicenseLabor al) throws DataAccessException, BusinessRuleException {
    	PtsPutDetailManager pdManager = this.getPtsPutDetailManager();
    	Date startTime = al.getStartTime();
    	Date endTime = al.getEndTime();
    	Long licenseId = al.getLicense().getId();
        al.setDuration(new Long(endTime.getTime() - startTime.getTime()));
        al.setQuantityPut(pdManager.sumQuantityPut(licenseId, startTime, endTime));
        al.setLicenseProrateCount(
            pdManager.sumLicenseProrateCount(licenseId, startTime, endTime));
        al.setGroupProrateCount(
            pdManager.sumGroupProrateCount(al.getOperator().getId(), startTime, endTime));

        if (al.getDuration() > 0) {
            al.setActualRate(new Double(
                al.getQuantityPut() / (al.getDuration()
                                                               / MILLISEC_CONVERSION_FACTOR)));
            al.setPercentOfGoal(new Double(al.getActualRate()
                / (al.getLicense().getRegion().getGoalRate()) * ONE_HUNDRED));
        }
        this.save(al);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#closeLaborRecord(java.util.Date, com.vocollect.voicelink.puttostore.model.PtsLicense)
     */
    public PtsLicenseLabor closeLaborRecord(Date endTime, PtsLicense license)
        throws DataAccessException, BusinessRuleException {

        long licenseId = license.getId();
        PtsLicenseLabor ptsLicenseLabor = this.findOpenRecordByLicenseId(licenseId);

        if (ptsLicenseLabor != null) {
            PtsPutDetailManager pdManager = this.getPtsPutDetailManager();
            Date startTime = ptsLicenseLabor.getStartTime();
            ptsLicenseLabor.setEndTime(endTime);
            ptsLicenseLabor.setDuration(new Long(endTime.getTime() - startTime.getTime()));
            ptsLicenseLabor.setQuantityPut(pdManager.sumQuantityPut(licenseId, startTime, endTime));
            ptsLicenseLabor.setLicenseProrateCount(
                pdManager.sumLicenseProrateCount(licenseId, startTime, endTime));
            ptsLicenseLabor.setGroupProrateCount(
                pdManager.sumGroupProrateCount(ptsLicenseLabor.getOperator().getId(), startTime, endTime));

            if (ptsLicenseLabor.getDuration() > 0) {
                ptsLicenseLabor.setActualRate(new Double(
                    ptsLicenseLabor.getQuantityPut() / (ptsLicenseLabor.getDuration()
                                                                   / MILLISEC_CONVERSION_FACTOR)));
                ptsLicenseLabor.setPercentOfGoal(new Double(ptsLicenseLabor.getActualRate()
                    / (license.getRegion().getGoalRate()) * ONE_HUNDRED));
            }
            this.save(ptsLicenseLabor);
        }
        return ptsLicenseLabor;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#findOpenRecordByAssignmentId(long)
     */
    public PtsLicenseLabor findOpenRecordByLicenseId(long licenseId)
        throws DataAccessException {
        return getPrimaryDAO().findOpenRecordByLicenseId(licenseId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#listLicensesLaborForPtsLicenseLaborReport(String,
     *      Date, Date, Long)
     */
    public List<PtsLicenseLaborReport> listLicensesLaborForPtsLicenseLaborReport(QueryDecorator decorator, 
        Set<String> allOperatorIds,
        Date startTime,
        Date endTime,
        Long siteId)
        
        throws DataAccessException {
        WhereClause whereClause = new WhereClause();
        whereClause.add(allOperatorIds, "obj.operator.common.operatorIdentifier");

        if (!whereClause.toString().equalsIgnoreCase("")) {
            decorator.setWhereClause(whereClause.toString());
        }
        
        return getPrimaryDAO().listLicensesLaborForPtsLicenseLaborReport(
            decorator, startTime, endTime, siteId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#listAllRecordsByOperatorLaborId(long)
     */
    public List<PtsLicenseLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException {
        return getPrimaryDAO().listAllRecordsByOperatorLaborId(operatorLaborId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#listClosedRecordsByBreakLaborId(long)
     */
    public List<PtsLicenseLabor> listClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException {
        return getPrimaryDAO().listClosedRecordsByBreakLaborId(breakLaborId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#listOpenRecordsByOperatorId(long)
     */
    public List<PtsLicenseLabor> listOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listOpenRecordsByOperatorId(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#openLaborRecord(java.util.Date, com.vocollect.voicelink.core.model.Operator, com.vocollect.voicelink.puttostore.model.PtsLicense, com.vocollect.voicelink.core.model.OperatorLabor)
     */
    public void openLaborRecord(Date startTime,
                                Operator operator,
                                PtsLicense license,
                                OperatorLabor operLaborRecord)
        throws DataAccessException, BusinessRuleException {
        // close any open labor records for this license.
        // this is to handle the rare case of where the operator gets out of
        // radio range and get assignment is called for a second (or more) time.
        this.closeLaborRecord(startTime, license);

        PtsLicenseLabor ptsLicenseLabor = new PtsLicenseLabor();
        ptsLicenseLabor.setLicense(license);
        ptsLicenseLabor.setOperator(operator);
        ptsLicenseLabor.setStartTime(startTime);
        ptsLicenseLabor.setOperatorLabor(operLaborRecord);
        ptsLicenseLabor.setEndTime(null);
        ptsLicenseLabor.setDuration(new Long(0));
        ptsLicenseLabor.setQuantityPut(new Integer(0));
        ptsLicenseLabor.setGroupCount(license.getGroupInfo().getGroupCount());
        ptsLicenseLabor.setGroupNumber(license.getGroupInfo().getGroupNumber());
        ptsLicenseLabor.setLicenseProrateCount(new Integer(0));
        ptsLicenseLabor.setGroupProrateCount(new Integer(0));
        ptsLicenseLabor.setActualRate(new Double(0.0));
        ptsLicenseLabor.setPercentOfGoal(new Double(0.0));
        this.save(ptsLicenseLabor);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManagerRoot#sumQuantityPut(long)
     */
    public Long sumQuantityPut(long operatorLaborId)
        throws DataAccessException {

        Long result = getPrimaryDAO().sumQuantityPut(operatorLaborId);

        if (result == null) {
            result =  new Long(0);
        }
        return result;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listPtsLicenseLabor(new QueryDecorator(rdi));
    }


    /**
     * Getter for the putDetailManager property.
     * @return PutDetailManager value of the property
     */
    public PtsPutDetailManager getPtsPutDetailManager() {
        return this.ptsPutDetailManager;
    }


    /**
     * Setter for the putDetailManager property.
     * @param ptsPutDetailManager the new ptsPutDetailManager value
     */
    public void setPtsPutDetailManager(PtsPutDetailManager ptsPutDetailManager) {
        this.ptsPutDetailManager = ptsPutDetailManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 