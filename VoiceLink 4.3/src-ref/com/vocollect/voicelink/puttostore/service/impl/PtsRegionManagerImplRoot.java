/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCode;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCodeRoot;
import com.vocollect.voicelink.puttostore.dao.PtsRegionDAO;
import com.vocollect.voicelink.puttostore.dao.PtsSummaryDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.model.PtsSummary;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>PtsRegion</code> model object.
 *
 * @author mnichols
 * @author svoruganti
 */
public class PtsRegionManagerImplRoot extends
    GenericManagerImpl<PtsRegion, PtsRegionDAO> implements PtsRegionManager {

    //workgroup manager
    private WorkgroupManager workgroupManager;

    //region manager
    private RegionManager regionManager;

    private OperatorDAO operatorDAO;

    private PtsSummaryDAO  ptsSummaryDAO;

    //All Summaries.
    private static final int REGION = 0;

    //License summary query positions
    private static final int TOTAL_LICENSES = 1;
    private static final int INPROGRESS = 2;
    private static final int AVAILABLE = 3;
    private static final int COMPLETE = 4;
    private static final int NON_COMPLETE = 5;
    private static final int ASSIGN_SITE = 6;

    //Current work summary query positions
    private static final int OPERATOR_IN_REGION = 1;
    private static final int OPERATOR_ASSIGNED = 2;
    private static final int TOTAL_ITEMS_REMAINING = 3;
    private static final int TOTAL_ITEMS_COMPLETED = 4;
    private static final int TOTAL_ITEMS = 5;
    private static final int CURRENT_WORK_SITE = 6;

    //Current route summary query positions
    private static final int ROUTE = 0;
    private static final int NUMBER_OF_CUSTOMERS = 1;
    private static final int PERCENT = 100;
    private static final int CARTONS_REMAINING = 3;
    private static final int CARTONS_COMPLETE = 4;
    private static final int TOTAL_CARTONS = 5;
    private static final int ROUTE_SITE = 6;
    private static final int CUSTOMER_ID = 2;

    /**
     * Getter for the operatorDAO property.
     * @return OperatorDAO value of the property
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }


    /**
     * Setter for the operatorDAO property.
     * @param operatorDAO the new operatorDAO value
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsRegionManagerImplRoot(PtsRegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * Implementation to get specified region.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.RegionManager#findRegionByNumber(int)
     */
    public PtsRegion findRegionByNumber(int regionNumber)
        throws DataAccessException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(PtsRegion region)
        throws BusinessRuleException, DataAccessException {
        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the putaway task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        }  else {

            //Save current site context filter and turn it off for these
            //queries
            boolean siteContextFilter = SiteContextHolder.getSiteContext()
                .isFilterBySite();
            SiteContextHolder.getSiteContext().setFilterBySite(false);

            int spokenLicenseLength =
                region.getSpokenLicenseLength();
            if (spokenLicenseLength == 0) {
                getPrimaryDAO().updatePartialLicenseNumberAll(region);
            } else {
                getPrimaryDAO().updateCustomPartialLicenseNumber(
                    spokenLicenseLength, spokenLicenseLength, region.getId());
            }

            //reset site context filter
            SiteContextHolder.getSiteContext()
                .setFilterBySite(siteContextFilter);

}

        return super.save(region);
    }


    /**
     * Take the passed in PutToStore Region and add it to all the
     * workgroups where AutoAddRegions is turned on and the task
     * function is a PutToStore task function.
     * @param region - a PutToStore region
     * @throws DataAccessException - any database exception
     */
    protected void addToWorkgroups(PtsRegion region)
        throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.PutToStore) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listLicenseSummary()
     */
    public List<DataObject> listLicenseSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getPtsSummaryDAO().listLicenseSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            PtsSummary summaryObject = new PtsSummary();

            summaryObject.setRegion((PtsRegion) objArray[REGION]);
            summaryObject.setTotalLicenses(convertNumberToInt(objArray[TOTAL_LICENSES]));
            summaryObject.setInProgress(convertNumberToInt(objArray[INPROGRESS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setComplete(convertNumberToInt(objArray[COMPLETE]));
            summaryObject.setNonComplete(convertNumberToInt(objArray[NON_COMPLETE]));
            summaryObject.setSite((Site) objArray[ASSIGN_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            newList.add(summaryObject);
        }
        return newList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listCurrentWorkSummary()
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getPtsSummaryDAO().listCurrentWorkSummary((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            PtsSummary summaryObject = new PtsSummary();

            summaryObject.setRegion((PtsRegion) objArray[REGION]);
            summaryObject.setOperatorsWorkingIn(convertNumberToInt(objArray[OPERATOR_IN_REGION]));
            summaryObject.setOperatorsAssigned(convertNumberToInt(objArray[OPERATOR_ASSIGNED]));
            summaryObject.setItemsComplete(convertNumberToInt(objArray[TOTAL_ITEMS_COMPLETED]));
            summaryObject.setTotalItemsRemaining(convertNumberToInt(objArray[TOTAL_ITEMS_REMAINING]));
            summaryObject.setTotal(convertNumberToInt(objArray[TOTAL_ITEMS]));
            summaryObject.setSite((Site) objArray[CURRENT_WORK_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            Double estimatedCompleted = 0.0;

            if ((summaryObject.getRegion().getGoalRate() != 0)
                && (summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn() != 0)) {

                Double totalRemain = new Double(summaryObject.getTotalItemsRemaining());
                Double operators = new Double(summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn());
                Double goalRate = new Double(summaryObject.getRegion().getGoalRate());

                estimatedCompleted = totalRemain / goalRate / operators;
            }

            //Round to 2 decimal places and set property.
            summaryObject.setEstimatedCompleted(new BigDecimal(estimatedCompleted)
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            newList.add(summaryObject);
        }
        return newList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listShortsSummary()
     */
    public List<DataObject> listRouteSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getPtsSummaryDAO().listRouteSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            PtsSummary summaryObject = new PtsSummary();

            summaryObject.setRoute((String) objArray[ROUTE]);
            summaryObject.setNumberOfCustomers(convertNumberToInt(objArray[NUMBER_OF_CUSTOMERS]));
            summaryObject.setTotalItems(convertNumberToInt(objArray[TOTAL_CARTONS]));
            summaryObject.setItemsRemaining(convertNumberToInt(objArray[CARTONS_REMAINING]));
            summaryObject.setItemsComplete((convertNumberToInt(objArray[CARTONS_COMPLETE])));
            summaryObject.setSite((Site) objArray[ROUTE_SITE]);

            Double percentComplete = 0.0;
            if ((summaryObject.getTotalItems() > 0)
                && (summaryObject.getItemsComplete() != 0)) {

                Double complete = new Double(summaryObject.getItemsComplete());
                Double total = new Double(summaryObject.getTotalItems());
               percentComplete = (complete / total) * PERCENT;

            }

           summaryObject.setId((Long) objArray[CUSTOMER_ID]);
           summaryObject.setPercentComplete(new BigDecimal(percentComplete)
           .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
            newList.add(summaryObject);
        }
            return newList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        PtsRegion ptsRegion = get(id);
        return delete(ptsRegion);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(PtsRegion instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("puttostore.region.delete.error.inUse");
            } else {
                throw ex;
        }
    }
        return null;
    }


    /**
     * There can be no operators signed into the region when editing or deleting.
     *
     * @param region - to be persisted or deleted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void verifyNoOperatorsSignedIn(PtsRegion region)
        throws BusinessRuleException, DataAccessException {

        Number numSignedIn = this.getOperatorDAO().countNumberOfOperatorsSignedIn(
            region);

        if ((numSignedIn != null) && (numSignedIn.intValue() > 0)) {
            throw new BusinessRuleException(
                PutToStoreErrorCodeRoot.REGION_OPERATORS_SIGNED_IN, new UserMessage(
                    "puttostore.region.edit.error.operatorsSignedIn"));
        }
    }

    /**
     * Getter for the ptsSummaryDAO property.
     * @return PtsSummaryDAO value of the property
     */
    public PtsSummaryDAO getPtsSummaryDAO() {
        return ptsSummaryDAO;
    }



    /**
     * Setter for the putToStoreSummaryDAO property.
     * @param ptsSummaryDAO the new putToStoreSummaryDAO value
     */
    public void setPtsSummaryDAO(PtsSummaryDAO ptsSummaryDAO) {
        this.ptsSummaryDAO = ptsSummaryDAO;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.puttostore.model.PtsRegion)
     */
    public void executeValidateEditRegion(PtsRegion region)
        throws DataAccessException, BusinessRuleException {

        //Save current site context filter and turn it off for these
        //queries
        boolean siteContextFilter = SiteContextHolder.getSiteContext().isFilterBySite();
        SiteContextHolder.getSiteContext().setFilterBySite(false);

            // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            verifyNoOperatorsSignedIn(region);
            validateLicenseStatus(region);
            validateDigitsSpoken(region);
          }

        //reset site context filter
        SiteContextHolder.getSiteContext().setFilterBySite(siteContextFilter);

    }

    /**
     * verify if a user modifies partial item length , all licenses with a status
     * Available or Unavailable must have an SKU  value as long as or longer than this value.
     *
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateDigitsSpoken(PtsRegion region)
        throws BusinessRuleException, DataAccessException {

        int spokenLicenseLength = region.getSpokenLicenseLength();

        if (spokenLicenseLength != 0) {
            Number minLicenseLength = getPrimaryDAO().minLicenseLength(region);

            if ((minLicenseLength != null)
                        && (minLicenseLength.intValue() < spokenLicenseLength)) {
                throw new BusinessRuleException(
                    PutToStoreErrorCode.REGION_INVALID_SPOKEN_LICENSE_LENGTH,
                    new UserMessage(
                        "puttostore.region.edit.error.invalidSpokenLicense"));

            }
        }

    }

    /**
     * From EDD - Edit Region: There can be no assignments in the region with a
     * status of In-Progress, Suspended, or Passed.
     *
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateLicenseStatus(PtsRegion region)
        throws BusinessRuleException, DataAccessException {

        final int numInProgress = convertNumberToInt(
            getPrimaryDAO().countNumberOfLicenses(region, PtsLicenseStatus.InProgress));

        final int numSuspended = convertNumberToInt(
            getPrimaryDAO().countNumberOfLicenses(region, PtsLicenseStatus.Suspended));

        final int numPassed = convertNumberToInt(
            getPrimaryDAO().countNumberOfLicenses(region, PtsLicenseStatus.Passed));

        if (numInProgress + numSuspended + numPassed > 0) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.REGION_INVALID_LICENSE_STATUS,
                new UserMessage("puttostore.region.edit.error.invalidLicenseStatus"));

        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 