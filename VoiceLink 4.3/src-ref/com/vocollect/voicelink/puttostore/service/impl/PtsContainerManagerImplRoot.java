/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsContainerDAO;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author mnichols
 */

public abstract class PtsContainerManagerImplRoot
    extends GenericManagerImpl<PtsContainer, PtsContainerDAO>
    implements PtsContainerManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsContainerManagerImplRoot(PtsContainerDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get a list of all containers.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManager
     * #listAllContainers()
     */
    public List<PtsContainer> listAllContainers()
        throws DataAccessException {
        return getPrimaryDAO().listAllContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listPtsContainers(new QueryDecorator(rdi));
    }


    /**
     * @param containerNumber - Container number to search for
     * @return PtsContainer that belongs to the containerNumber searched.
     * @throws DataAccessException -.
     */
    public PtsContainer findPtsContainerByNumber(String containerNumber)
    throws DataAccessException {
        return this.getPrimaryDAO().findPtsContainerByNumber(containerNumber);
    }


    /**
     * Updates the status of the container passed in to Closed.
     *
     * @param containerToClose - the ptsContainer to close
     * @throws DataAccessException - if container can't be saved.
     * @throws BusinessRuleException - if container can't be closed.
     */
    public void closeContainer(PtsContainer containerToClose) throws
        DataAccessException, BusinessRuleException {
        containerToClose.setStatus(PtsContainerStatus.Closed);
        this.save(containerToClose);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerManagerRoot#listOpenPtsContainersByLocation(java.lang.String)
     */
    public List<PtsContainer> listOpenPtsContainersByCustomerLocation(PtsCustomerLocation customerLocation)
                                                                      throws DataAccessException {
        return getPrimaryDAO().listOpenPtsContainersByCustomerLocation(customerLocation);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerManagerRoot#listOpenPtsContainersByPartialNumberAndLocation(java.lang.String)
     */
    public List<PtsContainer> listOpenPtsContainersByPartialNumberAndLocation(String containerNumber,
                                                                              PtsCustomerLocation customerLocation)
                                                                              throws DataAccessException {
        // Add the % to the beginning of the container number so it finds anything that ends
        // with the container # passed in.
        containerNumber = "%" + containerNumber;
        return getPrimaryDAO().listOpenPtsContainersByPartialNumberAndLocation(containerNumber, customerLocation);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerManagerRoot#listOpenPtsContainersByNumberAndLocation(java.lang.String)
     */
    public PtsContainer findPtsContainerByNumberAndLocation(String containerNumber,
                                                            PtsCustomerLocation customerLocation)
                                                            throws DataAccessException {
        return getPrimaryDAO().findPtsContainerByNumberAndLocation(containerNumber, customerLocation);
    }



    /**
     * Adds leading zeros to the container number.
     *
     * @param contNumber - the container number to pad
     * @param length - length to pad to
     * @return the padded container number
     */
    public String padContainerNumber(String contNumber, int length) {
        while (contNumber.length() < length) {
            contNumber = "0" + contNumber;
        }
        return contNumber;
    }


    /**
     * Get the last (minSpokenLength) number of digits of the container number.
     * If container number is shorter than specified length,
     * then pad up to correct length with zeros so it can be spoken by operator.
     *
     * @param containerNumber - container number to check
     * @param minSpokenLength - Minimum spoken length
     * @return - last minSpokenLength digits of container (padded on left with zeros)
     */
    public String getContainerDigitsToSpeak(String containerNumber, int minSpokenLength) {

        if (containerNumber.length() <= minSpokenLength) {
            return padContainerNumber(containerNumber, minSpokenLength);
        }
        return containerNumber.substring(containerNumber.length() - minSpokenLength);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerManagerRoot#listPtsContainersForContainerReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.lang.String, java.util.Set, java.util.Date, java.util.Date)
     */
    public List<PtsContainerReport> listPtsContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                        String containerNumber,
                                                                        Set<String> operatorIdentifiers,
                                                                        Date startDate,
                                                                        Date endDate)
        throws DataAccessException {
        WhereClause whereClause = new WhereClause();
        whereClause.add(containerNumber, "obj.containerNumber");
        whereClause.add(operatorIdentifiers,
            "cd.operator.common.operatorIdentifier");

        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }

        return getPrimaryDAO().listPtsContainersForPtsContainerReport(
            queryDecorator, startDate, endDate);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 