/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.puttostore.dao.ArchivePtsLicenseDAO;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Additional service methods for the <code>ArchivePtsLicense</code> model
 * object.
 *
 * @author mlashinsky
 */
public class ArchivePtsLicenseManagerImplRoot extends
    GenericManagerImpl<ArchivePtsLicense, ArchivePtsLicenseDAO> implements
    ArchivePtsLicenseManager {

    private PtsLicenseManager ptsLicenseManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Passes the ArchivePtsLicenseDAO as the primaryDAO. Constructor.
     * @param primaryDAO ArchivePtsLicenseDAO object
     */
    public ArchivePtsLicenseManagerImplRoot(ArchivePtsLicenseDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.util.Date,
     *      com.vocollect.voicelink.puttostore.model.PtsLicenseStatus)
     */
    public Integer updateOlderThan(Date createdDate, PtsLicenseStatus status)
        throws DataAccessException {
        return getPrimaryDAO().updateOlderThan(createdDate, status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.puttostore.model.PtsLicenseStatus,
     *      java.util.Date)
     */
    public int executePurge(PtsLicenseStatus status, Date olderThan) {
        int returnRecords = 0;

        // Get List of Archive Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Fetching Archive PTS License(s) to Purge :::");
        }
        try {
            returnRecords = updateOlderThan(olderThan, status);
        } catch (DataAccessException e) {
            log.warn("Error getting Archive "
                + "PTS License(s) from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Archive PTS Licenses for Purge :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }


    /**
     * @return PtsLicenseManager
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return ptsLicenseManager;
    }

    /**
     * @param ptsLicenseManager - the put to store license manager
     */
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }
    
    /**
    *
    * {@inheritDoc}
    * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#listLicensesInGroup(long)
    */
   public List<PtsLicenseReport> listArchiveLicensesForPtsLicenseReport(
       QueryDecorator queryDecorator, 
       String licenseNumber, 
       Set<String> operatorId, 
       Date startTime, 
       Date endTime, 
       Long siteId) throws DataAccessException {
       
       WhereClause whereClause = new WhereClause();
       whereClause.add(licenseNumber, "obj.number");
       whereClause.add(operatorId, "pd.operatorIdentifier");
       
       if (!whereClause.toString().equalsIgnoreCase("")) {
           queryDecorator.setWhereClause(whereClause.toString());
       }
       return getPrimaryDAO().listArchiveLicensesForPtsLicenseReport(
           queryDecorator, startTime, endTime, siteId);
   }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 