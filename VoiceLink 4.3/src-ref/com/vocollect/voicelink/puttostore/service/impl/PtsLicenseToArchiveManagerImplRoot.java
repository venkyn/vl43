/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseToArchiveManager;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author mlashinsky
 */
public class PtsLicenseToArchiveManagerImplRoot implements
    PtsLicenseToArchiveManager {

    private PtsLicenseManager ptsLicenseManager;

    private ArchivePtsLicenseManager archivePtsLicenseManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseToArchiveManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.puttostore.model.PTSLicenseStatus,
     *      java.util.Date, boolean)
     */
    public int executeArchive(QueryDecorator decorator) {

        int returnRecords = 0;

        // Get List of PTS Licenses to Archive

        try {
            List<PtsLicense> licenses = getPtsLicenseManager().getPrimaryDAO()
                .listArchive(decorator);
            returnRecords = licenses.size();

            for (PtsLicense l : licenses) {
                getArchivePtsLicenseManager().save(new ArchivePtsLicense(l));
                l.setPurgeable(2);
                getPtsLicenseManager().save(l);
            }

        } catch (DataAccessException e) {
            log.warn("!!!Error: DataAccessException Archiving PTS License(s): "
                + e);
            return 0;
        } catch (BusinessRuleException e) {
            log.warn("!!!Error: BusinessRuleException creating Archive "
                + "PTS License: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Archived " + returnRecords + " PTS License(s) ");
            log.debug("### PTS License Archive Process Complete :::");
        }

        getPtsLicenseManager().getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseToArchiveManagerRoot#executeMarkForPurge(com.vocollect.voicelink.puttostore.model.PtsLicenseStatus,
     *      java.util.Date, boolean)
     */
    public Integer executeMarkForPurge(PtsLicenseStatus status,
                                       Date olderThan,
                                       boolean archive) {
        Integer recordsMarked = 0;

        // Mark all PTS Licenses to be purged
        if (log.isDebugEnabled()) {
            log.debug("### Finding PTS License - " + status.toString()
                + " that are elligible for purge/archive :::");
        }
        try {
            if (archive) {
                recordsMarked = getPtsLicenseManager().getPrimaryDAO()
                    .updateOlderThan(1, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked + " PTS License - "
                        + status.toString() + " for Archive :::");
                }

            } else {
                recordsMarked = getPtsLicenseManager().getPrimaryDAO()
                    .updateOlderThan(2, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked + " PTS License - "
                        + status.toString() + " for Purge :::");
                }
            }
        } catch (DataAccessException e) {
            log
                .warn("!!!Error: DataAccessException updating purgeable field of transactional "
                    + " PTS License - " + status.toString() + ": " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Updated " + recordsMarked + " PTS License - "
                + status.toString() + " purge-able status:::");
        }
        return recordsMarked;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseToArchiveManagerRoot#executePurgePtsLicense()
     */
    public int executePurgePtsLicense(QueryDecorator queryDecorator) {

        int returnRecords = 0;

        List<PtsLicense> ptsLicenses = null;

        try {
            ptsLicenses = getPtsLicenseManager().getPrimaryDAO().listPurge(
                queryDecorator);

            returnRecords = ptsLicenses.size();
        } catch (DataAccessException e) {
            log.warn("!!!Error: DataAccessException purging transactional "
                + "PTS Licenses from database: " + e);
        }

        for (PtsLicense pl : ptsLicenses) {
            try {
                getPtsLicenseManager().delete(pl);

            } catch (BusinessRuleException e) {
                log.warn("!!!Error: BusinessRuleException purging transactional "
                    + "PTS Licenses from database: " + e);
            } catch (DataAccessException e) {
                log.warn("!!!Error: DataAccessException purging transactional "
                    + "PTS Licenses from database: " + e);

            }

        }
        if (log.isDebugEnabled()) {
            log.debug("### Purged " + returnRecords
                + " rows from the database.");
            log.debug("### PTS License Purge Process Complete :::");
        }
        return returnRecords;
    }

    /**
     * @return ArchivePtsLicenseManager
     */
    public ArchivePtsLicenseManager getArchivePtsLicenseManager() {
        return archivePtsLicenseManager;
    }

    /**
     * @param archivePtsLicenseManager - the archive put to store license manager
     */
    public void setArchivePtsLicenseManager(ArchivePtsLicenseManager archivePtsLicenseManager) {
        this.archivePtsLicenseManager = archivePtsLicenseManager;
    }

    /**
     * @return PtsLicenseManager
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return ptsLicenseManager;
    }

    /**
     * @param ptsLicenseManager - the put to store license manager
     */
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 