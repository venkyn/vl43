/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.puttostore.model.ArchivePtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsContainerToArchiveManager;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author mlashinsky
 */
public class PtsContainerToArchiveManagerImplRoot implements
    PtsContainerToArchiveManager {

    private PtsContainerManager ptsContainerManager;

    private ArchivePtsContainerManager archivePtsContainerManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerToArchiveManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.puttostore.model.PTSContainerStatus,
     *      java.util.Date, boolean)
     */
    public int executeArchive(QueryDecorator decorator) {

        int returnRecords = 0;

        // Get List of PTS Containers to Archive
        if (log.isDebugEnabled()) {
            log.debug("### Finding PTS Containers to Archive :::");
        }
        try {
            List<PtsContainer> containers = getPtsContainerManager()
                .getPrimaryDAO().listArchive(decorator);
            returnRecords = containers.size();

            for (PtsContainer c : containers) {
                getArchivePtsContainerManager()
                    .save(new ArchivePtsContainer(c));
                c.setPurgeable(2);
                getPtsContainerManager().save(c);
            }

        } catch (DataAccessException e) {
            log.warn("!!! Error: DataAccessException while archiving PTS "
                + " Containers: " + e);
            return 0;
        } catch (BusinessRuleException e) {
            log.warn("!!! Error: BusinessRuleException creating Archive PTS Container: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Archived " + returnRecords + " PTS Container ");
        }

        getPtsContainerManager().getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerToArchiveManagerRoot#executeMarkForPurge(com.vocollect.voicelink.puttostore.model.PtsContainerStatus,
     *      java.util.Date, boolean)
     */
    public Integer executeMarkForPurge(PtsContainerStatus status,
                                       Date olderThan,
                                       boolean archive) {
        Integer recordsMarked = 0;

        // Mark all PTS Containers to be purged
        if (log.isDebugEnabled()) {
            log.debug("### Finding PTS Container - " + status.toString()
                + " that are elligible for purge/archive :::");
        }
        try {
            if (archive) {
                recordsMarked = getPtsContainerManager().getPrimaryDAO()
                    .updateOlderThan(1, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked
                        + " PTS Container - " + status.toString()
                        + " for Archive :::");
                }
            } else {
                recordsMarked = getPtsContainerManager().getPrimaryDAO()
                    .updateOlderThan(2, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked
                        + " PTS Container - " + status.toString()
                        + " for Purge :::");
                }
            }
        } catch (DataAccessException e) {
            log.warn("!!! Error updating purgeable field of transactional "
                + " PTS Container - " + status.toString() + ": " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Updated purgeable status of " + recordsMarked
                + " PTS Container - " + status.toString() + " :::");
        }

        return recordsMarked;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerToArchiveManagerRoot#executePurgePtsContainer()
     */
    public int executePurgePtsContainer(QueryDecorator queryDecorator) {

        int returnRecords = 0;

        List<PtsContainer> ptsContainers = null;

        try {
            ptsContainers = getPtsContainerManager().getPrimaryDAO().listPurge(
                queryDecorator);
            returnRecords = ptsContainers.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error: DataAccessException purging transactional "
                + "PTS Containers from database: " + e);
        }
        for (PtsContainer c : ptsContainers) {
            try {
                getPtsContainerManager().delete(c);
            } catch (BusinessRuleException e) {
                log.warn("!!! Error: BusinessRuleException purging transactional "
                    + "PTS Containers from database: " + e);
            } catch (DataAccessException e) {
                log.warn("!!! Error: DataAccessException purging transactional "
                    + "PTS Containers from database: " + e);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("### Purged " + returnRecords
                + " rows from the database.");
        }
        return returnRecords;
    }

    /**
     * @return ArchivePtsContainerManager
     */
    public ArchivePtsContainerManager getArchivePtsContainerManager() {
        return archivePtsContainerManager;
    }

    /**
     * @param archivePtsContainerManager - the archive put to store container manager
     */
    public void setArchivePtsContainerManager(ArchivePtsContainerManager archivePtsContainerManager) {
        this.archivePtsContainerManager = archivePtsContainerManager;
    }

    /**
     * Getter for the ptsContainerManager property.
     * @return PtsContainerManager value of the property
     */
    public PtsContainerManager getPtsContainerManager() {
        return ptsContainerManager;
    }

    /**
     * Setter for the ptsContainerManager property.
     * @param ptsContainerManager the new ptsContainerManager value
     */
    public void setPtsContainerManager(PtsContainerManager ptsContainerManager) {
        this.ptsContainerManager = ptsContainerManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 