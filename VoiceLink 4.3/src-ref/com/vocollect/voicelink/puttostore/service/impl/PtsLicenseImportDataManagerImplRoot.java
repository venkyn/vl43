/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.puttostore.dao.PtsLicenseImportDataDAO;
import com.vocollect.voicelink.puttostore.dao.PtsPutImportDataDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicenseImportData;
import com.vocollect.voicelink.puttostore.service.PtsLicenseImportDataManager;

import java.util.List;

/**
 * @author svoruganti
 *
 */
public abstract class PtsLicenseImportDataManagerImplRoot extends
        GenericManagerImpl<PtsLicenseImportData, PtsLicenseImportDataDAO> implements PtsLicenseImportDataManager {

    private static final int RECORDS_TO_QUERY = 10;

   private PtsPutImportDataDAO ptsPutImportDataDAO = null;


    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsLicenseImportDataManagerImplRoot(PtsLicenseImportDataDAO primaryDAO) {
        super(primaryDAO);
    }



    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    public List<PtsLicenseImportData> listPtsLicenseBySiteName(String siteName)
            throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(RECORDS_TO_QUERY);
        return getPrimaryDAO().listPtsLicenseBySiteName(qd, siteName);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
        getPtsPutImportDataDAO().updateToComplete();
    }



    /**
     * Gets the value of ptsPutImportDataDAO.
     * @return the ptsPutImportDataDAO
     */
    public PtsPutImportDataDAO getPtsPutImportDataDAO() {
        return ptsPutImportDataDAO;
    }



    /**
     * Sets the value of the pickImportDataDAO.
     * @param ptsPutImportDataDAO the ptsPutImportDataDAO to set
     */
    public void setPtsPutImportDataDAO(PtsPutImportDataDAO ptsPutImportDataDAO) {
        this.ptsPutImportDataDAO = ptsPutImportDataDAO;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 