/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.puttostore.dao.PtsContainerDAO;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerDetail;
import com.vocollect.voicelink.puttostore.service.PtsContainerExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author svoruganti
 *
 */
public abstract class PtsContainerExportTransportManagerImplRoot extends PtsContainerManagerImpl
        implements PtsContainerExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * @param primaryDAO the DAO for this manager.
     */
    public PtsContainerExportTransportManagerImplRoot(PtsContainerDAO primaryDAO) {
        super(primaryDAO);
        // Nothing special here
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerExportTransportManagerRoot#updateClosedContainers()
     */
    public Integer updateClosedContainers() throws DataAccessException {
        boolean siteFilter = SiteContextHolder.getSiteContext().isFilterBySite();
        SiteContextHolder.getSiteContext().setFilterBySite(false);

        Integer i;
        try {
            i = getPrimaryDAO().updateClosedContainers();
        } finally {
            SiteContextHolder.getSiteContext().setFilterBySite(siteFilter);
        }
        return i;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsContainerExportTransportManager#listClosedContainers()
     */
    public List<Map<String, Object>> listClosedPtsContainers() throws DataAccessException {
        List<Map<String, Object>> finishedData = new ArrayList<Map<String, Object>>();

        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<PtsContainer> rawData = this.getPrimaryDAO().listClosedContainers(qd);

        //Stuff to format timestamps
        String formattedExportTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);

        for (PtsContainer container : rawData) {
            // This will count down to zero so we can find the last one.
            int i = container.getContainerDetails().size();
            for (PtsContainerDetail cd : container.getContainerDetails()) {
                Map<String, Object> mirror = new HashMap<String, Object>();

                mirror.put("QuantityPut", cd.getQuantityPut());
                mirror.put("Item", cd.getItem().getNumber());

                mirror.put("LocationID", container.getCustomer().getLocation().getScannedVerification());
                mirror.put("Route", container.getCustomer().getRoute());
                mirror.put("CustomerNumber", container.getCustomer().getCustomerInfo().getCustomerNumber());
                mirror.put("DeliveryLocation",  container.getCustomer().getDeliveryLocation());

                mirror.put("RecordType", "CONTAINER");

                //Format the ExportTime
                formattedExportTime = sdf.format(Calendar.getInstance().getTime());
                mirror.put("ExportTime", formattedExportTime);
                mirror.put("ContainerNumber", container.getContainerNumber());
                mirror.put("Status", "CLOSED");
                // Countdown to zero
                i--;
                if (i == 0) {
                    // last put Detail for this container, add the Container only
                    // only to this record so its status doesn't get updated
                    // redundantly every time a put detail is processed.
                    mirror.put("source", container);
                }
                finishedData.add(mirror);
            }

        }

        return finishedData;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 