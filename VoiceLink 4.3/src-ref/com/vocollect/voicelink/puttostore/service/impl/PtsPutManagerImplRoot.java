/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsPutDAO;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.service.PtsPutManager;

import java.util.List;



/**
 * Additional service methods for the <code>Put</code> model object.
 *
 * @author mnichols
 */
/**
 * @author pfunyak
 *
 */
public class PtsPutManagerImplRoot extends
    GenericManagerImpl<PtsPut, PtsPutDAO> implements PtsPutManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutManagerImplRoot(PtsPutDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * List all puts for the given group number.
     *
     * @param groupNumber - group number on license object.
     * @return - list of puts for the group
     * @throws DataAccessException - on db exception
     */
    public List<PtsPut> listPutsForGroup(Long groupNumber)
                                            throws DataAccessException {
        return this.getPrimaryDAO().listPutsForGroup(groupNumber);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listPuts(new QueryDecorator(rdi));
    }

    /**
     *
     */
    public void initializeState() {
        // TODO Auto-generated method stub

    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 