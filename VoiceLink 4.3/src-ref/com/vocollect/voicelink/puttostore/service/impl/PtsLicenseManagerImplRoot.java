/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCode;
import com.vocollect.voicelink.puttostore.dao.PtsLicenseDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;



/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author mnichols
 */
public class PtsLicenseManagerImplRoot extends
    GenericManagerImpl<PtsLicense, PtsLicenseDAO> implements PtsLicenseManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsLicenseManagerImplRoot(PtsLicenseDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#listLicenseByNumber(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.lang.String, com.vocollect.voicelink.core.model.Operator, com.vocollect.voicelink.core.model.Region)
     */
    public List<PtsLicense> listLicenseByNumber(QueryDecorator queryDecorator,
                                                String number,
                                                Region regionId)
        throws DataAccessException {
        return this.getPrimaryDAO().listLicenseByNumberAndRegion(queryDecorator,
                                                        number,
                                                        regionId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listLicenses(new QueryDecorator(rdi));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#findLicenseByPartialNumber(java.lang.String)
     */
    public List<PtsLicense> listLicenseByPartialNumber(QueryDecorator queryDecorator,
                                                       String partialNumber,
                                                       Region regionId)
    throws DataAccessException {
        return this.getPrimaryDAO().listLicenseByPartialNumberAndRegion(queryDecorator,
                                                                        partialNumber,
                                                                        regionId);
    }
    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#initializeState()
     */
    public void initializeState() {
        // TODO Auto-generated method stub
    }

    /**
     * On saving an license.
     *
     * @param license - pts license
     * @return the saved license
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    @Override
    public Object save(PtsLicense license)
    throws DataAccessException, BusinessRuleException {
        if (license.isNew()) {
            calculateLicenseInfo(license);
        }
        this.getPrimaryDAO().save(license);
     return license;
    }

    /**
     * Method to ensure all assignment in a group are updated the same
     * when assignments are Imported.
     *
     * @param licenses - assignment that was edited
     * @return saved Object
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    @Override
    public Object save(List<PtsLicense> licenses)
    throws DataAccessException, BusinessRuleException {
        for (PtsLicense license : licenses) {
            if (license.isNew()) {
                calculateLicenseInfo(license);
            }
         }
        this.getPrimaryDAO().save(licenses);
        return null;
    }

    /**
     * Calculates the summary information of a License based
     * on the puts within the License.  Also sets site tags
     * for the puts if they are new.
     * Also ensures the relationship back to the license is
     * not null.
     *
     * @param license - license to calculate info for
     */
    protected void calculateLicenseInfo(PtsLicense license) {
        int totalQuantity = 0;
        for (PtsPut p : license.getPuts()) {
            //Calculate summary information
             totalQuantity += p.getQuantityToPut();

            // if the object is being created and is a taggable object
            // get the current site from the context and add it to the object
            if (p.isNew()) {
                assignSiteTagsToPut(p);
            }

            //ensure relationship to parent is completely set up
            if (p.getLicense() == null) {
                p.setLicense(license);
            }

        }
        license.setTotalItemQuantity(totalQuantity);

        /**
         * Based upon the region setting for spokenLicenseLength
         * build a partial license value and update the object.
         */
         String partial = license.getNumber();
         int partialLength = license.getRegion().getSpokenLicenseLength();
         if ((0 != partialLength) && (partialLength < partial.length())) {
             partial = partial.substring(partial.length() - partialLength);
         }
         license.setPartialNumber(partial);
    }


    /**
     * Assign new site tag to put.
     *
     * @param put - put to assign tag to
     */
    public void assignSiteTagsToPut(PtsPut put) {
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        siteContext.setSiteToCurrentSite(put);

    }


    /**
     * Clear any licenses reserved by the given operator.
     *
     * @param operator - the requested operator
     * @throws DataAccessException - on db failure
     */
    public void executeClearReservedLicenses(Operator operator)
    throws DataAccessException {
        List<PtsLicense> licenses = listReservedLicenses(operator.getOperatorIdentifier());

        if (licenses != null) {
            for (PtsLicense l : licenses) {
                l.setReservedBy(null);
            }
        }
    }

    /**
     * Get a list of licenses reserved by an operator.
     *
     * @param reserveValue - the value used to reserve the license.
     * @return - a list of reserved licenses
     * @throws DataAccessException - on db failure
     */
    public List<PtsLicense> listReservedLicenses(String reserveValue)
                                                throws DataAccessException {
        return getPrimaryDAO().listReservedLicenses(reserveValue);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#listActiveLicenses(com.vocollect.voicelink.core.model.Operator)
     */
    public List<PtsLicense> listActiveLicenses(Operator operator)
                                                   throws DataAccessException {
        return getPrimaryDAO().listActiveLicenses(operator);
    }


    /**
     * Method to ensure all licenses in a group are updated the same
     * when an license was edited from the UI.
     *
     * @param license - license that was edited
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    public void executeLicenseChangeSave(PtsLicense license)
    throws DataAccessException, BusinessRuleException {

        //Refresh the region object to ensure there are no flushing
        //errors while saving an assignment.
        getPrimaryDAO().refresh(license.getRegion());

        if (license.getGroupInfo().getGroupNumber() != null) {
            List<PtsLicense> group =
                this.listLicensesInGroup(license.getGroupInfo().getGroupNumber());

            //Modify status and operator to match assignment that was edited
            for (PtsLicense a : group) {
                if (!a.equals(license)) {
                    a.setOperator(license.getOperator());
                   // a.changeStatus(license.getStatus());
                }
            }
        }
        save(license);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#listLicensesInGroup(long)
     */
    public List<PtsLicense> listLicensesInGroup(Long groupNumber)
                                                throws DataAccessException {
        return getPrimaryDAO().listLicensesInGroup(groupNumber);
    }

    /**
    *
    * {@inheritDoc}
    * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#listLicensesInGroup(long)
    */
   public List<PtsLicenseReport> listLicensesForPtsLicenseReport(
       QueryDecorator queryDecorator, 
       String licenseNumber, 
       Set<String> operatorId, 
       Date startTime, 
       Date endTime) throws DataAccessException {
       
       WhereClause whereClause = new WhereClause();
       whereClause.add(licenseNumber, "obj.number");
       whereClause.add(operatorId, "pd.operator.common.operatorIdentifier");
       
       if (!whereClause.toString().equalsIgnoreCase("")) {
           queryDecorator.setWhereClause(whereClause.toString());
       }
       return getPrimaryDAO().listLicensesForPtsLicenseReport(
           queryDecorator, startTime, endTime);
   }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#findLicenseByNumber(java.lang.Long)
     */
    public PtsLicense findLicenseByNumber(String licenseNumber)
                                          throws DataAccessException {
        return this.getPrimaryDAO().findLicenseByNumber(licenseNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManager#executeUngroupAssignments(java.util.ArrayList, boolean)
     */
    public void executeUngroupAssignments(ArrayList<PtsLicense> licenses)
    throws DataAccessException, BusinessRuleException {

        List<PtsLicense> group = listLicensesInGroup(
            licenses.get(0).getGroupInfo().getGroupNumber());

        // all assignment must be specified if from task
        if (!licenses.containsAll(group)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.UNGROUP_TASK_NOT_ALL_ASSIGNMENTS,
                new UserMessage("assignment.ungroup.error.notAllSelected"));
        }

        for (PtsLicense l : group) {
            //If 1 or less assignments would remain in group
            //of if assignment is in list being ungrouped
            if ((group.size() - licenses.size() <= 1)
                || licenses.contains(l)) {
                l.getGroupInfo().setGroupCount(1);
                l.getGroupInfo().setGroupNumber(null);
                l.getGroupInfo().setGroupPosition(null);
                l.getGroupInfo().setRequestedOrderNo(1);
            } else {
                l.getGroupInfo().setGroupCount(group.size()
                    - licenses.size());
            }
        }
        // Now that we are done -- renumber the group positions
        // for assignments left in the list --
        // Assignments still in the list do not have NULL group position
        Integer iPosition = 1;
        for (PtsLicense a : group) {
            if (a.getGroupInfo().getGroupNumber() != null) {
                a.getGroupInfo().setGroupPosition(iPosition++);
            }
        }
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 