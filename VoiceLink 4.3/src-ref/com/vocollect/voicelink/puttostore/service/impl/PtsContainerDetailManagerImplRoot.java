/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsContainerDetailDAO;
import com.vocollect.voicelink.puttostore.model.PtsContainerDetail;
import com.vocollect.voicelink.puttostore.service.PtsContainerDetailManager;

import java.util.List;

/**
 * Additional service methods for the <code>PtsContainerDetail</code> model object.
 *
 * @author mkoenig
 */
public abstract class PtsContainerDetailManagerImplRoot extends
    GenericManagerImpl<PtsContainerDetail, PtsContainerDetailDAO> implements PtsContainerDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsContainerDetailManagerImplRoot(PtsContainerDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
            return this.getPrimaryDAO().
                listContainerDetails(new QueryDecorator(rdi));
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 