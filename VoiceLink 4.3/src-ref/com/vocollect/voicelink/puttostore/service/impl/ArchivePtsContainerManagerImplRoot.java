/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.puttostore.dao.ArchivePtsContainerDAO;
import com.vocollect.voicelink.puttostore.model.ArchivePtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Additional service methods for the
 * <code>Archive Put To Store Container</code> model object.
 *
 * @author mlashinsky
 */

public abstract class ArchivePtsContainerManagerImplRoot extends
    GenericManagerImpl<ArchivePtsContainer, ArchivePtsContainerDAO> implements
    ArchivePtsContainerManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchivePtsContainerManagerImplRoot(ArchivePtsContainerDAO primaryDAO) {
        super(primaryDAO);
    }

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Implementation to get a list of all containers.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManager
     *      #listAllContainers()
     */
    public List<ArchivePtsContainer> listAllArchivePtsContainers()
        throws DataAccessException {
        return getPrimaryDAO().listAllContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        return this.getPrimaryDAO().listArchivePtsContainers(
            new QueryDecorator(rdi));
    }

    /**
     * @param containerNumber - Container number to search for
     * @return PtsContainer that belongs to the containerNumber searched.
     * @throws DataAccessException -.
     */
    public ArchivePtsContainer findArchivePtsContainerByNumber(String containerNumber)
        throws DataAccessException {
        return this.getPrimaryDAO().findArchivePtsContainerByNumber(
            containerNumber);
    }

    /**
     * Adds leading zeros to the container number.
     *
     * @param contNumber - the container number to pad
     * @param length - length to pad to
     * @return the padded container number
     */
    public String padContainerNumber(String contNumber, int length) {
        while (contNumber.length() < length) {
            contNumber = "0" + contNumber;
        }
        return contNumber;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManagerRoot#updateOlderThan(java.util.Date,
     *      com.vocollect.voicelink.puttostore.model.PtsContainerStatus)
     */
    public Integer updateOlderThan(Date createdDate, PtsContainerStatus status)
        throws DataAccessException {
        return getPrimaryDAO().updateOlderThan(createdDate, status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.puttostore.model.PtsContainerStatus,
     *      java.util.Date)
     */
    public int executePurge(PtsContainerStatus status, Date olderThan) {
        int returnRecords = 0;

        // Get List of Archive PTS Containers to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Fetching Archive PTS Container(s) to Purge :::");
        }
        try {
            returnRecords = updateOlderThan(olderThan, status);
        } catch (DataAccessException e) {
            log.warn("Error getting Archive "
                + "PTS Container(s) from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Archive PTS Container(s) for Purge :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManagerRoot#listPtsArchiveContainersForContainerReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.lang.String, java.util.Set, java.util.Date, java.util.Date,
     *      java.lang.Long)
     */
    public List<PtsContainerReport> listPtsArchiveContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                               String containerNumber,
                                                                               Set<String> operatorIds,
                                                                               Date startDate,
                                                                               Date endDate,
                                                                               Long siteId)
        throws DataAccessException {

        WhereClause whereClause = new WhereClause();
        whereClause.add(containerNumber, "obj.containerNumber");
        whereClause.add(operatorIds, "cd.operatorIdentifier");
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }
        return getPrimaryDAO().listArchivePtsContainersForPtsContainerReport(
            queryDecorator, startDate, endDate, siteId);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 