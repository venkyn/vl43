/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.ArchivePtsLicenseLaborDAO;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseLaborManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Manager Implementation class for Archive PTS License Labor.
 * @author kudupi
 */
public abstract class ArchivePtsLicenseLaborManagerImplRoot extends 
        GenericManagerImpl <ArchivePtsLicenseLabor, ArchivePtsLicenseLaborDAO>
    implements ArchivePtsLicenseLaborManager {


    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchivePtsLicenseLaborManagerImplRoot(ArchivePtsLicenseLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.getPrimaryDAO().listArchivePtsLicenseLabor(new QueryDecorator(rdi));
    }
    
     /**
       * {@inheritDoc}
       * @see com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseLaborManagerRoot#listArchiveLicensesLaborForPtsLicenseLaborReport(String, Date, Date, Long)  
       */    
       public List<PtsLicenseLaborReport> listArchiveLicensesLaborForPtsLicenseLaborReport(
           QueryDecorator decorator, 
           Set<String> allOperatorIds,
           Date startTime, 
           Date endTime,
           Long siteId) throws DataAccessException {
           
           WhereClause whereClause = new WhereClause();
           whereClause.add(allOperatorIds, "obj.operatorIdentifier");

           if (!whereClause.toString().equalsIgnoreCase("")) {
               decorator.setWhereClause(whereClause.toString());
           }
           
         return getPrimaryDAO().listArchiveLicensesLaborForPtsLicenseLaborReport(
             decorator, startTime, endTime, siteId);       
      }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 