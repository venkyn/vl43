/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.puttostore.dao.PtsLicenseLaborDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author treed
 */
public interface PtsLicenseLaborManagerRoot extends GenericManager <PtsLicenseLabor, PtsLicenseLaborDAO>,
    DataProvider {

    /**
     *
     * @param startTime - Assignment Start time.
     * @param operator - Operator that is picking the assignment.
     * @param license - The PtsLicense object.
     * @param operLaborRecord - The operator labor to associate the assignment labor.
     *
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
     void openLaborRecord(Date startTime,
                          Operator operator,
                          PtsLicense license,
                          OperatorLabor operLaborRecord)
                          throws DataAccessException, BusinessRuleException;


    /**
     * @param endTime - Assignment Start time.
     * @param license - The PtsLicense object.
     * @return - Labor record closed.
     *
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
     PtsLicenseLabor closeLaborRecord(Date endTime,
                           PtsLicense license)
                           throws DataAccessException, BusinessRuleException;

     /**
      * @param al - Assignment Labor record to recalculate values and save
      * 
      * @throws DataAccessException - if db failure
      * @throws BusinessRuleException - if save fails.
      */
     void recalculateAggregatesAndSave(PtsLicenseLabor al) throws DataAccessException, BusinessRuleException;
     
    /**
     * Retrieves the open PtsLicense Labor record by license ID,
     * or returns null if no labor record exists.
     *
     * @param licenseId - the license ID
     * @return the opened labor record.
     * @throws DataAccessException on any failure.
     */
     PtsLicenseLabor findOpenRecordByLicenseId(long licenseId) throws DataAccessException;


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager#listOpenRecordsByOperatorId(java.lang.String)
     */
     List<PtsLicenseLabor> listOpenRecordsByOperatorId(long operatorId) throws DataAccessException;


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager#findOpenLaborRecord(java.lang.String)
     */
     List<PtsLicenseLabor> listClosedRecordsByBreakLaborId(long breakLaborId) throws DataAccessException;


     /**
      * Retrieves the open license labor records for the given operator
      * or returns null if there is no open assignment labor records.
      *
      * @param operatorLaborId - the ID of the Operator
      * @return the requested list of license labor records or null
      * @throws DataAccessException on any failure.
      */
      List<PtsLicenseLabor>  listAllRecordsByOperatorLaborId(long operatorLaborId)
                                                             throws DataAccessException;

     /**
      * {@inheritDoc}
      * @see com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager#sumQuantityPicked(java.lang.String)
      */
      Long sumQuantityPut(long operatorLaborId) throws DataAccessException;

    /**
     * Get list of licenses labor for PTS License Labor Report.
     *
     * @param decorator - the decorator for the where clause.
     * @param allOperatorIds - operator ID for PTS License Labor Report.
     * @param startTime - Start Time of the PTS License Labor Report.
     * @param endTime - endTime of the PTS License Labor Report.
     * @param siteId - the site of the Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseLaborReport> listLicensesLaborForPtsLicenseLaborReport(QueryDecorator decorator, 
        Set<String> allOperatorIds,
        Date startTime,
        Date endTime,
        Long siteId)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 