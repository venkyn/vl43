/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.puttostore.dao.ArchivePtsLicenseDAO;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Purge Archive-related operations.
 *
 * @author mlashinsky
 */
public interface ArchivePtsLicenseManagerRoot extends
    GenericManager<ArchivePtsLicense, ArchivePtsLicenseDAO>, DataProvider {

    /**
     * deletes archived PTS Licenses.
     *
     * @param createdDate - date to check for
     * @param status - status of PTS Licenses
     * @return - Number of row deleted
     * @throws DataAccessException - Database Access Exceptions
     */
    Integer updateOlderThan(Date createdDate, PtsLicenseStatus status)
        throws DataAccessException;

    /**
     * Purges PTS Licenses based on date and status.
     *
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of PTS Licenses purged
     */
    public int executePurge(PtsLicenseStatus status, Date olderThan);
    
    /**
     * Get list of Archive licenses for PTS License Report.
     *
     * @param licenseNumber - the license number for PTS Report.
     * @param operatorIDAll - operator ID for PTS Report.
     * @param startTime - Start Time of the Report.
     * @param endTime - endTime of the Report. 
     * @param siteId - Site Id for the Report.
     * @param operators - operators for PTS Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseReport> listArchiveLicensesForPtsLicenseReport(
        QueryDecorator queryDecorator,
        String licenseNumber, 
        Set<String> operatorIDAll, 
        Date startTime, 
        Date endTime, 
        Long siteId) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 