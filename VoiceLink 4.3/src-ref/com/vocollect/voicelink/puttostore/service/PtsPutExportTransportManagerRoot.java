/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * @author svoruganti
 *
 */
public interface PtsPutExportTransportManagerRoot extends PtsPutManager {

    /**
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedPuts()
    throws DataAccessException;


    /**
     * @return list of maps.  Each map contains the fields for a puts file picked record,
     *         the object, and the matchingStatus.
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listPutsPut()
    throws DataAccessException;

    /**
     *
     * @return list of export finish
     * @throws DataAccessException on database error.
     */
    public List<Map<String, Object>> listExportFinish() throws DataAccessException;

    /**
     * get license labor records to export.
     *
     * @return list of export labor records
     * @throws DataAccessException - database errors
     */
    public List<Map<String, Object>> listExportLicenseLabor() throws DataAccessException;

    /**
     * update put details to be exported.
     *
     * @return - number of container updated
     * @throws DataAccessException - database exceptions
     */
    public Integer updatePutsPut() throws DataAccessException;


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 