/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.puttostore.dao.PtsRegionDAO;
import com.vocollect.voicelink.puttostore.model.PtsRegion;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Put to Store-related operations.
 *
 * @author svoruganti
 */
public interface PtsRegionManagerRoot extends
    GenericManager<PtsRegion, PtsRegionDAO>, DataProvider {

    /**
     * retrieves the region specified.
     *
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    PtsRegion findRegionByNumber(int regionNumber)
        throws DataAccessException;

    /**
     * Get the list of summary objects for license summary.
     *
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listLicenseSummary(ResultDataInfo rdi)
    throws DataAccessException;

    /**
     * Get the list of summary objects for current work summary.
     *
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)
    throws DataAccessException;

    /**
     * Get the list of summary objects for route summary.
     *
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listRouteSummary(ResultDataInfo rdi)
    throws DataAccessException;

   /** Validate the business rules for editing a puttostore region region.
    * @param region - region to edit
    * @throws DataAccessException - indicates database error
    * @throws BusinessRuleException - thrown in business rule violated
    */
   void executeValidateEditRegion(PtsRegion region)
       throws DataAccessException, BusinessRuleException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 