/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.puttostore.dao.PtsLicenseDAO;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for License-related operations.
 *
 * @author mnichols
 */
public interface PtsLicenseManagerRoot extends
    GenericManager<PtsLicense, PtsLicenseDAO>, DataProvider {

    /**
     * Used for reseting state during unit tests.
     */
    public void initializeState();

    /**
     * list licenses for the given number, operator, region.
     *
     * @param queryDecorator - limit result set
     * @param number - License Number
     * @param regionId - filter results by region
     * @return - the license object or null
     * @throws DataAccessException - on database exception
     */
    public List<PtsLicense> listLicenseByNumber(QueryDecorator queryDecorator,
                                                String number,
                                                Region regionId)
    throws DataAccessException;

    /**
     * Get a list of licenses that has this partial number.
     * @param queryDecorator - limit result set
     * @param partialNumber - partial License Number
     * @param regionId - filter results by region
     * @return list of requested licenses
     * @throws DataAccessException - on database exception
     */
    public List<PtsLicense> listLicenseByPartialNumber(QueryDecorator queryDecorator,
                                                       String partialNumber,
                                                       Region regionId)
    throws DataAccessException;


    /**
     * Find license by number.
     *
     * @param licenseNumber - group number to retrieve
     * @return - list of licenses in group
     * @throws DataAccessException - database exception
     */
    PtsLicense findLicenseByNumber(String licenseNumber)
        throws DataAccessException;


    /**
     * Get the list of licenses reserved by an operator.
     *
     * @param reserveValue - the value of the reserved property in the license object
     * @return - the list of reserved licenses
     * @throws DataAccessException - on db exception
     */
    List<PtsLicense> listReservedLicenses(String reserveValue)
    throws DataAccessException;

    /**
     * un-reserves all licenses for the given operator.
     *
     * @param operator - the operator to un-reserve the licenses for.
     * @throws DataAccessException - on db exception
     */
    public void executeClearReservedLicenses(Operator operator)
    throws DataAccessException;

    /**
     * get list of active licenses for an operator.
     *
     * @param operator -  operator to get licenses for.
     * @return a list of active licenses.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicense> listActiveLicenses(Operator operator)
    throws DataAccessException;

    /**
     * Save the changes to a license.
     *
     * @param license - modified license
     * @throws BusinessRuleException - business rule exception
     * @throws DataAccessException - on database exceptions
     */
    void executeLicenseChangeSave(PtsLicense license)
    throws DataAccessException, BusinessRuleException;


    /**
     * get list of licenses by group number.
     *
     * @param groupNumber - the groupNumber to use for retrieval.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicense> listLicensesInGroup(Long groupNumber)
    throws DataAccessException;

    /**
     * Get list of licenses for PTS License Report.
     *
     * @param queryDecorator - the decorator for the where clause.
     * @param licenseNumber - the license number for PTS Report.
     * @param operatorId - operator ID's for PTS Report.
     * @param startTime - Start Time of the Report.
     * @param endTime - endTime of the Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseReport> listLicensesForPtsLicenseReport(
         QueryDecorator queryDecorator,
         String licenseNumber,
         Set<String> operatorId,
         Date startTime,
         Date endTime)
        throws DataAccessException;


    /**
     * Validate and ungroups assignments.
     * @param licenses group of assignments to ungroup
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on violation of business logic.
     */
    void executeUngroupAssignments(ArrayList<PtsLicense> licenses)
    throws DataAccessException, BusinessRuleException;
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 