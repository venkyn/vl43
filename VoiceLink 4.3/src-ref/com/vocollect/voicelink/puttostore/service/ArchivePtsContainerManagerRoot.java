/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.puttostore.dao.ArchivePtsContainerDAO;
import com.vocollect.voicelink.puttostore.model.ArchivePtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Put To Store Container related operations.
 *
 * @author mlashinsky
 */
public interface ArchivePtsContainerManagerRoot extends
    GenericManager<ArchivePtsContainer, ArchivePtsContainerDAO>, DataProvider {


    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<ArchivePtsContainer> listAllArchivePtsContainers() throws DataAccessException;

    /**
     * retrieves a container by the container number.
     *
     * @param containerNumber - value to retrieve by
     * @return the PtsContainer object
     * @throws DataAccessException - database exceptions
     */
    ArchivePtsContainer findArchivePtsContainerByNumber(String containerNumber)
        throws DataAccessException;

    /**
     * Adds leading zeros to the container number.
     *
     * @param contNumber - the container number to pad
     * @param length - length to pad to
     * @return the padded container number
     */
    public String padContainerNumber(String contNumber, int length);

    /**
     * Purges archived PTS Containers.
     *
     * @param createdDate - date to check for
     * @param status - status of PTS Containers
     * @return - Number of row deleted
     * @throws DataAccessException - Database Access Exceptions
     */
    Integer updateOlderThan(Date createdDate, PtsContainerStatus status)
        throws DataAccessException;

    /**
     * Purges PTS Containers based on date and status.
     *
     * @param status - PTS Container status to purge
     * @param olderThan - date to purge by
     * @return - number of PTS Containers purged
     */
    public int executePurge(PtsContainerStatus status, Date olderThan);
    
    
    /**
     * Gets data for the PTS Containers Report.
     * 
     * @param queryDecorator contains the generated where clause
     * @param containerNumber the String value of the container number
     * @param allOperatorId Set of all operator ids.
     * @param startTime Date containing the start time
     * @param endTime Date containing the end time
     * @param siteId site containing the information required.
     * @throws DataAccessException on failure to retrieve data.
     * @return list of PtsContainers meeting the criteria
     */
    List<PtsContainerReport> listPtsArchiveContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                        String containerNumber,
                                                                        Set<String> allOperatorId,
                                                                        Date startTime,
                                                                        Date endTime,
                                                                        Long siteId)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 