/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.puttostore.dao.PtsContainerDAO;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Put To Store Container related operations.
 *
 * @author mnichols
 */
public interface PtsContainerManagerRoot extends
    GenericManager<PtsContainer, PtsContainerDAO>, DataProvider {


    /** This is the minimum length for a system generated container number.
     * Auto-generated PtsContainer numbers will be padded to this length.
     */
    static final int CONTAINER_NUMBER_SIZE = 10;



    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer> listAllContainers()
        throws DataAccessException;


    /**
     * retrieves a container by the container number.
     *
     * @param containerNumber - value to retrieve by
     * @return the PtsContainer object
     * @throws DataAccessException - database exceptions
     */
    PtsContainer findPtsContainerByNumber(String containerNumber)
        throws DataAccessException;



    /**
     * Updates the status of the container passed in to Closed.
     *
     * @param containerToClose - the ptsContainer to close
     * @throws DataAccessException - if container can't be saved.
     * @throws BusinessRuleException - if container can't be closed.
     */
    void closeContainer(PtsContainer containerToClose) throws
    DataAccessException, BusinessRuleException;


    /**
     * Retrieves all open containers at the requested customer location.
     * @param customerLocation -Pts customer location object
     *
     * @return the list of PtsContainer objects
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer>
        listOpenPtsContainersByCustomerLocation(PtsCustomerLocation customerLocation)
                                                throws DataAccessException;


    /**
     * Retrieves all open containers with the partial container number
     * at the requested customer location.
     *
     * @param partialContainerNumber - the requested partial container number.
     * @param customerLocation - Pts customer location object
     *
     * @return the list of PtsContainer objects
     *
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer>
        listOpenPtsContainersByPartialNumberAndLocation(String partialContainerNumber,
                                                        PtsCustomerLocation customerLocation)
                                                        throws DataAccessException;


    /**
     * Retrieves the open container for the requested container number
     * at the requested customer location.
     *
     * @param containerNumber - the requested container number.
     * @param customerLocation - the customer location object
     *
     * @return the list of PtsContainer objects
     *
     * @throws DataAccessException - database exceptions
     */
     PtsContainer findPtsContainerByNumberAndLocation(String containerNumber,
                                                      PtsCustomerLocation customerLocation)
                                                      throws DataAccessException;


    /**
     * Get the last (minSpokenLength) number of digits of the container number.
     * If container number is shorter than specified length,
     * then pad up to correct length with zeros so it can be spoken by operator.
     *
     * @param containerNumber - container number to check
     * @param minSpokenLength - Minimum spoken length
     * @return - last minSpokenLength digits of container (padded on left with zeros)
     */
    String getContainerDigitsToSpeak(String containerNumber, int minSpokenLength);


    /**
     * Adds leading zeros to the container number.
     *
     * @param contNumber - the container number to pad
     * @param length - length to pad to
     * @return the padded container number
     */
    public String padContainerNumber(String contNumber, int length);

    /**
     * Gets Data for PTS Container Report.
     * 
     * @param queryDecorator the query decorator for the where clause
     * @param containerNumber the String value of the container number
     * @param operatorIds Set of all operator ids to fetch data for
     * @param startTime Date value of the start time
     * @param endTime Date value of the end time
     * @return List of containers meeting the criteria
     * @throws DataAccessException
     */
    List<PtsContainerReport> listPtsContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                 String containerNumber,
                                                                 Set<String> operatorIds,
                                                                 Date startTime,
                                                                 Date endTime)
        throws DataAccessException;
}


*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 