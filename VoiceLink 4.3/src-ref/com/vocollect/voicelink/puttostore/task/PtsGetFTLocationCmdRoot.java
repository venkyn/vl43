/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 * @author pfunyak
 *
 *  This command returns the flow through location to the task
 *  for the given region.
 *  NOTE: this location can be empty.
 *
 */
public class PtsGetFTLocationCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = 7866286997005708805L;

    private int                 regionNumber;

    private PtsRegionManager    ptsRegionManager;


    /**
     * @return the regionNumber
     */
    public int getRegionNumber() {
        return regionNumber;
    }

    /**
     * @param regionNumber the regionNumber to set
     */
    public void setRegionNumber(int regionNumber) {
        this.regionNumber = regionNumber;
    }

    /**
     * @return the ptsRegionManager
     */
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }

    /**
     * @param ptsRegionManager the ptsRegionManager to set
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     *  {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    protected Response doExecute() throws Exception {
        this.buildResponse();
        return this.getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - on task error
     */
    protected void buildResponse() throws DataAccessException, TaskCommandException {

        PtsRegion r = this.ptsRegionManager.findRegionByNumber(this.getRegionNumber());
        getResponse().addRecord(buildResponseRecord(r));
    }

    /**
     * Build Response Record.
     *
     * @param r - region to retrieve the flow through location from.
     * @return - response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(PtsRegion r) throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();
        record.put("ftLocation", r.getFlowThruLocation());
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 