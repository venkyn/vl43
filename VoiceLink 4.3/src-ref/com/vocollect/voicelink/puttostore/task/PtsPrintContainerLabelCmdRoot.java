/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.AbstractPrintServer;
import com.vocollect.voicelink.printserver.LabelData;
import com.vocollect.voicelink.printserver.PrintJob;
import com.vocollect.voicelink.printserver.PrintServerFactory;
import com.vocollect.voicelink.printserver.PtsContainerLabel;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 *
 *
 * @author treed
 */
public class PtsPrintContainerLabelCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = -3144540232911839443L;

    private String                      groupId;

    private Integer                     printerNumber;

    private Long                        locationId;

    private String                      containerNumber;

    private PrinterManager              printerManager;

    private Printer                     printer;

    private List <LabelData>            labels = new ArrayList<LabelData>();

    private PtsCustomerLocationManager  ptsCustomerLocationManager;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        // Check for valid printer number
        validatePrinter();
        printLabels();
        buildResponse();
        return getResponse();
    }

    /**
     * Getter for the groupId property.
     * @return Long value of the property
     */
    public String getGroupId() {
        return this.groupId;
    }

    /**
     * Setter for the groupId property.
     * @param groupId the new groupId value
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Getter for the labels property.
     * @return List&lt;LabelData&gt; value of the property
     */
    public List<LabelData> getLabels() {
        return this.labels;
    }

    /**
     * Setter for the labels property.
     * @param labels the new labels value
     */
    public void setLabels(List<LabelData> labels) {
        this.labels = labels;
    }

    /**
     * Getter for the printer property.
     * @return Printer value of the property
     */
    public Printer getPrinter() {
        return this.printer;
    }

    /**
     * Setter for the printer property.
     * @param printer the new printer value
     */
    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    /**
     * Getter for the printerManager property.
     * @return PrinterManager value of the property
     */
    public PrinterManager getPrinterManager() {
        return this.printerManager;
    }

    /**
     * Setter for the printerManager property.
     * @param printerManager the new printerManager value
     */
    public void setPrinterManager(PrinterManager printerManager) {
        this.printerManager = printerManager;
    }

    /**
     * Getter for the printerNumber property.
     * @return Long value of the property
     */
    public Integer getPrinterNumber() {
        return this.printerNumber;
    }

    /**
     * Setter for the printerNumber property.
     * @param printerNumber the new printerNumber value
     */
    public void setPrinterNumber(Integer printerNumber) {
        this.printerNumber = printerNumber;
    }

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }

    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for the locationId property.
     * @return String value of the property
     */
    public Long getLocationId() {
        return this.locationId;
    }

    /**
     * Setter for the locationId property.
     * @param locationId the new locationId value
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Getter for the ptsCustomerLocationManager property.
     * @return PtsCustomerLocationManager value of the property
     */
    public PtsCustomerLocationManager getPtsCustomerLocationManager() {
        return this.ptsCustomerLocationManager;
    }


    /**
     * Setter for the ptsCustomerLocationManager property.
     * @param ptsCustomerLocationManager the new ptsCustomerLocationManager value
     */
    public void setPtsCustomerLocationManager(PtsCustomerLocationManager ptsCustomerLocationManager) {
        this.ptsCustomerLocationManager = ptsCustomerLocationManager;
    }

    /**
     * Validate the printer number is in the VoiceLink database
     * and it is enabled.
     * @throws DataAccessException - on database exceptions
     * @throws TaskCommandException - when printer number is not found.
     */
    protected void validatePrinter()
    throws DataAccessException, TaskCommandException {
        printer = printerManager.findByNumber(printerNumber);
        if (printer == null) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_NOT_FOUND,
                printerNumber);
        }

        if (!this.printer.getIsEnabled()) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_IS_DISABLED,
                printerNumber);

        }
    }
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     *
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        return record;
    }

    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    protected void printLabels()
        throws DataAccessException, VocollectException {

        PtsContainer container = null;
        PtsCustomerLocation customerLocation;

        try {
            customerLocation = this.getPtsCustomerLocationManager().get(this.getLocationId());
        } catch (EntityNotFoundException e) {
            throw new TaskCommandException(TaskErrorCode.PTS_PRINT_LOCATION_NOT_FOUND,
                    getLocationId().toString());
        }


        // Get the container object
        container = this.getPtsContainerManager()
                        .findPtsContainerByNumberAndLocation(this.getContainerNumber(),
                                                             customerLocation);

        if (container == null || PtsContainerStatus.Closed.equals(container.getStatus())) {
            // If there are no open containers at the location, throw an error.
            throw new TaskCommandException(TaskErrorCode.NO_OPEN_PTS_CONTAINERS_AT_LOCATION,
                                           getLocationId().toString());
        } else {
            // Build the label and send it to the printer.
            PtsContainerLabel pcl = createContainerLabel(container);
            labels.add(pcl.getLabelData());
            sendLabelsToPrinter();
        }
    }

    /**
     * @param container - the container to be printed
     * @return PtsContainerLabel
     */
    protected PtsContainerLabel createContainerLabel(PtsContainer container) {
        Short copies = 1;
        return new PtsContainerLabel(container, copies);
    }

    /**
     * Send the member variable labels to the AbstractPrintServer.
     * @throws TaskCommandException when the print server throws errors.
     */
    protected void sendLabelsToPrinter() throws TaskCommandException {
        // If we have labels - print them
        PrintJob job = null;

        try {
            job = new PrintJob(this.printer.getName(),
                                this.labels);
        } catch (VocollectException e) {
            throw new TaskCommandException(
                TaskErrorCode.PRINTSERVER_EXCEPTION,
                e.getErrorCode());
        }

        try {
            AbstractPrintServer printServer =
                PrintServerFactory.getPrintServer();
            if (this.labels.size() > 0) {
                printServer.submitJob(job);
            }
        } catch (VocollectException e) {
            throw new TaskCommandException(
                  TaskErrorCode.PRINTSERVER_EXCEPTION,
                  e.getErrorCode());
        }
        return;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 