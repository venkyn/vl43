/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 * @author pfunyak
 */
public class PtsValidRegionsCmdRoot extends BasePutToStoreTaskCommand {


    private static final long serialVersionUID = 1L;

    private RegionManager               regionManager;

    private TaskFunctionManager         taskFunctionManager;

    private List<Region>                returnRegions;


    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

     /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the taskFunctionManager property.
     * @return TaskFunctionManager value of the property
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return this.taskFunctionManager;
    }

    /**
     * Setter for the taskFunctionManager property.
     * @param taskFunctionManager the new taskFunctionManager value
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * Getter for the returnRegions property.
     * @return List&lt;Region&gt; value of the property
     */
    public List<Region> getReturnRegions() {
        return returnRegions;
    }

    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<Region> returnRegions) {
        this.returnRegions = returnRegions;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    public Response doExecute() throws Exception {
        //Get a list of authorized regions
        this.getAuthorizedRegions();
        this.clearOperatorRegionInfo();
        this.buildResponse();
        // set the operators work type
        this.setOperatorWorkType();
        return getResponse();
    }


    /**
     * Get list of Putaway regions the operator is authorized to work in.
     *
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - on failure to find any authorized regions for
     *                                the given operator.
     **/
    protected void getAuthorizedRegions() throws DataAccessException, TaskCommandException {
        //Get list of authorized regions
        this.setReturnRegions(getRegionManager().listAuthorized(TaskFunctionType.PutToStore,
                              getOperator().getWorkgroup().getId()));
        //If no regions found then throw error not no regions found for function
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION,
                    ResourceUtil.getLocalizedKeyValue(TaskFunctionType.PutToStore.getResourceKey()));
        }
    }

    /**
     * Clear the region properties for the operator.
     * @throws DataAccessException - Database exception
     */
    protected void clearOperatorRegionInfo() throws DataAccessException {
        // clear operators regions before allowing them to choose.
        // since operators can not be assigned to a region via the UI
        // we need to set the assigned region property to null
        this.getOperator().setAssignedRegion(null);
        this.getOperator().setCurrentRegion(null);
        this.getOperator().getRegions().clear();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - on task error
     */
    protected void buildResponse() throws DataAccessException, TaskCommandException {

        // create the list of regions that the operator can choose from.
        for (Region r : getReturnRegions()) {
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build Response Record.
     *
     * @param r - region to build record for
     * @return - response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(Region r)  throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();
        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));

        return record;
    }



    /**
     * Assign the operator to the putaway work type for UI display.
     * @throws DataAccessException - on database exception
     */
    protected void setOperatorWorkType() throws DataAccessException {
        TaskFunction taskFunction =
            this.getTaskFunctionManager().findByType(TaskFunctionType.PutToStore);
        this.getOperator().setCurrentWorkType(taskFunction);

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 