/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;
/**
 *
 *
 * @author jtauberg
 */
public class PtsRegionConfigurationCmdRoot
    extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = 2061532168231195213L;

    private int                 regionNumber;

    private PtsRegion           region;

    private PtsRegionManager    ptsRegionManager;

    private RegionManager       regionManager;

    private LaborManager        laborManager;

    /**
     * Getter for the regionNumber property.
     * @return int value of the property
     */
    public int getRegionNumber() {
        return regionNumber;
    }


    /**
     * Setter for the regionNumber property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(int regionNumber) {
        this.regionNumber = regionNumber;
    }


    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }


    /**
     * Getter for the ptsRegionManager property.
     * @return PtsRegionManager value of the property
     */
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }


    /**
     * Setter for the ptsRegionManager property.
     * @param ptsRegionManager the new ptsRegionManager value
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }


    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }


    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }


    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }


    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    public Response doExecute() throws Exception {

        verifyPassedInRegion();
        checkWorkAuthorization();
        clearReservedLicenses();
        setCurrentRegion();
        openLaborRecord();
        buildResponse();
        return getResponse();
    }


    /**
     * Open a put to store labor record.
     * @throws DataAccessException on Database Error
     * @throws BusinessRuleException on BusinessRuleException
     */
    protected void openLaborRecord() throws DataAccessException, BusinessRuleException {
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
            this.getOperator(), OperatorLaborActionType.PutToStore);

    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record from region.
     *
     * @return - response record.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord()
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        PtsRegion r = this.getRegion();

        String useLut = "0";

        SystemProperty sp = getSystemPropertyManager()
                                .findByName("Use LUTs for ODR Calls");
        if (sp != null) {
            if (sp.getValue().equals("1") || sp.getValue().equals("2")) {
                useLut = sp.getValue();
            }
        }

        record.put("number", r.getNumber());
        record.put("name", translateUserData(r.getName()));
        record.put("skipAisleAllowed", r.isAllowSkipAisle());
        record.put("skipSlotAllowed", r.isAllowSkipSlot());
        record.put("repickSkips", r.isAllowRepickSkips());
        record.put("signOffAllowed", r.isAllowSignOff());
        record.put("allowPassAssignment", r.isAllowPassAssignment());
        record.put("allowMultipleOpenContainers", r.isAllowMultipleOpenContainers());
        record.put("maxLicensesInGroup", r.getMaxLicensesInGroup());
        record.put("systemGeneratesContainerID", r.isSystemGeneratesContainerID());
        record.put("spokenLicenseLength", r.getSpokenLicenseLength());
        record.put("confirmSpokenLicense", r.isConfirmSpokenLicense());
        record.put("validateContainers", r.isValidateContainerId());
        record.put("validateContainerLength", r.getValidateContainerLength());
        record.put("confirmSpokenLocation", r.isConfirmSpokenLocation());
        record.put("spokenLocationLength", r.getSpokenLocationLength());
        record.put("locationCheckDigitLength", r.getLocationCheckDigitLength());
        record.put("useLut", useLut);
        record.put("currentPreAisle", "X");
        record.put("currentAisle", "X");
        record.put("currentPostAisle", "X");
        record.put("currentSlot", "X");
        record.put("printExceptionLabel", false);
        return record;
    }

    /**
     * Verifies that the passed in region is a valid pts region.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void verifyPassedInRegion()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        //Look up passed in region number and save it in Region member.
        setRegion((getPtsRegionManager().findRegionByNumber(getRegionNumber())));
        if (getRegion() == null) {
            // Raise region not found error
            throw new TaskCommandException(TaskErrorCode.INVALID_REGION_NUMBER, String.valueOf(getRegionNumber()));
        }
    }


    /**
     * Verifies that the operator has permissions for the passed in region.
     * Note that ValidPtsRegionCmd also checks this, but things could have changed.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void checkWorkAuthorization() throws DataAccessException, TaskCommandException {
        // Get a list of authorized regions for the operator's worktype
        List<Region> authorizedPtsRegions = this.getRegionManager()
            .listAuthorized(TaskFunctionType.PutToStore,
            getOperator().getWorkgroup().getId());

        // Check to see if the requested region is in authorized regions
        boolean authorized = false;
        for (Region r : authorizedPtsRegions) {
            if (r.equals(getRegion())) {
                authorized = true;
                break;
            }
        }

        // If not authorized for the region raise error
        if (!authorized) {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION,
                TaskFunctionType.PutToStore, getRegionNumber());
        }
    }


    /**
     * Clears any reserved licenses for the operator.
     * @throws DataAccessException - Database Exception
     */
    protected void clearReservedLicenses() throws DataAccessException {
        getPtsLicenseManager().executeClearReservedLicenses(getOperator());
    }


    /**
     * Sets the operator's current region.
     * @throws DataAccessException - Database Exception
     */
    protected void setCurrentRegion() throws DataAccessException {
        // Sets the operators current region to passed in region number
        getOperator().setCurrentRegion(getRegion());
        getOperator().getRegions().add(getRegion());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 