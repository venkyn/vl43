/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCode;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;


/**
 *
 * This class is used to stop an assignment.
 *
 * @author svoruganti
 */
public class PtsStopAssignmentCmdRoot  extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = 2061532168231195213L;

    private LaborManager laborManager;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        if (getLicenses().size() > 0) {
            processStopAssignments();
            ungroupAssignments();
        }
        buildResponse();
        return getResponse();
    }

    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
            getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     *
     * @return record - the response record
     * @throws DataAccessException - database exception
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        //Check operator's assigned to region
        if (getOperator().getAssignedRegion() != null) {
            if (!(getOperator().getAssignedRegion().equals(getOperator().getCurrentRegion()))) {

                // Build LabelObjectPairs for the parameters
                LOPArrayList lop = new LOPArrayList();
                lop.add("task.regionNumber",
                    getOperator().getAssignedRegion().getNumber());

                lop.add("task.regionName", getOperator()
                    .getAssignedRegion().getName());

                // Create a TaskMessageInfo object and pass
                // it to the setErrorFields method.
                record.setErrorFields(createMessageInfo(
                    TaskErrorCode.ASSIGNED_TO_OTHER_REGION, lop));
            }
        }
        return record;
    }

    /**
     * Main Processing method for stopping assignments within the group.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Throws business rule exception
     * @throws TaskCommandException - Task Command Exception
     */
    protected void processStopAssignments()
    throws DataAccessException, BusinessRuleException, TaskCommandException {

        //Call Stop Assignment for each assignment in group
        for (PtsLicense l : getLicenses()) {
            try {
                this.getLaborManager().closePtsLicenseLaborRecord(getCommandTime(), l);
                l.stopAssignment(PtsLicenseStatus.Complete, getCommandTime());
            } catch (BusinessRuleException bre) {

                    if (PutToStoreErrorCode.ASSIGNMENT_PUT_NOT_COMPLETED.equals(
                        bre.getErrorCode())) {
                           throw new TaskCommandException(TaskErrorCode.NOT_ALL_PUTS_PUT,
                                                          l.getNumber());
                } else {
                    throw bre;
                }
            }
        }
    }

    /**
     * Ungroup assignments if they belong to a manual issuance region.
     *
     * @throws DataAccessException - database excetption
     * @throws BusinessRuleException Business rule exception
     */
    protected void ungroupAssignments()
    throws DataAccessException, BusinessRuleException {
        getPtsLicenseManager().executeUngroupAssignments(
            (ArrayList<PtsLicense>) getLicenses());
    }


    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }


    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 