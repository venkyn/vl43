/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.selection.dao.GroupNumberDAO;
import com.vocollect.voicelink.selection.model.GroupNumber;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.NotPut;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Partial;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Skipped;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * @author estoll
 */
public class PtsGetAssignmentCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = -8193931427093624706L;

    private GroupNumberDAO          groupNumberDAO;

    private PtsRegionManager        ptsRegionManager;
    private PtsRegion               ptsRegion;

    private ArrayList<PtsLicense>   ptsLicenses = new ArrayList<PtsLicense>();

    private LaborManager            laborManager;


    /**
     * Getter for the groupNumberDAO property.
     * @return GroupNumberDAO value of the property
     */
    public GroupNumberDAO getGroupNumberDAO() {
        return this.groupNumberDAO;
    }


    /**
     * Setter for the groupNumberDAO property.
     * @param groupNumberDAO the new groupNumberDAO value
     */
    public void setGroupNumberDAO(GroupNumberDAO groupNumberDAO) {
        this.groupNumberDAO = groupNumberDAO;
    }

    /**
     * Getter for the ptsLicenses property.
     * @return ArrayList PtsLicense  value of the property
     */
    public ArrayList<PtsLicense> getPtsLicenses() {
        return this.ptsLicenses;
    }

    /**
     * Setter for the ptsLicenses property.
     * @param ptsLicenses the new ptsLicenses value
     */
    public void setPtsLicenses(ArrayList<PtsLicense> ptsLicenses) {
        this.ptsLicenses = ptsLicenses;
    }

    /**
     * Getter for the ptsRegion property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return this.ptsRegion;
    }

    /**
     * Setter for the ptsRegion property.
     * @param ptsRegion the new prtRegion value
     */
    public void setRegion(PtsRegion ptsRegion) {
        this.ptsRegion = ptsRegion;
    }

    /**
     * Getter for the ptsRegionManager property.
     * @return PtsRegionManager value of the property
     */
    public PtsRegionManager getPtsRegionManager() {
        return this.ptsRegionManager;
    }

    /**
     * Setter for the ptsRegionManager property.
     * @param ptsRegionManager the new ptsRegionManager value
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    public Response doExecute() throws Exception {

        //Get PutToStore Region
        setRegion(getPtsRegionManager().get(getOperator().
            getCurrentRegion().getId()));

        //Get list of assignment that are reserved by operator
        //and in this region
        buildReservedLicensesList();

        // We the list of reserved licenses is empty - throw an error
        if (getPtsLicenses().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.NO_LICENSE_FOUND_FOR_OPERATOR);
        }

        //do final updates to the licenses and create labor records
        finalUpdates();

        buildResponse();
        return getResponse();
    }


    /**
     * checks for and retrieves any licenses reserved for an operator
     * and filters list down to current region.
     *
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void buildReservedLicensesList()
        throws DataAccessException, BusinessRuleException {

        //Get List of assignment with the operator ID in the reserved fields
        List<PtsLicense> queriedLicenses = getPtsLicenseManager()
                .listReservedLicenses(getOperatorId());

        //Check each reseved license to see if it is in the correct region
        for (PtsLicense p : queriedLicenses) {
            //Check for correct region
            if (p.getRegion().getId().equals(getRegion().getId())) {
                getPtsLicenses().add(p);
            //Else unreserve the assignment as it should not have been
            //reserved in the first place
            } else {
                p.setReservedBy(null);
            }
        }

    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Returns one response record - totaling information
     * from all the licenses in the group.
     *
     * @throws DataAccessException - database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord()
    throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();

        Integer expectedResiduals = 0;

        ArrayList<Item> distinctItems = new ArrayList<Item>();
        ArrayList<Location> distinctLocations = new ArrayList<Location>();
        Map<String, Integer> expResids = new HashMap<String, Integer>();
        String mapKey;

        for (PtsLicense l : getPtsLicenses()) {
            for (PtsPut put : l.getPuts()) {
                // Only puts that are not done get calculated -- the
                // not done status's are:  NotPut, Skipped and Partial
                // SHORT -- is done in PTS
                if (put.getStatus().isInSet(NotPut, Skipped, Partial)) {
                    // Put the expected residual into a hashmap
                    // based upon the string License number + put id
                    mapKey = l.getNumber() + "_" + put.getItem().getNumber().toString();
                    expResids.put(mapKey, put.getResidualQuantity());

                    // see if we have come across this item before,
                    // if not, add it to the list
                    if (!distinctItems.contains(put.getItem())) {
                        distinctItems.add(put.getItem());
                    }

                    // see if we have come across this location before,
                    // if not, add it to the list
                    if (!distinctLocations.contains(put.getCustomer().getLocation())) {
                        distinctLocations.add(put.getCustomer().getLocation());
                    }
                }
            }
        }

        // now sum up the expectedResidual values in the hashmap
        Collection<Integer> values = expResids.values();
        Iterator<Integer> valuesIter = values.iterator();
        Integer thisValue;
        while (valuesIter.hasNext()) {
            thisValue = valuesIter.next();
            expectedResiduals += thisValue;
        }

        // set the response fields
        record.put("groupID",
            getPtsLicenses().get(0).getGroupInfo().getGroupNumber());
        record.put("totalSlots", distinctLocations.size());
        record.put("totalItems", distinctItems.size());
        record.put("totalExpectedResidual", expectedResiduals);
        record.put("returnLocation", getRegion().getResidualLocation());
        record.put("returnLocationCD", getRegion().getResidualLocationCD());
        record.put("unExpReturnLocation",  getRegion().getUnExpResidualLocation());
        record.put("unExpReturnLocationCD", getRegion().getUnExpResidualLocationCD());
        record.put("performanceLast", "-1");  // VoiceLink does not implement this
        record.put("performanceDaily", "-1"); // VoiceLink does not implement this

        return record;
    }

    /**
     * Do the final updates to the license before issuing them out to
     * the operator.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Throws Task Command exception
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void finalUpdates()
    throws DataAccessException, TaskCommandException, BusinessRuleException {

        //Group assignments if not already grouped
        groupLicenses();

        for (PtsLicense p : getPtsLicenses()) {
            p.startLicense(getOperator(), getCommandTime());
            p.setReservedBy(null);
            // Need to open a labor record for the license assignment
            this.getLaborManager().openPtsLicenseLaborRecord(this.getCommandTime(),
                                                             this.getOperator(),
                                                             p);
        }

    }


    /**
     * Group the licenses together.
     *
     * @throws TaskCommandException - Throws task command exception
     * @throws DataAccessException - Throws data access exceptions
     */
    protected void groupLicenses()
    throws DataAccessException, TaskCommandException {
        validateGroup();

        Long groupNumber = getNextGroupNumber();

        //Set group information
        for (PtsLicense p : getPtsLicenses()) {
            //we don't care if the license is
            // already part of a group -- we make a big assumption
            // that the licenses reserved will be in this group.

            //set info
            p.getGroupInfo().setGroupCount(getPtsLicenses().size());
            p.getGroupInfo().setGroupNumber(groupNumber);
            p.getGroupInfo().setGroupPosition(p.getGroupInfo().getRequestedOrderNo());
        }

    }

    /**
     * Validate licenses can be grouped.
     *
     * @throws TaskCommandException - Task Command Exception
     */
    protected void validateGroup()
    throws TaskCommandException {
        if (getPtsLicenses().size() > getRegion().getMaxLicensesInGroup()) {

            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.maxLicensesInGroup",
                getRegion().getMaxLicensesInGroup());

            // This is a 98 error code
            // forcing sign off will clear the reserve field
            throw new TaskCommandException(
                TaskErrorCode.PTS_TOO_MANY_LICNESES_IN_GROUP, lop);
        }
    }


    /**
     * Get the next group number.
     * NOTE:  In selection, the group number is generated in a manager
     * because grouping can be done via the GUI or that task.  In
     * PTS -- grouping only happens via the task.
     * SHOULD a customization arise that requires the ability to
     * group from the GUI -- this method should be moved up to the
     * PtsLicenseManager.
     *
     * @return - last group number
     * @throws DataAccessException - Database Exceptions
     */
    protected long getNextGroupNumber() throws DataAccessException {
        long returnValue = 0;
        //Check to see if number needs intialized based on current data
        GroupNumber gn = new GroupNumber();
        getGroupNumberDAO().save(gn);
        returnValue = gn.getId();
        getGroupNumberDAO().delete(gn);

        return returnValue;
    }



    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }



    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 