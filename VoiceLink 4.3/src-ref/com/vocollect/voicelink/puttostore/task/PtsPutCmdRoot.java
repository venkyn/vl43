/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsPutDetail;
import com.vocollect.voicelink.puttostore.model.PtsPutDetailType;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.*;

import java.util.Date;
import java.util.List;

/**
 *
 * @author pfunyak
 *
 */
public class PtsPutCmdRoot  extends BasePutToStoreTaskCommand  {
    private static final long serialVersionUID = 8553837953832456540L;

    private long                        customerLocationId;

    private String                      itemNumber;

    private long                        putId;

    private int                         quantityPut;

    private String                      containerNumber;

    private String                      licenseNumber;

    private boolean                     partial;

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;

    private PtsCustomerLocationManager  ptsCustomerLocationManager;

    // The following properties are private to this class and are
    // intended to be used as working objects while updating the put
    // and container records.
    // The getters and setters of these objects are declared as private.

    private PtsPut                          putToUpdate = null;

    private PtsContainer                    openContainer = null;

    /**
     * @return the putToUpdate
     */
    private PtsPut getPutToUpdate() {
        return putToUpdate;
    }

    /**
     * @param putToUpdate the putToUpdate to set
     */
    private void setPutToUpdate(PtsPut putToUpdate) {
        this.putToUpdate = putToUpdate;
    }

    /**
     * @return the openContainer
     */
    private PtsContainer getOpenContainer() {
        return openContainer;
    }

    /**
     * @param openContainer the openContainer to set
     */
    private void setOpenContainer(PtsContainer openContainer) {
        this.openContainer = openContainer;
    }


    /**
     * Getter for the customerLocationId property.
     * @return long value of the property
     */
    public long getCustomerLocationId() {
        return customerLocationId;
    }

    /**
     * Setter for the customerLocationId property.
     * @param customerLocationId the new customerLocationId value
     */
    public void setCustomerLocationId(long customerLocationId) {
        this.customerLocationId = customerLocationId;
    }

    /**
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * @param itemNumber the itemNumber to set
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * Getter for the putId property.
     * @return long value of the property
     */
    public long getPutId() {
        return putId;
    }

    /**
     * Setter for the putId property.
     * @param putId the new putId value
     */
    public void setPutId(long putId) {
        this.putId = putId;
    }

    /**
     * @return the quantityPut
     */
    public int getQuantityPut() {
        return quantityPut;
    }

    /**
     * @param quantityPut the quantityPut to set
     */
    public void setQuantityPut(int quantityPut) {
        this.quantityPut = quantityPut;
    }

    /**
     * @return the containerNumber
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * @param containerNumber the containerNumber to set
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * @return the licenseNumber
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * @param licenseNumber the licenseNumber to set
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * @return the partial
     */
    public boolean isPartial() {
        return partial;
    }

    /**
     * @param partial the partial to set
     */
    public void setPartial(boolean partial) {
        this.partial = partial;
    }

    /**
     * @return the voicelinkNotificationUtil
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return voicelinkNotificationUtil;
    }

    /**
     * @param voicelinkNotificationUtil the voicelinkNotificationUtil to set
     */
    public void setVoicelinkNotificationUtil(
            VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }


    /**
     * @return the ptsCustomerLocationManager
     */
    public PtsCustomerLocationManager getPtsCustomerLocationManager() {
        return ptsCustomerLocationManager;
    }

    /**
     * @param ptsCustomerLocationManager the ptsCustomerLocationManager to set
     */
    public void setPtsCustomerLocationManager(
            PtsCustomerLocationManager ptsCustomerLocationManager) {
        this.ptsCustomerLocationManager = ptsCustomerLocationManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */

    protected Response doExecute() throws Exception {

        // Find the container or throw an exception
        this.findOpenContainer();
        // Find the put record to update or throw an exception.
        this.findPutRecordToUpdate();
        // Found the put record.  Go and update it
        this.updatePutRecord();
        // build the response record and return.
        buildResponse();

        return getResponse();
    }


    /**
     * Update the put record and create the put detail record.
     *
     * @throws DataAccessException - on Database access failure.
     * @throws BusinessRuleException - on business rule exception.
     */
    protected void updatePutRecord() throws DataAccessException, BusinessRuleException {

        PtsPut thePut = this.getPutToUpdate();
        PtsContainer theContainer = this.getOpenContainer();

        int currentQuantityPut = thePut.getQuantityPut() + this.getQuantityPut();
        PtsPutDetail detail = new PtsPutDetail();
        // create the put detail record.
        detail.setOperator(this.getOperator());
        detail.setPutTime(this.getCommandTime());
        detail.setType(PtsPutDetailType.TaskPut);

        // if the put was over packed, add two put detail records.
        // the first is for completed quantity and the second is for the
        // over pack amount.
        if (currentQuantityPut > thePut.getQuantityToPut()) {
           detail.setQuantityPut(thePut.getQuantityToPut() - thePut.getQuantityPut());
           // add the first detail to the put
           thePut.addPutDetail(detail, this.isPartial());
           // add the first detail to the container
           theContainer.addPutDetail(detail);
           detail.setContainerNumber(theContainer.getContainerNumber());
           // add the over pack detail to the put
           detail = new PtsPutDetail();
           detail.setOperator(this.getOperator());
           detail.setPutTime(this.getCommandTime());
           detail.setType(PtsPutDetailType.TaskOverPack);
           detail.setQuantityPut(currentQuantityPut - thePut.getQuantityPut());
        } else {
            detail.setQuantityPut(this.getQuantityPut());
         //   detail.setPut(thePut);
        }
        // add the detail record to the put
        thePut.addPutDetail(detail, this.isPartial());

        // add the detail record to the container if it was not shorted to zero;
        if (this.getQuantityPut() > 0) {
            theContainer.addPutDetail(detail);
            detail.setContainerNumber(theContainer.getContainerNumber());
        }


        // If the put was shorted, short all of the remaining puts for the license/item
        // combination.  Note that we need to create detail records for the shorted puts.
        if (thePut.getStatus() == Shorted) {
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.licenseNumber", thePut.getLicense().getNumber());
            lop.add("task.itemNumber", thePut.getItem().getNumber());
            lop.add("task.totalQuantityShorted",
                    this.getPtsLicenseManager().findLicenseByNumber(this.getLicenseNumber())
                                               .getShortedCountForItem(thePut.getItem()));
            lop.add("task.operator", thePut.getOperator().getOperatorIdentifier());
            try {
                getVoicelinkNotificationUtil().createNotification(
                    "notification.column.keyname.Process.Task",
                    "voicelink.notification.task.puttostore.autoshort",
                    NotificationPriority.CRITICAL,
                    new Date(),
                    "0",
                    lop);
            } catch (Exception ex) {
                // Do nothing -- we don't care about
                // failures to post the notification
            }
        }
    }


    /**
     * Find the put record that needs to be updated.
     * If found, save it for later use.
     *
     * @throws DataAccessException - on database access failure
     * @throws TaskCommandException - on task error
     */
    protected void findPutRecordToUpdate() throws DataAccessException, TaskCommandException {

        PtsPut thePut = null;
        try {
            thePut = this.getPtsPutManager().get(this.getPutId());

            if (thePut.getStatus().isInSet(Put, Shorted)) {
                throw new TaskCommandException(TaskErrorCode.PTS_PUT_ALREADY_COMPLETED,
                                               String.valueOf(this.getPutId()));
            }
        } catch (EntityNotFoundException e) {
            throw new TaskCommandException(TaskErrorCode.PTS_PUT_UPDATING_UNKNOWN_RECORD,
                                           String.valueOf(this.getPutId()));
        }
        // we found it.  Save it for later use
        this.setPutToUpdate(thePut);
    }




    /**
     * Attempt the find the open container for the given location.
     * If this method is successful, save the openContainer for later use.
     * NOTE: The container number can be empty and is probably a partial container number.
     *
     * @throws DataAccessException - database exception.
     * @throws TaskCommandException - on task error
     *
     */
    protected void findOpenContainer() throws DataAccessException, TaskCommandException {

        // if put was shorted to zero, we don't care about the container
        if (this.getQuantityPut() > 0) {
            PtsContainerManager cManager = this.getPtsContainerManager();
            List <PtsContainer>  containerList = null;
            String theContainerNumber = this.getContainerNumber();

            PtsCustomerLocation custLoc =
                this.getPtsCustomerLocationManager().get(this.getCustomerLocationId());
            // check to see if we can find the container.
            if (theContainerNumber.equals("")) {
                containerList =
                    cManager.listOpenPtsContainersByCustomerLocation(custLoc);
                if (containerList.isEmpty()) {
                    throw new TaskCommandException(TaskErrorCode.PTS_PUT_NONE_SPECIFIED_NO_OPEN_FOUND,
                                                   this.getCustomerLocationId());
                } else if (containerList.size() > 1) {
                    throw new TaskCommandException(TaskErrorCode.PTS_PUT_NONE_SPECIFIED_MULTIPLE_FOUND);
                }
            } else { // we were given a container number
                containerList =
                    cManager.listOpenPtsContainersByPartialNumberAndLocation(theContainerNumber, custLoc);
                if (containerList.isEmpty()) {
                    // the given container was not found open at the location.
                    throw new TaskCommandException(TaskErrorCode.PTS_PUT_CONTAINER_SPECIFIED_NOT_FOUND,
                        this.getContainerNumber());
                } else if (containerList.size() > 1) {
                    // we found multiple containers that match the partial number.
                    // for now simply throw a fatal task error (98)
                    throw new TaskCommandException(TaskErrorCode.PTS_PUT_CONTAINER_SPECIFIED_MULTIPLE_FOUND,
                        this.getContainerNumber());
                }
            }
            // if we get here we should only have one open container.
            // set the container number to the only open container.
            this.setOpenContainer(containerList.get(0));
        }
    }

    /**
     * Build Response Record.
     *
     * @return - response record
     * @throws DataAccessException - Database Exceptions
     *
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        return (new TaskResponseRecord());
    }



    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - on task error
     */
    protected void buildResponse() throws DataAccessException, TaskCommandException {

        getResponse().addRecord(buildResponseRecord());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 