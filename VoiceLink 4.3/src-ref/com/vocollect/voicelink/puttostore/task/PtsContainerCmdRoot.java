/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Task Command for Put To Store Container
 * Allows Open, Close, and get list.
 *
 * @author jtauberg
 */
public class PtsContainerCmdRoot extends BasePutToStoreTaskCommand {

    //
    private static final long serialVersionUID = 379297288371448221L;

    //Choices for requestType
    private static final int CONTAINER_OPERATION_GET_LIST = 0;
    private static final int CONTAINER_OPERATION_CLOSE = 1;
    private static final int CONTAINER_OPERATION_OPEN = 2;

    // The type of request for this command.  see choices above.
    private int                         requestType;

    //The Identifier for the put location (We are using the CustomerLocation.id)
    private Long                        customerLocationId;

    //The container number spoken or scanned by operator to be closed is passed in.
    private String                      containerNumber = "";

    // manager for PTS CustomerLocations
    private PtsCustomerLocationManager  ptsCustomerLocationManager;

    // used to retrieve operator current Pts region.
    private PtsRegionManager            ptsRegionManager;

    // Variable to hold the location we are working with. getter/setter is private.
    private PtsCustomerLocation         customerLocation = null;
    // Will store the region currently in use by operator  getter/setter is private.
    private PtsRegion                   ptsRegion;

    // List of "working" container - no getter/setter  for private use only.
    private List <PtsContainer> containerList = new ArrayList<PtsContainer>();

    /**
     * Getter for the requestType property.
     * @return int value of the property
     */
    public int getRequestType() {
        return requestType;
    }


    /**
     * Setter for the requestType property.
     * @param requestType the new requestType value
     */
    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }


    /**
     * @return the customerLocationId
     */
    public Long getCustomerLocationId() {
        return customerLocationId;
    }


    /**
     * @param customerLocationId the customerLocationId to set
     */
    public void setCustomerLocationId(Long customerLocationId) {
        this.customerLocationId = customerLocationId;
    }


    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return containerNumber;
    }


    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }


    /**
     * Getter for the ptsRegion property.
     * @return PtsRegion value of the property
     */
    private PtsRegion getPtsRegion() {
        return ptsRegion;
    }


    /**
     * Setter for the ptsRegion property.
     * @param ptsRegion the new ptsRegion value
     */
    private void setPtsRegion(PtsRegion ptsRegion) {
        this.ptsRegion = ptsRegion;
    }


    /**
     * @return the customerLocation
     */
    private PtsCustomerLocation getCustomerLocation() {
        return customerLocation;
    }


    /**
     * @param customerLocation the customerLocation to set
     */
    private void setCustomerLocation(PtsCustomerLocation customerLocation) {
        this.customerLocation = customerLocation;
    }


    /**
     * Getter for the ptsCustomerLocationManager property.
     * @return PtsCustomerLocationManager value of the property
     */
    public PtsCustomerLocationManager getPtsCustomerLocationManager() {
        return ptsCustomerLocationManager;
    }


    /**
     * Setter for the ptsCustomerLocationManager property.
     * @param ptsCustomerLocationManager the new ptsCustomerLocationManager value
     */
    public void setPtsCustomerLocationManager(PtsCustomerLocationManager ptsCustomerLocationManager) {
        this.ptsCustomerLocationManager = ptsCustomerLocationManager;
    }


    /**
     * @return the ptsRegionManager
     */
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }


    /**
     * @param ptsRegionManager the ptsRegionManager to set
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {

        // Get the customer location object for the given customer location id.
        this.setCustomerLocation(this.getPtsCustomerLocationManager().get(this.getCustomerLocationId()));
        this.containerRequestTypes();
        this.buildResponse();
        return getResponse();
    }

    /**
     * Executes the container request types.
     * @throws BusinessRuleException - Throws business rule exception
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void containerRequestTypes() throws BusinessRuleException, DataAccessException, TaskCommandException {

       // Get the pts region via the operator's current region.
       this.setPtsRegion(this.getPtsRegionManager().get(this.getOperator().getCurrentRegion().getId()));
        // Close container request
        if (this.getRequestType() == CONTAINER_OPERATION_CLOSE) {
            closeContainer();
        // Open new container request
        } else if (this.getRequestType() == CONTAINER_OPERATION_OPEN) {
            // Open the container
            openContainer();
        } else if (this.getRequestType() == CONTAINER_OPERATION_GET_LIST) {
            getOpenContainerListByLocation();
        }
    }

   /**
    * @throws DataAccessException - on db failure
    * @throws TaskCommandException on empty container list.
    */
    protected void getOpenContainerListByLocation() throws DataAccessException, TaskCommandException {
        // get the list of open containers for the customer location.
        this.containerList =
            this.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(this.getCustomerLocation());
        if (this.containerList.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.PTS_CONTAINERS_NO_OPEN_AT_LOCATION,
                    this.getCustomerLocation().getLocation().getSpokenVerification());
        }
    }


    /**
     *  Opens a container.
     *
     * @throws TaskCommandException - can throw
     * @throws DataAccessException - if container lookup fails.
     * @throws BusinessRuleException -
     */
    protected void openContainer() throws TaskCommandException,
                                          DataAccessException, BusinessRuleException {

        // get the list of open containers for the customer location.
        this.containerList =
            this.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(this.getCustomerLocation());
        // check for more than one open container and multiple open not allowed.
        if ((this.containerList.size() > 1) && (!this.getPtsRegion().isAllowMultipleOpenContainers())) {
            throw new TaskCommandException(TaskErrorCode.PTS_CONTAINER_FATAL_ERROR_MULTIPLE_OPENED_NOT_ALLOWED);
        }
        // If we found a single open container at the location and multiple open containers are not allowed,
        // close the open container before opening a new one.
        if ((this.containerList.size() == 1) && (!this.getPtsRegion().isAllowMultipleOpenContainers())) {
            this.getPtsContainerManager().closeContainer(containerList.get(0));
        }
        if (this.getContainerNumber().equals("")) {
            this.setContainerNumber(this.generateContainerNumber());
        } else {
            // If the container number was passed in, verify that the container number
            // does not already exist in the system.  Throw an exception if the
            // container number already exists.
            checkIfContainerAlreadyExists();
            // if we get here the container number is valid.
            // populate the container list with any open containers for the location.
            // Ensure that if multiple containers open, that spoken #s don't match.
            checkLastDigitsUniqueForLocation();
        }
        // clear the container list since we only want to return the container that is
        // currently being opened.
        this.containerList.clear();
        createNewContainer();
    }


    /**
     * This method is called when the container number is passes in to this command.
     * We will verify the the given container number does not already exist in the
     * PtsContainer table.
     *
     * If it already exists and is open, throw an exception.
     * If it already exists and is close, throw an exception.
     *
     * @throws TaskCommandException - Throws task command exceptions above.
     * @throws DataAccessException - Database exception
     */
    protected void checkIfContainerAlreadyExists() throws DataAccessException,
                                                          TaskCommandException {
        // Check if container already exists
        PtsContainer        containerFound = null;
        // Lookup the container number provided
        containerFound = this.getPtsContainerManager().findPtsContainerByNumber(this.getContainerNumber());
        //If container was found, throw an exception.
        if (containerFound != null) {
            throw new TaskCommandException(TaskErrorCode.PTS_CONTAINER_OPEN_ALREADY_EXISTS,
                    containerFound.getContainerNumber());
        }
    }


    /**
     *
     *      Lookup spoken digits at current location to see if the spoken digits are
     *      unique for all all open open containers at the locations.
     *      If it is not unique, throw an exception

     * Does check if region isAllowMultipleOpenContainers....
     * verifies ContainerDigitsToSpeak is unique for open containers at location.
     * @throws DataAccessException -.
     * @throws TaskCommandException - If container number not unique.
     */
    protected void checkLastDigitsUniqueForLocation() throws DataAccessException,
                                                             TaskCommandException {
        //If region ALLOWS multiple open containers...
        if (this.getPtsRegion().isAllowMultipleOpenContainers()) {
            //If we have some open containers...
            if (!this.containerList.isEmpty()) {
                //See if this spoken digits matches another open container
                int length = ptsRegion.getValidateContainerLength();
                String cont2;
                String cont1 =  this.getPtsContainerManager().
                    getContainerDigitsToSpeak(this.getContainerNumber(), length);

                //Loop through all open containers to compare spoken #'s.
                for (PtsContainer c : this.containerList) {
                    cont2 = this.getPtsContainerManager().
                        getContainerDigitsToSpeak(c.getContainerNumber(), length);
                    //If spoken values are same, we have a problem
                    if (cont1.equals(cont2)) {
                        throw new TaskCommandException(TaskErrorCode
                                .PTS_CONTAINER_OPEN_CONTAINER_NUMBER_DUPLICATE_FOR_LOCATION, cont1);
                    }
                }
            }
        }
    }


    /**
     *  Commands to close a container.
     * @throws DataAccessException if findPtsContainer fails.
     * @throws TaskCommandException if container already closed, or not found at location.
     * @throws BusinessRuleException if container close fails.
     */
    protected void closeContainer() throws DataAccessException, TaskCommandException,
                                           BusinessRuleException {

        // user didn't specify number
         if (this.getContainerNumber().equals("")) {
            // Look-up the container by location passed in
            containerList =
                this.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(this.getCustomerLocation());
            // If no containers found, throw task exception.
            if (containerList.isEmpty()) {
                throw new TaskCommandException(TaskErrorCode
                        .PTS_CONTAINER_CLOSE_NOT_SPECIFIED_NO_OPEN_CONTAINER_AT_LOCATION);
            }
            // If multiple containers found, see if region allows multiple open containers
            // throw an exception if multiple open is not allowed.
            if (containerList.size() > 1) {
                if (this.getPtsRegion().isAllowMultipleOpenContainers()) {
                    throw new TaskCommandException(TaskErrorCode.PTS_CONTAINER_CLOSE_NOT_SPCEIFIED_MULTIPLE_OPEN_FOUND);
                } else {
                    throw new TaskCommandException(TaskErrorCode.PTS_CONTAINER_FATAL_ERROR_MULTIPLE_OPENED_NOT_ALLOWED);
                }
            }
         } else {
             // User specified a container number
             // look-up the container by number and location.
             containerList =
                 this.getPtsContainerManager()
                     .listOpenPtsContainersByPartialNumberAndLocation(this.getContainerNumber(),
                                                                      this.getCustomerLocation());
             // If no open containers found, throw task exception.
             if (containerList.isEmpty()) {
                 throw new TaskCommandException(TaskErrorCode
                         .PTS_CONTAINER_CLOSE_SPECIFIED_NOT_FOUND_OR_ALREADY_CLOSED, this.getContainerNumber());
             }
             if ((containerList.size() > 1) && (!this.getPtsRegion().isAllowMultipleOpenContainers())) {
                 throw new TaskCommandException(
                         TaskErrorCode.PTS_CONTAINER_FATAL_ERROR_MULTIPLE_OPENED_NOT_ALLOWED);
             }
         }
         // If we get here, we:
         // 1. found the container to close
         if (containerList.size() == 1) {
             this.getPtsContainerManager().closeContainer(containerList.get(0));
         }
    }




    /**
     * This Routine is used to override the default system behavior for automatically
     * creating container numbers.  The default behavior of this routine is to return
     * an empty string which is saved into the containerNumber member.  This causes the
     * createNewContainer() method to use a database generated container #.
     * @return String with new container number.
     */
    protected String generateContainerNumber() {
        return "";
    }


        /**
         * Creates a new (open) PTSContainer and
         * Calls setContainerNumberOfNewContainer().
         * @throws DataAccessException -
         * @throws BusinessRuleException -
         */
    protected void createNewContainer() throws DataAccessException, BusinessRuleException {

        // Create new PtsContainer
        //Note that default behavior is set to OPEN and Not Exported
        PtsContainer c = new PtsContainer();
        // Set container number temporarily
        c.setContainerNumber("-1");
        // Set the customerLocation property to the one previously looked-up.
        c.setCustomer(this.getCustomerLocation());
        c.setCreatedDate(new Date());
        c.setVersion(1);
        getPtsContainerManager().save(c);
        // Set container number correctly
        setContainerNumberOfNewContainer(c.getId());
        // add the newly created container to our private list.
        this.containerList.add(c);
    }


    /**
     * Sets the container number.
     * @param containerId - the container id of the container to set the number
     * @throws DataAccessException - Database exception
     * @throws BusinessRuleException - Business rule exception
     */
    protected void setContainerNumberOfNewContainer(Long containerId)
                                                    throws DataAccessException, BusinessRuleException {
        PtsContainer c = this.getPtsContainerManager().get(containerId);
        // If no container number was passed in use the container ID as the container number
        if (getContainerNumber().equals("")) {
            c.setContainerNumber(this.getPtsContainerManager().
                padContainerNumber(containerId.toString(),
                PtsContainerManager.CONTAINER_NUMBER_SIZE));
        } else {
            c.setContainerNumber(getContainerNumber());
        }
        // Update the container with the correct container number
        this.getPtsContainerManager().save(c);
    }


    /**
     * Build the list of open containers at this location to return
     * to doExecute().
     * @throws DataAccessException - Database Exceptions
     *
     */
    protected void buildResponse() throws DataAccessException {

        // Loop through current list of open containers at this location
        if (this.containerList.isEmpty()) {
            //Pass in null to get a "No Containers" response.
            getResponse().addRecord(buildResponseRecord(null));
        } else {
            for (PtsContainer c : this.containerList) {
                // For each container, add a response record to the response.
                getResponse().addRecord(buildResponseRecord(c));
            }
        }
    }


    /**
     * Build response record for container passed in.
     * Note that if null container passed in, a
     * "No containers" record will be created.  This
     * record contains empty string for container # info.
     *
     * @param c - PtsContainer to build record for or null for No Containers.
     * @return - response record for that container.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(PtsContainer c) throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();

        record.put("customerLocationId", this.getCustomerLocationId());

        //If given a proper container, get container numbers.
        if (c != null) {
            record.put("containerNumber", c.getContainerNumber());
            record.put("spokenContainerNumber",
                this.getPtsContainerManager().getContainerDigitsToSpeak(
                    c.getContainerNumber(),
                    this.getPtsRegion().getValidateContainerLength()));
            // if we are attempting to close a container and multiple opened were found
            // set an error message so the task will handle this properly
            if ((this.getRequestType() == CONTAINER_OPERATION_CLOSE)
                && (this.containerList.size() > 1) && (!this.getContainerNumber().equals(""))) {
                // Create a TaskMessageInfo object and pass
                // it to the setErrorFields method.
                record.setErrorFields(createMessageInfo(
                        TaskErrorCode.PTS_CONTAINER_CLOSE_SPCEIFIED_MULTIPLE_OPEN_FOUND));
               }

        } else {
            //No containers - return empty container numbers
            record.put("containerNumber", "");
            record.put("spokenContainerNumber", "");
        }
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 