/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationDescription;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * Get the put list for the group of licenses.
 * @author pfunyak
 *
 */
public class PtsGetPutsCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = -6590186648160261390L;

    private List<PtsPut>        putList  = new ArrayList<PtsPut>();


    /**
     * @return the putList
     */
    public List<PtsPut> getPutList() {
        return putList;
    }

    /**
     * @param putList the putList to set
     */
    public void setPutList(List<PtsPut> putList) {
        this.putList = putList;
    }


    // Comparators to sort puts

    /**
     * Compares two strings.
     *
     * @param str1 - first string <br>
     * @param str2 - second string <br>
     * @return
     *  <b>negative integer value</b> - when str1 < str2 <br>
     *  <b>zero</b> - when str1 = str2 <br>
     *  <b>positive integer value</b> - when str1 > str2 <br>

     */
    private static final int compareStringObjects(String str1, String str2) {
        int result = 0;
        if ((str1 == null) && (str2 == null)) {
            result = 0;
        } else if (str1 == null) {
            result =  str2.compareToIgnoreCase("");
        } else if (str2 == null) {
            result = str1.compareToIgnoreCase("");
        } else {
            result = str1.compareToIgnoreCase(str2);
        }
        return result;
    }


    static final Comparator<PtsPut> SORT_PUTS =
        new Comparator<PtsPut>() {
        public int compare(PtsPut p1, PtsPut p2) {

            LocationDescription loc1 = p1.getCustomer().getLocation().getDescription();
            LocationDescription loc2 = p2.getCustomer().getLocation().getDescription();

            int result = 0;

            // Check the group count to see if we have a single assignment.
            // If we have a single assignment, use the sequence number as
            // one of the sort criteria.
            if (p1.getLicense().getGroupInfo().getGroupCount() == 1) {
                result = p1.getSequenceNumber().compareTo(p2.getSequenceNumber());
                if (result != 0) {
                    return result;
                }
            }
            result = compareStringObjects(loc1.getPreAisle(), loc2.getPreAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getAisle(), loc2.getAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getPostAisle(), loc2.getPostAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getSlot(), loc2.getSlot());
            if (result != 0) {
                return result;
            }
            return p1.getLicense().getGroupInfo().getGroupPosition()
                .compareTo(p2.getLicense().getGroupInfo().getGroupPosition());
        }
     };


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    protected Response doExecute() throws Exception {

        this.getPuts();
        // sort puts based on single or grouped assignment
        sortPuts();
        // build the response record and return.
        buildResponse();
        return getResponse();
    }

    /**
     * Return the "filtered" list of puts.<br><br>
     * This procedure gets the full put list for a given group and filters <br>
     * out the puts that should not be returned.
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - No puts found for group
     */
    protected void getPuts() throws DataAccessException, TaskCommandException {

        // put list will contain the list of puts to return
        // We should only return puts with a status of NotPut, Partial or Skipped
        this.putList = new ArrayList<PtsPut>();

        for (PtsLicense l : getLicenses()) {
            for (PtsPut p : l.getPuts())   {
                if (p.getStatus().isInSet(NotPut, Partial, Skipped)) {
                    putList.add(p);
                }
            }
        }
        // if there is nothing in the list after this call then throw an exception
        if (this.putList.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.PTS_PUTS_NO_PUTS_IN_GROUP);
        }
    }


    /**
     * Sort the list of filtered puts.
     */
    protected void sortPuts() {
        Collections.sort(this.putList, SORT_PUTS);
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - on task error
     */
    protected void buildResponse() throws DataAccessException, TaskCommandException {

        for (PtsPut p : this.putList) {
            getResponse().addRecord(buildResponseRecord(p));
        }
    }

    /**
     * Build Response Record.
     *
     * @param p - put record to use to build response record
     * @return - response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(PtsPut p) throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();
        Location loc = p.getCustomer().getLocation();
        Item item = p.getItem();
        // translate put status
        // set partials back to not put.
        switch(p.getStatus()) {
            case Partial      : record.put("status", "N"); break;
            case NotPut       : record.put("status", "N"); break;
            case Skipped      : record.put("status", "S"); break;
            default : record.put("status", "N");    // this should never happen.
        }
        record.put("putID", p.getId());
        record.put("locationID", p.getCustomer().getId());
        record.put("licenseNumber", p.getLicense().getNumber());
        record.put("preAisleDirection", loc.getPreAisle());
        record.put("aisle", loc.getAisle());
        record.put("postAisleDirection", loc.getPostAisle());
        record.put("slot", loc.getSlot());
        record.put("checkDigits", loc.getCheckDigits());
        record.put("scannedLocVerification", loc.getScannedVerification());
        record.put("spokenLocVerification", loc.getSpokenVerification());
        record.put("quantityToPut", p.getQuantityToPut());
        record.put("quantityPut", p.getQuantityPut());
        record.put("itemNumber", item.getNumber());
        record.put("phoneticDescription", getTranslatedItemPhoneticDescription(item));
        record.put("uom", translateUserData(p.getUnitOfMeasure()));
        record.put("allowOverPack", p.isAllowOverPack());
        record.put("residualQuantity", p.getResidualQuantity());
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 