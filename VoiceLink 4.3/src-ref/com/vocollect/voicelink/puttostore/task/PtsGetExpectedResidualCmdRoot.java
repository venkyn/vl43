/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * @author estoll
 */
public class PtsGetExpectedResidualCmdRoot extends BasePutToStoreTaskCommand {

    public static final String KEY_UNION_CHAR = "_";

    private static final long serialVersionUID = -8673925814549976323L;

    /**
     * Definition of expected residual object.
     */
    protected class ResidObject {
        private String licenseNumber;
        private String itemNumber;
        private String itemDesciption;
        private Integer expectedResidual;

        /**
         * Getter for the licenseNumber property.
         * @return String value of the property
         */
        protected String getLicenseNumber() {
            return this.licenseNumber;
        }

        /**
         * Setter for the licenseNumber property.
         * @param licenseNumber the new licenseNumber value
         */
        protected void setLicenseNumber(String licenseNumber) {
            this.licenseNumber = licenseNumber;
        }

        /**
         * Getter for the itemNumber property.
         * @return String value of the property
         */
        protected String getItemNumber() {
            return this.itemNumber;
        }

        /**
         * Setter for the itemNumber property.
         * @param itemNumber the new itemNumber value
         */
        protected void setItemNumber(String itemNumber) {
            this.itemNumber = itemNumber;
        }
        /**
         * @return the itemDesciption
         */
        protected String getItemDesciption() {
            return itemDesciption;
        }

        /**
         * @param itemDesciption the itemDesciption to set
         */
        protected void setItemDesciption(String itemDesciption) {
            this.itemDesciption = itemDesciption;
        }

        /**
         * Getter for the expectedResidual property.
         * @return Integer value of the property
         */
        protected Integer getExpectedResidual() {
            return this.expectedResidual;
        }

        /**
         * Setter for the expectedResidual property.
         * @param expectedResidual the new expectedResidual value
         */
        protected void setExpectedResidual(Integer expectedResidual) {
            this.expectedResidual = expectedResidual;
        }


    }

    private Map<String, ResidObject> expResids = new HashMap<String, ResidObject>();


    /**
     * Getter for the expResids property.
     * @return Map String,ResidObject value of the property
     */
    protected Map<String, ResidObject> getExpResids() {
        return this.expResids;
    }

    /**
     * Setter for the expResids property.
     * @param expResids the new expResids value
     */
    protected void setExpResids(Map<String, ResidObject> expResids) {
        this.expResids = expResids;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    public Response doExecute() throws Exception {

        buildList();
        buildResponse();

        return getResponse();
    }


    /**
     * Build a map of residObjects keyed on the License Number + the Item Number.
     * @throws DataAccessException on database exceptions
     * @throws TaskCommandException if there are no licenses for the group number
     */
    protected void buildList() throws DataAccessException, TaskCommandException {
        String mapKey;

        // Need to build a map of LicenseNumber+ItemNumber as the key
        // and then put the expected residual value in the map
        //
        // All PUTS for a given license/item should have the same
        // expected residual value

        if (getLicenses().size() > 0) {
            for (PtsLicense l : getLicenses()) {
                for (PtsPut put : l.getPuts()) {
                    mapKey = l.getNumber() + KEY_UNION_CHAR
                                + put.getItem().getNumber().toString();
                    ResidObject residObject = new ResidObject();
                    residObject.setLicenseNumber(l.getNumber());
                    residObject.setItemNumber(put.getItem().getNumber().toString());
                    residObject.setItemDesciption(getTranslatedItemPhoneticDescription(put.getItem()));
                    residObject.setExpectedResidual(put.getResidualQuantity());
                    getExpResids().put(mapKey, residObject);
                }
            }
        } else {
            throw new TaskCommandException(
                TaskErrorCode.PTS_NO_LICENSES_FOR_GROUP_NUMBER, getGroupNumber());

        }

    }


    /**
     * Populate the response.
     */
    protected void buildResponse() {
        // Convert the map into a list for simpler processing
        List<ResidObject> resids = new ArrayList<ResidObject>(
                                            getExpResids().values());

        for (ResidObject residObject : resids) {
            getResponse().addRecord(buildResponseRecord(residObject));
        }
    }

    /**
     * @param residObject - the object that contains the license number,
     *                      item number and expected residual quantity value.
     * @return TaskResponseRecord
     */
    protected ResponseRecord buildResponseRecord(ResidObject residObject) {

        ResponseRecord record = new TaskResponseRecord();

        record.put("licenseNumber", residObject.getLicenseNumber());
        record.put("itemNumber", residObject.getItemNumber());
        record.put("itemDescription", residObject.getItemDesciption());
        record.put("expectedResidual", residObject.getExpectedResidual());
        record.put("processed", "N");
        return record;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 