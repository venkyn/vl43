/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsPutStatus;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import java.util.List;


/**
 *  This class is used to have functionality for update command.
 *
 *
 * @author svoruganti
 */
public class PtsUpdateStatusCmdRoot extends BasePutToStoreTaskCommand {

 // Generated serial version id
    private static final long serialVersionUID = 1727647904475109495L;

    private String          slotAisle;

    private String          setStatusTo;

    private String          customerLocationID;

    private PtsCustomerLocationManager customerLocationManager;

    private Location        putLocation;

    /**
     * Getter for the pick location property.
     * @return pickLocation; value of the property
     */
    public Location getPutLocation() {
        return putLocation;
    }

    /**
     * Setter for the pick location property.
     * @param putLocation the new putLocation value
     */
    public void setPutLocation(Location putLocation) {
        this.putLocation = putLocation;
    }

    /**
     * Getter for the location ID property.
     * @return long value of the property
     */
    public String getCustomerLocationID() {
        return customerLocationID;
    }

    /**
     * Setter for the location ID property.
     * @param customerLocationID the new locationID value
     */
    public void setCustomerLocationID(String customerLocationID) {
        this.customerLocationID = customerLocationID;
    }

    /**
     * Getter for the slot aisle indicator property.
     * @return String value of the property
     */
    public String getSlotAisle() {
        return slotAisle;
    }

    /**
     * Setter for the slot aisle indicator property.
     * @param slotAisle the new slotAisle value
     */
    public void setSlotAisle(String slotAisle) {
        this.slotAisle = slotAisle;
    }

    /**
     * Getter for the set status to property.
     * @return String value of the property
     */
    public String getSetStatusTo() {
        return setStatusTo;
    }

    /**
     * Setter for the set status to property.
     * @param setStatusTo the new setStatusTo value
     */
    public void setSetStatusTo(String setStatusTo) {
        this.setStatusTo = setStatusTo;
    }

    /**
     * Getter for the locationManager property.
     * @return locationManager value of the property
     */
    public PtsCustomerLocationManager getCustomerLocationManager() {
        return customerLocationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param ptsCustomerLocationManager the new customerLocationManager value
     */
    public void setPtsCustomerLocationManager(PtsCustomerLocationManager ptsCustomerLocationManager) {
        this.customerLocationManager = ptsCustomerLocationManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //validates the status change
        validate();

        //Get Location object for the location ID
        getRecievedLocationObject();

        //Updates the statuses of the picks
        update();

        return getResponse();
     }


    /**
     * Validates that status can be updated.
     *
     * @throws Exception in the general case
     */
    protected void validate() throws Exception {
        if (!getSetStatusTo().equalsIgnoreCase("S") && !getSetStatusTo().equalsIgnoreCase("N")) {
            throw new TaskCommandException(TaskErrorCode.INVALID_STATUS_FOR_PUT);
        }
        if (Integer.parseInt(getSlotAisle()) != 0
            && Integer.parseInt(getSlotAisle()) != 1
            && Integer.parseInt(getSlotAisle()) != 2) {
            throw new TaskCommandException(TaskErrorCode.INVALID_INDICATOR_PUT);
        }
        if (Integer.parseInt(getSlotAisle()) == 2
            && getSetStatusTo().equalsIgnoreCase("S")) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_SKIP_ENTIRE_ASSIGNMENT_PTS);
        }
    }

    /**
     * Updates the status.
     * @throws BusinessRuleException - Throws business rule exception
     * @throws DataAccessException - Database Exception
     */
    protected void update() throws BusinessRuleException, DataAccessException {
        //Get List of assignment with the operator ID in the reserved fields
        List<PtsPut> puts = getPtsPutManager().listPutsForGroup(getGroupNumber());

        //for each put in the group update the status accordingly
        for (PtsPut p : puts) {
            if (Integer.parseInt(getSlotAisle()) == 0 && getSetStatusTo().equals("S")) {
                if (p.getStatus() == PtsPutStatus.NotPut
                    && p.getCustomer().getLocation().equals(getPutLocation())) {
                       p.reportPutSkipped();
                }
            }


            if (Integer.parseInt(getSlotAisle()) == 1 && getSetStatusTo().equals("S")) {
                 if (p.getStatus() == PtsPutStatus.NotPut
                    && stringEquals(p.getCustomer().getLocation().getDescription().getPreAisle(),
                        putLocation.getDescription().getPreAisle())
                    && stringEquals(p.getCustomer().getLocation().getDescription().getAisle(),
                        putLocation.getDescription().getAisle())) {
                     p.reportPutSkipped();
                  }
            }

            if (Integer.parseInt(getSlotAisle()) == 2 && getSetStatusTo().equals("N")) {
                  if (p.getStatus().isInSet(PtsPutStatus.Skipped)) {
                    p.setPutAsNotPut();
                 }
            }
        }
    }

    /**
     * Compare 2 strings and if both null then they are considered equal.
     * @param value1 - first value to compare
     * @param value2 - second value to compare
     * @return equal or not
     */
    protected boolean stringEquals(String value1, String value2) {
        if (value1 != null) {
            return value1.equals(value2);
        } else {
            return (value2 == null);
        }
    }

    /**
     * gets a Location object for the location ID.
     *
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void getRecievedLocationObject()
        throws DataAccessException, BusinessRuleException {
        //Get the location
        if (!StringUtil.isNullOrEmpty(getCustomerLocationID())) {
            setPutLocation(getCustomerLocationManager().
                   get(Long.valueOf(getCustomerLocationID())).getLocation());
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 