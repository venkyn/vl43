/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

/**
 *
 * Verify that the customer location exists by scanned value
 * or by spoken value and check digits.
 *
 * @author pfunyak
 */
public class PTSVerifyCustomerLocationCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -3676653102659770003L;

    private boolean                     scanned;

    private String                      locationToVerify;

    private String                      checkDigits;

    private PtsCustomerLocation         returnCustomerLocation;

    private PtsCustomerLocationManager  ptsCustomerLocationManager;


    /**
     * @return the scanned
     */
    public boolean isScanned() {
        return scanned;
    }

    /**
     * @param scanned the scanned to set
     */
    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    /**
     * Getter for the checkDigits property.
     * @return String value of the property
     */
    public String getCheckDigits() {
        return this.checkDigits;
    }

    /**
     * Getter for the enum type checkDigits property.
     * @return Long value of the property
     */
    public Long getCheckDigitsLong() {
        return Long.parseLong(this.checkDigits);
    }

    /**
     * Setter for the checkDigits property.
     * @param checkDigits the new checkDigits value
     */
    public void setCheckDigits(String checkDigits) {
        this.checkDigits = checkDigits;
    }


    /**
     * Getter for the locationToVerify property.
     * @return String value of the property
     */
    public String getLocationToVerify() {
        return this.locationToVerify;
    }

    /**
     * Setter for the locationToVerify property.
     * @param locationToVerify the new locationToVerify value
     */
    public void setLocationToVerify(String locationToVerify) {
        this.locationToVerify = locationToVerify;
    }

    /**
     * @return the returnCustomerLocation
     */
    public PtsCustomerLocation getReturnCustomerLocation() {
        return returnCustomerLocation;
    }

    /**
     * @param returnCustomerLocation the returnCustomerLocation to set
     */
    public void setReturnCustomerLocation(PtsCustomerLocation returnCustomerLocation) {
        this.returnCustomerLocation = returnCustomerLocation;
    }

    /**
     * @return the ptsCustomerLocationManager
     */
    public PtsCustomerLocationManager getPtsCustomerLocationManager() {
        return ptsCustomerLocationManager;
    }

    /**
     * @param ptsCustomerLocationManager the ptsCustomerLocationManager to set
     */
    public void setPtsCustomerLocationManager(
            PtsCustomerLocationManager ptsCustomerLocationManager) {
        this.ptsCustomerLocationManager = ptsCustomerLocationManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        getCustomerLocation();
        buildResponse();
        return getResponse();
    }

    /**
     * Gets the location to return.
     *
     * @throws TaskCommandException - task exception
     * @throws DataAccessException - database exception
     */
    protected void getCustomerLocation() throws TaskCommandException, DataAccessException {
        if (isScanned()) {
            setReturnCustomerLocation(getPtsCustomerLocationManager()
                                      .findCustomerLocationByScanned(getLocationToVerify()));
        } else {
            setReturnCustomerLocation(getPtsCustomerLocationManager()
                                     .findCustomerLocationBySpoken(getLocationToVerify(), getCheckDigits()));
        }
        //Verify the location
        verifyLocation();
    }


    /**
     * Verifies the location exists.
     *
     * @throws TaskCommandException - task exception
     */
    protected void verifyLocation() throws TaskCommandException {
        if (getReturnCustomerLocation() == null) {
            throw new TaskCommandException(TaskErrorCode.LOCATION_NOT_FOUND, getLocationToVerify());
        }
    }

    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     *
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        record.put("customerLocationId", getReturnCustomerLocation().getId());
        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 