/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.LabelData;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 *  This class handles exception label printing for unexpected residual items.
 *
 *  Exception label printing is not currently supported by VoiceLink
 *  but could be added as a customization.
 *
 *  The current implementation of the class will always return a successful response.
 *
 * @author pfunyak
 */
public class PtsPrintExceptionLabelCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = -6358494134731423203L;

    private String                      licenseNumber;

    private String                      itemNumber;

    private Integer                     printerNumber;

    private PrinterManager              printerManager;

    private Printer                     printer;

    private List <LabelData>            labels = new ArrayList<LabelData>();



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        // Check for valid printer number
        validatePrinter();
        printLabels();
        buildResponse();
        return getResponse();
    }

    /**
     * Validate the printer number is in the VoiceLink database
     * and it is enabled.
     * @throws DataAccessException - on database exceptions
     * @throws TaskCommandException - when printer number is not found.
     */
    protected void validatePrinter()
    throws DataAccessException, TaskCommandException {

        // Exception label printing is not currently supported by VoiceLink
        // but could be added as a customization.
        /*
        printer = printerManager.findByNumber(printerNumber);
        if (printer == null) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_NOT_FOUND, printerNumber);
        }

        if (!this.printer.getIsEnabled()) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_IS_DISABLED, printerNumber);
        }
        */
    }
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     *
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        return record;
    }

    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    protected void printLabels() throws DataAccessException, VocollectException {

        // TODO: add print logic to create the exception label
        // Exception label printing is not currently supported by VoiceLink
        // but could be added as a customization.

    }


    /**
     * Send the member variable labels to the AbstractPrintServer.
     * @throws TaskCommandException when the print server throws errors.
     */
    protected void sendLabelsToPrinter() throws TaskCommandException {

        // TODO: this code is here as an example at to how to send a label to the printer.
        // Exception label printing is not currently supported by VoiceLink
        // but could be added as a customization.

        /*
        PrintJob job = null;

        try {
            job = new PrintJob(this.printer.getName(), this.labels);
        } catch (VocollectException e) {
            throw new TaskCommandException(TaskErrorCode.PRINTSERVER_EXCEPTION, e.getErrorCode());
        }

        try {
            AbstractPrintServer printServer =  PrintServerFactory.getPrintServer();
            if (this.labels.size() > 0) {
                printServer.submitJob(job);
            }
        } catch (VocollectException e) {
            throw new TaskCommandException(TaskErrorCode.PRINTSERVER_EXCEPTION, e.getErrorCode());
        }
        return;
        */
    }


    /**
     * @return the licenseNumber
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * @param licenseNumber the licenseNumber to set
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * @param itemNumber the itemNumber to set
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * Getter for the printer property.
     * @return Printer value of the property
     */
    public Printer getPrinter() {
        return this.printer;
    }

    /**
     * Setter for the printer property.
     * @param printer the new printer value
     */
    public void setPrinter(Printer printer) {
        this.printer = printer;
    }


    /**
     * Getter for the labels property.
     * @return List&lt;LabelData&gt; value of the property
     */
    public List<LabelData> getLabels() {
        return this.labels;
    }

    /**
     * Setter for the labels property.
     * @param labels the new labels value
     */
    public void setLabels(List<LabelData> labels) {
        this.labels = labels;
    }


    /**
     * Getter for the printerManager property.
     * @return PrinterManager value of the property
     */
    public PrinterManager getPrinterManager() {
        return this.printerManager;
    }

    /**
     * Setter for the printerManager property.
     * @param printerManager the new printerManager value
     */
    public void setPrinterManager(PrinterManager printerManager) {
        this.printerManager = printerManager;
    }

    /**
     * Getter for the printerNumber property.
     * @return Long value of the property
     */
    public Integer getPrinterNumber() {
        return this.printerNumber;
    }

    /**
     * Setter for the printerNumber property.
     * @param printerNumber the new printerNumber value
     */
    public void setPrinterNumber(Integer printerNumber) {
        this.printerNumber = printerNumber;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 