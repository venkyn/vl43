/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.task.command.BasePutToStoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author estoll
 */
public class PtsVerifyLicenseCmdRoot extends BasePutToStoreTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private String licenseNumber;

    private boolean isPartialLicenseNumber;

    private PtsLicense license;

    private PtsRegionManager ptsRegionManager;

    private PtsRegion ptsRegion;

    private ArrayList<PtsLicense> ptsLicenses = new ArrayList<PtsLicense>();

    private static final int CONSTANT_FIFTY = 50;



    /**
     * Getter for the isPartialLicenseNumber property.
     * @return boolean value of the property
     */
    public boolean getIsPartialLicenseNumber() {
        return this.isPartialLicenseNumber;
    }


    /**
     * Setter for the isPartialLicenseNumber property.
     * @param isPartialLicenseNumber the new isPartialLicenseNumber value
     */
    public void setIsPartialLicenseNumber(boolean isPartialLicenseNumber) {
        this.isPartialLicenseNumber = isPartialLicenseNumber;
    }

    /**
     * Getter for the ptsLicenses property.
     *
     * @return ArrayList PtsLicense value of the property
     */
    public ArrayList<PtsLicense> getPtsLicenses() {
        return this.ptsLicenses;
    }

    /**
     * Setter for the ptsLicenses property.
     *
     * @param ptsLicenses
     *            the new ptsLicenses value
     */
    public void setPtsLicenses(ArrayList<PtsLicense> ptsLicenses) {
        this.ptsLicenses = ptsLicenses;
    }

    /**
     * Getter for the ptsRegion property.
     *
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return this.ptsRegion;
    }

    /**
     * Setter for the ptsRegion property.
     *
     * @param theRegion
     *            the new put to store Region value
     */
    public void setRegion(PtsRegion theRegion) {
        this.ptsRegion = theRegion;
    }

    /**
     * Getter for the ptsRegionManager property.
     *
     * @return PtsRegionManager value of the property
     */
    public PtsRegionManager getPtsRegionManager() {
        return this.ptsRegionManager;
    }

    /**
     * Setter for the ptsRegionManager property.
     *
     * @param ptsRegionManager
     *            the new ptsRegionManager value
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     * Getter for the license property.
     *
     * @return PtsLicense value of the property
     */
    public PtsLicense getLicense() {
        return this.license;
    }

    /**
     * Setter for the license property.
     *
     * @param license
     *            the new license value
     */
    public void setLicense(PtsLicense license) {
        this.license = license;
    }

    /**
     * Getter for the licenseNumber property.
     *
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Setter for the licenseNumber property.
     *
     * @param licenseNumber
     *            the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    public Response doExecute() throws Exception {

        // Get PutToStore Region
        setRegion(getPtsRegionManager().get(
                getOperator().getCurrentRegion().getId()));

        if (getIsPartialLicenseNumber()) {
            partialLicense();
        } else {
            fullLicense();
        }

        buildResponse();
        return getResponse();
    }

    /**
     * Populate the response.
     *
     * @throws DataAccessException -
     *             Database exception
     */
    protected void buildResponse() throws DataAccessException {
        // Create a return record, no fields to return
        for (PtsLicense p : getPtsLicenses()) {
            getResponse().addRecord(buildResponseRecord(p));
        }

    }

    /**
     * Builds the response record.
     *
     * @param p -
     *            the license to use for the response
     * @throws DataAccessException -
     *             database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(PtsLicense p)
            throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("licenseNumber", p.getNumber());
        // Do we have more than one -- if so - set an
        // error so the task knows what to do.
        if (getPtsLicenses().size() > 1) {
            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.licenseNumber", getLicenseNumber());

            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                    TaskErrorCode.PTS_LICENSES_MULTIPLE_MATCHING, lop));
        }
        return record;
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * Does the operations for a partial License Plate A partial may have many
     * matches. Limit the result set to 50 just to be safe.
     *
     * @throws DataAccessException -
     *             database exception
     * @throws TaskCommandException -
     *             task exception
     * @throws BusinessRuleException -
     *             business rule violation
     */
    protected void partialLicense() throws DataAccessException,
            TaskCommandException, BusinessRuleException {
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(CONSTANT_FIFTY);

        // Get a list of assignments with the partial work id
        List<PtsLicense> matchingLicenses = getPtsLicenseManager()
                .listLicenseByPartialNumber(queryDecorator, getLicenseNumber(),
                        getRegion());

        // If more than 1 partial license found then set multiple error
        if (matchingLicenses.size() > 1) {
            for (PtsLicense p : matchingLicenses) {
                getPtsLicenses().add(p);
            }
        } else {
            // If there is only one license -- then we
            // need to know which licenses the operator
            // has already reserved so we can check to see
            // if this license can be added to that list.
            List<PtsLicense> reservedLicenses = getPtsLicenseManager()
                    .listReservedLicenses(getOperator().getOperatorIdentifier());

            // Should only be one - but since it's a list
            // we'll use a for loop
            for (PtsLicense p : matchingLicenses) {
                // add license returns a true false, but we
                // are going to only check the resulting list
                // size to see if there was an error
                addLicense(reservedLicenses, p);
            }

            // No license found or could not be group with current assignments
            if (matchingLicenses.isEmpty() || getPtsLicenses().isEmpty()) {
                throw new TaskCommandException(
                        TaskErrorCode.PTS_LICENSE_REQUEST_FAILED, getLicenseNumber());
            }
        }

    }

    /**
     * @param reserved a list of PtsLicenses already reserved by the operator
     * @param theLicense the current license to be reserved
     * of the reserved list.
     * @throws TaskCommandException if an item is found
     */
    protected void duplicateItemsExist(List<PtsLicense> reserved,
                                          PtsLicense theLicense)
    throws TaskCommandException {
        for (PtsLicense ptsLicense : reserved) {
            for (PtsPut reservedPut : ptsLicense.getPuts()) {
                for (PtsPut put : theLicense.getPuts()) {
                    if (put.getItem().equals(reservedPut.getItem())) {
                        LOPArrayList lop = new LOPArrayList();
                        lop.add("task.licensesNumber", ptsLicense.getNumber());
                        lop.add("task.licensesNumber", getLicenseNumber());
                        lop.add("task.itemNumber", put.getItem().getNumber());

                        throw new TaskCommandException(
                                TaskErrorCode.PTS_VERIFY_LICENSE_DUPLICATE_ITEM,
                                    ptsLicense.getNumber(),
                                    getLicenseNumber(),
                                    put.getItem().getNumber());
                    }
                }
            }
        }
    }

    /**
     * Determine if a license can be "grouped" with the list of already reserved
     * licenses.
     *
     * @param reserved
     *            the list of reserved licenses
     * @param theLicense
     *            the license that is the basis of the group
     * @return success or not.
     * @throws DataAccessException -
     *             database failure
     * @throws TaskCommandException -
     *             Task command violation
     * @throws BusinessRuleException -
     *             business rule violation
     */
    protected boolean addLicense(List<PtsLicense> reserved,
            PtsLicense theLicense) throws DataAccessException,
            TaskCommandException, BusinessRuleException {
        boolean returnValue = false;

        // first verify we are not added a license that contains
        // an item that is already included in a license of the resered list
        // if theLicense contains items already reserved in other
        // licenses, a task exception will be thrown
        duplicateItemsExist(reserved, theLicense);

        // From here on -- assume success -- add the license to the list.
        reserved.add(theLicense);

        // if the list is only one - we are done.
        if (reserved.size() == 1) {
            getPtsLicenses().add(theLicense);
            returnValue = true;
        } else {
            // If there is more than one license in the list, we need to
            // determine if grouping rules pass
            if (reserved.size() > getRegion().getMaxLicensesInGroup()) {
                // This is a 98 error code
                // forcing sign off will clear the reserve field
                throw new TaskCommandException(
                        TaskErrorCode.PTS_TOO_MANY_LICNESES_IN_GROUP,
                            getRegion().getMaxLicensesInGroup());
            }

            getPtsLicenses().add(theLicense);

            returnValue = true;
        }

        // Reserve assignment
        if (returnValue) {
            reserveLicense(theLicense, reserved.size());
        }

        return returnValue;
    }

    /**
     * reserves licenses.
     *
     * @param p -
     *            license to reserve
     * @param count -
     *            number of licenses reserved
     * @throws DataAccessException -
     *             database exception
     * @throws BusinessRuleException -
     *             business exception
     */
    protected void reserveLicense(PtsLicense p, int count)
            throws DataAccessException, BusinessRuleException {
        p.setReservedBy(getOperator().getOperatorIdentifier());
        p.getGroupInfo().setRequestedOrderNo(count);
        getPtsLicenseManager().save(p);
    }

    /**
     * Find requested license.
     *
     * @throws DataAccessException -
     *             Database exception
     * @throws TaskCommandException -
     *             task command exception
     * @throws BusinessRuleException -
     *             grouping rule violation
     */
    protected void fullLicense() throws DataAccessException,
            TaskCommandException, BusinessRuleException {
        List<PtsLicense> licenses;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(CONSTANT_FIFTY);

        licenses = getPtsLicenseManager().listLicenseByNumber(queryDecorator,
                getLicenseNumber(), getRegion());

        if (licenses.size() == 0) {
            // Exception -- license not found
            throw new TaskCommandException(TaskErrorCode.LICENSE_NOT_FOUND,
                    getLicenseNumber());
        }

        // get the list of already reserved licenses
        List<PtsLicense> reservedLicenses = getPtsLicenseManager()
                .listReservedLicenses(getOperator().getOperatorIdentifier());

        // Loop through licenses until 1 is found that can be grouped
        // with others
        for (PtsLicense p : licenses) {
            if (addLicense(reservedLicenses, p)) {
                break;
            }
        }

        // No licenses found or could not be group with current licenses
        if (licenses.isEmpty() || getPtsLicenses().isEmpty()) {
            throw new TaskCommandException(
                    TaskErrorCode.PTS_LICENSE_REQUEST_FAILED, getLicenseNumber());
        }

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 