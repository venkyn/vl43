/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for Put-To-Store business.
 * Although the range for these error codes is 10000-10999, please use
 * only 10000-10499 for the predefined ones; 10500-10999 should be
 * reserved for customization error codes.
 */
public class PutToStoreErrorCodeRoot extends ErrorCode {


    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 10000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 10999;


    /**
     * Constructor.
     */
    private PutToStoreErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected PutToStoreErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(PutToStoreErrorCodeRoot.NO_ERROR, err);
    }

    /**
     * No error, just the base initialization.
     */
    public static final PutToStoreErrorCodeRoot NO_ERROR
        = new PutToStoreErrorCodeRoot();

    /**
     * cannot add to license to group - not same region.
     */
    public static final PutToStoreErrorCode GROUP_NOT_SAME_REGION
    = new PutToStoreErrorCode(10001);

    /**
     * cannot add to license to group - region limit exceeded.
     */
    public static final PutToStoreErrorCode GROUP_TOO_MANY_ASSIGNMENTS
    = new PutToStoreErrorCode(10002);

    /**
     * License cannot change to the target status code.
     */
    public static final PutToStoreErrorCode LICENSE_INVALID_STATUS_CHANGE
    = new PutToStoreErrorCode(10003);

    /**
     * Puts not complete.
     */
    public static final PutToStoreErrorCode PUTS_NOT_COMPLETE
    = new PutToStoreErrorCode(10004);


    /**
     * Operators signed into region.
     */
    public static final PutToStoreErrorCodeRoot REGION_OPERATORS_SIGNED_IN
    = new PutToStoreErrorCodeRoot(10005);


    /**
     * cannot have assignments with status in-progress, suspended,
     * or passed.
     */
    public static final PutToStoreErrorCodeRoot REGION_INVALID_LICENSE_STATUS
    = new PutToStoreErrorCodeRoot(10006);


    /**
     * Cannot edit the region spoken license length to be more than the licenses in that region.
     */
    public static final PutToStoreErrorCodeRoot REGION_INVALID_SPOKEN_LICENSE_LENGTH
    = new PutToStoreErrorCodeRoot(10007);


    /**
     * an invalid status change was attempted.
     */
    public static final PutToStoreErrorCodeRoot PUT_INVALID_STATUS_CHANGE
    = new PutToStoreErrorCodeRoot(10008);

    /**
     * Assignment cannot be completed from current status.
     */
    public static final PutToStoreErrorCodeRoot LICENSE_INVALID_STOP_STATUS
    = new PutToStoreErrorCodeRoot(10009);

    /**
     * cannot un-group unless all assignment in group selected.
     */
    public static final PutToStoreErrorCodeRoot UNGROUP_TASK_NOT_ALL_ASSIGNMENTS
    = new PutToStoreErrorCodeRoot(10010);

    /**
     * can not update put status. put is already complete.
     */
    public static final PutToStoreErrorCodeRoot PUT_ALREADY_COMPLETE
    = new PutToStoreErrorCodeRoot(10011);

    /**
     * an invalid status change was attempted.
     * Put for an assignment not completed.
     */
    public static final PutToStoreErrorCodeRoot ASSIGNMENT_PUT_NOT_COMPLETED =
        new PutToStoreErrorCodeRoot(10012);

}


*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 