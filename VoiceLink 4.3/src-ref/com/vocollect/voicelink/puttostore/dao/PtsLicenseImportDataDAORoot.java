/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.puttostore.model.PtsLicenseImportData;

import java.util.List;

/**
 * @author svoruganti
 *
 */
public interface PtsLicenseImportDataDAORoot extends GenericDAO<PtsLicenseImportData> {

    /**
     * Get the list of importable licenses by site name.
     * @param qd the decorator.
     * @param siteName name of the site to use
     * @return list of all importable licenses for the site.
     * @throws DataAccessException for database errors.
     */
    public List<PtsLicenseImportData> listPtsLicenseBySiteName(QueryDecorator qd,
                                                               String siteName)
        throws DataAccessException;


    /**
     * Update all PtsLicenseImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 