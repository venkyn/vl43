/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;

import java.util.List;

/**
 * PtsCustomerLocation Data Access Object (DAO) interface.
 *
 * @author mnichols
 */
public interface PtsCustomerLocationDAORoot extends GenericDAO<PtsCustomerLocation> {

    /**
     * List all customerlocations. This override is for a left join on the operatorid.
     * @param decorator - additional query instructions.
     * @return - list of chase assignments in decending number order
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listCustomerLocations(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Find customer location by customerNumber and locationID.
     *
     * @param customerNumber - customer number to find.
     * @param locationId - location customer is associated with
     * @return - customer number if found.
     */
    PtsCustomerLocation findCustomerLocationByNumberLocation(String customerNumber,
                                    String locationId);


    /**
     * Find customer location by the core location scanned verification value.
     *
     * @param scannedVerification - the scanned value to search by.
     * @return - customer location object.
     */
    PtsCustomerLocation findCustomerLocationByScanned(String scannedVerification);


    /**
     * Find customer location by the core location spoken verification
     * and check digits value.
     *
     * @param spokenVerification - the spoken value to search by.
     * @param checkDigits - the check digits to search by.
     * @return - customer number if found.
     */
    PtsCustomerLocation findCustomerLocationBySpoken(String spokenVerification,
                                                     String checkDigits);
    /**
     * Find if customer location exists by customerNumber and locationID.
     *
     * @param customerNumber - customer number to find.
     * @param locationId - location customer is associated with
     * @return - customer number if found.
     */
    PtsCustomerLocation findImportBlockingCondition(String customerNumber,
                                    String locationId);


 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 