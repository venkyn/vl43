/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;

import java.util.Date;
import java.util.List;

/**
 * Container Data Access Object (DAO) interface.
 *
 * @author jtauberg
 */
public interface PtsContainerDAORoot extends GenericDAO<PtsContainer> {

    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer> listAllContainers()
        throws DataAccessException;

    /**
     * Get all containers.
     * @param decorator - additional query instructions.
     * @return the List of pick details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listPtsContainers(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * retrieves a PtsContainer by the container number.
     *
     * @param containerNumber - value to retrieve by
     * @return the PtsContainer object found.
     * @throws DataAccessException - database exceptions
     */
    PtsContainer findPtsContainerByNumber(String containerNumber)
        throws DataAccessException;

    /**
     * @param customerLocation - the customerLocation ID of the
     *   CustomerLocation associated with the Containers.
     * @return the List of open containers at this location.
     */
    List<PtsContainer> listOpenPtsContainersByCustomerLocation(PtsCustomerLocation customerLocation);


    /**
     * Retrieve the list of open containers by partial container number and location.
     *
     * @param partialContainerNumber - the number of the container to find.
     * @param customerLocation - the customer location associated with the containers.
     *
     * @return the List of open containers.
     */
    List<PtsContainer> listOpenPtsContainersByPartialNumberAndLocation(String partialContainerNumber,
                                                                       PtsCustomerLocation customerLocation);

    /**
     * Retrieve the container by container number and customerLocation.
     * NOTE: the container number is the full container number
     *
     * @param containerNumber - the number of the container to find.
     * @param customerLocation - the customer location associated with the containers.
     *
     * @return the List of open containers.
     */
     PtsContainer findPtsContainerByNumberAndLocation(String containerNumber,
                                                      PtsCustomerLocation customerLocation);


    /**
     * @param qd - additional query instructions.
     * @return the list of closed containers.
     * @throws DataAccessException - database failure
     */
    List<PtsContainer> listClosedContainers(QueryDecorator qd)
    throws DataAccessException;

    /**
     * Updates container that are ready to be exported.
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateClosedContainers()
    throws DataAccessException;

    /**
     * Updates PTS Containers that are ready to be purged or archived.
     *
     * @param purgeable - whether to mark for archive(1) or purge (2)
     * @param date - Date to find assignment older than
     * @param status - status of containers to find
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateOlderThan(Integer purgeable,
                            Date date,
                            PtsContainerStatus status) throws DataAccessException;

    /**
     * Get a specified number of PTS Licenses to archive.
     *
     * @param queryDecorator - query decorator for number of row to retrieve
     * @return - list of PTS Containers
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer> listArchive(QueryDecorator queryDecorator)
        throws DataAccessException;


    /**
     * Get a specified number of PTS Containers to purge.
     *
     * @param queryDecorator - query decorator for number of row to retrieve
     * @return - list of PTS Licenses
     * @throws DataAccessException - database exceptions
     */
    List<PtsContainer> listPurge(QueryDecorator queryDecorator)
        throws DataAccessException;
    
    /**
     * Gets a list of containers to populate in Pts Container modified report
     * 
     * @param queryDecorator - the query decorator
     * @param startDate - start date entered
     * @param endDate - end date entered
     * @return list of containers
     * @throws DataAccessException
     */
    List<PtsContainerReport> listPtsContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                    Date startDate,
                                                                    Date endDate)
        throws DataAccessException;

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 