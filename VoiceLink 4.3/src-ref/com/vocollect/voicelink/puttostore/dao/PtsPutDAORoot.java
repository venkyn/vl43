/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.PtsPut;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Put Data Access Object (DAO) interface.
 *
 * @author mnichols
 */
public interface PtsPutDAORoot extends GenericDAO<PtsPut> {

    /**
     * List all puts. This override is for a left join on the operatorid.
     * @param decorator - additional query instructions.
     * @return - list of puts in desending order
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listPuts(QueryDecorator decorator)
                                    throws DataAccessException;

    /**
     * List all puts for the given group number.
     *
     * @param groupNumber - group number on license object.
     * @return - list of puts for the group
     * @throws DataAccessException - on db exception
     */
     List<PtsPut> listPutsForGroup(Long groupNumber)
                                    throws DataAccessException;

     /**
      * List all puts for the given license number.
      *
      * @param licenseNumber - retrieve the puts for this license number.
      * @return - list of puts for the license
      * @throws DataAccessException - on db exception
      */
      List<PtsPut> listPutsForLicense(Long licenseNumber)
                                     throws DataAccessException;


      //Following queries are for exports.

      /**
       * @return list of maps.  Each map contains the fields for a completed pick,
       *         the object, and the matchingStatus of '1,2'.
       * @param qd - query decorator for number of results
       * @throws DataAccessException .
       */
      List<Map<String, Object>> listCompletedPuts(QueryDecorator qd)
          throws DataAccessException;

      /**
       * Get list of labor records to export.
       *
       * @return the list of labor records to export
       * @param qd - query decorator for number of results
       * @throws DataAccessException - Database failure
       */
      List<Map<String, Object>> listExportLaborRecords(QueryDecorator qd)
      throws DataAccessException;

      /**
       * Mark assignment records as exported.
       *
       * @param qd - query decorator for number of results
       * @return list of export finish
       * @throws DataAccessException on database error
       */
      List<Map<String, Object>> listExportFinish(QueryDecorator qd) throws DataAccessException;



      /**
       * @return list of maps.  Each map contains the fields for a puts file picked record,
       *         the object, and the matchingStatus.
       * @param qd - query decorator for number of results
       * @throws DataAccessException .
       */
      List<Map<String, Object>> listPutsPut(QueryDecorator qd)
          throws DataAccessException;


      /**
       * Updates put details that are ready to be exported.
       * @return - number of records updated
       * @throws DataAccessException - database exceptions
       */
      Integer updatePutsPut() throws DataAccessException;

      /**
       * Get counts of location/item that where shorted.
       *
       * @param startTime - start of date range to search.
       * @param endTime - end of date range to search.
       * @return - list of arrarys containing location, item, count, last date shorted
       * @throws DataAccessException - database exceptions
       */
      List<Object[]> listShortedLocations(Date startTime, Date endTime)
      throws DataAccessException;

 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 