/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;

import java.util.Date;
import java.util.List;


/**
 * Data Access Object for ArchivePtsLicenseLabor.
 * @author kudupi
 */
public interface ArchivePtsLicenseLaborDAORoot extends GenericDAO<ArchivePtsLicenseLabor> {


    /**
     * Finds all Archive PTS License Labor records.
     * @param decorator - query decorator
     * @return - list of Archive PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listArchivePtsLicenseLabor(QueryDecorator decorator)
                                                    throws DataAccessException;

    /**
     * Get list of licenses labor for PTS License Labor Report.
     * @param decorator - the decorator for the where clause.
     * @param startTime - Start Time for PTS License Labor Report.
     * @param endTime - endTime for PTS License Labor Report.
     * @param siteId - the site for the PTS License Labor Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseLaborReport> listArchiveLicensesLaborForPtsLicenseLaborReport(
        QueryDecorator decorator,
        Date startTime, 
        Date endTime,
        Long siteId)
        
    throws DataAccessException;    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 