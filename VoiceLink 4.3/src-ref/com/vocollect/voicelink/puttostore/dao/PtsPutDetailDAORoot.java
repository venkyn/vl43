/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.PtsPutDetail;

import java.util.Date;
import java.util.List;

/**
 * PtsPutDetails  Data Access Object (DAO) interface.
 *
 * @author svoruganti
 */
public interface PtsPutDetailDAORoot extends GenericDAO<PtsPutDetail> {


    /**
     * Get all put details.
     * @param decorator - additional query instructions.
     * @return the List of put details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listPutDetails(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Sum the quantity put field of the put detail
     * records for the specified license that fall between
     * the given startTime and endTime.
     *
     * @param licenseId - the license.
     * @param startTime - the start time.
     * @param endTime = the end time.
     *
     * @return Number - the sum of the quantity put field that meets the
     * the criteria
     *
     * @throws DataAccessException - database exceptions
     */
     Number sumQuantityPut(long licenseId, Date startTime, Date endTime)
                              throws DataAccessException;

    /**
     * sum the license prorate field for the specified license for put
     * detail records that fall between the start and end time.
     * for VoiceLink this method does the same thing as sumQuantityPut
     * but is here to allow for easy customization.
     *
     * @param licenseId - license to use.
     * @param startTime - start time
     * @param endTime - end time
     *
     * @return Number - the sum of the license prorate field that meets the
     * the criteria
     *
     * @throws DataAccessException - database exceptions
     */
     Number sumLicenseProrateCount(long licenseId, Date startTime, Date endTime)
                                      throws DataAccessException;


     /**
      * sum the group prorate field for the specified operator for pick
      * detail records that fall between the start and end time.
      *
      * @param operatorId - Operator Id.
      * @param startTime - start time
      * @param endTime - end time
      *
      * @return Number - the sum of the group prorate field that meets the
      * the criteria
      *
      * @throws DataAccessException - database exceptions
      */
      Number sumGroupProrateCount(long operatorId, Date startTime, Date endTime)
                                  throws DataAccessException;

      /**
       * Find the time of the last put detail record for the given operator
       * that is greater than or equal the minTime.
       *
       * @param operatorId - the operator to search.
       * @param minTime - the date/time returned must be greater or equal to minTime
       *
       * @return Date - the maximum end time from the license labor record.
       *
       * @throws DataAccessException - database exceptions
       */
       Date maxEndTime(long operatorId, Date minTime, Date maxTime) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 