/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.model.PtsRegion;

/**
 * Put To Store Region Data Access Object (DAO) interface.
 *
 * @author mnichols
 * @author svoruganti
 */
public interface PtsRegionDAORoot extends GenericDAO<PtsRegion> {


    /**
     * retrieves the region specified.
     *
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    PtsRegion findRegionByNumber(int regionNumber)
        throws DataAccessException;

    /**
     * Count the number of Licenses with a given status for a given region.
     * @param region - the region query is based on
     * @param status - the license status query is based on
     * @throws DataAccessException - indicates database error
     * @return the number of licenses
     */
    public Number countNumberOfLicenses(Region region, PtsLicenseStatus status)
        throws DataAccessException;

    /**
     * Count the number of Licenses with partial item number less than the region
     * spoken license length for a given region.
     * @param region - the region query is based on
     * @throws DataAccessException - indicates database error
     * @return the number of licenses
     */
    public Number minLicenseLength(Region region)
        throws DataAccessException;


    /**
     * Set partial number to work id when number of digits to speak is all.
     *
     * @param region - region being updated
     * @return - number of rows updated
     * @throws DataAccessException - database exception
     */
    public Integer updatePartialLicenseNumberAll(PtsRegion region)
        throws DataAccessException;

    /**
     * Set the partial number to the last (x) digits of license number.
     *
     * @param size1 - number of digits to set work id to
     * @param size2 - number of digits to set work id to (repeated for
     * auto-parameter mapping reasons)
     * @param region - region being updated
     * @return - number of rows updated
     * @throws DataAccessException - database exception
     */
    public int updateCustomPartialLicenseNumber(int size1, int size2, Long region)
        throws DataAccessException;





}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 