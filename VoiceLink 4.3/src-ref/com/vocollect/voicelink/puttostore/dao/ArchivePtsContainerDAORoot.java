/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.ArchivePtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;

import java.util.Date;
import java.util.List;

/**
 * Container Data Access Object (DAO) interface.
 *
 * @author mlashinsky
 */
public interface ArchivePtsContainerDAORoot extends GenericDAO<ArchivePtsContainer> {

    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<ArchivePtsContainer> listAllContainers() throws DataAccessException;

    /**
     * Get all containers.
     * @param decorator - additional query instructions.
     * @return the List of pick details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listArchivePtsContainers(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * retrieves a PtsContainer by the container number.
     *
     * @param containerNumber - value to retrieve by
     * @return the PtsContainer object found.
     * @throws DataAccessException - database exceptions
     */
    ArchivePtsContainer findArchivePtsContainerByNumber(String containerNumber)
        throws DataAccessException;

    /**
     * Gets a list of archive PTS Containers based on date and status.
     * @param createdDate date criteria
     * @param status status criteria
     * @return a list of archived PTS Containers
     * @throws DataAccessException - database exception
     */
    Integer updateOlderThan(Date createdDate,
            PtsContainerStatus status)
    throws DataAccessException;

    /**
     * Gets a list of archive container to populate in Container modified report
     * @param queryDecorator - the query decorator
     * @param startDate - the start date of the report
     * @param endDate - the end date of the report
     * @param siteId - the site id for the report
     * @return List<ArchivePtsContainer> - List of Archive Containers.
     * @throws DataAccessException
     */
    List<PtsContainerReport> listArchivePtsContainersForPtsContainerReport(QueryDecorator queryDecorator,
                                                                           Date startDate,
                                                                           Date endDate,
                                                                           Long siteId)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 