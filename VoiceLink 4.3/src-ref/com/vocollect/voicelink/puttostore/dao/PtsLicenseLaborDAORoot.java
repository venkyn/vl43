/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;

import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author treed
 */
public interface PtsLicenseLaborDAORoot extends GenericDAO<PtsLicenseLabor> {

    /**
     * Finds all open PTS License Labor associated with an operator ID.
     * @param operatorId - operator identifier
     * @return - list of PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<PtsLicenseLabor> listOpenRecordsByOperatorId(long operatorId)
                                                    throws DataAccessException;

    /**
     * Finds all PTS License Labor associated with an operator ID.
     * @param operatorId - operator identifier
     * @return - list of PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<PtsLicenseLabor> listRecordsByOperatorId(long operatorId)
                                                    throws DataAccessException;

    /**
     * Finds open PTS License Labor associated with a license Id.
     * @param licenseId - license identifier
     * @return - PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    PtsLicenseLabor findOpenRecordByLicenseId(long licenseId)
                                                    throws DataAccessException;

    /**
     * Finds all closed PTS License Labor associated a break labor ID.
     * @param breakLaborId - break labor identifier
     * @return - list of PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<PtsLicenseLabor> listClosedRecordsByBreakLaborId(long breakLaborId)
                                                    throws DataAccessException;

    /**
     * Returns the sum of all the puts associated with an operator labor record..
     * @param operatorLaborId - operator labor identifier
     * @return Integer - sum of all puts associated with one operator labor record.  Can be zero.
     * @throws DataAccessException - Database Exception
     */
    Long sumQuantityPut(long operatorLaborId)
                                                    throws DataAccessException;

    /**
     * Finds all PTS License Labor associated a operator labor id.
     * @param operatorLaborId - break labor identifier
     * @return - list of PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<PtsLicenseLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
                                                    throws DataAccessException;

    /**
     * Finds all PTS License Labor records.
     * @param decorator - query decorator
     * @return - list of PTS License Labor
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listPtsLicenseLabor(QueryDecorator decorator)
                                                    throws DataAccessException;

    /**
     * Get list of licenses labor for PTS License Labor Report.
     *
     * @param decorator - the decorator for the where clause.
     * @param startTime - Start Time of the PTS License Labor Report.
     * @param endTime - endTime of the PTS License Labor Report.
     * @param siteId - the site of the PTS License Labor Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseLaborReport> listLicensesLaborForPtsLicenseLaborReport(
        QueryDecorator decorator,
        Date startTime,
        Date endTime,
        Long siteId)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 