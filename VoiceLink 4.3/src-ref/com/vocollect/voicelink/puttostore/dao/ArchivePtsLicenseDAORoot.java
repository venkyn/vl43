/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.puttostore.model.ArchivePtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author mlashinsky
 */
public interface ArchivePtsLicenseDAORoot extends GenericDAO<ArchivePtsLicense> {

    /**
     * Gets a list of archive assignments based on date and status.
     * @param createdDate date criteria
     * @param status status criteria
     * @return a list of archived assignments
     * @throws DataAccessException - database exception
     */
    Integer updateOlderThan(Date createdDate, PtsLicenseStatus status)
        throws DataAccessException;
    
    
    /**
     * Get list of Archive licenses for PTS License Report.
     *
     * @param queryDecorator - the query decorator for the where clause
     * @param startTime - Start Time of the Report.
     * @param endTime - endTime of the Report. 
     * @param siteId - Site Id for the Report. 
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseReport> listArchiveLicensesForPtsLicenseReport(
        QueryDecorator queryDecorator, Date startTime, Date endTime, Long siteId)
    throws DataAccessException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 