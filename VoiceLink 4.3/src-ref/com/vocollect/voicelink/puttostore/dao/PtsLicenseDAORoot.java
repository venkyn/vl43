/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;

import java.util.Date;
import java.util.List;

/**
 * Put to Store License Data Access Object (DAO) interface.
 *
 * @author mnichols
 */
public interface PtsLicenseDAORoot extends GenericDAO<PtsLicense> {

    /**
     * List all licenses. This override is for a left join on the operatorid.
     * @param decorator - additional query instructions.
     * @return - list of chase assignments in decending number order
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listLicenses(QueryDecorator decorator)
                                    throws DataAccessException;

    /**
     * Get license(s) with specific license number.
     * @param queryDecorator - limit result set
     * @param number - License Number
     * @param regionId - filter results by region
     * @return -  the requested license
     * @throws DataAccessException - on database exception
     */
    List<PtsLicense> listLicenseByNumberAndRegion(QueryDecorator queryDecorator,
                                                  String number,
                                                  Region regionId)
                                                  throws DataAccessException;

    /**
     * Get a list of licenses that has this partial number.
     * @param queryDecorator - limit result set
     * @param partialNumber - partial License Number
     * @param regionId - filter results by region
     * @return the requested license
     * @throws DataAccessException - on database exception
     */
    List<PtsLicense> listLicenseByPartialNumberAndRegion(QueryDecorator queryDecorator,
                                                         String partialNumber,
                                                         Region regionId)
                                                         throws DataAccessException;


    /**
     * Get a list of licenses that are reserved by a given operator.
     * @param reserveValue - the value in the reserved field of the license object.
     * @return - list of reserved licenses
     * @throws DataAccessException - on db failure
     */
    List<PtsLicense> listReservedLicenses(String reserveValue)
                                    throws DataAccessException;


    /**
     * retrieves a list of active licenses for the operator.
     *
     * @param operator - value to retrieve by
     * @return list of active licenses, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicense> listActiveLicenses(Operator operator)
                                    throws DataAccessException;

    /**
     * Get all licenses in specified group.
     *
     * @param groupNumber - group number to retrieve
     * @return - list of licenses in group
     * @throws DataAccessException - database exception
     */
    List<PtsLicense> listLicensesInGroup(Long groupNumber)
        throws DataAccessException;

    /**
     * Get list of licenses for PTS License Report.
     *
     * @param queryDecorator - the decorator for the where clause.
     * @param startTime - Start Time of the Report.
     * @param endTime - endTime of the Report.
     * @return a list of licenses contained in the group.
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicenseReport> listLicensesForPtsLicenseReport(
        QueryDecorator queryDecorator,
        Date startTime,
        Date endTime)
        throws DataAccessException;

    /**
     * Find license by number.
     *
     * @param licenseNumber - group number to retrieve
     * @return - list of licenses in group
     * @throws DataAccessException - database exception
     */
    PtsLicense findLicenseByNumber(String licenseNumber)
        throws DataAccessException;

    /**
     * Updates PTS Licenses that are ready to be purged or archived.
     *
     * @param purgeable - whether to mark for archive(1) or purge (2)
     * @param date - Date to find assignment older than
     * @param status - status of assignments to find
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateOlderThan(Integer purgeable,
                            Date date,
                            PtsLicenseStatus status) throws DataAccessException;

    /**
     * Get a specified number of PTS Licenses to archive.
     *
     * @param queryDecorator - query decorator for number of row to retrieve
     * @return - list of PTS Licenses
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicense> listArchive(QueryDecorator queryDecorator)
        throws DataAccessException;


    /**
     * Get a specified number of PTS Licenses to purge.
     *
     * @param queryDecorator - query decorator for number of row to retrieve
     * @return - list of PTS Licenses
     * @throws DataAccessException - database exceptions
     */
    List<PtsLicense> listPurge(QueryDecorator queryDecorator)
        throws DataAccessException;

 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 