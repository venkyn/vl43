/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;

/**
 * 
 * 
 * @author mlashinsky
 */
public class PtsContainerReportRoot {

    private String containerNumber;

    private PtsContainerStatus containerStatus;

    private Integer quantityPut;

    private String itemDescription;

    private String itemNumber;

    private String operatorName;

    private String operatorId;

    private Date putTime;

    private String customerNumber;

    private String customerName;

    private String route;

    private String customerAddress;

    private String aisle;

    private String postAisle;

    private String preAisle;

    private String slot;

    private String licenseNumber;

    /**
     * Constructor.
     * @param containerNumber String value of the container number
     * @param containerStatus PtsContainerStatus of the container
     * @param quantityPut Integer value of the quantity put into the container
     * @param itemDescription String value of the item description
     * @param itemNumber String value of the item number
     * @param operatorName String value of the operator name (may be null)
     * @param operatorId String value of the operator Identifier
     * @param putTime Date value of the put time
     * @param customerNumber String value of the Customer Number
     * @param customerName String value of the Customer Name
     * @param route String value of the route
     * @param customerAddress String value of the Customer Address
     * @param aisle String value of the aisle direction
     * @param postAisle String value of the post-aisle direction
     * @param preAisle String value of the pre-aisle direction
     * @param slot String value of the slot direction
     * @param licenseNumber String value of the licenseNumber
     */
    public PtsContainerReportRoot(String containerNumber,
                                  PtsContainerStatus containerStatus,
                                  Integer quantityPut,
                                  String itemDescription,
                                  String itemNumber,
                                  String operatorName,
                                  String operatorId,
                                  Date putTime,
                                  String customerNumber,
                                  String customerName,
                                  String route,
                                  String customerAddress,
                                  String aisle,
                                  String postAisle,
                                  String preAisle,
                                  String slot,
                                  String licenseNumber) {
        super();
        this.containerNumber = containerNumber;
        this.containerStatus = containerStatus;
        this.quantityPut = quantityPut;
        this.itemDescription = itemDescription;
        this.itemNumber = itemNumber;
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.putTime = putTime;
        this.customerNumber = customerNumber;
        this.customerName = customerName;
        this.route = route;
        this.customerAddress = customerAddress;
        this.aisle = aisle;
        this.postAisle = postAisle;
        this.preAisle = preAisle;
        this.slot = slot;
        this.licenseNumber = licenseNumber;
    }

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for the containerStatus property.
     * @return PtsContainerStatus value of the property
     */
    public PtsContainerStatus getContainerStatus() {
        return containerStatus;
    }

    /**
     * Setter for the containerStatus property.
     * @param containerStatus the new containerStatus value
     */
    public void setContainerStatus(PtsContainerStatus containerStatus) {
        this.containerStatus = containerStatus;
    }

    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }

    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }

    /**
     * Getter for the itemDescription property.
     * @return String value of the property
     */
    public String getItemDescription() {
        return itemDescription;
    }

    /**
     * Setter for the itemDescription property.
     * @param itemDescription the new itemDescription value
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    /**
     * Getter for the itemNumber property.
     * @return String value of the property
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Setter for the itemNumber property.
     * @param itemNumber the new itemNumber value
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * Getter for the operatorId property.
     * @return String value of the property
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * Setter for the operatorId property.
     * @param operatorId the new operatorId value
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }

    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }

    /**
     * Getter for the customerNumber property.
     * @return String value of the property
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Setter for the customerNumber property.
     * @param customerNumber the new customerNumber value
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * Getter for the customerName property.
     * @return String value of the property
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Setter for the customerName property.
     * @param customerName the new customerName value
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Getter for the route property.
     * @return String value of the property
     */
    public String getRoute() {
        return route;
    }

    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Getter for the customerAddress property.
     * @return String value of the property
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Setter for the customerAddress property.
     * @param customerAddress the new customerAddress value
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    /**
     * Getter for the aisle property.
     * @return String value of the property
     */
    public String getAisle() {
        return aisle;
    }

    /**
     * Setter for the aisle property.
     * @param aisle the new aisle value
     */
    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    /**
     * Getter for the postAisle property.
     * @return String value of the property
     */
    public String getPostAisle() {
        return postAisle;
    }

    /**
     * Setter for the postAisle property.
     * @param postAisle the new postAisle value
     */
    public void setPostAisle(String postAisle) {
        this.postAisle = postAisle;
    }

    /**
     * Getter for the preAisle property.
     * @return String value of the property
     */
    public String getPreAisle() {
        return preAisle;
    }

    /**
     * Setter for the preAisle property.
     * @param preAisle the new preAisle value
     */
    public void setPreAisle(String preAisle) {
        this.preAisle = preAisle;
    }

    /**
     * Getter for the slot property.
     * @return String value of the property
     */
    public String getSlot() {
        return slot;
    }

    /**
     * Setter for the slot property.
     * @param slot the new slot value
     */
    public void setSlot(String slot) {
        this.slot = slot;
    }

    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 