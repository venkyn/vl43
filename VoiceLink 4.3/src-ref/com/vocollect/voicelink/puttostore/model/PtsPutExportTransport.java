/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.puttostore.model;


import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.ExportTransportObject;


/**
 * @author svoruganti
 *
 */
public class PtsPutExportTransport extends ExportTransportObject {


    /**
     * Default Constructor.
     *
     */
    public PtsPutExportTransport() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToInProgress(Object object) {
        if (object instanceof PtsLicense) {
            ((PtsLicense) object).setExportStatus(ExportStatus.InProgress);
        } else if (object instanceof PtsPutDetail) {
            ((PtsLicense) object).setExportStatus(ExportStatus.InProgress);
        } else if (object instanceof PtsLicenseLabor) {
            ((PtsLicenseLabor) object).setExportStatus(ExportStatus.InProgress);
       }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToExported(Object object) {
        if (object instanceof PtsLicense) {
            ((PtsLicense) object).setExportStatus(ExportStatus.Exported);
        } else if (object instanceof PtsPutDetail) {
            ((PtsPutDetail) object).setExportStatus(ExportStatus.Exported);
        } else if (object instanceof PtsLicenseLabor) {
            ((PtsLicenseLabor) object).setExportStatus(ExportStatus.Exported);
        }
    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToInProgress(PtsLicense object) {
        object.setExportStatus(ExportStatus.InProgress);
    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToExported(PtsLicense object) {
        object.setExportStatus(ExportStatus.Exported);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 