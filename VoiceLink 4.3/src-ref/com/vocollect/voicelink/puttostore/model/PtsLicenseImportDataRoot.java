/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.AssignmentGroup;


import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author svoruganti
 *
 */
public class PtsLicenseImportDataRoot extends BaseModelObject implements
        Importable {

    /**
     * Serializable id.
     */
    private static final long serialVersionUID = 9184350839839851159L;

    private PtsLicense     myModel = new PtsLicense();

    private ImportableImpl impl = new ImportableImpl();

    private List<PtsPutImportData> importablePtsPutData;




    /**
     * Set the number in the region. This will be used to look up a region in the database.
     * @param number the number to set the regionnumber to.
     */
    public void setRegionNumber(Integer number) {
        PtsRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new PtsRegion();
            setRegion(tempRegion);
        }
        tempRegion.setNumber(number);
    }

    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public Integer getRegionNumber() {
        PtsRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new PtsRegion();
            setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return tempRegion.getNumber();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setRegion(com.vocollect.voicelink.puttostore.model.PtsRegion)
     */
    public void setRegion(PtsRegion region) {
        myModel.setRegion(region);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        for (PtsPutImportData put : this.importablePtsPutData) {
            myModel.addPut((PtsPut) put.convertToModel());
        }
        return myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public PtsLicenseStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long customerID) {
        impl.setImportID(customerID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setStatus(PtsLicenseStatus status) {
        myModel.setStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

       /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getEndTime()
     */
    public Date getEndTime() {
        return myModel.getEndTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getExportStatus()
     */
    public ExportStatus getExportStatus() {
        return myModel.getExportStatus();
    }

       /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getGUITransitionStatuses()
     */
    public Map<Integer, String> getGUITransitionStatuses() {
        return myModel.getGUITransitionStatuses();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getOperator()
     */
    public Operator getOperator() {
        return myModel.getOperator();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getReservedBy()
     */
    public String getReservedBy() {
        return myModel.getReservedBy();
    }

       /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getShortedCount()
     */
    public int getShortedCount() {
        return myModel.getShortedCount();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getStartTime()
     */
    public Date getStartTime() {
        return myModel.getStartTime();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
         return this.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
    * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#addPut(com.vocollect.voicelink.puttostore.model.PtsPut)
     */
    public void addPut(PtsPut newPut) {
        myModel.addPut(newPut);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#adjustResidualQuantityForPuts(com.vocollect.voicelink.core.model.Item, int)
     */
    public void adjustResidualQuantityForPuts(Item theItem,
                                              int newResidualQuantity) {
        myModel.adjustResidualQuantityForPuts(theItem, newResidualQuantity);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#autoShortRemainingPuts(com.vocollect.voicelink.core.model.Item)
     */
    public void autoShortRemainingPuts(Item theItem)
        throws BusinessRuleException {
        myModel.autoShortRemainingPuts(theItem);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getExpectedResiduals()
     */
    public Integer getExpectedResiduals() {
        return myModel.getExpectedResiduals();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getGroupInfo()
     */
    public AssignmentGroup getGroupInfo() {
        return myModel.getGroupInfo();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getLabor()
     */
    public List<PtsLicenseLabor> getLabor() {
        return myModel.getLabor();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getNotPutCount()
     */
    public int getNotPutCount() {
        return myModel.getNotPutCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getPartialCount()
     */
    public int getPartialCount() {
        return myModel.getPartialCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getPartialNumber()
     */
    public String getPartialNumber() {
        return myModel.getPartialNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getPutDetailCount()
     */
    public int getPutDetailCount() {
        return myModel.getPutDetailCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getPuts()
     */
    public List<PtsPut> getPuts() {
        return myModel.getPuts();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getRegion()
     */
    public PtsRegion getRegion() {
        return myModel.getRegion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getShortedCountForItem(com.vocollect.voicelink.core.model.Item)
     */
    public int getShortedCountForItem(Item theItem) {
        return myModel.getShortedCountForItem(theItem);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getSkipCount()
     */
    public int getSkipCount() {
        return myModel.getSkipCount();
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#getTotalItemQuantity()
     */
    public int getTotalItemQuantity() {
        return myModel.getTotalItemQuantity();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#isEditable()
     */
    public boolean isEditable() {
        return myModel.isEditable();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#processStatusChange(com.vocollect.voicelink.puttostore.model.PtsLicenseStatus)
     */
    public void processStatusChange(PtsLicenseStatus statusTo)
        throws BusinessRuleException {
        myModel.processStatusChange(statusTo);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setEndTime(java.util.Date)
     */
    public void setEndTime(Date endTime) {
        myModel.setEndTime(endTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setExpectedResiduals(java.lang.Integer)
     */
    public void setExpectedResiduals(Integer expectedResiduals) {
        myModel.setExpectedResiduals(expectedResiduals);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setExportStatus(com.vocollect.voicelink.core.model.ExportStatus)
     */
    public void setExportStatus(ExportStatus exportStatus) {
        myModel.setExportStatus(exportStatus);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setGroupInfo(com.vocollect.voicelink.selection.model.AssignmentGroup)
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        myModel.setGroupInfo(groupInfo);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setLabor(java.util.List)
     */
    public void setLabor(List<PtsLicenseLabor> labor) {
        myModel.setLabor(labor);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public void setOperator(Operator operator) {
        myModel.setOperator(operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setPartialNumber(java.lang.String)
     */
    public void setPartialNumber(String partialNumber) {
        myModel.setPartialNumber(partialNumber);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setPutDetailCount(int)
     */
    public void setPutDetailCount(int putDetailCount) {
        myModel.setPutDetailCount(putDetailCount);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setPuts(java.util.List)
     */
    public void setPuts(List<PtsPut> puts) {
        myModel.setPuts(puts);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setReservedBy(java.lang.String)
     */
    public void setReservedBy(String reservedBy) {
        myModel.setReservedBy(reservedBy);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setStartTime(java.util.Date)
     */
    public void setStartTime(Date startTime) {
        myModel.setStartTime(startTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#setTotalItemQuantity(int)
     */
    public void setTotalItemQuantity(int totalItemQuantity) {
        myModel.setTotalItemQuantity(totalItemQuantity);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#startLicense(com.vocollect.voicelink.core.model.Operator, java.util.Date)
     */
    public void startLicense(Operator assignOperator, Date assignStartTime)
        throws BusinessRuleException {
        myModel.startLicense(assignOperator, assignStartTime);
    }

    /**
     *{@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsLicenseRoot#stopAssignment(com.vocollect.voicelink.puttostore.model.PtsLicenseStatus, java.util.Date)
     */
    public void stopAssignment(PtsLicenseStatus statusTo, Date licenseEndTime)
        throws BusinessRuleException {
        myModel.stopAssignment(statusTo, licenseEndTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }


    /**
     * Getter for the importablePutData property.
     * @return List<PtsPutImportData> value of the property
     */
    public List<PtsPutImportData> getImportablePutData() {
        return importablePtsPutData;
    }


    /**
     * Setter for the importablePutData property.
     * @param importablePutData the new importablePutData value
     */
    public void setImportablePutData(List<PtsPutImportData> importablePutData) {
        this.importablePtsPutData = importablePutData;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 