/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;

/**
 *
 *
 * @author mlashinsky
 */
public class PtsOperatorLaborReportRoot {

    private String operatorName;

    private String operatorId;

    private String regionName;

    private Integer regionNumber;

    private OperatorLaborActionType actionType;

    private Long totalQuantity;

    private Double totalTimeHours;

    private Double totalTimeMins;

    private Integer goalRate;

    private Double actualRate;

    private Double percentOfGoal;

    private OperatorLaborFilterType filterType;

    /**
     * Constructor.
     * @param operatorName String value of Operator Name
     * @param operatorId String value of Operator Identifier
     * @param regionName String value of Region Name
     * @param regionNumber String value of Region Number
     * @param actionType OperatorLaborActionType value of the Action Type
     * @param totalQuantity Long value of the Total Quantity
     * @param totalTimeHours Double value of Hours in Total Time
     * @param totalTimeMins Double value of Mins in Total Time
     * @param goalRate Integer value of the Goal Rate (picks/hour)
     * @param actualRate Double value of the Actual Operator Rate (picks/hour)
     * @param percentOfGoal Double value of the Percent of Goal (percent)
     * @param filterType OperatorLaborFilterType of Filter Type
     */
    public PtsOperatorLaborReportRoot(String operatorName,
                                      String operatorId,
                                      String regionName,
                                      Integer regionNumber,
                                      OperatorLaborActionType actionType,
                                      Long totalQuantity,
                                      Double totalTimeHours,
                                      Double totalTimeMins,
                                      Integer goalRate,
                                      Double actualRate,
                                      Double percentOfGoal,
                                      OperatorLaborFilterType filterType) {
        super();
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.regionName = regionName;
        this.regionNumber = regionNumber;
        this.actionType = actionType;
        this.totalQuantity = totalQuantity;
        this.totalTimeHours = totalTimeHours;
        this.totalTimeMins = totalTimeMins;
        this.goalRate = goalRate;
        this.actualRate = actualRate;
        this.percentOfGoal = percentOfGoal;
        this.filterType = filterType;
    }

    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * Getter for the operatorId property.
     * @return String value of the property
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * Setter for the operatorId property.
     * @param operatorId the new operatorId value
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * Getter for the regionName property.
     * @return String value of the property
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * Setter for the regionName property.
     * @param regionName the new regionName value
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public Integer getRegionNumber() {
        return regionNumber;
    }

    /**
     * Setter for the regionNumber property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(Integer regionNumber) {
        this.regionNumber = regionNumber;
    }

    /**
     * Getter for the actionType property.
     * @return OperatorLaborActionType value of the property
     */
    public OperatorLaborActionType getActionType() {
        return actionType;
    }

    /**
     * Setter for the actionType property.
     * @param actionType the new actionType value
     */
    public void setActionType(OperatorLaborActionType actionType) {
        this.actionType = actionType;
    }

    /**
     * Getter for the totalQuantity property.
     * @return Long value of the property
     */
    public Long getTotalQuantity() {
        return totalQuantity;
    }

    /**
     * Setter for the totalQuantity property.
     * @param totalQuantity the new totalQuantity value
     */
    public void setTotalQuantity(Long totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    /**
     * Getter for the totalTimeHours property.
     * @return Double value of the property
     */
    public Double getTotalTimeHours() {
        return totalTimeHours;
    }

    /**
     * Setter for the totalTimeHours property.
     * @param totalTimeHours the new totalTimeHours value
     */
    public void setTotalTimeHours(Double totalTimeHours) {
        this.totalTimeHours = totalTimeHours;
    }

    /**
     * Getter for the totalTimeMins property.
     * @return Double value of the property
     */
    public Double getTotalTimeMins() {
        return totalTimeMins;
    }

    /**
     * Setter for the totalTimeMins property.
     * @param totalTimeMins the new totalTimeMins value
     */
    public void setTotalTimeMins(Double totalTimeMins) {
        this.totalTimeMins = totalTimeMins;
    }

    /**
     * Getter for the goalRate property.
     * @return Integer value of the property
     */
    public Integer getGoalRate() {
        return goalRate;
    }

    /**
     * Setter for the goalRate property.
     * @param goalRate the new goalRate value
     */
    public void setGoalRate(Integer goalRate) {
        this.goalRate = goalRate;
    }

    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        return actualRate;
    }

    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        return percentOfGoal;
    }

    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

    /**
     * Getter for the filterType property.
     * @return OperatorLaborFilterType value of the property
     */
    public OperatorLaborFilterType getFilterType() {
        return filterType;
    }

    /**
     * Setter for the filterType property.
     * @param filterType the new filterType value
     */
    public void setFilterType(OperatorLaborFilterType filterType) {
        this.filterType = filterType;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 