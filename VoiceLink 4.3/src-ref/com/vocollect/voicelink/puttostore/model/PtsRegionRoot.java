/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.Region;

import java.io.Serializable;
import java.util.Set;

/**
 * Put To Store Region Model Object.
 *
 * @author pfunyak
 */
public class PtsRegionRoot extends Region implements Serializable {

    //
    private static final long serialVersionUID = -7284981351124644810L;
    
    private static final int VALIDATE_CONTAINER_LEN_DEFAULT = 3; 

    private boolean allowSkipAisle = true;

    private boolean allowSkipSlot = true;

    private boolean allowRepickSkips = true;

    private boolean allowSignOff = true;

    private boolean allowPassAssignment  = false;

    private boolean allowMultipleOpenContainers = true;

    private boolean systemGeneratesContainerID  = true;

    private Integer spokenLicenseLength = 0;

    private Integer maxLicensesInGroup = 1;

    private boolean validateContainerId;

    private Integer validateContainerLength = VALIDATE_CONTAINER_LEN_DEFAULT;

    private boolean confirmSpokenLocation = true;

    private boolean confirmSpokenLicense = true;

    private Integer spokenLocationLength = 0;

    private Integer locationCheckDigitLength = 2;

    private String flowThruLocation;

    private String residualLocation;

    private String residualLocationCD;

    private String unExpResidualLocation;

    private String unExpResidualLocationCD;

    // Set of put to store licenses within the region
    private Set<PtsLicense> licenses;

    /**
     * Getter for the maxLicensesInGroup property.
     * @return Integer value of the property
     */
    public Integer getMaxLicensesInGroup() {
        return this.maxLicensesInGroup;
    }

    /**
     * Setter for the maxLicensesInGroup property.
     * @param maxLicensesInGroup the new maxLicensesInGroup value
     */
    public void setMaxLicensesInGroup(Integer maxLicensesInGroup) {
        this.maxLicensesInGroup = maxLicensesInGroup;
    }

    /**
     * Getter for the allowSkipAisle property.
     * @return boolean value of the property
     */
    public boolean isAllowSkipAisle() {
        return allowSkipAisle;
    }

    /**
     * Setter for the allowSkipAisle property.
     * @param allowSkipAisle the new allowSkipAisle value
     */
    public void setAllowSkipAisle(boolean allowSkipAisle) {
        this.allowSkipAisle = allowSkipAisle;
    }

    /**
     * Getter for the allowSkipSlot property.
     * @return boolean value of the property
     */
    public boolean isAllowSkipSlot() {
        return allowSkipSlot;
    }

    /**
     * Setter for the allowSkipSlot property.
     * @param allowSkipSlot the new allowSkipSlot value
     */
    public void setAllowSkipSlot(boolean allowSkipSlot) {
        this.allowSkipSlot = allowSkipSlot;
    }

    /**
     * Getter for the allowRepickSkips property.
     * @return boolean value of the property
     */
    public boolean isAllowRepickSkips() {
        return allowRepickSkips;
    }

    /**
     * Setter for the allowRepickSkips property.
     * @param allowRepickSkips the new allowRepickSkips value
     */
    public void setAllowRepickSkips(boolean allowRepickSkips) {
        this.allowRepickSkips = allowRepickSkips;
    }

    /**
     * Getter for the allowSignOff property.
     * @return boolean value of the property
     */
    public boolean isAllowSignOff() {
        return allowSignOff;
    }

    /**
     * Setter for the allowSignOff property.
     * @param allowSignOff the new allowSignOff value
     */
    public void setAllowSignOff(boolean allowSignOff) {
        this.allowSignOff = allowSignOff;
    }

    /**
     * Getter for the allowPassAssignment property.
     * @return boolean value of the property
     */
    public boolean isAllowPassAssignment() {
        return allowPassAssignment;
    }

    /**
     * Setter for the allowPassAssignment property.
     * @param allowPassAssignment the new allowPassAssignment value
     */
    public void setAllowPassAssignment(boolean allowPassAssignment) {
        this.allowPassAssignment = allowPassAssignment;
    }

    /**
     * Getter for the systemGeneratesContainerID property.
     * @return boolean value of the property
     */
    public boolean isSystemGeneratesContainerID() {
        return systemGeneratesContainerID;
    }

    /**
     * Setter for the systemGeneratesContainerID property.
     * @param systemGeneratesContainerID the new systemGeneratesContainerID value
     */
    public void setSystemGeneratesContainerID(boolean systemGeneratesContainerID) {
        this.systemGeneratesContainerID = systemGeneratesContainerID;
    }

    /**
     * Getter for the allowMultipleOpenContainers property.
     * @return boolean value of the property
     */
    public boolean isAllowMultipleOpenContainers() {
        return allowMultipleOpenContainers;
    }

    /**
     * Setter for the allowMultipleOpenContainers property.
     * @param allowMultipleOpenContainers the new allowMultipleOpenContainers value
     */
    public void setAllowMultipleOpenContainers(boolean allowMultipleOpenContainers) {
        this.allowMultipleOpenContainers = allowMultipleOpenContainers;
    }

    /**
     * Getter for the spokenLicenseLength property.
     * @return Integer value of the property
     */
    public Integer getSpokenLicenseLength() {
        return spokenLicenseLength;
    }

    /**
     * Setter for the spokenLicenseLength property.
     * @param spokenLicenseLength the new spokenLicenseLength value
     */
    public void setSpokenLicenseLength(Integer spokenLicenseLength) {
        this.spokenLicenseLength = spokenLicenseLength;
    }

    /**
     * Getter for the validateContainerIds property.
     * @return boolean value of the property
     */
    public boolean isValidateContainerId() {
        return validateContainerId;
    }

    /**
     * Setter for the validateContainerIds property.
     * @param validateContainerId the new validateContainerId value
     */
    public void setValidateContainerId(boolean validateContainerId) {
        this.validateContainerId = validateContainerId;
    }

    /**
     * Getter for the validateContainerLength property.
     * @return Integer value of the property
     */
    public Integer getValidateContainerLength() {
        return validateContainerLength;
    }

    /**
     * Setter for the validateContainerLength property.
     * @param validateContainerLength the new validateContainerLength value
     */
    public void setValidateContainerLength(Integer validateContainerLength) {
        this.validateContainerLength = validateContainerLength;
    }

    /**
     * Getter for the confirmSpokenLocation property.
     * @return boolean value of the property
     */
    public boolean isConfirmSpokenLocation() {
        return confirmSpokenLocation;
    }

    /**
     * Setter for the confirmSpokenLocation property.
     * @param confirmSpokenLocation the new confirmSpokenLocation value
     */
    public void setConfirmSpokenLocation(boolean confirmSpokenLocation) {
        this.confirmSpokenLocation = confirmSpokenLocation;
    }

    /**
     * Getter for the spokenLocationLength property.
     * @return Integer value of the property
     */
    public Integer getSpokenLocationLength() {
        return spokenLocationLength;
    }

    /**
     * Setter for the spokenLocationLength property.
     * @param spokenLocationLength the new spokenLocationLength value
     */
    public void setSpokenLocationLength(Integer spokenLocationLength) {
        this.spokenLocationLength = spokenLocationLength;
    }

    /**
     * Getter for the locationCheckDigitLength property.
     * @return Integer value of the property
     */
    public Integer getLocationCheckDigitLength() {
        return locationCheckDigitLength;
    }

    /**
     * Setter for the locationCheckDigitLength property.
     * @param locationCheckDigitLength the new locationCheckDigitLength value
     */
    public void setLocationCheckDigitLength(Integer locationCheckDigitLength) {
        this.locationCheckDigitLength = locationCheckDigitLength;
    }

    /**
     * Getter for the flowThruLocation property.
     * @return String value of the property
     */
    public String getFlowThruLocation() {
        return flowThruLocation;
    }

    /**
     * Setter for the flowThruLocation property.
     * @param flowThruLocation the new flowThruLocation value
     */
    public void setFlowThruLocation(String flowThruLocation) {
        this.flowThruLocation = flowThruLocation;
    }

    /**
     * Getter for the residualLocation property.
     * @return String value of the property
     */
    public String getResidualLocation() {
        return residualLocation;
    }

    /**
     * Setter for the residualLocation property.
     * @param residualLocation the new residualLocation value
     */
    public void setResidualLocation(String residualLocation) {
        this.residualLocation = residualLocation;
    }

    /**
     * Getter for the residualLocationCD property.
     * @return String value of the property
     */
    public String getResidualLocationCD() {
        return residualLocationCD;
    }

    /**
     * Setter for the residualLocationCD property.
     * @param residualLocationCD the new residualLocationCD value
     */
    public void setResidualLocationCD(String residualLocationCD) {
        this.residualLocationCD = residualLocationCD;
    }

    /**
     * Getter for the unExpResidualLocation property.
     * @return String value of the property
     */
    public String getUnExpResidualLocation() {
        return unExpResidualLocation;
    }

    /**
     * Setter for the unExpResidualLocation property.
     * @param unExpResidualLocation the new unExpResidualLocation value
     */
    public void setUnExpResidualLocation(String unExpResidualLocation) {
        this.unExpResidualLocation = unExpResidualLocation;
    }

    /**
     * Getter for the unExpResidualLocationCD property.
     * @return String value of the property
     */
    public String getUnExpResidualLocationCD() {
        return unExpResidualLocationCD;
    }

    /**
     * Setter for the unExpResidualLocationCD property.
     * @param unExpResidualLocationCD the new unExpResidualLocationCD value
     */
    public void setUnExpResidualLocationCD(String unExpResidualLocationCD) {
        this.unExpResidualLocationCD = unExpResidualLocationCD;
    }

    /**
     * Getter for the licenses property.
     * @return Set of PtsLicense value of the property
     */
    public Set<PtsLicense> getLicenses() {
        return licenses;
    }

    /**
     * Setter for the licenses property.
     * @param licenses the new licenses value
     */
    public void setLicenses(Set<PtsLicense> licenses) {
        this.licenses = licenses;
    }


    /**
     * Getter for the confirmSpokenLicense property.
     * @return boolean value of the property
     */
    public boolean isConfirmSpokenLicense() {
        return confirmSpokenLicense;
    }


    /**
     * Setter for the confirmSpokenLicense property.
     * @param confirmSpokenLicense the new confirmSpokenLicense value
     */
    public void setConfirmSpokenLicense(boolean confirmSpokenLicense) {
        this.confirmSpokenLicense = confirmSpokenLicense;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 