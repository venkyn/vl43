/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCode;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.model.AssignmentGroup;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Available;
import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Complete;
import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.InProgress;
import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Passed;
import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Short;
import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Suspended;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.NotPut;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Partial;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Shorted;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Skipped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


/**
 * Model object representing a VoiceLink Put To Store License.
 *
 * @author mnichols
 */
public class PtsLicenseRoot extends CommonModelObject implements Serializable, Taggable {

    private static final long serialVersionUID = 6480571401517467618L;


    private AssignmentGroup             groupInfo;

    private PtsRegion                   region;

    private String                      number;

    private String                      partialNumber;

    private PtsLicenseStatus            status = Available;

    private Operator                    operator;

    private Date                        startTime;

    private Date                        endTime;

    private Integer                     expectedResiduals;

    private String                      reservedBy;

    //Total item quantity
    private int                         totalItemQuantity = 0;

    private int                         putDetailCount = 0;

    private List<PtsPut>                puts  = new ArrayList<PtsPut>();

    // labor records belonging to this assignment (Added for archive purposes)
    private List<PtsLicenseLabor>       labor = new ArrayList<PtsLicenseLabor>();

    private Set<Tag>                    tags;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    private Integer purgeable = 0;

    /**
     * Getter for the partialNumber property.
     * @return String value of the property
     */
    public String getPartialNumber() {
        return this.partialNumber;
    }

    /**
     * Setter for the partialNumber property.
     * @param partialNumber the new partialNumber value
     */
    public void setPartialNumber(String partialNumber) {
        this.partialNumber = partialNumber;
    }

    /**
     * Getter for the reservedBy property.
     * @return String value of the property
     */
    public String getReservedBy() {
        return this.reservedBy;
    }



    /**
     * Setter for the reservedBy property.
     * @param reservedBy the new reservedBy value
     */
    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }


    /**
     * Getter for the groupInfo property.
     * @return AssignmentGroup value of the property
     */
    public AssignmentGroup getGroupInfo() {
        if (this.groupInfo == null) {
            setGroupInfo(new AssignmentGroup());
        }
        return groupInfo;
    }


    /**
     * Setter for the groupInfo property.
     * @param groupInfo the new groupInfo value
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        this.groupInfo = groupInfo;
    }


    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }


    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the status property.
     * @return PtsLicenseStatus value of the property
     */
    public PtsLicenseStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    protected void setStatus(PtsLicenseStatus status) {
        this.status = status;
    }

    /**
     * Publicly available method for safely changing the status property.
     * @param newStatus the new status value
     * @throws BusinessRuleException if status change is not valid.
     */
    public void changeStatus(PtsLicenseStatus newStatus) throws BusinessRuleException {
        this.processStatusChange(newStatus);
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return endTime;
    }


    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    /**
     * Getter for the expectedResiduals property.
     * @return Integer value of the property
     */
    public Integer getExpectedResiduals() {
        return expectedResiduals;
    }


    /**
     * Setter for the expectedResiduals property.
     * @param expectedResiduals the new expectedResiduals value
     */
    public void setExpectedResiduals(Integer expectedResiduals) {
        this.expectedResiduals = expectedResiduals;
    }


    /**
     * Getter for the puts property.
     * @return List of puts
    */
    public List<PtsPut> getPuts() {
        return puts;
    }


    /**
     * Setter for the puts list property.
     * This also ensures that these puts have their license member pointed at
     * this license.
     * @param puts the new puts value
     */
    public void setPuts(List<PtsPut> puts) {
        this.puts = puts;
        //Loop through the puts and ensure their license member points to this
        // license.
        for (PtsPut p : puts) {
            p.setLicense((PtsLicense) this);
        }
    }



    /**
     * Getter for the totalItemQuantity property.
     * @return int value of the property
     */
    public int getTotalItemQuantity() {
        return totalItemQuantity;
    }



    /**
     * Setter for the totalItemQuantity property.
     * @param totalItemQuantity the new totalItemQuantity value
     */
    public void setTotalItemQuantity(int totalItemQuantity) {
        this.totalItemQuantity = totalItemQuantity;
    }



    /**
     * Getter for the tags property.
     * @return get the set of tags
     */
    public Set<Tag> getTags() {
        return tags;
    }


    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsLicense)) {
            return false;
        }
        final PtsLicense other = (PtsLicense) obj;
        if (getNumber() != other.getNumber()) {
            return false;
        }
        return true;
    }
    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     *
     * Method to add and associate a new put object with the license.
     *
     * @param newPut - the put to add to the license
     */
    public void addPut(PtsPut newPut) {
        this.getPuts().add(newPut);
        newPut.setLicense((PtsLicense) this);
    }


    /**
     * Getter for the putDetailCount property.
     * @return int value of the property
     */
    public int getPutDetailCount() {
        return putDetailCount;
    }


    /**
     * Setter for the putDetailCount property.
     * @param putDetailCount the new putDetailCount value
     */
    public void setPutDetailCount(int putDetailCount) {
        this.putDetailCount = putDetailCount;
    }


    /**
     * Getter for the labor property.
     * @return List PtsLicenseLabor value of the property
     */
    public List<PtsLicenseLabor> getLabor() {
        return labor;
    }


    /**
     * Setter for the labor property.
     * @param labor the new labor value
     */
    public void setLabor(List<PtsLicenseLabor> labor) {
        this.labor = labor;
    }


    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }


    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }


    /**
     * Getter for the purgeable property.
     * @return Integer value of the property
     */
    public Integer getPurgeable() {
        return purgeable;
    }

    /**
     * Setter for the purgeable property.
     * @param purgeable the new purgeable value
     */
    public void setPurgeable(Integer purgeable) {
        this.purgeable = purgeable;
    }

    /**
     * Method used to update model, starting license for
     * the specified operator.
     *
     * @param assignOperator - Operator to assign to assignment
     * @param assignStartTime - Time assignment is being started
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    public void startLicense(Operator assignOperator, Date assignStartTime)
    throws BusinessRuleException {

        if (!getStatus().isInSet(Available, Passed, Suspended)) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_NOT_AVAILABLE,
                new UserMessage("assignment.updateStatus.error.notAvailable"));
        }

        if (getStatus() == PtsLicenseStatus.InProgress && getOperator() != assignOperator) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_ALREADY_IN_PROGRESS,
                new UserMessage("assignment.updateStatus.error.inprogressByAnotherOperator"));
        }

        if (getGroupInfo().getGroupNumber() == null) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_NOT_IN_GROUP,
                new UserMessage("assignment.updateStatus.error.inprogressByAnotherOperator"));
        }

        processStatusChange(PtsLicenseStatus.InProgress);

        setOperator(assignOperator);
        if (getStartTime() == null) {
            setStartTime(assignStartTime);
        }
    }

    /**
     * Method to validate and update status change to the model object.
     * @param statusTo - status to change assignment to
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    protected void processStatusChange(PtsLicenseStatus statusTo) throws BusinessRuleException {

        switch (getStatus()) {
        case Available:
            checkStatusChangeFromAvailable(statusTo);
            break;
        case InProgress:
            checkStatusChangeFromInProgress(statusTo);
            break;
        case Suspended:
            checkStatusChangeFromSuspended(statusTo);
            break;
        case Passed:
            checkStatusChangeFromPassed(statusTo);
            break;
        default:
            checkStatusChangeFromOther(statusTo);
            break;
        }

        // Short any remaining picks
        //  shortRemainingPicks(statusTo);  --- EMS -- NOT SURE THIS IS NEEDED

        //Set status to what caller asked
        setStatus(statusTo);

    }

    /**
     * Method to check if status to be changed from Available to the specified status.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromAvailable(PtsLicenseStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Available, InProgress)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.LICENSE_INVALID_STATUS_CHANGE,
                new UserMessage("puttostore.license.updateStatus.error.invalidStatusChange",
                getStatus().name(), statusTo.name()));
        }
    }


    /**
     * Method to check if status to be changed from In-Progress to the specified status.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromInProgress(PtsLicenseStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, Passed, Suspended, InProgress)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.LICENSE_INVALID_STATUS_CHANGE,
                new UserMessage("puttostore.license.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }

        // Make sure all puts are completed
        if (statusTo == Complete) {
            for (PtsPut p : getPuts()) {
                if (p.getStatus().isInSet(NotPut, Skipped, Partial)) {
                    throw new BusinessRuleException(
                        PutToStoreErrorCode.ASSIGNMENT_PUT_NOT_COMPLETED,
                        new UserMessage("puttostore.license.updateStatus.error.putsNotCompleted"));
                }
            }
        }
    }


    /**
     * Method to check if status to be changed from Available to the specified status.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromSuspended(PtsLicenseStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(InProgress, Suspended, Complete)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.LICENSE_INVALID_STATUS_CHANGE,
                new UserMessage("puttostore.license.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }


    /**
     * Method to check if status to be changed from Passed to the specified status.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromPassed(PtsLicenseStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(InProgress, Passed, Complete)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.LICENSE_INVALID_STATUS_CHANGE,
                new UserMessage("puttostore.license.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }


    /**
     * Method to check if status to be changed from other statuses to
     * the specified status. This method was added to allow
     * for customization that require the addition
     * of a new status.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromOther(PtsLicenseStatus statusTo)
    throws BusinessRuleException {
        if (statusTo != getStatus()) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.LICENSE_INVALID_STATUS_CHANGE,
                new UserMessage("puttostore.license.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }


    /**
     * @param statusTo - Status to set assignment to
     * @param licenseEndTime - Time stopping assignment
     *
     * Method to update model and stop the assignment an operator was working on
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    public void stopAssignment(PtsLicenseStatus statusTo, Date licenseEndTime)
                                                throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, Passed, Suspended)) {
            throw new BusinessRuleException(PutToStoreErrorCode.LICENSE_INVALID_STOP_STATUS,
                new UserMessage("puttostore.license.updateStatus.error.invalidStopStatus"));
        }

        // Must be called before following updates to ensure put statuses get
        // set correctly
        processStatusChange(statusTo);
        adjustStatus(statusTo);

        // Final Processing
        if (getStatus().isInSet(Complete, Short)) {
            //Only set end time if it wasn't already set.
            if (getEndTime() == null) {
                setEndTime(licenseEndTime);
            }
        } else if (getStatus().isInSet(Passed)) {
            setOperator(null);
        }

    }

    /**
     * Method to adjust the status from what the caller specified when appropriate
     * Mainly used for when a user passes a group of assignments and 1 or more
     * assignments in that group had all puts completed, therefore the assignment will
     * be set to complete (or short) instead of passed.
     *
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    protected void adjustStatus(PtsLicenseStatus statusTo) throws BusinessRuleException {

        // complete passed assignments instead of setting them to passed
        if (statusTo.isInSet(Passed, Complete)) {
            int shortCount = getShortedCount();
            int notPutCount = getNotPutCount() + getPartialCount();
            int skipOrPartialCount = getSkipCount() + getPartialCount();

            if (statusTo == Passed && skipOrPartialCount > 0) {
                throw new BusinessRuleException(TaskErrorCode.PTS_LICENSE_INVALID_PASS_STATUS,
                    new UserMessage("puttostore.license.updateStatus.error.invalidPassStatus"));
            } else if (statusTo == Passed && notPutCount == 0 && shortCount == 0) {
                setStatus(Complete);
            } else if (statusTo == Passed && notPutCount == 0
                && shortCount > 0) {
                setStatus(Short);
            } else if (statusTo == Complete && shortCount > 0) {
                setStatus(Short);
            } else {
                setStatus(statusTo);
            }
        }
    }


    /**
     * Short the remaining puts for the given item.
     *
     * @param theItem - the item to short.
     * @throws BusinessRuleException - on bad status change.
     *
     */
    public void autoShortRemainingPuts(Item theItem) throws BusinessRuleException {

        for (PtsPut p : this.getPuts()) {
            if ((p.getItem() == theItem) && (p.getStatus().isInSet(NotPut, Partial, Skipped))) {
                PtsPutDetail detail = new PtsPutDetail();
                detail.setOperator(this.getOperator());
                detail.setType(PtsPutDetailType.AutoShort);
                detail.setPutTime(new Date());
                detail.setQuantityPut(0);
                // add the detail record to the put
                // do not add the detail to the container.
                p.addPutDetail(detail, false);
            }
        }
    }

    /**
     * Adjust the residual quantity for all puts for the given item.
     *
     * @param theItem - the item to adjust.
     * @param newResidualQuantity - set the residual quantity for the item to this value.
     */
    public void adjustResidualQuantityForPuts(Item theItem, int newResidualQuantity) {

        for (PtsPut p : this.getPuts()) {
            if (p.getItem() == theItem) {
                p.setResidualQuantity(newResidualQuantity);
            }
        }
    }

    /**
     * get number of puts that are not put.
     *
     * @return - number of puts that are not put.
     */
    public int getNotPutCount() {
        int count = 0;

        for (PtsPut put : getPuts()) {
            if (put.getStatus().isInSet(NotPut, Skipped)) {
                count = count + 1;
            }
        }
         return count;
    }


    /**
     * get number of put that are shorted.
     *
     * @return - number of puts that are not put.
     */
    public int getShortedCount() {
        int count = 0;

        for (PtsPut put : getPuts()) {
            if (put.getStatus().isInSet(Shorted)) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * Count the number of items that are shorted for the given item.
     * @param theItem - the item to count.
     * @return - the total number of items shorted for this item within the license.
     */
    public int getShortedCountForItem(Item theItem) {

        int shortedCount = 0;
        for (PtsPut p : this.getPuts()) {
            if ((p.getItem() == theItem) && (p.getStatus().isInSet(Shorted))) {
                shortedCount += p.getQuantityToPut() - p.getQuantityPut();
            }
        }

        return shortedCount;
    }

    /**
     * get number of puts that are shorted.
     *
     * @return - number of puts that are shorted.
     */
    public int getPartialCount() {
        int count = 0;

        for (PtsPut put : getPuts()) {
            if (put.getStatus().isInSet(Partial)) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * get number of puts that are skipped.
     *
     * @return - number of puts that are skipped.
     */
    public int getSkipCount() {
        int count = 0;

        for (PtsPut put : getPuts()) {
            if (put.getStatus().isInSet(Skipped)) {
                count = count + 1;
            }
        }

        return count;
    }

    /**
     * Returns a list of statuses that an user can set the license to when
     * editing the license from the UI.
     *
     * @return list of status operator can change this license to from GUI
     */
    public Map<Integer, String> getGUITransitionStatuses() {

        Map<Integer, String> statuses = new TreeMap<Integer, String>();
        switch (getStatus()) {
        case InProgress:
          statuses.put(new Integer(PtsLicenseStatus.InProgress.getValue()),
              ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.InProgress));
          statuses.put(new Integer(PtsLicenseStatus.Suspended.getValue()),
              ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Suspended));
            break;
        case Suspended:
          statuses.put(new Integer(PtsLicenseStatus.Suspended.getValue()),
              ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Suspended));
          statuses.put(new Integer(PtsLicenseStatus.Complete.getValue()),
              ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Complete));
            break;
        case Passed:
          statuses.put(new Integer(PtsLicenseStatus.Passed.getValue()),
              ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Passed));
          statuses.put(new Integer(PtsLicenseStatus.Complete.getValue()),
            ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Complete));
            break;
        default:
            statuses.put(new Integer(getStatus().getValue()),
                ResourceUtil.getLocalizedEnumName(getStatus()));
            break;
        }

        return statuses;
    }


    /**
     * Function to check for editing validation business logic.
     * @return boolean - whether license is editable or not.
     */
    public boolean isEditable() {
        return (!getStatus().isInSet(
            PtsLicenseStatus.Short, PtsLicenseStatus.Complete, PtsLicenseStatus.Available));
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getNumber();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 