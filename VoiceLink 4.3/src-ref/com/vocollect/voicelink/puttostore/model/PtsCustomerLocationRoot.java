/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Location;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

/**
 * Model object representing a VoiceLink Put To Store PtsCustomerLocation.
 *
 * @author mnichols
 */
public class PtsCustomerLocationRoot extends CommonModelObject implements Serializable, Taggable {

    private static final long serialVersionUID = 6480571401517467618L;

    private Customer            customerInfo;

    private Location            location;

    private String              route;

    private String              deliveryLocation;

    private Set<Tag>            tags;

    private Double              percentComplete = 0.0;

    /**
     * Getter for the customerInfo property.
     * @return Customer value of the property
     */
    public Customer getCustomerInfo() {
        if (this.customerInfo == null) {
            setCustomerInfo(new Customer());
        }
        return customerInfo;
    }


    /**
     * Setter for the customerInfo property.
     * @param customerInfo the new customerInfo value
     */
    public void setCustomerInfo(Customer customerInfo) {
        this.customerInfo = customerInfo;
    }


    /**
     * Getter for the location property.
     * @return Location value of the property
     */
    public Location getLocation() {
        return location;
    }


    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }


    /**
     * Getter for the route property.
     * @return String value of the property
     */
    public String getRoute() {
        return route;
    }


    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }


    /**
     * Getter for the deliveryLocation property.
     * @return String value of the property
     */
    public String getDeliveryLocation() {
        return deliveryLocation;
    }


    /**
     * Setter for the deliveryLocation property.
     * @param deliveryLocation the new deliveryLocation value
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }



    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsCustomerLocation)) {
            return false;
        }
        final PtsCustomerLocation other = (PtsCustomerLocation) obj;
        if (this.getCustomerInfo().getCustomerNumber() == null) {
            if (other.getCustomerInfo().getCustomerNumber() != null) {
                return false;
            }
        } else if (!this.getCustomerInfo().getCustomerNumber()
            .equals(other.getCustomerInfo().getCustomerNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((this.getCustomerInfo().getCustomerNumber() == null) ? 0
               : this.getCustomerInfo().getCustomerNumber().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getCustomerInfo().getCustomerNumber();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the percentComplete property.
     * @return Double value of the property
     */
    public Double getPercentComplete() {
        return percentComplete;
    }



    /**
     * Setter for the percentComplete property.
     * @param percentComplete the new percentComplete value
     */
    public void setPercentComplete(Double percentComplete) {
        this.percentComplete = (new BigDecimal(percentComplete)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
    }

    /**
     * Helper for the Import, allows us to access a field in the location that we use for a lookup.
     * @return the ID of the Location.
     */
     public String getLocationIdentifier() {
         if (location != null) {
             return (this.getLocation().getScannedVerification());
         }
         return null;
     }

     /**
      * Helper for the import, to access a field in the location that we use for lookup.
      * @param locationIdentifier the id for the field.
      */
     public void setLocationIdentifier(String locationIdentifier) {
         if (location != null) {
             location.setScannedVerification(locationIdentifier);
         } else {
             this.setLocation(new Location());
             location.setScannedVerification(locationIdentifier);
         }
     }

     /**
     * @return String - customer address
     */
    public String getCustomerAddress() {
         return this.getCustomerInfo().getCustomerAddress();
     }


     /**
      * Setter for the customerAddress property.
      * @param customerAddress the new customerAddress value
      */
     public void setCustomerAddress(String customerAddress) {
         this.getCustomerInfo().setCustomerAddress(customerAddress);
     }


     /**
      * Getter for the customerName property.
      * @return String value of the property
      */
     public String getCustomerName() {
         return this.getCustomerInfo().getCustomerName();
     }


     /**
      * Setter for the customerName property.
      * @param customerName the new customerName value
      */
     public void setCustomerName(String customerName) {
         this.getCustomerInfo().setCustomerName(customerName);
     }


     /**
      * Helper for the Import, allows us to access a field in the customerInfo that we use for a lookup.
      * @return String value of the customerInfo's customer number.
      */
     public String getCustomerNumber() {
         if (this.getCustomerInfo() != null) {
             return this.getCustomerInfo().getCustomerNumber();
         }
         return null;
     }


     /**
      * Helper for the import, to access a field in the customerInfo that we use for lookup.
      * @param customerNumber the customer number of the customerInfo to set.
      */
     public void setCustomerNumber(String customerNumber) {
         if (this.getCustomerInfo() != null) {
             this.getCustomerInfo().setCustomerNumber(customerNumber);
         }
     }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 