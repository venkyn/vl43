/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is a model for Container Details for put to store.
 *
 * @author mkoenig
 */
public class PtsContainerDetailRoot extends CommonModelObject implements Serializable {

    // Container puts went in
    private PtsContainer        container;

    //generated serial version id
    private static final long   serialVersionUID = -8896492781744866764L;

    // Operator that performed type
    private Operator            operator;

    // Time of Put
    private Date                putTime;

    // Quantity of Put
    private Integer             quantityPut = 0;

    // License Number
    private String              licenseNumber;

    //Put Type
    private PtsPutDetailType    type;

    //Item
    private Item                item;

    //Region
    private PtsRegion           region;

    // ExportStatus
    private ExportStatus        exportStatus = ExportStatus.NotExported;

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }


    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }


    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }


    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }


    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }


    /**
     * Getter for the container property.
     * @return PtsContainer value of the property
     */
    public PtsContainer getContainer() {
        return container;
    }


    /**
     * Setter for the container property.
     * @param container the new container value
     */
    public void setContainer(PtsContainer container) {
        this.container = container;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }


    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }


    /**
     * Getter for the type property.
     * @return PtsPutDetailType value of the property
     */
    public PtsPutDetailType getType() {
        return type;
    }


    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PtsPutDetailType type) {
        this.type = type;
    }


    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }


    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }


    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsContainerDetail)) {
            return false;
        }
        final PtsContainerDetail other = (PtsContainerDetail) obj;
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        if (getPutTime() == null) {
            if (other.getPutTime() != null) {
                return false;
            }
        } else if (!getPutTime().equals(other.getPutTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result
            + ((putTime == null) ? 0 : putTime.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return "" + this.getId();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 