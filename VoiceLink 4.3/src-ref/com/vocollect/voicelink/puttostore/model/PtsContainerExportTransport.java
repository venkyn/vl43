/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.ExportTransportObject;

import java.util.HashMap;


/**
 *
 * Pts Container Export.
 *
 * @author svoruganti
 */
public class PtsContainerExportTransport  extends ExportTransportObject {

       /**
         * Default Constructor.
         */
        public PtsContainerExportTransport() {
            // Nothing special
        }

        /**
         * @param fieldValues A collection of fieldValues
         * @param object new object
         */
        public PtsContainerExportTransport(HashMap<String, Object> fieldValues,
                Object object) {
            super(fieldValues, object);
            // Nothing special
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void updateExportToExported(Object obj) {
            PtsContainer container = (PtsContainer) obj;
            container.setExportStatus(ExportStatus.Exported);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void updateExportToInProgress(Object obj) {
            PtsContainer container = (PtsContainer) obj;
            container.setExportStatus(ExportStatus.InProgress);

        }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 