/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 *
 *
 * @author treed
 */
public class PtsLicenseLaborRoot extends CommonModelObject implements Serializable {

    //
    private static final long serialVersionUID = 3852243471092978431L;

    //  License labor detail belongs to
    private PtsLicense          license;

    //Operator that worked the period of time
    private Operator            operator;

    //Operators Labor Detail record license labor associated with.
    private OperatorLabor       operatorLabor;

    // The id of the operator break labor record that is associated with the license.
    // This property is populated if the operator takes a break in the middle of the
    // license.
    private Long                breakLaborId;

    //Time Operator started
    private Date                startTime;

    //Time Operator Ended
    private Date                endTime;

    //The difference between startTime and endTime in milliseconds
    private Long                duration;

    //Total quantity of items picked during period
    private Integer             quantityPut;

    //Assignment value used to prorate labor when picking a group
    private Integer             licenseProrateCount;

    //Group value used to prorate labor when picking a group
    private Integer             groupProrateCount;

    //Group Number
    private Long                groupNumber;

    //Group value used to prorate labor when picking a group
    private Integer             groupCount;

    // actual put rate for this license
    private Double              actualRate;

    // actual precent of goal
    private Double              percentOfGoal;

    //Was this exported yet?
    private ExportStatus    exportStatus = ExportStatus.NotExported;

    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        //TODO Rupert wants to remove these
        return new BigDecimal(this.actualRate)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }


    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }


    /**
     * Getter for the breakLaborId property.
     * @return Long value of the property
     */
    public Long getBreakLaborId() {
        return this.breakLaborId;
    }


    /**
     * Setter for the breakLaborId property.
     * @param breakLaborId the new breakLaborId value
     */
    public void setBreakLaborId(Long breakLaborId) {
        this.breakLaborId = breakLaborId;
    }


    /**
     * Getter for the duration property.
     * @return Long value of the property
     */
    public Long getDuration() {
        return this.duration;
    }


    /**
     * Setter for the duration property.
     * @param duration the new duration value
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }


    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return this.endTime;
    }


    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }


    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }


    /**
     * Getter for the groupCount property.
     * @return Integer value of the property
     */
    public Integer getGroupCount() {
        return this.groupCount;
    }


    /**
     * Setter for the groupCount property.
     * @param groupCount the new groupCount value
     */
    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }


    /**
     * Getter for the groupNumber property.
     * @return Long value of the property
     */
    public Long getGroupNumber() {
        return this.groupNumber;
    }


    /**
     * Setter for the groupNumber property.
     * @param groupNumber the new groupNumber value
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }


    /**
     * Getter for the groupProrateCount property.
     * @return Integer value of the property
     */
    public Integer getGroupProrateCount() {
        return this.groupProrateCount;
    }


    /**
     * Setter for the groupProrateCount property.
     * @param groupProrateCount the new groupProrateCount value
     */
    public void setGroupProrateCount(Integer groupProrateCount) {
        this.groupProrateCount = groupProrateCount;
    }


    /**
     * Getter for the license property.
     * @return PtsLicense value of the property
     */
    public PtsLicense getLicense() {
        return this.license;
    }


    /**
     * Setter for the license property.
     * @param license the new license value
     */
    public void setLicense(PtsLicense license) {
        this.license = license;
    }


    /**
     * Getter for the licenseProrateCount property.
     * @return Integer value of the property
     */
    public Integer getLicenseProrateCount() {
        return this.licenseProrateCount;
    }


    /**
     * Setter for the licenseProrateCount property.
     * @param licenseProrateCount the new licenseProrateCount value
     */
    public void setLicenseProrateCount(Integer licenseProrateCount) {
        this.licenseProrateCount = licenseProrateCount;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the operatorLabor property.
     * @return OperatorLabor value of the property
     */
    public OperatorLabor getOperatorLabor() {
        return this.operatorLabor;
    }


    /**
     * Setter for the operatorLabor property.
     * @param operatorLabor the new operatorLabor value
     */
    public void setOperatorLabor(OperatorLabor operatorLabor) {
        this.operatorLabor = operatorLabor;
    }


    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        //TODO Rupert wants to remove these
        return new BigDecimal(this.percentOfGoal)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }


    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }


    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return this.quantityPut;
    }


    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return this.startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsLicenseLabor)) {
            return false;
        }
        final PtsLicenseLabor other = (PtsLicenseLabor) obj;
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        if (getStartTime() == null) {
            if (other.getStartTime() != null) {
                return false;
            }
        } else if (!getStartTime().equals(other.getStartTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((startTime == null) ? 0
            : startTime.hashCode());
        return result;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 