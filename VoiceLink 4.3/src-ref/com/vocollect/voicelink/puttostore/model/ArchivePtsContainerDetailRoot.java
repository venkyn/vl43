/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;

import java.io.Serializable;
import java.util.Date;

/**
 * This class is a model for Container Details for put to store.
 *
 * @author mlashinsky
 */
public class ArchivePtsContainerDetailRoot extends CommonModelObject implements Serializable {

    //
    private static final long serialVersionUID = -5236339689420200464L;

    // Container puts went in
    private ArchivePtsContainer        archivePtsContainer;

    //Operator Information
    private String operatorIdentifier;
    private String operatorName;


    // Time of Put
    private Date                putTime;

    // Quantity of Put
    private Integer             quantityPut = 0;

    // License Number
    private String              licenseNumber;

    //Put Type
    private PtsPutDetailType    type;

    //Item
    private Item                item;

    //Region
    private PtsRegion           region;

    // ExportStatus
    private ExportStatus        exportStatus = ExportStatus.NotExported;

    /**
     *
     * Constructor.
     */
    public ArchivePtsContainerDetailRoot() {

    }

    /**
     *
     * Constructor.
     * @param archPtsContainer Archive Container to use to construct ArchiveContainerDetail
     * @param ptsContainerDetail Pts Container Detail used to construct a new Archive Pts Container Detail
     */
    public ArchivePtsContainerDetailRoot(PtsContainerDetail ptsContainerDetail, ArchivePtsContainer archPtsContainer) {
        this.setArchivePtsContainer(archPtsContainer);
        this.setCreatedDate(new Date());
        this.setExportStatus(archPtsContainer.getExportStatus());
        this.setItem(ptsContainerDetail.getItem());
        this.setLicenseNumber(ptsContainerDetail.getLicenseNumber());
        this.setOperatorIdentifier(ptsContainerDetail.getOperator().getOperatorIdentifier());
        this.setOperatorName(ptsContainerDetail.getOperator().getName());
        this.setPutTime(ptsContainerDetail.getPutTime());
        this.setQuantityPut(ptsContainerDetail.getQuantityPut());
        this.setRegion(ptsContainerDetail.getRegion());
        this.setType(ptsContainerDetail.getType());

    }


    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }


    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }


    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }


    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }


    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }


    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }


    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }


    /**
     * Getter for the type property.
     * @return PtsPutDetailType value of the property
     */
    public PtsPutDetailType getType() {
        return type;
    }


    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PtsPutDetailType type) {
        this.type = type;
    }


    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }


    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }


    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsContainerDetail)) {
            return false;
        }
        final PtsContainerDetail other = (PtsContainerDetail) obj;
        if (getOperatorIdentifier() == null) {
            if (other.getOperator().getOperatorIdentifier() != null) {
                return false;
            }
        } else if (!getOperatorIdentifier().equals(other.getOperator().getOperatorIdentifier())) {
            return false;
        }
        if (getPutTime() == null) {
            if (other.getPutTime() != null) {
                return false;
            }
        } else if (!getPutTime().equals(other.getPutTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((operatorIdentifier == null) ? 0 : operatorIdentifier.hashCode());
        result = prime * result
            + ((putTime == null) ? 0 : putTime.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return "" + this.getId();
    }


    /**
     * Getter for the archivePtsContainer property.
     * @return ArchivePtsContainer value of the property
     */
    public ArchivePtsContainer getArchivePtsContainer() {
        return archivePtsContainer;
    }


    /**
     * Setter for the archivePtsContainer property.
     * @param archivePtsContainer the new archivePtsContainer value
     */
    public void setArchivePtsContainer(ArchivePtsContainer archivePtsContainer) {
        this.archivePtsContainer = archivePtsContainer;
    }


    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }


    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }


    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }


    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 