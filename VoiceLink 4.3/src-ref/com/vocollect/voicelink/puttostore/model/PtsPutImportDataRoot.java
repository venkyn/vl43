/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Pts Put data to be imported.
 * @author svoruganti
 *
 */
public class PtsPutImportDataRoot extends BaseModelObject implements Importable {

    private static final long serialVersionUID = 8165379852395496566L;

    private ImportableImpl impl = new ImportableImpl();

    private PtsPut myModel = new PtsPut();

    private PtsLicenseImportData parent = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        myModel.setStatus(PtsPutStatus.NotPut);
        return myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getLicense()
     */
    public PtsLicense getLicense() {
        return myModel.getLicense();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * Gets the value of parent.
     * @return the parent
     */
    public PtsLicenseImportData getParent() {
        return parent;
    }

    /**
     * Sets the value of the parent.
     * @param parent the parent to set
     */
    public void setParent(PtsLicenseImportData parent) {
        this.parent = parent;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }

    /**
     * Set the item number for this PtsPut.
     * @param itemNumber the item number for this PtsPut.
     */
    public void setItemNumber(String itemNumber) {
        Item item = getItem();
        if (null == item) {
            item = new Item();
            setItem(item);
        }
        item.setNumber(itemNumber);
    }

    /**
     * Get the item number for this pick.
     * @return the item number for this pick.
     */
    public String getItemNumber() {
        Item item = getItem();
        if (null == item) {
            item = new Item();
            setItem(item);
        }
        return item.getNumber();
    }

    /**
     * Set the location number for this put.
     * @param locationNumber the location number for this put.
     */
    public void setLocationNumber(String locationNumber) {
        //custloc is this PtsPut's customerlocation
        PtsCustomerLocation custloc = getCustomer();
        if (null == custloc) {
            //If it was null, create new one and set it...
            custloc = new PtsCustomerLocation();
            this.setCustomer(custloc);
        }
        //location is this PtsPut's CustomerLocation's location
        Location location = getCustomer().getLocation();
        if (null == location) {
            //If it is null, create a new location and set it.
            location = new Location();
            getCustomer().setLocation(location);
        }
        location.setScannedVerification(locationNumber);
    }

    /**
     * Get the item number for this put.
     * @return the item number for this put.
     */
    public String getLocationNumber() {
        //custloc is this PtsPut's customerlocation
        PtsCustomerLocation custloc = getCustomer();
        if (null == custloc) {
            //If it was null, create new one and set it...
            custloc = new PtsCustomerLocation();
            this.setCustomer(custloc);
        }
        //location is this PtsPut's CustomerLocation's location
        Location location = getCustomer().getLocation();
        if (null == location) {
            //If it is null, create a new location and set it.
            location = new Location();
            getCustomer().setLocation(location);
        }
        return location.getScannedVerification();
    }

    /**
     * Set the customer number for this PtsPut.
     * @param customerNumber the customer number for this PtsPut.
     */
    public void setCustomerNumber(String customerNumber) {
        PtsCustomerLocation customer = getCustomer();
        if (null == customer) {
            customer = new PtsCustomerLocation();
            Customer cust = new Customer();
            customer.setCustomerInfo(cust);
            setCustomer(customer);
        }
        customer.getCustomerInfo().setCustomerNumber(customerNumber);
    }

    /**
     * Get the customer number for this put.
     * @return the customer number for this put.
     */
    public String getCustomerNumber() {
        return getCustomer().getCustomerInfo().getCustomerNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getContainerNumber()
     */
    public String getContainerNumber() {
        return myModel.getContainerNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getCustomer()
     */
    public PtsCustomerLocation getCustomer() {
        return myModel.getCustomer();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getOperator()
     */
    public Operator getOperator() {
        return myModel.getOperator();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getPutDetailCount()
     */
    public int getPutDetailCount() {
        return myModel.getPutDetailCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getPutDetails()
     */
    public List<PtsPutDetail> getPutDetails() {
        return myModel.getPutDetails();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getPutTime()
     */
    public Date getPutTime() {
        return myModel.getPutTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getQuantityPut()
     */
    public Integer getQuantityPut() {
        return myModel.getQuantityPut();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getQuantityToPut()
     */
    public Integer getQuantityToPut() {
        return myModel.getQuantityToPut();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getResidualQuantity()
     */
    public Integer getResidualQuantity() {
        return myModel.getResidualQuantity();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getSequenceNumber()
     */
    public Integer getSequenceNumber() {
        return myModel.getSequenceNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getStatus()
     */
    public PtsPutStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#getUnitOfMeasure()
     */
    public String getUnitOfMeasure() {
        return myModel.getUnitOfMeasure();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#isAllowOverPack()
     */
    public boolean isAllowOverPack() {
        return myModel.isAllowOverPack();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#isOverPacked()
     */
    public boolean isOverPacked() {
        return myModel.isOverPacked();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#reportPutSkipped()
     */
    public void reportPutSkipped() throws BusinessRuleException {
        myModel.reportPutSkipped();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setAllowOverPack(boolean)
     */
    public void setAllowOverPack(boolean allowOverPack) {
        myModel.setAllowOverPack(allowOverPack);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setContainerNumber(java.lang.String)
     */
    public void setContainerNumber(String containerNumber) {
        myModel.setContainerNumber(containerNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setCustomer(com.vocollect.voicelink.puttostore.model.PtsCustomerLocation)
     */
    public void setCustomer(PtsCustomerLocation customer) {
        myModel.setCustomer(customer);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setLicense(com.vocollect.voicelink.puttostore.model.PtsLicense)
     */
    public void setLicense(PtsLicense license) {
        myModel.setLicense(license);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public void setOperator(Operator operator) {
        myModel.setOperator(operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setPutAsNotPut()
     */
    public void setPutAsNotPut() throws BusinessRuleException {
        myModel.setPutAsNotPut();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setPutDetailCount(int)
     */
    public void setPutDetailCount(int putDetailCount) {
        myModel.setPutDetailCount(putDetailCount);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setPutDetails(java.util.List)
     */
    public void setPutDetails(List<PtsPutDetail> putDetails) {
        myModel.setPutDetails(putDetails);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setPutTime(java.util.Date)
     */
    public void setPutTime(Date putTime) {
        myModel.setPutTime(putTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setQuantityPut(java.lang.Integer)
     */
    public void setQuantityPut(Integer quantityPut) {
        myModel.setQuantityPut(quantityPut);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setQuantityToPut(java.lang.Integer)
     */
    public void setQuantityToPut(Integer quantityToPut) {
        myModel.setQuantityToPut(quantityToPut);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setResidualQuantity(java.lang.Integer)
     */
    public void setResidualQuantity(Integer residualQuantity) {
        myModel.setResidualQuantity(residualQuantity);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setSequenceNumber(java.lang.Integer)
     */
    public void setSequenceNumber(Integer sequence) {
        myModel.setSequenceNumber(sequence);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.puttostore.model.PtsPutRoot#setUnitOfMeasure(java.lang.String)
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        myModel.setUnitOfMeasure(unitOfMeasure);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 