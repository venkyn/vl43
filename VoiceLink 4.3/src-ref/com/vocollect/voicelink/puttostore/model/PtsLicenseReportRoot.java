/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;

    /**
     * Bean for PTS License report. This bean has all the variables defined
     * which will be displayed in the report output.
     *
     * @author kudupi
     */
    public class PtsLicenseReportRoot {

        /**
         * Constructor.
         * @param licenseNumber the license number
         * @param licenseStatus the status
         * @param containerNumber the container number
         * @param customerName the customer name
         * @param deliveryLocation the delivery location
         * @param route the route
         * @param putID the put id
         * @param operatorID the operator id
         * @param operatorName the operator name
         * @param quantityPut the quantity put
         * @param quantityToPut the quantity to put
         * @param residualQuantity the residual quantity
         * @param itemNumber the item number
         * @param itemDescription the item description
         * @param preAisle the pre-aisle
         * @param aisle the aisle
         * @param postAisle the post-aisle
         * @param slot the slot
         * @param putTime the put time
         * @param putStatus the put status
         */
        public PtsLicenseReportRoot(String licenseNumber,
                                    PtsLicenseStatus licenseStatus,
                                    String containerNumber,
                                    String customerName,
                                    String deliveryLocation,
                                    String route,
                                    Long putID,
                                    String operatorID,
                                    String operatorName,
                                    Integer quantityPut,
                                    Integer quantityToPut,
                                    Integer residualQuantity,
                                    String itemNumber,
                                    String itemDescription,
                                    String preAisle,
                                    String aisle,
                                    String postAisle,
                                    String slot,
                                    Date putTime,
                                    PtsPutStatus putStatus) {
            super();
            this.licenseNumber = licenseNumber;
            this.licenseStatus = licenseStatus;
            this.containerNumber = containerNumber;
            this.customerName = customerName;
            this.deliveryLocation = deliveryLocation;
            this.route = route;
            this.putID = putID;
            this.operatorID = operatorID;
            this.operatorName = operatorName;
            this.quantityPut = quantityPut;
            this.quantityToPut = quantityToPut;
            this.residualQuantity = residualQuantity;
            this.itemNumber = itemNumber;
            this.itemDescription = itemDescription;
            this.preAisle = preAisle;
            this.aisle = aisle;
            this.postAisle = postAisle;
            this.slot = slot;
            this.putTime = putTime;
            this.putStatus = putStatus;
        }

        private String licenseNumber;

        private PtsLicenseStatus licenseStatus;

        private String containerNumber;

        private String customerName;

        private String deliveryLocation;

        private String route;

        private Long putID;

        private String operatorID;

        private String operatorName;

        private Integer quantityPut;

        private Integer quantityToPut;

        private Integer residualQuantity;

        private String itemNumber;

        private String itemDescription;

        private String preAisle;

        private String aisle;

        private String postAisle;

        private String slot;

        private Date putTime;

        private PtsPutStatus putStatus;

        /**
         * Getter for licenseNumber.
         * @return licenseNumber of type String.
         */
        public String getLicenseNumber() {
            return licenseNumber;
        }

        /**
         * Setter for licenseNumber.
         * @param licenseNumber of type String.
         */
        public void setLicenseNumber(String licenseNumber) {
            this.licenseNumber = licenseNumber;
        }

        /**
         * Getter for licenseStatus.
         * @return licenseStatus of type PtsLicenseStatus.
         */
        public PtsLicenseStatus getLicenseStatus() {
            return licenseStatus;
        }

        /**
         * Setter for licenseStatus.
         * @param licenseStatus of type PtsLicenseStatus.
         */
        public void setLicenseStatus(PtsLicenseStatus licenseStatus) {
            this.licenseStatus = licenseStatus;
        }

        /**
         * Getter for containerNumber.
         * @return containerNumber of type String.
         */
        public String getContainerNumber() {
            return containerNumber;
        }

        /**
         * Setter for containerNumber.
         * @param containerNumber of type String.
         */
        public void setContainerNumber(String containerNumber) {
            this.containerNumber = containerNumber;
        }

        /**
         * Getter for customerName.
         * @return customerName of type String.
         */
        public String getCustomerName() {
            return customerName;
        }

        /**
         * Setter for customerName.
         * @param customerName of type String.
         */
        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        /**
         * Getter for deliveryLocation.
         * @return deliveryLocation of type String.
         */
        public String getDeliveryLocation() {
            return deliveryLocation;
        }

        /**
         * Setter for deliveryLocation.
         * @param deliveryLocation of type String.
         */
        public void setDeliveryLocation(String deliveryLocation) {
            this.deliveryLocation = deliveryLocation;
        }

        /**
         * Getter for route.
         * @return route of type String.
         */
        public String getRoute() {
            return route;
        }

        /**
         * Setter for route.
         * @param route of type String.
         */
        public void setRoute(String route) {
            this.route = route;
        }

        /**
         * Getter for putID.
         * @return putID of type Long.
         */
        public Long getPutID() {
            return putID;
        }

        /**
         * Setter for putID.
         * @param putID of type Long.
         */
        public void setPutID(Long putID) {
            this.putID = putID;
        }

        /**
         * Getter for operatorID.
         * @return operatorID of type String.
         */
        public String getOperatorID() {
            return operatorID;
        }

        /**
         * Setter for operatorID.
         * @param operatorID of type String.
         */
        public void setOperatorID(String operatorID) {
            this.operatorID = operatorID;
        }

        /**
         * Getter for operatorName.
         * @return operatorName of type String.
         */
        public String getOperatorName() {
            return operatorName;
        }

        /**
         * Setter for operatorName.
         * @param operatorName of type String.
         */
        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        /**
         * Getter for quantityPut.
         * @return quantityPut of type Integer.
         */
        public Integer getQuantityPut() {
            return quantityPut;
        }

        /**
         * Setter for quantityPut.
         * @param quantityPut of type Integer.
         */
        public void setQuantityPut(Integer quantityPut) {
            this.quantityPut = quantityPut;
        }

        /**
         * Getter for quantityToPut.
         * @return quantityToPut of type Integer.
         */
        public Integer getQuantityToPut() {
            return quantityToPut;
        }

        /**
         * Setter for quantityToPut.
         * @param quantityToPut of type Integer.
         */
        public void setQuantityToPut(Integer quantityToPut) {
            this.quantityToPut = quantityToPut;
        }

        /**
         * Getter for residualQuantity.
         * @return residualQuantity of type Integer.
         */
        public Integer getResidualQuantity() {
            return residualQuantity;
        }

        /**
         * Setter for residualQuantity.
         * @param residualQuantity of type Integer.
         */
        public void setResidualQuantity(Integer residualQuantity) {
            this.residualQuantity = residualQuantity;
        }

        /**
         * Getter for itemNumber.
         * @return itemNumber of type String.
         */
        public String getItemNumber() {
            return itemNumber;
        }

        /**
         * Setter for itemNumber.
         * @param itemNumber of type String.
         */
        public void setItemNumber(String itemNumber) {
            this.itemNumber = itemNumber;
        }

        /**
         * Getter for itemDescription.
         * @return itemDescription of type String.
         */
        public String getItemDescription() {
            return itemDescription;
        }

        /**
         * Setter for itemDescription.
         * @param itemDescription of type String.
         */
        public void setItemDescription(String itemDescription) {
            this.itemDescription = itemDescription;
        }

        /**
         * Getter for preAisle.
         * @return preAisle of type String.
         */
        public String getPreAisle() {
            return preAisle;
        }

        /**
         * Setter for preAisle.
         * @param preAisle of type String.
         */
        public void setPreAisle(String preAisle) {
            this.preAisle = preAisle;
        }

        /**
         * Getter for aisle.
         * @return aisle of type String.
         */
        public String getAisle() {
            return aisle;
        }

        /**
         * Setter for aisle.
         * @param aisle of type String.
         */
        public void setAisle(String aisle) {
            this.aisle = aisle;
        }

        /**
         * Getter for postAisle.
         * @return postAisle of type String.
         */
        public String getPostAisle() {
            return postAisle;
        }

        /**
         * Setter for postAisle.
         * @param postAisle of type String.
         */
        public void setPostAisle(String postAisle) {
            this.postAisle = postAisle;
        }

        /**
         * Getter for slot.
         * @return slot of type String.
         */
        public String getSlot() {
            return slot;
        }

        /**
         * Setter for slot.
         * @param slot of type String.
         */
        public void setSlot(String slot) {
            this.slot = slot;
        }

        /**
         * Getter for putTime.
         * @return putTime of type Date.
         */
        public Date getPutTime() {
            return putTime;
        }

        /**
         * Setter for putTime.
         * @param putTime of type Date.
         */
        public void setPutTime(Date putTime) {
            this.putTime = putTime;
        }

        /**
         * Getter for putStatus.
         * @return putStatus of type PtsPutStatus.
         */
        public PtsPutStatus getPutStatus() {
            return putStatus;
        }

        /**
         * Setter for putStatus.
         * @param putStatus of type PtsPutStatus.
         */
        public void setPutStatus(PtsPutStatus putStatus) {
            this.putStatus = putStatus;
        }

    }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 