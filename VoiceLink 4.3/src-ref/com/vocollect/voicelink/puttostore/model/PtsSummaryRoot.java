/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.io.Serializable;
import java.util.Set;


/**
 * Put To Store Model selection.
 *
 * @author svoruganti
 */
public class PtsSummaryRoot extends BaseModelObject implements
    Serializable, Taggable {

    //
    private static final long serialVersionUID = 6911731003170392525L;

    private PtsRegion         region;

    private Long                    id;

    private int                     totalLicenses;

    private int                     inProgress;

    private int                     available;

    private int                     complete;

    private int                     nonComplete;

    private int                     operatorsWorkingIn;

    private int                     operatorsAssigned;

    private int                     totalItemsRemaining;

    private int                     total;

    private String                  route;

    private int                     numberOfCustomers;

    private Double                  percentComplete;

    private int                     totalItems;

    private int                     itemsComplete;

    private int                     itemsRemaining;

    private Double                  estimatedCompleted;

    private Site                    site = null; // cache the site to improve performance


    /**
     * Getter for the estimatedCompleted property.
     * @return Double value of the property
     */
    public Double getEstimatedCompleted() {
        return estimatedCompleted;
    }


    /**
     * Setter for the estimatedCompleted property.
     * @param estimatedCompleted the new estimatedCompleted value
     */
    public void setEstimatedCompleted(Double estimatedCompleted) {
        this.estimatedCompleted = estimatedCompleted;
    }

    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return region.getTags();
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        //do nothing. Tags are derived from the region
    }



    /**
     * Getter for the operatorsAssigned property.
     * @return int value of the property
     */
    public int getOperatorsAssigned() {
        return operatorsAssigned;
    }



    /**
     * Setter for the operatorsAssigned property.
     * @param operatorsAssigned the new operatorsAssigned value
     */
    public void setOperatorsAssigned(int operatorsAssigned) {
        this.operatorsAssigned = operatorsAssigned;
    }



    /**
     * Getter for the operatorsWorkingIn property.
     * @return int value of the property
     */
    public int getOperatorsWorkingIn() {
        return operatorsWorkingIn;
    }



    /**
     * Setter for the operatorsWorkingIn property.
     * @param operatorsWorkingIn the new operatorsWorkingIn value
     */
    public void setOperatorsWorkingIn(int operatorsWorkingIn) {
        this.operatorsWorkingIn = operatorsWorkingIn;
    }


    /**
     * Getter for the totalLicenses property.
     * @return int value of the property
     */
    public int getTotalLicenses() {
        return totalLicenses;
    }



    /**
     * Setter for the totalLicenses property.
     * @param totalLicenses the new totalLicenses value
     */
    public void setTotalLicenses(int totalLicenses) {
        this.totalLicenses = totalLicenses;
    }


    /**
     * Getter for the totalItemsRemaining property.
     * @return int value of the property
     */
    public int getTotalItemsRemaining() {
        return totalItemsRemaining;
    }



    /**
     * Setter for the totalItemsRemaining property.
     * @param totalItemsRemaining the new totalItemsRemaining value
     */
    public void setTotalItemsRemaining(int totalItemsRemaining) {
        this.totalItemsRemaining = totalItemsRemaining;
    }

    /**
     * Getter for the available property.
     * @return int value of the property
     */
    public int getAvailable() {
        return available;
    }


    /**
     * Setter for the available property.
     * @param available the new available value
     */
    public void setAvailable(int available) {
        this.available = available;
    }


    /**
     * Getter for the complete property.
     * @return int value of the property
     */
    public int getComplete() {
        return complete;
    }


    /**
     * Setter for the complete property.
     * @param complete the new complete value
     */
    public void setComplete(int complete) {
        this.complete = complete;
    }


    /**
     * Getter for the inProgress property.
     * @return int value of the property
     */
    public int getInProgress() {
        return inProgress;
    }


    /**
     * Setter for the inProgress property.
     * @param inProgress the new inProgress value
     */
    public void setInProgress(int inProgress) {
        this.inProgress = inProgress;
    }


    /**
     * Getter for the nonComplete property.
     * @return int value of the property
     */
    public int getNonComplete() {
        return nonComplete;
    }


    /**
     * Setter for the nonComplete property.
     * @param nonComplete the new nonComplete value
     */
    public void setNonComplete(int nonComplete) {
        this.nonComplete = nonComplete;
    }


    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }


    /**
     * Getter for the site property.
     * @return Site value of the property
     * @throws DataAccessException - Database exceptions
     */
    public Site getSite() throws DataAccessException {
        if (this.site == null) {
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            this.site = siteContext.getSite(region);
        }
        return this.site;
    }

    /**
     * Setter for the site property.
     * @param newSite the new site
     */
    public void setSite(Site newSite) {
        this.site = newSite;
    }


    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    @Override
    public Long getId() {
        return id;
    }


    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof PtsSummaryRoot) {
            return false;
        }
        final PtsSummaryRoot other = (PtsSummaryRoot) obj;
        if (region == null) {
            if (other.region != null) {
                return false;
            }
        } else if (!region.equals(other.region)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        return result;
    }



    /**
     * Getter for the total property.
     * @return int value of the property
     */
    public int getTotal() {
        return total;
    }



    /**
     * Setter for the total property.
     * @param total the new total value
     */
    public void setTotal(int total) {
        this.total = total;
    }



    /**
     * Getter for the route property.
     * @return int value of the property
     */
    public String getRoute() {
        return route;
    }



    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }



    /**
     * Getter for the numberOfCustomers property.
     * @return int value of the property
     */
    public int getNumberOfCustomers() {
        return numberOfCustomers;
    }



    /**
     * Setter for the numberOfCustomers property.
     * @param numberOfCustomers the new numberOfCustomers value
     */
    public void setNumberOfCustomers(int numberOfCustomers) {
        this.numberOfCustomers = numberOfCustomers;
    }


    /**
     * Getter for the totalItems property.
     * @return int value of the property
     */
    public int getTotalItems() {
        return totalItems;
    }



    /**
     * Setter for the totalItems property.
     * @param totalItems the new totalItems value
     */
    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }



    /**
     * Getter for the itemsComplete property.
     * @return int value of the property
     */
    public int getItemsComplete() {
        return itemsComplete;
    }



    /**
     * Setter for the itemsComplete property.
     * @param itemsComplete the new itemsComplete value
     */
    public void setItemsComplete(int itemsComplete) {
        this.itemsComplete = itemsComplete;
    }



    /**
     * Getter for the itemsRemaining property.
     * @return int value of the property
     */
    public int getItemsRemaining() {
        return itemsRemaining;
    }



    /**
     * Setter for the itemsRemaining property.
     * @param itemsRemaining the new itemsRemaining value
     */
    public void setItemsRemaining(int itemsRemaining) {
        this.itemsRemaining = itemsRemaining;
    }



    /**
     * Setter for the percentComplete property.
     * @param percentComplete the new percentComplete value
     */
    public void setPercentComplete(Double percentComplete) {
        this.percentComplete = percentComplete;
    }



    /**
     * Getter for the percentComplete property.
     * @return Double value of the property
     */
    public Double getPercentComplete() {
        return percentComplete;
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 