/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.selection.model.AssignmentGroup;

import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.Available;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Model object representing a VoiceLink Archive Put To Store License object.
 *
 * @author mlashinsky
 */
public class ArchivePtsLicenseRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = -2752552040085766869L;

    private AssignmentGroup groupInfo;

    private PtsRegion region;

    private String number;

    private String partialNumber;

    private PtsLicenseStatus status = Available;

    private String operatorIdentifier;

    private String operatorName;

    private Date startTime;

    private Date endTime;

    private Integer expectedResiduals;

    private String reservedBy;

    // Total item quantity
    private int totalItemQuantity = 0;

    private int putDetailCount = 0;

    // Puts belonging to this License
    private List<ArchivePtsPut> archivePuts = new ArrayList<ArchivePtsPut>();

    // Labor records belonging to this PTS License (Added for archive purposes)
    private List<ArchivePtsLicenseLabor> archiveLabor = new ArrayList<ArchivePtsLicenseLabor>();

    // Archive PTS Containers that this Archive Put was put into
    private Set<ArchivePtsContainer> containers = new HashSet<ArchivePtsContainer>();

    private Site site;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    /**
     *
     * Constructor.
     */
    public ArchivePtsLicenseRoot() {

    }

    /**
     *
     * Constructor.
     * @param ptsLicense Assignment to use to build ArchivePtsLicense.
     */
    public ArchivePtsLicenseRoot(PtsLicense ptsLicense) {
        if (ptsLicense != null) {

            for (PtsLicenseLabor pl : ptsLicense.getLabor()) {
                this.getArchiveLabor().add(
                    new ArchivePtsLicenseLabor(pl, (ArchivePtsLicense) this));
            }
            for (PtsPut p : ptsLicense.getPuts()) {
                getArchivePuts().add(
                    new ArchivePtsPut(p, (ArchivePtsLicense) this));
            }

            for (Tag t : ptsLicense.getTags()) {
                if (t.getTagType().equals(Tag.SITE)) {
                    this.setSite(getSiteMap().get(t.getTaggableId()));
                    break;
                }
            }
            if (ptsLicense.getOperator() != null) {
                this.setOperatorIdentifier(ptsLicense.getOperator()
                    .getOperatorIdentifier());
                this.setOperatorName(ptsLicense.getOperator().getName());
            } else {
                this.setOperatorIdentifier(" ");
                this.setOperatorName(" ");
            }
            this.setCreatedDate(new Date());
            this.setEndTime(ptsLicense.getEndTime());
            this.setExpectedResiduals(ptsLicense.getExpectedResiduals());
            this.setGroupInfo(ptsLicense.getGroupInfo());
            this.setNumber(ptsLicense.getNumber());
            this.setPartialNumber(ptsLicense.getPartialNumber());
            this.setPutDetailCount(ptsLicense.getPutDetailCount());
            this.setArchivePuts(this.getArchivePuts());
            this.setRegion(ptsLicense.getRegion());
            this.setStartTime(ptsLicense.getStartTime());
            this.setStatus(ptsLicense.getStatus());
            this.setSite(this.getSite());
            this.setTotalItemQuantity(ptsLicense.getTotalItemQuantity());
        }
    }

    /**
     * Getter for the partialNumber property.
     * @return String value of the property
     */
    public String getPartialNumber() {
        return this.partialNumber;
    }

    /**
     * Setter for the partialNumber property.
     * @param partialNumber the new partialNumber value
     */
    public void setPartialNumber(String partialNumber) {
        this.partialNumber = partialNumber;
    }

    /**
     * Getter for the reservedBy property.
     * @return String value of the property
     */
    public String getReservedBy() {
        return this.reservedBy;
    }

    /**
     * Setter for the reservedBy property.
     * @param reservedBy the new reservedBy value
     */
    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    /**
     * Getter for the groupInfo property.
     * @return AssignmentGroup value of the property
     */
    public AssignmentGroup getGroupInfo() {
        return groupInfo;
    }

    /**
     * Setter for the groupInfo property.
     * @param groupInfo the new groupInfo value
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        this.groupInfo = groupInfo;
    }

    /**
     * Getter for the region property.
     * @return PtsRegion value of the property
     */
    public PtsRegion getRegion() {
        return region;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PtsRegion region) {
        this.region = region;
    }

    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }

    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * Getter for the status property.
     * @return PtsLicenseStatus value of the property
     */
    public PtsLicenseStatus getStatus() {
        return status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(PtsLicenseStatus status) {
        this.status = status;
    }

    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Getter for the expectedResiduals property.
     * @return Integer value of the property
     */
    public Integer getExpectedResiduals() {
        return expectedResiduals;
    }

    /**
     * Setter for the expectedResiduals property.
     * @param expectedResiduals the new expectedResiduals value
     */
    public void setExpectedResiduals(Integer expectedResiduals) {
        this.expectedResiduals = expectedResiduals;
    }

    /**
     * Getter for the totalItemQuantity property.
     * @return integer value of the property
     */
    public int getTotalItemQuantity() {
        return totalItemQuantity;
    }

    /**
     * Setter for the totalItemQuantity property.
     * @param totalItemQuantity the new totalItemQuantity value
     */
    public void setTotalItemQuantity(int totalItemQuantity) {
        this.totalItemQuantity = totalItemQuantity;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsLicense)) {
            return false;
        }
        final PtsLicense other = (PtsLicense) obj;
        if (getNumber() != other.getNumber()) {
            return false;
        }
        return true;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * Getter for the putDetailCount property.
     * @return integer value of the property
     */
    public int getPutDetailCount() {
        return putDetailCount;
    }

    /**
     * Setter for the putDetailCount property.
     * @param putDetailCount the new putDetailCount value
     */
    public void setPutDetailCount(int putDetailCount) {
        this.putDetailCount = putDetailCount;
    }

    /**
     * @return Site
     */
    public Site getSite() {
        return site;
    }

    /**
     * @param site - the site
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * @return Set<ArchivePtsContainer>
     */
    public Set<ArchivePtsContainer> getContainers() {
        return containers;
    }

    /**
     * @param containers - archive put to store container set
     */
    public void setContainers(Set<ArchivePtsContainer> containers) {
        this.containers = containers;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * Getter for the archivePuts property.
     * @return List of ArchivePtsPuts
     */
    public List<ArchivePtsPut> getArchivePuts() {
        return archivePuts;
    }

    /**
     * Setter for the archivePuts property.
     * @param archivePuts the new archivePuts value
     */
    public void setArchivePuts(List<ArchivePtsPut> archivePuts) {
        this.archivePuts = archivePuts;
    }

    /**
     * Getter for the archiveLabor property.
     * @return List of ArchivePtsPuts
     */
    public List<ArchivePtsLicenseLabor> getArchiveLabor() {
        return archiveLabor;
    }

    /**
     * Setter for the archiveLabor property.
     * @param archiveLabor the new archiveLabor value
     */
    public void setArchiveLabor(List<ArchivePtsLicenseLabor> archiveLabor) {
        this.archiveLabor = archiveLabor;
    }

    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 