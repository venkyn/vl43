/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;

/**
     * Bean for PTS license labor report. This bean has all the variables defined
     * which will be displayed in the report output.
     *
     * @author kudupi
     */
    public class PtsLicenseLaborReportRoot {

   /**
     * Constructor.
     * @param operatorName - the name of the operator
     * @param operatorId - the id of the operator
     * @param endTime - the report end time
     * @param startTime - the report start time
     * @param proRateCount - the pro rate count
     * @param percentOfGoal - percent of the goal
     * @param quantityPut - quantity put
     * @param groupNumber - the number of the group
     * @param groupCount - the count of the group
     * @param groupProRateCount - the count of the pro rate group
     * @param licenseNumber - the license number
     */
    public PtsLicenseLaborReportRoot(String operatorName,
                                     String operatorId,
                                     Date endTime,
                                     Date startTime,
                                     Integer proRateCount,
                                     Double percentOfGoal,
                                     Integer quantityPut,
                                     Long groupNumber,
                                     Integer groupCount,
                                     Integer groupProRateCount,
                                     String licenseNumber) {
        super();
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.endTime = endTime;
        this.startTime = startTime;
        this.proRateCount = proRateCount;
        this.percentOfGoal = percentOfGoal;
        this.quantityPut = quantityPut;
        this.groupNumber = groupNumber;
        this.groupCount = groupCount;
        this.groupProRateCount = groupProRateCount;
        this.licenseNumber = licenseNumber;
    }

        private String operatorName;

        private String operatorId;

        private Date endTime;

        private Date startTime;

        private Integer proRateCount;

        private Double percentOfGoal;

        private Integer quantityPut;

        private Long groupNumber;

        private Integer groupCount;

        private Integer groupProRateCount;

        private String licenseNumber;

        /**
         * Getter for Operator Name.
         * @return operatorName String.
         */
        public String getOperatorName() {
            return operatorName;
        }

        /**
         * Setter for Operator Name.
         * @param operatorName String.
         */
        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        /**
         * Getter for Operator ID.
         * @return operatorId String.
         */
        public String getOperatorId() {
            return operatorId;
        }

        /**
         * Setter for operator ID.
         * @param operatorId String.
         */
        public void setOperatorId(String operatorId) {
            this.operatorId = operatorId;
        }

        /**
         * Getter for endTime.
         * @return endTime Date.
         */
        public Date getEndTime() {
            return endTime;
        }

        /**
         * Setter for endTime.
         * @param endTime Date.
         */
        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        /**
         * Getter for startTime.
         * @return startTime Date.
         */
        public Date getStartTime() {
            return startTime;
        }

        /**
         * Setter for startTime.
         * @param startTime Date.
         */
        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        /**
         * Getter for Pro Rate Count.
         * @return proRateCount Integer.
         */
        public Integer getProRateCount() {
            return proRateCount;
        }

        /**
         * Setter for Pro Rate Count.
         * @param proRateCount Integer.
         */
        public void setProRateCount(Integer proRateCount) {
            this.proRateCount = proRateCount;
        }

        /**
         * Getter for Percent Of Goal.
         * @return percentOfGoal Double.
         */
        public Double getPercentOfGoal() {
            return percentOfGoal;
        }

        /**
         * Setter for Percent Of Goal.
         * @param percentOfGoal Double.
         */
        public void setPercentOfGoal(Double percentOfGoal) {
            this.percentOfGoal = percentOfGoal;
        }

        /**
         * Getter for QuantityPut.
         * @return quantityPut Integer.
         */
        public Integer getQuantityPut() {
            return quantityPut;
        }

        /**
         * Setter for QuantityPut.
         * @param quantityPut Integer.
         */
        public void setQuantityPut(Integer quantityPut) {
            this.quantityPut = quantityPut;
        }

        /**
         * Getter for GroupNumber.
         * @return groupNumber Long.
         */
        public Long getGroupNumber() {
            return groupNumber;
        }

        /**
         * Setter for GroupNumber.
         * @param groupNumber Long.
         */
        public void setGroupNumber(Long groupNumber) {
            this.groupNumber = groupNumber;
        }

        /**
         * Getter for GroupCount.
         * @return groupCount Integer.
         */
        public Integer getGroupCount() {
            return groupCount;
        }

        /**
         * Setter for GroupCount.
         * @param groupCount Integer.
         */
        public void setGroupCount(Integer groupCount) {
            this.groupCount = groupCount;
        }

        /**
         * Getter for Group Pro Rate Count.
         * @return groupProRateCount Integer.
         */
        public Integer getGroupProRateCount() {
            return groupProRateCount;
        }

        /**
         * Setter for Group Pro Rate Count.
         * @param groupProRateCount Integer.
         */
        public void setGroupProRateCount(Integer groupProRateCount) {
            this.groupProRateCount = groupProRateCount;
        }

        /**
         * Getter for LicenseNumber.
         * @return licenseNumber String.
         */
        public String getLicenseNumber() {
            return licenseNumber;
        }

        /**
         * Setter for LicenseNumber.
         * @param licenseNumber String.
         */
        public void setLicenseNumber(String licenseNumber) {
            this.licenseNumber = licenseNumber;
        }

    }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 