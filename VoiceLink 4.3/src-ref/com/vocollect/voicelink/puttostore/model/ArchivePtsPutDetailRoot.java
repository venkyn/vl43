/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.Date;

/**
 * Model object representing a VoiceLink Archive Put To Store Put Details
 * Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsPutDetailRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = 2596930040054602312L;

    // WMS Put the details is associated with
    private ArchivePtsPut archivePut;

    // Action that occurred to generate detail record
    private PtsPutDetailType type;

    // Operator that performed type
    //private Operator operator;
    private String operatorName;
    private String operatorIdentifier;


    // Time of Put
    private Date putTime;

    // Quantity of Put
    private Integer quantityPut = 0;

    //Container Number
    private String containerNumber;

    // ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    /**
     *
     * Constructor.
     */
    public ArchivePtsPutDetailRoot() {

    }

    /**
     *
     * Constructor.
     * @param pd PtsPutDetail used to construct an ArchivePtsPutDetail object.
     * @param archivePtsPut ArchivePtsPut used to construct an
     *            ArchivePtsPutDetail object.
     */
    public ArchivePtsPutDetailRoot(PtsPutDetail pd, ArchivePtsPut archivePtsPut) {

        if (pd.getOperator() != null) {
            this.setOperatorIdentifier(pd.getOperator().getOperatorIdentifier());
            this.setOperatorName(pd.getOperator().getName());
        } else {
            this.setOperatorIdentifier(" ");
            this.setOperatorName(" ");
        }
        this.setArchivePut(archivePtsPut);
        this.setCreatedDate(new Date());
        this.setExportStatus(pd.getExportStatus());
        this.setPutTime(pd.getPutTime());
        this.setQuantityPut(pd.getQuantityPut());
        this.setType(pd.getType());
        this.setContainerNumber(pd.getContainerNumber());

    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the put property.
     * @return Put value of the property
     */
    public ArchivePtsPut getArchivePut() {
        return archivePut;
    }

    /**
     * Setter for the put property.
     * @param archivePut the new put value
     */
    public void setArchivePut(ArchivePtsPut archivePut) {
        this.archivePut = archivePut;
    }

    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }

    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }

    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }

    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }
//
//    /**
//     * Getter for the operator property.
//     * @return Operator value of the property
//     */
//    public Operator getOperator() {
//        return this.operator;
//    }
//
//    /**
//     * Setter for the operator property.
//     * @param operator the new operator value
//     */
//    public void setOperator(Operator operator) {
//        this.operator = operator;
//    }

    /**
     * Getter for the type property.
     * @return PtsPutDetailType value of the property
     */
    public PtsPutDetailType getType() {
        return type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PtsPutDetailType type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ArchivePtsPutDetail)) {
            return false;
        }
        final ArchivePtsPutDetail other = (ArchivePtsPutDetail) obj;
        if (getOperatorIdentifier() == null) {
            if (other.getOperatorIdentifier() != null) {
                return false;
            }
        } else if (!getOperatorIdentifier().equals(other.getOperatorIdentifier())) {
            return false;
        }
        if (getPutTime() == null) {
            if (other.getPutTime() != null) {
                return false;
            }
        } else if (!getPutTime().equals(other.getPutTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((operatorIdentifier == null) ? 0 : operatorIdentifier.hashCode());
        result = prime * result + ((putTime == null) ? 0 : putTime.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return "" + this.getId();
    }


    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return containerNumber;
    }


    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }


    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }


    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }


    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }


    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 