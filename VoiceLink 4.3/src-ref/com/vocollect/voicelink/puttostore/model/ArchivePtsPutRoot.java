/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.Item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model object representing a VoiceLink Archive Put To Store Put object.
 *
 * @author mlashinsky
 */
public class ArchivePtsPutRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = -4438874429901264755L;

    private ArchivePtsLicense archiveLicense;

    // Operator that last put the item.
    private String operatorIdentifier;

    private String operatorName;

    private Integer sequenceNumber;

    private Integer quantityToPut = 0;

    private Integer quantityPut = 0;

    private Integer residualQuantity = 0;

    private boolean allowOverPack = false;

    private Date putTime;

    private String unitOfMeasure = "";

    private PtsPutStatus status = PtsPutStatus.NotPut;

    // Customer Information.
    private String customerLocationPreAisle;

    private String customerLocationAisle;

    private String customerLocationPostAisle;

    private String customerLocationSlot;

    private String customerLocationDeliveryLocation;

    private Double customerLocationPercentComplete = 0.0;

    private String customerNumber;

    private String customerName;

    private String customerRoute;

    private String customerAddress;

    private Item item;

    // Count of put detail information available
    private int archivePutDetailCount = 0;

    // Put Details are actions performed on put
    private List<ArchivePtsPutDetail> archivePutDetails = new ArrayList<ArchivePtsPutDetail>();

    // Site the puts belong to
    private Site site;

    /**
     *
     * Constructor.
     */
    public ArchivePtsPutRoot() {

    }

    /**
     *
     * Constructor.
     * @param put Put used to construct ArchivePtsPut.
     * @param archivePtsLicense ArchivePtsLicense used to construct an
     *            ArchivePtsPut object.
     */
    public ArchivePtsPutRoot(PtsPut put, ArchivePtsLicense archivePtsLicense) {
        for (PtsPutDetail pd : put.getPutDetails()) {
            getArchivePutDetails().add(
                new ArchivePtsPutDetail(pd, (ArchivePtsPut) this));
        }

        for (Tag t : put.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }
        if (put.getOperator() != null) {
            this.setOperatorIdentifier(put.getOperator()
                .getOperatorIdentifier());
            this.setOperatorName(put.getOperator().getName());
        } else {
            this.setOperatorIdentifier(" ");
            this.setOperatorName(" ");
        }
        this.setCreatedDate(new Date());
        this.setArchiveLicense(archivePtsLicense);
        this.setArchivePutDetailCount(put.getPutDetailCount());
        this.setArchivePutDetails(this.getArchivePutDetails());

        //Customer Information Flattened
        this.setCustomerLocationDeliveryLocation(put.getCustomer()
            .getDeliveryLocation());
        this.setCustomerLocationPreAisle(put.getCustomer().getLocation()
            .getPreAisle());
        this.setCustomerLocationAisle(put.getCustomer().getLocation()
            .getAisle());
        this.setCustomerLocationPostAisle(put.getCustomer().getLocation()
            .getPostAisle());
        this.setCustomerLocationSlot(put.getCustomer().getLocation().getSlot());
        this.setCustomerLocationPercentComplete(put.getCustomer()
            .getPercentComplete());
        this.setCustomerNumber(put.getCustomer().getCustomerNumber());
        this.setCustomerName(put.getCustomer().getCustomerName());
        this.setCustomerRoute(put.getCustomer().getRoute());
        this.setCustomerAddress(put.getCustomer().getCustomerAddress());

        this.setItem(put.getItem());
        this.setPutTime(put.getPutTime());
        this.setQuantityPut(put.getQuantityPut());
        this.setQuantityToPut(put.getQuantityToPut());
        this.setResidualQuantity(put.getResidualQuantity());
        this.setSequenceNumber(put.getSequenceNumber());
        this.setStatus(put.getStatus());
        this.setUnitOfMeasure(put.getUnitOfMeasure());
    }

    /**
     * Getter for the ptsLicense property.
     * @return PtsLicense value of the property
     */
    public ArchivePtsLicense getLicense() {
        return archiveLicense;
    }

    /**
     * Setter for the archivePtsLicense property.
     * @param archivePtsLicense the new ptsLicense value
     */
    public void setLicense(ArchivePtsLicense archivePtsLicense) {
        this.archiveLicense = archivePtsLicense;
    }

    /**
     * @return the sequence
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequenceNumber(Integer sequence) {
        this.sequenceNumber = sequence;
    }

    /**
     * Setter for the quantityToPut property.
     * @param quantityToPut the new quantityToPut value
     */
    public void setQuantityToPut(Integer quantityToPut) {
        this.quantityToPut = quantityToPut;
    }

    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }

    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }

    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Getter for the quantityToPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityToPut() {
        return quantityToPut;
    }

    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }

    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }

    /**
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * @param unitOfMeasure the unitOfMeasure to set
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    protected void setStatus(PtsPutStatus status) {
        this.status = status;
    }

    /**
     * Getter for the status property.
     * @return PtsPutStatus value of the property
     */
    public PtsPutStatus getStatus() {
        return status;
    }

    /**
     * Getter for the archivePutDetailCount property.
     * @return integer value of the property
     */
    public int getArchivePutDetailCount() {
        return archivePutDetailCount;
    }

    /**
     * Setter for the archivePutDetailCount property.
     * @param archivePutDetailCount the new putDetailCount value
     */
    public void setArchivePutDetailCount(int archivePutDetailCount) {
        this.archivePutDetailCount = archivePutDetailCount;
    }

    /**
     * Getter for the archivePutDetails property.
     * @return List of ArchivePutDetails
     */
    public List<ArchivePtsPutDetail> getArchivePutDetails() {
        return archivePutDetails;
    }

    /**
     * Setter for the archivePutDetails property.
     * @param archivePutDetails - the new putArchiveDetails value
     */
    public void setArchivePutDetails(List<ArchivePtsPutDetail> archivePutDetails) {
        this.archivePutDetails = archivePutDetails;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getItem().getDescriptiveText();
    }

    /**
     * @return the residualQuantity
     */
    public Integer getResidualQuantity() {
        return residualQuantity;
    }

    /**
     * @param residualQuantity the residualQuantity to set
     */
    public void setResidualQuantity(Integer residualQuantity) {
        this.residualQuantity = residualQuantity;
    }

    /**
     * @return the allowOverPack
     */
    public boolean isAllowOverPack() {
        return allowOverPack;
    }

    /**
     * @param allowOverPack the allowOverPack to set
     */
    public void setAllowOverPack(boolean allowOverPack) {
        this.allowOverPack = allowOverPack;
    }

    /**
     * determines if the put has been over packed.
     *
     * @return true if the put was over packed otherwise return false.
     */
    public boolean isOverPacked() {

        return (this.getQuantityPut() > this.getQuantityToPut());
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsPut)) {
            return false;
        }
        final PtsPut other = (PtsPut) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * @return ArchivePtsLicense
     */
    public ArchivePtsLicense getArchiveLicense() {
        return archiveLicense;
    }

    /**
     * @param archiveLicense - the archive put to store license
     */
    public void setArchiveLicense(ArchivePtsLicense archiveLicense) {
        this.archiveLicense = archiveLicense;
    }

    /**
     * @return Site
     */
    public Site getSite() {
        return site;
    }

    /**
     * Setter for the Site property.
     * @param site the Site for this put.
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * Getter for the customerLocationPreAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationPreAisle() {
        return customerLocationPreAisle;
    }

    /**
     * Setter for the customerLocationPreAisle property.
     * @param customerLocationPreAisle the new customerLocationPreAisle value
     */
    public void setCustomerLocationPreAisle(String customerLocationPreAisle) {
        this.customerLocationPreAisle = customerLocationPreAisle;
    }

    /**
     * Getter for the customerLocationAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationAisle() {
        return customerLocationAisle;
    }

    /**
     * Setter for the customerLocationAisle property.
     * @param customerLocationAisle the new customerLocationAisle value
     */
    public void setCustomerLocationAisle(String customerLocationAisle) {
        this.customerLocationAisle = customerLocationAisle;
    }

    /**
     * Getter for the customerLocationPostAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationPostAisle() {
        return customerLocationPostAisle;
    }

    /**
     * Setter for the customerLocationPostAisle property.
     * @param customerLocationPostAisle the new customerLocationPostAisle value
     */
    public void setCustomerLocationPostAisle(String customerLocationPostAisle) {
        this.customerLocationPostAisle = customerLocationPostAisle;
    }

    /**
     * Getter for the customerLocationSlot property.
     * @return String value of the property
     */
    public String getCustomerLocationSlot() {
        return customerLocationSlot;
    }

    /**
     * Setter for the customerLocationSlot property.
     * @param customerLocationSlot the new customerLocationSlot value
     */
    public void setCustomerLocationSlot(String customerLocationSlot) {
        this.customerLocationSlot = customerLocationSlot;
    }

    /**
     * Getter for the customerLocationDeliveryLocation property.
     * @return String value of the property
     */
    public String getCustomerLocationDeliveryLocation() {
        return customerLocationDeliveryLocation;
    }

    /**
     * Setter for the customerLocationDeliveryLocation property.
     * @param customerLocationDeliveryLocation the new customerLocationDeliveryLocation value
     */
    public void setCustomerLocationDeliveryLocation(String customerLocationDeliveryLocation) {
        this.customerLocationDeliveryLocation = customerLocationDeliveryLocation;
    }

    /**
     * Getter for the customerLocationPercentComplete property.
     * @return Double value of the property
     */
    public Double getCustomerLocationPercentComplete() {
        return customerLocationPercentComplete;
    }

    /**
     * Setter for the customerLocationPercentComplete property.
     * @param customerLocationPercentComplete the new customerLocationPercentComplete value
     */
    public void setCustomerLocationPercentComplete(Double customerLocationPercentComplete) {
        this.customerLocationPercentComplete = customerLocationPercentComplete;
    }

    /**
     * Getter for the customerNumber property.
     * @return String value of the property
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Setter for the customerNumber property.
     * @param customerNumber the new customerNumber value
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * Getter for the customerName property.
     * @return String value of the property
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Setter for the customerName property.
     * @param customerName the new customerName value
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Getter for the customerRoute property.
     * @return String value of the property
     */
    public String getCustomerRoute() {
        return customerRoute;
    }

    /**
     * Setter for the customerRoute property.
     * @param customerRoute the new customerRoute value
     */
    public void setCustomerRoute(String customerRoute) {
        this.customerRoute = customerRoute;
    }

    /**
     * Getter for the customerAddress property.
     * @return String value of the property
     */
    public String getCustomerAddress() {
        return customerAddress;
    }

    /**
     * Setter for the customerAddress property.
     * @param customerAddress the new customerAddress value
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 