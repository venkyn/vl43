/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.puttostore.PutToStoreErrorCode;

import static com.vocollect.voicelink.puttostore.model.PtsLicenseStatus.InProgress;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.NotPut;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Partial;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Put;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Shorted;
import static com.vocollect.voicelink.puttostore.model.PtsPutStatus.Skipped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Model object representing a VoiceLink Put To Store Put.
 *
 * @author mnichols
 */
public class PtsPutRoot extends CommonModelObject implements Serializable, Taggable {

    private static final long serialVersionUID = 1784505344034702757L;

    private PtsLicense              license;

    // Operator that last put the item.
    // PutDetails have all operator that put parts of the put
    private Operator                operator;

    private Integer                 sequenceNumber;

    private Integer                 quantityToPut = 0;

    private Integer                 quantityPut = 0;

    private Integer                 residualQuantity = 0;

    private boolean                 allowOverPack = false;

    private Date                    putTime;

    private String                  unitOfMeasure = "";

    private PtsPutStatus            status = PtsPutStatus.NotPut;

    private PtsCustomerLocation     customer;

    private Item                    item;

    private String                  containerNumber = "";

    private Set<Tag>                tags;

    //Count of put detail information available
    private int                     putDetailCount = 0;

    // Put Details are actions performed on put
    private List<PtsPutDetail>      putDetails;


    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }



    /**
     * Getter for the ptsLicense property.
     * @return PtsLicense value of the property
     */
    public PtsLicense getLicense() {
        return license;
    }

    /**
     * Setter for the ptsLicense property.
     * @param license the new ptsLicense value
     */
    public void setLicense(PtsLicense license) {
        this.license = license;
    }

    /**
     * @return the sequence
     */
    public Integer getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequenceNumber(Integer sequence) {
        this.sequenceNumber = sequence;
    }

    /**
     * Getter for the tags property.
     * @return Set - value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }


    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    /**
     * Getter for the customer property.
     * @return PtsCustomerLocation value of the property
     */
    public PtsCustomerLocation getCustomer() {
        return customer;
    }


    /**
     * Setter for the customer property.
     * @param customer the new customer value
     */
    public void setCustomer(PtsCustomerLocation customer) {
        this.customer = customer;
    }


    /**
     * Setter for the quantityToPut property.
     * @param quantityToPut the new quantityToPut value
     */
    public void setQuantityToPut(Integer quantityToPut) {
        this.quantityToPut = quantityToPut;
    }


    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }


    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }


    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }


    /**
     * Getter for the quantityToPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityToPut() {
        return quantityToPut;
    }


    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }


    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }


    /**
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * @param unitOfMeasure the unitOfMeasure to set
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    protected void setStatus(PtsPutStatus status) {
        this.status = status;
    }


    /**
     * Getter for the status property.
     * @return PtsPutStatus value of the property
     */
    public PtsPutStatus getStatus() {
        return status;
    }


    /**
     * Getter for the putDetailCount property.
     * @return int value of the property
     */
    public int getPutDetailCount() {
        return putDetailCount;
    }


    /**
     * Setter for the putDetailCount property.
     * @param putDetailCount the new putDetailCount value
     */
    public void setPutDetailCount(int putDetailCount) {
        this.putDetailCount = putDetailCount;
    }


    /**
     * Getter for the putDetails property.
     * @return List of PutDetails
     */
    public List<PtsPutDetail> getPutDetails() {
        return putDetails;
    }


    /**
     * Setter for the putDetails property.
     * @param putDetails - the new putDetails value
     */
    public void setPutDetails(List<PtsPutDetail> putDetails) {
        this.putDetails = putDetails;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getItem().getDescriptiveText();
    }


    /**
     * @return the residualQuantity
     */
    public Integer getResidualQuantity() {
        return residualQuantity;
    }

    /**
     * @param residualQuantity the residualQuantity to set
     */
    public void setResidualQuantity(Integer residualQuantity) {
        this.residualQuantity = residualQuantity;
    }

    /**
     * @return the allowOverPack
     */
    public boolean isAllowOverPack() {
        return allowOverPack;
    }

    /**
     * @param allowOverPack the allowOverPack to set
     */
    public void setAllowOverPack(boolean allowOverPack) {
        this.allowOverPack = allowOverPack;
    }

    /** determines if the put has been over packed.
     *
     * @return true if the put was over packed otherwise return false.
     */
    public boolean isOverPacked() {

        return (this.getQuantityPut() > this.getQuantityToPut());
    }

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        List<String> containers = new ArrayList<String>();

        for (PtsPutDetail pd : getPutDetails()) {
            if (!StringUtil.isNullOrBlank(pd.getContainerNumber())) {
                if (!containers.contains(pd.getContainerNumber())) {
                    containers.add(pd.getContainerNumber());
                }
            }
        }

        if (containers.size() > 1) {
            return ("puttostore.container.multiple.link");
        } else if (containers.size() == 1) {
            return containers.get(0);
        }

        return containerNumber;
    }


    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }


    /**
     * Sets a picks status to skipped.
     * processStatusChange will validate if change is valid.
     * This method would typically be called from the UpdateStatus ODR.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void reportPutSkipped() throws BusinessRuleException {
        // Call the private method to change status
        processStatusChange(Skipped);
    }

    /**
     * Sets a picks status to not picked.
     * processStatusChange will validate if change is valid.
     * This method would typically be called from the UpdateStatus ODR.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void setPutAsNotPut() throws BusinessRuleException {
        // Call the private method to change status
        processStatusChange(NotPut);
    }

    /**
     * This is the main method for changing the status of a put.
     * It enforces that the requested status change is valid.
     * This method has been broken down to smaller methods to allow for easier customization.
     * The methods called internally by this method should not be called directly.
     *
     * @param statusTo - status attempting to change to

     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void processStatusChange(PtsPutStatus statusTo) throws BusinessRuleException {

        // Checks for Valid Status changes
        switch (getStatus()) {
        // Not Picked and Skipped have same processing
        case NotPut:
            checkStatusChangeFromNotPut(statusTo);
            break;
        case Skipped:
            checkStatusChangeFromSkipped(statusTo);
            break;
        // Put is a final status and cannot be changed
        case Shorted:
            checkStatusChangeFromShorted(statusTo);
            break;
        case Partial:
            checkStatusChangeFromPartial(statusTo);
            break;
        // Put is a final status and cannot be changed
        case Put:
            checkStatusChangeFromPut(statusTo);
            break;
         // Default is basically to throw error of invalid status change, although
        // we will allow setting status to be same as current status
        default:
            checkStatusChangeFromOther(statusTo);
            break;
        }

        // Update Put Status
        this.setStatus(statusTo);

     }

    /**
     * This method determines how the put status should be set for the put.
     * It determines the status based on the quantity put.
     * After determining the status, the method calls processStatusChange to
     * verify that the put status can be changed to the requested status.
     *
     * @param partial - is a partial put in progress?
     *        if true, a partial put is in progress.
     *        if false, a partial put is not in progress.
     *
     * @throws BusinessRuleException - on status change failure.
     */
    protected void updatePutStatus(boolean partial) throws BusinessRuleException {

        // grab these values here for efficiency.
        int pQuantityPut = this.getQuantityPut();
        int pQuantityToPut = this.getQuantityToPut();
        // Determine if we can change the put status.
        if (partial && this.getLicense().getStatus() == InProgress) {
            this.processStatusChange(Partial);
        } else {
            if (pQuantityPut < pQuantityToPut) {
                this.processStatusChange(Shorted);
            } else if (pQuantityPut >= pQuantityToPut) {
                  this.processStatusChange(Put);
            }
        }
    }


    /**
     * Add a put detail to the put record.
     *
     * @param putDetail - the put detail to add to the put
     * @param partial - determines if this is a partial detail.
     *        true - the put is being partial'ed.
     *        false - the put is not being partial'ed.
     *
     * @throws BusinessRuleException - on invalid status change
     */
    public void addPutDetail(PtsPutDetail putDetail, boolean partial)
                                throws BusinessRuleException {

        // Cannot add details if put is completed (Put or Shorted)
        // NOTE: we may be trying to add a second detail record if the put status is Put
        //       and the put detail type is TaskOverPack
        if (this.getStatus() == PtsPutStatus.Shorted) {
            throw new BusinessRuleException(PutToStoreErrorCode.PUT_ALREADY_COMPLETE,
                    new UserMessage("puttostore.put.updateStatus.error.putComplete"));
        } else if ((this.getStatus() == PtsPutStatus.Put) && (putDetail.getType() != PtsPutDetailType.TaskOverPack)) {
            throw new BusinessRuleException(PutToStoreErrorCode.PUT_ALREADY_COMPLETE,
            new UserMessage("puttostore.put.updateStatus.error.putComplete"));
        }
        // update the quantity put.
        this.setQuantityPut(this.getQuantityPut() + putDetail.getQuantityPut());

        putDetail.setPut((PtsPut) this);
        this.setOperator(putDetail.getOperator());
        this.setPutTime(putDetail.getPutTime());
        this.getPutDetails().add(putDetail);
        this.setPutDetailCount(this.getPutDetailCount() + 1);
        this.getLicense().setPutDetailCount(this.getLicense().getPutDetailCount() + 1);
        // If the PutDetailType is TaskOverPack, we do not need to update the put status.
        // The put will have been already marked as put.
        if (putDetail.getType() != PtsPutDetailType.TaskOverPack) {
            this.updatePutStatus(partial);
        }
        // if put was shorted auto short the remaining puts for the item
        // if the put was over packed, adjust the residual quantity by the
        // amount over packed.
        if (this.getStatus() == Shorted) {
            this.getLicense().autoShortRemainingPuts(this.getItem());
        } else if (this.isOverPacked()) {
            int newResidualQuantity =
                this.getResidualQuantity() - (this.getQuantityPut() - this.getQuantityToPut());
            this.getLicense().adjustResidualQuantityForPuts(this.getItem(), newResidualQuantity);
        }
    }


    /**
     * Validates the status changing to, is valid from current not pick status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromNotPut(PtsPutStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Put, Skipped, Partial, Shorted)) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.PUT_INVALID_STATUS_CHANGE,
                new UserMessage("put.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }

    /**
     * Validates the status changing to, is valid from current Skipped status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromSkipped(PtsPutStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(NotPut, Put, Skipped, Shorted)) {
             throw new BusinessRuleException(
                 PutToStoreErrorCode.PUT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }


    /**
     * Validates the status changing to, is valid from current shorted status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromShorted(PtsPutStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }

    /**
     * Validates the status changing to, is valid from current shorted status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromOverPacked(PtsPutStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }


    /**
     * Validates the status changing to, is valid from current partial status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromPartial(PtsPutStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Put, Shorted, Partial)) {
             throw new BusinessRuleException(
                 PutToStoreErrorCode.PUT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }

    /**
     * Validates the status changing to, is valid from current Picked status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromPut(PtsPutStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }

    /**
     * called from above methods always error to change status from final state.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromFinalState(PtsPutStatus statusTo)
    throws BusinessRuleException {
         throw new BusinessRuleException(
            PutToStoreErrorCode.PUT_INVALID_STATUS_CHANGE,
            new UserMessage("put.updateStatus.error.invalidStatusChange",
                getStatus().name(), statusTo.name()));
    }

    /**
     * Hook put in for check against a custom status that may be added to the system
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromOther(PtsPutStatus statusTo)
    throws BusinessRuleException {
        if (statusTo != getStatus()) {
            throw new BusinessRuleException(
                PutToStoreErrorCode.PUT_INVALID_STATUS_CHANGE,
                new UserMessage("put.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }



    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsPut)) {
            return false;
        }
        final PtsPut other = (PtsPut) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }


    /**
     * This is a helper method for the importer.
     * Returns this objects item's number or null if item is null.
     * @return - returns the Item number of this object's item.
     */
    public String getItemNumber() {
        if (this.getItem() == null) {
            return null;
        }
        return this.getItem().getNumber();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 