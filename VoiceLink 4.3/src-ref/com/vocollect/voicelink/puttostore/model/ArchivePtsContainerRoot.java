/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model object representing a VoiceLink Put To Store Archive Container Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsContainerRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = -3657499873087189673L;

    // container number supplied by task or system generated
    private String containerNumber = "";

    // Customer Information.
    private String customerLocationPreAisle;
    private String customerLocationAisle;
    private String customerLocationPostAisle;
    private String customerLocationSlot;
    private String customerLocationDeliveryLocation;
    private Double customerLocationPercentComplete = 0.0;
    private String customerNumber;
    private String customerName;
    private String customerRoute;
    private String customerAddress;


    // Has container been exported
    private ExportStatus exportStatus = ExportStatus.NotExported;

    // Status of the PTS container
    private PtsContainerStatus status = PtsContainerStatus.Open;

    //Container Details for this container
    private List<ArchivePtsContainerDetail> archiveContainerDetails;

    // Site to which Containers belong.
    private Site site;


    /**
     *
     * Constructor.
     */
    public ArchivePtsContainerRoot() {

    }

    /**
     *
     * Constructor.
     * @param ptsContainer Container to use to construct ArchiveContainer
     */
    public ArchivePtsContainerRoot(PtsContainer ptsContainer) {

        //Customer Information Flattened
        this.setCustomerLocationDeliveryLocation(ptsContainer.getCustomer().getDeliveryLocation());
        this.setCustomerLocationPreAisle(ptsContainer.getCustomer().getLocation().getPreAisle());
        this.setCustomerLocationAisle(ptsContainer.getCustomer().getLocation().getAisle());
        this.setCustomerLocationPostAisle(ptsContainer.getCustomer().getLocation().getPostAisle());
        this.setCustomerLocationSlot(ptsContainer.getCustomer().getLocation().getSlot());
        this.setCustomerLocationPercentComplete(ptsContainer.getCustomer().getPercentComplete());
        this.setCustomerNumber(ptsContainer.getCustomer().getCustomerNumber());
        this.setCustomerName(ptsContainer.getCustomer().getCustomerName());
        this.setCustomerRoute(ptsContainer.getCustomer().getRoute());
        this.setCustomerAddress(ptsContainer.getCustomer().getCustomerAddress());

        this.setContainerNumber(ptsContainer.getContainerNumber());
        this.setCreatedDate(new Date());

        //this probably isn't need since only exported containers are archived but left in for customizers
        this.setExportStatus(ptsContainer.getExportStatus());
        this.setStatus(ptsContainer.getStatus());

        this.setArchiveContainerDetails(this.getArchiveContainerDetails());

        for (Tag t : ptsContainer.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }

        for (PtsContainerDetail cd : ptsContainer.getContainerDetails()) {
            getArchiveContainerDetails().add(new ArchivePtsContainerDetail(cd, (ArchivePtsContainer) this));
        }

    }

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }

    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsContainer)) {
            return false;
        }
        final PtsContainer other = (PtsContainer) obj;
        if (getContainerNumber() == null) {
            if (other.getContainerNumber() != null) {
                return false;
            }
        } else if (!getContainerNumber().equals(other.getContainerNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((containerNumber == null) ? 0 : containerNumber.hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getContainerNumber();
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * Getter for the status property.
     * @return PtsContainerStatus value of the property
     */
    public PtsContainerStatus getStatus() {
        return status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(PtsContainerStatus status) {
        this.status = status;
    }

    /**
     * Getter for the putDetails property.
     * @return List - List of ArchivePtsContainerDetails
     */
    public List<ArchivePtsContainerDetail> getArchiveContainerDetails() {
        if (archiveContainerDetails == null) {
            archiveContainerDetails = new ArrayList<ArchivePtsContainerDetail>();
        }

        return archiveContainerDetails;
    }

    /**
     * @param archiveContainerDetails - archive put to store container details list
     */
    public void setArchiveContainerDetails(List<ArchivePtsContainerDetail> archiveContainerDetails) {
        this.archiveContainerDetails = archiveContainerDetails;
    }

    /**
     * @return Site
     */
    public Site getSite() {
        return site;
    }

    /**
     * @param site - the site
     */
    public void setSite(Site site) {
        this.site = site;
    }


    /**
     * Getter for the percentComplete property.
     * @return Double value of the property
     */
    public Double getCustomerLocationPercentComplete() {
        return customerLocationPercentComplete;
    }


    /**
     * Setter for the percentComplete property.
     * @param percentComplete the new percentComplete value
     */
    public void setCustomerLocationPercentComplete(Double percentComplete) {
        this.customerLocationPercentComplete = percentComplete;
    }


    /**
     * Getter for the customerNumber property.
     * @return String value of the property
     */
    public String getCustomerNumber() {
        return customerNumber;
    }


    /**
     * Setter for the customerNumber property.
     * @param customerNumber the new customerNumber value
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }


    /**
     * Getter for the customerName property.
     * @return String value of the property
     */
    public String getCustomerName() {
        return customerName;
    }


    /**
     * Setter for the customerName property.
     * @param customerName the new customerName value
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    /**
     * Getter for the customerAddress property.
     * @return String value of the property
     */
    public String getCustomerAddress() {
        return customerAddress;
    }


    /**
     * Setter for the customerAddress property.
     * @param customerAddress the new customerAddress value
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }


    /**
     * Getter for the customerLocationPreAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationPreAisle() {
        return customerLocationPreAisle;
    }


    /**
     * Setter for the customerLocationPreAisle property.
     * @param customerLocationPreAisle the new customerLocationPreAisle value
     */
    public void setCustomerLocationPreAisle(String customerLocationPreAisle) {
        this.customerLocationPreAisle = customerLocationPreAisle;
    }


    /**
     * Getter for the customerLocationAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationAisle() {
        return customerLocationAisle;
    }


    /**
     * Setter for the customerLocationAisle property.
     * @param customerLocationAisle the new customerLocationAisle value
     */
    public void setCustomerLocationAisle(String customerLocationAisle) {
        this.customerLocationAisle = customerLocationAisle;
    }


    /**
     * Getter for the customerLocationPostAisle property.
     * @return String value of the property
     */
    public String getCustomerLocationPostAisle() {
        return customerLocationPostAisle;
    }


    /**
     * Setter for the customerLocationPostAisle property.
     * @param customerLocationPostAisle the new customerLocationPostAisle value
     */
    public void setCustomerLocationPostAisle(String customerLocationPostAisle) {
        this.customerLocationPostAisle = customerLocationPostAisle;
    }


    /**
     * Getter for the customerLocationSlot property.
     * @return String value of the property
     */
    public String getCustomerLocationSlot() {
        return customerLocationSlot;
    }


    /**
     * Setter for the customerLocationSlot property.
     * @param customerLocationSlot the new customerLocationSlot value
     */
    public void setCustomerLocationSlot(String customerLocationSlot) {
        this.customerLocationSlot = customerLocationSlot;
    }


    /**
     * Getter for the customerRoute property.
     * @return String value of the property
     */
    public String getCustomerRoute() {
        return customerRoute;
    }


    /**
     * Setter for the customerRoute property.
     * @param customerRoute the new customerRoute value
     */
    public void setCustomerRoute(String customerRoute) {
        this.customerRoute = customerRoute;
    }


    /**
     * Setter for the customerLocationDeliveryLocation property.
     * @param customerLocationDeliveryLocation the new customerLocationDeliveryLocation value
     */
    public void setCustomerLocationDeliveryLocation(String customerLocationDeliveryLocation) {
        this.customerLocationDeliveryLocation = customerLocationDeliveryLocation;
    }


    /**
     * Getter for the customerLocationDeliveryLocation property.
     * @return String value of the property
     */
    public String getCustomerLocationDeliveryLocation() {
        return customerLocationDeliveryLocation;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 