/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 *  This is the root container model for Put To Store.
 *
 * @author jtauberg
 */
public class PtsContainerRoot extends CommonModelObject implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -3657499873087189673L;

    //container number supplied by task or system generated
    private String                  containerNumber = "";

    //This defines the PtsCustomerLocation that this container was packed for.
    private PtsCustomerLocation     customer;

    //Has container been exported
    private ExportStatus            exportStatus = ExportStatus.NotExported;

    //Status of the PTS container
    private PtsContainerStatus      status = PtsContainerStatus.Open;

    //Put Detail records put into this container
    private List<PtsContainerDetail>      containerDetails;

    private Integer purgeable = 0;

    private Set<Tag> tags;

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }


    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PtsContainer)) {
            return false;
        }
        final PtsContainer other = (PtsContainer) obj;
        if (getContainerNumber() == null) {
            if (other.getContainerNumber() != null) {
                return false;
            }
        } else if (!getContainerNumber().equals(other.getContainerNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((containerNumber == null) ? 0 : containerNumber.hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getContainerNumber();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the customer property.
     * @return PtsCustomerLocation value of the property
     */
    public PtsCustomerLocation getCustomer() {
        return customer;
    }



    /**
     * Setter for the customer property.
     * @param customer the new customer value
     */
    public void setCustomer(PtsCustomerLocation customer) {
        this.customer = customer;
    }



    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }



    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }



    /**
     * Getter for the status property.
     * @return PtsContainerStatus value of the property
     */
    public PtsContainerStatus getStatus() {
        return status;
    }



    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(PtsContainerStatus status) {
        this.status = status;
    }



    /**
     * @return - container details
     */
    public List<PtsContainerDetail> getContainerDetails() {
        if (containerDetails == null) {
            containerDetails = new ArrayList<PtsContainerDetail>();
        }

        return containerDetails;
    }



    /**
     * @param containerDetails - the container details list
     */
    public void setContainerDetails(List<PtsContainerDetail> containerDetails) {
        this.containerDetails = containerDetails;
    }


    /**
     * Add the put detail record to the container.
     * Add the container to the put detail record.
     *
     * @param detail - the detail record to add.
     *
     */
    public void addPutDetail(PtsPutDetail detail) {
        PtsContainerDetail cd = new PtsContainerDetail();

        cd.setContainer((PtsContainer) this);
        cd.setItem(detail.getPut().getItem());
        cd.setLicenseNumber(detail.getPut().getLicense().getNumber());
        cd.setOperator(detail.getOperator());
        cd.setPutTime(detail.getPutTime());
        cd.setQuantityPut(detail.getQuantityPut());
        cd.setRegion(detail.getPut().getLicense().getRegion());
        cd.setType(detail.getType());

        this.getContainerDetails().add(cd);
    }



    /**
     * Getter for the purgeable property.
     * @return Integer value of the property
     */
    public Integer getPurgeable() {
        return purgeable;
    }



    /**
     * Setter for the purgeable property.
     * @param purgeable the new purgeable value
     */
    public void setPurgeable(Integer purgeable) {
        this.purgeable = purgeable;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 