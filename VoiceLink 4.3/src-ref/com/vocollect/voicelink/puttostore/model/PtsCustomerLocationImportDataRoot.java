/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Location;

/**
 * Image of an Customer Location that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the
 * tool to do it. Additionally, any methods we need to override exist in the body.
 *
 * @author mnichols
 *
 */
public class PtsCustomerLocationImportDataRoot extends BaseModelObject implements
    Importable {

    //
    private static final long serialVersionUID = 5197709489615125926L;

    /**
     * Default constructor.
     */
    public PtsCustomerLocationImportDataRoot() {
        super();
    }

    private ImportableImpl impl = new ImportableImpl();

    private PtsCustomerLocation myModel = new PtsCustomerLocation();

    /**
     * {@inheritDoc}
     * @return
     */
    public Object convertToModel() {
        return this.getMyModel();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long customerID) {
        impl.setImportID(customerID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * Sets the value of the myModel.
     * @param myModel the myModel to set
     */
    public void setMyModel(PtsCustomerLocation myModel) {
        this.myModel = myModel;
    }

    /**
     * Gets the value of myModel.
     * @return the myModel
     */
    public PtsCustomerLocation getMyModel() {
        return myModel;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * @see com.vocollect.epp.model.DataObjectRoot#getId()
     * @return import id
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getCustomerInfo()
     */
    public Customer getCustomerInfo() {
        return myModel.getCustomerInfo();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getRoute()
     */
    public String getRoute() {
        return myModel.getRoute();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getDeliveryLocation()
     */
    public String getDeliveryLocation() {
        return myModel.getDeliveryLocation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setDeliveryLocation(java.lang.String)
     */
    public void setDeliveryLocation(String deliveryLocation) {
        myModel.setDeliveryLocation(deliveryLocation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setRoute(java.lang.String)
     */
    public void setRoute(String route) {
        myModel.setRoute(route);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }

    /**
     * Set the Location id (scannedVerification) in the Location.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the Location.scannedVerification to.
     */
    public void setLocationId(String scannedVerification) {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }

    /**
     * Get the Location's id (scannedVerification).
     * @return the Location ID (scannedVerification) (String).
     */
    public String getLocationId() {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            return null;
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setCustomerInfo(com.vocollect.voicelink.core.model.Customer)
     */
    public void setCustomerInfo(Customer customerInfo) {
        myModel.setCustomerInfo(customerInfo);
    }

    /**
     * Set the customer number.
     * @param number cust number
     */
    public void setCustomerNumber(String number) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerNumber(number);
    }

    /**
     * Set the customer address.
     * @param address customer address
     */
    public void setCustomerAddress(String address) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerAddress(address);
    }

    /**
     * Set the customer name.
     * @param name customer name
     */
    public void setCustomerName(String name) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerName(name);
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 