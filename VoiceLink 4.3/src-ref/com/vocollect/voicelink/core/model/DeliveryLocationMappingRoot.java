/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.Set;

/**
 * Model object for a DeliveryLocationMapping. This represents a rule that can
 * determine for assignments that had no delivery location specified on import
 * which delivery location to send the operator to.
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingRoot extends CommonModelObject implements
    Taggable {

    /**
     * Generated SUID.
     */
    private static final long serialVersionUID = -4496013547527128933L;

    /**
     * The value used to determine the mapping. Note, what the value is depends
     * on the DeliveryLocationMappingSetting (e.g. Customer, Route, etc.).
     */
    private String mappingValue;

    /**
     * Delivery Location value that this field will be mapped to.
     */
    private String deliveryLocation;

    /**
     * The type of delivery location mapping (e.g. Customer number, route, etc.)
     */
    private DeliveryLocationMappingType mappingType;

    /**
     * A list of tags for this mapping (used for Multi-Site management).
     */
    private Set<Tag> tags;

    /**
     * A prime number for the hashCode function - here to avoid Checkstyle
     * errors.
     */
    private static final int PRIME = 31;

    /**
     * Default constructor.
     */
    public DeliveryLocationMappingRoot() {
        // default, do nothing
    }

    /**
     * Convenience constructor.
     * @param mappingValue - the value (e.g. customer number, route, etc.)
     * @param deliveryLocation - the id of the deliveryLocation route, etc.).
     * @param mappingType - the mapping type (e.g. customer number, route, etc.)
     */
    public DeliveryLocationMappingRoot(String mappingValue,
                                       String deliveryLocation,
                                       DeliveryLocationMappingType mappingType) {
        this.mappingValue = mappingValue;
        this.deliveryLocation = deliveryLocation;
        this.mappingType = mappingType;
    }

    /**
     * Getter for the mappingValue property.
     * @return String value of the property
     */
    public String getMappingValue() {
        return this.mappingValue;
    }

    /**
     * Setter for the mappingValue property.
     * @param mappingValue the new mappingValue value
     */
    public void setMappingValue(String mappingValue) {
        this.mappingValue = mappingValue;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        int result = 1;
        result = PRIME * result
            + ((deliveryLocation == null) ? 0 : deliveryLocation.hashCode());
        result = PRIME * result
            + ((mappingType == null) ? 0 : mappingType.hashCode());
        result = PRIME * result
            + ((mappingValue == null) ? 0 : mappingValue.hashCode());
        result = PRIME * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DeliveryLocationMappingRoot other = (DeliveryLocationMappingRoot) obj;
        if (deliveryLocation == null) {
            if (other.deliveryLocation != null) {
                return false;
            }
        } else if (!deliveryLocation.equals(other.deliveryLocation)) {
            return false;
        }
        if (mappingType == null) {
            if (other.mappingType != null) {
                return false;
            }
        } else if (!mappingType.equals(other.mappingType)) {
            return false;
        }
        if (mappingValue == null) {
            if (other.mappingValue != null) {
                return false;
            }
        } else if (!mappingValue.equals(other.mappingValue)) {
            return false;
        }
        if (tags == null) {
            if (other.tags != null) {
                return false;
            }
        } else if (!tags.equals(other.tags)) {
            return false;
        }
        return true;
    }

    /**
     * Getter for the deliveryLocation property.
     * @return String value of the property
     */
    public String getDeliveryLocation() {
        return this.deliveryLocation;
    }

    /**
     * Setter for the deliveryLocation property.
     * @param deliveryLocation the new deliveryLocation value
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Getter for the mappingType property.
     * @return DeliveryLocationMappingType value of the property
     */
    public DeliveryLocationMappingType getMappingType() {
        return this.mappingType;
    }

    /**
     * Setter for the mappingType property.
     * @param mappingType the new mappingType value
     */
    public void setMappingType(DeliveryLocationMappingType mappingType) {
        this.mappingType = mappingType;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 