/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ddoubleday
 */
public class LocationRoot extends CommonModelObject implements Serializable, Taggable {
    
   
    private static final long serialVersionUID = -8849363432093508670L;

    //check digits user must speak to verify location
    public static final String  BLANK_CHECK_DIGIT = "     ";
    
    private String              checkDigits;

    // The description that comprise the location description.
    private LocationDescription description;

    //string operator can scan to specify location
    private String              scannedVerification;

    //string operator can speak in task to specify location
    private String              spokenVerification;

    //current state of location
    private LocationStatus      status = LocationStatus.Replenished;
    
    private Set<Tag>            tags;
    
    private Set<LocationItem>   items;
    
    private Set<Lot>            lots;

    private int                 lotCount = 0;
    
    
    /**
     * Getter for the items property.
     * @return Set&lt;LocationItem&gt; value of the property
     */
    public Set<LocationItem> getItems() {
        if (items == null) {
            items = new HashSet<LocationItem>();
        }
        return items;
    }

    
    /**
     * Setter for the items property.
     * @param items the new items value
     */
    public void setItems(Set<LocationItem> items) {
        this.items = items;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Location)) {
            return false;
        }
        final Location other = (Location) obj;
        if (getScannedVerification() == null) {
            if (other.getScannedVerification() != null) {
                return false;
            }
        } else if (!getScannedVerification().
                equals(other.getScannedVerification())) {
            return false;
        }
        return true;
    }


    /**
     * Getter for the description property.
     * @return LocationDescription value of the property
     */
    public LocationDescription getDescription() {
      if (this.description != null) {
        return this.description;
      } else {
        return new LocationDescription();
      }
    }

    /**
     * Getter for the scannedVerification property.
     * @return String value of the property
     */
    public String getScannedVerification() {
        return this.scannedVerification;
    }

    /**
     * Getter for the spokenVerification property.
     * @return String value of the property
     */
    public String getSpokenVerification() {
        return this.spokenVerification;
    }

    /**
     * Getter for the replenishIndicator property.
     * @return int value of the property
     */
    public LocationStatus getStatus() {
        return this.status;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.scannedVerification == null
            ? 0 : this.scannedVerification.hashCode();
    }

    
    /**
     * Getter for the checkDigits property.
     * If the check digit value is 5 blank spaces or if the value is null,
     * we will return an empty string.  Otherwise, we will return the trimmed
     * check digit value.
     * @return String value of the property
     */
    public String getCheckDigits() {
        if (this.checkDigits != null) {
            return this.checkDigits.trim();
        } else {
            return "";
        }
    }
    
    
    /**
     * Setter for the checkDigits property.
     * 
     * If an empty/blank string is passed for the check digit value, 
     * store the value as 5 blank spaces in the database so we can keep
     * the underlying database consistent.  We need to store the spaces
     * because Oracle does not differentiate between a empty string and null.
     * Oracle stores empty strings as null and SQL server 
     * stores them as an empty string.
     * 
     * @param checkDigits the new checkDigits value
     */
    public void setCheckDigits(String checkDigits) {
        
        /* Furthermore, if we choose to store null value to remain consistent
         * with oracle, we run into other issues with database queries and 
         * with import validation.
         */
        if (checkDigits == null) {
            this.checkDigits = BLANK_CHECK_DIGIT;
        } else if (checkDigits.trim().length() == 0) {
            this.checkDigits = BLANK_CHECK_DIGIT;
        } else {
            this.checkDigits = checkDigits;
        }
    }

    /**
     * Setter for the description property.
     * @param components the new description value
     */
    public void setDescription(LocationDescription components) {
        this.description = components;
    }

    /**
     * Setter for the scannedVerification property.
     * @param scannedVerification the new scannedVerification value
     */
    public void setScannedVerification(String scannedVerification) {
        this.scannedVerification = scannedVerification;
    }

    /**
     * Setter for the spokenVerification property.
     * @param spokenVerification the new spokenVerification value
     */
    public void setSpokenVerification(String spokenVerification) {
        this.spokenVerification = spokenVerification;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(LocationStatus status) {
        this.status = status;
    }

    /**
     * @param item the item in question
     * @return the status of the item at this location.
     */
    public LocationStatus getStatus(Item item) {
        
        if (getItems().size() == 0) {
            //No items so return location's status
            return getStatus();
        } else {
            //See if item in list and return item's status
            for (LocationItem li : getItems()) {
                if (li.getItem().equals(item)) {
                    return li.getStatus();
                }
            }
        }

        //No status found so return Unknown.
        return LocationStatus.Unknown;
    }

    /**
     * set status for a particular item.
     * 
     * @param theStatus - Status to set to
     * @param item - item to set for
     */
    public void setStatus(LocationStatus theStatus, Item item) {

        if (getItems().size() == 0) {
            //No items so return location's status
            setStatus(theStatus);
        } else {
            //See if item in list and return item's status
            for (LocationItem li : getItems()) {
                if (li.getItem().equals(item)) {
                    li.setStatus(theStatus);
                }
            }
        }
        
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getScannedVerification();
    }
    
    /*
     * These methods delegate to the LocationDescription in order to make
     * the Import simpler. Depending on design enhancements in the import,
     * these may be removed.
     * TODO Remove the delegates or this comment after refactoring the Import 
     */

    /**
     * Delegate to the LocationDescription method.
     * @return the aisle for this location
     * @see LocationDescription#getAisle()
     */
    public String getAisle() {
        if (null == description) {
            description = new LocationDescription();
        }
        return description.getAisle();
    }

    /**
     * Delegate to the LocationDescription method.
     * @return the PostAisle directions
     * @see LocationDescription#getPostAisle()
     */
    public String getPostAisle() {
        if (null == description) {
            description = new LocationDescription();
        }
        return description.getPostAisle();
    }

    /**
     * Delegate to the LocationDescription method.
     * @return the preAisle direction
     * @see LocationDescription#getPreAisle()
     */
    public String getPreAisle() {
        if (null == description) {
            description = new LocationDescription();
        }
        return description.getPreAisle();
    }

    /**
     * Delegate to the LocationDescription method.
     * @return the slot
     * @see LocationDescription#getSlot()
     */
    public String getSlot() {
        if (null == description) {
            description = new LocationDescription();
        }
        return description.getSlot();
    }

    /**
     * Delegate to the LocationDescription method.
     * @param aisle the aisle of the location
     * @see LocationDescription#setAisle(java.lang.String)
     */
    public void setAisle(String aisle) {
        if (null == description) {
            description = new LocationDescription();
        }
        description.setAisle(aisle);
    }

    /**
     * Delegate to the LocationDescription method.
     * @param postAisle postAisle direction
     * @see LocationDescription#setPostAisle(java.lang.String)
     */
    public void setPostAisle(String postAisle) {
        if (null == description) {
            description = new LocationDescription();
        }
        description.setPostAisle(postAisle);
    }

    /**
     * Delegate to the LocationDescription method.
     * @param preAisle preAisle direction
     * @see LocationDescription#setPreAisle(java.lang.String)
     */
    public void setPreAisle(String preAisle) {
        if (null == description) {
            description = new LocationDescription();
        }
        description.setPreAisle(preAisle);
    }

    /**
     * Delegate to the LocationDescription method.
     * @param slot for the location
     * @see LocationDescription#setSlot(java.lang.String)
     */
    public void setSlot(String slot) {
        if (null == description) {
            description = new LocationDescription();
        }
        description.setSlot(slot);
    }
    
    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    /**
     * @return the lots
     */
    public Set<Lot> getLots() {
        return lots;
    }


    /**
     * @param lots the lots to set
     */
    public void setLots(Set<Lot> lots) {
        this.lots = lots;
    }


    /**
     * @return the lotCount
     */
    public int getLotCount() {
        return lotCount;
    }


    /**
     * @param lotCount the lotCount to set
     */
    public void setLotCount(int lotCount) {
        this.lotCount = lotCount;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 