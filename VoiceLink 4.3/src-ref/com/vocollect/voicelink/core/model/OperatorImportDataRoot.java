/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;




/**
 * @author jtauberg
 * 
 * Image of an Item that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 */
public class OperatorImportDataRoot  extends BaseModelObject implements Importable {

    private static final long serialVersionUID = -6278176777607393072L;

    private ImportableImpl impl = new ImportableImpl();
    
    private Operator myModel = new Operator();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        //We don't import workgroup, but we set it.
        //Create a new workgroup for this object so that later we can look up the
        //default workgroup using this new one.
        myModel.setWorkgroup(new Workgroup());
        return this.myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setStatus(int)
     */
    public void setImportStatus(int importStatus) {
        impl.setImportStatus(importStatus);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#getName()
     */
    public String getOperatorName() {
        return myModel.getName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#getOperatorIdentifier()
     */
    public String getOperatorIdentifier() {
        return myModel.getOperatorIdentifier();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#getPassword()
     */
    public String getPassword() {
        return myModel.getPassword();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#setName(java.lang.String)
     */
    public void setOperatorName(String name) {
        myModel.setName(name);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#setOperatorIdentifier(java.lang.String)
     */
    public void setOperatorIdentifier(String operatorId) {
        myModel.setOperatorIdentifier(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.OperatorRoot#setPassword(java.lang.String)
     */
    public void setPassword(String password) {
        myModel.setPassword(password);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }
    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 