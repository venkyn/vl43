/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Model object for the lot number used with lot tracking.
 *
 * @author mnichols
 */
public abstract class LotRoot extends CommonModelObject implements Serializable, Taggable {

    // Number used to distinquish the lot
    private String number;
    
    // Number spoken by the task to distinquish the lot
    private String speakableNumber;
    
    // Item associated with lot number
    private Item item;
    
    // Expiration date of the lot number
    private Date expirationDate;
    
    // Location associated with lot number
    private Location location;

    private static final long serialVersionUID = 1296439746097655490L;
    
    //private LotImportQueryType importQueryType;
    
    private String importQueryType;
    
    private Set<Tag> tags;    

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Lot)) {
            return false;
        }
        final Lot other = (Lot) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getNumber();
    }    
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }
    
    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return the expirationDate
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate the expirationDate to set
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the lot number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the lot number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the speakableNumber
     */
    public String getSpeakableNumber() {
        return speakableNumber;
    }

    /**
     * @param speakableNumber the speakable number to set
     */
    public void setSpeakableNumber(String speakableNumber) {
        this.speakableNumber = speakableNumber;
    }

    /**
     * @param importQueryType the importQueryType to set
     */
    public void setImportQueryType(String importQueryType) {
        this.importQueryType = importQueryType;
    }
    
    /**
     * @return the importQueryType
     */
     public String getImportQueryType() {
        return (importQueryType);
    }

   /**
    * Helper for the Import, allows us to access a field in the location that we use for a lookup.
    * @return the ID of the Location.
    */
    public String getLocationIdentifier() {
        if (location != null) {
            return (location.getScannedVerification());
        }
        return null;
    }
    
    /**
     * Helper for the import, to access a field in the location that we use for lookup.
     * @param locationIdentifier the id for the field.
     */
    public void setLocationIdentifier(String locationIdentifier) {
        if (location != null) {
            location.setScannedVerification(locationIdentifier);
        }
    }
    
    /**
     * Helper for the import, allow us access to the Item field we use for a lookup.
     * @param itemNumber the number field from the item.
     */
    public void setItemNumber(String itemNumber) {
        if (item != null) {
            item.setNumber(itemNumber);
        }
    }
    
    /**
     * Helper for the import, allow us access to the Item for a lookup value.
     * @return the number field from the item.
     */
    public String getItemNumber() {
        if (item != null) {
            return item.getNumber();
        }
        return null;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 