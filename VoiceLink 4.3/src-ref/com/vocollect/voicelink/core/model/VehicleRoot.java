/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * 
 * @author khazra
 */
public class VehicleRoot extends CommonModelObject implements Serializable,
    Taggable {

    //
    private static final long serialVersionUID = -1681557126097717509L;

    // Vehicle ID
    private String vehicleNumber;
    
    private String spokenVehicleNumber;

    // Vehicle Type
    private VehicleType vehicleType;

    //Vehicle category, is it default vehicle or some other
    private VehicleCategory vehicleCategory = VehicleCategory.Normal;
    
    //Operator currently performing safety checks. Could be >1 in case of default vehicle 
    private Set<Operator> operators = new HashSet<Operator>();
    
    private Set<Tag> tags;

    /**
     * Getter for the vehicleId property.
     * @return String value of the property
     */
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    /**
     * Setter for the vehicleId property.
     * @param vehicleId the new vehicleId value
     */
    public void setVehicleNumber(String vehicleId) {
        this.vehicleNumber = vehicleId;
    }

    
    
    /**
     * Getter for the spokenVehicleNumber property.
     * @return String value of the property
     */
    public String getSpokenVehicleNumber() {
        return spokenVehicleNumber;
    }

    
    /**
     * Setter for the spokenVehicleNumber property.
     * @param spokenVehicleNumber the new spokenVehicleNumber value
     */
    public void setSpokenVehicleNumber(String spokenVehicleNumber) {
        this.spokenVehicleNumber = spokenVehicleNumber;
    }

    /**
     * Getter for the vehicleType property.
     * @return VehicleType value of the property
     */
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    /**
     * Setter for the vehicleType property.
     * @param vehicleType the new vehicleType value
     */
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    
    /**
     * Getter for the vehicleCategory property.
     * @return VehicleCategory value of the property
     */
    public VehicleCategory getVehicleCategory() {
        return vehicleCategory;
    }

    
    /**
     * Setter for the vehicleCategory property.
     * @param vehicleCategory the new vehicleCategory value
     */
    public void setVehicleCategory(VehicleCategory vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }
    
    
    
    /**
     * @return the operators
     */
    public Set<Operator> getOperators() {
        return operators;
    }

    
    /**
     * @param operators the operators to set
     */
    public void setOperators(Set<Operator> operators) {
        this.operators = operators;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getVehicleNumber();
    }    

    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Vehicle)) {
            return false;
        }
        final Vehicle other = (Vehicle) obj;
        if (getVehicleNumber() == null) {
            if (other.getVehicleNumber() != null) {
                return false;
            }
        } else if (!getVehicleNumber().equals(other.getVehicleNumber())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.vehicleNumber == null ? 0 : this.vehicleNumber.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 