/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;

import java.io.Serializable;

/**
 * @author smittal
 * 
 */
public class ShiftTimeRoot implements Serializable {

    private static final long serialVersionUID = -937003048451356786L;

    private Integer hours;

    private Integer minutes;

    /**
     * @return the hours
     */
    public Integer getHours() {
        return hours;
    }

    
    /**
     * @param hours the hours to set
     */
    public void setHours(Integer hours) {
        this.hours = hours;
    }

    
    /**
     * @return the minutesR
     */
    public Integer getMinutes() {
        return minutes;
    }

    
    /**
     * @param minutes the minutes to set
     */
    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }
    
    /**
     * method to get the descriptive text.
     * @return descriptive text
     */
    public String getDescriptiveText() {
        return String.format("%02d:%02d", hours, minutes);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 