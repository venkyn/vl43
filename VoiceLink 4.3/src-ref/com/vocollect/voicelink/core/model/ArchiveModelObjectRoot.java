/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Site;

import java.util.Map;

/**
 * 
 *
 * @author treed
 */
public abstract class ArchiveModelObjectRoot extends CommonModelObject {

    //private Long    id;
    
    private static Map<Long, Site> siteMap;
    
//    /**
//     * Getter for the id property.
//     * @return long value of the property
//     */
//    public Long getId() {
//        return this.id;
//    }
//
//    
//    /**
//     * Setter for the id property.
//     * @param id the new id value
//     */
//    public void setId(Long id) {
//        this.id = id;
//    }

    
    /**
     * Getter for the siteMap property.
     * @return Map value of the property
     */
    public static Map<Long, Site> getSiteMap() {
        return siteMap;
    }


    
    /**
     * Setter for the siteMap property.
     * @param siteMap the new siteMap value
     */
    public static void setSiteMap(Map<Long, Site> siteMap) {
        ArchiveModelObjectRoot.siteMap = siteMap;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 