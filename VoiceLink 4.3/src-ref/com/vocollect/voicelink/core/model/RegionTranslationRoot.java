/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * Translation class for Region Sorting.
 *
 */
public class RegionTranslationRoot {

    private Long        id;

    //Name of region specified by user
    private String            name;

    //Description of region
    private String            description;

    //Properties for localized sorting
    private List<DataTranslation>    nameTrans;

    //Properties for localized sorting
    private List<DataTranslation>    descriptionTrans;

    
    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return description;
    }

    
    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    
    /**
     * Getter for the descriptionTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getDescriptionTrans() {
        return descriptionTrans;
    }

    
    /**
     * Setter for the descriptionTrans property.
     * @param descriptionTrans the new descriptionTrans value
     */
    public void setDescriptionTrans(List<DataTranslation> descriptionTrans) {
        this.descriptionTrans = descriptionTrans;
    }

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * Getter for the nameTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getNameTrans() {
        return nameTrans;
    }

    
    /**
     * Setter for the nameTrans property.
     * @param nameTrans the new nameTrans value
     */
    public void setNameTrans(List<DataTranslation> nameTrans) {
        this.nameTrans = nameTrans;
    }

    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 