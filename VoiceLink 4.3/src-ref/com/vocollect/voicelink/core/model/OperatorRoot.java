/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.OperatorCommon;
import com.vocollect.epp.model.Sharable;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ddoubleday
 */
public class OperatorRoot extends CommonModelObject
    implements java.io.Serializable, DataObject, Taggable, Sharable {

    private static final long serialVersionUID = 5417486216405499229L;

    private OperatorCommon common;

    // Tracks the last location the operatorIdentifier was at, used in forkapps
    private Location lastLocation;

    // Selection Region Operator is currently working in (If any)
    private Region currentRegion;

    // Selection Region operatorIdentifier has been assigned to
    private Region assignedRegion;

    // Selection Workgroup the operator is assigned to
    private Workgroup workgroup;

    // Current Work type of operator (current task function they signed n for)
    private TaskFunction currentWorkType;

    // Terminals Operator is currently signed on to
    // TODO: remove this Set and replace with a single terminal property, since
    // an operator can't be associated with more that one terminal at a time.
    private Set<Device> terminals;

    // Regions that this WorkgroupFunction was put into
    private Set<Region> regions;

    // Operators current status
    private OperatorStatus status = OperatorStatus.SignedOff;

    private String password;

    private Date signOnTime;

    private Date signOffTime;
    
    private Set<OperatorTeam> operatorTeams;

    /**
     * Constructor.
     */
    public OperatorRoot() {
        common = new OperatorCommon();
    }

    /**
     * Getter for the common property.
     * @return OperatorCommon value of the property
     */
    public OperatorCommon getCommon() {
        return common;
    }

    /**
     * Setter for the common property.
     * @param common the new common value
     */
    public void setCommon(OperatorCommon common) {
        this.common = common;
    }

    /**
     * @return the identifier
     * @see com.vocollect.epp.model.OperatorCommonRoot#getOperatorIdentifier()
     */
    public String getOperatorIdentifier() {
        return common.getOperatorIdentifier();
    }

    /**
     * Getter for the password property.
     * @return String value of the property
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Setter for the password property.
     * @param password the new password value
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for the signOffTime property.
     * @return Date value of the property
     */
    public Date getSignOffTime() {
        return this.signOffTime;
    }

    /**
     * Setter for the signOffTime property.
     * @param signOffTime the new signOffTime value
     */
    public void setSignOffTime(Date signOffTime) {
        this.signOffTime = signOffTime;
    }

    /**
     * Getter for the signOnTime property.
     * @return Date value of the property
     */
    public Date getSignOnTime() {
        return this.signOnTime;
    }

    /**
     * Setter for the signOnTime property.
     * @param signOnTime the new signOnTime value
     */
    public void setSignOnTime(Date signOnTime) {
        this.signOnTime = signOnTime;
    }

    /**
     * @return whether or not the object is a new one (not persisted yet.)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    @Override
    public boolean isNew() {
        return common.isNew();
    }

    /**
     * @param operatorIdentifier the operator identifier
     * @see com.vocollect.epp.model.OperatorCommonRoot#setOperatorIdentifier(java.lang.String)
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        common.setOperatorIdentifier(operatorIdentifier);
    }

 
    /**
     * Getter for the currentWorkType property.
     * @return TaskFunction value of the property
     */
    public TaskFunction getCurrentWorkType() {
        return currentWorkType;
    }

    /**
     * Setter for the currentWorkType property.
     * @param currentWorkType the new currentWorkType value
     */
    public void setCurrentWorkType(TaskFunction currentWorkType) {
        this.currentWorkType = currentWorkType;
    }

    /**
     * Getter for the assignedRegion property.
     * @return Region value of the property
     */
    public Region getAssignedRegion() {
        return this.assignedRegion;
    }

    /**
     * Setter for the assignedRegion property.
     * @param assignedRegion the new assignedRegion value
     */
    public void setAssignedRegion(Region assignedRegion) {
        this.assignedRegion = assignedRegion;
    }

    /**
     * Getter for the currentRegion property.
     * @return Region value of the property
     */
    public Region getCurrentRegion() {
        return this.currentRegion;
    }

    /**
     * Setter for the currentRegion property.
     * @param currentRegion the new currentRegion value
     */
    public void setCurrentRegion(Region currentRegion) {
        this.currentRegion = currentRegion;
    }

    /**
     * Getter for the lastLocation property.
     * @return Location value of the property
     */
    public Location getLastLocation() {
        return this.lastLocation;
    }

    /**
     * Setter for the lastLocation property.
     * @param lastLocation the new lastLocation value
     */
    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    /**
     * Getter for the workgroup property.
     * @return Workgroup value of the property
     */
    public Workgroup getWorkgroup() {
        return this.workgroup;
    }

    /**
     * Setter for the workgroup property.
     * @param workgroup the new workgroup value
     */
    public void setWorkgroup(Workgroup workgroup) {
        this.workgroup = workgroup;
    }

    /**
     * Getter for the terminals property.
     * @return the set of terminals associated with the operator.
     */
    public Set<Device> getTerminals() {
        return this.terminals;
    }

    /**
     * Setter for the terminals property.
     * @param terminals the new terminals value
     */
    public void setTerminals(Set<Device> terminals) {
        this.terminals = terminals;
    }
    
    /**
     * The terminal the operator is currently associated with.
     * @return The terminal the operator is currently associated with.
     */
    public Device getTerminal() {
        if (getTerminals().isEmpty()) {
            return null;
        }
        return getTerminals().iterator().next();
    }

    /**
     * Getter for the region property.
     * @return Set&lt;region&gt; value of the property
     */
    public Set<Region> getRegions() {
        return this.regions;
    }

    /**
     * Setter for the Region property.
     * @param regions the new region value
     */
    public void setRegions(Set<Region> regions) {
        this.regions = regions;
    }

    /**
     * @return the name
     * @see com.vocollect.epp.model.OperatorCommonRoot#getName()
     */
    public String getName() {
        return common.getName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    public Set<Tag> getTags() {
        return this.common.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        this.common.setTags(tags);
    }

    /**
     * @param name the operator name
     * @see com.vocollect.epp.model.OperatorCommonRoot#setName(java.lang.String)
     */
    public void setName(String name) {
        common.setName(name);
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Operator)) {
            return false;
        }
        final Operator other = (Operator) obj;

        if (!(getCommon().equals(other.getCommon()))) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.getOperatorIdentifier() == null ? 0 : this
            .getOperatorIdentifier().hashCode();
    }

    /**
     * Modify the model when the operator signs on.
     * @param time the sign on time.
     */
    public void signOn(Date time) {
        setStatus(OperatorStatus.SignedOn);
        setSignOnTime(time);
        setSignOffTime(null);
    }

    /**
     * Modify the model when the operator signs off.
     * @param time the sign off time.
     */
    public void signOff(Date time) {
        setStatus(OperatorStatus.SignedOff);
        setSignOffTime(time);
        setCurrentRegion(null);
        setLastLocation(null);
        setCurrentWorkType(null);

        // remove all regions for this operator
        if (getRegions() != null) {
            getRegions().clear();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getOperatorIdentifier();
    }

    /**
     * Assigns terminal to operator.
     * 
     * @param terminal - terminal to add to operator
     */
    public void addTerminal(Device terminal) {
        if (this.terminals == null) {
            this.terminals = new HashSet<Device>();
        } else {
            removeAllTerminals();
        }

        terminal.setOperator((Operator) this);
        this.terminals.add(terminal);
    }

    /**
     * Takes the operator off of all terminals.
     */
    protected void removeAllTerminals() {
        // Remove operator from all other temrinals
        for (Device term : getTerminals()) {
            term.setOperator(null);
        }
        getTerminals().clear();
    }

    /**
     * Getter for the status property.
     * @return OperatorStatus value of the property
     */
    public OperatorStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(OperatorStatus status) {
        this.status = status;
    }

    /**
     * Setter for the operatorTeams property.
     * @param operatorTeams the new operatorTeams value
     */
    public void setOperatorTeams(Set<OperatorTeam> operatorTeams) {
        this.operatorTeams = operatorTeams;
        
    }

    /**
     * Getter for the operatorTeams property.
     * @return the value of the property
     */
    public Set<OperatorTeam> getOperatorTeams() {
        return this.operatorTeams;
        
    }

    /**
     * @param operatorTeam - the operator team to add
     */
    public void addOperatorTeam(OperatorTeam operatorTeam) {
        if (getOperatorTeams() == null) {
            setOperatorTeams(new HashSet<OperatorTeam>());
        }
        getOperatorTeams().add(operatorTeam);
    }
    
    /**
     * Getter for the operator identifier drop down for reports
     * @return Either the operator identifier or the name appended if it exists
     */
    public String getReportOperatorIdentifier() {
        String opId =  this.getOperatorIdentifier();
        if (this.getName() != null && this.getName().trim() != "") {
            opId += " - " + this.getName();            
        }
        return opId;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 