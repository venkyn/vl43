/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 *
 * @author estoll
 */
public class WorkgroupFunctionRoot extends CommonModelObject {

    //
    private static final long serialVersionUID = 404648639859502425L;
    
    private Workgroup workgroup;
    private TaskFunction taskFunction;

    // Regionss that this WorkgroupFunction was put into
    private List<Region> regions  = new ArrayList<Region>();
    
    /**
     * Getter for the region property.
     * @return Set&lt;region&gt; value of the property
     */
    public List<Region> getRegions() {
        return this.regions;
    }

    /**
     * Setter for the Region property.
     * @param regions the new region value
     */
    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }
    
    
    /**
     * Getter/Setter for the taskFunction.
     * @return taskFunction value of the property
     */
    public TaskFunction getTaskFunction() {
        return this.taskFunction;
    }

    
    /**
     * @param taskFunction the new taskFunction value
     */
    public void setTaskFunction(TaskFunction taskFunction) {
        this.taskFunction = taskFunction;
    }

    
    /**
     * Getter/Setter for the Workgroup.
     * @return workgroup value of the property
     */
    public Workgroup getWorkgroup() {
        return this.workgroup;
    }

    
    /**
     * @param workgroup the new workgroup value
     */
    public void setWorkgroup(Workgroup workgroup) {
        this.workgroup = workgroup;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.taskFunction == null) ? 0 : this.taskFunction.hashCode());
        result = prime * result + ((this.workgroup == null) ? 0 : this.workgroup.hashCode());
        return result;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WorkgroupFunction)) {
            return false;
        }
        final WorkgroupFunction other = (WorkgroupFunction) obj;
        
        if (getTaskFunction() == null) {
            if (other.getTaskFunction() != null) {
                return false;
            }
        } else if (!getTaskFunction().equals(other.getTaskFunction())) {
            return false;
        }
        if (getWorkgroup() == null) {
            if (other.getWorkgroup() != null) {
                return false;
            }
        } else if (!getWorkgroup().equals(other.getWorkgroup())) {
            return false;
        }
        return true;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 