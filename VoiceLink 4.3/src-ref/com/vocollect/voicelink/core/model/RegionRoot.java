/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.Set;


/**
 * Model object representing a Region.
 *
 * @author ddoubleday
 */
public class RegionRoot extends CommonModelObject implements Serializable, Taggable {

    private static final long serialVersionUID = -1706207315244200436L;

    //Region number specified by user, and used for selecting region in task
    private Integer               number;

    //Name of region specified by user
    private String            name;

    //Description of region
    private String            description;

    //Application region belongs to
    private RegionType        type;

    //Estimated rate an operator should be working within region. Specified by user
    private Integer           goalRate;

    // WorkGroupFunctions that this Region was put into
    private Set<WorkgroupFunction> workgroupFunctions;

    private Set<Tag> tags;

    /**
     * Getter for the workgroupFunction property.
     * @return Set&lt;workgroupFunction&gt; value of the property
     */
    public Set<WorkgroupFunction> getWorkgroupFunctions() {
        return this.workgroupFunctions;
    }

    /**
     * Setter for the WorkgroupFunction property.
     * @param workgroupFunctions the new workgroupFunctions value
     */
    public void setWorkgroupFunctions(Set<WorkgroupFunction> workgroupFunctions) {
        this.workgroupFunctions = workgroupFunctions;
    }
    

    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Region)) {
            return false;
        }
        final Region other = (Region) obj;
        if (getNumber() != other.getNumber()) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((number == null) ? 0 : number);
        result = prime * result + ((type == null) ? 0 : type.toValue());
        return result;
    }

    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the goalRate property.
     * @return Integer value of the property
     */
    public Integer getGoalRate() {
        return this.goalRate;
    }

    /**
     * Setter for the goalRate property.
     * @param goalRate the new goalRate value
     */
    public void setGoalRate(Integer goalRate) {
        this.goalRate = goalRate;
    }

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the number property.
     * @return int value of the property
     */
    public Integer getNumber() {
        return this.number;
    }

    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * Getter for the type property.
     * @return short value of the property
     */
    public RegionType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(RegionType type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
    
    public String getReportName() {
        String regionType = ResourceUtil.getLocalizedKeyValue(
            this.getType().getResourceKey());
        return (regionType + " - " + this.getName());
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 