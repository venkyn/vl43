/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.Date;
import java.util.Set;

/**
 * 
 *
 * @author Administrator
 */
public class OperatorLaborRoot extends CommonModelObject implements Taggable {

    //
    private static final long serialVersionUID = -9059084128411694814L;


    //Operator detail information belongs to
    private Operator                    operator;
    
    //Function operator was performing
    private OperatorLaborActionType     actionType;
   
    //Time operator started function
    private Date                        startTime;
    
    //Time operator completed function NULL if not compleded
    private Date                        endTime;
    
    //The difference between startTime and endTime in milliseconds.
    private Long                        duration;
    
    // has this record been exported?
    private ExportStatus                exportStatus = ExportStatus.NotExported;
    
    // filter type for filtering
    private OperatorLaborFilterType     filterType;
    
    private Set<Tag>                    tags;
    

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }
    
    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    
    
    /**
     * Getter for the actionType property.
     * @return OperatorLaborActionType value of the property
     */
    public OperatorLaborActionType getActionType() {
        return this.actionType;
    }

    
    /**
     * Setter for the actionType property.
     * @param actionType the new actionType value
     */
    public void setActionType(OperatorLaborActionType actionType) {
        this.actionType = actionType;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return this.startTime;
    }
    
    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    
    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return this.endTime;
    }
    
    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    
    /**
     * Getter for the duration property.
     * @return Long value of the duration property in seconds 
     */
    public Long getDuration() {
        return this.duration;
    }

    
    /**
     * Setter for the duration property.
     * @param duration the new duration value in milliSeconds
     */
    public void setDuration(Long duration) {
        this.duration = duration;  
    }
     
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }
    
    /**
     * Getter for the filterType property.
     * @return OperatorLaborFilterType value of the property
     */
    public OperatorLaborFilterType getFilterType() {
        return this.filterType;
    }
    
    /**
     * Setter for the filterType property.
     * @param filterType the new filterType value
     */
    public void setFilterType(OperatorLaborFilterType filterType) {
        this.filterType = filterType;
    }
    
    
    /**
     * Getter for the tags property.
     * @return Set of tags 
     */
    public Set<Tag> getTags() {
        return this.tags;
    }

    
    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((actionType == null) ? 0 : actionType.hashCode());
        result = prime * result + ((duration == null) ? 0 : duration.hashCode());
        result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OperatorLabor other = (OperatorLabor) obj;
        if (actionType == null) {
            if (other.getActionType() != null) {
                return false;
            }    
        } else if (!actionType.equals(other.getActionType())) {
            return false;
        }
        if (duration == null) {
            if (other.getDuration() != null) {
                return false;
            }    
        } else if (!duration.equals(other.getDuration())) {
            return false;
        }
        if (endTime == null) {
            if (other.getEndTime() != null) {
                return false;
            }    
        } else if (!endTime.equals(other.getEndTime())) {
            return false;
        }
        if (operator == null) {
            if (other.getOperator() != null) {
                return false;
            }    
        } else if (!operator.equals(other.getOperator())) {
            return false;
        }
        if (startTime == null) {
            if (other.getStartTime() != null) {
                return false;
            }    
        } else if (!startTime.equals(other.getStartTime())) {
            return false;
        }    
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getOperator().getDescriptiveText();
    }
    
    /**
     * this is just here so we dont get <code>NoSuchMethodException</code>s
     * from the automatic JSON creation.
     * @return null;
     */
    public Region getRegion() {
        return null;
    }
    
    /**
     * this is just here so we dont get <code>NoSuchMethodException</code>s
     * from the automatic JSON creation.
     * @return null
     */
    public Double getActualRate() {
        return null;
    }
    
    /**
     * this is just here so we dont get <code>NoSuchMethodException</code>s
     * from the automatic JSON creation.
     * @return null
     */
    public Integer getCount() {
        return null;
    }
    
    /**
     * this is just here so we dont get <code>NoSuchMethodException</code>s
     * from the automatic JSON creation.
     * @return null
     */
    public Double getPercentOfGoal() {
        return null;
    }
    
    /**
     * this is just here so we dont get <code>NoSuchMethodException</code>s
     * from the automatic JSON creation.
     * @return null
     */
    public BreakType getBreakType() {
        return null;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 