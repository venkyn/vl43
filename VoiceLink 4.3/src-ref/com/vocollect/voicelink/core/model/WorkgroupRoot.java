/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author estoll
 */
public class WorkgroupRoot extends CommonModelObject implements Taggable {

    //
    private static final long serialVersionUID = 2232664770381996088L;

    // Descriptive name
    private String groupName;
    
    // Is this the default workgroup
    private Boolean isDefault = false;
    
    // Does this Work Group gain new regions as they are added
    // to the system.
    private Boolean autoAddRegions = true;
    
    // Number of users in this workgroup
    private int operatorCount;
    
    private List<WorkgroupFunction> workgroupFunctions = new ArrayList<WorkgroupFunction>();

    private Set<Tag> tags;

    /**
     * Getter for the userCount property.
     * @return int value of the property
     */
    public int getOperatorCount() {
        return this.operatorCount;
    }

    /**
     * Setter for the userCount property.
     * @param operatorCount the new operatorCount value
     */
    public void setOperatorCount(int operatorCount) {
        this.operatorCount = operatorCount;
    }

    /**
     * @return this.groupName
     */
    public String getGroupName() {
        return this.groupName;
    }

    /**
     * @param groupName the new groupName value
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return this.isDefault
     */
    public Boolean getIsDefault() {
        return this.isDefault;
    }

    /**
     * @param isDefault the new isDefault value
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    /**
     * @return this.autoAddRegions
     */
    public Boolean getAutoAddRegions() {
        return this.autoAddRegions;
    }
    
    /**
     * @param autoAddRegions the value of the property
     */
    public void setAutoAddRegions(Boolean autoAddRegions) {
        this.autoAddRegions = autoAddRegions;
    }

    
    /**
     * Getter for the workgroupFunctions property.
     * @return List value of the property
     */
    public List<WorkgroupFunction> getWorkgroupFunctions() {
        return this.workgroupFunctions;
    }

    
    /**
     * Setter for the workgroupFunctions property.
     * @param workgroupFunctions the new workgroupFunctions value
     */
    public void setWorkgroupFunctions(List<WorkgroupFunction> workgroupFunctions) {
        this.workgroupFunctions = workgroupFunctions;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getGroupName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && obj instanceof Workgroup) {
            return this.getId().equals(((Workgroup) obj).getId());
        }
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return this.groupName == null ? 0 : this.groupName.hashCode();
    }
    
    /**
     * @param taskFunction The function to be added to this workgroup.
     * @param regions The list of regions for the task function.
     */
    public void addTaskFunction(TaskFunction taskFunction, List<Region> regions) {
        WorkgroupFunction wgf = new WorkgroupFunction();
        wgf.setWorkgroup((Workgroup) this);
        wgf.setTaskFunction(taskFunction);
        wgf.setRegions(regions);
        if (!getWorkgroupFunctions().contains(wgf)) {
            getWorkgroupFunctions().add(wgf);
        }
    }
    
    /**
     * @param taskFunction The function to be removed from the workgroup.
     */
    public void removeTaskFunction(TaskFunction taskFunction) {
        WorkgroupFunction wgf = new WorkgroupFunction();
        wgf.setWorkgroup((Workgroup) this);
        wgf.setTaskFunction(taskFunction);
        if (getWorkgroupFunctions().contains(wgf)) {
            getWorkgroupFunctions().remove(wgf);
        }
    }
    
    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 