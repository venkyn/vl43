/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.DataTranslation;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

/**
 * @author dgold
 *
 */
public class DataTranslationImportDataRoot extends BaseModelObject implements
        Importable {

    private static final long serialVersionUID = 1872248974281565054L;
    private ImportableImpl impl = new ImportableImpl();
    private DataTranslation myModel = new DataTranslation();
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        return myModel;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#getKey()
     */
    public String getKey() {
        return myModel.getKey();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#getLocale()
     */
    public String getLocale() {
        return myModel.getLocale();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#getTranslation()
     */
    public String getTranslation() {
        return myModel.getTranslation();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#setKey(java.lang.String)
     */
    public void setKey(String key) {
        myModel.setKey(key);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#setLocale(java.lang.String)
     */
    public void setLocale(String locale) {
        myModel.setLocale(locale);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#setTranslation(java.lang.String)
     */
    public void setTranslation(String translation) {
        myModel.setTranslation(translation);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DataTranslationImportDataRoot)) {
            return false;
        }
        final DataTranslationImportDataRoot other = (DataTranslationImportDataRoot) obj;
        if (getKey() == null) {
            if (other.getKey() != null) {
                return false;
            }
        } else if (!getKey().equals(other.getKey())) {
            return false;
        }
        if (getLocale() == null) {
            if (other.getLocale() != null) {
                return false;
            }
        } else if (!getLocale().equals(other.getLocale())) {
            return false;
        }
        if (getTranslation() == null) {
            if (other.getTranslation() != null) {
                return false;
            }
        } else if (!getTranslation().equals(other.getTranslation())) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataTranslationRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return myModel.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 