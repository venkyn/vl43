/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.Set;

/**
 * @author estoll
 */

public class PrinterRoot extends CommonModelObject implements Taggable  {

    private static final long serialVersionUID = -371000135807094546L;

    //User defined printer number
    private Integer number;

    // Descriptive name of printer    
    private String name;

    // Whether printer is enabled or not
    private Boolean isEnabled;
    
    private Set<Tag> tags;

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
    
    /**
     * Getter for the isEnabled property.
     * @return Boolean value of the property
     */
    public Boolean getIsEnabled() {
        return this.isEnabled;
    }
    
    /**
     * Setter for the isEnabled property.
     * @param isEnabled the new isEnabled value
     */
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    /**
     * Getter for the number property.
     * @return int value of the property
     */
    public Integer getNumber() {
        return number;
    }

    
    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(Integer number) {
        this.number = number;
    }


    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Printer)) {
            return false;
        }

        // Name only makes equal
        final Printer other = (Printer) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.getNumber();
    }
   
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getName();
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 