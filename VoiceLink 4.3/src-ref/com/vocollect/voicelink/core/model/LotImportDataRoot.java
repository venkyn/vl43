/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

import java.util.Date;


/**
 * Image of an Lot that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 * 
 * @author mnichols
 *
 */
public class LotImportDataRoot extends BaseModelObject {

    private static final long serialVersionUID = 449799234950405415L;

    /**
     * Default constructor.
     */
    public LotImportDataRoot() {
        super();
    }
    
    private ImportableImpl impl = new ImportableImpl();
    
    private Lot myModel = new Lot();

    /**
     * {@inheritDoc}
     * @return
     */
    public Object convertToModel() {
        return this.getMyModel();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long customerID) {
        impl.setImportID(customerID);
    }    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#getSpeakableNumber()
     */
    public String getSpeakableNumber() {
        return myModel.getSpeakableNumber();
    }    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setSpeakableNumber(java.lang.String)
     */
    public void setSpeakableNumber(String number) {
        myModel.setSpeakableNumber(number);
    }    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#getExpirationDate()
     */
    public Date getExpirationDate() {
        return myModel.getExpirationDate();
    }  
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setExpirationDate(java.util.Date)
     */
    public void setExpirationDate(Date expDate) {
        myModel.setExpirationDate(expDate);
    }
    
    /**
     * Set the number in the Item. This will be used to look up an Item in the database.
     * @param itemId the ID to set the Item.number to.
     */
    public void setItemId(String itemId) {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        tempItem.setNumber(itemId);
    }

    
    /**
     * Get the Item Id (Item.number).
     * @return the Item Id (Item.number a String).
     */
    public String getItemId() {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            return null;        
        }
        if (null == tempItem.getNumber()) {
            return null;
        }
        return tempItem.getNumber();
    }
    
    /**
     * Set the Location id (scannedVerification) in the Location.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the Location.scannedVerification to.
     */
    public void setLocationId(String scannedVerification) {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }

    /**
     * Get the Location's id (scannedVerification).
     * @return the Location ID (scannedVerification) (String).
     */
    public String getLocationId() {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            return null;
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#getImportQueryType()
     */
    public String getImportQueryType() {
        return myModel.getImportQueryType();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setImportQueryType(java.lang.String)
     */
    public void setImportQueryType(String queryType) {
        myModel.setImportQueryType(queryType);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    } 

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * Sets the value of the myModel.
     * @param myModel the myModel to set
     */
    public void setMyModel(Lot myModel) {
        this.myModel = myModel;
    }

    /**
     * Gets the value of myModel.
     * @return the myModel
     */
    public Lot getMyModel() {
        return myModel;
    }   

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LotRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }    

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * @see com.vocollect.epp.model.DataObjectRoot#getId()
     * @return import id
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 