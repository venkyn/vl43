/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;


/**
 * Model for items defined at location.
 *
 */
public class LocationItemRoot extends CommonModelObject  
implements Serializable, Taggable {
    
    //
    private static final long serialVersionUID = 4038305394601514838L;

    private Location        location = null;
    
    private Item            item = null;
    
    private LocationStatus  status = LocationStatus.Replenished;
    
    private String          importAction = null;
    
    private Set<Tag> tags = null;
    

    

    
    
    /**
     * Getter for the importAction property.
     * @return String value of the property
     */
    public String getImportAction() {
        return importAction;
    }


    
    /**
     * Setter for the importAction property.
     * @param importAction the new importAction value
     */
    public void setImportAction(String importAction) {
        this.importAction = importAction;
    }


    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }

    
    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }
    
    /**
     * Helper for the import, allow us access to the Item field we use for a lookup.
     * @param itemNumber the number field from the item.
     */
    public void setItemNumber(String itemNumber) {
        this.item.setNumber(itemNumber);
    }
    
    /**
     * Helper for the import, allow us access to the Item for a lookup value.
     * @return the number field from the item.
     */
    public String getItemNumber() {
        return this.item.getNumber();
    }

    
    /**
     * Getter for the location property.
     * @return Location value of the property
     */
    public Location getLocation() {
        return location;
    }

    
    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }
    
    /**
     * Helper for the import, to access a field in the location that we use for lookup.
     * @param scannedVerification the id for the field.
     */
    public void setLocationIdentifier(String scannedVerification) {
        this.location.setScannedVerification(scannedVerification);
    }
    
    /**
     * Helper for the Import, allows us to access a field in the location that we use for a lookup.
     * @return the ID of the Location.
     */
    public String getLocationIdentifier() {
        return this.location.getScannedVerification();
    }

    
    /**
     * Getter for the status property.
     * @return LocationStatus value of the property
     */
    public LocationStatus getStatus() {
        return status;
    }

    
    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(LocationStatus status) {
        this.status = status;
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
    
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocationItem)) {
            return false;
        }
        final LocationItem other = (LocationItem) obj;
        if (getItem() == null) {
            if (other.getItem() != null) {
                return false;
            }
        } else if (!getItem().equals(other.getItem())) {
            return false;
        }
        if (getLocation() == null) {
            if (other.getLocation() != null) {
                return false;
            }
        } else if (!getLocation().equals(other.getLocation())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((item == null) ? 0 : item.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        return result;
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.location.getScannedVerification();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 