/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


import com.vocollect.epp.model.BaseModelObject;

/**
 * 
 *
 * @author estoll
 */
public class TaskFunctionRoot extends BaseModelObject {

    private static final long serialVersionUID = -4200706303720116797L;

    // The type of function
    private TaskFunctionType functionType;
    
    //regionType indicates what type of region is associated with this function
    private RegionType regionType;
    
    //is this task function enabled - by default, all are enabled
    private Boolean isEnabled = true;

    //does this task function have regions
    private Boolean hasRegions = true;
    
    
    /**
     * Getter for the functionType property.
     * @return TaskFunctionType value of the property
     */
    public TaskFunctionType getFunctionType() {
        return this.functionType;
    }

    
    /**
     * Setter for the functionType property.
     * @param functionType the new functionType value
     */
    public void setFunctionType(TaskFunctionType functionType) {
        this.functionType = functionType;
    }

    /**
     * Getter for the hasRegions property.
     * @return Boolean value of the property
     */
    public Boolean getHasRegions() {
        return this.hasRegions;
    }
    
    /**
     * Setter for the hasRegions property.
     * @param hasRegions the new hasRegions value
     */
    public void setHasRegions(Boolean hasRegions) {
        this.hasRegions = hasRegions;
    }

    /**
     * @return isEnabled
     */
    public Boolean getIsEnabled() {
        return this.isEnabled;
    }

    
    /**
     * @param isEnabled value for the new isEnabled
     */
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    
    /**
     * @return regionType
     */
    public RegionType getRegionType() {
        return this.regionType;
    }

    
    /**
     * @param regionType value for new regionType
     */
    public void setRegionType(RegionType regionType) {
        this.regionType = regionType;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TaskFunction)) {
            return false;
        }
        final TaskFunction other = (TaskFunction) obj;
        if (getFunctionType() == null) {
            if (other.getFunctionType() != null) {
                return false;
            }
        } else if (!getFunctionType().equals(other.getFunctionType())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.functionType == null 
            ? 0 : this.functionType.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 