/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;

/**
 * @author pkolonay
 *
 */
public class UnitOfMeasureTranslationRoot {
    
    private Long        id;
    
    private String      name;
    
    //Properties for localized sorting
    private List<DataTranslation>    nameTrans;

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    
    /**
     * Setter for the name property.
     * @param unitOfMeasure the new name value
     */
    public void setName(String unitOfMeasure) {
        this.name = unitOfMeasure;
    }

    
    /**
     * Getter for the nameTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getNameTrans() {
        return nameTrans;
    }

    
    /**
     * Setter for the nameTrans property.
     * @param nameTrans the new unitOfMeasureTrans value
     */
    public void setNameTrans(List<DataTranslation> nameTrans) {
        this.nameTrans = nameTrans;
    }

    
    
 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 