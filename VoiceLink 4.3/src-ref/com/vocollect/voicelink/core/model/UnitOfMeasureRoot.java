/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * @author pkolonay
 *
 */
public class UnitOfMeasureRoot extends CommonModelObject implements Serializable,
        Taggable {

    /**
     * 
     */
    private static final long serialVersionUID = 2980922348369648563L;
    
    private String  name;
    
    private Set<Tag> tags;
    
    
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * @param unitOfMeasure the unitOfMeasure to set
     */
    public void setName(String unitOfMeasure) {
        this.name = unitOfMeasure;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getName();
    }
    
    
    /**
     * @return the tags
     */
    public Set<Tag> getTags() {
        return tags;
    }


    /**
     * @param tags the tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((name == null) ? 0 : name.hashCode());
        return result;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UnitOfMeasure other = (UnitOfMeasure) obj;
        if (name == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!name.equals(other.getName())) {
            return false;
        }
        return true;
    }



    
    
    
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 