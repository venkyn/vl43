/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

import java.util.Date;
import java.util.Set;


/**
 * Image of an Item that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 * 
 * @author dgold
 *
 */
public class ItemImportDataRoot extends BaseModelObject {

    /**
     * Default constructor.
     */
    public ItemImportDataRoot() {
        super();
    }
    private static final long serialVersionUID = -2491992580006410647L;
    
    private ImportableImpl impl = new ImportableImpl();
    
    private Item myModel = new Item(); 

    /**
     * {@inheritDoc}
     * @return
     */
    public Object convertToModel() {
        return this.getMyModel();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long customerID) {
        impl.setImportID(customerID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * Sets the value of the myModel.
     * @param myModel the myModel to set
     */
    public void setMyModel(Item myModel) {
        this.myModel = myModel;
    }

    /**
     * Gets the value of myModel.
     * @return the myModel
     */
    public Item getMyModel() {
        return myModel;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getCube()
     */
    public double getCube() {
        return myModel.getCube();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getDescription()
     */
    public String getDescription() {
        return myModel.getDescription();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getIsSerialNumberItem()
     */
    public boolean getIsSerialNumberItem() {
        return myModel.getIsSerialNumberItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getIsVariableWeightItem()
     */
    public boolean getIsVariableWeightItem() {
        return myModel.getIsVariableWeightItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getPack()
     */
    public String getPack() {
        return myModel.getPack();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getPhoneticDescription()
     */
    public String getPhoneticDescription() {
        return myModel.getPhoneticDescription();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getScanVerificationCode()
     */
    public String getScanVerificationCode() {
        return myModel.getScanVerificationCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getSize()
     */
    public String getSize() {
        return myModel.getSize();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getSpokenVerificationCode()
     */
    public String getSpokenVerificationCode() {
        return myModel.getSpokenVerificationCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getUpc()
     */
    public String getUpc() {
        return myModel.getUpc();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getVariableWeightTolerance()
     */
    public Integer getVariableWeightTolerance() {
        return myModel.getVariableWeightTolerance();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#getWeight()
     */
    public double getWeight() {
        return myModel.getWeight();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setCube(double)
     */
    public void setCube(double cube) {
        myModel.setCube(cube);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        myModel.setDescription(description);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setIsSerialNumberItem(boolean)
     */
    public void setIsSerialNumberItem(boolean isSerialNumberItem) {
        myModel.setIsSerialNumberItem(isSerialNumberItem);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setIsVariableWeightItem(boolean)
     */
    public void setIsVariableWeightItem(boolean isVariableWeightItem) {
        myModel.setIsVariableWeightItem(isVariableWeightItem);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setPack(java.lang.String)
     */
    public void setPack(String pack) {
        myModel.setPack(pack);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setPhoneticDescription(java.lang.String)
     */
    public void setPhoneticDescription(String phoneticDescription) {
        myModel.setPhoneticDescription(phoneticDescription);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setScanVerificationCode(java.lang.String)
     */
    public void setScanVerificationCode(String scanVerificationCode) {
        myModel.setScanVerificationCode(scanVerificationCode);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setSize(java.lang.String)
     */
    public void setSize(String size) {
        myModel.setSize(size);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setSpokenVerificationCode(java.lang.String)
     */
    public void setSpokenVerificationCode(String spokenVerificationCode) {
        myModel.setSpokenVerificationCode(spokenVerificationCode);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setUpc(java.lang.String)
     */
    public void setUpc(String upc) {
        myModel.setUpc(upc);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setVariableWeightTolerance(java.lang.Integer)
     */
    public void setVariableWeightTolerance(Integer variableWeightTolerance) {
        myModel.setVariableWeightTolerance(variableWeightTolerance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.ItemRoot#setWeight(double)
     */
    public void setWeight(double weight) {
        myModel.setWeight(weight);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * @see com.vocollect.epp.model.DataObjectRoot#getId()
     * @return import id
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }


}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 