/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.math.BigDecimal;
import java.util.Set;


/**
 * 
 *
 * @author pfunyak
 */
public class OperatorLaborByRegionRoot implements DataObject, Taggable {
    // hacked up id 
    private Long                        id;
    
    // Site 
    private Site                        site; 
    
    // Region
    private Region                      region;

    // Calculated precentage of goal
    private Double                      percentOfGoal;

    // Calculated actual rate
    private Double                      actualRate;
    
    // goal rate
    private Integer                     goalRate;

    // total items picked for this region
    private Integer                     totalQuantity;

    // total pick time for this region
    private Long                        totalTime;
    
    // the count of operators 
    private Long                        numberOfOperators;
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getId()
     */
    public Long getId() {
        // Theres no such thing as an ID for an aggregate, so use region.
       // if (getRegion() == null) {
       //     return null;
       // } else {
       //     return getRegion().getId();
       // }
       return this.id;
    }
    
    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return this.site;
    }

    
    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Getter for the regionName property.
     * @return String value of the property
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Setter for the regionName property.
     * @param region the  region object
     */
    public void setRegion(Region region) {
        this.region = region;
    }
    
    /**
     * Getter for the percentOfGoal property.
     * @return Float value of the property
     */
    public Double getPercentOfGoal() {
        return new BigDecimal(this.percentOfGoal)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 
    }

    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }
    
    
    /**
     * Getter for the actualRate property.
     * @return Float value of the property
     */
    public Double getActualRate() {
        return new BigDecimal(this.actualRate)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    
    /**
     * Getter for the goalRate property.
     * @return Integer value of the property
     */
    public Integer getGoalRate() {
        return this.goalRate;
    }

    
    /**
     * Setter for the goalRate property.
     * @param goalRate the new goalRate value
     */
    public void setGoalRate(Integer goalRate) {
        this.goalRate = goalRate;
    }


    /**
     * Getter for the totalQuantity property.
     * @return Integer value of the property
     */
    public Integer getTotalQuantity() {
        return this.totalQuantity;
    }

    /**
     * Setter for the totalQuantity property.
     * @param totalQuantity the new totalQuantity value
     */
    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    /**
     * Getter for the totalTime property.
     * @return Long value of the property
     */
    public Long getTotalTime() {
        return this.totalTime;
    }

    /**
     * Setter for the totalTime property.
     * @param totalTime the new totalTime value
     */
    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }
    
    
    /**
     * Getter for the numberOfOperators property.
     * @return Integer value of the property
     */
    public Long getNumberOfOperators() {
        return this.numberOfOperators;
    }

    /**
     * Setter for the numberOfOperators property.
     * @param numberOfOperators the new numberOfOperators value
     */
    public void setNumberOfOperators(Long numberOfOperators) {
        this.numberOfOperators = numberOfOperators;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof OperatorLaborByRegion) {
            return getRegion().equals(((OperatorLaborByRegion) o).getRegion());
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        if (this.getRegion() != null) {
            return getRegion().hashCode();
        }
        return 0;
    
    
    } 
 
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        if (getRegion() == null) {
            return null;
        } else {
            return getRegion().getDescriptiveText();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    public Set<Tag> getTags() {
        // TODO Auto-generated method stub
        return null;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        // TODO Auto-generated method stub
        
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 