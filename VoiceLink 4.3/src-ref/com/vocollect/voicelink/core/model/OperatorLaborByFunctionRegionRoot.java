/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.math.BigDecimal;
import java.util.Set;

/**
 * 
 *
 * @author pfunyak
 */
public class OperatorLaborByFunctionRegionRoot implements DataObject, Taggable {
     
    
    // Site 
    private Site                        site;
    
    // Operator
    private Operator                    operator;
  
    // Region
    private Region                      region;
    
    // function - selection, putaway, etc
    private OperatorLaborActionType     actionType;
    
    // rate stored on Operator Labor record
    private Integer                     goalRate;

    // Calculated precentage of goal
    private Double                      percentOfGoal;

    // Calculated actual rate
    private Double                      actualRate;

    // total items picked for this region
    private Integer                     totalQuantity;

    // total pick time for this region
    private Long                        totalTime;
    
    // filter type for filtering feature.
    private OperatorLaborFilterType     filterType;

  

    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return this.site;
    }

    
    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

  
    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the regionName property.
     * @return String value of the property
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Setter for the regionName property.
     * @param region the  region object
     */
    public void setRegion(Region region) {
        this.region = region;
    }

    
    /**
     * Getter for the actionType property.
     * @return OperatorLaborActionType value of the property
     */
    public OperatorLaborActionType getActionType() {
        return this.actionType;
    }


    
    /**
     * Setter for the actionType property.
     * @param actionType the new actionType value
     */
    public void setActionType(OperatorLaborActionType actionType) {
        this.actionType = actionType;
    }


    /**
     * Getter for the goalRate property.
     * @return Integer value of the property
     */
    public Integer getGoalRate() {
        return this.goalRate;
    }

    
    /**
     * Setter for the goalRate property.
     * @param goalRate the new goalRate value
     */
    public void setGoalRate(Integer goalRate) {
        this.goalRate = goalRate;
    }


    /**
     * Getter for the totalPicked property.
     * @return Integer value of the property
     */
    public Integer getTotalQuantity() {
        return this.totalQuantity;
    }

    /**
     * Setter for the totalPicked property.
     * @param totalQuantity the new totalQuantity value
     */
    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    /**
     * Getter for the totalTime property.
     * @return Long value of the property
     */
    public Long getTotalTime() {
        return this.totalTime;
    }

    /**
     * Setter for the totalTime property.
     * @param totalTime the new totalTime value
     */
    public void setTotalTime(Long totalTime) {
        this.totalTime = totalTime;
    }
   
    
    /**
     * Getter for the actualRate property.
     * @return Float value of the property
     */
    public Double getActualRate() {
        // TODO Rupert wants to remove this in phase 3
        return new BigDecimal(this.actualRate)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
        
    }

    /**
     * Getter for the percentOfGoal property.
     * @return Float value of the property
     */
    public Double getPercentOfGoal() {
        // TODO Rupert wants to remove this in phase 3
        return new BigDecimal(this.percentOfGoal)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue(); 

    }

    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

  
    /**
     * Getter for the filterType property.
     * @return OperatorLaborFilterType value of the property
     */
    public OperatorLaborFilterType getFilterType() {
        return this.filterType;
    }

    
    /**
     * Setter for the filterType property.
     * @param filterType the new filterType value
     */
    public void setFilterType(OperatorLaborFilterType filterType) {
        this.filterType = filterType;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof OperatorLaborByFunctionRegion) {
            OperatorLaborByFunctionRegion obj = (OperatorLaborByFunctionRegion) o;
            if (obj.getId().equals(this.getId())) { 
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        // hash by operator and region because this combination is always unique
        // to an object of this type, and any other properties may change
        // very often if the operator is currently working.
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((region == null) ? filterType.hashCode() : region.hashCode());
        return result;
    }
    
    /**
     * Because aggregates are not stored in the database, there is no actual
     * ID property. This method returns this objects hashCode.
     * @return A unique identifier.
     * @see com.vocollect.epp.model.DataObject#getId()
     */
    public Long getId() {
        Long opHash = new Long(operator.hashCode());
        Long otherHash = new Long((region == null) ? filterType.hashCode() : region.hashCode());
        Long result = (opHash * Integer.MAX_VALUE) + otherHash; 
        return new Long(result.hashCode());
    }
    
    /**
     * Getter for the filterType property.
     * @return OperatorLaborFilterType value of the property
     */
    public Integer getFilterTypeAsInt() {
        return this.filterType.toValue();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    public Set<Tag> getTags() {
        // TODO Auto-generated method stub
        return null;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        // TODO Auto-generated method stub
        
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 