/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;




/**
 * @author jtauberg
 * 
 * Image of a Core Location that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 */
public class LocationImportDataRoot  extends BaseModelObject implements Importable {

    private static final long serialVersionUID = 3279140911784670007L;

    private ImportableImpl impl = new ImportableImpl();
    
    private Location myModel = new Location();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        return this.myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setStatus(int)
     */
    public void setImportStatus(int importStatus) {
        impl.setImportStatus(importStatus);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getAisle()
     */
    public String getAisle() {
        return myModel.getAisle();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getCheckDigits()
     */
    public String getCheckDigits() {
        return myModel.getCheckDigits();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getDescription()
     */
    public LocationDescription getDescription() {
        return myModel.getDescription();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getPostAisle()
     */
    public String getPostAisle() {
        return myModel.getPostAisle();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getPreAisle()
     */
    public String getPreAisle() {
        return myModel.getPreAisle();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getScannedVerification()
     */
    public String getScannedVerification() {
        return myModel.getScannedVerification();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getSlot()
     */
    public String getSlot() {
        return myModel.getSlot();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#getSpokenVerification()
     */
    public String getSpokenVerification() {
        return myModel.getSpokenVerification();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setAisle(java.lang.String)
     */
    public void setAisle(String aisle) {
        myModel.setAisle(aisle);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setCheckDigits(java.lang.String)
     */
    public void setCheckDigits(String checkDigits) {
        myModel.setCheckDigits(checkDigits);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setDescription(com.vocollect.voicelink.core.model.LocationDescription)
     */
    public void setDescription(LocationDescription components) {
        myModel.setDescription(components);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setPostAisle(java.lang.String)
     */
    public void setPostAisle(String postAisle) {
        myModel.setPostAisle(postAisle);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setPreAisle(java.lang.String)
     */
    public void setPreAisle(String preAisle) {
        myModel.setPreAisle(preAisle);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setScannedVerification(java.lang.String)
     */
    public void setScannedVerification(String scannedVerification) {
        myModel.setScannedVerification(scannedVerification);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setSlot(java.lang.String)
     */
    public void setSlot(String slot) {
        myModel.setSlot(slot);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationRoot#setSpokenVerification(java.lang.String)
     */
    public void setSpokenVerification(String spokenVerification) {
        myModel.setSpokenVerification(spokenVerification);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 