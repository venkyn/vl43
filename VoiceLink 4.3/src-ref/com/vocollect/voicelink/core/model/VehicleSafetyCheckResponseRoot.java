/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;


import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * @author mraj
 *
 */
public class VehicleSafetyCheckResponseRoot extends CommonModelObject implements
Serializable, Taggable {

    /**
     * 
     */
    private static final long serialVersionUID = -8050540936338994021L;

    private Vehicle vehicle;
    
    private VehicleSafetyCheck safetyCheck;
    
    private Integer checkResponse;
    
    private VehicleSafetyCheckRepairActionType repairAction;
    
    private Operator operator;
    
    private Long internalGroupId;

    private String note = "";
    
    private Set<Tag> tags;
    
    
    
    /**
     * @return the vehicle
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    
    /**
     * @param vehicle the vehicle to set
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    
    /**
     * @return the safetyCheck
     */
    public VehicleSafetyCheck getSafetyCheck() {
        return safetyCheck;
    }

    
    /**
     * @param safetyCheck the safetyCheck to set
     */
    public void setSafetyCheck(VehicleSafetyCheck safetyCheck) {
        this.safetyCheck = safetyCheck;
    }

    
    /**
     * @return the checkResponse
     */
    public Integer getCheckResponse() {
        return checkResponse;
    }

    
    /**
     * @param checkResponse the checkResponse to set
     */
    public void setCheckResponse(Integer checkResponse) {
        this.checkResponse = checkResponse;
    }


    
    /**
     * @return the repairAction
     */
    public VehicleSafetyCheckRepairActionType getRepairAction() {
        return repairAction;
    }


    
    /**
     * @param repairAction the repairAction to set
     */
    public void setRepairAction(VehicleSafetyCheckRepairActionType repairAction) {
        this.repairAction = repairAction;
    }


    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    
    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    
    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    
    /**
     * Delegate method used in search
     * @return vehicle number
     * @see com.vocollect.voicelink.core.model.VehicleRoot#getVehicleNumber()
     */
    public String getVehicleNumber() {
        return vehicle.getVehicleNumber();
    }

    /**
     * @return the internalGroupId:
     */
    public Long getInternalGroupId() {
        return internalGroupId;
    }

    /**
     * @param internalGroupId the groupdId to set
     */
    public void setInternalGroupId(Long internalGroupId) {
        this.internalGroupId = internalGroupId;
    }


    /**
     * @see com.vocollect.epp.model.Taggable#getTags()
     * @return set of tags
     */
    @Override
    public Set<Tag> getTags() {
        return this.tags;
    }

    /** 
     * @param tags -  the tags associated with the objects
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VehicleSafetyCheckResponse)) {
            return false;
        }
        final VehicleSafetyCheckResponse other = (VehicleSafetyCheckResponse) obj;
        if (getVehicle() == null) {
            if (other.getVehicle() != null) {
                return false;
            }
        } else if (!getVehicle().equals(other.getVehicle())) {
            return false;
        }
        
        if (getSafetyCheck() == null) {
            if (other.getSafetyCheck() != null) {
                return false;
            }
        } else if (!getSafetyCheck().equals(other.getSafetyCheck())) {
            return false;
        }
        
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        
        if (getInternalGroupId() == null) {
            if (other.getInternalGroupId() != null) {
                return false;
            }
        } else if (!getInternalGroupId().equals(other.getInternalGroupId())) {
            return false;
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((vehicle == null) ? 0 : vehicle.hashCode());
        result = prime * result
            + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result
            + ((safetyCheck == null) ? 0 : safetyCheck.hashCode());
        result = prime * result
            + ((getCreatedDate() == null) ? 0 : getCreatedDate().hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 