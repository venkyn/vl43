/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * Translation class for Location Sorting.
 *
 */
public class LocationTranslationRoot {
    
    private Long                     id;
    
    private String postAisle;

    private String preAisle;


    //Properties for localized sorting
    private List<DataTranslation>    postAisleTrans;
    
    //Properties for localized sorting
    private List<DataTranslation>    preAisleTrans;

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the postAisle property.
     * @return String value of the property
     */
    public String getPostAisle() {
        return postAisle;
    }

    
    /**
     * Setter for the postAisle property.
     * @param postAisle the new postAisle value
     */
    public void setPostAisle(String postAisle) {
        this.postAisle = postAisle;
    }

    
    /**
     * Getter for the postAisleTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getPostAisleTrans() {
        return postAisleTrans;
    }

    
    /**
     * Setter for the postAisleTrans property.
     * @param postAisleTrans the new postAisleTrans value
     */
    public void setPostAisleTrans(List<DataTranslation> postAisleTrans) {
        this.postAisleTrans = postAisleTrans;
    }

    
    /**
     * Getter for the preAisle property.
     * @return String value of the property
     */
    public String getPreAisle() {
        return preAisle;
    }

    
    /**
     * Setter for the preAisle property.
     * @param preAisle the new preAisle value
     */
    public void setPreAisle(String preAisle) {
        this.preAisle = preAisle;
    }

    
    /**
     * Getter for the preAisleTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getPreAisleTrans() {
        return preAisleTrans;
    }

    
    /**
     * Setter for the preAisleTrans property.
     * @param preAisleTrans the new preAisleTrans value
     */
    public void setPreAisleTrans(List<DataTranslation> preAisleTrans) {
        this.preAisleTrans = preAisleTrans;
    }

    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 