/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


/**
 * Operator teams model object.
 *
 * @author mnichols
 */
public class OperatorTeamRoot extends CommonModelObject implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 6232268427718395269L;

    private String name = null;
    
    private Set<Operator> operators = null;

    private Set<Tag> tags;
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OperatorTeam)) {
            return false;
        }
        final OperatorTeam other = (OperatorTeam) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * Setter for the operators property.
     * @param operators the new operators value
     */
    public void setOperators(Set<Operator> operators) {
        this.operators = operators;
        
    }

    /**
     * Getter for the operators property.
     * @return the value of the property
     */
    public Set<Operator> getOperators() {
        return this.operators;
        
    }

    /**
     * Setter for the teamName property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
        
    }

    /**
     * Getter for the teamName property.
     * @return the value of the property
     */
    public String getName() {
        return this.name;
        
    }

    
    /**
     * @param operator - the operator to add
     */
    public void addOperator(Operator operator) {
        if (getOperators() == null) {
            setOperators(new HashSet<Operator>());
        }
        getOperators().add(operator);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    public Set<Tag> getTags() {
        return this.tags;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 