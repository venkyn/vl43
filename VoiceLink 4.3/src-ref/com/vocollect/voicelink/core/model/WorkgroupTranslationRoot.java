/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * Translation class for Work Group Sorting.
 *
 */
public class WorkgroupTranslationRoot {

    private Long        id;

    // Descriptive name
    private String groupName;
    
    //Properties for localized sorting
    private List<DataTranslation>    groupNameTrans;

    
    /**
     * Getter for the groupName property.
     * @return String value of the property
     */
    public String getGroupName() {
        return groupName;
    }

    
    /**
     * Setter for the groupName property.
     * @param groupName the new groupName value
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    
    /**
     * Getter for the groupNameTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getGroupNameTrans() {
        return groupNameTrans;
    }

    
    /**
     * Setter for the groupNameTrans property.
     * @param groupNameTrans the new groupNameTrans value
     */
    public void setGroupNameTrans(List<DataTranslation> groupNameTrans) {
        this.groupNameTrans = groupNameTrans;
    }

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 