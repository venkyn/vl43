/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * Reason Codes - often used for providing a pick discrepancy.
 * 
 * @author estoll
 */
public class ReasonCodeRoot extends CommonModelObject implements Serializable, Taggable {
    
    private static final long serialVersionUID = 6803572793703081858L;

    private TaskFunctionType taskFunctionType;
    
    private Integer reasonNumber;
    private String reasonDescription;
    
    private Set<Tag> tags;
    
    /**
     * @return reasonDescription
     */
    public String getReasonDescription() {
        return this.reasonDescription;
    }
    
    /**
     * @param reasonDescription value of the property
     */
    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }
    
    /**
     * @return reasonNumber
     */
    public Integer getReasonNumber() {
        return this.reasonNumber;
    }

    
    /**
     * @param reasonNumber value of the property
     */
    public void setReasonNumber(Integer reasonNumber) {
        this.reasonNumber = reasonNumber;
    }

    
    /**
     * @return taskApplication
     */
    public TaskFunctionType getTaskFunctionType() {
        return this.taskFunctionType;
    }

    
    /**
     * @param taskFunctionType value of the property
     */
    public void setTaskFunctionType(TaskFunctionType taskFunctionType) {
        this.taskFunctionType = taskFunctionType;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReasonCode)) {
            return false;
        }
        final ReasonCode other = (ReasonCode) obj;
        if (getReasonNumber() != other.getReasonNumber()) {
            return false;
        }
        return true;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return this.reasonNumber.intValue();
    }
    

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
       return Integer.toString(this.getReasonNumber());
    }
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 