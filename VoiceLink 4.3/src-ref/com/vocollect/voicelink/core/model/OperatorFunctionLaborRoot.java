/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.math.BigDecimal;


/**
 * 
 *
 * @author pfunyak
 */
public class OperatorFunctionLaborRoot extends OperatorLabor {

    //
    private static final long serialVersionUID = -4190579723309509573L;
    
    //Counter for tracking performance in function 4-7
    private Integer         count;
    
    // actual labor rate vs goal rate
    private Double          actualRate;
    
    // percentage of goal
    private Double          percentOfGoal;
    
    // goal rate at the time of data capture
    private Integer         goalRate;
    
    // the region the action was performed in.
    // will be null if signed into multiple regions
    private Region          region;
    
    /**
     * Getter for the count property.
     * @return int value of the property
     */
    public Integer getCount() {
        return this.count;
    }
    
    /**
     * Setter for the count property.
     * @param count the new count value
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        // TODO Rupert wants to remove this in phase 3
        if (this.actualRate != null) {
            return new BigDecimal(this.actualRate)
            .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        } else {
            return null;
        }
    }
    
    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    
    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        // TODO Rupert wants to remove this in phase 3
        if (this.percentOfGoal != null) {
            return new BigDecimal(this.percentOfGoal)
            .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        } else {
            return null;
        }
    }

    
    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }
  
    
    /**
     * Getter for the goalRate property.
     * @return Integer value of the property
     */
    public Integer getGoalRate() {
        return this.goalRate;
    }

    
    /**
     * Setter for the goalRate property.
     * @param goalRate the new goalRate value
     */
    public void setGoalRate(Integer goalRate) {
        this.goalRate = goalRate;
    }
    
    
    /**
     * Getter for the region property.
     * @return Region value of the property
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(Region region) {
        this.region = region;
    }
    
  
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((actualRate == null) ? 0 : actualRate.hashCode());
        result = prime * result + ((count == null) ? 0 : count.hashCode());
        result = prime * result + ((percentOfGoal == null) ? 0 : percentOfGoal.hashCode());
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }    
        if (!super.equals(obj)) {
            return false;
        }    
        if (getClass() != obj.getClass()) {
            return false;
        }    
        final OperatorFunctionLaborRoot other = (OperatorFunctionLaborRoot) obj;
        if (actualRate == null) {
            if (other.getActualRate() != null) {
                return false;
            }    
        } else if (!actualRate.equals(other.getActualRate())) {
            return false;
        }    
        if (count == null) {
            if (other.getCount() != null) {
                return false;
            }    
        } else if (!count.equals(other.getCount())) {
            return false;
        }    
        if (percentOfGoal == null) {
            if (other.getPercentOfGoal() != null) {
                return false;
            }    
        } else if (!percentOfGoal.equals(other.getPercentOfGoal())) {
            return false;
        }
        if (region == null) {
            if (other.getRegion() != null) {
                return false;
            }    
        } else if (!region.equals(other.getRegion())) {
            return false;
        }    
        
        return true;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 