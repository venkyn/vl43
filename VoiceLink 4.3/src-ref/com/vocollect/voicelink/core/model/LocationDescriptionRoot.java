/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.io.Serializable;

/**
 * This class is a component that groups together the fields that
 * make up the description of the Location that can be spoken
 * to the user.
 */
public class LocationDescriptionRoot implements Serializable {

    //
    private static final long serialVersionUID = 5269184521548015390L;

    private String aisle;

    private String postAisle;

    private String preAisle;

    private String slot;   
    
    private String direction;
    
    /**
     * Using the direction variable to concatenate values 
     * which were previously displayed using the Objects toString()method.
     * It is used in the table component and tool tip.
     * @return String value of the location direction
     */
    public String getDirection() {
        
        direction = " ";
        if (getPreAisle() != null && getPreAisle() != "") {
            direction += getPreAisle() + "-";
        }
        if (getAisle() != null && getAisle() != "") {
            direction += getAisle() + "-";
        }
        if (getPostAisle() != null && getPostAisle() != "") {
            direction += getPostAisle() + "-";
        }        
        if (getSlot() != null && getSlot() != "") {
            direction += getSlot();
        }
        
        int descrLength = direction.length();
        if (direction.charAt(descrLength - 1) == '-') {
            direction = direction.substring(0, descrLength - 1);
        }
       
        return direction;
    }

    /**
     * Getter for the aisle property.
     * @return String value of the property
     */
    public String getAisle() {
        return this.aisle;
    }

    /**
     * Getter for the postAisle property.
     * @return String value of the property
     */
    public String getPostAisle() {
        return this.postAisle;
    }

    /**
     * Getter for the preAisle property.
     * @return String value of the property
     */
    public String getPreAisle() {
        return this.preAisle;
    }

    /**
     * Getter for the slot property.
     * @return String value of the property
     */
    public String getSlot() {
        return this.slot;
    }

    /**
     * Setter for the aisle property.
     * @param aisle the new aisle value
     */
    public void setAisle(String aisle) {
        this.aisle = aisle;
    }

    /**
     * Setter for the postAisle property.
     * @param postAisle the new postAisle value
     */
    public void setPostAisle(String postAisle) {
        this.postAisle = postAisle;
    }

    /**
     * Setter for the preAisle property.
     * @param preAisle the new preAisle value
     */
    public void setPreAisle(String preAisle) {
        this.preAisle = preAisle;
    }

    /**
     * Setter for the slot property.
     * @param slot the new slot value
     */
    public void setSlot(String slot) {
        this.slot = slot;
    }    
      
   
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 