/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;


import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;


/**
 * Image of a Core LocationItem that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 * @author jtauberg
 */
public class LocationItemImportDataRoot  extends BaseModelObject implements Importable {

    private static final long serialVersionUID = -3284745929195542315L;

    private ImportableImpl impl = new ImportableImpl();
    
    private LocationItem myModel = new LocationItem();

    //Member specific to the Import
    // A = Add Mapping
    // D = Delete Mapping
    // R = Mark as Replenished
    private String action = null;
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        return this.myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setStatus(int)
     */
    public void setImportStatus(int importStatus) {
        impl.setImportStatus(importStatus);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return impl.toString();
    }

    
    
    //Method to set/get action (specific to import)
    
    /**
     * Set the action for this LocationItemImportData record.
     * A = Add Mapping
     * D = Delete Mapping
     * R = Mark as Replenished
     * @param action - 1 character String (see above).
     */
    public void setAction(String action) {
        myModel.setImportAction(action);
        this.action = action;
    }

    
    /**
     * Get the action for this LocationItemImportData record.
     * A = Add Mapping
     * D = Delete Mapping
     * R = Mark as Replenished
     * @return the action (String).
     */
    public String getAction() {
        return this.action;
    }
    
    
    //Helper methods for setting and getting complex members

    
    /**
     * Set the number in the Item. This will be used to look up an Item in the database.
     * @param itemId the ID to set the Item.number to.
     */
    public void setItemId(String itemId) {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        tempItem.setNumber(itemId);
    }

    
    /**
     * Get the Item Id (Item.number).
     * @return the Item Id (Item.number a String).
     */
    public String getItemId() {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        if (null == tempItem.getNumber()) {
            return null;
        }
        return tempItem.getNumber();
    }

    
    
    /**
     * Set the LocationID (scannedVerification) in the Location.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the Location.scannedVerification to.
     */
    public void setLocationId(String scannedVerification) {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }

    
    /**
     * Get the Location's id (scannedVerification).
     * @return the Location ID (Location.scannedVerification) (String).
     */
    public String getLocationId() {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }

    
    //Delegates from Replenishment model below

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationItemRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationItemRoot#getLocation()
     */
    public Location getLocation() {
        return myModel.getLocation();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationItemRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.LocationItemRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 