/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.util.HashMap;



/**
 * This object is used to pass data from a query to the export.
 *
 * @author jtauberg
 */
public abstract class ExportTransportObject {

    private HashMap<String, Object> fieldValues = new HashMap<String, Object>();

    private Object object = null;
    
    private Integer matchingStatus = null;

    /**
     * Constructor.
     */
    public ExportTransportObject() {
       super();
    }
    
    /**
     * Constructor.
     * @param fieldValues values for the fields
     * @param object the object that is the target of the export
     */
    public ExportTransportObject(HashMap<String, Object> fieldValues, Object object) {
       super();
       this.setObject(object);
       this.setFieldValues(fieldValues);
    }
    
    
    
    /**
     * Getter for the fieldValues property.
     * Usage - get the value by name, and call toString on it:
     * field.setFieldData(ETO.getFieldValues( field.getFieldName() ).toString());
     * @return value of the property
     */
    public HashMap<String, Object> getFieldValues() {
        return this.fieldValues;
    }

    
    /**
     * Setter for the fieldValues property.
     * 
     * Called by the constructor.
     * @param fieldValues the new fieldValues value
     */
    public void setFieldValues(HashMap<String, Object> fieldValues) {
        this.fieldValues = fieldValues;
    }

    
    /**
     * Getter for the object property.
     * @return Object value of the property
     */
    public Object getObject() {
        return this.object;
    }

    
    /**
     * Setter for the object property.
     * @param object the new object value
     */
    public void setObject(Object object) {
        this.object = object;
    }
    
    /**
     * Set appropriate fields to InProgress.
     * @param obj the Object
     */
    public abstract void updateExportToInProgress(Object obj);

    /**
     * Set appropriate fields to Exported.
     *  @param obj the Object
     */
    public abstract void updateExportToExported(Object obj);

    /**
     * Gets the value of matchingStatus.
     * @return the matchingStatus
     */
    public Integer getMatchingStatus() {
        return matchingStatus;
    }

    /**
     * Sets the value of the matchingStatus.
     * @param matchingStatus the matchingStatus to set
     */
    public void setMatchingStatus(Integer matchingStatus) {
        this.matchingStatus = matchingStatus;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 