/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.StringUtil;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ddoubleday
 */
public class ItemRoot  extends CommonModelObject implements Serializable, Taggable {

    private static final long serialVersionUID = 3737846579451322000L;

    //Item Number from WMS
    private String      number;
     
    //Viewable description of Item
    private String      description;
     
    //TTS Speakable description of item
    private String      phoneticDescription;
     
    //Item size
    private String      size;
     
    //Item pack
    private String      pack;
     
    //Item Weight
    private double      weight = 0;
     
    //Item Cube
    private double      cube = 0;
     
    //Item's UPC code
    private String      upc;
     
    //Determines if item is a variable weight item
    private boolean     isVariableWeightItem;
         
    //Tolerance allowed from specified weight
    private Integer     variableWeightTolerance = 0;
     
    //Scan code to verify correct item instead of check digits
    private String      scanVerificationCode;
     
    //Spoken code to verify item instead of check digits
    private String      spokenVerificationCode;
     
    //Determines if item requires serial number capture
    private boolean     isSerialNumberItem;

    private Set<Tag> tags;
    
    private Set<LocationItem> locationItems;
    
    private Set<Lot> lots;
    
    private int lotCount = 0;
    
    /**
     * Getter for the locationItems property.
     * @return Set&lt;LocationItem&gt; value of the property
     */
    public Set<LocationItem> getLocationItems() {
        if (locationItems == null) {
            locationItems = new HashSet<LocationItem>();
        }
        return locationItems;
    }

    
    /**
     * Setter for the locationItems property.
     * @param locationItems the new locationItems value
     */
    public void setLocationItems(Set<LocationItem> locationItems) {
        this.locationItems = locationItems;
    }

    /**
     * Getter for the cube property.
     * @return double value of the property
     */
    public double getCube() {
        return this.cube;
    }
    
    /**
     * Setter for the cube property.
     * @param cube the new cube value
     */
    public void setCube(double cube) {
        this.cube = cube;
    }
    
    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return this.description;
    }
    
    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    /**
     * Getter for the isVariableWeightItem property.
     * @return boolean value of the property
     */
    public boolean getIsVariableWeightItem() {
        return this.isVariableWeightItem;
    }

    /**
     * Setter for the isVariableWeightItem property.
     * @param isVariableWeightItem the new isVariableWeightItem value
     */
    public void setIsVariableWeightItem(boolean isVariableWeightItem) {
        this.isVariableWeightItem = isVariableWeightItem;
    }

    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return this.number;
    }
    
    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }

    
    /**
     * Getter for the pack property.
     * @return String value of the property
     */
    public String getPack() {
        return this.pack;
    }
    
    /**
     * Setter for the pack property.
     * @param pack the new pack value
     */
    public void setPack(String pack) {
        this.pack = pack;
    }
    
    /**
     * Getter for the phoneticDescription property.
     * @return If phonetic description has a non empty string value, return it.
     * If phonetic description is empty string or null, return the description
     * field (this may be null).
     */
    public String getPhoneticDescription() {
        //If phoneticDescription is "" or null
        if (StringUtil.isNullOrBlank(this.phoneticDescription)) {
            return getDescription();
        } else {
            // return phonetic description
            return this.phoneticDescription;
        }
    }
    
    /**
     * Setter for the phoneticDescription property.
     * @param phoneticDescription the new phoneticDescription value
     */
    public void setPhoneticDescription(String phoneticDescription) {
        this.phoneticDescription = phoneticDescription;
    }
    
    /**
     * Getter for the scanVerificationCode property.
     * @return String value of the property
     */
    public String getScanVerificationCode() {
        return this.scanVerificationCode;
    }
    
    /**
     * Setter for the scanVerificationCode property.
     * @param scanVerificationCode the new scanVerificationCode value
     */
    public void setScanVerificationCode(String scanVerificationCode) {
        this.scanVerificationCode = scanVerificationCode;
    }
    
    /**
     * Getter for the size property.
     * @return String value of the property
     */
    public String getSize() {
        return this.size;
    }
    
    /**
     * Setter for the size property.
     * @param size the new size value
     */
    public void setSize(String size) {
        this.size = size;
    }
    
    /**
     * Getter for the spokenVerificationCode property.
     * @return String value of the property
     */
    public String getSpokenVerificationCode() {
        return this.spokenVerificationCode;
    }
    
    /**
     * Setter for the spokenVerificationCode property.
     * @param spokenVerificationCode the new spokenVerificationCode value
     */
    public void setSpokenVerificationCode(String spokenVerificationCode) {
        this.spokenVerificationCode = spokenVerificationCode;
    }
    
    /**
     * Getter for the upc property.
     * @return String value of the property
     */
    public String getUpc() {
        return this.upc;
    }
    
    /**
     * Setter for the upc property.
     * @param upc the new upc value
     */
    public void setUpc(String upc) {
        this.upc = upc;
    }
    
    /**
     * Getter for the variableWeightTolerance property.
     * @return Integer value of the property
     */
    public Integer getVariableWeightTolerance() {
        return this.variableWeightTolerance;
    }
    
    /**
     * Setter for the variableWeightTolerance property.
     * @param variableWeightTolerance the new variableWeightTolerance value
     */
    public void setVariableWeightTolerance(Integer variableWeightTolerance) {
        this.variableWeightTolerance = variableWeightTolerance;
    }
    
    /**
     * Getter for the weight property.
     * @return double value of the property
     */
    public double getWeight() {
        return this.weight;
    }
    
    /**
     * Setter for the weight property.
     * @param weight the new weight value
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    
    
    /**
     * Getter for the isSerialNumberItem property.
     * @return boolean value of the property
     */
    public boolean getIsSerialNumberItem() {
        return isSerialNumberItem;
    }

    
    /**
     * Setter for the isSerialNumberItem property.
     * @param isSerialNumberItem the new isSerialNumberItem value
     */
    public void setIsSerialNumberItem(boolean isSerialNumberItem) {
        this.isSerialNumberItem = isSerialNumberItem;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Item)) {
            return false;
        }
        final Item other = (Item) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.number == null ? 0 : this.number.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getNumber();
    }
    
    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    /**
     * @return the lots
     */
    public Set<Lot> getLots() {
        return lots;
    }


    /**
     * @param lots the lots to set
     */
    public void setLots(Set<Lot> lots) {
        this.lots = lots;
    }


    /**
     * @return the lotCount
     */
    public int getLotCount() {
        return lotCount;
    }


    /**
     * @param lotCount the lotCount to set
     */
    public void setLotCount(int lotCount) {
        this.lotCount = lotCount;
    }   

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 