/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * 
 * @author khazra
 */
public class VehicleSafetyCheckRoot extends CommonModelObject implements
    Serializable, Taggable {

    //
    private static final long serialVersionUID = 7250251904471043936L;

    // Sequence in which the checks will be presented
    private Integer sequence;

    // Descriptive check text
    private String description;

    // Check text spoken in task
    private String spokenDescription;

    // The type for which this safety check is
    private VehicleType vehicleType;

    // Distinguisher between numeric and boolaen reponse
    private SafetyCheckType responseType = SafetyCheckType.STOPONNO;
    
    private String responseTypeStringValue;

    private Set<Tag> tags;

    /**
     * Getter for the sequence property.
     * @return Integer value of the property
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     * Setter for the sequence property.
     * @param sequence the new sequence value
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the spokenDescription property.
     * @return String value of the property
     */
    public String getSpokenDescription() {
        return spokenDescription;
    }

    /**
     * Setter for the spokenDescription property.
     * @param spokenDescription the new spokenDescription value
     */
    public void setSpokenDescription(String spokenDescription) {
        this.spokenDescription = spokenDescription;
    }

    /**
     * Getter for the vehicleType property.
     * @return VehicleType value of the property
     */
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    /**
     * Setter for the vehicleType property.
     * @param vehicleType the new vehicleType value
     */
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    /**
     * @return the responseType
     */
    public SafetyCheckType getResponseType() {
        return responseType;
    }

    /**
     * @param responseType the responseType to set
     */
    public void setResponseType(SafetyCheckType responseType) {
        this.responseType = responseType;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getSpokenDescription();
    }

    /**
     * Primarily used in search
     * @return Vehicle type description
     */
    public String getVehicleTypeDescription() {
        return this.vehicleType.getDescription();
    }
    
    /**
     * Getter for the responseTypeStringValue property.
     * @return String value of the property
     */
    public String getResponseTypeStringValue() {
        if (this.getResponseType() == SafetyCheckType.CONTINUEONNO) {
            return ResourceUtil.getLocalizedKeyValue("com.vocollect.voicelink.core.model.SafetyCheckType.1");
        } else if (this.getResponseType() == SafetyCheckType.STOPONNO) {
            return ResourceUtil.getLocalizedKeyValue("com.vocollect.voicelink.core.model.SafetyCheckType.2");
        } else {
            return ResourceUtil.getLocalizedKeyValue("com.vocollect.voicelink.core.model.SafetyCheckType.3");
        }
    }    

    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VehicleSafetyCheck)) {
            return false;
        }
        final VehicleSafetyCheck other = (VehicleSafetyCheck) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.getId() == null ? 0 : this.getId().hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 