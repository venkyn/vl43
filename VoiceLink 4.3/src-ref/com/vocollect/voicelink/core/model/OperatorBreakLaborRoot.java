/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


/**
 * 
 *
 * @author pfunyak
 */
public class OperatorBreakLaborRoot extends OperatorLabor {

    private static final long serialVersionUID = -3461107746357130531L;

    //Type of break if function is Break
    private BreakType               breakType;
    
    //Save the OperatorLabor object to record what the operator was
    //doing prior to going on break.
    private OperatorLabor           previousOperatorLabor;
   
    
    /**
     * Getter for the breakType property.
     * @return BreakType value of the property
     */
    public BreakType getBreakType() {
        return this.breakType;
    }

    
    /**
     * Setter for the breakType property.
     * @param breakType the new breakType value
     */
    public void setBreakType(BreakType breakType) {
        this.breakType = breakType;
    }

   
    /**
     * Getter for the previousOperatorLabor property.
     * @return OperatorLabor value of the property
     */
    public OperatorLabor getPreviousOperatorLabor() {
        return this.previousOperatorLabor;
    }

    
    /**
     * Setter for the previousOperatorLabor property.
     * @param previousOperatorLabor the new previousOperatorLabor value
     */
    public void setPreviousOperatorLabor(OperatorLabor previousOperatorLabor) {
        this.previousOperatorLabor = previousOperatorLabor;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((breakType == null) ? 0 : breakType.hashCode());
        result = prime * result + ((previousOperatorLabor == null) ? 0 : previousOperatorLabor.hashCode());
        return result;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }    
        if (getClass() != obj.getClass()) {
            return false;
        }    
        final OperatorBreakLabor other = (OperatorBreakLabor) obj;
        if (breakType == null) {
            if (other.getBreakType() != null) {
                return false;
            }    
        } else if (!breakType.equals(other.getBreakType())) {
            return false;
        }    
        if (previousOperatorLabor == null) {
            if (other.getPreviousOperatorLabor() != null) {
                return false;
            }    
        } else if (!previousOperatorLabor.equals(other.getPreviousOperatorLabor())) {
            return false;
        }    
        return true;
    }
    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 