/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DeviceCommon;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.VersionedModelObject;

import java.util.Locale;
import java.util.Set;

/**
 * Model object representing a Device, or device.
 * 
 * @author ddoubleday
 */
public class DeviceRoot extends VersionedModelObject
    implements java.io.Serializable, Taggable {

    private static final long serialVersionUID = 5749560550980850124L;

    // Operator currently loaded on terminal
    private Operator operator;

    // Locale of task currently loaded on terminal
    private Locale taskLocale;

    // TaskVersion in use
    private String taskVersion;

    // The common Device
    private DeviceCommon common;

    /**
     * Constructor.
     */
    public DeviceRoot() {
        common = new DeviceCommon();
    }

    /**
     * @return the serial number of the device
     * @see com.vocollect.epp.model.DeviceCommonRoot#getSerialNumber()
     */
    public String getSerialNumber() {
        return common.getSerialNumber();
    }

    /**
     * @param serialNumber the serial number
     * @see com.vocollect.epp.model.DeviceCommonRoot#setSerialNumber(java.lang.String)
     */
    public void setSerialNumber(String serialNumber) {
        common.setSerialNumber(serialNumber);
    }

    /**
     * Getter for the common property.
     * @return DeviceCommonRoot value of the property
     */
    public DeviceCommon getCommon() {
        return common;
    }

    /**
     * Setter for the common property.
     * @param common the new common value
     */
    public void setCommon(DeviceCommon common) {
        this.common = common;
    }

    /**
     * Getter for the taskLocale property.
     * @return String value of the property
     */
    public Locale getTaskLocale() {
        return this.taskLocale;
    }

    /**
     * Setter for the taskLocale property.
     * @param locale the new taskLocale value
     */
    public void setTaskLocale(Locale locale) {
        this.taskLocale = locale;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Device)) {
            return false;
        }
        final Device other = (Device) obj;

        if (!(getCommon().equals(other.getCommon()))) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.getSerialNumber() == null ? 0 : this.getSerialNumber()
            .hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getSerialNumber();
    }

    /**
     * Getter for the taskVersion property.
     * @return String value of the property
     */
    public String getTaskVersion() {
        return this.taskVersion;
    }

    /**
     * Setter for the taskVersion property.
     * @param taskVersion the new taskVersion value
     */
    public void setTaskVersion(String taskVersion) {
        this.taskVersion = taskVersion;
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return this.common.getTags();
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.common.setTags(tags);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 