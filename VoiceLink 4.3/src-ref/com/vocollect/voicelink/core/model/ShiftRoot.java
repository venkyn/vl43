/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * 
 * @author smittal
 */
public class ShiftRoot extends CommonModelObject implements
    Serializable, Taggable {

    //
    private static final long serialVersionUID = -7872244807295670361L;

    private String name;
    
    private ShiftTime startTime;
    
    private ShiftTime endTime;
    
    private Set<Tag> tags;

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the startTime property.
     * @return ShiftTime value of the property
     */
    public ShiftTime getStartTime() {
        return startTime;
    }

    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(ShiftTime startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for the endTime property.
     * @return ShiftTime value of the property
     */
    public ShiftTime getEndTime() {
        return endTime;
    }

    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(ShiftTime endTime) {
        this.endTime = endTime;
    }

    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Shift)) {
            return false;
        }
        final Shift other = (Shift) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.getName() == null ? 0 : this.getName().hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 