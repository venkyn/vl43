/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

//import com.vocollect.voicelink.selection.model.Assignment;
//import com.vocollect.voicelink.selection.model.AssignmentStatus;
//import com.vocollect.voicelink.selection.model.Container;

/**
 * @author dgold
 *
 */
public class OperatorActionsExportTransport extends ExportTransportObject {

    /**
     * {@inheritDoc}
     * @param obj
     */
    @Override
    public void updateExportToExported(Object obj) {
        OperatorLabor ol = (OperatorLabor) obj;
        ol.setExportStatus(ExportStatus.Exported);
    }

    /**
     * {@inheritDoc}
     * @param obj
     */
    @Override
    public void updateExportToInProgress(Object obj) {
        OperatorLabor ol = (OperatorLabor) obj;
        ol.setExportStatus(ExportStatus.InProgress);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 