/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * Translation class for Reason Code Sorting.
 *
 */
public class ReasonCodeTranslationRoot {

    private Long        id;
    
    private String      reasonDescription;
    
    //Properties for localized sorting
    private List<DataTranslation>    reasonDescriptionTrans;

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the reasonDescription property.
     * @return String value of the property
     */
    public String getReasonDescription() {
        return reasonDescription;
    }

    
    /**
     * Setter for the reasonDescription property.
     * @param reasonDescription the new reasonDescription value
     */
    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    
    /**
     * Getter for the reasonDescriptionTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getReasonDescriptionTrans() {
        return reasonDescriptionTrans;
    }

    
    /**
     * Setter for the reasonDescriptionTrans property.
     * @param reasonDescriptionTrans the new reasonDescriptionTrans value
     */
    public void setReasonDescriptionTrans(List<DataTranslation> reasonDescriptionTrans) {
        this.reasonDescriptionTrans = reasonDescriptionTrans;
    }

    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 