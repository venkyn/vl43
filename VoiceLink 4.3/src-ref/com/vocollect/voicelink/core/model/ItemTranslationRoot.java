/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * @author ddoubleday
 */
public class ItemTranslationRoot  {

    private Long                     id;
    
    //Viewable description of Item
    private String      description = "No Description";
     
    //TTS Speakable description of item
    private String      phoneticDescription = "No Description";

    //Properties for localized sorting
    private List<DataTranslation>    descriptionTrans;
    
    private List<DataTranslation>    phoneticDescriptionTrans;
        
    /**
     * Getter for the descriptionTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getDescriptionTrans() {
        return descriptionTrans;
    }

    
    /**
     * Setter for the descriptionTrans property.
     * @param descriptionTrans the new descriptionTrans value
     */
    public void setDescriptionTrans(List<DataTranslation> descriptionTrans) {
        this.descriptionTrans = descriptionTrans;
    }

    
    /**
     * Getter for the phoneticDescriptionTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getPhoneticDescriptionTrans() {
        return phoneticDescriptionTrans;
    }

    
    /**
     * Setter for the phoneticDescriptionTrans property.
     * @param phoneticDescriptionTrans the new phoneticDescriptionTrans value
     */
    public void setPhoneticDescriptionTrans(List<DataTranslation> phoneticDescriptionTrans) {
        this.phoneticDescriptionTrans = phoneticDescriptionTrans;
    }
    
    
    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }


    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }


    
    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return description;
    }


    
    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }


    
    /**
     * Getter for the phoneticDescription property.
     * @return String value of the property
     */
    public String getPhoneticDescription() {
        return phoneticDescription;
    }


    
    /**
     * Setter for the phoneticDescription property.
     * @param phoneticDescription the new phoneticDescription value
     */
    public void setPhoneticDescription(String phoneticDescription) {
        this.phoneticDescription = phoneticDescription;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 