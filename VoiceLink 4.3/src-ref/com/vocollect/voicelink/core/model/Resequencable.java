/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


/**
 * Interface for resequencable objects.  Mostly a marker interface
 * 
 * @author bnichols
 */
public interface Resequencable {
    /**
     * Getter for the id property.
     * @return the peristent ID for this object, which will be <code>null</code>
     * if the object has never be persisted.
     */
    public Long getId();    
    
    /**
     *
     * @return the new sequence number associated with the objects
     */
    public Long getNewSequenceNumber();
    
    
    /**
     * @param newSequenceNumber - the new sequence number associated 
     * with the objects
     */
    public void setNewSequenceNumber(Long newSequenceNumber);
    
    /**
     * Getter for the originalSequenceNumber property.
     * @return Long value of the property
     */
    public Long getOriginalSequenceNumber();

        
    /**
     * Getter for the sequenceNumber property.
     * @return Long value of the property
     */
    public Long getSequenceNumber();
    
    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(Long sequenceNumber);
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 