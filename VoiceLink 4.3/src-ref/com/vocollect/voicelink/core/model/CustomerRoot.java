/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.io.Serializable;


/**
 * 
 *
 * @author mkoenig
 */
public class CustomerRoot implements Serializable {

    //
    private static final long serialVersionUID = 3511832235736368267L;

    //Customer Number from WMS, Imported
    private String            customerNumber;
    
    //Customer Name from WMS, Imported
    private String            customerName;
    
    //Customer Address from WMS, Imported
    private String            customerAddress;

    
    /**
     * Getter for the customerAddress property.
     * @return String value of the property
     */
    public String getCustomerAddress() {
        return this.customerAddress;
    }

    
    /**
     * Setter for the customerAddress property.
     * @param customerAddress the new customerAddress value
     */
    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    
    /**
     * Getter for the customerName property.
     * @return String value of the property
     */
    public String getCustomerName() {
        return this.customerName;
    }

    
    /**
     * Setter for the customerName property.
     * @param customerName the new customerName value
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    
    /**
     * Getter for the customerNumber property.
     * @return String value of the property
     */
    public String getCustomerNumber() {
        return this.customerNumber;
    }

    
    /**
     * Setter for the customerNumber property.
     * @param customerNumber the new customerNumber value
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 