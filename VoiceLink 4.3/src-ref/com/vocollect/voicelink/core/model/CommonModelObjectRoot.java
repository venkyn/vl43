/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.VersionedModelObject;

import java.util.Date;



/**
 * 
 *
 * @author ddoubleday
 */
public abstract class CommonModelObjectRoot extends VersionedModelObject {

    private Date    createdDate;
    
    /**
     * Getter for the createdDate property.
     * @return Date value of the property
     */
    public Date getCreatedDate() {
        // This is lazily evaluated. When a model object is being persisted,
        // this will be called and set with the current time. This is preferable
        // to initializing the date at instance creation time, since the object
        // may not be persisted until some time after creation.
        if (this.createdDate == null) {
            this.createdDate = new Date();
        }
        return this.createdDate;
    }

    
    /**
     * Setter for the createdDate property.
     * @param createdDate the new createdDate value
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 