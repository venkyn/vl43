/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author khazra
 * 
 */
public class VehicleTypeRoot extends CommonModelObject implements Serializable,
    Taggable {

    private static final long serialVersionUID = -8423439382199161195L;

    // The Vehicle type number or just Vehicle type
    private Integer number;

    // The description of the type
    private String description;

    // Determines if vehicle id needs to be captured for this type. Default is
    // true
    private boolean isCaptureVehicleID = false;

    // Collection of vehicles belonging to this vehicle type
    private List<Vehicle> vehicles = new ArrayList<Vehicle>();

    // Collection of vehicle safety checks belonging to this vehicle type
    private List<VehicleSafetyCheck> safetyChecks = new ArrayList<VehicleSafetyCheck>();

    // Count of number of vehicles having this vehicle type
    private Integer vehicleCount;

    // Safety check count for this vehicle type
    private Integer safetyCheckCount;

    private Set<Tag> tags;

    /**
     * @return the number
     */
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the captureVehicleID property.
     * @return boolean value of the property
     */
    public boolean getIsCaptureVehicleID() {
        return isCaptureVehicleID;
    }

    /**
     * Setter for the captureVehicleID property.
     * @param isCaptureVehicleID the new CaptureVehicleID value
     */
    public void setIsCaptureVehicleID(boolean isCaptureVehicleID) {
        this.isCaptureVehicleID = isCaptureVehicleID;
    }

    /**
     * @return the vehicles
     */
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    /**
     * @param vehicles the vehicles to set
     */
    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     * @return the safetyChecks
     */
    public List<VehicleSafetyCheck> getSafetyChecks() {
        return safetyChecks;
    }

    /**
     * @param safetyChecks the safetyChecks to set
     */
    public void setSafetyChecks(List<VehicleSafetyCheck> safetyChecks) {
        this.safetyChecks = safetyChecks;
    }

    /**
     * @return the vehicleCount
     */
    public Integer getVehicleCount() {
        return getVehicles().size();
    }

    /**
     * @param vehicleCount the vehicleCount to set
     */
    public void setVehicleCount(Integer vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    /**
     * @return the safetyCheckCount
     */
    public Integer getSafetyCheckCount() {
        return getSafetyChecks().size();
    }

    /**
     * @param safetyCheckCount the safetyCheckCount to set
     */
    public void setSafetyCheckCount(Integer safetyCheckCount) {
        this.safetyCheckCount = safetyCheckCount;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getDescription();
    }    

    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VehicleType)) {
            return false;
        }
        final VehicleType other = (VehicleType) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.number == null ? 0 : this.number.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 