/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.voicelink.core.importer.FieldMap;

/**
 * Lookup trigger that fires for every line : similar implementation as the EveryLineCreateObject trigger.
 * @author dgold
 *
 */
public class LineLookupTrigger extends LookupTrigger {

    /**
     * Default constructor.
     */
    public LineLookupTrigger() {
        // Nothing special.
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        String result = null;
        // openContext writes <obj>, and any fields marked for openContext (unless useAllFields is true)
        result = super.openContext(xmlRepresentation, fields);
        result = closeContext(result, fields);
        return result;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 