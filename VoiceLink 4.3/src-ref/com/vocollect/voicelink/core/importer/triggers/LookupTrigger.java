/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.TriggerErrors;

import java.util.ArrayList;
import java.util.List;

/**
 * A trigger that looks up its target in the database.
 * The default behavior is to succeed if the entity being looked up is in the DB.
 * Typical use:
 * An entity is imported, and part of its composition is another object, which should
 * already be present in the DB. Thw lookup, on success, extracts the entity from the DB
 * and replaces the one used for the search with the one found in the DB. 
 * We are using this approach because you can't copy the data fomr one object to another, the 
 * if from the DB does not transfer. Oh, well.
 * 
 * The behavior for this trigger can be inverted: setting the failIfFound flag will cause 
 * the trigger to fail (throw an exception) if the entity IS present in the DB, which is 
 * how we'll enforce no-duplicates in the import.
 * 
 * When the trigger 'fails', an exception is thrown.
 * 
 * @author dgold
 *
 */
public class LookupTrigger implements CreateObjectTrigger {
    
    private static final Logger log = new Logger(LookupTrigger.class);
    
    private FieldMap keyFields = new FieldMap(); 


    // Default behavior, sufficient for many things.
    private CreateObjectTriggerImpl guts = new CreateObjectTriggerImpl();

    // Lookup Default is to succeed regardless if the entity is found or not..
    // Setting this to true causes the lookup to fail if the entity is present.
    private boolean failIfFound = false;
    
    // Setting this to true causes the lookup to fail if the entity is not present.
    private boolean failIfNotFound = false;
    
    // Setting this to true causes the trigger to first check all the key fields.
    // If the keys are all empty (or null), it will set the Subject to null and
    // not do the lookup.
    //Default behavior is NOT to do this (false).
    private boolean setNullIfKeysAreEmpty = false;
    
    
    // Name of the finder method for the trigger to use.
    private String finderName = null;
    
    /**
     * default constructor.
     */
    public LookupTrigger() {
        // Nothing special to do.
    }
    
    /**
     * Check that the trigger is configured appropriately, and throw a runtime if not.
     */
    public void verifyConfiguration() {
        if ((null == finderName) 
                || (0 == finderName.length())) {
            log.error("Finder name not set for lookup in trigger for " + this.getNewObjectName(),
                      TriggerErrors.NO_FINDER_SPECIFIED);
                      
            throw new RuntimeException("Finder name not set for lookup");
        }
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        return guts.detectTriggeringConditions(fields);
    }
    
    /**
     * Gets the value of keyFields.
     * @return the keyFields
     */
    public FieldMap getKeyFields() {
        return keyFields;
    }

    /**
     * Sets the value of the keyFields.
     * @param keyFields the keyFields to set
     */
    public void setKeyFields(FieldMap keyFields) {
        this.keyFields = keyFields;
    }

    /**
     * Look up the subject in the database, and depending on the parameter fail if absent or present.
     * @param subject the object we're completing
     * @param objectFinder the DLookup object to use to look up this object.
     * @return the object, with parts completed.
     * @throws VocollectException if the object cannot be gotten or set
     */
    public Object lookupObject(Object subject, DBLookup objectFinder) throws VocollectException {
        Object result = null;

        // This is the object we want to look up in the database
        
        
        //If flag is set, check to see if all the key fields are empty or null
        //if they are, set the Subject to null and return.
        if (this.isSetNullIfKeysAreEmpty()) {
            boolean allEmpty = true;
            Object theObject = null;
            
            for (Field field : this.getKeyFields().values()) {
                //Get the getter for this keyfield
                theObject = DBLookup.getCompositePart(subject, field.getGetterName());
                //if keyfield in subject isn't null && it isn't empty string
                if (theObject != null && (0 != theObject.toString().compareTo(""))) {
                    //allEmpty is false... do the lookup.
                    allEmpty = false;
                    break;
                }
            }
            if (allEmpty) {
                // SetNullIfKeysAreEmpty and keys are empty, so...
                // set subject to null
                subject = null;
                // don't do lookup and return null object
                return result;
            }
        }
        
        
        verifyConfiguration();
        
//      Recurse for all of the triggers under this one.
        guts.lookupObject(subject, objectFinder);
        
        // if failIfFound, throw an exception if found, otherwise if not found throw the exception.
        if (this.isFailIfFound()) {
            result = lookupAssumingNotFound(subject, objectFinder);
        } 
        if (isFailIfNotFound()) {
            result = lookupAssumingFind(subject, objectFinder);
        }
        if (!(isFailIfNotFound() || isFailIfFound())) {
            List<Object> resultList = null;
            // Use the object finder to look up the object
            resultList = objectFinder.lookup(subject, this.getFinderName(), this.getKeyFields());
            if ((null != resultList) && (!resultList.isEmpty())) {
                result = resultList.get(0);
            } else {
                result = subject;
            }
        }
        
        if (isMainObject()) {
            subject = result;
        }
        
        return result;
    }
    
    /**
     * Look up the subject in the database, and fail if absent.
     * @param subject the object we're completing
     * @param objectFinder the DLookup object to use to look up this object.
     * @return the object, with parts completed.
     * @throws VocollectException if the object cannot be found, or the methods [get|set]{object} are not found
     */
    @SuppressWarnings("unchecked")
    private Object lookupAssumingFind(Object subject, DBLookup objectFinder) throws VocollectException {
        List<Object> resultList = null;
        // Use the object finder to look up the object
        resultList = objectFinder.lookup(subject, this.getFinderName(), this.getKeyFields());
        if (resultList.isEmpty()) {
            log.error("Object " + getNewObjectName() 
                    + " was not found when expecting to find said object. Finder: " + this.getFinderName() 
                    + " Parameters: " 
                    + parametersToString((DataObject) subject, getKeyFields()),
                       TriggerErrors.EXPECTED_TO_FIND_SUBJECT);
            throw new LookupException(TriggerErrors.EXPECTED_TO_FIND_SUBJECT, 
                new UserMessage(LocalizedMessage.formatMessage(
                                TriggerErrors.EXPECTED_TO_FIND_SUBJECT, 
                                new Object[]{this.getNewObjectName(),
                                parametersToString((DataObject) subject, getKeyFields())})));
        }
        return resultList.get(0);
    }

    /**
     * Look up the subject in the database, and fail if present.
     * @param subject the object we're completing
     * @param objectFinder the DLookup object to use to look up this object.
     * @return the object, with parts completed.
     * @throws VocollectException if the object is found
     */
    @SuppressWarnings("unchecked")
    private Object lookupAssumingNotFound(Object subject, DBLookup objectFinder) throws VocollectException {
        List<Object> resultList = 
            objectFinder.lookup(subject, getFinderName(), getKeyFields());
        if (!resultList.isEmpty()) {
            log.error("Object " + getNewObjectName() + " was found when not expecting to find said object."
                    + " Parameters: " 
                    + parametersToString((DataObject) subject, getKeyFields()),
                    TriggerErrors.EXPECTED_NOT_TO_FIND_SUBJECT);
            throw new LookupException(TriggerErrors.EXPECTED_NOT_TO_FIND_SUBJECT, 
                new UserMessage(LocalizedMessage.formatMessage(
                    TriggerErrors.EXPECTED_NOT_TO_FIND_SUBJECT, new Object[]{this.getNewObjectName(),
                            parametersToString((DataObject) subject, getKeyFields())})));
        }
        return subject;
    }

    /**
     * Gets the value of failIfFound.
     * @return the failIfFound
     */
    public boolean isFailIfFound() {
        return failIfFound;
    }


    /**
     * Sets the value of the failIfFound.
     * @param failIfFound the failIfFound to set
     */
    public void setFailIfFound(boolean failIfFound) {
        this.failIfFound = failIfFound;
    }


    /**
     * Gets the value of finderName.
     * @return the finderName
     */
    public String getFinderName() {
        return finderName;
    }


    /**
     * Sets the value of the finderName.
     * @param finderName the finderName to set
     */
    public void setFinderName(String finderName) {
        this.finderName = finderName;
    }

    /**
     * Render the lookup parameters to a string, for error reporting purposes.
     * @param exampleObject The object submitted as an example.
     * @param paramList list of field names used
     * @return the string representing field-value pairs as : [field:value][field:value]...
     * @throws VocollectException when DBLookup.getCompositePart fails
     */
    @SuppressWarnings("unchecked")
    protected String parametersToString(DataObject exampleObject, FieldMap paramList) 
    throws VocollectException {

        String result = "";
        
        // Build an array of values from the exampleObject that matches the fields passed in
        for (Field field : paramList.values()) {
            Object part = DBLookup.getCompositePart(exampleObject, field.getGetterName());
            String objectValue = ""; 
            if (null == part) {
                objectValue = "null";
            } else {
                objectValue = part.toString(); 
            }
                result += "[" + field.getGetterName() + ":" 
                            + objectValue
                            + "]";
        }
        
        return result;
    }

    /**
     * Add a field designated ad KeyField to the lookup field map.
     * @param aField the field to add to the key list.
     */
    public void addKeyField(Field aField) {
        this.keyFields.addField(aField);
    }

    /**
     * This is doing the same thing as the implementation (guts), but I want to use this.addField.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setFieldMap(FieldMap fieldMapP) {
        for (Field field : fieldMapP.values()) {
            addField(field);
        }
    }


    /**
     * Add the field, and if it's a key field, add to list of keys.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        guts.addField(aField);
        if (aField.isKeyField()) {
            addKeyField(aField);
        }
    }


    
//------------------------------Delegate methods---------------------------------------//    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String closeContext(String openContext, FieldMap fields) {
        return guts.closeContext(openContext, fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        return guts.createObjectIfTriggered(fields, xmlString);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseContextFields()
     */
    public FieldMap getCloseContextFields() {
        return guts.getCloseContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()
     */
    public String getCloseTag() {
        return guts.getCloseTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()
     */
    public FieldMap getFieldMap() {
        return guts.getFieldMap();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()
     */
    public String getNewObjectName() {
        return guts.getNewObjectName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenContextFields()
     */
    public FieldMap getOpenContextFields() {
        return guts.getOpenContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()
     */
    public String getOpenTag() {
        return guts.getOpenTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()
     */
    public boolean getUseAllFields() {
        return guts.getUseAllFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isLookup()
     */
    public boolean isLookup() {
        return guts.isLookup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()
     */
    public boolean isOpen() {
        return guts.isOpen();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        return guts.openContext(xmlRepresentation, fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setCloseContextFields(FieldMap fields) {
        guts.setCloseContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)
     */
    public void setCloseTag(String newObjectTagContents) {
        guts.setCloseTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)
     */
    public void setIsOpen(boolean isThisOpen) {
        guts.setIsOpen(isThisOpen);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setLookup(boolean)
     */
    public void setLookup(boolean lookup) {
        guts.setLookup(lookup);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)
     */
    public void setNewObjectName(String newObjectname) {
        guts.setNewObjectName(newObjectname);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setOpenContextFields(FieldMap fields) {
        guts.setOpenContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)
     */
    public void setOpenTag(String newObjectTagContents) {
        guts.setOpenTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)
     */
    public void setUseAllFields(boolean useAll) {
        guts.setUseAllFields(useAll);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#writeFields(com.vocollect.voicelink.core.importer.FieldMap, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String writeFields(FieldMap fields, 
                              FieldMap fieldsToWrite) {
        return guts.writeFields(fields, fieldsToWrite);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addTrigger(com.vocollect.voicelink.core.importer.CreateObjectTrigger)
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        guts.addTrigger(newTrigger);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getTriggers()
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return guts.getTriggers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#fireSympatheticTriggers(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        return guts.fireSympatheticTriggers(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#reset()
     */
    public void reset() {
        guts.reset();
    }

    /**
     * Gets the value of list.
     * @return the list
     */
    public String getList() {
        return guts.getList();
    }

    /**
     * Sets the value of the list.
     * @param list the list to set
     */
    public void setList(String list) {
        guts.setList(list);
    }

    /**
     * True if this trigger's objects are in a list in the main object.
     * @return the useList
     */
    public boolean isUseList() {
        return guts.isUseList();
    }

    /**
     * Sets the value of the useList.
     * @param useList the useList to set
     */
    public void setUseList(boolean useList) {
        guts.setUseList(useList);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isMainObject()
     */
    public boolean isMainObject() {
        return guts.isMainObject();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setMainObject(boolean)
     */
    public void setMainObject(boolean mainFlag) {
        guts.setMainObject(mainFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getGetterName()
     */
    public String getGetterName() {
        return guts.getGetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getSetterName()
     */
    public String getSetterName() {
        return guts.getSetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setGetterName(java.lang.String)
     */
    public void setGetterName(String getterName) {
        guts.setGetterName(getterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSetterName(java.lang.String)
     */
    public void setSetterName(String setterName) {
        guts.setSetterName(setterName);
    }

    /**
     * Gets the value of failIfNotFound.
     * @return the failIfNotFound
     */
    public boolean isFailIfNotFound() {
        return failIfNotFound;
    }

    /**
     * Sets the value of the failIfNotFound.
     * @param failIfNotFound the failIfNotFound to set
     */
    public void setFailIfNotFound(boolean failIfNotFound) {
        this.failIfNotFound = failIfNotFound;
    }

    
    /**
     * @return the setNullIfKeysAreEmpty
     */
    public boolean isSetNullIfKeysAreEmpty() {
        return setNullIfKeysAreEmpty;
    }

    
    /**
     * Setting this to true causes the trigger to first check all the key fields.
     * If the keys are all empty (or null), it will set the Subject to null and
     * not do the lookup.
     * 
     * @param setNullIfKeysAreEmpty the setNullIfKeysAreEmpty to set
     */
    public void setSetNullIfKeysAreEmpty(boolean setNullIfKeysAreEmpty) {
        this.setNullIfKeysAreEmpty = setNullIfKeysAreEmpty;
    }

    /**
     * @return boolean
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isSuppress()
     */
    public boolean isSuppress() {
        return guts.isSuppress();
    }

    /**
     * @param isSuppressed the suppressed flag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        guts.setSuppress(isSuppressed);
    }

    
//    /**
//     * {@inheritDoc}
//     */
//    public Object lookupObject(Collection subject, DBLookup objectFinder) 
//        throws VocollectException {
//        for (Object bugger : subject) {
//            lookupObject(bugger, objectFinder);
//        }
//        return subject;
//    }
//
//
//    /**
//     * {@inheritDoc}
//     */
//    public Object recurseLookups(Object subject, DBLookup objectFinder) throws VocollectException {
//
//        Object thisTriggersSubject = DBLookup.getCompositePart(subject, getGetterName());
//        try {
//            lookupObject((Collection) thisTriggersSubject, objectFinder);
//        } catch (ClassCastException e) {
//            // Expected - not a collection
//        }
//        lookupObject(thisTriggersSubject, objectFinder);
//        return null;
//    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 