/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Trigger that will insert an object if triggered and the object is not present in the DB. 
 * Otherwise, it will use the object found in the DB.
 * This trigger is FieldChange Trigger. For first record it gets trigger
 * open the context , save the FM. When change occurs again it close the prev context 
 * using the previous FM, open new context and Save the new FM.
 * @author Kalpna
 *
 */
public class ConditionalInsertTrigger implements CreateObjectTrigger {

    private LookupTrigger guts = new LookupTrigger();
    
    // Map of field-name-to-value of fields where a change in the field value triggers a new object
    private HashMap<String, String> triggeringFields;

    //Hold the last context fieldMap
    private FieldMap prevContextFieldMap = null;
    
    private boolean openUntilTriggered = false;
        
    /**
     * Default constructor.    
     */
    public ConditionalInsertTrigger() {
        this.setUseAllFields(false);
    }

    /**
     * If there was a value change for any of the fields listed in triggeringFields, return true.
     * @param fields list of fields from the incoming data
     * @return true if there was a change in the value for any of the fields in triggeringFields
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        boolean retval = false;

        for (String fieldName : triggeringFields.keySet()) {

            Field tf = fields.get(fieldName);
            if (null != tf) {
                String data = triggeringFields.get(fieldName);
                if ((null == data) || (0 != tf.getFieldData().compareTo(data))) {
                    // There was a value change, rturn true
                    retval = true;
                    // save the value
                    triggeringFields.put(fieldName, tf.getFieldData());
                }
            }
        }
        return retval;
    }
    
    /**
     * Get the list of fields which will trigger a new object when a value changes.
     * @return the triggeringFields
     */
    public HashMap<String, String> getTriggeringFields() {
        return triggeringFields;
    }

    /**
     * Add the field to the list of triggering fields.
     * @param aField the field top add
     */
    public void addTriggeringField(Field aField) {
        if (null == triggeringFields) {
            triggeringFields = new HashMap<String, String>();
        }
        triggeringFields.put(aField.getFieldName(), "");
    }

    /**
     * Set the list of fields which will trigger a new object when a value changes.
     * @param triggeringFields the triggeringFields to set
     */
    public void setTriggeringFields(HashMap<String, String> triggeringFields) {
        this.triggeringFields = triggeringFields;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        guts.addField(aField);
        if (aField.isKeyField()) {
            this.addTriggeringField(aField);
            this.addKeyField(aField);
        }
        
    }

    /**
     * Create the new object in XML, adding the fields listed in openContextFields.
     * @param xmlRepresentation the XML context passed in
     * @param fields the list of fields from the input source
     * @return the new XMl with the opened context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        
        //Hold on to the last FieldMap
        this.setPrevContextFieldMap(fields);  
        // Open the context...
        xmlRepresentation = guts.openContext(xmlRepresentation, fields);
        return xmlRepresentation;
    }

    /**
     * Close the XMl context for this type.
     * @param openContext specifies the context as a String
     * @param fields the list of fields from the input source for the current record - it ignores this
     * and uses the previous context fieldMap to close the this context.
     * @return the new XMl with the closing of the context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String closeContext(String openContext, FieldMap fields) {
        // for every string in the close-context field, add it to the data stream        
        openContext = guts.closeContext(openContext, getPrevContextFieldMap());

        return openContext;
    }

    /**
     * open a new object context if appropriate.
     * @param fields list of fields from the input source
     * @param xmlString The XML string to append to.
     * @return the new XML with the open context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        if (this.detectTriggeringConditions(fields)) {
            return xmlString + openContext(xmlString, fields);
        }
        return xmlString;
    }


    //  -----------------------Delegates, simple getters & setters-------------------//

    /**
     * Get the list of fields used to populate the new object.
     * @return the openContextFields as a HashMap of Strings
     */
    public FieldMap getOpenContextFields() {
        return guts.getOpenContextFields();
    }

    /**
     * Get the list fo fields to be used when closing the context.
     * @return the closeContextFields as a HashMap of Strings
     */
    public FieldMap getCloseContextFields() {
        return guts.getCloseContextFields();
    }

    /**
     * Pass-thru to delegate. Get the close-tag for this object.
     * @return the close-tag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()
     */
    public String getCloseTag() {
        return guts.getCloseTag();
    }

    /**
     * Pass-thru to delegate. get the open-tag 
     * @return the open-tag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()
     */
    public String getOpenTag() {
        return guts.getOpenTag();
    }

    /**
     * Pass-thru to delegate. Get the new object name for this object.
     * @return the new object name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()
     */
    public String getNewObjectName() {
        return guts.getNewObjectName();
    }


    /**
     * Pass-thru to delegate - set the Open tag.
     * @param newObjectTagContents - the Open tag, including the object name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)
     */
    public void setOpenTag(String newObjectTagContents) {
        guts.setOpenTag(newObjectTagContents);
    }

    /**
     * Simple setter for Open flag - delegated.
     * @param isThisOpen new value for isOpen
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)
     */
    public void setIsOpen(boolean isThisOpen) {
        guts.setIsOpen(isThisOpen);
    }

    /**
     * @return the Field Map that was orignially passed to the trigger.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()
     */
    public FieldMap getFieldMap() {
        return guts.getFieldMap();
    }

    /**
     * Set the flag that says this trigger uses all of the fields when writing to the XML stream.
     * 
     * @return the flag as above.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()
     */
    public boolean getUseAllFields() {
        return guts.getUseAllFields();
    }

    /**
     * Set the fields this trigger uses to write data to the XML stream.
     * @param fieldMapP the map of fields this trigger has defined.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setFieldMap(FieldMap fieldMapP) {
        guts.setFieldMap(fieldMapP);
    }

    /**
     * Set the flag that says this trigger uses all of the fields when writing to the XML stream.
     * @param useAll flag that indicates this trigger uses all of the fields when writing.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)
     */
    public void setUseAllFields(boolean useAll) {
        guts.setUseAllFields(useAll);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#lookupObject(java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)
     */
    public Object lookupObject(Object owner, DBLookup objectCompleter) throws VocollectException {
        verifyConfiguration();

        for (CreateObjectTrigger trigger : this.getTriggers()) {
            Object thisTriggersSubject = DBLookup.getCompositePart(owner, trigger.getGetterName());
            thisTriggersSubject = trigger.lookupObject(thisTriggersSubject, objectCompleter);
            DBLookup.setCompositePart(owner, thisTriggersSubject, trigger.getSetterName());
        }

        return objectCompleter.conditionalInsert((DataObject) owner, getFinderName(), this.getKeyFields());

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setCloseContextFields(FieldMap fields) {
        guts.setCloseContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)
     */
    public void setCloseTag(String newObjectTagContents) {
        guts.setCloseTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setLookup(boolean)
     */
    public void setLookup(boolean lookup) {
        guts.setLookup(lookup);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isLookup()
     */
    public boolean isLookup() {
        return guts.isLookup();
    }

    /**
     * Pass-thru to delegate, get the Open flag.
     * @return true if the context is open, false otherwise.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()
     */
    public boolean isOpen() {
        return guts.isOpen();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)
     */
    public void setNewObjectName(String newObjectname) {
        guts.setNewObjectName(newObjectname);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setOpenContextFields(FieldMap fields) {
        guts.setOpenContextFields(fields);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#writeFields(com.vocollect.voicelink.core.importer.FieldMap, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String writeFields(FieldMap fields, 
                              FieldMap fieldsToWrite) {
        return guts.writeFields(fields, fieldsToWrite);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addTrigger(com.vocollect.voicelink.core.importer.CreateObjectTrigger)
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        guts.addTrigger(newTrigger);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getTriggers()
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return guts.getTriggers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#fireSympatheticTriggers(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        return guts.fireSympatheticTriggers(fields);
    }

    /**
     * Clear the data from the list of triggers. Can be used if the trigger is set off in error.
     */
    public void reset() {
        HashMap<String, String> temp = new HashMap<String, String>();
        for (String name : triggeringFields.keySet()) {
            temp.put(name, null);
        }
        triggeringFields.clear();
        triggeringFields.putAll(temp);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isMainObject()
     */
    public boolean isMainObject() {
        return guts.isMainObject();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setMainObject(boolean)
     */
    public void setMainObject(boolean mainFlag) {
        guts.setMainObject(mainFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isUseList()
     */
    public boolean isUseList() {
        return guts.isUseList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setUseList(boolean)
     */
    public void setUseList(boolean useList) {
        guts.setUseList(useList);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getList()
     */
    public String getList() {
        return guts.getList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setList(java.lang.String)
     */
    public void setList(String list) {
        guts.setList(list);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getGetterName()
     */
    public String getGetterName() {
        return guts.getGetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getSetterName()
     */
    public String getSetterName() {
        return guts.getSetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setGetterName(java.lang.String)
     */
    public void setGetterName(String getterName) {
        guts.setGetterName(getterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSetterName(java.lang.String)
     */
    public void setSetterName(String setterName) {
        guts.setSetterName(setterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#addKeyField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addKeyField(Field aField) {
        guts.addKeyField(aField);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#getFinderName()
     */
    public String getFinderName() {
        return guts.getFinderName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#getKeyFields()
     */
    public FieldMap getKeyFields() {
        return guts.getKeyFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#isFailIfFound()
     */
    public boolean isFailIfFound() {
        return guts.isFailIfFound();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setFailIfFound(boolean)
     */
    public void setFailIfFound(boolean failIfFound) {
        guts.setFailIfFound(failIfFound);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setFinderName(java.lang.String)
     */
    public void setFinderName(String finderName) {
        guts.setFinderName(finderName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setKeyFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setKeyFields(FieldMap keyFields) {
        guts.setKeyFields(keyFields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#verifyConfiguration()
     */
    public void verifyConfiguration() {
        guts.verifyConfiguration();
    }

    /**
     * This method maintains a list of FieldMap using which xml contexts are created.
     * This could be used later to get some reporting information.
     * @param fields FieldMap
     */
    public void setPrevContextFieldMap(FieldMap fields) {
        this.prevContextFieldMap = fields;
    }
    
    /**
     * Getter for the prevContextFieldMap.
     * @return list of FieldMap
     */
    public FieldMap getPrevContextFieldMap() {
          return this.prevContextFieldMap;          
    }
    
    /**
     * Gets the value of openUntilTriggered.
     * @return the openUntilTriggered
     */
    public boolean isOpenUntilTriggered() {
        return openUntilTriggered;
    }

    /**
     * Sets the value of the openUntilTriggered.
     * @param openUntilTriggered the openUntilTriggered to set
     */
    public void setOpenUntilTriggered(boolean openUntilTriggered) {
        this.openUntilTriggered = openUntilTriggered;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        this.guts.setSuppress(isSuppressed);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isSuppress()
     */
    public boolean isSuppress() {
        return this.guts.isSuppress();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 