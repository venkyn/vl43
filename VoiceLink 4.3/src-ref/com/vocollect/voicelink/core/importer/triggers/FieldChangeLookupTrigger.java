/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import java.util.HashMap;

/**
 * Trigger that fires based on field value changes, but looks up its data from the DB.
 * @author dgold
 *
 */
public class FieldChangeLookupTrigger extends LookupTrigger {
    
    private HashMap<String, String> triggeringFields = new HashMap<String, String>();
    //  Map of field-name-to-value of fields where a change in the field value triggers a new object



    /**
     * Default c'tor.
     */
    public FieldChangeLookupTrigger() {
        super();
    }

    //------------ Pirated form the FieldChangeCreateObjectTrigger------------
    /**
     * If there was a value change for any of the fields listed in triggeringFields, return true.
     * @param fields list of fields from the incoming data
     * @return true if there was a change in the value for any of the fields in triggeringFields
     */
    @Override
    public boolean detectTriggeringConditions(FieldMap fields) {
        boolean retval = false;

        for (String fieldName : triggeringFields.keySet()) {

            Field tf = fields.get(fieldName);
            if (null != tf) {
                String data = triggeringFields.get(fieldName);
                if ((null == data) || (0 != tf.getFieldData().compareTo(data))) {
                    // There was a value change, rturn true
                    retval = true;
                    // save the value
                    triggeringFields.put(fieldName, tf.getFieldData());
                }
            }
        }
        return retval;
    }

    /**
     * Get the list of fields which will trigger a new object when a value changes.
     * @return the triggeringFields
     */
    public HashMap<String, String> getTriggeringFields() {
        return triggeringFields;
    }

    /**
     * Add the field to the list of triggering fields.
     * @param aField the field top add
     */
    public void addTriggeringField(Field aField) {
        if (null == triggeringFields) {
            triggeringFields = new HashMap<String, String>();
        }
        triggeringFields.put(aField.getFieldName(), "");
    }

    /**
     * Set the list of fields which will trigger a new object when a value changes.
     * @param triggeringFields the triggeringFields to set
     */
    public void setTriggeringFields(HashMap<String, String> triggeringFields) {
        this.triggeringFields = triggeringFields;
    }

    /**
     * Clear the data from the list of triggers. Can be used if the trigger is set off in error.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#reset()
     */
    @Override
    public void reset() {
        HashMap<String, String> temp = new HashMap<String, String>();
        for (String name : triggeringFields.keySet()) {
            temp.put(name, null);
        }
        triggeringFields.clear();
        triggeringFields.putAll(temp);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    @Override
    public void addField(Field aField) {
        super.addField(aField);
        if (aField.isKeyField()) {
            this.addTriggeringField(aField);
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 