/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;


/**
 * 
 *
 * @author jtauberg
 */
@SuppressWarnings("serial")
public class LookupException extends VocollectException {
    

    /**
     * Default empty constructor.
     */
    public LookupException() {
        super();
        // Nothing to do
    }

    /**
     * @param code specifies the ErrorCode object
     * @param msg specifies the UserMessage object
     */
    public LookupException(ErrorCode code, UserMessage msg) {
        super(code, msg);
        // Nothing to do
    }

    /**
     * @param code specifies the ErrorCode object
     * @param msg specifies the UserMessage object
     * @param t specifies the Throwable object
     */
    public LookupException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
        // Nothing to do
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 