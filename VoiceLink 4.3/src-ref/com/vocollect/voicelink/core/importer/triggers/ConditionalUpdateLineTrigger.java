/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.FieldMap;

/**
 * Object trigger that will insert a new object if one is not present in the DB,
 * or update the existing match with the data contained herein.
 * @author dgold
 *
 */
public class ConditionalUpdateLineTrigger extends LookupTrigger {
    
//    private static final Logger log = new Logger(ConditionalUpdateLineTrigger.class);


    /**
     * Default constructor.
     */
    public ConditionalUpdateLineTrigger() {
        // Nothing special
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        String result = null;
        // openContext writes <obj>, and any fields marked for openContext (unless useAllFields is true)
        result = super.openContext(xmlRepresentation, fields);
        result = closeContext(result, fields);
        return result;
    }
    
    /**
     * Call lookupObject for every nested trigger.
     * @param subject the object to look up
     * @param objectCompleter the thing we would use to look up this entity in the DB
     * @return whatever we get
     * @throws VocollectException when we look something up and find it (or not) when expected
     */
    public Object lookupComposite(Object subject, DBLookup objectCompleter) throws VocollectException {
        // Nothing, just pass on through.
        for (CreateObjectTrigger trigger : this.getTriggers()) {
            trigger.lookupObject(subject, objectCompleter);
        }
        return subject;
    }



    /**
     * Look up the subject in the database, and depending on the parameter fail if absent or present.
     * @param owner the object we're completing
     * @param objectFinder the DLookup object to use to look up this object.
     * @return the object, with parts completed.
     * @throws VocollectException if the object cannot be gotten or set
     */
    public Object lookupObject(Object owner, DBLookup objectFinder) throws VocollectException {

        
        for (CreateObjectTrigger trigger : this.getTriggers()) {
            Object thisTriggersSubject = DBLookup.getCompositePart(owner, trigger.getGetterName());
            thisTriggersSubject = trigger.lookupObject(thisTriggersSubject, objectFinder);
            DBLookup.setCompositePart(owner, thisTriggersSubject, trigger.getSetterName());
        }

        // If this is the main object trigger, we won't save - the Importer will.
        return objectFinder.conditionalUpdate((DataObject) owner, 
                getFinderName(), getKeyFields(), getFieldMap(), isMainObject());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 