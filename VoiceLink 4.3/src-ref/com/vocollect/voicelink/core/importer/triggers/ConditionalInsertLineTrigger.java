/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import java.util.ArrayList;

/**
 * Trigger that will insert an object if triggered and the object is not present in the DB. 
 * Otherwise, it will use the object found in the DB.
 * @author dgold
 *
 */
public class ConditionalInsertLineTrigger implements CreateObjectTrigger {

    private LineLookupTrigger guts = new LineLookupTrigger();
    
//    private PersistenceManager pm = new PersistenceManager();

//    private DBLookup dbLookup = new DBLookup(pm);

    // Setting this to true causes the trigger to first check all the key fields.
    // If the keys are all empty (or null), it will set the Subject to null and
    // not do the lookup.
    //Default behavior is NOT to do this (false).
    private boolean setNullIfKeysAreEmpty = false;
     
    
    /**
     * Default constructor.
     */
    public ConditionalInsertLineTrigger() {
        // No action
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        guts.addField(aField);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String closeContext(String openContext, FieldMap fields) {
        return guts.closeContext(openContext, fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        return guts.createObjectIfTriggered(fields, xmlString);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        return guts.detectTriggeringConditions(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseContextFields()
     */
    public FieldMap getCloseContextFields() {
        return guts.getCloseContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()
     */
    public String getCloseTag() {
        return guts.getCloseTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()
     */
    public FieldMap getFieldMap() {
        return guts.getFieldMap();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()
     */
    public String getNewObjectName() {
        return guts.getNewObjectName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenContextFields()
     */
    public FieldMap getOpenContextFields() {
        return guts.getOpenContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()
     */
    public String getOpenTag() {
        return guts.getOpenTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()
     */
    public boolean getUseAllFields() {
        return guts.getUseAllFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isLookup()
     */
    public boolean isLookup() {
        return guts.isLookup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()
     */
    public boolean isOpen() {
        return guts.isOpen();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#lookupObject(java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)
     */
    public Object lookupObject(Object owner, DBLookup objectCompleter) throws VocollectException {
        verifyConfiguration();

        for (CreateObjectTrigger trigger : this.getTriggers()) {
            Object thisTriggersSubject = DBLookup.getCompositePart(owner, trigger.getGetterName());
            thisTriggersSubject = trigger.lookupObject(thisTriggersSubject, objectCompleter);
            DBLookup.setCompositePart(owner, thisTriggersSubject, trigger.getSetterName());
        }

        
        //If flag is set, check to see if all the key fields are empty or null
        //if they are, set the owner to null and return.
        if (this.isSetNullIfKeysAreEmpty()) {
            boolean allEmpty = true;
            Object theObject = null;
            
            for (Field field : this.getKeyFields().values()) {
                //Get the getter for this keyfield
                theObject = DBLookup.getCompositePart(owner, field.getGetterName());
                //if keyfield in owner isn't null && it isn't empty string
                if (theObject != null && (0 != theObject.toString().compareTo(""))) {
                    //allEmpty is false... do the lookup.
                    allEmpty = false;
                    break;
                }
            }
            if (allEmpty) {
                // SetNullIfKeysAreEmpty and keys are empty, so...
                // set owner to null
                owner = null;
                // don't do conditional insert and return null object
                return null;
            }
        }
        
        
        return objectCompleter.conditionalInsert((DataObject) owner, getFinderName(), this.getKeyFields());

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        return guts.openContext(xmlRepresentation, fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setCloseContextFields(FieldMap fields) {
        guts.setCloseContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)
     */
    public void setCloseTag(String newObjectTagContents) {
        guts.setCloseTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setFieldMap(FieldMap fieldMapP) {
        guts.setFieldMap(fieldMapP);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)
     */
    public void setIsOpen(boolean isThisOpen) {
        guts.setIsOpen(isThisOpen);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setLookup(boolean)
     */
    public void setLookup(boolean lookup) {
        guts.setLookup(lookup);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)
     */
    public void setNewObjectName(String newObjectname) {
        guts.setNewObjectName(newObjectname);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setOpenContextFields(FieldMap fields) {
        guts.setOpenContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)
     */
    public void setOpenTag(String newObjectTagContents) {
        guts.setOpenTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)
     */
    public void setUseAllFields(boolean useAll) {
        guts.setUseAllFields(useAll);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#writeFields(com.vocollect.voicelink.core.importer.FieldMap, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String writeFields(FieldMap fields, 
                              FieldMap fieldsToWrite) {
        return guts.writeFields(fields, fieldsToWrite);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addTrigger(com.vocollect.voicelink.core.importer.CreateObjectTrigger)
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        guts.addTrigger(newTrigger);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getTriggers()
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return guts.getTriggers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#fireSympatheticTriggers(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        return guts.fireSympatheticTriggers(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#reset()
     */
    public void reset() {
        guts.reset();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isMainObject()
     */
    public boolean isMainObject() {
        return guts.isMainObject();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setMainObject(boolean)
     */
    public void setMainObject(boolean mainFlag) {
        guts.setMainObject(mainFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isUseList()
     */
    public boolean isUseList() {
        return guts.isUseList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setUseList(boolean)
     */
    public void setUseList(boolean useList) {
        guts.setUseList(useList);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getList()
     */
    public String getList() {
        return guts.getList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setList(java.lang.String)
     */
    public void setList(String list) {
        guts.setList(list);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getGetterName()
     */
    public String getGetterName() {
        return guts.getGetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getSetterName()
     */
    public String getSetterName() {
        return guts.getSetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setGetterName(java.lang.String)
     */
    public void setGetterName(String getterName) {
        guts.setGetterName(getterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSetterName(java.lang.String)
     */
    public void setSetterName(String setterName) {
        guts.setSetterName(setterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#addKeyField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addKeyField(Field aField) {
        guts.addKeyField(aField);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#getFinderName()
     */
    public String getFinderName() {
        return guts.getFinderName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#getKeyFields()
     */
    public FieldMap getKeyFields() {
        return guts.getKeyFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#isFailIfFound()
     */
    public boolean isFailIfFound() {
        return guts.isFailIfFound();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setFailIfFound(boolean)
     */
    public void setFailIfFound(boolean failIfFound) {
        guts.setFailIfFound(failIfFound);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setFinderName(java.lang.String)
     */
    public void setFinderName(String finderName) {
        guts.setFinderName(finderName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setKeyFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setKeyFields(FieldMap keyFields) {
        guts.setKeyFields(keyFields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#verifyConfiguration()
     */
    public void verifyConfiguration() {
        guts.verifyConfiguration();
    }

    
    /**
     * @return the setNullIfKeysAreEmpty
     */
    public boolean isSetNullIfKeysAreEmpty() {
        return setNullIfKeysAreEmpty;
    }

    
    /**
     * Setting this to true causes the trigger to first check all the key fields.
     * If the keys are all empty (or null), it will set the Subject to null and
     * not do the lookup.
     * Default behavior is NOT to do this (false).
     * 
     * @param setNullIfKeysAreEmpty the setNullIfKeysAreEmpty to set
     */
    public void setSetNullIfKeysAreEmpty(boolean setNullIfKeysAreEmpty) {
        this.setNullIfKeysAreEmpty = setNullIfKeysAreEmpty;
    }

    /**
     * @return boolean
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#isSuppress()
     */
    public boolean isSuppress() {
        return guts.isSuppress();
    }

    /**
     * @param isSuppressed the suppressed flag
     * @see com.vocollect.voicelink.core.importer.triggers.LookupTrigger#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        guts.setSuppress(isSuppressed);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 