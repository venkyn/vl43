/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * This object will trigger if there is a difference in the value of any field listed in triggeringFields.
 * The openContextFields are used to populate the new object being created.
 * 
 * We need to be able to pass in lists of fields.
 * 
 * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger
 * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl
 * 
 * @author dgold
 *
 */
public class FieldChangeCreateObjectTrigger implements CreateObjectTrigger {

    private HashMap<String, String> triggeringFields;
        //  Map of field-name-to-value of fields where a change in the field value triggers a new object

    private CreateObjectTriggerImpl basis = new CreateObjectTriggerImpl(); 
        // Delegate object. No sense in re-doing all of the simple getter/setter stuff.
    
    private boolean openUntilTriggered = false;

    /**
     * Default constructor.
     */
    public FieldChangeCreateObjectTrigger() {
        this.setUseAllFields(false);

    }

    /**
     * If there was a value change for any of the fields listed in triggeringFields, return true.
     * @param fields list of fields from the incoming data
     * @return true if there was a change in the value for any of the fields in triggeringFields
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        boolean retval = false;

        for (String fieldName : triggeringFields.keySet()) {

            Field tf = fields.get(fieldName);
            if (null != tf) {
                String data = triggeringFields.get(fieldName);
                if ((null == data) || (0 != tf.getFieldData().compareTo(data))) {
                    // There was a value change, rturn true
                    retval = true;
                    // save the value
                    triggeringFields.put(fieldName, tf.getFieldData());
                }
            }
        }
        return retval;
    }

    /**
     * Create the new object in XML, adding the fields listed in openContextFields.
     * @param xmlRepresentation the XML context passed in
     * @param fields the list of fields from the input source
     * @return the new XMl with the opened context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        // Open the context...
        return basis.openContext(xmlRepresentation, fields);
    }

    /**
     * Close the XMl context for this type.
     * @param openContext specifies the context as a String
     * @param fields the list of fields from the input source
     * @return the new XMl with the closing of the context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String closeContext(String openContext, FieldMap fields) {
        // for every string in the close-context field, add it to the data stream
        return basis.closeContext(openContext, fields);
    }
    
    /**
     * Write the fields from fields which are listed in fieldsToWrite to a string.
     * @param fields list of fields from the input source
     * @param fieldsToWrite list of fields to write
     * @return a string containing the apporpriate markup.
     */
    public String writeFields(FieldMap fields, 
                              FieldMap fieldsToWrite) {
        return basis.writeFields(fields, fieldsToWrite);
    }



    /**
     * open a new object context if appropriate.
     * @param fields list of fields from the input source
     * @param xmlString The XML string to append to.
     * @return the new XML with the open context appended.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        if (this.detectTriggeringConditions(fields)) {
            return xmlString + openContext(xmlString, fields);
        }
        return xmlString;
    }

    //  -----------------------Delegates, simple getters & setters-------------------//

    /**
     * Get the list of fields used to populate the new object.
     * @return the openContextFields as a HashMap of Strings
     */
    public FieldMap getOpenContextFields() {
        return basis.getOpenContextFields();
    }

    /**
     * Get the list fo fields to be used when closing the context.
     * @return the closeContextFields as a HashMap of Strings
     */
    public FieldMap getCloseContextFields() {
        return basis.getCloseContextFields();
    }

    /**
     * Get the list of fields which will trigger a new object when a value changes.
     * @return the triggeringFields
     */
    public HashMap<String, String> getTriggeringFields() {
        return triggeringFields;
    }

    /**
     * Add the field to the list of triggering fields.
     * @param aField the field top add
     */
    public void addTriggeringField(Field aField) {
        if (null == triggeringFields) {
            triggeringFields = new HashMap<String, String>();
        }
        triggeringFields.put(aField.getFieldName(), "");
    }

    /**
     * Set the list of fields which will trigger a new object when a value changes.
     * @param triggeringFields the triggeringFields to set
     */
    public void setTriggeringFields(HashMap<String, String> triggeringFields) {
        this.triggeringFields = triggeringFields;
    }

    /**
     * Pass-thru to delegate. Get the close-tag for this object.
     * @return the close-tag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()
     */
    public String getCloseTag() {
        return basis.getCloseTag();
    }

    /**
     * Pass-thru to delegate. Get the new object name for this object.
     * @return the new object name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()
     */
    public String getNewObjectName() {
        return basis.getNewObjectName();
    }

    /**
     * Pass-thru to delegate. get the open-tag 
     * @return the open-tag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()
     */
    public String getOpenTag() {
        return basis.getOpenTag();
    }

    /**
     * Pass-thru to delegate, get the Open flag.
     * @return true if the context is open, false otherwise.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()
     */
    public boolean isOpen() {
        return basis.isOpen();
    }

    /**
     * Pass-thru to delegate - get the close-tag.
     * @param newObjectTagContents The tag, including the name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)
     */
    public void setCloseTag(String newObjectTagContents) {
        basis.setCloseTag(newObjectTagContents);
    }

    /**
     * Pass-thru to delegate - set the new object name.
     * @param newObjectTag - the tag, including the name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)
     */
    public void setNewObjectName(String newObjectTag) {
        basis.setNewObjectName(newObjectTag);
    }

    /**
     * Pass-thru to delegate - set the Open tag.
     * @param newObjectTagContents - the Open tag, including the object name
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)
     */
    public void setOpenTag(String newObjectTagContents) {
        basis.setOpenTag(newObjectTagContents);
    }

    /**
     * Simple setter for Open flag - delegated.
     * @param isThisOpen new value for isOpen
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)
     */
    public void setIsOpen(boolean isThisOpen) {
        basis.setIsOpen(isThisOpen);
    }

    /**
     * @return the Field Map that was orignially passed to the trigger.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()
     */
    public FieldMap getFieldMap() {
        return basis.getFieldMap();
    }

    /**
     * Set the flag that says this trigger uses all of the fields when writing to the XML stream.
     * 
     * @return the flag as above.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()
     */
    public boolean getUseAllFields() {
        return basis.getUseAllFields();
    }

    /**
     * Set the fields this trigger uses to write data to the XML stream.
     * @param fieldMapP the map of fields this trigger has defined.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setFieldMap(FieldMap fieldMapP) {
        basis.setFieldMap(fieldMapP);
    }

    /**
     * Set the flag that says this trigger uses all of the fields when writing to the XML stream.
     * @param useAll flag that indicates this trigger uses all of the fields when writing.
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)
     */
    public void setUseAllFields(boolean useAll) {
        basis.setUseAllFields(useAll);
    }

    /**
     * Set the list fo fields to be used when closing the context.
     * @param fields the closeContextFields to set
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setCloseContextFields(FieldMap fields) {
        basis.setCloseContextFields(fields);
    }

    /**
     * Set the list of fields used to populate the new object that are written when we open the XMl context.
     * @param fields the openContextFields to set
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setOpenContextFields(FieldMap fields) {
        basis.setOpenContextFields(fields);
    }

    /**
     * Clear the data from the list of triggers. Can be used if the trigger is set off in error.
     */
    public void reset() {
        HashMap<String, String> temp = new HashMap<String, String>();
        for (String name : triggeringFields.keySet()) {
            temp.put(name, null);
        }
        triggeringFields.clear();
        triggeringFields.putAll(temp);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isLookup()
     */
    public boolean isLookup() {
        return basis.isLookup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setLookup(boolean)
     */
    public void setLookup(boolean lookup) {
        basis.setLookup(lookup);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        basis.addField(aField);
        if (aField.isKeyField()) {
            this.addTriggeringField(aField);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#lookupObject(java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)
     */
    public Object lookupObject(Object subject, DBLookup objectCompleter) throws VocollectException {
        return basis.lookupObject(subject, objectCompleter);
    }

    /**
     * Gets the value of openUntilTriggered.
     * @return the openUntilTriggered
     */
    public boolean isOpenUntilTriggered() {
        return openUntilTriggered;
    }

    /**
     * Sets the value of the openUntilTriggered.
     * @param openUntilTriggered the openUntilTriggered to set
     */
    public void setOpenUntilTriggered(boolean openUntilTriggered) {
        this.openUntilTriggered = openUntilTriggered;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addTrigger(com.vocollect.voicelink.core.importer.CreateObjectTrigger)
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        basis.addTrigger(newTrigger);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getTriggers()
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return basis.getTriggers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#fireSympatheticTriggers(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        return basis.fireSympatheticTriggers(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isMainObject()
     */
    public boolean isMainObject() {
        return basis.isMainObject();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setMainObject(boolean)
     */
    public void setMainObject(boolean mainFlag) {
        basis.setMainObject(mainFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isUseList()
     */
    public boolean isUseList() {
        return basis.isUseList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setUseList(boolean)
     */
    public void setUseList(boolean useList) {
        basis.setUseList(useList);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getList()
     */
    public String getList() {
        return basis.getList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setList(java.lang.String)
     */
    public void setList(String list) {
        basis.setList(list);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getGetterName()
     */
    public String getGetterName() {
        return basis.getGetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getSetterName()
     */
    public String getSetterName() {
        return basis.getSetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setGetterName(java.lang.String)
     */
    public void setGetterName(String getterName) {
        basis.setGetterName(getterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSetterName(java.lang.String)
     */
    public void setSetterName(String setterName) {
        basis.setSetterName(setterName);
    }

    /**
     * @return boolean
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isSuppress()
     */
    public boolean isSuppress() {
        return basis.isSuppress();
    }

    /**
     * @param isSuppressed the suppressed flag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        basis.setSuppress(isSuppressed);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 