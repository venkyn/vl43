/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import java.util.ArrayList;


/**
 * Create a new object every time this is called. The trigger is intended for
 * use as a new-object-for-every-record. The trigger fires, and appends a new
 * object to the end of the XML string. The object context is always closed -
 * the context ends with &lt;/xxx&gt; in the XML output.
 * 
 * 
 * 
 * @author dgold
 */
public class EveryRecordCreateObjectTrigger implements CreateObjectTrigger {
    
    private CreateObjectTriggerImpl guts = new CreateObjectTriggerImpl();

    /**
     * Default constructor.
     */
    public EveryRecordCreateObjectTrigger() {
        // Nothing special to do here.
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String closeContext(String openContext, FieldMap fields) {
        return guts.closeContext(openContext, fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        return guts.createObjectIfTriggered(fields, xmlString);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        return guts.detectTriggeringConditions(fields);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return guts.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()
     */
    public String getCloseTag() {
        return guts.getCloseTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()
     */
    public FieldMap getFieldMap() {
        return guts.getFieldMap();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()
     */
    public String getNewObjectName() {
        return guts.getNewObjectName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()
     */
    public String getOpenTag() {
        return guts.getOpenTag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()
     */
    public boolean getUseAllFields() {
        return guts.getUseAllFields();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return guts.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()
     */
    public boolean isOpen() {
        return guts.isOpen();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String openContext(String xmlRepresentation, FieldMap fields) {
        String result = null;
        // openContext writes <obj>, and any fields marked for openContext (unless useAllFields is true)
        result = guts.openContext(xmlRepresentation, fields);
        result = guts.closeContext(result, fields);
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)
     */
    public void setCloseTag(String newObjectTagContents) {
        guts.setCloseTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(java.util.LinkedHashMap)
     */
    public void setFieldMap(FieldMap fieldMap) {
        guts.setFieldMap(fieldMap);
    }

    /**
      * {@inheritDoc}
      * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)
     */
    public void setIsOpen(boolean isThisOpen) {
        // this should not ever be allowed to change - this is always closed.
        return;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)
     */
    public void setNewObjectName(String newObjectname) {
        guts.setNewObjectName(newObjectname);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)
     */
    public void setOpenTag(String newObjectTagContents) {
        guts.setOpenTag(newObjectTagContents);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)
     */
    public void setUseAllFields(boolean useAll) {
        guts.setUseAllFields(useAll);
        return;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return guts.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#writeFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String writeFields(FieldMap fields, 
                              FieldMap fieldsToWrite) {
        return guts.writeFields(fields, fieldsToWrite);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseContextFields()
     */
    public FieldMap getCloseContextFields() {
        return guts.getCloseContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenContextFields()
     */
    public FieldMap getOpenContextFields() {
        return guts.getOpenContextFields();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setCloseContextFields(FieldMap fields) {
        guts.setCloseContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public void setOpenContextFields(FieldMap fields) {
        guts.setOpenContextFields(fields);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isLookup()
     */
    public boolean isLookup() {
        return guts.isLookup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setLookup(boolean)
     */
    public void setLookup(boolean lookup) {
        guts.setLookup(lookup);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        guts.addField(aField);
    }

    /**
     * Just return the subject b- this is not a lokup trigger per se.
     * @param subject the object we have in hand
     * @param objectFinder the thing that does all the work
     * @return the subject
     * @throws VocollectException in this case, never, but the interface requires it.
     */ 
    public Object lookupObject(Object subject, DBLookup objectFinder) throws VocollectException {
        return guts.lookupObject(subject, objectFinder);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#addTrigger(com.vocollect.voicelink.core.importer.CreateObjectTrigger)
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        guts.addTrigger(newTrigger);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getTriggers()
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return guts.getTriggers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#fireSympatheticTriggers(com.vocollect.voicelink.core.importer.FieldMap)
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        return guts.fireSympatheticTriggers(fields);
    }

    /**
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#reset()
     */
    public void reset() {
        guts.reset();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isMainObject()
     */
    public boolean isMainObject() {
        return guts.isMainObject();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setMainObject(boolean)
     */
    public void setMainObject(boolean mainFlag) {
        guts.setMainObject(mainFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isUseList()
     */
    public boolean isUseList() {
        return guts.isUseList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setUseList(boolean)
     */
    public void setUseList(boolean useList) {
        guts.setUseList(useList);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getList()
     */
    public String getList() {
        return guts.getList();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setList(java.lang.String)
     */
    public void setList(String list) {
        guts.setList(list);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getGetterName()
     */
    public String getGetterName() {
        return guts.getGetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getSetterName()
     */
    public String getSetterName() {
        return guts.getSetterName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setGetterName(java.lang.String)
     */
    public void setGetterName(String getterName) {
        guts.setGetterName(getterName);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSetterName(java.lang.String)
     */
    public void setSetterName(String setterName) {
        guts.setSetterName(setterName);
    }

    /**
     * @return boolean
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isSuppress()
     */
    public boolean isSuppress() {
        return guts.isSuppress();
    }

    /**
     * @param isSuppressed the suppressed flag
     * @see com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        guts.setSuppress(isSuppressed);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 