/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.Map;

/**
 * 
 * 
 * @author dgold
 */
public class ErrorRecord {

    private Object badguy;

    private Map    errors;

    private long   lineNumber;

    private String dataSource;

    /**
     * Default Constructor.
     *
     */
    public ErrorRecord() {
        super();
    }

    /**
     * Constructor with ValidatorContext from xwork and invalidObject.
     * @param vcontext specifies the ValidatorContext
     * @param invalidObject specifies the object to check
     * @param line specifies the line number
     * @param dataSource specifies the datasource as a String 
     */
    public ErrorRecord(ValidatorContext vcontext, Object invalidObject,
            long line, String dataSource) {
        super();
        this.errors = vcontext.getFieldErrors();
        this.badguy = invalidObject;
        this.lineNumber = line;
        this.dataSource = dataSource;
    }

    /**
     * @return Returns the badguy.
     */
    public Object getBadguy() {
        return this.badguy;
    }

    /**
     * @param badguy
     *            The badguy to set.
     */
    public void setBadguy(Object badguy) {
        this.badguy = badguy;
    }

    /**
     * @return Returns the dataSource.
     */
    public String getDataSource() {
        return this.dataSource;
    }

    /**
     * @param dataSource
     *            The dataSource to set.
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return Returns the errors.
     */
    public Map getErrors() {
        return this.errors;
    }

    /**
     * @param errors
     *            The errors to set.
     */
    public void setErrors(Map errors) {
        this.errors = errors;
    }

    /**
     * @return Returns the lineNumber.
     */
    public long getLineNumber() {
        return this.lineNumber;
    }

    /**
     * @param lineNumber The lineNumber to set.
     */
    public void setLineNumber(long lineNumber) {
        this.lineNumber = lineNumber;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 