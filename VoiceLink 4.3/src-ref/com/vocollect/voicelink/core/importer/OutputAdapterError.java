/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;


/**
 * ErrorCode instances for OutputAdapters.
 * This is for all adapters in 'com.vocollect.voicelink.core.importer.adapters'
 * Customizers should use a separate file corresponding to the
 * name of the custom Adapter.
 * 
 * @author jtauberg from code by dgold
 *
 */
public final class OutputAdapterError extends ErrorCode {

    public static final int                          LOWER_BOUND                   = 6000;

    public static final int                          UPPER_BOUND                   = 6099;

    /** No error, just the base initialization. */
    public static final OutputAdapterError NO_ERROR = new OutputAdapterError();

    // Configuration hosed.
    public static final OutputAdapterError BAD_CONFIG = new OutputAdapterError(6001);
    
    public static final OutputAdapterError FILENAME_EXTENSION_NOT_SET = new OutputAdapterError(6002);

    public static final OutputAdapterError FILENAME_PREFIX_NOT_SET = new OutputAdapterError(6003);

    public static final OutputAdapterError ABNORMAL_CLOSE_ON_SUCCESS = new OutputAdapterError(6004);
    
    public static final OutputAdapterError ABNORMAL_CLOSE_ON_FAILURE = new OutputAdapterError(6005);

    public static final OutputAdapterError FAILURE_SUCCESSDIRECTORY_SAME_AS_MOVETO = new OutputAdapterError(6006);
    
    public static final OutputAdapterError FILENAME_NOT_SET = new OutputAdapterError(6007);

    public static final OutputAdapterError CREATE_MOVE_TO_DIRECTORY_FAILED = new OutputAdapterError(6008);

    public static final OutputAdapterError CREATE_OUTPUT_DIRECTORY_FAILED = new OutputAdapterError(6009);

    public static final OutputAdapterError UNABLE_TO_CREATE_DIRECTORY  = new OutputAdapterError(6010);

    public static final OutputAdapterError OUTPUT_DIRECTORY_NOT_SET  = new OutputAdapterError(6012);
    
    public static final OutputAdapterError OUTPUT_FILE_NOT_MOVED = new OutputAdapterError(6013);

    public static final OutputAdapterError OPEN_FOR_WRITE_TYPE_NOT_SET = new OutputAdapterError(6014);

    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR = new OutputAdapterError(6015);

    public static final OutputAdapterError OUTPUT_FILE_CAN_NOT_BE_OPENED = new OutputAdapterError(6016);

    public static final OutputAdapterError OUTPUT_FILE_CAN_NOT_BE_WRITTEN = new OutputAdapterError(6017);

    public static final OutputAdapterError OUTPUT_FILE_MOVED = new OutputAdapterError(6018);
    
    public static final OutputAdapterError ENCODING_NOT_SUPPORTED_BY_VOICELINK = new OutputAdapterError(6019);

    //renameOnMove called but filename Extension not set (Export Config error)
    public static final OutputAdapterError FILENAME_EXTENSION_NOT_SET2 = new OutputAdapterError(6020);

    //renameOnMove called but filename Prefix not set (Export Config error)
    public static final OutputAdapterError FILENAME_PREFIX_NOT_SET2 = new OutputAdapterError(6021);
  
    //Filename Prefix may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR3 = new OutputAdapterError(6022);

    //appendToFile may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR4 = new OutputAdapterError(6023);

    //Output Directory may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR5 = new OutputAdapterError(6024);

    //moveToDirectory may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR6 = new OutputAdapterError(6025);

    //deleteOnFailure may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR7 = new OutputAdapterError(6026);

    //renameOnMove may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR8 = new OutputAdapterError(6027);

    //Filename Extension may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR2 = new OutputAdapterError(6028);

    //renameOnMove called but filename Extension not set (Export Config error) in setupOpenForOverwrite.
    public static final OutputAdapterError FILENAME_EXTENSION_NOT_SET3 = new OutputAdapterError(6029);

    //renameOnMove called but filename Prefix not set (Export Config error) in setupOpenForOverwrite.
    public static final OutputAdapterError FILENAME_PREFIX_NOT_SET3 = new OutputAdapterError(6030);

    //setupOpenForOverwrite requires a filename be specified. 
    public static final OutputAdapterError FILENAME_NOT_SET2 = new OutputAdapterError(6031);

    //Invalid fileMode setting specified. Valid modes include:  "writeUnique" (Default),
    //"append", or "overwrite"
    public static final OutputAdapterError INVALID_FILE_MODE_SPECIFIED = new OutputAdapterError(6032);

    //setFileMode may not be changed while file is open.
    public static final OutputAdapterError INTERFACE_SEQUENCE_ERROR9 = new OutputAdapterError(6033);

    
    /**
     * Constructor.
     */
    private OutputAdapterError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private OutputAdapterError(long err) {
        super(OutputAdapterError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 