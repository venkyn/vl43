/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;


/**
 * ErrorCode instances for ValidatorsManipulatorsError.
 * This contains the error codes for import validators
 * and manipulators
 *
 * @author KalpnaT
 *
 */
public final class ValidatorManipulatorErrors extends ErrorCode {

    public static final int LOWER_BOUND = 6100;

    public static final int UPPER_BOUND = 6199;

    /** No error, just the base initialization. */
    public static final ValidatorManipulatorErrors NO_ERROR =
        new ValidatorManipulatorErrors();

    //The specified defalut field name for
    //      DefaultFieldManipulator does not exists
    public static final ValidatorManipulatorErrors
                    DEFAULT_FIELD_NAME_DOES_NOT_EXIST =
                                new ValidatorManipulatorErrors(6101);

    //The default field name is not specified for DefaultFieldManipulator
    public static final ValidatorManipulatorErrors
                    DEFAULT_FIELD_NAME_NOT_SPECIFIED =
                                new ValidatorManipulatorErrors(6102);

    // Null parameter is specified for DefaultFieldManipulator
    public static final ValidatorManipulatorErrors NULL_PARAMENTER_SPECIFIED =
        new ValidatorManipulatorErrors(6103);

    //Incorrect format is specified for NumberFormatManipulator
    public static final ValidatorManipulatorErrors ILLEGAL_FORMAT_CONVERSION =
        new ValidatorManipulatorErrors(6104);

    //Null format is specified for NumberFormatManipulator
    public static final ValidatorManipulatorErrors NULL_FIELD_FORMAT_SPECIFIED =
        new ValidatorManipulatorErrors(6105);

    //Improper class cast
    public static final ValidatorManipulatorErrors CLASS_CAST_IMPROPER =
        new ValidatorManipulatorErrors(6106);

    //Improper format of date specified for DateValidator
    public static final ValidatorManipulatorErrors ILLEGAL_DATE_FORMAT =
        new ValidatorManipulatorErrors(6107);

    //Null size is specified for RightJustifyManipulator
    public static final ValidatorManipulatorErrors NULL_SIZE_SPECIFIED =
        new ValidatorManipulatorErrors(6108);
    
    // Specified field does not exist in class for ValueBasedEnumSetDefaultManipulator
    public static final ValidatorManipulatorErrors FIELD_NAME_DOES_NOT_EXIST =
        new ValidatorManipulatorErrors(6109);

    // Specified value does not exist in class for ValueBasedEnumSetDefaultManipulator
    public static final ValidatorManipulatorErrors VALUE_DOES_NOT_EXIST =
        new ValidatorManipulatorErrors(6110);
    
    // Specified field value is not an enumerated type for ValueBasedEnumSetDefaultManipulator
    public static final ValidatorManipulatorErrors NOT_AN_ENUMERATED_TYPE =
        new ValidatorManipulatorErrors(6111);
    
    /**
     * Constructor.
     */
    private ValidatorManipulatorErrors() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private ValidatorManipulatorErrors(long err) {
        super(ValidatorManipulatorErrors.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 