/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.exporters;

import com.vocollect.epp.errors.ErrorCode;

/**
 * @author Kalpna
 */
public final class ExporterError extends ErrorCode {

    public static final int LOWER_BOUND = 6300;

    public static final int UPPER_BOUND = 6399;

    /** No error, just the base initialization. */
    public static final ExporterError NO_ERROR = new ExporterError();

    public static final ExporterError FAILED_EXPORT = new ExporterError(6301);
    
    public static final ExporterError NO_WORK_TO_DO = new ExporterError(6302);
    
    public static final ExporterError EXPORT_RECORD_FILTERED = new ExporterError(6303);
    
    public static final ExporterError WRONG_RECORD_COUNT = new ExporterError(6304);
    
    public static final ExporterError UNABLE_TO_FIND_FIELD_GETTER = new ExporterError(6305);

    public static final ExporterError MORE_THAN_ONE_LIST_OBJECT_REQUESTED_FROM_QUERIED_OBJECT = new ExporterError(6306);
    
    public static final ExporterError LOOKUP_FAILURE_FOR_MARKUP_FIELD = new ExporterError(6307);
    
    public static final ExporterError NESTED_LEVEL_COLLECTION_NOT_SUPPORTED = new ExporterError(6308);
    
    public static final ExporterError BAD_FAILED_VALIDATION_FILE_NAME = new ExporterError(6309);
    

    /**
     * Constructor.
     */
    private ExporterError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private ExporterError(long err) {
        super(ExporterError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 