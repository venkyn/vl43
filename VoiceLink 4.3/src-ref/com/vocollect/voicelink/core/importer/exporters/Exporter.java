/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.exporters;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.CreateNotification;
import com.vocollect.voicelink.core.importer.DataTransferBase;
import com.vocollect.voicelink.core.importer.Describable;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.NoWorkException;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.OutOfDataException;
import com.vocollect.voicelink.core.importer.OutputFabricator;
import com.vocollect.voicelink.core.importer.RecordLengthException;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter;
import com.vocollect.voicelink.core.importer.adapters.OutputAdapterConfigurationException;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Level;

/**
 * The Exporter is the part that exports a datatype.
 * 
 * This essentially consists of an runDatatypeJob method, which get the record
 * from DatabaseDataSourceParser. This prepare the record using the
 * OutputFabricator and write it to the destination using the OutputAdapter.
 * 
 * @author Kalpna
 * 
 */
@SuppressWarnings("unchecked")
public class Exporter extends DataTransferBase implements Describable {

    static final Logger log = new Logger(Exporter.class);

    // OutputFabricator
    private OutputFabricator outputFabricator;

    // This is the object that manages the validation framework.
    private ValidationResult validator = new ValidationResult();

    // Handles Notifications
    private CreateNotification createNotification = new CreateNotification();

    private File failedValidationFile = null;

    /**
     * Default constructor. Set the legal state changes.
     */
    public Exporter() {
        super();
        this.getStateChanges().setCurrentState(CONFIGURED_STATE);
    }

    /**
     * This is the main routine for the export. This essencially open the
     * SourceParser, get the fieldMap
     * @return true if successful
     */
    @Override
    public boolean runDatatypeJob() {

        FieldMap fm = null;
        ObjectContext objectContext = null;
        int recordsRead = 0;
        getSourceParser().setSiteName(getSiteName());
        getPersistenceManager().setSiteName(getSiteName());
        boolean retValue = true;

        try {
            this.getSourceParser().setDatatype(this.getDatatype());
            this.getSourceParser().setUnmar(this.getUnmar());

            this.logStartUpInfo();
            // Set to Running.
            this.getStateChanges().setCurrentState(RUNNING_STATE);
            boolean outOfData = false;
            // Set the validation context string for validator
            if (this.getValidationContextString() != null) {
                this.validator.setdataTransferContextString(EXPORT_VCS_PREFIX
                    + "-" + this.getValidationContextString());
            } else {
                this.validator.setdataTransferContextString(EXPORT_VCS_PREFIX);
            }
            while ((0 == this.getCurrentState().compareTo(RUNNING_STATE))
                && (!outOfData)) {

                objectContext = getSourceParser().getElement();
                // Get the record from DatabaseDataSourceParser

                recordsRead++;
                this.getStats().setDataElements(
                    this.getStats().getDataElements() + 1);
                fm = (FieldMap) objectContext.getFieldMaps().get(0);

                if (0 < getBatchSize()) {
                    if (0 == (recordsRead % getBatchSize())) {
                        takeThrottlingNap();
                    }
                }

                // check for outOfData
                outOfData = this.getSourceParser().isOutOfData();

                if (objectContext.getModelObject() != null) {
                    if (!(this.validateRecord(objectContext.getModelObject()))) {
                        if (log.isEnabledFor(Level.DEBUG)) {
                            // Save the record, with validation results
                            log.debug(
                                "The record is filtered for "
                                    + objectContext.toString(),
                                ExporterError.EXPORT_RECORD_FILTERED);
                        }
                        this.saveErrorRecord(validator);
                        continue;
                    }
                }
                this.getOutputFabricator().createRecord(
                    (FixedLengthFieldMap) fm);
            }
        } catch (RecordLengthException e) {
            log.error(getDescription() + ": " + "Bad record at record "
                + this.getStats().getDataElements() + ". "
                + e.getUserMessage().getKey(), ExporterError.FAILED_EXPORT);
            this.getStateChanges().setCurrentState(FAILED_STATE);
            retValue = false;
        } catch (OutOfDataException e) {
            // Normal termination for the database parser.
            if (0 == getStats().getDataElements()) {
                // This is OK - there was no data to report on, which can
                // happen.
                log.info(
                    getDescription() + ": " + "No work found.",
                    ExporterError.NO_WORK_TO_DO);
            }
            retValue = true;
        } catch (ExportConfigurationException e) {
            // This is an IO exception, which should fail the export?
            log.error(getDescription() + ": " + "Caught exception "
                + e.getUserMessage().getKey(), ExporterError.FAILED_EXPORT);
            this.getStateChanges().setCurrentState(FAILED_STATE);
            retValue = false;
        } catch (OutputAdapterConfigurationException e) {
            // This occurs when FileOutput Adapter couldn't be configured or any
            // of it's IO operations failed
            log.error(getDescription() + ": " + "Caught exception "
                + e.getUserMessage().getKey(), ExporterError.FAILED_EXPORT);
            this.getStateChanges().setCurrentState(FAILED_STATE);
            retValue = false;
        } catch (NoWorkException e) {
            log.info(
                getDescription() + ": " + "No work found.",
                ExporterError.NO_WORK_TO_DO);
            this.getStateChanges().setCurrentState(NO_WORK_STATE);
            retValue = false;
        } catch (VocollectException e) {
            // This is an IO exception, which should fail the export?
            log.error(getDescription() + ": " + "Caught exception "
                + e.getUserMessage().getKey(), ExporterError.FAILED_EXPORT);
            this.getStateChanges().setCurrentState(FAILED_STATE);
            retValue = false;
        } finally {
            cleanup();
            finish();
        }

        if (null != getOutputFabricator()) {
            // Log the information when records are filtered
            if (recordsRead != this.getOutputFabricator().getRecordsCreated()) {
                log.error(
                    "Records read not equal to number of records created by export."
                        + " Created: "
                        + this.getOutputFabricator().getRecordsCreated()
                        + " Read: " + recordsRead,
                    ExporterError.WRONG_RECORD_COUNT);
            }
        }
        return retValue;
    }

    /**
     * Getter for outputFabricator.
     * @return OutputFabricator
     */
    public OutputFabricator getOutputFabricator() {
        return this.outputFabricator;
    }

    /**
     * Setter for outputFabricator.
     * @param outputFabricator the OutputFabricator
     */
    public void setOutputFabricator(OutputFabricator outputFabricator) {
        this.outputFabricator = outputFabricator;
    }

    /**
     * Validate the object formed by the input data.
     * @param myObj the object represented by input data.
     * @return true if the object passed validation, false OW
     */
    private boolean validateRecord(Object myObj) {
        String exportContext = "Output source "
            + this.getSourceParser().getDescription();
        return this.validator.validate(exportContext, this.getSourceParser()
            .getFieldList(), myObj);

    }

    /**
     * Well, this will look weird, but since incorrect state changes are
     * ignored, we just try the applicable end states. Only the correct one will
     * prevail.
     */
    public synchronized void finish() {

        String failedValidation = "";

        releaseSession();

        // This has to be first, no data elements => no failed elements
        if (0 == this.getStats().getFailedElements()) {
            this.getStateChanges().setCurrentState(SUCCESSFUL_STATE);
            this.getStateChanges().setCurrentState(STOPPED_STATE);
        } else if (this.getStats().getDataElements() == this.getStats()
            .getFailedElements()) {
            this.getStateChanges().setCurrentState(FAILED_STATE);
        }

        if (this.getStateChanges().getCurrentState().toString().contains(
            "Failed")) {
            try {
                if (failedValidationFile == null) {
                    failedValidation = "";
                } else {
                    failedValidation = failedValidationFile.getCanonicalPath();
                }
            } catch (IOException e) {
                log.error(
                    getDescription()
                        + ": The FailedValidationFilename is bad/unreachable "
                        + failedValidation,
                    ExporterError.BAD_FAILED_VALIDATION_FILE_NAME, e);
            }
            this.createNotification.createNotify(
                this.getStateChanges().getCurrentState(), getNotificationMap(),
                this.getDescription(), this.getSiteName(), failedValidation);

        }
        this.getStateChanges().setCurrentState(IMPERFECT_STATE);
        this.getStateChanges().setCurrentState(STOPPED_IMPERFECT_STATE);

        saveStats();
        purgeUncommittedRecords();
        this.getSourceParser().close();

        return;

    }

    /**
     * Cleanup resourses.
     */
    @Override
    public void cleanup() {
        if ((null != getOutputFabricator())
            && (null != getOutputFabricator().getOutputAdapter())) {
            if (this.getOutputFabricator().getOutputAdapter().isOpen()) {
                if (this.getCurrentState() == FAILED_STATE) {
                    this.getOutputFabricator().closeOnFailure();
                } else {
                    this.getOutputFabricator().closeOnSuccess();
                }

            }
        }
    }

    /**
     * This is just one common place to dump all the export startup information
     * which could be useful in tracing bug or getting some import information.
     */
    private void logStartUpInfo() {
        // Log the exporter information
        String logMessage = new String(
            "###Exporter Info: Site: $SITE - Type: $TYPE - Output Directory: $OUTPUTDIR");

        logMessage = logMessage.replace("$SITE", this.getSiteName()).replace(
            "$TYPE", this.getDatatype());

        String parentOutputDir = null;
        if ((null != getOutputFabricator())
            && (null != getOutputFabricator().getOutputAdapter())) {
            try {
                File f = new File(((FileOutputAdapter) this
                    .getOutputFabricator().getOutputAdapter())
                    .getOutputDirectory());
                parentOutputDir = f.getAbsolutePath();
            } catch (ClassCastException e) {
                logMessage = logMessage.replace(
                    "$OUTPUTDIR", "source other than filesystem");
            }

            if (parentOutputDir != null) {
                logMessage = logMessage.replace("$OUTPUTDIR", parentOutputDir);
            }
        }
        
        log.info(logMessage);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 