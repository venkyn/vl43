/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;


/**
 * ErrorCode instances for DataSourceAdapters.
 * This is for all adapters in 'com.vocollect.voicelink.core.importer.adapters'
 * Customizers should use a separate file corresponding to the
 * name of the custom Adapter.
 * 
 * @author dgold
 *
 */
public final class DataSourceAdapterError extends ErrorCode {

    public static final int LOWER_BOUND = 5000;

    public static final int UPPER_BOUND = 5099;

    /** No error, just the base initialization. */
    public static final DataSourceAdapterError NO_ERROR = 
        new DataSourceAdapterError();

    // Configuration hosed.
    public static final DataSourceAdapterError BAD_CONFIG = 
        new DataSourceAdapterError(5001);

    public static final DataSourceAdapterError SOURCE_DIRECTORY_DOES_NOT_EXIST = 
        new DataSourceAdapterError(5002);

    public static final DataSourceAdapterError SOURCE_DIRECTORY_DOES_NOT_EXIST2 = 
        new DataSourceAdapterError(5003);

    public static final DataSourceAdapterError UNABLE_TO_SET_NEXT_FILE_NAME = 
        new DataSourceAdapterError(5004);

    public static final DataSourceAdapterError INPUT_FILE_DELETED = 
        new DataSourceAdapterError(5005);

    public static final DataSourceAdapterError NO_WORK = 
        new DataSourceAdapterError(5006);

    public static final DataSourceAdapterError MID_STREAM_INPUT_ERROR = 
        new DataSourceAdapterError(5007);

    public static final DataSourceAdapterError ABNORMAL_CLOSE_ON_SUCCESS = 
        new DataSourceAdapterError(5008);

    public static final DataSourceAdapterError ABNORMAL_CLOSE_ON_FAILURE = 
        new DataSourceAdapterError(5009);

    public static final DataSourceAdapterError SOURCE_DIRECTORY_NOT_A_DIRECTORY = 
        new DataSourceAdapterError(5010);

    public static final DataSourceAdapterError SUCCESS_DIRECTORY_SAME_AS_INPUT = 
        new DataSourceAdapterError(5011);

    public static final DataSourceAdapterError SUCCESS_DIRECTORY_SAME_AS_INPUT2 = 
        new DataSourceAdapterError(5012);

    public static final DataSourceAdapterError FAILURE_DIRECTORY_SAME_AS_INPUT = 
        new DataSourceAdapterError(5013);

    public static final DataSourceAdapterError FAILURE_DIRECTORY_SAME_AS_INPUT2 = 
        new DataSourceAdapterError(5014);

    public static final DataSourceAdapterError BAD_FILENAME_PATTERN = 
        new DataSourceAdapterError(5015);

    public static final DataSourceAdapterError CREATE_FAILURE_DIRECTORY_FAILED = 
        new DataSourceAdapterError(5016);

    public static final DataSourceAdapterError CREATE_FAILURE_DIRECTORY_FAILED2 = 
        new DataSourceAdapterError(5017);

    public static final DataSourceAdapterError CREATE_SUCCESS_DIRECTORY_FAILED = 
        new DataSourceAdapterError(5018);

    public static final DataSourceAdapterError CREATE_SUCCESS_DIRECTORY_FAILED2 = 
        new DataSourceAdapterError(5019);

    public static final DataSourceAdapterError UNABLE_TO_CREATE_DIRECTORY = 
        new DataSourceAdapterError(5020);

    public static final DataSourceAdapterError SOURCE_DIRECTORY_NOT_SET =
        new DataSourceAdapterError(5021);

    public static final DataSourceAdapterError INPUT_FILE_MOVED = 
        new DataSourceAdapterError(5022);

    public static final DataSourceAdapterError INPUT_FILE_NOT_MOVED = 
        new DataSourceAdapterError(5023);
    
    public static final DataSourceAdapterError INVALID_PARENT_DIRECTORY = 
        new DataSourceAdapterError(5024);

    public static final DataSourceAdapterError ENCODING_NOT_SUPPORTED_BY_JAVA = 
        new DataSourceAdapterError(5025);
    
    public static final DataSourceAdapterError ENCODING_NOT_SUPPORTED_BY_VOICELINK = 
        new DataSourceAdapterError(5026);

    public static final DataSourceAdapterError PARENT_DIRECTORY_COULD_NOT_BE_CREATED = 
        new DataSourceAdapterError(5027);
    
    public static final DataSourceAdapterError IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING = 
        new DataSourceAdapterError(5028);

    
    //-- Stuff for file ordering (sorting/indexes)
    public static final DataSourceAdapterError SORT_TEMP_FILE_CAN_NOT_BE_OPENED_WRITE = 
        new DataSourceAdapterError(5029);
    
    //This shouldn't happen as the sort output file should be new.
    // If one is out there, and has wrong formatting, stop service, delete file and restart.
    public static final DataSourceAdapterError SORT_TEMP_FILE_UNSUPPORTED_ENCODING = 
        new DataSourceAdapterError(5030);
    
    //After the file was sorted, the number of records read did not match the
    //number of recors written.
    public static final DataSourceAdapterError SORT_OUTPUT_LENGTH_FAILURE = 
        new DataSourceAdapterError(5031);

    //During sort process, the sorted file was requested to be moved and was.
    public static final DataSourceAdapterError SORTED_INPUT_FILE_MOVED = 
        new DataSourceAdapterError(5032);
    
    //During sort process, the sorted file was requested to be moved and failed to be moved.
    public static final DataSourceAdapterError SORTED_INPUT_FILE_NOT_MOVED = 
        new DataSourceAdapterError(5033);

    public static final DataSourceAdapterError INPUT_FILE_DELETED2 = 
        new DataSourceAdapterError(5034);

    public static final DataSourceAdapterError ENCODING_NOT_SUPPORTED_BY_JAVA2 = 
        new DataSourceAdapterError(5035);

    public static final DataSourceAdapterError SORT_READ_FAILURE = 
        new DataSourceAdapterError(5036);

    public static final DataSourceAdapterError FILE_TO_SORT_FILE_CLOSE_FAILURE = 
        new DataSourceAdapterError(5037);

    public static final DataSourceAdapterError SORT_INPUT_FILE_FAILURE = 
        new DataSourceAdapterError(5038);

    public static final DataSourceAdapterError SORT_WRITE_FILE_CLOSE_FAILURE = 
        new DataSourceAdapterError(5039);
    
    public static final DataSourceAdapterError SORT_TEMP_FILE_CAN_NOT_BE_OPENED_READ = 
        new DataSourceAdapterError(5040);
   
    public static final DataSourceAdapterError SORT_TEMP_FILE_RANDOM_ACCESS_READ_FAILURE = 
        new DataSourceAdapterError(5041);

    public static final DataSourceAdapterError SORTED_FILE_WRITE_UNSUPPORTED_ENCODING = 
        new DataSourceAdapterError(5042);
  
    //During write of intermediate UTF16 sort file, the #lines read did not match
    // the # lines written.
    public static final DataSourceAdapterError SORT_OUTPUT_LENGTH_FAILURE2 = 
        new DataSourceAdapterError(5043);
    
    //Not keeping sorted file, but delete failed in successful file close.
    public static final DataSourceAdapterError SORTED_FILE_NOT_DELETED = 
        new DataSourceAdapterError(5044);

    //Not keeping intermediate UTF16 file, but delete failed during sort process.
    public static final DataSourceAdapterError SORT_INTERMEDIATE_UTF16_FILE_DELETE_FAILURE = 
        new DataSourceAdapterError(5045);
   
    //The Intermediate UTF-16 file used for sorting was moved as requested.
    public static final DataSourceAdapterError INTERMEDIATE_SORT_FILE_MOVED = 
        new DataSourceAdapterError(5046);

    //The Intermediate UTF-16 file used for sorting was requested to be moved, but
    // failed the move process.
    public static final DataSourceAdapterError INTERMEDIATE_SORT_FILE_NOT_MOVED = 
        new DataSourceAdapterError(5047);

    //The input file (sort process) was moved
    public static final DataSourceAdapterError INPUT_FILE_MOVED2 = 
        new DataSourceAdapterError(5048);

    //The input file (sort process) was supposed to move, but failed the move.
    public static final DataSourceAdapterError INPUT_FILE_NOT_MOVED2 = 
        new DataSourceAdapterError(5049);

    //Not keeping sorted file, but delete failed in file close.
    public static final DataSourceAdapterError SORTED_FILE_NOT_DELETED2 = 
        new DataSourceAdapterError(5050);

    //Did random access read of intermediate UTF-16 file for sorting,
    // tried to close it, and the close failed.
    public static final DataSourceAdapterError SORT_TEMP_FILE_CAN_NOT_BE_CLOSED = 
        new DataSourceAdapterError(5051);

    // FilePurgeAdapter - unable to delete file due to ./..
    public static final ErrorCode INSUFFICIENT_PRIVILEGES_FOR_DELETE = 
        new DataSourceAdapterError(5052);
  
    
    /**
     * Constructor.
     */
    private DataSourceAdapterError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private DataSourceAdapterError(long err) {
        super(DataSourceAdapterError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 