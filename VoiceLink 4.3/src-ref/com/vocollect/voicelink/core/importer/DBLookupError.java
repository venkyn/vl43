/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Errors for the DBLookup.
 * @author dgold
 *
 */
public final class DBLookupError extends ErrorCode {
    public static final int LOWER_BOUND = 5600;

    public static final int UPPER_BOUND = 5699;

    /** No error, just the base initialization. */
    public static final DBLookupError NO_ERROR = 
        new DBLookupError();
    
    /** Returned more than one element on a conditionalInsert. */
    public static final DBLookupError INDETERMINATE_CONDITIONAL_UPDATE_RESULT_SET = 
        new DBLookupError(5600);

    // No method in created object by the name given
    public static final DBLookupError NO_SUCH_GETTER_METHOD_NAME = 
        new DBLookupError(5605);

    // No setter method in created object by given name
    public static final DBLookupError NO_SUCH_SETTER_METHOD_NAME =
        new DBLookupError(5606);

    // Default is to pass a null object array to the getter. If the getter requires more...
    public static final DBLookupError BAD_GETTER_PARAMETERS = 
        new DBLookupError(5607);

    // Private or protected getter method
    public static final DBLookupError GETTER_ACCESS_DENIED = 
        new DBLookupError(5608);

    // Getter threw an exception
    public static final ErrorCode GETTER_EXCEPTION_THROWN = 
        new DBLookupError(5609);

    // Default is to pass a null object array to the setter. If the getter requires more...
    public static final ErrorCode BAD_SETTER_PARAMETERS = 
        new DBLookupError(5610);

    // Private or protected setter method
    public static final ErrorCode SETTER_ACCESS_DENIED = 
        new DBLookupError(5611);

    // Setter threw an exception
    public static final ErrorCode SETTER_EXCEPTION_THROWN = 
        new DBLookupError(5612);

    public static final ErrorCode BAD_GETTER_SETTER_MATCH = 
        new DBLookupError(5613);

    public static final ErrorCode GETTER_SETTER_ACCESS_DENIED = 
        new DBLookupError(5614);

    public static final ErrorCode GETTER_SETTER_EXCEPTION_THROWN = 
        new DBLookupError(5615);

    public static final ErrorCode INDETERMINATE_CONDITIONAL_INSERT_RESULT_SET = 
        new DBLookupError(5616);

    public static final ErrorCode NO_SUCH_SETTER_METHOD_NAME_2 = 
        new DBLookupError(5617);
    
    public static final ErrorCode NO_SUCH_GETTER_METHOD_NAME_2 = 
        new DBLookupError(5618);

    public static final ErrorCode INACCESSIBLE_CLASS_OR_CONSTRUCTOR = 
        new DBLookupError(5619);

    public static final ErrorCode UNINSTANTIATABLE_CLASS = 
        new DBLookupError(5620);

    public static final ErrorCode NO_SUCH_CLASS = 
        new DBLookupError(5621);
    
    /**
     * Constructor.
     */
    private DBLookupError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private DBLookupError(long err) {
        super(DBLookupError.NO_ERROR, err);
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 