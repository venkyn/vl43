/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class holds the data from and information about a field. Pretty much a
 * POD class. This class probably needs to be subclassed to add more data, e.g.
 * FixedLengthField. These fields will be configured externally, via (Castor or
 * Spring), so there is no need for complex constructors. Yet. This is intended
 * to be owned by the DataSourceParser, which populates the data in the Field,
 * and used by the Importer/Exporter to translate the data into objects. The
 * tagging information is used by the Importer to generate an XML description of
 * the object, which Castor interprets and uses to create an Object of the
 * appropriate type.
 *
 * @author dgold
 */
public class Field {

    // character Data from the input source for this field
    private String fieldData;

    // List of validators to be applied to this field
    private FieldValidator validationList = null;

    // name for Tag to be applied to the data. Optional, default to fieldName.
    private String tagName = null;

    // Populated by the Importer or Exporter, this is <getTagName()>
    private String openTag = null;

    // Populated by the Importer or Exporter, this is </getTagName()>
    private String closeTag = null;

    // Name of the field. Default is to make tag name assume the value of the
    // fieldName.
    // Proper use is to make the tagname be our name for the data, corresponding
    // to
    // the setter's name, and must match the tagging set up in the Object
    // mapping.
    // The FieldName is the customer's name for the data as specified in the IDS
    private String fieldName = null;

    // determines whether the field is an indexable field or not in order to
    // create keyFieldCache objects
    private boolean indexable = false;

    // Populated by the Importer or Exporter,
    //this suppresses output from an AllFields trigger
    private boolean remapped = false;

    // Populated by the Imported or Exporter,
    //this suppresses output of this field.
    private boolean suppress = false;

    // Type of data this is. Default to String
    private String dataType = "String";

    // Indicates that this field should be
    //used to look up the object created.
    // Used by the Triggers
    private boolean keyField = false;

    // Setter name, in the owner object, if different from the field name
    private String setterName = null;

    // Getter name, in the owner name, if different from the field name
    private String getterName = null;

    private boolean treatBlankAsNull = false;

    //Is set if the field contains any badData.
    private boolean badData = false;

    //Stores the value of the bad field data.
    private String badFieldData = null;
    
    /**
     * This is used to indicate where the field should be written in the XML stream.
     * Some -most? - fields can be written to the XML stream when we open the context.
     * Others - calculated fields in particular - may need to be written when we close the tag,
     * after all oif the elements have been added to the main object.
     * This is used by the Triggers.
     * @author dgold
     *
     */

    // Where in the output stream this should be written. Default to closeContext.
    private boolean writeOnOpenContext = false;

    //    private List<Filter>      filters        = new LinkedList<Filter>();
    // Filters to apply to this field

    //    private List<Manipulator> manipulators   = new LinkedList<Manipulator>();
    // Manipulators to apply to this field

    /**
     * Default constructor.
     */
    public Field() {
        super();
    }

    /**
     * Set the field data.
     * 
     * @param data specifies the String object that represents the Field
     */
    public Field(String data) {
        this.fieldData = data;
    }

    /**
     * Add a filter to this field description.
     * @param theFilter specifies Fielter object to add to field
     */
    //    public void addFieldFilter(Filter theFilter) {
    //        this.filters.add(theFilter);
    //
    //    }
    /**
     * Add a manipulator to the list of manipulators.
     * @param theManipulator specifies the Manipulator to add to field
     */
    //    public void addFieldManipulator(Manipulator theManipulator) {
    //        this.manipulators.add(theManipulator);
    //    }
    /**
     * Get the field data.
     * @return the String representing the Field
     */
    public String getFieldData() {        
        if (0 == (getDataType().compareTo("date")) && (!this.fieldData.contains("T"))) {
            if ((this.fieldData != null)
                && (0 < this.fieldData.trim().length())) {
                String dateFormat = "yyyyMMdd";
                String dateTimeFormat = "yyyyMMddHHmmss";

                DateField df = new DateField();
                if (this.fieldData.trim().length() == dateFormat.length()) {
                    df.setDateFormat(dateFormat);
                } else {
                    df.setDateFormat(dateTimeFormat);
                }

                String f1 = df.dateFormat(this.fieldData);
                if (f1.length() == 0 || f1.equals("")) {
                    this.badData = true;
                    this.setBadFieldData(this.fieldData);
                    this.fieldData = "";
                }
                return f1;
            }   
        }
        return this.fieldData;
    }

    /**
     * Determines whether or not the passed in string is numeric.
     * @param stringtoCheck String to check if it is numeric
     * @return true if string is a number
     */
    public boolean isStringNumeric(String stringtoCheck) {
        String patternStr = " *-?[0-9]*\\.?[0-9]+E?-?[0-9]* *";
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(stringtoCheck);
        return matcher.matches();
    }

    /**
     * Set the field data.
     * @param fieldData specifies the String object to set
     */
    public void setFieldData(String fieldData) {
        this.fieldData = fieldData;
        this.setBadData(false);
        if (null != fieldData) {
            this.fieldData.replace("<", "&lt;");
            this.fieldData.replace(">", "&gt;");
            this.fieldData.replace("&", "&amp;");
            this.badData = false;
            if (0 < this.fieldData.trim().length()) {
                if (0 == (getDataType().compareTo("int"))) {
                    if (!isStringNumeric(this.fieldData.trim())) {
                        this.badData = true;
                        this.setBadFieldData(this.fieldData);
                        this.fieldData = "";
                    }
                }
            }
        }
    }

    /**
     * Set the field data.
     * @param fieldData the char array to set
     */
    public void setFieldData(char[] fieldData) {
        this.fieldData = new String(fieldData);
        CharSequence bad = "";
        bad = "&";
        this.fieldData = this.fieldData.replace(bad, "&amp;");
        bad = "<";
        this.fieldData = this.fieldData.replace(bad, "&lt");
        bad = ">";
        this.fieldData = this.fieldData.replace(bad, "&gt");
        this.badData = false;
        if (0 < this.fieldData.trim().length()) {
            if (0 == (getDataType().compareTo("int"))) {
                if (!isStringNumeric(this.fieldData.trim())) {
                    this.badData = true;
                    this.setBadFieldData(this.fieldData);
                    this.fieldData = "";
                }
            }        
        }        
    }

    /**
     * Set the field data.
     * @param fielddata specifies the char array to set
     * @param offset specifies the number to offset
     * @param count specifies the number of characters to set
     */
    public void setFieldData(char[] fielddata, int offset, int count) {
        this.fieldData = new String(fielddata, offset, count);
        CharSequence bad = "";
        bad = "&";
        this.fieldData = this.fieldData.replace(bad, "&amp;");
        bad = "<";
        this.fieldData = this.fieldData.replace(bad, "&lt;");
        bad = ">";
        this.fieldData = this.fieldData.replace(bad, "&gt;");
        this.badData = false;
        if (0 < this.fieldData.trim().length()) {
            if (0 == (getDataType().compareTo("int"))) {
                if (!isStringNumeric(this.fieldData.trim())) {
                    this.badData = true;
                    this.setBadFieldData(this.fieldData);
                    this.fieldData = "";
                }
            }            
        }
    }

    /**
     * @return Returns the dataType.
     */
    public String getDataType() {
        return this.dataType;
    }

    /**
     * @param dataType
     *            The dataType to set.
     */
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    /**
     * @return Returns the fieldName.
     */
    public String getFieldName() {
        return this.fieldName;
    }

    /**
     * Set the field name. If the tag name is not set, set it as well.
     * 
     * @param fieldName
     *            The fieldName to set.
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName.trim();
        if (getTagName() == null) {
            setTagName(fieldName);
        }
        if (null == getGetterName()) {
            setGetterName(fieldName);
        }
        if (null == getSetterName()) {
            setSetterName(fieldName);
        }
    }

    /**
     * @return Returns the filters.
     */
    //    public List<Filter> getFilters() {
    //        return this.filters;
    //    }
    /**
     * @param filters
     *            The filters to set.
     */
    //    public void setFilters(List<Filter> filters) {
    //        this.filters = filters;
    //    }
    /**
     * @return Returns the manipulators.
     */
    //    public List<Manipulator> getManipulators() {
    //        return this.manipulators;
    //    }
    /**
     * Set the manipulator list. Used by configuration.
     * 
     * @param manipulators
     *            The manipulators to set.
     */
    //    public void setManipulators(List<Manipulator> manipulators) {
    //        this.manipulators = manipulators;
    //    }
    /**
     * @return Returns the tagName.
     */
    public String getTagName() {
        return this.tagName;
    }

    /**
     * @param tagName
     *            The tagName to set.
     */
    public void setTagName(String tagName) {
        this.tagName = tagName.trim();
    }

    /**
     * @return Returns the validationList.
     */
    public FieldValidator getValidationList() {
        return this.validationList;
    }

    /**
     * Set the validation list. Used by configuration.
     * 
     * @param validationList
     *            The validationList to set.
     */
    public void setValidationList(FieldValidator validationList) {
        this.validationList = validationList;
    }

    /**
     * @return Returns the closeTag.
     */
    public String getCloseTag() {
        return this.closeTag;
    }

    /**
     * @param closeTag
     *            The closeTag to set.
     */
    public void setCloseTag(String closeTag) {
        this.closeTag = closeTag;
    }

    /**
     * @return Returns the openTag.
     */
    public String getOpenTag() {
        return this.openTag;
    }

    /**
     * @param openTag The openTag to set.
     */
    public void setOpenTag(String openTag) {
        this.openTag = openTag;
    }

    /**
     * Getter for the isIndexable property.
     * @return boolean value of the property
     */
    public boolean isIndexable() {
        return indexable;
    }

    /**
     * Setter for the isIndexable property.
     * @param isIndexable the new isIndexable value
     */
    public void setIndexable(boolean isIndexable) {
        this.indexable = isIndexable;
    }

    /**
     * Gets the value of remapped. This is used to suppress the output 
     * of a field from an allFields trigger. It me3ans that the field is to be absent from the main 
     * object, as it is being used elsewhere. Default = false.
     * @return the remapped
     */
    public boolean isRemapped() {
        return remapped;
    }

    /**
     * Sets the value of the remapped.
     * @param remapped the remapped to set
     */
    public void setRemapped(boolean remapped) {
        this.remapped = remapped;
    }

    /**
     * Gets the value of suppress. This is used to suppress the output of this field's value 
     * in the XML stream entirely. Used by the Triggers.
     * @return the suppress
     */
    public boolean isSuppress() {
        return suppress;
    }

    /**
     * Sets the value of the suppress.
     * @param suppress the suppress to set
     */
    public void setSuppress(boolean suppress) {
        this.suppress = suppress;
    }

    /**
     * Get the indicator that says (true) this field should be used in a lookup key.
     * @return the keyField
     */
    public boolean isKeyField() {
        return keyField;
    }

    /**
     * Set the indicator that says (true) this field should be used in a lookup key.
     * @param keyField the keyField to set
     */
    public void setKeyField(boolean keyField) {
        this.keyField = keyField;
    }

    /**
     * Gets the value of writeOnOpenContext.
     * @return the writeOnOpenContext
     */
    public boolean isWriteOnOpenContext() {
        return writeOnOpenContext;
    }

    /**
     * Sets the value of the writeOnOpenContext.
     * @param writeOnOpenContext the writeOnOpenContext to set
     */
    public void setWriteOnOpenContext(boolean writeOnOpenContext) {
        this.writeOnOpenContext = writeOnOpenContext;
    }

    /**
     * Gets the value of getterName.
     * @return the getterName
     */
    public String getGetterName() {
        return getterName;
    }

    /**
     * Sets the value of the getterName.
     * @param getterName the getterName to set
     */
    public void setGetterName(String getterName) {
        this.getterName = getterName.trim();
    }

    /**
     * Gets the value of setterName.
     * @return the setterName
     */
    public String getSetterName() {
        return setterName;
    }

    /**
     * Sets the value of the setterName.
     * @param setterName the setterName to set
     */
    public void setSetterName(String setterName) {
        this.setterName = setterName.trim();
    }

    /**
     * @return the treatBlankAsNull
     */
    public boolean isTreatBlankAsNull() {
        return treatBlankAsNull;
    }

    /**
     * @param treatBlankAsNull the treatBlankAsNull to set
     */
    public void setTreatBlankAsNull(boolean treatBlankAsNull) {
        this.treatBlankAsNull = treatBlankAsNull;
    }

    /**
     * If the data is null, or if we are suposed to treat blank strings as null and the field data is blank, return true. 
     * @return true if the field data is equivalently null.
     */
    public boolean isNullField() {
        if (null == this.fieldData) {
            return true;
        }
        if (this.isTreatBlankAsNull() && (0 == this.fieldData.trim().length())) {
            return true;
        }
        return false;
    }

    /**
     * @return the badData
     */
    public boolean isBadData() {
        return badData;
    }

    /**
     * Sets the value of the BadData.
     * @param badData the BadData to set
     */
    public void setBadData(boolean badData) {
        this.badData = badData;
    }

    /**
     * @return the badFieldData
     */
    public String getBadFieldData() {
        return badFieldData;
    }

    /**
     * Sets the value of the BadFieldData.
     * @param badFieldData the BadFieldData to set
     */
    public void setBadFieldData(String badFieldData) {
        this.badFieldData = badFieldData;
    }
    
    /**
     * Reset internal state.
     */
    public void reset() {
        badFieldData = "";
        fieldData = "";
        badData = false;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 