/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


import com.vocollect.epp.exceptions.VocollectException;

import java.util.ArrayList;

/**
 * Interface for the object which manages detection of the condition(s) that trigger the \creation of a new object. This
 * is managed by appending the dataTag to the smlString.
 * 
 * If there are no triggers set up, the XML string remains unchanged.
 * 
 * This implies that this needs to keep a list of the fields it's watching, and the previous value for each.
 * 
 * @author dgold
 * 
 */
public interface CreateObjectTrigger {

    /**
     * Set the name of the new object.
     * 
     * @param newObjectTag
     *            specifies the String to use in new object creation
     */
    public void setNewObjectName(String newObjectTag);

    /**
     * Set the new-object open tag.
     * 
     * @param newObjectTagContents
     *            Tag to use
     */
    public void setOpenTag(String newObjectTagContents);

    /**
     * Get the open-tag for this trigger/object.
     * 
     * @return the open-tag
     */
    public String getOpenTag();

    /**
     * Get the name of the tag for this trigger.
     * 
     * @return the String new object name
     */
    public String getNewObjectName();

    /**
     * Set the new-object close tag.
     * 
     * @param newObjectTagContents
     *            tag to use
     */
    public void setCloseTag(String newObjectTagContents);

    /**
     * Get the context-close XML tag.
     * 
     * @return the close tag
     */
    public String getCloseTag();

    /**
     * Close the object in XML.
     * 
     * @param openContext
     *            specifies the String context
     * @param fields
     *            specifies the FieldMap object
     * @return the close tag
     */
    public String closeContext(String openContext, FieldMap fields);

    /**
     * Detect and append a new object trigger in XML to the xmlString. Given the data in the fields passed in, use
     * detectTriggeringConditions() to see if we need to append a new object trigger to the xmlString param
     * 
     * @param fields
     *            the fields from the input source
     * @param xmlString
     *            the resulting xml string
     * @return the xmlString
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString);
    
    /**
     * Recurse through the list of triggers (and their lists), evaluating triggering conditions.
     * @param fields List of fields containing data from the inpit stream.
     * @return the result of the sympathetic triggers being triggered. If they do.
     */
    public String fireSympatheticTriggers(FieldMap fields);

    /**
     * Determine if the data in the fields indicates that we are dealing with a new object.
     * 
     * @param fields
     *            specifies the FieldMap object
     * @return the appropriate tag for the new object
     */
    public boolean detectTriggeringConditions(FieldMap fields);

    /**
     * Start a new object in XML.
     * 
     * @param xmlRepresentation
     *            is the String xmlrep object
     * @param fields
     *            specifies the FieldMap object
     * @return the String context
     */
    public String openContext(String xmlRepresentation, FieldMap fields);

    /**
     * Return true if the object context is open (open called with no close).
     * 
     * @return true if the object context is open
     */
    public boolean isOpen();

    /**
     * Simple setter for the isOpen flag.
     * 
     * @param isThisOpen
     *            the new vallue for isOpne
     */
    public void setIsOpen(boolean isThisOpen);

    /**
     * Get the map of field names that are a part of this object represented by this trigger.
     * 
     * @return map of fields that should be written for this trigger
     */
    public FieldMap getFieldMap();

    /**
     * Set the map of field names that are a part of this object represented by this trigger.
     * 
     * @param fields
     *            the list of field names that should be written as part of the object represented by this trigger.
     */
    public void setFieldMap(FieldMap fields);

    /**
     * Get the fields we write when we open the XML context.
     * 
     * @return the map of fields we write when we open a context for the object represented by this trigger.
     */
    public FieldMap getOpenContextFields();

    /**
     * Set the fields we write when we open the XML context.
     * 
     * @param fields the fields to be written when we open the XML context.
     */
    public void setOpenContextFields(FieldMap fields);

    /**
     * Get the fields we write when we close the XML context.
     * 
     * @return the map of fields we write when we open an XML context for the object represented by this trigger.
     */
    public FieldMap getCloseContextFields();

    /**
     * Set the fields we write when we close the XML context.
     * 
     * @param fields
     *            the list of fields we write when we close the XML context.
     */
    public void setCloseContextFields(FieldMap fields);

    /**
     * Write all of the fields listed in the field map, or all fields if writeAllFields is true.
     * 
     * @param fields
     *            list of fields and data.
     * @param fieldsToWrite
     *            the list of fields to write, w=so we can differentiate between open and close contexts
     * @return A string with all of the fieldsToWrite in full markup..
     */
    public String writeFields(FieldMap fields, FieldMap fieldsToWrite);

    /**
     * Get the flag that indicates whether to write all fields or just those listed.
     * 
     * @return flag indicating that the trigger should (true) write all fields, or just those in the fieldMap element
     *         (false)
     */
    public boolean getUseAllFields();

    /**
     * Get the flag that indicates whether to write all fields or just those listed. Set indicating that the trigger
     * should (true) write all fields, or just those in the fieldMap element (false)
     * @param useAll the value for the above mentioned flag.
     */
    public void setUseAllFields(boolean useAll);
    
    /**
     * Gets the value of the flag that indicates this trigger creates an object that needs to be looked up.
     * @return the valure of the flag. False means lookup is not required.
     */
    public boolean isLookup();

    /**
     * Gets the value of the flag that indicates this trigger creates an object that needs to be looked up.
     * @param lookup value of the flag. False means lookup not required.
     */
    public void setLookup(boolean lookup);
    
    /**
     * Add a field to this trigger's field map.
     * As a consequence of the configuration, we need a way to add fields to the field mapping.
     * This allows us to do so one field at a time.
     * @param aField the field to add to the field map
     */
    public void addField(Field aField);
    
    /**
     * Using the lookup tool provided, look the object up in the DB. What happens, depending on
     * what is found, depends on the type of trigger.
     * For non-lookup type triggers, where we assume non-existance of the object, we do nothing.
     * For lookup-type triggers, we can lookup and bounce the transcaction, lookup
     * and use what we find, lookup and insert if not found, or lookup and update/insert if not found.
     * Default behavior is to pass through.
     * @param subject the thing we're trying to look up, or the thing we're trying to complete.
     * @param objectFinder the DBLookup object to use for this lookup 
     * @return the object, completed by this lookup
     * @throws VocollectException if the object is found (or not) unexpectedly.
     */
    public Object lookupObject(Object subject, DBLookup objectFinder) throws VocollectException;
    
    /** 
     * Same as the above, but for every element of the list...
     * @param subject the list of things we're trying to look up, or the thing we're trying to complete.
     * @param objectFinder the DBLookup object to use for this lookup 
     * @return the object, completed by this lookup
     * @throws VocollectException if the object is found (or not) unexpectedly.
     */
//    public Object lookupObject(Collection subject, DBLookup objectFinder) throws VocollectException;

    /**
     * Calls lookupObject for every composite part
     * @param subject the thing we're trying to look up, or the thing we're trying to complete.
     * @param objectFinder the DBLookup object to use for this lookup 
     * @return the object, completed by this lookup
     * @throws VocollectException if the object is found (or not) unexpectedly.
     */
//    public Object recurseLookups(Object subject, DBLookup objectFinder) throws VocollectException;
    /**
     * Add a trigger to the list of triggers owned by this trigger.
     * These triggers all will be evaluated when the owner is triggered.
     * A LineTrigger will fire whenever the Owner does.
     * @param newTrigger the trigger to add
     */
    public void addTrigger(CreateObjectTrigger newTrigger);
    
    /**
     * Get all of the triggers owned by this trigger.
     * @return the list of triggers
     */
    public ArrayList<CreateObjectTrigger> getTriggers();

    /**
     * Clear the data from the trigger's fields, so it will fire next time we eval it.
     */
    public void reset();
    
    /**
     * Set to true if the object is the main object for this import. This will allow us to determine 
     * how to look this object up, and how to save it. 
     * @return value of mainObject
     */
    public boolean isMainObject();
    
    /**
     * Set the value of the flag which indicates that this trigger represents the main object.
     * @param mainFlag value for the flag
     */
    public void setMainObject(boolean mainFlag);
    
    /**
     * True if this trigger's objects are in a list in the main object.
     * @return the useList
     */
    public boolean isUseList();

    /**
     * Sets the value of the useList.
     * @param useList the useList to set
     */
    public void setUseList(boolean useList);
    
    /**
     * Get the name of the member that represents the list these objects are in.
     * If the objects represented by this trigger are in a list in the main object,
     * get the name of the list holding them.
     * @return name of member which is the list these objects are in in the main object.
     */
    public String getList();
    
    /**
     * Set the name of the member that represents the list these objects are in.
     * If the objects represented by this trigger are in a list in the main object,
     * get the name of the list holding them.
     * @param listName the name of the member that represents the list these guys are in.
     */
    public void setList(String listName);
    
    /**
     * Gets the value of getterName.
     * @return the getterName
     */
    public String getGetterName();

    /**
     * Sets the value of the getterName.
     * @param getterName the getterName to set
     */
    public void setGetterName(String getterName);

    /**
     * Gets the value of setterName.
     * @return the setterName
     */
    public String getSetterName();
    
    /**
     * Sets the value of the setterName.
     * @param setterName the setterName to set
     */
    public void setSetterName(String setterName);

    /**
     * @param isSuppressed - suppressed boolean
     */
    public void setSuppress(boolean isSuppressed);
    
    /**
     * @return boolean
     */
    public boolean isSuppress();

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 