/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.Calendar;

/**
 * This class just keeps all stats information for the import together.
 * As opposed to muddying up the Importer with the stats & reporting.
 * 
 * @author dgold
 *
 */
/**
 * @author dgold
 *
 */
public class DataTransferStat {
        
        private long dataElements = 0;
        //     Number of data elements imported in this import
        private long elementStartTime = 0;
        // time this element was started 
        private long elementStopTime = 0;
        // Time the element was completed
        private long datasetStartTime = 0;
        // Time the dataset was started
        private long datasetStopTime = 0;
        // Time the dataset was completed
        private long failedElements = 0;
        // Number of failed elements for this import
        
         private long commitStartTime = 0;
        // Time the commit was started
        private long commitStopTime = 0;
        // Time the commit completed
 
        private boolean commitTimeUsed = false;
        //If you never start commitTime, it won't be used.
        
        private long totalCommitTime = 0;
        
        private long elementsInCommit = 1;
        
        private long numberOfCommits = 0;
        
        private long totalElementTime = 0;
        
        private long elapsedDatasetTime = 0;
        
        private String datasetIdentification = "";
        // end of stats stuff
        
        /**
         * Default constructor. Set the legal state changes.
         */
        public DataTransferStat() {
            super();
        }

        /**
         * Set everything to zero.
         */
        public void clear() {
            commitStartTime = 0;
            commitStopTime = 0;
            dataElements = 0;
            datasetStartTime = 0;
            datasetStopTime = 0;
            elapsedDatasetTime = 0;
            elementStartTime = 0;
            elementStopTime = 0;
            elementsInCommit = 0;
            failedElements = 0;
            numberOfCommits = 0;
            totalCommitTime = 0;
            totalElementTime = 0;
        }
        
        /**
         * Set the processing-start time of the element .
         */
        public void elementStart() {
            elementStartTime = now();
        }
        
        /**
         * Action for recording the processing-stop time and the total elapsed element processing time.
         * Record the element processing-stop time.
         * Add the elapsed processing time for the element to the total processing
         * time for all elements 
         */
        public void elementStop() {
            elementStopTime = now();
            totalElementTime += elementStopTime - elementStartTime;
        }
        
        /**
         * Set the commit-start time.
         */
        public void commitStart() {
            commitTimeUsed = true;
            commitStartTime = now();
        }
        
        /**
         * Action for recording the commit-stop time and the total elapsed commit processing time.
         * Record the commit-stop time.
         * Add the elapsed commit time for the element to the total elapsed
         * time for all commits.
         * increment numbre of ciommits.
         */
        public void commitStop() {
            commitStopTime = now();
            totalCommitTime += commitStopTime - commitStartTime;
            numberOfCommits++;
        }
        
        /**
         * Statr time for the dataset.
         */
        public void datasetStart() {
            clear();
            datasetStartTime = now();
        }
        
        /**
         * Stop & elapsed time for the dataset.
         */
        public void datasetStop() {
            datasetStopTime = now();
            elapsedDatasetTime += datasetStopTime - datasetStartTime;
        }
        /**
         * @return number of data elements so afr.
         */
        public long getDataElements() {
            return this.dataElements;
        }
        
        /**
         * @param dataElements number of data elements seen so far.
         */
        public void setDataElements(long dataElements) {
            this.dataElements = dataElements;
        }
        /**
         * @return number of failed elements s0 far
         */
        public long getFailedElements() {
            return this.failedElements;
        }
        
        /**
         * @param failedElements numbre of elements that failed validation so far.
         */
        public void setFailedElements(long failedElements) {
            this.failedElements = failedElements;
        }
        
        
        /**
         * @return current time in ms from zero-time (1-1-1970)
         */
        public long now() {
            return Calendar.getInstance().getTimeInMillis(); 
        }
        
        /** 
         * convert to a string for the log.
         * @returns a string of the form: Dataset:{id} elements: dataElements (Failed: failedElements). 
         * Elapsed time: elapsedDatasetTime/1000. Batch size: elementsInCommit. 
         * Average commit time: totalCommitTime/numberOfCommits.
         * @return the stats, formatted for humans.
         */
        public String toString() {
            String result = "Dataset:" + getDatasetIdentification() 
                + ". Element count: " + dataElements
                + " (Failed: " + failedElements + "). " 
                + "Elapsed time: " + elapsedDatasetTime / DataTransferBase.A_SECOND + " sec. " 
                + "Batch size: " + elementsInCommit + ". ";
                
                if (commitTimeUsed) {
                    result = result + (numberOfCommits > 0 ? "Average commit time: " + totalCommitTime
                        / numberOfCommits + " ms." : "Total commit time: " + totalCommitTime + " ms.");
                }
            return result;
        }
        
        /**
         * @return Returns the elementsInCommit.
         */
        public long getElementsInCommit() {
            return elementsInCommit;
        }
        
        /**
         * @param elementsInCommit The elementsInCommit to set.
         */
        public void setElementsInCommit(long elementsInCommit) {
            this.elementsInCommit = elementsInCommit;
        }
        
        /**
         * @return Returns the datasetIdentification.
         */
        public String getDatasetIdentification() {
            return datasetIdentification;
        }
        
        /**
         * @param datasetIdentification The datasetIdentification to set.
         */
        public void setDatasetIdentification(String datasetIdentification) {
            this.datasetIdentification = datasetIdentification;
        }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 