/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
    package com.vocollect.voicelink.core.importer.validators;

    import com.vocollect.voicelink.core.importer.VocollectCustomValidator;
import com.opensymphony.xwork2.validator.ValidationException;

    /**
     * @author jtauberg
     * Use the value of a field in this object as the min length allowed
     * for the field that this validator was placed on.
     * 
     */
    public class ConditionalFieldValueMinLengthValidator extends VocollectCustomValidator {
        
        private String controllingField = "";
        private String minDataLength = null;


        /**
         * Use the value of a field in this object as the min length allowed
         * for the field that this validator was placed on.
         * {@inheritDoc}
         * @param arg0
         * @throws ValidationException
         */
        public void validate(Object subject) throws ValidationException {
            boolean result = true;
            String fieldname = getFieldName();
            Object value = this.getFieldValue(fieldname, subject);
            minDataLength = this.getFieldValue(controllingField, subject).toString();
            //Since data lengths are integer, test that it could be converted to an integer
            Integer.parseInt(minDataLength);
                
            // Perform the validation
            if (value == null) {
                // Make null equate to empty string for length check.
                value = "";
            }
            result =  minimumDataLengthCheck(value.toString(), this.minDataLength);
            addValidationError(fieldname, subject, result);
        }

        
        /**
         * Gets the value of controllingField.
         * @return the controllingField
         */
        public String getControllingField() {
            return controllingField;
        }

        /**
         * Sets the value of the controllingField.
         * @param controllingField the controllingField to set
         */
        public void setControllingField(String controllingField) {
            this.controllingField = controllingField;
        }

        
        /**
         * Should only be called after validation or vaule returned will be null.
         * This is used to output a meaningful error message.
         * @return the minDataLength which is the value of the controllingField.
         */
        public String getMinDataLength() {
            if (minDataLength == null) {
                return "<null>";
            }
            return minDataLength;
        }

        
        /**
         * this also in MinDataLengthValidator and ConditionalFieldMinLengthValidator.
         * 
         * Checks to make sure that a string of characters is >= a min length.
         * @param stringtocheck specifies the string to check for validity
         * @param stringMinDataLength specifies the minimum length that we are validating
         *         inclusive.
         * @return true if the length is > = the min length passed in.
         */
        private boolean minimumDataLengthCheck(String stringtocheck, String stringMinDataLength) {
            Integer iDataLength = Integer.parseInt(stringMinDataLength);
            return (iDataLength.compareTo(Integer.valueOf(stringtocheck.length())) <= 0);
        }
        
    }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 