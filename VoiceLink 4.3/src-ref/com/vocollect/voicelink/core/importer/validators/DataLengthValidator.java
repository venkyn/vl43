/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;


/**
 * This class validates fields and makes sure they are a given length. 
 *
 * @author jtauberg
 */
public class DataLengthValidator extends VocollectCustomValidator {
    private String dataLength = null;
    
    /**
     * Setter for the dataLength property.
     * @param dataLength the new dataLength value
     */
    public void setDataLength(String dataLength) {
        //Since data lengths are integer, test that it could be converted to an integer
        Integer.parseInt(dataLength);
        this.dataLength = dataLength;
    }
    
    /**
     * Getter for the dataLength property.
     * @return String value of the property
     */
    public String getDataLength() {
        return this.dataLength;
    }
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;
        
        //If value is null return.
        if (value == null) {
            return;
        }
        result = isDataLength(value.toString(), this.dataLength);
        addValidationError(fieldname, actionObj, result);
    }
    
     
    /**
     * Checks to make sure that a string of characters is exactly a given length.
     * @param stringToCheck specifies the string to check for validity
     * @param strDataLength specifies the length that we are validating.
     * @return true if the length equals the length passed in.
     * @author jtauberg
     */
    private boolean isDataLength(String stringToCheck, String strDataLength) {
        Integer iDataLength = Integer.parseInt(strDataLength);
        return 0 == iDataLength.compareTo(stringToCheck.length());
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 