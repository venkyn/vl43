/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.text.DecimalFormat;


/**
 * This class validates fields and makes sure they are greater than or equal to a given min length (inclusive). 
 * @author jtauberg
 */
public class MinDataLengthValidator extends VocollectCustomValidator {
    private String minDataLength = null;
    
    /**
     * Setter for the min property.
     * @param minDataLength the new dataLength value
     */
    public void setMinDataLength(String minDataLength) {
        //Since data lengths are integer, test that it could be converted to an integer
        Integer.parseInt(minDataLength);
        this.minDataLength = minDataLength;
    }
    
    /**
     * Getter for the dataLength property.
     * @return String value of the property
     */
    public String getMinDataLength() {
        return this.minDataLength;
    }
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;
        String valueString = null;
        
        //If value is null return.
        if (value == null) {
            return;
        }
        if (value instanceof Double) {
            String fmtString = "";
            //create a string of dataLength number of pound signs.
            for (int x = 0; x < Integer.parseInt(this.minDataLength); x++) {
                fmtString = fmtString + "#";
            }
            //fmtString will be ####.#### (example 4).
            fmtString = fmtString + "." + fmtString;
            DecimalFormat fmt = new DecimalFormat(fmtString);
            valueString = fmt.format(value).toString();
        } else {
            valueString = value.toString();
        }
        
        result =  minimumDataLengthCheck(valueString, this.minDataLength);
        addValidationError(fieldname, actionObj, result);
    }
    
    
    /**
     * this also in ConditionalFieldMinLengthValidator
     *  and ConditionalFieldValueMinLengthValidator.
     * 
     * Checks to make sure that a string of characters is >= a min length.
     * @param stringtocheck specifies the string to check for validity
     * @param stringMinDataLength specifies the minimum length that we are validating
     *         inclusive.
     * @return true if the length is > = the min length passed in.
     */
    private boolean minimumDataLengthCheck(String stringtocheck, String stringMinDataLength) {
        Integer iDataLength = Integer.parseInt(stringMinDataLength);
        return (iDataLength.compareTo(Integer.valueOf(stringtocheck.length())) <= 0);
    }
 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 