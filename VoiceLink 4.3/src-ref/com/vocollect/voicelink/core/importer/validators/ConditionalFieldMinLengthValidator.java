/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;
import com.opensymphony.xwork2.validator.ValidationException;


/**
 * @author jtauberg
 *
 */
public class ConditionalFieldMinLengthValidator extends VocollectCustomValidator {
    
    private String controllingField = "";
    private String controllingFieldValue = "";
    private String minDataLength = null;


    /**
     * If a field in this object has a given value, then ensure the field that
     * this validator was placed on meets minimum length requirements given.
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    public void validate(Object subject) throws ValidationException {
        boolean result = true;
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, subject);
        Object controllingValue = this.getFieldValue(controllingField, subject);
        
        //If the controlling Field's value is equal to the controllingFieldValue passed in
        if (0 == controllingValue.toString().compareTo(controllingFieldValue)) {
            //If minDataLength passed in is empty or null return.
            if (minDataLength == null) {
                return;
            }
            // Perform the validation
            result =  minimumDataLengthCheck(value.toString(), this.minDataLength);
            addValidationError(fieldname, subject, result);
        }
    }

    /**
     * Gets the value of controllingField.
     * @return the controllingField
     */
    public String getControllingField() {
        return controllingField;
    }

    /**
     * Sets the value of the controllingField.
     * @param controllingField the controllingField to set
     */
    public void setControllingField(String controllingField) {
        this.controllingField = controllingField;
    }

    /**
     * Gets the value of controllingFieldValue.
     * @return the controllingFieldValue
     */
    public String getControllingFieldValue() {
        return controllingFieldValue;
    }

    /**
     * Sets the value of the controllingFieldValue.
     * @param controllingFieldValue the controllingFieldValue to set
     */
    public void setControllingFieldValue(String controllingFieldValue) {
        this.controllingFieldValue = controllingFieldValue;
    }

    /**
     * Setter for the min property.
     * @param minDataLength the new dataLength value
     */
    public void setMinDataLength(String minDataLength) {
        //Since data lengths are integer, test that it could be converted to an integer
        Integer.parseInt(minDataLength);
        this.minDataLength = minDataLength;
    }
    
    /**
     * Getter for the dataLength property.
     * @return String value of the property
     */
    public String getMinDataLength() {
        return this.minDataLength;
    }

    
    /**
     * this also in MinDataLengthValidator and ConditionalFieldValueMinLengthValidator.
     * 
     * Checks to make sure that a string of characters is >= a min length.
     * @param stringtocheck specifies the string to check for validity
     * @param stringMinDataLength specifies the minimum length that we are validating
     *         inclusive.
     * @return true if the length is > = the min length passed in.
     */
    private boolean minimumDataLengthCheck(String stringtocheck, String stringMinDataLength) {
        Integer iDataLength = Integer.parseInt(stringMinDataLength);
        return (iDataLength.compareTo(Integer.valueOf(stringtocheck.length())) <= 0);
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 