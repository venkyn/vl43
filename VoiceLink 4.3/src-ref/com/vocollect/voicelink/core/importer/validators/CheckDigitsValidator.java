/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.List;

/**
 * This class validates value in a list of values in accordance to the
 * requireCaseLabelCheckDigits.
 *
 * @author vsubramani
 */
public class CheckDigitsValidator extends VocollectCustomValidator {

    private static final Logger log = new Logger(CheckDigitsValidator.class);

    private String target = null;

    private String targetFieldValue;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator
     *      .Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {

        String fieldname = getFieldName();
        boolean result = false;

        Assignment asgt = (Assignment) actionObj;
        List<Pick> list = asgt.getPicks();

        boolean regionFlag = asgt.getRegion().getProfileNormalAssignment()
                .isRequireCaseLabelCheckDigits();

        if (!regionFlag) {
            return;
        }

        try {
            // For every object in the list get the value of
            // Region.profileNormalAssignment.requireCaseLabelCheckDigits

            for (Object o : list) {

                // If requireCaseLabelCheckDigits is true, get the
                // field value from the object and compare to
                // the value in the XML document. Both should match.
                if (regionFlag) {
                    Object val = this.getFieldValue(target, o);
                    String s = String.valueOf(val);
                    result = (0 == s.compareTo(getTargetFieldValue()));
                    addValidationError(fieldname, actionObj, result);
                }
            }
        } catch (ClassCastException e) {
            log.warn("Validation Context is not for import "
                     + actionObj.toString() + e.getMessage(), e);
        }
    }

    /**
     * 
     * @return target
     */
    public String getTarget() {
        return target;
    }

    /**
     * Sets target.
     * @param target the target
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * 
     * @return targetFieldValue
     */
    public String getTargetFieldValue() {
        return targetFieldValue;
    }

    /**
     * 
     * @param targetFieldValue the field value
     */
    public void setTargetFieldValue(String targetFieldValue) {
        this.targetFieldValue = targetFieldValue;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 