/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.List;

/**
 * This validator replaces the open symphony required field validator. This one checks the raw data 
 * to make sure the input stream was populated and so works on priimtive datatypes as well as objects.
 * It will also work on fields (in the object) with default values.
 * @author dgold
 *
 */
public class RequiredFieldValidator extends VocollectCustomValidator {

    private static final Logger log                     = new Logger(
        RequiredFieldValidator.class);
    
    //Target field name needed to setup through markup when
    //field required validation is different from model field name
    private String targetFieldName = null;
    
    /**
     * @param targetFieldName the new targetFieldName value
     * Setter for the targetFieldName property.
     */
    public void setTargetFieldName(String targetFieldName) {
        this.targetFieldName = targetFieldName;
    }
    
    /**
     * Getter for the targetFieldName property.
     * @return String value of the property
     */
    public String getTargetFieldName() {
        return this.targetFieldName;
    }


    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    public void validate(Object actionObj) throws ValidationException {        
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = true;

        try {

            // get our special context which contains the map of fields for this object.
            ValidationResult.VocollectValidatorContext vvC = 
                (ValidationResult.VocollectValidatorContext) this.getValidatorContext();
            for (FieldMap fm : (List<FieldMap>) vvC.getFields().getFieldMaps()) {

                // Get the raw data (field from the input stream)
                Field rawData = fm.get(fieldname);
                result = true;
                // If the field exists, check the data
                if (null != rawData) {
                    if ((null == rawData.getFieldData()) 
                            || (rawData.getFieldData().trim().length() == 0)) {
                        result = false;
                    }
                } else {
                    //If field doesn't exists check based on the targetFieldName
                    // Get the raw data (field from the input stream)
                    rawData = fm.get(this.getTargetFieldName());
                    // If the field exists, check the data
                    if (null != rawData) {
                        if ((null == rawData.getFieldData()) 
                                || (rawData.getFieldData().trim().length() == 0)) {
                            result = false;
                        }
                    }
                }
                addValidationError(fieldname, actionObj, result);

            }
        } catch (ClassCastException e) {
            //This exception will occur when validator context is not for imports
            //This is not an error. Above validation is applicable for imports only
            log
            .debug("Validation Context is not for import " + actionObj.toString() + e.getMessage());
        }

        //If value is null return.
        if (value == null) {
            return;
        }

        // Check the field in the object.
        if (value.toString().length() == 0) {
            addValidationError(fieldname, actionObj, false);
        }
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 