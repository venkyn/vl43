/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.text.DecimalFormat;


/**
 * This class validates fields and makes sure they are less than or equal to a given max length (inclusive). 
 * @author jtauberg
 */
public class MaxDataLengthValidator extends VocollectCustomValidator {
    private String dataLength = null;
    
    /**
     * Setter for the maxDataLength property.
     * @param maxDataLength the new maxDataLength value
     */
    public void setMaxDataLength(String maxDataLength) {
        //Since data lengths are integer, test that it could be converted to an integer
        Integer.parseInt(maxDataLength);
        this.dataLength = maxDataLength;
    }
    
    /**
     * Getter for the maxDataLength property.
     * @return String value of the property
     */
    public String getMaxDataLength() {
        return this.dataLength;
    }
    

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;
        String valueString = null;
        
        //If value is null return.
        if (value == null) {
            return;
        }
        if (value instanceof Double) {
            String fmtString = "";
            //create a string of dataLength number of pound signs.
            for (int x = 0; x < Integer.parseInt(this.dataLength); x++) {
                fmtString = fmtString + "#";
            }
            //fmtString will be ####.#### (example 4).
            fmtString = fmtString + "." + fmtString;
            DecimalFormat fmt = new DecimalFormat(fmtString);
            valueString = fmt.format(value).toString();
        } else {
            valueString = value.toString();
        }

        result = maximumDataLengthCheck(valueString, this.dataLength);
        addValidationError(fieldname, actionObj, result);
    }
    
     
    /**
     * Checks to make sure that a string of characters is <= a max length.
     * @param stringtocheck specifies the string to check for validity.
     * @param maxDataLength specifies the maximum length that we are validating inclusive.
     * @return true if the length is <= the max length passed in.
     */
    private boolean maximumDataLengthCheck(String stringtocheck, String maxDataLength) {
        Integer iDataLength = Integer.parseInt(maxDataLength);
        return (iDataLength.compareTo(Integer.valueOf(stringtocheck.length())) >= 0);
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 