/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class validates fields and makes sure they are a valid date based on a
 * fprmat passed in.
 *
 * @author jtauberg
 */
public class DateValidator extends VocollectCustomValidator {

    private String dateFormat = null;

    private Date resultantDate;

    private static final Logger log = new Logger(DateValidator.class);

    /**
     * Setter for the dateFormat property.
     * @param stringSimpleDateFormat the new SimpleDateFormat value
     */
    public void setDateFormat(String stringSimpleDateFormat) {
        // test that passed string could be converted to a SimpleDateFormat
        try {
            new SimpleDateFormat(stringSimpleDateFormat);
            this.dateFormat = stringSimpleDateFormat;
        } catch (IllegalArgumentException e) {
            log.fatal(
                "Bad format for date " + stringSimpleDateFormat + ".",
                ValidatorManipulatorErrors.ILLEGAL_DATE_FORMAT, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Getter for the dateFormat property.
     * @return String value of the property
     */
    public String getDateFormat() {
        return this.dateFormat;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.
     *Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;

        // If value is null or empty string return.
        if ((value == null) || (value == "")) {
            return;
        }

        result = isValidDate(value.toString(), dateFormat);
        addValidationError(fieldname, actionObj, result);
    }

    /**
     * Checks to make sure a string passed in is a date in the format passed in.
     * @param stringtocheck specifies the string to check for validity
     * @param stringfdt specifies the format of the date to check.
     * @return true if the string passed in is a date in the proper format.
     * @author jtauberg
     */
    public boolean isValidDate(String stringtocheck, String stringfdt) {
        // example of stringfdt = "yyMMdd";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(stringfdt);
            sdf.setLenient(false);
            resultantDate = sdf.parse(stringtocheck);
            return true;
        } catch (ParseException e) {
            return false;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    
    /**
     * Getter for the resultantDate property.
     * @return the value of the property
     */
    public Date getResultantDate() {
        return this.resultantDate;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 