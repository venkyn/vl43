/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;


/**
 * This class validates fields and makes sure they do not contain any of the
 * disallowed characters passed in as list of characters. 
 *
 * @author jtauberg
 */
public class DisallowedCharactersValidator extends VocollectCustomValidator {
    private String disallowedCharacters = null;
    

    /**
     * Setter for the disallowedCharacters property.
     * @param disallowedCharacters the new disallowedCharacters value
     */
    public void setDisallowedCharacters(String disallowedCharacters) {
        this.disallowedCharacters = disallowedCharacters;
    }
    
    /**
     * Getter for the disallowedCharacters property.
     * @return String value of the property
     */
    public String getDisallowedCharacters() {
        return this.disallowedCharacters;
    }


    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;
        
        //If value is null return.
        if (value == null) {
            return;
        }

        //If disallowedCharacters is empty or null return.
        if ((this.disallowedCharacters == null) || (this.disallowedCharacters == "")) {
            return;
        }

        result = containsDisallowedCharacters(value.toString(), this.disallowedCharacters);
        addValidationError(fieldname, actionObj, result);
    }


    /**  Checks to see if string passed in contains any of the disallowed
     *   characters passed in the disallowed character list.
     * @param stringToCheck specifies the string to check.
     * @param stringDisallowedCharacters contains list of characters to search for.
     * @return true if the stringtocheck contains at least one of the characters in stringDisallowedCharacters.
     * @author jtauberg
     */
    private boolean containsDisallowedCharacters(String stringToCheck, String stringDisallowedCharacters) {
        char c;
        boolean result = true;
        //Loop through each disallowed character
        for (int i = 0; i < stringDisallowedCharacters.length(); i++) {
            c = stringDisallowedCharacters.charAt(i);
            if (stringToCheck.indexOf(c) != -1) {
                //A disallowed character was found.  Fail.
                result = false;
                break;
            }
        }
        return result;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 