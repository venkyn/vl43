/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.List;

/**
 * Class to validate values in dependent field based on the pattern of values in
 * main field. A <code>main</code> field can have 2 pattern of values: Uniform or
 * Non-Uniform. Values are uniform if a field has same value across the records,
 * eg: picks in an assignment. When even if a single record has a different
 * value, it is treated as Non-uniform. If the main field has uniform value, the
 * dependent field can have any value. But, if the main has Non-uniform value,
 * then the <code>dependedentField</code> must have value equal to provided
 * <code>dependentFieldValue</code>, otherwise the validation will fail
 * 
 * @author mraj
 * 
 */
public class MainAndDependentDataListValidator extends
    VocollectCustomValidator {

    //Field which will be checked for Uniform or Non-Uniform value
    private String mainField = "";
    
    //Field of which value will be validated w.r.t. to dependentFieldValue if the mainField is non-uniform
    private String dependentField = "";
    
    //value to which the dependentField value should be equal to in case mainField has non-uniform value 
    private String dependentFieldValue = "";
    
    private boolean fieldDataSet = false;
    
    private String fieldData = "";
    
    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    @Override
    public void validate(Object subject) throws ValidationException {
        fieldDataSet = false;
        boolean isMainListNonUniform = false;
        
        if (0 == mainField.trim().compareTo("")) {
            mainField = this.getFieldName();
        }
        
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        List<? extends FieldMap> list = vvC.getFields().getFieldMaps();

        for (FieldMap fm : list) {
            if (fieldDataSet) {
                if (0 != fieldData.compareTo(fm.get(mainField).getFieldData())) {
                    isMainListNonUniform = true;
                    break;
                }
            } else {
                fieldData = fm.get(mainField).getFieldData();
                fieldDataSet = true;
            }
        }
        
        // If the values in main field are not uniform across the list,
        // validation will be successfull only when the dependent field has no
        // value 
        if (isMainListNonUniform) {
            // Check for the dependent field value should match the provided
            // value
            for (FieldMap fm : list) {
                String fieldVal = fm.get(dependentField).getFieldData().trim();
                if (0 != dependentFieldValue.trim().compareTo(fieldVal)) {
                    this.addValidationError(dependentField, subject, false);
                }
            }
        }
    }

    
    /**
     * @return the mainField
     */
    public String getMainField() {
        return mainField;
    }

    
    /**
     * @param mainField the mainField to set
     */
    public void setMainField(String mainField) {
        this.mainField = mainField;
    }

    
    /**
     * @return the dependedentField
     */
    public String getDependentField() {
        return dependentField;
    }

    
    /**
     * @param dependentField the dependedentField to set
     */
    public void setDependentField(String dependentField) {
        this.dependentField = dependentField;
    }


    
    /**
     * @return the dependedentFieldData
     */
    public String getDependentFieldValue() {
        return dependentFieldValue;
    }


    
    /**
     * @param dependentFieldValue the dependedentFieldValue to set
     */
    public void setDependentFieldValue(String dependentFieldValue) {
        this.dependentFieldValue = dependentFieldValue;
    }


    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 