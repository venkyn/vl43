/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.List;

/**
 * @author dgold
 *
 */
public class UniformValueInOriginalDataList extends VocollectCustomValidator {

    private String targetField = "";
    
    private boolean fieldDataSet = false;
    
    private String fieldData = "";
    

    /**
     * For this one, we just care about the name of the field we're checking in the original data.
     * We just want to be sure that all of the elements are the same.
     * 
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    @SuppressWarnings("unchecked")
    public void validate(Object subject) throws ValidationException {
        fieldDataSet = false;
        
        if (0 == targetField.trim().compareTo("")) {
            targetField = this.getFieldName();
        }
        
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        List<? extends FieldMap> list = vvC.getFields().getFieldMaps();

        for (FieldMap fm : list) {
            if (fieldDataSet) {
                if (0 != fieldData.compareTo(fm.get(targetField).getFieldData())) {
                    this.addValidationError(targetField, subject, false);
                }
            } else {
                fieldData = fm.get(targetField).getFieldData();
                fieldDataSet = true;
            }
        }

    }


    /**
     * Gets the value of targetField.
     * @return the targetField
     */
    public String getTargetField() {
        return targetField;
    }



    /**
     * Sets the value of the targetField.
     * @param targetField the targetField to set
     */
    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 