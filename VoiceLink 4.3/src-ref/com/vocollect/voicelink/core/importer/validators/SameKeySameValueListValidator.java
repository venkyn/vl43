/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;

/**
 * This class validates value in a list of values and ensures that for a given
 * key, all entries have the same value.
 *
 * For each item in list whose keyField is same, target field must also be same.
 *
 * @author jtauberg
 */
public class SameKeySameValueListValidator extends VocollectCustomValidator  {

    private static final Logger log = new Logger(ListValidator.class);

    //The field we are evaluating.
    private String targetField = "";

    //The getter for the field we are evaluating in the object (in the list).
    private String targetFieldGetter = "";

    //The keyField we are using to segment the list.
    private String keyField = "";

    //The getter for the keyField in the object (in the list).
    private String keyFieldGetter = "";



    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator
     *      .Validator#validate(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    public void validate(Object actionObj) throws ValidationException {
        Method keyGetterMethod = null;
        Method targetGetterMethod = null;

        //Create a hashmap to store the values found...
        HashMap hm = new HashMap();

        //Get the fieldname of the list object.
        String fieldname = getFieldName();
        //Get that fieldname's value (the list) fromo actionObj and call it value.
        Object value = this.getFieldValue(fieldname, actionObj);
        //Convert the value into a Collection called list.
        Collection list = (Collection) value;

        //Find the getter methods for key and target.
        for (Object o : list) {
            keyGetterMethod = getGetterMethod(this.getKeyFieldGetter(), o);
            targetGetterMethod = getGetterMethod(this.getTargetFieldGetter(), o);
            break;
        }


        try {
            for (Object o : list) {
                Object key = null;
                Object target = null;
                // Get the key and target value.
                try {
                    key = keyGetterMethod.invoke(o, (Object[]) null);
                    target = targetGetterMethod.invoke(o, (Object[]) null);
                } catch (InvocationTargetException e) {
                    Class<? extends Object> exampleClass = o.getClass();
                    String message = "Caught Target Excepiton: " + e.getTargetException()
                    + " while trying to execute the two getter methods named "
                    + exampleClass.getCanonicalName() + "." + this.getKeyFieldGetter() + " or else "
                    + exampleClass.getCanonicalName() + "." + this.getTargetFieldGetter() + " for "
                    + exampleClass.getCanonicalName();
                    log.error(message);
                    throw new RuntimeException(message);
                } catch (Exception e) {
                    Class<? extends Object> exampleClass = o.getClass();
                    String message = "Caught " + e.getClass()
                                     + " while trying to execute the two getter methods named "
                                     + exampleClass.getCanonicalName() + "." + this.getKeyFieldGetter() + " or else "
                                     + exampleClass.getCanonicalName() + "." + this.getTargetFieldGetter() + " for "
                                     + exampleClass.getCanonicalName();
                    // This is due to a problem with a getter name passed in.
                    // Most likely due to a typo or other name-related issue
                    log.error(message);
                    throw new RuntimeException(message);
                }

                //Try and lookup the key to see if we have seen it yet...
                if (hm.get(key) == null) {
                    // we didn't find the key so we will add this key and target
                    hm.put(key, target);
                } else {
                    // we found the key in hash map. Make sure this new target
                    // value matched the value in map.
                    if (!hm.get(key).equals(target)) {
                        // adding validation error if fields
                        this.addValidationError(targetField, actionObj, false);
                    }
                }
            }

        } catch (ClassCastException e) {
            log.debug("Validation Context is not for import "
                + actionObj.toString() + e.getMessage());
        }
    }


    /**
     * Getter for the targetField property.
     * @return String value of the property
     */
    public String getTargetField() {
        return targetField;
    }


    /**
     * Setter for the targetField property.
     * @param targetField the new targetField value
     */
    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }


    /**
     * Getter for the targetFieldGetter property.
     * @return String value of the property
     */
    public String getTargetFieldGetter() {
        return targetFieldGetter;
    }


    /**
     * Getter for the keyField property.
     * @return String value of the property
     */
    public String getKeyField() {
        return keyField;
    }


    /**
     * Setter for the keyField property.
     * @param keyField the new keyField value
     */
    public void setKeyField(String keyField) {
        this.keyField = keyField;
    }


    /**
     * Setter for the targetFieldGetter property.
     * Note that the value passed in will have the first letter capitalized and
     * then  have "get" added to the front.
     * @param targetFieldGetter the new targetFieldGetter value
     */
    public void setTargetFieldGetter(String targetFieldGetter) {
        //Add "get" to Getter Name and camelcase.
        char[] temp = targetFieldGetter.toCharArray();
        temp[0] = Character.toUpperCase(temp[0]);
        this.targetFieldGetter = "get" + String.valueOf(temp);
    }


    /**
     * Getter for the keyFieldGetter property.
     * @return String value of the property
     */
    public String getKeyFieldGetter() {
        return keyFieldGetter;
    }


    /**
     * Setter for the keyFieldGetter property.
     * Note that the value passed in will have "get" added to the
     * front and the first letter Capitalized.
     * @param keyFieldGetter the new keyFieldGetter value
     */
    public void setKeyFieldGetter(String keyFieldGetter) {
        //Add "get" to Getter Name and camelcase.
        char[] temp = keyFieldGetter.toCharArray();
        temp[0] = Character.toUpperCase(temp[0]);
        this.keyFieldGetter = "get" + String.valueOf(temp);
    }


    /**
     * This tries to find the getter method by the exact name passed in
     * fieldGetterName from the object passed in as o.
     * @param fieldGetterName - a getter name of the method to find in o.
     * @param o - the object to find the getter method in.
     * @return - returns the first method from o whose name matches the one
     *              passed in.
     */
    public Method getGetterMethod(String fieldGetterName, Object o) {
        Method result = null;

        //Get the class of the object passed in.
        Class<? extends Object> exampleClass = o.getClass();
        //Get array of methods for that class.
        Method[] methods = exampleClass.getMethods();

        // Can't just getMethod, don't have a list of parameters...
        //Find it by name...
        for (Method m : methods) {
            if (0 == fieldGetterName.compareTo(m.getName())) {
                // Found it, so exit
                result = m;
                break;
            }
        }
        return result;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 