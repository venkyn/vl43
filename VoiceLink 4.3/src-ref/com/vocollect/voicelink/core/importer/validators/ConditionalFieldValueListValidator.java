/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.Collection;

/**
 * Validator that conditionally checks to see that a field in the list is populated.
 * This validator says 
 * "If the value for the controllingField matches controllingFieldValue, evaluate:
 * if the targetFieldValue is null, just require that the dependent field is populated
 * otherwise, require that the value for the dependent field == the targetFieldValue
 * 
 * The ifAndOnlyIf modifier changes this slightly. With ifAndOnlyIf == true, this
 * will only permit values in the dependentField if the value for the controllingField
 * matches the controllingFieldValue (used primarily for booleans).
 * 
 * @author dgold
 *
 */
public class ConditionalFieldValueListValidator extends
VocollectCustomValidator {

    /**
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */

    private String controllingField = "";
    private String controllingFieldValue = "";
    private String dependentField = "";
    private String targetFieldValue = "";
    private boolean ifAndOnlyIf = false;

    /**
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    public void validate(Object subject) throws ValidationException {
        String fieldname = getFieldName();
        Object listField = this.getFieldValue(fieldname, subject);
        Object controllingFieldData = getFieldValue(controllingField, subject);
        Collection<?> list = (Collection<?>) listField;
        boolean result = true;

        if (null == controllingFieldData) {
            return;
        }
        try {
            // IfAndOnlyIf says that If the controlling field has the right value, then 
            // the field should be populated. If not, then the field should NOT be populated.
            // targetFieldValue controls whether we require a specific value in the field, or any value.
            if (!ifAndOnlyIf) {
                if (0 != controllingFieldData.toString().compareTo(controllingFieldValue)) {
                    // If the controlling field is not matched, we don't care.
                    return;
                }
                // If the targetFieldValue is null, we just want any value in the field
                if (null == targetFieldValue) {
                    result = requireAny(list);
                } else {
                    result = requireMatching(list);
                }
            } else {
                // If the controlling field value matches, we match target or any, as above
                if (0 == controllingFieldData.toString().compareTo(controllingFieldValue)) {
                    if (null == targetFieldValue) {
                        result = requireAny(list);
                    } else {
                        result = requireMatching(list);
                    }
                } else {
                    // We require NO value for the field
                    result = requireNone(list);
                }
            }
                
        } catch (ClassCastException e) {
            log.debug("Validation Context is not for import "
                    + subject.toString() + e.getMessage());
        }
        addValidationError(fieldname, subject, result);
    }
    
    /**
     * Require that all of the objects' field values match the target field value.
     * @param list list of obects to examine
     * @return true if all the dependentField's values match the targetfielfdvalue
     * @throws ValidationException framework, from getFieldValue
     */
    public boolean requireMatching(Collection<?> list) throws ValidationException {
        boolean result = true;
        for (Object o : list) {
            Object dependentFieldValue = getFieldValue(dependentField, o);

            if (0 != dependentFieldValue.toString().trim().compareTo(targetFieldValue)) {
                result = false;
            }
        }
        return result;
    }
    
    /**
     * Require that all elements have a value for field.
     * @param list list of obects to examine
     * @return true if all the dependentFields are populated
     * @throws ValidationException framework, from getFieldValue
     */
    public boolean requireAny(Collection<?> list) throws ValidationException {
        boolean result = true;
        for (Object o : list) {
            Object dependentFieldValue = getFieldValue(dependentField, o);
            if (null == dependentFieldValue || (dependentFieldValue.toString().trim().length() == 0)) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Require that all elements have a value for field.
     * @param list list of obects to examine
     * @return true if all the dependentFields are not populated
     * @throws ValidationException framework, from getFieldValue
     */
    public boolean requireNone(Collection<?> list) throws ValidationException {
        boolean result = true;
        for (Object o : list) {
            Object dependentFieldValue = getFieldValue(dependentField, o);
            if (!(null == dependentFieldValue || (dependentFieldValue.toString().trim().length() == 0))) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Gets the value of controllingFieldValue.
     * @return the controllingFieldValue
     */
    public String getControllingFieldValue() {
        return controllingFieldValue;
    }

    /**
     * Sets the value of the controllingFieldValue.
     * @param controllingFieldValue the controllingFieldValue to set
     */
    public void setControllingFieldValue(String controllingFieldValue) {
        this.controllingFieldValue = controllingFieldValue;
    }

    /**
     * Gets the value of dependentField.
     * @return the dependentField
     */
    public String getDependentField() {
        return dependentField;
    }

    /**
     * Sets the value of the dependentField.
     * @param dependentField the dependentField to set
     */
    public void setDependentField(String dependentField) {
        this.dependentField = dependentField;
    }

    /**
     * Gets the value of controllingField.
     * @return the controllingField
     */
    public String getControllingField() {
        return controllingField;
    }

    /**
     * Sets the value of the controllingField.
     * @param controllingField the controllingField to set
     */
    public void setControllingField(String controllingField) {
        this.controllingField = controllingField;
    }

    /**
     * Gets the value of targetFieldValue.
     * @return the targetFieldValue
     */
    public String getTargetFieldValue() {
        return targetFieldValue;
    }

    /**
     * Sets the value of the targetFieldValue.
     * @param targetFieldValue the targetFieldValue to set
     */
    public void setTargetFieldValue(String targetFieldValue) {
        this.targetFieldValue = targetFieldValue;
    }

    /**
     * Gets the value of ifAndOnlyIf.
     * @return the ifAndOnlyIf
     */
    public boolean isIfAndOnlyIf() {
        return this.ifAndOnlyIf;
    }

    /**
     * Sets the value of the ifAndOnlyIf.
     * @param ifAndOnlyIf the ifAndOnlyIf to set
     */
    public void setIfAndOnlyIf(boolean ifAndOnlyIf) {
        this.ifAndOnlyIf = ifAndOnlyIf;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 