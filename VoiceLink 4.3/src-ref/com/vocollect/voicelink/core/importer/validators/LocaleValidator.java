/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.model.DataTranslation;
import com.vocollect.epp.util.LocaleAdapter;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.Locale;

/**
 * This class validates a Locale string to ensure that it is one of the
 * Locales that we support.
 *
 * @author ddoubleday
 */
public class LocaleValidator extends VocollectCustomValidator {

    // This is used in displaying the error message on failed validation.
    private String localeString;

    private static final int SHORT_LOCALE_LENGTH = 2;
    private static final int LONG_LOCALE_LENGTH = 5;
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {

        String fieldName = getFieldName();
        localeString = (String) getFieldValue(fieldName, actionObj);
        // The localeString must be either 2 characters long or 5 characters
        // long with an underscore
        if (localeString.length() != SHORT_LOCALE_LENGTH
            && ((localeString.length() != LONG_LOCALE_LENGTH) 
                    && (localeString.indexOf('_') == -1))) {
            
            addValidationError(fieldName, actionObj, false);
        } else {
            // Adjust the Locale, if possible, and test that the adjustedLocale
            // (which is one our supported ones) is not merely the fallback,
            // meaning the adjustment failed to produce a supported Locale.
            Locale adjustedLocale = LocaleAdapter.adjustLocale(localeString);
            // Now Validation succeeds if either the adjusted Locale is not
            // the DEFAULT_LOCALE, or if the original Locale was.
            boolean result = !LocaleAdapter.DEFAULT_LOCALE.toString()
                                    .equals(adjustedLocale.toString()) 
                                    ||
                              LocaleAdapter.DEFAULT_LOCALE.toString()
                                    .equals(localeString);
            if (result && !adjustedLocale.toString().equals(localeString)) {
                // The adjustedLocale differed from what was in the file.
                // Set the adjusted value back into the DataTranslation
                // object.
                ((DataTranslation) actionObj).setLocale(adjustedLocale.toString());
            }
            addValidationError(fieldName, actionObj, result);
        }
    }
    
    /**
     * Getter for the localeString property.
     * @return String value of the property
     */
    public String getLocaleString() {
        return this.localeString;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 