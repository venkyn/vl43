/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;

/**
 * This class validates value in a list of values.
 *
 * @author vsubramani
 */
public class ListValidator extends VocollectCustomValidator {

    private static final Logger log = new Logger(ListValidator.class);

    private ValidationResult validator = new ValidationResult();

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator
     *      .Validator#validate(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        Collection list = (Collection) value;
        try {
            ValidationResult.VocollectValidatorContext vvC =
                (ValidationResult.VocollectValidatorContext)
                                this.getValidatorContext();
            validator.setWriteErrors(false);
            for (Object o : list) {
                // Iterate through the elements in the list,
                // calling validate on each one.
                validator.validate("", vvC.getFields(), o);
                LinkedHashMap<String, ArrayList> temp = new LinkedHashMap<String, ArrayList>(
                        validator.getResultHolder().getFieldErrors());
                int i = 0;
                for (ArrayList<String> errors : temp.values()) {
                    for (String errorString : errors) {
                        vvC.addFieldError(errorString, errors.get(i));
                        i++;
                        //vvC.addActionError(errorString);
//                        log.error("Validation error for : " + errorString,
//                                ImporterError.FAILED_FIELD_VALIDATION);
                    }
                }

                validator.clearErrors();
            }

        } catch (ClassCastException e) {
            log.debug("Validation Context is not for import "
                + actionObj.toString() + e.getMessage());
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 