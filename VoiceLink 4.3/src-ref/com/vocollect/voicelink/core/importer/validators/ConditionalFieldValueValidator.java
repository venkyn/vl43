/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

/**
 * @author dgold
 *
 */
public class ConditionalFieldValueValidator extends VocollectCustomValidator {
    
    private String controllingField = "";
    private String controllingFieldValue = null;
    private String targetValue = null;


    /**
     * Check to see if a field in this object has a value - specific or otherwise - 
     * if a field in this object has a specified value.
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    public void validate(Object subject) throws ValidationException {
        boolean result = true;
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, subject);
        Object controllingValue = this.getFieldValue(controllingField, subject);
         
        if ((controllingFieldValue == null) || (0 == controllingValue.toString().compareTo(controllingFieldValue))) {
            // perform the check
            if (null == targetValue) {
                // The field is required to have any value
                result = (!((null == value) || (0 == "".compareTo(value.toString()))));
            } else {
                // the field is required to have this value
                result = (0 == value.toString().compareTo(targetValue));
            }
        }
        addValidationError(fieldname, subject, result);

    }

    /**
     * Gets the value of controllingField.
     * @return the controllingField
     */
    public String getControllingField() {
        return controllingField;
    }

    /**
     * Sets the value of the controllingField.
     * @param controllingField the controllingField to set
     */
    public void setControllingField(String controllingField) {
        this.controllingField = controllingField;
    }

    /**
     * Gets the value of controllingFieldValue.
     * @return the controllingFieldValue
     */
    public String getControllingFieldValue() {
        return controllingFieldValue;
    }

    /**
     * Sets the value of the controllingFieldValue.
     * @param controllingFieldValue the controllingFieldValue to set
     */
    public void setControllingFieldValue(String controllingFieldValue) {
        this.controllingFieldValue = controllingFieldValue;
    }

    /**
     * Gets the value of targetValue.
     * @return the targetValue
     */
    public String getTargetValue() {
        return targetValue;
    }

    /**
     * Sets the value of the targetValue.
     * @param targetValue the targetValue to set
     */
    public void setTargetValue(String targetValue) {
        this.targetValue = targetValue;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 