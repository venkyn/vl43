/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;


import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;


/**
 * @author jtauberg
 *
 */
public class SkipCurrentRecordBecauseOfDup extends VocollectCustomValidator {

    private Long sourceLineNumber = null;
    
    /**
     * The objectContext has a boolean called SkipCurrentRecordBecauseOfDup.
     * Some parser implementations use an index to set this conditionally to skip
     * duplicate records that should not be processed.  When this is done via the
     * markup (eg. fixedLengthDataSourceParser's index UseLastDup OR UseFirstDup
     * flag), this validator must be called to check the value of the
     * objectContext.SkipCurrentRecordBecauseOfDup flag.
     * 
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    @SuppressWarnings("unchecked")
    public void validate(Object subject) throws ValidationException {
        
        //this can be reported for a field if needed, but not necessarily.
        String fieldname = getFieldName();
        
        //set up the validator context
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        //Set sourceLineNumber
        sourceLineNumber = vvC.getFields().getSourceLineNumber();

        //If the context member isSkipCurrentRecordBecauseOfDup is true then fail
        //the validator, otherwise, pass the validation.
        this.addValidationError(fieldname, subject, !vvC.getFields()
            .isSkipCurrentRecordBecauseOfDup());
    }

    
    /**
     * @return the sourceLineNumber
     */
    public Long getSourceLineNumber() {
        return sourceLineNumber;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 