/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;

/**
 * This class validates fields and makes sure they contain AlphaNumeric.
 * When invertResult is set to false, validator will add the error when
 * validation fails else it will add errors when validation succeed.
 * AlphaNumeric - uses is letter or digit on chracters to support localization.
 * 
 * @author svoruganti
 */
public class AlphaNumericValidator extends VocollectCustomValidator {
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;

        //If value is null return.
        if (value == null) {
            return;
        }
        
        result = isStringAlphaNumeric(value.toString());
        addValidationError(fieldname, actionObj, result);
    }
    
    
    /**
     * Determines whether or not the passed in string alpha numeric.
     * @param stringToCheck specifies the string to check for validity
     * @return false if string is alpha Numeric only
     */
    private boolean isStringAlphaNumeric(String stringToCheck) {
        char c;
        boolean result = true;
        for (int i = 0; i < stringToCheck.length(); i++) {
            c = stringToCheck.charAt(i);
            if (!Character.isLetterOrDigit(c)) {
                result = false;
                break;
            }
        }
         return result;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 