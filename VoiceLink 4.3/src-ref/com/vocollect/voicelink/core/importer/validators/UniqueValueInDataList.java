/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;


import com.vocollect.voicelink.core.importer.VocollectCustomValidator;
import com.opensymphony.xwork2.validator.ValidationException;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Import validator class to verify that every element value in the list is unique.
 * @author mraj
 *
 */
public class UniqueValueInDataList extends VocollectCustomValidator {

    //The field we are evaluating.
    private String targetField = "";

    //The getter for the field we are evaluating in the object (in the list).
    private String targetFieldGetter = "";
    
    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    @Override
    public void validate(Object subject) throws ValidationException {
        Method targetGetterMethod = null;

        // Get the field name of the list object.
        String fieldname = getFieldName();
        
        // Get that fieldname's value (the list) from actionObj and call it
        // value.
        Object value = this.getFieldValue(fieldname, subject);
        
        // Convert the value into a Collection called list.
        Collection list = (Collection) value;

        // Find the getter methods for key and target.
        targetGetterMethod = getGetterMethod(this.getTargetFieldGetter(), list
            .iterator().next());

        Set<Object> fieldVal = new HashSet<Object>();

        try {
            for (Object o : list) {
                Object target = null;
                // Get the key and target value.
                try {
                    target = targetGetterMethod.invoke(o, (Object[]) null);
                } catch (Exception e) {
                    Class<? extends Object> exampleClass = o.getClass();
                    String message = "Caught " + e.getClass()
                        + " while trying to execute the getter method named "
                        + exampleClass.getCanonicalName() + "."
                        + this.getTargetFieldGetter() + " for "
                        + exampleClass.getCanonicalName();
                    // This is due to a problem with a getter name passed in.
                    // Most likely due to a typo or other name-related issue
                    log.error(message);
                    throw new RuntimeException(message);
                }

                if (!fieldVal.add(target)) {
                    this.addValidationError(targetField, subject, false);
                }
            }

        } catch (ClassCastException e) {
            log.debug("Validation Context is not for import "
                + subject.toString() + e.getMessage());
        }
    }

    /**
     * Getter for the keyFieldGetter property.
     * @return String value of the property
     */
    public String getTargetFieldGetter() {
        return targetFieldGetter;
    }


    /**
     * Setter for the keyFieldGetter property.
     * Note that the value passed in will have "get" added to the
     * front and the first letter Capitalized.
     * @param targetFieldGetter the new targetFieldGetter value
     */
    public void setTargetFieldGetter(String targetFieldGetter) {
        //Add "get" to Getter Name and camelcase.
        char[] temp = targetFieldGetter.toCharArray();
        temp[0] = Character.toUpperCase(temp[0]);
        this.targetFieldGetter = "get" + String.valueOf(temp);
    }

    /**
     * This tries to find the getter method by the exact name passed in
     * fieldGetterName from the object passed in as o.
     * @param fieldGetterName - a getter name of the method to find in o.
     * @param o - the object to find the getter method in.
     * @return - returns the first method from o whose name matches the one
     *              passed in.
     */
    public Method getGetterMethod(String fieldGetterName, Object o) {
        Method result = null;

        //Get the class of the object passed in.
        Class<? extends Object> exampleClass = o.getClass();
        //Get array of methods for that class.
        Method[] methods = exampleClass.getMethods();

        // Can't just getMethod, don't have a list of parameters...
        //Find it by name...
        for (Method m : methods) {
            if (0 == fieldGetterName.compareTo(m.getName())) {
                // Found it, so exit
                result = m;
                break;
            }
        }
        return result;
    }
    
    /**
     * @return the targetField
     */
    public String getTargetField() {
        return targetField;
    }

    
    /**
     * @param targetField the targetField to set
     */
    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 