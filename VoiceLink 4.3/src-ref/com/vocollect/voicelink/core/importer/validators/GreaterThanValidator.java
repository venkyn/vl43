/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;


/**
 * This class validates fields and makes sure the value is greater than a minimum value (exclusive).
 *
 * @author jtauberg
 */
public class GreaterThanValidator extends VocollectCustomValidator {
    private Double min = null;
        
    /**
     * Setter for the min property.
     * @param min the new min value
     */
    public void setMin(Double min) {
        this.min = min;
 }
        
    /**
     * Getter for the min property.
     * @return Double value of the property
     */
    public Double getMin() {
        return this.min;
    }
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        Double doubleValue = Double.parseDouble(value.toString());
        boolean result = false;
        
        //If value is null return.
        if (value == null) {
            return;
        }
        
        result = (doubleValue > min);
        addValidationError(fieldname, actionObj, result);
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 