/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.validator.ValidationException;


/**
 * This class validates fields and makes sure they are valid numbers. 
 *
 * @author astein
 */
public class NumericValidator extends VocollectCustomValidator {
    

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        boolean result = false;
        
        //If value is null return.
        if (value == null) {
            return;
        }
        
        if (value.toString().length() != 0) {
            result = isStringNumeric(value.toString()); 
            addValidationError(fieldname, actionObj, result);
        }
    }
    
    
    /**
     * Wrote a boolean returned method for test purposes only.
     * @param valueToCheck is the String object we are checking.
     * @return true if the item validates correctly
     * @throws ValidationException if the method explodes
     */
    public boolean validate(String valueToCheck) throws ValidationException {
        return isStringNumeric(valueToCheck);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 