/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.validator.ActionValidatorManagerFactory;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * @author dgold
 *
 */
public class Validator {
    private static final Logger           log    = new Logger(Validator.class);

    private static ArrayList<ErrorRecord> errors = new ArrayList<ErrorRecord>();

    /**
     * 
     */
    public Validator() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * 
     * @param newGuy Object to validate
     * @param currentLineNumber long to use for logging
     * @param dataSourceName String for logging
     * @return true if item validates
     */
    public boolean validate(Object newGuy, long currentLineNumber,
            String dataSourceName) {
        ValidatorContext vcontext = new DelegatingValidatorContext(newGuy);

        try {
            ActionValidatorManagerFactory.getInstance().validate(newGuy, null,
                    vcontext);
        } catch (com.opensymphony.xwork2.validator.ValidationException e) {
            // TODO This should probably keep track of which pieces are hosed,
            // and refuse to validate them.
            e.printStackTrace();
            log.fatal("Bad configuration for validation of "
                    + newGuy.getClass().getName() + ". Type mismatch.",
                    ImporterError.VALIDATION_TYPE_MISMATCH, e);
            throw new RuntimeException(e);
        } catch (ClassCastException e) {
            log.fatal("Bad configuration for validation of "
                    + newGuy.getClass().getName() + ". Type mismatch.",
                    ImporterError.VALIDATION_TYPE_MISMATCH, e);
            throw new RuntimeException(e);
        }

        if (vcontext.hasErrors() || vcontext.hasFieldErrors()) {
            log.error("Import for " + newGuy.getClass().getName()
                    + " has the following validation errors beginning on line "
                    + currentLineNumber + " of the input source "
                    + dataSourceName, ImporterError.VALIDATION_ERRORS_EXIST);
            for (String err : (LinkedList<String>) vcontext.getActionErrors()) {
                log.error("Validation error for : " + err,
                        ImporterError.FAILED_GENERAL_VALIDATION);
            }
            for (String err : ((LinkedHashMap<String, String>) vcontext
                    .getFieldErrors()).values()) {
                log.error("Validation error for : " + err,
                        ImporterError.FAILED_FIELD_VALIDATION);
            }
            log.error("End of errors for " + newGuy.getClass().getName()
                    + " of the input source " + dataSourceName
                    + " beginning on line " + currentLineNumber,
                    ImporterError.VALIDATION_ERRORS_EXIST);
            errors.add(new ErrorRecord(vcontext, newGuy, currentLineNumber,
                    dataSourceName));
            return false;
        }
        return true;

    }

    /**
     * @return Returns the errors.
     */
    public ArrayList<ErrorRecord> getErrors() {
        return errors;
    }

    /**
     * Clears errors.
     *
     */
    public void clearErrors() {
        errors.clear();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 