/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;


/**
 * Base class that is intended to hold a description of what role the (owner, subclass)
 * is to play, and its specific duties.
 * 
 * @author dgold
 *
 */
public class Description implements Describable {

    private String description = "";

    // Description of this element and its task

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Describable#getDescription()
     */
    public String getDescription() {
        return description;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Describable#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 