/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * This class allows us to handle an object's persistence without the caller being aware of the object's type.
 * We use Spring to handle the instantiation of the GenericManagers we're interested in. This requires
 * us to adhere to a strict naming convention - {datatype}Manager for the bean name,
 * which Spring maps to the actual instance for us.
 *
 *  The class uses a map of type-to-GenericManager to keep track of the object types we've already saved.
 *  It locates the bean by gettinbg the bean by id.
 *
 *  I have left the map publicly settable so we can, if we wish, populate the map using Spring.
 *
 *
 * @author dgold
 *
 */
//@SuppressWarnings({"unchecked", "unused"})

public class PersistenceManager
    implements ApplicationContextAware, BeanFactoryPostProcessor {

    private static final Logger                   log                       = new Logger(
                                                                                    PersistenceManager.class);

    // The sole instance of a log tfor this class.

    private LinkedHashMap<String, GenericManager<Object, ?>> persistenceManagerMap     =
        new LinkedHashMap<String, GenericManager<Object, ?>>();

    // Map of object name to the manager that handles its persistence

    private GenericManager<Object, ?>                        currentPersistenceManager = null;

    // The manager currently handling object persistence

    private String                                currentType               = "";

    // Name of the class for the object being persisted

    private static ApplicationContext             ctx = null;

    // Spring application context

    /**
     * Clear the internal map of entries.
     */
    public void clearMap() {
        persistenceManagerMap.clear();
    }

    // Site name that imported objects are tagged with.
    private String siteName = Site.DEFAULT_SITE_NAME;

    /**
     * Set the persistence manager (GenericManager instance) that corresponds to
     * this object type. This takes the object type and sets the name to lower
     * case.
     *
     * @param subject the object to use for type information.
     * @return true if we were able to set the Manager, false otherwise.
     */
    private boolean setAppropriatePersistenceManager(Object subject) {
        return setAppropriatePersistenceManager(subject.getClass());

    }

    /**
     * Set the persistence manager (GenericManager instance) that corresponds to
     * this Class. This takes the object type and sets the name to lower case.
     *
     * @param clazz
     *            the Class to use for type information.
     * @return true if we were able to set the Manager, false otherwise.
     */
    private boolean setAppropriatePersistenceManager(Class<?> clazz) {
        boolean done = false;
        String inheritanceChain = "";
        setCurrentType(clazz.getSimpleName());
        Class<?> tmpClass = clazz;
        while ((!done) && (tmpClass != null)) {
            inheritanceChain += tmpClass.getSimpleName() + ", ";
            done = findAppropriatePersistenceManager(tmpClass.getSimpleName());
            tmpClass = tmpClass.getSuperclass();
        }
        if (!done) {
            log.error("No bean known by naming convention. Looking for "
                    + getCurrentType() + "Manager" + ". Inheritance chain: " + inheritanceChain,
                    PersistenceManagerError.NO_SUCH_BEAN);
        }
        return done;
    }

    /**
     * Using the class name passed in, find the instance of GenericManager
     * associated with it. If the GenericManager is not in the map, it uses the
     * name of the class to find the bean with the ID the same as the classname,
     * with Manager appended. Ex: Item -> itemManager
     *
     * @param className
     *            name of the class whose manager we're looking for.
     * @return true if we can find it
     */
    private boolean findAppropriatePersistenceManager(String className) {

        // Check to see if it is already mapped
        setCurrentPersistenceManager(persistenceManagerMap
                .get(getCurrentType()));

        if (null == getCurrentPersistenceManager()) {
            // go find the right one by naming convention, which is
            // {typename}Manager
            String nameOfManagerForObject =  className.substring(0, 1).toLowerCase()
            + className.substring(1) + "Manager";

            log.info("Persistence manager for " + getCurrentType()
                    + " not loaded by configuration, loading by bean name: "
                    + nameOfManagerForObject,
                    PersistenceManagerError.LOAD_BY_BEAN_NAME);
            Object cm = null;
            try {
                // get the bean from the site context
                if (!ctx.containsBean("siteContext")) {
                    log.error(
                        "Spring config does not contain site context",
                        SystemErrorCode.CONFIGURATION_ERROR);
                    throw  new NullPointerException("CTX does not contain a site context");
                }
                SiteContext sc = (SiteContext) ctx.getBean("siteContext");

                // set the site context for future use
                SiteContextHolder.setSiteContext(sc);

                // setup the tags for the user
                // this name can be found.

                Site currentSite = null;
                try {
                    currentSite = sc.getSiteByName(getSiteName());

                 } catch (RuntimeException e) {
                    log.error("Unable to get site by name: '" + getSiteName() + "'",
                     ImporterError.BAD_SITE_NAME);
                    throw new BadSiteException(ImporterError.BAD_SITE_NAME, this.getSiteName());
                 }

                 if (currentSite == null) {
                     log.error("Unable to get site tag for site name " + getSiteName(),
                         ImporterError.UNABLE_TO_GET_SITE_TAG);
                        throw new BadSiteException(ImporterError.UNABLE_TO_GET_SITE_TAG, this.getSiteName());
                 }

                sc.setCurrentSite(currentSite);

                sc.setFilterBySite(true);
                sc.setHasAllSiteAccess(true);

                cm = ctx.getBean(nameOfManagerForObject);
            } catch (DataAccessException e) {
                log.error("Error getting site context",
                    SystemErrorCode.CONFIGURATION_ERROR);
               throw new RuntimeException("Error getting site context");
            } catch (BeansException e1) {
                // TODO Figure out what to do about the bean exception - throw?
                // e1.printStackTrace();
                log.info("Unable to find a manager for '" + getCurrentType()
                        + "'.", PersistenceManagerError.NO_MANAGER_BY_NAME);
            }
            // If null, there is no such bean, and we cannot persist this object
            if (null != cm) {
                // Now cast it to a GM type
                try {
                    GenericManager<Object, ?> gm = (GenericManager<Object, ?>) cm;
                    this.setCurrentPersistenceManager(gm);
                    this.persistenceManagerMap.put(getCurrentType(), gm);
                } catch (ClassCastException e) {
                    log
                            .error(
                                    "Bean id '"
                                            + nameOfManagerForObject
                                            + "' is not of GenericManager type. No manager available for "
                                            + getCurrentType(),
                                    PersistenceManagerError.INAPPROPRIATE_BEAN_TYPE);
                }
            } else {
                log.info("No bean known by naming convention. Looking for "
                        + nameOfManagerForObject,
                        PersistenceManagerError.NO_SUCH_BEAN);
                return false;
            }
        }
        return (null != getCurrentPersistenceManager());

    }

    /**
     * get the map of class name to GenericManager.
     *
     * @return LinkedHashMap of String and GenericManager object
     */
    public LinkedHashMap<String, GenericManager<Object, ?>> getPersistenceManagerMap() {
        return persistenceManagerMap;
    }

    /**
     * Set the map of class name to GenericManager.
     *
     * @param persistenceManagerMap
     *            the map to use.
     */
    public void setPersistenceManagerMap(
            LinkedHashMap<String, GenericManager<Object, ?>> persistenceManagerMap) {
        this.persistenceManagerMap = persistenceManagerMap;
    }

    /**
     * Set the persistence manager, and if successful, delete the object by id.
     *
     * @see com.vocollect.epp.service.GenericManager#delete(java.lang.Long)
     * @param id id of the object to delete from persistent storage
     * @param clazz Class used for type information
     * @return true if successful, false if unable to find the manager
     * @throws BusinessRuleException if should not be allowed to delete
     * @throws DataAccessException if not successful
     */
    public boolean delete(Long id, Class<?> clazz) throws BusinessRuleException,
            DataAccessException {
        if (setAppropriatePersistenceManager(clazz)) {
            currentPersistenceManager.delete(id);
            return true;
        }
        return false;
    }

    /**
     * Delete the object passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#delete(T)
     * @param instance
     *            the instance to delete from persistent storage
     * @return true if successful, false if unable to find the manager
     * @throws BusinessRuleException if not allowed to delete
     * @throws DataAccessException if unsuccessful
     */
    public boolean delete(Object instance) throws BusinessRuleException,
            DataAccessException {
        if (setAppropriatePersistenceManager(instance)) {
            currentPersistenceManager.delete(instance);
            return true;
        }
        return false;
    }

    /**
     * Get the object by the ID passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#get(java.lang.Long)
     * @param id
     *            id of the object to get from persistent storage
     * @param clazz
     *            Type information
     * @return true if successful, false if unable to find the manager
     * @throws DataAccessException if unsuccessful
     */
    public Object get(Long id, Class<?> clazz) throws DataAccessException {
        if (setAppropriatePersistenceManager(clazz)) {
            return currentPersistenceManager.get(id);
        }
        return null;
    }

    /**
     * Get all of the type of object passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#getaLL(T)
     * @param clazz
     *            Type information
     * @return a new list if successful, null if unable to find the manager
     * @throws DataAccessException if unsuccessful
     */
    @SuppressWarnings("unchecked")
    public List getAll(Class<?> clazz) throws DataAccessException {
        if (setAppropriatePersistenceManager(clazz)) {
            return currentPersistenceManager.getAll();
        }
        return null;
    }

    /**
     * Get a count of the type of object passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#getCount()
     * @param clazz Type information
     * @return the count of objects if successful, -1 if unable to find the
     *         manager
     * @throws DataAccessException if unsuccessful
     */
    public long getCount(Class<?> clazz) throws DataAccessException {
        if (setAppropriatePersistenceManager(clazz)) {
            return currentPersistenceManager.getCount();
        }
        return -1;
    }

    /**
     * Get the primary DAO of the type of object passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#getPrimaryDAO()
     * @param clazz Type information
     * @return the GenericDAO if successful, null if unable to find the manager
     * @throws BusinessRuleException
     * @throws DataAccessException
     */
    public GenericDAO<?> getPrimaryDAO(Class<?> clazz) {
        if (setAppropriatePersistenceManager(clazz)) {
            return currentPersistenceManager.getPrimaryDAO();
        }
        return null;
    }

    /**
     * Get the objects matching the selection criteria provided by the
     * exampleInstance.
     *
     * @see com.vocollect.epp.service.GenericManager#queryByExample(T)
     * @param exampleInstance Type information and query-by-example instance
     * @return a new list if successful, null if unable to find the manager
     * @throws DataAccessException if unsuccessful
     */
    public List<Object> queryByExample(Object exampleInstance)
            throws DataAccessException {
        if (setAppropriatePersistenceManager(exampleInstance)) {
            return currentPersistenceManager.queryByExample(exampleInstance);
        }
        return null;
    }

    /**
     * Save the objecy passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#save(T)
     * @param instance
     *            Type information and instance to save
     * @return true if successful, false if unable to find the manager
     * @throws BusinessRuleException if not allowed to save
     * @throws DataAccessException if unsuccessful
     */
    public boolean save(Object instance) throws BusinessRuleException,
            DataAccessException {
        if (setAppropriatePersistenceManager(instance)) {
            currentPersistenceManager.save(instance);
            return true;
        }
        return false;
    }

    /**
     * Save the list of objects passed in.
     *
     * @see com.vocollect.epp.service.GenericManager#save(List)
     * @param instance
     *            Type information and instance to save
     * @return true if successful, false if unable to find the manager
     * @throws BusinessRuleException if not allowed to save
     * @throws DataAccessException if unsuccessful
     */
    public boolean save(List<Object> instance) throws BusinessRuleException,
    DataAccessException {
        if (setAppropriatePersistenceManager(instance.get(0))) {
            currentPersistenceManager.save(instance);
            currentPersistenceManager.getPrimaryDAO().clearSession();
            return true;
        }
        return false;
    }

    /**
     * @return Returns the currentPersistenceManager.
     */
    private GenericManager<Object, ?> getCurrentPersistenceManager() {
        return currentPersistenceManager;
    }

    /**
     * @param currentPersistenceManager
     *            The currentPersistenceManager to set.
     */
    private void setCurrentPersistenceManager(
            GenericManager<Object, ?> currentPersistenceManager) {
        this.currentPersistenceManager = currentPersistenceManager;
    }

    /**
     * @return Returns the currentType.
     */
    public String getCurrentType() {
        return currentType;
    }

    /**
     * @param currentType The currentType to set.
     */
    private void setCurrentType(String currentType) {
        this.currentType = currentType.substring(0, 1).toLowerCase()
                + currentType.substring(1);
    }

    /**
     * @param context specifies ApplicationContext to set
     * @throws BeansException if unsuccessful
     */
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        ctx = context;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
        throws BeansException {
        // Just implementing this causes the static ctx variable to be set,
        // because an instance is created by Spring.
    }


    /**
     * @return the site name
     */
    public String getSiteName() {
        return siteName;
    }


    /**
     * Sets a site name.
     * @param siteName the name
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * Find one or more objects in the database using a generic finder.
     *
     * @param exampleObject the instance of the object containing the values
     *                      we'll use to look up the object we're interested in.
     * @param finderName name of the finder method we want to use to look up values.
     * @param fields List of the field names we want to use.
     * @return either a list of a single oDataObject
     * @throws Throwable on any error
     */
    public Object find(Object exampleObject, String finderName, String[] fields) throws Throwable {
        // get the appropriate generic manager for the type passed in.
        Object result = null;
        if (setAppropriatePersistenceManager(exampleObject.getClass())) {

            GenericManager<?, ?> gm = getCurrentPersistenceManager();
            // get the finder by name from the persistence manager
            Class<?> cm = gm.getClass();
            Method[] methods = cm.getMethods();
            Method finder = null;

            // Can't just getMethod, don't have a list of parameters...
            for (Method m : methods) {
                if (0 == (finderName.trim()).compareTo(m.getName())) {
                    // May want to keep a map of the finders around, rather than this way. Or not.
                    finder = m;
                    // Found what we're looking for, pop out.
                    break;
                }
            }

            if (null == finder) {
                String message = "No finder by name '" + finderName
                    + "' for manager for " + getCurrentType();
                log.error(message, PersistenceManagerError.NO_SUCH_FINDER);
                throw new RuntimeException(message);
            }

            Class<? extends Object> exampleClass = exampleObject.getClass();
            Object[] values = new Object[fields.length];
            int index = 0;

            // Build an array of values from the exampleObject that matches the fields passed in
            for (String fieldGetterName : fields) {
                try {
                    char[] temp = fieldGetterName.toCharArray();
                    temp[0] = Character.toUpperCase(temp[0]);
                    fieldGetterName = "get" + String.valueOf(temp);
                    values[index++] =
                        exampleClass.
                            getMethod(fieldGetterName, (Class[]) null).invoke(exampleObject, (Object[]) null);
                } catch (Exception e) {
                    String message = "Caught " + e.getClass()
                                     + " while trying to get or invoke the method named "
                                     + exampleClass.getCanonicalName() + "." + fieldGetterName;
                    // This is due to one a member access problem,
                    // and represents a problem accessing the named parameter in the subject.
                    // Most likely due to a typo or other name-related issue
                    log.error(message, PersistenceManagerError.ILLEGAL_MEMBER_ACCESS);
                    throw new RuntimeException(message);
                    // TODO Need user message?
                }
            }

            // call the method
            try {
                result = finder.invoke(gm, values);
            } catch (InvocationTargetException e) {
                // if a reflection error happened fish the actual exception from the message
                throw e.getTargetException();
            } catch (Exception e) {
                String message = "Caught " + e.getClass()
                                 + " while trying to execute the finder method named "
                                 + cm.getCanonicalName() + "." + finderName + " for "
                                 + exampleClass.getCanonicalName();
                // This is due to a problem with the finder
                // Most likely due to a typo or other name-related issue
                log.error(message, PersistenceManagerError.BAD_FINDER_INVOCATION, e);
                throw new RuntimeException(message);
                // TODO Need user message?
            }

            return result;

        }
        return result;
    }

    /**
     * method used to clear session between processes.
     */
    public void clearSession() {
        if (currentPersistenceManager != null) {
            currentPersistenceManager.getPrimaryDAO().clearSession();
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Import/Export - could not clear session");
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 