/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.HashSet;
import java.util.Set;

/**
 * @author dgold
 */

public class StateChanges {

    private Set<StateChange> legalStateChanges = null;

    private InternalState    currentState      = null;

    /**
     * 
     */
    public StateChanges() {
        super();
        this.legalStateChanges = new HashSet<StateChange>();
    }

    /**
     * Constructor with initialSize integer.
     * @param initialSize int to set
     */
    public StateChanges(int initialSize) {
        super();
        this.legalStateChanges = new HashSet<StateChange>(initialSize);
    }

    /**
     * 
     * @param legalChange specifies StateChange object to add
     */
    public void add(StateChange legalChange) {
        this.legalStateChanges.add(legalChange);
    }

    /**
     * 
     * @param from specifies InternalState object to add from
     * @param to specifies InternalState object to add to
     */
    public void add(InternalState from, InternalState to) {
        add(new StateChange(from, to));
        return;
    }

    /**
     * 
     * @param newState specifies the InternalState object to set to
     * @return true if the request is allowed
     */
    public boolean setCurrentState(InternalState newState) {
        if (null == this.currentState) {
            this.currentState = newState;
            return true;
        }
        StateChange request = new StateChange(this.getCurrentState(), newState);
        if (this.legalStateChanges.contains(request)) {
            this.currentState = newState;
            return true;
        }
        return false;
    }

    /**
     * 
     * @return the InternalState object
     */
    public InternalState getCurrentState() {
        return this.currentState;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 