/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Calendar;
import java.util.HashMap;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Base class for the Importer and Exporter.
 * @author dgold
 *
 */
public abstract class DataTransferBase 
    implements ApplicationContextAware, BeanFactoryPostProcessor {

    static final Logger               log                        = new Logger(
                                                                         DataTransferBase.class);
    
    // Spring context
    private static ApplicationContext ctx                        = null;


    // milliseconds in a second
    public static final long          A_SECOND                   = 1000;

    // quarter second - convenient throttling nap
    public static final long          QUARTER_SECOND             = 250;

    // Place for the purpose of the data transfer
    private Description               description                = new Description();
    
    // String representing the Site with which this (IMporter/Exporter) is associated.
    private String                    siteName                   = null;

    // Handle database operations anonymously
    private PersistenceManager      persistenceManager         = new PersistenceManager();

    //  Handles Notifications
    private CreateNotification    createNotification         = new CreateNotification();

    // Default delay for throttling
    private final long                defaultThrottlingDelay     = 0;

    // Actual throttling delay
    private long                      throttlingDelay            = defaultThrottlingDelay;

    // What datatype this thing is supposed to be working on
    private String                    datatype                   = null;

    // Critical Setp?
    private boolean                   critical                   = false;

    // The mapping used for unmarshalling
    private Mapping                   mapping                    = null;

    // Name of the file used to set up the marshalling & unmarshalling
    private String                    mappingFileName            = null;

    // Marshaller used to output the objects failing validation
    private Marshaller                failedValidationMarshaller = null;

    // Name of the file to write the failures to. This will include more than just validation failures.
    private String                    failedValidationFileName   = null;


    // UNmarshaller
    private Unmarshaller            unmar                      = null;

    // number of elements to process at one time. Currently MUST be '1'
    private long                      batchSize                  = 1;

    //Nested hash map to store the fieldName/Bind-xml name pair for each class in mapping
    private HashMap<String, HashMap<String, String>> fieldToBindNameMap = null;

    // Statistics on the processing
    private DataTransferStat             stats                      = new DataTransferStat();

    // Number of states that the Transfer can assume
    private final int               numberOfStateChanges       = 35;

    // Special wrapper around the default Castor output file, to allow us to wrrite a list of elements one-at-a-time
    private ErrorFileWriter           failedOutput               = null;

    // Extension for the output file
    private String                    xmlExtension               = ".xml";

    private int                       xmlExtensionLength         = xmlExtension
                                                                         .length();
    //Used to create validation context
    private String validationContextString = null;
    
    public static final String IMPORT_VCS_PREFIX = "import";
    
    public static final String EXPORT_VCS_PREFIX = "export";

    // States for the Transfer
    public static final InternalState UNINITIALIZED_STATE =
        new InternalState(0, "Importer.UnInitialized");

    public static final InternalState INITIALIZED_STATE =
        new InternalState(1, "Importer.Initialized");

    public static final InternalState CONFIGURED_STATE =
        new InternalState(2, "Importer.Configured");

    public static final InternalState MISCONFIGURED_STATE =
        new InternalState(3, "Importer.MisConfigured");

    public static final InternalState STARTED_STATE =
       new InternalState(4, "Importer.Started");

    public static final InternalState RUNNING_STATE =
        new InternalState(5, "Importer.Running");

    public static final InternalState STOPPING_STATE =
        new InternalState(6, "Importer.Stopping");

    public static final InternalState STOPPED_STATE =
        new InternalState(7, "Importer.Stopped", InternalState.FinalStateType.Success);

    public static final InternalState STOPPED_IMPERFECT_STATE =
        new InternalState(8, "Importer.StoppedImperfect", InternalState.FinalStateType.Imperfect);

    public static final InternalState NOT_RUN_STATE =
        new InternalState(9, "Importer.NotRun", InternalState.FinalStateType.Success);

    public static final InternalState NO_WORK_STATE =
        new InternalState(10, "Importer.NoWork", InternalState.FinalStateType.NoWork);

    public static final InternalState SUCCESSFUL_STATE =
        new InternalState(11, "Importer.Successful", InternalState.FinalStateType.Success);

    public static final InternalState IMPERFECT_STATE =
        new InternalState(12, "Importer.Imperfect", InternalState.FinalStateType.Imperfect);

    public static final InternalState FAILED_STATE =
        new InternalState(13, "Importer.Failed", InternalState.FinalStateType.Failed);

    public static final InternalState PENDING_STATE =
        new InternalState(14, "Importer.Pending");

//    public static final InternalState RUNNING_NO_WORK_STATE =
//        new InternalState(15, "Importer.Pending");
//
//    public static final InternalState RUNNING_FAILED_STATE =
//        new InternalState(16, "Importer.Pending");

    public static final NotificationProperties MISCONFIGURED_STATE_NOTIFICATION = new NotificationProperties(
            "0", "notification.column.keyname.Message.ImportExport.Misconfigured",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties STOPPED_STATE_NOTIFICATION = new NotificationProperties(
            "1", "notification.column.keyname.Message.ImportExport.ManualStopNoErrors",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties STOPPED_IMPERFECT_STATE_NOTIFICATION = new NotificationProperties(
            "2", "notification.column.keyname.Message.ImportExportManualStopErrors",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties SUCCESSFUL_STATE_NOTIFICATION = new NotificationProperties(
            "3", "notification.column.keyname.Message.ImportExport.Successful",
            NotificationPriority.INFORMATION);

    public static final NotificationProperties IMPERFECT_STATE_NOTIFICATION = new NotificationProperties(
            "4", "notification.column.keyname.Message.ImportExport.SomeErrors",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties FAILED_STATE_NOTIFICATION = new NotificationProperties(
            "5", "notification.column.keyname.Message.ImportExport.Failed",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties STOPPED_NO_WORK_STATE_NOTIFICATION = new NotificationProperties(
            "6", "notification.column.keyname.Message.ImportExport.Stopped",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties PARTIAL_SUCCESS_STATE_NOTIFICATION = new NotificationProperties(
            "7", "notification.column.keyname.Message.ImportExport.PartialSuccess",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties STOPPED_PARTIAL_SUCCESS_STATE_NOTIFICATION = new NotificationProperties(
            "8", "notification.column.keyname.Message.ImportExport.StoppedPartialSuccess",
            NotificationPriority.CRITICAL);

    public static final NotificationProperties NOT_RUN_STATE_NOTIFICATION = new NotificationProperties(
            "9", "notification.column.keyname.Message.ImportExport.NotRun",
            NotificationPriority.WARNING);

    // Nested hash map to store the state/notification properties
    private HashMap<InternalState, NotificationProperties> notificationMap;
    
    // State change management
    private StateChanges            stateChanges               = null;

    // Handle all XML tagging stuff TODO DHG moved this.
    private XmlTagger               xmlrepresentation          = new XmlTagger();

    private DataSourceParser<?, ?>          sourceParser               = null;
    
    // Hibernate session factory - required to permit us to access lazily initialized data from validation and export
    private SessionFactory            sessionFactory;

    private File failedValidationFile = null;
    
    private String failedValidation = "";
    
    // When in fileBatch mode, this stores the total failed elements for the run.
    private long fileBatchTotalFailedElements = 0;

    // When in fileBatch mode, this stores the total elements for the run.
    private long fileBatchTotalElements = 0;

    private Object failedValidationFileNameRoot;

    
    /**
     * default constructor.
     */
    public DataTransferBase() {
        super();
        if (this.getStateChanges() == null) {
            this.setStateChanges(new StateChanges(numberOfStateChanges));
            this.getStateChanges().add(UNINITIALIZED_STATE,     INITIALIZED_STATE);
            this.getStateChanges().add(INITIALIZED_STATE,       CONFIGURED_STATE);
            this.getStateChanges().add(INITIALIZED_STATE,       MISCONFIGURED_STATE);
            this.getStateChanges().add(CONFIGURED_STATE,        PENDING_STATE);
            this.getStateChanges().add(STARTED_STATE,           RUNNING_STATE);
            this.getStateChanges().add(STARTED_STATE,           NOT_RUN_STATE);
            this.getStateChanges().add(STARTED_STATE,           FAILED_STATE);
            this.getStateChanges().add(STARTED_STATE,           NO_WORK_STATE);
            this.getStateChanges().add(RUNNING_STATE,           SUCCESSFUL_STATE);
            this.getStateChanges().add(RUNNING_STATE,           IMPERFECT_STATE);
            this.getStateChanges().add(RUNNING_STATE,           FAILED_STATE);
            this.getStateChanges().add(RUNNING_STATE,           STOPPING_STATE);
            this.getStateChanges().add(RUNNING_STATE,           NO_WORK_STATE);
//            this.getStateChanges().add(RUNNING_STATE,           RUNNING_NO_WORK_STATE);
            this.getStateChanges().add(STOPPING_STATE,          STOPPED_STATE);
            this.getStateChanges().add(STOPPING_STATE,          FAILED_STATE);
            this.getStateChanges().add(PENDING_STATE,           NOT_RUN_STATE);
            this.getStateChanges().add(PENDING_STATE,           STARTED_STATE);
            this.setNotificationMap(new HashMap<InternalState, NotificationProperties>(numberOfStateChanges));
            this.getNotificationMap().put(MISCONFIGURED_STATE, MISCONFIGURED_STATE_NOTIFICATION);
            this.getNotificationMap().put(STOPPED_STATE, STOPPED_STATE_NOTIFICATION);
            this.getNotificationMap().put(STOPPED_IMPERFECT_STATE, STOPPED_IMPERFECT_STATE_NOTIFICATION);
            this.getNotificationMap().put(SUCCESSFUL_STATE, SUCCESSFUL_STATE_NOTIFICATION);
            this.getNotificationMap().put(IMPERFECT_STATE, IMPERFECT_STATE_NOTIFICATION);
            this.getNotificationMap().put(FAILED_STATE, FAILED_STATE_NOTIFICATION);
            this.getNotificationMap().put(NOT_RUN_STATE, NOT_RUN_STATE_NOTIFICATION);
            getNotificationMap().put(DataSourceParser.FAILED_STATE, FAILED_STATE_NOTIFICATION);
            getNotificationMap().put(DataSourceParser.PARTIAL_SUCCESS_STATE, IMPERFECT_STATE_NOTIFICATION);
            getNotificationMap().put(DataSourceParser.SUCCESSFUL_STATE, SUCCESSFUL_STATE_NOTIFICATION);
        }
        this.getStateChanges().setCurrentState(INITIALIZED_STATE);
        return;

    }

    /**
     * Abstract method that runs the import or export for the datatype.
     * 
     * @return true if successful, false otherwise.
     */
    public abstract boolean runDatatypeJob();

    /**
     * This basically sets the state appropriately. If the Importer is running, this will be ignored. The importDatatype
     * method polls the state, and must be called separately.
     * 
     */
    @SuppressWarnings("unchecked")
    public synchronized void start() {
        this.getStats().datasetStart();
        this.getStateChanges().setCurrentState(STARTED_STATE);
        this.getStateChanges().setCurrentState(PENDING_STATE);

        // Create a session for the datatype job.
        sessionFactory = (SessionFactory) this.getApplicationContext().getBean("sessionFactory");
        getNewSession();
        getSourceParser().setFieldToBindNameMap(getFieldToBindNameMap());

    }
    
    /**
     * Get the current session.
     * @return the current hibernate session
     */
    public Session getCurrentSession() {
        return ((SessionHolder) 
                TransactionSynchronizationManager.getResource(sessionFactory)).getSession();
    }
    
    /**
     * Get a new Hibernate session. This is always done when we 'start', and may be done if we encounter
     * an error from the database layer.
     */
    public void getNewSession() {
        if (TransactionSynchronizationManager.hasResource(sessionFactory)) {
            // There shouldn't be a resource bound here, so release it
            // before binding a new one.
            log.warn("Session already bound to thread for key '" 
                + sessionFactory + "' releasing it");
            releaseSession();
        }
        // Bind a new session
        Session session = SessionFactoryUtils.getSession(this.sessionFactory, true);
        TransactionSynchronizationManager.bindResource(
            sessionFactory, new SessionHolder(session));

    }

    /**
     * This basically sets the state appropriately. If the Importer is running, this will be ignored. The importDatatype
     * method polls the state, and must be called separately.
     * 
     */
    public synchronized void prepare() {
        this.getStateChanges().setCurrentState(STARTED_STATE);
        this.getStateChanges().setCurrentState(PENDING_STATE);
    }

    /**
     * This basically sets the state appropriately. If the Importer is running, this will be ignored. The importDatatype
     * method polls the state, and must be called separately.
     * 
     */
    public synchronized void stop() {
        this.getStateChanges().setCurrentState(STOPPING_STATE);
        this.getStateChanges().setCurrentState(NOT_RUN_STATE);
        log.info("Job stopped manually");
    }

    /**
     * Well, this will look weird, but since incorrect state changes are ignored, we just try the applicable end states.
     * Only the correct one will prevail.
     */
    public synchronized void finish() {
        
        releaseSession();
        
        this.getStats().datasetStop();
        if (null != this.failedOutput) {
            try {
                failedOutput.close();
            } catch (IOException e) {
                log.error(getDescription() + ": " + "The failed output file '"
                        + this.getFailedValidationFileName()
                        + "did not close properly. ",
                        ImporterError.FAILED_OUTPUT_FILE_DID_NOT_CLOSE, e);
            }
        }

        // This has to be first, no data elements => no failed elements
        if (0 == this.getStats().getFailedElements()) {
            this.getStateChanges().setCurrentState(SUCCESSFUL_STATE);
            this.getStateChanges().setCurrentState(STOPPED_STATE);            
        } else if (this.getStats().getDataElements() == this.getStats().getFailedElements()) {
            this.getStateChanges().setCurrentState(FAILED_STATE);
        }

        // If the state did not transition, we transition depending on the current state
        this.getStateChanges().setCurrentState(IMPERFECT_STATE);
        this.getStateChanges().setCurrentState(STOPPED_IMPERFECT_STATE);            
        // Creates notification in accordance to the state for import/export.
        try {
            if (failedValidationFile == null) {
                failedValidation = "";
            } else {
                failedValidation = failedValidationFile.getCanonicalPath();
            }
        } catch (IOException e) {
            log.error(getDescription()
                    + ": The FailedValidationFilename is bad/unreachable " 
                    + failedValidation, ImporterError.BAD_FAILED_VALIDATION_FILE_NAME, e);
        }
        this.createNotification.createNotify(this.stateChanges.getCurrentState(),
                getNotificationMap(), this.getDescription(), this.getSiteName(), failedValidation);
 

        saveStats();
        
        purgeUncommittedRecords();
        
        return;
        
    }
    
    /**
     * Unbind the Hibernate session from the thread and release it.
     */
    protected void releaseSession() {
        try {
            Session session = ((SessionHolder) 
                TransactionSynchronizationManager.getResource(sessionFactory)).getSession();
            TransactionSynchronizationManager.unbindResource(sessionFactory);
            SessionFactoryUtils.releaseSession(session, sessionFactory);
        } catch (SessionException e) {
            log.error(getDescription() + ": " + " Hibernate session is closed. "
                    , ImporterError.HIBERNATE_SESSION_ERROR, e);
        }        
    }
    
    
    /**
     * This should be called when completing one of a batch of processing.
     * There is also a batchFinish that needs to be called after the last call
     * of this at the end of the batch.
     * @return booean - true if success, false if fail importer
     */
    public synchronized boolean batchItemFinish() {
        this.getStats().datasetStop();
        if (null != this.failedOutput) {
            try {
                failedOutput.close();
            } catch (IOException e) {
                log.error(getDescription() + ": " + "The failed output file '"
                        + this.getFailedValidationFileName()
                        + "did not close properly. ",
                        ImporterError.FAILED_OUTPUT_FILE_DID_NOT_CLOSE, e);
                saveStats();
                this.getStats().datasetStart();
                return false;
            }
            //Reset failedOutput to null;
            failedOutput = null;
            this.setFailedValidationMarshaller(null);
        }

        // Creates notification in accordance to the state for import/export.
        try {
            if (failedValidationFile == null) {
                failedValidation = "";
            } else {
                failedValidation = failedValidationFile.getCanonicalPath();
            }
        } catch (IOException e) {
            log.error(getDescription()
                    + ": The FailedValidationFilename is bad/unreachable " 
                    + failedValidation, ImporterError.BAD_FAILED_VALIDATION_FILE_NAME, e);
        }
        //Reset failedValidationFile
        failedValidationFile = null;
        failedValidationMarshaller = null;
        resetFailedValidationFileName();
        this.createNotification.createNotify(getSourceParser().getCurrentState(),
                getNotificationMap(), this.getDescription(), this.getSiteName(), failedValidation);

        // Manage self's state
//        getStateChanges().setCurrentState(newState);
        
        saveStats();

        purgeUncommittedRecords();
        return true;
    }
    
    
    
    
    /**
     * When processing batches, this should be called after the last batchItemFinish
     * has been called.
     */
    public synchronized void batchFinish() {
        //Close hibernate session        
        releaseSession();
        this.getStats().datasetStop();

        // This has to be first, no data elements => no failed elements
        if (0 == this.fileBatchTotalFailedElements) {
            this.getStateChanges().setCurrentState(SUCCESSFUL_STATE);
            this.getStateChanges().setCurrentState(STOPPED_STATE);            
        } else if (this.fileBatchTotalElements == this.fileBatchTotalFailedElements) {
            this.getStateChanges().setCurrentState(FAILED_STATE);
        }

        // If the state did not transition, we transition depending on the current state
        this.getStateChanges().setCurrentState(IMPERFECT_STATE);
        this.getStateChanges().setCurrentState(STOPPED_IMPERFECT_STATE);            

        // If the state did not transition, we transition depending on the current state
        this.getStateChanges().setCurrentState(IMPERFECT_STATE);
        this.getStateChanges().setCurrentState(STOPPED_IMPERFECT_STATE); 
        
        // Creates notification in accordance to the state for import/export.
//        this.createNotification.createNotify(this.stateChanges.getCurrentState(),
//                getNotificationMap(), this.getDescription(), this.getSiteName(), "");
        return;
        
    }
    
    /**
     * @return boolean to indicate if the entire import needs to be
     * failed based on the threshold value
     */
    public boolean failEntireImport() {
        boolean failed = false;
        // This has to be first, no data elements => no failed elements
        if (this.getStats().getFailedElements() > 0) {
            if (this.getStats().getDataElements() == this.getStats().getFailedElements()) {
                failed = true;
            }
        }
        return failed;
    }
    

    /**
     * @return the state of this Import
     */
    public synchronized String getState() {
        return getCurrentState().toString();

    }

    /**
     * Thread.sleep for the throttlingDelay number of milliseconds. This allows us to let the rest of the system get
     * resources.
     * 
     */
    protected void takeThrottlingNap() {
        if (0 < this.getThrottlingDelay()) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug(getDescription() + ": "
                            + "Starting delay for throttling ");
                }
                Thread.sleep(this.getThrottlingDelay());
                if (log.isDebugEnabled()) {
                    log.debug(getDescription() + ": "
                            + "Ending delay for throttling ");
                }
            } catch (InterruptedException e) {
                // This is expected if the Stop or Start button is pressed in the ImportManager,
                // and does not call for action
                log.debug(getDescription() + ": "
                        + "Caught interrupted exception during throttling nap");
            }
        }
    }

    /**
     * Rollback,in database terms TODO Make this real after we make batching work..
     * 
     */
    protected void purgeUncommittedRecords() {
        return;
    }

    /**
     * Save the bad record, along with the validation, to the bad record file. TODO Finish this. When we make batching
     * work, we need to make this count up the errors correctly.
     * 
     * @param newGuy
     *            the new object to save
     */
    protected void saveErrorRecord(Object newGuy) {
        this.getStats().setFailedElements(this.getStats().getFailedElements() + 1);
        reportErroredElement(newGuy);
    }
    /**
     * Save the bad record, along with the validation, to the bad record file. TODO Finish this. When we make batching
     * work, we need to make this count up the errors correctly.
     * 
     * @param newGuy
     *            the new object to save
     */
    protected void reportErroredElement(Object newGuy) {
        getSourceParser().failRecord();
        try {
            // Castor currently can't marshal an object while keeping that marshalling file open
            // for more objects to be entered into a subhierarchy
            this.getFailedValidationMarshaller().marshal(newGuy);
        } catch (MarshalException e) {
            log.error(getDescription() + ": "
                    + "Marshalling exception during save of failed record",
                    ImporterError.MARSHALLING_ERROR_RECORD, e);
            // This should NEVER happen - the XML is being generated by Castor,
            // and should be able to be interpreted by castor...
            throw new RuntimeException("ImporterError.MarshallingErrorRecord",
                    e);
        } catch (ValidationException e) {
            log.error(getDescription() + ": "
                      + "Caught XML Validation exception, which should not be turned on",
                      ImporterError.VALIDATION_ERRORS_EXIST3, e);
            return;
        }
    }

    /**
     * Save the stats we have gathered to the log, and maybe to the database.
     * 
     * log first
     * 
     */
    protected void saveStats() {
        getStats().setElementsInCommit(getBatchSize());
        log.info(getDescription() + ": " + getStats().toString());
        //Update the batch totals for elements and failed elements
        this.fileBatchTotalFailedElements = this.fileBatchTotalFailedElements
            + getStats().getFailedElements();
        this.fileBatchTotalElements = this.fileBatchTotalElements
            + getStats().getDataElements();
        getStats().clear();
    }

    /**
     * Configure this using the named configuration file. This sets up the mappings and etc. It also throws a buncha
     * eceptions.
     * 
     * @throws VocollectException
     *             a wrapped exception if there is a confiuration errpr
     */
    @SuppressWarnings("unchecked")
    public void configure() throws VocollectException {
        log.debug(getDescription() + ": " + "Called configure() for Importer");

        CastorConfiguration objectMapping = new CastorConfiguration(
                getMappingFileName(), "");
        // Just for testing purposes just now...
        setMapping(objectMapping.loadMapping());

        try {
            this.setUnmar(new Unmarshaller(getMapping()));
        } catch (MappingException e) {
            log.fatal(getDescription() + ": "
                    + "Configuration error: Unable to set mapping. Bad mapping file? "
                    + getMappingFileName(),
                    ImporterError.BAD_MAPPING_FILE2, e);
             this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);
            //Create an 'Critical' notification there is problem with configuration.
             if (this.getDescription() == null || this.getDescription() == "") {
                 this.setDescription("Import/Export");  
             }
             this.createNotification.createNotify(this.stateChanges.getCurrentState(),
                 getNotificationMap(), this.getDescription(), this.getSiteName(),
                 failedValidation);
            throw new ImportConfigurationException(
                    ImporterError.BAD_MAPPING_FILE2,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(ImporterError.BAD_MAPPING_FILE2),
                            getFailedValidationFileName()));
        }

        this.getStateChanges().setCurrentState(CONFIGURED_STATE);
       
        //populate the nested hashmap fieldToBindNameMap
        this.fieldToBindNameMap = objectMapping.getFieldToBindNameMap(this.getMapping());

        return;
    }

    /**
     * Getter for fieldToBindNameMap property.
     * @return fieldToBindNameMap
     */
    public HashMap<String, HashMap<String, String>> getFieldToBindNameMap() {
        return fieldToBindNameMap;
    }
    
    /**
     * Get the state of the import. See InternalState public final vars.
     * 
     * @return the current state
     */
    public InternalState getCurrentState() {
        return this.getStateChanges().getCurrentState();
    }

    /**
     * number of elements in a batch.
     * 
     * @return batch size.
     */
    public long getBatchSize() {
        return this.batchSize;
    }

    /**
     * Set the size of the batch.
     * 
     * @param batchSize
     *            number of elements in a batch
     */
    public void setBatchSize(long batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * Ansewrs the question "Is this a critical step?" Used by the ImportManager to know when to abort the entire job,
     * as opposed to failing just this step.
     * 
     * @return if this is a critical step.
     */
    public boolean isCritical() {
        return this.critical;
    }

    /**
     * Set if this is a critical step. The configuration will set this. If an Importer flagged as a critical step fails,
     * the import as a whole will fail.
     * 
     * @param critical
     *            flag to indicate (true) that this is a critical step.
     */
    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    /**
     * Get the delay, in ms, that we will sleep to allow rsources to get to the rest of the system.
     * 
     * @return Returns the throttlingDelay.
     */
    public long getThrottlingDelay() {
        return this.throttlingDelay;
    }

    /**
     * Set the delay, in ms, that we will sleep to allow rsources to get to the rest of the system.
     * 
     * @param throttlingDelay
     *            The throttlingDelay to set.
     */
    public void setThrottlingDelay(long throttlingDelay) {
        this.throttlingDelay = throttlingDelay;
    }

    /**
     * This file contains the information about the objects we can create.
     * 
     * @return Returns the mappingFileName.
     */
    public String getMappingFileName() {
        return this.mappingFileName;
    }

    /**
     * This file contains the information about the objects we can create.
     * 
     * @param mappingFileName
     *            The mappingFileName to set.
     */
    public void setMappingFileName(String mappingFileName) {
        this.mappingFileName = mappingFileName;
        try {
            configure();
        } catch (VocollectException e) {
            this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);
            throw new RuntimeException("Bad configuration file.", e);
        }
    }

    /**
     * This file contains an XML representation of the objects that faile validation.
     * 
     * @return Returns the failedValidationFileName.
     */
    public String getFailedValidationFileName() {
        return this.failedValidationFileName;
    }

    /**
     * @param failedValidationFileName
     *            The failedValidationFileName to set.
     */
    public void setFailedValidationFileName(String failedValidationFileName) {

        int index;
        if (failedValidationFileName.endsWith(xmlExtension)) {
            index = failedValidationFileName.length() - xmlExtensionLength;
            String first = failedValidationFileName.substring(0, index);
            failedValidationFileNameRoot = first;
        } else {
            failedValidationFileNameRoot = failedValidationFileName;
        }
        this.failedValidationFileName = failedValidationFileNameRoot + "."
                    + Calendar.getInstance().getTimeInMillis() + ".xml";
        }
    
    /**
     * Set the failed validation file name to the current timestamp appended to the root.
     */
    private void resetFailedValidationFileName() {
        this.failedValidationFileName = failedValidationFileNameRoot + "."
        + Calendar.getInstance().getTimeInMillis() + ".xml";
    }
    /**
     * @return Returns the failedValidationMarshaller after opening the output file..
     */
    protected Marshaller getFailedValidationMarshaller() {
        if (null == this.failedValidationMarshaller) {
            // We are going to use this to get the full path for the file, for reporting.
            //File failedValidationFile = null;
            try {
                
                failedValidationFile = new File(getFailedValidationFileName());
                File dir = failedValidationFile.getParentFile();
                dir.mkdirs();
                //Create output stream with file and append set to true.
                OutputStream os = new FileOutputStream(failedValidationFile, true);
                Writer w = new OutputStreamWriter(os, EncodingSupport.UTF8);
                failedOutput = new ErrorFileWriter(w);
                log.error(getDescription()
                          + ": Some objects have failed validation. Objects failing validation will be written to "
                          + failedValidationFile.getCanonicalPath(),
                                ImporterError.ANNOUNCE_FAILED_VALIDATION);
                this.failedValidationMarshaller = new Marshaller(failedOutput);
                // this.failedValidationMarshaller.setRootElement("");
                this.failedValidationMarshaller.setMapping(this.getMapping());
                failedOutput.write(this.xmlrepresentation.getXmlPrologue());
                failedOutput.setIdentification(this.description
                        .getDescription());
                failedOutput.write(failedOutput.getPrefix());
                this.failedValidationMarshaller.setSupressXMLDeclaration(true);
            } catch (IOException e) {
                log.fatal(getDescription() + ": "
                        + "Configuration error: Unable to write to "
                        + getFailedValidationFileName(),
                        ImporterError.BAD_FAILED_VALIDATION_FILE, e);
                this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);
                throw new RuntimeException(getDescription()
                        + ": Unable to open failed validation file: "
                        + getFailedValidationFileName() + " ("
                        + e.getLocalizedMessage() + ")");
            } catch (MappingException e) {
                // This should have happened during configuration - we're using the same mapping file...
                log.fatal(getDescription() + ": "
                        + "Configuration error: Bad mapping for output ",
                        ImporterError.BAD_MAPPING_FILE3, e);
                this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);
                throw new RuntimeException("Bad or missing mapping file");
            }
        }
        return this.failedValidationMarshaller;
    }

    /**
     * @param failedValidationMarshaller The failedValidationMarshaller to set.
     */
    protected void setFailedValidationMarshaller(
            Marshaller failedValidationMarshaller) {
        this.failedValidationMarshaller = failedValidationMarshaller;
    }

    /**
     * Delegate to description.
     * @see com.vocollect.epp.util.Description#getDescription()
     * @return the description of the improter
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * Delegate to description.
     * @param description the description of the importer
     * @see com.vocollect.epp.util.Description#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        if (null != getSiteName()) {
            this.description.setDescription(getSiteName() + ": " + description);
        }
        this.description.setDescription(description);
        getStats().setDatasetIdentification(description);
    }

    /**
     * @return Returns the mapping.
     */
    private Mapping getMapping() {
        return this.mapping;
    }

    /**
     * @param mapping The mapping to set.
     */
    private void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }

    /**
     * 
     * @return the name of the data imported here
     */
    public String getDatatype() {
        return this.datatype;
    }

    /**
     * Set the name - should reflect the datatype being imported.
     * This is a plain-language description of the datatype being imported
     * @param datatype name of the datatype being imported. 
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
        if ((null == getDescription()) || (0 == getDescription().length())) {
            setDescription(this.datatype);
        }
    }

    /**
     * @return Returns the persistenceManager.
     */
    public PersistenceManager getPersistenceManager() {
        return persistenceManager;
    }

    /**
     * @param persistenceManager The persistenceManager to set.
     */
    public void setPersistenceManager(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

    /**
     * @return the xmlrepresentation
     */
    protected XmlTagger getXmlrepresentation() {
        return xmlrepresentation;
    }

    /**
     * Set the XMLTagger. Cleaner.
     * 
     * @param xmlrepresentation
     *            the xmlrepresentation to set
     */
    protected void setXmlrepresentation(XmlTagger xmlrepresentation) {
        this.xmlrepresentation = xmlrepresentation;
    }

    /**
     * Get the source parser.
     * @return the source parsed
     */
    public DataSourceParser<?, ?> getSourceParser() {
        return this.sourceParser;
    }

    /**
     * Set the source parser.
     * @param sourceParser the parser to use - set by configuration
     */
    public void setSourceParser(DataSourceParser<?, ?> sourceParser) {
        this.sourceParser = sourceParser;
    }

    /**
     * This lets us specify a specific file to use for input.
     * Primarily used for testing just now, but as the UI gets expanded, this will be required.
     * @param dataSourceName name of the data source 
     */
    public void setDataSourceName(String dataSourceName) {
        this.getSourceParser().getSourceAdapter().setDataSourceName(
                dataSourceName);
    }

    /**
     * Gets the value of siteName, the name of the site for which this (Import/Export) will run.
     * @return the siteName
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName, the name of the site for which this (Import/Export) will run.
     * @param siteName the siteName to set
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
        setDescription(siteName + ": " + getDescription());
    }
    
    /**
     * Impplement this method in importer exporter to do the
     * appropriate clean up.
     */
    public abstract void cleanup();

    
    /**
     * @param context specifies ApplicationContext to set
     * @throws BeansException if unsuccessful
     */
    public void setApplicationContext(ApplicationContext context)
            throws BeansException {
        ctx = context;
    }
    
    /**
     * Get the Sprning application context.
     * @return the single application context for all instances of DataTransferBase
     */
    public ApplicationContext getApplicationContext() {
        return ctx;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
        throws BeansException {
        // Just implementing this causes the static ctx variable to be set, 
        // because an instance is created by Spring.
    }

    /**
     * Sets the value of the unmar.
     * @param unmar the unmar to set
     */
    protected void setUnmar(Unmarshaller unmar) {
        this.unmar = unmar;
    }

    /**
     * Gets the value of unmar.
     * @return the unmar
     */
    protected Unmarshaller getUnmar() {
        return unmar;
    }

    /**
     * Sets the value of the stats.
     * @param stats the stats to set
     */
    protected void setStats(DataTransferStat stats) {
        this.stats = stats;
    }

    /**
     * Gets the value of stats.
     * @return the stats
     */
    protected DataTransferStat getStats() {
        return stats;
    }

    /**
     * Sets the value of the stateChanges.
     * @param stateChanges the stateChanges to set
     */
    protected void setStateChanges(StateChanges stateChanges) {
        this.stateChanges = stateChanges;
    }

    /**
     * Gets the value of stateChanges.
     * @return the stateChanges
     */
    protected StateChanges getStateChanges() {
        return stateChanges;
    }
    
    /**
     * Getter for validationContextString.
     * @return validationContextString
     */
    public String getValidationContextString() {
        return this.validationContextString;
    }

    /**
     * Setter for validationContextString.
     * @param vContextString the new Validation Context String to set.
     */
    public void setValidationContextString(String vContextString) {
        this.validationContextString = vContextString;
    }

    /**
     * Sets the value of the notificationMap.
     * @param notificationMap the notificationMap to set
     */
    void setNotificationMap(HashMap<InternalState, NotificationProperties> notificationMap) {
        this.notificationMap = notificationMap;
    }

    /**
     * Gets the value of notificationMap.
     * @return the notificationMap
     */
    protected HashMap<InternalState, NotificationProperties> getNotificationMap() {
        return notificationMap;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 