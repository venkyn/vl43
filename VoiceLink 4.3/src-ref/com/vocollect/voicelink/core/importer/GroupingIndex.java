/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Vector;


/**
 * A grouping index contains one or more fields that are strung together to 
 * create a key.  These are used to generate the index (UpdateIndex).  Once
 * generated, the grouping index can be used when reading a file with random
 * access, to provide the records in a grouped order based on the key similar
 * to a "Group By" clause in SQL. 
 *   
 * All records in the file that share the key will be grouped
 * together in the index.  The index maintains natural order where possible.
 * For example if a file contains all of a certain key first, and then all of
 * a second key second, these will be preserved in order in the index.
 * In the case where the file contains a third key separated by subsequent keys,
 * and then another record of the third key, this latter third key record will
 * be re-ordered in the index to come after the first item of the third key but
 * ahead of items of the fourth key, thus grouping like keys together in the
 * index.
 * 
 * @author jtauberg
 *
 */
public class GroupingIndex extends BaseIndex {

    private static final int DEFAULT_GROWTH_INCREMENT = 5;

    //This structure stores the index key, and an array containing the
    //list of charCounts where record(s) of this key exists in the file.
    private LinkedHashMap <String, Vector <Record>> indexDataPoints = null;
    
    //This stores by how many elements the vector grows when capacity exceeded.
    // (Vector capacityIncrement)  0 = double in size each growth.
    private Integer growBy = DEFAULT_GROWTH_INCREMENT;
    
    //This stores the initial size of Vector to create (Vector capacity)
    private Integer initialSize = DEFAULT_GROWTH_INCREMENT;
    
    //This stores the Character count from the beginning of the file to the
    // data to operate on.
    private Long charCount = 0L;

    //This stores the Record lenght in characters of the record being read.
    private Long recordLength = 0L;
    
    
    //This counts the number of times the index was updated (should be record count)
    private Integer updateCount = 0;

    /**
     * @author jtauberg
     * Each record has a start and a length.
     */
    class Record {
        //Stores the start value of the record.
        private Long start;

        //Stores the length of the record
        private Long length;

        /**
         * @return the length
         */
        public Long getLength() {
            return length;
        }

        /**
         * @param length the length to set
         */
        public void setLength(Long length) {
            this.length = length;
        }

        /**
         * @return the start
         */
        public Long getStart() {
            return start;
        }

        /**
         * @param start the start to set
         */
        public void setStart(Long start) {
            this.start = start;
        }
    }

    
    
    
    
    /**
     * This is the default constructor.
     */
    public GroupingIndex() {
        super();
        indexDataPoints = new LinkedHashMap <String, Vector <Record>>();
        updateCount = 0;
        recordLength = 0L;
        charCount = 0L;
    }

    
    /**
     * Constructor takes the list of fields used to create the grouping index.
     * @param fields - Collection of field objects that make up the index key.
     */
    public GroupingIndex(Collection<Field> fields) {
        super(fields);
        indexDataPoints = new LinkedHashMap <String, Vector <Record> >();
        charCount = 0L;
        recordLength = 0L;
        updateCount = 0;
    }

    
    /**
     * {@inheritDoc}
     * 
     * This will add the currently set GroupingIndex.charCount and
     * GroupingIndex.recordLength into a new Record and add this to the
     * current key's Vector of the indexDataPoints.
     * 
     * @see com.vocollect.voicelink.core.importer.BaseIndex#updateIndex()
     **/
    @Override
    public void updateIndex() {
        Vector <Record> v = null;
        String tempString = this.currentFieldsIndexValue();
        //Create a Record with this charCount and recordLength
        Record rec = new Record();
        rec.setLength(this.recordLength);
        rec.setStart(this.charCount);
        
        //if this tempstring is already there..
        if (this.indexDataPoints.containsKey(tempString)) {
            //get current Vector and add to it (existing vector)...
            v = this.indexDataPoints.get(tempString);
            //add this record to the current Vector
            v.addElement(rec);
        } else {
            //No vector yet, create a new vector
            v = new Vector <Record>(this.initialSize, this.growBy);
            //add this record to the new Vector
            v.addElement(rec);
        }
        //add vector and key to index and increment the count.
        this.indexDataPoints.put(tempString, v);
        updateCount = updateCount + 1;
    }


    
    
    /**
     * Gets charCount from the beginning of the file to the
     *  data to operate on.
     * 
     * @return Long charCount from the beginning of the file to the
     *  data to operate on.
     */
    public Long getCharCount() {
        return this.charCount;
    }


    
    /**
     * Sets charCount from the beginning of the file to the
     *  data to operate on..
     * 
     * @param charCount the Long charCount from the beginning of the file to the
     *  data to operate on.
     */
    public void setCharCount(Long charCount) {
        this.charCount = charCount;
    }

    
    /**
     * @return the Long recordLength in characters of the record being operated on.
     */
    public Long getRecordLength() {
        return recordLength;
    }

    
    /**
     * @param recordLength Long recordLength in chars of the record being
     *         operated on.
     */
    public void setRecordLength(Long recordLength) {
        this.recordLength = recordLength;
    }


    /**
     * This gets the growBy.
     * initialSize refers to the initial number of times a record's key is
     * expected to be duplicated within the file.
     * 
     * If the initialSize is exceeded, the store for this information will grow
     * by the amount specified by growBy.
     * 
     * If speed is of main concern, and the file is of smaller size (extra memory
     *  avail.), this number may be increased.
     * If memory is of main concern, and the file is of larger size, this number
     * may be decreased at the cost of some speed.
     * (See also: java Vector capacityIncrement)
     * 
     * @return the growBy
     */
    public Integer getGrowBy() {
        return growBy;
    }


    
    /**
     * This sets the growBy. (Default is 5)
     * initialSize refers to the initial number of times a record's key is
     * expected to be duplicated within the file.
     * 
     * If the initialSize is exceeded, the store for this information will grow
     * by the amount specified by growBy.
     * 
     * If speed is of main concern, and the file is of smaller size (extra memory
     *  avail.), this number may be increased.
     * If memory is of main concern, and the file is of larger size, this number
     * may be decreased at the cost of some speed.
     * (See also: java Vector capacityIncrement)
     * 
     * @param growBy the growBy to set
     */
    public void setGrowBy(Integer growBy) {
        this.growBy = growBy;
    }


    
    /**
     * This gets the initialSize.
     * initialSize refers to the initial number of times a record's key is
     * expected to be duplicated within the file.
     * If speed is of main concern, and the file is of smaller size, this number
     * may be increased.
     * If memory is of main concern, and the file is of larger size, this number
     * may be decreased.
     * (See also: java Vector capacity)
     * 
     * @return the Integer initialSize
     */
    public Integer getInitialSize() {
        return initialSize;
    }

    
    /**
     * This counts how many times update was called (Record Count).
     * @return the updateCount
     */
    public Integer getUpdateCount() {
        return updateCount;
    }

    /**
     * This resets the data and counts on this configured index.
     * Should be called before reusing Index.
     */
    @Override
    public void resetIndex() {
        indexDataPoints = new LinkedHashMap <String, Vector <Record>>();
        updateCount = 0;
        recordLength = 0L;
        charCount = 0L;
    }

    /**
     * This sets the initialSize. (Default is 5)
     * initialSize refers to the initial number of times a record's key is
     * expected to be duplicated within the file.
     * If speed is of main concern, and the file is of smaller size, this number
     * may be increased.
     * If memory is of main concern, and the file is of larger size, this number
     * may be decreased.
     * (See also: java Vector capacity)
     * 
     * @param initialSize the Integer initialSize to set
     */
    public void setInitialSize(Integer initialSize) {
        this.initialSize = initialSize;
    }

    
    /**
     *  This returns an array of charCounts in order of the defined index.
     *  It can be be passed to an Adapter to provide records in this order.

     * @return Record properOrder[2][count] - 
     *          properOrder[0] - contains indexed charCounts (start) in properly
     *          indexed order.
     *          properOrder[1] - contains indexed recordLengths (length) in 
     *          properly indexed order.
     */
    public Long[][] getProperOrder() {
      Long[][] properOrder = new Long [2][this.updateCount];
      int aIndex = 0;
      Record rec = null;

      //loop through the values in indexDataPoints and get each vector in order.
      for (Vector <Record> v : this.indexDataPoints.values()) {
          //loop through the vector containing the indexed data records and
          // add them to the properOrder array sequentially.
          for (int vIndex = 0; vIndex < v.size(); vIndex++) {
              //Get the next record in the Vector
              rec = v.get(vIndex);
              //Store the charCount for this record and add it to properOrder.
              properOrder[0][aIndex] = rec.getStart();
              //Store the recordLength for this record and add it to properOrder.
              properOrder[1][aIndex] = rec.getLength();
              //Increment the index to the properOrder array.
              aIndex++;
              // check if index will go out of bounds... this shouldn't happen
              if (aIndex > this.updateCount) {
                  //TODO:  throw error here.  index is corrupt.
              }
          } //end vector loop
      } // end indexDataPoints loop

      //Check to see that we put one value in properOrder array for each record.
      if (aIndex != this.updateCount) {
          //TODO:  throw error here.  index is corrupt.
      }
      return properOrder;
    }
 
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 