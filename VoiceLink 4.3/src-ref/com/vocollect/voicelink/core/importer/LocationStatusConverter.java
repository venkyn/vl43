/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.model.LocationStatus;

import java.util.Map;

import ognl.DefaultTypeConverter;


/**
 * This converts String to LocationStatus Object and
 * LocationStatus object to string.
 * 
 * @author Kalpna
 *
 */  

public class LocationStatusConverter extends DefaultTypeConverter {

    /**
     * Converts the given object to a given type. How this is to be done is
     * implemented in toClass.
     * @param context - OGNL context under which the conversion is being done
     * @param o - the object to be converted
     * @param toClass - the class that contains the code to convert to enumeration
     * @return Converted value of type declared in toClass 
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object convertValue(Map context, Object o, Class toClass) {

        if (toClass == LocationStatus.class) {
            if (o instanceof String[]) {
                return LocationStatus.toEnum(Integer
                    .parseInt(((String[]) o)[0]));
            } else if (o instanceof String) {
                return LocationStatus.toEnum(Integer.parseInt(o.toString()));
            }
        } else if (toClass == String.class) {
            LocationStatus ls = (LocationStatus) o;
            return ls.toString();
        }
        return null;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 