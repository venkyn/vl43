/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

/**
 * @author dgold
 *
 */
class StateChange implements Comparable<StateChange> {

    private InternalState from;

    private InternalState to;

    /**
     * 
     * @param from specifies InternalState object
     * @param to specifies InternalState object
     */
    public StateChange(InternalState from, InternalState to) {
        super();
        this.from = from;
        this.to = to;
    }

    /**
     * @param o StateChange object to compare this InternalState instance to
     * @return the integer difference between the objects
     */
    public int compareTo(StateChange o) {
        return this.hashCode() - o.hashCode();
    }

    /**
     * @param o Object to see if this InternalState instance is equal to
     * @return true if the items are equal
     */
    public boolean equals(Object o) {
        StateChange other = (StateChange) o;
        return ((this.from.getCode() == other.from.getCode()) && (this.to.getCode() == other.to.getCode()));
    }

    /**
     * @return the integer hashcode
     */
    public int hashCode() {
        return (this.from.getDescription() + this.to.getDescription()).hashCode();
    }

    /**
     * @return the from
     */
    public InternalState getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(InternalState from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public InternalState getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(InternalState to) {
        this.to = to;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 