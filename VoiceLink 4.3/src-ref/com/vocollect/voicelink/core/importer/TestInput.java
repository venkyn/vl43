/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

/**
 * @author dgold
 */
public class TestInput { // implements ValidationAware {
// List errors = new LinkedList();
// LinkedHashMap<String,String> fieldErrors = new LinkedHashMap<String,
// String>();
// List messages = new LinkedList();

    // private String father;
    private int     father;

    private String  lastName;

    private String  firstName;

    private String  month;

    private String  day;

    private String  year;

    private String  comment;

    private String  field_a;

    private String  field_b;

    private String  field_c;

    private String  field_d;

    private String  field_e;

    private String  field_f;

    private String  field_g;

    private Integer field_h;

    /**
     * 
     * @return Integer object
     */
    public Integer getField_h() {
        return field_h;
    }

    /**
     * 
     * @param field_h - Field H
     */
    public void setField_h(Integer field_h) {
        this.field_h = field_h;
    }

    /**
     * 
     * @return string comment
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * 
     * @param comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return day as a string
     */
    public String getDay() {
        return this.day;
    }

    /**
     * 
     * @param day to set
     */
    public void setDay(String day) {
        this.day = day;
    }

    /**
     * 
     * @return first name
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * 
     * @param firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * 
     * @return last name
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * 
     * @param lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * 
     * @return month as string
     */
    public String getMonth() {
        return this.month;
    }

    /**
     * 
     * @param mo to set
     */
    public void setMonth(String mo) {
        this.month = mo;
    }

    /**
     * 
     * @return int
     */
    public int getFather() {
        return this.father;
    }

    /**
     * 
     * @param father to set
     */
    public void setFather(int father) {
        this.father = father;
    }

    /**
     * 
     * @return year
     */
    public String getYear() {
        return this.year;
    }

    /**
     * 
     * @param year to set
     */
    public void setYear(String year) {
        this.year = year;
    }

    /**
     * 
     */
    public TestInput() {
        super();
    }

    /**
     * 
     * @return string
     */
    public String getField_a() {
        return this.field_a;
    }

    /**
     * 
     * @param field_a to set
     */
    public void setField_a(String field_a) {
        this.field_a = field_a;
    }

    /**
     * 
     * @return string
     */
    public String getField_b() {
        return this.field_b;
    }

    /**
     * 
     * @param field_b to set
     */
    public void setField_b(String field_b) {
        this.field_b = field_b;
    }

    /**
     * 
     * @return string
     */
    public String getField_c() {
        return this.field_c;
    }

    /**
     * 
     * @param field_c to set
     */
    public void setField_c(String field_c) {
        this.field_c = field_c;
    }

    /**
     * 
     * @return String
     */
    public String getField_d() {
        return this.field_d;
    }

    /**
     * 
     * @param field_d - Field D
     */
    public void setField_d(String field_d) {
        this.field_d = field_d;
    }

    /**
     * 
     * @return String
     */
    public String getField_e() {
        return this.field_e;
    }

    /**
     * 
     * @param field_e - Field E
     */
    public void setField_e(String field_e) {
        this.field_e = field_e;
    }

    /**
     * 
     * @return String
     */
    public String getField_f() {
        return this.field_f;
    }

    /**
     * 
     * @param field_f - Field F
     */
    public void setField_f(String field_f) {
        this.field_f = field_f;
    }

    /**
     * 
     * @return String
     */
    public String getField_g() {
        return this.field_g;
    }

    /**
     * 
     * @param field_g - Field G
     */
    public void setField_g(String field_g) {
        this.field_g = field_g;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 