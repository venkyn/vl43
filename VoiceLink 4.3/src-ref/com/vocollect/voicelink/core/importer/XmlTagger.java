/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

/**
 * @author dgold
 *
 */
public class XmlTagger {

    private static final Logger log = new Logger(XmlTagger.class);

    // create the logger object

    private String xmlPrologue = DEFAULT_XML_PROLOGUE;

    // XML header string. See default below (XML v 1.0)
    private static final String DEFAULT_XML_PROLOGUE = "<?xml version=\"1.0\" encoding='UTF-8'?>";

    // Place to put the closed XML context(s). This is what you get
    // when you ask for the XML representation.
    private String closedXmlContext = this.xmlPrologue;

    // Place to save the open XML context(s)
    private String openXmlContext = "";

    // The marked-up result

    private ArrayList<CreateObjectTrigger> newObjectTriggers = null;

    // List of open object contextx. These are created-objects that have not
    // been closed.
    private HashMap<String, String> openObjectContexts = new HashMap<String, String>();

    // Indicates when there is content in the closed XML context string
    private boolean contentReady = false;

    //Array List to store the fieldmap of open context
    private ArrayList<FieldMap> openDataFM = new ArrayList<FieldMap>();

    //Array List to store the fieldmap of closed context
    private ArrayList<FieldMap> closedDataFM = new ArrayList<FieldMap>();

    private int count = 0;

    private FieldMap fieldMap = null;

    /**
     * Default Constructor.
     */
    public XmlTagger() {
        super();
    }

    /**
     * reset the string holding the current XML representation of the data to
     * just the XML prologue.
     */
    public void clearXmlRepresentation() {
        
        if (0 != openXmlContext.compareTo(getXmlPrologue())) {
            this.openXmlContext = this.xmlPrologue + this.openXmlContext;
        }

        this.closedXmlContext = "";
        this.setContentReady(false);
        this.closedDataFM.clear();
    }

    /**
     * Set the tagging decorations for the fields.
     * 
     * @param fields
     *            list of fields to mark up
     */
    @SuppressWarnings("unchecked")
    public void markUpFields(FieldMap fields) {
        for (Field x : fields.values()) {
            x.setOpenTag(openXmlTag(x.getTagName()));
            x.setCloseTag(closeXmlTag(x.getTagName()));
        }

    }

    /**
     * Using the tagging information in the field objects, convert the record
     * into its tagged XML form. Append this to the closedXmlContext string.
     * This string will consist of at least the XML prologue, and may contain
     * other tagged data from previous calls to makeXmlStream
     * 
     * @param fields specifies FieldMap object
     * @param dataType specifies the datatype to utilize as a Stream
     * @return the tagged XML representation of the data appended to the current
     *         XML representation
     */
    @SuppressWarnings("unchecked")
    public String makeXmlStream(FieldMap fields, String dataType) {
        String xmlSoFar = "";
        // For every trigger
        for (CreateObjectTrigger x : this.newObjectTriggers) {
            // If the trigger fires
            if (x.detectTriggeringConditions(fields)) {
                
                // If the trigger left its context open
                if (openContextExists(x.getNewObjectName()) && !x.isSuppress()) {
                    // Close it

                    xmlSoFar = x.closeContext(xmlSoFar, fields);
                    // Get rid of the marker for this context

                    removeOpenContext(x.getNewObjectName());
                    // Now, the context is not open, and the local var has the
                    // current context, up to the end of the context that was just closed.
                    if (!this.openContextExists()) {
                        this.closedXmlContext += this.openXmlContext + xmlSoFar;
                        // move open fields to the closed fields list
                        this.closeObjectSource();
                        // clear open field list
                        this.openDataFM.clear();
                        xmlSoFar = "";
                        this.openXmlContext = "";
                        this.setContentReady(true);
                        count = 0;
                    }
                }
                if (!x.isSuppress()) {
                    xmlSoFar = x.openContext(xmlSoFar, fields);
//                  this.openObjectSource(fields);
                    if (x.isOpen()) {
                        this.addOpenObjectContext(x.getNewObjectName());
                    }
                }
            }


        }
        
      
        this.openObjectSource(fields);
        // If there are any open contexts


        
        if (this.openContextExists()) {

            this.openXmlContext += xmlSoFar;
            // move fields to the open field list
        } else {

            this.closedXmlContext += this.openXmlContext + xmlSoFar;
            this.openXmlContext = "";
            
            this.setContentReady(true);
            // move open fields to the closed field list

            this.closeObjectSource();

            this.openDataFM.clear();
            count = 0;
        }

        return this.closedXmlContext;
    }

    /**
     * Create the last record from the still-open context.
     * @param fields Field map containing the last dataset of daqta
     * @return the string representing the closed XML context.
     */
    @SuppressWarnings("unchecked")
    public String makeFinalRecord(FieldMap fields) {

        String xmlSoFar = "";
        for (CreateObjectTrigger x : this.newObjectTriggers) {
            if (openContextExists(x.getNewObjectName())) {
                // Close it
                xmlSoFar = x.closeContext(xmlSoFar, fields);
                // Get rid of the marker for this context
                removeOpenContext(x.getNewObjectName());
                // Now, the context is not open, and the local var has the
                // current context, up to the end of the context that was just closed.
                if (!this.openContextExists()) {
                    this.closedXmlContext += this.openXmlContext + xmlSoFar;
                    xmlSoFar = "";
                    this.openXmlContext = "";
                    this.setContentReady(true);
                    this.closeObjectSource();
                    this.openDataFM.clear();
                    count = 0;
                }
            }
        }
        this.setContentReady(true);
        return closedXmlContext;
    }

    /**
     * Check to see if there are any open contexts. Used by Importer to see if it has a complete
     * object ion the XML stream.
     * @return true if there exists any open context.
     */
    public boolean openContextExists() {
        return !getOpenObjectContexts().isEmpty();
    }

    /**
     * 
     * @param newObjectTag String to set
     */
    private void removeOpenContext(String newObjectTag) {
        this.getOpenObjectContexts().remove(newObjectTag);

    }

    /**
     * 
     * @param newObjectTag String to set
     */
    private void addOpenObjectContext(String newObjectTag) {
        this.getOpenObjectContexts().put(newObjectTag, newObjectTag);

    }

    /**
     * 
     * @param contextName String to set
     * @return true if context exists
     */
    private boolean openContextExists(String contextName) {
        return this.getOpenObjectContexts().containsKey(contextName);
    }

    /**
     * @return Returns the xmlPrologue.
     */
    public String getXmlPrologue() {
        return this.xmlPrologue;
    }

    /**
     * @param xmlPrologue
     *            The xmlPrologue to set.
     */
    public void setXmlPrologue(String xmlPrologue) {
        this.xmlPrologue = xmlPrologue;
    }

    /**
     * Open and close tagging. For efficiency, we might want to move these to
     * the Field.
     * 
     * @param tagName String of data within open tag
     * @return the String result
     */
    private String openXmlTag(String tagName) {
        return "<" + tagName + ">";
    }

    /**
     * Open and close tagging. For efficiency, we might want to move these to
     * the Field.
     * 
     * @param tagName String of data within close tag
     * @return the String result
     */
    private String closeXmlTag(String tagName) {
        return "</" + tagName + ">";
    }

    /**
     * @return the String representation of XML
     */
    public String getXmlRepresentation() {
        return closedXmlContext;
    }

    /**
     * @param xmlRepresentation String to set
     */
    public void setXmlRepresentation(String xmlRepresentation) {
        this.closedXmlContext = xmlRepresentation;
    }

    /**
     * @return an ArrayList of CreateObjectTriggers
     */
    public ArrayList<CreateObjectTrigger> getNewObjectTriggers() {
        if (null == newObjectTriggers) {
            newObjectTriggers = new ArrayList<CreateObjectTrigger>();
        }
        return newObjectTriggers;
    }

    /**
     * Add a single trigger to the list of triggers.
     * @param trigger trigger to add
     */
    public void addTrigger(CreateObjectTrigger trigger) {
        this.getNewObjectTriggers().add(trigger);
        tagTrigger(trigger);
    }

    /**
     * Tag up the trigger appropriately. Tag it's list of triggers, too.
     * @param newRecordTrigger the trigger to tag
     */
    public void tagTrigger(CreateObjectTrigger newRecordTrigger) {
        newRecordTrigger.setOpenTag(this.openXmlTag(newRecordTrigger
                .getNewObjectName()));
        newRecordTrigger.setCloseTag(this.closeXmlTag(newRecordTrigger
                .getNewObjectName()));
        tagTriggerList(newRecordTrigger.getTriggers());
    }

    /**
     * Tag up a list of triggers.
     * @param objectTriggers list of triggers to tag
     */
    private void tagTriggerList(ArrayList<CreateObjectTrigger> objectTriggers) {
        for (CreateObjectTrigger newRecordTrigger : objectTriggers) {
            if (null == newRecordTrigger.getNewObjectName()) {
                log.fatal(
                    "The name of the new object to create nust be set in the XML definition of the trigger",
                    ImporterError.NO_NEW_OBJECT_NAME);
                throw new RuntimeException("ImporterError.NoNewObjectName ");
            }
            tagTrigger(newRecordTrigger);
        }
    }

    /**
     * Tag every trigger in the list, and make the list mine.
     * @param newObjectTriggers an ArrayList of CreateObjectTriggers
     */
    public void setNewObjectTriggers(
            ArrayList<CreateObjectTrigger> newObjectTriggers) {

        if (null == newObjectTriggers) {
            return; // No action, expected
        }

        this.newObjectTriggers = newObjectTriggers;
        tagTriggerList(newObjectTriggers);
    }

    /**
     * 
     * @return a HashMap of String, String objects
     */
    private HashMap<String, String> getOpenObjectContexts() {
        return openObjectContexts;
    }

    /**
     * Gets the value of contentReady.
     * @return the contentReady
     */
    public boolean isContentReady() {
        return contentReady;
    }

    /**
     * Sets the value of the contentReady.
     * @param contentReady the contentReady to set
     */
    public void setContentReady(boolean contentReady) {
        this.contentReady = contentReady;
    }

    /**
     * This method takes a field map as parameter and copies the
     * contents of the fieldmap to a new fieldmap and adds the
     * newly created fieldmap to the openDataFM list.
     * @param fields the field map to copy
     */
    public void openObjectSource(FieldMap fields) {

        fieldMap = new FixedLengthFieldMap();
        fieldMap.setBadData(fields.isBadData());
        Iterator<Entry<String, Field>> it = fields.entrySet().iterator();
        try {
            while (it.hasNext()) {

                Entry<String, Field> pairs = it.next();
                FixedLengthField fieldValue = new FixedLengthField();
                fieldValue = copy((FixedLengthField) pairs.getValue());
                fieldMap.addField(fieldValue);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (openDataFM.isEmpty()) {
            openDataFM.add(fieldMap);
            count++;
        } else {
            openDataFM.add(count, fieldMap);
            count++;
        }
    }

    /**
     * This method transfers the contents of openDataFM list
     * to closedDataFM List. This method is called when the old
     * context is closed.
     *
     */
    public void closeObjectSource() {
        for (FieldMap fm : openDataFM) {
            closedDataFM.add(fm);
        }
    }

    /**
     * This method returns the closedDataFM list.
     * @return List
     */
    public List<FieldMap> closeObjSource() {
        if (closedDataFM.isEmpty()) {
            FieldMap f = new FieldMap();
            closedDataFM.add(f);
            return closedDataFM;
        }
        return closedDataFM;
    }

    /**
     * This method makes a copy of FixedLengthField.
     * @param oldValue FixedLengthField
     * @return copy of passed param FixedLengthField
     */
    public FixedLengthField copy(FixedLengthField oldValue) {
        FixedLengthField newValue = new FixedLengthField();
        try {
            newValue.setCloseTag(oldValue.getCloseTag());
            newValue.setDataType(oldValue.getDataType());
            newValue.setEnd(oldValue.getEnd());
            newValue.setFieldName(oldValue.getFieldName());
            newValue.setFieldData(oldValue.getFieldData());
            newValue.setGetterName(oldValue.getGetterName());
            newValue.setIndexable(oldValue.isIndexable());
            newValue.setKeyField(oldValue.isKeyField());
            newValue.setLength(oldValue.getLength());
            newValue.setOpenTag(oldValue.getOpenTag());
            newValue.setRemapped(oldValue.isRemapped());
            newValue.setSetterName(oldValue.getSetterName());
            newValue.setStart(oldValue.getStart());
            newValue.setSuppress(oldValue.isSuppress());
            newValue.setTagName(oldValue.getTagName());
            newValue.setTreatBlankAsNull(oldValue.isTreatBlankAsNull());
            newValue.setValidationList(oldValue.getValidationList());
            newValue.setWriteOnOpenContext(oldValue.isWriteOnOpenContext());
            // These need to be down here, because setting the data to "" clears the bad data flag...
            newValue.setBadData(oldValue.isBadData());
            newValue.setBadFieldData(oldValue.getBadFieldData());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newValue;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 