/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * This will allow us to populate parts of objects based on the list of triggers. This class is used exclusively by the
 * triggers, at present.
 * 
 * 
 * @author dgold
 * 
 */
public class DBLookup {

    private static final Logger log = new Logger(DBLookup.class);

    // This is the access point for the DAO layer.
    private PersistenceManager  pm  = new PersistenceManager();

    /**
     * Default constructor.
     * 
     * @param pm
     *            the persistence manager to use.
     */
    public DBLookup(PersistenceManager pm) {
        // nothing detectable right now.
        this.pm = pm;
    }

    /**
     * Make a new object, given the class name.
     * 
     * @param className
     *            String representing the class name for the object
     * @return the Object desired, or null on failure.
     */
    public static Object makeNewObject(String className) {
        Object object = null;
        Class<?> classDefinition = null;
        try {
            classDefinition = Class.forName(className);
            object = classDefinition.newInstance();
        } catch (InstantiationException e) {
            // Class or default constructor is not accessible
            log.fatal("Class or default constructor is not accessible for '"
                    + classDefinition.getCanonicalName() + "'",
                    DBLookupError.INACCESSIBLE_CLASS_OR_CONSTRUCTOR);
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            // if this Class represents an abstract class, an interface, an
            // array class, a primitive type, or void; or if the class has no
            // nullary constructor; or if the instantiation fails for some other
            // reason
            log.fatal("Cannot instantiate class '"
                    + classDefinition.getCanonicalName()
                    + "', class is not instantiatible",
                    DBLookupError.UNINSTANTIATABLE_CLASS);
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            // if the initialization provoked by this method fails.
            log.fatal("No such class for '" + className + "'",
                    DBLookupError.NO_SUCH_CLASS);
            throw new RuntimeException(e);
        }
        return object;
    }

    /**
     * Get the object we want to work with form the subject.
     * 
     * @param subject
     *            The owner of the object we want.
     * @param compositeName
     *            Name of the part we want
     * @return the object we are after
     * @throws VocollectException
     *             if the methiod get{compositeName} is not found
     */
    public static Object getCompositePart(Object subject, String compositeName)
            throws VocollectException {

        Method getterMethod = findGetter(subject, compositeName, null);

        if (null == getterMethod) {
            log.error("No public method by name '"
                    + subject.getClass().getCanonicalName() + ".get"
                    + uppercaseFirstLetter(compositeName) + "()'",
                    DBLookupError.NO_SUCH_GETTER_METHOD_NAME);
            throw new VocollectException();
        }
        try {
            return getterMethod.invoke(subject, (Object[]) null);
        } catch (IllegalArgumentException e) {
            log.error("Passed null for parameter list for getter "
                    + getterMethod.getName() + ". "
                    + "Requires parameter list: "
                    + getterMethod.toGenericString(),
                    DBLookupError.BAD_GETTER_PARAMETERS);
            throw new VocollectException();
        } catch (IllegalAccessException e) {
            log.error(
                    "Getter method is not accessible. Description of getter: "
                            + getterMethod.toGenericString(),
                    DBLookupError.GETTER_ACCESS_DENIED);
            throw new VocollectException();
        } catch (InvocationTargetException e) {
            log.error("Getter threw an exception: " + e.getCause(),
                    DBLookupError.GETTER_EXCEPTION_THROWN);
            throw new VocollectException();
        }
    }

    /**
     * Set the composite part of the subject.
     * 
     * @param subject
     *            Object we're inporting
     * @param missingLink
     *            the object that is part of the subject
     * @param compositePartName
     *            name of the missing link
     * @throws VocollectException
     *             if the method does not exist
     */
    public static void setCompositePart(Object subject, Object missingLink,
            String compositePartName) throws VocollectException {

        Method getterMethod = findGetter(subject, compositePartName, null);
        if (null == getterMethod) {
            String missingLinkName = "null";
            if (null != missingLink) {
                missingLinkName = missingLink.getClass().getSimpleName();
            }
            log.error("No public method by name '"
                    + subject.getClass().getCanonicalName() + ".get"
                    + uppercaseFirstLetter(compositePartName) + "("
                    + missingLinkName + ")'",
                    DBLookupError.NO_SUCH_GETTER_METHOD_NAME);
            throw new VocollectException();
        }

        Method setterMethod = findSetter(subject, compositePartName,
            new Class[] {getterMethod.getReturnType()});

        if (null == setterMethod) {
            String missingLinkName = "null";
            if (null != missingLink) {
                missingLinkName = missingLink.getClass().getSimpleName();
            }
            log.error("No public method by name '"
                    + subject.getClass().getCanonicalName() + ".set"
                    + uppercaseFirstLetter(compositePartName) + "("
                    + missingLinkName + ")'",
                    DBLookupError.NO_SUCH_SETTER_METHOD_NAME);
            throw new VocollectException();
        }
        try {
            setterMethod.invoke(subject, missingLink);
        } catch (IllegalArgumentException e) {
            log.error("Passed null for parameter list for setter "
                    + setterMethod.getName() + ". "
                    + "Requires parameter list: "
                    + setterMethod.toGenericString(),
                    DBLookupError.BAD_SETTER_PARAMETERS);
            throw new VocollectException();
        } catch (IllegalAccessException e) {
            log.error(
                    "Getter method is not accessible. Description of setter: "
                            + setterMethod.toGenericString(),
                    DBLookupError.SETTER_ACCESS_DENIED);
            throw new VocollectException();
        } catch (InvocationTargetException e) {
            log.error("Setter threw an exception: " + e.getCause(),
                    DBLookupError.SETTER_EXCEPTION_THROWN);
            throw new VocollectException();
        }

    }

    /**
     * Look up the example object in the database. Mine the given object for pertinent data, as defined by the params.
     * Ask the PersistenceManager for the data. Return an empty list - which preserves order - if no data found.
     * 
     * Originally, this was goping to be real simple, but Hibernate QBE is broken by our model objects. Using the given
     * object as query-by-example, this becomes: // try { // return pm.queryByExample(exampleInstance); // } catch
     * (DataAccessException e) { // log.error("Bad database access for query-by-example",
     * TriggerErrors.BAD_DB_ACCESS_FOR_QUERY_BY_EXAMPLE, e); // }
     */
    /**
     * @param exampleInstance
     *            the instance to use as an example.
     * @param finderName
     *            name of the finder method in the DAO
     * @param params
     *            List of fields, in the correct order, that will be used to populate the query
     * @return a list containing the elements from the DB that matched the example. Empty list if no data found.
     */
    @SuppressWarnings("unchecked")
    public List<Object> lookup(Object exampleInstance,
            String finderName, FieldMap params) {
        // Get the list of parameters for the finder.
        String[] paramList = new String[params.size()];
        int index = 0;
        for (com.vocollect.voicelink.core.importer.Field field 
                : params.values()) {
            paramList[index] = field.getGetterName();
            index++;
        }
        // Look up an element, which might be a list or an element
        Object primaryResult = null;
        try {
            primaryResult = pm.find(exampleInstance, finderName, paramList);
        } catch (BadSiteException e) {
            throw e;
        } catch (Throwable e) {
            String temp = "";
            String separator = "";
            for (String fieldName : params.keySet()) {
                temp += separator + fieldName;
                separator = ", ";
                index++;
            }
            log.error(
                    "Encountered exception during find operation for finder '"
                            + finderName + "(" + temp + "). "
                            + (e.getCause() == null ? "" : e.getCause()),
                    TriggerErrors.BAD_FIND, e);
            // This is a configuration error
            throw new RuntimeException(e.getLocalizedMessage());
        }

        List<Object> resultSet = null;

        // We are returning a list, always, so we do some converting

        // If we got back a null, return the empty set.
        if (null == primaryResult) {
            resultSet = new LinkedList<Object>();
            return resultSet;
        }

        // If the result is a list, return it - otherwise, make a list, add the
        // element, and return the new list
        try {
            resultSet = (List<Object>) primaryResult;
        } catch (ClassCastException c) {
            // Not a list. Add object to the list, and return that.
            resultSet = new LinkedList<Object>();
            resultSet.add(primaryResult);
        }
        return resultSet;
    }

    /**
     * Look up the object in the DB and, if missing, insert it.
     * 
     * @param exampleInstance
     *            instance of the target object.
     * @param finderName
     *            Name of the finder method on the DAO
     * @param params
     *            list of fields, which we use to populate the query
     * @return the object to use, or null on failure.
     * @throws VocollectException
     *             on database error, or if the result set > 1
     */
    public DataObject conditionalInsert(DataObject exampleInstance,
            String finderName, FieldMap params) throws VocollectException {
        // Use the PM to 'find' the right query and populate it with data from
        // the object.
        List<Object> resultSet = lookup(exampleInstance, finderName, params);
        // Use the result as the data for the object (replace the object with
        // the new one).
        // There had better be only one of these objects
        switch (resultSet.size()) {
        case 0: {
            if (pm.save(exampleInstance)) {
                return exampleInstance;
            }

            return exampleInstance;
        }
        case 1: {
            // Found the object - return the real object
            return (DataObject) resultSet.get(0);
        }
        default: {
            log.error(
                    "Result set contained more than one element for conditional insert. "
                            + "Query name: " + finderName + "Lookup data: "
                            + exampleInstance.toString() + " using fields: "
                            + params.toString(),
                    DBLookupError.INDETERMINATE_CONDITIONAL_INSERT_RESULT_SET);
            // Since this is likely a programming error, throw an exception
            throw new VocollectException(
                    DBLookupError.INDETERMINATE_CONDITIONAL_INSERT_RESULT_SET,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(DBLookupError.INDETERMINATE_CONDITIONAL_INSERT_RESULT_SET),
                            "Result set contained more than one element for conditional insert"));
        }

        }
    }

    /**
     * Look up the object in the DB and, if present, update it, else insert it.
     * 
     * @param exampleInstance
     *            instance of the type containing the lookup and replacement data
     * @param finderName
     *            Name of the finder method on the DAO
     * @param triggerParams
     *            list of fields, which we use to populate the query
     * @param inputFields
     *            fields to use as source of names of the properties
     *            @param needsValidation if tryue, don't save the object, we'll do that after we do validation
     * @return the object to use, or null on failure.
     * @throws VocollectException
     *             on database error, or result set > 1
     */
    public DataObject conditionalUpdate(DataObject exampleInstance,
            String finderName, FieldMap triggerParams, FieldMap inputFields, boolean needsValidation)
            throws VocollectException {
        // Use the PM to 'find' the right query and populate it with data from
        // the object.
        // Replace the found element's data with data from the new one.
        // Insert if not found
        List<Object> resultSet = lookup(exampleInstance, finderName, triggerParams);
        // Use the result as the data for the object (replace the object with
        // the new one).
        // There had better be only one of these objects
        switch (resultSet.size()) {
        case 0: {
            if (!needsValidation) {
                if (pm.save(exampleInstance)) {
                    return exampleInstance;
                }
            }

            return exampleInstance;
        }
        case 1: {
            DataObject foundInstance = (DataObject) resultSet.get(0);
            // copy the non-null fields from the example to the found instance
            copyNonNullFields(exampleInstance, foundInstance, inputFields);
            // Save the updated found object
            if (!needsValidation) {
                pm.save(foundInstance);
            }
            return foundInstance;
        }
        default: {
            log.error(
                    "Result set contained more than one element for conditional update"
                            + "Query name: " + finderName + "Lookup data: "
                            + exampleInstance.toString() + " using fields: "
                            + triggerParams.toString(),
                    DBLookupError.INDETERMINATE_CONDITIONAL_UPDATE_RESULT_SET);
            // Since this is likely a programming error, throw an exception
            throw new VocollectException(
                    DBLookupError.INDETERMINATE_CONDITIONAL_UPDATE_RESULT_SET,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(DBLookupError.INDETERMINATE_CONDITIONAL_UPDATE_RESULT_SET),
                            "Result set contained more than one element for conditional update"));
        }

        }
    }

    /**
     * Copy the data from the fields listed the params from exampleInstance to foundInstance.
     * 
     * @param exampleInstance
     *            the input instance, containing new values & lookup values
     * @param foundInstance
     *            The instance found matching the criteria in the example
     * @param params
     *            the list of fields we want to transfer.
     * @throws VocollectException
     *             if unable to find or use the getter or setter
     */
    @SuppressWarnings("unchecked")
    public static void copyNonNullFields(Object exampleInstance,
            Object foundInstance, FieldMap params) throws VocollectException {
        Method getterMethod = null;
        Method setterMethod = null;

        for (com.vocollect.voicelink.core.importer.Field field : params.values()) {
            // for each field in the map...
            
            // get the getter for the field
            getterMethod = findGetter(exampleInstance, field.getGetterName(), null);
            if (null == getterMethod) {
                log.error("Unable to locate method to get '" + field.getFieldName()
                        + "' by name : " + "get"
                        + uppercaseFirstLetter(field.getGetterName()),
                        DBLookupError.NO_SUCH_GETTER_METHOD_NAME_2);
                throw new VocollectException();
            }

            // get the setter for the field
            setterMethod = findSetter(exampleInstance, field.getSetterName(),
                new Class[] {getterMethod.getReturnType()});
            if (null == setterMethod) {
                log.error("Unable to locate method to set '" + field.getFieldName()
                        + "' by name : " + "set"
                        + uppercaseFirstLetter(field.getSetterName()),
                        DBLookupError.NO_SUCH_SETTER_METHOD_NAME_2);
                throw new VocollectException();
            }

            // invoke: equivalently found.set{objName}(example.get{objName}())
            // ex: foundOperator.setPassword(exampleOperator.getPassword());
            try {
                setterMethod.invoke(foundInstance, getterMethod.invoke(
                        exampleInstance, (Object[]) null));
            } catch (IllegalArgumentException e) {
                log.error(
                        "Setter does not accept the output of Getter by the same name. "
                                + "Getter: " + getterMethod.toGenericString()
                                + " Setter: " + setterMethod.toGenericString(),
                        DBLookupError.BAD_GETTER_SETTER_MATCH);
                throw new VocollectException();
            } catch (IllegalAccessException e) {
                log.error("Getter or Setter method is not accessible. "
                        + "Description of getter: "
                        + getterMethod.toGenericString()
                        + "Description of setter: "
                        + setterMethod.toGenericString(),
                        DBLookupError.GETTER_SETTER_ACCESS_DENIED);
                throw new VocollectException();
            } catch (InvocationTargetException e) {
                log.error("Getter or setter threw an exception: "
                        + e.getCause(),
                        DBLookupError.GETTER_SETTER_EXCEPTION_THROWN);
                throw new VocollectException();
            }
        }
        return;
    }
    
    /**
     * Copy all fields form source object to the dest object.
     * @param source Source of the data 
     * @param dest Destination for the data
     * @throws VocollectException if we cannot find appropriate methods
     */
    public static void transferAllFields(Object source, Object dest) throws VocollectException {
        java.lang.reflect.Field[] fields = source.getClass().getFields();
        String fieldname = "";
        for (Field f : fields) {
            fieldname = f.getName();
            Method getter = findGetter(source, fieldname, null);
            Method setter = findSetter(dest, fieldname,
                new Class[] {getter.getReturnType()});
            try {
                setter.invoke(dest, getter.invoke(
                        source, (Object[]) null));
            } catch (IllegalArgumentException e) {
                log.error(
                        "Setter does not accept the output of Getter by the same name. "
                                + "Getter: " + getter.toGenericString()
                                + " Setter: " + setter.toGenericString(),
                        DBLookupError.BAD_GETTER_SETTER_MATCH);
                throw new VocollectException();
            } catch (IllegalAccessException e) {
                log.error("Getter or Setter method is not accessible. "
                        + "Description of getter: "
                        + getter.toGenericString()
                        + "Description of setter: "
                        + setter.toGenericString(),
                        DBLookupError.GETTER_SETTER_ACCESS_DENIED);
                throw new VocollectException();
            } catch (InvocationTargetException e) {
                log.error("Getter or setter threw an exception: "
                        + e.getCause(),
                        DBLookupError.GETTER_SETTER_EXCEPTION_THROWN);
                throw new VocollectException();
            }

        }
        
    }

    /**
     * Get the getter for the field by looking up the method by name
     * 'get[fieldName]'. The [fieldName] will have the first letter capitalized
     * by this routine.
     * 
     * @param exampleInstance
     *            the object to investigate for the getter
     * @param fieldName
     *            name of the field we're interested in
     * @param arguments
     *              arguments that our getter takes... usually null for a getter.
     * @return the method we're after.
     */
    public static Method findGetter(Object exampleInstance, String fieldName,
                                    Class<?>[] arguments) {
        String getterName = "get" + uppercaseFirstLetter(fieldName);
        return findMethodByName(exampleInstance, getterName, arguments);
    }

    /**
     * Get the setter for the field by looking up the method by name
     * 'set[fieldName]'. The [fieldName] will have the first letter capitalized
     * by this routine.
     * 
     * @param exampleInstance
     *            the object to investigate for the setter
     * @param fieldName
     *            name of the field we're interested in
     * @param arguments
     *              arguments that our setter takes... usually
     *              new Class[] {getterMethod.getReturnType()}
     * @return the method we're after.
     */
    public static Method findSetter(Object exampleInstance, String fieldName, Class<?>[] arguments) {
        String setterName = "set" + uppercaseFirstLetter(fieldName);
        return findMethodByName(exampleInstance, setterName, arguments);
    }

    /**
     * Use the reflection API to get a method by name only. We are using this to
     * grab setters and getters.
     * 
     * @param exampleInstance
     *            Instance of the class we wish to investigate
     * @param methodName
     *            name of setter/getter we want
     * @param arguments
     *              arguments that our setter or getter takes.  For a getter this
     *              should be null, for a setter, it is usually
     *              new Class[] {getterMethod.getReturnType()}
     * @return the Method matching the name, or null if not found.
     */
    public static Method findMethodByName(Object exampleInstance,
            String methodName, Class<?>[] arguments) {
        Method[] methods = exampleInstance.getClass().getMethods();
        Method namedMethod = null;
        try {
            namedMethod = exampleInstance.getClass().getMethod(methodName, arguments);
            return namedMethod;
        } catch (SecurityException e) {
            // Expected
        } catch (NoSuchMethodException e) {
            // Expected
        }
        for (Method m : methods) {
            if (0 == methodName.compareTo(m.getName())) {
                // May want to keep a map of the finders around, rather than
                // this way. Or not.
                namedMethod = m;
                // Found what we're looking for, pop out.
                break;
            }
        }
        return namedMethod;
    }

    /**
     * Turn the first letter of the method name into Uppercase. Used when
     * prepending 'set' or 'get' to the method name.
     * 
     * @param name
     *            name of the method th be uppercased.
     * @return name of the method, first letter capitalized.
     */
    public static String uppercaseFirstLetter(final String name) {
        if (0 == name.length()) {
            return name;
        }
        char[] temp = name.toCharArray();
        temp[0] = Character.toUpperCase(temp[0]);
        return String.valueOf(temp);
    }
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 