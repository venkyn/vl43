/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;

/**
 * 
 * @author astein
 *
 */
public class FixedLengthField extends Field {
    
    private int length;

    private int start;

    private int end;

    /**
     * Constructor.
     */
    public FixedLengthField() {
        super();
        this.length = 0;
        this.start = 0;
        this.end = 0;
    }

    /**
     * Constructor with parameters.
     * 
     * @param start specifies integer to start FixedLengthField object
     * @param length specifies integer length
     * @throws VocollectException if unsuccessful 
     */
    public FixedLengthField(int start, int length) throws VocollectException {
        super();
        this.start = start;
        if (length < 0) {
            throw new VocollectException(DataSourceParserError.BAD_CONFIG,
                    new UserMessage(LocalizedMessage
                            .formatMessage(DataSourceParserError.BAD_CONFIG)));
        }
        this.length = length;
        this.end = start + length;
    }

    /**
     * Returns the number of the character at the end of the data within field.
     * @return the integer
     */
    public int getEnd() {
        return this.end;
    }

    /**
     * Sets the number of characters of the field.
     * 
     * @param end the integer to set
     */
    public void setEnd(int end) {
        this.end = end;
    }

    /**
     * @return the integer of the Length
     */
    public int getLength() {
        return this.length;
    }

    /**
     * @param length the integer to set
     */
    public void setLength(int length) {
        this.length = length;
        setEnd(length + getStart());
    }

    /**
     * @return the integer of the Start
     */
    public int getStart() {
        return this.start;
    }

    /**
     * 
     * @param start the integer to set
     */
    public void setStart(int start) {
        this.start = start;
        setEnd(getLength() + start);
    }

    /**
     * 
     * @return true if valid
     */
    public boolean valid() {
        return ((this.length >= 0) && (this.start <= this.end));
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 