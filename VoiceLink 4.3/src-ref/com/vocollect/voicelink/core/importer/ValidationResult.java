/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.LocaleProvider;
import com.opensymphony.xwork2.TextProvider;
import com.opensymphony.xwork2.TextProviderSupport;
import com.opensymphony.xwork2.ValidationAware;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ActionValidatorManagerFactory;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author dgold
 */
public class ValidationResult {

    private static final Logger    log                 = new Logger(
                                                               ValidationResult.class);

    // The log to use.

    private Object                 thingToValidate     = null;

    // The object to validate. This will typically be a model object

    private String                 objectImportContext = "";

    // Information that can be used to locate or identify the object in the
    // input source.
    // This will be output with the list of errors for the object being
    // validated.

    private ValidationAwareSupport resultHolder        = new ValidationAwareSupport();
    
    private String dataTransferContextString = "import";
    
    private boolean writeErrors        = true;
    
    //provide the locale information
    private static ImportExportLocaleProvider impExpLocaleProvider = new ImportExportLocaleProvider();

    //TextProvider
    private static final TextProviderSupport tps = 
        new TextProviderSupport(ResourceBundle.getBundle(
            "resources/ApplicationResources-VoiceLink-Import",
            impExpLocaleProvider.getLocale()), impExpLocaleProvider);


    /** 
     * This is the thing that is used by the DelegatingValidatorContext to keep
     * the result of the validation.
     */   
    public class VocollectValidatorContext extends DelegatingValidatorContext {

        private ObjectContext<? extends FieldMap, Object> fields = null;

        // Flag to indicate whether this validation run is before or after stuff is looked up in the DB.
        private boolean preLookup          = true;


        /**
         * DelegatingValidatorContext constructor.
         * Constructor.
         * @param arg0 class
         */
        public VocollectValidatorContext(Class<?> arg0) {
            super(arg0);

        }

        /**
         * DelegatingValidatorContext constructor.
         * Constructor.
         * @param arg0 object
         */
        public VocollectValidatorContext(Object arg0) {
            super(arg0);
        }

        /**
         * DelegatingValidatorContext constructor.
         * Constructor.
         * @param arg0 ValidationAware object
         * @param arg1 TextProvider
         * @param arg2 LocaleProvider
         */
        public VocollectValidatorContext(ValidationAware arg0, TextProvider arg1, LocaleProvider arg2) {
            super(arg0, arg1, arg2);
        }

        /**
         * Get the object context, which contains the fields from the original data.
         * @return the ObjectContext
         */
        public ObjectContext<? extends FieldMap, Object> getFields() {
            return fields;
        }

        /**
         * Set the ObjectContext for the object being validated.
         * @param fields the Object Context
         */
        public void setFields(ObjectContext<? extends FieldMap, Object> fields) {
            this.fields = fields;
        }
        
        /**
         * Gets the value of preLookup.
         * @return the preLookup
         */
        public boolean isPreLookup() {
            return this.preLookup;
        }

        /**
         * Sets the value of the preLookup.
         * @param preLookupParam the preLookup to set
         */
        public void setPreLookup(boolean preLookupParam) {
            this.preLookup = preLookupParam;
        }

        /**
         * @return the map of object.field - to - localName
         */
        public HashMap<String, HashMap<String, String>> getObjectFieldMapping() {
            return objectFieldMapping;
        }
        /**
         * Set the first char to a lowercase char.
         * @param name String to act on
         * @return the string with the fiorst letter lowercased.
         */
        private String othercaseFirstLetter(final String name) {
            if (0 == name.length()) {
                return name;
            }
            char[] temp = name.toCharArray();
            if (Character.isLowerCase(temp[0])) {
                temp[0] = Character.toUpperCase(temp[0]);
            } else {
                temp[0] = Character.toLowerCase(temp[0]);
            }
            return String.valueOf(temp);
        }

        /**
         * {@inheritDoc}
         * @param arg0
         * @param arg1
         */
        @Override
        public void addFieldError(String fieldName, String errorMessage) {
            if ((null != getFields()) 
                    && (null != ValidationResult.getObjectFieldMapping()) 
                    && (null != this.getFields().getModelObject())) {
                String parentObject = this.getFields().getModelObject().getClass().getSimpleName();
                String fullFieldName = parentObject + "." + fieldName;
                // Now, we want the object that directly owns the field...
                int lastObjectEnds = fullFieldName.lastIndexOf(".");
                int lastObjectBegins = fullFieldName.substring(0, lastObjectEnds).lastIndexOf(".");
                parentObject = fullFieldName.substring(lastObjectBegins + 1, lastObjectEnds);
                HashMap<String, String> objectAliases = ValidationResult.getObjectFieldMapping().get(parentObject);
                if (null == objectAliases) {
                    parentObject = othercaseFirstLetter(parentObject);
                    objectAliases = ValidationResult.getObjectFieldMapping().get(parentObject);
                } 
                if (null != objectAliases) {
                    String fieldAlias = objectAliases.get(fieldName);
                    if (null != fieldAlias) {
                        fieldName = fieldAlias;
                        errorMessage = "(" + fieldAlias + ") " + errorMessage;
                    } else {
                        fieldName = fullFieldName.substring(lastObjectEnds + 1);
                        fieldAlias = objectAliases.get(fieldName);
                        if (null != fieldAlias) {
                            fieldName = fieldAlias;
                            errorMessage = "(" + fieldAlias + ") " + errorMessage;
                        } 
                    }
                }
            }
            super.addFieldError(fieldName, errorMessage);
        }
    }   // End of inner class

    // This is the thing that is used to do the validation
    private VocollectValidatorContext       vContext            = new VocollectValidatorContext(
                                             resultHolder, tps, impExpLocaleProvider);



    // List of object.field - to - localName
    private static HashMap<String, HashMap<String, String>> objectFieldMapping;

    /**
     * Default constructor. Do nothing special.
     */
    public ValidationResult() {
        super();
    }

    /**
     * Constructor that sets the object to validate.
     * 
     * @param o Object to validate in instantiation with constructor
     */
    public ValidationResult(Object o) {
        thingToValidate = o;
    }

    /**
     * Constructor that sets the object to validate and the object's import
     * context.
     * 
     * @param importContext String of context
     * @param o Object to validate
     */
    public ValidationResult(String importContext, Object o) {
        this.setThingToValidate(o);
        this.setObjectImportContext(importContext);
    }

    /**
     * Actually run the vbalidation. This throws runtime exceptions if the
     * validation file is hosed, or if the datatype for the import is bad.
     * 
     * @return True if the validation was successful, false otherwise.
     */

    @SuppressWarnings("unchecked")
    public boolean validate() {

        // Flag used to check if there are any bad-original-field-data errors
        boolean badDataFlag = false;
        this.getResultHolder().clearErrorsAndMessages();
        
        if (null == thingToValidate) {
            log.error("cannot validate a null object. "
                    + this.getObjectImportContext(),
                    ImporterError.NULL_VALIDATION_OBJECT);
            return false;

        }
        try {
            ActionValidatorManagerFactory.getInstance().validate(
                    thingToValidate, dataTransferContextString, vContext);
        } catch (com.opensymphony.xwork2.validator.ValidationException e) {
            e.printStackTrace();
            log.fatal(
                    "Bad configuration for validation of "
                            + thingToValidate.getClass().getName()
                            + ". Type mismatch.",
                    ImporterError.VALIDATION_TYPE_MISMATCH, e);
            throw new RuntimeException(e);
        } catch (ClassCastException e) {
            log.fatal(
                    "Bad configuration for validation of "
                            + thingToValidate.getClass().getName()
                            + ". Type mismatch.",
                    ImporterError.VALIDATION_TYPE_MISMATCH, e);
            throw new RuntimeException(e);
        }

        if (null == vContext.getFields()) {
            badDataFlag = false;
        } else {
            badDataFlag = vContext.getFields().getBadDataFlag(); 
        }
        if (getResultHolder().hasErrors() 
                || getResultHolder().hasFieldErrors()
                || badDataFlag
           ) {
            if (this.isWriteErrors()) {
                log.error("Import for " + thingToValidate.getClass().getName()
                        + " has the following validation errors. "
                        + this.getObjectImportContext(),
                        ImporterError.VALIDATION_ERRORS_EXIST);
                for (String err : (ArrayList<String>) getResultHolder()
                        .getActionErrors()) {
                    log.error("Validation error for : " + err,
                            ImporterError.FAILED_GENERAL_VALIDATION);
                }
                LinkedHashMap<String, ArrayList> temp = new LinkedHashMap<String, ArrayList>(
                        this.getResultHolder().getFieldErrors());
                for (ArrayList<String> errors : temp.values()) {
                    for (String errorString : errors) {
                        log.error("Validation error for : " + errorString,
                                ImporterError.FAILED_FIELD_VALIDATION);
                    }
                }
                if (badDataFlag) {
                    log.error(vContext.getFields().getBadData(), ImporterError.FAILED_FIELD_VALIDATION);
                    // We really only want to report on this one time...
                    vContext.getFields().setBadDataFlag(false);
                }
                log.error("End of errors for "
                        + thingToValidate.getClass().getName() + ". "
                        + this.getObjectImportContext(),
                        ImporterError.VALIDATION_ERRORS_EXIST2);
            }
            return false;
        }
        return true;

    }

    /**
     * Set the object to validate and validate.
     * 
     * @param o
     *            the object to validate
     * @return True if the validation was successful, false otherwise.
     */
    public boolean validate(Object o) {
        this.setThingToValidate(o);
        return validate();
    }

    /**
     * Set the object to validate and validate.
     * 
     * @param importContext
     *            information that can be used to identify the object that
     *            failed validation
     * @param fields the field map of original data.Vestigial.            
     * @param o
     *            the object to validate
     * @return True if the validation was successful, false otherwise.
     */
    public boolean validate(String importContext, FieldMap fields, Object o) {
        this.setThingToValidate(o);
        this.setObjectImportContext(importContext);
        return validate();
    }

    /**
     * Validate the model object in the ObjectContext passed in.
     * @param importContext string (like 'import' whichhelps determina which validation to use.
     * @param objectContext the objectcontext for the object being validated.
     * @param o the object to validate.
     * @return true if the object passes validation.
     */
    public boolean validate(String importContext, ObjectContext<? extends FieldMap, Object> objectContext, Object o) {
        this.setThingToValidate(o);
        this.setObjectImportContext(importContext);
        vContext.setFields(objectContext);
        return validate();
    }

    /**
     * Get the string that represents the information that can be used to
     * identify the object. This string is emitted in the log, and will be part
     * of the output for the tagged error data file
     * 
     * @return the string that represents the information that can be used to
     *         identify the object.
     */
    public String getObjectImportContext() {
        return objectImportContext;
    }

    /**
     * Set the string that represents the information that can be used to
     * identify the object. This string is emitted in the log, and will be part
     * of the output for the tagged error data file
     * 
     * @param objectImportContext
     *            the string that represents the information that can be used to
     *            identify the object.
     */
    public void setObjectImportContext(String objectImportContext) {
        this.objectImportContext = objectImportContext;
    }

    /**
     * Get the object being validated/to be validated.
     * @return the object to validate.
     */
    public Object getThingToValidate() {
        return thingToValidate;
    }

    /**
     * Set the object being validated/to be validated.
     * @param thingToValidate the object being validated/to be validated.
     */
    public void setThingToValidate(Object thingToValidate) {
        this.thingToValidate = thingToValidate;
    }

    /**
     * 
     * @return ValidationAwareSupport object
     */
    public ValidationAwareSupport getResultHolder() {
        return resultHolder;
    }

    /**
     * 
     * @return Map object
     */
    public Map<String, Collection<String>> getErrorList() {
        return this.getResultHolder().getFieldErrors();
    }

    /**
     * Gets the value of writeErrors.
     * @return the writeErrors
     */
    public boolean isWriteErrors() {
        return writeErrors;
    }

    /**
     * Sets the value of the writeErrors.
     * @param writeErrors the writeErrors to set
     */
    public void setWriteErrors(boolean writeErrors) {
        this.writeErrors = writeErrors;
    }
    
    /**
     * Clears the errros messages.
     */
    public void clearErrors() {
        this.getResultHolder().clearErrorsAndMessages();
    }
        
    /**
     * Sets the value of the dataTransferContext. 
     * @param dataTransferContextStringParam the import/export context String
     */
    public void setdataTransferContextString(String dataTransferContextStringParam) {
        this.dataTransferContextString = dataTransferContextStringParam;
    }

    /**
     * Gets the value of the dataTransferContextString.
     * @return dataTransferContextString
     */
    public String getDataTransferContextString() {
        return this.dataTransferContextString;
    }

    /**
     * Set the flag which indicates if the validation is being run before or after stuff is looked up in the DB.
     * @param preLookupFlag flag as above.
     */
    public void setValidationType(boolean preLookupFlag) {
        vContext.setPreLookup(preLookupFlag);
    }

    /**
     * Gets the value of objectFieldMapping.
     * @return the objectFieldMapping
     */
    public static HashMap<String, HashMap<String, String>> getObjectFieldMapping() {
        return objectFieldMapping;
    }

    /**
     * Sets the value of the objectFieldMapping.
     * @param objectFieldMapping the objectFieldMapping to set
     */
    public static void setObjectFieldMapping(
            HashMap<String, HashMap<String, String>> objectFieldMapping) {
        ValidationResult.objectFieldMapping = objectFieldMapping;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 