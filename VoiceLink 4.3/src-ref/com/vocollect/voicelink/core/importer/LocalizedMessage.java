/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.util.ResourceUtil;

import java.text.MessageFormat;
import java.util.Locale;

/**
 * Utility class to wrap localizing exception messages.
 * @author astein
 */
public final class LocalizedMessage {

    /**
     * Private c'tor- this has all static methods...
     */
    private LocalizedMessage() {
        // Nothing to do
    }
    
    /**
     * This method will return the string derived from the key within the
     * properties file.
     * 
     * @param key specifies the key to look up within the properties file
     * @param args specifies the Object array
     * @param locale specifies the Locale object
     * @return the String value of the properties lookup
     */
    public static String getMessageString(final String key, final Object[] args,
                                          Locale locale) {
        if (locale == null) {
            locale = getDefaultLocale();
        }
        return ResourceUtil.getLocalizedMessage(key, args, locale);
    }

    /**
     * This method will format the string derived from the key within the
     * properties file.
     * 
     * @param key
     *            specifies the key to look up within the properties file
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final String key) {
        final MessageFormat mf = new MessageFormat(getMessageString(key, null, null));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the error code object and
     * will check for that key within the properties file.
     * 
     * @param errorCode
     *            specifies the errorCode to look up within the properties file
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final ErrorCode errorCode) {
        final String key = errorCode.getDefaultMessageKey();
        final MessageFormat mf = new MessageFormat(getMessageString(key, null, null));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the error code object and
     * will check for that key within the properties file and will also pass in
     * any parameters to be displayed in the error message.
     * 
     * @param errorCode specifies the errorCode to look up within the properties file
     * @param args specifies an Object array of parameters to display within the
     *            error
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final ErrorCode errorCode, final Object[] args) {
        final String key = errorCode.getDefaultMessageKey();
        final MessageFormat mf = new MessageFormat(getMessageString(key, args, null));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the key within the
     * properties file and will also pass in any parameters to be displayed in
     * the error message.
     * 
     * @param key
     *            specifies the key to look up within the properties file
     * @param args
     *            specifies an Object array of parameters to display within the
     *            error
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final String key, final Object[] args) {
        final MessageFormat mf = new MessageFormat(getMessageString(key, args, null));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the error code object and
     * will check for that key within the properties file and will also pass in
     * any parameters to be displayed in the error message.
     * 
     * @param errorCode
     *            specifies the errorCode to look up within the properties file
     * @param args
     *            specifies an Object array of parameters to display within the
     *            error
     * @param locale
     *            specifies a Locale object to display
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final ErrorCode errorCode, final Object[] args,
            final Locale locale) {
        final String key = errorCode.getDefaultMessageKey();
        final MessageFormat mf = new MessageFormat(
                getMessageString(key, args, locale));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the key within the
     * properties file and will also pass in any parameters to be displayed in
     * the error message.
     * 
     * @param key
     *            specifies the key to look up within the properties file
     * @param args
     *            specifies an Object array of parameters to display within the
     *            error
     * @param locale
     *            specifies a Locale object to display
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final String key, final Object[] args, final Locale locale) {
        final MessageFormat mf = new MessageFormat(
                getMessageString(key, args, locale));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the error code object and
     * will check for that key within the properties file.
     * 
     * @param errorCode
     *            specifies the errorCode to look up within the properties file
     * @param locale
     *            specifies a Locale object to display
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final ErrorCode errorCode, final Locale locale) {
        final String key = errorCode.getDefaultMessageKey();
        final MessageFormat mf = new MessageFormat(
                getMessageString(key, null, locale));
        return mf.format(new String[0]);
    }

    /**
     * This method will format the string derived from the key within the
     * properties file.
     * 
     * @param key specifies the key to look up within the properties file
     * @param locale specifies a Locale object to display
     * @return the String value in the appropriate MessageFormat
     */
    public static String formatMessage(final String key, final Locale locale) {
        final MessageFormat mf = new MessageFormat(
                getMessageString(key, null, locale));
        return mf.format(new String[0]);
    }

    /**
     * This method will get the default Locale and if none is found, will set it
     * to English.
     * 
     * @return the default Locale
     */
    public static Locale getDefaultLocale() {
        Locale locale = Locale.getDefault();
        if (locale == null) {
            locale = new Locale("en");
        }
        return locale;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 