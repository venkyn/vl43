/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */package com.vocollect.voicelink.core.importer;


import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

//------------------------------------------------------Start of inner class.------------------------------------//
/**
 * This class simply wraps the XML output in a root element.
 * Castor is unable to do this - leave a tag open so that we can add to it.
 */
/**
 * @author dgold
 *
 */
public class ErrorFileWriter extends BufferedWriter {
    
    private String prefix = "<import-errors>";
    private String suffix = "</import-errors>";

    /**
     * Constructor delegates to BufferedWriter.
     * @param w writer
     */
    public ErrorFileWriter(Writer w) {
        super(w);
    }
    
 
    /**
     * @see java.io.BufferedWriter#flush()
     * @throws IOException -
     */
    @Override
    public void flush() throws IOException {
        // TODO Auto-generated method stub
        super.flush();
    }

    /**
     * @see java.io.BufferedWriter#newLine()
     * @throws IOException -
     */
    @Override
    public void newLine() throws IOException {
        // TODO Auto-generated method stub
        super.newLine();
    }

    /**
     * @see java.io.BufferedWriter#write(char[], int, int)
     * @param cbuf - char buffer
     * @param off - offset
     * @param len - length
     * @throws IOException -
     */
    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        // TODO Auto-generated method stub
        super.write(cbuf, off, len);
    }

    /**
     * @see java.io.BufferedWriter#write(int)
     * @param c character
     * @throws IOException -
     */
    @Override
    public void write(int c) throws IOException {
        // TODO Auto-generated method stub
        super.write(c);
    }

    /**
     * @see java.io.BufferedWriter#write(java.lang.String, int, int)
     * @param s String
     * @param off Offset
     * @param len length
     * @throws IOException -
     */
    @Override
    public void write(String s, int off, int len) throws IOException {
        // TODO Auto-generated method stub
        super.write(s, off, len);
    }

    
   
    /**
     * Set the identification for the error file.
     * This becomes an attribute of the root element as follows:
     * <import-errors import-id="..."/>
     * The import id should be the description of the import, more detailed if warranted.
     * @param importIdentification The string to use for the id field in the root element of the XML
     */
    public void setIdentification(String importIdentification) {
        this.setPrefix("<import-errors importId=\"" + importIdentification + "\">");
    }
    
    /**
    * Override to add the root-element-close to the end of the file.
     * @see java.io.Closeable#close()
     * @throws IOException passthru from parent
     */
    @Override
    public void close() throws IOException {
        write(this.suffix);
        super.close();
    }


    /**
     * @param prefix the prefix to set
     */
    void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    /**
     * @return the prefix
     */
    String getPrefix() {
        return prefix;
    }
    
}
//------------------------------------------------------End of inner class.-------------------------------------//
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 