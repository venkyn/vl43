/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.io.IOException;

/**
 * This abstract class declare methods to create records from list of fields.
 * Also holds generically-useful information: the output adapter, 
 * the configuration file name and mapping file name
 * 
 * @author Kalpna
 * @param <FMT> extends FieldMap field map and field to create record
 * @param <FT> extends Field 
 */
public abstract class OutputFabricator<FMT extends FieldMap, FT extends Field>
        implements Describable {

    private static final Logger log          = new Logger(
                                                  OutputFabricator.class);

    //Output adaper which writes/append the record to the destination
    private OutputAdapter outputAdapter;

    //class, subclass role
    private Description       description = new Description();
    
    //Hold the field definition
    private String            configFileName;

    //Holds field mapping information
    private String            mappingFileName;
    
    //Holds the list of fields definition for the export markup
    private FMT fieldList;


    /**
     *      
     * Constructor for OutputFabricator. This initialize all vars and set the configuration file.
     * @param configFileName
     *            to be set
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    //TODO not sure why will need this constructor
    public OutputFabricator(String configFileName)
            throws ConfigurationException {
        super();
        init();
        this.setConfigFileName(configFileName);
        return;
    }
    
    /**
     * Default constructor. Set all internal vars to null.
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    public OutputFabricator() throws ConfigurationException {
        super();
        init();
        return;
    }


    /**
     * Central initialization for all vars.      
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    private void init() throws ConfigurationException {
       
        this.setConfigFileName(null);
        this.setOutputAdapter(null);
        this.setFieldList(null);
        return;
    }

    /**
     * Close the output adapter.
     */
    public void close() {
        try {
            if (getOutputAdapter() != null) {
                this.getOutputAdapter().close();
            }
        } catch (IOException e) {
            log.debug("Error closing the output adapter connection " 
                +  e.getMessage() + OutputFabricator.class, 
                OutputFabricatorError.CLOSE_CONNECTION_ERROR);
            
        }
        return;
    }

    /**
     * Call the appropriate method on the output adapter
     * to close the connection. This is just the 
     * wrapper for OutputAdapter closeOnSuccess method.
     * @return boolean returns true when successful. This
     * returns false when failed to move the exported file
     * to the output directory.

     */
    public boolean closeOnSuccess() {

       return this.getOutputAdapter().closeOnSuccess();
    }
    
    /**
     * Call the appropriate method on the output adapter
     * to close the connection. This is just the wrapper
     * for OutputAdapter closeOnFailure method.
     * @return boolean returns true when successful. This
     * returns false when failed to move the exported file
     * to the output directory.
     */
    public boolean closeOnFailure() {

        return this.getOutputAdapter().closeOnFailure();
    }

    

    // -----------------Abstract methods----------------------//

    /**
     * This method creates records based on the input FixedLengthFieldMap.
     * This method either write or append the record to the configured destination
     * Possible reasons to throw an exception: : IO error : record too short :
     * Record too long.
     * @param fixLenFldMap holds the input data field values 
     * @throws VocollectException
     *             signature allows us to throw an exception if we can't create a record
     */
    public abstract void createRecord(FixedLengthFieldMap fixLenFldMap) throws VocollectException;
    
    /**
     * Setup the OutputFabricator using setConfig.
     * @return true if the configuration succeeded
     * @throws ConfigurationException
     *             for bad configuration
     */
    public abstract boolean configure() throws ConfigurationException;
    
    /**
     * Returns the number of records created / written to the 
     * destination by fabricator.
     * @return the recordsCreated
     */
    public abstract long getRecordsCreated();


    // -----------------Abstract methods end------------------//
    

    // ----------------Getters and setters----------------------//
    /**
     * @return Returns the outputAdapter.
     */
    public OutputAdapter getOutputAdapter() {
        return this.outputAdapter;
    }

    /**
     * @param outputAdapter
     *            The outputAdapter to set.
     */
    public void setOutputAdapter(OutputAdapter outputAdapter) {
        this.outputAdapter = outputAdapter;
    }
    
    /**     
     * @return Returns the FieldMap.
     */
    public FMT getFieldList() {
        return this.fieldList;
    }

    /**
     * @param fieldList
     *            The fieldList to set.
     */
    public void setFieldList(FMT fieldList) {
        this.fieldList = fieldList;
    }

    
    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.util.Describable#getDescription()
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.util.Describable#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description.setDescription(description);
    }

    /**
     * @return gets the configFileName
     */
    public String getConfigFileName() {
        return this.configFileName;
    }

    /**
     * Sets the configFileName and calls configure if the mapping file is also set.    
     * @param configFileName
     *            name of file holding field definitions
     * @throws ConfigurationException
     *             thrown by configure if the config file is bad
     */
    public void setConfigFileName(String configFileName)
            throws ConfigurationException {
        this.configFileName = configFileName;
        if ((null != getMappingFileName())
                && (0 != getMappingFileName().compareTo(""))) {
            configure();
        }
    }
    
    /**
     * @return Returns the mappingFileName.
     */
    public String getMappingFileName() {
        return this.mappingFileName;
    }

    /**
     * Set the mapping file name and call configure if the config filename is also set.
     * @param mappingFileName
     *            The mappingFileName to set.
     * @throws ConfigurationException
     *             thrown by configure
     */
    public void setMappingFileName(String mappingFileName)
            throws ConfigurationException {
        this.mappingFileName = mappingFileName;
        if ((null != this.getConfigFileName())
                && (0 != this.getConfigFileName().compareTo(""))) {
            configure();
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 