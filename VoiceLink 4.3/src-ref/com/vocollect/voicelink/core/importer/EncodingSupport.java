/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the enumeration for Encoding which will be supporting 
 * for VoiceLink import.
 * Usage call the appropriate static class method.
 * This is the exception for utility class , where class 
 * is not declare as final as we do want to provide the flexibility
 * to extend this class in order to support other encodings
 * @author Kalpna
 */
public final class EncodingSupport {
    
    /**
     * Defult Encoding. VoiceLink imports will use this encoding
     * if no input encoing is provided.
     */
    public  static final String DEFAULT_ENCODING              = "UTF-8";
    
    /**
     * This is the Byte Order Mark char for UTF-8 charset.
     */
    public static final char BOM = '\uFEFF';    
    
    //UTF-8 charset encoding.
    public static final String UTF8 = "UTF-8";
    //This is the charset where only the lower 127 values are valid (ASCII-7)    
    public static final String USASCII = "US-ASCII";
    //AKA Latin-1, the typical extended ASCII for western-european charsets
    public static final String ISO88591 = "ISO-8859-1";

    public static final String UTF16 = "UTF-16";
    //This List stores all the charset encodings those are supported by VoiceLink imports
    private static List<String> encodingList = new ArrayList<String>();
    
    /**
     *  Do not instantiate the class.
     */    
    private EncodingSupport() { }
    
    /*
     * This is static initialization block 
     * to populate the encodingList
     */
    static {
        encodingList.add(UTF8);
        encodingList.add(USASCII);
        encodingList.add(ISO88591);        
        encodingList.add(UTF16);        
    }
    
    /**
     * This method returns the charset encoding supported 
     * for VoiceLink imports.
     * @return encodingList the list of charset encoding
     */
    private static List<String> encodingSupported() {
        return encodingList;
    }

    /**
     * This method returns boolean to indicate if the charset encoding 
     * is supported or not for VoiceLink imports.
     * @param encodingP input charset encoding
     * @return boolean
     */
    public static boolean isEncodingSupported(String encodingP) {
        return EncodingSupport.encodingSupported().contains(encodingP);
    } 
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 