/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.Collection;
import java.util.LinkedHashMap;

/**
 * This class holds an indexed list of all of the fields,
 * which can also be accessed in the order that the fields were added to the map.
 * 
 * Populated by the DataSourceParser's configuration, and passed to the Importer
 * to use to construct objects.
 * @author dgold
 */
public class FieldMap extends LinkedHashMap<String, Field> {

    private static final long serialVersionUID = 8811684352469972962L;

    private int startingPosition = 0;
    
    private boolean badData = false;

    /**
     * Default empty constructor.
     *
     */
    public FieldMap() {
        super();
    }

    /**
     * Constructor with size integer.
     * @param size int to set
     */
    public FieldMap(int size) {
        super(size);
    }

    /**
     * 
     * @param aField FieldType object to add
     */
    public void addField(Field aField) {
        if (null != aField) {
            // Can only set starting position for FixedLengthFields.
            if (aField instanceof FixedLengthField) {
                ((FixedLengthField) aField).setStart(this.startingPosition);
                this.startingPosition = this.startingPosition
                    + ((FixedLengthField) aField).getLength();
            }

            this.put(aField.getFieldName(), aField);
            if (aField.isBadData()) {
                this.setBadData(true);
            }
        }
    }

    /**
     * Sends the output of values to the System error stream.
     *
     */
    public void recite() {
        for (Object x : this.values()) {
            System.err.println(x.toString());
        }

    }
    
    /**
     * {@inheritDoc}
     * @see java.util.AbstractMap#toString()
     */
    public String toString() {
        String fields = " [fields: ";
        for (Object x : this.values()) {
            fields += x.toString() + " ";
        }
        return fields + "]";

    }

    /**
     * 
     * @return the number to start at
     */
    public int getStartingPosition() {
        return this.startingPosition;
    }

    /**
     * 
     * @param startingPosition the number to set
     */
    public void setStartingPosition(int startingPosition) {
        this.startingPosition = startingPosition;
    }

    /**
     * @return the Collection of FieldType objects 
     */
    @Override
    public Collection<Field> values() {
        return super.values();
    }

    /**
     * Gets the value of badData.
     * @return the badData
     */
    public boolean isBadData() {
        return badData;
    }

    /**
     * Sets the value of the badData.
     * @param badData the badData to set
     */
    public void setBadData(boolean badData) {
        this.badData = badData;
    }
    
    /**
     * Reset internal state.
     */
    public void reset() {
        setBadData(false);
        for (Field x : this.values()) {
            x.reset();
        }

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 