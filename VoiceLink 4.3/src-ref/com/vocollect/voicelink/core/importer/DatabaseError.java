/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.model.DataObject;


/**
 * @author Kalpna
 * This class to hold any database error detail for
 * import/export
 */
public class DatabaseError {
    
    //Object failed to import
    private Object theObject = null;
    
    //Database Exception which cause failure for object import
    private Exception theException = null;
        
    /**
     * Default Constructor.
     * Constructor.
     */
    public DatabaseError() {
      super();
    }  
    /**
     * Database Error constructor.
     * @param theObject which could not be imported
     * @param theException which caused the object import failure
     */
    public DatabaseError(Object theObject, Exception theException) {
        this.theObject = theObject;
        this.theException = theException;
        
    }

    /**
     * @return the theObject
     */
    public Object getTheObject() {
        return theObject;
    }

    /**
     * @param theObject the theObject to set
     */
    public void setTheObject(Object theObject) {
        this.theObject = theObject;
    }

    /**
     * @return the theException
     */
    public Exception getTheException() {
        return theException;
    }

    /**
     * @param theException the theException to set
     */
    public void setTheException(Exception theException) {
        this.theException = theException;
    }

    /**
     * @return the theException String
     */
    public String getTheExceptionString() {
      return theException.getLocalizedMessage();
    }

    /**
     * @return the theObject String
     */
    public String getTheObjectString() {
        try {
            DataObject dataObject = (DataObject) theObject;
            dataObject.toString();
            dataObject.getId();
            return "Object: " + theObject.getClass().getName() 
                   + " Object descriptive text: " + dataObject.getDescriptiveText() 
                   + " Object id: " + dataObject.getId() + dataObject.toString();
        } catch (ClassCastException e) {
          //Possible   
        }
        return theObject.getClass().getName();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 