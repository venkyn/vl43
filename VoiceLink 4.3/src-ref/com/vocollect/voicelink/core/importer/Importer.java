/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.dao.exceptions.BatchDataAccessException;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;
import com.vocollect.voicelink.core.importer.triggers.LookupException;

import com.opensymphony.xwork2.ObjectFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 * The Importer is the part that imports a datatype.
 * 
 * This essentially consists of an ImportDatatype methods, which garbs a list of
 * fields, converts it to an object, applies validation and, if the validation
 * allows, stuffs the result into the database.
 * 
 * @author dgold
 * 
 */
@SuppressWarnings({ "unchecked", "unused" })
public class Importer extends DataTransferBase implements Describable {

    private String siteName = Site.DEFAULT_SITE_NAME;

    static final Logger log = new Logger(Importer.class);

    private ValidationResult validator = new ValidationResult();

    // This is the object that manages the validation framework.

    private DBLookup objectCompleter = new DBLookup(getPersistenceManager());

    private int elementCount = 0;

    private ObjectContext objectContext = new ObjectContext();

    private List list = new ArrayList();

    // This is for the future- we will want to do a better job reporting an
    // error
    // when the batch update fails.
    // private List<ObjectContext> contextList = new
    // LinkedList<ObjectContext>();

    private boolean batchItemFailedState = false;

    private boolean cleanupRan = false;

    private static final boolean PRE_LOOKUP = true;

    private static final boolean POST_LOOKUP = false;

    /**
     * Default constructor. Set the legal state changes. If we ever make another
     * constructor, we should move this off to an init() member.
     */
    public Importer() {
        super();
        // This is a required workaround for a known issue in Struts2--without
        // this call the import validations fail because the ValidatorFactory
        // can't be instantiated -- ddoubleday
        ObjectFactory.setObjectFactory(new ObjectFactory()); 
    }

    /**
     * This is the main routine of the Import. Essentially, this will open the
     * data source, read the records within, convert them into objects via XML,
     * and save them appropriately.
     * 
     * @return true if successful
     */
    @Override
    public boolean runDatatypeJob() {
        int elementsInBatch = 0;
        boolean retValue = true;
        boolean closed = false;
        boolean didWorkThisJob = false;

        // Create the tagging for the fields, save with the field itself. Where
        // better to save it?
        try {

            this.logStartUpInfo();
            this.getSourceParser().setDatatype(this.getDatatype());
            this.getSourceParser().setTriggers(getTriggers());
            this.getSourceParser().setUnmar(this.getUnmar());
            this.getSourceParser().setSiteName(this.getSiteName());
            this.getPersistenceManager().setSiteName(this.getSiteName());

            // Set to Running.
            this.getStateChanges().setCurrentState(RUNNING_STATE);

            // Set the validation context string for validator
            if (this.getValidationContextString() != null) {
                this.validator.setdataTransferContextString(IMPORT_VCS_PREFIX
                    + "-" + this.getValidationContextString());
            } else {
                this.validator.setdataTransferContextString(IMPORT_VCS_PREFIX);
            }
            // This is supposed to be used to translate from the model object
            // names to the
            // reported field names. This implementation is incomplete:
            // The XWork validators use the model names, and we use a few of
            // those.
            // rather than introduce an inconsistency, we will always use the
            // model name.
            ValidationResult
                .setObjectFieldMapping(this.getFieldToBindNameMap());

            // While we are not done processing data.
            // This means either done with the table or no more files to
            // process.
            // Either of those should exit with an exception.
            while (0 == this.getCurrentState().compareTo(RUNNING_STATE)) {
                this.getStats().datasetStart();
                try {
                    // This will set the state of the parser to IN_PROGRESS.
                    // As the data source being parsed is read, the state will
                    // change accordingly.
                    getSourceParser().resetState();
                    try {
                        // Do a prescan if needed. If it passes, start
                        // processing the contents of the
                        // data source.
                        if (this.getSourceParser().preScan()) {

                            boolean outOfData = false;

                            // Go thru the input, parsing each record into the
                            // XML string
                            // This is for each record, and should exit when
                            // each discrete
                            // data source is exhausted
                            while (!outOfData) {
                                cleanupRan = false;

                                validateAndSaveRecords();
                                outOfData = this.getSourceParser()
                                    .isOutOfData();
                                elementCount = 0;
                                didWorkThisJob = true;
                            }
                        }
                    } catch (NoWorkException n) {
                        // Database import currently ends with a NoWorkException
                        if (!didWorkThisJob) {
                            // if you didn't do work this iteration or this job
                            // do this..
                            this.getStateChanges().setCurrentState(
                                DataTransferBase.NO_WORK_STATE);
                            log.info(
                                getDescription() + ": " + "No work found.",
                                ImporterError.NO_WORK_TO_DO);
                        }
                        // Exit the loop
                        break;
                    }
                    try {
                        // Call bulk update
                        bulkSaveUpdate();
                        getSourceParser().finalizeState();
                    } catch (BusinessRuleException b) {
                        // Cannot save tdue to business rules exception
                        log.error(
                            "Business rule exception thrown saving object "
                                + list.get(0).getClass().getCanonicalName()
                                + " from last " + list.size()
                                + " records of input" + ": "
                                + b.getUserMessage().getKey(),
                            ImporterError.BUSINESS_RULE_VIOLATION2);
                        Object[] o = {
                            list.get(0).getClass().getCanonicalName(),
                            " from last " + list.size() };
                        this.saveErrorRecord(new ExceptionError(
                            new UserMessage(LocalizedMessage.formatMessage(
                                ImporterError.BUSINESS_RULE_VIOLATION2, o))
                                .getKey(), validator, b));
                        getSourceParser().finalizeState();
                        this.batchItemFinish();
                        this.cleanup();
                    }
                } catch (OutOfDataException oe) {
                    // Normal termination for some data sources.
                    try {
                        // Call bulk update
                        bulkSaveUpdate();
                        getSourceParser().finalizeState();
                    } catch (BusinessRuleException b) {
                        // Cannot save tdue to business rules exception
                        log.error(
                            "Business rule exception thrown saving object "
                                + list.get(0).getClass().getCanonicalName()
                                + " from last " + list.size()
                                + " records of input" + ": "
                                + b.getUserMessage().getKey(),
                            ImporterError.BUSINESS_RULE_VIOLATION2);
                        Object[] o = {
                            list.get(0).getClass().getCanonicalName(),
                            " from last " + list.size() };
                        this.saveErrorRecord(new ExceptionError(
                            new UserMessage(LocalizedMessage.formatMessage(
                                ImporterError.BUSINESS_RULE_VIOLATION2, o))
                                .getKey(), validator, b));
                        getSourceParser().finalizeState();
                        this.batchItemFinish();
                        this.cleanup();
                    }
                    throw oe;
                } catch (PrescanException e) {
                    cleanupRan = false;
                    // Prepare the argument for the message. This is thrown when
                    // the prescan
                    // detects a duplicate record, and the file is suipposed to
                    // fail if there are ay duplicates
                    retValue = reportException(e);
                } catch (RecordLengthException e) {
                    cleanupRan = false;
                    // This will occur is the prescan can't be run.
                    // This is a bad record. report it, along with the record
                    // number
                    retValue = reportException(e);
                } catch (BadDataTransformException e) {
                    cleanupRan = false;
                    // Some record has invalid data
                    // Will not fail the entire import unless it exceeds the
                    // threshould
                    // value
                    log.error(
                        getDescription() + ": " + "Bad record at record "
                            + this.getStats().getDataElements() + ". "
                            + e.getUserMessage().getKey(),
                        ImporterError.FAILED_IMPORT);
                    retValue = reportException(e);
                }
                if (!cleanupRan) {
                    bulkSaveUpdate();
                    getSourceParser().finalizeState();
                    this.batchItemFinish();
                    this.cleanup();
                }
                // end individual-datasource-terminating errors.
            } // End while
            // Errors caught here are either job-terminating, or probably
            // programming errors.
        } catch (OutOfDataException o) {
            // normal end of execution
        } catch (VocollectException e) {
            // This is an IO exception, which should fail the import?
            log.error(
                getDescription() + ": " + "Caught exception " + e,
                ImporterError.FAILED_IMPORT);
            retValue = reportException(e);
        } catch (Exception e) {
            // Handles all exceptions
            log.error(
                getDescription() + ": " + "Caught exception " + e,
                ImporterError.FAILED_IMPORT);
            retValue = reportException(e);
        } catch (Throwable t) {
            log.error(
                getDescription() + ": " + "Caught exception " + t,
                ImporterError.FAILED_IMPORT);
            retValue = reportException(new Exception(t));
        } finally {
            if (!cleanupRan) {
                getSourceParser().finalizeState();
                this.batchItemFinish();
                this.cleanup();
            }
            batchFinish();
        }
        return retValue;
    }

    /**
     * This is bulk update wrapper method.
     * @throws BusinessRuleException from persistent manager
     * @throws DataAccessException from persistent manager
     */
    private void bulkSaveUpdate()
        throws BusinessRuleException, DataAccessException {
        while (!list.isEmpty()) {
            try {
                getPersistenceManager().save(list);
                list.clear();
                getCurrentSession().clear();
                getSourceParser().clearInputRecords();

                // Manage state of the current data source.
                getSourceParser().acceptRecord();
                takeThrottlingNap();
            } catch (BatchDataAccessException e) {
                // Manage state of the current data source.
                getNewSession();
                getSourceParser().failRecord();
                // get the number of records failed
                int failedIndex = e.getFailureCount();
                if (failedIndex >= list.size()) {
                    failedIndex = list.size() - 1;
                }
                if (failedIndex < 0) {
                    failedIndex = 0;
                }

                long eleNum = (this.getStats().getDataElements() - list.size()) + 1;
                BaseModelObject bmo = (BaseModelObject) list.get(failedIndex);
                log.error("Exception received saving element " + eleNum + ": "
                    + bmo.getClass().getSimpleName() + "("
                    + bmo.getDescriptiveText() + ").  Cause is: "
                    + e.getCause(), ImporterError.FAIL_LIST_SEGMENT);

                for (int k = 0; k <= failedIndex; k++) {
                    bmo = (BaseModelObject) list.get(failedIndex);
                    // Store name, descriptive text, and element number in
                    // object for localization.
                    Object[] o = {
                        bmo.getClass().getSimpleName(),
                        bmo.getDescriptiveText(),
                        (this.getStats().getDataElements() - list.size()) + k
                            + 1 };
                    this.saveErrorRecord(new DatabaseError(bmo, new Exception(
                        LocalizedMessage.formatMessage(
                            ImporterError.BATCH_SAVE_FAILURE, o))));
                }
                for (int j = failedIndex; j >= 0; --j) {
                    list.remove(j);
                }
            }
        }

    }

    /**
     * Cleanup resourses.
     */
    @Override
    public void cleanup() {
        if (!cleanupRan) {
            if (null != this.getSourceParser()) {
                // We will close on failure, and as long as not NO_WORK close on
                // success.
                if ((STOPPED_STATE == getCurrentState())
                    || (NOT_RUN_STATE == getCurrentState())) {
                    // Import Manager is Interrupted, so we don't move the
                    // files.
                } else if (NO_WORK_STATE != getCurrentState()) {
                    if (getSourceParser().getCurrentState() == DataSourceParser.FAILED_STATE) {
                        getSourceParser().closeOnFailure();
                    } else {
                        this.getSourceParser().closeOnSuccess();
                    }
                }
            }
            this.cleanupRan = true;
        }
    }

    /**
     * Write an exception record, XML and validation state to the
     * FailedValidation file. Manage the current state.
     * @param e the exception to report on
     * @return the value to set the return value of the caller to.
     */
    private boolean reportException(Exception e) {
        // this.getStateChanges().setCurrentState(FAILED_STATE);
        batchItemFailedState = true;
        this.saveErrorRecord(new ExceptionError(this.getXmlrepresentation()
            .getXmlRepresentation(), validator, e));
        return false;
    }

    /**
     * Write an exception record, XML and validation state to the
     * FailedValidation file. Manage the current state.
     * @param e the exception to report on
     * @return the value to set the return value of the caller to.
     */
    private boolean reportException(PrescanException e) {
        Object[] args = new Object[1];
        args[0] = this.getSourceParser().getSourceAdapter().getDataSourceName();
        String localizedLogMsg = LocalizedMessage.formatMessage(e
            .getErrorCode(), args);
        this.saveErrorRecord(new PrescanError(localizedLogMsg));
        // this.getStateChanges().setCurrentState(FAILED_STATE);
        batchItemFailedState = true;
        return false;
    }

    /**
     * Write an exception record, XML and validation state to the
     * FailedValidation file.
     * @param e the exception to report on
     * @return the value to set the return value of the caller to.
     */
    private boolean reportException(RecordLengthException e) {
        if (e.getErrorCode().equals(DataSourceParserError.WRONG_RECORD_COUNT)) {
            log.error(getDescription()
                + ": "
                + "Bad record at record "
                + this.getSourceParser().getSourceAdapter()
                    .getCurrentLineNumber() + ". "
                + "Record count is incorrect. ", ImporterError.FAILED_IMPORT);

        } else if (e.getErrorCode().equals(
            DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH)) {
            log.error(getDescription() + ": " + "Bad record at record "
                + this.getSourceParser().getRecordsRead() + ". "
                + "Record length is incorrect. ", ImporterError.FAILED_IMPORT);
        } else {
            log.error(getDescription() + ": " + "Bad record at record "
                + this.getSourceParser().getRecordsRead() + ". "
                + e.getUserMessage().getKey(), ImporterError.FAILED_IMPORT);
        }
        // Get the args for error to make the localized
        // error string
        Object[] args = null;
        Vector v = new Vector();

        int cnt = 0;
        for (Object obj : e.getUserMessage().getValues()) {
            v.add(obj);
        }

        args = v.toArray();

        String localizedLogString = LocalizedMessage.formatMessage(e
            .getErrorCode(), args);
        this.saveErrorRecord(new PrescanError(localizedLogString));
        batchItemFailedState = true;
        return false;
    }

    /**
     * Read, validate and save records in a batch.
     * 
     * @return true if there is more data
     * @throws ImportConfigurationException if a config error occurs
     * @throws VocollectException This will be from the data source parser or
     *             the data source adapter
     */
    private boolean validateAndSaveRecords()
        throws ImportConfigurationException, VocollectException {
        int elementsInBatch = 0;
        Object newGuy = null;
        boolean didSave = false;

        objectContext.clear();
        // This call also opens the data source.
        try {
            objectContext = getSourceParser().getElement();
            if ((getSourceParser().isOutOfData())
                && (null == objectContext.getModelObject())) {
                return true;
            }
            
            newGuy = objectContext.getModelObject();
            elementsInBatch = elementCount;
            this.getStats().setDataElements(
                this.getStats().getDataElements() + 1);
            cleanupRan = false;

            didSave = false;
            if (validateRecord(objectContext, PRE_LOOKUP)) { // Uses xwork
                                                                // validator
                didSave = true;
                // Save the record
                this.getStats().commitStart();
                this.saveValidRecord(objectContext);
                this.getStats().commitStop();
            } else {
                // Save the record, with validation results
                this.saveErrorRecord(new ValidationError(
                    validator, objectContext));
            }
        } catch (ImportConfigurationException ice) {
            // Config errors stop the import
            log.error(
                "Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation() + "'",
                ImporterError.NEW_OBJECT_CREATION_FAILURE);
            this.saveErrorRecord(new ExceptionError(this.getXmlrepresentation()
                .getXmlRepresentation(), validator, ice));
            return false;
        } catch (BadDataTransformException e) {
            // At least one record is imported so it is bad data
            log.error(
                "Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation() + "'",
                ImporterError.NEW_OBJECT_CREATION_FAILURE);
            this.saveErrorRecord(new ExceptionError(this.getXmlrepresentation()
                .getXmlRepresentation(), validator, e));
            if (failEntireImport()) {
                throw e;
            } else {
                return false;
            }
        } catch (DataAccessException e) {
            if (didSave) {
                this.getStats().commitStop();
            }
            log.error(
                getDescription() + ": " + "DataAccessException thrown saving "
                    + newGuy.getClass().getName() + ". XML: "
                    + this.getXmlrepresentation().getXmlRepresentation(),
                ImporterError.DATA_ACCESS_PROBLEM, e);

            this.saveErrorRecord(new ExceptionError(newGuy, validator, e));
        } catch (LookupException e) {
            if (didSave) {
                this.getStats().commitStop();
            }
            log.error(
                "Rejecting record " + getStats().getDataElements(),
                ImporterError.LOOKUP_TRIGGER_REPORTS_DO_NOT_SAVE);

            this.saveErrorRecord(new ExceptionError(newGuy, validator, e));
        } catch (BusinessRuleException e) {
            if (didSave) {
                this.getStats().commitStop();
            }
            log.error(
                getDescription() + ": Business rule thrown saving "
                    + newGuy.getClass().getName(),
                ImporterError.BUSINESS_RULE_VIOLATION, e);
            this.saveErrorRecord(new ExceptionError(newGuy, validator, e));
        } catch (BadSiteException e) {
            if (didSave) {
                this.getStats().commitStop();
            }
            // This is configuration exception and should fail the
            // import
            log.fatal("Unable to get the site for site: " + e.getMessage(), e
                .getErrorCode());
            this.saveErrorRecord(new ExceptionError(newGuy, validator, e));
            throw new VocollectException(e.getErrorCode(), new UserMessage(
                LocalizedMessage.formatMessage(e.getErrorCode()), e
                    .getMessage()));
        } catch (PrescanException p) {
            // Already logged - skip
            String localizedLogMsg = p.getUserMessage().getKey();
            this.saveErrorRecord(new PrescanError(localizedLogMsg));
        } catch (OutOfDataException o) {
            // Nothing to do normal completion
            throw o;
        } catch (NoWorkException n) {
            // Nothing to do
            throw n;
        } catch (RecordLengthException r) {
            throw r;
        } catch (VocollectException v) {
            // This is a data error, which equates to a validation
            // problem, we log and fail the element
            // We don't have an object to save. Have to think on how to
            // get something like this into the error file.
            log.error(
                "Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation() + "'",
                ImporterError.NEW_OBJECT_CREATION_FAILURE);
            this.saveErrorRecord(new ExceptionError(newGuy, validator, v));
        } catch (RuntimeException e) {
            log.error(
                "Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation() + "'",
                ImporterError.NEW_OBJECT_CREATION_FAILURE);
            this
                .saveErrorRecord(new ExceptionError(objectContext, validator, e));
            throw e;
        } catch (Exception e) {
            if (didSave) {
                this.getStats().commitStop();
            }
            log.error(
                getDescription() + ": Exception thrown saving "
                    + newGuy.getClass().getName(),
                ImporterError.GENERAL_EXCEPTION, e);
            this.saveErrorRecord(new ExceptionError(newGuy, validator, e));
        }
        return true;
    }

    /**
     * Validate the object formed by the input data.
     * 
     * @param currentObjectContext the object represented by input data, along
     *            with the input data et al.
     * @param preLookup flag to indicate if this validation is being performed
     *            before or after data is looked up in the DB.
     * @return true if the object passed validation, false OW
     */
    // private boolean validateRecord(Object newGuy) {
    private boolean validateRecord(ObjectContext currentObjectContext,
                                   boolean preLookup) {
        String importContext = "Input source "
            + this.getSourceParser().getDescription() + " beginning on line "
            + currentObjectContext.getSourceLineNumber();
        validator.setValidationType(preLookup);
        return this.validator.validate(
            importContext, objectContext, objectContext.getModelObject());

    }

    /**
     * Save this object to the database. If cacheing objects, we'll need to make
     * a new method that traverses a list.
     * 
     * @param newContext the new object to save
     * @throws DataAccessException if no manager exists for the datatype
     * @throws BusinessRuleException from the genericManager
     * @throws LookupException if a lookup trigger fails.
     * @throws VocollectException for a myriad of reasons.
     */
    private void saveValidRecord(ObjectContext newContext)
        throws DataAccessException, BusinessRuleException, LookupException,
        VocollectException {

        CreateObjectTrigger mainObjectTrigger = null;

        boolean objectOKtoSave = true;
        boolean saved = false;
        boolean mainObject = false;

        Object missingLink = null;

        Object newGuy = newContext.getModelObject();

        // for every trigger
        for (CreateObjectTrigger trigger : this.getXmlrepresentation()
            .getNewObjectTriggers()) {

            missingLink = newGuy;
            // get the thing we're looking up from the subject.
            if (trigger.isMainObject()) {
                mainObjectTrigger = trigger;
                mainObject = true;
            } else {
                try {
                    if (trigger.isUseList()) {
                        missingLink = DBLookup.getCompositePart(
                            missingLink, trigger.getList());
                        Collection missingList = (Collection) missingLink;
                        for (Object o : missingList) {
                            // Since these are part of a list, we don't need to
                            // collect the result.
                            trigger.lookupObject(o, objectCompleter);
                        }
                    } else {
                        if (!trigger.isSuppress()) {
                            missingLink = DBLookup.getCompositePart(
                            missingLink, trigger.getGetterName());
                        }                            
                        trigger.lookupObject(missingLink, objectCompleter);
                        
                    }
                } catch (ClassCastException c) {
                    // expected, not all triggers are lookup triggers
                    continue;
                } catch (BadSiteException e) {
                    objectOKtoSave = false;
                    throw e;
                } catch (LookupException le) {
                    this.reportErroredElement(new ExceptionError(
                        newGuy, validator, le));
                    objectOKtoSave = false;
                } catch (VocollectException e) {
                    this.reportErroredElement(new ExceptionError(
                        newGuy, validator, e));
                    objectOKtoSave = false;
                }
            }
        }

        try {
            Object result = mainObjectTrigger.lookupObject(
                newGuy, objectCompleter);
            if (!validateRecord(objectContext, POST_LOOKUP)) { // Uses xwork
                                                                // validator
                // Save the record, with validation results
                this.saveErrorRecord(new ValidationError(
                    validator, objectContext));
                return;
            }

            if (mainObject) {
                if (objectOKtoSave) {
                    list.add(result);
                    if (list.size() >= 0
                        && (getStats().getDataElements() % getBatchSize() == 0)
                        || getSourceParser().isOutOfData()) {
                        bulkSaveUpdate();
                    }
                }
            }
            saved = true;
        } catch (ClassCastException c) {
            // expected, not all triggers are lookup triggers
        } catch (LookupException le) {
            objectOKtoSave = false;
            log.info(getDescription() + ": " + "Unable to complete object "
                + newGuy.getClass().getName());
            throw le;
        } catch (BadSiteException e) {
            objectOKtoSave = false;
            throw e;
        } catch (BusinessRuleException b) {
            // Cannot save tdue to business rules exception
            log.error(
                "Business rule exception thrown saving object "
                    + objectContext.getModelObject().getClass()
                        .getCanonicalName() + " from record "
                    + objectContext.getSourceLineNumber() + ": "
                    + b.getUserMessage().getKey(),
                ImporterError.BUSINESS_RULE_VIOLATION2);
            Object[] o = {
                objectContext.getModelObject().getClass().getCanonicalName(),
                objectContext.getSourceLineNumber() };
            this.saveErrorRecord(new ExceptionError(
                new UserMessage(LocalizedMessage.formatMessage(
                    ImporterError.BUSINESS_RULE_VIOLATION2, o)).getKey(),
                validator, b));
            objectOKtoSave = false;
        } catch (VocollectException e) {
            // Object did not complete properly.
            objectOKtoSave = false;
        } catch (Exception e) {
            // Primarily runtimes - we want to add some identification to the
            // error
            throw new RuntimeException(getDescription() + ": "
                + e.getLocalizedMessage());
        }

        if (!mainObject) {
            if (objectOKtoSave) {
                if (!getPersistenceManager().save(newGuy)) {
                    log.info(getDescription() + ": "
                        + "Unable to find Manager for class "
                        + newGuy.getClass().getName());
                    throw new DataAccessException(
                        ImporterError.NO_SUCH_MANAGER, new UserMessage(
                            LocalizedMessage
                                .formatMessage(ImporterError.NO_SUCH_MANAGER)),
                        new Exception());
                }
            } else {
                if (!saved) {
                    log.info(getDescription() + ": "
                        + "Unable to complete object "
                        + newGuy.getClass().getName());
                    throw new DataAccessException(
                        ImporterError.UNABLE_TO_COMPLETE_OBJECT,
                        new UserMessage(
                            LocalizedMessage
                                .formatMessage(ImporterError.UNABLE_TO_COMPLETE_OBJECT)),
                        new Exception());
                }
            }
        }
    }

    /**
     * Set the list of new-object triggers. used by configuration.
     * 
     * @param triggers list of CreateObjectTrigger that this import uses.
     */
    public void setTriggers(ArrayList<CreateObjectTrigger> triggers) {
        this.getXmlrepresentation().setNewObjectTriggers(triggers);
    }

    /**
     * Get the list of new-object triggers. used by configuration.
     * 
     * @return the list of triggers
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return this.getXmlrepresentation().getNewObjectTriggers();
    }

    /**
     * Add a new trigger to the list of triggers for this importer.
     * @param trigger the trigger to add
     */
    public void addTrigger(CreateObjectTrigger trigger) {
        getXmlrepresentation().addTrigger(trigger);
    }

    /**
     * @return the site name
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the site name.
     * @param siteName the site name
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * This is just one common place to dump all the import startup information
     * which could be useful in tracing bug or getting some import information.
     */
    private void logStartUpInfo() {
        // Log the importer information
        String logMessage = new String(
            "###Importer Info: Site: $SITE - Type: $TYPE - Output Directory: $OUTPUTDIR");

        logMessage = logMessage.replace("$SITE", this.getSiteName()).replace(
            "$TYPE", this.getDatatype());
       
        String parentDir = null;
        String filePattern = null;

        try {
            File f = new File(((FileDataSourceAdapter) this.getSourceParser()
                .getSourceAdapter()).getParentDirectory());
            parentDir = f.getAbsolutePath();
            filePattern = ((FileDataSourceAdapter) this.getSourceParser()
                .getSourceAdapter()).getFileNamePattern();
        } catch (Exception e) {
            logMessage = logMessage.replace(
                "$OUTPUTDIR", "source other than filesystem");
        }

        if (parentDir != null) {
            logMessage = logMessage.replace("$OUTPUTDIR", parentDir);
        }
        if (filePattern != null) {
            logMessage = logMessage.replace("$OUTPUTDIR", filePattern);
        }
        
        log.info(logMessage);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 