/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class handles date field.
 *
 * @author vsubramani
 */
public class DateField extends FixedLengthField {
    private Date resultantDate;

    private String dateFormat = null;

    /**
     * 
     * @return dateFormat
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * set the format of the date string we will accept.
     * @param dateFormat format of the data as we are expecting it.
     */
    public void setDateFormat(String dateFormat) {
        try {
            @SuppressWarnings("unused")  // we are doing this to stimulate an exception if the format os bad.
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
            this.dateFormat = dateFormat;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Format the date string to something Castor will accept.
     * @param stringToConvert the incoming date representation.
     * @return the reformatted date representation.
     */
    public String dateFormat(String stringToConvert) {

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(getDateFormat());
            sdf.setLenient(false);
            resultantDate = sdf.parse(stringToConvert);

            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String stringDate = s.format(resultantDate);

            return stringDate;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
            return "";
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 