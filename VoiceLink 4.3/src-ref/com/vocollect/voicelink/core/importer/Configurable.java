/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.exolab.castor.mapping.Mapping;

/**
 * 
 * @author dgold
 *
 */
public interface Configurable {
    
    /**
     * Abstract configure method that takes a Class object parameter.
     * @param asRoot specifies Class to configure
     * @return the object being configured
     * @throws ConfigurationException if unable to configure
     */
    public abstract Object configure(Class<?> asRoot)
            throws ConfigurationException;

    /**
     * Default abstract configure method.
     * @return the object being configured
     * @throws ConfigurationException if unable to configure
     */
    public abstract Object configure() throws ConfigurationException;

    /**
     * @return Returns the mappingFileName.
     */
    public abstract String getMappingFileName();

    /**
     * @param mappingFileName
     *            The mappingFileName to set.
     */
    public abstract void setMappingFileName(String mappingFileName);

    /**
     * @return Returns the configFileName.
     */
    public abstract String getConfigFileName();

    /**
     * @param configFileName
     *            The configFileName to set.
     */
    public abstract void setConfigFileName(String configFileName);

    /**
     * Just load the mapping. Used particularly by the Importer when loading up
     * the object map
     * @param mappingFile specifies the filename to set
     * @return the Mapping object
     * @throws ConfigurationException if unsuccessful
     */
    public Mapping loadMapping(String mappingFile)
            throws ConfigurationException;

    /**
     * Just load the mapping. Used particularly by the Importer when loading up
     * the object map
     * @return the Mapping object
     * @throws ConfigurationException if unsuccessful
     */
    public Mapping loadMapping() throws ConfigurationException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 