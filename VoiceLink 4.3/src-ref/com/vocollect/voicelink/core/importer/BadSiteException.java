/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;


/**
 * This is a RuntimeException. This will be used
 * when unable to get the site information.
 * @author Kalpna Tinguria
 */
public class BadSiteException extends RuntimeException {

    //Serial vesion id
    private static final long serialVersionUID = 1480481817830938857L;

    /**
     * The error code associated with this exception.
     */
    private ErrorCode         errorCode;

    /**
     * The error message associated with this exception.
     */
    private String         errorMessage;


    /**
     * Constructs an empty instance of <code>BadSiteException</code>.
     */
    public BadSiteException() {
        super();
    }

    /**
     * Constructor. Specified message is used to construct the exception.
     * @param msg message for exception
     */
    public BadSiteException(String msg) {
        super(msg);
    }

    /**
     * Constructor. Specified message is used to construct the exception.
     * @param errorCode the message ErrorCode
     * @param msg message for exception
     */
    public BadSiteException(ErrorCode errorCode, String msg) {
        super();
        setErrorCode(errorCode);
        setErrorMessage(msg);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param msg Message associated with the exception
     * @param t throwable
     */
    public BadSiteException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Setter for the errorCode property.
     * @param newErrorCode - the error code to be set
     */
    public void setErrorCode(ErrorCode newErrorCode) {
        this.errorCode = newErrorCode;
    }

    /**
     * Getter for the errorCode property.
     * @return the code, or null if none set.
     */
    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    /**
     * Setter for the errorMessage property.
     * @param errorMessage - the error message to be set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Getter for the errorMessage property.
     * @return the errorMessage, or null if none set.
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }


    /**
     * This method ensures that the standard <code>RuntimeException.getMessage()</code>
     * will never return <code>null</code>.
     * @see java.lang.Throwable#getMessage()
     * @return the value returned by the super-class, unless it is null, in
     * which case it returns the canonical name of the exception class.
     */
    @Override
    public String getMessage() {
        String out = super.getMessage();
        if (out == null) {
            return this.getClass().getCanonicalName();
        } else {
            return out;
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 