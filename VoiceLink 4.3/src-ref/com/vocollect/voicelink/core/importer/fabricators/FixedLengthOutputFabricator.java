/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.fabricators;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.OutputFabricator;
import com.vocollect.voicelink.core.importer.OutputFabricatorError;
import com.vocollect.voicelink.core.importer.RecordLengthException;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;
import java.util.List;

/**
 * This is a concrete realization of the OutputFabricator. 
 * This creates a String record using only fields from incoming fieldlist
 * those are configured in the export markup and are not supressed.
 * This enables to have mutiple export using the same input Field List.
 * Also write/append the record to the destination by calling the appropriate OutputAdapter
 * method.
 * @author Kalpna
 * 
 */
public class FixedLengthOutputFabricator extends
        OutputFabricator<ExportFixedLengthFieldMap, FixedLengthField> {

    private static final Logger log          = new Logger(
                                                     FixedLengthOutputFabricator.class);

    //Length of a record, as defined by the configuration.
    private int                 recordLength = 0;

    //Number of records created from input fieldMap
    private long                recordsCreated   = 0;
    
    /**
     * Default constructor.
     * @throws ConfigurationException interface constraint allows the constructor to 
     * fail for bad configuration
     */
    public FixedLengthOutputFabricator() throws ConfigurationException {
        super();
    }

    /**
     * This method create the record based on the input fieldMap and either
     * write or append to the destination based on the
     * configuration param isAppendToFile of FileOutputAdapter.
     * @param fixLenFldMap holds the input data field values 
     * @throws VocollectException throws expection if not able to write record
     * @see com.vocollect.voicelink.core.importer.OutputFabricator#createRecord()
     */
    @Override
    public void createRecord(FixedLengthFieldMap fixLenFldMap) throws VocollectException {
        StringBuffer newRecord = new StringBuffer();
        if (!this.getOutputAdapter().isOpen()) {
            this.getOutputAdapter().openDestination();
            setRecordsCreated(0);
        }
        
        //Get the list of field names those are configured for export excluding the supressed fields
        List<String> fieldNameList = new ArrayList<String>();
        for (Field b : this.getFieldList().values()) {
            FixedLengthField a = (FixedLengthField) b;
            //Only use the unsupress fields to create the records
            //Also check for length , if requested field length is
            //0 we need to supress that as well
            if ((!(a.isSuppress())) && (a.getLength() > 0)) {
                fieldNameList.add(a.getFieldName());    
            }
            
        }
                
        // For each field, convert to a fixed-length field and get the 
        //field data to create record
        for (Field y : fixLenFldMap.values()) {
                FixedLengthField x = (FixedLengthField) y;
                if (fieldNameList.contains(x.getFieldName())) {
                    newRecord.append(x.getFieldData());
                }
        }
        
        // Check the defined record length against the actual.
        if (newRecord.length() != this
                .getRecordLength()) {            
            log.error("Record length not consistent with definition. Defined: "
                      + this.getRecordLength() + " Actual: "
                      + newRecord.length(),
                      OutputFabricatorError.WRONG_FIXED_LENGTH_RECORD_LENGTH);
            //Check the datalength for each input field against the markup
            //to find out which field length is not consistent with defition
            for (Field b : this.getFieldList().values()) {
                FixedLengthField a = (FixedLengthField) b;         
                //Only use the unsupress fields as 
                //that's what we used to create the record
                if (!(a.isSuppress())) {                    
                    for (Field y : fixLenFldMap.values()) {
                        FixedLengthField x = (FixedLengthField) y;
                        if (a.getFieldName().equals(x.getFieldName())) {
                            if (a.getLength() != x.getFieldData().length()) {
                                log.error("Field " + a.getFieldName() 
                                    + " length is not consistent with definition. Defined: "
                                    + a.getLength() + " Actual: "
                                    + x.getFieldData().length(), 
                                    OutputFabricatorError.WRONG_FIXED_LENGTH_RECORD_LENGTH);
                             break;   
                            }
                        }
                 }
                }
            }    
            throw new RecordLengthException(
                OutputFabricatorError.WRONG_FIXED_LENGTH_RECORD_LENGTH,
                      new UserMessage(
                           LocalizedMessage
                            .formatMessage(OutputFabricatorError.WRONG_FIXED_LENGTH_RECORD_LENGTH),
                                           this.getRecordLength(), 
                                           newRecord.length()));
            
        }

        //Call outputAdapter method to write the record to the destination
        //If writeRecord return false throw the runtime exception
        if  (!(this.getOutputAdapter().writeRecord(newRecord.toString()))) {            
            this.closeOnFailure();
            log.error("Error in writting the record to the destination. Record is: " 
                                        + newRecord.toString(), OutputFabricatorError.RECORD_WRITE_ERROR);
            throw new RuntimeException("Error in writting the record to the destination. Record is: " 
                                        + newRecord.toString());
        }
        incrementRecordsCreated();
                
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.OutputAdapter#configure()
     * @throws ConfigurationException CastorConfiguration throws the config exception
     */
    @Override
    public boolean configure() throws ConfigurationException {

        if ((null == getMappingFileName())
                || (0 == getMappingFileName().compareTo(""))
                || (null == getConfigFileName())
                || (0 == getConfigFileName().compareTo(""))) {
            return false;
        }

        CastorConfiguration cc = new CastorConfiguration(getMappingFileName(),
                getConfigFileName());

        this.setFieldList((ExportFixedLengthFieldMap) cc.configure());

        // The field list keeps track of the starting position of the (next) field.
        // After all the fields have been configured, the starting position is at the 
        // end-of-record position.
        this.setRecordLength(getFieldList().getStartingPosition());

        return true;
    }
    
    /**
     * Gets the length of the record.
     * @return the value of the record length 
     */
    public int getRecordLength() {
        return recordLength;
    }

    /**
     * Sets the length of the record.
     * @param recordLength length of the record.
     */
    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }
        
    /**
     * Gets the value of recordsCreated.
     * @return the recordsCreated
     */
    @Override
    public long getRecordsCreated() {
        return recordsCreated;
    }

    /**
     * Sets the value of the recordsCreated.
     * @param recordsCreated the recordsCreated to set
     */
    private void setRecordsCreated(long recordsCreated) {
        this.recordsCreated = recordsCreated;
    }

    /**
     * increment the number of records created by the Fabricator.
     */
    private void incrementRecordsCreated() {
        this.recordsCreated += 1;
        return;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 