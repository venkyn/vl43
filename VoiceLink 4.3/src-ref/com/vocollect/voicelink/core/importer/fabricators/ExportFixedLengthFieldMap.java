/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.fabricators;

import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;

/**
 * This class extends the FieldMap and override the addField method.
 * @author Kalpna
 */
public class ExportFixedLengthFieldMap extends FieldMap {

    private static final long serialVersionUID = 7006728084819540835L;

    /**
     * Default empty constructor.
     */
    public ExportFixedLengthFieldMap() {
        super();
    }

    /**
     * Constructor that sets the size of the map.
     * @param size specifies the size to set.
     */
    public ExportFixedLengthFieldMap(int size) {
        super(size);
    }
    
    /**
     * This method add the field also calculate the total
     * length for all the markup fields excluding the suppressed fields.
     * @param aField FixedLengthField object to add
     */
    public void addField(FixedLengthField aField) {
        if (null != aField) {
            aField.setStart(getStartingPosition());
           //In the total length only consider the the
           //unsupressed fields
           if (!(aField.isSuppress())) {
                    setStartingPosition(getStartingPosition()
                    + aField.getLength());
           }
           this.put(aField.getFieldName(), aField);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 