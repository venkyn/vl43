/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Definitions of ErrorCodes related to Triggers.
 * @author dgold
 *
 */
public final class TriggerErrors extends ErrorCode {
    public static final int LOWER_BOUND = 5500;

    public static final int UPPER_BOUND = 5599;

    /** No error, just the base initialization. */
    public static final TriggerErrors NO_ERROR = 
        new TriggerErrors();

    // Configuration lists no fields to write - all you will get will be the open & close tag
    public static final TriggerErrors NO_FIELDS_TO_WRITE = 
        new TriggerErrors(5501);

    // Bad database access doing a lookup
    public static final TriggerErrors BAD_DB_ACCESS_FOR_QUERY_BY_EXAMPLE = 
        new TriggerErrors(5502);

    // Bad database access doing a lookup
    public static final TriggerErrors EXPECTED_TO_FIND_SUBJECT = 
        new TriggerErrors(5503);

    // Bad database access doing a lookup
    public static final TriggerErrors EXPECTED_NOT_TO_FIND_SUBJECT = 
        new TriggerErrors(5504);

    public static final ErrorCode BAD_FIND = 
        new TriggerErrors(5505);

    public static final ErrorCode NO_FINDER_SPECIFIED =
        new TriggerErrors(5506);

    public static final ErrorCode No_KEY_FIELDS =
        new TriggerErrors(5507);
    
    /**
     * Constructor.
     */
    private TriggerErrors() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private TriggerErrors(long err) {
        super(TriggerErrors.NO_ERROR, err);
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 