/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.opensymphony.xwork2.validator.ShortCircuitableValidator;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * The manipulator is intended to manipulate upon fields for any type of
 * specific condition. This is an implementation class of the
 * VocollectCustomManipulator that implements customized manipulating behavior
 * including: isEmptyField() which checks to make sure a field is empty.
 * isNullField() which checks to make sure that an object is null.
 * 
 * @author astein
 */
public abstract class VocollectCustomManipulator extends FieldValidatorSupport
        implements ShortCircuitableValidator {

    private String stringtocheck = null;

    private Object objectocheck  = null;

    private char   defaultChar   = ' ';
    
    // Flag that governs when this manipulator is fired - pre-database-lookup for this one
    private boolean checkPreLookup = true;
    
    // Flag that governs when this manipulator is fired - post-database-lookup for this one
    private boolean checkPostLookup = true;
    
    /**
     * Return true if this manipulator should do its work at the time f this call.
     * @return true if this manipulator should run at present time.
     */
    public boolean shouldRun() {
        if (isPreLookup()) {
            return checkPreLookup;
        } else {
            return checkPostLookup;   
        }
    }

    /**
     * This method enables us to simply see if the field being checked is empty.
     * @param stringToCheck specifies the field being checked
     * @return true if the field is empty, false otherwise
     */
    public boolean isEmptyField(String stringToCheck) {
        this.stringtocheck = stringToCheck;
        if (stringToCheck == null || stringToCheck.trim().equals("")) {
            return true;
        } 
        return false;
    }

    /**
     * This method enables us to simply see if an object being checked is null.
     * @param objecttocheck specifies the object being checked
     * @return true if the object is null, false otherwise
     */
    public boolean isNullField(Object objecttocheck) {
        this.objectocheck = objecttocheck;
        if (objecttocheck == null) {
            return true;
        } 
        return false;
    }

    /**
     * This method helps us to determine if a string should be trimmed.
     * @param stringToCheck specifies the string being checked
     * @param c specifies the character to trim
     * @return true if the string does need trimming, false otherwise
     */
    public boolean needsTrimming(String stringToCheck, char c) {
        this.stringtocheck = stringToCheck;
        setDefaultChar(c);
        if (stringToCheck.charAt(0) == getDefaultChar()) {
            return true;
        } else if (stringToCheck.charAt(stringToCheck.length() - 1) == getDefaultChar()) {
            return true;
        }
        return false;
    }

    /**
     * Setter for defaultChar private char.
     * @param c specifies the char to set
     */
    public void setDefaultChar(char c) {
        this.defaultChar = c;
    }

    /**
     * Getter for the defaultChar private char.
     * @return the char
     */
    public char getDefaultChar() {
        return this.defaultChar;
    }

    /**
     * This method helps us to determine if a string should be trimmed of whitespace.
     * @param stringToCheck specifies the string being checked
     * @return true if the string does need trimming, false otherwise
     */
    public boolean needsTrimming(String stringToCheck) {
        this.stringtocheck = stringToCheck;
        if (stringToCheck.startsWith(" ")) {
            return true;
        } else if (stringToCheck.endsWith(" ")) {
            return true;
        }
        return false;
    }

    /**
     * This method is in case a boolean type was passed into the validate method
     * instead of an object.
     * @param actionObj the value being validated
     * @throws ValidationException if the validation explodes
     */
    public void validate(boolean actionObj) throws ValidationException {
        Boolean field = Boolean.valueOf(actionObj);
        validate(field);
    }

    /**
     * This method is in case a primitive type was passed into the validate
     * method instead of an object.
     * @param actionObj the value being validated
     * @throws ValidationException if the validation doesn't work
     */
    public void validate(double actionObj) throws ValidationException {
        Double field = Double.valueOf(actionObj);
        validate(field);
    }
    
    /**
     * Method to see if this validation is being done before or after database lookup.
     * @return true if this manipulator is being called before database lookup, false OW.
     */
    public boolean isPreLookup() {
        // Get the data to be added to the end of this (which should be a string)
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        return vvC.isPreLookup();
    }

    /**
     * Gets the value of checkPostLookup.
     * @return the checkPostLookup
     */
    public boolean isCheckPostLookup() {
        return this.checkPostLookup;
    }

    /**
     * Sets the value of the checkPostLookup.
     * @param checkPostLookup the checkPostLookup to set
     */
    public void setCheckPostLookup(boolean checkPostLookup) {
        this.checkPostLookup = checkPostLookup;
    }

    /**
     * Gets the value of checkPreLookup.
     * @return the checkPreLookup
     */
    public boolean isCheckPreLookup() {
        return this.checkPreLookup;
    }

    /**
     * Sets the value of the checkPreLookup.
     * @param checkPreLookup the checkPreLookup to set
     */
    public void setCheckPreLookup(boolean checkPreLookup) {
        this.checkPreLookup = checkPreLookup;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 