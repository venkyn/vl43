/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.LocaleProvider;

import java.util.Locale;

/**
 * This class is used to get the locale
 * for import/export.
 *
 * @author Kalpna
 */
public class ImportExportLocaleProvider implements LocaleProvider {
    static final Logger log = new Logger(ImportExportLocaleProvider.class);

   /**
    * Default constructor
    * Constructor.
    */
    public ImportExportLocaleProvider() {
        super();
    }
    
    /**
     * Overrides the com.opensymphony.xwork2.LocaleProvider#getLocale().
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.LocaleProvider#getLocale()
     */
    public Locale getLocale() {
      Locale locale = Locale.getDefault();
      if (locale == null) {
          locale = new Locale("en");
          if (log.isDebugEnabled()) {
              log.debug("the default locale was not found and will be set to en.");
          }
      }
      if (log.isDebugEnabled()) {
          log.debug("The import/export's locale will be set to " + locale.toString());
      }
      return locale;
    }
  
}
    

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 