/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.EPPAbstractQuartzJob;

import java.util.LinkedList;
import java.util.List;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

/**
 * ImportManager class that is executed by the Scheduler and runs all of the
 * importer objects.
 * 
 * @author astein
 */
public class ImportManager extends EPPAbstractQuartzJob implements Describable {

    // --------------------------------Variable
    // declarations--------------------------//
    private Description description = new Description();

    // Plain-language description of this instance - composite from importers
    // assigned.

    private List<? extends DataTransferBase> baseDataType = new LinkedList<DataTransferBase>();

    // List of Importers. This is set from the configuration

    private static final Logger log = new Logger(ImportManager.class);

    // create the logger object

    private String status = null;

    // status of the import

    private String configFileName = null;

    // Name of the configuration file

    private String mappingFileName = null;

    // Name of the mapping file. (This holds the mapping of objects this can use
    // to the tagging )

    // List of states this can assume
    public static final InternalState INITIALIZEDSTATE = new InternalState(
        0, "ImportManager.Initialized");

    public static final InternalState CONFIGUREDSTATE = new InternalState(
        1, "ImportManager.Configured");

    public static final InternalState MISCONFIGUREDSTATE = new InternalState(
        2, "ImportManager.MisConfigured", InternalState.FinalStateType.Failed);

    public static final InternalState STARTEDSTATE = new InternalState(
        3, "ImportManager.Started");

    public static final InternalState RUNNINGSTATE = new InternalState(
        4, "ImportManager.Running");

    public static final InternalState STOPPINGSTATE = new InternalState(
        5, "ImportManager.Stopping");

    public static final InternalState STOPPEDSTATE = new InternalState(
        6, "ImportManager.Stopped", InternalState.FinalStateType.Imperfect);

    public static final InternalState STOPPEDIMPERFECTSTATE = new InternalState(
        7, "ImportManager.StoppedIMperfect",
        InternalState.FinalStateType.Imperfect);

    public static final InternalState NOTRUNSTATE = new InternalState(
        8, "ImportManager.NotRun", InternalState.FinalStateType.NoWork);

    public static final InternalState SUCCESSFULSTATE = new InternalState(
        9, "ImportManager.Successful", InternalState.FinalStateType.Success);

    public static final InternalState IMPERFECTSTATE = new InternalState(
        10, "ImportManager.Imperfect", InternalState.FinalStateType.Imperfect);

    public static final InternalState FAILEDSTATE = new InternalState(
        11, "ImportManager.Failed", InternalState.FinalStateType.Failed);

    public static final InternalState NOWORKSTATE = new InternalState(
        12, "ImportManager.NoWork", InternalState.FinalStateType.NoWork);

    public static final InternalState STOPPEDNOWORKSTATE = new InternalState(
        13, "ImportManager.StoppedNoWork", InternalState.FinalStateType.NoWork);

    public static final InternalState RUNNINGFAILEDSTATE = new InternalState(
        14, "ImportManager.RunningWithFailures");

    public static final InternalState RUNNINGIMPERFECTSTATE = new InternalState(
        15, "ImportManager.RunningImperfect");

    public static final InternalState RUNNINGNOWORKSTATE = new InternalState(
        16, "ImportManager.RunningNoWork");

    public static final InternalState PARTIALSUCCESS = new InternalState(
        17, "ImportManager.PartialSuccess",
        InternalState.FinalStateType.Imperfect);

    public static final InternalState STOPPINGPARTIALSUCCESS = new InternalState(
        18, "ImportManager.StoppingPartialSuccess");

    public static final InternalState STOPPINGIMPERFECTSTATE = new InternalState(
        19, "ImportManager.StoppingImperfectState");

    public static final InternalState STOPPINGNOWORKSTATE = new InternalState(
        20, "ImportManager.StoppingNoWorkState");

    public static final InternalState STOPPEDPARTIALSUCCESS = new InternalState(
        21, "ImportManager.StoppedPartialSucces",
        InternalState.FinalStateType.Imperfect);

    private final String defaultDescription = "Importer.defaultDescription";

    // Root of the description. This will generally be
    // "Importer providing imports for the following datatypes:\n"

    private static String separator = "";

    // Separator character for the description. This is "\n" by default.

    private static final int INITIAL_STATE_CHANGE_COUNT = 50;

    private StateChanges stateChanges = new StateChanges(
        INITIAL_STATE_CHANGE_COUNT);

    // The key for the mapping file in the Job map
    private static final String MAPPING_FILE_KEY = "mappingfile";

    // The key for the config file in the Job map
    private static final String CONFIG_FILE_KEY = "configfile";

    // List of legal state changes

    private boolean allJobsFailed = true;

    // Flag showing that every job in the Import failed.

    private boolean isRunningNoWork = true;

    // Flag showing that despite the fact that the ImportManager is running,
    // there is currently no work to do.

    private boolean criticalFailure = false;

    // Flag indicating that one of the failed Imports was a critical step

    // ---------------------- End of variable declararions.
    // ----------------------------------//

    /**
     * Default constructor. Calls init()
     */
    public ImportManager() {
        super();
        init();
    }

    /**
     * Configuring constructor. Calls init(), then configure().
     * 
     * @param mappingFileNameP name of file contianing object-toxml mapping
     * @param configFileNameP name of file containing the xml describing the
     *            makeup of this Import For the ImportManager object, the
     *            following describes each state: Initialized = first state set
     *            after init method is run Configured = set upon successful
     *            configuration MisConfigured = set after problem with
     *            configuration STARTED_STATE = set after successful
     *            configuration and prior to starting import process
     *            RUNNING_STATE = set after each importer object is set to
     *            pending STOPPING_STATE = set if an interrupt was detected
     *            NoWorkState = set if there are no importer objects to run
     *            SUCCESSFUL_STATE = set after successful import process
     *            ImperfectState = set if errors were detected but import
     *            completed FAILED_STATE = set if errors stopped import process
     *            from completing and no successful imports occurred
     *            PartialSuccess = set if errors stopped import process from
     *            completing but with at least one successful import object
     *            finished
     */
    public ImportManager(String mappingFileNameP, String configFileNameP) {
        super();
        init();
        setMappingFileName(mappingFileNameP);
        setConfigFileName(configFileNameP);
    }

    /**
     * Set up the legal states. Set defaults. At the end, state = Initialized,
     * critical = false.
     */
    private void init() {
        // Add the allowed state changes to the state changes list
        // If you extend this list, add to the initial list size
        this.stateChanges.add(INITIALIZEDSTATE, CONFIGUREDSTATE);
        this.stateChanges.add(INITIALIZEDSTATE, MISCONFIGUREDSTATE);

        this.stateChanges.add(CONFIGUREDSTATE, STARTEDSTATE);

        this.stateChanges.add(STARTEDSTATE, RUNNINGSTATE);
        this.stateChanges.add(STARTEDSTATE, STOPPINGSTATE);

        this.stateChanges.add(RUNNINGSTATE, STOPPINGSTATE);
        this.stateChanges.add(RUNNINGSTATE, NOWORKSTATE);
        this.stateChanges.add(RUNNINGSTATE, SUCCESSFULSTATE);
        this.stateChanges.add(RUNNINGSTATE, IMPERFECTSTATE);
        this.stateChanges.add(RUNNINGSTATE, FAILEDSTATE);
        this.stateChanges.add(RUNNINGSTATE, RUNNINGFAILEDSTATE);
        this.stateChanges.add(RUNNINGSTATE, RUNNINGIMPERFECTSTATE);
        this.stateChanges.add(RUNNINGSTATE, RUNNINGNOWORKSTATE);

        this.stateChanges.add(RUNNINGFAILEDSTATE, FAILEDSTATE);
        this.stateChanges.add(RUNNINGFAILEDSTATE, PARTIALSUCCESS);
        this.stateChanges.add(RUNNINGFAILEDSTATE, STOPPINGPARTIALSUCCESS);

        this.stateChanges.add(RUNNINGIMPERFECTSTATE, RUNNINGFAILEDSTATE);
        this.stateChanges.add(RUNNINGIMPERFECTSTATE, IMPERFECTSTATE);
        this.stateChanges.add(RUNNINGIMPERFECTSTATE, STOPPINGIMPERFECTSTATE);

        this.stateChanges.add(RUNNINGNOWORKSTATE, RUNNINGIMPERFECTSTATE);
        this.stateChanges.add(RUNNINGNOWORKSTATE, RUNNINGFAILEDSTATE);
        this.stateChanges.add(RUNNINGNOWORKSTATE, NOWORKSTATE);
        this.stateChanges.add(RUNNINGNOWORKSTATE, STOPPINGNOWORKSTATE);

        this.stateChanges.add(STOPPINGSTATE, STOPPEDSTATE);
        this.stateChanges.add(STOPPINGSTATE, STOPPINGIMPERFECTSTATE);
        this.stateChanges.add(STOPPINGSTATE, STOPPINGNOWORKSTATE);
        this.stateChanges.add(STOPPINGPARTIALSUCCESS, STOPPEDPARTIALSUCCESS);
        this.stateChanges.add(STOPPINGIMPERFECTSTATE, STOPPEDIMPERFECTSTATE);
        this.stateChanges.add(STOPPINGNOWORKSTATE, STOPPEDNOWORKSTATE);

        // Final transitions -- these won't be used until we make this
        // ImportManager a daemon object
        this.stateChanges.add(STOPPEDSTATE, STARTEDSTATE);
        this.stateChanges.add(STOPPEDIMPERFECTSTATE, STARTEDSTATE);
        this.stateChanges.add(NOTRUNSTATE, STARTEDSTATE);
        this.stateChanges.add(NOWORKSTATE, STARTEDSTATE);
        this.stateChanges.add(STOPPEDNOWORKSTATE, STARTEDSTATE);
        this.stateChanges.add(STOPPEDPARTIALSUCCESS, STARTEDSTATE);
        this.stateChanges.add(SUCCESSFULSTATE, STARTEDSTATE);
        this.stateChanges.add(IMPERFECTSTATE, STARTEDSTATE);
        this.stateChanges.add(FAILEDSTATE, STARTEDSTATE);

        setCurrentState(INITIALIZEDSTATE);

        setCriticalFailure(false);

        setDescription(this.defaultDescription);

        clearStatus();
        setStatus("Initialized");
    }

    /**
     * Get the string describing the status of this instance.
     * 
     * @return string describing the status of this instance
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Set the string describing the status of this instance to "".
     */
    public void clearStatus() {
        this.status = "";
    }

    /**
     * Set the status of this instance to concatenation of the current status
     * string and the passed string.
     * 
     * @param status is the status to change to.
     */
    public void setStatus(String status) {
        this.status = this.status + status;
    }

    /**
     * Get the current state object for this instance.
     * 
     * @return current state object
     */
    public InternalState getCurrentState() {
        return this.stateChanges.getCurrentState();
    }

    /**
     * Set the current state appropriately, based on the state of the importer.
     * This gets called after each Importer is done executing. This will set the
     * variables criticalFailure, allJobsFailed as well as the current state
     * 
     * @param importer passes in the importer object.
     */
    public void setState(DataTransferBase importer) {
        InternalState importerState = importer.getCurrentState();
        if (importerState.isFailedState()) {
            setCurrentState(RUNNINGFAILEDSTATE);
            if (importer.isCritical()) {
                setCurrentState(FAILEDSTATE);
                setCriticalFailure(true);
            }
            return;
        }

        setAllJobsFailed(false);

        if (importerState.isSuccessState()) {
            return; // just leave this as is ...
        }
        if (importerState.isImperfectState()) {
            setCurrentState(RUNNINGIMPERFECTSTATE);
            return;
        }
        if (importerState.isNoWorkState()) {
            setCurrentState(RUNNINGNOWORKSTATE);
            return;
        }
    }

    /**
     * Manage the state of the ImportManager and its Importers. This will call
     * Start on all Importers and set the current state.
     */
    public synchronized void start() {
        if (this.stateChanges.getCurrentState() == STARTEDSTATE) {
            for (DataTransferBase baseType : getBaseDataType()) {
                // This is failing because the configure() method of the
                // importer has not been called.
                baseType.prepare();
                // Set all of the importers to Pending
            }
            setCurrentState(RUNNINGSTATE);
        }
    }

    /**
     * Runs the imports for the Importer objects.
     * 
     * @throws UnableToInterruptJobException passes the exception back to the
     *             executeInternal method.
     */
    public void runImports() throws UnableToInterruptJobException {
        if (this.stateChanges.getCurrentState() == RUNNINGSTATE) {
            this.determineRunningNoWork();
            for (DataTransferBase baseType : getBaseDataType()) {

                // Set this importer to Started
                baseType.start();

                // This immediately sets the importer's state to Running...
                baseType.runDatatypeJob();

                // Harvest the importer's state, and use it to figure out our
                // own.
                setState(baseType);

                if (this.isCriticalFailure()) {
                    this.stop();
                    log
                        .error(
                            "Failed ImportManager -- Critical Failure in ImportManager object",
                            ImportManagerError.FAILEDIMPORTMANAGER);
                    throw new RuntimeException("Critical step"
                        + baseType.getDescription() + "failed.");
                }
            }
            if (this.isAllJobsFailed()) {
                setStatus("... Finished But Failed");
                this.stateChanges.setCurrentState(ImportManager.FAILEDSTATE);
                setAllJobsFailed(true);
                log.error(
                    "Failed ImportManager -- All jobs failed",
                    ImportManagerError.FAILEDIMPORTMANAGER);
                throw new RuntimeException("All jobs failed.");
            }
            this.determineSuccess();
        }

    }

    /**
     * Sets the ImportManager object to the appropriate "end" state.
     */
    public void determineSuccess() {
        InternalState is = this.getCurrentState();
        if (is == RUNNINGSTATE) {
            setStatus("... Finished");
            setCurrentState(SUCCESSFULSTATE);
        } else if (is == RUNNINGIMPERFECTSTATE) {
            setCurrentState(IMPERFECTSTATE);
            log.warn(
                "Imperfect ImportManager -- Found errors in importing process",
                ImportManagerError.IMPERFECTIMPORTMANAGER);
        } else if (is == RUNNINGFAILEDSTATE) {
            setCurrentState(PARTIALSUCCESS);
            log
                .warn(
                    "PartialSuccess ImportManager -- importing process failed, some importers succeeded",
                    ImportManagerError.PARTIALSUCCESSIMPORTMANAGER);
        } else if (is == RUNNINGNOWORKSTATE) {
            setCurrentState(NOWORKSTATE);
        }
    }

    /**
     * Sets whether or not the ImportManager should be at a RunningNoWorkState.
     */
    public void determineRunningNoWork() {
        if (getBaseDataType() == null) {
            setCurrentState(RUNNINGNOWORKSTATE);
            setRunningNoWork(true);
        } else {
            setRunningNoWork(false);
        }
    }

    /**
     * Set the current state of this instance, then call stop on each of the
     * importers. Each IMporter will detect the change and stop, then the
     * execute() loop will stop.
     */
    public synchronized void stop() {
        if (this.getCurrentState() == RUNNINGSTATE
            || this.getCurrentState() == STARTEDSTATE) {
            setCurrentState(STOPPINGSTATE);
        } else if (this.getCurrentState() == RUNNINGIMPERFECTSTATE) {
            setCurrentState(STOPPINGIMPERFECTSTATE);
        } else if (this.getCurrentState() == RUNNINGNOWORKSTATE) {
            setCurrentState(STOPPINGNOWORKSTATE);
        } else if (this.getCurrentState() == RUNNINGFAILEDSTATE
            && !this.isAllJobsFailed()) {
            setCurrentState(STOPPINGPARTIALSUCCESS);
        }
        for (DataTransferBase baseType : getBaseDataType()) {
            baseType.stop();
            // baseType.finish();
            if (baseType.getCurrentState() == DataTransferBase.IMPERFECT_STATE) {
                setCurrentState(STOPPINGIMPERFECTSTATE);
            } else if (baseType.getCurrentState() == DataTransferBase.NO_WORK_STATE) {
                setCurrentState(STOPPINGNOWORKSTATE);
            }
        }
        if (this.getCurrentState() == STOPPINGSTATE) {
            setCurrentState(STOPPEDSTATE);
        } else if (this.getCurrentState() == STOPPINGIMPERFECTSTATE) {
            setCurrentState(STOPPEDIMPERFECTSTATE);
        } else if (this.getCurrentState() == STOPPINGNOWORKSTATE) {
            setCurrentState(STOPPEDNOWORKSTATE);
        } else if (this.getCurrentState() == STOPPINGPARTIALSUCCESS) {
            setCurrentState(STOPPEDPARTIALSUCCESS);
        }
    }

    /**
     * Sets the state for the ImportManager and writes a message to the log.
     * 
     * @param is InternalState object to set the current state to
     */
    public void setCurrentState(InternalState is) {
        if (this.stateChanges.setCurrentState(is)) {
            log.debug("ImportManager now at " + is.getDescription());
        }
    }

    /**
     * Run the configuration.
     * 
     * @throws VocollectException in case there is a problem with the
     *             configuraiton
     */
    @SuppressWarnings("unchecked")
    public void configure() throws VocollectException {
        try {
            if (getMappingFileName() == null) {
                setCurrentState(MISCONFIGUREDSTATE);
                log
                    .fatal(
                        "ImportManager configuration not correct--missing Mapping file",
                        ImportManagerError.MISSINGMAPPINGFILE);
                throw new VocollectException(
                    ImportManagerError.MISSINGMAPPINGFILE,
                    new UserMessage(
                        LocalizedMessage
                            .formatMessage(ImportManagerError.MISSINGMAPPINGFILE),
                        "Problems with ImportManager Configuration"));
            }
            if (getConfigFileName() == null) {
                setCurrentState(MISCONFIGUREDSTATE);
                log
                    .fatal(
                        "ImportManager configuration not correct--missing Config file",
                        ImportManagerError.MISSINGCONFIGFILE);
                throw new VocollectException(
                    ImportManagerError.MISSINGCONFIGFILE,
                    new UserMessage(
                        LocalizedMessage
                            .formatMessage(ImportManagerError.MISSINGCONFIGFILE),
                        "Problems with ImportManager Configuration"));
            }
            CastorConfiguration cc = new CastorConfiguration(
                getMappingFileName(), getConfigFileName());
            setBaseDataType((LinkedList<? extends DataTransferBase>) cc
                .configure(LinkedList.class));
            setCurrentState(CONFIGUREDSTATE);
        } catch (VocollectException ve) {
            setCurrentState(MISCONFIGUREDSTATE);
            log
                .fatal(
                    "ImportManager configuration not correct--check Mapping and Config files ",
                    ImportManagerError.BADCONFIGURATION, ve);
            throw ve;
        }
    }

    /**
     * Get the list of DataTransferBase for importers/exporters. Required for
     * configuration.
     * 
     * @return the list of Importers/Exporters this IMportManager owns.
     */
    public List<? extends DataTransferBase> getBaseDataType() {
        return this.baseDataType;
    }

    /**
     * Set the list of importers. This is used by the configuration
     * 
     * @param baseDataType the list of importers/exporters this instance is to
     *            manage
     */
    public void setBaseDataType(List<? extends DataTransferBase> baseDataType) {
        this.baseDataType = baseDataType;
    }

    /**
     * Get the value of the flag that indicates that all the importers failed.
     * 
     * @return Returns the allJobsFailed.
     */
    public boolean isAllJobsFailed() {
        return this.allJobsFailed;
    }

    /**
     * Set the value of the flag that indicates that all the importers failed.
     * 
     * @param allJobsFailed The allJobsFailed to set.
     */
    public void setAllJobsFailed(boolean allJobsFailed) {
        if (allJobsFailed) {
            setCurrentState(FAILEDSTATE);
        }
        this.allJobsFailed = allJobsFailed;
    }

    /**
     * Get the value of the flag that indicates that a critical step failed.
     * 
     * @return Returns the criticalFailure.
     */
    public boolean isCriticalFailure() {
        return this.criticalFailure;
    }

    /**
     * Set the value of the flag that indicates that a critical step failed.
     * 
     * @param criticalFailure The criticalFailure to set.
     */
    public void setCriticalFailure(boolean criticalFailure) {
        this.criticalFailure = criticalFailure;
    }

    /**
     * Get the description of this instance. Thie will be a composite, made up
     * of the descriptions of the Importers. Ex: "Importer providing imports for
     * the following datatypes: Items Import, Locations Import, Assignments
     * Import
     * 
     * @return the description string
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * Set the description of this instance. Appends the string to the current
     * description. Set the separator character to "\n" if a non-(null/blank)
     * string is passed.
     * 
     * @param additionalDescription is more description to append to the
     *            description.
     */
    public void setDescription(String additionalDescription) {
        if ((null == additionalDescription)
            || (0 == "".compareTo(additionalDescription))) {
            return;
        }
        String temp = getDescription();
        this.description.setDescription(temp + separator
            + additionalDescription);
        separator = "\n";
    }

    /**
     * Get the name of the file holding the configuration of this Import.
     * @return Returns the configFileName.
     */
    public String getConfigFileName() {
        return this.configFileName;
    }

    /**
     * Set the name of the file holding the configuration of this Import.
     * @param configFileName The configFileName to set.
     */
    public void setConfigFileName(String configFileName) {
        this.configFileName = configFileName;
    }

    /**
     * Get the name of the file holding the object mapping of this Import.
     * @return Returns the mappingFileName.
     */
    public String getMappingFileName() {
        return this.mappingFileName;
    }

    /**
     * Set the name of the file holding the object mapping of this Import.
     * @param mappingFileName The mappingFileName to set.
     */
    public void setMappingFileName(String mappingFileName) {
        this.mappingFileName = mappingFileName;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalInterrupt()
     */
    protected void internalInterrupt() {
        log.fatal(
            " ImportManager -- Interrupted ",
            ImportManagerError.INTERRUPTIBLEIMPORTMANAGER);
        stop();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalExecute(org.quartz.JobExecutionContext)
     */
    protected void internalExecute(JobExecutionContext jobExecutionContext)
        throws JobExecutionException {
        
        if (log.isDebugEnabled()) {
            // make sure state is initialized
            log.debug("Executing the import manager scheduler to look for scheduled imports...");
        }        
        if (getCurrentState() == INITIALIZEDSTATE) {
            JobDetail jobDetail = jobExecutionContext.getJobDetail();
            JobDataMap jobDataMap = jobDetail.getJobDataMap();
            if (jobDataMap.containsKey(CONFIG_FILE_KEY)) {
                setConfigFileName(jobDataMap.get(CONFIG_FILE_KEY).toString());
            }
            if (jobDataMap.containsKey(MAPPING_FILE_KEY)) {
                setMappingFileName(jobDataMap.get(MAPPING_FILE_KEY)
                    .toString());
            }
            try {
                configure();
                prepare();
                start();
                runImports();
            } catch (UnableToInterruptJobException e) {
                log.fatal(
                    "ImportManager -- Tried to interrupt job without success: "
                        + e.getMessage(),
                    ImportManagerError.UNINTERRUPTIBLEIMPORTMANAGER, e);
                stop();
            } catch (VocollectException e1) {
                log.fatal(
                    "ImportManager -- problem with execution: "
                        + e1.getMessage(),
                    ImportManagerError.FAILEDIMPORTMANAGER, e1);
                stop();
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Finished executing the import manager scheduler to look for scheduled imports...");
        }        
    }

    /**
     * Set the ImportManager object to a started state if in a configuredstate.
     */
    public void prepare() {
        if (getCurrentState() == CONFIGUREDSTATE) {
            setCurrentState(STARTEDSTATE);
        }
    }

    /**
     * Getter for the isRunningNoWork property.
     * @return boolean value of the property
     */
    public boolean isRunningNoWork() {
        return isRunningNoWork;
    }

    /**
     * Setter for the isRunningNoWork property.
     * @param runningNoWork the new isRunningNoWork value
     */
    public void setRunningNoWork(boolean runningNoWork) {
        this.isRunningNoWork = runningNoWork;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 