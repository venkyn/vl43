/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.CharBuffer;

/**
 * This class is the interface to a source of data, be it file, FTP socket or other.
 * It allows the caller to manipulate the data source as a buffered set of characters.
 * Note that this is a character-oriented interface. We don't track bytes. 
 * We really can't, at this level. If we want to do that, we need access to much
 * lower-level functions of the stream
 * 
 * @author dgold
 *
 */
public abstract class DataSourceAdapter implements Describable {
    
    private static final Logger log                     = new Logger(
        DataSourceAdapter.class);
    
    private Description               description            = new Description();

    private BufferedReader            internalDataSource;

    // Most data sources will use buffered readers, so we will have a common
    // reader here

    // States this object may assume. This will generally be in the Configured
    // state, but may occasionally
    // enter the DataSourceNotReady state.

    /**
     * Static member object set at an InternalState of "Uninitialized".
     */
    public static final InternalState UNINITIALIZEDSTATE     = new InternalState(
                                                                     0,
                                                                     "DataSourceAdapter.UnInitialized");

    // Before constructor state

    /**
     * Static member object set at an InternalState of "Initialized".
     */
    public static final InternalState INITIALIZEDSTATE       = new InternalState(
                                                                     1,
                                                                     "DataSourceAdapter.Initialized");

    // After constructor state

    /**
     * Static member object set at an InternalState of "configured".
     */
    public static final InternalState CONFIGUREDSTATE        = new InternalState(
                                                                     2,
                                                                     "DataSourceAdapter.Configured");

    // After successful configuration

    /**
     * Static member object set at an InternalState of "Misconfigured".
     */
    public static final InternalState MISCONFIGUREDSTATE     = new InternalState(
                                                                     3,
                                                                     "DataSourceAdapter.MisConfigured");

    // After unsuccessful configuration

    /**
     * Static member object set at an InternalState of "source not ready".
     */
    public static final InternalState DATASOURCENOTREADY     = new InternalState(
                                                                     4,
                                                                     "DataSourceAdapter.SourceNotReady");

    // After unsuccessful open or other IO error

    /**
     * Static member object set at an InternalState of "nowork".
     */
    public static final InternalState NOWORK                 = new InternalState(
                                                                     5,
                                                                     "DataSourceAdapter.NoWork");
    
    // After unsuccessful open or other IO error

    private InternalState             currentState           = UNINITIALIZEDSTATE;

    // Current state...

    private boolean                   isOpen                 = false;

    // Is the data source open for reading?

    private long                      charsRead              = 0;

    // Number of characters read from the buffer

    private long                      currentRecordLength    = 0;

    // Number of characters read into the buffer

    private long                      currentLineNumber      = 0;

    // Number of the 'current' line, a one-indexed number

    private String                    configFileName;

    // Name of configuration file.

    private int                       bufferedCharsRemaining = 0;

    // Number of characters remaining in the buffer

    private String                    dataSourceName;

    // Name of the data source. For a file, this is the full filename

    
    private boolean                   presort;
    
    //Encoding type used for input file
    private String                   encoding;
 
    //This is an array that is used when the read order needs to be affected by
    // an index.  If not used, this will default to null.
    // recordsInProperOrder[0][x] - starting character offset for each record in order.
    // recordsInProperOrder[1][x] - length for each record in order.
    private Long[][] recordsInProperOrder = null;

    
    /**
     * Default constructor. Set all vars to null or zero. Descendent needs to
     * manage state.
     * @throws VocollectException if initiated incorrectly.
     */
    @SuppressWarnings("unused")
    public DataSourceAdapter() throws VocollectException {
        super();
        this.setInternalDataSource(null);
        this.setCharsRead(0);
        this.setConfigFileName(null);
        this.setCurrentRecordLength(0);
        setOpen(false);
        this.setBufferedCharsRemaining(0);
        this.setDataSourceName(null);
        this.setPresort(false);
        return;
    }

    // --------------------------------Abstract
    // Methods----------------------------------------//
    /**
     * Check - is this the end of the record? This is only applicable to those
     * data sources that have a simple (CR/LF style) record separator This won't
     * really have any meaning for XML tagged data - the DataSourceParser
     * handles those types of definitions
     * 
     * @return true if all of the data for a record has been read from (this)
     */
    public abstract boolean isEndOfRecord();

    /**
     * Check - is this the end of the data? This equates to EOF or a disconnect.
     * For non-terminating data sources, this is a disconnect, not the condition
     * of no more data is on the connection.
     * 
     * @return True if there is no more data available frm the source.
     */
    public abstract boolean isEndOfData();

    /**
     * Open the current data source for reading. This may involve closing the
     * 'current' source.
     * @return true if successful.
     * @throws VocollectException if unsuccessful.
     */
    public abstract boolean openSource() throws VocollectException;

    /**
     * Read a new record, typically equates to readLine, but may not. If a
     * hierarchical datatype, read the next outer-level record.
     * 
     * @return true, unless the end of the data has been reached
     */
    public abstract boolean readRecord();

    /**
     * Close with a successful state. This, for the current system, involves
     * moving the file to a successfully-completed directory.
     * 
     * @return true if this operation is successful.
     */
    public abstract boolean closeOnSuccess();

    /**
     * Close with a failed state. This, for the current system, involves moving
     * the file to an unsuccessfully-completed directory.
     * 
     * @return true if this operation is successful.
     */
    public abstract boolean closeOnFailure();

    /**
     * Get the current offset into the data source. This will really be
     * meaningful for files, but may apply to others. This is intended to be
     * used for indexing purposes.
     */
    // --------------------------------End Abstract
    // Methods----------------------------------------//
    // -------------------------------Actions----------------------------------------------------//
    
    
    /**
     * Closes the internalDataSource.
     * 
     * @throws IOException if unsuccessful
     */
    public void close() throws IOException {
        if (this.internalDataSource != null) {
            this.internalDataSource.close();
            setOpen(false);
        }
    }

    // ---------------------------------------Getters and
    // Setters---------------------------------//
    /**
     * Get the current state...
     * 
     * @return the InternalState object
     */
    public InternalState getCurrentState() {
        return this.currentState;
    }

    /**
     * Sets the current state.
     * 
     * @param currentState
     *            to set.
     */
    protected void setCurrentState(InternalState currentState) {
        this.currentState = currentState;
    }

    /**
     * @return the InternalDataSource as a BufferedReader object.
     */
    protected BufferedReader getInternalDataSource() {
        return this.internalDataSource;
    }

    /**
     * Set the internal data source to the given buffered reader.
     * 
     * @param internalDataSource specifies the BufferedReader object
     */
    public void setInternalDataSource(BufferedReader internalDataSource) {
        this.internalDataSource = internalDataSource;
    }

    /**
     * Check - is the data source open for reading?
     * 
     * @return true if it is open
     */
    public boolean isOpen() {
        return this.isOpen;
    }

    /**
     * @param isopen to set the data source open
     */
    protected void setOpen(boolean isopen) {
        this.isOpen = isopen;
    }

    /**
     * Get the number of characters read from the buffer.
     * 
     * @return the number of characters read
     */
    public long getCharsRead() {
        return this.charsRead;
    }

    /**
     * Sets the charsread value.
     * 
     * @param charsRead the characters to be read
     */
    protected void setCharsRead(long charsRead) {
        this.charsRead = charsRead;
    }

    /**
     * Get the number of characters read from the source.
     * 
     * @return the number of characters read for the current record in the
     *         buffer
     */
    public long getCurrentRecordLength() {
        return this.currentRecordLength;
    }

    /**
     * Sets the currentrecordlength value.
     * 
     * @param currentRecordLength to be set
     */
    protected void setCurrentRecordLength(long currentRecordLength) {
        this.currentRecordLength = currentRecordLength;
    }

    /**
     * Get the configFileName file name.
     * 
     * @return name of configFileName file
     */
    public String getConfigFileName() {
        return this.configFileName;
    }

    /**
     * Set the configFileName file name - for use with XML configuration.
     * 
     * @param config name of configFileName file.
     */
    public void setConfigFileName(String config) {
        this.configFileName = config;
    }

    /**
     * Get the number of characters remaining in the buffer.
     * 
     * @return the number of chars remaining
     */
    public int getBufferedCharsRemaining() {
        return this.bufferedCharsRemaining;
    }

    /**
     * Sets the number of buffered chars remaining.
     * 
     * @param sourceCharsRemaining the number to set
     */
    protected void setBufferedCharsRemaining(int sourceCharsRemaining) {
        this.bufferedCharsRemaining = sourceCharsRemaining;
    }

    /**
     *
     */
    protected void useSourceChar() {
        if (0 < this.bufferedCharsRemaining) {
            this.bufferedCharsRemaining--;
        }
    }

    /**
     * @return Returns the dataSourceName.
     */
    public String getDataSourceName() {
        return this.dataSourceName;
    }

    /**
     * @param dataSourceName
     *            The dataSourceName to set.
     */
    protected void setDataSourceName(String dataSourceName) {
        this.dataSourceName = dataSourceName;
    }

    
    
    /**
     * Get the array of Long that is used to store the start and length
     * of each record in sorted order.
 
     * @return the recordsInProperOrder - the two-dimensional long array that
     * stores in sorted (indexed) record order the start and length of each record.
     */
    public Long[][] getRecordsInProperOrder() {
        return recordsInProperOrder;
    }

    
    /**
     * Set the array of Long that is used to store the start and length
     * of each record in sorted order.
     * @param recordsInProperOrder the two-dimensional long array that stores in
     * sorted (indexed) record order the start and length of each record.
     */
    public void setRecordsInProperOrder(Long[][] recordsInProperOrder) {
        this.recordsInProperOrder = recordsInProperOrder;
    }

    
    /**
     * Set the array of Long that is used to store the start and length
     * of each record in sorted order to null.
     * This should be done before a prescan to ensure original file will
     * be read.
     */
    public void resetRecordsInProperOrder() {
        this.recordsInProperOrder = null;
    }

    
    
    
    // --------------------------Delegated methods------------------//

    
 
    /**
     * Sets the value to readahead for the BufferedReader object.
     * @param readAheadLimit the value to set
     * @throws IOException if unsuccessful
     */
    public void mark(int readAheadLimit) throws IOException {
        this.internalDataSource.mark(readAheadLimit);
    }

    /**
     * Returns true if the mark operation of the BufferedReader is supported.
     * 
     * @return true if supported
     */
    public boolean markSupported() {
        return this.internalDataSource.markSupported();
    }

    /**
     * @return the int to read
     * @throws IOException if unsuccessful
     */
    public int read() throws IOException {
        return this.internalDataSource.read();
    }

    /**
     * BufferedReader read method.
     * 
     * @param cbuf the char array to read
     * @param off the offset
     * @param len the length
     * @return the number of characters read
     * @throws IOException if unsuccessful read
     */
    public int read(char[] cbuf, int off, int len) throws IOException {
        return this.internalDataSource.read(cbuf, off, len);
    }

    /**
     * @param cbuf the char array to read
     * @return the number of characters read
     * @throws IOException if unsuccessful read
     */
    public int read(char[] cbuf) throws IOException {
        return this.internalDataSource.read(cbuf);
    }

    /**
     * @param target the CharBuffer object.
     * @return the number of characters read
     * @throws IOException if unsuccessful
     */
    public int read(CharBuffer target) throws IOException {
        return this.internalDataSource.read(target);
    }

    /**
     * 
     * @return the line as a String
     * @throws IOException if unsuccessful
     */
    public String readLine() throws IOException {
        return this.internalDataSource.readLine();
    }

    /**
     * 
     * @return true if datasource is in ready state
     * @throws IOException if unsuccessful to determine ready state
     */
    public boolean ready() throws IOException {
        return this.internalDataSource.ready();
    }

    /**
     * 
     * @throws IOException if unable to reset datasource
     */
    public void reset() throws IOException {
        this.internalDataSource.reset();
    }

    /**
     * 
     * @param n specifies the number to skip
     * @return the long to skip
     * @throws IOException if unsuccessful
     */
    public long skip(long n) throws IOException {
        return this.internalDataSource.skip(n);
    }

    /**
     * 
     * @return true if Presort value is on
     */
    public boolean isPresort() {
        return this.presort;
    }

    /**
     * 
     * @param presort sets the presort value.
     */
    public void setPresort(boolean presort) {
        this.presort = presort;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Describable#getDescription()
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Describable#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description.setDescription(description);
    }

    /**
     * @return Returns the currentLineNumber.
     */
    public long getCurrentLineNumber() {
        return this.currentLineNumber;
    }

    /**
     * @param currentLine specifies the currentLineNumber to set.
     */
    public void setCurrentLineNumber(long currentLine) {
        this.currentLineNumber = currentLine;
    }
    
    /**
     * @return Returns the encoding.
     */
    public String getEncoding() {
        return this.encoding;
    }

    /**
     * @param encoding specify the type of encoding used for input file
     * @throws ConfigurationException if there is problem is setting the charset
     * encoding
     */
    public void setEncoding(String encoding) throws ConfigurationException {
        if (encoding == null || encoding == "") {
            //default to UTF-8
            this.encoding = EncodingSupport.DEFAULT_ENCODING;
        } else {   
            if (EncodingSupport.isEncodingSupported(encoding)) {
                this.encoding = encoding;   
            } else {
                log
                .error(
                        "VoiceLink does not support " + encoding + " character encoding." 
                        + " State is misconfigured, please verify the configuration",
                        DataSourceAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK);
                        setCurrentState(DataSourceAdapter.MISCONFIGUREDSTATE);
                throw new ConfigurationException(DataSourceAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK, 
                    new UserMessage(LocalizedMessage.formatMessage(
                         DataSourceAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK)));
            }

        }
    }    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 