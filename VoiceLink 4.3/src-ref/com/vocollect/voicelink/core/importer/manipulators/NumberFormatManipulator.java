/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import com.opensymphony.xwork2.validator.ValidationException;

import java.util.Formatter;
import java.util.IllegalFormatConversionException;
/**
 * Class to convert the field data to string. The field data can be of any type.
 * 
 * @author vsubramani
 */
public class NumberFormatManipulator extends VocollectCustomManipulator {

    private String fieldFormat = null;

    private static final Logger log = new Logger(NumberFormatManipulator.class);

    /**
     * {@inheritDoc}
     *
     */

    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        String newValue = null;

        if ((value == null)) {
            return;
        }

        newValue = convertToString(value, fieldFormat);

        ValidationResult.VocollectValidatorContext vvC = (ValidationResult.VocollectValidatorContext) this
                .getValidatorContext();
        FixedLengthField data = new FixedLengthField();
        data.setFieldName(fieldname);
        data.setFieldData(newValue);
        FixedLengthFieldMap fields = new FixedLengthFieldMap();
        fields.addField(data);
        ObjectContext<FixedLengthFieldMap, Object> oc = 
            new ObjectContext<FixedLengthFieldMap, Object>(
                fields, "No real object here");
        vvC.setFields(oc);

    }

    /**
     * Method to convert the field data to the specified format.
     * @param ff - format to convert.
     * @param oldValue - field data.
     * @return formatted field data.
     */
    private String convertToString(Object oldValue, String ff) {
        try {
            Formatter fmt = new Formatter();
            fmt.format(ff, oldValue);
            return fmt.toString();
        } catch (IllegalFormatConversionException e) {
            log.fatal("Incorrect format of data " + fieldFormat
                    + " for field value " + oldValue + ".",
                    ValidatorManipulatorErrors.ILLEGAL_FORMAT_CONVERSION, e);
            throw new RuntimeException(e);
        } catch (NullPointerException e) {
            log.fatal("Field Format data is " + fieldFormat + ".",
                    ValidatorManipulatorErrors.NULL_FIELD_FORMAT_SPECIFIED, e);
            throw new RuntimeException(
                    LocalizedMessage
                            .formatMessage(ValidatorManipulatorErrors.NULL_FIELD_FORMAT_SPECIFIED));
        }
    }

    /**
     * Gets the field format.
     * @return the fieldFormat.
     */
   public String getFieldFormat() {
        return fieldFormat;
    }

    /**
     * Sets the field format.
     * @param fieldFormat specifies the format to set
     */
    public void setFieldFormat(String fieldFormat) {
        this.fieldFormat = fieldFormat;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 