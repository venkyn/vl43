/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;


import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Class handles enum values.
 * 
 * @author vsubramani
 */
public class ValueBasedEnumManipulator  extends VocollectCustomManipulator {

    private static final Logger log = new Logger(ValueBasedEnumManipulator.class);

    private String allowedValues = null;
    
    private static final String ALLOWED_VALUE_SEPARATOR = ",";
    
    /**
     * Setter for the allowedValues property.
     * @param allowedValues the new allowedValues value
     */
    public void setAllowedValues(String allowedValues) {
        this.allowedValues = allowedValues;
    }
    
    /**
     * Getter for the allowedValues property.
     * @return String value of the property
     */
    public String getAllowedValues() {
        return this.allowedValues;
    }


    
    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        boolean result = false;
        if (fieldname == null) {
            return;
        }
        ValueBasedEnum vbe = (ValueBasedEnum) this.getFieldValue(getFieldName(), actionObj);
        
        try {
            ValidationResult.VocollectValidatorContext vvC =
                (ValidationResult.VocollectValidatorContext) this
                .getValidatorContext();
            for (FieldMap fm : (List<FieldMap>) vvC.getFields().getFieldMaps()) {

                Field rawData = fm.get(fieldname);
                
                //If allowedValues is empty or null do nothing proceed with manipulation
                if ((this.allowedValues == null) || (this.allowedValues == "")) {
                   //Do nothing;
                } else {
                    result = containsAllowedValues(rawData.getFieldData(), this.allowedValues);
                    //If contains values other than allowed values
                    if (!result) {
                        addFieldError(fieldname, actionObj);    
                        return;
                    }                                        
                }

                // Perform action only when rawData and fieldData are not null and
                // the field data is not an empty string
                if ((rawData != null) && (rawData.getFieldData() != null)
                        && (rawData.getFieldData().trim().length() != 0)) { 
                    try {
                        //Integer i = Integer.valueOf(rawData.getFieldData());
                        Object value = vbe.fromValue(Integer.valueOf(rawData.getFieldData()));
                        if (value != null) {
                            ValueStack stack = ActionContext.getContext().getValueStack();
                            stack.push(actionObj);
                            stack.setValue(fieldname, value, true);
                            stack.pop();

                        } 
                    } catch (IllegalArgumentException i) {
                        addFieldError(fieldname, actionObj);
                    }
                } else {
                    log.debug("RawData is either NULL or EMPTY for "
                            + rawData.getFieldName());
                }
            }
        } catch (ClassCastException e) {
            log.fatal(
                    "Bad configuration for validation of "
                    + actionObj.getClass().getName() + ". Type mismatch.",
                    ValidatorManipulatorErrors.CLASS_CAST_IMPROPER, e);
            throw new RuntimeException(LocalizedMessage.formatMessage(
                    ValidatorManipulatorErrors.CLASS_CAST_IMPROPER));
        }
    }

    /**
     * Check if the field contains the allowed values or not.
     * @param stringToCheck string which needs to be validated against allowed values
     * @param stringAllowedValues allowed values - comma separated string
     * @return false if input contains the value other than those are allowed else return true
     */
    private boolean containsAllowedValues(String stringToCheck, String stringAllowedValues) {
        boolean result = true;
        
        ArrayList<String> allowedList = new ArrayList<String>();
        StringTokenizer st = new StringTokenizer(stringAllowedValues, ALLOWED_VALUE_SEPARATOR, false);

        while (st.hasMoreTokens()) {
            allowedList.add(st.nextToken()); 
        }
        if (!(allowedList.contains(stringToCheck))) {
          result = false;  
        }
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 