/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.OgnlValueStack;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import ognl.OgnlException;

/**
 * Allow the importer to set a property in the model object with a default 
 * value that is NOT contained in the import file
 * 
 * @author pfunyak
 *
 */
public class ValueBasedEnumSetDefaultManipulator  extends VocollectCustomManipulator  {

    private static final Logger log = new Logger(ValueBasedEnumSetDefaultManipulator.class);
    
    private Object defaultValue = null;
    
    /**
     * {@inheritDoc}
     *
     */
    @Override
    public void validate(Object actionObj) throws ValidationException {
        
        // get the the name of the field in the model that we are trying to set.
        String fieldName = getFieldName();
        if (fieldName == null) {
            return;
        }
        // Try to get the value of the property we are trying to set (which must be an enumeration)
        ValueBasedEnum vbe = null;
        String localizedLogString = "";
        try {
            vbe = (ValueBasedEnum) this.getFieldValue(getFieldName(), actionObj);
            if (vbe == null) {
                // then the fieldName does not exist in the object.
                Object [] errorMessageArgs = {fieldName, actionObj.getClass().getName()};
                localizedLogString = LocalizedMessage.formatMessage(
                        ValidatorManipulatorErrors.FIELD_NAME_DOES_NOT_EXIST.getDefaultMessageKey(),
                        errorMessageArgs);
                log.error(localizedLogString, ValidatorManipulatorErrors.FIELD_NAME_DOES_NOT_EXIST);
                throw new RuntimeException(localizedLogString);
            }
        } catch (ClassCastException cce) {
            // we get here if the fieldName exists in the object but it is NOT a ValueBasedEnum
            Object [] errorMessageArgs = {fieldName};
            localizedLogString = LocalizedMessage.formatMessage(
                    ValidatorManipulatorErrors.NOT_AN_ENUMERATED_TYPE.getDefaultMessageKey(),
                    errorMessageArgs);
            log.error(localizedLogString, ValidatorManipulatorErrors.NOT_AN_ENUMERATED_TYPE);
            throw new RuntimeException(localizedLogString);
        }
        // see if the integer value we are attempting to set the enum to is actually 
        // in the enumeration.  If it does not exist, an illegal argument exception will
        // be thrown.
        try {
            vbe.fromValue(new Integer(this.getDefaultValue().toString()));
        } catch (IllegalArgumentException e) {
            Object [] errorMessageArgs = {this.getDefaultValue().toString(),
                    vbe.getClass().getName()};
            localizedLogString = LocalizedMessage.formatMessage(
                    ValidatorManipulatorErrors.VALUE_DOES_NOT_EXIST.getDefaultMessageKey(),
                    errorMessageArgs);
            throw new RuntimeException(localizedLogString);
        }

        ValueStack stack = ActionContext.getContext().getValueStack();
        OgnlValueStack.ObjectAccessor ognlObjectAccessor = new OgnlValueStack.ObjectAccessor();
        stack.push(actionObj);
        try {
            // Get the content of the property value from OGNL stack so that 
            // if property doesn't exits it will throw the OGNLException property not found
            ognlObjectAccessor.getProperty(stack.getContext(), actionObj, fieldName);    
        } catch (OgnlException e) {
            //Throw the runtime exception as this is configuration problem
            log.error("The name given for the field to set the defaut value for " + fieldName 
                +  "does not exist in " + actionObj.getClass().getName(),
                ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_DOES_NOT_EXIST);
            throw new RuntimeException(LocalizedMessage.formatMessage(
                ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_DOES_NOT_EXIST));
        }
        Object value = vbe.fromValue(new Integer(this.getDefaultValue().toString()));
        stack.setValue(fieldName, value, true);   
        stack.pop();
    }

    /**
     * @return the defaultValue
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * @param defaultValue the defaultValue to set
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 