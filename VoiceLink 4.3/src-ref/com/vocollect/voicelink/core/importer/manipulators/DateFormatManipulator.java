/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.validators.DateValidator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.List;

/**
 * Class to convert the default date string to Java date/time format. The named
 * field should be a date field.
 * 
 * @author vsubramani
 */

public class DateFormatManipulator extends DateValidator {

    private static final Logger log = new Logger(DateFormatManipulator.class);

    /**
     * {@inheritDoc}
     *
     */
    @Override
    @SuppressWarnings("unchecked")
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        boolean result = false;

        try {
            ValidationResult.VocollectValidatorContext vvC =
                (ValidationResult.VocollectValidatorContext) this
                .getValidatorContext();
            for (FieldMap fm : (List<FieldMap>) vvC.getFields().getFieldMaps()) {

                Field rawData = fm.get(fieldname);

                // Perform action only when rawData and fieldData are not null and
                // the field data is not an empty string
                if ((rawData != null) && (rawData.getFieldData() != null)
                        && (rawData.getFieldData().trim().length() != 0)) {
                    result = this.isValidDate(
                            rawData.getFieldData(), getDateFormat());
                    addValidationError(fieldname, actionObj, result);

                    // Set the new field value only if the string to date conversion
                    // is successful.
                    if (this.getResultantDate() != null) {
                        ValueStack stack = ActionContext.getContext().getValueStack();
                        stack.push(actionObj);
                        stack.setValue(fieldname, this.getResultantDate(), true);
                        stack.pop();

                    }
                } else {
                    log.debug("RawData is either NULL or EMPTY for "
                            + rawData.getFieldName());
                }
            }
        } catch (ClassCastException e) {
            log.fatal(
                    "Bad configuration for validation of "
                    + actionObj.getClass().getName() + ". Type mismatch.",
                    ValidatorManipulatorErrors.CLASS_CAST_IMPROPER, e);
            throw new RuntimeException(LocalizedMessage.formatMessage(
                    ValidatorManipulatorErrors.CLASS_CAST_IMPROPER));
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 