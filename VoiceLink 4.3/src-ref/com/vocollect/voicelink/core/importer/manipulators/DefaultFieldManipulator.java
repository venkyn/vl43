/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.OgnlValueStack;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import ognl.OgnlException;


/**
 * This class will populate a field with data from other similar field,
 * if it is blank or null and if configured to do so in the xml file.  
 * This class utilizes customized manipulators.
 *
 * @author KalpnaT
 */
public class DefaultFieldManipulator extends VocollectCustomManipulator {
    
    private String defaultFieldName = null;
    
   private static final Logger log                     = new Logger(
        DefaultFieldManipulator.class);

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     * This method will replace the specified field value by the defalut field 
     * value if it is blank and if configured to do so in xml file.
     * If the default field name is invalid , it will throw the runtime exception
     * as this is the configuration error and this can occur most likely only
     * when running the imports for the first time.
     */
    public void validate(Object actionObj) throws ValidationException {
        //This is the field name on which we are applying the manipulation
        String fieldname = getFieldName();
        //This holds the above field value
        Object value = this.getFieldValue(fieldname, actionObj);
        //This is the field name, it's value will replace the field value on
        //which manipulation is applied
        String defFieldName = this.getDefaultFieldName();
        //This holds the default value for the defFieldName
        Object defaultValue = null;        
        
        if (fieldname == null) {
            return;
        }
        
        //TODO check and see if you can do this check only once for a field using        
        if (this.isNullField(defFieldName) || this.isEmptyField(defFieldName.toString())) {
            addFieldError(fieldname, actionObj);
            log
            .error("The default field name is not specified for " + actionObj.getClass().getName(),
                ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_NOT_SPECIFIED);
            throw new RuntimeException(LocalizedMessage.formatMessage(
                ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_NOT_SPECIFIED));
        }    
        
        if (this.isNullField(value) || this.isEmptyField(value.toString())) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            OgnlValueStack.ObjectAccessor ognlObjectAccessor = new OgnlValueStack.ObjectAccessor();
            stack.push(actionObj);
            try {
                //Get the property value from OGNL stack so that if property doesn't exits
                //it will throw the OGNLException property not found
                defaultValue = ognlObjectAccessor.getProperty(stack.getContext(), actionObj, defFieldName);    
            } catch (OgnlException e) {
                //Throw the runtime exception as this is configuration problem
                log
                .error("The name given for the field to supply the default value " + getDefaultFieldName() 
                    +  "is not valid in " + actionObj.getClass().getName(),
                    ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_DOES_NOT_EXIST);
                throw new RuntimeException(LocalizedMessage.formatMessage(
                    ValidatorManipulatorErrors.DEFAULT_FIELD_NAME_DOES_NOT_EXIST));
 
            }
            
            stack.setValue(fieldname, defaultValue, true);   
            stack.pop();

        }
        return;
        
    }
    
    /**
     * @return the defaultFieldName
     */
    public String getDefaultFieldName() {
        return defaultFieldName;
    }

    /**
     * @param defaultFieldName the defaultField, who's
     * value will be used to replace the given field value
     */
    public void setDefaultFieldName(String defaultFieldName) {
        this.defaultFieldName = defaultFieldName;
    }    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 