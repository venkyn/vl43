/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;


/**
 * This class will trim a field utilizing a particular character to trim with if
 * configured to do so in the xml file. This class utilizes customized
 * manipulators.
 * 
 * @author astein
 */
public class TrimManipulator extends VocollectCustomManipulator {

    private char    defaultTrimChar = ' ';

    private String  trimmableString = "";

    private Object fieldValue      = null;

    /**
     * {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObj) throws ValidationException {
        String returnedString = "";
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        this.setValue(value);
        if (fieldname == null) {
            return;
        }

        this.trimmableString = value.toString();
        if (this.needsTrimming(this.trimmableString) && defaultTrimChar == ' ') {
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(actionObj);
            this.setValue(trimmableString.trim());
            stack.setValue(fieldname, trimmableString.trim(), true);
            stack.pop();
        } else if (this.needsTrimming(this.trimmableString, defaultTrimChar)) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(actionObj);
            String trimChar = Character.toString(defaultTrimChar);
            if (trimmableString.startsWith(trimChar)) {
                for (int i = 0; i < trimmableString.length(); i++) {
                    if (trimmableString.charAt(i) != defaultTrimChar) {
                        returnedString = trimmableString.substring(i);
                        break;
                    }
                }
            } else {
                returnedString = this.trimmableString;
            }
            this.trimmableString = returnedString;
            if (trimmableString.endsWith(trimChar)) {
                for (int i = trimmableString.length() - 1; i >= 0; i--) {
                    if (trimmableString.charAt(i) != defaultTrimChar) {
                        returnedString = trimmableString.substring(0, i + 1);
                        break;
                    }
                }
            }
            this.setValue(returnedString);
            stack.setValue(fieldname, returnedString, true);
            stack.pop();
        }
        return;

    }

    /**
     * Gets the fieldValue.
     * @return the fieldValue object
     */
    public Object getValue() {
        return this.fieldValue;
    }

    /**
     * Sets the fieldValue.
     * @param fieldvalue specifies the Object to set
     */
    public void setValue(Object fieldvalue) {
        this.fieldValue = fieldvalue;
    }

    /**
     * @return the defaultTrimChar
     */
    public char getDefaultTrimChar() {
        return defaultTrimChar;
    }

    /**
     * @param defaultTrimChar
     *            the defaultTrimChar to set
     */
    public void setDefaultTrimChar(char defaultTrimChar) {
        this.defaultTrimChar = defaultTrimChar;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 