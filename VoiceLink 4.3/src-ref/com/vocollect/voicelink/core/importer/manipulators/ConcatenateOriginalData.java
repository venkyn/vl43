/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.LinkedList;
import java.util.List;

/**
 * This class will concatenate the named original data field's content to the end of (this) field.
 * @author dgold
 *
 */
public class ConcatenateOriginalData extends VocollectCustomManipulator {

    static final Logger log = new Logger(ConcatenateOriginalData.class);


    private String sourceFieldName1 = "";
    private String sourceFieldName2 = "";
    private String sourceFieldName3 = "";
    
    private Integer maxStringLength = null;

    
    private List<String> sourceFieldNames = null;
    private String sourceFieldValue = "";

    private String result = "";

    /**
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    @SuppressWarnings("unchecked")
    public void validate(Object actionObj) throws ValidationException {

        if (!shouldRun()) {
            return;
        }
        if (getSourceFieldNames() == null) {
            sourceFieldNames = new LinkedList<String>();
            getSourceFieldNames().add(sourceFieldName1);
            getSourceFieldNames().add(sourceFieldName2);
            getSourceFieldNames().add(sourceFieldName3);
        }
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);
        if (value == null) {
            value = "";
        }

        if (fieldname == null) {
            return;
        }

        // Get the data to be added to the end of this (which should be a string)
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();
        
        result = "";

        // This will throw a null pointer exception if the field map is null or the field does not exist.
        List<? extends FieldMap> list = vvC.getFields().getFieldMaps();
        for (String fieldName : getSourceFieldNames()) {

            if ((fieldName != null) && (fieldName.length() > 0)) {
                sourceFieldValue = list.get(0).get(fieldName).getFieldData();
                if (sourceFieldValue == null) {
                    sourceFieldValue = "";
                }
                // Form the result
                result = result + sourceFieldValue.trim();
            }

        }
        if (null != getMaxStringLength()) {
            if (result.length() > getMaxStringLength()) {
                String temp = "HASH" + String.valueOf(result.hashCode()) + "hash"; 
                log.info("Concatenated string " + result + " longer than max of " + getMaxStringLength()
                         + " for field " + fieldname + ". Replacing with " + temp
                        );
                result = temp;
            }
        }

        // Set the field to the new value.
        ValueStack stack = ActionContext.getContext().getValueStack();
        stack.push(actionObj);
        stack.setValue(fieldname, result, true);
        stack.pop();

        return;

    }

    /**
     * Gets the value of sourceFieldName1.
     * @return the sourceFieldName1
     */
    public String getSourceFieldName1() {
        return this.sourceFieldName1;
    }

    /**
     * Sets the value of the sourceFieldName1.
     * @param sourceFieldName1 the sourceFieldName1 to set
     */
    public void setSourceFieldName1(String sourceFieldName1) {
        this.sourceFieldName1 = sourceFieldName1;
    }

    /**
     * Gets the value of sourceFieldName2.
     * @return the sourceFieldName2
     */
    public String getSourceFieldName2() {
        return this.sourceFieldName2;
    }

    /**
     * Sets the value of the sourceFieldName2.
     * @param sourceFieldName2 the sourceFieldName2 to set
     */
    public void setSourceFieldName2(String sourceFieldName2) {
        this.sourceFieldName2 = sourceFieldName2;
    }

    /**
     * Gets the value of sourceFieldName3.
     * @return the sourceFieldName3
     */
    public String getSourceFieldName3() {
        return this.sourceFieldName3;
    }

    /**
     * Sets the value of the sourceFieldName3.
     * @param sourceFieldName3 the sourceFieldName3 to set
     */
    public void setSourceFieldName3(String sourceFieldName3) {
        this.sourceFieldName3 = sourceFieldName3;
    }

    /**
     * Gets the value of sourceFieldNames.
     * @return the sourceFieldNames
     */
    public List<String> getSourceFieldNames() {
        return this.sourceFieldNames;
    }

    /**
     * Sets the value of the sourceFieldNames.
     * @param sourceFieldNames the sourceFieldNames to set
     */
    public void setSourceFieldNames(List<String> sourceFieldNames) {
        this.sourceFieldNames = sourceFieldNames;
    }

    /**
     * Gets the value of maxStringLength.
     * @return the maxStringLength
     */
    public Integer getMaxStringLength() {
        return this.maxStringLength;
    }

    /**
     * Sets the value of the maxStringLength.
     * @param maxStringLength the maxStringLength to set
     */
    public void setMaxStringLength(Integer maxStringLength) {
        this.maxStringLength = maxStringLength;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 