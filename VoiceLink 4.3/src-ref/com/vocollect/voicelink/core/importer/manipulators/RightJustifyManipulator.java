/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

/**
 * Class to Right Justify a string.
 * 
 * @author vsubramani
 */
public class RightJustifyManipulator extends VocollectCustomManipulator {

    private static final Logger log = new Logger(RightJustifyManipulator.class);

    private int size;

    private String newValue;

    /**
     * {@inheritDoc}
     *
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);

        if (value == null) {
            return;
        }
        newValue = rightJustify(value.toString(), size);
        if (newValue != null) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(actionObj);
            stack.setValue(fieldname, newValue, true);
            stack.pop();
        }
    }

    /**
     * Method for right justifying.
     * @param val - field value
     * @param s - size to which the field value is to be altered.
     * @return rightjustified field value
     */
    private String rightJustify(String val, int s) {
        String rightJustified = null;
        int len = 0;
        if (s > val.length()) {
            len = s - val.length();
        } else {
            len = s;
        }

        try {
            for (int i = 0; i < len; i++) {
                rightJustified = " " + val;
            }
            return rightJustified;
        } catch (NullPointerException e) {
            log.fatal("size is NULL",
                    ValidatorManipulatorErrors.NULL_SIZE_SPECIFIED, e);
            throw new RuntimeException(
                    LocalizedMessage
                            .formatMessage(ValidatorManipulatorErrors.NULL_SIZE_SPECIFIED));
        }
    }

    /**
     * 
     * @return size
     */
    public int getSize() {
        return size;
    }

    /**
     * 
     * @param size specifies the value to set.
     */
    public void setSize(int size) {
        this.size = size;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 