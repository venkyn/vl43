/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;


import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
/**
 * Class to replace one string with another.
 *
 * @author vsubramani
 */
public class NonPrintableCharacterReplaceManipulator extends VocollectCustomManipulator {

    private static final int TILDE_CHAR = 126;
    private static final int SPACE_CHAR = 32;

    //Default replacement behavior is to replace nonPrintables with a space.
    private String replacement = " ";

    private String newValue;

    /**
     * {@inheritDoc}
     *
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);

        if ((value == null)) {
            return;
        }
        newValue = nonPrintableCharacterReplace(value.toString(), replacement);

        if (newValue != null) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(actionObj);
            stack.setValue(fieldname, newValue, true);
            stack.pop();
        }
    }

    /**
     * Method replaces nonPrintable characters with replacement string in val.
     * @param val - source value to scan.
     * @param r - Replacement String.
     * @return val - with non printable chars replaced with r.
     */
    private String nonPrintableCharacterReplace(String val, String r) {
        String returnStr = "";
        char c = ' ';
        int asciiValue = 0;
        
        for (int x = 0; x < val.length(); x++) {
            c = val.charAt(x);
            asciiValue = c;
            if ((asciiValue >= SPACE_CHAR) && (asciiValue <= TILDE_CHAR)) {
                // Space is 32, tilde is 126 everything outside is non-printable
                //c is printable character, use c
                returnStr = returnStr + c;
            } else {
                //replace the value with r
                returnStr = returnStr + r;
            }
        }
        return returnStr;
    }

    /**
     * Gets the replacement Value.
     * @return replacement
     */
    public String getReplacement() {
        return replacement;
    }

    /**
     * Sets the replacement value.
     * NOTE:  outside most set of double quotes are stripped for strings of 2 or
     *        more characters.  eg.  " " = space   ""Hello"" = "Hello" "" = empty string
     * @param replacement specifies the value to set.
     */
    public void setReplacement(String replacement) {
        //If there are at least 2 characters...
        if (replacement.length() >= 2) {
            //If replacement starts and ends with a double-quote,
            // then strip them off.
            if ((replacement.charAt(0) == '"') && (replacement.charAt(replacement.length() - 1) == '"')) {
                this.replacement = replacement.substring(1, replacement.length() - 1);
                return;
            }
        }
        this.replacement = replacement;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 