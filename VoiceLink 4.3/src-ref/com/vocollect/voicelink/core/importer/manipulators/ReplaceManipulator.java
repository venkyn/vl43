/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;


import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ValidatorManipulatorErrors;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;
/**
 * Class to replace one string with another.
 *
 * @author vsubramani
 */
public class ReplaceManipulator extends VocollectCustomManipulator {

    //
    private static final int MIN_QUOTED_LENGTH = 3;

    private String target = null;

    private String replacement = null;

    private static final Logger log = new Logger(ReplaceManipulator.class);

    private String newValue;

    /**
     * {@inheritDoc}
     *
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, actionObj);

        if ((value == null)) {
            return;
        }
        newValue = replace(value.toString(), target, replacement);

        if (newValue != null) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(actionObj);
            stack.setValue(fieldname, newValue, true);
            stack.pop();
        }
    }

    /**
     * Method replaces target matches with replacement in val.
     * @param val - field value.
     * @param t - string to be replaced.
     * @param r - Replacement String.
     * @return val.
     */
    private String replace(String val, String t, String r) {
        try {
            return val.replace(t, r);
        } catch (NullPointerException e) {
            log.fatal(
                "Target/Replacement is NULL",
                ValidatorManipulatorErrors.NULL_PARAMENTER_SPECIFIED, e);
            throw new RuntimeException(LocalizedMessage.formatMessage(
                ValidatorManipulatorErrors.NULL_PARAMENTER_SPECIFIED));
        }
    }

    /**
     * Gets the replacement Value.
     * @return replacement
     */
    public String getReplacement() {
        return replacement;
    }

    /**
     * Sets the replacement value.
     * NOTE:  outside most set of double quotes are stripped for strings of 3 or
     *        more characters.  eg.  " " = space   ""Hello"" = "Hello"
     * @param replacement specifies the value to set.
     */
    public void setReplacement(String replacement) {
        //If there are at least 3 characters...
        if (replacement.length() >= MIN_QUOTED_LENGTH) {
            //If replacement starts and ends with a double-quote,
            // then strip them off.
            if ((replacement.charAt(0) == '"') && (replacement.charAt(replacement.length() - 1) == '"')) {
                this.replacement = replacement.substring(1, replacement.length() - 1);
                return;
            }
        }
        this.replacement = replacement;
    }

    
    /**
     * Gets the target Value.
     * @return target
     */
    public String getTarget() {
        return target;
    }

    
    /**
     * Sets the target value.
     * NOTE:  outside most set of double quotes are stripped for strings of 3 or
     *        more characters.  eg.  " " = space   ""Hello"" = "Hello"
     * @param target specifies the value to set.
     */
    public void setTarget(String target) {
        //If there are at least 3 characters...
        if (target.length() >= MIN_QUOTED_LENGTH) {
            //If target starts and ends with a double-quote,
            // then strip them off.
            if ((target.charAt(0) == '"') && (target.charAt(target.length() - 1) == '"')) {
                this.target = target.substring(1, target.length() - 1);
                return;
            }
        }
        this.target = target;
    }

    
    /**
     * Getter for the newValue property.
     * @return the value of the property
     */
    public String getNewValue() {
        return this.newValue;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 