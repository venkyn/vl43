/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomValidator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.Collection;
/**
 * @author dgold
 *
 */
public class ReplaceIfNonUniformOriginaListData extends
        VocollectCustomValidator {

    // This is the field we will be checking in the original data
    private String targetField = "";
    private String replacementValue = "";
    
    private String  fieldData = "";
    private boolean fieldDataSet = false;

    /**
     * We want to replace the data (taken from the first element in the list) with the replacement supplied.
     * This allows us to (foe example) show "MULTIPLE" for the delivery location
     * when different picks have differend delivery locations.
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    @SuppressWarnings("unchecked")
    public void validate(Object subject) throws ValidationException {
        fieldDataSet = false;
        // This is the field data we will be changing
        String fieldname = getFieldName();
        if (0 == targetField.trim().compareTo("")) {
            targetField = fieldname;
        }
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        Collection<? extends FieldMap> list = vvC.getFields().getFieldMaps();

        for (FieldMap fm : list) {
            if (fieldDataSet) {
                if (0 != fieldData.compareTo(fm.get(targetField).getFieldData())) {
                    ValueStack stack = ActionContext.getContext().getValueStack();
                    stack.push(subject);
                    stack.setValue(fieldname, replacementValue, true);
                    stack.pop();
                }
            } else {
                fieldData = fm.get(targetField).getFieldData();
                fieldDataSet = true;
            }
        }
    }

    /**
     * Gets the value of replacementValue.
     * @return the replacementValue
     */
    public String getReplacementValue() {
        return replacementValue;
    }

    /**
     * Sets the value of the replacementValue.
     * @param replacementValue the replacementValue to set
     */
    public void setReplacementValue(String replacementValue) {        
        this.replacementValue = replacementValue;
    }

    /**
     * Gets the value of targetField.
     * @return the targetField
     */
    public String getTargetField() {
        return targetField;
    }

    /**
     * Sets the value of the targetField.
     * @param targetField the targetField to set
     */
    public void setTargetField(String targetField) {
        this.targetField = targetField;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 