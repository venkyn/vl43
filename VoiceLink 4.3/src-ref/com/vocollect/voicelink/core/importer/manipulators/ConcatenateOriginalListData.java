/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * This class will concatenate the named original data field's content to the end of (this) field.
 * @author dgold
 *
 */
public class ConcatenateOriginalListData extends VocollectCustomManipulator {

    static final Logger log = new Logger(ConcatenateOriginalListData.class);

    private String sourceFieldName1 = "";
    private String sourceFieldName2 = "";
    private String sourceFieldName3 = "";
    
    private String targetFieldName = null;
    
    private Integer maxStringLength = null;
    
    private List<String> sourceFieldNames = null;
    private String sourceFieldValue = "";

    private String result = "";

    /**
     * {@inheritDoc}
     * @param arg0
     * @throws ValidationException
     */
    @SuppressWarnings("unchecked")
    public void validate(Object subject) throws ValidationException {

        if (!shouldRun()) {
            return;
        }
        if (getSourceFieldNames() == null) {
            sourceFieldNames = new LinkedList<String>();
            getSourceFieldNames().add(sourceFieldName1);
            getSourceFieldNames().add(sourceFieldName2);
            getSourceFieldNames().add(sourceFieldName3);
        }
        String fieldname = getFieldName();
        Object value = this.getFieldValue(fieldname, subject);
        if (value == null) {
            value = "";
        }

        if (fieldname == null) {
            return;
        }
        
        Object listField = this.getFieldValue(fieldname, subject);
        ArrayList modelList = (ArrayList) listField;

        // Get the data to be added to the end of this (which should be a string)
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();
        
        int listElement = 0;

        // This will throw a null pointer exception if the field map is null or the field does not exist.
        List<? extends FieldMap> list = vvC.getFields().getFieldMaps();
        
        // Get each field map in order.
        for (FieldMap fields : list) {
            result = "";

            // Get the corresponding element from the model - they will be in the same order
            Object modelElement = modelList.get(listElement);

            // For each field named, get the data and concatenate it onto the result.
            for (String sourceFieldName : getSourceFieldNames()) {
                if ((sourceFieldName != null) && (sourceFieldName.length() > 0)) {
                    sourceFieldValue = fields.get(sourceFieldName).getFieldData();
                    if (sourceFieldValue == null) {
                        sourceFieldValue = "";
                    }
                    // Form the result
                    result = result + sourceFieldValue.trim();
                }
            }
            // Check the length. Over limit? Use hash code from the string
            if (null != getMaxStringLength()) {
                if (result.length() > getMaxStringLength()) {
                    String temp = "HASH" + String.valueOf(result.hashCode()) + "hash"; 
                    log.info("Concatenated string " + result + " longer than max of " + getMaxStringLength()
                             + " for field " + targetFieldName + " in " + fieldname 
                             + ". Replacing with " + temp
                            );
                    result = temp;
                }
            }
            // Set the field to the new value.
            ValueStack stack = ActionContext.getContext().getValueStack();
            stack.push(modelElement);
            stack.setValue(targetFieldName, result, true);
            stack.pop();
            listElement++;
        }

        return;

    }

    /**
     * Gets the value of sourceFieldName1.
     * @return the sourceFieldName1
     */
    public String getSourceFieldName1() {
        return this.sourceFieldName1;
    }

    /**
     * Sets the value of the sourceFieldName1.
     * @param sourceFieldName1 the sourceFieldName1 to set
     */
    public void setSourceFieldName1(String sourceFieldName1) {
        this.sourceFieldName1 = sourceFieldName1;
    }

    /**
     * Gets the value of sourceFieldName2.
     * @return the sourceFieldName2
     */
    public String getSourceFieldName2() {
        return this.sourceFieldName2;
    }

    /**
     * Sets the value of the sourceFieldName2.
     * @param sourceFieldName2 the sourceFieldName2 to set
     */
    public void setSourceFieldName2(String sourceFieldName2) {
        this.sourceFieldName2 = sourceFieldName2;
    }

    /**
     * Gets the value of sourceFieldName3.
     * @return the sourceFieldName3
     */
    public String getSourceFieldName3() {
        return this.sourceFieldName3;
    }

    /**
     * Sets the value of the sourceFieldName3.
     * @param sourceFieldName3 the sourceFieldName3 to set
     */
    public void setSourceFieldName3(String sourceFieldName3) {
        this.sourceFieldName3 = sourceFieldName3;
    }

    /**
     * Gets the value of sourceFieldNames.
     * @return the sourceFieldNames
     */
    public List<String> getSourceFieldNames() {
        return this.sourceFieldNames;
    }

    /**
     * Sets the value of the sourceFieldNames.
     * @param sourceFieldNames the sourceFieldNames to set
     */
    public void setSourceFieldNames(List<String> sourceFieldNames) {
        this.sourceFieldNames = sourceFieldNames;
    }

    /**
     * Gets the value of targetFieldName.
     * @return the targetFieldName
     */
    public String getTargetFieldName() {
        return this.targetFieldName;
    }

    /**
     * Sets the value of the targetFieldName.
     * @param targetFieldName the targetFieldName to set
     */
    public void setTargetFieldName(String targetFieldName) {
        this.targetFieldName = targetFieldName;
    }

    /**
     * Gets the value of maxStringLength.
     * @return the maxStringLength
     */
    public Integer getMaxStringLength() {
        return this.maxStringLength;
    }

    /**
     * Sets the value of the maxStringLength.
     * @param maxStringLength the maxStringLength to set
     */
    public void setMaxStringLength(Integer maxStringLength) {
        this.maxStringLength = maxStringLength;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 