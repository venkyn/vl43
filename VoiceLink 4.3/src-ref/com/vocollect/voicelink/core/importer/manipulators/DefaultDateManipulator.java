/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.VocollectCustomManipulator;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.Calendar;

/**
 * Class to supply a default date/time to the named field. 
 * The date will be (now).
 * The named field should be a date field. Odd results will occur for a string
 * or other type.
 * 
 * @author dgold
 *
 */
public class DefaultDateManipulator extends VocollectCustomManipulator {

    /**
     * Default constructor. Nothing to do
     */
    public DefaultDateManipulator() {
        // Nothing to do
    }

    /**
     * Validate. This is the method called by the validation framework.
     * This method gets the field from the object by name, checks to see if the field
     * is empty and, if so, supplies (now) as the value.
     * @param actionObj the object to validate
     * @throws ValidationException on any error. 
     * 
     */
    public void validate(Object actionObj) throws ValidationException {
        String fieldname = getFieldName(); // This is set by the framework

        if (fieldname == null) {
            return;
        }

        Object value = this.getFieldValue(fieldname, actionObj);

        if (this.isNullField(value) || this.isEmptyField(value.toString())) {
            ValueStack stack = ActionContext.getContext().getValueStack();
            // Set the object up to be manipulated by the framework.
            stack.push(actionObj);
            // The flag says a flag to throw an  exception if there is no property with the given name
            stack.setValue(fieldname, Calendar.getInstance().getTime(), true);
            stack.pop();

        }
        return;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 