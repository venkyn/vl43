/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The CreateNotification class is used to create notification and
 * sends e-mail to all registered users when a critical notification
 * is generated.
 *
 * @author vsubramani
 *
 */
public class CreateNotification implements ApplicationContextAware,
        BeanFactoryPostProcessor {

    static final Logger log = new Logger(CreateNotification.class);

    private static ApplicationContext ctx = null;

    private VoicelinkNotificationUtil voicelinkNotificationUtil = null;

    private TagManager tagManager = null;

    /**
     * Default constructor.
     *
     */
    public CreateNotification() {

    }

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext appCtx)
            throws BeansException {
        this.ctx = appCtx;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor
     * #postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
            throws BeansException {
        // Just implementing this causes the static ctx variable to be set,
        // because an instance is created by Spring.
    }

    /**
     *
     * @return voicelinkNotificationUtil
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        voicelinkNotificationUtil = (VoicelinkNotificationUtil) ctx
                .getBean("voicelinkNotificationUtil");
        return voicelinkNotificationUtil;
    }

    /**
     * Sets voicelinkNotificationUtil.
     * @param voicelinkNotificationUtil bean
     */
    public void setVoicelinkNotificationUtil(
        VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }

    /**
     *
     * @return tagManager
     */
    public TagManager getTagManager() {
        tagManager = (TagManager) ctx.getBean("tagManager");
        return tagManager;
    }

    /**
     * Sets tagManager.
     * @param tagManager object
     */
    public void setTagManager(TagManager tagManager) {
        this.tagManager = tagManager;
    }

    /**
     * Method to create notification.
     * @param state of the import
     * @param notificationMap of notification information
     * @param process -- either import or export
     * @param siteName -- site import/export is processing
     * @param failedValidation reason for notification
     */
    public void createNotify(InternalState state, HashMap notificationMap,
            String process, String siteName, String failedValidation) {

        // Get a notification object with host and application populated
        Notification notification = getVoicelinkNotificationUtil()
                                        .voiceLinkNotification();

        if (!state.getDescription().contains("NoWork")) {
            if (notificationMap.containsKey(state)) {
                try {
                    SiteContext siteContext = SiteContextHolder.getSiteContext();
                    if (null == siteContext) {
                        siteContext = (SiteContext)
                        ctx.getBean("siteContext");
                        SiteContextHolder.setSiteContext(siteContext);
                    }
                    if (siteContext != null) {
                        Site site = siteContext.getSiteByName(siteName);
                        siteContext.setSite(notification, site);

                        // Get the properties for the InternalState
                        NotificationProperties np = (NotificationProperties)
                                                notificationMap.get(state);

                        notification.setProcess(process);
                        notification.setErrorNumber(np.getErrorNumber());
                        notification.setMessage(np.getMessage());
                        notification.setPriority(np.getPriority());
                        notification.setCreationDateTime(new Date());
                    }

                    if (failedValidation.length() != 0) {
                        Set<NotificationDetail> nDetailList =
                            new HashSet<NotificationDetail>();
                        NotificationDetail notificationDetail =
                            new NotificationDetail();

                        notificationDetail = new NotificationDetail();
                        notificationDetail.setOrdering("0");
                        notificationDetail
                                .setKey("notification.detail.validation.name");
                        notificationDetail.setValue(failedValidation);
                        notificationDetail.setRenderType("1");
                        nDetailList.add(notificationDetail);

                        notification.setDetails(nDetailList);
                    }

                    getVoicelinkNotificationUtil()
                        .saveNotification(notification);
                } catch (Exception e) {
                    log.info(" Exception in createNotification ");
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 