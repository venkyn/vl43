/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.OutputAdapter;
import com.vocollect.voicelink.core.importer.OutputAdapterError;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Calendar;


/**
 * 
 *
 * @author jtauberg
 */
public class FileOutputAdapter extends OutputAdapter {
    
    private static final Logger log = new Logger(OutputAdapter.class);

    // Name of file we will be writing (used for Append mode).
    private String fileName = null;

    // Name of directory the output files are going to
    private String outputDirectory = null;

    // Name of directory to move file to when closed.
    private String moveToDirectory = null;
    
    //This will be the prefix of the output name passed in by the user.
    private String filenamePrefix = null;

    //This will be the extension of the output name passed in by the user.
    private String filenameExtension = null;

    /**
     * The fileMode can be either "writeUnique" (Default), "append", or "overwrite"
     * writeUnique - file is opened for overwrite, with a unique filename generated
     *               using prefix and suffix.
     * append -     file is opened for append using given filename.
     * overwrite -  file is opened for overwrite using given filename.               
     */
    private String fileMode = "writeUnique";

    
    /**
     * This is set internally when writemode is set.
     */
    private boolean appendToFile = false;

    
    /**
     *  User can set to true to automatically delete any partially created files
     *  on failure.  Default is false to leave partially created files in the
     *  output directory.
     */  
     private boolean deleteOnFailure = false;
    
    //the file object containing the directory to move the file to on successful close.
    private File fileMoveToDirectory = null;
    
    //the file object containing the output directory (before move).
    private File fileOutputDirectory = null;

    //Stores the fully qualified path (based on OutputDirectory and filename of the file being written.
    private File fileWriteFile = null;

    //Flag to denote if a new filename should be generated based on supplied
    // Prefix & Extension during the file move.  This is particularly valuable
    // when appending records to a temp file and then moving it when it is done.
    // Because to append, you need to maintain a constant filename.
    private boolean renameOnMove = false; 
    

    /**
     * Constructor.
      * @throws VocollectException this is an exception.
     */
    public FileOutputAdapter() throws VocollectException {
        super();
        this.setCurrentState(INITIALIZEDSTATE);
        return;    
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.OutputAdapter#closeOnSuccess()
     */
    @Override
    public boolean closeOnSuccess() {
        try {
            this.close();
        } catch (IOException e) {
            // Log this
            log.error("Abnormal close - close() threw an IO Exception ",
                    OutputAdapterError.ABNORMAL_CLOSE_ON_SUCCESS, e);
        }

        //now move file
        //Was the move To Directory specified?
        if (null != this.moveToDirectory) {
            try {
                if (moveFileToOutputDirectory()) {
                    //file was moved
                    return true;
                }
                //file was not moved
                log.error("Failed to move file to '"
                        + moveToDirectory + "'.",
                        OutputAdapterError.OUTPUT_FILE_NOT_MOVED);
                return false;
            } catch (IOException e) {
                log.error("Failed to move export file from "
                        + this.getDataDestinationName() + " to " + moveToDirectory + ".",
                        OutputAdapterError.OUTPUT_FILE_NOT_MOVED,
                        e);
                return false;
            }
        }
        return true;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.OutputAdapter#closeOnFailure()
     */
    @Override
    public boolean closeOnFailure() {
        try {
            this.close();
        } catch (IOException e) {
            // Log this
            log.error("Abnormal close - close() threw an IO Exception ",
                    OutputAdapterError.ABNORMAL_CLOSE_ON_SUCCESS, e);
            return false;
        }

        //do we need to delete this file?
        if (this.isDeleteOnFailure()) {
            //Try to delete the file.
            boolean success = (this.fileWriteFile.delete());
            if (!success) {
                log.error("Failed to delete file '"
                    + this.getDataDestinationName() + "' on failure.",
                    OutputAdapterError.ABNORMAL_CLOSE_ON_FAILURE);
                return false;
            }
        }
        return true;
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.OutputAdapter#openDestination()
     */
    @Override
    public boolean openDestination() throws VocollectException {
        checkConfiguration();
        
        //Based on the fileMode, check proper setup
        if (this.fileMode == "append") {
            setupOpenForAppend();
        } else if (this.fileMode == "writeUnique") {
            setupOpen();
        } else if (this.fileMode == "overwrite") {
            setupOpenForOverwrite();
        }
        
        // Open the file for writing
        try {
            OutputStream os = new FileOutputStream(fileWriteFile, this.appendToFile);
            Writer w = new OutputStreamWriter(os, this.getEncoding());
            //setInternalDataDestination(new BufferedWriter(new FileWriter(
            //    fileWriteFile, (this.appendToFile))));
            setInternalDataDestination(new BufferedWriter(w));
        } catch (IOException e) {
            // can't open the file!
            log.error("Can not open the file.:  " + this.getDataDestinationName(),
                            OutputAdapterError.OUTPUT_FILE_CAN_NOT_BE_OPENED);
            setCurrentState(OutputAdapter.DATADESTINATIONNOTREADY);
            //set isOpen to lock down changes.
            setOpen(false);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.OUTPUT_FILE_CAN_NOT_BE_OPENED,
                new UserMessage(LocalizedMessage.formatMessage(
                    OutputAdapterError.OUTPUT_FILE_CAN_NOT_BE_OPENED)));
        }
        return true;
    }


    /**
     * This is used during the openDestination.
     * It checks that an outptut directory was specified and that it exists.
     * If it does not exist, it creates it.
     * @throws VocollectException unable to perform IO operations
     */
    private void checkConfiguration() throws VocollectException {
    //Was the (Success) Output directory specified?
    if (null == this.getOutputDirectory()) {
        this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
        log.fatal("Bad configuration for " + this.getClass().getName()
                + " - Missing output directory for output of successful files.",
                OutputAdapterError.OUTPUT_DIRECTORY_NOT_SET);
        throw new OutputAdapterConfigurationException(
                   OutputAdapterError.OUTPUT_DIRECTORY_NOT_SET,
                         new UserMessage(LocalizedMessage.formatMessage(
                             OutputAdapterError.OUTPUT_DIRECTORY_NOT_SET)));
    }
    fileOutputDirectory = new File(this.outputDirectory);

    //Does the (Success) Output directory exist?  If not try to create.
    if (!fileOutputDirectory.exists()) {
        //Create Output directory tree since it does not exist
        if (!fileOutputDirectory.mkdirs()) {
            log.fatal(this.getClass().getName()
                + " - requested output directory could not be created.",
                OutputAdapterError.CREATE_OUTPUT_DIRECTORY_FAILED);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.CREATE_OUTPUT_DIRECTORY_FAILED,
                             new UserMessage(LocalizedMessage.formatMessage(
                                  OutputAdapterError.CREATE_OUTPUT_DIRECTORY_FAILED)));
        }
    }
}
     
    
    /**
     * This is used before opening a new file (writeUnique).
     * It checks that a filename prefix and suffix were passed,
     * creates a filename with the date and time, sets the file open to true,
     * and points the file object fileWriteFile to the created filename in
     * the output directory.  It also sets the OutputAdapter's DataDestinationName.
     * @throws VocollectException Bad Config filename prefix and extension is not provided 
     */
    private void setupOpen() throws VocollectException {
        //Is there a filename prefix?
        if (null == this.filenamePrefix) {
            this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
            log.fatal("Bad configuration for " + this.getClass().getName()
                    + " - When creating new output files (writeUnique), a file "
                    + "prefix is required.", OutputAdapterError.FILENAME_PREFIX_NOT_SET);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.FILENAME_PREFIX_NOT_SET,
                             new UserMessage(LocalizedMessage.formatMessage(
                                 OutputAdapterError.FILENAME_PREFIX_NOT_SET)));
        }
        //Is there a filename extension?
        if (null == this.filenameExtension) {
            this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
            log.fatal("Bad configuration for " + this.getClass().getName()
                    + " - When creating new output files (writeUnique), a file "
                    + "extension is required."
                    , OutputAdapterError.FILENAME_EXTENSION_NOT_SET);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.FILENAME_EXTENSION_NOT_SET,
                             new UserMessage(LocalizedMessage.formatMessage(
                                 OutputAdapterError.FILENAME_EXTENSION_NOT_SET)));
        }
        //set isOpen to lock down changes.
        setOpen(true);
        //Create a filename string.
        this.fileName = this.buildFilename();
        //create a file that includes the path and filename.
        fileWriteFile = new File(fileOutputDirectory, this.fileName);
        this.setDataDestinationName(fileWriteFile.toString());
    }   

    
    /**
     * This is used before opening a file for append.
     * It checks that a filename was passed, sets the file open to true,
     * and points the file object fileWriteFile to the selected file in
     * the output directory.  It also sets the OutputAdapter's DataDestinationName.
     * @throws VocollectException - bad config file name or file name prefix not provided
     */
    private void setupOpenForAppend() throws VocollectException {
        //Require a filename.    
        if (null == this.fileName) {
            this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
            log.fatal("Bad configuration for " + this.getClass().getName()
                    + " - When appending output files, a fileName is required.",
                    OutputAdapterError.FILENAME_NOT_SET);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.FILENAME_NOT_SET,
                             new UserMessage(LocalizedMessage.formatMessage(
                                 OutputAdapterError.FILENAME_NOT_SET)));
        }
        if (this.isRenameOnMove()) {
            //Is there a filename prefix?
            if (null == this.filenamePrefix) {
                this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
                log.fatal("Bad configuration for " + this.getClass().getName()
                        + " - When specifying RenameOnMove, a file prefix is required.",
                        OutputAdapterError.FILENAME_PREFIX_NOT_SET2);
                throw new OutputAdapterConfigurationException(
                    OutputAdapterError.FILENAME_PREFIX_NOT_SET2,
                                 new UserMessage(LocalizedMessage.formatMessage(
                                     OutputAdapterError.FILENAME_PREFIX_NOT_SET2)));
            }
            //Is there a filename extension?
            if (null == this.filenameExtension) {
                this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
                log.fatal("Bad configuration for " + this.getClass().getName()
                        + " - When specifying RenameOnMove, a file extension is required.",
                        OutputAdapterError.FILENAME_EXTENSION_NOT_SET2);
                throw new OutputAdapterConfigurationException(
                    OutputAdapterError.FILENAME_EXTENSION_NOT_SET2,
                                 new UserMessage(LocalizedMessage.formatMessage(
                                     OutputAdapterError.FILENAME_EXTENSION_NOT_SET2)));
            }
        }
        //set isOpen to lock down changes.
        setOpen(true);
        
        //create a file that includes the path and filename.
        fileWriteFile = new File(fileOutputDirectory, this.fileName);
        this.setDataDestinationName(fileWriteFile.toString());
    }
    
    
    
    /**
     * This is used before opening a filename with fileMode overwrite.
     * It checks that a filename was passed, sets the file open to true,
     * and points the file object fileWriteFile to the selected file in
     * the output directory.  It also sets the OutputAdapter's DataDestinationName.
     * 
     * Note that this will be destructive to any existing files and therefore
     * should not be used for exporting.  It may be used as a dummy process.
     * @throws VocollectException - bad config file name or file name prefix or
     * extension not provided
     */
    private void setupOpenForOverwrite() throws VocollectException {
        //Require a filename.    
        if (null == this.fileName) {
            this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
            log.fatal("Bad configuration for " + this.getClass().getName()
                    + " - When overwriting output files (overwrite), a fileName is required.",
                    OutputAdapterError.FILENAME_NOT_SET2);
            throw new OutputAdapterConfigurationException(
                OutputAdapterError.FILENAME_NOT_SET2,
                             new UserMessage(LocalizedMessage.formatMessage(
                                 OutputAdapterError.FILENAME_NOT_SET2)));
        }
        if (this.isRenameOnMove()) {
            //Is there a filename prefix?
            if (null == this.filenamePrefix) {
                this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
                log.fatal("Bad configuration for " + this.getClass().getName()
                        + " - When specifying RenameOnMove, a file prefix is required.",
                        OutputAdapterError.FILENAME_PREFIX_NOT_SET3);
                throw new OutputAdapterConfigurationException(
                    OutputAdapterError.FILENAME_PREFIX_NOT_SET3,
                                 new UserMessage(LocalizedMessage.formatMessage(
                                     OutputAdapterError.FILENAME_PREFIX_NOT_SET3)));
            }
            //Is there a filename extension?
            if (null == this.filenameExtension) {
                this.setCurrentState(FileOutputAdapter.MISCONFIGUREDSTATE);
                log.fatal("Bad configuration for " + this.getClass().getName()
                        + " - When specifying RenameOnMove, a file extension is required.",
                        OutputAdapterError.FILENAME_EXTENSION_NOT_SET3);
                throw new OutputAdapterConfigurationException(
                    OutputAdapterError.FILENAME_EXTENSION_NOT_SET3,
                                 new UserMessage(LocalizedMessage.formatMessage(
                                     OutputAdapterError.FILENAME_EXTENSION_NOT_SET3)));
            }
        }
        //set isOpen to lock down changes.
        setOpen(true);
        
        //create a file that includes the path and filename.
        fileWriteFile = new File(fileOutputDirectory, this.fileName);
        this.setDataDestinationName(fileWriteFile.toString());
    }

    
    /**
     * 
     * @return String filename consisting of filename prefix concatenated with
     * date and time based unique identifier (20041225132533 example for
     * December 25, 2004 at 1:25:33 PM) concatenated with filename extension.
     */
    private String buildFilename() {
        String dateTimeCode;
        String tempstr;
        Integer iMonth;
        Calendar now = Calendar.getInstance();
        now.setLenient(false);

        //Add year
        dateTimeCode = Integer.toString(now.get(Calendar.YEAR));
        
        //Add month digit padded on left with a zero to 2 chars.  Note that month is off by 1.
        iMonth = now.get(Calendar.MONTH) + 1;
        tempstr = "0" + Integer.toString(iMonth);
        dateTimeCode = dateTimeCode + tempstr.substring((tempstr.length() - 2), (tempstr.length()));
        
        //Add day digit padded on left with a zero to 2 chars.
        tempstr = "0" + Integer.toString(now.get(Calendar.DAY_OF_MONTH));
        dateTimeCode = dateTimeCode + tempstr.substring((tempstr.length() - 2), (tempstr.length()));
        
        //Add Hour(24 hr format) digit padded on left with a zero to 2 chars.
        tempstr = "0" + Integer.toString(now.get(Calendar.HOUR_OF_DAY));
        dateTimeCode = dateTimeCode + tempstr.substring((tempstr.length() - 2), (tempstr.length()));
        
        //Add Minute digit padded on left with a zero to 2 chars.
        tempstr = "0" + Integer.toString(now.get(Calendar.MINUTE));
        dateTimeCode = dateTimeCode + tempstr.substring((tempstr.length() - 2), (tempstr.length()));

        //Add Second digit padded on left with a zero to 2 chars.
        tempstr = "0" + Integer.toString(now.get(Calendar.SECOND));
        dateTimeCode = dateTimeCode + tempstr.substring((tempstr.length() - 2), (tempstr.length()));
        
        return this.filenamePrefix + dateTimeCode + this.filenameExtension;
    }
    
    
    /**
     * Move the current file to the output directory which must already be defined.
     * 
     * @return true if successful
     * @throws IOException
     *             if problems with the move.
     */
    private boolean moveFileToOutputDirectory() throws IOException {

    fileMoveToDirectory = new File(this.getMoveToDirectory());

        //Does the Move To directory exist?  If not try to create.
        if (!fileMoveToDirectory.exists()) {
            //Create directory tree since it does not exist
            if (!fileMoveToDirectory.mkdirs()) {
                log.fatal(this.getClass().getName()
                    + " - requested Move To directory could not be created.",
                    OutputAdapterError.CREATE_MOVE_TO_DIRECTORY_FAILED);
            throw new RuntimeException(this.getClass().getName()
                    + " - Could not create requested Move To directory.");
            }
        }
        
        // If we are renaming the file on move, 
        if (this.renameOnMove) {
            //Create a new filename string based on Prefix, date & Extension.
            this.fileName = this.buildFilename();
        }
        
        //create handle to new file in MoveToDirectory with same name as current fileWriteFile
        File moveTo = new File(this.fileMoveToDirectory, this.fileName);
        
        //try and rename (move) the current fileWriteFile to the MoveTo file.
        boolean result = this.fileWriteFile.renameTo(moveTo);
        if (result) {
           log.info("Saved file " + fileWriteFile.getName() + " to "
                   + moveTo.getCanonicalPath(),
                   OutputAdapterError.OUTPUT_FILE_MOVED);
           return true;
        }
        log.error("Failed to save file " + fileWriteFile.getName() + " to "
               + moveTo.getCanonicalPath(),
               OutputAdapterError.OUTPUT_FILE_NOT_MOVED);
        return false;
    }
    
    
    /**
     * Getter for the fileName property.
     * @return String value of the property
     */
    public String getFileName() {
        return this.fileName;
    }

    
    /**
     * Setter for the fileName property.
     * Note:  this may not be set while file is open.
     * @param fileName the new fileName value
     */
    public void setFileName(String fileName) {
        if (!this.isOpen()) {
            this.fileName = fileName;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - Filename may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - Filename may not be changed while file is open.");
        }
    }

    
    /**
     * Getter for the filenameExtension property.
     * @return String value of the property
     */
    public String getFilenameExtension() {
        return this.filenameExtension;
    }

    
    /**
     * Setter for the filenameExtension property.
     * Note:  this may not be set while file is open.
     * @param filenameExtension the new filenameExtension value
     */
    public void setFilenameExtension(String filenameExtension) {
        if (!this.isOpen()) {
            this.filenameExtension = filenameExtension;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - Filename Extension may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR2);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - Filename Extension may not be changed while file is open.");
        }
    }

    
    /**
     * Getter for the filenamePrefix property.
     * @return String value of the property
     */
    public String getFilenamePrefix() {
        return this.filenamePrefix;
    }

    
    /**
     * Setter for the filenamePrefix property.
     * Note:  this may not be set while file is open.
     * @param filenamePrefix the new filenamePrefix value
     */
    public void setFilenamePrefix(String filenamePrefix) {
        if (!this.isOpen()) {
            this.filenamePrefix = filenamePrefix;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - Filename Prefix may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR3);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - Filename Prefix may not be changed while file is open.");
        }
    }


// This is now done internally based on fileMode
//    
//    /**
//     * Getter for the appendToFile property.
//     * @return boolean value of the property
//     */
//    public boolean isAppendToFile() {
//        return this.appendToFile;
//    }
//
//    
//    /**
//     * Setter for the appendToFile property.
//     * Note:  this may not be set while file is open.
//     * @param appendToFile the new appendToFile value
//     */
//    public void setAppendToFile(boolean appendToFile) {
//        if (!this.isOpen()) {
//            this.appendToFile = appendToFile;
//        } else {
//            log.fatal("Interface sequence error on " + this.getClass().getName()
//                + " - appendToFile may not be changed while file is open.",
//                OutputAdapterError.INTERFACE_SEQUENCE_ERROR4);
//        throw new RuntimeException("Interface sequence error for "
//                + this.getClass().getName()
//                + " - appendToFile may not be changed while file is open.");
//        }
//    }

    
    /**
     * Getter for the outputDirectory property.
     * @return String value of the property
     */
    public String getOutputDirectory() {
        return this.outputDirectory;
    }

    
    /**
     * Setter for the outputDirectory property.
     * Note:  this may not be set while file is open.
     * @param outputDirectory the new outputDirectory value
     */
    public void setOutputDirectory(String outputDirectory) {
        if (!this.isOpen()) {
            this.outputDirectory = outputDirectory;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - Output Directory may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR5);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - Output Directory may not be changed while file is open.");
        }
    }

    
    /**
     * Getter for the moveToDirectory property.
     * @return String value of the property
     */
    public String getMoveToDirectory() {
        return moveToDirectory;
    }

    
    /**
     * Setter for the moveToDirectory property.
     * Note:  this may not be set while file is open.
     * @param moveToDirectory the new moveToDirectory value
     */
    public void setMoveToDirectory(String moveToDirectory) {
        if (!this.isOpen()) {
            this.moveToDirectory = moveToDirectory;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - moveToDirectory may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR6);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - moveToDirectory may not be changed while file is open.");
        }
    }

    
    /**
     * Getter for the deleteOnFailure property.
     * @return boolean value of the property
     */
    public boolean isDeleteOnFailure() {
        return this.deleteOnFailure;
    }

    
    /**
     * Setter for the deleteOnFailure property.
     * @param deleteOnFailure the new deleteOnFailure value
     */
    public void setDeleteOnFailure(boolean deleteOnFailure) {
        if (!this.isOpen()) {
            this.deleteOnFailure = deleteOnFailure;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - deleteOnFailure may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR7);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - deleteOnFailure may not be changed while file is open.");
        }
    }

    
    /**
     * Getter for the renameOnMove property.
     * @return boolean value of the property
     */
    public boolean isRenameOnMove() {
        return this.renameOnMove;
    }

    
    /**
     * Setter for the renameOnMove property.
     * @param renameOnMove the new renameOnMove value
     */
    public void setRenameOnMove(boolean renameOnMove) {
        if (!this.isOpen()) {
            this.renameOnMove = renameOnMove;
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - renameOnMove may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR8);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - renameOnMove may not be changed while file is open.");
        }
    }
    
    
    /**
     * @return the fileMode.  Either "writeUnique" "append" or "overwrite"
     */
    public String getFileMode() {
        return fileMode;
    }

    
    /**
     * The fileMode can be either "writeUnique" (Default), "append", or "overwrite"
     * writeUnique - file is opened for overwrite, with a unique filename generated
     *               using prefix and suffix.
     * append -     file is opened for append using given filename.
     * overwrite -  file is opened for overwrite using given filename.               
     *
     * @param fileMode the fileMode to set
     */
    public void setFileMode(String fileMode) {
        //If file is not open, the you can set this
        if (!this.isOpen()) {
            //Is fileMode set to a valid mode?
            if (fileMode.equalsIgnoreCase("writeunique")) {
                //Valid mode, so set internal fileMode and appendToFile
                this.fileMode = "writeUnique";
                this.appendToFile = false;
            } else if (fileMode.equalsIgnoreCase("overwrite")) {
                //Valid mode, so set internal fileMode and appendToFile
                this.fileMode = "overwrite";
                this.appendToFile = false;
            } else if (fileMode.equalsIgnoreCase("append")) {
                //Valid mode, so set internal fileMode and appendToFile
                this.fileMode = "append";
                this.appendToFile = true;
            } else {
                //Invalid mode throw error!
                if (fileMode == null) {
                    //since fileMode was null, replace with a nice string for
                    // error output
                    fileMode = "<null>";
                }
                log.fatal(this.getClass().getName()
                    + " - requested file mode: " + fileMode + " not supported."
                    + "  Valid modes: writeUnique, append, or overwrite"
                    , OutputAdapterError.INVALID_FILE_MODE_SPECIFIED);
            throw new RuntimeException(this.getClass().getName()
                    + " - requested file mode: " + fileMode + " not supported.  Valid modes: writeUnique,"
                    + " append, or overwrite");
            }
        } else {
            //File was open, so complain about sequence.
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - setFileMode may not be changed while file is open.",
                OutputAdapterError.INTERFACE_SEQUENCE_ERROR9);
        throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - setFileMode may not be changed while file is open.");
        }
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 