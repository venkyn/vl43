/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceAdapter;
import com.vocollect.voicelink.core.importer.DataSourceAdapterError;
import com.vocollect.voicelink.core.importer.ImportConfigurationException;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.NoWorkException;


import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * This is a kind of a bastardization of the abstract DataSourceAdapter.
 * This, when openSource is called, opens the directory and generates a list of files to delete.
 * ReadRecord has been overridden to actually delete the files. 
 * 
 * This will only get one file, 
 *
 * @author dgold
 */
public class FilePurgeAdapter extends DataSourceAdapter {

    private static final Logger log = new Logger(FilePurgeAdapter.class);

    // A day, in seconds
    private static final long ONE_DAY = 86400;

    // A second, in milliseconds
    private static final long MILLISECONDS = 1000;

    private String fileName = null;

    // Name of file we are currently attached to.

    private String parentDirectory = null;

    // Name of directory the input files are coming from
    
    private List<String> parentDirectories = new LinkedList<String>();

    private String fileNamePattern = null;

    // regexp pattern for the files we should be looking for.



    // Internal character buffer

    private FilenameFilter myWork = null;

    
    // Cutoff, in days old, for the file to be purged. If the file is older than 
    // purgeCutoff days old, it is eligible to be purged.
    private long purgeCutoff = 0L;
    
    // This is the time - in the same format as the last modified date on the file - 
    // manufactured from the purgeCutoff. now - purgeCutoff * 86400 * 1000
    private long lastModifiedPurgeCutoff = 0L;

    
    //Stores sorted list of files to process for this importer run.
    private LinkedList <File> candidates = null;
    
    //Stores path of file we are working on
    private String nextFileToImport = null;

    
    /**
     * This class is a filename filter that uses regexp to filter out files we
     * are not interested in.
     */
    private class RegexpFilenameFilter implements FilenameFilter {

        private Pattern filenamePattern;

        private Matcher filenameMatcher;

        private File parentDirectory;

        /**
         * Constructor - set the directory, and the filename pattern. Then call
         * setPattern.
         *
         *@param parentDirectory
         *           is the parent directory to set the pattern to
         *@param filenamePattern
         *           is the filename pattern to use
         *@throws ConfigurationException
         *            if the regex blows up
         */
        public RegexpFilenameFilter(String parentDirectory,
                                    String filenamePattern)
            throws ConfigurationException {
            super();
            setPattern(parentDirectory, filenamePattern);
        }

        /**
         *Setup to match the directory and filename pattern. Compiles the
         *regexp that filters out unwanted filenames from the listing of the
         *directory.
         *
         *@param parentDirectoryP
         *           is the parent directory to use
         *@param filenamePatternP
         *           to set the regex pattern
         *@throws ConfigurationException
         *            if the regex blows up
         */
        public void setPattern(String parentDirectoryP, String filenamePatternP)
            throws ConfigurationException {
            // Create a pattern to match breaks
            try {
                this.filenamePattern = Pattern.compile(
                    filenamePatternP, Pattern.CASE_INSENSITIVE);
            } catch (PatternSyntaxException e) {
                FilePurgeAdapter.getLog().fatal(
                    "Cannot compile input filename pattern.",
                    DataSourceAdapterError.BAD_FILENAME_PATTERN);
                throw new ConfigurationException(
                    DataSourceAdapterError.BAD_FILENAME_PATTERN,
                    new UserMessage(
                        LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                .BAD_FILENAME_PATTERN)));
            }
            // Split input with the pattern
            this.parentDirectory = new File(parentDirectoryP);
        }

        /**
         *{@inheritDoc}
         *
         *@see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
         */
        public boolean accept(File dir, String name) {
            this.filenameMatcher = this.filenamePattern.matcher(name);
            if (0 == dir.compareTo(this.parentDirectory)) {
                if (this.filenameMatcher.matches()) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     *Class used to sort the files by date-time, oldest on top (ascending).
     *
     *@author dgold
     */
    private class SortByFileDate implements Comparator<File> {

        /**
         *Constructor.
         */
        public SortByFileDate() {
            super();
        }

        /**
         *{@inheritDoc}
         *
         *@see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(File f1, File f2) {
            long dateDiff = f1.lastModified() - f2.lastModified();

            if (dateDiff <= 0) {
                if (dateDiff < 0) {
                    return -1;
                }
                return 0;
            }
            return 1;
        }

    }


    /**
     *default constructor. Calls init()
     *
     *@throws VocollectException
     *            if the FileDataSourceAdapter does not initialize
     */
    public FilePurgeAdapter() throws VocollectException {
        super();
        this.setCurrentState(INITIALIZEDSTATE);
        try {
            String thePath = new File("./").getCanonicalPath();
            log.info("The current working directory is " + thePath);
        } catch (IOException e) {
            e.printStackTrace();
            log.info("Cannot find current working directory.");
        }
        return;
    }


    /**
     *This method selects the 'next' file to work on, according to the filename
     *pattern given and deletes it. 
     *
     *@see com.vocollect.voicelink.core.importer.DataSourceAdapter#openSource()
     *@return true if the file could be opened, false OW.
     *@throws VocollectException for a bad configuration,
     *an invalid parent directory,
     *if the next file is null and no work is to be done,
     *or if the datasource file
     *cannot be found
     */
    @Override
    public boolean openSource() throws VocollectException {

        candidates = null;
//        this.setCurrentLineNumber(0);
        // Make sure the parent directory is set
        if (0 >= parentDirectories.size()) {
            return false;
        }
        setParentDirectory(parentDirectories.get(0));
        if (0 < parentDirectories.size()) {
            parentDirectories.remove(0);
        }
        if (null == this.getParentDirectory()) {
            this.setCurrentState(FileDataSourceAdapter.MISCONFIGUREDSTATE);
            log.fatal("Bad configuration for " + this.getClass().getName() 
                    + " - Missing parent directory for input files.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_NOT_SET);
            throw new RuntimeException("Bad configuration for " + this.getClass().getName()
                    + " - Missing parent directory for input files.");
        }
        File importDirectory = new File(this.getParentDirectory());

        // Make sure the directory exists
        if (!importDirectory.exists()) {
            log.error(
                    "Parent directory '" + getParentDirectory()
                    + "' does not exist.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST);
            setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
            throw new ImportConfigurationException(
                    DataSourceAdapterError.INVALID_PARENT_DIRECTORY,
                new UserMessage(
                    LocalizedMessage
                        .formatMessage(DataSourceAdapterError
                            .INVALID_PARENT_DIRECTORY)));
        }


        // Get the name of the next file to import. If the directory doesn't
        // exist, its a config error
        try {
            this.nextFileToImport = determineNextFileToProcess();
            setFileName(this.nextFileToImport);
            this.setDescription(getDataSourceName());
        } catch (ConfigurationException e1) {
            log.error(
                    "Unable to set next file name. ",
                    DataSourceAdapterError.UNABLE_TO_SET_NEXT_FILE_NAME);
            setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
            return false;
        }

        // If the next file is null, there is no work.
        if ((0 >= parentDirectories.size()) && (null == this.nextFileToImport)) {
            log.info("No work to do. ", DataSourceAdapterError.NO_WORK);
            setCurrentState(DataSourceAdapter.NOWORK);
            throw new NoWorkException(
                    DataSourceAdapterError.NO_WORK, new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError.NO_WORK)));
        } 
        
        // Set the name (which in turn sets the full name of the file)
        setFileName(this.nextFileToImport);

        return true;
    }

    /**
     * This resets the CurrentLineNumber to zero and calls the following.
     * @see com.vocollect.voicelink.core.importer.DataSourceAdapter#close()
     * @throws IOException when file close fails.
     **/
    @Override
    public void close() throws IOException {
        setOpen(false);
    }

    /**
     *Close with a successful state. This, for the current system, involves
     *moving the file to a successfully-completed directory.
     *
     *@return true if this operation is successful.
     */
    @Override
    public boolean closeOnSuccess() {
        setOpen(false);
        
        return true;
    }

    
    /**
     *Close with a failed state. This, for the current system, involves moving
     *the file to an unsuccessfully-completed directory.
     *
     *@return true if this operation is successful.
     */
    @Override
    public boolean closeOnFailure() {
        setOpen(false);
        
        return true;
    }

    
    /**
     *get the name of the file we are currently attached to.
     *
     *@return the filename
     */
    private String getFileName() {
        return this.fileName;
    }

    /**
     *Set the name fo the file we are currently attached to. Set the data
     *source name as well.
     *
     *@param fileName
     *           the filename to set
     */
    private void setFileName(String fileName) {
        this.fileName = fileName;
        this.setDataSourceName(getFullFilename());
    }

    /**
     *get the name of the directory that the input files should be in.
     *
     *@return name of the directory that the input files should be in
     */
    public String getParentDirectory() {
        return this.parentDirectory;
    }

    /**
     *set the name of the directory that the input files should be in.
     *
     *@param parentDirectoryP
     *           sets the parentDirectory string
     *@throws ConfigurationException
     *            if an error is found
     */
    private void setParentDirectory(String parentDirectoryP)
        throws ConfigurationException {
        if ((null == parentDirectoryP) || parentDirectoryP.equals("")) {
            log
            .fatal(
                "Import directory is supplied as empty string or null string",
                DataSourceAdapterError.IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING);
        setCurrentState(MISCONFIGUREDSTATE);
        throw new ConfigurationException(
            DataSourceAdapterError.IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING,
            new UserMessage(
                LocalizedMessage
                    .formatMessage(DataSourceAdapterError
                        .IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING)));
        }

        try {
            File thePath = new File(parentDirectoryP);
            if (!thePath.isDirectory()) {
                thePath.mkdirs();
            }
            this.parentDirectory = thePath.getCanonicalPath();
            this.setDataSourceName(getFullFilename());
            if (null != getFileNamePattern()) {
                this.myWork = new RegexpFilenameFilter(
                    parentDirectoryP, getFileNamePattern());
            }
            log.info("The current parent directory has been set to "
                    + thePath.getCanonicalPath());
        } catch (IOException e) {
            log.fatal(
                "Cannot find parent directory. The input supplied for parentDirectory is "
                + parentDirectoryP,
                DataSourceAdapterError.INVALID_PARENT_DIRECTORY);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                DataSourceAdapterError.INVALID_PARENT_DIRECTORY,
                new UserMessage(
                    LocalizedMessage
                        .formatMessage(DataSourceAdapterError
                            .INVALID_PARENT_DIRECTORY)));
        }
    }

    /**
     *get the full name of the input file, including path.
     *
     *@return the fullfilename
     */
    public String getFullFilename() {
        return this.getParentDirectory() + File.separator + this.getFileName();
    }

    /**
     *Using the parent directory and the filename pattern, generate and set the
     *'next' file to process. The next file will be the oldest file found in
     *the parent directory which matches the pattern.
     *
     *@return the name of the next file to process.
     *@throws ConfigurationException
     *            if the process blows up.
     */
    @SuppressWarnings("unchecked")
    private String determineNextFileToProcess() throws ConfigurationException {
        // open parent directory
        File sourceDirectory = new File(this.getParentDirectory());
        setFileNamePattern(getFileNamePattern());
        try {
            if (!sourceDirectory.isDirectory()) {
                sourceDirectory.mkdirs();
                log.info("Current parent directory created again while determining the " 
                    + "next file to process");
                log.info("The current parent directory has been set to "
                    + sourceDirectory.getCanonicalPath());
            }            
        } catch (IOException e) {
            log.fatal(
                "Cannot create parent directory.  ",
                DataSourceAdapterError.PARENT_DIRECTORY_COULD_NOT_BE_CREATED);
            throw new ConfigurationException(
                DataSourceAdapterError.PARENT_DIRECTORY_COULD_NOT_BE_CREATED,
                new UserMessage(
                    LocalizedMessage
                        .formatMessage(DataSourceAdapterError
                            .PARENT_DIRECTORY_COULD_NOT_BE_CREATED)));
        }
        if (null == sourceDirectory) {
            log
                .error(
                    "No source directory (null) when "
                    + "determining next file to process.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST2);
            throw new ConfigurationException(
                DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST2,
                new UserMessage(
                    LocalizedMessage
                        .formatMessage(DataSourceAdapterError
                            .SOURCE_DIRECTORY_DOES_NOT_EXIST2)));
        }
        if (!sourceDirectory.isDirectory()) {
            log
                .error(
                    "Source directory is not a directory "
                    + "when determining next file to process.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_NOT_A_DIRECTORY);
            throw new ConfigurationException(
                DataSourceAdapterError.SOURCE_DIRECTORY_NOT_A_DIRECTORY,
                new UserMessage(
                    LocalizedMessage
                        .formatMessage(DataSourceAdapterError
                            .SOURCE_DIRECTORY_NOT_A_DIRECTORY)));
        }

        try {
            log.debug("looking at " + sourceDirectory.getCanonicalPath());
        } catch (IOException e) {
            // This should never occur
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        //If our list of candidate files is not set up yet, set it up.
        if (candidates == null) {
            this.candidates = new LinkedList <File>(); 
            // Get of list of files from source directory that match filename
            //pattern defined in this.myWork and load the candidates array
            //with the list of files to process in this instance of Adapter.

            File[] fileArray  = sourceDirectory.listFiles(this.myWork);
            log.debug("Found " + fileArray.length + " files");
            // sort on date
            Arrays.sort(fileArray, new SortByFileDate());
            //put the list of files into the candidates ArrayList
            long currentTime = Calendar.getInstance().getTimeInMillis();
            for (int x = 0; x < fileArray.length; x++) {
                if (fileArray[x].lastModified() > (currentTime - lastModifiedPurgeCutoff)) {
                    continue;
                }
                // Add the real files, not the directories.
                if (!fileArray[x].isDirectory()) {
                    this.candidates.add(fileArray[x]);
                }
            }
        }

        if (0 < candidates.size()) {
            log.debug("Returning " + candidates.get(0).getName());
            return candidates.get(0).getName();
        }
        return null;

    }
    /**
     *Get the pattern used to find files we are interested in.
     *
     *@return Returns the fileNamePattern.
     */
    public String getFileNamePattern() {
        return this.fileNamePattern;
    }

    /**
     *Set the pattern used to identify the files to process.
     *
     *@param fileNamePattern
     *           The fileNamePattern to set.
     *@throws ConfigurationException
     *            if problems in setting the regex FileName pattern
     */
    public void setFileNamePattern(String fileNamePattern)
        throws ConfigurationException {
        this.fileNamePattern = fileNamePattern;
        if (null != getParentDirectory()) {
            this.myWork = new RegexpFilenameFilter(
                getParentDirectory(), fileNamePattern);
        }
    }

    /**
     *@return Returns the log.
     */
    private static Logger getLog() {
        return FilePurgeAdapter.log;
    }


    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public boolean isEndOfData() {
        return ((null == candidates) || (0 == candidates.size()));
    }


    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public boolean isEndOfRecord() {
        return ((null == candidates) || (0 == candidates.size()));
    }


    /**
     * This is part of the bastardization. 
     * I suppose, looked at strictly, we 'read' a record from the directory, and act on it.
     * 
     * {@inheritDoc}
     * @return
     */
    @Override
    public boolean readRecord()  {
        
        if ((null == candidates) || (0 >= candidates.size())) {
            candidates = null;
            return false;   // Equivalent of out-of-data
        }
        // Get the next file from the list of files to delete
        File deleteCandidate = candidates.get(0);
        setFileName(deleteCandidate.getName());
        this.setDescription(getDataSourceName());
        this.setCurrentLineNumber(this.getCurrentLineNumber() + 1);

        try {
            deleteCandidate.delete();
            candidates.remove(0);
        } catch (SecurityException e) {
            log.error("Insufficient privileges to delete file '" + getDataSourceName(),
                       DataSourceAdapterError.INSUFFICIENT_PRIVILEGES_FOR_DELETE, e);
            setCurrentState(DataSourceAdapter.MISCONFIGUREDSTATE);
            return false;
        }

        return true;
    }


    /**
     * Gets the value of purgeCutoff.
     * @return the purgeCutoff
     */
    public long getPurgeCutoff() {
        return this.purgeCutoff;
    }


    /**
     * Sets the value of the purgeCutoff.
     * @param purgeCutoff the purgeCutoff to set
     */
    public void setPurgeCutoff(long purgeCutoff) {
        this.purgeCutoff = purgeCutoff;
        setLastModifiedPurgeCutoff(purgeCutoff * ONE_DAY * MILLISECONDS);
    }


    /**
     * Gets the value of lastModifiedPurgeCutoff.
     * @return the lastModifiedPurgeCutoff
     */
    public long getLastModifiedPurgeCutoff() {
        return this.lastModifiedPurgeCutoff;
    }


    /**
     * Sets the value of the lastModifiedPurgeCutoff.
     * @param lastModifiedPurgeCutoff the lastModifiedPurgeCutoff to set
     */
    public void setLastModifiedPurgeCutoff(long lastModifiedPurgeCutoff) {
        this.lastModifiedPurgeCutoff = lastModifiedPurgeCutoff;
    }

    /**
     * Add a directory to this thing's list of dierctories to purge.
     * @param directory the string representing the dir to purge.
     */
    public void addDirectory(String directory) {
        if ((directory.length() - 1) > (directory.lastIndexOf(File.separator))) {
            directory += File.separator;
        }
        parentDirectories.add(directory);
    }


    /**
     * Gets the value of parentDirectories.
     * @return the parentDirectories
     */
    public List<String> getParentDirectories() {
        return this.parentDirectories;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 