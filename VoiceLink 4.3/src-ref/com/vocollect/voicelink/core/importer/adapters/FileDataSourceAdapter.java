/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceAdapter;
import com.vocollect.voicelink.core.importer.DataSourceAdapterError;
import com.vocollect.voicelink.core.importer.EncodingSupport;
import com.vocollect.voicelink.core.importer.ImportConfigurationException;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.NoWorkException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.nio.CharBuffer;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * This is the realization of the abstract DataSourceAdapter that talks to
 * files. This is either configured, or misconfigured, mostly. Misconfigured
 * should not block execution, as that concerns missing directories ONLY, and
 * those can be added. Each time this thing opens a file, it needs to check for
 * existing input directory but will create the move-to directories at need. So
 * I guess it's only one missing directory.
 * 
 * This will only get one file, 
 *
 * @author dgold
 */
public class FileDataSourceAdapter extends DataSourceAdapter {

    private static final Logger log = new Logger(FileDataSourceAdapter.class);

    private String fileName = null;

    // Name of file we are currently attached to.

    private String parentDirectory = null;

    // Name of directory the input files are coming from

    private String fileNamePattern = null;

    // regexp pattern for the files we should be looking for.

    private final String defaultSuccessDirectory = "./data/import/successfulImport";

    // Default destination for the files that are successfully imported.

    private final String defaultFailureDirectory = "./data/import/failedImport";

    // Default destination for the files that are unsuccessfully imported.

    private String successDirectory = this.defaultSuccessDirectory;

    // Actual destination for the files that are successfully imported.

    private String failureDirectory = this.defaultFailureDirectory;

    // Actual destination for the files that are unsuccessfully imported.

    private boolean moveFilesOnCompletion = true;

    // Flag indicating that files should be moved after import.

    private static final int DEFAULTLINELENGTH = 5120;

    // Default length of the buffer

    private int lineLength = FileDataSourceAdapter.DEFAULTLINELENGTH;

    // Length of the buffer to read from the file into

    private boolean eof = true;

    // Flag indicating we hit EOF

    private CharBuffer currentLine = null;

    // Internal character buffer

    private FilenameFilter myWork = null;

    //Flag to indicate the first record read from the source input

    private boolean firstRecord = true;

    //Stores the fully qualified path (based on OutputDirectory and filename of the file being written.
    private File fileWriteFile = null;

    //Stores count of lines read that are to be sorted.
    private Long sortLinesRead = 0L;

    // Flag to indicate that at least one file wa sfound to work on...
    private boolean foundWork = false;


    /**
     * This flag determines if OpenSource() will open the next file in the
     * parent directory, or use the existing file.
     * 
     * When instantiated, the FileDataSourceAdapter's inProgress flag is set to
     * false. When OpenSource is called the flag is set to true.
     * When CloseOnSuccess or CloseOnFailure is called, the flag is reset to false.
     */
    private boolean inProgress = false;

    //Flag to indicate if existing sorted file should be used, or a new one created?
    private boolean useExistingSortedFile = false;


    //random access file used to open intermediate UTF16 file for read.
    private RandomAccessFile raf = null;

    //Stores if we want to keep sorted file for debugging purposes.
    private boolean keepSortedFile = false;

    //Stores if we want to keep IntermediateUTF16 file for debugging purposes.
    private boolean keepIntermediateUTF16File = false;

    //stores the original file's name before sorting.
    private String originalFileName = null;

    // Original file representation for the input data
    private TextFile internalSourceFile = null;

    // File representation for the UTF-16 file
    private TextFile intermediateUTF16File = null; 

    // File representation for the sorted UTF-8 file.
    private TextFile sortedFile = null;

    //Stores sorted list of files to process for this importer run.
    private LinkedList <File> candidates = null;

    //Stores path of file we are working on
    private String nextFileToImport = null;

    /**
     * This class is a filename filter that uses regexp to filter out files we
     * are not interested in.
     */
    private class RegexpFilenameFilter implements FilenameFilter {

        private Pattern filenamePattern;

        private Matcher filenameMatcher;

        private File parentDirectory;

        /**
         * Constructor - set the directory, and the filename pattern. Then call
         * setPattern.
         *
         *@param parentDirectory
         *           is the parent directory to set the pattern to
         *@param filenamePattern
         *           is the filename pattern to use
         *@throws ConfigurationException
         *            if the regex blows up
         */
        public RegexpFilenameFilter(String parentDirectory,
                String filenamePattern)
        throws ConfigurationException {
            super();
            setPattern(parentDirectory, filenamePattern);
        }

        /**
         *Setup to match the directory and filename pattern. Compiles the
         *regexp that filters out unwanted filenames from the listing of the
         *directory.
         *
         *@param parentDirectoryP
         *           is the parent directory to use
         *@param filenamePatternP
         *           to set the regex pattern
         *@throws ConfigurationException
         *            if the regex blows up
         */
        public void setPattern(String parentDirectoryP, String filenamePatternP)
        throws ConfigurationException {
            // Create a pattern to match breaks
            try {
                this.filenamePattern = Pattern.compile(
                        filenamePatternP, Pattern.CASE_INSENSITIVE);
            } catch (PatternSyntaxException e) {
                FileDataSourceAdapter.getLog().fatal(
                        "Cannot compile input filename pattern.",
                        DataSourceAdapterError.BAD_FILENAME_PATTERN);
                throw new ConfigurationException(
                        DataSourceAdapterError.BAD_FILENAME_PATTERN,
                        new UserMessage(
                                LocalizedMessage
                                .formatMessage(DataSourceAdapterError
                                        .BAD_FILENAME_PATTERN)));
            }
            // Split input with the pattern
            this.parentDirectory = new File(parentDirectoryP);
        }

        /**
         *{@inheritDoc}
         *
         *@see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
         */
        public boolean accept(File dir, String name) {
            this.filenameMatcher = this.filenamePattern.matcher(name);
            if (0 == dir.compareTo(this.parentDirectory)) {
                if (this.filenameMatcher.matches()) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     *Class used to sort the files by date-time, oldest on top (ascending).
     *
     *@author dgold
     */
    private class SortByFileDate implements Comparator<Object> {

        /**
         *Constructor.
         */
        public SortByFileDate() {
            super();
        }

        /**
         *{@inheritDoc}
         *
         *@see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        public int compare(Object o1, Object o2) {
            File f1 = (File) o1;
            File f2 = (File) o2;
            long dateDiff = f1.lastModified() - f2.lastModified();

            if (dateDiff <= 0) {
                if (dateDiff < 0) {
                    return -1;
                }
                return 0;
            }
            return 1;
        }

    }


    /**
     *default constructor. Calls init()
     *
     *@throws VocollectException
     *            if the FileDataSourceAdapter does not initialize
     */
    public FileDataSourceAdapter() throws VocollectException {
        super();
        this.setCurrentState(INITIALIZEDSTATE);
        try {
            String thePath = new File("./").getCanonicalPath();
            log.debug("The current working directory is " + thePath);
        } catch (IOException e) {
            e.printStackTrace();
            log.info("Cannot find current working directory.");
        }
        return;
    }

    /**
     *{@inheritDoc}
     *
     *@see com.vocollect.voicelink.core.importer
     *.DataSourceAdapter#isEndOfRecord()
     */
    @Override
    public boolean isEndOfRecord() {
        // If this source is set up to read a record at a time, we will read
        // into a string and
        // overload read() to read from the string. Else, we will look for the
        // end-of-record mark
        // Otherwise, if there is no end-of-record, or the end-of-record is a
        // string (</foo>), we let the
        // parser worry about that and this always returns false.
        if (null == getCurrentLine()) {
            return true;
        }
        return (this.getBufferedCharsRemaining() == 0);
    }

    /**
     *{@inheritDoc}
     *
     *@see com.vocollect.voicelink.core.importer.DataSourceAdapter#isEndOfData()
     */
    @Override
    public boolean isEndOfData() {
        // Looking for EOF
        return this.eof;
    }

    /**
     *This method selects the 'next' file to work on, according to the filename
     *pattern given and opens it for reading. It returns true if the file can
     *be opened. If not, it returns false, sets the state appropriately, and
     *logs the issue. The only normal way for this to not open a file is to not
     *find a file, so that gets logged at the info level.
     *
     *@see com.vocollect.voicelink.core.importer.DataSourceAdapter#openSource()
     *@return true if the file could be opened, false OW.
     *@throws VocollectException for a bad configuration,
     *an invalid parent directory,
     *if the next file is null and no work is to be done,
     *or if the datasource file
     *cannot be found
     */
    @Override
    public boolean openSource() throws VocollectException {
        if (null == this.getParentDirectory()) {
            this.setCurrentState(FileDataSourceAdapter.MISCONFIGUREDSTATE);
            log.fatal(
                    "Bad configuration for " + this.getClass().getName()
                    + " - Missing parent directory for input files.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_NOT_SET);
            throw new RuntimeException("Bad configuration for "
                    + this.getClass().getName()
                    + " - Missing parent directory for input files.");
        }
        File importDirectory = new File(this.getParentDirectory());

        if (!importDirectory.exists()) {
            log.error(
                    "Parent directory '" + getParentDirectory()
                    + "' does not exist.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST);
            setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
            throw new ImportConfigurationException(
                    DataSourceAdapterError.INVALID_PARENT_DIRECTORY,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .INVALID_PARENT_DIRECTORY)));
        }


        //If a file isn't already in progress, then get a new one to work with
        if (!this.inProgress) {
            // Get the name of the next file to import. If the directory doesn't
            // exist, its an error
            try {
                this.nextFileToImport = determineNextFileToProcess();
                setFileName(this.nextFileToImport);
                this.setDescription(getDataSourceName());
            } catch (ConfigurationException e1) {
                log.error(
                        "Unable to set next file name. ",
                        DataSourceAdapterError.UNABLE_TO_SET_NEXT_FILE_NAME);
                setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
                return false;
            }
        }

        // If the next file is null, there is no work.
        if (null == this.nextFileToImport) {
            if (!foundWork) {
                log.info("No work to do. ", DataSourceAdapterError.NO_WORK);
            }
            setCurrentState(DataSourceAdapter.NOWORK);
            throw new NoWorkException(
                    DataSourceAdapterError.NO_WORK, new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError.NO_WORK)));
        } else {
            foundWork = true;
        }

        // Set the name (which in turn sets the full name of the file)
        setFileName(this.nextFileToImport);
        this.inProgress = true;

        // Get the current file encoding. We will temporarily change the encoding
        // to the encoding used in the sorted file if we need to. We need to change back
        // to the original encoding if we are processing more than one file in this run...
        //If we are supposed to have this file sorted...
        if (this.isOutputToBeSorted()) {
            //If aren't using the existing sorted file (or haven't created one)...
            if (!this.useExistingSortedFile) {
                //Try to create a sorted file now...
                if (doSortFile()) {
                    //we made a file so set this to true.
                    this.useExistingSortedFile = true;
                } else {
                    //Sorting of file failed... this is bad.
                    return false;
                }
            }
        }
        // The configured encoding of the adapter is passed in. Added by Krishna Udupi
        internalSourceFile = new TextFile(getDataSourceName(), getEncoding());
        // Set the internal buffers and whatnot to point to the correct file
        try {
            internalSourceFile.openForRead();
        } catch (FileNotFoundException e) {
            // Log this problem
            log.error(
                    "Input file not found after being selected from directory: ",
                    DataSourceAdapterError.INPUT_FILE_DELETED);
            setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
            return false;
        } catch (UnsupportedEncodingException e) {
            // Log this exception although this will not happen as we check for
            // encodings those aren't supported by VoiceLink
            log
            .error(
                    "Encoding is not supported by java "
                    + this.getEncoding()
                    + " State is misconfigured, "
                    + "please verify the configuration.",
                    DataSourceAdapterError.ENCODING_NOT_SUPPORTED_BY_JAVA, e);
            setCurrentState(DataSourceAdapter.MISCONFIGUREDSTATE);
            return false;
        }

        BufferedReader br = internalSourceFile.getInternalReader(); 
        setInternalDataSource(br);
        firstRecord = true;

        // If we haven't specified the linelength, we get the default 5k buffer
        this.setCurrentLine(CharBuffer.allocate(this.getLineLength()));
        this.setBufferedCharsRemaining(0);
        this.setOpen(true);
        this.setEof(false);
        this.setCurrentRecordLength(0);
        return true;
    }

    /**
     *{@inheritDoc}
     *
     *@see com.vocollect.voicelink.core.importer.DataSourceAdapter#read()
     */
    @Override
    public int read() throws IOException {

        if (!isOpen()) {
            return 0;
        }
        if (!isEndOfRecord()) {
            useSourceChar();
            return this.getCurrentLine().get();
        }
        return 0;
    }

    /**
     *Clear the current internal buffer. Read a line into the current buffer.
     *
     *@see com.vocollect.voicelink.core.importer.DataSourceAdapter#readRecord()
     *@return false if the file is not open or we are at end-of-file, or if the
     *        read gets null.
     */
    @Override
    public boolean readRecord() {
        if (!isOpen() || isEndOfData()) {
            return false;
        }
        this.getCurrentLine().clear();
        String temp = null;
        try {
            temp = this.getInternalDataSource().readLine();
        } catch (IOException e) {
            // Something bad happened
            log.error(
                    "Error reading opened import file.",
                    DataSourceAdapterError.MID_STREAM_INPUT_ERROR, e);
            setCurrentState(DataSourceAdapter.DATASOURCENOTREADY);
        }
        if (temp == null) {
            this.setEof(true);
            return false;
        }

        //Remove the BOM for UTF-8 format from the first record
        if (firstRecord) {
            firstRecord = false;
            if (getEncoding().equals(EncodingSupport.UTF8)) {
                if (temp.charAt(0) == EncodingSupport.BOM) {
                    temp = temp.substring(1);
                }
            }
        }

        this.setBufferedCharsRemaining(temp.length());
        this.setCurrentRecordLength(temp.length());
        if (this.getCurrentLine().capacity() < temp.length()) {
            this.setCurrentLine(CharBuffer.allocate(temp.length()));
        }
        this.getCurrentLine().put(temp);
        this.getCurrentLine().flip();
        this.setCurrentLineNumber(this.getCurrentLineNumber() + 1);
        return true;
    }

    /**
     * This resets the CurrentLineNumber to zero and calls the following.
     * @see com.vocollect.voicelink.core.importer.DataSourceAdapter#close()
     * @throws IOException when file close fails.
     **/
    @Override
    public void close() throws IOException {
        this.setCurrentLineNumber(0);
        if (internalSourceFile != null) {
            this.internalSourceFile.close();
        }
        super.close();
    }

    /**
     *Close with a successful state. This, for the current system, involves
     *moving the file to a successfully-completed directory.
     *
     *@return true if this operation is successful.
     */
    @Override
    public boolean closeOnSuccess() {
        try {
            this.close();
        } catch (IOException e) {
            // Log this, but otherwise ignore it? Or should we attempt to change
            // the filename extension?
            log.error(
                    "Abnormal close - close() threw an IO Exception ",
                    DataSourceAdapterError.ABNORMAL_CLOSE_ON_SUCCESS, e);
        } finally {
            //Reset this flag to get next file on openSource.
            this.inProgress = false;
            //set flag to make a fresh sort file next time it is needed.
            this.useExistingSortedFile = false;
        }
        if (this.moveFilesOnCompletion) {
            try {
                if (moveFileToDirectory(getSuccessDirectory())) {
                    return true;
                }
                log.error(
                        "Failed to move successful import file to '"
                        + getSuccessDirectory() + "'.",
                        DataSourceAdapterError.CREATE_FAILURE_DIRECTORY_FAILED);
                return false;
            } catch (IOException e) {
                log.error(
                        "Failed to create Successful Import directory '"
                        + getSuccessDirectory() + "'.",
                        DataSourceAdapterError.CREATE_SUCCESS_DIRECTORY_FAILED, e);
                return false;

            }
        }
        return true;
    }


    /**
     *Close with a failed state. This, for the current system, involves moving
     *the file to an unsuccessfully-completed directory.
     *
     *@return true if this operation is successful.
     */
    @Override
    public boolean closeOnFailure() {
        if (null != this.getInternalDataSource()) {
            try {
                this.close();
            } catch (IOException e) {
                // Log this, but otherwise ignore it? Or should we attempt to
                // change the filename extension?
                log.error(
                        "Abnormal close - close() threw an IO Exception ",
                        DataSourceAdapterError.ABNORMAL_CLOSE_ON_FAILURE, e);
            } finally {
                //Reset this flag to get next file on openSource.
                this.inProgress = false;
                //set flag to make a fresh sort file next time it is needed.
                this.useExistingSortedFile = false;
            }
        }
        if (this.moveFilesOnCompletion) {
            try {
                if (moveFileToDirectory(getFailureDirectory())) {
                    return true;
                }
                log.error(
                        "Failed to move failed import file to '"
                        + getFailureDirectory() + "'.",
                        DataSourceAdapterError.CREATE_FAILURE_DIRECTORY_FAILED);
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                log.error(
                        "Failed to create Failed Import directory '"
                        + getFailureDirectory() + "'.",
                        DataSourceAdapterError.CREATE_FAILURE_DIRECTORY_FAILED2, e);
                return false;
            }
        }
        return true;
    }

    /**
     * Move the spec'd file to the spec'd directory, log success or failure with the spec'd errorCode instances.
     * @param dir specified directory
     * @param fileToMove specified file
     * @param appendedName Name to append to the filename when moving
     * @param successCode error code to use to log success
     * @param errorCode error code to use to log failure
     * @return true if successful.
     * @throws IOException if the specified directory does not exist.
     */
    private boolean moveFile(File dir, String fileToMove, String appendedName, 
            ErrorCode successCode, ErrorCode errorCode) throws IOException {
        File currentFile = new File(fileToMove);
        File moveTo = new File(dir, currentFile.getName() + appendedName);
        if (currentFile.renameTo(moveTo)) {
            log.info("Saved file " + fileToMove + " to " + moveTo.getCanonicalPath(),
                    successCode);
        } else {
            log.error("Failed to save file " + fileToMove + " to " + moveTo.getCanonicalPath(),
                    errorCode);
            return false;
        }
        return true;
    }

    /**
     *Move the 'current' file to the named directory.
     *  If using sorted files, this will also consider moving the original file
     *  and the intermediate UTF-16 file to the proper directory based on the
     *  isKeepSortedFile and isKeepIntermediateUTF16File flags.
     *  All moved files get a number tacked to the end (time in ms).
     *  If moving original and sorted, all will have same ms number added.
     *
     *@param toDirectory
     *           specifies the directory to move the file(s) to.
     *@return true if successful
     *@throws IOException
     *            if problems with the move.
     */
    private boolean moveFileToDirectory(String toDirectory) throws IOException {
        Long millisecondName = Calendar.getInstance().getTimeInMillis();
        File currentFile = new File(this.getDataSourceName());

        // Destination directory
        File dir = new File(toDirectory);

        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                log.error("Unable to create directory '" + toDirectory + "'.",
                        DataSourceAdapterError.UNABLE_TO_CREATE_DIRECTORY);
                return false;
            }
        }

        // Move file(s) to new directory

        if (!this.isOutputToBeSorted()) {
            //Output not sorted   -- move original file
            moveFile(dir, this.getDataSourceName(), millisecondName.toString(), 
                    DataSourceAdapterError.INPUT_FILE_MOVED, 
                    DataSourceAdapterError.INPUT_FILE_NOT_MOVED);
        } else {  //We are dealing with a Sorted file scenario.

            if (this.isKeepSortedFile()) {
                //Since we are keeping sorted file, Move it.
                moveFile(dir, this.getDataSourceName(), millisecondName.toString(), 
                        DataSourceAdapterError.SORTED_INPUT_FILE_MOVED, 
                        DataSourceAdapterError.SORTED_INPUT_FILE_NOT_MOVED);
            } else {
                //Not keeping sorted file, so delete it.
                if (!currentFile.delete()) {
                    log.error("Failed to delete Sorted file " + currentFile.getName(),
                            DataSourceAdapterError.SORTED_FILE_NOT_DELETED);
                    return false;
                }
            }

            //Now move original file.
            moveFile(dir, getParentDirectory() + File.separator + originalFileName, millisecondName.toString(), 
                    DataSourceAdapterError.INPUT_FILE_MOVED2, 
                    DataSourceAdapterError.INPUT_FILE_NOT_MOVED2);

            //Move Intermediate file if required.
            if (this.isKeepIntermediateUTF16File()) {
                moveFile(dir, this.getParentDirectory() + File.separator
                        + this.intermediateUTF16File.getFileName(), millisecondName.toString(), 
                        DataSourceAdapterError.INTERMEDIATE_SORT_FILE_MOVED, 
                        DataSourceAdapterError.INTERMEDIATE_SORT_FILE_NOT_MOVED);

            } else {
                intermediateUTF16File.delete();
            }
        }

        return true;
    }


    /**
     *  Opens the intermediate (tempOutFileName) for random access read.
     * @return boolean returns true if intermediate UTF16 was opened for read
     *          returns false if not.
     * @param source the TextFile whose name we will use to open the Random access file.
     */
    private boolean openIntermediateFileForRandomAccessRead(TextFile source) {
        try {
            raf = new RandomAccessFile(source.getFileName(), "r");
        } catch (FileNotFoundException e) {
            log.error("Can not open the internal intermediate UTF-16 sort file for read.:  " + source.getFileName(),
                    DataSourceAdapterError.SORT_TEMP_FILE_CAN_NOT_BE_OPENED_READ);
            return false;
        }
        return true;
    }

    /**
     * Copy data from the sourceFile to the (UTF16) destination file to allow us to sort UTF8 data.
     * We depend on the files being fixed-length. "Length" is a slippery term. For ASCII files
     * it is measured in bytes. For UTF8 files it's measured in characters, which are made up of 1 or 2 bytes.
     * So we copy everything to UTF-16 to make it uniform line length.
     * @param sourceFile file to read in    
     * @param destinationFile UTF16 file to write
     * @return  true iof successful.
     */
    public boolean copyToUTF16File(TextFile sourceFile, TextFile destinationFile) {

        // Both files should be ready to open (ie: closed, named, with encoding set)
        if (!sourceFile.isReadyToOpen()) {
            return false;
        }
        if (!destinationFile.isReadyToOpen()) {
            return false;
        }

        //Open the source intermediate file (for read)
        try {
            sourceFile.openForRead();
        } catch (FileNotFoundException e) {
            log.error("Can not open the " + sourceFile.getFileName() + " " + sourceFile.getEncoding(),
                    DataSourceAdapterError.SORT_INPUT_FILE_FAILURE);
            return false;
        } catch (UnsupportedEncodingException e) {
            //This should never happen.  was the file already there or something?
            log.error("Can not open the " + sourceFile.getFileName()  + " source file:  " 
                    + sourceFile.getEncoding() + " it is of an unsupported encoding.",
                    DataSourceAdapterError.SORT_INPUT_FILE_FAILURE);
            return false;
        }

        //Open the UTF16 intermediate file (for write)
        try {
            destinationFile.openForWrite();
        } catch (FileNotFoundException e) {
            log.error("Can not open the " + " " + destinationFile.getEncoding() 
                    + " intermediate file.:  " + destinationFile.getFileName(),
                    DataSourceAdapterError.SORT_TEMP_FILE_CAN_NOT_BE_OPENED_WRITE);
            return false;
        } catch (UnsupportedEncodingException e) {
            //This should never happen.  was the file already there or something?
            log.error("Can not open the intermediate sort file:  "
                    + destinationFile.getFileName()  + " as " + destinationFile.getEncoding() 
                    + " it is of an unsupported encoding.",
                    DataSourceAdapterError.SORT_TEMP_FILE_UNSUPPORTED_ENCODING);
            return false;
        }

        // Locals for the read/write loop
        String aLine        = null;
        Long   linesWritten = 0L;
        sortLinesRead = -1L;    // Pre-load for the extra count we'll get when we read a null off the end

        // For every line in the source, write the data to the intermediate file.
        // We are not writing the newlines to simplify indexing.
        do {
            try {
                aLine = sourceFile.readLine();
                sortLinesRead = sortLinesRead + 1L;
            } catch (IOException e) {
                log.error("Sort routine got IOException during read: " + this.getDataSourceName(),
                        DataSourceAdapterError.SORT_READ_FAILURE);
                return false;
            }
            if (aLine != null) {
                destinationFile.write(aLine);
                linesWritten = linesWritten + 1L;
            }
        } while (aLine != null);

        //Find out if we read as many as we wrote...
        if (this.sortLinesRead.longValue() != linesWritten.longValue()) {
            log.error("Sort routine's intermediate file write failed to write"
                    + " the same number of lines read from file: " + sourceFile.getFileName(),
                    DataSourceAdapterError.SORT_OUTPUT_LENGTH_FAILURE2);
            return false;
        }
        return true;

    }

    /**
     * Clean up (close) the passed in file and call sortCleanup().
     * @param source the added file to close.
     * @param finish flag used to signal cleanup of sorted files. 
     *        True means delete them (unless we're supposed to save),
     *        False means leave them alone. 
     */
    private void sortCleanup(TextFile source, boolean finish) {
        try {
            if (null != source) {
                source.close();
            }
        } catch (IOException e) {
            // We really don't care about close errors here.
        }
        sortCleanup(finish);

    }
    /**
     * close the intermediate, sorted and random-access files.
     * @param finish flag used to signal cleanup of sorted files. 
     *        True means delete them (unless we're supposed to save),
     *        False means leave them alone. 
     */
    private void sortCleanup(boolean finish) {
        try {
            if (null != intermediateUTF16File) {
                intermediateUTF16File.close();
                if (finish && !this.isKeepIntermediateUTF16File()) {
                  intermediateUTF16File.delete();
                }
            }
            if (null != sortedFile) {
                sortedFile.close();
                if (finish && !this.isKeepSortedFile()) {
                  sortedFile.delete();
                }
            }
            if (raf != null) {
                raf.close();
            }
        } catch (IOException e) {
            // We really don't care about close errors here.
        }


    }


    /**
     * Using the fixedByteLength file's name, open for random-access read and, using the index, write it in the correct order.
     * 
     * @param fixedByteLength the TextFile we use to get the filename.
     * @param destinationFile the TextFile that will hold sorted, UTF8 data.
     * @return true if successful.
     */
    private boolean writeSortedFile(TextFile fixedByteLength, TextFile destinationFile) {
        Integer linesRead       = 0;
        Integer linesWritten    = 0;

        if (!openIntermediateFileForRandomAccessRead(fixedByteLength)) {
            return false;
        }

        // Assume that if the destination is open, it's open for writing.
        if (destinationFile.isReadyToOpen()) {
            //Open the UTF8 destination file (for write)
            try {
                destinationFile.openForWrite();
            } catch (FileNotFoundException e) {
                log.error("Can not open the " + " " + destinationFile.getEncoding() 
                        + " intermediate file.:  " + destinationFile.getFileName(),
                        DataSourceAdapterError.SORT_TEMP_FILE_CAN_NOT_BE_OPENED_WRITE);
                return false;
            } catch (UnsupportedEncodingException e) {
                //This should never happen.  was the file already there or something?
                log.error("Can not open the intermediate sort file:  "
                        + destinationFile.getFileName()  + " as " + destinationFile.getEncoding() 
                        + " it is of an unsupported encoding.",
                        DataSourceAdapterError.SORT_TEMP_FILE_UNSUPPORTED_ENCODING);
                return false;
            }
        }
        //Get the proper order array.
        Long[][] properOrder = this.getRecordsInProperOrder();

        //loop through array, get each record and write it out.
        for (int recordIndex = 0; recordIndex < properOrder[0].length; recordIndex++) {
            // make a byte array as long as the length (converted from long to int)
            byte[] b = new byte[(properOrder[1][recordIndex].intValue() * 2)];
            try {
                // Seek the the charCount for this record. * 2 (* 2 because 2
                // bytes for each), and + 2 because the first 2 bytes describe
                //the UTF-16 file, so skip them throughout.
                raf.seek((properOrder[0][recordIndex] * 2) + 2);
                //Read the proper sized buffer to equal 1 record.
                raf.read(b);
                linesRead++;
            } catch (IOException e) {
                log.error("Error reading random access intermediate UTF-16 sort"
                        + " file:  " + fixedByteLength.getFileName(),
                        DataSourceAdapterError.SORT_TEMP_FILE_RANDOM_ACCESS_READ_FAILURE);
                return false;
            }
            try {
                //Write this record to the sorted file and the string is UTF-16.
                destinationFile.println(new String(b, "UTF-16"));
                linesWritten++;
            } catch (UnsupportedEncodingException e) {
                log.error("Line write failed to Sorted file: "
                        + fileWriteFile + " - unsupported encoding exception.",
                        DataSourceAdapterError.SORTED_FILE_WRITE_UNSUPPORTED_ENCODING);
                return false;
            }
        }


        return true;
    }


    /**
     * Sortfile does 2 things, first it writes an intermediate utf16 file,
     *  then it opens this file with random read, and sorts it to a
     *  utf8 sorted file.  If this adapter has isKeepIntermediateUTF16File false
     *  (Default state), then this routine will delete that file as well.
     *  
     * @return - true if success or false if failed.
     */
    private boolean doSortFile() {
        boolean result          = true;

        // Save off the original filename, we are going to switch later to the sorted filename for input.
        originalFileName = getFileName();

        /* 
         * Create the source & intermediate UTF-16 files, then copy the data from the source to the UTF-16 file.
         * We do this so that all lines are the same length - it simplifies the indexing.
         */
        TextFile source = new TextFile(getParentDirectory() + File.separator + getFileName(), getEncoding());

        //Create intermediate UTF16
        intermediateUTF16File = 
            new TextFile(getParentDirectory() + File.separator 
                    + "IntermediateUTF16_" + Calendar.getInstance().getTimeInMillis() + getFileName(), 
                    EncodingSupport.UTF16);

        // Open original input, copy to UTF-16 and close both.
        result = copyToUTF16File(source, intermediateUTF16File);

        // Done with source. Close it.
        // Going to reopen UTF-16 file in a different mode. Close it.
        // Why !result? If result is false, we want to tell cleanup to delete the intermediate files, if necessary.
        sortCleanup(source, !result);   

        // If the data copy failed, bail.
        if (!result) {
            return false;
        }

        /*
         * OK, now we need to use the UTF-16 file in random-access mode. We also need to open the 
         * sorted output file, to which we will be writing the (sorted) contents of the UTF-16 file.
         */
        // Make a new text file for the sorted output
        String sortedFileName = "Sorted_" + Calendar.getInstance().getTimeInMillis() + getFileName();
       // below code is added as the fix for bug VLINK-2915 by Yash Arora 
        sortedFile = new TextFile(getParentDirectory() + File.separator + sortedFileName, 
            this.getEncoding());
        
        this.setFileName(sortedFileName);

        // and open it.
        result = writeSortedFile(intermediateUTF16File, sortedFile);

        sortCleanup(!result); // Clean up, and delete the intermediate files if necessary.

        if (!result) {
            return false;
        }

        return true;

    }



    /**
     *Set the end-of-file flag.
     *
     *@param isEOF
     *           sets the EOF value.
     */
    private void setEof(boolean isEOF) {
        this.eof = isEOF;
    }

    /**
     *get the name of the file we are currently attached to.
     *
     *@return the filename
     */
    private String getFileName() {
        return this.fileName;
    }

    /**
     *Set the name fo the file we are currently attached to. Set the data
     *source name as well.
     *
     *@param fileName
     *           the filename to set
     */
    private void setFileName(String fileName) {
        this.fileName = fileName;
        this.setDataSourceName(getFullFilename());
    }

    /**
     *get the name of the directory that the input files should be in.
     *
     *@return name of the directory that the input files should be in
     */
    public String getParentDirectory() {
        return this.parentDirectory;
    }

    /**
     *set the name of the directory that the input files should be in.
     *
     *@param parentDirectoryP
     *           sets the parentDirectory string
     *@throws ConfigurationException
     *            if an error is found
     */
    public void setParentDirectory(String parentDirectoryP)
    throws ConfigurationException {
        if ((null == parentDirectoryP) || parentDirectoryP.equals("")) {
            log
            .fatal(
                    "Import directory is supplied as empty string or null string",
                    DataSourceAdapterError.IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .IMPORT_DIRECTORY_SUPPLIED_AS_EMPTY_OR_NULL_STRING)));
        }
        if (new File(getSuccessDirectory()).equals(parentDirectoryP)) {
            log
            .fatal(
                    "Not permitted to have the success directory "
                    + "same as parent import directory. ",
                    DataSourceAdapterError.SUCCESS_DIRECTORY_SAME_AS_INPUT2);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.SUCCESS_DIRECTORY_SAME_AS_INPUT2,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .SUCCESS_DIRECTORY_SAME_AS_INPUT2)));
        }

        if (new File(getFailureDirectory()).equals(parentDirectoryP)) {
            log
            .fatal(
                    "Not permitted to have the failure directory "
                    + "same as parent import directory. ",
                    DataSourceAdapterError.FAILURE_DIRECTORY_SAME_AS_INPUT2);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.FAILURE_DIRECTORY_SAME_AS_INPUT2,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .FAILURE_DIRECTORY_SAME_AS_INPUT2)));
        }

        try {
            //String thePath = new File(parentDirectoryP).getCanonicalPath();
            File thePath = new File(parentDirectoryP);
            if (!thePath.isDirectory()) {
                thePath.mkdirs();
            }
            this.parentDirectory = parentDirectoryP;
            this.setDataSourceName(getFullFilename());
            if (null != getFileNamePattern()) {
                this.myWork = new RegexpFilenameFilter(
                        parentDirectoryP, getFileNamePattern());
            }
            log.debug("The current parent directory has been set to "
                    + thePath.getCanonicalPath());
        } catch (IOException e) {
            log.fatal(
                    "Cannot find parent directory. The input supplied for parentDirectory is "
                    + parentDirectoryP,
                    DataSourceAdapterError.INVALID_PARENT_DIRECTORY);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.INVALID_PARENT_DIRECTORY,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .INVALID_PARENT_DIRECTORY)));
        }
    }

    /**
     *get the full name of the input file, including path.
     *
     *@return the fullfilename
     */
    public String getFullFilename() {
        return this.getParentDirectory() + File.separator + this.getFileName();
    }

    /**
     *Using the parent directory and the filename pattern, generate and set the
     *'next' file to process. The next file will be the oldest file found in
     *the parent directory which matches the pattern.
     *
     *@return the name of the next file to process.
     *@throws ConfigurationException
     *            if the process blows up.
     */
    @SuppressWarnings("unchecked")
    private String determineNextFileToProcess() throws ConfigurationException {
        // open parent directory
        File sourceDirectory = new File(this.getParentDirectory());
        try {
            if (!sourceDirectory.isDirectory()) {
                sourceDirectory.mkdirs();
                log.info("Current parent directory created again while determining the " 
                        + "next file to process");
                log.info("The current parent directory has been set to "
                        + sourceDirectory.getCanonicalPath());
            }            
        } catch (IOException e) {
            log.fatal(
                    "Cannot create parent directory.  ",
                    DataSourceAdapterError.PARENT_DIRECTORY_COULD_NOT_BE_CREATED);
            throw new ConfigurationException(
                    DataSourceAdapterError.PARENT_DIRECTORY_COULD_NOT_BE_CREATED,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .PARENT_DIRECTORY_COULD_NOT_BE_CREATED)));
        }
        if (null == sourceDirectory) {
            // log & throw?
            log
            .error(
                    "No source directory (null) when "
                    + "determining next file to process.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST2);
            throw new ConfigurationException(
                    DataSourceAdapterError.SOURCE_DIRECTORY_DOES_NOT_EXIST2,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .SOURCE_DIRECTORY_DOES_NOT_EXIST2)));
        }
        if (!sourceDirectory.isDirectory()) {
            log
            .error(
                    "Source directory is not a directory "
                    + "when determining next file to process.",
                    DataSourceAdapterError.SOURCE_DIRECTORY_NOT_A_DIRECTORY);
            throw new ConfigurationException(
                    DataSourceAdapterError.SOURCE_DIRECTORY_NOT_A_DIRECTORY,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .SOURCE_DIRECTORY_NOT_A_DIRECTORY)));
        }


        //If our list of candidate files is not set up yet, set it up.
        if (candidates == null) {
            this.candidates = new LinkedList <File>(); 
            // Get of list of files from source directory that match filename
            //pattern defined in this.myWork and load the candidates array
            //with the list of files to process in this instance of Adapter.
            File[] fileArray  = sourceDirectory.listFiles(this.myWork);
            // sort on date
            Arrays.sort(fileArray, new SortByFileDate());
            //put the list of files into the candidates ArrayList
            for (int x = 0; x < fileArray.length; x++) {
                this.candidates.add(fileArray[x]);
            }
        }
        //Loop through files and directories and get the next filename.
        File x = null;
        while (!candidates.isEmpty()) {
            x = candidates.getFirst();
            candidates.removeFirst();
            if (!x.isDirectory()) {
                return x.getName();
            }

        }
        // if file is read-only, throw an exception.
        return null;

    }

    /**
     *Get the length of the data in the buffer.
     *
     *@return length of the data in the buffer
     */
    public int getLineLength() {
        return this.lineLength;
    }

    /**
     *Get the length of the data in the buffer.
     *
     *@param lineLength
     *           is the linelength to set.
     */
    public void setLineLength(int lineLength) {
        this.lineLength = lineLength;
    }


    /**
     * By default when files are sorted, the IntermediateUTF16_*
     * file is not kept.  If this flag is set, the system will keep this
     * intermedaite file for debugging sorting issues.
     * 
     * @return boolean value of keepIntermediateUTF16File.
     */
    public boolean isKeepIntermediateUTF16File() {
        return keepIntermediateUTF16File;
    }


    /**
     * By default when files are sorted, the IntermediateUTF16_*
     * file is not kept.  If this flag is set, the system will keep this
     * intermedaite file for debugging sorting issues.
     * 
     * @param keepIntermediateUTF16File - boolean TRUE if keep.
     */
    public void setKeepIntermediateUTF16File(boolean keepIntermediateUTF16File) {
        this.keepIntermediateUTF16File = keepIntermediateUTF16File;
    }


    /**
     * By default when files are sorted, the Sorted_*
     * file is not kept.  If this flag is set, the system will keep this
     * Sorted_ file for debugging sorting issues.
     * 
     * @return boolean value of keepSortedFile
     */
    public boolean isKeepSortedFile() {
        return keepSortedFile;
    }


    /**
     * By default when files are sorted, the Sorted_*
     * file is not kept.  If this flag is set, the system will keep this
     * Sorted_ file for debugging sorting issues.
     * 
     * @param keepSortedFile - boolean TRUE if keep.
     */
    public void setKeepSortedFile(boolean keepSortedFile) {
        this.keepSortedFile = keepSortedFile;
    }


    /**
     *Read characters from the buffer. Throws IOException if off the end of the
     *buffer
     *
     *@see java.io.BufferedReader#read(char[], int, int)
     *@param cbuf
     *           is the character array to be read
     *@param off
     *           is the integer offset
     *@param len
     *           is the length of the line to get and be read
     *@return the length read
     *@throws IOException
     *            if problems in the read
     */
    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        this.getCurrentLine().get(cbuf, off, len);
        setBufferedCharsRemaining(getBufferedCharsRemaining() - len);
        return len;
    }

    /**
     *Read characters from the buffer. Throws IOException if off the end of the
     *buffer
     *
     *@see java.io.Reader#read(char[])
     *@param cbuf
     *           is the character array to be read
     *@return the length read
     *@throws IOException
     *            if problems in the read
     */
    @Override
    public int read(char[] cbuf) throws IOException {
        this.getCurrentLine().get(cbuf);
        int temp = getBufferedCharsRemaining();
        setBufferedCharsRemaining(0);
        return temp;
    }

    /**
     *Read characters from the buffer. Throws IOException if off the end of the
     *buffer
     *
     *@see java.io.Reader#read(java.nio.CharBuffer)
     *@param target
     *           is the charbuffer object to put the currentLine into
     *@return the length read
     *@throws IOException
     *            if problems in the read
     */
    @Override
    public int read(CharBuffer target) throws IOException {
        target.put(this.getCurrentLine());
        int temp = getBufferedCharsRemaining();
        setBufferedCharsRemaining(0);
        return temp;
    }

    /**
     *read a line from the file and return it as a string, sans the (new-line)
     *character(s) at the EOL.
     *
     *@see java.io.BufferedReader#readLine()
     *@return the String that is the currentLine
     *@throws IOException
     *            if problems in the read
     */
    @Override
    public String readLine() throws IOException {
        readRecord();
        return this.getCurrentLine().toString();
    }

    /**
     *@return Returns the currentLine.
     */
    private CharBuffer getCurrentLine() {
        return this.currentLine;
    }

    /**
     *Set the internal buffer to a copy of the param passed in.
     *
     *@param currentLine
     *           The currentLine to set.
     */
    private void setCurrentLine(CharBuffer currentLine) {
        this.currentLine = currentLine;
    }

    /**
     *Get the pattern used to find files we are interested in.
     *
     *@return Returns the fileNamePattern.
     */
    public String getFileNamePattern() {
        return this.fileNamePattern;
    }

    /**
     *Set the pattern used to identify the files to process.
     *
     *@param fileNamePattern
     *           The fileNamePattern to set.
     *@throws ConfigurationException
     *            if problems in setting the regex FileName pattern
     */
    public void setFileNamePattern(String fileNamePattern)
    throws ConfigurationException {
        this.fileNamePattern = fileNamePattern;
        if (null != getParentDirectory()) {
            this.myWork = new RegexpFilenameFilter(
                    getParentDirectory(), fileNamePattern);
        }
    }

    /**
     *Get the name of the directory that we use to save failed files.
     *
     *@return Returns the failureDirectory.
     */
    public String getFailureDirectory() {
        return this.failureDirectory;
    }

    /**
     *Set the name of the directory that we use to save failed files.
     *
     *@param failureDirectoryP
     *           The failureDirectory to set.
     *@throws ConfigurationException
     *            if problems setting the directory.
     */
    public void setFailureDirectory(String failureDirectoryP)
    throws ConfigurationException {
        if (new File(failureDirectoryP).equals(this.getParentDirectory())) {
            log
            .fatal(
                    "Not permitted to have the success directory "
                    + "same as parent import directory. ",
                    DataSourceAdapterError.FAILURE_DIRECTORY_SAME_AS_INPUT);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.FAILURE_DIRECTORY_SAME_AS_INPUT,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .FAILURE_DIRECTORY_SAME_AS_INPUT)));
        }
        try {
            String thePath = new File(failureDirectoryP).getCanonicalPath();
            this.failureDirectory = failureDirectoryP;
            log
            .debug("The current failure directory has been set to "
                    + thePath);
        } catch (IOException e) {
            log.fatal(
                    "Cannot find failure directory. ",
                    DataSourceAdapterError.CREATE_FAILURE_DIRECTORY_FAILED);
            throw new ConfigurationException(
                    DataSourceAdapterError.CREATE_FAILURE_DIRECTORY_FAILED,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .CREATE_FAILURE_DIRECTORY_FAILED)));
        }
    }

    /**
     *Get the value of the flag that says whether to move the files when they
     *succeed or fail.
     *
     *@return Returns the moveFilesOnCompletion.
     */
    public boolean isMoveFilesOnCompletion() {
        return this.moveFilesOnCompletion;
    }

    /**
     *Set the value of the flag that says whether to move the files when they
     *succeed or fail.
     *
     *@param moveFilesOnCompletion
     *           The moveFilesOnCompletion to set.
     */
    public void setMoveFilesOnCompletion(boolean moveFilesOnCompletion) {
        this.moveFilesOnCompletion = moveFilesOnCompletion;
    }

    /**
     *Get the name fo the directory that we move 'successful' file to.
     *
     *@return Returns the successDirectory.
     */
    public String getSuccessDirectory() {
        return this.successDirectory;
    }

    /**
     *set the name of the directory that we move 'successful' file to.
     *
     *@param successDirectoryP
     *           The successDirectory to set.
     *@throws ConfigurationException
     *            if problems setting the directory.
     */
    public void setSuccessDirectory(String successDirectoryP)
    throws ConfigurationException {
        if (new File(successDirectoryP).equals(this.getParentDirectory())) {
            log
            .fatal(
                    "Not permitted to have the success "
                    + "directory same as parent import directory. ",
                    DataSourceAdapterError.SUCCESS_DIRECTORY_SAME_AS_INPUT);
            setCurrentState(MISCONFIGUREDSTATE);
            throw new ConfigurationException(
                    DataSourceAdapterError.SUCCESS_DIRECTORY_SAME_AS_INPUT,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .SUCCESS_DIRECTORY_SAME_AS_INPUT)));
        }
        try {
            String thePath = new File(successDirectoryP).getCanonicalPath();
            this.successDirectory = successDirectoryP;
            log
            .debug("The current success directory has been set to "
                    + thePath);
        } catch (IOException e) {
            e.printStackTrace();
            log.fatal(
                    "Cannot find success directory. ",
                    DataSourceAdapterError.CREATE_SUCCESS_DIRECTORY_FAILED);
            throw new ConfigurationException(
                    DataSourceAdapterError.CREATE_SUCCESS_DIRECTORY_FAILED,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceAdapterError
                                    .CREATE_SUCCESS_DIRECTORY_FAILED)));
        }
    }

    /**
     *@return Returns the log.
     */
    private static Logger getLog() {
        return FileDataSourceAdapter.log;
    }


    /**
     * Query to find when the file is opened, if the output should be sorted.
     * 
     * @return boolean - true if charOffsetOfRecordsInProperOrder has been set,
     *                   false if not.
     */
    public boolean isOutputToBeSorted() {
        return this.getRecordsInProperOrder() != null;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 