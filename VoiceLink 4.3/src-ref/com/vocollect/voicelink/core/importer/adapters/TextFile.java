/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.importer.EncodingSupport;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.CharBuffer;
import java.util.Locale;


/**
 * Class to represent a real, readable text file, composed of character data. 
 * 
 * You must set both the filename and encoding. 
 * 
 * Then you can open for reading or writing but not both.
 * 
 * If you want to open a file for reading, then writing (or t'other way 'round), 
 * you have to close it first.
 * 
 * The motivation behind this is fairly clear- we frequently have need to open a file 
 * and read or write it, and we seem to have made a hash of it every time - 
 * the encoding is not directly associated with the mental image we have for the 
 * thing we're manipulating.
 * 
 * The standard Java interface requires that you create a file, use the file to get a reader in 
 * order to set the encoding, and then, if you intend to have decent performance, use that reader
 * to create a buffered reader. Suppose I could have added the encoding c'tor to a buffered-reader
 * extension, and used that to build the TextFile.
 * 
 * This class handles all of that cruft, and makes dealing with on-disk files a bit easier.
 * 
 * As a benefit, I've included the interfaces for the underlying reader & writer _and_ file.
 * Kind of a one-stop-shop for disk files so we can treat them as we typically think of them. 
 * 
 * @author dgold
 *
 */
public class TextFile implements Closeable, Readable {

    // logger for this class.
    private static final Logger log = new Logger(TextFile.class);

    // String representing the character encoding used in this file
    private String encoding = EncodingSupport.UTF8;

    // Full pathname of the file
    private String fileName = null;

    // Flag to tell if this file is open for reading.
    private boolean openForRead = false;

    // Flag to tell if this file is open for writing.
    private boolean openForWrite = false;



    // Internal data structures, all access private or delegated.
    private BufferedReader internalReader = null;

    private PrintWriter internalWriter = null;

    private File internalFile = null;



    /**
     * Constructor taking the filename.
     * @param fileNameP full pathname
     */
    public TextFile(final String fileNameP) {
        fileName = fileNameP;
    }

    /**
     * Constructor taking the filename and encoding.
     * @param fileNameP pathname
     * @param encodingP character encoding (see EncodingSupport)
     */
    public TextFile(final String fileNameP, final String encodingP) {
        fileName = fileNameP;
        setEncoding(encodingP);
    }

    /**
     * Default constructor.
     */
    public TextFile() {

    }

    /**
     * Check to see if we can open the file. 
     * Is not ready if it's already open, or the filename's not set. 
     * @return true if ready to open.
     */
    public boolean isReadyToOpen() {
        boolean ready = true;
        if (StringUtil.isNullOrEmpty(getFileName())) {
            log.info("File name not set for TextFile in isReadyToOpen");
            ready = false;
        }
        if (isOpenForRead() || isOpenForWrite()) {
            ready = false;
        }
        if (log.isInfoEnabled()) {
            log.debug("TextFile " + fileName + " is using " + getEncoding() + " encoding");
        }
        return ready;
    }


    /**
     * @return name of file
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName pathname of file
     */
    public void setFileName(String fileName) {
        if (!isOpen()) {
            this.fileName = fileName;
        } else {
            throw new RuntimeException("Not permitted to set filename when file is open");
        }
    }

    /**
     * @return encoding for this file.
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Open this file for read. Can't open for read if the filename is not set or the file is open.
     * @return true if the file is opened for reading.
     * @throws FileNotFoundException if we can't find the file
     * @throws UnsupportedEncodingException if we are using an encoding that isn't supported by Java
     */
    public boolean openForRead() throws FileNotFoundException, UnsupportedEncodingException  {
        if (!isReadyToOpen()) {
            return false;
        }
        setInternalFile(new File(getFileName()));
        FileInputStream fis = new FileInputStream(getInternalFile());
        InputStreamReader isr = new InputStreamReader(fis, getEncoding());
        BufferedReader br = new BufferedReader(isr);

        setInternalReader(br);
        setOpenForRead(true);
        return  true;
    }

    /**
     * Open this file for write. Can't open for write if the filename is not set or the file is open.
     * @return true if the file is opened for reading.
     * @throws FileNotFoundException if we can't find the file
     * @throws UnsupportedEncodingException if we are using an encoding that isn't supported by Java
     */
    public boolean openForWrite() throws FileNotFoundException, UnsupportedEncodingException {
        if (!isReadyToOpen()) {
            return false;
        }
        setInternalFile(new File(getFileName()));
        setInternalWriter(new PrintWriter(getInternalFile(), getEncoding()));
        setOpenForWrite(true);
        return true;
    }

    /**
     * Set the character encoding for this file.
     * @param encodingP the encoding for this file
     */
    public void setEncoding(String encodingP) {
        if (isOpenForRead() || isOpenForWrite()) {
            log.info("Attempted to change encoding for TextFile " + fileName + " from " + getEncoding() 
                    + " to " + encodingP);
            throw new RuntimeException("Cannot change encoding unless the file is closed.");
        }
        if (EncodingSupport.isEncodingSupported(encodingP)) {
            encoding = encodingP;
        } else {
            log.info("Encoding" + encodingP 
                    + " is not supported. See com.vocollect.voicelink.core.importer.EncodingSupport");
            throw new RuntimeException("Encoding" + encodingP 
                    + " is not supported. See com.vocollect.voicelink.core.importer.EncodingSupport");
        }
    }



    /**
     * This closes the file. Once closed, you can reopen in either read or write mode.
     * @throws IOException on file access errors
     */
    public void close() throws IOException {
        if (null != getInternalReader()) {
            getInternalReader().close();
            setInternalReader(null);
            setOpenForRead(false);
        }

        if (null != getInternalWriter()) {
            getInternalWriter().close();
            setInternalWriter(null);
            setOpenForWrite(false);
        }

    }

    //---------------------------Getters & Setters------------------------//

    /**
     * Gets the value of openForRead.
     * @return the openForRead
     */
    public boolean isOpenForRead() {
        return openForRead;
    }

    /**
     * Sets the value of the openForRead.
     * @param openForRead the openForRead to set
     */
    private void setOpenForRead(boolean openForRead) {
        this.openForRead = openForRead;
    }

    /**
     * Gets the value of openForWrite.
     * @return the openForWrite
     */
    public boolean isOpenForWrite() {
        return openForWrite;
    }

    /**
     * @return true if the file is open for reading or writing.
     */
    public boolean isOpen() {
        return (isOpenForRead() || isOpenForWrite());
    }

    /**
     * Sets the value of the openForWrite.
     * @param openForWrite the openForWrite to set
     */
    private void setOpenForWrite(boolean openForWrite) {
        this.openForWrite = openForWrite;
    }

    /**
     * Sets the value of the internalFile.
     * @param internalFile the internalFile to set
     */
    public void setInternalFile(File internalFile) {
        this.internalFile = internalFile;
    }

    /**
     * Gets the value of internalFile.
     * @return the internalFile
     */
    public File getInternalFile() {
        return internalFile;
    }


    /**
     * @return the reader for this file
     */
    public BufferedReader getInternalReader() {
        return internalReader;
    }

    /**
     * Set the internal reader for this file. Only active if the file is closed.
     * @param internalReader the reader for this file
     */
    private void setInternalReader(BufferedReader internalReader) {
        this.internalReader = internalReader;
    }

    /**
     * @return the writer for this file
     */
    private PrintWriter getInternalWriter() {
        return internalWriter;
    }

    /**
     * Set the internal writer for this file. Only active if the file is closed.
     * @param internalWriter the writer for this file
     */
    private void setInternalWriter(PrintWriter internalWriter) {
        this.internalWriter = internalWriter;
    }

    //---------------------------File delegates-----------------------------------//



    /**
     * {@inheritDoc}
     * @see java.io.File#canRead()
     */
    public boolean canRead() {
        return getInternalFile().canRead();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#canWrite()
     */
    public boolean canWrite() {
        return getInternalFile().canWrite();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#createNewFile()
     */
    public boolean createNewFile() throws IOException {
        return getInternalFile().createNewFile();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#delete()
     */
    public boolean delete() {
        return getInternalFile().delete();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#deleteOnExit()
     */
    public void deleteOnExit() {
        getInternalFile().deleteOnExit();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#exists()
     */
    public boolean exists() {
        return getInternalFile().exists();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getAbsoluteFile()
     */
    public File getAbsoluteFile() {
        return getInternalFile().getAbsoluteFile();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getAbsolutePath()
     */
    public String getAbsolutePath() {
        return getInternalFile().getAbsolutePath();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getCanonicalFile()
     */
    public File getCanonicalFile() throws IOException {
        return getInternalFile().getCanonicalFile();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getCanonicalPath()
     */
    public String getCanonicalPath() throws IOException {
        return getInternalFile().getCanonicalPath();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getName()
     */
    public String getName() {
        return getInternalFile().getName();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getParent()
     */
    public String getParent() {
        return getInternalFile().getParent();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getParentFile()
     */
    public File getParentFile() {
        return getInternalFile().getParentFile();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#getPath()
     */
    public String getPath() {
        return getInternalFile().getPath();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#hashCode()
     */
    public int hashCode() {
        return getInternalFile().hashCode();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#isDirectory()
     */
    public boolean isDirectory() {
        return getInternalFile().isDirectory();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#isFile()
     */
    public boolean isFile() {
        return getInternalFile().isFile();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#setReadOnly()
     */
    public boolean setReadOnly() {
        return getInternalFile().setReadOnly();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#toURI()
     */
    public URI toURI() {
        return getInternalFile().toURI();
    }

    /**
     * {@inheritDoc}
     * @see java.io.File#toURL()
     */
    public URL toURL() throws MalformedURLException {
        return getInternalFile().toURI().toURL();
    }

    //----------------------------Read delegates----------------------------------//

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#mark(int)
     */
    public void mark(int readAheadLimit) throws IOException {
        internalReader.mark(readAheadLimit);
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#markSupported()
     */
    public boolean markSupported() {
        return internalReader.markSupported();
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#read()
     */
    public int read() throws IOException {
        return internalReader.read();
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#read(char[], int, int)
     */
    public int read(char[] cbuf, int off, int len) throws IOException {
        return internalReader.read(cbuf, off, len);
    }

    /**
     * {@inheritDoc}
     * @see java.io.Reader#read(char[])
     */
    public int read(char[] cbuf) throws IOException {
        return internalReader.read(cbuf);
    }

    /**
     * {@inheritDoc}
     * @see java.io.Reader#read(java.nio.CharBuffer)
     */
    public int read(CharBuffer target) throws IOException {
        return internalReader.read(target);
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#readLine()
     */
    public String readLine() throws IOException {
        return internalReader.readLine();
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#ready()
     */
    public boolean readReady() throws IOException {
        return internalReader.ready();
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#reset()
     */
    public void reset() throws IOException {
        internalReader.reset();
    }

    /**
     * {@inheritDoc}
     * @see java.io.BufferedReader#skip(long)
     */
    public long skip(long n) throws IOException {
        return internalReader.skip(n);
    }


    //------------------------------------------Write delegates-------------------//


    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#append(char)
     */
    public PrintWriter append(char c) {
        return internalWriter.append(c);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#append(java.lang.CharSequence, int, int)
     */
    public PrintWriter append(CharSequence csq, int start, int end) {
        return internalWriter.append(csq, start, end);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#append(java.lang.CharSequence)
     */
    public PrintWriter append(CharSequence csq) {
        return internalWriter.append(csq);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#checkError()
     */
    public boolean checkError() {
        return internalWriter.checkError();
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#flush()
     */
    public void flush() {
        internalWriter.flush();
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#format(java.util.Locale, java.lang.String, java.lang.Object[])
     */
    public PrintWriter format(Locale l, String format, Object... args) {
        return internalWriter.format(l, format, args);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#format(java.lang.String, java.lang.Object[])
     */
    public PrintWriter format(String format, Object... args) {
        return internalWriter.format(format, args);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(boolean)
     */
    public void print(boolean b) {
        internalWriter.print(b);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(char)
     */
    public void print(char c) {
        internalWriter.print(c);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(char[])
     */
    public void print(char[] s) {
        internalWriter.print(s);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(double)
     */
    public void print(double d) {
        internalWriter.print(d);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(float)
     */
    public void print(float f) {
        internalWriter.print(f);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(int)
     */
    public void print(int i) {
        internalWriter.print(i);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(long)
     */
    public void print(long l) {
        internalWriter.print(l);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(java.lang.Object)
     */
    public void print(Object obj) {
        internalWriter.print(obj);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#print(java.lang.String)
     */
    public void print(String s) {
        internalWriter.print(s);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#printf(java.util.Locale, java.lang.String, java.lang.Object[])
     */
    public PrintWriter printf(Locale l, String format, Object... args) {
        return internalWriter.printf(l, format, args);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#printf(java.lang.String, java.lang.Object[])
     */
    public PrintWriter printf(String format, Object... args) {
        return internalWriter.printf(format, args);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println()
     */
    public void println() {
        internalWriter.println();
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(boolean)
     */
    public void println(boolean x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(char)
     */
    public void println(char x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(char[])
     */
    public void println(char[] x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(double)
     */
    public void println(double x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(float)
     */
    public void println(float x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(int)
     */
    public void println(int x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(long)
     */
    public void println(long x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(java.lang.Object)
     */
    public void println(Object x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#println(java.lang.String)
     */
    public void println(String x) {
        internalWriter.println(x);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#write(char[], int, int)
     */
    public void write(char[] buf, int off, int len) {
        internalWriter.write(buf, off, len);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#write(char[])
     */
    public void write(char[] buf) {
        internalWriter.write(buf);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#write(int)
     */
    public void write(int c) {
        internalWriter.write(c);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#write(java.lang.String, int, int)
     */
    public void write(String s, int off, int len) {
        internalWriter.write(s, off, len);
    }

    /**
     * {@inheritDoc}
     * @see java.io.PrintWriter#write(java.lang.String)
     */
    public void write(String s) {
        internalWriter.write(s);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 