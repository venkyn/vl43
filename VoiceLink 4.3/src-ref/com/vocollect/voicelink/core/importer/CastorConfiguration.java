/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.mapping.xml.ClassMapping;
import org.exolab.castor.mapping.xml.FieldMapping;
import org.exolab.castor.mapping.xml.MappingRoot;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.InputSource;

/**
 * This encapsulates the Castor configuration, so we can swap it out, and so I
 * can just code this 1x This is intended to configure an object from the
 * owner's perspective, mostly. The interface member which takes a class is used
 * to configure, say, a lilst of items for the caller. This is used in the
 * ImportManager, when loading the list of Importers.
 * 
 * @author dgold
 */
public class CastorConfiguration implements Configurable {

    private static final Logger log          = new Logger(
                                                     CastorConfiguration.class);

    private String              mappingFileName;

    // This file contains the definitions of the objects that can be
    // instantiated, and the matching tags

    private String              configFileName;

    // This file contains the tags that make up this object - they get converted
    // to objects.

    private Mapping             mapping;

    // used by Castor

//    private Marshaller          marshaller;

    // This is the part that makes a tagged XML representation of whatever is

    private Unmarshaller        unmarshaller = null;

    // This is the part that interprets the tagged XML stream

    /**
     * Default constructor. Does nothing special
     */
    public CastorConfiguration() {
        super();
    }

    /**
     * Constructor that sets up the files.
     * @param mappingFileName specifies the mapping filename
     * @param configFileName specifies the configuration filename
     */
    public CastorConfiguration(String mappingFileName, String configFileName) {
        super();
        this.setMappingFileName(mappingFileName);
        this.setConfigFileName(configFileName);
    }

    /**
     * Load the mapping using the mapping file set previously. Throws a
     * ConfigurationException if the mapping file is not set, missing or bad
     * @throws ConfigurationException if mapping loads incorrectly
     * @return the new mapping
     */
    public Mapping loadMapping() throws ConfigurationException {
        if (null == getMappingFileName()) {
            log.fatal("Configuration error: mapping file not set",
                    ImporterError.MAPPING_FILE_NOT_SET);
            throw new ConfigurationException(ImporterError.MAPPING_FILE_NOT_SET,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.MAPPING_FILE_NOT_SET)));
        }
        setMapping(new Mapping(getClass().getClassLoader()));
        // 1. Load the mapping information from the file
        URL mappingResource = this.getClass().getClassLoader().getResource(getMappingFileName());
        if (null == mappingResource) {
            log.fatal("Configuration error: missing mapping file "
                    + getMappingFileName(), ImporterError.MISSING_MAPPING_FILE);
            throw new ConfigurationException(ImporterError.MISSING_MAPPING_FILE,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.MISSING_MAPPING_FILE),
                            getMappingFileName()));
        }
        try {
            getMapping().loadMapping(mappingResource);
        } catch (IOException e) {
            e.printStackTrace();
            log.fatal("Configuration error: unable to open mapping file "
                    + getMappingFileName() + "missing or permissions problem?", 
                    ImporterError.MISSING_MAPPING_FILE2);
            throw new ConfigurationException(ImporterError.MISSING_MAPPING_FILE2,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.MISSING_MAPPING_FILE2),
                            getMappingFileName()));
        } catch (MappingException e) {
            e.printStackTrace();
            log.fatal("Configuration error: bad mapping file "
                    + getMappingFileName(), ImporterError.BAD_MAPPING_FILE);
            throw new ConfigurationException(ImporterError.BAD_MAPPING_FILE,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.BAD_MAPPING_FILE),
                            getMappingFileName()));
        }
        return getMapping();
    }

    /**
     * Set the mapping file name and load the mapping.
     * @param mappingFile specifies the file to load
     * @throws ConfigurationException if unable to load mapping
     * @return Mapping object for Castor configuration
     */
    public Mapping loadMapping(String mappingFile)
            throws ConfigurationException {
        setMappingFileName(mappingFile);
        return loadMapping();

    }

    /**
     * Set the InputSource from the config file name passed in. This sets the
     * config file, the one that says what is in the makeup of the object being
     * passed back. The mapping file is the one that shows what tags mean what.
     * Throws a ConfigurationException if the config file is not set or missing
     * @return the new InputSource tied to the configuration file passed in
     * @throws ConfigurationException if unable to set InputSource object
     */
    private InputSource setInputSource()
            throws ConfigurationException {
        if (null == getConfigFileName()) {
            log.fatal("Configuration error: configuration file name not set"
                    + getConfigFileName(), ImporterError.CONFIG_FILE_NOT_SET);
            throw new ConfigurationException(ImporterError.CONFIG_FILE_NOT_SET,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.CONFIG_FILE_NOT_SET)));
        }

        URL unmarshalResource = null;
        File temp = null;
        try {
            //If there are no path separators getConfigFileName() then get from classpath.
            if (!isPath(getConfigFileName())) {
                unmarshalResource = this.getClass().getClassLoader().getResource(getConfigFileName());
                if (null == unmarshalResource) {
                    log.fatal(
                            "Configuration error: cannot locate configuration file '"
                                    + getConfigFileName() + "' on the classpath.",
                            ImporterError.CONFIG_FILE_NOT_ON_CLASSPATH);
                    throw new ConfigurationException(
                            ImporterError.CONFIG_FILE_NOT_ON_CLASSPATH,
                            new UserMessage(
                                    LocalizedMessage
                                            .formatMessage(ImporterError.CONFIG_FILE_NOT_ON_CLASSPATH),
                                    getConfigFileName()));
                }
                temp = new File(unmarshalResource.toURI());
            } else {
                temp = new File(getConfigFileName());
            }
            return new InputSource(new InputStreamReader(new FileInputStream(temp), "UTF-8"));
        } catch (UnsupportedEncodingException uee) {
            uee.printStackTrace();
            throw new ConfigurationException(ImporterError.BAD_URI_FOR_CONFIG_FILE,
                new UserMessage(LocalizedMessage
                        .formatMessage(ImporterError.BAD_URI_FOR_CONFIG_FILE),
                        getConfigFileName())); 
        } catch (FileNotFoundException e1) {
            try {
                log.fatal("Configuration error: missing configuration file "
                        + unmarshalResource.toURI(), ImporterError.MISSING_CONFIG_FILE);
            } catch (URISyntaxException e) {
            }
            throw new ConfigurationException(ImporterError.MISSING_CONFIG_FILE,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.MISSING_CONFIG_FILE),
                            getConfigFileName()));
        } catch (URISyntaxException e) {
            log.fatal("Configuration error: missing configuration file - bad URI "
                    + unmarshalResource.getPath(), ImporterError.MISSING_CONFIG_FILE);
            throw new ConfigurationException(ImporterError.BAD_URI_FOR_CONFIG_FILE,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.BAD_URI_FOR_CONFIG_FILE),
                            getConfigFileName()));
        }
    }

    
    /**
     * This method will return true if a string passed in contains a slash or a 
     * backslash.  Otherwise it returns false.
     * @param inputString - String to be parsed.
     * @return - true if string passed contains a slash or backslash, otherwise false.
     */
    private Boolean isPath(String inputString) {
        if (inputString.contains("/")) {
            return true;
        } else {
            return inputString.contains("\\");
        }
        
    }
    
    
    
    /**
     * Get the objects represented by the tagged data in the InputSource.
     * @param xmlConfig specifies the InputSource object to unmarshal
     * @return a new Object, which is actually whatever was defined by the combo
     *         of the mapping and config files.
     * @throws ConfigurationException
     *             if the mapping file is bad, the mapping/config is mismatched,
     *             or the configuration is missing any required element
     */
    private Object unmarshalInputSource(InputSource xmlConfig)
            throws ConfigurationException {
        try {
            this.unmarshaller.setMapping(this.mapping);
            return this.unmarshaller.unmarshal(xmlConfig);
        } catch (MappingException e) {
            e.printStackTrace();
            log.fatal("Configuration error: bad mapping file "
                    + getMappingFileName(), ImporterError.BAD_MAPPING_FILE2);
            throw new ConfigurationException(ImporterError.BAD_MAPPING_FILE2,
                    new UserMessage(LocalizedMessage
                            .formatMessage(ImporterError.BAD_MAPPING_FILE2),
                            getMappingFileName()));
        } catch (MarshalException e1) {
            e1.printStackTrace();
            log.fatal("Configuration error: mismatched configuration '"
                    + getConfigFileName() + "' and mapping '"
                    + getMappingFileName() + "' files ",
                    ImporterError.CONFIGURATION_MISMATCH);
            throw new ConfigurationException(
                    ImporterError.CONFIGURATION_MISMATCH,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(ImporterError.CONFIGURATION_MISMATCH),
                            getConfigFileName(), getMappingFileName()));
        } catch (ValidationException e2) {
            e2.printStackTrace();
            log.fatal(
                    "Configuration error: missing required element in configuration file '"
                            + getConfigFileName()
                            + "'. Associated mapping file: '"
                            + getMappingFileName(),
                    ImporterError.CONFIGURATION_MISSING_ELEMENT);
            throw new ConfigurationException(
                    ImporterError.CONFIGURATION_MISSING_ELEMENT,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(ImporterError.CONFIGURATION_MISSING_ELEMENT),
                            getConfigFileName(), getMappingFileName()));
        }

    }
    
    /**
     * This method is used to populate the nested fieldToBindNameMap HashMap.
     * This holds the fieldName/BindName pair for fields for each class in mapping file.
     * @param objMapping for which we need to find the field name / bind name
     * cross ref.
     * @return HashMap - nested HashMap which contains the field name / bind name
     * cross ref for each object in the mapping.
     */
    public HashMap<String, HashMap<String, String>> getFieldToBindNameMap(Mapping objMapping) {
        HashMap<String, HashMap<String, String>> fieldToBindNameMap = 
                     new HashMap<String, HashMap<String, String>>();
        //Create a nested hashmap to store the bind-xml/fieldname pair for each class
        MappingRoot mappingRoot =   objMapping.getRoot();
        ClassMapping classMap;
        FieldMapping fieldMap;
        FieldMapping[] fieldMaps;
        HashMap<String, String> innerHashMap = null;
        // Flag to turn on debugging in here. This delivers maybe 15 pages of info...
        boolean debugMapping = false;

        int mappingCount = mappingRoot.getClassMappingCount();
        if (debugMapping) {
             log.debug("Number of classes in the mapping file: " + mappingCount);            
        }

        // loop over the classes
        for (int i = 0; i < mappingCount; ++i) {
            innerHashMap = new HashMap<String, String>();
            classMap = mappingRoot.getClassMapping(i);
            if (null == classMap || null == classMap.getMapTo() || null == classMap.getClassChoice()) {
                if (debugMapping) {
                    log.info("Either couldn't get the classMapping from the root or classMap map " 
                        + "to name not specified or no class specification provided");
                }
                continue;
            }

            int fieldCount = classMap.getClassChoice().getFieldMappingCount();
            fieldMaps = classMap.getClassChoice().getFieldMapping();
            
            if (debugMapping) {
                log.debug("Number of fields for class " + classMap.getName() + " in the mapping file: " 
                          + fieldCount);    
                log.debug("Class name is: " + classMap.getName());
                log.debug("Class map-to name is: " + classMap.getMapTo().getXml());
                log.debug("Number of fields for class " + classMap.getName() + " in the mapping file: " 
                + fieldCount);
           }
            
           // loop over the fields in each class
           for (int j = 0; j < fieldMaps.length; ++j) {
                fieldMap = fieldMaps[j];
                if (fieldMap.getBindXml() != null) {
                    innerHashMap.put(fieldMap.getBindXml().getName(), fieldMap.getName());   
                    innerHashMap.put(fieldMap.getName(), fieldMap.getBindXml().getName());   
                } else {
                    innerHashMap.put("No Bind XML Name Specified", fieldMap.getName());
                    innerHashMap.put(fieldMap.getName(), "No Bind XML Name Specified");
                }
                if (debugMapping) {
                    log.debug("        Field name: " + fieldMap.getName());
                    log.debug("        Field type: " + fieldMap.getType());
                    if (fieldMap.getBindXml() != null) {
                        log.debug("        Bind XML type: " + fieldMap.getBindXml().getName());
                    }
                }
            }
           fieldToBindNameMap.put(classMap.getMapTo().getXml(), innerHashMap);
           fieldToBindNameMap.put(classMap.getName(), innerHashMap);
           fieldToBindNameMap.put(classMap.getName().substring(classMap.getName().lastIndexOf(".") + 1), innerHashMap);
        }
        return fieldToBindNameMap;
    }
    


    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.Configurable#configure(java.lang.Class)
     */
    public Object configure(Class<?> asRoot)
            throws ConfigurationException {

        loadMapping(getMappingFileName());
        InputSource xmlConfig = setInputSource();
        this.unmarshaller = new Unmarshaller(asRoot);
        return unmarshalInputSource(xmlConfig);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.Configurable#configure()
     */
    public Object configure() throws ConfigurationException {

        loadMapping(getMappingFileName());
        InputSource xmlConfig = setInputSource();
        this.unmarshaller = new Unmarshaller();
        return unmarshalInputSource(xmlConfig);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Configurable#getMappingFileName()
     */
    public String getMappingFileName() {
        return this.mappingFileName;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Configurable#setMappingFileName(java.lang.String)
     */
    public void setMappingFileName(String mappingFileName) {
        this.mappingFileName = mappingFileName;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Configurable#getConfigFileName()
     */
    public String getConfigFileName() {
        return this.configFileName;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.Configurable#setConfigFileName(java.lang.String)
     */
    public void setConfigFileName(String configFileName) {
        this.configFileName = configFileName;
    }

    /**
     * @return Returns the mapping object.
     */
    public Mapping getMapping() {
        return this.mapping;
    }

    /**
     * @param mapping The mapping object to set.
     */
    public void setMapping(Mapping mapping) {
        this.mapping = mapping;
    }

    /**
     * @return Returns the marshaller object.
     */
    //public Marshaller getMarshaller() {
    //return this.marshaller;
    //}
    /**
     * @param marshaller The marshaller object to set.
     */
    //public void setMarshaller(Marshaller marshaller) {
    //this.marshaller = marshaller;
    //}
    /**
     * @return Returns the unmarshaller object.
     */
    //public Unmarshaller getUnmarshaller() {
    //return this.unmarshaller;
    //}
    /**
     * @param unmarshaller The unmarshaller to set.
     */
    //public void setUnmarshaller(Unmarshaller unmarshaller) {
    //this.unmarshaller = unmarshaller;
    //}
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 