/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


/**
 * @author Kalpna
 * Simple object to handle the prescan errors so
 * that it can be populated in validation file
 *
 */
public class PrescanError {
    
    
    private Object theProblem = null;
    //private ErrorCode theErrorCode = null;

    /**
     * Default Constructor.
     * Constructor.
     */
    public PrescanError() {
      super();
    }  
    /**
     * 
     * @param problem specifies the object to report
     */
    public PrescanError(Object problem) {
        this.theProblem = problem;        
    }

    /**
     * @return the theProblem
     */
    public Object getTheProblem() {
        return theProblem;
    }

    /**
     * @param theProblem the theProblem to set
     */
    public void setTheProblem(Object theProblem) {
        this.theProblem = theProblem;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 