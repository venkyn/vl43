/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Unmarshaller;
import org.exolab.castor.xml.ValidationException;
import org.xml.sax.InputSource;

/**
 * This abstract class interprets the data from the DataSourceAdapter and breaks it down into fields.
 * It will, if necessary, create an XML representation, and the instantiate the object represented
 * by the import.
 * 
 * This base holds generically-useful information: the data source adapter the configuration file name the map of fields
 * the cached data. It also provides the esoteric create-an-object and create-xml-from-fields
 * that is sometimes required.
 * 
 * All descendents may define new interface elements, but in order to populate them you will either have to name a
 * config file in the XML setup, which I consider a poor choice, or add them to the definition of the class. Refer to
 * the Customization EDD "Customizing Imports" for instructions.
 * 
 * @author dgold
 * @param FMType
 *            the type of field map this uses
 * @param FieldType
 *            the type pf field this operates on
 * 
 */
/**
 * @author dgold
 * 
 * @param <FMType>
 * @param <FieldType>
 */
public abstract class DataSourceParser<FMType extends FieldMap, FieldType extends Field>
        implements Describable {

    private static final Logger     log                   = new Logger(
        DataSourceParser.class);
    
    private DataSourceAdapter sourceAdapter;
    
    private XmlTagger               xmlrepresentation          = new XmlTagger();
    
    // UNmarshaller
    private Unmarshaller            unmar                      = new Unmarshaller();


    
    private ObjectContext<FieldMap, Object> objectContext = new ObjectContext<FieldMap, Object>();

    private int elementCount = 0;
    
    private long contextBeginsLineNumber = 1;

    // What datatype this thing is supposed to be working on
    private String                    datatype                   = null;

    // Number of states that the Transfer can assume
    private final int               numberOfStateChanges       = 14;

    // List of acceptable state changes, and the current state.
    private StateChanges            stateChanges               
                                    = new StateChanges(numberOfStateChanges);

    // Access to the data source. Looks like a stream.

    private String            configFileName;

    // Name of the config file. Largely unused except during development

    private String            mappingFileName;

    // Name of the mapping file used to define the o bjects in the config file

    private FMType            fieldNameIndex;

    // Fieldname-to-field-Object map


    // Cache of the fields we want to key on, used during prescan.
    private Indexes     indexes;


    // Flag indicating whether this data source should be prescanned to fill the
    // cache.
    private boolean           preScan     = false;


    private Description       description = new Description();

    private String            fieldDefinitionFile;
    
    private String            siteName = "";

    // Holds the definition of the fileds
    private HashMap<String, HashMap<String, String>> fieldToBindNameMap;
    
    // If the last line of the input source is of a different length that all of the others,
    // this will set the allowed length. Must be exactly equal.
    private Long              lastLineLength = null;   
    
    // True if the last line is different from the rest of the lines in the file. 
    private boolean           lastLineDiffers = false;
    
    // States for the Parser. Keeps track of the individual parser's source's state.
    public static final InternalState UNINITIALIZED_STATE =
        new InternalState(0, "DataSourceParser.UnInitialized");

    public static final InternalState INITIALIZED_STATE =
        new InternalState(1, "DataSourceParser.Initialized");

    public static final InternalState CONFIGURED_STATE =
        new InternalState(2, "DataSourceParser.Configured");

    public static final InternalState MISCONFIGURED_STATE =
        new InternalState(3, "DataSourceParser.MisConfigured");

    public static final InternalState IN_PROGRESS_SUCCESSFUL_STATE =
        new InternalState(4, "DataSourceParser.InProgressSuccessful");

    public static final InternalState IN_PROGRESS_FAILED_STATE =
        new InternalState(5, "DataSourceParser.InProgressFailed");

    public static final InternalState IN_PROGRESS_PARTIAL_SUCCESS_STATE =
        new InternalState(6, "DataSourceParser.InProgressPartialSuccess");

    public static final InternalState SUCCESSFUL_STATE =
        new InternalState(7, "DataSourceParser.Successful", InternalState.FinalStateType.Success);

    public static final InternalState FAILED_STATE =
        new InternalState(8, "DataSourceParser.Failed", InternalState.FinalStateType.Failed);

    public static final InternalState PARTIAL_SUCCESS_STATE =
        new InternalState(9, "DataSourceParser.PartialSuccess", InternalState.FinalStateType.Imperfect);

    public static final InternalState IN_PROGRESS_STATE =
        new InternalState(10, "DataSourceParser.InProgress");

    /**
     * @return Returns the fieldDefinitionFile.
     */
    public String getFieldDefinitionFile() {
        return this.fieldDefinitionFile;
    }

    /**
     * @param fieldDefinitionFile
     *            The fieldDefinitionFile to set.
     */
    public void setFieldDefinitionFile(String fieldDefinitionFile) {
        this.fieldDefinitionFile = fieldDefinitionFile;
    }

    /**
     * Constructor that configures the parser. The descendents need to specialize this. This should call configure(). I
     * don't want to do that here, odd things can happen. Set the config file name, set all other internal vars to null.
     * 
     * @param configFileName
     *            to be set
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    public DataSourceParser(String configFileName)
            throws ConfigurationException {
        super();
        getStateChanges().setCurrentState(UNINITIALIZED_STATE);
        init();
        this.setConfigFileName(configFileName);
        return;
    }

    /**
     * Default constructor. Set all internal vars to null.
     * 
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    public DataSourceParser() throws ConfigurationException {
        super();
        getStateChanges().setCurrentState(UNINITIALIZED_STATE);
        init();
        return;
    }

    /**
     * Central ppint to do all of the initialization.
     * 
     * @throws ConfigurationException
     *             Signature allows us to throw a config exception
     */
    private void init() throws ConfigurationException {
        this.setConfigFileName(null);
        this.setFieldList(null);
        this.setIndexes(null);
        this.setPreScan(false);
        this.setSourceAdapter(null);
        this.getStateChanges().add(UNINITIALIZED_STATE,     INITIALIZED_STATE);
        this.getStateChanges().add(INITIALIZED_STATE,       CONFIGURED_STATE);
        this.getStateChanges().add(INITIALIZED_STATE,       MISCONFIGURED_STATE);
        this.getStateChanges().add(INITIALIZED_STATE, IN_PROGRESS_STATE);
        this.getStateChanges().add(CONFIGURED_STATE, IN_PROGRESS_STATE);
        this.getStateChanges().add(IN_PROGRESS_STATE, IN_PROGRESS_FAILED_STATE);
        this.getStateChanges().add(IN_PROGRESS_STATE, IN_PROGRESS_PARTIAL_SUCCESS_STATE);
        this.getStateChanges().add(IN_PROGRESS_STATE, IN_PROGRESS_SUCCESSFUL_STATE);
        this.getStateChanges().add(IN_PROGRESS_SUCCESSFUL_STATE, IN_PROGRESS_PARTIAL_SUCCESS_STATE);
        this.getStateChanges().add(IN_PROGRESS_FAILED_STATE, IN_PROGRESS_PARTIAL_SUCCESS_STATE);
        this.getStateChanges().add(IN_PROGRESS_PARTIAL_SUCCESS_STATE, PARTIAL_SUCCESS_STATE);
        this.getStateChanges().add(IN_PROGRESS_SUCCESSFUL_STATE, SUCCESSFUL_STATE);
        this.getStateChanges().add(IN_PROGRESS_FAILED_STATE, FAILED_STATE);
        this.getStateChanges().add(PARTIAL_SUCCESS_STATE, IN_PROGRESS_STATE);
        this.getStateChanges().add(SUCCESSFUL_STATE, IN_PROGRESS_STATE);
        this.getStateChanges().add(FAILED_STATE, IN_PROGRESS_STATE);
        getStateChanges().setCurrentState(INITIALIZED_STATE);

        return;
    }

    // -----------------Action methods----------------------//
    
    /**
     * Fail the record we just read. Adjust current state appropriately.
     */
    public void failRecord() {
        // Attempt to set to failed. If we have already passed a record, this will be ignored.
        getStateChanges().setCurrentState(IN_PROGRESS_FAILED_STATE);
        // This will always succeed
        if (0 != getCurrentState().compareTo(DataSourceParser.IN_PROGRESS_FAILED_STATE)) {
            getStateChanges().setCurrentState(IN_PROGRESS_PARTIAL_SUCCESS_STATE);
        }
    }
    
    /**
     * Pass the record we just read. Adjust current state appropriately.
     */
    public void acceptRecord() {
        // If we have failed any records, this will be ognored.
        getStateChanges().setCurrentState(IN_PROGRESS_SUCCESSFUL_STATE);
        if (0 != getCurrentState().compareTo(DataSourceParser.IN_PROGRESS_SUCCESSFUL_STATE)) {
            getStateChanges().setCurrentState(IN_PROGRESS_PARTIAL_SUCCESS_STATE);
        }
    }

    /**
     * Reset the state to begin a new data source.
     */
    public void resetState() {
        // Reset the state to begin a new data source.
        getStateChanges().setCurrentState(IN_PROGRESS_STATE);
    }
    /**
     * Close the data source, clear the caches. This is not the most efficient, we should just clear the individual
     * lists of found items. This should be sufficient.
     * 
     * Note that this does NOT indicate a completion of use of the data source.
     */
    public void close() {
        try {
            if (getSourceAdapter() != null) {
                this.getSourceAdapter().close();
            }
        } catch (IOException e) {
            // This should be OK, and we generally don't care if the file
            // closes, we are done with it.
            // Log and ignore.
        }
        return;
    }

    /**
     * Set the final state of the data source.
     */
    public void finalizeState() {
        getStateChanges().setCurrentState(SUCCESSFUL_STATE);
        getStateChanges().setCurrentState(PARTIAL_SUCCESS_STATE);
        getStateChanges().setCurrentState(FAILED_STATE);
    }
    
    /**
     * Get the current state of the Parser.
     * @return the current state
     */
    public InternalState getCurrentState() {
        return getStateChanges().getCurrentState();
    }
    /**
     * Clear the cache, and call the appropriate method on the data source. adapter
     *  This should be called on successful completion of the data source.
     */
    public void closeOnSuccess() {

        if (null != this.indexes) {
            this.indexes.reset();
        }
        this.setContextBeginsLineNumber(1);
        this.getSourceAdapter().closeOnSuccess();
        return;

    }

    /**
     * Clear the cache, and call the appropriate method on the data source. adapter
     *  This should be called on the unsuccessful completion of the data source.
     */
    public void closeOnFailure() {

        if (null != this.indexes) {
            this.indexes.reset();
        }
        this.setContextBeginsLineNumber(1);
        this.getSourceAdapter().closeOnFailure();
        return;

    }


    
    
    /**
     * This method does nothing but return true... override this to implement
     * preScan feature (and to use Indexes).
     * @return always returns true
     * @throws VocollectException .
     */
    public boolean preScan() throws VocollectException {
        return true;
    }

    
    
    // -----------------Abstract methods----------------------//

    /**
     * Get a record, return a list of fields. This should read a record, break the record up into fields, and return
     * that list of fields. Possible reasons to throw an exception: : reach End Of Data : IO error : record too short :
     * Record too long This method will also cache key fields.
     * 
     * @return the map of fields that make up a record
     * @throws VocollectException
     *             signature allows us to throw an exception if we can't get a record
     */
    public abstract ObjectContext<FieldMap, Object> getRecord() throws VocollectException;

    /**
     * Get the next field from the stream. Returns NULL at end of record - need to call goToNextRecord().
     * 
     * @return The field, with the data garnered from the input source.
     * @throws OutOfDataException
     *             when the DataSourceAdapter is out of data
     */
    public abstract FieldType getNextField() throws OutOfDataException;

    /**
     * Set up this parser, using the config file named by setConfig(...).
     * 
     * @return true if the configuration succeeded
     * @throws ConfigurationException
     *             for bad configuration
     */
    public abstract boolean configure() throws ConfigurationException;

    /**
     * Return true if at the end of the datasource. EOF for files.
     * 
     * @return true if the data source adapter is out of data
     */
    public abstract boolean isOutOfData();

    /**
     * Get an object, and all of the data that went into making it.
     * For a fixed-length datasource, this will be the field maps, XML and object.
     * For a database source, this will be the object, or - if the data comes form a query, 
     * the same as the fixed length datasource.
     * @return a complete object context
     * @throws VocollectException dependent on the implementation, typically problems parsing...
     */
    public abstract ObjectContext<FieldMap, Object> getElement() throws VocollectException;
    

    // -----------------Abstract methods end------------------//

    // ----------------Getters and setters----------------------//
    /**
     * @return Returns the sourceAdapter.
     */
    public DataSourceAdapter getSourceAdapter() {
        return this.sourceAdapter;
    }

    /**
     * @param sourceAdapter
     *            The sourceAdapter to set.
     */
    public void setSourceAdapter(DataSourceAdapter sourceAdapter) {
        this.sourceAdapter = sourceAdapter;
    }

    /**
     * 
     * @return Returns the FMType object of the FieldList.
     */
    public FMType getFieldList() {
        return this.fieldNameIndex;
    }

    /**
     * 
     * @param fieldList
     *            The fieldList to be set.
     */
    public void setFieldList(FMType fieldList) {
        this.fieldNameIndex = fieldList;
    }

    /**
     * Determines whether or not to run the prescan method.
     * 
     * @return true if prescan should be run
     */
    public boolean isPreScan() {
        return this.preScan;
    }

    /**
     * 
     * @param preScan
     *            Set to true to run prescan method.
     */
    public void setPreScan(boolean preScan) {
        this.preScan = preScan;
    }

    /**
     * 
     * @return gets the configFileName
     */
    public String getConfigFileName() {
        return this.configFileName;
    }

    /**
     * Sets the configFileName and calls config if the mapping file is also set.
     * 
     * @param configFileName
     *            name of file holding field definitions
     * @throws ConfigurationException
     *             thrown by configure if the config file is bad
     */
    public void setConfigFileName(String configFileName)
            throws ConfigurationException {
        this.configFileName = configFileName;
        if ((null != getMappingFileName())
                && (0 != getMappingFileName().compareTo(""))) {
            configure();
            if (null != getFieldList()) {
                getXmlrepresentation().markUpFields(getFieldList());
            }
        }
    }

    
    /**
     * Retrieves the Indexes object.  (list of index)
     * @return the Indexes object containing each index.
     *
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     */
    public Indexes getIndexes() {
        if (this.indexes == null) {
            this.indexes = new Indexes();
        }
        return this.indexes;
    }

    
    /**
     * Pass in an indexes object to set up the parser's indexes.
     * @param indexes the new indexes value
     */
    public void setIndexes(Indexes indexes) {
        this.indexes = indexes;
    }

    
    /**
     * @param indexToAdd - an Index object to be added.
     * @return - true if index was added, false if not.
     * @see com.vocollect.voicelink.core.importer.Indexes#addIndex(com.vocollect.voicelink.core.importer.Index)
     */
    public boolean addIndex(Index indexToAdd) {
        if (this.indexes == null) {
            //If indexes hasn't been created yet, create it.
            indexes = new Indexes();
        }
        return this.getIndexes().addIndex(indexToAdd);
    }

    /**
     * Clears all indexes defined.
     * 
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     * @see com.vocollect.voicelink.core.importer.Indexes#clear()
     */
    public void clearIndexes() {
        this.getIndexes().clear();
    }

    /**
     * @return - If at least 1 index marked as failRecord contains duplicated
     *           data for the CURRENT DATA SOURCE POSITION, this will return a
     *           string listing each occurrence of the index and the data from 
     *           the current record that causes that index to have failed.
     *           If no data in the current record causes
     *           the index to fail it returns an empty string.
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().

     * @see com.vocollect.voicelink.core.importer.Indexes#getRecordDataDuped()
     */
    public String getRecordDataDuped() {
        return this.getIndexes().getRecordDataDuped();
    }

    /**
     * @return true if no indexes, false if Indexes exist.
     * 
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     *        
     * @see com.vocollect.voicelink.core.importer.Indexes#isEmpty()
     */
    public boolean isIndexesEmpty() {
        return this.getIndexes().isEmpty();
    }

    /**
     * @return - returns true if at least 1 index marked as failFile contains
     *            duplicated data, otherwise returns false.
     * 
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     * @see com.vocollect.voicelink.core.importer.Indexes#isFileDataDuped()
     */
    public boolean isAnyFailFileIndexBlown() {
        return this.getIndexes().isFileDataDuped();
    }

    /**
     * @return - returns true if at least 1 index marked as failRecord contains
     *           duplicated data for the CURRENT DATA SOURCE POSITION.
     *           otherwise, returns false.
     *           
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     * 
     * @see com.vocollect.voicelink.core.importer.Indexes#isRecordDataDuped()
     */
    public boolean isAnyFailRecordIndexBlown() {
        return this.getIndexes().isRecordDataDuped();
    }

    /**
     * This is called during a prescan to load and process the defined indexes.
     * It should be called once for each new record.
     * 
     * Note:  this will  throw a RuntimeException if it is called before
     *        either the call of setIndexes() or addIndex().
     * @see com.vocollect.voicelink.core.importer.Indexes#updateIndexes()
     */
    public void updateIndexes() {
        this.getIndexes().updateIndexes();
    }

    
    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.Describable#getDescription()
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.Describable#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description.setDescription(description);
    }

    /**
     * @return Returns the mappingFileName.
     */
    public String getMappingFileName() {
        return this.mappingFileName;
    }

    /**
     * Set the mapping file name and call configure if the config filename is also set.
     * 
     * @param mappingFileName
     *            The mappingFileName to set.
     * @throws ConfigurationException
     *             thrown by configure
     */
    public void setMappingFileName(String mappingFileName)
            throws ConfigurationException {
        this.mappingFileName = mappingFileName;
        if ((null != this.getConfigFileName())
                && (0 != this.getConfigFileName().compareTo(""))) {
            configure();
            if (null != getFieldList()) {
                getXmlrepresentation().markUpFields(getFieldList());
            }

        }
    }

    /**
     * @return the siteName
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * @param siteName the siteName to set
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * Read getBatchCount() records. One-by-one, convert to XML on a single
     * stream Increment the count of data elements for this dataset
     * @return Count of the elements in this batch
     * @throws VocollectException -
     *             see DataSourceParser
     */
    @SuppressWarnings("unchecked")
    protected ObjectContext parseBatchToXML() throws VocollectException {
        boolean outOfData = false;
        boolean done = false;
        FieldMap fields = null;
        ObjectContext objContext = null;
        
        this.getXmlrepresentation().clearXmlRepresentation();
        
        for (int i = 0; ((!outOfData) && !done); i++) {
            objContext = getRecord();
            fields = (FieldMap) objContext.getFieldMaps().get(0);

            //Set context to hold if this record should be skipped because of
            //duplication rules.
            objectContext.setSkipCurrentRecordBecauseOfDup(this.getIndexes()
                    .isSkipCurrentRecordBecauseOfDup());

            
            if (isAnyFailRecordIndexBlown()) {
                String logString = "Data on line: "
                    + getSourceAdapter().getCurrentLineNumber()
                    + " of file" + getDescription()
                    + " contains the following data duplications: "
                    + getIndexes().getRecordDataDuped();
                
                log.error(logString,
                        ImporterError.DUPLICATED_DATA);
                
                //prepare the argument for the messages
                Object[] args 
                          = { new Long(getSourceAdapter().getCurrentLineNumber()),
                              getSourceAdapter().getDataSourceName(),
                              getIndexes().getRecordDataDuped() };
                throw 
                new PrescanException(
                     DataSourceParserError.DUPLICATED_DATA,
                      new UserMessage(
                       LocalizedMessage.formatMessage(DataSourceParserError.DUPLICATED_DATA, args)));
            }

            
            
            
            
            if (isOutOfData()) {
                // For multi-line records, any open context needs to be closed.
                outOfData = true;
                getXmlrepresentation().makeFinalRecord(fields);
                if (0 < getXmlrepresentation().getXmlRepresentation().compareTo("")) {
                    elementCount++;
                }
            } else {
                // Make an XML stream from the fields.
                this.getXmlrepresentation()
                .makeXmlStream(fields, this.getDatatype());
                if (0 < getXmlrepresentation().getXmlRepresentation().compareTo("")) {
                    elementCount++;
                }
            }
            done = getXmlrepresentation().isContentReady();
            fields.reset();
        }
        if (done) {
                objectContext.setSourceLineNumber(this.getContextBeginsLineNumber());
                // For imports like assignment that have multiple lines, our triggers read ahead.
                //But for simple imports (op, itm, coreloc, palic, replen, rplloc, ctn) our triggers
                // do not need to read ahead.  The else below is set up for assignment-like imports.
                //If contextbegins == currentline then our trigger is still on same line, we
                //need to add one to the source line number to make error messages report the correct line.
                if (this.getContextBeginsLineNumber() == getSourceAdapter().getCurrentLineNumber()) {
                    //For simple imports (contextBegins == current line, we need to +1
                    setContextBeginsLineNumber(getSourceAdapter().getCurrentLineNumber() + 1);
                } else {
                    //For multi-line imports contextBegins <> currentline, everything reports OK as is.
                    setContextBeginsLineNumber(getSourceAdapter().getCurrentLineNumber());
                }
        }
        objectContext.setXmlRepresentation(getXmlrepresentation()
                .getXmlRepresentation());
        if (getXmlrepresentation().closeObjSource().get(0) != null) {
            objectContext.setFieldMaps(getXmlrepresentation().closeObjSource());
        }
        return objectContext;
    }

    /**
     * Gets the value of xmlrepresentation.
     * @return the xmlrepresentation
     */
    protected XmlTagger getXmlrepresentation() {
        return xmlrepresentation;
    }

    /**
     * Gets the value of contextBeginsLineNumber.
     * @return the contextBeginsLineNumber
     */
    protected long getContextBeginsLineNumber() {
        return contextBeginsLineNumber;
    }

    /**
     * Sets the value of the contextBeginsLineNumber.
     * @param contextBeginsLineNumber the contextBeginsLineNumber to set
     */
    protected void setContextBeginsLineNumber(long contextBeginsLineNumber) {
        this.contextBeginsLineNumber = contextBeginsLineNumber;
    }

    /**
     * Gets the value of datatype.
     * @return the datatype
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * Sets the value of the datatype.
     * @param datatype the datatype to set
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    /**
     * Gets the value of objectContext.
     * @return the objectContext
     */
    public ObjectContext<FieldMap, Object> getObjectContext() {
        return objectContext;
    }

    /**
     * Sets the value of the objectContext.
     * @param objectContext the objectContext to set
     */
    public void setObjectContext(ObjectContext<FieldMap, Object> objectContext) {
        this.objectContext = objectContext;
    }

    /**
     * Using the object mapper Castor, make a new whatever-it-is.
     * 
     * @param objData
     *            Object Context with the tagged data to convert.
     * @return A new instance of the object represented by the XML string passed
     *         in
     * @throws ImportConfigurationException
     *             if a configuration error occurs
     * @throws VocollectException
     *             on bad data transformation
     */
    @SuppressWarnings("unchecked")
    public ObjectContext populateObject(ObjectContext objData)
            throws ImportConfigurationException, VocollectException {

      InputSource xmlData = new InputSource(new StringReader(
      this.getXmlrepresentation().getXmlRepresentation()));


        Object result = null;
        // Using Castor...
        // 2. Unmarshal the data
        try {
            result = this.getUnmar().unmarshal(xmlData);
        } catch (MarshalException e) {
            /*
             * First try to figure out the configuration error by ourself by
             * looking for any unmapped fields. During configuration we store
             * the bind-xml name and field name map. Here using that map and
             * trying to identify if the exception is due to mis-mapped field.
             * If we can't detect by ourself then let castor figure it out and
             * log it's the exception
             */
            ArrayList<String> errorFieldList = validateTriggerFieldMapping();
            if (errorFieldList.size() > 0) {
                StringBuffer sb = new StringBuffer();
                for (String str : errorFieldList) {
                    sb.append("  ");
                    sb.append(str);
                }
                String errorFields = sb.toString();
                log
                        .fatal(
                                "Configuration error: Object mapping ("
                                        + getMappingFileName()
                                        + ") is incorrect, does not match the tagging set up in"
                                        + " the field definition file "
                                        + " Following fields are incorrectly mapped "
                                        + errorFields + ".",
                                ImporterError.BAD_OBJECT_FIELD_MAPPING);
//                this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);

                throw new ImportConfigurationException(
                        ImporterError.BAD_OBJECT_FIELD_MAPPING,
                        new UserMessage(
                                LocalizedMessage
                                        .formatMessage(ImporterError.BAD_OBJECT_FIELD_MAPPING),
                                getMappingFileName(), errorFields));
            } else {
                if (this.elementCount > 1) {
                    // Since at least one record passed, this is most likely a
                    // data error, isolated
                    // to the current record.
                    log.error("Bad data transformation.",
                            ImporterError.BAD_DATA_TRANSFORMATION);
                    throw new BadDataTransformException(
                            ImporterError.BAD_DATA_TRANSFORMATION,
                            new UserMessage(
                                    LocalizedMessage
                                            .formatMessage(ImporterError.BAD_DATA_TRANSFORMATION),
                                    getMappingFileName()), e);
                } else {
                    log.error("Configuration error: Object mapping ("
                                            + getMappingFileName()
                              + ") may be incorrect, a conversion error occurred when unmarshalling "
                              + "the XML for the object: see "
                              + getFieldDefinitionFile() + ".",
                                    ImporterError.BAD_OBJECT_MAPPING, e);
//                    this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);

                    throw new ImportConfigurationException(
                            ImporterError.BAD_OBJECT_MAPPING,
                            new UserMessage(
                                    LocalizedMessage
                                            .formatMessage(ImporterError.BAD_OBJECT_MAPPING),
                                    getMappingFileName()), e);
                }

            }
        } catch (ValidationException e) {
            log
                    .fatal(
                            getDescription()
                                    + ": Configuration error: Validation is not permitted in the object mapping "
                                    + getMappingFileName(),
                            ImporterError.VALIDATION_IN_MAPPING, e);

//            this.getStateChanges().setCurrentState(MISCONFIGURED_STATE);

            throw new ImportConfigurationException(
                    ImporterError.VALIDATION_IN_MAPPING,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(ImporterError.VALIDATION_IN_MAPPING),
                            getMappingFileName()));
        }
        objData.setModelObject(result);
        return objData;
    }

    /**
     * This method checks to see that trigger doesn't have field mapping
     * configuration issue.
     * 
     * @return ArrayList list of fields those are not mapped properly
     */
    private ArrayList<String> validateTriggerFieldMapping() {
        ArrayList<String> errorFieldName = new ArrayList<String>();
        if (null != this.getFieldToBindNameMap()) {
            if (this.getTriggers() != null) {
                for (CreateObjectTrigger cot : this.getTriggers()) {
                    if (log.isDebugEnabled()) {
                        log.info("The Trigger Object Name is "
                                + cot.getNewObjectName());
                    }
                    if (this.getFieldToBindNameMap().containsKey(cot.getNewObjectName())) {
                        String triggerNewObjName = cot.getNewObjectName();
                        HashMap<String, String> tempHashMap = 
                            this.getFieldToBindNameMap().get(triggerNewObjName);
                        for (Field field : cot.getFieldMap().values()) {
                            if (!field.isSuppress() 
                                    && !(tempHashMap.containsKey(field.getFieldName()))
                               ) {
                                errorFieldName.add(field.getFieldName());
                            }
                        }
                    }
                }
            }
        }
        return errorFieldName;
    }

    /**
     * Getter for fieldToBindNameMap property.
     * @return fieldToBindNameMap
     */
    public HashMap<String, HashMap<String, String>> getFieldToBindNameMap() {
        return fieldToBindNameMap;
    }

    /**
     * Sets the value of the fieldToBindNameMap.
     * @param fieldToBindNameMap the fieldToBindNameMap to set
     */
    public void setFieldToBindNameMap(
            HashMap<String, HashMap<String, String>> fieldToBindNameMap) {
        this.fieldToBindNameMap = fieldToBindNameMap;
    }
    
    /**
     * Set the list of new-object triggers. used by configuration.
     * 
     * @param triggers
     *            list of CreateObjectTrigger that this import uses.
     */
    public void setTriggers(ArrayList<CreateObjectTrigger> triggers) {
        this.getXmlrepresentation().setNewObjectTriggers(triggers);
    }

    /**
     * Get the list of new-object triggers. used by configuration.
     * 
     * @return the list of triggers
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return this.getXmlrepresentation().getNewObjectTriggers();
    }

    /**
     * Gets the value of unmar.
     * @return the unmar
     */
    public Unmarshaller getUnmar() {
        return unmar;
    }

    /**
     * Sets the value of the unmar.
     * @param unmar the unmar to set
     */
    public void setUnmar(Unmarshaller unmar) {
        this.unmar = unmar;
    }

    /**
     * Gets the value of lastLineLength.
     * @return the lastLineLength
     */
    public Long getLastLineLength() {
        return this.lastLineLength;
    }

    /**
     * Sets the value of the lastLineLength.
     * @param lastLineLength the lastLineLength to set
     */
    public void setLastLineLength(Long lastLineLength) {
        this.lastLineLength = lastLineLength;
    }

    /**
     * Gets the value of lastLineDiffers.
     * @return the lastLineDiffers
     */
    public boolean isLastLineDiffers() {
        return this.lastLineDiffers;
    }

    /**
     * Sets the value of the lastLineDiffers.
     * @param lastLineDiffers the lastLineDiffers to set
     */
    public void setLastLineDiffers(boolean lastLineDiffers) {
        this.lastLineDiffers = lastLineDiffers;
    }

    /**
     * Get the number of records read, primarily for error reporting.
     * @return the number of records read.
     */
    public abstract long getRecordsRead();

    /**
     * Clear input records.
     */
    public void clearInputRecords() {
        // Does nothing for now.
    }

    /**
     * Gets the value of stateChanges.
     * @return the stateChanges
     */
    public StateChanges getStateChanges() {
        return this.stateChanges;
    }

    /**
     * Sets the value of the stateChanges.
     * @param stateChanges the stateChanges to set
     */
    public void setStateChanges(StateChanges stateChanges) {
        this.stateChanges = stateChanges;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 