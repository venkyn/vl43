/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.BadDataTransformException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceParser;
import com.vocollect.voicelink.core.importer.DataSourceParserError;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ImportConfigurationException;
import com.vocollect.voicelink.core.importer.ImporterError;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.OutOfDataException;
import com.vocollect.voicelink.core.importer.PrescanException;
import com.vocollect.voicelink.core.importer.RecordLengthException;

import java.io.IOException;
import java.util.Iterator;

/**
 * Thsi is a concrete realization of the DataSourceParser. An instance
 * of this class will parse the incoming data into fields based on a fixed-length
 * schema passed in in the field definition file used for configuration.
 * This really manages breaking up the incoming string into fields, 
 * and parceling them out to a caller.
 * @author dgold
 * 
 */
public class FixedLengthDataSourceParser extends
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> {

    private static final Logger log          = new Logger(
                                                     FixedLengthDataSourceParser.class);

    private int                 recordLength = 0;
    // Length of a record, as defined by the configuration.

    private Iterator            currentField = null;
    // Iterator into list of fields, pointing to the 'current' one.
    // This is called by the function that manages getting the 'next' field
    
    // These two form the basis for a self-check, to make sure we did not skip any records.
    private long                recordsToRead = 0;
    // Internal, count of records that pre-scan found in the input source.
    
    private long                recordsRead   = 0;
    // internal, count of records read from the input source
    
    private boolean             duringPreScan = false;
    

    /**
     * Default constructor.
     * @throws ConfigurationException interface constraint allows the constructor to 
     * fail for bad configuration
     */
    public FixedLengthDataSourceParser() throws ConfigurationException {
        super();
        setPreScan(true);
    }

    /**
     * Check to see if EOF. If so, throw OUtOfData of record coutn excreptyion.
     * @return true if not eof
     * @throws RecordLengthException if the file length is not what we counted initially
     * @throws OutOfDataException if the eof condition is true & the file length is OK.
     */
    private boolean eofCheck() throws RecordLengthException, OutOfDataException {
        // check for end-of-data, throw out-of-data exception if so.
        if (this.getSourceAdapter().isEndOfData()) {
            throw new OutOfDataException();
        }
        return  true;
    }
    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#getRecord()
     */
    @Override
    public ObjectContext getRecord() throws VocollectException {

        if (!this.getSourceAdapter().isOpen()) {
            this.getSourceAdapter().openSource();
            setRecordsRead(0);
            setDescription("Fixed-length: " + getSourceAdapter().getDataSourceName());
        }

        eofCheck();
        
        this.getSourceAdapter().readRecord();
        incrementRecordsRead();

        // Check the defined record length against the actual.
        if (this.getSourceAdapter().getCurrentRecordLength() != this
                .getRecordLength()) {
            // Since we may need to read another record, and we always need the count to come out right
            // we hold and possibly restore the record count.
            long badRecordLength = getSourceAdapter().getCurrentRecordLength();
            long badRecord = this.getRecordsRead();
            if (isLastLineDiffers()) {
                // If we allow the last line to be different from the rest, read another line. 
                // We'd better hit EOF!
                if (null != this.getLastLineLength()) {
                    if (getLastLineLength().longValue() != getSourceAdapter().getCurrentRecordLength()) {
                        log.error("Record length not consistent with definition. Defined: "
                                + this.getRecordLength() + " Actual: "
                                + this.getSourceAdapter().getCurrentRecordLength() 
                                + " on record " + this.getRecordsRead() 
                                + " in " + getSourceAdapter().getDescription(),
                                DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH2);
                        throw new RecordLengthException(
                                DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH2,
                                new UserMessage(
                                        LocalizedMessage
                                        .formatMessage(DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH2),
                                        this.getRecordLength(), 
                                        this.getSourceAdapter().getCurrentRecordLength(),
                                        this.getRecordsRead(),
                                        this.getSourceAdapter().getDescription()
                                )
                        );
                    }
                }
                this.getSourceAdapter().readRecord();
                incrementRecordsRead();
                // This will throw an exception and aviod complaining about bad record length if at eof 
                eofCheck();
            } 
            log.error("Record length not consistent with definition. Defined: "
                    + this.getRecordLength() + " Actual: "
                    + badRecordLength 
                    + " on record " + badRecord 
                    + " in " + getSourceAdapter().getDescription(),
                    DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH);
            this.setRecordsRead(badRecord);
            this.getStateChanges().setCurrentState(DataSourceParser.IN_PROGRESS_FAILED_STATE);
            throw new RecordLengthException(
                    DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH,
                    new UserMessage(
                            LocalizedMessage
                            .formatMessage(DataSourceParserError.WRONG_FIXED_LENGTH_RECORD_LENGTH),
                            this.getRecordLength(), 
                            this.getSourceAdapter().getCurrentRecordLength(),
                            this.getRecordsRead(),
                            this.getSourceAdapter().getDescription()
                    )
            );
        }
        // Set iterator to null, so we create a new one.
        setCurrentField(null);

        // Set aside space for the data transfer
        char[] temp = new char[this.getRecordLength()];

        // For each field, convert to a fixed-length field and read 
        // the appropriate number of bytes from the data source adapter
        try {
            for (Field y : this.getFieldList().values()) {
                FixedLengthField x = (FixedLengthField) y;
                this.getSourceAdapter().read(temp, 0, x.getLength());
                x.setFieldData(temp, 0, x.getLength());
            }
        } catch (IOException e) {
            throw new VocollectException(DataSourceParserError.BAD_READ,
                    new UserMessage(LocalizedMessage
                            .formatMessage(DataSourceParserError.BAD_READ)));
        }

        Object d = null;                        
        ObjectContext<FixedLengthFieldMap, Object> objectContext = 
            new ObjectContext<FixedLengthFieldMap, Object>(this.getFieldList(), d);
        return objectContext; 
    }

    /**
     * This is called externally to get the fields sequentially. Return null 
     * when reading off the end of the list.
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#getNextField()
     */
    @Override
    public FixedLengthField getNextField() throws OutOfDataException {
        if (getSourceAdapter().isEndOfData()) {
            throw new OutOfDataException();
        }
        if (getCurrentField() == null) {
            setCurrentField(getFieldList().values().iterator());
        }
        if (getCurrentField().hasNext()) {
            return (FixedLengthField) getCurrentField().next();
        }
        return null;
    }

    /**
     * This method gets the particular field object of fieldName.
     * 
     * @param fieldName
     *            of the Field object you wish to return
     * @return the Field object with the specific fieldName
     */
    public Field getField(String fieldName) {
        return getFieldList().get(fieldName);
    }


    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#configure()
     * @throws ConfigurationException CastorConfiguration throws the config exception
     */
    @Override
    public boolean configure() throws ConfigurationException {

        if ((null == getMappingFileName())
                || (0 == getMappingFileName().compareTo(""))
                || (null == getConfigFileName())
                || (0 == getConfigFileName().compareTo(""))) {
            return false;
        }

        CastorConfiguration cc = new CastorConfiguration(getMappingFileName(),
                getConfigFileName());

        this.setFieldList((FixedLengthFieldMap) cc.configure());

        // The field list keeps track of the starting position of the (next) field.
        // After all the fields have been configured, the starting position is at the 
        // end-of-record position.
        this.setRecordLength(getFieldList().getStartingPosition());

        return true;
    }


    /**
     * For a fixed length record, we do a prescan to verify the record length.
     * We also update the Indexes so they can be queried later.
     * @throws VocollectException for other errors
     * @throws PrescanException when prescan error occurs
     * @return true if all lines are the same length. One, prescan for correct
     *         record length Two, prescan and fill caches
     */
    @Override
    public boolean preScan() throws VocollectException, PrescanException {
        Long charCount = 0L;
        boolean result = true;
        // Reset the counts for the sanity check.
        setRecordsRead(0);
        setRecordsToRead(0);
        //Reset the recordsInProperOrder array to null because we are starting
        // a new file in prescan.
        this.getSourceAdapter().resetRecordsInProperOrder();
        
        
        // If we want the datasource prescanned (typically for files, could be databases)
        if (this.isPreScan()) {
            setDuringPreScan(true);
            //reset our indexes for next file.
            this.getIndexes().reset();
            
            this.getIndexes().setUp(this.getFieldList());
            // For every record
            this.getSourceAdapter().openSource();
            while (true) {
                // This throws an exception if the line length in the source differes from the
                // value that was calculated
                try {
                    ObjectContext objectContext = this.getRecord();
                } catch (OutOfDataException o) {
                    // Expected
                }
                incrementRecordsToRead();
                if (this.isOutOfData()) {
                    break;
                }
                //If we are using a grouping index, also include a start & length
                if (this.getIndexes().containsGroupingIndex()) {
                     this.getIndexes().setCharCount(charCount);
                    this.getIndexes().setRecordLength(new Long(this.recordLength));
                }
                
                this.getIndexes().updateIndexes();
                //update the charCount to include this record (for next record's index).
                charCount = charCount + this.recordLength;
            }
            // Log the result.
            if (log.isInfoEnabled()) {
                log.info("Read " + getRecordsToRead() + " records from input source " 
                        + getSourceAdapter().getDescription(), 
                        DataSourceParserError.PRESCAN_RESULT);
            }
            if (this.isAnyFailFileIndexBlown()) {
                result = false;
                log.error("The file: " + getSourceAdapter().getDescription()
                    + " contains duplicated data that has caused this file to fail.", 
                        DataSourceParserError.DUPLICATED_DATA_FAILS_FILE);
               
               if (getSourceAdapter() != null) {
                    this.close();
               }
               
               throw new PrescanException(
                    DataSourceParserError.DUPLICATED_DATA_FAILS_FILE,
                    new UserMessage(
                            LocalizedMessage
                                    .formatMessage(DataSourceParserError.DUPLICATED_DATA_FAILS_FILE)
                                    , getSourceAdapter().getDataSourceName()));
                                    
            }
            if (getSourceAdapter() != null) {
                this.close();
            }
            if (this.getIndexes().containsGroupingIndex()) {
                //If we have a Grouping index, here's where we should build the
                // proper order array and send it to the adapter to sort the file.
                this.getSourceAdapter().setRecordsInProperOrder(
                    this.getIndexes().getGroupingIndex().getProperOrder());
            }
        }
        setDuringPreScan(false);
        return result;
    }
    
    

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        super.close();
        this.setRecordsRead(0);
    }

    /**
     * Gets the length of the record.
     * @return the value of the record length 
     */
    public int getRecordLength() {
        return recordLength;
    }

    /**
     * Sets the length of the record.
     * @param recordLength length of the record.
     */
    public void setRecordLength(int recordLength) {
        this.recordLength = recordLength;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#isOutOfData()
     */
    @Override
    public boolean isOutOfData() {
        return getSourceAdapter().isEndOfData();
    }

    /**
     * Sets the value of the currentField.
     * @param currentField the currentField to set
     */
    void setCurrentField(Iterator currentField) {
        this.currentField = currentField;
    }

    /**
     * Gets the value of currentField.
     * @return the currentField
     */
    Iterator getCurrentField() {
        return currentField;
    }

    /**
     * Gets the value of recordsRead.
     * @return the recordsRead
     */
    @Override
    public long getRecordsRead() {
        return recordsRead;
    }

    /**
     * Sets the value of the recordsRead.
     * @param recordsRead the recordsRead to set
     */
    private void setRecordsRead(long recordsRead) {
        this.recordsRead = recordsRead;
    }
    
    /**
     * increment the number of records read by the parser.
     */
    private void incrementRecordsRead() {
        this.recordsRead += 1;
        return;
    }

    /**
     * Gets the value of recordsToRead.
     * @return the recordsToRead
     */
    private long getRecordsToRead() {
        return recordsToRead;
    }

    /**
     * Sets the value of the recordsToRead.
     * @param recordsToRead the recordsToRead to set
     */
    private void setRecordsToRead(long recordsToRead) {
        this.recordsToRead = recordsToRead;
    }
    
    /**
     * increment the number of records to read by the parser.
     */
    private void incrementRecordsToRead() {
        this.recordsToRead += 1;
        return;
    }

    /**
     * {@inheritDoc}
     * @return the completed object context
     * @throws VocollectException see parseBatchToXML
     * @throws ImportConfigurationException for configuration - XML - errors
     * @throws BadDataTransformException if the data cannot be transformed to the object datatype
     */
    @Override
    public ObjectContext getElement() 
        throws VocollectException, ImportConfigurationException, BadDataTransformException 
        {
        
        getObjectContext().clear();
        // Using the raw data, parse it into an XML representation
        parseBatchToXML();
        
        // The hierarchical datatypes require that we re-enter the parsing
        // routine once pase the last record (actually, any field-change record does)
        // The one-record-per-line types don't, and will cause an error if we do.
        // So, we look to see if we are out of data AND there is no XML to convert.
        if (this.isOutOfData() && (getObjectContext().getXmlRepresentation().length() == 0)) {
            return getObjectContext();
        }
        
        // Make an object from the stream, using Castor
        try {
            populateObject(getObjectContext());
        } catch (ImportConfigurationException ice) {
            // Config errors stop the import
            log.error("Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation()
                    + "'", ImporterError.NEW_OBJECT_CREATION_FAILURE);
            throw ice;
        } catch (BadDataTransformException e) {
            // At least one record is imported so it is bad data
            log.error("Failed to create new object. XML stream: '"
                    + this.getXmlrepresentation().getXmlRepresentation()
                    + "'", ImporterError.NEW_OBJECT_CREATION_FAILURE);
            throw e;
        }
        return getObjectContext();
    }

    /**
     * Gets the value of duringPreScan.
     * @return the duringPreScan
     */
    private boolean isDuringPreScan() {
        return this.duringPreScan;
    }

    /**
     * Sets the value of the duringPreScan.
     * @param duringPreScan the duringPreScan to set
     */
    private void setDuringPreScan(boolean duringPreScan) {
        this.duringPreScan = duringPreScan;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 