/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.parsers;

/**
 * Interface that must be implemented for objects imported via table.
 * @author dgold
 *
 */
public interface ImportObject {
    
    /**
     * Gets the value of inProgress.
     * @return the inProgress
     */
    public boolean isInProgress();

    /**
     * Sets the value of the inProgress.
     * @param inProgress the inProgress to set
     */
    public void setInProgress(boolean inProgress);
    

    /**
     * Gets the value of completed.
     * @return the completed
     */
    public boolean isCompleted();

    /**
     * Sets the value of the completed.
     * @param completed the completed to set
     */
    public void setCompleted(boolean completed);
    
    /**
     * Member that generates a new model object from the data in this.
     * Don't have to do database lookups, justtransfer the data to the new
     * model object. The data used for the lookups is contained in the field 
     * maps used by the triggers.
     * 
     * @return a new model object.
     */
    public Object convertToModel();


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 