/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.ImporterError;

/**
 * Implements the common functionality of the Importable interface.
 * @author dgold
 *
 */
public class ImportableImpl implements Importable {
    
    private boolean completed     = false;
    private boolean inProgress    = false;
    private Long  importID;
    private String  siteName      = "Default";
    private int     importStatus  = 0;

    /**
     * {@inheritDoc}
     * @return nothing, throws an exception
     */
    public Object convertToModel() throws VocollectException {
        // Cannot use this, need to implement for the class you are making importable.
        throw new VocollectException(ImporterError.MUST_IMPLEMENT, 
                new UserMessage("impl.convertToModel.notImplemented"));
    }

    /**
     * {@inheritDoc}
     * @return
     */
    public boolean isCompleted() {
        return completed;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    public boolean isInProgress() {
        return inProgress;
    }

    /**
     * {@inheritDoc}
     * @param completed
     */
    public void setCompleted(boolean completedParam) {
        this.completed = completedParam;
        this.importStatus = 2;
    }

    /**
     * {@inheritDoc}
     * @param inProgress
     */
    public void setInProgress(boolean inProgressParam) {
        this.inProgress = inProgressParam;
        this.importStatus = 1;
    }

    /**
     * Gets the value of importID.
     * @return the importID
     */
    public Long getImportID() {
        return this.importID;
    }

    /**
     * Sets the value of the importID.
     * @param importID the importID to set
     */
    public void setImportID(Long importID) {
        this.importID = importID;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * {@inheritDoc}
     * @param siteName
     */
    public void setSiteName(String siteNameParam) {
        this.siteName = siteNameParam;
        
    }

    /**
     * Gets the value of importStatus.
     * @return the importStatus
     */
    public int getImportStatus() {
        return importStatus;
    }

    /**
     * Sets the value of the importStatus.
     * @param status the importStatus to set
     */
    public void setImportStatus(int status) {
        this.importStatus = status;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 