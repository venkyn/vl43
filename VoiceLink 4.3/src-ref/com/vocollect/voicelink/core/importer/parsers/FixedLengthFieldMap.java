/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.voicelink.core.importer.FieldMap;

/**
 * @author dgold
 */
public class FixedLengthFieldMap extends FieldMap {

    private static final long serialVersionUID = 7006728084819540835L;

    /**
     * Default empty constructor.
     */
    public FixedLengthFieldMap() {
        super();
    }

    /**
     * Constructor that sets the size of the map.
     * @param size specifies the size to set.
     */
    public FixedLengthFieldMap(int size) {
        super(size);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 