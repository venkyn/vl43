/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceParser;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.NoWorkException;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.OutOfDataException;

/**
 * @author dgold
 *
 */
public class PurgeParser extends DataSourceParser {
    
    /**
     * Default c'tor. 
     * @throws ConfigurationException if misconfigured.
     */
    public PurgeParser() throws ConfigurationException {
        super();
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws ConfigurationException
     */
    @Override
    public boolean configure() throws ConfigurationException {
        // Nothing to do here. This would typically set up the field list
        return true;
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws VocollectException
     */
    @Override
    public ObjectContext getElement() throws VocollectException {
        // Delete files.
        while (getSourceAdapter().openSource()) {
            if (getSourceAdapter().isEndOfData()) {
                throw new NoWorkException();
            }
            while (!getSourceAdapter().isEndOfData()) {
                getSourceAdapter().readRecord();
            }
        }
        throw new OutOfDataException();
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws OutOfDataException
     */
    @Override
    public Field getNextField() throws OutOfDataException {
        // Nothing to do
        return null;
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws VocollectException
     */
    @Override
    public ObjectContext getRecord() throws VocollectException {
        // Nothing to do
        return null;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public long getRecordsRead() {
        // Nothing to do
        return this.getSourceAdapter().getCurrentLineNumber();
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public boolean isOutOfData() {
        
        return this.getSourceAdapter().isEndOfData();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 