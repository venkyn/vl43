/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.parsers;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.importer.BadDataTransformException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.DataSourceAdapter;
import com.vocollect.voicelink.core.importer.DataSourceParser;
import com.vocollect.voicelink.core.importer.DataSourceParserError;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ImportConfigurationException;
import com.vocollect.voicelink.core.importer.LocalizedMessage;
import com.vocollect.voicelink.core.importer.NoWorkException;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.OutOfDataException;
import com.vocollect.voicelink.core.importer.PersistenceManager;
import com.vocollect.voicelink.core.importer.exporters.ExportConfigurationException;
import com.vocollect.voicelink.core.importer.exporters.ExporterError;
import com.vocollect.voicelink.core.model.ExportTransportObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.StringTokenizer;


// import com.vocollect.voicelink.core.importer.DataSourceParserError.*;

/**
 * This class will get an object from the database (query) and will output a
 * list of fields.
 * @author jtauberg based on fixedLengthDataSourceParser by dgold
 */
public class DatabaseDataSourceParser extends
    DataSourceParser<FixedLengthFieldMap, FixedLengthField> {

    private static final Logger     log                   = new Logger(
                                                              DatabaseDataSourceParser.class);

    // Flag that if set, will return all records instead of using named query.
    private boolean                 queryAll              = false;
    
    // Flag to indicate whether queryResultObjectName is passed or queryResultObject itself passed
    private boolean                 objectNameString      = false;

    // Name of the named query for the lookup to use.
    private String                  queryName             = null;

    // Name of the ExportTransportObject whose fields are to be returned by the query.
    private String                  queryResultObjectName = null;
    
    // Object whose fields are to be returned by the query.
    private Object              queryResultObject     = null;
    
    // Object implementinf Importable, for the Database import.
    private Importable          importObject            = null;
    
    // Export Transport Object whose fields are to be returned by the query. This object 
    // set by the markup when objectNameString = false
    private ExportTransportObject      exportTransportObject     = null;

    
    // List of keyFields used by the query.
    private FieldMap keyFields             = new FieldMap();
    
    // This is the access point for the DAO layer.
    private PersistenceManager      pm                    = new PersistenceManager();

    // This is the record pointer for the current record in dbRecordsList.
    private Iterator<? extends Object>  currentRecord         = null;

    // This is the field pointer for the current field in
    // this.getFieldList().values().
    private Iterator<? extends Field>                currentField          = null;

    // This list stores the results of the database query
    private List<Object>        dbRecordsList         = null;

    // This is the internal dbLookup
    private DBLookup                dbLook                = new DBLookup(pm);

    // private variable stores status of query.
    private boolean                 queryOpen             = false;

    // Set aside string for field data.
    private String                  fieldData             = null;
    
    //This indicate whether to set the status as Exported or not during export process
    private boolean updateToExported                      = false;
    
    //This indicate whether to set the status as in-progess or not during export process
    private boolean exportInProgress                      = false;

    // Name of query to use for preprocessing the dataset, typically used for multi-step exports
    private String preProcessQuery                        = null;
    
    // Name of query to use for postprocessing the dataset, typically used for multi-step exports
    private String postProcessQuery                       = null;


    // Internal map to keep track for fields name in the markup and coresponding
    // nested objects for that field
    private HashMap<String, ArrayList<String>> nestedFieldsMap = new HashMap<String, ArrayList<String>>();
    
    // This contains the list of selective field specified in markup which
    // needs to be exported
    private ArrayList<Field> exportFields = null;

    // This is a internal map which holds the fields which needs to be exported
    // based on the Markup
    private LinkedHashMap<String, String> exportHashMap = null;

    // This is to store the list of object which needs to be exported based on
    // the markup
    private List<HashMap<String, String>> exportRecordList = new LinkedList<HashMap<String, String>>();
    
    // Flag that gets set when we read past the last element of the dataset.
    private boolean outOfData = false;


    /**
     * Gets the value of keyFields.
     * @return the keyFields
     */
    
    public FieldMap getKeyFields() {
        return keyFields;
    }

    /**
     * Sets the value of the keyFields.
     * @param keyFields the keyFields to set
     */
    public void setKeyFields(FieldMap keyFields) {
        if (!queryOpen) {
            this.keyFields = keyFields;
        } else {
            log
                .fatal(
                    "Interface sequence error on "
                        + this.getClass().getName()
                        + " - setKeyFields may not be changed while query is open.",
                    DataSourceParserError.INTERFACE_SEQUENCE_ERROR_1);
            throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - setKeyFields may not be changed while query is open.");
        }
    }

    /**
     * @param fieldToAdd - a field to add to keyFields.
     */
    public void addField(Field fieldToAdd) {
         this.keyFields.addField(fieldToAdd);
    }

    /**
     * Getter for exportFields property.
     * @return exportFields Arraylist of fields which needs to be exported
     */
    public ArrayList<Field> getExportFields() {
        return this.exportFields;
    }

    /**
     * This adds the Fields to the ArrayList exportFields property.
     * @param field Field which needs to be added to the exportFields
     */
    public void addExportField(Field field) {
        if (this.exportFields == null) {
            exportFields = new ArrayList<Field>();
        }
        this.exportFields.add(field);
    }


    /**
     * Getter for the queryAll property.
     * this is just for testing that's why keeping it here as private
     * @return boolean value of the property
     */
    public boolean isQueryAll() {
        return this.queryAll;
    }

    /**
     * Setter for the queryAll property. If TRUE, parser will return all
     * records. Note that specifying a queryName will override this setting.
     * this is just for testing that's why keeping it here as private
     * @param queryAll the new queryAll value
     */
    public void setQueryAll(boolean queryAll) {
        if (!queryOpen) {
            this.queryAll = queryAll;
        } else {
            log.fatal(
                "Interface sequence error on " + this.getClass().getName()
                    + " - setQueryAll may not be changed while query is open.",
                    DataSourceParserError.INTERFACE_SEQUENCE_ERROR_2);
            throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - setQueryAll may not be changed while query is open.");
        }
    }

    /**
     * Getter for the updateToExported property.
     * @return boolean value of the property
     */
    public boolean getUpdateToExported() {
        return this.updateToExported;
    }

    /**
     * Setter for the updateToExported property. If TRUE, parser 
     * will call the ETO method to set the status as exported after the
     * object has been extracted from the database.
     * @param updateToExported the new updateToExported value
     */
    public void setUpdateToExported(boolean updateToExported) {
         this.updateToExported = updateToExported;    
    }
    
    /**
     * Getter for the exportInProgress property.
     * @return boolean value of the property
     */
    public boolean getExportInProgress() {
        return this.exportInProgress;
    }

    /**
     * Setter for the exportInProgress property. If TRUE, parser 
     * will call the ETO method to set the status as in-progress after the
     * object has been extracted from the database.
     * @param exportInProgress the new exportInProgress value
     */
    public void setExportInProgress(boolean exportInProgress) {
         this.exportInProgress = exportInProgress;    
    }


    
    /**
     * Getter for the queryName property.
     * @return String value of the property
     */
    public String getQueryName() {
        return this.queryName;
    }

    /**
     * Setter for the queryName property.
     * @param queryName the new queryName value
     */
    public void setQueryName(String queryName) {
        if (!queryOpen) {
            this.queryName = queryName;
        } else {
            log
                .fatal(
                    "Interface sequence error on "
                        + this.getClass().getName()
                        + " - setQueryName may not be changed while query is open.",
                        DataSourceParserError.INTERFACE_SEQUENCE_ERROR_3);
            throw new RuntimeException("Interface sequence error for "
                + this.getClass().getName()
                + " - setQueryName may not be changed while query is open.");
        }
    }

    /**
     * Getter for objectNameString.
     * @return objectNameString boolean
     */
    public boolean getObjectNameString() {
        return this.objectNameString;
    }
    
    /**
     * Setter for objectNameString.
     * @param objectNameString boolean - indicate whether Object 
     */
    public void setObjectNameString(boolean objectNameString) {
        if (!queryOpen) {
           this.objectNameString = objectNameString;    
        }        
    }

    
    /**
     * Getter for the queryResultObject property.
     * This returns the object which will be as of now either eto or
     * model object.
     * The object is set by the name only - setQueryResultObjectName
     * @return Object value of the property
     */
    private Object getQueryResultObject() {
        return this.queryResultObject;    
    }

    /**
     * Getter for the exportTransportObject property.
     * @return the Object
     */
    public ExportTransportObject getExportTransportObject() {
        return this.exportTransportObject;           
    }

    
    /**
     * Setter for the exportTransportObject property.
     * @param exportTransportObject the new object
     */
    public void setExportTransportObject(ExportTransportObject exportTransportObject) {
        if (!queryOpen) {
            //Only set this object when the object is passed not the object name
            if (!getObjectNameString()) {
                this.exportTransportObject = exportTransportObject;   
                this.queryResultObject = exportTransportObject;
            }            
         } else {
            log
                .fatal(
                    "Interface sequence error on "
                        + this.getClass().getName()
                        + " - setQueryResultObjectName may not be changed while query is open.",
                        DataSourceParserError.INTERFACE_SEQUENCE_ERROR_4);
            throw new RuntimeException(
                "Interface sequence error for "
                    + this.getClass().getName()
                    + " - setQueryResultObjectName may not be changed while query is open.");
        }
    }

    /**
     * Getter for the queryResultObjectName property.
     * The object is set by the name only - setQueryResultObjectName
     * @return String value of the property
     */
    public String getQueryResultObjectName() {
        return this.queryResultObjectName;           
    }

    
    /**
     * Setter for the queryResultObjectName property.
     * @param queryResultObjectName the new queryResultObjectName value
     */
    public void setQueryResultObjectName(String queryResultObjectName) {
        if (!queryOpen) {
          if (getObjectNameString()) {
              this.queryResultObjectName = queryResultObjectName;
              try {
                  // Create a Object based on this name.
                  this.queryResultObject = DBLookup
                      .makeNewObject(queryResultObjectName);
              } catch (Exception e) {
                  log
                      .fatal(
                          this.getClass().getName()
                              + " was configured "
                              + "with a queryResultObjectName of: "
                              + queryResultObjectName
                              + ".  This parameter must be configured with a class that is "
                              + "based on a ExportTransportObject.  ",
                          DataSourceParserError.BAD_QUERY_RESULT_OBJECT);
                  throw new RuntimeException(
                      this.getClass().getName()
                          + " was configured "
                          + "with a queryResultObjectName of: "
                          + queryResultObjectName
                          + ".  This parameter must be configured with a class that is "
                          + "based on a ExportTransportObject.");
              }              
          }
        } else {
            log
                .fatal(
                    "Interface sequence error on "
                        + this.getClass().getName()
                        + " - setQueryResultObjectName may not be changed while query is open.",
                        DataSourceParserError.INTERFACE_SEQUENCE_ERROR_4);
            throw new RuntimeException(
                "Interface sequence error for "
                    + this.getClass().getName()
                    + " - setQueryResultObjectName may not be changed while query is open.");
        }
    }

    /**
     * Default constructor.
     * @throws ConfigurationException interface constraint allows the
     *             constructor to fail for bad configuration
     */
    public DatabaseDataSourceParser() throws ConfigurationException {
        super();
    }

    
    /**
     * getSourceAdapter can not be used in DatabaseDataSourceParser because
     * database does not have an adapter... it uses jdbc/hibernate as adapter.
     * @throws RuntimeException if called.
     * @return will never return a DataSourceAdapter, there is none.
     */
    @Override
    public DataSourceAdapter getSourceAdapter() throws RuntimeException {
        // return null, there is no adapter for this type of connection.
        return null;
    }

    /**
     * Fills DBRecordsList with results of query. /**
     * @return a list containing the elements from the DB that were the results
     *         of the query. Empty list if no data found.
     */
    @SuppressWarnings("unchecked")
    public List<Object> lookup() {
        //Only need to populate values 
        if (queryResultObject instanceof DataObject) {
            Class<? extends Object> queryClass = queryResultObject.getClass();
            String fieldSetterName = null;
            
            for (Field f : this.keyFields.values()) {
                try {
                    char[] temp = f.getFieldName().toCharArray();
                    temp[0] = Character.toUpperCase(temp[0]);                
                    fieldSetterName = "set" + String.valueOf(temp);
                    
                    Class[] partypes = new Class[1];
                    partypes[0] = String.class;
                    Method m = queryClass.getMethod(fieldSetterName, partypes);
                    Object[] arglist = new Object[1];
                    arglist[0] = f.getFieldData();
                    m.invoke(queryResultObject, arglist);
                } catch (Exception e) {
                    String message = "Caught " + e.getClass() 
                                     + " while trying to get or invoke the method named "
                                     + queryClass.getCanonicalName() + "." + fieldSetterName;
                    // This is due to one a member access problem, 
                    // and represents a problem accessing the named parameter in the subject.
                    // Most likely due to a typo or other name-related issue
                    log.error(message, DataSourceParserError.ILLEGAL_MEMBER_ACCESS);
                    throw new RuntimeException(message);
                    // TODO Need user message?
                }
              
            }
              
        }
      return dbLook.lookup(queryResultObject, queryName, keyFields);
    }

    /**
     * Fills DBRecordsList with results of query. 
     * @return a list containing the elements from the DB that were the results
     *         of the query. Empty list if no data found.
     * @param subject - the subject to lookup
     */
    @SuppressWarnings("unchecked")
    public List<Object> lookup(Object subject) {
        for (Field field : this.keyFields.values()) {
            try {
                field.setFieldData(dbLook.getCompositePart(subject, field.getFieldName()).toString());
            } catch (VocollectException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return dbLook.lookup(subject, getQueryName(), keyFields);
    }
    /**
     * Look up all objects like the example object in the database. Mine the
     * given object for pertinent data, as defined by the params. Ask the
     * PersistenceManager for the data. Return an empty list - which preserves
     * order - if no data found.
     * 
     */
    /**
     * @param exampleInstance the instance to use as an example.
     * @return a list containing the elements from the DB that matched the
     *         example. Empty list if no data found.
     * @throws DataAccessException when problems accessing database.
     * this is just for testing that's why keeping it here as private
     */
    @SuppressWarnings("unchecked")
    private List<Object> lookupAll(Object exampleInstance)
        throws DataAccessException {
        Class c = exampleInstance.getClass();

        // Look up an element, which might be a list or an element
        try {
            List<Object> resultList = pm.getAll(c);
            return resultList;
        } catch (DataAccessException e) {
            log.fatal(
                "lookupAll method caused DataAccessException in "
                    + this.getClass().getName(),
                DataSourceParserError.LOOKUP_ALL_FAILED);
            throw e;
        }
    }

    /**
     * Tries to open query and fill dbRecordsList. Also sets currentRecord and
     * CurrentField.
     * @return true if query open succeeds. false if not.
     * @throws ExportConfigurationException if the export is misconfigured
     */
    @SuppressWarnings("unchecked")
    private boolean openQuery() throws ExportConfigurationException {
        if (!queryOpen) {
            setOutOfData(false);
            if (0 == getSiteName().trim().compareTo("")) {
                setSiteName("Default");
            }
            this.pm.setSiteName(getSiteName());
            if (null != this.getImportObject()) {
                if (this.getImportObject() instanceof Importable) {
                    this.getImportObject().setSiteName(this.getSiteName());
                }
                try {
                    if ((null == getQueryName()) || (getQueryName().length() == 0)) {
                        dbRecordsList = this.lookupAll(this.getImportObject());
                    } else {
                        dbRecordsList = this.lookup(getImportObject());
                    }
                } catch (DataAccessException e) {
                    log.fatal(
                        this.getClass().getName() + " LookupAll query "
                            + "caused a dataAccessException",
                        DataSourceParserError.QUERY_FAILED);
                    throw new RuntimeException(this.getClass().getName()
                        + " LookupAll query caused a DataAccessException.");
                }
            } else if (queryAll && (queryName == null)) {
                // Lookup all.
                try {
                    dbRecordsList = this.lookupAll(queryResultObject);
                } catch (DataAccessException e) {
                    log.fatal(
                        this.getClass().getName() + " LookupAll query "
                            + "caused a dataAccessException",
                        DataSourceParserError.QUERY_FAILED);
                    throw new RuntimeException(this.getClass().getName()
                        + " LookupAll query caused a DataAccessException.");
                }
            } else {                
                // Named query Lookup make sure the object is set
                if (this.getQueryResultObject() == null) {
                    log
                    .fatal(
                        this.getClass().getName()
                            + " was not configured "
                            + ".  This parameter must be configured with a class that is "
                            + "based on a ExportTransportObject.  ",
                        DataSourceParserError.BAD_QUERY_RESULT_OBJECT);
                throw new RuntimeException(
                    this.getClass().getName()
                        + " was not configured "
                        + ".  This parameter must be configured with a class that is "
                        + "based on a ExportTransportObject.");
                }
                try {
                    dbRecordsList = this.lookup();
                } catch (Exception e) {
                    log.fatal(
                        this.getClass().getName() + " named query: "
                            + queryName + " caused an Exception",
                        DataSourceParserError.QUERY_FAILED_2, e);
                    throw new RuntimeException(this.getClass().getName()
                        + " named query: " + queryName
                        + " caused an Exception.", e);
                }
            }
        }
        queryOpen = true;

        if (this.getExportFields() != null) {
            parseExportField(this.getExportFields());
            getExportFieldsMap(dbRecordsList, this.nestedFieldsMap);            
            currentRecord = exportRecordList.iterator();
        } else {
            currentRecord = dbRecordsList.iterator();    
        }
        currentField = this.getFieldList().values().iterator();
        return true;
    }
    
    /**
     * This parse the ArrayList of fields and prepare a Map
     * where key(String) the name of the field and
     * value(Arraylist) contains all the nested object information if any.
     * @param exportFieldList ArrayList of markup fields. Last one in the
     *            Arraylist will be the fieldName which needs to be exported.
     */
    private void parseExportField(ArrayList<Field> exportFieldList) {
        String fieldSeparator = ".";
        for (Field myField : exportFieldList) {
            String fieldDetail = myField.getFieldName();
            ArrayList<String> nestedFields = new ArrayList<String>();
            StringTokenizer st = new StringTokenizer(fieldDetail, fieldSeparator, false);

            while (st.hasMoreTokens()) {
               nestedFields.add(st.nextToken()); 
            }
            nestedFieldsMap.put(fieldDetail, nestedFields);
        }   
        
   }


    /**
     * This method checks if we have more than one nested list.
     * If so will throw the exception else will return the first level object 
     * name for the collection object. 
     * @param queryObjName String Name of the main object which is queried
     * @param nestedFldMap FieldMap from Markup. This contains the field which needs to be exported.
     * @return String method name which retuns the list
     * @throws ExportConfigurationException when unable to find the field getter or more than collection
     * for the nested objects
     */
    private String getListFieldName(Object queryObjName, HashMap<String, ArrayList<String>> nestedFldMap) 
                  throws ExportConfigurationException {
        String propName = null;
        Class<? extends Object> queryClass = queryObjName.getClass();
        Method[] theMethods = queryClass.getMethods();   
        int listCounter = 0;
        for (ArrayList<String> fieldList : nestedFldMap.values()) {
            String topLevelField = fieldList.get(0);
            char[] temp = topLevelField.toCharArray();
            temp[0] = Character.toUpperCase(temp[0]);
            String fieldGetterName = "get" + String.valueOf(temp);
            // Check if the getter exists
            boolean foundMethod = false;
            Method fieldMethod = null;            
            for (Method m : theMethods) {
                if (0 == fieldGetterName.compareTo(m.getName())) {
                    foundMethod = true;
                    fieldMethod = m;
                    break;
                }
            }
            
            if (!foundMethod) {
               //We expect to find the getter and setter for each field
               //This will be configuring issue. We aren't doing this check explicitly 
               //but we happened to be checking here.Here we are looking for the first level of objects only

                log.error("Unable to find the getter method for field name: " + topLevelField 
                    + " in the object: " + queryClass.getName()
                    , ExporterError.UNABLE_TO_FIND_FIELD_GETTER);
                throw new ExportConfigurationException(
                                 ExporterError.UNABLE_TO_FIND_FIELD_GETTER,
                                 new UserMessage(LocalizedMessage
                                         .formatMessage(ExporterError.UNABLE_TO_FIND_FIELD_GETTER),
                                         topLevelField, queryClass.getName()));

            } else {
                Class<?> retClass = fieldMethod.getReturnType();
                
                if (retClass.getName().equals("java.util.List") || retClass.getName().equals("java.util.Set")) {
                    if (listCounter == 0) {
                        propName = topLevelField;
                        listCounter = listCounter + 1;
                    } else if (propName.equals(topLevelField)) {
                        continue;
                    } else {
                        //Throw exception. This we are enforcing as
                        //business rule as data will not make sense. 
                        log.error("Markup has specified more than one list objects for " 
                            + "queried object: " + queryClass.getName(), 
                            ExporterError.MORE_THAN_ONE_LIST_OBJECT_REQUESTED_FROM_QUERIED_OBJECT);
                        throw new ExportConfigurationException(
                                         ExporterError.MORE_THAN_ONE_LIST_OBJECT_REQUESTED_FROM_QUERIED_OBJECT,
                                         new UserMessage(LocalizedMessage.formatMessage(
                                             ExporterError.MORE_THAN_ONE_LIST_OBJECT_REQUESTED_FROM_QUERIED_OBJECT),
                                                 topLevelField, queryClass.getName()));
                    }
                }
            }
        }
       
        return propName;
    }
    
    /**
     * This will prepare a internal HashMap based on the export fields specified
     * in Markup.
     * @param dbRecords list of object got from the Markup query
     * @param nestedObjectMap HashMap of markup fields which needs to be exported
     * @throws ExportConfigurationException throws when configuration issues occurs
     */
     @SuppressWarnings("unchecked")
    private void getExportFieldsMap(
         List<Object> dbRecords, HashMap<String, ArrayList<String>> nestedObjectMap)
                  throws ExportConfigurationException {
        Object theDbObject = null;
        Object theCheckObj = null;    
        String listFieldName = null;
        
        if (dbRecords.size() > 0) {
            Object checkObject = dbRecords.get(0);
            if (checkObject instanceof Map) {
                // Get the object from the map
                Map internalMap = (Map) checkObject;
                Iterator it = internalMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    if (pairs.getKey().equals("source")) {
                        theCheckObj = pairs.getValue();
                        break;
                    }
                }
            } else {
                theCheckObj = checkObject;
            }
            
           listFieldName = getListFieldName(theCheckObj, nestedObjectMap);            
        }

        for (Object dbObject : dbRecords) {
                //processingObject = true;
                if (dbObject instanceof Map) {
                    // Get the object from the map
                    Map internalMap = (Map) dbObject;
                    Iterator it = internalMap.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pairs = (Map.Entry) it.next();
                        if (pairs.getKey().equals("source")) {
                            theDbObject = pairs.getValue();
                            break;
                        }
                    }
                } else {
                   theDbObject = dbObject;
                }
                
                if (null != listFieldName) {
                    //Get the nested list object first 
                    //Then loop through this list to get all the map values
                    Object listFieldObject = null;
                    try {
                        listFieldObject = DBLookup.getCompositePart(
                            theDbObject, listFieldName);                       
                    } catch (VocollectException e) {
                        //DBLookup throws the VocollectException
                        //But for export this is configuration issue as something 
                        //is specified in markup for which we are unable to do the lookup
                        log.error("Unable to do the lookup for field: " + listFieldName 
                            + " in the object: " + theDbObject.getClass().getName(),
                            ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD);
                        throw new ExportConfigurationException(
                                         ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD,
                                         new UserMessage(LocalizedMessage.formatMessage(
                                             ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD),
                                             listFieldName, theDbObject.getClass().getName()));
                    }
                    
                    Collection<Object> tempObject = null;
                    
                    //TODO see if you can make it work for set as well
                    if (listFieldObject instanceof List) {
                        tempObject = (List<Object>) listFieldObject;
                    } else if (listFieldObject instanceof Set) {
                        tempObject = (Set<Object>) listFieldObject;
                    }
                    
                    //Not sure what to do if that happens
                    int listSize = tempObject.size();  
                    Iterator tempObjIt = tempObject.iterator();
                    do {
                        Object listObject = null;
                        if (listSize > 0) {
                            listObject = tempObjIt.next();    
                        }                        
                        Iterator it = nestedObjectMap.entrySet().iterator();
                        exportHashMap = new LinkedHashMap<String, String>();
                        while (it.hasNext()) {
                            Map.Entry pairs = (Map.Entry) it.next();
                            ArrayList<? extends String> nestedFieldList = new ArrayList<String>();
                            String fieldDetail = (String) pairs.getKey();
                            Object workingObject = null;
                            Object fieldObject = null;
                            Object whichObject = null;
                            boolean listObjectFound = false;
                            nestedFieldList = (ArrayList<? extends String>) pairs.getValue();
                            if (nestedFieldList.get(0).equals(listFieldName)) {
                                whichObject = listObject;
                                listObjectFound = true;
                            } else {
                                whichObject = theDbObject; 
                            }   
                            for (String fieldString : nestedFieldList) {
                                try {                    
                                    //list Object has been already queried so skip it
                                    if (listObjectFound) {
                                        listObjectFound = false;
                                        continue;
                                    }
                                    //When working object is null it is the first level
                                    //for the list object                                    
                                       if (workingObject == null) {
                                           if (whichObject == null) {
                                               continue;
                                           }
                                           fieldObject = DBLookup.getCompositePart(
                                               whichObject, fieldString);
                                       } else {
                                            fieldObject = DBLookup.getCompositePart(
                                               workingObject, fieldString);
                                       }
                                       workingObject = fieldObject;
                                       if (workingObject instanceof Collection) {
                                           //Throw Export ConfigException
                                           //We are preventing the second and third level collection
                                           log.error("Nested level collection found. The Object is: " + workingObject 
                                               + ". Lookup field is: " + fieldString,
                                               ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED);
                                           throw new ExportConfigurationException(
                                                            ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED,
                                                            new UserMessage(LocalizedMessage.formatMessage(
                                                                ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED),
                                                                listFieldName, theDbObject.getClass().getName()));
                                  }
                                       if (fieldObject == null) {
                                           break;
                                       }
                                   } catch (VocollectException e) {
                                       //DBLookup throws the VocollectException
                                       //But for export this is configuration issue as something 
                                       //is specified in markup for which we are unable to do the lookup
                                       log.error("Unable to do the lookup for field: " + listFieldName 
                                           + " in the object: " + theDbObject.getClass().getName(),
                                           ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD);
                                       throw new ExportConfigurationException(
                                                        ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD,
                                                        new UserMessage(LocalizedMessage.formatMessage(
                                                            ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD),
                                                            listFieldName, theDbObject.getClass().getName()));
  
                                   }     
                            } //End of the arraylist of nested fields
                                
                            if (fieldObject == null) {                                
                               exportHashMap.put(fieldDetail, " ");
                            } else {
                               exportHashMap.put(fieldDetail, fieldObject
                                           .toString());
                               fieldObject = null;
                            }                            
                                
                        } //end of while looping through the nested map
                        exportRecordList.add(exportHashMap);                        
                    } while(tempObjIt.hasNext());
                } else {                   
                    Iterator it = nestedObjectMap.entrySet().iterator();
                    exportHashMap = new LinkedHashMap<String, String>();
                    while (it.hasNext()) {
                        Map.Entry pairs = (Map.Entry) it.next();
                        ArrayList<? extends String> nestedFieldList = new ArrayList<String>();
                        String fieldDetail = (String) pairs.getKey();
                        Object workingObject = null;
                        Object fieldObject = null;
                        nestedFieldList = (ArrayList<? extends String>) pairs.getValue();
                        for (String fieldString : nestedFieldList) {
                            try {                                    
                                //When working object is null it is the first level
                                //for the list object
                                   if (workingObject == null) {
                                       fieldObject = DBLookup.getCompositePart(
                                           theDbObject, fieldString);
                                   } else {
                                        fieldObject = DBLookup.getCompositePart(
                                           workingObject, fieldString);
                                   }
                                   workingObject = fieldObject;
                                   if (workingObject instanceof Collection) {
                                       //We are preventing the second and third level collection
                                       log.error("Nested level collection found. The Object is: " + workingObject 
                                           + ". Lookup field is: " + fieldString,
                                           ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED);
                                       throw new ExportConfigurationException(
                                                        ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED,
                                                        new UserMessage(LocalizedMessage.formatMessage(
                                                            ExporterError.NESTED_LEVEL_COLLECTION_NOT_SUPPORTED),
                                                            listFieldName, theDbObject.getClass().getName()));

                                   }
                                   if (fieldObject == null) {
                                       break;
                                   }
                               } catch (VocollectException e) {
                                   //DBLookup throws the VocollectException
                                   //But for export this is configuration issue as something 
                                   //is specified in markup for which we are unable to do the lookup
                                   log.error("Unable to do the lookup for field: " + listFieldName 
                                       + " in the object: " + theDbObject.getClass().getName(),
                                       ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD);
                                   throw new ExportConfigurationException(
                                                    ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD,
                                                    new UserMessage(LocalizedMessage.formatMessage(
                                                        ExporterError.LOOKUP_FAILURE_FOR_MARKUP_FIELD),
                                                        listFieldName, theDbObject.getClass().getName()));

                               }     
                        } //End of the arraylist of nested fields
                            
                        if (fieldObject == null) {
                           exportHashMap.put(fieldDetail, " ");
                        } else {
                           exportHashMap.put(fieldDetail, fieldObject
                                       .toString());
                           fieldObject = null;
                        }                               
                    } //end of while looping through the nested map
                    exportRecordList.add(exportHashMap);
                } // If any of the nested Object is list object
        } // End of looping for each object in the list of objects
        
    } 
    
    /**
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#getRecord()
     * @throws VocollectException When out of data.
     * @throws ExportConfigurationException when config issues
     * @return FixedLengthFieldMap
     */
    @SuppressWarnings("unchecked")
    @Override
    public ObjectContext getRecord() throws VocollectException, ExportConfigurationException {

        ObjectContext<FixedLengthFieldMap, Object> objectContext = null;
        
        if (!queryOpen) {
            preProcessingStep();
            openQuery();
            if (currentRecord == null || !currentRecord.hasNext()) {
                throw new NoWorkException();
            }
        }

        // check for end-of-data, throw out-of-data exception if so.
        
        if (queryResultObject instanceof ExportTransportObject) {
            // Standard export
            if (currentRecord == null || !currentRecord.hasNext()) {
                checkForMoreRecords(true, true, true);
            }
            objectContext = this.processMaps();
            
        } else if (queryResultObject instanceof DataObject) {
            // Another way to export fields from a model object
            if (currentRecord == null || !currentRecord.hasNext()) {
                checkForMoreRecords(false, false, false);
            }
            objectContext = this.processModelObject();
            
        } else {
            // Database import
            if (currentRecord == null || !currentRecord.hasNext()) {
                checkForMoreRecords(true, true, false);
            }
            objectContext = this.processImportObject();
            objectContext.setSourceLineNumber(this.getContextBeginsLineNumber());
        }
        
        return objectContext;
    }

    /**
     * Check to see if we should requery or not.
     * 
     * @param checkForMore - check for more records
     * @param updateLineNumber - update context begins line number
     * @param clearSession - clear session before more records
     * @throws ExportConfigurationException - export exceptions
     * @throws OutOfDataException - out of data exception
     */
    private void checkForMoreRecords(boolean checkForMore, 
                                     boolean updateLineNumber,
                                     boolean clearSession) 
    throws ExportConfigurationException, OutOfDataException {
        
        //Determie if we should requery
        if (checkForMore) {
            if (updateLineNumber) {
                setContextBeginsLineNumber(getContextBeginsLineNumber() + 1);
            }
            
            if (clearSession) {
                pm.clearSession();
            }
            
            //re-query database
            queryOpen = false;
            openQuery();
        }
        
        //if no more records then throw error
        if (currentRecord == null || !currentRecord.hasNext()) {
            outOfData = true;
            throw new OutOfDataException();
        }
    }
    
    /**
     * Process the result of a query, that was returned as a map.
     * @return the completed object context.
     * @throws VocollectException when a field is not contained in the map.
     */
    public ObjectContext processMaps() throws VocollectException {
        int requestedFieldLength = 0;
        ObjectContext<FixedLengthFieldMap, Object> objectContext = null;
        Object myObj = null;
        Map exportMap = null;
        exportMap = (Map) currentRecord.next();

        boolean matchingFieldFound = false;
        String oneSpaceStr = " ";
        for (Field y : this.getFieldList().values()) {
            matchingFieldFound = false;
            FixedLengthField x = (FixedLengthField) y;
            // get the values from the fieldMap
            Iterator it = exportMap.entrySet().iterator();           
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();
                if (pairs.getKey().equals("source")) {
                    myObj = pairs.getValue();
                }
                if (pairs.getKey().equals(x.getFieldName())) {
                    if (pairs.getValue() == null) {
                        fieldData = oneSpaceStr;   
                    } else {
                        fieldData = pairs.getValue().toString();
                    }                  
                    matchingFieldFound = true;
                    break;
                }
            }
            //Populated the data as per the markup only
            if (matchingFieldFound) {
                requestedFieldLength = x.getLength();
                // if this field does not request a length, then use the whole
                // field. This field might be needed for calculation only
                //later will suppress this field
                if (requestedFieldLength == 0) {                               
                    x.setFieldData(fieldData);
                    //If the requested length is more than add the additional spaces    
                } else if (requestedFieldLength >= fieldData.length()) {
                    x.setFieldData(pad(requestedFieldLength, fieldData));
                } else {
                    // Data longer than requested length, so trim it...
                    x.setFieldData(fieldData.substring(
                            0, requestedFieldLength));
                }

            } else if (!(x.isSuppress())) {
                log.fatal(
                        "Export Markup Field " + x.getFieldName() + " Not queried by database.",
                        DataSourceParserError.EXPORT_MARKUP_FIELD_NOT_QUERIED_BY_DATABASE);
                throw new VocollectException(DataSourceParserError.EXPORT_MARKUP_FIELD_NOT_QUERIED_BY_DATABASE,
                        new UserMessage(
                                LocalizedMessage
                                .formatMessage(DataSourceParserError.EXPORT_MARKUP_FIELD_NOT_QUERIED_BY_DATABASE),
                                x.getFieldName()));
            }
        }

        boolean isEto =  false;
        boolean isDataObject = false;
        if (queryResultObject instanceof ExportTransportObject) {
            isEto = true;
        } else if (queryResultObject instanceof DataObject) {
            isDataObject = true;
        } else {
            log.fatal("Trying to export object " + queryResultObject.toString() 
                    + "which isn't ExportTransportObject type " 
                    + " or DataObject type.",
                    DataSourceParserError.INVALID_EXPORT_OBJECT);
            throw new VocollectException(DataSourceParserError.INVALID_EXPORT_OBJECT,
                    new UserMessage(LocalizedMessage
                            .formatMessage(DataSourceParserError.INVALID_EXPORT_OBJECT)));
        }
        //Set the in progess status based on the Markup
        //This might be set to true for intermidiate exports in the large export
        if (getExportInProgress()) {
            if (exportMap.get("source") != null) {
                //Set the status to InProgress and save
                if (isEto) {
                    ((ExportTransportObject) queryResultObject).updateExportToInProgress(exportMap.get("source"));
                } else if (isDataObject) {
                    //TODO not sure what to do 
                }
                this.pm.save(exportMap.get("source"));
            }
        }

        //Set the exported status based on the Markup 
        if (getUpdateToExported()) {
            //If this thing isn't null
            if (exportMap.get("source") != null) {
                //Set the status to Exported and save
                if (isEto) {
                    ((ExportTransportObject) queryResultObject).updateExportToExported(exportMap.get("source"));
                } else if (isDataObject) {
                    //TODO not sure what to do 
                } 
                this.pm.save(exportMap.get("source"));
            }
        }

        objectContext = new ObjectContext<FixedLengthFieldMap, Object>(this.getFieldList(), myObj);            
        return objectContext; 


    }
    
    /**
     * Process a model object into a list of fields.
     * @return the completer ObjectContext
     * 
     * 
     */
    public ObjectContext processModelObject() {
        Object subject = currentRecord.next();
        
        ObjectContext result = getObjectContext();
        
        result.setSourceObject(subject);
        
        // extract to field maps?
//        for (Field y : this.getFieldList().values()) {
//        }

        return result;
    }
    
    /**
     * Process an import object.
     * @return the completed ObjectContext
     * @throws BusinessRuleException from the model
     * @throws DataAccessException from the database, when updating to inprogress or completed.
     * @throws VocollectException from underlying objects.
     */
    @SuppressWarnings("unchecked")
    public ObjectContext processImportObject() 
            throws BusinessRuleException, DataAccessException, VocollectException {
        
        Importable subject = null;
        try {
            subject = (Importable) currentRecord.next();
        } catch (NoSuchElementException e) {
            outOfData = true;
            throw new OutOfDataException();
        }
        
        
        ObjectContext result = getObjectContext();
        
        result.setSourceObject(subject);

        result.setModelObject(subject.convertToModel());
        
        if (getExportInProgress()) {
            subject.setInProgress(true);
                this.pm.save(subject);

        }

        //Set the exported status based on the Markup 
        if (getUpdateToExported()) {
            //If this thing isn't null
            subject.setCompleted(true);
            this.pm.save(subject);

        }

        return result;
    }

    /**
     * This method gets the particular field object of fieldName.
     * 
     * @param fieldName of the Field object you wish to return
     * @return the Field object with the specific fieldName
     */
    public Field getField(String fieldName) {
        if (this.queryOpen) {
            return getFieldList().get(fieldName);
        } else {
            log.fatal(
                "getField can not be called before getRecord in "
                    + this.getClass().getName(),
                DataSourceParserError.INTERFACE_SEQUENCE_ERROR_5);
            throw new RuntimeException("getField can not be called before"
                + " getRecord in " + this.getClass().getName() + ".");
        }
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#configure()
     * @throws ConfigurationException CastorConfiguration throws the config
     *             exception
     */
    @Override
    public boolean configure() throws ConfigurationException {

        if ((null == getMappingFileName())
            || (0 == getMappingFileName().compareTo(""))
            || (null == getConfigFileName())
            || (0 == getConfigFileName().compareTo(""))) {
            return false;
        }

        CastorConfiguration cc = new CastorConfiguration(
            getMappingFileName(), getConfigFileName());

        // This is creating a list of fields that we want from the source
        // object.
        this.setFieldList((FixedLengthFieldMap) cc.configure());

        //createIndex();
        return true;
    }

    /**
     * This is called externally to get the fields sequentially. Return null
     * when reading off the end of the list. {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#getNextField()
     */
    @Override
    public FixedLengthField getNextField() throws OutOfDataException {
        if (this.queryOpen) {
            if (currentField.hasNext()) {
                return (FixedLengthField) currentField.next();
            } else {
                // There was no next field, so throw exception
                throw new OutOfDataException();
            }
        } else {
            log.fatal(
                "getNextField can not be called before getRecord in "
                    + this.getClass().getName(),
                DataSourceParserError.INTERFACE_SEQUENCE_ERROR_6);
            throw new RuntimeException("getNextField can not be called before"
                + " getRecord in " + this.getClass().getName() + ".");
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#isOutOfData()
     */
    @Override
    public boolean isOutOfData() {
//        if (this.queryOpen) {
            return this.outOfData;
//        } else {
//            log.fatal(
//                "isOutOfData can not be called before getRecord in "
//                    + this.getClass().getName(),
//                DataSourceParserError.INTERFACE_SEQUENCE_ERROR_7);
//            throw new RuntimeException("isOutOfData can not be called before"
//                + " getRecord in " + this.getClass().getName() + ".");
//        }
    }
    
    /**
     * This is used to add the additional spaces to the String
     * to make it requested length or fixed length String.
     * @param len requested length for the String
     * @param s input String which needs to be modified to add additional spaces
     * @return the modified string
     */    
    public String pad(int len, String s) {        
        int l = s.length();
        StringBuffer sb = new StringBuffer(s);          
        String spaceStr = " ";
        while (l < len) {
            sb.append(spaceStr);
            l = sb.length();
        }            
        return sb.toString();
    }


    /**
     * @return the preProcessQuery
     */
    public String getPreProcessQuery() {
        return preProcessQuery;
    }

    /**
     * @param preProcessQuery the preProcessQuery to set
     */
    public void setPreProcessQuery(String preProcessQuery) {
        this.preProcessQuery = preProcessQuery;
    }
    

    /**
     * Allow us to run some pre-processing stage.
     */
    public void preProcessingStep() {
        if ((null != this.getPreProcessQuery()) && (this.getPreProcessQuery().length() > 0)) {
            dbLook.lookup(getQueryResultObject(), getPreProcessQuery(), new FieldMap());
        }
    }

    /**
     * Allow us to run some post-processing stage.
     */
    public void postProcessingStep() {
        if ((null != this.getPostProcessQuery()) && (this.getPostProcessQuery().length() > 0)) {
            if (null != getQueryResultObject()) {
                dbLook.lookup(getQueryResultObject(), getPostProcessQuery(), new FieldMap());
            }
            if (null != getImportObject()) {
                dbLook.lookup(getImportObject(), getPostProcessQuery(), new FieldMap());
            }
        }
    }

    /**
     * @return the postProcessQuery
     */
    public String getPostProcessQuery() {
        return postProcessQuery;
    }

    /**
     * @param postProcessQuery the postProcessQuery to set
     */
    public void setPostProcessQuery(String postProcessQuery) {
        this.postProcessQuery = postProcessQuery;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        postProcessingStep();
    }

    /**
     * {@inheritDoc}
     * @return the completed object context
     * @throws VocollectException see parseBatchToXML
     * @throws ImportConfigurationException for configuration - XML - errors
     * @throws BadDataTransformException if the data cannot be transformed to the object datatype
     */
    @Override
    public ObjectContext getElement() 
        throws VocollectException, ImportConfigurationException, BadDataTransformException 
        {
        
        getObjectContext().clear();
        setObjectContext(getRecord());
        

        return getObjectContext();
        
    }

    /**
     * Gets the value of importObject.
     * @return the importObject
     */
    public Importable getImportObject() {
        return importObject;
    }

    /**
     * Sets the value of the importObject.
     * @param importObject the importObject to set
     */
    public void setImportObject(Importable importObject) {
        this.importObject = importObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeOnFailure() {
        close();
        return;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closeOnSuccess() {
        close();
    }

    /**
     * Sets the value of the outOfData.
     * @param outOfData the outOfData to set
     */
    private void setOutOfData(boolean outOfData) {
        this.outOfData = outOfData;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public long getRecordsRead() {
        // Not relevant as yet.
        return 0;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.DataSourceParser#clearInputRecords()
     */
    @Override
    public void clearInputRecords() {
        currentRecord = null;
    }
    
    /**
     * Override setSiteName to set the site name in the persistence manager.
     * {@inheritDoc}
     * @param siteName
     */
    @Override
    public void setSiteName(String siteName) {
        super.setSiteName(siteName);
        if (pm != null) {
            pm.setSiteName(siteName);
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 