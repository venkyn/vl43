/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * @author astein
 *
 */
public final class ImportManagerError extends ErrorCode {

    private static final int LOWERBOUND = 5300;
    private static final int UPPERBOUND = 5399;

    /** No error, just the base initialization. */
    public static final ImportManagerError NO_ERROR = 
        new ImportManagerError();
    public static final ImportManagerError BADCONFIGURATION = 
        new ImportManagerError(5301);
    public static final ImportManagerError FAILEDIMPORTMANAGER = 
        new ImportManagerError(5302);
    public static final ImportManagerError MISSINGCONFIGFILE = 
        new ImportManagerError(5303);
    public static final ImportManagerError MISSINGMAPPINGFILE = 
        new ImportManagerError(5304);
    public static final ImportManagerError IMPERFECTIMPORTMANAGER = 
        new ImportManagerError(5305);
    public static final ImportManagerError PARTIALSUCCESSIMPORTMANAGER = 
        new ImportManagerError(5306);
    public static final ImportManagerError UNINTERRUPTIBLEIMPORTMANAGER = 
        new ImportManagerError(5307);
    public static final ImportManagerError INTERRUPTIBLEIMPORTMANAGER = 
        new ImportManagerError(5308);
    
    /**
     * Constructor.
     */
    private ImportManagerError() {
        super("VoiceLink", LOWERBOUND, UPPERBOUND);
    }

    /**
     * @param err - error to be logged
     */
    private ImportManagerError(long err) {
        super(ImportManagerError.NO_ERROR, err);
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 