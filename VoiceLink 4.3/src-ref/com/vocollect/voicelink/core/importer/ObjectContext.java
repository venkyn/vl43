/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.LinkedList;
import java.util.List;

/**
 * This class is the wrapper for FieldMap and the DataObject.
 * ObjectContext is populated by the parsers. DatabaseDataSourceParser will populate the
 * DataObject as well. FixedLengthDataSourceParser will populate the null for the DataObject.
 * This modelObject passed to the importer/exporter for import/export.
 * @author Kalpna
 * @param <FM> FieldMap
 * @param <OBJ> the Object
 */
public class ObjectContext<FM extends FieldMap, OBJ extends Object> {

    // The model object which will be saved to the database.
    private OBJ modelObject = null;
    
    // Primarily for database imports, where we are converting from one object to another.
    private Object sourceObject = null;

    
    private List<FM> fieldMaps = null;

    private String xmlRepresentation = "";

    private boolean badDataFlag = false;
    
    private long sourceLineNumber = 0;
    
    //Boolean shows if current record should be skipped because of a duplication
    // skipping rule such as UseLastDup OR UseFirstDup on the index.
    // the default behavior is false (records are not skipped).
    private boolean skipCurrentRecordBecauseOfDup = false;

    /**
     * Default constructor.
     */
    public ObjectContext() {
        super();
        fieldMaps = new LinkedList<FM>();
    }

    /**
     * Construct the ObjectContext modelObject.
     * Constructor.
     * @param fieldMap the FieldMap
     * @param modelObject the Object
     */
    public ObjectContext(FM fieldMap, OBJ modelObject) {
        super();
        setBadDataFlag(false);
        if (null == this.fieldMaps) {
            fieldMaps = new LinkedList<FM>();
        }
        this.setFieldMaps(fieldMap);
        this.setModelObject(modelObject);
    }

    /**
     * Setter for modelObject property.
     * @param modelObject the Object to set
     */
    public void setModelObject(OBJ modelObject) {
        this.modelObject = modelObject;
    }

    /**
     * Getter for modelObject property.
     * @return the OBJ
     */
    public OBJ getModelObject() {
        return this.modelObject;
    }


    /**
     * Getter for xmlRepresentation property.
     * @return the XmlTagger
     */
    public String getXmlRepresentation() {
        return xmlRepresentation;
    }

    /**
     * Setter for xmlRepresentation property.
     * @param xmlRepresentation to set
     */
    public void setXmlRepresentation(String xmlRepresentation) {
        this.xmlRepresentation = xmlRepresentation;
    }

    /**
     * 
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        String str = this.getFieldMaps().toString();
        //TODO complete the toString
        /* if (this.getDataObject() != null) {
         DataObject d = this.getDataObject();
         str = str + d.getDescriptiveText();                  
         }*/
        return str;
    }

    /**
     * Get a string that can be logged that lists the fields with bad data, along with the bad data.
     * A field contains bad data when the data in the string can't be converted to the model object's datatype.
     * @param fmList The field maps to investigate.
     * @return A string that contains the field name & the data for each field that contains bad data.
     */
    @SuppressWarnings("unchecked")
    public String getBadData(List<FM> fmList) {
        String badDataList = "";
        int index = 0;
        for (FieldMap fm : fmList) {
            if (fm.isBadData()) {
                for (Field f : fm.values()) {
                    if (f.isBadData()) {
                        badDataList += f.getFieldName() + " on line " + (index + sourceLineNumber)
                        + " contains invalid data " 
                        + "(" + f.getBadFieldData() + ")"
                        + " for type "
                        + f.getDataType()
                        + ". ";
                    }
                }
            }
            index++;
        }
        return badDataList;
    }

    /**
     * @return String containing the errors from the raw data analysis
     */
    public String getBadData() {
        return getBadData(this.getFieldMaps());
    }
    /**
     * Getter for badDataFlag.
     * @return boolean
     */
    public boolean getBadDataFlag() {
        return badDataFlag;
    }

    /**
     * Setter for badDataFlag.
     * @param badDataFlag the badDataFlag to set
     */
    public void setBadDataFlag(boolean badDataFlag) {
        this.badDataFlag = badDataFlag;
    }

    /**
     * Gets the value of fieldMaps.
     * @return the fieldMaps
     */
    public List<FM> getFieldMaps() {
        return fieldMaps;
    }

    /**
     * Sets the value of the fieldMaps.
     * @param fmList the fieldMaps to set
     */
    public void setFieldMaps(List<FM> fmList) {
        this.fieldMaps = fmList;
        setBadDataFlag(false);
        for (FieldMap fm : fmList) {
            if (fm.isBadData()) {
                this.setBadDataFlag(true);
            }
        }
    }
    /**
     * Sets the value of the fieldMaps.
     * @param fieldMap the fieldMaps to set
     */
    public void setFieldMaps(FM fieldMap) {
        fieldMaps.add(fieldMap);

        if (fieldMap.isBadData()) {
            this.setBadDataFlag(true);
        }
    }

    /**
     * Gets the value of sourceLineNumber.
     * @return the sourceLineNumber
     */
    public long getSourceLineNumber() {
        return sourceLineNumber;
    }

    /**
     * Sets the value of the sourceLineNumber.
     * @param sourceLineNumber the sourceLineNumber to set
     */
    public void setSourceLineNumber(long sourceLineNumber) {
        this.sourceLineNumber = sourceLineNumber;
    }
    
    /**
     * Clear for the next element.
     */
    public void clear() {
        modelObject = null;

        xmlRepresentation = "";

        badDataFlag = false;
        
    }

    /**
     * Gets the value of sourceObject.
     * @return the sourceObject
     */
    public Object getSourceObject() {
        return sourceObject;
    }

    /**
     * Sets the value of the sourceObject.
     * @param sourceObject the sourceObject to set
     */
    public void setSourceObject(Object sourceObject) {
        this.sourceObject = sourceObject;
    }

    
    /**
     * @return the skipCurrentRecordBecauseOfDup
     */
    public boolean isSkipCurrentRecordBecauseOfDup() {
        return skipCurrentRecordBecauseOfDup;
    }

    
    /**
     * @param skipCurrentRecordBecauseOfDup the skipCurrentRecordBecauseOfDup to set
     */
    public void setSkipCurrentRecordBecauseOfDup(boolean skipCurrentRecordBecauseOfDup) {
        this.skipCurrentRecordBecauseOfDup = skipCurrentRecordBecauseOfDup;
    }
    
    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 