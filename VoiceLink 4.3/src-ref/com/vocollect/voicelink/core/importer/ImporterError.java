/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * @author dgold
 */
public final class ImporterError extends ErrorCode {

    public static final int LOWER_BOUND = 5200;

    public static final int UPPER_BOUND = 5299;

    /** No error, just the base initialization. */
    public static final ImporterError NO_ERROR = new ImporterError();

    public static final ImporterError BAD_CONFIG = new ImporterError(5201);

    public static final ImporterError FAILED_IMPORT = new ImporterError(5202);

    public static final ImporterError MISSING_CONFIG_FILE =
        new ImporterError(5203);

    public static final ImporterError BAD_MAPPING_FILE =
        new ImporterError(5204);

    public static final ImporterError MISSING_MAPPING_FILE =
        new ImporterError(5205);

    public static final ImporterError BAD_FAILED_VALIDATION_FILE =
        new ImporterError(5206);

    public static final ImporterError BAD_MAPPING_FILE2 =
        new ImporterError(5207);

    public static final ImporterError BAD_OBJECT_MAPPING =
        new ImporterError(5208);

    public static final ImporterError VALIDATION_IN_MAPPING =
        new ImporterError(5209);

    public static final ImporterError CONFIGURATION_MISMATCH =
        new ImporterError(5210);

    public static final ImporterError CONFIGURATION_MISSING_ELEMENT =
        new ImporterError(5211);

    public static final ImporterError MAPPING_FILE_NOT_SET =
        new ImporterError(5212);

    public static final ImporterError CONFIG_FILE_NOT_SET =
        new ImporterError(5213);

    public static final ImporterError VALIDATION_TYPE_MISMATCH =
        new ImporterError(5214);

    public static final ImporterError VALIDATION_ERRORS_EXIST =
        new ImporterError(5215);

    public static final ImporterError FAILED_GENERAL_VALIDATION =
        new ImporterError(5216);

    public static final ImporterError FAILED_FIELD_VALIDATION =
        new ImporterError(5217);

    public static final ImporterError VALIDATION_ERRORS_EXIST2 =
        new ImporterError(5218);

    public static final ImporterError BAD_MAPPING_FILE3 =
        new ImporterError(5219);

    public static final ImporterError FAILED_OUTPUT_FILE_DID_NOT_CLOSE =
        new ImporterError(5220);

    public static final ImporterError NULL_VALIDATION_OBJECT =
        new ImporterError(5221);

    public static final ImporterError DATA_ACCESS_PROBLEM =
        new ImporterError(5222);

    public static final ImporterError BUSINESS_RULE_VIOLATION =
        new ImporterError(5223);

    public static final ImporterError NO_SUCH_MANAGER =
        new ImporterError(5224);

    public static final ImporterError VALIDATION_ERRORS_EXIST3 =
        new ImporterError(5225);

    public static final ImporterError MARSHALLING_ERROR_RECORD =
        new ImporterError(5226);

    public static final ImporterError NO_WORK_TO_DO =
        new ImporterError(5227);

    public static final ImporterError NO_NEW_OBJECT_NAME =
        new ImporterError(5228);

    public static final ImporterError CONFIG_FILE_NOT_ON_CLASSPATH =
        new ImporterError(5229);

    public static final ImporterError BAD_URI_FOR_CONFIG_FILE =
        new ImporterError(5230);

    public static final ErrorCode UNABLE_TO_COMPLETE_OBJECT =
        new ImporterError(5231);

    public static final ErrorCode ANNOUNCE_FAILED_VALIDATION =
        new ImporterError(5232);

    public static final ErrorCode NEW_OBJECT_CREATION_FAILURE =
        new ImporterError(5233);

    public static final ErrorCode BAD_DATA_TRANSFORMATION =
        new ImporterError(5234);

    // Pasrser indicates thet the current record is duplicated in the input stream.
    public static final ErrorCode DUPLICATED_DATA =
        new ImporterError(5235);

    // Could not lok up site by name
    public static final ErrorCode BAD_SITE_NAME =
        new ImporterError(5236);

    // Lookup Trigger reported that object should not be saved.
    public static final ErrorCode LOOKUP_TRIGGER_REPORTS_DO_NOT_SAVE =
        new ImporterError(5237);

    //Configuration error trigger fields are not present in the mapping file
    public static final ImporterError BAD_OBJECT_FIELD_MAPPING =
        new ImporterError(5238);

    // General Exception
    public static final ErrorCode GENERAL_EXCEPTION =
        new ImporterError(5239);

    //Unable to get the site id from site tag
    public static final ErrorCode UNABLE_TO_SET_SITE_ID =
        new ImporterError(5240);
    
    //Unable to get the site tag
    public static final ErrorCode UNABLE_TO_GET_SITE_TAG =
        new ImporterError(5241);

    // IO error opening the mapping file.
    public static final ImporterError MISSING_MAPPING_FILE2 =
        new ImporterError(5242);
    
    //Bad failed validation file name.
    public static final ImporterError BAD_FAILED_VALIDATION_FILE_NAME =
        new ImporterError(5243);

    // Hibernate session is closed.
    public static final ImporterError HIBERNATE_SESSION_ERROR =
        new ImporterError(5244);

    public static final ErrorCode BUSINESS_RULE_VIOLATION2 = 
        new ImporterError(5245);

    public static final ErrorCode FAIL_LIST_SEGMENT = 
        new ImporterError(5246);

    public static final ErrorCode MUST_IMPLEMENT =
        new ImporterError(5247);

    public static final ErrorCode BATCH_SAVE_FAILURE =
        new ImporterError(5248);
    
    
    /**
     * Constructor.
     */
    private ImporterError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private ImporterError(long err) {
        super(ImporterError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 