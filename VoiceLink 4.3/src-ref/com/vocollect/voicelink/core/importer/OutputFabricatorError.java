/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * ErrorCode instances for OutputFabricatorError in
 * com.vocollect.voicelink.core.importer.parsers Customizers should create a new
 * class corresponding to the name of the customized fabricator.
 * 
 * @author KalpnaT
 * 
 */
public final class OutputFabricatorError extends ErrorCode {

    public static final int LOWER_BOUND = 6200;

    public static final int UPPER_BOUND = 6299;

    /** No error, just the base initialization. */
    public static final OutputFabricatorError NO_ERROR = 
        new OutputFabricatorError();

    // IOException when trying to close the connection with OutputAdapter
    public static final OutputFabricatorError CLOSE_CONNECTION_ERROR = 
        new OutputFabricatorError(6201);
    
    public static final OutputFabricatorError WRONG_FIXED_LENGTH_RECORD_LENGTH = 
        new OutputFabricatorError(6202);
    
    public static final OutputFabricatorError RECORD_WRITE_ERROR = 
        new OutputFabricatorError(6203);
    
    /**
     * Constructor.
     */
    private OutputFabricatorError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private OutputFabricatorError(long err) {
        super(OutputFabricatorError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 