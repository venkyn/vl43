/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.opensymphony.xwork2.validator.ShortCircuitableValidator;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is an implementation class of the VocollectCustomValidator that 
 * implements customized validating behavior including:
 *     isValidNumber() which checks to make sure a string of characters is a valid number.
 *
 * @author astein
 */
public abstract class VocollectCustomValidator extends FieldValidatorSupport
        implements ShortCircuitableValidator {

    /*boolean to indicate if the Validation needs to be inverted.
    if invertResult is true then will add the validation error
    when validation is successful otherwise will add on the validation failure*/
    private boolean invertResult = false; 
    
    // Flag that governs when this validator is fired - pre-database-lookup for this one
    private boolean checkPreLookup = true;
    
    // Flag that governs when this validator is fired - post-database-lookup for this one
    private boolean checkPostLookup = true;
    
    /**
     * Return true if this validator should do its work at the time f this call.
     * @return true if this validator should run at present time.
     */
    public boolean shouldRun() {
        if (isPreLookup()) {
            return checkPreLookup;
        } else {
            return checkPostLookup;   
        }
    }


    /**
     * Setter for the invertResult property.
     * @param invertResult the new invertResult value
     */
    public void setInvertResult(boolean invertResult) {
        this.invertResult = invertResult;
 }
        
    /**
     * Getter for the invertResult property.
     * @return boolean value of the property
     */
    public boolean getInvertResult() {
        return this.invertResult;
    }
    
    /**
     * This the method to add the validation errors based on the invertResult. 
     * @param fieldName field on which the validation applies
     * @param actionObj Object on which action is performed
     * @param result indicate the validation result
     */
    public void addValidationError(String fieldName, Object actionObj, boolean result) {
        if (this.getInvertResult()) {
            result = !result;
        }
        if  (!result) {
            addFieldError(fieldName, actionObj);
        }
    }
    
    //  Below here are helpers...

    /**
     * Determines whether or not the passed in string is numeric.
     * @param stringtoCheck String to check if it is numeric
     * @return true if string is a number
     * @author jtauberg
     */
    public boolean isStringNumeric(String stringtoCheck) {

        // compile the regex so that only numeric digits are allowed
        // and only one decimal anywhere in the pattern
        // also a negative sign is allowed at the beginning.
        String patternStr = "-?[0-9]*\\.?[0-9]+E?-?[0-9]*";
        Pattern pattern = Pattern.compile(patternStr);

        // see if there is a match
        Matcher matcher = pattern.matcher(stringtoCheck);
        return matcher.matches();
    }
    
    /**
     * Method to see if this validation is being done before or after database lookup.
     * @return true if this validator is being called before database lookup, false OW.
     */
    public boolean isPreLookup() {
        // Get the data to be added to the end of this (which should be a string)
        ValidationResult.VocollectValidatorContext vvC = 
            (ValidationResult.VocollectValidatorContext) this.getValidatorContext();

        return vvC.isPreLookup();
    }

    /**
     * Gets the value of checkPostLookup.
     * @return the checkPostLookup
     */
    public boolean isCheckPostLookup() {
        return this.checkPostLookup;
    }

    /**
     * Sets the value of the checkPostLookup.
     * @param checkPostLookup the checkPostLookup to set
     */
    public void setCheckPostLookup(boolean checkPostLookup) {
        this.checkPostLookup = checkPostLookup;
    }

    /**
     * Gets the value of checkPreLookup.
     * @return the checkPreLookup
     */
    public boolean isCheckPreLookup() {
        return this.checkPreLookup;
    }

    /**
     * Sets the value of the checkPreLookup.
     * @param checkPreLookup the checkPreLookup to set
     */
    public void setCheckPreLookup(boolean checkPreLookup) {
        this.checkPreLookup = checkPreLookup;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 