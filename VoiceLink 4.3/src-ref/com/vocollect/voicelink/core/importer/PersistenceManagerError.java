/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * @author dgold
 */
public final class PersistenceManagerError extends ErrorCode {

    public static final int LOWER_BOUND = 5400;

    public static final int UPPER_BOUND = 5499;

    /** No error, just the base initialization. */
    public static final PersistenceManagerError NO_ERROR = 
        new PersistenceManagerError();

    public static final PersistenceManagerError LOAD_BY_BEAN_NAME = 
        new PersistenceManagerError(5400);

    public static final PersistenceManagerError NO_SUCH_BEAN = 
        new PersistenceManagerError(5401);

    public static final PersistenceManagerError INAPPROPRIATE_BEAN_TYPE = 
        new PersistenceManagerError(5402);

    public static final PersistenceManagerError QUERY_OBJECT_HAS_NO_MANAGER = 
        new PersistenceManagerError(5403);

    public static final PersistenceManagerError NO_MANAGER_BY_NAME = 
        new PersistenceManagerError(5404);

    public static final ErrorCode ILLEGAL_MEMBER_ACCESS =
        new PersistenceManagerError(5405);

    public static final ErrorCode BAD_FINDER_INVOCATION =
        new PersistenceManagerError(5406);

    public static final ErrorCode NO_SUCH_FINDER =
        new PersistenceManagerError(5407);
        

    /**
     * Constructor.
     */
    private PersistenceManagerError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private PersistenceManagerError(long err) {
        super(PersistenceManagerError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 