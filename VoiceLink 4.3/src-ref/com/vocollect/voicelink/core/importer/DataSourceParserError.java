/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.ErrorCode;

/**
 * ErrorCode instances for DataSourceParsers in
 * com.vocollect.voicelink.core.importer.parsers Customizers should create a new
 * class corresponding to the name of the customized parser.
 * 
 * @author dgold
 * 
 */
public final class DataSourceParserError extends ErrorCode {

    public static final int LOWER_BOUND = 5100;

    public static final int UPPER_BOUND = 5199;

    /** No error, just the base initialization. */
    public static final DataSourceParserError NO_ERROR = 
        new DataSourceParserError();

    // Configuration hosed
    public static final DataSourceParserError BAD_CONFIG = 
        new DataSourceParserError(5100);

    // Got a bad read form the data
    public static final DataSourceParserError BAD_READ = 
        new DataSourceParserError(5101);

    // The length of the record read != defined length for a fixed length
    // record.
    public static final DataSourceParserError WRONG_FIXED_LENGTH_RECORD_LENGTH = 
        new DataSourceParserError(5102);

    // Failed to read (Adapter threw an error) in the pre-scan of the input
    // file.
    public static final DataSourceParserError PRESCAN_FAILED = 
        new DataSourceParserError(5103);

    // Just for reporting the result of the prescan for the data source.
    public static final DataSourceParserError PRESCAN_RESULT = 
        new DataSourceParserError(5104);

    // Just for reporting the result of the prescan for the data source.
    public static final DataSourceParserError WRONG_RECORD_COUNT = 
        new DataSourceParserError(5105);

    
    

    // User tried to configure queryResultObject that was not based on DataObject.
    public static final DataSourceParserError BAD_QUERY_RESULT_OBJECT = 
        new DataSourceParserError(5106);

    // User tried to do a getSourceAdapter on DatabaseDataSourceParser.
    public static final DataSourceParserError GET_SOURCE_ADAPTER_FAILED = 
        new DataSourceParserError(5107);

    // LookupAll failed in DatabaseDataSourceParser.
    public static final DataSourceParserError LOOKUP_ALL_FAILED = 
        new DataSourceParserError(5108);

    // User query failed.
    public static final DataSourceParserError QUERY_FAILED = 
        new DataSourceParserError(5109);

    // User query failed.
    public static final DataSourceParserError QUERY_FAILED_2 = 
        new DataSourceParserError(5110);

    
    // User tried to use or configure the interface out of order.
    // setKeyFields may not be changed while query is open.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_1 = 
        new DataSourceParserError(5111);

    // User tried to use or configure the interface out of order.
    // setQueryAll may not be changed while query is open.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_2 = 
        new DataSourceParserError(5112);

    // User tried to use or configure the interface out of order.
    // setQueryName may not be changed while query is open.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_3 = 
        new DataSourceParserError(5113);

   // User tried to use or configure the interface out of order.
    // setQueryResultObjectName may not be changed while query is open.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_4 = 
        new DataSourceParserError(5114);

    // User tried to use or configure the interface out of order.
    // getField can not be called before getRecord.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_5 = 
        new DataSourceParserError(5115);

    // User tried to use or configure the interface out of order.
    // getNextField can not be called before getRecord.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_6 = 
        new DataSourceParserError(5116);

    // User tried to use or configure the interface out of order.
    // isOutOfData can not be called before getRecord.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_7 = 
        new DataSourceParserError(5117);
    
    // User tried to use or configure the interface out of order.
    // getIndexes was indirectly called with Indexes=null.
    // Indexes must be created with setIndexes() or addIndex() before
    // Indexes may be used.
    public static final DataSourceParserError INTERFACE_SEQUENCE_ERROR_8 = 
        new DataSourceParserError(5118);

    //An import index was created that was defined to fail the record if
    // data was duplicated in the file.... well, it happened.
    public static final ErrorCode DUPLICATED_DATA = 
        new DataSourceParserError(5119);

    //An import index was created that was defined to fail the file if
    // data was duplicated in the file.... well, it happened.
    public static final ErrorCode DUPLICATED_DATA_FAILS_FILE =
        new DataSourceParserError(5120);

    // User tries to get the queryResultObject before setting it
    public static final DataSourceParserError NULL_QUERY_RESULT_OBJECT = 
        new DataSourceParserError(5121);
 
    // User tried to do a getSourceAdapter on DatabaseDataSourceParser.
    public static final DataSourceParserError GET_QUERY_RESULT_OBJECT_FAILED = 
        new DataSourceParserError(5122);
    
    // Try to access method from a class which doesn't exists    
    public static final DataSourceParserError ILLEGAL_MEMBER_ACCESS =
        new DataSourceParserError(5123);
    
    // Field required by the Export Markup is not queried by database    
    public static final DataSourceParserError EXPORT_MARKUP_FIELD_NOT_QUERIED_BY_DATABASE =
        new DataSourceParserError(5124);

    // Trying to export the invalid object    
    public static final DataSourceParserError INVALID_EXPORT_OBJECT =
        new DataSourceParserError(5125);

    //the Indexes markup was configured with more than 1 grouping index.
    public static final ErrorCode INDEXES_CAN_ONLY_CONTAIN_ONE_GROUPING_INDEX =
        new DataSourceParserError(5126);
    
    //getCharCount can only be used on an Indexes collection that contains a 
    // grouping index.
    public static final ErrorCode INTERFACE_SEQUENCE_ERROR_9 =
        new DataSourceParserError(5127);

    //setCharCount can only be used on an Indexes collection that contains a 
    // grouping index.
    public static final ErrorCode INTERFACE_SEQUENCE_ERROR_10 =
        new DataSourceParserError(5128);

    //getRecordLength can only be used on an Indexes collection that contains a 
    // grouping index.
    public static final ErrorCode INTERFACE_SEQUENCE_ERROR_11 =
        new DataSourceParserError(5129);

    //setRecordLength can only be used on an Indexes collection that contains a 
    // grouping index.
    public static final ErrorCode INTERFACE_SEQUENCE_ERROR_12 =
        new DataSourceParserError(5130);

    public static final ErrorCode WRONG_FIXED_LENGTH_RECORD_LENGTH2 =
        new DataSourceParserError(5131);
   
    
    /**
     * Constructor.
     */
    private DataSourceParserError() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private DataSourceParserError(long err) {
        super(DataSourceParserError.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 