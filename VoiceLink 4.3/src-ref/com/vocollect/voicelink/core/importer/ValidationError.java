/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

/**
 * @author vsubramani
 *
 */
public class ValidationError {

    private String theProblemReport = null;

    private ValidationResult theValidationResult = null;

    /**
     * Constructor.
     */
    public ValidationError() {
        // nothing to do - this is required by the Castor framework.
    }

    /**
     *
     *  @param validationResult
     *            specifies the ValidationResult object
     * @param objContext
     *            specifies the Object Context
     */
    public ValidationError(ValidationResult validationResult,
            ObjectContext<FieldMap, Object> objContext) {
        
        this.theValidationResult = validationResult;
        this.theProblemReport = objContext.getBadData(objContext.getFieldMaps());
        
    }

    /**
     * @return the theProblemReport
     */
    public String getTheProblemReport() {
        return theProblemReport;
    }

    /**
     * @param theProblemReport
     *            the theProblemReport to set
     */
    public void setTheProblemReport(String theProblemReport) {
        this.theProblemReport = theProblemReport;
    }

    /**
     * @return the theValidationResult
     */
    public ValidationResult getTheValidationResult() {
        return theValidationResult;
    }

    /**
     * @param theValidationResult
     *            the theValidationResult to set
     */
    public void setTheValidationResult(ValidationResult theValidationResult) {
        this.theValidationResult = theValidationResult;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 