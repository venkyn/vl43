/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import java.util.ArrayList;

/**
 * Default implementation of the CreateObjectTrigger.
 * Generally speaking, you will need to override the following methods:
 * <nl>
 * <le>openContext</le>
 * <le>detectTriggeringConditions</le>
 * <le>closeContext</le>
 * </nl>
 * 
 * you will also need to implement setters for the stuff that gets passed in during the configuration.
 * 
 * @author dgold
 *
 */
public class CreateObjectTriggerImpl implements CreateObjectTrigger {
    
    private static final Logger log          = new Logger(
            CreateObjectTriggerImpl.class);

    // Flag - is this context open?
    private boolean open = false;
    
    // Flag to show this trigger creates the markup for an object that 
    // needs to be looked up in the database.
    private boolean lookup = false;
    
    // Name of object
    private String newObjectName = null;
    
    // Name of setter for this object in parent
    private String setterName = null;
    
    // Name of getter for this object in parent
    private String getterName = null;
    
    // Tag to use to open an object context
    private String openTag = null;
    
    // Tag to use to close an object context
    private String closeTag = null;
    
    // List of fields to be written by this trigger.
    private FieldMap fieldMap = new FieldMap();

    // Flag to indicate that this trigger should use all of the fields in the fieldList
    private boolean useAllFields = true;
    
    // Map of field names to be added to the XML context of the new object when opened
    private FieldMap openContextFields    = new FieldMap();
    
    // Map of field names to be added to the XML context of the new object when closed
    private FieldMap closeContextFields   = new FieldMap();
    
    // Array of triggers that should be evaluated when this trigger fires. Typically used for sub-objects.
    private ArrayList<CreateObjectTrigger> sympatheticTriggers = 
        new ArrayList<CreateObjectTrigger>();

    // indicator says this object is the top-level object, or not
    private boolean mainObject = true;
    
    // If this trigger represents objects in a list, name of the list as a member
    private String list = null;

    // Flag to indicate that the trigger represents objects in a list.
    private boolean useList;
    
    private boolean suppress = false;


    /**
     * Default constructor.
     */
    public CreateObjectTriggerImpl() {
        // Nothing special to do.
    }

    /**
     * Do nothing but return the original string.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#closeContext(java.lang.String)
     * @param openContext
     *            XML string
     * @param fields
     *            fields to use to populate the object when closing the context
     * @return the Strig context
     */
    public String closeContext(String openContext, FieldMap fields) {
        String newContext = openContext;
        setIsOpen(false);
        if (this.isSuppress()) {
            return "";
        }
        if (null != fields) {
            if (!this.getUseAllFields()) {
                newContext += writeFields(fields, closeContextFields);
            } else {
                newContext += writeFields(fields, fields);
            }
        }


        newContext += getCloseTag();
        return newContext;
    }

    /**
     * Open an object context if triggering event occurs.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap,
     *      java.lang.String)
     * @param fields specifies the FieldMap object
     * @param xmlString specifies the String object
     * @return the String context or null
     */
    public String createObjectIfTriggered(FieldMap fields, String xmlString) {
        if (detectTriggeringConditions(fields)) {
            String newXmlString = openContext(xmlString, fields);
            return closeContext(newXmlString, fields);
        }
        return null;
    }

    /**
     * return true.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#detectTriggeringConditions()
     * @param fields
     *            fields to earch for triggering events.
     * @return true
     */
    public boolean detectTriggeringConditions(FieldMap fields) {
        // This always returns true
        return true;
    }

    /**
     * Get the name of the object to create when the trigger fires.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#getNewObjectTag()
     * @return new object name
     */
    public String getNewObjectName() {
        return this.newObjectName;
    }

    /**
     * get the tag that opebns the context.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#getOpenTag()
     * @return the open tag <...>
     */
    public String getOpenTag() {
        return this.openTag;
    }

    /**
     * Is this context open? This impl always returns false.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isOpen()
     * @return true if the context is open.
     */
    public boolean isOpen() {
        return this.open;
    }

    /**
     * Open the context. Im this impl, this means to <xxx>, then all the tagged
     * data, then </xxx>
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#openContext(java.lang.String,
     *      com.vocollect.voicelink.core.importer.FieldMap)
     * @param xmlRepresentation
     *            the string to mark up
     * @param fields
     *            the fields to use for tags & data
     * @return the marked up string
     */

    public String openContext(String xmlRepresentation, FieldMap fields) {
        
        String newXmlRep = xmlRepresentation + getOpenTag();
        // We can designate a trigger to 'just use all of the fields', in which case
        // those fields will be written by the closeContext. It's safer that way -
        // we don't care if there are fields that require some data from the rest of
        // the input stream.
        if (!this.getUseAllFields()) {
            newXmlRep += writeFields(fields, openContextFields);
        }
        newXmlRep += fireSympatheticTriggers(fields);
        if (this.isSuppress()) {
            return "";
        }
        setIsOpen(true);
        
        return newXmlRep;
    }

    /**
     * Write all fields, or just the ones listed in fieldMapping.
     * @param fields list of available fields
     * @param fieldsToWrite the list of fields to write. Ignored for this trigger. 
     * @return the string with all of them fields in full markup
     */
    @SuppressWarnings("unchecked")
    public String writeFields(FieldMap fields, 
                                FieldMap fieldsToWrite) {
        String xmlString = "";
        Field field = null;
        if (getUseAllFields()) {
            for (Field x : fields.values()) {
                if (!(x.isRemapped() || x.isSuppress() || x.isNullField())) {
                    xmlString += x.getOpenTag() + x.getFieldData().trim()
                    + x.getCloseTag();
                }
            }

        } else {
            if ((null == fields) || (fields.isEmpty()) || (null == fieldsToWrite)) {
                // This is OK, but would be unusual.
                log.warn("Trigger '" + this.getClass().getSimpleName() 
                        + "' has an empty field list, but is not set to use all fields.", 
                         TriggerErrors.NO_FIELDS_TO_WRITE);
            } else {
                // for every field listed in fieldsToWrite, get the field from the input & write it
                for (Field x : fieldsToWrite.values()) {
                    field = fields.get(x.getFieldName());
                    if ((null != field) && (!field.isSuppress()) && (!x.isSuppress())) {
                        xmlString += field.getOpenTag() + field.getFieldData().trim()
                        + field.getCloseTag();
                    }
                }
            }
        }
        return xmlString;
    }
    /**
     * Set the close tag.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setCloseTag(java.lang.String)
     * @param newObjectTagContents
     *            the tag to use to close the context.
     */
    public void setCloseTag(String newObjectTagContents) {
        this.closeTag = newObjectTagContents;
    }

    /**
     * Set the name of the object to create.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setNewObjectTag(java.lang.String)
     * @param newObjectname
     *            name of the obj to create
     */
    public void setNewObjectName(String newObjectname) {
        // Due to a defect in the Castor unmarshalling, we may need to trim the resulting data (newObjectname).
        // In at least one instance, the unmarshaller picked up one space immediately preceeding the real data,
        // causing the newObjectname to be " pick", which when rendered asa tag came out as < pick>, causing
        // the marshaller to fail.
        this.newObjectName = newObjectname.trim();
        
        // Make sure the setter and getter names are always set.
        if (!useList) {
            if (null == getGetterName()) {
                setGetterName(newObjectname);
            }
            if (null == getSetterName()) {
                setSetterName(newObjectname);
            }
        }
        

    }

    /**
     * Set the tag to use to open the obj context.
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setOpenTag(java.lang.String)
     * @param newObjectTagContents
     *            tag to use to set the open context.
     */
    public void setOpenTag(String newObjectTagContents) {
        this.openTag = newObjectTagContents;

    }

    /**
     * Get the tage used to close the context .
     * 
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#getCloseTag()
     * @return the closing tag
     */
    public String getCloseTag() {
        return closeTag;
    }

    /**
     * Simple setter for the isOpen flag.
     * 
     * @param isThisOpen
     *            the new vallue for isOpne
     */

    public void setIsOpen(boolean isThisOpen) {
        this.open = isThisOpen;
    }

    /**
     * Gets list of fields this trigger should write to the output.
     * @return the fieldMap
     */
    public FieldMap getFieldMap() {
        return fieldMap;
    }

    /**
     * Sets the list of fields this trigger should write to the output.
     * @param fieldMapP the fieldMap to set
     */
    public void setFieldMap(FieldMap fieldMapP) {
        // This should take the list of fields, set by the Importer configuration,
        // and copy the fields to the correct list.
        for (Field field : fieldMapP.values()) {
            addField(field);
        }
    }

    /**
     * Get the flag that says to use all fields in this trigger.
     * @return the flag that says to use all fields in this trigger.
     */
    public boolean getUseAllFields() {
        return useAllFields;
    }

    /**
     * Set the flag that says to use all fields in this trigger.
     * @param useAll The flag that says to use all fields in this trigger.
     */
    public void setUseAllFields(boolean useAll) {
        useAllFields = useAll;
    }

    /**
     * @return the fields we write when we close an xml context for this trigger.
     */
    public FieldMap getCloseContextFields() {
        return closeContextFields;
    }

    /**
     * @return the fields we write when we open an xml context for this trigger.
     */
    public FieldMap getOpenContextFields() {
        return openContextFields;
    }

    /**
     * @param fields the fields we write when we close an xml context for this trigger.
     */
    public void setCloseContextFields(FieldMap fields) {
        closeContextFields = fields;
        return;
    }

    /**
     * @param fields the fields we write when we open an xml context for this trigger.
     */
    public void setOpenContextFields(FieldMap fields) {
        openContextFields = fields;
    
    }

    /**
     * Gets the value of lookup.
     * @return the lookup
     */
    public boolean isLookup() {
        return lookup;
    }

    /**
     * Sets the value of the lookup.
     * @param lookup the lookup to set
     */
    public void setLookup(boolean lookup) {
        this.lookup = lookup;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.FieldMap#addField(com.vocollect.voicelink.core.importer.Field)
     */
    public void addField(Field aField) {
        if (aField.isWriteOnOpenContext()) {
            openContextFields.addField(aField);
        } else {
            this.closeContextFields.addField(aField);
        }
        fieldMap.addField(aField);
    }

    /**
     * Call lookupObject for every nested trigger.
     * @param owner owner of the composite.
     * @param objectCompleter the thing we would use to look up this entity in the DB
     * @return whatever we get
     * @throws VocollectException when we look something up and find it (or not) when expected
     */
    public Object lookupObject(Object owner, DBLookup objectCompleter) throws VocollectException {

        for (CreateObjectTrigger trigger : this.getTriggers()) {
            Object thisTriggersSubject = DBLookup.getCompositePart(owner, trigger.getGetterName());
            thisTriggersSubject = trigger.lookupObject(thisTriggersSubject, objectCompleter);
            DBLookup.setCompositePart(owner, thisTriggersSubject, trigger.getSetterName());
        }
        return owner;
    }
    
//    /**
//     * {@inheritDoc}
//     */
//    public Object lookupObject(Collection subject, DBLookup objectFinder) 
//        throws VocollectException {
//        for (Object bugger : subject) {
//            lookupObject(bugger, objectFinder);
//        }
//        return subject;
//    }
//
//
//    /**
//     * {@inheritDoc}
//     */
//    public Object recurseLookups(Object subject, DBLookup objectFinder) throws VocollectException {
//
//        Object thisTriggersSubject = DBLookup.getCompositePart(subject, getterName);
//        try {
//            lookupObject((Collection) thisTriggersSubject, objectFinder);
//        } catch (ClassCastException e) {
//            // Expected - not a collection
//        }
//        lookupObject(thisTriggersSubject, objectFinder);
//        return null;
//    }

    /**
     * Add a trigger that is evaluated when this one does.
     * @param newTrigger the trigger to add
     */
    public void addTrigger(CreateObjectTrigger newTrigger) {
        sympatheticTriggers.add(newTrigger);
    }

    /**
     * Get all of the triggers, so we can evaluate them.
     * @return the list of sympathetic triggers.
     */
    public ArrayList<CreateObjectTrigger> getTriggers() {
        return sympatheticTriggers;
    }

    /**
     * Fire each trigger listed under this trigger.
     * @param fields the list of filed (original data) to operate on.
     * @return the result of the contexts.
     */
    public String fireSympatheticTriggers(FieldMap fields) {
        String newXmlString = "";
        
        for (CreateObjectTrigger trigger : this.getTriggers()) {
            newXmlString = trigger.createObjectIfTriggered(fields, newXmlString);
        }
        return newXmlString;
    }

    /**
     * Reset the field data so the trigger fires next time it's evaluated.
     */
    public void reset() {
        for (Field f : this.getFieldMap().values()) {
            f.setFieldData("");
        }
        
    }

    /**
     * Set the indicator that this trigger represents the main object.
     * This will determine how we treat the object when looking it up and when saving it.
     * @return flag indicating that this trigger represents the main object.
     */
    public boolean isMainObject() {
        return mainObject;
    }

    /**
     * Get the indicator that this trigger represents the main object.
     * @param mainFlag flag indicating that this trigger represents the main object.
     */
    public void setMainObject(boolean mainFlag) {
        mainObject = mainFlag;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isUseList() {
        return useList;
    }

    /**
     * {@inheritDoc}
     */
    public void setUseList(boolean useList) {
        this.useList = useList;
    }

    /**
     * Gets the value of list.
     * @return the list
     */
    public String getList() {
        return list;
    }

    /**
     * Sets the value of the list.
     * @param list the list to set
     */
    public void setList(String list) {
        this.list = list;
        setSetterName(list);
        setGetterName(list);
        setUseList(true);
    }

    /**
     * Gets the value of getterName.
     * @return the getterName
     */
    public String getGetterName() {
        return getterName;
    }

    /**
     * Sets the value of the getterName.
     * @param getterName the getterName to set
     */
    public void setGetterName(String getterName) {
        if (!useList) {
            this.getterName = getterName;
        }
    }

    /**
     * Gets the value of setterName.
     * @return the setterName
     */
    public String getSetterName() {
        return setterName;
    }

    /**
     * Sets the value of the setterName.
     * @param setterName the setterName to set
     */
    public void setSetterName(String setterName) {
        if (!useList) {
            this.setterName = setterName;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#setSuppress(boolean)
     */
    public void setSuppress(boolean isSuppressed) {
        this.suppress = isSuppressed;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.CreateObjectTrigger#isSuppress()
     */
    public boolean isSuppress() {
        return this.suppress;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 