/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.LinkedHashMap;

/**
 * @author dgold
 *
 */

public class KeyFieldCache {

    private LinkedHashMap<String, LinkedHashMap<String, Integer>> keyCache                = null;

    /*
     * This is a map containing a set of caches indexed by name Each cache has a
     * string key paired with a count of the times it has been encountered in
     * (this) import. LinkedHashMap<String,LinkedHashMap<String,Integer>>
     * field name-^ | | Field data--+ | number of times field data was
     * found----+ So, asking to 'find', you need the name of the cache(field),
     * and the value you want to find.
     */

    private int                                                   initialKeyCount;

    private int                                                   initialCacheSize;

    // default values for the size of the data caches.
    private final int                                             defaultInitialKeyCount  = 2;

    private final int                                             defaultInitialCacheSize = 2000;

    /**
     * Default constructor. Set sizes to defaults.
     */
    public KeyFieldCache() {
        super();
        this.setInitialKeyCount(this.defaultInitialKeyCount);
        this.setInitialCacheSize(this.defaultInitialCacheSize);
        this.keyCache = new LinkedHashMap<String, LinkedHashMap<String, Integer>>(
                this.getInitialKeyCount());
    }

    /**
     * Constructor that sizes the cache.
     * 
     * @param keys
     *            number of caches
     * @param elements
     *            number of expected entries
     */
    public KeyFieldCache(int keys, int elements) {
        super();
        this.setInitialKeyCount(keys);
        this.setInitialCacheSize(elements);
        this.keyCache = new LinkedHashMap<String, LinkedHashMap<String, Integer>>(
                this.getInitialKeyCount());
    }

    /**
     * Add a new key field map to the cache.
     * 
     * @param keyFieldName
     *            name of key field map to add
     * @return true if the key can be added, false if it already exists.
     */
    @SuppressWarnings("unchecked")
    public boolean addNewKeyField(String keyFieldName) {
        if (this.keyCache.containsKey(keyFieldName)) {
            return false;
        }
        LinkedHashMap newCache = new LinkedHashMap<String, Integer>(
                getInitialCacheSize());
        this.keyCache.put(keyFieldName, newCache);
        return true;
    }

    /**
     * Get the size that new caches are set to.
     * 
     * @return the initial cache size as an integer
     */
    public int getInitialCacheSize() {
        return this.initialCacheSize;
    }

    /**
     * Get the size that new caches are set to.
     * 
     * @param initialCacheSize as int to set
     */
    public void setInitialCacheSize(int initialCacheSize) {
        this.initialCacheSize = initialCacheSize;
    }

    /**
     * Find the value in the field.
     * 
     * @param keyFieldName
     *            name of the field to look in
     * @param keyFieldValue
     *            value of the data to look for
     * @return true if the value exists within the field, false otherwise.
     */
    public boolean find(String keyFieldName, String keyFieldValue) {
        if (!this.keyCache.containsKey(keyFieldName)) {
            return false;
        }
        LinkedHashMap requestedKeys = this.keyCache.get(keyFieldName);
        return requestedKeys.containsKey(keyFieldValue);
    }

    /**
     * Add a particular data value to a particular keyfield cache. If not
     * present, it will add the value to the cache and set the field count to
     * zero If the field is present, but the data isn't, it will add the data
     * and set the field count to zero
     * 
     * @param keyFieldName
     *            field to look in
     * @param key
     *            Field data to find
     * @return true if the key was added
     */
    public boolean addKey(String keyFieldName, String key) {
        final int elementCount = 0;
        LinkedHashMap<String, Integer> requestedKeys = this.keyCache
                .get(keyFieldName);

        if (requestedKeys == null) {
            // Key field not being tracked. Add it.
            addNewKeyField(keyFieldName);
            requestedKeys = this.keyCache.get(keyFieldName);
            requestedKeys.put(key, elementCount);
            return true;
        } else {
            // possible that the field is there, but the key isn't
            if (!requestedKeys.containsKey(key)) {
                requestedKeys.put(key, elementCount);
                return true;
            }
            return false;
        }
    }

    /**
     * Increment the element count of the desired element, if found.
     * 
     * @param keyFieldName the String to set
     * @param key the String to set
     * @return the current count. Zero indicates the element is not being
     *         tracked.
     */
    public int incrementKeyCount(String keyFieldName, String key) {
        int elementCount = 0;

        if (!find(keyFieldName, key)) {
            return 0;
        }
        Integer keyValueCount = this.keyCache.get(keyFieldName).get(key);
        elementCount = keyValueCount.intValue();
        elementCount++;
        this.keyCache.get(keyFieldName).put(key, elementCount);
        return elementCount;
    }

    /**
     * Get the count of the times the data element was seen in the input field.
     * 
     * @param keyFieldName
     *            name of the field
     * @param key
     *            data value to look up
     * @return the count of times the element ewas seen in the input.
     */
    public int getKeyCount(String keyFieldName, String key) {
        if (!find(keyFieldName, key)) {
            return 0;
        }
        return this.keyCache.get(keyFieldName).get(key);

    }

    /**
     * @return Returns the initialKeyCount - the value this list is initialized to
     */
    protected int getInitialKeyCount() {
        return this.initialKeyCount;
    }

    /**
     * @param initialKeyCount The initialKeyCount to set.
     */
    protected void setInitialKeyCount(int initialKeyCount) {
        this.initialKeyCount = initialKeyCount;
    }
    
    /**
     * Clears the cache.
     *
     */
    public void clear() {
        this.keyCache.clear();
    }

    /**
     * @return Returns the defaultInitialCacheSize.
     */
    protected int getDefaultInitialCacheSize() {
        return this.defaultInitialCacheSize;
    }

    /**
     * @return Returns the defaultInitialKeyCount.
     */
    protected int getDefaultInitialKeyCount() {
        return this.defaultInitialKeyCount;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 