/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;

/**
 * @author dgold
 *
 */
public class ExceptionError {
    
    // TODO Add this to the mapping file so it gets exported to the error file correctly.
    
    private Object theProblem = null;
    private Exception theProblemReport = null;
    private ValidationResult theValidationResult = null;

    /**
     * Default c'tor.
     */
    public ExceptionError() {
           // nothing to do - this is required by the Castor framework.
    }  
    /**
     * 
     * @param problem specifies the object to report
     * @param validationResult specifies the ValidationResult object
     * @param problemReport specifies the Exception
     */
    public ExceptionError(Object problem, ValidationResult validationResult, Exception problemReport) {
        this.theProblem = problem;
        this.theValidationResult = validationResult;
        this.theProblemReport = problemReport;
    }

    /**
     * @return the theProblem
     */
    public Object getTheProblem() {
        return theProblem;
    }

    /**
     * @param theProblem the theProblem to set
     */
    public void setTheProblem(Object theProblem) {
        this.theProblem = theProblem;
    }

    /**
     * @return the theProblemReport
     */
    public Exception getTheProblemReport() {
        return theProblemReport;
    }

    /**
     * @return the theProblemReport
     */
    public String getTheProblemReportString() {
        try {
            return ((VocollectException) theProblemReport).getUserMessage().getKey();
        } catch (ClassCastException e) {
            // expected
        }
        return theProblemReport.getLocalizedMessage();
    }

    /**
     * @param theProblemReport the theProblemReport to set
     */
    public void setTheProblemReport(Exception theProblemReport) {
        this.theProblemReport = theProblemReport;
    }

    /**
     * @return the theValidationResult
     */
    public ValidationResult getTheValidationResult() {
        return theValidationResult;
    }

    /**
     * @param theValidationResult the theValidationResult to set
     */
    public void setTheValidationResult(ValidationResult theValidationResult) {
        this.theValidationResult = theValidationResult;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 