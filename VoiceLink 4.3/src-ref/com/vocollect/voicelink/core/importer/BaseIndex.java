/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.voicelink.core.importer;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The BaseIndex class is used to create other indexes.
 * It provides the basis of a list of Key fields that are concatenated
 * together and separated with a separator character.  it also provides methods
 * to set and get these, as well as to SetUp the structure and update
 * and clear the structure.
 *
 * @author jtauberg
 */

public abstract class BaseIndex {

    //This is a list of fields that represent one index.
    private ArrayList <Field> indexFieldList;

    /* This is the string that separates the data concatenated for each field of
     * a compound index.
     */
    private String compoundIndexSeparator = "|";
    

    /**
     * Default constructor.
     */
    public BaseIndex() {
        super();
        this.setIndexFieldList(new ArrayList <Field>());
    }

    
    /**
     * constructor of an index containing the list of fields passed.
     * @param fields - Collection of fields that make up this index.
     */
    public BaseIndex(Collection<Field> fields) {
        super();
        this.setIndexFieldList(new ArrayList <Field>());
        if (!fields.isEmpty()) {
            this.getIndexFieldList().addAll(fields);
        } 
    }
    
    
    /**
     * @param fieldToAdd - a field to add to this Index.
     * @return true
     */
    public boolean addField(Field fieldToAdd) {
        // if it doesn't exist yet, create it.
        if (this.getIndexFieldList() == null) {
            this.setIndexFieldList(new ArrayList <Field>()); 
        }
        return this.getIndexFieldList().add(fieldToAdd);
    }

    
    /**
     *  This must be called (one time) before using updateIndex.
     *  It will replace the fields named in the index with actual fields passed
     *  in via the fieldmap.
      * @param dataFields - FieldMap containing the actual data holding fields.
     */
    public void setUp(FieldMap dataFields) {
        ArrayList <Field> newDataFields = new ArrayList<Field>();
        for (Field myField : this.getIndexFieldList()) {
            newDataFields.add(dataFields.get(myField.getFieldName()));
        }
        this.setIndexFieldList(newDataFields);
    }

    /**
     * Causes this index to update itself.
     * It will do whatever work it needs to do based on the data in the
     * associated fields.
     */
    public abstract void updateIndex();

    /**
     * Causes this index to reset itself.
     * This will reset its data and counts but will not remove it's
     * key fields or base configuration.
     * Should be called when re-using same index on multiple input sources.
     */
    public abstract void resetIndex();
    
    
    
    /**
     * Creates a string using the current fields that stores the indexed
     * data value.
     * @return String containing index value of current fields.
     */
    public String currentFieldsIndexValue() {
        String tempString = "";
        //Loop through the fields in this index
        for (Field curField : this.getIndexFieldList()) {
            if (curField.getFieldData() != null) {
                //Add field data to tempString with a separator
                tempString = tempString + curField.getFieldData().toString()
                + this.compoundIndexSeparator;
            } else {
                // field is null, so just add a separator.
                tempString = tempString + this.compoundIndexSeparator; 
            }
        }
        return tempString;
    }

    
    /**
     * Creates a string using the index's key field names separated by separator.
     * @return String containing index name.
     */
    public String indexFieldsName() {
        String tempString = "";
        //Loop through the fields in this index
        for (Field curField : this.getIndexFieldList()) {
            //Add field name to tempString with a separator
            tempString = tempString + curField.getFieldName().toString()
            + this.compoundIndexSeparator;
        }
        return tempString;
    }

    
    /**
     * Getter for the compoundIndexSeparator property.
     * @return String value of the property
     */
    public String getCompoundIndexSeparator() {
        return this.compoundIndexSeparator;
    }

    
    /**
     * Setter for the compoundIndexSeparator property.
     * @param compoundIndexSeparator the new compoundIndexSeparator value
     */
    public void setCompoundIndexSeparator(String compoundIndexSeparator) {
        this.compoundIndexSeparator = compoundIndexSeparator;
    }


    /**
     * Sets the value of the indexFieldList.
     * @param indexFieldList the indexFieldList to set
     */
    protected void setIndexFieldList(ArrayList <Field> indexFieldList) {
        this.indexFieldList = indexFieldList;
    }


    /**
     * Gets the value of indexFieldList.
     * @return the indexFieldList
     */
    protected ArrayList <Field> getIndexFieldList() {
        return indexFieldList;
    }


 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 