/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.LinkedList;

/**
 * 
 * @author astein
 *
 */
public abstract class Filter
// implements com.opensymphony.xwork2.validator.Validator
        extends org.apache.commons.validator.ValidatorAction {

    /**
     * Default empty constructor.
     *
     */
    public Filter() {
        super();
        // TODO Auto-generated constructor stub
    }

    private LinkedList<Filter> filterList = new LinkedList<Filter>();

    /**
     * 
     * @param fieldData Object to set
     * @return true if execution possible
     */
    public abstract boolean execute(Object fieldData);

    /**
     * 
     * @param fieldData Object to set
     * @return true if executeList possible
     */
    public boolean executeList(Object fieldData) {
        for (Filter filter : filterList) {
            if (!filter.execute(fieldData)) {
                return false;
            }
        }
        return true;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 