/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;
import java.util.ArrayList;

/**
 * The Indexes class is a container for (base Index) Counting Index and 
 * grouping Index.
 * For (Counting) Index
 * It stores and counts each unique piece of data for each counting index it
 *  holds.  It includes methods to act on this data store. 
 *  
 *  It provides methods for accessing the Grouping Index.
 *
 * @author jtauberg
 */
public class Indexes {

    private ArrayList <Index> indexes;
    
    // The collection of indexes supports up to 1 grouping index.  This is it.
    private ArrayList <GroupingIndex> groupByIndex;

    //This sets up a the logger for Indexes
    private static final Logger log = new Logger(Indexes.class);


    /**
     * Constructor.
     */
    public Indexes() {
        super();
        indexes = new ArrayList<Index>();
        groupByIndex = new ArrayList<GroupingIndex>();
    }


    /**
     * @param indexToAdd - an index to add to this indexes.
     * @return true
     */
    public boolean addIndex(Index indexToAdd) {
        return this.indexes.add(indexToAdd);
    }


    /**
     * Adds a grouping index to the Indexes collection.
     * Note: Only one grouping index per Indexes is permitted.
     * @param indexToAdd - a grouping index to add to this indexes collection.
     * @return true
     */
    public boolean addIndex(GroupingIndex indexToAdd) {
        if (groupByIndex.isEmpty()) {
            groupByIndex.add(indexToAdd);
            return true;
        } else {
            log.fatal(
                "Configuration Error for " + this.getClass().getName()
                + " - Indexes collection can not contain more than one GroupingIndex",
                DataSourceParserError.INDEXES_CAN_ONLY_CONTAIN_ONE_GROUPING_INDEX);
            throw new RuntimeException("Configuration Error - Indexes collection"
                + " can not contain more than one GroupingIndex in "
                + this.getClass().getName() + ".");
        }
    }
    
    
    /**
     * This causes all stored indexes to update themselves with values
     * currently stored in fields.
     */
    public void updateIndexes() {
        for (BaseIndex anIndex : this.indexes) {
            anIndex.updateIndex();
        }
        if (this.containsGroupingIndex()) {
            groupByIndex.get(0).updateIndex();
        }
    }

    
    /**
     * Clears all elements from this structure.
     */
    public void clear() {
        this.indexes.clear();
        groupByIndex.clear();
    }
    

    /**
     * Resets all data and counts in the indexes in this structure.
     * Keeps the index configuration (the indexed fields).
     * Should be called before reusing same index on new data.
     */
    public void reset() {
        for (BaseIndex anIndex : this.indexes) {
            anIndex.resetIndex();
        }
        if (this.containsGroupingIndex()) {
            groupByIndex.get(0).resetIndex();
        }
    }
    
    

    
    /**
     *  This must be called one time and before using updateIndexes.
     *  It will replace the fields named in the index with actual fields passed
     *  in via the fieldmap.
      * @param dataFields - FieldMap containing the actual data holding fields.
     */
    public void setUp(FieldMap dataFields) {
        for (BaseIndex anIndex : this.indexes) {
            anIndex.setUp(dataFields);
        }
        if (this.containsGroupingIndex()) {
            groupByIndex.get(0).setUp(dataFields);
        }
    }
 
    
    /**
     * Boolean shows if current record should be skipped because of a duplication
     * skipping rule such as UseLastDup OR UseFirstDup on the index.
     * @return true if record should be skipped, or false if not.
     */
    public boolean isSkipCurrentRecordBecauseOfDup() {
        boolean result = false;
        for (Index anIndex : this.indexes) {
            if (anIndex.isSkipCurrentRecordBecauseOfDup()) {
                //if any indexes say to skip, then skip.
                result = true;
                break;
            }
        }
        return result;
    }
    
    
    
    
    
    /**
     * Boolean shows if file should fail based on a file level index constraint.
     * Note that this does NOT run on the groupingIndex.
     * @return true if file level index constraints are NOT met.
     *        False if file level index constraints are met.
     */
    public boolean isFileDataDuped() {
        for (Index anIndex : this.indexes) {
            if (anIndex.isIndexBlown()) {
                return true;
            }
        }
        return false;
    }

    
    /**
     * Boolean shows if file has dup data regardless of failFile flag settings.
     * Note that this does NOT run on the groupingIndex.
     * @return true if file has a dup regardless of failFile index setting.
     *        false if file has no dups regardless of failFile index settings.
     */
    public boolean isFileDataDupedIgnoreFlags() {
        for (Index anIndex : this.indexes) {
            if (anIndex.isIndexBlownIgnoreFlags()) {
                return true;
            }
        }
        return false;
    }


    /**
     * Boolean shows if record should fail based on record level index
     *      constraints for duplicate data (based on failRecord).
     * Note that this does NOT run on the groupingIndex.
     * @return true if current record's data does NOT meet index constraints.
     *        False if current record's data does meet index constraints.
     */
    public boolean isRecordDataDuped() {
        for (Index anIndex : this.indexes) {
            if (anIndex.isDataDuped()) {
                return true;
            }
        }
        return false;
    }

    
    /**
     * Boolean shows if current record has duplicated data ignoring failRecord
     * flag.
     * Note that this does NOT run on the groupingIndex.
     * @return true if current record's data is duplcated in index.
     *        False if current record's data is NOT duplicated in index.
     */
    public boolean isRecordDataDupedIgnoreFlags() {
        for (Index anIndex : this.indexes) {
            if (anIndex.isDataDupedIgnoreFlags()) {
                return true;
            }
        }
        return false;
    }

    
    /**
     * Creates a string listing indexes that were blown by current data.
     * Note:  adheres to rules of record level index constraints, so dup data on
     *          fields that are set failRecord=false will NOT be shown.
     * Note that this does NOT run on the groupingIndex.
     * @return empty string if no constraint blown,
     *           or listing of index name and current data that blew the index.
     */
    public String getRecordDataDuped() {
        String retStr = "";
        for (Index anIndex : this.indexes) {
            if (anIndex.isDataDuped()) {
                retStr = retStr + "Index named: " + anIndex.indexFieldsName() + " indicates duplicated data: "
                + anIndex.currentFieldsIndexValue() + "; ";
            }
        }
        return retStr;
    }

    
    /**
     * Creates a string listing all indexes that were blown by current data.
     * Note:  shows ALL data ignoring record level index constraints, so dup
     *        data on fields that are set failRecord=false WILL be shown.
     * Note that this does NOT run on the groupingIndex.
     * @return empty string if no dup data,
     *           or listing of index name and current data that was duped.
     */
    public String getRecordDataDupedIgnoreFlags() {
        String retStr = "";
        for (Index anIndex : this.indexes) {
            if (anIndex.isDataDupedIgnoreFlags()) {
                retStr = retStr + "Index named: " + anIndex.indexFieldsName() + " indicates duplicated data: "
                + anIndex.currentFieldsIndexValue() + "; ";
            }
        }
        return retStr;
    }
    

    /**
     * @param index - integer location of the index to get from indexes.
     * @return returns an index based on the int passed in.
     * @see java.util.ArrayList#get(int)
     */
    public Index get(int index) {
        return indexes.get(index);
    }

    
    /**
     * This gets the grouping index in the collection if one exists.
     * @return returns the grouping index if it exists, or null if not.
     */
    public GroupingIndex getGroupingIndex() {
        if (!this.containsGroupingIndex()) {
            return null;
        } else {
            return this.groupByIndex.get(0);
        }
    }
    
    
    
    /**
     * Tells if the Indexes collection contains a grouping index.
     * @return Boolean returns true if Indexes contains a grouping index, else false.
     */
    public boolean containsGroupingIndex() {
        return !this.groupByIndex.isEmpty();
    }
    

    /**
     * @return ture if no index contained in indexes. else false.
     * @see java.util.ArrayList#isEmpty()
     */
    public boolean isEmpty() {
        return (indexes.isEmpty() && (groupByIndex.isEmpty()));
    }


    /**
     * @return returns Long current charCount of grouping index valid in setup and
     * updating of index.
     * 
     * THIS MAY ONLY BE USED WHEN Indexes.containsGroupingIndex.
     * 
     * @see com.vocollect.voicelink.core.importer.GroupingIndex#getCharCount()
     */
    public Long getCharCount() {
        if (this.containsGroupingIndex()) {
            return groupByIndex.get(0).getCharCount();
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - getCharCount may only be used on Indexes containing a "
                + "grouping index.", DataSourceParserError.INTERFACE_SEQUENCE_ERROR_9);
        throw new RuntimeException("Interface sequence error for "
          + this.getClass().getName() + " - getCharCount may only be used on "
          + "Indexes containing a grouping index.");
        }
    }


    /**
     * @param charCount - Long current charCount to be stored with current
     *                     data's key.  Should be set immediately before
     *                     calling the updateIndexes if using a grouping index.
     *                     
     *                     THIS MAY ONLY BE USED WHEN containsGroupingIndex.
     *                     
     *                     The charCount is the count of characters from the
     *                     beginning of the file to the start of this record.
     *                     
     *             Note:   Default charCount is zero, so if not set before update
     *                     and grouping index is being used, each record of the
     *                     file will read as the first record each time.
     * @see com.vocollect.voicelink.core.importer.GroupingIndex#setCharCount(java.lang.Long)
     */
    public void setCharCount(Long charCount) {
        if (this.containsGroupingIndex()) {
            groupByIndex.get(0).setCharCount(charCount);
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - setCharCount may only be used on Indexes containing a "
                + "grouping index.", DataSourceParserError.INTERFACE_SEQUENCE_ERROR_10);
        throw new RuntimeException("Interface sequence error for "
          + this.getClass().getName() + " - setCharCount may only be used on "
          + "Indexes containing a grouping index.");
        }
    }

    
    /**
     * @return returns Long current recordLength of grouping index valid in setup and
     * updating of index.
     * 
     * THIS MAY ONLY BE USED WHEN Indexes.containsGroupingIndex.
     * 
     * @see com.vocollect.voicelink.core.importer.GroupingIndex#getRecordLength()
     */
    public Long getRecordLength() {
        if (this.containsGroupingIndex()) {
            return groupByIndex.get(0).getRecordLength();
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - getREcordLength may only be used on Indexes containing a "
                + "grouping index.", DataSourceParserError.INTERFACE_SEQUENCE_ERROR_11);
        throw new RuntimeException("Interface sequence error for "
          + this.getClass().getName() + " - getRecordLength may only be used on "
          + "Indexes containing a grouping index.");
        }
    }


    /**
     * @param recordLength - Long current recordLength to be stored with current
     *                     data's key.  Should be set immediately before
     *                     calling the updateIndexes if using a grouping index.
     *                     
     *                     THIS MAY ONLY BE USED WHEN containsGroupingIndex.
     *                     
     *                     The recordLength is the count of characters in this
     *                     current record.
     *                     
     *             Note:   Default recordLength is zero, so if not set before update
     *                     and grouping index is being used, each record of the
     *                     file will read as having no length.
     * @see com.vocollect.voicelink.core.importer.GroupingIndex#setRecordLength(java.lang.Long)
     */
    public void setRecordLength(Long recordLength) {
        if (this.containsGroupingIndex()) {
            groupByIndex.get(0).setRecordLength(recordLength);
        } else {
            log.fatal("Interface sequence error on " + this.getClass().getName()
                + " - setRecordLength may only be used on Indexes containing a "
                + "grouping index.", DataSourceParserError.INTERFACE_SEQUENCE_ERROR_12);
        throw new RuntimeException("Interface sequence error for "
          + this.getClass().getName() + " - setRecordLength may only be used on "
          + "Indexes containing a grouping index.");
        }
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 