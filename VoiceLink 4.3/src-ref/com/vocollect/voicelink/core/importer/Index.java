/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;


/**
 * The (Counting) Index class is a list of fields and a map for these fields
 * that stores a string (index key) and a IndexRecord (count and trackOneDupValue).
 * 
 * 
 * For each record in a file,
 * the value of the data of the field(s) in the list will be concatenated
 * together (separated by the separator) and stored as the (key) string in the
 * map.  The count (and trackOneDupValue) will be incremented each time the value
 * is attepmted to be added to the map.
 * 
 * There are also several ways to determine if the index has duplicates.
 *
 * The trackOneDupValue contains a destroyable copy of the count for this
 * index.  This member is decremented to track a dup and in conjunction with a
 * manipulator this combination implements the useFirstDup or useLastDup feature.
 *
 * The way the useFirstDup and useLastDup are implemented is as follows:
 * 
 * @author jtauberg
 */
public class Index extends BaseIndex {

    /*  This is a map that contains the data points for a given index and its
     *  IndexRecord consisting of the count and the trackOneDupValue
     */
    private LinkedHashMap<String, IndexRecord> indexDataPoints = null;
    
    // Stores a count of how many data points are dup.
    private Long dupSets = 0L;
    
    /* Stores a total count of how many items in the index 
     *  are duped (one data point can have multiple items duped).
     */
    private Long dupItems = 0L;

    
   //Flag that indicates if index should care at record level if there are dups.
    //if this is set to false, the index won't report dups even if asked.
    private boolean failRecord = false;

    //Flag that indicates if index should care at file level if there are dups.
    //if this is set to false, the index won't report dups even if asked.
    private boolean failFile = false;
    
    //Flag that sets how index will respond to manipulator that can be used
    // to only process first duplicate record and to fail all others.
    private boolean useFirstDup = false;

    //Flag that sets how index will respond to manipulator that can be used
    // to only process last duplicate record and to fail all others.
    private boolean useLastDup = false;
    
    
    
    /**
     * @author jtauberg
     * Each indexRecord has a count and a trackOneDupValue.
     * 
     */
    class IndexRecord {
        //Stores the count for this index.
        private Integer count = 0;

        //Stores the trackOneDup value.  When indexed, count = trackOneDupValue
        //count is never decremented, but trackOneDupValue can be for that purpose.
        private Integer trackOneDupValue = 0;

        
        /**
         * @return the count
         */
        public Integer getCount() {
            return count;
        }

        
        /**
         * @param count the count to set
         */
        public void setCount(Integer count) {
            this.count = count;
        }

        
        /**
         * @return the trackOneDupValue
         */
        public Integer getTrackOneDupValue() {
            return trackOneDupValue;
        }

        
        /**
         * @param trackOneDupValue the trackOneDupValue to set
         */
        public void setTrackOneDupValue(Integer trackOneDupValue) {
            this.trackOneDupValue = trackOneDupValue;
        }

        /**
         * This is called to reduce the value of TrackOneDupValue by 1.
         */
        public void decrementTrackOneDupValue() {
            this.trackOneDupValue--;
        }
        
        /**
         * Calling this will increment both the count and trackOneDupValue.
         */
        public void incrementValues() {
            this.trackOneDupValue++;
            this.count++;
        }
    }
    
    
    
    /**
     * Default constructor. Set sizes to defaults.
     */
    public Index() {
        super();
        this.setIndexFieldList(new ArrayList <Field>());
        this.indexDataPoints = new LinkedHashMap<String, IndexRecord>();
    }
    
    
    /**
     * constructor of an index containing the list of fields passed.
     * @param fields - Collection of fields that make up this index.
     */
    public Index(Collection<Field> fields) {
        super();
        this.setIndexFieldList(new ArrayList <Field>());
        this.indexDataPoints = new LinkedHashMap<String, IndexRecord>();
        if (!fields.isEmpty()) {
            this.getIndexFieldList().addAll(fields);
        } 
    }

    /**
     * Causes previously loaded index to check the current data and return
     * a count for it.
     * @return Integer count of how many times current data run through index.
     *          or null if data doesn't exist in index.
     */
    public Integer getCount() {
        String tempString = currentFieldsIndexValue();
        //get count for this key (tempstring).
        if (this.indexDataPoints.containsKey(tempString)) {
            return this.indexDataPoints.get(tempString).getCount(); 
        } else {
            // this key is not even in the data....  error condition?
            return null;
        }
    }
    
    
    /**
     * Causes this index to update itself.
     * It will generate a new value based on the data in the associated fields.
     * If the value is not present in this index, it will be added.
     * If the value is present, the counter will increment by 1.
     */
    public void updateIndex() {
        String tempString = currentFieldsIndexValue();
        IndexRecord aRecord = null;
        
        //if this tempstring is already there, we increment count...
        if (this.indexDataPoints.containsKey(tempString)) {
            if (this.indexDataPoints.get(tempString).getCount() == 1) {
                // set wasn't dupe, but now it is so add one to dup sets.
                this.dupSets++;
                //This item wasn't previously dupe... now it is...
                // need to extra count to dupItems to represent original one.
                this.dupItems++;
            }
            //Get this record, increment the count and trackOneDupValue to the
            //value calculated.
            aRecord = this.indexDataPoints.get(tempString);
            aRecord.incrementValues();
            //put the incremented record back in.
            this.indexDataPoints.put(tempString, aRecord);
            // increment count of dup items.
            this.dupItems++;
        } else {
            //create new record (starts at 0), increment it to 1 and save it.
            aRecord = new IndexRecord();
            aRecord.incrementValues();
            this.indexDataPoints.put(tempString, aRecord);
        }
    }


    
    /**
     * Causes previously loaded index to check the current data to see if it is
     *  a dup (record level).
     * Note that this will hide dup issues if failRecord is false. 
     * @return true if failRecord=true AND current data is duplicated in index.
     *          otherwise it returns False.
     */
    public boolean isDataDuped() {
        if (failRecord) {
            String tempString = currentFieldsIndexValue();
            //get count for this key (tempstring).
            if (this.indexDataPoints.containsKey(tempString)) {
                return (this.indexDataPoints.get(tempString).getCount() > 1L); 
            } else {
                // this key is not even in the data....  error condition?
                return false;
            }
        } else {
            return false;
        }
    }

    
    /**
     * Causes previously loaded index to check the current data to see if it is
     *  a dup (record level).
     * @return true if current data is duplicated in index.  False if not.
     */
    public boolean isDataDupedIgnoreFlags() {
        String tempString = currentFieldsIndexValue();
        //get count for this key (tempstring).
        if (this.indexDataPoints.containsKey(tempString)) {
            return (this.indexDataPoints.get(tempString).getCount() > 1L); 
        } else {
            // this key is not even in the data....  error condition?
            return false;
        }
    }

    
    /**
     * This boolean tells if the index has any duplications (file level).
     * Note that this Ignores failFile flag and returns details about any dup.  
     * @return true if index contains any duplicated data points.  false if not.
     */
    public boolean isIndexBlownIgnoreFlags() {
        return (this.dupItems > 0);
    }

    
    /**
     * This boolean tells if the index has any duplications (File level).
     * Note that this index will hide dup issues if failFile is false. 
     * @return true failFile = true and index contains any duplicate data points
     *          otherwise it returs false. 
     */
    public boolean isIndexBlown() {
        if (failFile) {
            return (this.dupItems > 0);
        } else {
            return false;
        }
    }
    

/**
 *  * @return true if we should skip the current record because of a duplication
 * or false if we should not.  This is affected by the setting of either
 * isUseLastDup OR isUseFirstDup.
 */
public boolean isSkipCurrentRecordBecauseOfDup() {
       boolean result = false;
       IndexRecord aRecord = null;

       if (this.isUseLastDup()) {
           String tempString = currentFieldsIndexValue();
           aRecord = this.indexDataPoints.get(tempString);
           //decrement this
           aRecord.decrementTrackOneDupValue();
           //If the record is not the last one...
           if (aRecord.getTrackOneDupValue() != 0) {
               //Since it's not the last one, skip it (true).
               result = true;
           }
           //save incremented record back into indexDataPoints
           this.indexDataPoints.put(tempString, aRecord);
       } else if (this.isUseFirstDup()) {
           String tempString = currentFieldsIndexValue();
           aRecord = this.indexDataPoints.get(tempString);
           //if the value 
           if (aRecord.getTrackOneDupValue() <= 0) {
               result = true;
           }
           //set this to zero, since we just accepted the first record.
           aRecord.setTrackOneDupValue(0);
       }
       return result;
   }

    
    /**
     * Getter for the dupItems property.
     * @return Long value of the property
     */
    public Long getDupItems() {
        return this.dupItems;
    }


    /**
     * Getter for the dupSets property.
     * @return Long value of the property
     */
    public Long getDupSets() {
        return this.dupSets;
    }


    /**
     * Getter for the failFile property.
     * @return boolean value of the property
     */
    public boolean isFailFile() {
        return this.failFile;
    }

    
    /**
     * Setter for the failFile property.
     * @param failFile the new failFile value
     */
    public void setFailFile(boolean failFile) {
        this.failFile = failFile;
    }

    
    /**
     * Getter for the failRecord property.
     * @return boolean value of the property
     */
    public boolean isFailRecord() {
        return this.failRecord;
    }

    
    /**
     * Setter for the failRecord property.
     * @param failRecord the new failRecord value
     */
    public void setFailRecord(boolean failRecord) {
        this.failRecord = failRecord;
    }

    /**
     * This resets the data and counts on this configured index.
     * Should be called before reusing Index.
     */
    public void resetIndex() {
        this.indexDataPoints = new LinkedHashMap<String, IndexRecord>();
        dupSets = 0L;
        dupItems = 0L;
    }


    
    /**
     * Returns Flag that sets how index will respond to manipulator that can be used
     * to only process first duplicate record and to fail all others.
     * 
     * @return the useFirstDup
     */
    public boolean isUseFirstDup() {
        return useFirstDup;
    }


    
    /**
     * Sets Flag that sets how index will respond to manipulator that can be used
     * to only process first duplicate record and to fail all others.
     * 
     * @param useFirstDup the useFirstDup to set
     */
    public void setUseFirstDup(boolean useFirstDup) {
        this.useFirstDup = useFirstDup;
    }


    
    /**
     * Returns Flag that sets how index will respond to manipulator that can be used
     * to only process last duplicate record and to fail all others.
     * 
     * @return the useLastDup
     */
    public boolean isUseLastDup() {
        return useLastDup;
    }


    
    /**
     * Sets Flag that sets how index will respond to manipulator that can be used
     * to only process last duplicate record and to fail all others.
     * @param useLastDup the useLastDup to set
     */
    public void setUseLastDup(boolean useLastDup) {
        this.useLastDup = useLastDup;
    }
    
    
}


*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 