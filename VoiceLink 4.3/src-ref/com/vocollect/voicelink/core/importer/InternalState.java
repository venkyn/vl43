/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

/**
 * 
 * @author astein
 *
 */
public class InternalState implements Comparable<InternalState> { // POD class

    /**
     * 
     */
    public enum FinalStateType {
        NotPermittedAsFinalState, Success, Imperfect, NoWork, Failed
    }

    private int            code;

    private String         description;

    private FinalStateType typeOfState;

    /**
     * 
     * @param code specifies the integer code
     * @param description specifies the String description
     * @param myType specifies the FinalStateType object
     */
    InternalState(int code, String description, FinalStateType myType) {
        this.code = code;
        this.description = description;
        this.typeOfState = myType;
    }

    /**
     * 
     * @param code specifies the integer code
     * @param description specifies the String description
     */
    InternalState(int code, String description) {
        this.code = code;
        this.description = description;
        this.typeOfState = FinalStateType.NotPermittedAsFinalState;
    }

    /**
     * @param o the Object to compare this InternalState object to
     * @return the result as an integer 
     */
    public int compareTo(InternalState o) {
        return this.code - o.code;
    }

    /**
     * @return a String representation of the description of the InternalState object
     */
    public String toString() {
        return this.description;
    }

    /**
     * 
     * @return true if object is in a final state
     */
    public boolean isFinalState() {
        if (this.typeOfState != FinalStateType.NotPermittedAsFinalState) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @return true if object is in a failed state
     */
    public boolean isFailedState() {
        return (this.typeOfState == FinalStateType.Failed);
    }

    /**
     * 
     * @return true if object is in a success state
     */
    public boolean isSuccessState() {
        return this.typeOfState == FinalStateType.Success;
    }

    /**
     * 
     * @return true if object is in an imperfect state
     */
    public boolean isImperfectState() {
        return this.typeOfState == FinalStateType.Imperfect;
    }

    /**
     * 
     * @return true if object is in a no work state
     */
    public boolean isNoWorkState() {
        return this.typeOfState == FinalStateType.NoWork;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the typeOfState
     */
    public FinalStateType getTypeOfState() {
        return typeOfState;
    }

    /**
     * @param typeOfState the typeOfState to set
     */
    public void setTypeOfState(FinalStateType typeOfState) {
        this.typeOfState = typeOfState;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 