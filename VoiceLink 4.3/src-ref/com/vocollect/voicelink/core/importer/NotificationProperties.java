/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.model.NotificationPriority;

/**
 * This class holds the notification properties information.  
 * @author vsubramani
 *
 */
public class NotificationProperties {

    //Error Number associated with the notification.
    private String errorNumber;

    //Brief description of the notification.
    private String message;

    //Priority of the notification.
    private NotificationPriority priority;

    /**
     * 
     * @param errorNumber error number associated with the notification
     * @param message brief description of the notification
     * @param priority the priority of the notification.
     */
    NotificationProperties(String errorNumber, String message, NotificationPriority priority) {
        this.errorNumber = errorNumber;
        this.message = message;
        this.priority = priority;
    }

    /**
     * Get the Error Number.
     * @return errorNumber
     */
    public String getErrorNumber() {
        return errorNumber;
    }

    /**
     * Sets the Error Number.
     * @param errorNumber specifies the String object to set
     */
    public void setErrorNumber(String errorNumber) {
        this.errorNumber = errorNumber;
    }

    /**
     * Get the message.
     * @return message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the message.
     * @param message specifies the String object to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get the priority.
     * @return priority
     */
    public NotificationPriority getPriority() {
        return priority;
    }

    /**
     * Set the priority.
     * @param priority specifies the String object to set
     */
    public void setPriority(NotificationPriority priority) {
        this.priority = priority;
    }

    /**
     * Sets errorNumber, message and priority as a group.
     * @param newErrorNumber error number
     * @param newMessage message
     * @param newPriority priority
     */
    public void setProperties(String newErrorNumber,
                              String newMessage,
                              NotificationPriority newPriority) {
        this.setErrorNumber(newErrorNumber);
        this.setMessage(newMessage);
        this.setPriority(newPriority);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 