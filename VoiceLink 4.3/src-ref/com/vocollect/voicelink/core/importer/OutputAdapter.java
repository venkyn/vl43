/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * This class is the interface to a destination of data, be it file, FTP socket,
 * database, or other.
 * It allows the caller to manipulate the data output a buffered set of characters.
 * Note that this is a character-oriented interface. We don't track bytes. 
 * We really can't, at this level. If we want to do that, we need access to much
 * lower-level functions of the stream
 * 
 * @author jtauberg
 *
 */
public abstract class OutputAdapter implements Describable {
    
    private static final Logger log                     = new Logger(
        OutputAdapter.class);

    private Description               description            = new Description();

    private BufferedWriter            internalDataDestination;

    // Most data Destinations will use buffered writers, so we will have a common
    // writer here

    // States this object may assume. This will generally be in the Configured
    // state, but may occasionally
    // enter the DataDestinationNotReady state.

    /**
     * Static member object set at an InternalState of "Uninitialized".
    *  UNINITIALIZEDSTATE : Before constructor state
     */
    public static final InternalState UNINITIALIZEDSTATE     = new InternalState(
                                                                     0,
                                                                     "OutputAdapter.UnInitialized");

    /**
     * Static member object set at an InternalState of "Initialized".
     * INITIALIZEDSTATE : After constructor state
     */
    public static final InternalState INITIALIZEDSTATE       = new InternalState(
                                                                     1,
                                                                     "OutputAdapter.Initialized");

    /**
     * Static member object set at an InternalState of "configured".
     * CONFIGUREDSTATE : After successful configuration
     */
    public static final InternalState CONFIGUREDSTATE        = new InternalState(
                                                                     2,
                                                                     "OutputAdapter.Configured");

    /**
     * Static member object set at an InternalState of "Misconfigured".
     * MISCONFIGUREDSTATE : After unsuccessful configuration
     */
    public static final InternalState MISCONFIGUREDSTATE     = new InternalState(
                                                                     3,
                                                                     "OutputAdapter.MisConfigured");


    /**
     * Static member object set at an InternalState of "destination not ready".
     * DATADESTINATIONNOTREADY : After unsuccessful open or other IO error
     */
    public static final InternalState DATADESTINATIONNOTREADY     = new InternalState(
                                                                     4,
                                                                     "OutputAdapter.DestinationNotReady");

    /**
     * Static member object set at an InternalState of "nowork".
     * NOWORK : After unsuccessful check of ready to export flag or other IO error
    */
    public static final InternalState NOWORK                 = new InternalState(
                                                                     5,
                                                                     "OutputAdapter.NoWork");

    // Current state...
    private InternalState             currentState           = UNINITIALIZEDSTATE;


    // Is the data destination open for writing?
    private boolean                   isOpen                 = false;

    
    // Number of characters written in this record.
    private long                      recordLength    = 0;


    // Count of records written.
    private long                      recordCount      = 0;

    
    // Name of the data Destination. For a file, this is the full filename
    private String                    dataDestinationName;
    
    //Encoding type used for input file
    private String                   encoding;

    /**
     * Default constructor. Set all vars to null or zero. Descendent needs to
     * manage state.
     * @throws VocollectException if initiated incorrectly
     */
    @SuppressWarnings("unused")
    public OutputAdapter() throws VocollectException {
        super();
        this.setInternalDataDestination(null);
        this.setRecordLength(0);
        this.setRecordCount(0);
        setOpen(false);
        this.setDataDestinationName(null);
        return;
    }
    
    /**
     * 
     */
    public void incrementRecordCount() {
        recordCount = recordCount + 1;
    }

    /**
     * @param increment the amount to increment by.
     */
    public void incrementRecordCount(long increment) {
        recordCount = recordCount + increment;
    }
    
    /**
     * 
     */
    public void incrementRecordLength() {
        recordLength = recordLength + 1;
    }

    /**
     * @param increment the amount to increment by.
     */
    public void incrementRecordLength(long increment) {
        recordLength = recordLength + increment;
    }

    
    /**
     * Write a new record, typically equates to writeLine, but may not. If a
     * hierarchical datatype, write the next outer-level record.
     *
     * @param writeRecord - the string (record) to be written.
     * @return true if succeeds
     */
    public boolean writeRecord(String writeRecord) {
        try {
            this.getInternalDataDestination().write(writeRecord);
            this.getInternalDataDestination().newLine();
            this.incrementRecordCount();
            this.setRecordLength(0);
        } catch (IOException e) {
            // can't write to the file!
            setCurrentState(OutputAdapter.DATADESTINATIONNOTREADY);
            return false;
        }
        return true;
    }
  
    
    
    
    // --------------------------------Abstract
    // Methods----------------------------------------//
    /**
     * Open the current data destination for writing.
     *  This may involve closing the 'current' destination.
     * @return true if successful.
     * @throws NoWorkException if unsuccessful
     */
    public abstract boolean openDestination() throws NoWorkException, VocollectException;
 
    
    /**
     * Close with a successful state.
     *  This may include things such as moving the file to a
     *  successfully-completed directory.
     * 
     * @return true if this operation is successful.
     */
    public abstract boolean closeOnSuccess();

    
    /**
     * Close with a Failrue state.
     *  This may include things such as moving the file to a
     *  failure directory or deleting the file.
     * @return true if this operation is successful.
     */
    public abstract boolean closeOnFailure();

    
    // --------------------------------End Abstract
    // Methods----------------------------------------//
    // -------------------------------Actions----------------------------------------------------//
    /**
     * Closes the internalDataSource.
     * 
     * @throws IOException if unsuccessful
     */
    public void close() throws IOException {
        if (this.internalDataDestination != null) {
            this.internalDataDestination.close();
            setOpen(false);
        }
    }

    // ---------------------------------------Getters and
    // Setters---------------------------------//
    /**
     * Get the current state...
     * 
     * @return the InternalState object
     */
    public InternalState getCurrentState() {
        return this.currentState;
    }

    /**
     * Sets the current state.
     * 
     * @param currentState
     *            to set.
     */
    protected void setCurrentState(InternalState currentState) {
        this.currentState = currentState;
    }

    /**
     * @return the InternalDataDestination as a BufferedWriter object.
     */
    protected BufferedWriter getInternalDataDestination() {
        return this.internalDataDestination;
    }

    /**
     * Set the internal data destination to the given buffered writer.
     * 
     * @param internalDataDestination specifies the BufferedWriter object
     */
    public void setInternalDataDestination(BufferedWriter internalDataDestination) {
        this.internalDataDestination = internalDataDestination;
    }

    /**
     * Check - is the data source open for reading?
     * 
     * @return true if it is open
     */
    public boolean isOpen() {
        return this.isOpen;
    }

    /**
     * @param isopen to set the data source open
     */
    protected void setOpen(boolean isopen) {
        this.isOpen = isopen;
    }
    
    /**
     * Getter for the recordCount property.
     * @return long value of the property
     */
    public long getRecordCount() {
        return this.recordCount;
    }

    
    /**
     * Setter for the recordCount property.
     * @param recordCount the new recordCount value
     */
    public void setRecordCount(long recordCount) {
        this.recordCount = recordCount;
    }

    
    /**
     * Getter for the recordLength property.
     * @return long value of the property
     */
    public long getRecordLength() {
        return this.recordLength;
    }

    
    /**
     * Setter for the recordLength property.
     * @param recordLength the new recordLength value
     */
    public void setRecordLength(long recordLength) {
        this.recordLength = recordLength;
    }

    /**
     * @return Returns the dataDestinationName.
     */
    protected String getDataDestinationName() {
        return this.dataDestinationName;
    }

    /**
     * @param dataDestinationName
     *            The dataDestinationName to set.
     */
    protected void setDataDestinationName(String dataDestinationName) {
        this.dataDestinationName = dataDestinationName;
    }

    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.util.Describable#getDescription()
     */
    public String getDescription() {
        return this.description.getDescription();
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.util.Describable#setDescription(java.lang.String)
     */
    public void setDescription(String description) {
        this.description.setDescription(description);
    }
    
    
    // --------------------------Delegated methods------------------//

    /**
     * @throws IOException .
     * @see java.io.BufferedWriter#flush()
     */
    public void flush() throws IOException {
        internalDataDestination.flush();
    }

    /**
     * @throws IOException .
     * @see java.io.BufferedWriter#newLine()
     */
    public void newLine() throws IOException {
        this.incrementRecordCount();
        this.setRecordLength(0);
        internalDataDestination.newLine();
    }

    /**
     * @param c char to write.
     * @throws IOException .
     * @see java.io.BufferedWriter#write(int)
     */
    public void write(int c) throws IOException {
        internalDataDestination.write(c);
        this.incrementRecordLength();
    }

    /**
     * @param cbuf char buffer to write.
     * @param off offset.
     * @param len length.
     * @throws IOException .
     * @see java.io.BufferedWriter#write(char[], int, int)
     */
    public void write(char[] cbuf, int off, int len) throws IOException {
        internalDataDestination.write(cbuf, off, len);
        this.incrementRecordLength(len);
    }

    /**
     * @param s String to write.
     * @param off offset.
     * @param len length.
     * @throws IOException .
     * @see java.io.BufferedWriter#write(java.lang.String, int, int)
     */
    public void write(String s, int off, int len) throws IOException {
        internalDataDestination.write(s, off, len);
        this.incrementRecordLength(len);
    }

    /**
     * @param csq charSequence.
     * @return OutputAdapter.
     * @throws IOException .
     * @see java.io.Writer#append(java.lang.CharSequence)
     */
    public OutputAdapter append(CharSequence csq) throws IOException {
        this.incrementRecordLength(csq.length());
        internalDataDestination.append(csq);
        return this; 
    }

    /**
     * @param c - char to write
     * @return OutputAdapter
     * @throws IOException .
     * @see java.io.Writer#append(char)
     */
    public OutputAdapter append(char c) throws IOException {
        this.incrementRecordLength();
        this.internalDataDestination.append(c);
        return this;
    }

    /**
     * @param csq - CharSequence to write
     * @param start - start char
     * @param end - end char
     * @return OutputAdapter
     * @throws IOException .
     * @see java.io.Writer#append(java.lang.CharSequence, int, int)
     */
    public OutputAdapter append(CharSequence csq, int start, int end) throws IOException {
        this.incrementRecordLength(end - start);
        internalDataDestination.append(csq, start, end);
        return this;
    }

    /**
     * @param cbuf char buffer to write.
     * @throws IOException .
     * @see java.io.Writer#write(char[])
     */
    public void write(char[] cbuf) throws IOException {
        this.incrementRecordLength(cbuf.length);
        internalDataDestination.write(cbuf);
    }

    /**
     * @param str String to write.
     * @throws IOException .
     * @see java.io.Writer#write(java.lang.String)
     */
    public void write(String str) throws IOException {
        this.incrementRecordLength(str.length());
        internalDataDestination.write(str);
    }
    
    /**
     * @return Returns the encoding.
     */
    public String getEncoding() {
        return this.encoding;
    }

    /**
     * @param encoding specify the type of encoding used for input file
     * @throws ConfigurationException if there is problem is setting the charset
     * encoding
     */
    public void setEncoding(String encoding) throws ConfigurationException {
        if (encoding == null || encoding == "") {
            //default to UTF-8
            this.encoding = EncodingSupport.DEFAULT_ENCODING;
        } else {   
            if (EncodingSupport.isEncodingSupported(encoding)) {
                this.encoding = encoding;   
            } else {
                log
                .error(
                        "VoiceLink does not support " + encoding + " character encoding." 
                        + " State is misconfigured, please verify the configuration",
                        OutputAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK);
                        setCurrentState(OutputAdapter.MISCONFIGUREDSTATE);
                throw new ConfigurationException(OutputAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK, 
                    new UserMessage(LocalizedMessage.formatMessage(
                        OutputAdapterError.ENCODING_NOT_SUPPORTED_BY_VOICELINK)));
            }

        }
    }    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 