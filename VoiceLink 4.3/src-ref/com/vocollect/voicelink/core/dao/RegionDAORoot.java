/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * Region Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface RegionDAORoot extends GenericDAO<Region> {
    
    /**
     * retrieves the region specified.
     * 
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    Region findRegionByNumber(int regionNumber) 
        throws DataAccessException;
    
    /**
     * @param functionType - task function type
     * @param workgroupId - workgroup value
     * @return list of authorized regions
     * @throws DataAccessException - database exceptions
     */
    List<Region> listAuthorized(TaskFunctionType functionType, Long workgroupId) 
        throws DataAccessException;
    
    /**
     * @param regionType - type of regions (selection, putaway, etc)
     * @return list of regions for the passed in region type, order by region name
     * @throws DataAccessException - any database exception
     */
    List<Region> listByTypeOrderByName(RegionType regionType)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param number - the number to look for
     * @param type - region type (selection, put away, etc)
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByNumber(int number, RegionType type)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the region to look for
     * @param type - region type (selection, put away, etc)
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByName(String name, RegionType type)
        throws DataAccessException;
    
    /**
     * Returns the list of selection regions that support automatic issuance.
     * @return list of regions
     * @throws DataAccessException - any database exception
     */
    List<Region> listSelectionResequenceRegions() throws DataAccessException;    

    /**
     * Returns the list of replenishment regions.
     * @return list of regions
     * @throws DataAccessException - any database exception
     */
    List<Region> listReplenishmentResequenceRegions() throws DataAccessException;    

    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return list of regions for the passed in region type, order by region
     *         number
     * @throws DataAccessException -
     *             any database exception
     */
    List<Region> listByTypeOrderByNumber(RegionType regionType)
            throws DataAccessException;

    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return average goal rate for the region type.
     * @throws DataAccessException -
     *             any database exception
     */
    Double avgGoalRateForType(RegionType regionType)
            throws DataAccessException;

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 