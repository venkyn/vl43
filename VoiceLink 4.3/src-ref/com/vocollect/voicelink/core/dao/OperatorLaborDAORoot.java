/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.puttostore.model.PtsOperatorLaborReport;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * OperatorLabor Data Access Object (DAO) interface.
 *
 * @author Dennis Doubleday
 */
public interface OperatorLaborDAORoot extends GenericDAO<OperatorLabor> {


    /**
     * Retrieves the open labor record for the given operator
     * or returns null is there is no open labor record for the operator.
     *
     * @param operatorId - the ID of the Operator
     * @return the requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
     OperatorLabor findOpenRecordByOperatorId(long operatorId) throws DataAccessException;


    /**
     * Retrieves the list of labor record for the given operator
     * or returns null is there are no labor records for the operator.
     *
     * @param operatorId - the ID of the Operator
     * @return the requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
     List<OperatorLabor> listAllRecordsByOperatorId(long operatorId)
                                                    throws DataAccessException;
     
     /**
      * Retrieves the list of closed break labor record for the given operator or
      * returns null is there are no closed break labor records for the operator in 
      * the time window.
      * 
      * @param operatorId - the ID of the Operator
      * @param timeWindowStartTime -  Start Time of time window filter
      * @param timeWindowEndTime -  End Time of time window filter
      * @return the requested Operator Labor record or null
      * @throws DataAccessException on any failure.
      */
     List<OperatorLabor> listAllClosedBreakRecordsByOperatorId(long operatorId,
                                                               Date timeWindowStartTime,
                                                               Date timeWindowEndTime)
         throws DataAccessException;     

    /**
     * Get all operator labor records associated with any of the specified
     * operators.
     * @param decorator - additional query instructions.
     * @param timeWindow - limits the amount of data retrieved.
     * @return the List of operator labor records.
     * @throws DataAccessException - database exceptions.
     */
     List<DataObject> listOperatorLabor(QueryDecorator decorator, Date timeWindow)
         throws DataAccessException;

    /**
     * Get the data for the operator actions report.
     * @param decorator extra instructions for the query
     * @return List of maps of the return values
     * @throws DataAccessException on any failure.
     */
     List<Map<String, Object>> listOperatorActions(QueryDecorator decorator)
     throws DataAccessException;

    /**
     * Gets all operator labor older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @return a list of assignments
     * @throws DataAccessException Database failure
     */
     public List<OperatorLabor> listOlderThan(QueryDecorator decorator, Date date)
                                              throws DataAccessException;

    /**
     * Query for operator break report.
     *
     * @param startTime - start of time range
     * @param endTime - end of time range
     * @param operatorId - operator to report on (null if all)
     * @param breakId - break to report on (null if all)
     * @param operator - any operator from the operatorId set. Null when
     *            operatorId set is empty
     * @param durationInMilliSeconds - minimum duration to look for in milli
     *            seconds
     * @return - list of break records
     * @throws DataAccessException - database exceptions
     */
    public List<Object[]> listBreaksLongerThan(Date startTime,
                                               Date endTime,
                                               Long durationInMilliSeconds,
                                               Set<String> operatorId,
                                               String breakId,
                                               String operator)
        throws DataAccessException;

    /**
     * Query for the operator performance report.
     * @param startTime - start of time window
     * @param endTime - end of time window
     * @param actionType - specific function to return.
     * @param region - region number.
     * @return - list of information needed for report.
     */
    public List<Object[]> listPerformanceCounts(Date startTime, Date endTime,
                                                OperatorLaborActionType actionType, Long region);

    /**
     *
     * @param operatorId - the selected operator
     * @param startDate - the start of the date range
     * @param endDate -  the end of the date range
     * @return - a list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorId, Date startDate, Date endDate)
                                                throws DataAccessException;


    /**
     *
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @return - list of operators who have labor records with the given
     *           action type and date range.
     * @throws DataAccessException - on db failure.
     */
    List<Operator> listAllOperatorsActionBetweenDates(Date startDate, Date endDate,
                                                      OperatorLaborActionType actionType)
                                                      throws DataAccessException;


    /**
     *
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @param regionNumber - ther requested region
     * @return - list of operators who have labor records with the given
     *           action type, region and date range.
     * @throws DataAccessException - on db failure.
     */
    List<Operator> listAllOperatorsActionRegionBetweenDates(Date startDate, Date endDate,
                                                            OperatorLaborActionType actionType,
                                                            Integer regionNumber)
                                                            throws DataAccessException;

    /**
     *
     * @param operatorId - the selected operator
     * @param startDate - the start of the date range
     * @param endDate -  the end of the date range
     * @return - a list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId, Date startDate, Date endDate)
                                                throws DataAccessException;

    /**
     * Query for PTS Operator Labor Report.
     * @param queryDecorator Additional case based where clause for the query
     * @param startDate Date type for the start date that a report runs against
     * @param endDate Date type for the end date that a report runs against
     * @return list of operator labor record objects
     * @throws DataAccessException - ob db failure
     */
    List<PtsOperatorLaborReport> listLaborRecordsForPtsOperatorLaborReport(QueryDecorator queryDecorator,
                                                                                      Date startDate,
                                                                                      Date endDate)
        throws DataAccessException;
    
    /**
     * @param operatorId -
     *            operator id
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return list of goal rates for the region type.
     * @throws DataAccessException -
     *             any database exception
     */
    List<Integer> listGoalRateForTypeAndOperator(Long operatorId, OperatorLaborActionType regionType)
            throws DataAccessException;
    

    /**
     * Method to get labor records by Operator id within a time window.
     * @param startTime - the window start time
     * @param endTime - the window end time
     * @return - List of operator labor records
     * @throws DataAccessException - any database exception
     */
    List<OperatorLabor> listLaborRecordInTimeWindow(Date startTime,
                                                                Date endTime)
        throws DataAccessException;
    
    // Methods in Data Aggregators
    // Used for operator required calculations and goal rate calculations
    
    //Operator DA
    
    /**
     * Method to get all operator's actual rate by region .
     * @return list of map returned.
     */
    List<Map<String, Object>> listOperatorActualRateByRegion();
    

    /**
     * Retrieves the list of closed break labor record or
     * returns null is there are no closed break labor records for the operators in 
     * the time window.
     * 
     * @param timeWindowStartTime -  Start Time of time window filter
     * @param timeWindowEndTime -  End Time of time window filter
     * @return the requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<OperatorLabor> listAllClosedBreakRecords(Date timeWindowStartTime,
                                                              Date timeWindowEndTime)
        throws DataAccessException;   
    
    /**
     * Method to get list of all open records.
     * @return list of open records
     * @throws DataAccessException - dae
     */
    List<OperatorLabor> listAllOpenRecords() throws DataAccessException;
    
    /**
     * Method to find the time when operator first time signed off after the specified break end time.
     * @param operatorId id of operator
     * @param breakEndTime end time of break
     * @return sign off time
     * @throws DataAccessException dae
     */
    Date minFirstSignOffTimeAfterBreak(Long operatorId, Date breakEndTime) throws DataAccessException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 