/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.BreakType;

import java.util.List;

/**
 * Break Type Data Access Object (DAO) interface.
 * 
 * @author Ed Stoll
 */
public interface BreakTypeDAORoot extends GenericDAO<BreakType> {
    
    /**
     * @return List of BreakTypes
     * @throws DataAccessException - on any error
     */
    List<BreakType> listAllOrderByNumber() throws DataAccessException;
    
    /**
     * @return the breaktype object that corresponds to the given number
     * 
     * @param breakNumber - the break number to find.
     * 
     * @throws DataAccessException - on any error
     */
    BreakType findByNumber(int breakNumber) throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param breakNumber - the breakNumber to look for
     * @throws DataAccessException - indicates database error
     * @return breaktype id or null
     */
    public Long uniquenessByNumber(int breakNumber)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the breaktype to look for
     * @throws DataAccessException - indicates database error
     * @return breaktype id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 