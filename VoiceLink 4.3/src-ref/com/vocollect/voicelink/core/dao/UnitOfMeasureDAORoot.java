/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.UnitOfMeasure;

import java.util.List;

/**
 * @author pkolonay
 *
 */
public interface UnitOfMeasureDAORoot extends GenericDAO<UnitOfMeasure> {

    /**
     * Returns the Long of the uon number, or null if not found.
     * @param name - the name of the uom to look for
     * @throws DataAccessException - indicates database error
     * @return reason code id or null
     */
    public Long uniquenessByName(String name) throws DataAccessException;
    
    
    
    /**
     * List all Units Of Measure.
     * This method should be used when selection UOMS for a Site..
     * 
     * @return - Units of Measure filtered by Site Context
     * @throws DataAccessException
     *             - Database Exception
     */
    public List<UnitOfMeasure> listUnitsOfMeasure()  throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 