/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.ItemImportData;

import java.util.List;

/**
 * Importable Item DAO - import an item from a table using customer data.
 * @author dgold
 *
 */
public interface ItemImportDataDAORoot extends GenericDAO<ItemImportData> {
    
    /**
     * retrieves the item by the number.
     * 
     * @param itemNumber - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Item findItemByNumber(String itemNumber) 
        throws DataAccessException;

    /**
     * Get the list of importable items by site name.
     * @param siteName name of the site to use
     * @return list of all items for the site.
     * @throws DataAccessException for database errors.
     */
    public List<ItemImportData> listItemBySiteName(String siteName) throws DataAccessException;
    
    /**
     * Update all ImportItemData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 