/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * DAO for TaskFunctions.
 *
 * @author mkoenig
 */
public interface TaskFunctionDAORoot extends GenericDAO<TaskFunction> {
    
    /**
     * Find task function by number.
     * 
     * @param type - type to find.
     * @return - returns task function
     * @throws DataAccessException - database exceptions
     */
    TaskFunction findByType(TaskFunctionType type) throws DataAccessException;

    /**
     * @param workgroupId - workgroup value
     * @return list of task functions
     * @throws DataAccessException - database exceptions
     */
    List<TaskFunction> listFunctionsByWorkgroup(Long workgroupId) 
        throws DataAccessException;

    /**
     * @return all taskfunctions order by decsending functionNumber. 
     * @throws DataAccessException - database exception
     */
    List<TaskFunction> listAllOrderByNumberDesc() 
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 