/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.core.model.Location;

import java.util.List;


/**
 * Location Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface LocationDAORoot extends GenericDAO<Location> {
    
    /**
     * retrieves the location of the specified location.
     * 
     * @param locationID - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Location findLocationFromID(long locationID) 
        throws DataAccessException;
    
    /**
     * retrieves the location by the specified scanned verification.
     * 
     * @param scannedVerification - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    Location findLocationByScanned(String scannedVerification) 
        throws DataAccessException;
    
    /**
     * retrieves the location scanned verification 
     * for auto complete feature of loading dock doors.
     * 
     * @param scannedVerification - value to retrieve by
     * @param queryDecorator - Used to restrict results returned
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    List<String> listLocationsAutoComplete(QueryDecorator queryDecorator, String scannedVerification) 
        throws DataAccessException;    
    
    /**
     * retrieves the location by the specified spoken verification and check digits.
     * 
     * @param spokenVerification - value to retrieve by
     * @param checkDigits - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    Location findLocationBySpoken(String spokenVerification, String checkDigits) 
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param scannedVerification - the item number to look for
     * @throws DataAccessException - indicates database error
     * @return location id or null
     */
    public Long uniquenessByScannedVerification(String scannedVerification)
        throws DataAccessException;    


    /**
     * This query finds all locations that have the same spokenVerification and
     * check digits that has a different scannedVerification and returns those locations.
     * It is used for a location import validation.
     * 
     * @param spokenVerification - spokenVerification to find. 
     * @param checkDigits - checkDigits of location to find. 
     * @param scannedVerification - scannedVerification not to find. 
     * @return - matching location.
     */
    Location findLocationImportBlockingCondition(String spokenVerification, 
                                    String checkDigits,
                                    String scannedVerification);    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 