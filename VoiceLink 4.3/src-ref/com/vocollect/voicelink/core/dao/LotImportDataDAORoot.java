/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.core.model.LotImportData;

import java.util.List;

/**
 * Importable Lot DAO - import an lot from a table using customer data.
 * @author mnichols
 *
 */
public interface LotImportDataDAORoot extends GenericDAO<LotImportData> {
    
    /**
     * retrieves the lot by the number.
     * 
     * @param lotNumber - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Lot findLotByNumber(String lotNumber) 
        throws DataAccessException;

    /**
     * Get the list of importable lots by site name.
     * @param siteName name of the site to use
     * @return list of all lots for the site.
     * @throws DataAccessException for database errors.
     */
    public List<LotImportData> listLotBySiteName(String siteName) throws DataAccessException;
    
    /**
     * Update all ImportLotData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 