/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.LocationItem;

import java.util.List;


/**
 * Location/Item mapping Data Access Object (DAO) interface.
 * 
 */
public interface LocationItemDAORoot extends GenericDAO<LocationItem> {
    
    /**
     * Get the ItemLocation mapping by item number and location-scannedVerification.
     * @param itemNumber itemNumber
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public LocationItem findReplenishmentByItemAndLoc(String itemNumber, String scannedLocation)
        throws DataAccessException;

    /**
     * Get the ItemLocation mapping by location-scannedVerification.
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public List<LocationItem> listReplenishmentByLocation(String scannedLocation)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 