/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorImportData;

import java.util.List;


/**
 ** Importable Operator DAO - import Operator from a table using customer data.
 *
 * @author jtauberg
 */
public interface OperatorImportDataDAORoot extends GenericDAO<OperatorImportData> {
    
    /**
     * Retrieves an operator by ID string, or returns null is there is no such
     * operator.
     *
     * @param operatorId - the ID of the Operator
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    Operator findByIdentifier(String operatorId) throws DataAccessException;

    
    /**
     * Get the list of importable Operators by site name.
     * @param siteName name of the site to use
     * @return list of all Operators for the site.
     * @throws DataAccessException for database errors.
     */
    public List<OperatorImportData> listOperatorBySiteName(String siteName)
        throws DataAccessException;
    
    /**
     * Update all OperatorImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 