/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Printer;

import java.util.List;

/**
 * Printer Data Access Object (DAO) interface.
 * 
 * @author Ed Stoll
 */
public interface PrinterDAORoot extends GenericDAO<Printer> {
    
    /**
     * @return List of Printers
     * @throws DataAccessException - on any error
     */
    List<Printer> listAllOrderByNumber() throws DataAccessException;
    
    
    /**
     * @return the Printer object that corresponds to the given number
     * 
     * @param printerNumber - the Printer Number number to find.
     * 
     * @throws DataAccessException - on any error
     */
    Printer findByNumber(int printerNumber) throws DataAccessException;
    
    /**
     * @return the Printer object that corresponds to the given number
     * 
     * @param printerName - the Printer name to find.
     * 
     * @throws DataAccessException - on any error
     */
    Printer findByName(String printerName) throws DataAccessException;
    
    /**
     * @return list of enabled printers
     * @throws DataAccessException on any database exception
     */
    List<Printer> listAllEnabledPrinters() throws DataAccessException;

    /**
     * @return max printer number in database
     * @throws DataAccessException on any database exception
     */
    Number maxNumber() throws DataAccessException;
    
    /**
     * Returns the Long of the id, or null if not found.
     * @param number - the number of the printer to look for
     * @throws DataAccessException - indicates database error
     * @return printer id or null
     */
    public Long uniquenessByNumber(int number)
        throws DataAccessException;
    
    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the printer to look for
     * @throws DataAccessException - indicates database error
     * @return printer id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 