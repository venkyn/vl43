/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Vehicle;

/**
 * 
 * DAO interface for Vehicle object
 * @author mraj
 *
 */
public interface VehicleDAORoot extends GenericDAO<Vehicle> {

    /**
     * Method to validate if the vehicle id requested exist or not
     * @param vehicleId - Id of the vehicle to be validated
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return true: if the vehicle belonging to vehicleType exist
     */
    Vehicle findVehicleByNumberAndType(String vehicleId,
                                       Integer vehicleTypeNumber)
        throws DataAccessException;
    
    /**
     * Method to validate if the spoken vehicle number requested exist or not
     * @param spokenVehicleNumber - spoken number of the vehicle to be validated
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return true: if the vehicle belonging to vehicleType exist
     */
    Vehicle findVehicleBySpokenNumberAndType(String spokenVehicleNumber,
                                       Integer vehicleTypeNumber)
        throws DataAccessException;
    
    /**
     * Method to fetch the default vehicle based on Vehicle Type
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return vehicle: Default Vehicle for the VehicleType.
     */
    Vehicle findDefaultVehicleByType(Integer vehicleTypeNumber)
        throws DataAccessException;    
    
    /**
     * Method to fetch all vehicles the Operator is associated with
     * @param operator - the operator
     * @return vehicle: fetched vehicle
     */
    Vehicle findVehicleByOperator(Operator operator) throws DataAccessException;   
    
    /**
     * Returns the id of the vehicle if found by the vehicle number
     * @param vehicleNumber the vehicle number to search
     * @return id or null
     */
    public Long uniquenessByNumber(String vehicleNumber) throws DataAccessException;
    
    /**
     * Returns the id of the vehicle if found by the spoken vehicle number
     * @param spokenVehicleNumber the vehicle number to search
     * @return id or null
     */
    public Long uniquenessBySpokenNumber(String spokenVehicleNumber) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 