/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingSetting;

import java.util.List;

/**
 * Root DAO for DeliveryLocationMappingSetting.
 * 
 * @author bnorthrop
 */
public interface DeliveryLocationMappingSettingDAORoot extends
    GenericDAO<DeliveryLocationMappingSetting> {

    /**
     * Find the mapping setting for the logged in site.
     * @return DeliveryLocationMappingSetting - for the current site.
     * @throws DataAccessException - any db exception.
     */
    public DeliveryLocationMappingSetting findMappingSetting()
        throws DataAccessException;

    /**
     * List all the mapping settings for a given site (should only be one).
     * @return List - of DeliveryLocationMappingSettings.
     * @throws DataAccessException - any db exception.
     */
    public List<DeliveryLocationMappingSetting> listMappingSettings()
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 