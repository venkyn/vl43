/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.List;


/**
 * Vehicle Type Data Access Object (DAO) interface.
 *
 * @author kudupi
 */
public interface VehicleTypeDAORoot extends GenericDAO<VehicleType> {
    
    /**
     * @return List of VehicleTypes
     * @throws DataAccessException - on any error
     */
    List<VehicleType> listAllVehicleTypesByNumber() throws DataAccessException;
    
    /**
     * Returns the id of the type if found by the number
     * @param number the type number to search
     * @return id or null
     */
    public Long uniquenessByNumber(Integer number) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 