/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.List;

/**
 * 
 * 
 * 
 * @author khazra
 */
public interface VehicleSafetyCheckDAORoot extends
    GenericDAO<VehicleSafetyCheck> {

  
    /**
     * 
     * @param typeNumber Vehicle type number
     * @return List of safety checks
     * @throws DataAccessException on error
     */
    List<VehicleSafetyCheck> listAllChecksByVehicleType(Integer typeNumber)
        throws DataAccessException;

    
    /**
     * Method to find safety check object by name and vehicle type
     * @param safetyCheck - the safety check
     * @param vehicleType - the vehicle type
     * @return - fetched safety check object
     * @throws DataAccessException
     */
    VehicleSafetyCheck findSafetyCheckByNameAndVehicleType(String safetyCheck,
                                                           VehicleType vehicleType)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 