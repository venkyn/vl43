/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;

import java.util.List;
import java.util.Map;

/**
 * Operator Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface OperatorDAORoot extends GenericDAO<Operator> {

    /**
     * Retrieves an operator by ID string, or returns null is there is no such
     * operator.
     * 
     * @param operatorId - the ID of the Operator
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    Operator findByIdentifier(String operatorId) throws DataAccessException;

    /**
     * Get a list of operators that are autorized for a specified region.
     * 
     * @param regionId - region to get operators for
     * @return - List of operators that are autorized
     * @throws DataAccessException - Database Exception
     */
    List<Operator> listAuthorizedForRegion(Long regionId)
        throws DataAccessException;

    /**
     * @param workgroup to filter the operator list by
     * @return list of operators assigned to the workgroup
     * @throws DataAccessException for any database exception
     */
    List<Operator> listForWorkgroup(Workgroup workgroup)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param operatorIdentifier - the operator identifier to look for
     * @throws DataAccessException - indicates database error
     * @return operator id or null
     */
    public Long uniquenessByOperatorIdentifier(String operatorIdentifier)
        throws DataAccessException;

    /**
     * Returns a list of operators who have permission to work in the
     * <code>regionId</code> and are not currently working in this region.
     * @param regionId to get operators who have permission to work in region
     * @return list of operators that have permission and are not in region
     * @throws DataAccessException for any database exception
     */
    List<Operator> listAvailableToWorkInRegion(Long regionId)
        throws DataAccessException;

    /**
     * Returns a list of operators that are currently working in the region.
     * @param regionId to get operators who are working in this region.
     * @return list of operators
     * @throws DataAccessException for any database exception
     */
    List<Operator> listCurrentlyInRegion(Long regionId)
        throws DataAccessException;

    /**
     * Returns a list of operators.
     * @return list of operators
     * @throws DataAccessException for any database exception
     */
    List<Operator> listAllOrderByName()
        throws DataAccessException;
    /**
     * Returns the Long of the id, or null if not found.
     * @param region - the region to count operators in.
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Number countNumberOfOperatorsSignedIn(ReplenishmentRegion region)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param region - the region to count operators in.
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Number countNumberOfOperatorsSignedIn(PutawayRegion region)
        throws DataAccessException;
    
    /**
     * Count the number of operators that are signed in for a given region.
     * @param region - the region query is based on
     * @throws DataAccessException - indicates database error
     * @return the operator count
     */
    public Number countNumberOfOperatorsSignedIn(Region region)
        throws DataAccessException;
    
    //Used in Region Data Aggregator
    
    /**
     * Method to get operators per region.
     * @return List- map of Region- operators
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listCurrentlyInAllRegion()
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 