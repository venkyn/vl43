/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;

import java.util.List;

/**
 * Root DAO fro DeliveryLocationMapping.
 * 
 * @author bnorthrop
 */
public interface DeliveryLocationMappingDAORoot extends
    GenericDAO<DeliveryLocationMapping> {

    /**
     * Find a DeliveryLocationMapping using all the fields of the mapping (i.e.
     * looking for duplicates).
     * 
     * @param mappingValue - the value (e.g. customer number, route, etc.)
     * @param mappingType - A type of mapping for delivery location.
     * @throws DataAccessException - a database exception.
     * @return Integer - number of mappings matching parameters.
     */
    public Number countNumberOfMappings(String mappingValue, DeliveryLocationMappingType mappingType)
        throws DataAccessException;

    /**
     * Find a delivery location given a mapping value (e.g. customer number,
     * route, etc.).
     * @param mappingValue - the value (e.g. customer number, route, etc.)
     * @param mappingType - A type of mapping for delivery location.
     * @throws DataAccessException - a database exception.
     * @return String - the delivery location or null if doesn't exist.
     */
    public DeliveryLocationMapping findDeliveryLocation(String mappingValue, DeliveryLocationMappingType mappingType)
        throws DataAccessException;
    
    /**
     * Get a list of all mappings.
     * @param mappingType - A type of mapping for delivery location.
     * @return list of DeliveryLocationMapping
     * @throws DataAccessException - a database excetpion.
     */
    public List<DeliveryLocationMapping> listMappings(DeliveryLocationMappingType mappingType) 
        throws DataAccessException;

    /**
     * Fetches the list of delivery mapping data for the given 
     * filter criteria and Mapping type
     * route, etc.).
     * @param qd - additional query instructions.
     * @throws DataAccessException - a database exception.
     * @return the List of delivery mapping for the given filter criteria and Mapping type.
     */
    public List<DataObject> listMappingsForFilter(QueryDecorator qd)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 