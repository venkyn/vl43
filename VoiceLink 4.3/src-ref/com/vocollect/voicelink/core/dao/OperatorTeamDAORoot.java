/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorTeam;

import java.util.List;
/**
 * 
 *
 * @author mnichols
 */
public interface OperatorTeamDAORoot extends GenericDAO<OperatorTeam> {

    /**
     * @return List of OperatorTeams
     * @throws DataAccessException - on any error
     */
    List<OperatorTeam> listAllOrderByName() throws DataAccessException;
    
    /**
     * @return the operatorTeam object that corresponds to the given name
     * 
     * @param teamName - the operator team to find.
     * 
     * @throws DataAccessException - on any error
     */
    OperatorTeam findByName(String teamName) throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the operator team to look for
     * @throws DataAccessException - indicates database error
     * @return operator team id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 