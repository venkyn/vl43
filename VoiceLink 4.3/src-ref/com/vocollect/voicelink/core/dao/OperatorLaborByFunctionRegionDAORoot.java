/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorLaborByFunctionRegion;

import java.util.Date;
import java.util.List;

/**
 * Labor Summary By Region Data Access Object (DAO) interface.
 * 
 * @author pfunyak
 */
public interface OperatorLaborByFunctionRegionDAORoot extends 
        GenericDAO<OperatorLaborByFunctionRegion> {
    
    
    /**
     * Return the labor summary statstics for operators.
     * 
     * @param timeWindow - time window filter value
     *
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
    List<Object[]> listOtherLaborByOperator(Date timeWindow) throws DataAccessException;
    
    
    /**
     * Return the labor summary statstics for operators by function and by region.
     *
     * @param timeWindow - time window filter value
     *
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
    List<Object[]> listLaborByFunctionRegion(Date timeWindow) throws DataAccessException;    
    
    
    /**
     * Return the labor summary statstics for operator where the region is null.
     * 
     * @param timeWindow - time window filter value
     *
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
    List<Object[]> listLaborByFunctionAndNullRegion(Date timeWindow) throws DataAccessException;
    

}



*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 