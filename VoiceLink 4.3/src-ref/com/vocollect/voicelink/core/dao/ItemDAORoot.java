/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Item;

/**
 * Item Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface ItemDAORoot extends GenericDAO<Item> {
    
    /**
     * retrieves the item by the number.
     * 
     * @param itemNumber - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Item findItemByNumber(String itemNumber) 
        throws DataAccessException;
 
    /**
     * Returns the Long of the id, or null if not found.
     * @param number - the item number to look for
     * @throws DataAccessException - indicates database error
     * @return item id or null
     */
    public Long uniquenessByNumber(String number)
        throws DataAccessException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 