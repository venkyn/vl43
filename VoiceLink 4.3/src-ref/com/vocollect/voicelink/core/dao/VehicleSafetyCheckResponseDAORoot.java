/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.Date;
import java.util.List;

/**
 * Vehicle safety check Response DAO interface.
 * @author mraj
 * 
 */
public interface VehicleSafetyCheckResponseDAORoot extends
    GenericDAO<VehicleSafetyCheckResponse> {

    /**
     * Method used to find list of safety check responses
     * @param operator - the operator who performed the checks
     * @param safetyChecks - the list of safety checks
     * @param sequence - sequence of current check
     * @param vehicle - vehicle to be inspected
     * @return list of found safety checks
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listIntermediateSafetyChecks(Operator operator,
                                                                       List<VehicleSafetyCheck> safetyChecks,
                                                                       Vehicle vehicle,
                                                                       Integer sequence)
        throws DataAccessException;
    
    /**
     * Method used to find an already existing Vehicle safety check response
     * object. This object has an initial state having
     * response=VehicleSafetyCheckResponseType.INITIAL. Only Objects having this
     * value are fetched
     * 
     * @param checkName - Safety check name to be fetched
     * @param operator - Operator performing the check
     * @return VehicleSafetyCheckResponse fetched from DB based on criteria
     * @throws DataAccessException - thrown when error encountered fetching
     *             response
     */
    public VehicleSafetyCheckResponse findVehicleSafetyCheckResponse(String checkName,
                                                                     Operator operator)
        throws DataAccessException;

    /**
     * Method used to fetch response objects by response type and operator.
     * 
     * @param operator - Operator performing the check
     * @param responseType - search criteria for responses
     * @return List of found VehicleSafetyCheckResponse objects
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listVehicleSafetyCheckResponseByOperatorAndResponse(Operator operator,
                                                                                                int responseType)
        throws DataAccessException;
    
    /**
     * Method to fetch most recent vehicle safety check responses of the given
     * operator. This method does NOT filter responses by checkResponse value
     * 
     * @param operator - Operator performing the check
     * @return List of found VehicleSafetyCheckResponse objects
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listPreviousResponsesByOperator(Operator operator)
        throws DataAccessException;

    /**
     * Method to fetch performed safety checks for the given vehicle type
     * 
     * @param vehicleType - the vehicle type
     * @return List of found VehicleSafetyCheckResponse objects
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listSafetyChecksByVehicleType(VehicleType vehicleType)
        throws DataAccessException;

    /**
     * Method to get count of vehicle safety check responses
     * @param vehicle - the vehicle
     * @return count of vehicle safety check response for given vehicle
     * @throws DataAccessException
     */
    public Number countVehiclSafetyCheckResponsePerVehicle(Vehicle vehicle) throws DataAccessException;
    
    /**
     * Method to get count of vehicle safety check responses associated to a given safety check
     * @param safetyCheck - the vehicle safety check object
     * @return count of vehicle safety check response for given vehicle safety check
     * @throws DataAccessException
     */
    public Number countVehiclSafetyCheckResponsePerSafetyCheck(VehicleSafetyCheck safetyCheck)
        throws DataAccessException;
    
    /**
     * Method to get all Vehicle safety check responses matching the criteria
     * @param queryDecorator - the query decorator
     * @param vehicleType - the vehicle type
     * @param vehicle - the vehicle
     * @param startDate - the start date
     * @param endDate - the end date
     * @return list of vehicle safety check responses matching the criteria
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listVehicleSafetyCheckResponseForReport(QueryDecorator queryDecorator,
                                                                                    Integer vehicleType,
                                                                                    String vehicle,
                                                                                    Date startDate,
                                                                                    Date endDate)
        throws DataAccessException;
    
    /**
     * Get max group Id number currently in use.
     * 
     * @return - maximum chase group Id in use
     * @throws DataAccessException - Database Exception
     */
    public Long maxGroupId() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 