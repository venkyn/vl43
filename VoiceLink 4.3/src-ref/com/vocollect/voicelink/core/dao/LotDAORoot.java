/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Lot;
import java.util.List;

/**
 * Lot Data Access Object (DAO) interface.
 * 
 * @author Matthew Nichols
 */

public interface LotDAORoot extends GenericDAO<Lot> {

    /**
     * @return List of Lots
     * @throws DataAccessException - on any error
     */
    List<Lot> listAllOrderByNumber() throws DataAccessException;
    
    /**
     * @return the lot object that corresponds to the given number
     * 
     * @param lotNumber - the break number to find.
     * 
     * @throws DataAccessException - on any error
     */
    Lot findLotByNumber(String lotNumber) throws DataAccessException;
    
    /**
     * Find lot for item and location(optional).
     * 
     * @param lotNumber - lot number to find.
     * @param itemNumber - item lot belongs to 
     * @param locationId - location lot is in (can be null) 
     * @return - lot number if found.
     */
    Lot findLotByNumberItemLocation(String lotNumber, 
                                    String itemNumber, 
                                    String locationId);

    /**
     * get count of lots.
     * 
     * @return - total lots in a site
     */
    Long countAll();
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 