/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Workgroup;

import java.util.List;

/**
 * Workgroup Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface WorkgroupDAORoot extends GenericDAO<Workgroup> {
    
    /**
     * Retrieves the default workgroup.
     *
     * @return the default workgroup, or null
     * @throws DataAccessException on any failure.
     */
    Workgroup findDefaultWorkgroup() throws DataAccessException;
    
    /**
     * 
     * @return the ID of the default workgroup.
     * @throws DataAccessException on a database failure.
     */
    Number maxDefaultWorkgroup() throws DataAccessException;

    /**
     * @return list of workgroups with autoAddRegions is true.
     * @throws DataAccessException on any database exception.
     */
    List<Workgroup> listAutoAddWorkgroups() throws DataAccessException;
    
    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the workgroup to look for
     * @throws DataAccessException - indicates database error
     * @return workgroup id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 