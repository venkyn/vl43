/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorLaborByRegion;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;

import java.util.Date;
import java.util.List;

/**
 * Labor Summary By Region Data Access Object (DAO) interface.
 * 
 * @author pfunyak
 */
public interface OperatorLaborByRegionDAORoot extends 
        GenericDAO<OperatorLaborByRegion> {
    
    /**
     * Return the labor summary statistics for all regions.
     * @param filterType - the filter type to summarize by. <br/>
     *      0 - Other <br/>
     *      1 - Selection <br/>
     *      2 - PutAway <br/>
     *      3 - Replenishment <br/>
     *      4 - LineLoading <br/>
     * @param regionType -  the region type to search.
     * @param timeWindow - the window to limit the search by.
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
     List<Object[]> listEmptyLaborSummaryByRegion(OperatorLaborFilterType filterType,
                                                  RegionType regionType,
                                                  Date timeWindow)
                                                  throws DataAccessException;
    
    /**
     * Return the labor summary statistics by action type and region.
     * @param filterType - the filter type to summarize by.
     * @param timeWindow - the window to limit the search by.
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
     List<Object[]> listLaborByRegionAndFilterType(OperatorLaborFilterType filterType,
                                                   Date timeWindow)
                                                   throws DataAccessException;
      
      
    /**
     * Return the labor summary statistics by action type and region.
     * @param filterType - the filter type to summarize by.
     * @param timeWindow - the window to limit the search by.
     * @return List of object arrays containing the aggregates.
     * @throws DataAccessException on any failure.
     */
     List<Object[]> listLaborByFilterTypeAndNullRegion(OperatorLaborFilterType filterType,
                                                       Date timeWindow)
                                                       throws DataAccessException;
}



*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 