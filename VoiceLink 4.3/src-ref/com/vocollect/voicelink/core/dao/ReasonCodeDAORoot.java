/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.ReasonCode;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * DAO for ReasonCodes.
 *
 * @author sfahnestock
 */
public interface ReasonCodeDAORoot extends GenericDAO<ReasonCode> {
    
    /**
     * Retrieves all reason codes for the specified task application.
     *
     * @param taskFunctionType - the task function to search by
     * @return the requested reason codes, or null
     * @throws DataAccessException on any failure.
     */
    List<ReasonCode> listByTaskFunctionType(TaskFunctionType taskFunctionType) throws DataAccessException;
    
    /**
     * Returns the Long of the reason number, or null if not found.
     * @param number - the number of the reason code to look for
     * @throws DataAccessException - indicates database error
     * @return reason code id or null
     */
    public Long uniquenessByNumber(int number) throws DataAccessException;

    
    /**
     * Retrieves an reason code object by number string, or returns null is 
     * there is no such reason code.
     *
     * @param reasonNumber - the number of the License
     * @return the requested reason code object, or null
     * @throws DataAccessException on any failure.
     */
    ReasonCode findByReasonNumber(int reasonNumber) throws DataAccessException;    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 