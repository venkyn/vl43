/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for core business. Although the
 * range for these error codes is 2000-2999, please use only 2000-2499 for
 * the predefined ones; 2500-2999 should be reserved for customization error
 * codes.
 * 
 * @author mkoenig
 */
public class CoreErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 2000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 2999;

    /** 
     * No error, just the base initialization.
     */
    public static final CoreErrorCodeRoot NO_ERROR
        = new CoreErrorCodeRoot();
    
    
    /** 
     * Operator is already signed off.
     */
    public static final CoreErrorCode OPERATOR_ALREADY_SIGNED_OFF
    = new CoreErrorCode(2000);
    
    
    /**
     * cannot modify open labor records.
     */
    public static final CoreErrorCode LABOR_CANNOT_MODIFY_OPEN_LABOR_RECORD
    = new CoreErrorCode(2001); 

    /**
     * cannot modify this type of labor record.
     */
    public static final CoreErrorCode LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE
    = new CoreErrorCode(2002); 
    
    /**
     * Did not find the requested Operator labor record.
     */
    public static final CoreErrorCode LABOR_DID_NOT_FIND_REQUESTED_RECORD
    = new CoreErrorCode(2003); 

    
    /**
     * Selected Assignment is a container assignment, but no containers
     * have been created.
     */
    public static final CoreErrorCode PRINT_ASSIGNMENT_DOESNOT_HAVE_CONTAINERS
    = new CoreErrorCode(2100);
    
    /**
     * There were no labels created to print.
     */
    public static final CoreErrorCode PRINT_ASSIGNMENT_NO_LABELS
    = new CoreErrorCode(2101);
    
    /**
     * Print Server does not know about this printer.
     */
    public static final CoreErrorCode PRINTSERVER_PRINTER_NOT_KNOWN
    = new CoreErrorCode(2102);

    
    
    

    /**
     * Item Location Import Erorr Already Exists.
     */
    public static final CoreErrorCode ITEM_LOCATION_ALREADY_EXISTS
    = new CoreErrorCode(2201);

    /**
     * Item Location Import Error item not defined.
     */
    public static final CoreErrorCode ITEM_LOCATION_ITEM_NOT_DEFINED
    = new CoreErrorCode(2202);
    
    /**
     * Item Location Import Error mapping does not exist.
     */
    public static final CoreErrorCode ITEM_LOCATION_DOES_NOT_EXISTS
    = new CoreErrorCode(2203);
    
    /**
     * Item Location replenishment action item must be specified.
     */
    public static final CoreErrorCode ITEM_LOCATION_ITEM_MUST_BE_DEFINED
    = new CoreErrorCode(2204);
    
    /**
     * Item Location replenishment action item not mapped.
     */
    public static final CoreErrorCode ITEM_LOCATION_ITEM_NOT_MAPPED
    = new CoreErrorCode(2205);
     
    /**
     * Import Export Setup could not be updated for new site.
     */
    public static final CoreErrorCode IMPORT_SETUP_ERROR_FOR_NEW_SITE
    = new CoreErrorCode(2206);
   
    /**
     * Import Export Setup could not be update for deleted site.
     */
    public static final CoreErrorCode IMPORT_SETUP_ERROR_FOR_DELETED_SITE
    = new CoreErrorCode(2207);
   
    // Trying to delete an item-location mapping that does not exist
    public static final CoreErrorCode DELETING_ITEM_LOCATION_DOES_NOT_EXIST
    = new CoreErrorCode(2208);
    
    // Trying to delete a location-item mapping using only the location
    public static final CoreErrorCode DELETING_ITEM_LOCATION_ITEM_NOT_DEFINED
    = new CoreErrorCode(2209);
    
    // Action R with a new mapping
    public static final CoreErrorCode ITEM_LOCATION_REPLEN_NEW_MAPPING
    = new CoreErrorCode(2210);
    
    // Trying to replen a location with no mapping, no item defined & there are no mappings for the location
    public static final CoreErrorCode ITEM_LOCATION_REPLEN_NO_MAPPING_NO_ITEM
    = new CoreErrorCode(2211);
    
    // Report cannot be found to print/render (in ReportPrintManager).
    public static final CoreErrorCode REPORT_NOT_FOUND = new CoreErrorCode(2212);

    // Printer number does not map to anything in app context reportPrinters.xml
    public static final CoreErrorCode PRINTER_NOT_FOUND = new CoreErrorCode(2213);
    
    // Lot is not invalidlot status for clearing invalid lot
    public static final CoreErrorCode ITEM_LOCATION_ITEM_NOT_INVALID_LOT = new CoreErrorCode(2214);
    
    // Lot is invalidlot status for clearing invalid lot
    public static final CoreErrorCode ITEM_LOCATION_ITEM_INVALID_LOT = new CoreErrorCode(2215);
    
    // Lot is reported as invalid
    public static final CoreErrorCode INVALID_LOT = new CoreErrorCode(2216);

    /**
     * Import Export Setup was trying to get all sites to do a refresh, but could
     *  not get the list of sites.
     */
    public static final CoreErrorCode IMPORT_SETUP_REFRESH_ALL_SITES_DB_ERROR
    = new CoreErrorCode(2217);
    
    //Vehicles exist for Vehicle type
    public static final CoreErrorCode VEHICLE_EXISTS_FOR_VEHICLETYPE = new CoreErrorCode(2218);
    
    //Vehicle safety checks exist for Vehicle type
    public static final CoreErrorCode SAFETYCHECK_EXISTS_FOR_VEHICLETYPE = new CoreErrorCode(2219);
    
    //Vehicles safety check responses exist for Vehicle type
    public static final CoreErrorCode SAFETYCHECK_RESPONSES_EXISTS_FOR_VEHICLETYPE = new CoreErrorCode(2220);
    
    //Default Vehicle cannot be deleted
    public static final CoreErrorCode DEFAULT_VEHICLE_DELETE_NOT_ALLOWED = new CoreErrorCode(2221);
    
    //Duplicate sequence for safety check
    public static final CoreErrorCode DUPLICATE_SEQUENCE_FOR_SAFETY_CHECK = new CoreErrorCode(2222);
    
    //========================================================================
    // Lot web service error codes
    // Site not found.
    public static final CoreErrorCode SITE_NOT_FOUND = new CoreErrorCode(2401);
    
    //Lot Not Found
    public static final CoreErrorCode LOT_NOT_FOUND = new CoreErrorCode(2402);

    //Item Not Found
    public static final CoreErrorCode ITEM_NOT_FOUND = new CoreErrorCode(2403);
    
    //Location Not Found
    public static final CoreErrorCode LOCATION_NOT_FOUND = new CoreErrorCode(2404);
    
    //Lot Number is empty
    public static final CoreErrorCode LOT_NUMBER_EMTPY = new CoreErrorCode(2405);

    //Lot Number to long
    public static final CoreErrorCode LOT_NUMBER_TO_LONG = new CoreErrorCode(2406);
    
    //Speakable Lot Number is empty
    public static final CoreErrorCode SPEAKABLE_LOT_NUMBER_EMTPY = new CoreErrorCode(2407);

    //Speakable Lot Number to long
    public static final CoreErrorCode SPEAKABLE_LOT_NUMBER_TO_LONG = new CoreErrorCode(2408);
    
    // Lot is invalidlot status for short setting to replenished
    public static final CoreErrorCode LOCATION_WITH_INVALID_LOT = new CoreErrorCode(2409);
    
    // The new end time is unparsable (probably a multi-byte language coming from a non-multi-byte OS)
    public static final CoreErrorCode UNPARSABLE_OPERATOR_END_TIME = new CoreErrorCode(2410);
    
    public static final CoreErrorCode DELIVERY_MAPPING_VALUES_NOT_FOUND = new CoreErrorCode(2411);
    
    public static final CoreErrorCode MAPPING_TYPE_NOT_DEFINED = new CoreErrorCode(2412);
    
    public static final CoreErrorCode DELIVERY_MAPPING_VALUES_ALREADY_EXIST = new CoreErrorCode(2413);
    
    public static final CoreErrorCode DELIVERY_MAPPING_VALUES_CANNOT_BE_SAVED = new CoreErrorCode(2414);
    
    public static final CoreErrorCode UPDATES_WITH_DUPLICATE_MAPPINGS_FOUND = new CoreErrorCode(2415);
    
    public static final CoreErrorCode PARAMETER_LENGTH_NOT_ACCEPTABLE = new CoreErrorCode(2416);
    /**
     * Constructor.
     */
    private CoreErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected CoreErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(CoreErrorCode.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 