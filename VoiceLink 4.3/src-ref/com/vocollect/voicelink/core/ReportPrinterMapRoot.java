/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.util.StringUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Printer map for defined printers that can be used for manifest reports in
 * line loading.
 * 
 * @author mkoenig
 */
public class ReportPrinterMapRoot {

    private Map<String, String> printers = new HashMap<String, String>();

    /**
     * Getter for the printers property.
     * @return Map&lt;Integer,String&gt; value of the property
     */
    public Map<String, String> getPrinters() {
        return printers;
    }

    /**
     * Setter for the printers property.
     * @param printers the new printers value
     */
    public void setPrinters(Map<String, String> printers) {
        this.printers = printers;
    }

    /**
     * Get the printer from the printer map given the key (i.e. the printer
     * number).
     * @param printerNumber - the mapped number of the printer.
     * @throws VocollectException - if printer not in the configuration.
     * @return String address/name of the printer
     */
    public String getPrinter(String printerNumber) throws VocollectException {

        validatePrinterNumber(printerNumber);

        String printerAddress = printers.get(printerNumber);

        if (printerAddress == null) {
            throw new VocollectException(CoreErrorCode.PRINTER_NOT_FOUND);
        }
        return printerAddress;
    }

    /**
     * Getter for the printerNumber as integer property.
     * @param printerNumber - the number of the printer, from the
     *            applicationContext-reportPrinters.xml file
     * @throws VocollectException - if the printer is not found.
     */
    private void validatePrinterNumber(String printerNumber)
        throws VocollectException {
        if (StringUtil.isNullOrEmpty(printerNumber)) {
            throw new VocollectException(CoreErrorCode.PRINTER_NOT_FOUND);
        } else {
            try {
                Integer.parseInt(printerNumber);
            } catch (NumberFormatException nfe) {
                throw new VocollectException(CoreErrorCode.PRINTER_NOT_FOUND);
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 