/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.Date;
import java.util.StringTokenizer;


/**
 * 
 *
 * @author estoll
 */
public abstract class PostNotificationCmdRoot extends BaseTaskCommand {
    
    private static final long serialVersionUID = 4297318879626968645L;

    public static final String PAIR_SEPARATOR = "=";

    // NOTE: This is injected via Spring -- this is not
    // a task command parameter
    private String          pairDelimiter = "|";
    
    // Task command parameters
    private String          messageKey;
    
    private String          priority;
    
    private String          detailPairs;
    
    // other class members
    private LOPArrayList    notificaitonDetails;

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;
    
    /**
     * Getter for the pairDelimiter property.
     * @return String value of the property
     */
    public String getPairDelimiter() {
        return this.pairDelimiter;
    }
    
    /**
     * Setter for the pairDelimiter property.
     * @param pairDelimiter the new pairDelimiter value
     */
    public void setPairDelimiter(String pairDelimiter) {
        this.pairDelimiter = pairDelimiter;
    }

    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }

    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }    
    
    /**
     * Getter for the notificaitonDetails property.
     * @return LOPArrayList value of the property
     */
    public LOPArrayList getNotificaitonDetails() {
        return this.notificaitonDetails;
    }
    
    /**
     * Setter for the notificaitonDetails property.
     * @param notificaitonDetails the new notificaitonDetails value
     */
    public void setNotificaitonDetails(LOPArrayList notificaitonDetails) {
        this.notificaitonDetails = notificaitonDetails;
    }


    /**
     * Getter for the messageKey property.
     * @return String value of the property
     */
    public String getMessageKey() {
        return this.messageKey;
    }

    
    /**
     * Setter for the messageKey property.
     * @param messageKey the new messageKey value
     */
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    
    /**
     * Getter for the priority property.
     * @return String value of the property
     */
    public String getPriority() {
        return this.priority;
    }

    
    /**
     * Setter for the priority property.
     * @param priority the new priority value
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    
    /**
     * Getter for the detailPairs property.
     * @return String value of the property
     */
    public String getDetailPairs() {
        return this.detailPairs;
    }

    
    /**
     * Setter for the detailPairs property.
     * @param detailPairs the new detailPairs value
     */
    public void setDetailPairs(String detailPairs) {
        this.detailPairs = detailPairs;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        populateNotificationDetails();
        
        postNotification();
        
        return getResponse();
    }

    /**
     * Post the information as a voicelink notification.
     * @throws Exception upon errors
     */
    protected void postNotification() throws Exception {
        NotificationPriority p = NotificationPriority.INFORMATION;
        p.fromValue(new Integer(getPriority()));
        
        if (p.equals(null)) {
            p = NotificationPriority.INFORMATION;
        }
        
        getVoicelinkNotificationUtil().createNotification(
            "notification.column.keyname.Process.Task", 
            getMessageKey(), 
            NotificationPriority.toEnum(new Integer(getPriority())), 
            new Date(), 
            "0",
            getNotificaitonDetails());
    }
    

    /**
     * Fill the notification details.
     * Command date
     * Terminal serial number
     * Operator Id
     * then parse the detailPairs 
     * The detailPairs are keyValue pairs delimited by a pipe.
     */
    protected void populateNotificationDetails() {
        setNotificaitonDetails(new LOPArrayList());
        getNotificaitonDetails().add("task.timeStamp", getCommandTime());
        getNotificaitonDetails().add("task.serialNumber", getSerialNumber());
        getNotificaitonDetails().add("task.operator", getOperatorId());

        StringTokenizer st = new StringTokenizer(getDetailPairs(), getPairDelimiter());
        
        while (st.hasMoreTokens()) {
            String keyValuePair = st.nextToken();
            
            String[] tokens = keyValuePair.split(PAIR_SEPARATOR);
            if (tokens.length == 2) {
                getNotificaitonDetails().add(tokens[0], tokens[1]);
            } else {
                // Key value pair is not properly defined - so
                // put the value in the notificaiton detail so
                // the task developer can see....
                getNotificaitonDetails().add("task.malformed.detail", keyValuePair);
            }
        }
        
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 