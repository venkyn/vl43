/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

/**
 * 
 *
 * @author sfahnestock
 */
public class VerifyLocationCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -3676653102659770003L;

    private int                 scanned; 
    
    private int                 startLocation;
    
    private String              locationToVerify;
    
    private String              checkDigits;
    
    private Location            returnLocation;
    
    private LocationManager     locationManager;
    
    /**
     * Getter for the scanned property.
     * @return int value of the property
     */
    public int getScanned() {
        return this.scanned;
    }

    /**
     * Setter for the scanned property.
     * @param scanned the new scanned value
     */
    public void setScanned(int scanned) {
        this.scanned = scanned;
    }
    
    /**
     * Getter for the checkDigits property.
     * @return String value of the property
     */
    public String getCheckDigits() {
        return this.checkDigits;
    }
    
    /**
     * Getter for the enum type checkDigits property.
     * @return Long value of the property
     */
    public Long getCheckDigitsLong() {
        return Long.parseLong(this.checkDigits);
    }

    /**
     * Setter for the checkDigits property.
     * @param checkDigits the new checkDigits value
     */
    public void setCheckDigits(String checkDigits) {
        this.checkDigits = checkDigits;
    }
    
    /**
     * Getter for the startLocation property.
     * @return int value of the property
     */
    public int getStartLocation() {
        return this.startLocation;
    }

    /**
     * Setter for the startLocation property.
     * @param startLocation the new startLocation value
     */
    public void setStartLocation(int startLocation) {
        this.startLocation = startLocation;
    }
    
    /**
     * Getter for the locationToVerify property.
     * @return String value of the property
     */
    public String getLocationToVerify() {
        return this.locationToVerify;
    }

    /**
     * Setter for the locationToVerify property.
     * @param locationToVerify the new locationToVerify value
     */
    public void setLocationToVerify(String locationToVerify) {
        this.locationToVerify = locationToVerify;
    }
    
    /**
     * Getter for the returnLocation property.
     * @return Location value of the property
     */
    public Location getReturnLocation() {
        return this.returnLocation;
    }

    /**
     * Setter for the returnLocation property.
     * @param returnLocation the new returnLocation value
     */
    public void setReturnLocation(Location returnLocation) {
        this.returnLocation = returnLocation;
    }
    
    /**
     * Getter for the locationManager property.
     * @return Location value of the property
     */
    public LocationManager getLocationManager() {
        return this.locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        getLocation();
        buildResponse();
        return getResponse();
    }
    
    /**
     * Gets the location to return.
     * 
     * @throws TaskCommandException - task exception
     * @throws DataAccessException - database exception
     */
    protected void getLocation() throws TaskCommandException, DataAccessException {
        if (getScanned() == 1) {
            setReturnLocation(getLocationManager().findLocationByScanned(getLocationToVerify()));
        } else {
            setReturnLocation(getLocationManager().findLocationBySpoken(getLocationToVerify(), getCheckDigits()));
        }
        
        //Verify the location
        verifyLocation();
        
        //Check for starting location and set operators last location
        setOperatorsLastLocation();
    }
    
    /**
     * Verifies the location exists.
     * 
     * @throws TaskCommandException - task exception
     */
    protected void verifyLocation() throws TaskCommandException {
        if (getReturnLocation() == null) {
            throw new TaskCommandException(TaskErrorCode.LOCATION_NOT_FOUND, getLocationToVerify());
        }
    }
    
    /**
     * Checks for start location and sets the operators last
     * location as necessary.
     * @throws DataAccessException - database exception
     */
    protected void setOperatorsLastLocation() throws DataAccessException {
        if (getStartLocation() == 1) {
            getOperator().setLastLocation(getReturnLocation());
        }
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }
    
    /**
     * Builds the response record.
     * 
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        record.put("locationNumber", getReturnLocation().getId());
        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 