/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.ReasonCode;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.ReasonCodeManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 *
 * @author sfahnestock
 */
public class GetReasonCodesCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -3676653102659770003L;

    private String              requests;
    
    private Integer             functionNumber;
    
    private ReasonCodeManager   reasonCodeManager;
    
    private List<ReasonCode>    returnCodes = new ArrayList<ReasonCode>();
    
    /**
     * Getter for the function number property.
     * @return Integer value of the property
     */
    public Integer getFunctionNumber() {
        return this.functionNumber;
    }

    /**
     * Setter for the function number property.
     * @param functionNumber the new functionNumber value
     */
    public void setFunctionNumber(Integer functionNumber) {
        this.functionNumber = functionNumber;
    }
    
    /**
     * Getter for the requests property.
     * @return String value of the property
     */
    public String getRequests() {
        return this.requests;
    }

    /**
     * Setter for the requests property.
     * @param requests the new requests value
     */
    public void setRequests(String requests) {
        this.requests = requests;
    }
    
    /**
     * Getter for the reasonCodeManager property.
     * @return ReasonCodeManager value of the property
     */
    public ReasonCodeManager getReasonCodeManager() {
        return this.reasonCodeManager;
    }

    /**
     * Setter for the reasonCodeManager property.
     * @param reasonCodeManager the new reasonCodeManager value
     */
    public void setReasonCodeManager(ReasonCodeManager reasonCodeManager) {
        this.reasonCodeManager = reasonCodeManager;
    }
    
    /**
     * Getter for the returnCodes property.
     * @return ReasonCode value of the property
     */
    public List<ReasonCode> getReturnCodes() {
        return this.returnCodes;
    }

    /**
     * Setter for the returnCodes property.
     * @param returnCodes the new returnCodes value
     */
    public void setReturnCodes(List<ReasonCode> returnCodes) {
        this.returnCodes = returnCodes;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        getReasonCodes();
        buildResponse();
        
        return getResponse();
    }
    
    /**
     * Checks to see if there are any reason codes in the system.
     * 
     * @throws DataAccessException - database exception
     */
    protected void getReasonCodes() throws DataAccessException {
        setReturnCodes(getReasonCodeManager().listByTaskFunctionType(TaskFunctionType.toEnum(getFunctionNumber())));
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        if (getReturnCodes().isEmpty()) {
            getResponse().addRecord(buildResponseRecord(null));
        } else {
            for (ReasonCode r : getReturnCodes()) {
                getResponse().addRecord(buildResponseRecord(r));
            }
        }
    }
    
    /**
     * Builds the response record.
     * 
     * @param r - the reason code to build the response with
     * @return record - the response record
     * @throws DataAccessException - database exceptions 
     */
    protected ResponseRecord buildResponseRecord(ReasonCode r) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        if (r != null) {
            record.put("reasonNumber", r.getReasonNumber());
            record.put("reasonDescription", 
                       translateUserData(r.getReasonDescription()));
        }
        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 