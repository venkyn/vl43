/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Device;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 *
 * @author mkoenig
 */
public class SignOnCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -1901159214069942094L;

    private String      password;

    private SignOffManager  signOffManager;

    private WorkgroupManager    workgroupManager;

    private LaborManager laborManager;



    /**
     * Getter for the password property.
     * @return String value of the property
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for the password property.
     * @param password the new password value
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for the signOffManager property.
     * @return SignOffManager value of the property
     */
    public SignOffManager getSignOffManager() {
        return signOffManager;
    }

    /**
     * Setter for the signOffManager property.
     * @param signOffManager the new signoffManager value
     */
    public void setSignOffManager(SignOffManager signOffManager) {
        this.signOffManager = signOffManager;
    }


    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }


    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }


    /**
     * Setter for the operatorLaborManager property.
     * @param laborManager the new operatorLaborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }


    /**
     * Main Execution of prTaskLUTCoreSignOn.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //Check is operator exists, if not create it
        createOperator();
        confirmPassword();
        checkSignedOnOtherTerminal();
        signOffOperator();
        assignOperatorToTerminal();
        signOnOperator();
        buildResponse();

        return getResponse();
    }



    /**
     * Checks if operator exists and if not creates new operator.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Buisness Rule Exception
     */
    protected void createOperator() throws DataAccessException, BusinessRuleException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        //Check is operator exists, if not create it
        if (getOperator() == null) {
            Operator operator = new Operator();
            operator.setOperatorIdentifier(getOperatorId());
            operator.setPassword(getPassword());
            Workgroup defaultGroup = getWorkgroupManager().getPrimaryDAO().findDefaultWorkgroup();
            if (defaultGroup != null) {
                operator.setWorkgroup(defaultGroup);
            }
            getOperatorManager().save(operator);
            setOperator(operator);
        }

        //Set Operators Site information to be the same as the terminals
        boolean resetOperatorTags = false;
        if ((getOperator().getTags() == null)
            || getOperator().getTags().isEmpty()) {
            resetOperatorTags = true;
        } else {
            siteContext.setSiteToCurrentSite(getOperator());
        }
        //If tags are not same or not set then set tags
        if (resetOperatorTags) {
            siteContext.setSite(getOperator(), siteContext.getSite(getTerminal()));
        }

    }

    /**
     * Confirm password operator spoke in.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     */
    protected void confirmPassword() throws DataAccessException, TaskCommandException {
        if (!getOperator().getPassword().equals(getPassword())) {
            throw new TaskCommandException(TaskErrorCode.INVALID_PASSWORD);
        }
    }

    /**
     * Check to see if the operator is currently signed on to
     * another terminal.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     */
    protected void checkSignedOnOtherTerminal()
    throws DataAccessException, TaskCommandException {
        //If operator is currently signed on, make sure they are not
        //on a different terminal
        if (getOperator().getStatus() == OperatorStatus.SignedOn) {
            for (Device term : getOperator().getTerminals()) {
                if (!term.getSerialNumber().equals(getSerialNumber())) {
                    throw new TaskCommandException(
                        TaskErrorCode.NOT_ALLOWED_MULTIPLE_TERMINALS,
                        term.getSerialNumber());
                }
            }
        }
    }

    /**
     * Makes sure operator is properly signed off before singing
     * them back in.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Business Rules Exceptions
     */
    protected void signOffOperator()
    throws DataAccessException, BusinessRuleException {
        //If Operator is currently signed in, sign operator off
        if (getOperator().getStatus() == OperatorStatus.SignedOn) {
            getSignOffManager().executeSignOffOperator(getOperator(), getCommandTime());
        }
    }

    /**
     * Assign the operator to the specified terminal.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - BusinessRuleException
     */
    protected void assignOperatorToTerminal() throws DataAccessException, BusinessRuleException {
        //Assign terminal

        //if ((getTerminal().getOperator() == null)
        //    || (!getTerminal().getOperator().equals(getOperator()))) {
            getOperator().addTerminal(getTerminal());
        //}
    }

    /**
     *
     * @throws DataAccessException - if failure.
     * @throws BusinessRuleException - if failure.
     *
     */
    protected void signOnOperator() throws DataAccessException, BusinessRuleException {

        this.getOperator().signOn(this.getCommandTime());
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                                                       this.getOperator(),
                                                       OperatorLaborActionType.SignOn);
    }


    /**
     * Build the response object to return to terminal.
     */
    protected void buildResponse() {
        //Create a return record, populate it, and add to response
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build a response record to add to the respons.
     *
     * @return - a response record
     */
    protected ResponseRecord buildResponseRecord() {
        //Build Response
        ResponseRecord record = new TaskResponseRecord();

        //Create a return record, populate it, and add to response
        //customerName,operatorId,confirmPassword
        record.put("interleave", 0);

        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 