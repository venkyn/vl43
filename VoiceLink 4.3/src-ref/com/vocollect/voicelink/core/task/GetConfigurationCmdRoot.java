/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.voicelink.core.model.Device;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.Locale;


/**
 *
 *
 * @author mkoenig
 */
public class GetConfigurationCmdRoot extends BaseTaskCommand {

    public static final String NEW_OPERATOR_CONFIRM_PASSWORD = "2";

    private static final long serialVersionUID = 551273254959741333L;

    private Locale locale;

    private String terminalSiteName;

    /**
     * Getter for the terminalSiteName property.
     * @return String value of the property
     */
    public String getTerminalSiteName() {
        return terminalSiteName;
    }



    /**
     * Setter for the terminalSiteName property.
     * @param terminalSiteName the new terminalSiteName value
     */
    public void setTerminalSiteName(String terminalSiteName) {
        this.terminalSiteName = terminalSiteName;
    }


    /**
     * Getter for the locale property.
     * @return String value of the property
     */
    public Locale getLocale() {
        return this.locale;
    }


    /**
     * Setter for the locale property.
     * @param locale the new locale value
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        buildResponse();

        return getResponse();
    }
    
    /**
     * Do standard validations and intitializations for all task commands.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exceptions
     */
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        createTerminal();
        setSiteContextForCommand();
        setResponseLocale();
        checkDuplicateCommandCall();
        checkTaskCompatibility();
    }

    /**
     * Check if terminal already exists and if note create it.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Buinsess Rule Exception
     * @throws TaskCommandException - Task Command Exceptions
     */
    protected void createTerminal()
    throws DataAccessException, BusinessRuleException, TaskCommandException {
        Site currentSite = null;

        //Find Site with name specified by task
        currentSite = getSiteContext().getSiteByName(getTerminalSiteName());

        //If not found refresh sites and try again
        if (currentSite == null) {
            getSiteContext().refreshSites();
            currentSite = getSiteContext().getSiteByName(getTerminalSiteName());
        }


        //Check if current site is found, and set context, otherwise throw error
        if (currentSite != null) {
            getSiteContext().setCurrentSite(currentSite);

        } else {
            throw new TaskCommandException(TaskErrorCode.SITE_UNDEFINED,
                getTerminalSiteName());
        }

        //If terminal does not exist create it.
        if (getTerminal() == null) {
            Device terminal = makeNewTerminal();
            terminal.setTaskLocale(getLocale());
            terminal.setTaskVersion(getTaskVersion());
            getTerminalManager().save(terminal);
            setTerminal(terminal);
        } else {

            // Just assign the Locale
            getTerminal().setTaskLocale(getLocale());

            //Update terminals site
            getSiteContext().setSiteToCurrentSite(getTerminal());
            // Update terminal task version
            getTerminal().setTaskVersion(getTaskVersion());
        }

    }


    /**
     * Build response object to teturn to terminal.
     *
     * @throws DataAccessException - Database Exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build a response record to add to the respons.
     *
     * @return - a response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        //Get System Properties
        String customerName = "Vocollect";
        String operatorConfirmPassword = "0";


        //Get System properties
        SystemProperty sp = null;
        //Find customer name
        sp = getSystemPropertyManager().findByName("Company Name");
        if (sp != null) {
            customerName = sp.getValue();
        }

        //Find confirm password setting
        sp = getSystemPropertyManager().findByName("Operator Confirm Password");
        if (sp != null) {
            operatorConfirmPassword = sp.getValue();
        }

        //Operator doesn't currently exist so they
        //must confirm password the first time they sign in
        if (getOperator() == null) {
            operatorConfirmPassword = NEW_OPERATOR_CONFIRM_PASSWORD;
        }

        //Build Response
        ResponseRecord record;

        //Create a return record, populate it, and add to response
        //customerName,operatorId,confirmPassword
        record = new TaskResponseRecord();
        record.put("customerName", customerName);
        record.put("operatorId", getOperatorId());
        record.put("confirmPassword", operatorConfirmPassword);

        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 