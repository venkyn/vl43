/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleTypeManager;
import com.vocollect.voicelink.task.command.BaseCoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * Vehicle Type Command Implementation.
 * 
 * @author Ed Stoll
 */
public class ValidVehicleTypesCmdRoot extends BaseCoreTaskCommand {

    private static final long serialVersionUID = 5597846158596979400L;

    private String taskId;

    private VehicleTypeManager vehicleTypeManager;

    /**
     * Getter for the taskId property.
     * 
     * @return String value of the property
     */
    public String getTaskId() {
        return this.taskId;
    }

    /**
     * Setter for the taskId property.
     * 
     * @param taskId the new taskId value
     */
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * @return vehicleTypeManager - the vehicleTypeManager
     */
    public VehicleTypeManager getVehicleTypeManager() {
        return vehicleTypeManager;
    }

    /**
     * @param vehicleTypeManager
     */
    public void setVehicleTypeManager(VehicleTypeManager vehicleTypeManager) {
        this.vehicleTypeManager = vehicleTypeManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        buildResponse();

        return getResponse();
    }

    /**
     * Method to get all the vehicle types in sequence
     * @return list of fetched vehicle types
     * 
     * @throws DataAccessException
     */
    private List<VehicleType> getVehicleTypes() throws DataAccessException {
        List<VehicleType> vehicleTypes = getVehicleTypeManager()
            .listAllVehicleTypesByNumber();
        if (vehicleTypes.isEmpty()) {
            vehicleTypes.add(new VehicleType());
        }
        return vehicleTypes;
    }

    /**
     * Build empty response for VoiceLink.
     * @throws DataAccessException
     */
    protected void buildResponse() throws DataAccessException {
        for (VehicleType vt : getVehicleTypes()) {
            getResponse().addRecord(buildResponseRecord(vt));
        }
    }

    /**
     * Build a response record to add to the response.
     * @param vehicleType the vehicleType record to be added to response
     * @return response to task command
     */
    protected ResponseRecord buildResponseRecord(VehicleType vehicleType) {
        // Build Response
        ResponseRecord record = new TaskResponseRecord();

        // Create a return record, populate it, and add to response
        record.put("vehicleType", vehicleType.getNumber() == null
            ? "" : vehicleType.getNumber());
        record.put("vehicleDescription", vehicleType.getDescription() == null
            ? "" : vehicleType.getDescription());
        record.put("captureVehicleId", !vehicleType.getIsCaptureVehicleID()
            ? "0" : "1");

        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 