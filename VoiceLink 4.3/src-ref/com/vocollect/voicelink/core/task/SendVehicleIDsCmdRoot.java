/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.SafetyCheckType;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;
import com.vocollect.voicelink.task.command.BaseCoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * This is the Command class to handle Send Vehicle ID LUT request. Response of
 * this task command will be either an empty list if Vehicle ID is not sent and
 * will send list of safety checks for the VehicleType sent in the request
 * 
 * @author Ed Stoll
 * @author kudupi
 */
public class SendVehicleIDsCmdRoot extends BaseCoreTaskCommand {

    //
    private static final long serialVersionUID = 5597846158596979400L;

    private String vehicleType;

    private String vehicleID;
    
    private Vehicle vehicle = null;

    private VehicleManager vehicleManager;

    private VehicleSafetyCheckManager vehicleSafetyCheckManager;


    /**
     * Getter for the vehicleType property.
     * @return String value of the property
     */
    public String getVehicleType() {
        return this.vehicleType;
    }

    /**
     * Setter for the vehicleType property.
     * @param vehicleType the new vehicleType value
     */
    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    /**
     * Getter for the vehicleID property.
     * @return String value of the property
     */
    public String getVehicleID() {
        return this.vehicleID;
    }

    /**
     * Setter for the vehicleID property.
     * @param vehicleID the new vehicleID value
     */
    public void setVehicleID(String vehicleID) {
        this.vehicleID = vehicleID;
    }

    /**
     * Getter for the vehicleManager property.
     * @return VehicleManager value of the property
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    /**
     * Setter for the vehicleManager property.
     * @param vehicleManager the new vehicleManager value
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    /**
     * Getter for the vehicleSafetyCheckManager property.
     * @return VehicleSafetyCheckManager value of the property
     */
    public VehicleSafetyCheckManager getVehicleSafetyCheckManager() {
        return vehicleSafetyCheckManager;
    }

    /**
     * Setter for the vehicleSafetyCheckManager property.
     * @param vehicleSafetyCheckManager the new vehicleSafetyCheckManager value
     */
    public void setVehicleSafetyCheckManager(VehicleSafetyCheckManager vehicleSafetyCheckManager) {
        this.vehicleSafetyCheckManager = vehicleSafetyCheckManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        buildResponse();

        if (this.vehicle != null) {
            this.vehicle.getOperators().add(getOperator());
            getVehicleManager().save(this.vehicle);
        } else {
            throw new TaskCommandException(
                TaskErrorCode.VEHICLE_NOT_FOUND, this.vehicleID);
        }
        
        return getResponse();
    }

    /**
     * Build Safety Checks response.
     */
    protected void buildResponse() throws DataAccessException,
        BusinessRuleException {
        
        if (!StringUtil.isNullOrBlank(getVehicleID())) {
            vehicle = getVehicleManager().findVehicleBySpokenNumberAndType(
                vehicleID, Integer.parseInt(vehicleType));
        } else { // Fetch the default vehicle if Vehicle ID is blank.
            vehicle = getVehicleManager().findDefaultVehicleByType(
                Integer.parseInt(vehicleType));
        }

        List<VehicleSafetyCheck> listVehicleSafetyChecks = getVehicleSafetyCheckManager()
            .listAllCheckByVehicleType(Integer.parseInt(vehicleType));
        if (listVehicleSafetyChecks.isEmpty()) {
            //Vehicle types can exist w/o safety checks
            listVehicleSafetyChecks.add(new VehicleSafetyCheck());
        } 

        for (VehicleSafetyCheck safetyCheck : listVehicleSafetyChecks) {
            // Create a return record, populate it, and add to response
            getResponse().addRecord(buildResponseRecord(safetyCheck));
        }
    }

    /**
     * Build a response record to add to the response.
     * @param safetyCheck - VehicleSafetyCheck
     * @return - a response record
     */
    protected ResponseRecord buildResponseRecord(VehicleSafetyCheck safetyCheck) {
        // Build Response
        ResponseRecord record = new TaskResponseRecord();

        // Create a return record, populate it, and add to response
        record.put("safetyCheck", safetyCheck.getSpokenDescription() == null
            ? "" : safetyCheck.getSpokenDescription());
        
        if (safetyCheck.getResponseType() == SafetyCheckType.STOPONNO) {
            record.put("responseType", "B");
        } else if (safetyCheck.getResponseType() == SafetyCheckType.CONTINUEONNO) {
            record.put("responseType", "C");
        } else if (safetyCheck.getResponseType() == SafetyCheckType.NUMERIC) {
            record.put("responseType", "N");
        } else {
            record.put("responseType", "");
        }
        
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 