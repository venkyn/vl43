/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


import java.util.List;


/**
 * 
 *
 * @author 
 */
public class BreakTypeCmdRoot extends BaseTaskCommand {

    //
    private static final long serialVersionUID = -3676653102659770003L;

    private BreakTypeManager    breakTypeManager;
    
    
    
    
    /**
     * Getter for the breakTypeManager property.
     * @return BreakTypeManager value of the property
     */
    public BreakTypeManager getBreakTypeManager() {
        return breakTypeManager;
    }



    
    /**
     * Setter for the breakTypeManager property.
     * @param breakTypeManager the new breakTypeManager value
     */
    public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
        this.breakTypeManager = breakTypeManager;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
                
        buildResponse();
        
        return getResponse();
    }
    
    /**
     * Build response object to teturn to terminal.
     * 
     * @throws DataAccessException - Database Exception
     */
    protected void buildResponse() throws DataAccessException {
        List<BreakType> breakTypes = 
            getBreakTypeManager().listAllOrderByNumber();

        //Build Empty response if no break types
        if (breakTypes.isEmpty()) {
            getResponse().addRecord(buildResponseRecord(null));
        // Else build list of responses, 1 for each break type
        } else {
            for (BreakType bt : breakTypes) {
                //Create a return record, populate it, and add to response
                getResponse().addRecord(buildResponseRecord(bt));
            }
        }
    }
    
    /**
     * Build a response record.
     * 
     * @param breakType - break type to add to response, may be null
     * @return - return response record 
     * @throws DataAccessException - Database Exception
     */
    protected ResponseRecord buildResponseRecord(BreakType breakType) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        if (breakType != null) {
            record.put("breakDescription", 
                       translateUserData(breakType.getDescriptiveText()));
            record.put("breakNumber", breakType.getNumber());
        } else {
            record.put("breakDescription", "");
            record.put("breakNumber", "");
        }
        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 