/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 * 
 *
 * @author mkoenig
 */
public class SignOffCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -7505356186336690301L;
    
    private SignOffManager  signOffManager;
    
    

    
    /**
     * Getter for the signOffManager property.
     * @return SignOffManager value of the property
     */
    public SignOffManager getSignOffManager() {
        return signOffManager;
    }



    
    /**
     * Setter for the signOffManager property.
     * @param signOffManager the new signoffManager value
     */
    public void setSignOffManager(SignOffManager signOffManager) {
        this.signOffManager = signOffManager;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
 
        if (getOperator() != null) {
            if (getOperator().getStatus() == OperatorStatus.SignedOff) {
                throw new TaskCommandException(TaskErrorCode.OPERATOR_ALREADY_SIGNED_OFF);
            } else {
                getSignOffManager().executeSignOffOperator(getOperator(), getCommandTime());
            }
        }
        
        ResponseRecord record = new TaskResponseRecord();
        getResponse().addRecord(record);
        
        return getResponse();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 