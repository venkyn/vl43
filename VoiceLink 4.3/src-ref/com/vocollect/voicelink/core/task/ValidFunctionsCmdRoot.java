/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.task.command.BaseCoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 *
 * @author mkoenig
 */
public class ValidFunctionsCmdRoot extends BaseCoreTaskCommand {

    private static final Logger log = new Logger(ValidFunctionsCmd.class);
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private Integer                 taskId;
    
    private TaskFunctionManager     taskFunctionManager;
    
    private List<TaskFunction>      returnFunctions = new ArrayList<TaskFunction>();

    
    /**
     * Getter for the taskId property.
     * @return Integer value of the property
     */
    public Integer getTaskId() {
        return taskId;
    }

    
    /**
     * Setter for the taskId property.
     * @param taskId the new taskId value
     */
    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }
    
    /**
     * Getter for the returnFunctions property.
     * @return List value of the property
     */
    public List<TaskFunction> getReturnFunctions() {
        return returnFunctions;
    }

    
    /**
     * Setter for the returnFunctions property.
     * @param returnFunctions the new returnFunctions value
     */
    public void setReturnFunctions(List<TaskFunction> returnFunctions) {
        this.returnFunctions = returnFunctions;
    }
    
    /**
     * Getter for the taskFunctionManager property.
     * @return List value of the property
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    
    /**
     * Setter for the taskFunctionManager property.
     * @param taskFunctionManager the new taskFunctionManager value
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //Get a list of the valid functions
        getValidFunctions();
        //Build the response
        buildResponse();
        
        return getResponse();
    }
    
    /**
     * Gets a list of the operator's valid functions.
     * 
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     */
    protected void getValidFunctions() throws DataAccessException, TaskCommandException {
        //Get a list of all functions authorized for the operator's work group
        if (getOperator().getWorkgroup() != null) {
            setReturnFunctions(getTaskFunctionManager()
                .listFunctionsByWorkgroup(getOperator().getWorkgroup().getId()));
        }
        
        //Update the list to remove any functions not allowed by the task ID
        validateReturnFunctionsByTaskId();
        //Verify that the list is not empty
        validateReturnFunctionsNotEmpty();
        
    }
    
    /**
     * Validates that the return functions are not empty.
     * 
     * @throws TaskCommandException - task exception
     */
    protected void validateReturnFunctionsNotEmpty() throws TaskCommandException {
        if (getReturnFunctions().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_FOR_ANY_FUNCTIONS);
        }
    }
    
    /**
     * Validates the return functions by the task id.
     * 
     * @throws TaskCommandException - task exception
     */
    protected void validateReturnFunctionsByTaskId() throws TaskCommandException {
        if (getTaskId() != 0) {
            //Make a list of any functions in return list that need match task ID
            List<TaskFunction> functionsForType = new ArrayList<TaskFunction>();
            
            try {
                RegionType regionType = RegionType.toEnum(getTaskId());
                for (TaskFunction t : getReturnFunctions()) {
                    if ((t.getRegionType().equals(regionType))) {
                        functionsForType.add(t);
                    }
                }
            } catch (IllegalArgumentException e) {
                // An invalid task Id, doesn't convert to a region type.
                if (log.isDebugEnabled()) {
                    log.debug("User sent an invalid region type");
                }
            }
            
            //Make list the new return list
            setReturnFunctions(functionsForType);
        }
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (TaskFunction t : getReturnFunctions()) {
            getResponse().addRecord(buildResponseRecord(t));
        }
        
    }
    
    /**
     * Builds the response record.
     * 
     * @param t - task function to return
     * @return - return response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(TaskFunction t) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("functionNumber", t.getFunctionType().toValue());
        record.put("functionName", 
            ResourceUtil.getLocalizedEnumName(t.getFunctionType()));
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 