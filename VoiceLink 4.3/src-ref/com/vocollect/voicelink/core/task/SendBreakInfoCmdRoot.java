/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

/**
 * 
 *
 * @author sfahnestock
 */
public class SendBreakInfoCmdRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -3676653102659770003L;

    private int                     breakTypeSent;
    
    private String                  description;
    
    private int                     startEndFlag;
    
    private LaborManager            laborManager;
    
    private BreakTypeManager        breakTypeManager;
 
    /**
     * Getter for the breakTypeSent property.
     * @return String value of the property
     */
    public int getBreakTypeSent() {
        return this.breakTypeSent;
    }

    /**
     * Setter for the breakTypeSent property.
     * @param breakTypeSent the new breakTypeSent value
     */
    public void setBreakTypeSent(int breakTypeSent) {
        this.breakTypeSent = breakTypeSent;
    }
    
    
    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
     
    /**
     * Getter for the startEndFlag property.
     * @return int value of the property
     */
    public int getStartEndFlag() {
        return this.startEndFlag;
    }

    /**
     * Setter for the startEndFlag property.
     * @param startEndFlag the new startEndFlag value
     */
    public void setStartEndFlag(int startEndFlag) {
        this.startEndFlag = startEndFlag;
    }
    
  
    /**
     * Getter for the laborManager property.
     * @return laborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    
    /**
     * Setter for the operatorLaborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }
    
        
    /**
     * Getter for the breakTypeManager property.
     * @return BreakTypeManager value of the property
     */
    public BreakTypeManager getBreakTypeManager() {
        return this.breakTypeManager;
    }

    
    /**
     * Setter for the breakTypeManager property.
     * @param breakTypeManager the new breakTypeManager value
     */
    public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
        this.breakTypeManager = breakTypeManager;
    }

    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        
        // start break when startEndFlag == 0 
        if (getStartEndFlag() == 0) {
            // throw exception if break type object does not exist????
            BreakType breakType = this.getBreakTypeManager().findByNumber(this.getBreakTypeSent());
     
            this.getLaborManager().openBreakLaborRecord(this.getCommandTime(), 
                                                        this.getOperator(), breakType);
          } else { // end break when startEndFlag == 1
                this.getLaborManager().closeBreakLaborRecord(this.getCommandTime(),
                                                             this.getOperator());
        }
        ResponseRecord record = new TaskResponseRecord();
        getResponse().addRecord(record);
        return getResponse();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 