/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckRepairActionType;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;
import com.vocollect.voicelink.task.command.BaseCoreTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * This is the Command class to handle Send Safety Checklist LUT request. This
 * message is sent after the operator completes the safety checklist. The safety
 * checklist is complete when the operator has said, "Yes," to all checks or,
 * "No," to a single check.
 * 
 * @author kudupi
 */
public class SendSafetyChecklistCmdRoot extends BaseCoreTaskCommand {

    //
    private static final long serialVersionUID = -8690707232297119367L;

    private String checkResponse;

    private String failure;

    private String repairAction;

    private VehicleManager vehicleManager;
  
    private VehicleSafetyCheckManager vehicleSafetyCheckManager;
    
    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;

    /**
     * Getter for the checkResponse property.
     * @return String value of the property
     */
    public String getCheckResponse() {
        return checkResponse;
    }

    /**
     * Setter for the checkResponse property.
     * @param checkResponse the new checkResponse value
     */
    public void setCheckResponse(String checkResponse) {
        this.checkResponse = checkResponse;
    }

    
    
    /**
     * @return the vehicleManager
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    
    /**
     * @param vehicleManager the vehicleManager to set
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    
    /**
     * @return the vehicleSafetyCheckManager
     */
    public VehicleSafetyCheckManager getVehicleSafetyCheckManager() {
        return vehicleSafetyCheckManager;
    }

    
    /**
     * @param vehicleSafetyCheckManager the vehicleSafetyCheckManager to set
     */
    public void setVehicleSafetyCheckManager(VehicleSafetyCheckManager vehicleSafetyCheckManager) {
        this.vehicleSafetyCheckManager = vehicleSafetyCheckManager;
    }

    /**
     * Getter for the failure property.
     * @return String value of the property
     */
    public String getFailure() {
        return failure;
    }

    /**
     * Setter for the failure property.
     * @param failure the new failure value
     */
    public void setFailure(String failure) {
        this.failure = failure;
    }

    /**
     * Getter for the repairAction property.
     * @return String value of the property
     */
    public String getRepairAction() {
        return repairAction;
    }

    /**
     * Setter for the repairAction property.
     * @param repairAction the new repairAction value
     */
    public void setRepairAction(String repairAction) {
        this.repairAction = repairAction;
    }

    /**
     * Getter for the vehicleSafetyCheckResponseManager property.
     * @return VehicleSafetyCheckResponseManager value of the property
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }

    /**
     * Setter for the vehicleSafetyCheckResponseManager property.
     * @param vehicleSafetyCheckResponseManager the new
     *            vehicleSafetyCheckResponseManager value
     */
    public void setVehicleSafetyCheckResponseManager(
               VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        int repairActionVal = Integer.valueOf(getRepairAction());
        Vehicle vehicle = getVehicleManager().findVehicleByOperator(getOperator());
        List<VehicleSafetyCheck> safetyChecks = getVehicleSafetyCheckManager()
            .listAllCheckByVehicleType(vehicle.getVehicleType().getNumber()); 
        
        getVehicleSafetyCheckResponseManager().executeCheckResponse(
            getOperator(), getFailure(), repairActionVal, vehicle, safetyChecks, getCheckResponse(),
            getCommandTime());
        if (StringUtil.isNullOrEmpty(getFailure())
            || repairActionVal == VehicleSafetyCheckRepairActionType.NewEquipment.toValue()) {
            getVehicleSafetyCheckResponseManager().executeClearDefaultResponses(
                getOperator());
            getVehicleManager().deleteVehicleByOperator(getOperator());
        }
        
        ResponseRecord record = new TaskResponseRecord();
        getResponse().addRecord(record);

        return getResponse();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 