/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.VehicleTypeDAO;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for VehicleType-related operations.
 *
 * @author kudupi
 */
public interface VehicleTypeManagerRoot extends 
    GenericManager<VehicleType, VehicleTypeDAO>, DataProvider {
    
    /**
     * Retrieves all Vehicle types ordered by number, or returns null if there are none.
     *
     * @return all Vehicle Types ordered by number, or null
     * @throws DataAccessException on any failure.
     */
    List<VehicleType> listAllVehicleTypesByNumber() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 