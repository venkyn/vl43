/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.LocationDAO;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Location-related operations.
 * 
 * @author ddoubleday
 */
public interface LocationManagerRoot extends 
    GenericManager<Location, LocationDAO>, DataProvider {
    
    /**
     * retrieves the location of the specified location.
     * 
     * @param locationID - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Location findLocationFromID(long locationID) 
        throws DataAccessException;
    
    /**
     * retrieves the location by the specified scanned verification.
     * 
     * @param scannedVerification - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    Location findLocationByScanned(String scannedVerification) 
        throws DataAccessException;
    
    /**
     * retrieves the location scanned verification 
     * for auto complete feature of loading dock doors.
     * 
     * @param scannedVerification - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    List<String> listLocationsAutoComplete(String scannedVerification) 
        throws DataAccessException;
    
    /**
     * retrieves the location by the specified spoken verification and check digits.
     * 
     * @param spokenVerification - value to retrieve by
     * @param checkDigits - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    Location findLocationBySpoken(String spokenVerification, String checkDigits) 
        throws DataAccessException;

    /**
     * Report a short.
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void reportShort(Location location, Item item) 
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Wrapper method for Report a short to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void executeReportShort(Location location, Item item) 
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Trigger a replenishement.
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void triggerReplenishment(Location location, Item item)
    throws BusinessRuleException, DataAccessException;

    /**
     * Wrapper method for Trigger a replenishement to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void executeTriggerReplenishment(Location location, Item item)
    throws BusinessRuleException, DataAccessException;

    /**
     * Report Location/Item as Replenished.
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void reportReplenished(Location location, Item item)
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Wrapper method for Report Location/Item as Replenished to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void executeReportReplenished(Location location, Item item)
    throws BusinessRuleException, DataAccessException;
 
    /**
     * Wrapper method for Report Location/Item as Replenished to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - Database exception
     */
    public void executeClearInvalidLot(Location location, Item item)
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Report a replenishment was started for location.
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     */
    public void startReplenishment(Location location, Item item)
    throws BusinessRuleException;

    /**
     * Wrapper method for Report a replenishment was started for location
     * to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     */
    public void executeStartReplenishment(Location location, Item item)
    throws BusinessRuleException;

    /**
     * Report a replenishment has been completed. 
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     */
    public void completeReplenishment(Location location, Item item)
    throws BusinessRuleException;

    /**
     * Report a replenishment has been completed. 
     * Wrapper method for Report a replenishment has been completed
     * to enclose in transaction.
     * this needs done for proper error handling from the task
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     */
    public void executeCompleteReplenishment(Location location, Item item)
    throws BusinessRuleException;

    
    
    /**
     * This method is used in a location import validation.
     * It finds locations that have a different scannedVerification but have the
     * same combination of spokenVerification and checkDigits.  If one is found,
     * an import record should fail validation.
     * 
     * @param spokenVerification - spokenVerification to look for.
     * @param checkDigits - checkDigits to look for.
     * @param scannedVerification - scannedVerification (unique ID) not to find.
     * @return - location that blocks import, if any.
     * @throws DataAccessException -.
     */
    public Location findLocationImportBlockingCondition(
                                           String spokenVerification,
                                           String checkDigits,
                                           String scannedVerification)
                    throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 