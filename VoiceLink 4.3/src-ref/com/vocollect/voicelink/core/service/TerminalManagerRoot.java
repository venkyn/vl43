/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.TerminalDAO;
import com.vocollect.voicelink.core.model.Device;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Device-related operations.
 * 
 * @author ddoubleday
 */
public interface TerminalManagerRoot extends 
    GenericManager<Device, TerminalDAO>, DataProvider {

    /**
     * Retrieves a Device by serial number, or returns null is 
     * there is no such terminal in the system.
     *
     * @param serial - the serial number
     * @return the requested Device, or null
     * @throws DataAccessException on any failure.
     */
    Device findBySerialNumber(String serial) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 