/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

/**
 * This is the model object representing an Operator Labor Details Report.
 * 
 * @author someone
 */
public interface LaborDetailReportRoot {

    /**
     * @return the operator
     */
    public Operator getOperator();

    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator);

    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager();

    /**
     * @param operatorManager the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager);

    /**
     * @return the archiveAssignmentLaborManager
     */
    public ArchiveAssignmentLaborManager getArchiveAssignmentLaborManager();

    /**
     * @param archiveAssignmentLaborManager the archiveAssignmentLaborManager to
     *            set
     */
    public void setArchiveAssignmentLaborManager(ArchiveAssignmentLaborManager archiveAssignmentLaborManager);

    /**
     * @return the assignmentLaborManager
     */
    public AssignmentLaborManager getAssignmentLaborManager();

    /**
     * @param cycleCountingLaborManager The CycleCountingLaborManager to set
     */
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager);

    /**
     * @return the CycleCountingLaborManager
     */
    public CycleCountingLaborManager getCycleCountingLaborManager();

    /**
     * @param archiveCycleCountingLaborManager The
     *            ArchiveCycleCountingLaborManager to set
     */
    public void setArchiveCycleCountingLaborManager(ArchiveCycleCountingLaborManager archiveCycleCountingLaborManager);

    /**
     * @return The ArchiveCycleCountingLaborManager
     */
    public ArchiveCycleCountingLaborManager getArchiveCycleCountingLaborManager();

    /**
     * @param assignmentLaborManager the assignmentLaborManager to set
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager);

    /**
     * @return the operatorLaborManager
     */
    public OperatorLaborManager getOperatorLaborManager();

    /**
     * @param operatorLaborManager the operatorLaborManager to set
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager);

    /**
     * @return the replenishmentManager
     */
    public ReplenishmentManager getReplenishmentManager();

    /**
     * @param replenishmentManager the replenishmentManager to set
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager);

    /**
     * @param values Map of Parameter Values for the Jasper Reports Data Source
     * @return Jasper Report Data Source
     */
    public JRDataSource getDataSource(Map<String, Object> values);

    /**
     * @param arg0 The Jasper Reports Data Source to create
     * @return Jasper Reports Data Source return value
     * @throws JRException Jasper Reports Exception
     */
    public JRDataSource create(JasperReport arg0) throws JRException;

    /**
     * @param arg0 The Jasper Reports Data Source to destroy
     * @throws JRException Jasper Reports Exception
     */
    public void dispose(JRDataSource arg0) throws JRException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 