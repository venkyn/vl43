/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.RegionDAO;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Region-related operations.
 * 
 * @author ddoubleday
 */
public interface RegionManagerRoot extends 
    GenericManager<Region, RegionDAO>, DataProvider {

    /**
     * retrieves the region specified.
     * 
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    Region findRegionByNumber(int regionNumber) 
        throws DataAccessException;

    /**
     * @param functionType for the task function
     * @param workgroupId the operator's workgroup id
     * @return list of authorized regions
     * @throws DataAccessException - database exceptions
     */
    List<Region> listAuthorized(TaskFunctionType functionType, Long workgroupId)
        throws DataAccessException;
 
    /**
     * @param regionType - selection, putaway, etc.
     * @return list of regions for the passed in type, ordered by region name
     * @throws DataAccessException any database exception
     */
    List<Region> listByTypeOrderByName(RegionType regionType)
        throws DataAccessException;
    
    /**
     * @return list of selection regions for resequence(automatic issuance), 
     * ordered by region name
     * @throws DataAccessException any database exception
     */
    List<Region> listSelectionResequenceRegions()
        throws DataAccessException;    


    /**
     * @return list of replenishment regions for resequence(all), 
     * ordered by region name
     * @throws DataAccessException any database exception
     */
    List<Region> listReplenishmentResequenceRegions()
        throws DataAccessException;    

    /**
     * Implementation to check to see if the name and number are unique.
     * @param region the region to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    void verifyUniqueness(Region region) 
    throws BusinessRuleException, DataAccessException;
    
    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return list of regions for the passed in region type, order by region
     *         number
     * @throws DataAccessException -
     *             any database exception
     */
    List<Region> listByTypeOrderByNumber(RegionType regionType)
            throws DataAccessException;

    /**
     * @return list of Selection regions ordered by region name
     * @throws DataAccessException -
     *             any database exception
     */
    List<Region> getSelectionRegionsOrderByName()
            throws DataAccessException;
    
    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return average goal rate for type provided
     * @throws DataAccessException -
     *             any database exception
     */
    Double avgGoalRateForType(RegionType regionType)
            throws DataAccessException;

    /**
     * Utility method to get the user configured region intervals.
     * @param intervals - array of system default intervals
     * @return - interval array
     * @throws DataAccessException - thrown when error encountered in reading
     *             region interval configuration
     */
    Integer[] getUserConfiguredIntervals(Integer[] intervals) throws DataAccessException; 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 