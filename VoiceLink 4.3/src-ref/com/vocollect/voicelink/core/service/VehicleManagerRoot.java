/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.VehicleDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Vehicle;

/**
 * Business Service Interface to handle communication between web/task command
 * and persistence layer for Vehicle-related operations.
 * @author mraj
 * 
 */
public interface VehicleManagerRoot extends GenericManager<Vehicle, VehicleDAO>, DataProvider {

    /**
     * Method to validate if the vehicle id requested exist or not
     * @param vehicleNumber - number of the vehicle to be validated
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return true: if the vehicle belonging to vehicleType exist
     */
    Vehicle findVehicleByNumberAndType(String vehicleNumber,
                                       Integer vehicleTypeNumber)
        throws DataAccessException;
    
    /**
     * Method to delete Vehicle-Operator association upon sign-off
     * @param operator Operator getting signed off
     * @throws DataAccessException
     */
    void deleteVehicleByOperator(Operator operator) throws DataAccessException;
    
    /**
     * Method to validate if the spoken vehicle number requested exist or not
     * @param spokenVehicleNumber - spoken number of the vehicle to be validated
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return true: if the vehicle belonging to vehicleType exist
     */
    Vehicle findVehicleBySpokenNumberAndType(String spokenVehicleNumber,
                                       Integer vehicleTypeNumber)
        throws DataAccessException;
    
    /**
     * Method to fetch the vehicle based on Operator
     * @param operator - the operator
     * @return vehicle: fetched vehicle
     */
    Vehicle findVehicleByOperator(Operator operator) throws DataAccessException;   
    
    /**
     * Method to fetch the default vehicle based on Vehicle Type
     * @param vehicleTypeNumber - Id of the vehicle type to be validated
     * @return vehicle: Default Vehicle for the VehicleType.
     */
    Vehicle findDefaultVehicleByType(Integer vehicleTypeNumber)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 