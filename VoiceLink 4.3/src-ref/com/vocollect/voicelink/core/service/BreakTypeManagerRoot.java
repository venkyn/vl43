/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.BreakTypeDAO;
import com.vocollect.voicelink.core.model.BreakType;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for BreakType-related operations.
 * 
 * @author Ed Stoll
 */
public interface BreakTypeManagerRoot extends 
    GenericManager<BreakType, BreakTypeDAO>, DataProvider {

    /**
     * Retrieves all break types ordered by ID string, or returns null if there are none.
     *
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    List<BreakType> listAllOrderByNumber() throws DataAccessException;
    
        
    /**
     * Retrieves the break type object for the given break type number or
     * or returns null if the break type object is not found.
     * 
     * @param breakNumber - the requested break number
     *
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    BreakType findByNumber(int breakNumber) throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 