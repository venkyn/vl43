/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Operator-related operations.
 * 
 * @author ddoubleday
 */
public interface SignOffManagerRoot extends 
    GenericManager<Operator, OperatorDAO>, DataProvider {

    /**
     * Signs an operator off the VoiceLink System.
     *
     * @param operator - the Operator to sign off
     * @param signOffTime - Time operator is signing off
     * @throws DataAccessException on any failure.
     * @throws BusinessRuleException on any failure.
     */
    void executeSignOffOperator(Operator operator, Date signOffTime)    
    throws DataAccessException, BusinessRuleException;

    /**
     * Signs an operator off from the UI - also raises an
     * information level notification.
     * @param operator - the Operator to sign off.
     * @param signOffTime - Time operator is signing off
     * @throws DataAccessException on any database failure.
     * @throws BusinessRuleException on any businessrule exception.
     */
    void executeSignOffOperatorFromUI(Operator operator, Date signOffTime)
    throws DataAccessException, BusinessRuleException;
    
    
    /**
     * Returns the earliest possible sign off time for the given.
     * 
     * @param operator -  the operator to get the sign off time for.
     * @return Date - the earilest sign off date/time
     * @throws DataAccessException on any database failure.
     * @throws BusinessRuleException on any businessrule exception.
     */
    Date getEarliestSignOffTime(Operator operator) 
    throws DataAccessException, BusinessRuleException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 