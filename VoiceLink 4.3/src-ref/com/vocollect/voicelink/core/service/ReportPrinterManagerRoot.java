/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.exceptions.VocollectException;

import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

/**
 * Used for printing a Jasper report directly from a task. Specifically, this
 * will be used for manifest reports in line loading.
 * 
 * Note that in the future, this manager (and related entities) should be pulled
 * out into a more generic abstraction (probably located in EPP, or at least
 * VoiceLink core). In other words, printing reports straight from a task is
 * something that might be needed in other applications (e.g. Selection, etc.),
 * and should be globally available so not to redundantly implement this same
 * logic.
 * 
 * @author bnorthrop
 */
public interface ReportPrinterManagerRoot {

    /**
     * Print a report directly to the given printer number.
     * @param reportName - the name of the report (e.g. "PalletManifest.jrxml).
     * @param printerNumber - found in the
     *            applicationContext-voicelink-reportPrinters.xml
     * @param parameterValues - name value pairs of the parameters to be
     *            injected in the report.
     * @throws VocollectException - if any error occurs.
     */
    public void printReport(String reportName,
                            String printerNumber,
                            Map<String, String> parameterValues)
        throws VocollectException;
    
    /**
     * Print a report directly to the given printer number.
     * @param printerAddress - found in the
     *            applicationContext-voicelink-reportPrinters.xml
     * @param reportResult - JasperPrint value.
     * @throws VocollectException - if any error occurs.
     */
    public void printReport(String printerAddress, JasperPrint reportResult)
    throws VocollectException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 