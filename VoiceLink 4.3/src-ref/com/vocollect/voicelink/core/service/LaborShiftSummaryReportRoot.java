package com.vocollect.voicelink.core.service;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;

public interface LaborShiftSummaryReportRoot {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values);

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException;

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException;

    /**
     * @return the operatorLaborManager
     */
    public OperatorLaborManager getOperatorLaborManager();

    /**
     * @param operatorLaborManager the operatorLaborManager to set
     */
    public void setOperatorLaborManager(
            OperatorLaborManager operatorLaborManager);

    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager();

    /**
     * @param operatorManager the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager);

    /**
     * @return the archiveAssignmentLaborManager
     */
    public ArchiveAssignmentLaborManager getArchiveAssignmentLaborManager();

    /**
     * @param archiveAssignmentLaborManager the archiveAssignmentLaborManager to
     *            set
     */
    public void setArchiveAssignmentLaborManager(
            ArchiveAssignmentLaborManager archiveAssignmentLaborManager);

    /**
     * @return the assignmentLaborManager
     */
    public AssignmentLaborManager getAssignmentLaborManager();

    /**
     * @param assignmentLaborManager the assignmentLaborManager to set
     */
    public void setAssignmentLaborManager(
            AssignmentLaborManager assignmentLaborManager);

    /**
     * @return the replenishmentManager
     */
    public ReplenishmentManager getReplenishmentManager();

    /**
     * @param replenishmentManager the replenishmentManager to set
     */
    public void setReplenishmentManager(
            ReplenishmentManager replenishmentManager);

    /**
     * @return the replenishmentGoalRate
     */
    public String getReplenishmentGoalRate();

    /**
     * @param replenishmentGoalRate the replenishmentGoalRate to set
     */
    public void setReplenishmentGoalRate(String replenishmentGoalRate);

    /**
     * @return the regionManager
     */
    public RegionManager getRegionManager();

    /**
     * @param regionManager the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager);

}*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 