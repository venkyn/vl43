/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.LabelValuePair;
import com.vocollect.voicelink.core.model.Region;

import java.util.List;


/**
 * Interface for implementation of drop down lists.  
 */
public interface OperatorPerformanceReportRoot {

    /**
     * List of functions to choose from on parameter page.
     * @return - List of functions
     */
    public List<LabelValuePair> getFunctions();
    
    /**
     * List of sort order to choose from.
     * @return - List of sort orders
     */
    public List<LabelValuePair> getSortOrders();
    
    /**
     * @return - List of all regions
     * @throws DataAccessException
     */
    public List<Region> getRegions() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 