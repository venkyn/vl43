/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.WorkgroupDAO;
import com.vocollect.voicelink.core.model.Workgroup;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Operator-related operations.
 * 
 * @author ddoubleday
 */
public interface WorkgroupManagerRoot extends 
    GenericManager<Workgroup, WorkgroupDAO>, DataProvider {

    /**
     * Retrieves the default work group.
     *
     * @return the default work group, or null
     * @throws DataAccessException on any failure.
     */
    Workgroup findDefaultWorkgroup() throws DataAccessException;

    /**
     * @return list of workgroups where autoAddRegions is true.
     * @throws DataAccessException on database exceptions
     */
    List<Workgroup> listAutoAddWorkgroups() throws DataAccessException;

    /**
     * Creates and returns default workgroup.
     * 
     * @return default workgroup.
     * @throws DataAccessException on database exceptions
     * @throws BusinessRuleException on business rule violation
     */
    Workgroup createDefaultWorkgroup() throws DataAccessException, BusinessRuleException;
    
    /**
     * getter for the list of workgroups to be assigned to a operator.
     * @return list of Workgroups
     * @throws DataAccessException if any
     */
    List<Workgroup> getWorkgroupPerSite() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 