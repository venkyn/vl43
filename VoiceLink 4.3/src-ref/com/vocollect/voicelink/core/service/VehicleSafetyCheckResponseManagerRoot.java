/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.VehicleSafetyCheckResponseDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business class interface for VehicleSafetyCheckResponse
 * @author mraj
 * 
 */
public interface VehicleSafetyCheckResponseManagerRoot extends
    GenericManager<VehicleSafetyCheckResponse, VehicleSafetyCheckResponseDAO> {

    /**
     * Method to handle task commands for quick repair, numeric response and
     * final yes/no of safety check list for a vehicle
     * @param operator - Operator performing the checks
     * @param safetyCheck - the safety check name, if not null
     * @param repairAction - the repair action sent by task
     * @param safetyChecks - the safety checks of the vehicle type
     * @param vehicle - the vehicle
     * @param chekResponse - the check response sent by operator
     * @param checkDate - the time when check was performed
     * @return list of added/updated check responses
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> executeCheckResponse(Operator operator,
                                     String safetyCheck,
                                     int repairAction,
                                     Vehicle vehicle, List<VehicleSafetyCheck> safetyChecks, String chekResponse,
                                     Date checkDate) throws DataAccessException;
    
    /**
     * Method used to remove VSC responses with specified operator and having
     * response=-3 (Initial). This method is invoked to clear system of response
     * objects which were created for the vehicle whose inspection could not be
     * completed due to operator sign off from the system or device power off
     * 
     * @param operator - the Operator
     * @throws DataAccessException
     */
    public void executeClearDefaultResponses(Operator operator)
        throws DataAccessException;
    
    /**
     * Method to fetch all the vehicle safety check responses performed for
     * given vehicle type
     * 
     * @param vehicleType - the vehicle type
     * @return list of vehicle check responses 
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listSafetyChecksByVehicleType(VehicleType vehicleType)
        throws DataAccessException;
    
    /**
     * Method to get count of vehicle safety check responses
     * @param vehicle - the vehicle
     * @return count of vehicle safety check response for given vehicle
     * @throws DataAccessException
     */
    public Number countVehiclSafetyCheckResponsePerVehicle(Vehicle vehicle) throws DataAccessException;
    
    /**
     * Method to get count of vehicle safety check responses associated to a given safety check
     * @param safetyCheck - the vehicle safety check object
     * @return count of vehicle safety check response for given vehicle safety check
     * @throws DataAccessException
     */
    public Number countVehiclSafetyCheckResponsePerSafetyCheck(VehicleSafetyCheck safetyCheck)
        throws DataAccessException;
    
    
    /**
     * Method to get all Vehicle safety check responses matching the criteria
     * @param queryDecorator - the query decorator
     * @param vehicleType - the vehicle type
     * @param vehicle - the vehicle
     * @param operatorId - operators selected
     * @param startDate - the start date
     * @param endDate - the end date
     * @return list of vehicle safety check responses matching the criteria
     * @throws DataAccessException
     */
    public List<VehicleSafetyCheckResponse> listVehicleSafetyCheckResponseForReport(QueryDecorator queryDecorator,
                                                                                    Integer vehicleType,
                                                                                    String vehicle,
                                                                                    Set<String> operatorId,
                                                                                    Date startDate,
                                                                                    Date endDate)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 