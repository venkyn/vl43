/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.DeliveryLocationMappingSettingDAO;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingSetting;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;

/**
 * Manager for managing the mapping types for delivery locations. 
 * 
 * @see com.vocollect.voicelink.core.model.DeliveryLocationMappingType
 * @author Ben Northrop
 */
public interface DeliveryLocationMappingSettingManagerRoot
    extends
    GenericManager<DeliveryLocationMappingSetting, DeliveryLocationMappingSettingDAO>,
    DataProvider {

    /**
     * Retrieve the mapping setting for the current site, and if no mapping
     * setting is defined, then get the default.
     * @return DeliveryLocationMappingSetting - for the logged in site.
     * @throws DataAccessException - any db exception.
     */
    public DeliveryLocationMappingType getMappingType()
        throws DataAccessException;

    /**
     * Change the mapping setting to the given, but first make sure to delete
     * the existing.
     * 
     * @param instance - mapping setting to save.
     * @throws BusinessRuleException - if business rule violated.
     * @throws DataAccessException - any db exception.
     * @return Object - the saved instance.
     */
    public Object executeChangeMappingType(DeliveryLocationMappingType instance)
        throws BusinessRuleException, DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 