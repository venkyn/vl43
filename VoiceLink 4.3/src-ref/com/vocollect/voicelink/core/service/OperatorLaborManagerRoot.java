/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.dao.OperatorLaborDAO;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.impl.OperatorLaborManagerImplRoot.ActionTypesForReport;
import com.vocollect.voicelink.core.service.impl.OperatorLaborManagerImplRoot.ShiftHoursForReport;
import com.vocollect.voicelink.puttostore.model.PtsOperatorLaborReport;

import java.util.Date;
import java.util.List;
import java.util.Map;

import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Operator-related operations.
 * 
 * @author ddoubleday
 */
public interface OperatorLaborManagerRoot extends
    GenericManager<OperatorLabor, OperatorLaborDAO>, DataProvider {

    /**
     * Opens an Operator Labor record based on record type.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object
     * @param actionType - the Operator Labor Record type SignOn SignOff
     *            Selection PutAway Replenishment LineLoading
     * @throws DataAccessException for data access exceptions.
     * @throws BusinessRuleException for business rule exceptions.
     */
    void openLaborRecord(Date startTime,
                         Operator operator,
                         OperatorLaborActionType actionType)
        throws DataAccessException, BusinessRuleException;

    /**
     * Closes an Operator Labor record.
     * 
     * @param endTime - the end time of the labor record.
     * @param operator - the Operator object
     * 
     * @return OperatorLabor - the labor record that was just closed.
     * 
     * @throws DataAccessException for data access exceptions.
     * @throws BusinessRuleException for business rule exceptions.
     */
    OperatorLabor closeLaborRecord(Date endTime, Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * Opens an Operator Labor record based on record type.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object
     * @param breakType - BreakType object.
     * @param previousLabor - previously closed labor record.
     * 
     * @throws DataAccessException on any failure.
     * @throws BusinessRuleException for business rule exceptions.
     * 
     */
    void openBreakLaborRecord(Date startTime,
                              Operator operator,
                              BreakType breakType,
                              OperatorLabor previousLabor)
        throws DataAccessException, BusinessRuleException;

    /**
     * Close an Operator Break Labor record.
     * 
     * @param time - the start time of the labor record.
     * @param operator - the Operator object
     * 
     * @return OperatorLabor - the closed labor record.
     * 
     * @throws DataAccessException on any failure.
     * @throws BusinessRuleException for business rule exceptions.
     * 
     */
    OperatorBreakLabor closeBreakLaborRecord(Date time, Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * Retrieves the open OperatorLabor record by operator ID, or returns null
     * if no Labor record exists.
     * 
     * @param operatorId - the ID of the Operator
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    OperatorLabor findOpenRecordByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the list of OperatorLabor record by operator ID, or returns
     * null if no Labor record exists.
     * 
     * @param operatorId - the ID of the Operator
     * @return the list of Operator Labor Records, or null
     * @throws DataAccessException on any failure.
     */
    List<OperatorLabor> listAllRecordsByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the list of closed break labor record for the given operator or
     * returns null is there are no closed break labor records for the operator in 
     * the time window.
     * 
     * @param operatorId - the ID of the Operator
     * @param timeWindowStartTime -  Start Time of time window filter
     * @param timeWindowEndTime -  End Time of time window filter
     * @return the requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<OperatorLabor> listAllClosedBreakRecordsByOperatorId(long operatorId,
                                                              Date timeWindowStartTime,
                                                              Date timeWindowEndTime)
        throws DataAccessException;

    /**
     * Return the labor summary statstics for all operators.
     * 
     * @param rdi the Result Data Info object.
     * @return List of DataObjects
     * @throws DataAccessException on any failure.
     */
    List<DataObject> listLaborByOperator(ResultDataInfo rdi)
        throws DataAccessException;

    /**
     * Return the labor summary statistics for an operator by region.
     * @param rdi the Result Data Info object.
     * @return List of DataObjects
     * @throws DataAccessException on any failure.
     */
    List<DataObject> listLaborByRegionAndFilterType(ResultDataInfo rdi)
        throws DataAccessException;

    /**
     * Gets all operator labor older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @return a list of assignments
     * @throws DataAccessException Database failure
     */
    public List<OperatorLabor> listOlderThan(QueryDecorator decorator, Date date)
        throws DataAccessException;

    /**
     * Purges assignments based on date and status.
     * 
     * @param decorator - query decorator
     * @param olderThan - date to purge by
     * @return - number of assignments purged
     */
    public int executePurge(QueryDecorator decorator, Date olderThan);

    /**
     * Query for operator break report.
     * 
     * @param startTime - start of time range
     * @param endTime - end of time range
     * @param durationInMilliSeconds - minimum duration to look for
     * @param operatorId - Operator id(s) for selected by user
     * @param breakId - Break type id(s) for selected by user
     * @param operators - any operator from the operatorId set. Null when
     *            operatorId set is empty
     * @return - list of break records
     * @throws DataAccessException - database exceptions
     */
    public List<Object[]> listBreaksLongerThan(Date startTime,
                                               Date endTime,
                                               Long durationInMilliSeconds,
                                               Set<String> operatorId,
                                               String breakId,
                                               String operators)
        throws DataAccessException;

    /**
     * Query for the operator performance report.
     * @param startTime - start of time window
     * @param endTime - end of time window
     * @param actionType - specific function to return.
     * @param region - region number.
     * @return - list of information needed for report.
     */
    public List<Object[]> listPerformanceCounts(Date startTime,
                                                Date endTime,
                                                OperatorLaborActionType actionType,
                                                Long region);

    /**
     * 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @return - list of operators who have labor records with the given action
     *         type and date range.
     * @throws DataAccessException - on db failure.
     */
    List<Operator> listAllOperatorsActionBetweenDates(Date startDate,
                                                      Date endDate,
                                                      OperatorLaborActionType actionType)
        throws DataAccessException;

    /**
     * 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @param regionNumber - ther requested region
     * @return - list of operators who have labor records with the given action
     *         type, region and date range.
     * @throws DataAccessException - on db failure.
     */
    List<Operator> listAllOperatorsActionRegionBetweenDates(Date startDate,
                                                            Date endDate,
                                                            OperatorLaborActionType actionType,
                                                            Integer regionNumber)
        throws DataAccessException;

    /**
     * 
     * @param operatorId - operator used to get records
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of objects
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorId,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;

    /**
     * 
     * @param operatorId - operator used to get records
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of objects
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId,
                                                Date startDate,
                                                Date endDate)
        throws DataAccessException;

    /**
     * Query for PTS Operator Labor Report.
     * @param queryDecorator Additional query arguments
     * @param regionNumber The Region Number that the query executes against
     * @param operatorIdentifiers Operator Identifiers to execute against
     * @param startDate Start Date
     * @param endDate End Date
     * @return List of Operator Labor Records
     * @throws DataAccessException on db failure
     */
    List<PtsOperatorLaborReport> listLaborRecordsForPtsOperatorLaborReport(QueryDecorator queryDecorator,
                                                                           String regionNumber,
                                                                           Set<String> operatorIdentifiers,
                                                                           Date startDate,
                                                                           Date endDate)
        throws DataAccessException;

    /**
     * @return list of action types for reports
     */
    List<ActionTypesForReport> getActionTypesForReport();

    /**
     * @return list of shift start times for reports
     */
    List<ShiftHoursForReport> getShiftStartHoursForReport();

    /**
     * @return list of shift lengths for reports
     */
    List<ShiftHoursForReport> getShiftLengthHoursForReport();

    /**
     * @param operatorId - operator id
     * @param regionType - type of regions (selection, putaway, etc)
     * @return average goal rate for the region type.
     * @throws DataAccessException - any database exception
     */
    Double avgGoalRateForTypeAndOperator(Long operatorId,
                                         OperatorLaborActionType regionType)
        throws DataAccessException;

    
    // Methods in Data Aggregators
    // Used for operator required calculations and goal rate calculations
    
    //Operator DA
    /**
     * Method to get labor records by Operator id within a time window.
     * @param startTime - the window start time
     * @param endTime - the window end time
     * @return - List of operator labor records
     * @throws DataAccessException - any database exception
     */
    List<OperatorLabor> listLaborRecordInTimeWindow(Date startTime, Date endTime)
        throws DataAccessException;
    
    /**
     * Method to get all operator's actual rate by region .
     * @return list of map returned.
     */
    List<Map<String, Object>> listOperatorActualRateByRegion();
    
    /**
     * Retrieves the list of closed break labor record or
     * returns null is there are no closed break labor records for the operators in 
     * the time window.
     * 
     * @param timeWindowStartTime -  Start Time of time window filter
     * @param timeWindowEndTime -  End Time of time window filter
     * @return the requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<OperatorLabor> listAllClosedBreakRecords(Date timeWindowStartTime,
                                                              Date timeWindowEndTime)
        throws DataAccessException;
    
    /**
     * Method to get list of all open records.
     * @return list of open records
     * @throws DataAccessException - dae
     */
    List<OperatorLabor> listAllOpenRecords() throws DataAccessException;
    
    /**
     * Method to find the time when operator first time signed off after the specified break end time.
     * @param operatorId id of operator
     * @param breakEndTime end time of break
     * @return sign off time
     * @throws DataAccessException dae
     */
    Date getFirstSignOffTimeAfterBreak(Long operatorId, Date breakEndTime) throws DataAccessException; 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 