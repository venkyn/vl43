/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.VehicleSafetyCheckDAO;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleType;

import java.util.List;

/**
 * Business Service Interface to handle communication between user interface and
 * persistence layer for VehilceSafetyCheck-related operations.
 * 
 * @author khazra
 */
public interface VehicleSafetyCheckManagerRoot extends
    GenericManager<VehicleSafetyCheck, VehicleSafetyCheckDAO>, DataProvider {


    /**
     * @param typeNumber The type number
     * @return List of checks ordered in sequences
     * @throws DataAccessException
     */
    List<VehicleSafetyCheck> listAllCheckByVehicleType(Integer typeNumber)
        throws DataAccessException;
    
    
    /**
     * Method to find safety check object by name and vehicle type
     * @param safetyCheck - the safety check
     * @param vehicleType - the vehicle type
     * @return - fetched safety check object
     * @throws DataAccessException
     */
    VehicleSafetyCheck findSafetyCheckByNameAndVehicleType(String safetyCheck,
                                                           VehicleType vehicleType)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 