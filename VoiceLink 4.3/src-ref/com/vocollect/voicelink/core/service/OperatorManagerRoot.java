/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Workgroup;

import java.util.List;
import java.util.Map;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Operator-related operations.
 * 
 * @author ddoubleday
 */
public interface OperatorManagerRoot extends 
    GenericManager<Operator, OperatorDAO>, DataProvider {

    /**
     * Retrieves an operator by ID string, or returns null is there is no such
     * operator.
     *
     * @param operatorId - the ID of the Operator
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    Operator findByIdentifier(String operatorId) throws DataAccessException;

    /**
     * Get a list of operators that are autorized for a specified region.
     * 
     * @param regionId - region to get operators for
     * @return - List of operators that are autorized
     * @throws DataAccessException - Database Exception
     */
    List<Operator> listAuthorizedForRegion(Long regionId) 
    throws DataAccessException;

    /**
     * @param workgroup to filter the operators by
     * @return list of operators assigned to the workgroup
     * @throws DataAccessException for any database exception
     */
    List<Operator> listForWorkgroup(Workgroup workgroup)
    throws DataAccessException;

    
    /**
     * @param operator being worked on
     * @param workgroup to assign to the operator
     * @throws BusinessRuleException on rule exceptions
     * @throws DataAccessException on data failures
     */
    void executeChangeWorkgroup(Operator operator, Workgroup workgroup) 
        throws BusinessRuleException, DataAccessException;    
    
    /**
     * @param regionId to retrieve operators by
     * @return list of operators available to work in region
     * @throws DataAccessException for any database exceptions
     */
    List<Operator> listAvailableToWorkInRegion(Long regionId)
    throws DataAccessException;
    
    /**
     * @param rdi the constructed result data info object
     * @return list of operators available to work in region
     * @throws DataAccessException for any database exceptions
     */
    List<Operator> getAvailableToWorkInRegion(ResultDataInfo rdi) 
    throws DataAccessException;
    
    /**
     * @param regionId to retrieve operators by
     * @return list of operators currently working in region
     * @throws DataAccessException for any database exceptions
     */
    List<Operator> listCurrentlyInRegion(Long regionId)
    throws DataAccessException;
    
    /**
     * @return list of operators 
     * @throws DataAccessException for any database exceptions
     */    
    List<Operator> listAllOrderByName()
    throws DataAccessException;
    
    List<OperatorLaborActionType> getActionTypes();
    
    //Used in Region Data Aggregator
    
    /**
     * Method to get operators per region.
     * @return List- map of Region- operators
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listCurrentlyInAllRegion()
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 