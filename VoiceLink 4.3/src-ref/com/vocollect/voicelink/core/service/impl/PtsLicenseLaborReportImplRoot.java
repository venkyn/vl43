/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseLaborManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Implementation class for Put To Store License Labor Report.
 *
 * @author kudupi
 */
public class PtsLicenseLaborReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager {

    /**
     * PtsLicenseLaborManager is the manager that is used for
     * extracting transactional data for PTS License Labor Report.
     */
    private PtsLicenseLaborManager laborManager;

    /**
     * ArchivePtsLicenseLaborManager is the manager that is used for
     * extracting archive data for PTS License Labor Report.
     */
    private ArchivePtsLicenseLaborManager archiveLaborManager;

    /**
     * Getter for PtsLicenseLaborManager object.
     * @return the ptsLicenseLaborManager - PtsLicenseLaborManager object.
     */
    public PtsLicenseLaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for PtsLicenseLaborManager object.
     * @param laborManager - PtsLicenseLaborManager object.
     */
    public void setLaborManager(
            PtsLicenseLaborManager laborManager) {
        this.laborManager = laborManager;
    }

     /**
     * Getter for ArchivePtsLicenseLaborManager object.
     * @return archivePtsLicenseLaborManager - ArchivePtsLicenseLaborManager Object.
     */
    public ArchivePtsLicenseLaborManager getArchiveLaborManager() {
        return archiveLaborManager;
    }

    /**
     * Setter for ArchivePtsLicenseLaborManager object.
     * @param archiveLaborManager - ArchivePtsLicenseLaborManager object.
     */
    public void setArchiveLaborManager(
            ArchivePtsLicenseLaborManager archiveLaborManager) {
        this.archiveLaborManager = archiveLaborManager;
    }

    /**
     * Overriding the getDataSource method of ReportDataSourceManager
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values)
            throws Exception {

        // Capturing parameter values entered by the user.
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        String siteId = (String) values.get("SITE");

        Set<String> allOperatorIds = (Set<String>) values
        .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            allOperatorIds = null;
        }        

        //Get a list of report objects from the archive db
        List<PtsLicenseLaborReport> archiveList = getArchiveLaborManager()
        .listArchiveLicensesLaborForPtsLicenseLaborReport(new QueryDecorator(), allOperatorIds, 
            startTime, endTime, Long.parseLong(siteId));

        //Get a list of report objects from the transactional db
        List<PtsLicenseLaborReport> licenseList = getLaborManager()
        .listLicensesLaborForPtsLicenseLaborReport(new QueryDecorator(), allOperatorIds, 
            startTime, endTime, Long.parseLong(siteId));

        //Add the two lists together
        licenseList.addAll(archiveList);
        return new JRBeanCollectionDataSource(licenseList);        
    }

    /**
     * Constructor.
     * @param beanClass - class of bean report is based on
     */
    public PtsLicenseLaborReportImplRoot(Class<PtsLicenseLaborReport> beanClass) {
        super(PtsLicenseLaborReport.class);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<PtsLicenseLaborReport> reportList = new ArrayList<PtsLicenseLaborReport>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     */
    public PtsLicenseLaborReportImplRoot() {
        super(PtsLicenseLaborReport.class);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 