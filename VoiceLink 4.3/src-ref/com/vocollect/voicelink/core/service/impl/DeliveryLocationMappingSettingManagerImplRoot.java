/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.DeliveryLocationMappingSettingDAO;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingSetting;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager;

import java.util.Collection;

/**
 * The manager for the mapping settings, which define the mapping type used by a
 * particular site (for example, site 1 might use the customer number mapping
 * type but site 2 might use route).
 * 
 * @author bnorthrop
 */
public abstract class DeliveryLocationMappingSettingManagerImplRoot
    extends
    GenericManagerImpl<DeliveryLocationMappingSetting, DeliveryLocationMappingSettingDAO>
    implements DeliveryLocationMappingSettingManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public DeliveryLocationMappingSettingManagerImplRoot(DeliveryLocationMappingSettingDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager#get()
     */
    public DeliveryLocationMappingType getMappingType()
        throws DataAccessException {

        DeliveryLocationMappingSetting setting = getPrimaryDAO()
            .findMappingSetting();
        if (setting == null) {
            return DeliveryLocationMappingType.getDefault();
        }
        return setting.getMappingType();

    }

    /**
     * Private method for deleting the mapping setting for the current site.
     * @throws BusinessRuleException - if business rule violated.
     * @throws DataAccessException - any db exception.
     */
    private void delete() throws DataAccessException, BusinessRuleException {
        Collection<DeliveryLocationMappingSetting> settings = getPrimaryDAO()
            .listMappingSettings();
        for (DeliveryLocationMappingSetting setting : settings) {
            this.delete(setting);
        }
    }

    /**
     * This operation is not supported - use executeChange() instead, since a
     * site should have one and only one mapping setting.
     * 
     * @param instance - mapping setting to save.
     * @throws IllegalStateException - not supported.
     * @return Object - the saved instance.
     */
    @Override
    public Object save(DeliveryLocationMappingSetting instance)
        throws IllegalStateException {
        throw new IllegalStateException("save() not supported!");
    }

    /**
     * Change the mapping setting to the given, but first make sure to delete
     * the existing.
     * 
     * @param type - mapping setting to save.
     * @throws BusinessRuleException - if business rule violated.
     * @throws DataAccessException - any db exception.
     * @return Object - the saved instance.
     */
    public Object executeChangeMappingType(DeliveryLocationMappingType type)
        throws BusinessRuleException, DataAccessException {
        this.delete();
        DeliveryLocationMappingSetting setting = new DeliveryLocationMappingSetting(
            type);
        return super.save(setting);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 