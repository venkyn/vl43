/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.dao.RegionDAO;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Additional service methods for the <code>Region</code> model object.
 *
 * @author ddoubleday
 */
public abstract class RegionManagerImplRoot extends
    GenericManagerImpl<Region, RegionDAO> implements RegionManager {
    
    private SystemPropertyManager systemPropertyManager;
    
    /**
     * @return the systemPropertyManager
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    
    /**
     * @param systemPropertyManager the systemPropertyManager to set
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public RegionManagerImplRoot(RegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get specified region.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.RegionManager#findRegionByNumber(int)
     */
    public Region findRegionByNumber(int regionNumber) 
        throws DataAccessException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }
    /**
     * Implementation to get a list of regions for the function number and work group id.
     * @param functionType (the corresponding type for selection normal, putaway, etc)
     * @param workgroupId to search for the function number
     * @return List of authorized Regions
     * @throws DataAccessException database error
     */
    public List<Region> listAuthorized(TaskFunctionType functionType, Long workgroupId)
        throws DataAccessException {
        return getPrimaryDAO().listAuthorized(functionType, workgroupId);
    }
    
    /**
     * Implementation to get a list of regions of a specific type, ordered
     * by region name.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.RegionManager#listByTypeOrderByName(com.vocollect.voicelink.core.model.RegionType)
     */
    public List<Region> listByTypeOrderByName(RegionType regionType)
        throws DataAccessException {
        return getPrimaryDAO().listByTypeOrderByName(regionType);
    }
    
    /**
     * Implementation to get a list of selection regions for resequence(automatic issuance).
     * @return list of regions for resequence(automatic issuance), 
     * ordered by region name
     * @throws DataAccessException any database exception
     */
    public List<Region> listSelectionResequenceRegions() throws DataAccessException {
        return getPrimaryDAO().listSelectionResequenceRegions();
    }
    
    /**
     * Implementation to get a list of replenishment regions for resequence(all).
     * @return list of regions for resequence(all), 
     * ordered by region name
     * @throws DataAccessException any database exception
     */
    public List<Region> listReplenishmentResequenceRegions() throws DataAccessException {
        return getPrimaryDAO().listReplenishmentResequenceRegions();
    }    

    /**
     * Implementation to check to see if the name and number are unique.
     * @param region the region to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    public void verifyUniqueness(Region region) 
    throws BusinessRuleException, DataAccessException {
        boolean isNew = region.isNew();
        //Uniqueness check for name
        Long nameUniquenessId = getPrimaryDAO().
            uniquenessByName(region.getName(), region.getType());  
        if (nameUniquenessId != null && (isNew || (!isNew && nameUniquenessId.longValue() 
            != region.getId().longValue()))) {
            throw new FieldValidationException("region.name",
                region.getName(),
                new UserMessage("region.edit.error.duplicateName", region.getName()));
        }
        
        //Uniqueness check for number
        Long numberUniquenessId = getPrimaryDAO().
            uniquenessByNumber(region.getNumber(), region.getType());  
        if (numberUniquenessId != null && (isNew || (!isNew && numberUniquenessId.longValue() 
            != region.getId().longValue()))) {
            throw new FieldValidationException("region.number",
                region.getNumber().toString(),
                new UserMessage("region.edit.error.duplicateNumber", region.getNumber()));            
        }
    }    
    
    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return list of regions for the passed in region type, order by region
     *         number
     * @throws DataAccessException -
     *             any database exception
     */
    public List<Region> listByTypeOrderByNumber(RegionType regionType)
            throws DataAccessException {
        return this.getPrimaryDAO().listByTypeOrderByNumber(regionType);
    }

    /**
     * Implementation to get a list of Selection regions, ordered by region name.
     * @return - list of selection regions ordered by region name.
     * @throws DataAccessException -
     *             any database exception
     */
    public List<Region> getSelectionRegionsOrderByName()
        throws DataAccessException {
        ArrayList<Region> regions = new ArrayList<Region>();        
        regions.addAll(getPrimaryDAO().listByTypeOrderByName(RegionType.Selection));
         
         return regions;
    }
    
    /**
     * @param regionType -
     *            type of regions (selection, putaway, etc)
     * @return list of regions for the passed in region type, order by region
     *         number
     * @throws DataAccessException -
     *             any database exception
     */
    public Double avgGoalRateForType(RegionType regionType)
            throws DataAccessException {
        return this.getPrimaryDAO().avgGoalRateForType(regionType);
    }


    @Override
    public Integer[] getUserConfiguredIntervals(Integer[] defaultIntervals) throws DataAccessException {
        SystemProperty propertyRegionIntervalConfig = getSystemPropertyManager()
            .findByName("RegionInterval_Configuration");

        Integer[] intervals = null;
        if (propertyRegionIntervalConfig != null
            && !StringUtil.isNullOrBlank(propertyRegionIntervalConfig
                .getValue())) {
            
            String[] str = propertyRegionIntervalConfig.getValue().trim()
                .split(",");
            List<Integer> list = new ArrayList<Integer>(str.length);
            for (int i = 0; i < str.length; i++) {
                list.add(Integer.parseInt(str[i].trim()));
            }

            Collections.sort(list);
            intervals = list.toArray(new Integer[str.length]);
        }

        return intervals == null ? defaultIntervals : intervals;
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 