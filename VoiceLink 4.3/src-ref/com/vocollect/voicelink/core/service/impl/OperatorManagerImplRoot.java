/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.OperatorManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>Operator</code> model object.
 * 
 * @author ddoubleday
 */
public abstract class OperatorManagerImplRoot
    extends GenericManagerImpl<Operator, OperatorDAO>
    implements OperatorManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public OperatorManagerImplRoot(OperatorDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#findByIdentifier(java.lang.String)
     */
    public Operator findByIdentifier(String operatorId)
        throws DataAccessException {
        return getPrimaryDAO().findByIdentifier(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#listAuthorizedForRegion(java.lang.Long)
     */
    public List<Operator> listAuthorizedForRegion(Long regionId)
        throws DataAccessException {
        return getPrimaryDAO().listAuthorizedForRegion(regionId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#listForWorkgroup(com.vocollect.voicelink.core.model.Workgroup)
     */
    public List<Operator> listForWorkgroup(Workgroup workgroup)
        throws DataAccessException {
        return getPrimaryDAO().listForWorkgroup(workgroup);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#getAvailableToWorkInRegion(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<Operator> getAvailableToWorkInRegion(ResultDataInfo rdi) 
        throws DataAccessException {
        return getPrimaryDAO().listAvailableToWorkInRegion((Long) rdi.getQueryArgs()[0]);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManagerRoot#listAvailableToWorkInRegion(java.lang.Long)
     */
    public List<Operator> listAvailableToWorkInRegion(Long regionId)
        throws DataAccessException {
        return getPrimaryDAO().listAvailableToWorkInRegion(regionId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManagerRoot#listCurrentlyInRegion(java.lang.Long)
     */
    public List<Operator> listCurrentlyInRegion(Long regionId)
        throws DataAccessException {
        return getPrimaryDAO().listCurrentlyInRegion(regionId);
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManagerRoot#listCurrentlyInRegion(java.lang.Long)
     */
    public List<Operator> listAllOrderByName()
        throws DataAccessException {
        return getPrimaryDAO().listAllOrderByName();
    }
    
    /**
     * @param operator - operator to update
     * @param workgroup - workgroup
     * @throws BusinessRuleException - business exception
     * @throws DataAccessException - database error
     */
    public void executeChangeWorkgroup(Operator operator, Workgroup workgroup)
        throws BusinessRuleException, DataAccessException {

        if (workgroup == null) {
            operator.setWorkgroup(null);
        } else {
            operator.setWorkgroup(workgroup);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Operator operator = get(id);
        return delete(operator);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Operator instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("operator.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(Operator operator) 
        throws BusinessRuleException, DataAccessException {
        boolean isNew = operator.isNew();
        Long operatorIdentifierUniquenessId = getPrimaryDAO()
            .uniquenessByOperatorIdentifier(operator.getOperatorIdentifier());
        if (operatorIdentifierUniquenessId != null) {
            if (isNew || (!isNew && operatorIdentifierUniquenessId.longValue() != operator.getId().longValue())) {
                throw new FieldValidationException("operator.operatorIdentifier", 
                    operator.getOperatorIdentifier(), 
                    new UserMessage("operator.create.error.existing", operator.getOperatorIdentifier()));
            }
        }
        return super.save(operator);
    }
    
    
    /**
     * Save method for Operators in a list.  Note that this is used for import
     * and will set a new non-null operator.workgroup to null.
     * 
     * @param operators - list of operators to be saved.
     * @return the saved list of operators
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    public Object save(List<Operator> operators) throws DataAccessException,
        BusinessRuleException {
        for (Operator operator : operators) {
            boolean isNew = operator.isNew();
            Long operatorIdentifierUniquenessId = getPrimaryDAO()
                .uniquenessByOperatorIdentifier(operator.getOperatorIdentifier());
            if (operatorIdentifierUniquenessId != null) {
                if (isNew || (!isNew && operatorIdentifierUniquenessId.longValue() != operator.getId().longValue())) {
                    throw new FieldValidationException("operator.operatorIdentifier", 
                        operator.getOperatorIdentifier(), 
                        new UserMessage("operator.create.error.existing", operator.getOperatorIdentifier()));
                }
            }
            
            //If default workgroup is not found, operator import used to fail.
            // To get around this, if workgroup is new, then set it to null.
            if (operator.getWorkgroup() != null && operator.getWorkgroup().isNew()) {
                operator.setWorkgroup(null);
            }
        }
        return super.save(operators);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManagerRoot#getActionTypes()
     */
    public List<OperatorLaborActionType> getActionTypes() {
        ArrayList<OperatorLaborActionType> actionList = new ArrayList<OperatorLaborActionType>();
        OperatorLaborActionType[] actionTypes = OperatorLaborActionType.values();
        for (OperatorLaborActionType actionType : actionTypes) {
            if (!actionType.name().equalsIgnoreCase("signon")
                && !actionType.name().equalsIgnoreCase("signoff")
                && !actionType.name().equalsIgnoreCase("break")) {
                actionList.add(actionType);
            }
        }
        return (actionList);
    }
    
    //Used in Region Data Aggregator
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManagerRoot#listCurrentlyInAllRegion()
     */
    @Override
    public List<Map<String, Object>> listCurrentlyInAllRegion()
        throws DataAccessException {
        return getPrimaryDAO().listCurrentlyInAllRegion();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 