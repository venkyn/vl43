/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.LaborDetailReportRoot;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.loading.service.ArchiveLoadingLaborManager;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.util.ReportTimeFormatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * This is the implementation class for the Operator Labor Details Report.
 * 
 * @author someone
 */
public class LaborDetailReportImplRoot extends JRAbstractBeanDataSourceProvider
    implements ReportDataSourceManager, LaborDetailReportRoot {

    /**
     * enum to tag records with a type
     * 
     * @author
     */
    public enum RecordType {
        OPERATOR_LABOR,
        ASSIGNMENT_LABOR,
        ARCHIVE_ASSIGNMENT_LABOR,
        CYCLECOUNTING_LABOR,
        ARCHIVE_CYCLECOUNTING_LABOR,
        REPLENISHMENT,
        LOADING,
        ARCHIVE_LOADING;
    }

    private static final long MILLISEC_PER_DAY = 86400000L;

    private static final float MILLISEC_PER_HOUR = (float) 3600000.00;

    // order details by startTime
    private static final Comparator<LaborDetailDetailData> ORDER_BY_START_TIME = new Comparator<LaborDetailDetailData>() {

        public int compare(LaborDetailDetailData detail1,
                           LaborDetailDetailData detail2) {
            if (detail1.getStartTime().compareTo(detail2.getStartTime()) < 0) {
                return -1;
            } else if (detail1.getStartTime().compareTo(detail2.getStartTime()) > 0) {
                return 1;
            }
            return 0;
        }
    };

    // fudge factor for choosing whether or not to create a down time record
    private static final int FUDGE_FACTOR = 1000;

    // logger
    private static Logger log = new Logger(LaborDetailReportImplRoot.class);

    // bean class for the report master level data
    /**
     * All Labor Detail Report Data
     * 
     * @author
     */
    public class LaborDetailReportData {

        private String operatorName;

        private String operatorIdentifier;

        private Date shiftStartDate;

        private Date shiftEndDate;

        private JRBeanCollectionDataSource details;

        private JRBeanCollectionDataSource functionRegionTotals;

        private JRBeanCollectionDataSource shiftTotals;

        /**
         * @return the endDate
         */
        public Date getShiftEndDate() {
            return shiftEndDate;
        }

        /**
         * @param shiftEndDate - the endDate to set
         */
        public void setShiftEndDate(Date shiftEndDate) {
            this.shiftEndDate = shiftEndDate;
        }

        /**
         * @return the operatorIdentifier
         */
        public String getOperatorIdentifier() {
            return operatorIdentifier;
        }

        /**
         * @param operatorIdentifier the operatorIdentifier to set
         */
        public void setOperatorIdentifier(String operatorIdentifier) {
            this.operatorIdentifier = operatorIdentifier;
        }

        /**
         * @return the operatorName
         */
        public String getOperatorName() {
            return operatorName;
        }

        /**
         * @param operatorName the operatorName to set
         */
        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        /**
         * @return the startDate
         */
        public Date getShiftStartDate() {
            return shiftStartDate;
        }

        /**
         * @param shiftStartDate the startDate to set
         */
        public void setShiftStartDate(Date shiftStartDate) {
            this.shiftStartDate = shiftStartDate;
        }

        /**
         * @return the details
         */
        public JRBeanCollectionDataSource getDetails() {
            return details;
        }

        /**
         * @param details the details to set
         */
        public void setDetails(JRBeanCollectionDataSource details) {
            this.details = details;
        }

        /**
         * @return the functionRegionTotals
         */
        public JRBeanCollectionDataSource getFunctionRegionTotals() {
            return functionRegionTotals;
        }

        /**
         * @param functionRegionTotals the functionRegionTotals to set
         */
        public void setFunctionRegionTotals(JRBeanCollectionDataSource functionRegionTotals) {
            this.functionRegionTotals = functionRegionTotals;
        }

        /**
         * @return the shiftTotals
         */
        public JRBeanCollectionDataSource getShiftTotals() {
            return shiftTotals;
        }

        /**
         * @param shiftTotals the shiftTotals to set
         */
        public void setShiftTotals(JRBeanCollectionDataSource shiftTotals) {
            this.shiftTotals = shiftTotals;
        }
    }

    /**
     * bean class for the report detail data
     * 
     * @author someone
     */
    public class LaborDetailDetailData {

        private RecordType recordType;

        private OperatorLaborActionType action;

        private String function;

        private String regionNumber;

        private String regionName;

        private Integer regionGoalRate;

        private String assignmentNumber;

        private Date startTime;

        private Date endTime;

        private Long duration;

        private String time;

        private Long count;

        private Double rate;

        /**
         * @return the action
         */
        public OperatorLaborActionType getAction() {
            return action;
        }

        /**
         * @param action the action to set
         */
        public void setAction(OperatorLaborActionType action) {
            this.action = action;
        }

        /**
         * @return the assignmentNumber
         */
        public String getAssignmentNumber() {
            return assignmentNumber;
        }

        /**
         * @param assignmentNumber the assignmentNumber to set
         */
        public void setAssignmentNumber(String assignmentNumber) {
            this.assignmentNumber = assignmentNumber;
        }

        /**
         * @return the count
         */
        public Long getCount() {
            return count;
        }

        /**
         * @param count the count to set
         */
        public void setCount(Long count) {
            this.count = count;
        }

        /**
         * @return the time
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the time to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }

        /**
         * @return the endTime
         */
        public Date getEndTime() {
            return endTime;
        }

        /**
         * @param endTime the endTime to set
         */
        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        /**
         * @return the rate
         */
        public Double getRate() {
            return rate;
        }

        /**
         * @param rate the rate to set
         */
        public void setRate(Double rate) {
            this.rate = rate;
        }

        /**
         * @return the regionNumber
         */
        public String getRegionNumber() {
            return regionNumber;
        }

        /**
         * @param regionNumber the regionNumber to set
         */
        public void setRegionNumber(String regionNumber) {
            this.regionNumber = regionNumber;
        }

        /**
         * @return the startTime
         */
        public Date getStartTime() {
            return startTime;
        }

        /**
         * @param startTime the startTime to set
         */
        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        /**
         * @return the regionName
         */
        public String getRegionName() {
            return regionName;
        }

        /**
         * @param regionName the regionName to set
         */
        public void setRegionName(String regionName) {
            this.regionName = regionName;
        }

        /**
         * @return the regionGoalRate
         */
        public Integer getRegionGoalRate() {
            return regionGoalRate;
        }

        /**
         * @param regionGoalRate the regionGoalRate to set
         */
        public void setRegionGoalRate(Integer regionGoalRate) {
            this.regionGoalRate = regionGoalRate;
        }

        /**
         * @return the recordType
         */
        public RecordType getRecordType() {
            return recordType;
        }

        /**
         * @param recordType the recordType to set
         */
        public void setRecordType(RecordType recordType) {
            this.recordType = recordType;
        }

        /**
         * @return the function
         */
        public String getFunction() {
            return function;
        }

        /**
         * @param function the function to set
         */
        public void setFunction(String function) {
            this.function = function;
        }

        /**
         * @return the time
         */
        public String getTime() {
            return time;
        }

        /**
         * @param time the time to set
         */
        public void setTime(String time) {
            this.time = time;
        }

    }

    /**
     * bean class for the report function region totals
     * 
     * @author someone
     */
    public class LaborDetailFunctionRegionData {

        private String functionRegion;

        private Integer standardRate;

        private Long totalWorkTimeDuration;

        private Long totalDownTimeDuration;

        private String totalWorkTime;

        private String totalDownTime;

        private Long totalCount;

        private Double rate;

        /**
         * @return the functionRegion
         */
        public String getFunctionRegion() {
            return functionRegion;
        }

        /**
         * @param functionRegion the functionRegion to set
         */
        public void setFunctionRegion(String functionRegion) {
            this.functionRegion = functionRegion;
        }

        /**
         * @return the rate
         */
        public Double getRate() {
            return rate;
        }

        /**
         * @param rate the rate to set
         */
        public void setRate(Double rate) {
            this.rate = rate;
        }

        /**
         * @return the standardRate
         */
        public Integer getStandardRate() {
            return standardRate;
        }

        /**
         * @param standardRate the standardRate to set
         */
        public void setStandardRate(Integer standardRate) {
            this.standardRate = standardRate;
        }

        /**
         * @return the totalCount
         */
        public Long getTotalCount() {
            return totalCount;
        }

        /**
         * @param totalCount the totalCount to set
         */
        public void setTotalCount(Long totalCount) {
            this.totalCount = totalCount;
        }

        /**
         * @return the totalDownTime
         */
        public String getTotalDownTime() {
            return totalDownTime;
        }

        /**
         * @param totalDownTime the totalDownTime to set
         */
        public void setTotalDownTime(String totalDownTime) {
            this.totalDownTime = totalDownTime;
        }

        /**
         * @return the totalWorkTime
         */
        public String getTotalWorkTime() {
            return totalWorkTime;
        }

        /**
         * @param totalWorkTime the totalWorkTime to set
         */
        public void setTotalWorkTime(String totalWorkTime) {
            this.totalWorkTime = totalWorkTime;
        }

        /**
         * @return the totalDownTimeDuration
         */
        public Long getTotalDownTimeDuration() {
            return totalDownTimeDuration;
        }

        /**
         * @param totalDownTimeDuration the totalDownTimeDuration to set
         */
        public void setTotalDownTimeDuration(Long totalDownTimeDuration) {
            this.totalDownTimeDuration = totalDownTimeDuration;
        }

        /**
         * @return the totalWorkTimeDuration
         */
        public Long getTotalWorkTimeDuration() {
            return totalWorkTimeDuration;
        }

        /**
         * @param totalWorkTimeDuration the totalWorkTimeDuration to set
         */
        public void setTotalWorkTimeDuration(Long totalWorkTimeDuration) {
            this.totalWorkTimeDuration = totalWorkTimeDuration;
        }
    }

    /**
     * bean class for the report shift totals
     * 
     * @author someone
     */
    public class LaborDetailShiftTotalsData {

        private String totalTime;

        private String totalNonWorkingTime;

        private String totalWorkTime;

        private String totalBreakTime;

        private String totalDownTime;

        private Integer totalCasesPicked;

        private Integer totalReplenishments;

        private Integer totalLocationsCounted;
        
        private Integer totalContainersLoaded;

        
        
        /**
         * getter for totalContainersLoaded
         * @return totalContainersLoaded
         */
        public Integer getTotalContainersLoaded() {
            return totalContainersLoaded;
        }

        /**
         * setter for totalContainersLoaded
         * @param totalContainersLoaded
         */
        public void setTotalContainersLoaded(Integer totalContainersLoaded) {
            this.totalContainersLoaded = totalContainersLoaded;
        }

        /**
         * @return the totalBreakTime
         */
        public String getTotalBreakTime() {
            return totalBreakTime;
        }

        /**
         * @param totalBreakTime the totalBreakTime to set
         */
        public void setTotalBreakTime(String totalBreakTime) {
            this.totalBreakTime = totalBreakTime;
        }

        /**
         * @return the totalCasesPicked
         */
        public Integer getTotalCasesPicked() {
            return totalCasesPicked;
        }

        /**
         * @param totalCasesPicked the totalCasesPicked to set
         */
        public void setTotalCasesPicked(Integer totalCasesPicked) {
            this.totalCasesPicked = totalCasesPicked;
        }

        /**
         * @return the totalDownTime
         */
        public String getTotalDownTime() {
            return totalDownTime;
        }

        /**
         * @param totalDownTime the totalDownTime to set
         */
        public void setTotalDownTime(String totalDownTime) {
            this.totalDownTime = totalDownTime;
        }

        /**
         * @return the totalNonWorkingTime
         */
        public String getTotalNonWorkingTime() {
            return totalNonWorkingTime;
        }

        /**
         * @param totalNonWorkingTime the totalNonWorkingTime to set
         */
        public void setTotalNonWorkingTime(String totalNonWorkingTime) {
            this.totalNonWorkingTime = totalNonWorkingTime;
        }

        /**
         * @return the totalReplenishments
         */
        public Integer getTotalReplenishments() {
            return totalReplenishments;
        }

        /**
         * @param totalReplenishments the totalReplenishments to set
         */
        public void setTotalReplenishments(Integer totalReplenishments) {
            this.totalReplenishments = totalReplenishments;
        }

        /**
         * Getter for the totalLocationsCounted property.
         * @return Integer value of the property
         */
        public Integer getTotalLocationsCounted() {
            return totalLocationsCounted;
        }

        /**
         * Setter for the totalLocationsCounted property.
         * @param totalLocationsCounted the new totalLocationsCounted value
         */
        public void setTotalLocationsCounted(Integer totalLocationsCounted) {
            this.totalLocationsCounted = totalLocationsCounted;
        }

        /**
         * @return the totalTime
         */
        public String getTotalTime() {
            return totalTime;
        }

        /**
         * @param totalTime the totalTime to set
         */
        public void setTotalTime(String totalTime) {
            this.totalTime = totalTime;
        }

        /**
         * @return the totalWorkTime
         */
        public String getTotalWorkTime() {
            return totalWorkTime;
        }

        /**
         * @param totalWorkTime the totalWorkTime to set
         */
        public void setTotalWorkTime(String totalWorkTime) {
            this.totalWorkTime = totalWorkTime;
        }

    }

    // the managers needed to get the data
    private AssignmentLaborManager assignmentLaborManager;

    private ReplenishmentManager replenishmentManager;

    private OperatorLaborManager operatorLaborManager;

    private ArchiveAssignmentLaborManager archiveAssignmentLaborManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    private ArchiveCycleCountingLaborManager archiveCycleCountingLaborManager;

    private OperatorManager operatorManager;
    
    private LoadingRouteLaborManager loadingLaborManager;
    
    private ArchiveLoadingLaborManager archiveLoadingLaborManager;

    
    /**
     * getter for archiveLoadingLaborManager
     * @return archiveLoadingLaborManager
     */
    public ArchiveLoadingLaborManager getArchiveLoadingLaborManager() {
        return archiveLoadingLaborManager;
    }

    /**
     * setter for archiveLoadingLaborManager
     * @param archiveLoadingLaborManager
     */
    public void setArchiveLoadingLaborManager(
            ArchiveLoadingLaborManager archiveLoadingLaborManager) {
        this.archiveLoadingLaborManager = archiveLoadingLaborManager;
    }

    /**
     * getter for loadingLaborManager
     * @return loadingLaborManager
     */
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }

    /**
     * setter for loadingLaborManager
     * @param loadingLaborManager
     */
    public void setLoadingLaborManager(LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    // the operator this report is for
    private Operator operator;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getOperator()
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getOperatorManager()
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setOperatorManager(com.vocollect.voicelink.core.service.OperatorManager)
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getArchiveAssignmentLaborManager()
     */
    public ArchiveAssignmentLaborManager getArchiveAssignmentLaborManager() {
        return archiveAssignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setArchiveAssignmentLaborManager(com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager)
     */
    public void setArchiveAssignmentLaborManager(ArchiveAssignmentLaborManager archiveAssignmentLaborManager) {
        this.archiveAssignmentLaborManager = archiveAssignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getCycleCountingLaborManager()
     */
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setCycleCountingLaborManager(com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager)
     */
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getArchiveCycleCountingLaborManager()
     */
    public ArchiveCycleCountingLaborManager getArchiveCycleCountingLaborManager() {
        return archiveCycleCountingLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setArchiveCycleCountingLaborManager(com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager)
     */
    public void setArchiveCycleCountingLaborManager(ArchiveCycleCountingLaborManager archiveCycleCountingLaborManager) {
        this.archiveCycleCountingLaborManager = archiveCycleCountingLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getAssignmentLaborManager()
     */
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setAssignmentLaborManager(com.vocollect.voicelink.selection.service.AssignmentLaborManager)
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getOperatorLaborManager()
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setOperatorLaborManager(com.vocollect.voicelink.core.service.OperatorLaborManager)
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#getReplenishmentManager()
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborDetailReportRoot#setReplenishmentManager(com.vocollect.voicelink.replenishment.service.ReplenishmentManager)
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values) {

        // get parameters from report
        String operatorId = (String) values.get("OPERATOR_ID");
        Date startDate = (Date) values.get("START_DATE");
        Date endDate = (Date) values.get("END_DATE");
        Integer shiftStartTime = (Integer) values.get("SHIFT_START");
        Integer shiftLength = (Integer) values.get("SHIFT_LENGTH");
        Boolean isInterval = (values.get("IS_INTERVAL") == null)
            ? Boolean.TRUE : (Boolean) values.get("IS_INTERVAL");

        TimeZone siteTimeZone = SiteContextHolder.getSiteContext().getCurrentSite().getTimeZone();
        TimeZone systemTimeZone = Calendar.getInstance().getTimeZone();
        
        // the main report list from which the master JRBeanCollectionDataSource
        // will be created
        List<LaborDetailReportData> reportDataList = new ArrayList<LaborDetailReportData>();

        if (operatorId != null) {
            // get the operator this report is for
            try {
                this.setOperator(getOperatorManager().findByIdentifier(
                    operatorId));
            } catch (DataAccessException e) {
                log.warn("Error getting Operator with operatorIdentifier "
                    + operatorId + " from database: " + e + "\n");
            }
            // calculate the number of shifts
            int numberOfShifts = getNumberOfShifts(startDate, endDate,
                shiftStartTime, shiftLength);
            Date serverStartDate;
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            serverStartDate = DateUtil.convertDateTimeZone(cal,
                    cal.getTimeZone(), siteTimeZone);
            
            // set shift start and end dates
            Calendar shiftStartDate = Calendar.getInstance();
            Calendar shiftEndDate = Calendar.getInstance();
            
            shiftStartDate.setTime(serverStartDate);
            shiftStartDate.set(Calendar.HOUR_OF_DAY, shiftStartTime);
            
            shiftEndDate.setTime(shiftStartDate.getTime());
            shiftEndDate.add(Calendar.HOUR_OF_DAY, shiftLength);
            
            Calendar recordsStartDate = Calendar.getInstance();
            Calendar recordsEndDate = Calendar.getInstance();

            recordsStartDate.setTime(DateUtil.convertDateTimeZone(shiftStartDate, siteTimeZone, systemTimeZone));
            recordsEndDate.setTime(DateUtil.convertDateTimeZone(shiftEndDate, siteTimeZone, systemTimeZone));
            
            shiftStartDate.setTime(serverStartDate);
            shiftStartDate.set(Calendar.HOUR_OF_DAY, shiftStartTime);
            
            shiftEndDate.setTime(shiftStartDate.getTime());
            shiftEndDate.add(Calendar.HOUR_OF_DAY, shiftLength);
            
            // for each shift, add a LaborDetailReportData bean
            for (int shift = 1; shift <= numberOfShifts; shift++) {
                
                addReportDataForShift(reportDataList, shift,
                    recordsStartDate.getTime(), recordsEndDate.getTime(),
                    shiftStartDate.getTime(), shiftEndDate.getTime(), isInterval);
                recordsStartDate.set(Calendar.DAY_OF_YEAR,
                    recordsStartDate.get(Calendar.DAY_OF_YEAR) + 1);
                recordsEndDate.set(Calendar.DAY_OF_YEAR,
                    recordsEndDate.get(Calendar.DAY_OF_YEAR) + 1);
                shiftStartDate.set(Calendar.DAY_OF_YEAR,
                    shiftStartDate.get(Calendar.DAY_OF_YEAR) + 1);
                shiftEndDate.set(Calendar.DAY_OF_YEAR,
                    shiftEndDate.get(Calendar.DAY_OF_YEAR) + 1);
            }
        }

        if (reportDataList.isEmpty()) {
            log.debug("This Labor Detail report is empty");
            LaborDetailReportData blank = new LaborDetailReportData();
            if (operatorId != null) {
                blank.setOperatorName("No Data for operator");
                blank.setOperatorIdentifier(this.operator
                    .getOperatorIdentifier());
            } else {
                blank.setOperatorName("No operator selected.");
            }
            blank.setShiftStartDate(startDate);
            blank.setShiftEndDate(endDate);
            reportDataList.add(blank);
        }

        // return the master datasource
        return new JRBeanCollectionDataSource(reportDataList);
    }

    /**
     * @param reportDataList List<LaborDetailReportData
     * @param shift int
     * @param reportStartDate Date
     * @param reportEndDate Date
     * @param shiftStartDate Date
     * @param shiftEndDate Date
     * @param interval Boolean
     */
    private void addReportDataForShift(List<LaborDetailReportData> reportDataList,
                                       int shift,
                                       Date reportStartDate,
                                       Date reportEndDate,
                                       Date shiftStartDate,
                                       Date shiftEndDate,
                                       Boolean interval) {

        // create a new LaborDetailReportData bean for this shift
        LaborDetailReportData reportData = new LaborDetailReportData();

        // set the master level fields of the bean
        reportData.setOperatorIdentifier(this.getOperator()
            .getOperatorIdentifier());

        if (this.getOperator().getName() != null) {
            reportData.setOperatorName(this.getOperator().getName());
        } else {
            reportData.setOperatorName("");
        }
        
        reportData.setShiftStartDate(shiftStartDate);
        reportData.setShiftEndDate(shiftEndDate);

        // the lists that will be used to create data sources for the three
        // subreports
        List<LaborDetailDetailData> unprocessedLaborDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> detailsList = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailFunctionRegionData> functionRegionTotalsList = new ArrayList<LaborDetailFunctionRegionData>();
        List<LaborDetailShiftTotalsData> shiftTotalsList = new ArrayList<LaborDetailShiftTotalsData>();

        // the lists that will be used to get the data from the database
        List<Object[]> operatorLaborRecords;
        List<Object[]> archiveAssignmentLaborRecords;
        List<Object[]> assignmentLaborRecords;
        List<Object[]> replenishmentRecords;
        List<Object[]> archiveCycleCountingLaborRecords;
        List<Object[]> cycleCountingLaborRecords;
        List<Object[]> loadingLaborRecords;
        List<Object[]> archiveLoadingLaborRecords;

        // the temporary lists of LaborDetailDetailData objects that will be
        // combined into the unprocessedLaborDetailsList
        List<LaborDetailDetailData> operatorLaborDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> assignmentLaborDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> replenishmentDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> cycleCountingLaborDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> loadingLaborDetails = new ArrayList<LaborDetailDetailData>();
        List<LaborDetailDetailData> archiveloadingLaborDetails = new ArrayList<LaborDetailDetailData>();

        // get the data for this shift from the database
        try {
            // Operator Labor
            operatorLaborRecords = this.getOperatorLaborManager()
                .listLaborDetailReportRecords(this.getOperator().getId(),
                    reportStartDate, reportEndDate);
            // Archive Selction
            archiveAssignmentLaborRecords = this
                .getArchiveAssignmentLaborManager()
                .listLaborDetailReportRecords(
                    this.getOperator().getOperatorIdentifier(), reportStartDate,
                    reportEndDate);
            // Selection
            assignmentLaborRecords = this.getAssignmentLaborManager()
                .listLaborDetailReportRecords(this.getOperator().getId(),
                    reportStartDate, reportEndDate);
            // Replenishment
            replenishmentRecords = this.getReplenishmentManager()
                .listLaborDetailReportRecords(this.getOperator().getId(),
                    reportStartDate, reportEndDate);
            // Archive Cycle Counting
            archiveCycleCountingLaborRecords = this
                .getArchiveCycleCountingLaborManager()
                .listLaborDetailReportRecords(
                    this.getOperator().getOperatorIdentifier(), reportStartDate,
                    reportEndDate);
            // Cycle Counting
            cycleCountingLaborRecords = this.getCycleCountingLaborManager()
                .listLaborDetailReportRecords(this.getOperator().getId(),
                    reportStartDate, reportEndDate);
         // Loading
            loadingLaborRecords = this.getLoadingLaborManager()
                .listLaborDetailReportRecords(this.getOperator().getId(),
                    reportStartDate, reportEndDate);
            // Archive Loading
            archiveLoadingLaborRecords = this
                .getArchiveLoadingLaborManager()
                .listLaborDetailReportRecords(
                    this.getOperator().getOperatorIdentifier(), reportStartDate,
                    reportEndDate);
        } catch (DataAccessException e) {

            log.warn("Error getting report data from database: " + e + "\n");
            return;
        }

        // add the data to the unprocessed details list
        if (!operatorLaborRecords.isEmpty()) {
            addToList(operatorLaborDetails, operatorLaborRecords,
                RecordType.OPERATOR_LABOR);
        }

        if (!archiveAssignmentLaborRecords.isEmpty()) {
            addToList(assignmentLaborDetails, archiveAssignmentLaborRecords,
                RecordType.ARCHIVE_ASSIGNMENT_LABOR);
        }

        if (!assignmentLaborRecords.isEmpty()) {
            addToList(assignmentLaborDetails, assignmentLaborRecords,
                RecordType.ASSIGNMENT_LABOR);
        }

        if (!archiveCycleCountingLaborRecords.isEmpty()) {
            addToList(cycleCountingLaborDetails,
                archiveCycleCountingLaborRecords,
                RecordType.ARCHIVE_CYCLECOUNTING_LABOR);
        }

        if (!cycleCountingLaborRecords.isEmpty()) {
            addToList(cycleCountingLaborDetails, cycleCountingLaborRecords,
                RecordType.CYCLECOUNTING_LABOR);
        }
        
        if (!assignmentLaborDetails.isEmpty()) {
            Collections.sort(assignmentLaborDetails, ORDER_BY_START_TIME);
        }

        if (!cycleCountingLaborDetails.isEmpty()) {
            Collections.sort(cycleCountingLaborDetails, ORDER_BY_START_TIME);
        }

        if (!replenishmentRecords.isEmpty()) {
            addToList(replenishmentDetails, replenishmentRecords,
                RecordType.REPLENISHMENT);
        }
        
        if (!loadingLaborRecords.isEmpty()) {
            addToList(loadingLaborDetails, loadingLaborRecords,
                RecordType.LOADING);
        }
        
        if (!archiveLoadingLaborRecords.isEmpty()) {
            addToList(loadingLaborDetails,
                archiveLoadingLaborRecords,
                RecordType.ARCHIVE_LOADING);
        }

        // adjust open OperatorLabor record, if it exists.

        if (!operatorLaborDetails.isEmpty()) {
            adjustOpenOperatorLaborDetail(operatorLaborDetails,
                assignmentLaborDetails, cycleCountingLaborDetails,
                replenishmentDetails, loadingLaborDetails, shiftEndDate, reportEndDate, interval);
        }

        // combine individual details list into one unprocessed labor details
        // list
        combineDetails(unprocessedLaborDetails, operatorLaborDetails,
            assignmentLaborDetails, cycleCountingLaborDetails,
            replenishmentDetails, loadingLaborDetails);

        if (!unprocessedLaborDetails.isEmpty()) {
            // adjust the first OL record for the shift window
            adjustFirstRecordForShiftWindow(unprocessedLaborDetails,
                shiftStartDate);

            // process the details and store in detailsList
            processDetailsList(unprocessedLaborDetails, detailsList);

            // perform the aggregation for the totals subreports
            aggregateFunctionRegionTotals(detailsList, functionRegionTotalsList);
            aggregateShiftTotals(detailsList, shiftTotalsList, shiftEndDate);
        }

        // set the subreport datasource fields for this shift
        reportData.setDetails(new JRBeanCollectionDataSource(detailsList));
        reportData.setFunctionRegionTotals(new JRBeanCollectionDataSource(
            functionRegionTotalsList));
        reportData.setShiftTotals(new JRBeanCollectionDataSource(
            shiftTotalsList));

        // add the bean for this shift to the master list.
        reportDataList.add(reportData);

    }

    /**
     * @param operatorLaborDetails List<LaborDetailDetailData>
     * @param assignmentLaborDetails List<LaborDetailDetailData>
     * @param cycleCountingLaborDetails List<LaborDetailDetailData>
     * @param replenishmentDetails List<LaborDetailDetailData>
     * @param loadingLaborDetails List<LaborDetailDetailData> 
     * @param shiftEndDate Date
     * @param reportEndDate Date
     * @param interval Boolean
     */
    private void adjustOpenOperatorLaborDetail(List<LaborDetailDetailData> operatorLaborDetails,
                                               List<LaborDetailDetailData> assignmentLaborDetails,
                                               List<LaborDetailDetailData> cycleCountingLaborDetails,
                                               List<LaborDetailDetailData> replenishmentDetails,
                                               List<LaborDetailDetailData> loadingLaborDetails,
                                               Date shiftEndDate, 
                                               Date reportEndDate, 
                                               Boolean interval) {

        // get the last index
        int lastIndex = operatorLaborDetails.size() - 1;

        // get the last record
        LaborDetailDetailData lastRecord = operatorLaborDetails.get(lastIndex);

        if ((lastRecord.getEndTime() == null)
            || (lastRecord.getEndTime().compareTo(shiftEndDate) > 0)) {
            // set the end time of the record to the shift end time
            if (interval) {
                Calendar endTimeValue = Calendar.getInstance();
                endTimeValue.setTime(shiftEndDate);
                Date endTimeForLastRecord = DateUtil.convertDateTimeZone(endTimeValue,
                    SiteContextHolder.getSiteContext()
                    .getCurrentSite().getTimeZone(), endTimeValue.getTimeZone());
                operatorLaborDetails.get(lastIndex).setEndTime(endTimeForLastRecord);
            } else {
                operatorLaborDetails.get(lastIndex).setEndTime(reportEndDate);
            }
            
            // recalculate duration of record
            operatorLaborDetails.get(lastIndex).setDuration(
                operatorLaborDetails.get(lastIndex).getEndTime().getTime()
                    - operatorLaborDetails.get(lastIndex).getStartTime()
                        .getTime());

            operatorLaborDetails.get(lastIndex).setTime(
                ReportTimeFormatter.formatTime(operatorLaborDetails.get(
                    lastIndex).getDuration()));
        } 
    }

    /**
     * @param unprocessedLaborDetails List<LaborDetailDetailData>
     * @param operatorLaborDetails List<LaborDetailDetailData>
     * @param assignmentLaborDetails List<LaborDetailDetailData>
     * @param cycleCountingLaborDetails List<LaborDetailDetailData>
     * @param replenishmentDetails List<LaborDetailDetailData>
     * @param loadingLaborDetails List<LaborDetailDetailData>
     */
    private void combineDetails(List<LaborDetailDetailData> unprocessedLaborDetails,
                                List<LaborDetailDetailData> operatorLaborDetails,
                                List<LaborDetailDetailData> assignmentLaborDetails,
                                List<LaborDetailDetailData> cycleCountingLaborDetails,
                                List<LaborDetailDetailData> replenishmentDetails, 
                                List<LaborDetailDetailData> loadingLaborDetails) {

        // index for assignmentLaborDetails
        int a = 0;

        // index for replenishmentDetails
        int r = 0;

        // index for cycleCountingDetails
        int cc = 0;
        
     // index for loadingDetails
        int l = 0;

        for (LaborDetailDetailData detail : operatorLaborDetails) {
            unprocessedLaborDetails.add(detail);
            if (detail.getAction() == OperatorLaborActionType.Selection) {
                // put all the AssignmentLabor records that correspond to this
                // OperatorLabor record into the unprocessed list
                if (!assignmentLaborDetails.isEmpty()) {
                    while (a < assignmentLaborDetails.size()
                        && assignmentLaborDetails.get(a).getEndTime()
                            .compareTo(detail.getEndTime()) <= 0) {
                        unprocessedLaborDetails.add(assignmentLaborDetails
                            .get(a));
                        a++;
                    }
                }
            } else if (detail.getAction() == OperatorLaborActionType.CycleCounting) {
                // put all the CycleCountingLabor records that correspond to
                // this OperatorLabor record into the unprocessed list
                if (!cycleCountingLaborDetails.isEmpty()) {
                    while (cc < cycleCountingLaborDetails.size()
                        && cycleCountingLaborDetails.get(cc).getEndTime()
                            .compareTo(detail.getEndTime()) <= 0) {
                        unprocessedLaborDetails.add(cycleCountingLaborDetails
                            .get(cc));
                        cc++;
                    }
                }
            } else if (detail.getAction() == OperatorLaborActionType.Replenishment) {
                // put all the Replenishment records that correspond to this
                // OperatorLabor record into the unprocessed list
                if (!replenishmentDetails.isEmpty()) {
                    while (r < replenishmentDetails.size()
                        && replenishmentDetails.get(r).getEndTime()
                            .compareTo(detail.getEndTime()) <= 0) {
                        unprocessedLaborDetails
                            .add(replenishmentDetails.get(r));
                        r++;
                    }
                }
            } else if (detail.getAction() == OperatorLaborActionType.Loading) {
                // put all the Loading records that correspond to this
                // OperatorLabor record into the unprocessed list
                if (!loadingLaborDetails.isEmpty()) {
                    while (l < loadingLaborDetails.size()
                        && loadingLaborDetails.get(l).getEndTime()
                            .compareTo(detail.getEndTime()) <= 0) {
                        unprocessedLaborDetails
                            .add(loadingLaborDetails.get(l));
                        l++;
                    }
                }
            }
        }
    }

    /**
     * @param unprocessedLaborDetails List<LaborDetailDetailData>
     * @param detailsList List<LaborDetailDetailData>
     */
    private void processDetailsList(List<LaborDetailDetailData> unprocessedLaborDetails,
                                    List<LaborDetailDetailData> detailsList) {

        // store the current region information to fill records with appropriate
        // region information
        String currentRegionNumber = "";
        String currentRegionName = "";
        Integer currentRegionGoalRate = 0;

        // walk through the unprocessed list and process each record
        for (int i = 0; i < unprocessedLaborDetails.size() - 1; i++) {

            // get the current record and the next record
            LaborDetailDetailData detail1 = unprocessedLaborDetails.get(i);
            LaborDetailDetailData detail2 = unprocessedLaborDetails.get(i + 1);

            // check if we should shove the current record into the processed
            // list
            if (detail1.getAction() == OperatorLaborActionType.SignOn
                || detail1.getAction() == OperatorLaborActionType.SignOff
                || detail1.getAction() == OperatorLaborActionType.Break) {
                // shove it in the processed list
                detailsList.add(detail1);
            } else if (detail1.getRecordType() == RecordType.OPERATOR_LABOR) {

                // set current region information
                if (detail1.getRegionNumber() == null) {
                    // operator is currently performing a function in
                    // multiple regions
                    currentRegionNumber = ResourceUtil
                        .getLocalizedKeyValue("selection.report.LaborDetail.Details.multiple");
                    currentRegionName = ResourceUtil
                        .getLocalizedKeyValue("selection.report.LaborDetail.Details.multiple");
                    if (detail1.getAction() == OperatorLaborActionType.Replenishment) {
                        currentRegionGoalRate = getReplenRegionGoalRate(unprocessedLaborDetails);
                    } else if (detail1.getAction() == OperatorLaborActionType.Loading) {
                        currentRegionGoalRate = getLoadingRegionGoalRate(unprocessedLaborDetails);
                    } else {
                        currentRegionGoalRate = getCycleCountingRegionGoalRate(unprocessedLaborDetails);
                    }
                } else {
                    // use the available region info
                    currentRegionNumber = detail1.getRegionNumber();
                    currentRegionName = detail1.getRegionName();
                    currentRegionGoalRate = detail1.getRegionGoalRate();
                }

                // compare the start time of the next record to the current
                // record OperatorLabor Selection, Replenishment, and Cycle
                // Counting are treated as singular points in time these records
                // are not added - only the down time they allow us to
                // calculate.
                Long downTime = detail2.getStartTime().getTime()
                    - detail1.getStartTime().getTime();
                if (downTime >= FUDGE_FACTOR) {
                    // we should create a down time record because there is a
                    // big enough gap
                    LaborDetailDetailData downTimeRecord = new LaborDetailDetailData();
                    downTimeRecord.setAction(OperatorLaborActionType.DownTime);
                    downTimeRecord.setFunction(detail1.getFunction());
                    downTimeRecord.setRegionNumber(currentRegionNumber);
                    downTimeRecord.setRegionName(currentRegionName);
                    downTimeRecord.setRegionGoalRate(currentRegionGoalRate);
                    downTimeRecord.setStartTime(detail1.getStartTime());
                    downTimeRecord.setEndTime(detail2.getStartTime());
                    downTimeRecord.setDuration(downTime);
                    downTimeRecord.setTime(ReportTimeFormatter
                        .formatTime(downTimeRecord.getDuration()));
                    // add the newly created record to the processed list
                    detailsList.add(downTimeRecord);
                }
            } else {
                // this is an assignment record (either selection,
                // replenishment, or cycle counting assignment)
                detail1.setRegionNumber(currentRegionNumber);
                detail1.setRegionName(currentRegionName);
                detail1.setRegionGoalRate(currentRegionGoalRate);
                detailsList.add(detail1);

                // check for down time and shove it in
                Long downTime = detail2.getStartTime().getTime()
                    - detail1.getEndTime().getTime();
                if (downTime >= FUDGE_FACTOR) {
                    // we should create a down time record because there is a
                    // big enough gap
                    LaborDetailDetailData downTimeRecord = new LaborDetailDetailData();
                    downTimeRecord.setAction(OperatorLaborActionType.DownTime);
                    downTimeRecord.setFunction(detail1.getFunction());
                    downTimeRecord.setRegionNumber(currentRegionNumber);
                    downTimeRecord.setRegionName(currentRegionName);
                    downTimeRecord.setRegionGoalRate(currentRegionGoalRate);
                    downTimeRecord.setStartTime(detail1.getEndTime());
                    downTimeRecord.setEndTime(detail2.getStartTime());
                    downTimeRecord.setDuration(downTime);
                    downTimeRecord.setTime(ReportTimeFormatter
                        .formatTime(downTimeRecord.getDuration()));
                    // add the newly created record to the processed list
                    detailsList.add(downTimeRecord);
                }
            }
        }

        // handle the last record
        int lastIndex = unprocessedLaborDetails.size() - 1;
        LaborDetailDetailData lastRecord = unprocessedLaborDetails
            .get(lastIndex);
        if (lastRecord.getAction() == OperatorLaborActionType.SignOn
            || lastRecord.getAction() == OperatorLaborActionType.SignOff
            || lastRecord.getAction() == OperatorLaborActionType.Break) {
            // shove it in the processed list
            detailsList.add(lastRecord);
        } else if (lastRecord.getRecordType() != RecordType.OPERATOR_LABOR) {
            // this is an assignment record (either selection, cycle counting or
            // replenishment assignment) push the record
            lastRecord.setRegionGoalRate(currentRegionGoalRate);
            lastRecord.setRegionNumber(currentRegionNumber);
            lastRecord.setRegionName(currentRegionName);
            detailsList.add(lastRecord);
        }
    }

    /**
     * @param unprocessedLaborDetails List<LaborDetailDetailData>
     * @return GoalRate
     */
    private Integer getReplenRegionGoalRate(List<LaborDetailDetailData> unprocessedLaborDetails) {
        // get the first goal rate we come to
        int i = 0;
        boolean goalRateNotFound = true;
        Integer regionGoalRate = 0;

        while (goalRateNotFound && i < unprocessedLaborDetails.size()) {
            if (unprocessedLaborDetails.get(i).getRecordType() == RecordType.REPLENISHMENT) {
                regionGoalRate = unprocessedLaborDetails.get(i)
                    .getRegionGoalRate();
                goalRateNotFound = false;
            }
            i++;
        }

        return regionGoalRate;
    }

    /**
     * @param unprocessedLaborDetails List<LaborDetailDetailData>
     * @return GoalRate
     */
    private Integer getCycleCountingRegionGoalRate(List<LaborDetailDetailData> unprocessedLaborDetails) {
        // get the first goal rate we come to
        int i = 0;
        boolean goalRateNotFound = true;
        Integer regionGoalRate = 0;

        while (goalRateNotFound && i < unprocessedLaborDetails.size()) {
            if (unprocessedLaborDetails.get(i).getRecordType() == RecordType.CYCLECOUNTING_LABOR
                || unprocessedLaborDetails.get(i).getRecordType() == RecordType.ARCHIVE_CYCLECOUNTING_LABOR) {
                regionGoalRate = unprocessedLaborDetails.get(i)
                    .getRegionGoalRate();
                goalRateNotFound = false;
            }
            i++;
        }

        return regionGoalRate;
    }
    
    
    /**
     * @param unprocessedLaborDetails List<LaborDetailDetailData>
     * @return GoalRate
     */
    private Integer getLoadingRegionGoalRate(List<LaborDetailDetailData> unprocessedLaborDetails) {
        // get the first goal rate we come to
        int i = 0;
        boolean goalRateNotFound = true;
        Integer regionGoalRate = 0;

        while (goalRateNotFound && i < unprocessedLaborDetails.size()) {
            if (unprocessedLaborDetails.get(i).getRecordType() == RecordType.LOADING
                || unprocessedLaborDetails.get(i).getRecordType() == RecordType.ARCHIVE_LOADING) {
                regionGoalRate += unprocessedLaborDetails.get(i)
                    .getRegionGoalRate();
                goalRateNotFound = false;
            }
            i++;
        }
        regionGoalRate = regionGoalRate/i;

        return regionGoalRate;
    }

    /**
     * @param detailsList List<LaborDetailDetailData>
     * @param shiftTotalsList List<LaborDetailShiftTotalsData>
     * @param shiftEndDate Date
     */
    private void aggregateShiftTotals(List<LaborDetailDetailData> detailsList,
                                      List<LaborDetailShiftTotalsData> shiftTotalsList,
                                      Date shiftEndDate) {

        Long totalTimeDuration = 0L;
        Long totalWorkTimeDuration = 0L;
        Long totalBreakTimeDuration = 0L;
        Long totalDownTimeDuration = 0L;
        Long totalNonWorkingTimeDuration = 0L;
        Integer totalCasesPicked = 0;
        Integer totalReplenishments = 0;
        Integer totalLocationsCounted = 0;
        Integer totalLoadings = 0;

        for (LaborDetailDetailData detail : detailsList) {

            if (detail.getAction() == OperatorLaborActionType.Selection
                || detail.getAction() == OperatorLaborActionType.Replenishment
                || detail.getAction() == OperatorLaborActionType.CycleCounting 
                || detail.getAction() == OperatorLaborActionType.Loading) {
                // add to total work time
                totalWorkTimeDuration += detail.getDuration();

                if (detail.getAction() == OperatorLaborActionType.Selection) {
                    totalCasesPicked += detail.getCount().intValue();
                } else if (detail.getAction() == OperatorLaborActionType.CycleCounting) {
                    totalLocationsCounted += detail.getCount().intValue();
                } else if (detail.getAction() == OperatorLaborActionType.Loading) {
                    totalLoadings += detail.getCount().intValue();
                }  else {
                    totalReplenishments += detail.getCount().intValue();
                }
            } else if (detail.getAction() == OperatorLaborActionType.DownTime) {
                // add to total down time
                totalDownTimeDuration += detail.getDuration();
            } else if (detail.getAction() == OperatorLaborActionType.SignOn
                || detail.getAction() == OperatorLaborActionType.SignOff) {
                // add to total non-working time
                totalNonWorkingTimeDuration += detail.getDuration();
            } else if (detail.getAction() == OperatorLaborActionType.Break) {
                // add to total break time
                totalBreakTimeDuration += detail.getDuration();
            }

        }

        // calculate total time
        if (detailsList.isEmpty()) {
            // this only happens in the case where we had just one spanning OL
            // Selection or Replenishment record for the shift that did not have
            // any assignments that completed within the shift. In this case we
            // don't need to calculate total time because there is nothing to
            // report on.
            return;
        }

        LaborDetailDetailData firstRecord = detailsList.get(0);
        LaborDetailDetailData lastRecord = detailsList
            .get(detailsList.size() - 1);
        totalTimeDuration = lastRecord.getEndTime().getTime()
            - firstRecord.getStartTime().getTime();

        LaborDetailShiftTotalsData shiftTotals = new LaborDetailShiftTotalsData();
        shiftTotals.setTotalTime(ReportTimeFormatter
            .formatTime(totalTimeDuration));
        shiftTotals.setTotalWorkTime(ReportTimeFormatter
            .formatTime(totalWorkTimeDuration));
        shiftTotals.setTotalBreakTime(ReportTimeFormatter
            .formatTime(totalBreakTimeDuration));
        shiftTotals.setTotalDownTime(ReportTimeFormatter
            .formatTime(totalDownTimeDuration));
        shiftTotals.setTotalNonWorkingTime(ReportTimeFormatter
            .formatTime(totalNonWorkingTimeDuration));
        shiftTotals.setTotalCasesPicked(totalCasesPicked);
        shiftTotals.setTotalReplenishments(totalReplenishments);
        shiftTotals.setTotalLocationsCounted(totalLocationsCounted);
        shiftTotals.setTotalContainersLoaded(totalLoadings);

        shiftTotalsList.add(shiftTotals);
    }

    /**
     * @param detailsList List<LaborDetailData>
     * @param functionRegionTotalsList List<LaborDetailFuntionRegionData>
     */
    private void aggregateFunctionRegionTotals(List<LaborDetailDetailData> detailsList,
                                               List<LaborDetailFunctionRegionData> functionRegionTotalsList) {

        // process each record in the details list,
        // aggregating the work time, down time, and count into
        // LaborDetailFunctionRegionData objects in a list.
        for (LaborDetailDetailData detail : detailsList) {

            // only add up Selection, Replenishment, and Down Time actions
            if (detail.getAction() == OperatorLaborActionType.Selection
                || detail.getAction() == OperatorLaborActionType.CycleCounting
                || detail.getAction() == OperatorLaborActionType.Replenishment
                || detail.getAction() == OperatorLaborActionType.Loading
                || detail.getAction() == OperatorLaborActionType.DownTime) {

                String regionNumber = detail.getRegionNumber();
                String regionName = detail.getRegionName();
                Integer standardRate = detail.getRegionGoalRate();

                // set the functionRegion
                String localizedFunctionRegionStr;
                if (detail.getFunction().equals("Replenishment")) {
                    localizedFunctionRegionStr = ResourceUtil
                        .getLocalizedEnumName(OperatorLaborActionType.Replenishment)
                        + " - "
                        + ResourceUtil
                            .getLocalizedKeyValue("selection.report.LaborDetail.Details.allregions");
                } else if (detail.getFunction().equals("CycleCounting")) {
                    localizedFunctionRegionStr = ResourceUtil
                        .getLocalizedEnumName(OperatorLaborActionType.CycleCounting)
                        + " - "
                        + ResourceUtil
                            .getLocalizedKeyValue("selection.report.LaborDetail.Details.allregions");
                } else if (detail.getFunction().equals("Loading")) {
                    localizedFunctionRegionStr = ResourceUtil
                            .getLocalizedEnumName(OperatorLaborActionType.Loading)
                            + " - " + regionNumber + " - " + regionName;
                } else {
                    localizedFunctionRegionStr = ResourceUtil
                        .getLocalizedEnumName(OperatorLaborActionType.Selection)
                        + " - " + regionNumber + " - " + regionName;
                }

                if (!functionRegionTotalsList.isEmpty()) {

                    // check to see if the function - region has already been
                    // added to the functionRegionTotalsList
                    // if so, add the values from this record
                    int i = 0;
                    boolean functionRegionNotFound = true;
                    while (functionRegionNotFound
                        && i < functionRegionTotalsList.size()) {
                        LaborDetailFunctionRegionData functionRegionData = functionRegionTotalsList
                            .get(i);
                        if (functionRegionData.getFunctionRegion().equals(
                            localizedFunctionRegionStr)) {
                            // we have a match, aggregate this record's values
                            // into the existing record
                            if (detail.getAction() != OperatorLaborActionType.DownTime) {
                                functionRegionData
                                    .setTotalWorkTimeDuration(functionRegionData
                                        .getTotalWorkTimeDuration()
                                        + detail.getDuration());
                                functionRegionData
                                    .setTotalCount(functionRegionData
                                        .getTotalCount() + detail.getCount());
                            } else {
                                functionRegionData
                                    .setTotalDownTimeDuration(functionRegionData
                                        .getTotalDownTimeDuration()
                                        + detail.getDuration());
                            }
                            functionRegionTotalsList.set(i, functionRegionData);
                            functionRegionNotFound = false;
                        }
                        i++;
                    }
                    if (functionRegionNotFound) {
                        // there was no existing record - so create a new one
                        // and add it to the list
                        LaborDetailFunctionRegionData functionRegionData = new LaborDetailFunctionRegionData();

                        functionRegionData
                            .setFunctionRegion(localizedFunctionRegionStr);
                        functionRegionData.setStandardRate(standardRate);
                        if (detail.getAction() != OperatorLaborActionType.DownTime) {
                            functionRegionData.setTotalWorkTimeDuration(detail
                                .getDuration());
                            functionRegionData.setTotalDownTimeDuration(0L);
                            functionRegionData.setTotalCount(detail.getCount());
                        } else {
                            functionRegionData.setTotalWorkTimeDuration(0L);
                            functionRegionData.setTotalDownTimeDuration(detail
                                .getDuration());
                            functionRegionData.setTotalCount(0L);
                        }
                        functionRegionTotalsList.add(functionRegionData);
                    }
                } else {
                    // there are no existing records in functionRegionTotalsList
                    // so create a new one and add it
                    LaborDetailFunctionRegionData functionRegionData = new LaborDetailFunctionRegionData();

                    functionRegionData
                        .setFunctionRegion(localizedFunctionRegionStr);
                    functionRegionData.setStandardRate(standardRate);
                    if (detail.getAction() != OperatorLaborActionType.DownTime) {
                        functionRegionData.setTotalWorkTimeDuration(detail
                            .getDuration());
                        functionRegionData.setTotalDownTimeDuration(0L);
                        functionRegionData.setTotalCount(detail.getCount());
                    } else {
                        functionRegionData.setTotalWorkTimeDuration(0L);
                        functionRegionData.setTotalDownTimeDuration(detail
                            .getDuration());
                        functionRegionData.setTotalCount(0L);
                    }
                    functionRegionTotalsList.add(functionRegionData);
                }
            }
        }

        // calculate the rate for each function region total object
        if (!functionRegionTotalsList.isEmpty()) {
            for (LaborDetailFunctionRegionData functionRegionData : functionRegionTotalsList) {
                Double rate = new Double(
                    functionRegionData.getTotalCount()
                        / (new Long(functionRegionData
                            .getTotalWorkTimeDuration()
                            + functionRegionData.getTotalDownTimeDuration()) / MILLISEC_PER_HOUR));
                functionRegionData.setRate(rate);
            }
        }

        // set the printable times
        if (!functionRegionTotalsList.isEmpty()) {
            for (LaborDetailFunctionRegionData functionRegionData : functionRegionTotalsList) {
                functionRegionData.setTotalWorkTime(ReportTimeFormatter
                    .formatTime(functionRegionData.getTotalWorkTimeDuration()));
                functionRegionData.setTotalDownTime(ReportTimeFormatter
                    .formatTime(functionRegionData.getTotalDownTimeDuration()));
            }
        }
    }

    /**
     * @param unprocessedLaborDetails List<LaborDetailData>
     * @param shiftStartDate Date
     */
    private void adjustFirstRecordForShiftWindow(List<LaborDetailDetailData> unprocessedLaborDetails,
                                                 Date shiftStartDate) {

        // get the first record
        LaborDetailDetailData firstRecord = unprocessedLaborDetails.get(0);

        if (firstRecord.getRecordType() == RecordType.OPERATOR_LABOR
            && (firstRecord.getAction() == OperatorLaborActionType.Selection
                || firstRecord.getAction() == OperatorLaborActionType.Replenishment || firstRecord
                .getAction() == OperatorLaborActionType.CycleCounting 
                || firstRecord.getAction() == OperatorLaborActionType.Loading)) {
            // if there is a next record and the start time of the next record
            // is before the shift start date, set the start date of the first
            // record to the start date of the next record
            if (unprocessedLaborDetails.size() > 1) {
                LaborDetailDetailData secondRecord = unprocessedLaborDetails
                    .get(1);
                if (secondRecord.getStartTime().compareTo(shiftStartDate) < 0) {
                    // set the start time of the first record to the start time
                    // of the next record this is valid because it cannot be
                    // another OL record because there can only be one OL record
                    // with a start time outside the shift and this second
                    // record has a start time before the shift, therefore it's
                    // possible to conclude that it is an assignment
                    unprocessedLaborDetails.get(0).setStartTime(
                        secondRecord.getStartTime());
                } else {
                    // set the start time to the shift start time
                    unprocessedLaborDetails.get(0).setStartTime(shiftStartDate);
                }
            } else {
                // there aren't any other records in the list - this would be an
                // odd sort of situation just set the start time to the shift
                // start time
                unprocessedLaborDetails.get(0).setStartTime(shiftStartDate);
            }
        }
        // recalculate duration of record
        if (unprocessedLaborDetails.get(0).getEndTime() != null) {
            unprocessedLaborDetails.get(0).setDuration(
                unprocessedLaborDetails.get(0).getEndTime().getTime()
                    - unprocessedLaborDetails.get(0).getStartTime().getTime());
            unprocessedLaborDetails.get(0).setTime(
                ReportTimeFormatter.formatTime(unprocessedLaborDetails.get(0)
                    .getDuration()));
        }
    }

    /**
     * @param startDate Date
     * @param endDate Date
     * @param shiftStartTime Integer
     * @param shiftLength Integer
     * @return Integer Number of Intervals
     */
    private int getNumberOfShifts(Date startDate,
                                  Date endDate,
                                  Integer shiftStartTime,
                                  Integer shiftLength) {

        Long intervals; // number of possible shift slots.

        // Get total millisecs in the reporting period
        Long mSecPerReport = new Long(endDate.getTime() - startDate.getTime());

        // determine the total number of shift slots that the report
        // encompasses.
        intervals = mSecPerReport / MILLISEC_PER_DAY;
        // include partial slots.
        if ((mSecPerReport % MILLISEC_PER_DAY) > 0) {
            intervals++;
        }

        return intervals.intValue();
    }

    /**
     * @param unprocessedLaborDetails Lists<LaborDetailData>
     * @param records List<Object[]>
     * @param recordType RecordType Type
     */
    private void addToList(List<LaborDetailDetailData> unprocessedLaborDetails,
                           List<Object[]> records,
                           RecordType recordType) {
        // TODO Break this into at least 2 smaller methods

        switch (recordType) {
        case OPERATOR_LABOR:
            for (Object[] record : records) {
                if (record[0] != null) {
                    // create new labor detail detail data object
                    LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                    laborDetail.setRecordType(recordType);

                    // get values from Object array
                    if (record[0] != null) {
                        laborDetail
                            .setAction((OperatorLaborActionType) record[0]);
                    }
                    if (record[1] != null) {
                        laborDetail.setRegionNumber(record[1].toString());
                    }
                    if (record[2] != null) {
                        laborDetail.setRegionName((String) record[2]);
                    }
                    if (record[3] != null) {
                        laborDetail.setRegionGoalRate((Integer) record[3]);
                    }
                    if (record[4] != null) {
                        laborDetail.setStartTime((Date) record[4]);
                    }
                    if (record[5] != null) {
                        laborDetail.setEndTime((Date) record[5]);
                    }
                    if (record[6] != null) {
                        laborDetail.setDuration((Long) record[6]);
                        laborDetail.setTime(ReportTimeFormatter
                            .formatTime((Long) record[6]));
                    }

                    if (laborDetail.getAction() == OperatorLaborActionType.Selection) {
                        laborDetail.setFunction("Selection");
                    } else if (laborDetail.getAction() == OperatorLaborActionType.Replenishment) {
                        laborDetail.setFunction("Replenishment");
                    } else if (laborDetail.getAction() == OperatorLaborActionType.CycleCounting) {
                        laborDetail.setFunction("CycleCounting");
                    } else if (laborDetail.getAction() == OperatorLaborActionType.Loading) {
                        laborDetail.setFunction("Loading");
                    }

                    // add new record to unprocesedLaborDetails
                    unprocessedLaborDetails.add(laborDetail);
                }
            }
            break;
        case ARCHIVE_ASSIGNMENT_LABOR:
            for (Object[] record : records) {

                // create new labor detail detail data object
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.Selection);
                laborDetail.setFunction("Selection");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setAssignmentNumber(record[2].toString());
                }
                if (record[3] != null) {
                    laborDetail.setStartTime((Date) record[3]);
                }
                if (record[4] != null) {
                    laborDetail.setEndTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setDuration((Long) record[5]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[5]));
                }
                if (record[6] != null) {
                    laborDetail.setCount((Long) record[6]);
                }
                if (record[7] != null) {
                    laborDetail.setRate((Double) record[7]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case ASSIGNMENT_LABOR:
            for (Object[] record : records) {

                // create new labor detail detail data object and set fields
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.Selection);
                laborDetail.setFunction("Selection");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setRegionGoalRate((Integer) record[2]);
                }
                if (record[3] != null) {
                    laborDetail.setAssignmentNumber(record[3].toString());
                }
                if (record[4] != null) {
                    laborDetail.setStartTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setEndTime((Date) record[5]);
                }
                if (record[6] != null) {
                    laborDetail.setDuration((Long) record[6]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[6]));
                }
                if (record[7] != null) {
                    laborDetail.setCount((Long) record[7]);
                }
                if (record[8] != null) {
                    laborDetail.setRate((Double) record[8]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case ARCHIVE_CYCLECOUNTING_LABOR:
            for (Object[] record : records) {

                // create new labor detail detail data object
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.CycleCounting);
                laborDetail.setFunction("CycleCounting");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setAssignmentNumber(record[2].toString());
                }
                if (record[3] != null) {
                    laborDetail.setStartTime((Date) record[3]);
                }
                if (record[4] != null) {
                    laborDetail.setEndTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setDuration((Long) record[5]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[5]));
                }
                if (record[6] != null) {
                    laborDetail.setCount((Long) record[6]);
                }
                if (record[7] != null) {
                    laborDetail.setRate((Double) record[7]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case CYCLECOUNTING_LABOR:
            for (Object[] record : records) {

                // create new labor detail detail data object and set fields
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.CycleCounting);
                laborDetail.setFunction("CycleCounting");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setRegionGoalRate((Integer) record[2]);
                }
                if (record[3] != null) {
                    laborDetail.setAssignmentNumber(record[3].toString());
                }
                if (record[4] != null) {
                    laborDetail.setStartTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setEndTime((Date) record[5]);
                }
                if (record[6] != null) {
                    laborDetail.setDuration((Long) record[6]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[6]));
                }
                if (record[7] != null) {
                    laborDetail.setCount((Long) record[7]);
                }
                if (record[8] != null) {
                    laborDetail.setRate((Double) record[8]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case REPLENISHMENT:
            for (Object[] record : records) {

                // create new labor detail detail data object and set fields
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.Replenishment);
                laborDetail.setFunction("Replenishment");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setRegionGoalRate((Integer) record[2]);
                }
                if (record[3] != null) {
                    laborDetail.setAssignmentNumber((String) record[3]);
                }
                if (record[4] != null) {
                    laborDetail.setStartTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setEndTime((Date) record[5]);
                    laborDetail.setDuration(laborDetail.getEndTime().getTime()
                        - laborDetail.getStartTime().getTime());
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime(laborDetail.getDuration()));

                }
                laborDetail.setCount(1L);

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case LOADING:
            for (Object[] record : records) {

                // create new labor detail detail data object and set fields
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.Loading);
                laborDetail.setFunction("Loading");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setRegionGoalRate((Integer) record[2]);
                }
                if (record[3] != null) {
                    laborDetail.setAssignmentNumber(record[3].toString());
                }
                if (record[4] != null) {
                    laborDetail.setStartTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setEndTime((Date) record[5]);
                }
                if (record[6] != null) {
                    laborDetail.setDuration((Long) record[6]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[6]));
                }
                if (record[7] != null) {
                    laborDetail.setCount((Long) record[7]);
                }
                if (record[8] != null) {
                    laborDetail.setRate((Double) record[8]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        case ARCHIVE_LOADING:
            for (Object[] record : records) {

                // create new labor detail detail data object and set fields
                LaborDetailDetailData laborDetail = new LaborDetailDetailData();
                laborDetail.setRecordType(recordType);
                laborDetail.setAction(OperatorLaborActionType.Loading);
                laborDetail.setFunction("Loading");

                // get values from Object array
                if (record[0] != null) {
                    laborDetail.setRegionNumber(record[0].toString());
                }
                if (record[1] != null) {
                    laborDetail.setRegionName((String) record[1]);
                }
                if (record[2] != null) {
                    laborDetail.setAssignmentNumber(record[2].toString());
                }
                if (record[3] != null) {
                    laborDetail.setStartTime((Date) record[3]);
                }
                if (record[4] != null) {
                    laborDetail.setEndTime((Date) record[4]);
                }
                if (record[5] != null) {
                    laborDetail.setDuration((Long) record[5]);
                    laborDetail.setTime(ReportTimeFormatter
                        .formatTime((Long) record[5]));
                }
                if (record[6] != null) {
                    laborDetail.setCount((Long) record[6]);
                }
                if (record[7] != null) {
                    laborDetail.setRate((Double) record[7]);
                }

                // add new record to unprocesedLaborDetails
                unprocessedLaborDetails.add(laborDetail);
            }
            break;
        default:
            break;
        }
    }

    // following four methods for JRDataSourceProvider for iReports

    /**
     * Constructor.
     * @param beanClass Class
     */
    public LaborDetailReportImplRoot(Class<?> beanClass) {
        super(LaborDetailReportData.class);
    }

    /**
     * Constructor.
     */
    public LaborDetailReportImplRoot() {
        super(LaborDetailReportData.class);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {

        Map<String, Object> values = new HashMap<String, Object>();
        return this.getDataSource(values);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 