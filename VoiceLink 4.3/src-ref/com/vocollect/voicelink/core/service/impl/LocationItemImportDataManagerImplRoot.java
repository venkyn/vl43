/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.LocationItemImportDataDAO;
import com.vocollect.voicelink.core.model.LocationItemImportData;
import com.vocollect.voicelink.core.service.LocationItemImportDataManager;

import java.util.List;



/**
 * Additional service methods for the <code>LocationItemImportData</code> model object.
 * 
 * @author jtauberg
 *
 */
public abstract class LocationItemImportDataManagerImplRoot extends
        GenericManagerImpl<LocationItemImportData, LocationItemImportDataDAO>
        implements LocationItemImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LocationItemImportDataManagerImplRoot(LocationItemImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     */
    public List<LocationItemImportData> listLocationItemBySiteName(String siteName)
        throws DataAccessException {
            return getPrimaryDAO().listLocationItemBySiteName(siteName);
    }
    
    /**
     * Update all LocationItemImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }
    


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 