/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Resequencable;
import com.vocollect.voicelink.core.service.ResequenceManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Additional service methods for the resequencable model objects.
 * 
 * @author bnichols
 */
public abstract class ResequenceManagerImplRoot implements ResequenceManager {
       

    /**
     * Implmentation of list assignments to resequence for the given region.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ResequenceManager#executeResequence(ArrayList sequenceList, Long regionId)
     */
    public void executeResequence(ArrayList<Long> resequenceList, List<DataObject> results, GenericManager manager) 
        throws DataAccessException, BusinessRuleException {
        ArrayList<Long> sequenceNumberList = new ArrayList<Long>();

        // Update the resequenceList and create the sequenceNumber list
        ArrayList<Long> ids = new ArrayList<Long>();
        Iterator<DataObject> assignmentIter = results.iterator();
        while (assignmentIter.hasNext()) {
            Resequencable assignment = (Resequencable) assignmentIter.next();
            ids.add(assignment.getId());
            sequenceNumberList.add(assignment.getSequenceNumber());
        }
        for (int i = 0; i < resequenceList.size(); i++) {
            if (!ids.contains(resequenceList.get(i))) {
                resequenceList.remove(i);
            }
        }

        //Iterate over the resequenceList to make the properly sorted return list
        for (int i = 0; i < resequenceList.size(); i++) {
            Long id = resequenceList.get(i);
            //Find this assignment, set its new sequence number based on the 
            //sequenceNumberList  
            //(NewSequenceNumber(assignment) = SequenceNumberList[index of assignment in NewSequenceNumber])
            assignmentIter = results.iterator();
            while (assignmentIter.hasNext()) {
                Resequencable assignment = (Resequencable) assignmentIter.next();
                if (assignment.getId().longValue() == id.longValue()) {
                    assignment.setSequenceNumber(sequenceNumberList.get(i));
                    manager.save(assignment); 
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.ResequenceManagerRoot#setupResequenceResults(java.util.ArrayList, java.util.List)
     */
    public List<DataObject> setupResequenceResults(ArrayList<Long> resequenceList, 
                                                   List<DataObject> results) {
        ArrayList<Long> sequenceNumberList = new ArrayList<Long>();
        List<DataObject> finalList = new ArrayList<DataObject>();

        // Update the resequenceList and create the sequenceNumber list
        ArrayList<Long> ids = new ArrayList<Long>();
        Iterator<DataObject> assignmentIter = results.iterator();
        while (assignmentIter.hasNext()) {
            Resequencable assignment = (Resequencable) assignmentIter.next();
            ids.add(assignment.getId());
            sequenceNumberList.add(assignment.getSequenceNumber());
        }
        for (int i = 0; i < resequenceList.size(); i++) {
            if (!ids.contains(resequenceList.get(i))) {
                resequenceList.remove(i);
            }
        }

        //Iterate over the resequenceList to make the properly sorted return list
        for (int i = 0; i < resequenceList.size(); i++) {
            Long id = resequenceList.get(i);
            //Find this assignment, set its new sequence number based on the 
            //sequenceNumberList  
            //(NewSequenceNumber(assignment) = SequenceNumberList[index of assignment in NewSequenceNumber])
            assignmentIter = results.iterator();
            while (assignmentIter.hasNext()) {
                Resequencable assignment = (Resequencable) assignmentIter.next();
                if (assignment.getId().longValue() == id.longValue()) {
                    assignment.setNewSequenceNumber(sequenceNumberList.get(i));
                    finalList.add((DataObject) assignment);
                }
            }
        }
        
        return finalList;        
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 