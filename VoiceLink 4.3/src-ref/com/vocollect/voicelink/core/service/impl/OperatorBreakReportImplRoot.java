/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Implementation of Operator breaks Report.
 *
 * @author krishna.udupi
 */
public abstract class OperatorBreakReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager {

    private static final Long HOURS = 3600000L;
    private static final Long MINUTES = 60000L;

    /**
     * Bean for report.
     *
     * @author krishna.udupi
     */
    public class ReportOperatorBreak {
        private String operatorID;
        private String name;
        private String breakType;
        private Long durationHours;
        private Long durationMinutes;
        private Date startTime;
        private Date endTime;

        /**
         * Getter for the operatorID property.
         * @return String value of the property
         */
        public String getOperatorID() {
            return operatorID;
        }

        /**
         * Setter for the operatorID property.
         * @param operatorID the new operatorID value
         */
        public void setOperatorID(String operatorID) {
            this.operatorID = operatorID;
        }

        /**
         * Getter for the name property.
         * @return String value of the property
         */
        public String getName() {
            return name;
        }

        /**
         * Setter for the name property.
         * @param name the new name value
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * Getter for the breakType property.
         * @return String value of the property
         */
        public String getBreakType() {
            return breakType;
        }

        /**
         * Setter for the breakType property.
         * @param breakType the new breakType value
         */
        public void setBreakType(String breakType) {
            this.breakType = breakType;
        }

        /**
         * Getter for the startTime property.
         * @return Date value of the property
         */
        public Date getStartTime() {
            return startTime;
        }

        /**
         * Setter for the startTime property.
         * @param startTime the new startTime value
         */
        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        /**
         * Getter for the endTime property.
         * @return Date value of the property
         */
        public Date getEndTime() {
            return endTime;
        }

        /**
         * Setter for the endTime property.
         * @param endTime the new endTime value
         */
        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }


        /**
         * Getter for the durationHours property.
         * @return Long value of the property
         */
        public Long getDurationHours() {
            return durationHours;
        }


        /**
         * Setter for the durationHours property.
         * @param durationHours the new durationHours value
         */
        public void setDurationHours(Long durationHours) {
            this.durationHours = durationHours;
        }


        /**
         * Getter for the durationMinutes property.
         * @return Long value of the property
         */
        public Long getDurationMinutes() {
            return durationMinutes;
        }


        /**
         * Setter for the durationMinutes property.
         * @param durationMinutes the new durationMinutes value
         */
        public void setDurationMinutes(Long durationMinutes) {
            this.durationMinutes = durationMinutes;
        }

    }


    //Managers for queries
    private OperatorManager operatorManager;
    private BreakTypeManager breakTypeManager;
    private OperatorLaborManager operatorLaborManager;
    private DataTranslationManager dataTranslationManager;


    /**
     * Getter for the dataTranslationManager property.
     * @return DataTranslationManager value of the property
     */
    public DataTranslationManager getDataTranslationManager() {
        return dataTranslationManager;
    }

    /**
     * Setter for the dataTranslationManager property.
     * @param dataTranslationManager the new dataTranslationManager value
     */
    public void setDataTranslationManager(DataTranslationManager dataTranslationManager) {
        this.dataTranslationManager = dataTranslationManager;
    }


    /**
     * Getter for the breakTypeManager property.
     * @return BreakTypeManager value of the property
     */
    public BreakTypeManager getBreakTypeManager() {
        return breakTypeManager;
    }




    /**
     * Setter for the breakTypeManager property.
     * @param breakTypeManager the new breakTypeManager value
     */
    public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
        this.breakTypeManager = breakTypeManager;
    }




    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }




    /**
     * Setter for the operatorLaborManager property.
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }



    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }



    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    public JRDataSource getDataSource(Map<String, Object> values) throws Exception {
        List<ReportOperatorBreak> reportOperatorBreakList = new ArrayList<ReportOperatorBreak>();

        List<Object[]> oblRecords = null;

        //Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        Set<String> operatorId = (Set<String>) values.get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        String breakIdName = (String) values.get("BREAK_TYPE");
        Long duration = (Long) values.get("MIN_DURATION");
        String operator = (String) values.get(ReportUtilities.OPERATOR_ALL);
        Long durationInMilliSeconds = null;

        // convert in milliseconds only if duration is not null
        if (null != duration) {
            durationInMilliSeconds = duration * MINUTES;
        }

        String breakTypeIdForQuery = new String();

        if ((breakIdName == null)) {
            breakTypeIdForQuery = null;
        } else {
            breakTypeIdForQuery = breakIdName;
        }

        oblRecords = getOperatorLaborManager().listBreaksLongerThan(startTime, endTime,
                durationInMilliSeconds, operatorId, breakTypeIdForQuery, operator);

        if (breakIdName == null) {
            values.put("BREAK_TYPE", ResourceUtil.getLocalizedKeyValue("report.all"));
        } else {
            values.put("BREAK_TYPE", getDataTranslationManager().translate(breakIdName));
        }


        for (Object[] obj : oblRecords) {
             int objectCounter = 0;
             ReportOperatorBreak reportOperatorBreak = new ReportOperatorBreak();
             Timestamp startTimestamp = (Timestamp) obj[objectCounter];
             Timestamp endTimeStamp = (Timestamp) obj[++objectCounter];
             Long durationInMillis = (Long) obj[++objectCounter];
             Long durationInMinutes = getMinutes(durationInMillis);
             Long durationInHours = getHours(durationInMillis);
             String operID = (String) obj[++objectCounter];
             String operatorName = (String) obj[++objectCounter];
             Long breakTypeID = (Long) obj[++objectCounter];
             String breakTypeName = getDataTranslationManager().
                                           translate(getBreakTypeManager().get(breakTypeID).getName());

             reportOperatorBreak.setBreakType(breakTypeName);
             reportOperatorBreak.setName(operatorName);
             reportOperatorBreak.setOperatorID(operID);
             reportOperatorBreak.setDurationMinutes(durationInMinutes);
             reportOperatorBreak.setDurationHours(durationInHours);
             reportOperatorBreak.setStartTime(startTimestamp);
             reportOperatorBreak.setEndTime(endTimeStamp);

             reportOperatorBreakList.add(reportOperatorBreak);
        }

        return new JRBeanCollectionDataSource(reportOperatorBreakList);
    }

    /**
     * Convert value parameter to long checking for null.
     *
     * @param value - value to convert
     * @return - Long or null
     */
    public Long convertParamToLong(String value) {
        if (value == null) {
            return null;
        } else {
            return Long.valueOf(value);
        }

    }

    /**
     * Convert milliseconds to total hours.
     *
     * @param duration - number of milliseconds
     * @return - total number of hours
     */
    public Long getHours(Long duration) {
        return duration / HOURS;
    }

    /**
     * Convert millisecods to minutes (minus hours).
     * @param duration - total duration
     * @return - minutes (minus total hours)
     */
    private Long getMinutes(Long duration) {
        Long minutes = duration - (getHours(duration) * HOURS);
        return minutes / MINUTES;
    }
    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<ReportOperatorBreak> reportList = new ArrayList<ReportOperatorBreak>();
        return new JRBeanCollectionDataSource(reportList);
    }



    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * @param beanClass - class report is based on.
     */
    public OperatorBreakReportImplRoot(Class<ReportOperatorBreak> beanClass) {
        super(ReportOperatorBreak.class);
    }

    /**
     * Constructor.
     */
    public OperatorBreakReportImplRoot() {
        super(ReportOperatorBreak.class);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 