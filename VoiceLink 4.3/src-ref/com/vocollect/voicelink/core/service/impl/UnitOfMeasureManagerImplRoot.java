/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.UnitOfMeasureDAO;
import com.vocollect.voicelink.core.model.UnitOfMeasure;
import com.vocollect.voicelink.core.service.UnitOfMeasureManager;

import java.util.List;

/**
 * @author pkolonay
 *
 */
public abstract class UnitOfMeasureManagerImplRoot extends
        GenericManagerImpl<UnitOfMeasure, UnitOfMeasureDAO> implements
        UnitOfMeasureManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public UnitOfMeasureManagerImplRoot(UnitOfMeasureDAO primaryDAO) {
        super(primaryDAO);
    }
    
    
    List<UnitOfMeasure> listUnitsOfMeasure()  throws DataAccessException {
        return this.getPrimaryDAO().listUnitsOfMeasure();
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 