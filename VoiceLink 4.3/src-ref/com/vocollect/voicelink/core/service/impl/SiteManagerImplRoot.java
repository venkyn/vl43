/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.importer.ImportSetupException;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.ImportSetupManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;

/**
 * Implementation of SiteManager interface for VoiceLink. This overrides save in
 * order to create a default workgroup for a newly created site.
 *
 * @author Nick Kocur
 */
public abstract class SiteManagerImplRoot
    extends com.vocollect.epp.service.impl.SiteManagerImpl {

    private WorkgroupManager workgroupManager;

    //Manager to update import/export setup when new site gets added or deleted
    private ImportSetupManager importSetupManager = null;

    /**
     * Constructor.
     * @param primaryDAO the primary DAO (the instantiation for the persistent
     *            class)
     */
    public SiteManagerImplRoot(SiteDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * Setter for the importSetupManager property.
     * @param importSetupManager the new importSetupManager value
     */
    public void setImportSetupManager(ImportSetupManager importSetupManager) {
        this.importSetupManager = importSetupManager;
    }

    /**
     * Getter for the importSetupManager property.
     * @return importSetupManager the importSetupManager value
     */
    public ImportSetupManager getImportSetupManager() {
        return this.importSetupManager;
    }



    /**
     * Calls parent to save site, then creates a default workgroup for this site
     * (if it is new). Also update the import/export setup
     * @param site the site to save
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     * @return Object that was saved in parent
    */
    @Override
    public Object save(Site site)
        throws BusinessRuleException,
        DataAccessException {
        // only add a tag if we are creating
        boolean bNew = site.isNew();

        // These two variables used only when editing the site
        Site oldSite = null;
        String oldSiteName = null;

        // If editing the site, keep the old site name info
        if (!bNew) {
            oldSite = super.get(site.getId());
            oldSiteName = oldSite.getName();
            this.getPrimaryDAO().detach(oldSite);
        }

        Object retVal = super.save(site);
        if (bNew) {
            createWorkgroup(site, oldSiteName);
        }
        setUpImportExport(site, bNew, oldSiteName);

        return retVal;
    }


    /**
     * On creating a site a default workgroup for this site is created (if it is
     * new). Also update the import/export setup(If site name is edited).
     * @param site the site that was created or edited
     * @param oldSiteName - if editing the site name old site name
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     */
    public void createWorkgroup(Site site, String oldSiteName)
                                  throws BusinessRuleException, DataAccessException {
            // create default workgroup
        Workgroup wg = getWorkgroupManager().createDefaultWorkgroup();

        SiteContext sc = SiteContextHolder.getSiteContext();
        sc.setSite(wg, site);
     }

    /**
     * Update the import/export setup(If site name is edited).
     * @param site the site that was created or edited
     * @param bNew - the created site is new or editing already created site
     * @param oldSiteName - if editing the site name old site name
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     */
    public void setUpImportExport(Site site, boolean bNew, String oldSiteName)
               throws BusinessRuleException, DataAccessException {
        if (bNew) {

            try {
                this.getImportSetupManager().addNewSite(site.getName());
            } catch (ImportSetupException e) {
                throw new BusinessRuleException(null, new UserMessage(
                    "site.add.import.message"));
            }
        } else {
            if (!(oldSiteName.equals(site.getName()))) {
                // Site name is modified. Remove the old site and create the
                // import for
                // changed name
                try {
                    this.getImportSetupManager().deleteSite(oldSiteName);
                } catch (ImportSetupException e) {
                    throw new BusinessRuleException(null, new UserMessage(
                        "site.delete.import.message"));
                }
                try {
                    this.getImportSetupManager().addNewSite(site.getName());
                } catch (ImportSetupException e) {
                    throw new BusinessRuleException(null, new UserMessage(
                        "site.add.import.message"));
                }
            }
        }
    }


    /**
     * Calls parent to delete site, then update the import/export setup.
     * @param site the site to delete
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     * @return Object that was saved in parent
     */
    @Override
    public Object delete(Site site)
    throws BusinessRuleException, DataAccessException {
    	
    	SiteContext sc = SiteContextHolder.getSiteContext();    	
    	for (Workgroup wg : getWorkgroupManager().getAll()) {
    		sc.removeSite(wg, site);
    	}

        Object retVal = super.delete(site);
        try {
            this.getImportSetupManager().deleteSite(site.getName());
        } catch (ImportSetupException e) {
            throw new BusinessRuleException(null, new UserMessage("site.delete.import.message"));
        }

        return retVal;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 