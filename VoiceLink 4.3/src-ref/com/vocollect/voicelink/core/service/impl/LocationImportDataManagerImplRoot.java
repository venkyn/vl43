/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.LocationImportDataDAO;
import com.vocollect.voicelink.core.model.LocationImportData;
import com.vocollect.voicelink.core.service.LocationImportDataManager;

import java.util.List;



/**
 * Additional service methods for the <code>LocationImportData</code> model object.
 * 
 * @author jtauberg
 *
 */
public abstract class LocationImportDataManagerImplRoot extends
        GenericManagerImpl<LocationImportData, LocationImportDataDAO> implements
        LocationImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LocationImportDataManagerImplRoot(LocationImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     */
    public List<LocationImportData> listLocationBySiteName(String siteName) throws DataAccessException
    {
        return getPrimaryDAO().listLocationBySiteName(siteName);
    }
    
    /**
     * Update all LocationImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }
    


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 