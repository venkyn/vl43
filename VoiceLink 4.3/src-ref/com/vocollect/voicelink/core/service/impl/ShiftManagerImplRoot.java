/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.ShiftDAO;
import com.vocollect.voicelink.core.model.Shift;
import com.vocollect.voicelink.core.service.ShiftManagerRoot;


/**
 * Additional service methods for the <code>VehicleType</code> model object.
 * 
 * @author smittal
 */
public class ShiftManagerImplRoot extends
    GenericManagerImpl<Shift, ShiftDAO> implements
    ShiftManagerRoot {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ShiftManagerImplRoot(ShiftDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.ShiftManagerRoot#verifyUniquenessByName(com.vocollect.voicelink.core.model.Shift)
     */
    public void verifyUniquenessByName(Shift shift)
        throws BusinessRuleException, DataAccessException {
        boolean isNew = shift.isNew();
        // Uniqueness check for name
        Long nameUniquenessId = getPrimaryDAO().uniquenessByName(
            shift.getName());
        if (nameUniquenessId != null
            && (isNew || (!isNew && nameUniquenessId.longValue() != shift
                .getId().longValue()))) {
            throw new FieldValidationException("shift.name",
                shift.getName(), new UserMessage(
                    "shift.error.duplicateName", shift.getName()));
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 