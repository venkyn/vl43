/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.VehicleSafetyCheckDAO;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManagerRoot;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

import java.util.List;

/**
 * Additional service methods for the <code>VehicleType</code> model object.
 * 
 * @author kudupi
 */
public class VehicleSafetyCheckManagerImplRoot extends
    GenericManagerImpl<VehicleSafetyCheck, VehicleSafetyCheckDAO> implements
    VehicleSafetyCheckManagerRoot {

    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleSafetyCheckManagerImplRoot(VehicleSafetyCheckDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * @return the vehicleSafetyCheckResponseManager
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }

    /**
     * @param vscResponseManager the
     *            vehicleSafetyCheckResponseManager to set
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager vscResponseManager) {
        this.vehicleSafetyCheckResponseManager = vscResponseManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckManagerRoot#listAllCheckByType()
     */
    @Override
    public List<VehicleSafetyCheck> listAllCheckByVehicleType(Integer typeNumber)
        throws DataAccessException {
        return getPrimaryDAO().listAllChecksByVehicleType(typeNumber);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckManagerRoot#findSafetyCheckByNameAndVehicleType(java.lang.String, com.vocollect.voicelink.core.model.VehicleType)
     */
    @Override
    public VehicleSafetyCheck findSafetyCheckByNameAndVehicleType(String safetyCheck,
                                                           VehicleType vehicleType)
        throws DataAccessException {
        return getPrimaryDAO().findSafetyCheckByNameAndVehicleType(safetyCheck, vehicleType);
    }
    
    @Override
    public Object delete(VehicleSafetyCheck instance)
        throws BusinessRuleException, DataAccessException {
        if (getVehicleSafetyCheckResponseManager()
            .countVehiclSafetyCheckResponsePerSafetyCheck(instance).longValue() > 0) {
            throw new DataAccessException(
                CoreErrorCode.SAFETYCHECK_RESPONSES_EXISTS_FOR_VEHICLETYPE,
                new UserMessage("vsc.safetyCheck.response.error.exist",
                    instance.getDescription()), null);
        } 
        
        //Check is there are any associated operators with the corresponding vehicle
        boolean isOperatorActive = false;
        List<Vehicle> vehicles = instance.getVehicleType().getVehicles();
        for (Vehicle vehicle : vehicles) {
            if (!vehicle.getOperators().isEmpty()) {
                isOperatorActive = true;
                break;
            }
        }

        if (isOperatorActive) {
            throw new BusinessRuleException(
                CoreErrorCode.SAFETYCHECK_EXISTS_FOR_VEHICLETYPE,
                new UserMessage("vsc.safetyCheck.edit.error.vehicleType.inUse"));
        } else {
            //dis-associate from VT first and then attempt delete 
            List<VehicleSafetyCheck> checks = instance.getVehicleType().getSafetyChecks();
            checks.remove(instance);
            
            return super.delete(instance);
        }
    }

    @Override
    public Object save(VehicleSafetyCheck instance)
        throws BusinessRuleException, DataAccessException {

        validateDuplicacy(instance);
        return super.save(instance);
    }

    /**
     * Checks if the criteria fields are duplicate to any existing object. The
     * method will not work if hibernate is not lazy loading the safety checks
     * in vehicle type
     * @param instance The safety check instance
     */
    private void validateDuplicacy(VehicleSafetyCheck instance) throws BusinessRuleException {
        // Check if sequence is duplicate before saving
        VehicleType vehicleType = instance.getVehicleType();
        List<VehicleSafetyCheck> existingChecks = vehicleType.getSafetyChecks();

        for (VehicleSafetyCheck existingCheck : existingChecks) {
            if (existingCheck.getId().equals(instance.getId())) {
                continue;
            }
            
            if (existingCheck.getSequence().equals(instance.getSequence())) {
                throw new FieldValidationException(
                    "safetyCheck.sequence", instance.getSequence().toString(),
                    new UserMessage(
                        "vsc.safetyCheck.sequence.error.duplicate", instance
                            .getSequence().toString()));
            }

            if (existingCheck.getSpokenDescription().equals(
                instance.getSpokenDescription())) {
                throw new FieldValidationException(
                    "safetyCheck.spokenDescription", instance
                        .getSpokenDescription().toString(), new UserMessage(
                        "vsc.safetyCheck.spokenDescription.error.duplicate",
                        instance.getSpokenDescription().toString()));
            }

            if (existingCheck.getDescription()
                .equals(instance.getDescription())) {
                throw new FieldValidationException(
                    "safetyCheck.description", instance.getDescription()
                        .toString(), new UserMessage(
                        "vsc.safetyCheck.description.error.duplicate", instance
                            .getDescription().toString()));
            }

        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 