/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.dao.VehicleSafetyCheckResponseDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.SafetyCheckType;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckRepairActionType;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponseType;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author mraj
 * 
 */
public class VehicleSafetyCheckResponseManagerImplRoot
    extends
    GenericManagerImpl<VehicleSafetyCheckResponse, VehicleSafetyCheckResponseDAO>
    implements VehicleSafetyCheckResponseManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleSafetyCheckResponseManagerImplRoot(VehicleSafetyCheckResponseDAO primaryDAO) {
        super(primaryDAO);
    }

    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot#executeCheckResponse(com.vocollect.voicelink.core.model.Operator, java.lang.String, int, com.vocollect.voicelink.core.model.Vehicle, java.util.List, java.lang.String, java.util.Date)
     */
    @Override
    public List<VehicleSafetyCheckResponse> executeCheckResponse(Operator operator,
                                     String safetyCheck,
                                     int repairAction,
                                     Vehicle vehicle,
                                     List<VehicleSafetyCheck> safetyChecks,
                                     String chekResponse,
                                     Date checkDate) throws DataAccessException {
        VehicleSafetyCheck check = null;
        if (!StringUtil.isNullOrEmpty(safetyCheck)) {
            //in case of all checks pass
            for (VehicleSafetyCheck vehicleSafetyCheck : safetyChecks) {
                if (vehicleSafetyCheck.getSpokenDescription().equals(
                    safetyCheck)) {
                    check = vehicleSafetyCheck;
                    break;
                }
            }
        }
        
        List<VehicleSafetyCheckResponse> newResponses = new ArrayList<VehicleSafetyCheckResponse>();
        if (repairAction == VehicleSafetyCheckRepairActionType.QuickRepair.toValue()) {
            Integer integerValue = Integer.valueOf(VehicleSafetyCheckResponseType.INITIAL.toValue());
            
            Integer sequence = check != null ? check.getSequence() : null;
            //Find list of safety checks
            List<VehicleSafetyCheck> currentSafetyCheck = new ArrayList<VehicleSafetyCheck>();
            currentSafetyCheck.add(check);
            List<VehicleSafetyCheckResponse> quickRepairedResponses = getPrimaryDAO()
                .listIntermediateSafetyChecks(operator, currentSafetyCheck, vehicle, sequence);
            if (quickRepairedResponses.isEmpty()) {
                newResponses.add(createCheckResponse(
                    vehicle, check, integerValue,
                    VehicleSafetyCheckRepairActionType.QuickRepair, operator,
                    checkDate, null));
            } else {
                // Do nothing. Since a record already exists for Quick Repair.
                // All we are ensuring is not to create a new record for the second iteration.
            }
        } else if (StringUtil.isNullOrEmpty(safetyCheck)
            || repairAction == VehicleSafetyCheckRepairActionType.NewEquipment.toValue()) {
            newResponses = createAllPerformedCheckResponses(
                vehicle, check, operator, checkDate, safetyChecks,
                getNextGroupId());
        } else if (!StringUtil.isNullOrEmpty(chekResponse)
            && check.getResponseType() == SafetyCheckType.NUMERIC) {
            //Case of numeric response
            Integer integerValue = Integer.valueOf(chekResponse);
            newResponses.add(createCheckResponse(
                vehicle, check, integerValue,
                VehicleSafetyCheckRepairActionType.NoAction, operator,
                checkDate, null));
        } else if (StringUtil.isNullOrEmpty(safetyCheck)
            || Integer.valueOf(chekResponse) == VehicleSafetyCheckResponseType.NOBUTCONTINUE
                .toValue()) {
            newResponses.add(createCheckResponse(vehicle, check,
                Integer.valueOf(chekResponse),
                VehicleSafetyCheckRepairActionType.NoAction, operator,
                checkDate, null));
        }
        
        return newResponses; //For consumption of test cases
    }
    
    /**
     * Method to invoke creation/updation of safety check responses based on
     * repair action and response type
     * @param vehicle - the vehicle
     * @param failedCheck - the failed check name, if any
     * @param operator - the operator performing the checks
     * @param checkDate - the date time when the check was performed
     * @param safetyChecks - list of safety checks associated to Vehicle type
     * @param groupId - internal groupId for associated responses
     * @return list of safety check responses for the vehicle
     * @throws DataAccessException - thrown when any exception occurs
     */
    private List<VehicleSafetyCheckResponse> createAllPerformedCheckResponses(Vehicle vehicle,
                                                  VehicleSafetyCheck failedCheck,
                                                  Operator operator,
                                                  Date checkDate, List<VehicleSafetyCheck> safetyChecks, Long groupId)
        throws DataAccessException {
        List<VehicleSafetyCheckResponse> newResponses = new ArrayList<VehicleSafetyCheckResponse>(safetyChecks.size());
        
        Integer sequence = failedCheck != null ? failedCheck.getSequence() : null;
        //Find list of safety checks
        List<VehicleSafetyCheckResponse> quickRepairedResponses = getPrimaryDAO()
            .listIntermediateSafetyChecks(operator, safetyChecks, vehicle, sequence);
        
        //Create a set of already created quick repaired check responses 
        Set<VehicleSafetyCheck> quickRepairedChecks = new HashSet<VehicleSafetyCheck>();
        for (VehicleSafetyCheckResponse vehicleSafetyCheckResponse : quickRepairedResponses) {
            quickRepairedChecks.add(vehicleSafetyCheckResponse.getSafetyCheck());
        }
        
        if (failedCheck == null) {
            // All pass
            for (VehicleSafetyCheck vehicleSafetyCheck : safetyChecks) {
                if (quickRepairedChecks.contains(vehicleSafetyCheck)) {
                    newResponses.add(updateQuickRepairedResponse(
                        vehicleSafetyCheck,
                        VehicleSafetyCheckResponseType.YES, checkDate,
                        quickRepairedResponses, groupId));
                } else {
                    newResponses.add(createCheckResponse(
                        vehicle, vehicleSafetyCheck,
                        VehicleSafetyCheckResponseType.YES.toValue(),
                        VehicleSafetyCheckRepairActionType.NoAction, operator,
                        checkDate, groupId));
                }
            }
        } else {
            // One failed
            for (VehicleSafetyCheck vehicleSafetyCheck : safetyChecks) {
                // Assuming all checks are in sequence
                VehicleSafetyCheckResponseType responseType = VehicleSafetyCheckResponseType.YES;
                VehicleSafetyCheckRepairActionType repairActionType = VehicleSafetyCheckRepairActionType.NoAction;
                if (vehicleSafetyCheck.equals(failedCheck)) {
                    responseType = VehicleSafetyCheckResponseType.NO;
                    repairActionType = VehicleSafetyCheckRepairActionType.NewEquipment;
                }
                
                if (quickRepairedChecks.contains(vehicleSafetyCheck)) {
                    newResponses.add(updateQuickRepairedResponse(
                        vehicleSafetyCheck,
                        responseType, checkDate,
                        quickRepairedResponses, groupId));
                } else {
                    newResponses.add(createCheckResponse(
                        vehicle, vehicleSafetyCheck,
                        responseType.toValue(),
                        repairActionType, operator,
                        checkDate, groupId));
                }
                
                if (vehicleSafetyCheck.equals(failedCheck)) {
                    break;
                }
            }
        }
        return newResponses;
    }
    
    /**
     * Method to create a new safety check response based on information passed as parameters
     * @param vehicle - the vehicle
     * @param safetyCheck - the safety check
     * @param checkResponse - the check response sent by operator
     * @param repairAction - the repair action
     * @param operator -  the operator performing the checks
     * @param checkDate - the date time when the check was performed 
     * @param groupId - internal groupId for associated responses
     * @return new created VehicleSafetyCheckResponse object
     * @throws DataAccessException
     */
    private VehicleSafetyCheckResponse createCheckResponse(Vehicle vehicle,
                                     VehicleSafetyCheck safetyCheck,
                                     Integer checkResponse,
                                     VehicleSafetyCheckRepairActionType repairAction,
                                     Operator operator,
                                     Date checkDate, Long groupId) throws DataAccessException {
        VehicleSafetyCheckResponse response = new VehicleSafetyCheckResponse();
        response.setVehicle(vehicle);
        response.setOperator(operator);
        response.setSafetyCheck(safetyCheck);
        response.setCheckResponse(checkResponse);
        response.setRepairAction(repairAction);
        response.setInternalGroupId(groupId);
        response.setCreatedDate(checkDate); //TODO replace with new field
        getPrimaryDAO().save(response);
        return response;
    }
    
    /**
     * @param check - the quick repaired safety check
     * @param responseType - the response type
     * @param checkDate - the date time when the check was performed 
     * @param responses - the quick repaired responses
     * @param groupId - internal groupId for associated responses
     * @return updated VehicleSafetyCheckResponse
     */
    private VehicleSafetyCheckResponse updateQuickRepairedResponse(VehicleSafetyCheck check,
                                                                   VehicleSafetyCheckResponseType responseType,
                                                                   Date checkDate,
                                                                   List<VehicleSafetyCheckResponse> responses,
                                                                   Long groupId) {
        for (VehicleSafetyCheckResponse response : responses) {
            if (response.getSafetyCheck().equals(check)) {
                // Update unless numeric or continue on no response exist
                if (response.getCheckResponse() < 0
                    && response.getCheckResponse() != VehicleSafetyCheckResponseType.NOBUTCONTINUE
                        .toValue()) {
                    response.setCreatedDate(checkDate); // TODO replace with new
                                                        // field
                }

                if (response.getCheckResponse() < 0
                    && response.getCheckResponse() != VehicleSafetyCheckResponseType.NOBUTCONTINUE
                        .toValue()) {
                    response.setCheckResponse(responseType.toValue());
                }

                response.setInternalGroupId(groupId);
                return response;
            }
        }

        return null;
    }
    
    /**
     * Method to get the next internal group Id, to be associated with check
     * responses
     * @return next internal group id.
     */
    private synchronized Long getNextGroupId() throws DataAccessException {
        Long groupId = getPrimaryDAO().maxGroupId();
        if (groupId == null) {
            groupId = 0L;
        }
        
        return ++groupId;
    }
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot
     * #
     * executeClearDefaultResponses(com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public void executeClearDefaultResponses(Operator operator)
        throws DataAccessException {
        List<VehicleSafetyCheckResponse> responses = getPrimaryDAO()
            .listPreviousResponsesByOperator(operator);
        
        for (VehicleSafetyCheckResponse response : responses) {
            if (response.getCheckResponse().intValue() == VehicleSafetyCheckResponseType.INITIAL
                .toValue()
                || (response.getCheckResponse().intValue() == VehicleSafetyCheckResponseType.NOBUTCONTINUE
                    .toValue() && response
                    .getInternalGroupId() == null)
                || (response.getCheckResponse() > 0 && response
                    .getInternalGroupId() == null)) {
                getPrimaryDAO().delete(response);
            }
        }
        
    }


    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot#listSafetyChecksByVehicleType(com.vocollect.voicelink.core.model.VehicleType)
     */
    @Override
    public List<VehicleSafetyCheckResponse> listSafetyChecksByVehicleType(VehicleType vehicleType)
        throws DataAccessException {
        return getPrimaryDAO().listSafetyChecksByVehicleType(vehicleType);
    }


    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot#getVehiclSafetyCheckResponseCount(com.vocollect.voicelink.core.model.Vehicle)
     */
    @Override
    public Number countVehiclSafetyCheckResponsePerVehicle(Vehicle vehicle)
        throws DataAccessException {
        return getPrimaryDAO().countVehiclSafetyCheckResponsePerVehicle(vehicle);
    }


    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot#countVehiclSafetyCheckResponsePerSafetyCheck(com.vocollect.voicelink.core.model.VehicleSafetyCheck)
     */
    @Override
    public Number countVehiclSafetyCheckResponsePerSafetyCheck(VehicleSafetyCheck safetyCheck)
        throws DataAccessException {
        return getPrimaryDAO().countVehiclSafetyCheckResponsePerSafetyCheck(safetyCheck);
    }
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManagerRoot#listVehicleSafetyCheckResponseForReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.lang.Integer, java.lang.String, java.util.Set, java.util.Date, java.util.Date)
     */
    @Override
    public List<VehicleSafetyCheckResponse> listVehicleSafetyCheckResponseForReport(QueryDecorator queryDecorator,
                                                                                    Integer vehicleType,
                                                                                    String vehicle,
                                                                                    Set<String> operatorIdentifiers,
                                                                                    Date startDate,
                                                                                    Date endDate)
        throws DataAccessException {
        
        WhereClause whereClause = new WhereClause();
        if (!(operatorIdentifiers == null || operatorIdentifiers.isEmpty())) {
            whereClause.add(
                operatorIdentifiers, "obj.operator.common.operatorIdentifier");
        }
        
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }
        
        List<VehicleSafetyCheckResponse> responses = new ArrayList<VehicleSafetyCheckResponse>();
        responses.addAll(getPrimaryDAO()
            .listVehicleSafetyCheckResponseForReport(
                queryDecorator, vehicleType, vehicle, startDate, endDate));
        return responses;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 