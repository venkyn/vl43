/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.voicelink.selection.service.ArchivePickManager;
import com.vocollect.voicelink.selection.service.PickManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Implementation of Shorted Locations Report.
 *
 * @author krishna.udupi
 */
public abstract class ShortedLocationReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager {

    private static final int SCANNED_LOCATION = 0;
    private static final int ITEM_NUMBER = 1;
    private static final int SHORT_COUNT = 2;
    private static final int LAST_SHORTED = 3;
    
    /**
     * Bean for report. 
     *
     * @author krishna.udupi
     */
    public class ShortedLocationItem 
    implements Comparable<ShortedLocationItem> {
        private String scannedLocation;
        private String itemNumber;
        private Long shortedCount;
        private Date lastShorted;
        
        /**
         * Getter for the scannedLocation property.
         * @return String value of the property
         */
        public String getScannedLocation() {
            return scannedLocation;
        }
        
        /**
         * Setter for the scannedLocation property.
         * @param scannedLocation the new scannedLocation value
         */
        public void setScannedLocation(String scannedLocation) {
            this.scannedLocation = scannedLocation;
        }
        
        /**
         * Getter for the itemNumber property.
         * @return String value of the property
         */
        public String getItemNumber() {
            return itemNumber;
        }
        
        /**
         * Setter for the itemNumber property.
         * @param itemNumber the new itemNumber value
         */
        public void setItemNumber(String itemNumber) {
            this.itemNumber = itemNumber;
        }
        
        /**
         * Getter for the shortedCount property.
         * @return Long value of the property
         */
        public Long getShortedCount() {
            return shortedCount;
        }
        
        /**
         * Setter for the shortedCount property.
         * @param shortedCount the new shortedCount value
         */
        public void setShortedCount(Long shortedCount) {
            this.shortedCount = shortedCount;
        }
        
        /**
         * Getter for the lastShorted property.
         * @return Date value of the property
         */
        public Date getLastShorted() {
            return lastShorted;
        }
        
        /**
         * Setter for the lastShorted property.
         * @param lastShorted the new lastShorted value
         */
        public void setLastShorted(Date lastShorted) {
            this.lastShorted = lastShorted;
        }

        /**
         * {@inheritDoc}
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(ShortedLocationItem o) {
            return o.getShortedCount().compareTo(this.getShortedCount());
        }
    }
    

    //Managers for queries
    private PickManager pickManager;
    private ArchivePickManager archivePickManager;
    
    /**
     * Getter for the pickManager property.
     * @return PickManager value of the property
     */
    public PickManager getPickManager() {
        return pickManager;
    }

    
    /**
     * Setter for the pickManager property.
     * @param pickManager the new pickManager value
     */
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }

    
    /**
     * Getter for the archivePickManager property.
     * @return ArchivePickManager value of the property
     */
    public ArchivePickManager getArchivePickManager() {
        return archivePickManager;
    }


    /**
     * Setter for the archivePickManager property.
     * @param archivePickManager the new archivePickManager value
     */
    public void setArchivePickManager(ArchivePickManager archivePickManager) {
        this.archivePickManager = archivePickManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values) throws Exception {
        Map<String, ShortedLocationItem> shortedLocations = 
            new HashMap<String, ShortedLocationItem>();

        //Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        Long count = (Long) values.get("LOCATION_COUNT");
        String siteId = (String) values.get("SITE");
        
        //Get Archive shorts between date range
        List<Object[]> shorted = getArchivePickManager()
            .listShortedLocations(startTime, endTime, Long.parseLong(siteId));
        
        addToMap(shortedLocations, shorted);
        
        //Get live data shorts between date range
        shorted = getPickManager()
            .listShortedLocations(startTime, endTime);
        
        addToMap(shortedLocations, shorted);
        
        return new JRBeanCollectionDataSource(getTopShorted(shortedLocations,
                                                            count));
    }

    /**
     * Build list to return to report.
     * @param count - number of locations user is requesting.
     * @param shortedLocations - map of locations to turn into list.
     * @return - list of locations based on top count.
     */
    protected List<ShortedLocationItem> getTopShorted(
        Map<String, ShortedLocationItem> shortedLocations, Long count) {
        List<ShortedLocationItem> reportList = new ArrayList<ShortedLocationItem>();
        List<ShortedLocationItem> fullList = 
            new ArrayList<ShortedLocationItem>(shortedLocations.values());
        Collections.sort(fullList);
        
        //Loop through list adding items to report list until the 
        //specified count is reached. 
        Long shortCounts = 1L;
        Long lastCount = 0L;
        //TODO This loop should be rewritten for readability.
        for (ShortedLocationItem li : fullList) {
            //check if loop should stop, if a tie at last position, then
            // keep including
            if (shortCounts++ > count 
                && !lastCount.equals(li.getShortedCount())) {
                break;
            }
            
            //Add item to report list
            reportList.add(li);
            lastCount = li.getShortedCount();
        }
        
        return reportList;
    }
    
    /**
     * Build map of location/items that where shorted.
     * 
     * @param shortedLocations - map of locations to add list to.
     * @param shorted - list of shorted items
     */
    protected void addToMap(Map<String, ShortedLocationItem> shortedLocations, 
                          List<Object[]> shorted) {
        
        for (Object[] obj : shorted) {
            String location = (String) obj[SCANNED_LOCATION];
            String item = (String) obj[ITEM_NUMBER];
            Long count = (Long) obj[SHORT_COUNT];
            Date lastShorted = (Date) obj[LAST_SHORTED];
            String key = location + "_&_" + item;

            if (count > 0) {
                ShortedLocationItem li = shortedLocations.get(key);
                if (li == null) {
                    li = new ShortedLocationItem();
                    li.setScannedLocation(location);
                    li.setItemNumber(item);
                    li.setShortedCount(count);
                    li.setLastShorted(lastShorted);
                    shortedLocations.put(key, li);
                } else {
                    
                    li.setShortedCount(count + li.getShortedCount());
                    if (lastShorted.after(li.getLastShorted())) {
                        li.setLastShorted(lastShorted);
                    }
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<ShortedLocationItem> reportList = new ArrayList<ShortedLocationItem>();
        return new JRBeanCollectionDataSource(reportList);
    }



    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * @param beanClass - class report is based on.
     */
    public ShortedLocationReportImplRoot(Class<ShortedLocationItem> beanClass) {
        super(ShortedLocationItem.class);
    }

    /**
     * Constructor.
     */
    public ShortedLocationReportImplRoot() {
        super(ShortedLocationItem.class);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 