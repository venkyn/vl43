/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.importer.ImportSetupException;
import com.vocollect.voicelink.core.service.ImportSetupManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URL;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;

/**
 * This class update for the import/export setup when a new site gets added or
 * delete.
 * @author Kalpna T
 */
public class ImportSetupManagerImplRoot implements ImportSetupManager {

    private static final Logger log = new Logger(
        ImportSetupManagerImplRoot.class);

    // Spring application context
    private static ApplicationContext ctx = null;

    // import open tag
    private static final String IMPORTCLOSETAG = "</Importer>";

    // import close tag
    private static final String IMPORTSCLOSETAG = "</Importers>";

    // export open tag
    private static final String EXPORTCLOSETAG = "</Exporter>";

    // export close tag
    private static final String EXPORTSCLOSETAG = "</Exporters>";

    // imports open tag
    private static final String IMPORTSOPENTAG = "<Importers>";

    // import open tag
    private static final String IMPORTOPENTAG = "<Importer>";

    // exports open tag
    private static final String EXPORTSOPENTAG = "<Exporters>";

    // export open tag
    private static final String EXPORTOPENTAG = "<Exporter>";
    
    // config directory name - used to store import and export setup files
    // when clustering.
    private static final String CONFIGDIR = "Config";
    
    // import-setup file
    private final String importSetupFile = "import-setup.xml";

    // import-setup file
    private final String exportSetupFile = "export-setup.xml";

    // import-setup template file
    private final String importSetupTemplateFile = "import-setup-template.xml";

    // export-setup template file
    private final String exportSetupTemplateFile = "export-setup-template.xml";

    // SiteSpecific String needs to be replaced in template file
    private final String siteString = "$SiteName";

    private static final String ENDOFLINESEPARATOR = System
        .getProperty("line.separator");

    
    /**
     * Default Constructor Constructor.
     */
    public ImportSetupManagerImplRoot() {
        super();
    }

    /**
     * @param context specifies ApplicationContext to set
     * @throws BeansException if unsuccessful
     */
    public void setApplicationContext(ApplicationContext context)
        throws BeansException {
        ctx = context;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
        throws BeansException {
        // Just implementing this causes the static ctx variable to be set,
        // because an instance is created by Spring.
    }

    /**
     * This prepares the new import/export.
     * @param templateFile for the import/export
     * @param newLine String buffer to hold new import/export
     * @param siteName name of the newly created site
     * @throws FileNotFoundException when template file not found
     * @throws IOException I/O error occurs
     */
    private void prepareImportExportSetup(File templateFile,
                                          StringBuffer newLine,
                                          String siteName) throws IOException,
        FileNotFoundException {
        // prepare the new import for the new site
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(templateFile), "UTF-8"));
            String strLine = null;
            int index;
            // Read File Line By Line
            while ((strLine = br.readLine()) != null) {
                index = strLine.indexOf(siteString);
                if (index != -1) {
                    // String was found.
                    log.debug(strLine);
                    newLine.append(strLine.substring(0, index))
                        .append(siteName).append(
                            strLine.substring(index + siteString.length()))
                        .append(ENDOFLINESEPARATOR);
                } else {
                    newLine.append(strLine).append(ENDOFLINESEPARATOR);
                }
            }
            br.close();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Update import/export setup.
     * @param setupFile import/export setup file
     * @param newLine buffer holds the new import/export
     * @param tag TODO: not apparently used
     * @param closingTag import/export closing tag
     * @param tag1 TODO: not apparently used
     * @param openTag import/export open tag
     * @throws FileNotFoundException when srtup file not present
     * @throws IOException for io error
     */
    private void updateImportExportSetup(File setupFile,
                                         StringBuffer newLine,
                                         String tag,
                                         String closingTag,
                                         String tag1,
                                         String openTag)
        throws FileNotFoundException, IOException {
        // Append the new import/export setup to import/export-setup.xml
        try {
            String importStrLine = null;
            long newImportOffset = 0;
            RandomAccessFile raf = new RandomAccessFile(setupFile, "rw");

            // Read File Line By Line
            while ((importStrLine = raf.readLine()) != null) {
                if ((importStrLine.indexOf("<!--") != -1)
                    && (importStrLine.indexOf("Site:") != -1)
                    && (importStrLine.indexOf("end") != -1)
                    && (importStrLine.indexOf("-->") != -1)) {
                    newImportOffset = raf.getFilePointer();
                } else {
                    if (importStrLine.indexOf(openTag) != -1) {
                        newImportOffset = raf.getFilePointer();
                    }
                }
                if (importStrLine.indexOf(closingTag) != -1) {
                    newLine.trimToSize();
                    raf.seek(newImportOffset);
                    raf.write(newLine.toString().getBytes("UTF-8"));
                    raf.writeBytes(closingTag);
                    raf.close();
                    break;
                }
            }
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Add the import/export for new site.
     * @param siteName name of the site which is just added
     * @throws ImportSetupException all IO exceptions are wrapped around it
     */
    public void addNewSite(String siteName) throws ImportSetupException {
        //Get the clustering shareDirectory property
        String shareDirectory = System.getProperty("vocollect.share.directory");

        // getImportExportFiles();
        // Contains the updated import information
        StringBuffer newLineImport = new StringBuffer();

        // Contains the updates export information
        StringBuffer newLineExport = new StringBuffer();

        // import setup template file
        File impSetupTemplateFile;
        // Export setup Template file
        File expSetupTemplateFile;
        // import setup file
        File impSetupFile = null;
        // export setup file
        File expSetupFile = null;

        URL locImportSetup = null;
        URL locExportSetup = null;
        URL locImportSetupTemplate = null;
        URL locExportSetupTemplate = null;
        // Create each file from URL
        try {
            // Create new file using URL from getResource
            locImportSetupTemplate = this.getClass().getClassLoader()
                .getResource(importSetupTemplateFile);
            locExportSetupTemplate = this.getClass().getClassLoader()
                .getResource(exportSetupTemplateFile);
            //If clustering, don't do import setup and export setup
            if (shareDirectory == null) {
                locImportSetup = this.getClass().getClassLoader().getResource(
                    importSetupFile);
                locExportSetup = this.getClass().getClassLoader().getResource(
                    exportSetupFile);
            }
            try {
                URI u2 = java.net.URI.create(locImportSetupTemplate.toString());
                impSetupTemplateFile = new File(u2);

                URI u3 = java.net.URI.create(locExportSetupTemplate.toString());
                expSetupTemplateFile = new File(u3);
                //If clustering, don't do import setup and export setup
                if (shareDirectory == null) {
                    URI u = java.net.URI.create(locImportSetup.toString());
                    impSetupFile = new File(u);
    
                    URI u1 = java.net.URI.create(locExportSetup.toString());
                    expSetupFile = new File(u1);
                }

            } catch (IllegalArgumentException ex) {
                log.error(
                    "IllegalArgumentException caught when trying to get the import/export setup "
                        + " and template Files from the URL. "
                        + ex.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
                throw new ImportSetupException(
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE,
                    new UserMessage("site.add.import.message"));
            }
        } catch (Throwable t) {
            log.error(
                "Exception caught when trying to get the import export setup and "
                    + " template Files from the URL. " + t.getMessage(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        //If we are using clustering...
        if (shareDirectory != null) {
            //If clustering, then point import-setup and export-setup files to share
            //They will be stored in \\shareDirectory\CONFIGDIR\setupfile.xml
            //See if shareDirectory ends in a separator
            if (shareDirectory.endsWith("/") || shareDirectory.endsWith("\\")) {
                //shareDirectory has separator, so don't need another.
                impSetupFile = new File(shareDirectory + CONFIGDIR
                    + File.separator + importSetupFile);
                expSetupFile = new File(shareDirectory + CONFIGDIR
                    + File.separator + exportSetupFile);
            } else {
                //shareDirectory doesn't have separator, so need another.
                impSetupFile = new File(shareDirectory + File.separator + CONFIGDIR
                    + File.separator + importSetupFile);
                expSetupFile = new File(shareDirectory + File.separator + CONFIGDIR
                    + File.separator + exportSetupFile);
            }
        }
        
        
        if (!(impSetupTemplateFile.exists())) {
            log
                .error(
                    "Import setup template file doesn't exists. Directory path for template/setup files: "
                        + locImportSetupTemplate.toString(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(impSetupTemplateFile.canRead())) {
            log
                .error(
                    "Not able to read import setup template file. Directory path for template/setup files: "
                        + locImportSetupTemplate.toString(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(impSetupFile.exists())) {
            log.error(
                "Import setup file does not exists. Directory path for template/setup files: "
                    + locImportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(impSetupFile.canRead())) {
            log.error(
                "Not able to read import setup file. Directory path for template/setup files: "
                    + locImportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(expSetupTemplateFile.exists())) {
            log
                .error(
                    "Export setup template file does not exits. Directory path for template/setup files: "
                        + locExportSetupTemplate.toString(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(expSetupTemplateFile.canRead())) {
            log
                .error(
                    "Not able to read export setup template file. Directory path for template/setup files: "
                        + locExportSetupTemplate.toString(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(expSetupFile.exists())) {
            log.error(
                "Export setup file does not exists. Directory path for template/setup files: "
                    + locExportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        if (!(expSetupFile.canRead())) {
            log.error(
                "Not able to read export setup file. Directory path for template/setup files: "
                    + locExportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        // prepare the new import for the new site
        try {
            prepareImportExportSetup(
                impSetupTemplateFile, newLineImport, siteName);
            updateImportExportSetup(
                impSetupFile, newLineImport, IMPORTCLOSETAG, IMPORTSCLOSETAG,
                IMPORTOPENTAG, IMPORTSOPENTAG);
            newLineImport.setLength(0);
        } catch (FileNotFoundException e) {
            log
                .error(
                    "Import setup template file not found. Directory path for template/setup files: "
                        + e.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        } catch (IOException e) {
            log.error(
                "IO Exception occured when trying to read the import template file. "
                    + "Directory path for template/setup files: "
                    + locImportSetup.toString() + " " + e.getMessage(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

        // prepare the new export for the new site
        try {
            prepareImportExportSetup(
                expSetupTemplateFile, newLineExport, siteName);
            updateImportExportSetup(
                expSetupFile, newLineExport, EXPORTCLOSETAG, EXPORTSCLOSETAG,
                EXPORTOPENTAG, EXPORTSOPENTAG);
            newLineExport.setLength(0);
        } catch (FileNotFoundException e) {
            log
                .error(
                    "Export setup template file not found. Directory path for template/setup files: "
                        + locExportSetup.toString() + " " + e.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        } catch (IOException e) {
            log.error(
                "IO Exception occured when trying to read the export template file. "
                    + "Directory path for template/setup files: "
                    + locExportSetup.toString() + " " + e.getMessage(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.add.import.message"));
        }

    }

    /**
     * This prepares for the import/export setup update when site gets deleted.
     * @param setupFile which needs to be updated
     * @param siteName site which needs to be deleted
     * @param deleteStrLine StringBuffer which holds the updated data
     * @throws FileNotFoundException when file not found
     * @throws IOException when io error occurs
     */
    private void prepareDeleteImportExportSetup(File setupFile,
                                                String siteName,
                                                StringBuffer deleteStrLine)
        throws FileNotFoundException, IOException {

        // Prepare Delete the import-setup in import-setup.xml for given site
        try {
            BufferedReader br = new BufferedReader(new FileReader(setupFile));
            String importStrLine = null;
            boolean deletedSite = false;
            // Read File Line By Line
            while ((importStrLine = br.readLine()) != null) {

                // Check for the commented start site tag
                if ((importStrLine.indexOf("<!--") != -1)
                    && (importStrLine.indexOf("Site:") != -1)
                    && (importStrLine.indexOf("start") != -1)
                    && (importStrLine.indexOf("-->") != -1)) {
                    if (importStrLine.indexOf(siteName) != -1) {
                        deletedSite = true;
                    }
                }

                if (!deletedSite) {
                    deleteStrLine.append(importStrLine).append(
                        ENDOFLINESEPARATOR);
                }

                // Check for the commented end site tag
                if ((importStrLine.indexOf("<!--") != -1)
                    && (importStrLine.indexOf("Site:") != -1)
                    && (importStrLine.indexOf("end") != -1)
                    && (importStrLine.indexOf("-->") != -1)) {
                    deletedSite = false;
                }
            }
            br.close();

        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * This updates the import/export setup when site gets deleted.
     * @param setupFile which needs to be updated
     * @param deleteStrLine which holds the updated data
     * @throws FileNotFoundException when file not found
     * @throws IOException when io error occurs
     */
    private void deleteImportExportSetup(File setupFile,
                                         StringBuffer deleteStrLine)
        throws FileNotFoundException, IOException {

        // Prepare Delete the import-setup in import-setup.xml for given site
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(setupFile));
            br.write(deleteStrLine.toString());
            br.close();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Deletes the import/export for the deleted site.
     * @param siteName name of the site which is just deleted
     * @throws ImportSetupException wrapper for any IOException
     */
    public void deleteSite(String siteName) throws ImportSetupException {
        //Get the clustering shareDirectory property
        String shareDirectory = System.getProperty("vocollect.share.directory");

        StringBuffer deleteImportStrLine = new StringBuffer();

        StringBuffer deleteExportStrLine = new StringBuffer();

        URL locImportSetup = null;
        URL locExportSetup = null;

        // import setup file
        File impSetupFile;
        // export setup file
        File expSetupFile;

        if (shareDirectory == null) {
            // Create each file from URL (not clustering)
            try {
                // Create new file using URL from getResource
                locImportSetup = this.getClass().getClassLoader().getResource(
                    importSetupFile);
                locExportSetup = this.getClass().getClassLoader().getResource(
                    exportSetupFile);
                try {
                    URI u = java.net.URI.create(locImportSetup.toString());
                    impSetupFile = new File(u);

                    URI u1 = java.net.URI.create(locExportSetup.toString());
                    expSetupFile = new File(u1);

                } catch (IllegalArgumentException ex) {
                    log.error(
                        "IllegalArgumentException caught when trying to get the import/export setup "
                            + " and template Files from the URL. "
                            + ex.getMessage(),
                        CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
                    throw new ImportSetupException(
                        CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE,
                        new UserMessage("site.delete.import.message"));
                }
            } catch (Throwable t) {
                log.error(
                    "Exception caught when trying to get the import export setup and "
                        + " template Files from the URL. " + t.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
                throw new ImportSetupException(
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                        "site.delete.import.message"));
            }
        } else {
            //We are using clustering...
            //Point import-setup and export-setup files to share
            //They will be stored in \\shareDirectory\CONFIGDIR\setupfile.xml
            
            //See if shareDirectory ends in a separator
            if (shareDirectory.endsWith("/") || shareDirectory.endsWith("\\")) {
                //shareDirectory has separator, so don't need another.
                impSetupFile = new File(shareDirectory + CONFIGDIR
                    + File.separator + importSetupFile);
                expSetupFile = new File(shareDirectory + CONFIGDIR
                    + File.separator + exportSetupFile);
            } else {
                //shareDirectory has separator, so don't need another.
                impSetupFile = new File(shareDirectory + File.separator + CONFIGDIR
                    + File.separator + importSetupFile);
                expSetupFile = new File(shareDirectory + File.separator + CONFIGDIR
                    + File.separator + exportSetupFile);
            }
        }
        
        if (!(impSetupFile.exists())) {
            log.error(
                "Import setup file does not exists. Directory path for template/setup files: "
                    + locImportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        }

        if (!(impSetupFile.canRead())) {
            log.error(
                "Not able to read import setup file. Directory path for template/setup files: "
                    + locImportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        }

        if (!(expSetupFile.exists())) {
            log.error(
                "Export setup file does not exists. Directory path for template/setup files: "
                    + locExportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        }

        if (!(expSetupFile.canRead())) {
            log.error(
                "Not able to read export setup file. Directory path for template/setup files: "
                    + locExportSetup.toString(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        }

        // Delete the import-setup in import-setup.xml for given site
        try {
            prepareDeleteImportExportSetup(
                impSetupFile, siteName, deleteImportStrLine);
            deleteImportExportSetup(impSetupFile, deleteImportStrLine);
            deleteImportStrLine.setLength(0);
        } catch (FileNotFoundException e) {
            log
                .error(
                    "Import setup template file not found. Directory path for template/setup files: "
                        + locImportSetup.toString() + " " + e.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_DELETED_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_DELETED_SITE,
                new UserMessage("site.delete.import.message"));
        } catch (IOException e) {
            log.error(
                "IO Exception occured when trying to read the import template file. "
                    + "Directory path for template/setup files: "
                    + locImportSetup.toString() + " " + e.getMessage(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_DELETED_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_DELETED_SITE,
                new UserMessage("site.delete.import.message"));
        }

        // Delete the export-setup in export-setup.xml for given site
        try {
            prepareDeleteImportExportSetup(
                expSetupFile, siteName, deleteExportStrLine);
            deleteImportExportSetup(expSetupFile, deleteExportStrLine);
            deleteExportStrLine.setLength(0);
        } catch (FileNotFoundException e) {
            log
                .error(
                    "Export setup template file not found. Directory path for template/setup files: "
                        + locImportSetup.toString() + " " + e.getMessage(),
                    CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        } catch (IOException e) {
            log.error(
                "IO Exception occured when trying to read the export template file. "
                    + "Directory path for template/setup files: "
                    + locImportSetup.toString() + " " + e.getMessage(),
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_ERROR_FOR_NEW_SITE, new UserMessage(
                    "site.delete.import.message"));
        }

    }

    
    /**
     * Looks for import-setup.xml and export-setup.xml.  The files are re-built
     * (refreshed) by querying the sites table and first calling the deleteSite()
     * method for each site (catching all throwable and doing nothing on failure)
     * then calling addNewSite for each site.  During install or upgrade the
     * files begin with no site level data in them and only have either the 
     * <Exporters> </Exporters> or <Importers> </Importers> tags with an empty
     * line in between.  If this is to be done as a "field fix" it is recommended
     * that the users blank out the files in a similar way, although, the deleteSite()
     * calling will accomplish this automatically as well.
     * @throws ImportSetupException wrapper for IO exceptions, and DataAccessExcepiton.
     */
    public void refreshAllSites() throws ImportSetupException {
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        
        // Get a list of all the sites in the system
        List <Site> siteList = null;
        try {
            siteList = siteContext.getAllSites();
        } catch (DataAccessException e) {
            String message = "DataAccessException occurrred trying to get list "
                + "of all sites for refreshAllSites() call to rebuild import and export setup files.";
            log.error(message, CoreErrorCode.IMPORT_SETUP_REFRESH_ALL_SITES_DB_ERROR);
            throw new ImportSetupException(
                CoreErrorCode.IMPORT_SETUP_REFRESH_ALL_SITES_DB_ERROR, new UserMessage(
                    "site.refreshAll.import.message"));
        }

        
        //For Install and Upgrade, import-setup.xml and export-setup.xml are empty.
        //When done in the field, we recommend removing everything between the <IMPORTERS>
        // tags or <EXPORTERS> tags (Except a blank line).  Just in case...
        //Loop through all the sites, delete any found from the import and export files.
        for (Site s : siteList) {
            try {
                    //Note that this part could be done more efficiently by rewriting this code,
                    //so that the file finding portion does not need to be repeated, however,
                    //we are using it since this is well tested and will be rarely used.
                this.deleteSite(s.getName());
            } catch (Throwable t) {
                //Deleting sites only happens if someone manually sets bit.
                //In install or upgrade, the file will be empty and this throwable
                //will always happen.
            }
        }
        
        //Loop through all the sites, and add them to the import and export files.
        for (Site s : siteList) {
                //Note that this part could be done more efficiently by rewriting this code,
                //so that the file finding portion does not need to be repeated, however,
                //we are using it since this is well tested and will be rarely used.
            this.addNewSite(s.getName());
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 