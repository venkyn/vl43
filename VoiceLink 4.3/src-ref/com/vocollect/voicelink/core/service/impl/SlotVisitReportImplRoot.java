/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.voicelink.selection.service.ArchivePickManager;
import com.vocollect.voicelink.selection.service.PickManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Implementation of Frequency of Slot Visit Report.
 *
 * @author krishna.udupi
 */
public abstract class SlotVisitReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager {

    private static final int SCANNED_LOCATION = 0;
    private static final int NUMBER_OF_VISITS = 1;
    private static final int UNITS_PICKED = 2;

    /**
     * Bean for report.
     *
     * @author krishna.udupi
     */
    public class SlotVisitFrequency
    implements Comparable<SlotVisitFrequency> {
        private String scannedLocation;
          private Long numberOfVisits;
          private Long numberOfItemsPicked;



          /**
         * Getter for the scannedLocation property.
         * @return String value of the property
         */
        public String getScannedLocation() {
            return scannedLocation;
        }

        /**
         * Setter for the scannedLocation property.
         * @param scannedLocation the new scannedLocation value
         */
        public void setScannedLocation(String scannedLocation) {
            this.scannedLocation = scannedLocation;
        }

        /**
         * @return numberOfVisits Long
         */
        public Long getNumberOfVisits() {
            return numberOfVisits;
        }

       /**
        * @param numberOfVisits Long
        */
        public void setNumberOfVisits(Long numberOfVisits) {
             this.numberOfVisits = numberOfVisits;
        }

        /**
         * @return numberOfItemsPicked Long
         */
        public Long getNumberOfItemsPicked() {
             return numberOfItemsPicked;
        }

        /**
      * @param numberOfItemsPicked Long
      */
     public void setNumberOfItemsPicked(Long numberOfItemsPicked) {
             this.numberOfItemsPicked = numberOfItemsPicked;
        }

        /**
         * {@inheritDoc}
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(SlotVisitFrequency o) {
            return o.getNumberOfVisits().compareTo(this.getNumberOfVisits());
        }
    }


    //Managers for queries
    private PickManager pickManager;
    private ArchivePickManager archivePickManager;

    /**
     * Getter for the pickManager property.
     * @return PickManager value of the property
     */
    public PickManager getPickManager() {
        return pickManager;
    }


    /**
     * Setter for the pickManager property.
     * @param pickManager the new pickManager value
     */
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }


    /**
     * Getter for the archivePickManager property.
     * @return ArchivePickManager value of the property
     */
    public ArchivePickManager getArchivePickManager() {
        return archivePickManager;
    }


    /**
     * Setter for the archivePickManager property.
     * @param archivePickManager the new archivePickManager value
     */
    public void setArchivePickManager(ArchivePickManager archivePickManager) {
        this.archivePickManager = archivePickManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values) throws Exception {
        Map<String, SlotVisitFrequency> slotVisited = new HashMap<String, SlotVisitFrequency>();

        List<Object[]> slotVisits = null;

        //Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");

        Long count = (Long) values.get("LOCATION_COUNT");
        String siteId = (String) values.get("SITE");
        Long siteID = Long.valueOf(siteId);

        if (startTime == null && endTime == null) {
            slotVisits = getArchivePickManager()
                .listSlotVisitLocationsBySiteId(siteID);
        } else {
            // Get Archive Slot Visits between date range and add them to map
            slotVisits = getArchivePickManager().listSlotVisitLocations(
                startTime, endTime, siteID);
        }
        addToMap(slotVisited, slotVisits);

        // get transactional Slot visits
        slotVisits = getPickManager()
            .listSlotVisitLocations(startTime, endTime);
        addToMap(slotVisited, slotVisits);

        return new JRBeanCollectionDataSource(getTopVisitedSlots(slotVisited,
                                                            count));
    }

    /**
     * Build list to return to report.
     * @param count - number of locations user is requesting.
     * @param slotsVisited - map of locations to turn into list.
     * @return - list of locations based on top count.
     */
    protected List<SlotVisitFrequency> getTopVisitedSlots(Map<String, SlotVisitFrequency> slotsVisited, Long count) {
        List<SlotVisitFrequency> reportList = new ArrayList<SlotVisitFrequency>();
        List<SlotVisitFrequency> fullList =
            new ArrayList<SlotVisitFrequency>(slotsVisited.values());
        Collections.sort(fullList);
        //Loop through list adding items to report list until the
        //specified count is reached.
        Long slotVisitsCounts = 1L;
        Long lastCount = 0L;
        for (SlotVisitFrequency li : fullList) {
            //check if loop should stop, if a tie at last position, then
            // keep including
            if (slotVisitsCounts++ > count
                && !lastCount.equals(li.getNumberOfVisits())) {
                break;
            }
            //Add item to report list
            reportList.add(li);
            lastCount = li.getNumberOfVisits();
        }
        return reportList;
    }

    /**
     * Build map of location/items that where shorted.
     *
     * @param slotsVisited - map of locations to add list to.
     * @param visited - list of visited locations
     */
    protected void addToMap(Map<String, SlotVisitFrequency> slotsVisited,
                          List<Object[]> visited) {

        for (Object[] obj : visited) {
            String location = (String) obj[SCANNED_LOCATION];
            Long numberOfVisits = (Long) obj[NUMBER_OF_VISITS];
            Long unitsPicked = (Long) obj[UNITS_PICKED];
            String key = location; //+ "_&_" + item;

            if (numberOfVisits > 0) {
                SlotVisitFrequency li = slotsVisited.get(key);
                if (li == null) {
                    li = new SlotVisitFrequency();
                    li.setScannedLocation(location);
                    li.setNumberOfVisits(numberOfVisits);
                    li.setNumberOfItemsPicked(unitsPicked);
                    slotsVisited.put(key, li);
                } else {
                    li.setNumberOfVisits(numberOfVisits + li.getNumberOfVisits());
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<SlotVisitFrequency> reportList = new ArrayList<SlotVisitFrequency>();
        return new JRBeanCollectionDataSource(reportList);
    }



    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * @param beanClass - class report is based on.
     */
    public SlotVisitReportImplRoot(Class<SlotVisitFrequency> beanClass) {
        super(SlotVisitFrequency.class);
    }

    /**
     * Constructor.
     */
    public SlotVisitReportImplRoot() {
        super(SlotVisitFrequency.class);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 