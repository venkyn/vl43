/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.selection.model.ContainerReport;
import com.vocollect.voicelink.selection.service.ArchiveContainerManager;
import com.vocollect.voicelink.selection.service.ContainerManager;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Implementation of Container Report.
 *
 * @author yash arora
 */

public class ContainerReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager {

  //Managers for queries
    private ArchiveContainerManager archiveContainerManager;

    private ContainerManager containerManager;

    /**
     * Constructor.
     * @param beanClass for container Report Item bean
     */
    public ContainerReportImplRoot(Class<ContainerReport> beanClass) {
        super(ContainerReport.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    @SuppressWarnings("unchecked")
    public JRDataSource getDataSource(
                                      Map<String, Object> values)
        throws Exception {
        //Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        String containerNumber = (String) values.get("CONTAINER_NUMBER");
        String siteId = (String) values.get("SITE");
        Set<String> allOperatorId = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            allOperatorId = null;
        }

        // Get archive containers for report
        List<ContainerReport> archiveContainers = getArchiveContainerManager()
            .listArchiveContainersForContainerReport(
                new QueryDecorator(), containerNumber, allOperatorId, startTime,
                endTime, Long.valueOf(siteId));

        // Get transactional containers for report
        List<ContainerReport> containers = getContainerManager()
            .listContainersForContainerReport(
                new QueryDecorator(), containerNumber, allOperatorId,
                startTime, endTime);

        // Add lists together
        containers.addAll(archiveContainers);

        return new JRBeanCollectionDataSource(containers);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // TODO Auto-generated method stub

    }


    /**
     * Getter for the archiveContainerManager property.
     * @return the value of the property
     */
    public ArchiveContainerManager getArchiveContainerManager() {
        return this.archiveContainerManager;
    }


    /**
     * Setter for the archiveContainerManager property.
     * @param archiveContainerManager the new archiveContainerManager value
     */
    public void setArchiveContainerManager(ArchiveContainerManager archiveContainerManager) {
        this.archiveContainerManager = archiveContainerManager;
    }


    /**
     * Getter for the containerManager property.
     * @return the value of the property
     */
    public ContainerManager getContainerManager() {
        return this.containerManager;
    }


    /**
     * Setter for the containerManager property.
     * @param containerManager the new containerManager value
     */
    public void setContainerManager(ContainerManager containerManager) {
        this.containerManager = containerManager;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 