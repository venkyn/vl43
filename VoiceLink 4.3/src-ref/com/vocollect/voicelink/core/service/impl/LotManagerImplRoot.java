/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.BatchDataAccessException;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.LotDAO;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.LotManager;

import java.util.Date;
import java.util.List;

/**
 * Additional service methods for the <code>Lot</code> model object.
 * 
 * @author mnichols
 */
public abstract class LotManagerImplRoot extends GenericManagerImpl<Lot, LotDAO> 
implements LotManager {

    public static final int MAX_LOT_NUMBER_LENGTH = 50;
    public static final int MAX_SPEAKABLE_LOT_NUMBER_LENGTH = 50;
    
    private ItemManager itemManager;
    
    private LocationManager locationManager;


    /**
     * get the max allowed lot number length.
     * 
     * @return max allowed lot number length
     */
    protected int getMaxLotNumberLength() {
        return MAX_LOT_NUMBER_LENGTH;
    }
    
    /**
     * get the max allowed speakable lot number length.
     * 
     * @return max allowed speakable lot number length
     */
    protected int getMaxSpeakableLotNumberLength() {
        return MAX_SPEAKABLE_LOT_NUMBER_LENGTH;
    }
    
    
    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    
    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    
    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    
    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LotManagerImplRoot(LotDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @return the saved instances list that was passed in.
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.util.List)
     */
    @Override
    public Object save(List<Lot> instances) throws BusinessRuleException, DataAccessException {
        int i = 0;
        try {
            for (Lot li : instances) {
                save(li);
            }
        } catch (Exception e) {
            throw new BatchDataAccessException(e, i);
        }
        return instances;
    }
    
    /**
     * Method to handle lot import logic based on action.
     * 
     * @param lot - lot that was edited/created
     * @return the saved lot
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    @Override
    public Object save(Lot lot) throws DataAccessException, BusinessRuleException {

        //Increment the item lot count
        if (lot.isNew()) {
            lot.getItem().setLotCount(lot.getItem().getLotCount() + 1);
            //Increment the location lot count
            if (lot.getLocation() != null) {
                lot.getLocation().setLotCount(lot.getLocation().getLotCount() + 1);
            }
        }
        
        //Normal Save from UI creation
        if (lot.getImportQueryType() == null) {
            return super.save(lot);
            
        //Save from import
        } else if (lot.getImportQueryType().equalsIgnoreCase("A")) {
            return super.save(lot);
            
        //Delete from import
        } else if (lot.getImportQueryType().equalsIgnoreCase("D")) {
            return delete(lot);
        }
        
        return lot;
    }
      
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManager#listAll()
     */
    public List<Lot> listAllOrderByNumber() throws DataAccessException {
        return getPrimaryDAO().listAllOrderByNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManager#findByNumber(int)
     */
    public Lot findLotByNumber(String lotNumber) throws DataAccessException {
        return getPrimaryDAO().findLotByNumber(lotNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Lot lot = get(id);
        return delete(lot);
    }  
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
/*    @Override
    public Object delete(Lot instance)
        throws BusinessRuleException, DataAccessException {
        try {
            //Decrement the item lot count
            instance.getItem().setLotCount(instance.getItem().getLotCount() - 1);
            //Decrement the location lot count
            if (instance.getLocation() != null) {
                instance.getLocation().setLotCount(instance.getLocation().getLotCount() - 1);
            }
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("lot.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
     }*/

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManagerRoot#findLotByNumberItemLocation(java.lang.String, java.lang.String, java.lang.String)
     */
    public Lot findLotByNumberItemLocation(String lotNumber,
                                           String itemNumber,
                                           String locationId) {

        //ensure location ID is null if blank or empty
        String tmpLocationId = locationId;
        if (StringUtil.isNullOrBlank(locationId)) {
            tmpLocationId = null;
        }
        
        return getPrimaryDAO().findLotByNumberItemLocation(
            lotNumber, itemNumber, tmpLocationId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManagerRoot#countAll()
     */
    public Long countAll() {
        return getPrimaryDAO().countAll();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManagerRoot#executeCreateUpdateLot(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date)
     */
    public void executeCreateUpdateLot(String lotNumber,
                                String itemNumber,
                                String locationId,
                                String speakableLotNumber,
                                Date expirationDate)
    throws BusinessRuleException, DataAccessException {
        //Validate lot number and speakable lot
        validateLotNumber(lotNumber);
        validateSpeakableLotNumber(speakableLotNumber);
        
        //See if lot already exists
        Lot lot = findLotByNumberItemLocation(lotNumber, 
            itemNumber, locationId);
        
        //if lot does not exist, then create a new one.
        if (lot == null) {
            //Get item and location
            Item item = getItemByNumber(itemNumber);
            Location location = getLocationById(locationId);
            
            lot = new Lot();
            lot.setItem(item);
            lot.setLocation(location);
            lot.setNumber(lotNumber);
        }
        
        //update additional fields.
        lot.setExpirationDate(expirationDate);
        lot.setSpeakableNumber(speakableLotNumber);
        
        save(lot);
    }

    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LotManagerRoot#executeDeleteLot(java.lang.String, java.lang.String, java.lang.String)
     */
    public void executeDeleteLot(String lotNumber,
                          String itemNumber,
                          String locationId) 
    throws BusinessRuleException, DataAccessException {
        //Validate lot number
        validateLotNumber(lotNumber);

        //See if lot already exists
        Lot lot = findLotByNumberItemLocation(lotNumber, 
            itemNumber, locationId);
        
        if (lot != null) {
            delete(lot);
        } else {
            throw new BusinessRuleException(CoreErrorCode.LOT_NOT_FOUND);
        }
    }

    /**
     * Find item based on item number.
     * 
     * @param itemNumber - item number to find
     * @return - Item object
     * @throws DataAccessException - database exceptions
     * @throws BusinessRuleException - business rule exceptions
     */
    protected Item getItemByNumber(String itemNumber) 
    throws DataAccessException, BusinessRuleException {
        Item item = getItemManager().findItemByNumber(itemNumber);
        if (item == null) {
            throw new BusinessRuleException(CoreErrorCode.ITEM_NOT_FOUND);
        }
        return item;
    }
    
    /**
     * Find location based on location number.
     * 
     * @param locationId - location id to find
     * @return - Location object
     * @throws DataAccessException - database exceptions
     * @throws BusinessRuleException - business rule exceptions
     */
    protected Location getLocationById(String locationId) 
    throws DataAccessException, BusinessRuleException {
        Location location = null; 
        
        //Get location if location ID is not null
        if (!StringUtil.isNullOrBlank(locationId)) {
            location = getLocationManager().findLocationByScanned(locationId);
            if (location == null) {
                throw new BusinessRuleException(CoreErrorCode.LOCATION_NOT_FOUND);
            }
        }
        return location;
    }
    
    /**
     * Validate the length and existance of lot number.
     * 
     * @param lotNumber - lot number
     * @throws BusinessRuleException - business rule exceptions
     */
    protected void validateLotNumber(String lotNumber) throws BusinessRuleException {
        
        if (StringUtil.isNullOrBlank(lotNumber)) {
            throw new BusinessRuleException(CoreErrorCode.LOT_NUMBER_EMTPY);
        }
        
        if (lotNumber.length() > getMaxLotNumberLength()) {
            throw new BusinessRuleException(CoreErrorCode.LOT_NUMBER_TO_LONG);
        }
        
    }
    
    /**
     * Validate the length and existence of the speakable lot number.
     * 
     * @param speakableLotNumber - speakable lot number
     * @throws BusinessRuleException - business rule exceptions 
     */
    protected void validateSpeakableLotNumber(String speakableLotNumber) 
    throws BusinessRuleException {
        if (StringUtil.isNullOrBlank(speakableLotNumber)) {
            throw new BusinessRuleException(
                CoreErrorCode.SPEAKABLE_LOT_NUMBER_EMTPY);
        }

        if (speakableLotNumber.length() > getMaxSpeakableLotNumberLength()) {
            throw new BusinessRuleException(
                CoreErrorCode.SPEAKABLE_LOT_NUMBER_TO_LONG);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 