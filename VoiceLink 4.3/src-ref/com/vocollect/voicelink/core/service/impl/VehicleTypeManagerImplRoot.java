/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.VehicleTypeDAO;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;
import com.vocollect.voicelink.core.service.VehicleTypeManager;

import java.util.List;

/**
 * Additional service methods for the <code>VehicleType</code> model object.  
 *
 * @author kudupi
 */
public class VehicleTypeManagerImplRoot extends
    GenericManagerImpl<VehicleType, VehicleTypeDAO> implements VehicleTypeManager {
    
    private VehicleManager vehicleManager;
    
    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleTypeManagerImplRoot(VehicleTypeDAO primaryDAO) {
        super(primaryDAO);
    }
    
    
    
    /**
     * @return the vehicleManager
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }


    
    /**
     * @param vehicleManager the vehicleManager to set
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }


    
    /**
     * @return the vehicleSafetyCheckResponseManager
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }


    
    /**
     * @param vehicleSafetyCheckResponseManager the vehicleSafetyCheckResponseManager to set
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.VehicleTypeManager#listAllVehicleTypesByNumber()
     */
    public List<VehicleType> listAllVehicleTypesByNumber() throws DataAccessException {
        return getPrimaryDAO().listAllVehicleTypesByNumber();
    }    
    
    @Override
    public Object save(VehicleType instance) throws BusinessRuleException,
        DataAccessException {
        boolean isNew = instance.isNew();
        Long numberUniquenessId = getPrimaryDAO().uniquenessByNumber(instance.getNumber());
        
        if (numberUniquenessId != null && (isNew || (!isNew && numberUniquenessId.longValue() 
            != instance.getId().longValue()))) {
            throw new FieldValidationException("vehicleType.number",
                instance.getNumber().toString(),
                new UserMessage("vsc.vehicleType.error.duplicateNumber", instance.getNumber().toString()));            
        }
        
        
        return super.save(instance);
    }

    
    @Override
    public Object delete(VehicleType instance) throws BusinessRuleException,
    DataAccessException {
        List<VehicleSafetyCheckResponse> responses = getVehicleSafetyCheckResponseManager()
            .listSafetyChecksByVehicleType(instance);
        
        if (instance.getVehicleCount() > 1) {
            throw new DataAccessException(
                CoreErrorCode.VEHICLE_EXISTS_FOR_VEHICLETYPE, new UserMessage(
                    "vsc.vehicleType.vehicle.error.exist", instance.getNumber()), null);
        } else if (!instance.getSafetyChecks().isEmpty()) {
            throw new DataAccessException(
                CoreErrorCode.SAFETYCHECK_EXISTS_FOR_VEHICLETYPE, new UserMessage(
                    "vsc.vehicleType.safetyCheck.error.exist", instance.getNumber()), null);
        } else if (!responses.isEmpty()) {
            throw new DataAccessException(
                CoreErrorCode.SAFETYCHECK_RESPONSES_EXISTS_FOR_VEHICLETYPE, new UserMessage(
                    "vsc.vehicleType.safetyCheck.response.error.exist", instance.getNumber()), null);
        } else {
            return super.delete(instance);
        } 
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 