/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.ReasonCodeDAO;
import com.vocollect.voicelink.core.model.ReasonCode;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.ReasonCodeManager;

import java.util.List;


/**
 * Additional service methods for the <code>ReasonCode</code> model object.
 *
 * @author Ed Stoll
 */
public abstract class ReasonCodeManagerImplRoot extends
    GenericManagerImpl<ReasonCode, ReasonCodeDAO> 
    implements ReasonCodeManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReasonCodeManagerImplRoot(ReasonCodeDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.ReasonCodeManager#listByTaskApplication()
     */
    public List<ReasonCode> listByTaskFunctionType(TaskFunctionType taskFunctionType) throws DataAccessException {
        return getPrimaryDAO().listByTaskFunctionType(taskFunctionType);
    }
    
    
    /**
     * Retrieves an reason code object by number string, or returns null is 
     * there is no such reason code.
     *
     * @param reasonNumber - the number of the License
     * @return the requested reason code object, or null
     * @throws DataAccessException on any failure.
     */
    public ReasonCode findByReasonNumber(int reasonNumber) throws DataAccessException {
        return this.getPrimaryDAO().findByReasonNumber(reasonNumber);
    }
    
    
    
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 