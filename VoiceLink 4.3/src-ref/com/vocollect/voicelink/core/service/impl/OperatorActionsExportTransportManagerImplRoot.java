/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.core.dao.OperatorLaborDAO;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.OperatorActionsExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dgold
 *
 */
public abstract class OperatorActionsExportTransportManagerImplRoot extends
        OperatorLaborManagerImpl implements
        OperatorActionsExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    private static final int START_FUNCTIONS = 4;
    /**
     * Default c'tor.
     * @param primaryDAO the DAO used to look up data.
     */
    public OperatorActionsExportTransportManagerImplRoot(OperatorLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     */
    public List<Map<String, Object>> listOperatorActions() throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);
        
        List<Map<String, Object>> rawData = this.getPrimaryDAO().listOperatorActions(qd);
        List<Map<String, Object>> finishedData = new ArrayList<Map<String, Object>>();
        //Stuff to format timestamps
        String formattedStartTime = "";
        String formattedEndTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        
        // The list will contain the operator and the labor records: source, and OperatorLabor
        for (Map<String, Object> m : rawData) {
            OperatorLabor ol = (OperatorLabor) m.get("OperatorLabor");
            if (null == ol) {
                continue;
            }
            if (null == ol.getOperator()) {
                continue;
            }
            Map<String, Object> mirror = new HashMap<String, Object>();
            mirror.put("RecordType", "LABOR");
            mirror.put("Action", ol.getActionType().toValue());
            mirror.put("Operator", ol.getOperator().getOperatorIdentifier());

            //Format the start time
            if (ol.getStartTime() == null) {
                // If there is no Start Time, we will put "No Timestamp"
                formattedStartTime = "No Timestamp";
            } else {
                formattedStartTime = sdf.format(ol.getStartTime());
            }
            
            //Format the end time
            if (ol.getEndTime() == null) {
                // If there is no End Time, we will put "No Timestamp"
                formattedEndTime = "No Timestamp";
            } else {
                formattedEndTime = sdf.format(ol.getEndTime());
            }
            
            mirror.put("StartTime", formattedStartTime);
            mirror.put("EndTime", formattedEndTime);
            //Only output the Break Type and Name if
            if (ol.getActionType() == OperatorLaborActionType.Break) {
                mirror.put("BreakID", ol.getBreakType().getNumber());
                mirror.put("BreakTypeName", ol.getBreakType().getName());
            } else {
                mirror.put("BreakID", null);
                mirror.put("BreakTypeName", null);
            }
            //Only output region if Action is Selection or PTS
            if (OperatorLaborActionType.Selection.equals(ol.getActionType())
                || OperatorLaborActionType.PutToStore.equals(ol.getActionType())) {
                mirror.put("RegionNumber", ol.getRegion().getNumber());
            } else {
                mirror.put("RegionNumber", null);
            }
            // Only output count if the Action # is >= 4
            //    (Selection, Put Away, Replenishment, Line Loading ...) 
            if (ol.getActionType().toValue() >= START_FUNCTIONS) {
                mirror.put("Count", ol.getCount());
            } else {
                mirror.put("Count", null);
            }
            mirror.put("source", ol);
            finishedData.add(mirror);
        }
        return finishedData;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 