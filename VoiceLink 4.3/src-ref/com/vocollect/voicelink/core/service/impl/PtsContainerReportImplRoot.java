/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.puttostore.model.PtsContainerReport;
import com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * 
 * 
 * @author mlashinsky
 */
public class PtsContainerReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    // Managers
    private ArchivePtsContainerManager archivePtsContainerManager;

    private PtsContainerManager ptsContainerManager;

    /**
     * Constructor.
     * @param beanClass PtsContainer report for populating data
     */
    public PtsContainerReportImplRoot(Class<PtsContainerReport> beanClass) {
        super(PtsContainerReport.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    @Override
    public JRDataSource getDataSource(Map<String, Object> values)
        throws Exception {
        // Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        String containerNumber = (String) values.get("CONTAINER_NUMBER");
        String siteId = (String) values.get("SITE");
        @SuppressWarnings("unchecked")
        Set<String> allOperatorId = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            allOperatorId = null;
        }

        // Get archive PTS containers for report
        List<PtsContainerReport> archiveContainers = getArchivePtsContainerManager()
            .listPtsArchiveContainersForPtsContainerReport(new QueryDecorator(),
                containerNumber, allOperatorId, startTime, endTime,
                Long.valueOf(siteId));

        // Get transactional PTS containers for report
        List<PtsContainerReport> containers = getPtsContainerManager()
            .listPtsContainersForPtsContainerReport(new QueryDecorator(),
                containerNumber, allOperatorId, startTime, endTime);

        // Add lists of PTS Containers together
        containers.addAll(archiveContainers);
        return new JRBeanCollectionDataSource(containers);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    @Override
    public JRDataSource create(JasperReport arg0) throws JRException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    @Override
    public void dispose(JRDataSource arg0) throws JRException {
        // TODO Auto-generated method stub

    }

    /**
     * Getter for the archivePtsContainerManager property.
     * @return ArchivePtsContainerManager value of the property
     */
    public ArchivePtsContainerManager getArchivePtsContainerManager() {
        return archivePtsContainerManager;
    }

    /**
     * Setter for the archivePtsContainerManager property.
     * @param archivePtsContainerManager the new archivePtsContainerManager
     *            value
     */
    public void setArchivePtsContainerManager(ArchivePtsContainerManager archivePtsContainerManager) {
        this.archivePtsContainerManager = archivePtsContainerManager;
    }

    /**
     * Getter for the ptsContainerManager property.
     * @return PtsContainerManager value of the property
     */
    public PtsContainerManager getPtsContainerManager() {
        return ptsContainerManager;
    }

    /**
     * Setter for the ptsContainerManager property.
     * @param ptsContainerManager the new ptsContainerManager value
     */
    public void setPtsContainerManager(PtsContainerManager ptsContainerManager) {
        this.ptsContainerManager = ptsContainerManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 