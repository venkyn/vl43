/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.PurgeArchiveException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.PurgeResult;
import com.vocollect.epp.service.impl.PurgeArchiveManagerImpl;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.VoiceLinkPurgeArchiveManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentToArchiveManager;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.lineloading.service.RouteStopManager;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.service.ArchiveLoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingRouteToArchiveManager;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.putaway.service.LicenseManager;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.service.ArchivePtsContainerManager;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsContainerToArchiveManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseToArchiveManager;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentToArchiveManager;

/**
 * Additional service methods for the Purge Archive Service.
 * 
 * @author mlashinsky
 */
public abstract class VoiceLinkPurgeArchiveManagerImplRoot extends
    PurgeArchiveManagerImpl implements VoiceLinkPurgeArchiveManager {

    // Assignment Data Access Object
    private AssignmentToArchiveManager assignmentToArchiveManager;

    private LicenseManager licenseManager;

    private ReplenishmentManager replenishmentManager;

    private RouteStopManager routeStopManager;

    private OperatorLaborManager operatorLaborManager;

    // ArchiveAssignment Data Access Object
    private ArchiveAssignmentManager archiveAssignmentManager;

    private ArchivePtsLicenseManager archivePtsLicenseManager;

    private ArchivePtsContainerManager archivePtsContainerManager;

    private PtsLicenseToArchiveManager ptsLicenseToArchiveManager;

    private PtsContainerToArchiveManager ptsContainerToArchiveManager;
    
    private ArchiveCycleCountingAssignmentManager archiveCycleCountingAssignmentManager;

    private CycleCountingAssignmentToArchiveManager cycleCountingAssignmentToArchiveManager;
    
    private ArchiveLoadingRouteManager archiveLoadingRouteManager;
    
    private LoadingRouteToArchiveManager loadingRouteToArchiveManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.PurgeArchiveManagerImplRoot#processRule(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.epp.service.impl.PurgeArchiveManagerImplRoot.Rule)
     */
    @Override
    protected PurgeResult processRule(QueryDecorator queryDecorator, Rule rule)
        throws DataAccessException, PurgeArchiveException {
        // call super to process any rules it cares about
        super.processRule(queryDecorator, rule);

        ArchiveModelObject.setSiteMap(getSiteMap());

        // process rules for VoiceLink
        if (rule.modelObjectName.equals("Assignment")) {
            // Purge and archive(if needed) transaction data
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeAssignmentData(queryDecorator, rule);
            } else {
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Starting Archive Assignment Purge Process :::");
                }
                purgeArchivedAssignmentData(queryDecorator, rule);
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Archive Assignment Purge Process Complete :::");
                }
            }
        } else if (rule.modelObjectName.equals("License")) {
            // Purge and archive(if needed) transaction data
            if (log.isDebugEnabled()) {
                log.debug("### Starting Put Away License Purge Process :::");
            }
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeLicenseData(queryDecorator, rule);
            }
            if (log.isDebugEnabled()) {
                log.debug("### Put Away License Purge Process Complete :::");
            }
        } else if (rule.modelObjectName.equals("Replenishment")) {
            // Purge and archive(if needed) transaction data
            if (log.isDebugEnabled()) {
                log.debug("### Starting Replenishment Assignment Process :::");
            }
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeReplenishmentData(queryDecorator, rule);
            }
            if (log.isDebugEnabled()) {
                log
                    .debug("### Replenishment License Purge Process Complete :::");
            }
        } else if (rule.modelObjectName.equals("RouteStop")) {
            // Purge and archive(if needed) transaction data
            if (log.isDebugEnabled()) {
                log.debug("### Starting Route Stop Purge 1 at a time Process :::");
            }
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeRouteStopData(queryDecorator, rule);
            }
            if (log.isDebugEnabled()) {
                log.debug("### Route Stop Purge 1 at a time Process Complete :::");
            }
        } else if (rule.modelObjectName.equals("OperatorLabor")) {
            // Purge and archive(if needed) transaction data
            if (log.isDebugEnabled()) {
                log.debug("### Starting Operator Labor Purge Process :::");
            }
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeOperatorLaborData(queryDecorator, rule);
            }
            if (log.isDebugEnabled()) {
                log.debug("### Operator Labor Purge Process Complete :::");
            }
        } else if (rule.modelObjectName.equals("PtsLicense")) {
            // Purge and archive(if needed) transaction data
            if (rule.action.equals(RULE_ACTION_PURGE)) {

                purgePtsLicenseData(queryDecorator, rule);
            } else {
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Starting Archive PTS License Purge Process :::");
                }
                purgeArchivedPtsLicenseData(queryDecorator, rule);
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Archive PTS License Purge Process Complete :::");
                }
            }
        } else if (rule.modelObjectName.equals("PtsContainer")) {
            // Purge and archive(if needed) transaction data
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgePtsContainerData(queryDecorator, rule);
            } else {
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Starting Archive PTS Container Purge Process :::");
                }
                purgeArchivedPtsContainerData(queryDecorator, rule);
                if (log.isDebugEnabled()) {
                    log.debug("### Archive PTS Container Purge Process :::");
                }
            }
        } else if (rule.modelObjectName.equals("CycleCountingAssignment")) {
            // Purge and archive(if needed) transaction data
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeCycleCountingAssignmentData(queryDecorator, rule);
            } else {
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Starting Archive Cycle Counting Assignment Purge Process :::");
                }
                purgeArchivedCycleCountingAssignmentData(queryDecorator, rule);
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Archive Cycle Counting Assignment Purge Process Complete :::");
                }
            }
        } else if (rule.modelObjectName.equals("LoadingRoute")) {
            // Purge and archive(if needed) transaction data
            if (rule.action.equals(RULE_ACTION_PURGE)) {
                purgeLoadingRouteData(queryDecorator, rule);
            } else {
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Starting Archive Loading Route Purge Process :::");
                }
                purgeArchivedLoadingRouteData(queryDecorator, rule);
                if (log.isDebugEnabled()) {
                    log
                        .debug("### Archive Loading Route Purge Process Complete :::");
                }
            }
        }
        
        return null;
    }

    /**
     * Purge Operator Labor records.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeOperatorLaborData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        // Purge marked records
        do {
            records = getOperatorLaborManager().executePurge(queryDecorator,
                rule.purgeDate);
            purgeArchiveSleep(records);

        } while (records == getBatchSize());
    }

    /**
     * Purge Lineloading Route Stops.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeRouteStopData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        QueryDecorator queryDec = new QueryDecorator();
        // Purge marked records
        // Route stops should be purged 1 at a time.
        queryDec.setRowCount(1);

        do {
            records = getRouteStopManager().executePurge(queryDec,
                RouteStopStatus.valueOf(rule.statusName), rule.purgeDate);

            purgeArchiveSleep(records);

        } while (records == 1);
    }

    /**
     * Purge Replenishment Assignments.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeReplenishmentData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        // Purge marked records
        do {
            records = getReplenishmentManager().executePurge(queryDecorator,
                ReplenishStatus.valueOf(rule.statusName), rule.purgeDate);
            purgeArchiveSleep(records);

        } while (records == getBatchSize());

    }

    /**
     * Purge Put Away Licenses.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeLicenseData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        // Purge marked records
        do {
            records = getLicenseManager().executePurge(queryDecorator,
                LicenseStatus.valueOf(rule.statusName), rule.purgeDate);

            purgeArchiveSleep(records);

        } while (records == getBatchSize());
    }

    /**
     * Purge Selection Assignment data and archive if needed.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeAssignmentData(QueryDecorator queryDecorator, Rule rule) {

        // mark records to archive
        Integer updated = getAssignmentToArchiveManager().executeMarkForPurge(
            AssignmentStatus.valueOf(rule.statusName), rule.purgeDate,
            rule.archive);

        if (updated > 0) {
            // Archive marked records is needed
            int records = 0;
            if (rule.archive) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting Assignment Archive Process :::");
                }
                do {
                    records = getAssignmentToArchiveManager().executeArchive(
                        queryDecorator);

                    purgeArchiveSleep(records);
                } while (records == getBatchSize());
            }
            if (log.isDebugEnabled()) {
                log.debug("### Starting Assignment Purge Process :::");
            }
            do {
                records = getAssignmentToArchiveManager()
                    .executePurgeAssignment();
                purgeArchiveSleep(records);
            } while (records == getBatchSize());
        }

    }

    /**
     * Purge Archive Selection Assignment data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeArchivedAssignmentData(QueryDecorator queryDecorator,
                                             Rule rule) {

        int records = 0;
        // Purge Archive Assignment records
        do {
            records = getArchiveAssignmentManager().executePurge(
                AssignmentStatus.valueOf(rule.statusName), rule.purgeDate);
            purgeArchiveSleep(records);
        } while (records == getBatchSize());
    }

    /**
     * Purge PTS License data and archive if needed.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgePtsLicenseData(QueryDecorator queryDecorator, Rule rule) {

        Integer updated = getPtsLicenseToArchiveManager().executeMarkForPurge(
            PtsLicenseStatus.valueOf(rule.statusName), rule.purgeDate,
            rule.archive);

        if (updated > 0) {
            // Archive marked records if needed
            int records = 0;
            if (rule.archive) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting PTS License Archive Process :::");
                }
                do {
                    records = getPtsLicenseToArchiveManager().executeArchive(
                        queryDecorator);

                    purgeArchiveSleep(records);
                } while (records == getBatchSize());
            }

            // Purge marked records
            if (log.isDebugEnabled()) {
                log.debug("### Starting PTS License Purge Process :::");
            }
            do {
                records = getPtsLicenseToArchiveManager()
                    .executePurgePtsLicense(queryDecorator);
                purgeArchiveSleep(records);
            } while (records == getBatchSize());

        }
    }

    /**
     * Purge Archived PTS License data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeArchivedPtsLicenseData(QueryDecorator queryDecorator,
                                             Rule rule) {

        int records = 0;
        do {
            // Purge records from archive
            records = getArchivePtsLicenseManager().executePurge(
                PtsLicenseStatus.valueOf(rule.statusName), rule.purgeDate);
        } while (records == getBatchSize());

    }

    /**
     * Purge PTS Container data and archive if needed.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgePtsContainerData(QueryDecorator queryDecorator, Rule rule) {

        Integer updated = getPtsContainerToArchiveManager()
            .executeMarkForPurge(PtsContainerStatus.valueOf(rule.statusName),
                rule.purgeDate, rule.archive);

        if (updated > 0) {
            int records = 0;
            // Archive marked records is needed
            if (rule.archive) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting PTS Container Archive Process :::");
                }

                do {
                    records = getPtsContainerToArchiveManager().executeArchive(
                        queryDecorator);

                    purgeArchiveSleep(records);
                } while (records == getBatchSize());
            }
            if (log.isDebugEnabled()) {
                log.debug("### Starting PTS Container Purge Process :::");
            }
            do {
                // Purge PTS Containers
                records = getPtsContainerToArchiveManager()
                    .executePurgePtsContainer(queryDecorator);
            } while (records == getBatchSize());

        }
    }

    /**
     * Purge Archived PTS Container data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeArchivedPtsContainerData(QueryDecorator queryDecorator,
                                               Rule rule) {
        int records = 0;
        do {
            // Purge PTS Containers from archive
            getArchivePtsContainerManager().executePurge(
                PtsContainerStatus.valueOf(rule.statusName), rule.purgeDate);

        } while (records == getBatchSize());

    }
    
    /**
     * Purge Cycle Counting Assignment data and archive if needed.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeCycleCountingAssignmentData(QueryDecorator queryDecorator, Rule rule) {

        // mark records to archive
        Integer updated = getCycleCountingAssignmentToArchiveManager().executeMarkForPurge(
                CycleCountStatus.valueOf(rule.statusName), rule.purgeDate,
            rule.archive);

        if (updated > 0) {
            // Archive marked records is needed
            int records = 0;
            if (rule.archive) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting Cycle Counting Assignment Archive Process :::");
                }
                do {
                    records = getCycleCountingAssignmentToArchiveManager().executeArchive(
                        queryDecorator);

                    purgeArchiveSleep(records);
                } while (records == getBatchSize());
            }
            if (log.isDebugEnabled()) {
                log.debug("### Starting Cycle CountingAssignment Purge Process :::");
            }
            do {
                records = getCycleCountingAssignmentToArchiveManager()
                    .executePurgeCycleCountingAssignment(queryDecorator);
                purgeArchiveSleep(records);
            } while (records == getBatchSize());
        }

    }

    /**
     * Purge Archive Cycle Counting Assignment data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeArchivedCycleCountingAssignmentData(QueryDecorator queryDecorator,
                                             Rule rule) {

        int records = 0;
        // Purge Archive Cycle Counting Assignment records
        do {
            records = getArchiveCycleCountingAssignmentManager().executePurge(
                    CycleCountStatus.valueOf(rule.statusName), rule.purgeDate);
            purgeArchiveSleep(records);
        } while (records == getBatchSize());
    }
    
    
    /**
     * Purge Loading Route data and archive if needed.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeLoadingRouteData(QueryDecorator queryDecorator, Rule rule) {

        // mark records to archive
        Integer updated = getLoadingRouteToArchiveManager().executeMarkForPurge(
                LoadingRouteStatus.valueOf(rule.statusName), rule.purgeDate,
            rule.archive);

        if (updated > 0) {
            // Archive marked records is needed
            int records = 0;
            if (rule.archive) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting Loading Route Archive Process :::");
                }
                do {
                    records = getLoadingRouteToArchiveManager().executeArchive(
                        queryDecorator);

                    purgeArchiveSleep(records);
                } while (records == getBatchSize());
            }
            if (log.isDebugEnabled()) {
                log.debug("### Starting Loading Route Purge Process :::");
            }
            do {
                records = getLoadingRouteToArchiveManager()
                    .executePurge(queryDecorator);
                purgeArchiveSleep(records);
            } while (records == getBatchSize());
        }
    }
    
    /**
     * Purge Archive Loading Route data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeArchivedLoadingRouteData(QueryDecorator queryDecorator,
                                             Rule rule) {
        int records = 0;
        // Purge Archive Loading Route records
        do {
            records = getArchiveLoadingRouteManager().executePurge(
                LoadingRouteStatus.valueOf(rule.statusName), rule.purgeDate);
            purgeArchiveSleep(records);
        } while (records == getBatchSize());
    }
    /**
     * @return ArchivePtsLicenseManager
     */
    public ArchivePtsLicenseManager getArchivePtsLicenseManager() {
        return archivePtsLicenseManager;
    }

    /**
     * @param archivePtsLicenseManager - archive put to store license manager
     */
    public void setArchivePtsLicenseManager(ArchivePtsLicenseManager archivePtsLicenseManager) {
        this.archivePtsLicenseManager = archivePtsLicenseManager;
    }

    /**
     * Getter for the assignmentToArchiveManager property.
     * @return AssignmentToArchiveManager value of the property
     */
    public AssignmentToArchiveManager getAssignmentToArchiveManager() {
        return assignmentToArchiveManager;
    }

    /**
     * Setter for the assignmentToArchiveManager property.
     * @param assignmentToArchiveManager the new assignmentToArchiveManager
     *            value
     */
    public void setAssignmentToArchiveManager(AssignmentToArchiveManager assignmentToArchiveManager) {
        this.assignmentToArchiveManager = assignmentToArchiveManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * Getter for the licenseManager property.
     * @return LicenseManager value of the property
     */
    public LicenseManager getLicenseManager() {
        return licenseManager;
    }

    /**
     * Setter for the licenseManager property.
     * @param licenseManager the new licenseManager value
     */
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }

    /**
     * Getter for the replenishmentManager property.
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }

    /**
     * Setter for the replenishmentManager property.
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * Getter for the routeStopManager property.
     * @return RouteStopManager value of the property
     */
    public RouteStopManager getRouteStopManager() {
        return routeStopManager;
    }

    /**
     * Setter for the routeStopManager property.
     * @param routeStopManager the new routeStopManager value
     */
    public void setRouteStopManager(RouteStopManager routeStopManager) {
        this.routeStopManager = routeStopManager;
    }

    /**
     * Getter for the archiveAssignmentManager property.
     * @return ArchiveAssignmentManager value of the property
     */
    public ArchiveAssignmentManager getArchiveAssignmentManager() {
        return archiveAssignmentManager;
    }

    /**
     * Setter for the archiveAssignmentManager property.
     * @param archiveAssignmentManager the new archiveAssignmentManager value
     */
    public void setArchiveAssignmentManager(ArchiveAssignmentManager archiveAssignmentManager) {
        this.archiveAssignmentManager = archiveAssignmentManager;
    }

    /**
     * Getter for the ptsLicenseToArchiveManager property.
     * @return PtsLicenseToArchiveManager value of the property
     */
    public PtsLicenseToArchiveManager getPtsLicenseToArchiveManager() {
        return ptsLicenseToArchiveManager;
    }

    /**
     * Setter for the ptsLicenseToArchiveManager property.
     * @param ptsLicenseToArchiveManager the new ptsLicenseToArchiveManager
     *            value
     */
    public void setPtsLicenseToArchiveManager(PtsLicenseToArchiveManager ptsLicenseToArchiveManager) {
        this.ptsLicenseToArchiveManager = ptsLicenseToArchiveManager;
    }

    /**
     * Getter for the archivePtsContainerManager property.
     * @return ArchivePtsContainerManager value of the property
     */
    public ArchivePtsContainerManager getArchivePtsContainerManager() {
        return archivePtsContainerManager;
    }

    /**
     * Setter for the archivePtsContainerManager property.
     * @param archivePtsContainerManager the new archivePtsContainerManager
     *            value
     */
    public void setArchivePtsContainerManager(ArchivePtsContainerManager archivePtsContainerManager) {
        this.archivePtsContainerManager = archivePtsContainerManager;
    }

    /**
     * Getter for the ptsContainerToArchiveManager property.
     * @return PtsContainerToArchiveManager value of the property
     */
    public PtsContainerToArchiveManager getPtsContainerToArchiveManager() {
        return ptsContainerToArchiveManager;
    }

    /**
     * Setter for the ptsContainerToArchiveManager property.
     * @param ptsContainerToArchiveManager the new ptsContainerToArchiveManager
     *            value
     */
    public void setPtsContainerToArchiveManager(PtsContainerToArchiveManager ptsContainerToArchiveManager) {
        this.ptsContainerToArchiveManager = ptsContainerToArchiveManager;
    }

    /**
     * Getter for the archiveCycleCountingAssignmentManager property.
     * @return ArchiveCycleCountingAssignmentManager value of the property
     */
    public ArchiveCycleCountingAssignmentManager getArchiveCycleCountingAssignmentManager() {
        return archiveCycleCountingAssignmentManager;
    }

    /**
     * Setter for the archiveCycleCountingAssignmentManager property.
     * @param archiveCycleCountingAssignmentManager the new archiveCycleCountingAssignmentManager value
     */
    public void setArchiveCycleCountingAssignmentManager(
            ArchiveCycleCountingAssignmentManager archiveCycleCountingAssignmentManager) {
        this.archiveCycleCountingAssignmentManager = archiveCycleCountingAssignmentManager;
    }

    /**
     * Getter for the assignmentToCycleCountingArchiveManager property.
     * @return CycleCountingAssignmentToArchiveManager value of the property
     */
    public CycleCountingAssignmentToArchiveManager getCycleCountingAssignmentToArchiveManager() {
        return cycleCountingAssignmentToArchiveManager;
    }

    /**
     * Setter for the CycleCountingAssignmentToArchiveManager property.
     * @param cycleCountingAssignmentToArchiveManager the new cycleCountingAssignmentToArchiveManager
     *            value
     */
    public void setCycleCountingAssignmentToArchiveManager(
            CycleCountingAssignmentToArchiveManager cycleCountingAssignmentToArchiveManager) {
        this.cycleCountingAssignmentToArchiveManager = cycleCountingAssignmentToArchiveManager;
    }

    
    /**
     * @return the loadingRouteToArchiveManager
     */
    public LoadingRouteToArchiveManager getLoadingRouteToArchiveManager() {
        return loadingRouteToArchiveManager;
    }

    
    /**
     * @param loadingRouteToArchiveManager the loadingRouteToArchiveManager to set
     */
    public void setLoadingRouteToArchiveManager(LoadingRouteToArchiveManager loadingRouteToArchiveManager) {
        this.loadingRouteToArchiveManager = loadingRouteToArchiveManager;
    }

    
    /**
     * @return the archiveLoadingRouteManager
     */
    public ArchiveLoadingRouteManager getArchiveLoadingRouteManager() {
        return archiveLoadingRouteManager;
    }

    
    /**
     * @param archiveLoadingRouteManager the archiveLoadingRouteManager to set
     */
    public void setArchiveLoadingRouteManager(ArchiveLoadingRouteManager archiveLoadingRouteManager) {
        this.archiveLoadingRouteManager = archiveLoadingRouteManager;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 