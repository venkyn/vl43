/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.service.ReportParameterManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.OperatorTeamDAO;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.service.OperatorTeamManager;

import java.util.List;


/**
 * 
 *
 * @author mnichols
 */
public abstract class OperatorTeamManagerImplRoot extends
    GenericManagerImpl<OperatorTeam, OperatorTeamDAO> implements OperatorTeamManager {

    
    private ReportParameterManager reportParameterManager;
    /**
     * Constructor.
     * @param primaryDAO - the Operator Team DAO
     */
    public OperatorTeamManagerImplRoot(OperatorTeamDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorTeamManager#listAll()
     */
    public List<OperatorTeam> listAllOrderByName() throws DataAccessException {
        return getPrimaryDAO().listAllOrderByName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id) throws BusinessRuleException,
        DataAccessException {
        List<ReportParameter> params = reportParameterManager.getTeamReferences(id);
        if (params != null && params.size() > 0) {
            throw new BusinessRuleException(null, new UserMessage("operatorTeam.delete.message.report.references"));
        }
        return super.delete(id);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#delete(java.lang.Object)
     */
    @Override
    public Object delete(OperatorTeam instance) throws BusinessRuleException,
        DataAccessException {
        List<ReportParameter> params = reportParameterManager.getTeamReferences(instance.getId());
        if (params != null && params.size() > 0) {
            throw new BusinessRuleException(null, new UserMessage("operatorTeam.delete.message.report.references"));
        }
        return super.delete(instance);
    }

    
    /**
     * Getter for the ReportParameterManager property.
     * @return the value of the property
     */
    public ReportParameterManager getReportParameterManager() {
        return this.reportParameterManager;
    }

    
    /**
     * Setter for the ReportParameterManager property.
     * @param reportParameterManager the new ReportParameterManager value
     */
    public void setReportParameterManager(ReportParameterManager reportParameterManager) {
        this.reportParameterManager = reportParameterManager;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 