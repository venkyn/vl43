/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.dao.WorkgroupDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>Operator</code> model object.
 *
 * @author estoll
 */
public abstract class WorkgroupManagerImplRoot extends
    GenericManagerImpl<Workgroup, WorkgroupDAO> implements WorkgroupManager {

    private OperatorManager    operatorManager;

    // The task function management service.
    private TaskFunctionManager taskFunctionManager;


    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public WorkgroupManagerImplRoot(WorkgroupDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(Workgroup workgroup) throws BusinessRuleException, DataAccessException {
        // call the protectedSave with instruction to
        // check the defaultWorkgroup flag and if on,
        // turn off the flag on other workgroups
        return protectedSave(workgroup, true);
    }

    /**
     * Protected function to determine if checking to turn off default
     * workgroup is needed.
     * @param workgroup object
     * @param checkForDefault flag to indicate if we are concerned about
     * turning off the other default workgroup
     * @return results of super:save
     * @throws BusinessRuleException when saving fails
     * @throws DataAccessException when database exceptions occur
     */
    protected Object protectedSave(Workgroup workgroup,
                                   Boolean checkForDefault)
    throws BusinessRuleException, DataAccessException {

        // Business rule: If the workgroup being saved has the default
        // flag turned on, turn off the default flag on any other workgroup.
        // ie, only one workgroup may have the default flag turn on at
        // any given time.

        // There is a findDefaultWorkgroup method that returns a Workgroup object
        // but we were getting hibernate concurrency exceptions when we were trying
        // to save the current default workgroup.  The easy fix was to add a named
        // query to return the ID of the default Workgroup -- but so as not to return
        // a list of IDs, we used the MAX function because there should never be
        // more than one Workgroup with isDefault = true.
        if (workgroup.getIsDefault() && checkForDefault) {
            Number defaultId = this.maxDefaultWorkgroup();
            // If the ID is null, we don't care if its the same object, so we
            // can update the default status regardless. However, if the Ids are
            // the same for two different objects, Hibernate will throw an
            // exception about two different objects with the same ID being
            // associated with the session.
            if (((defaultId != null) && (workgroup.getId() == null))
                || ((defaultId != null) && (defaultId.longValue() != workgroup.getId().longValue()))) {

                Workgroup defaultWG = this.get(defaultId.longValue());
                defaultWG.setIsDefault(false);
                super.save(defaultWG);
            }
        }
        return super.save(workgroup);
    }

    /**
     * Pass this call on to the delete(object) method.
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Workgroup workgroup = get(id);
        return delete(workgroup);
    }

    /**
     * Deleting a workgroup means we must reassign operators
     * that are currently assigned to the workgroup being deleted.
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Workgroup wg)
        throws BusinessRuleException, DataAccessException {
        UserMessage userMessage = null;
        try {
        if (wg.getIsDefault()) {
            userMessage = assignOperatorsToNullWorkgroup(wg);
        } else {
            userMessage = assignOperatorsToDefaultWorkgroup(wg);
        }

        super.delete(wg);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("workgroup.delete.error.inUse");
            } else {
                throw ex;
            }
        }
        return userMessage;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#findByIdentifier(java.lang.String)
     */
    public Workgroup findDefaultWorkgroup() throws DataAccessException {
        return getPrimaryDAO().findDefaultWorkgroup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorManager#findByIdentifier(java.lang.String)
     */
    public Number maxDefaultWorkgroup() throws DataAccessException {
        return getPrimaryDAO().maxDefaultWorkgroup();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.WorkgroupManager#listAutoAddWorkgroups()
     */
    public List<Workgroup> listAutoAddWorkgroups() throws DataAccessException {
        return getPrimaryDAO().listAutoAddWorkgroups();
    }

    /**
     * This routine will get a list of operators that are assigned
     * to the workgroup being deleted, and reassign them to a null
     * workgroup.
     * @param wg being deleted
     * @return UserMessage that contains information for the GUI
     * @throws DataAccessException for any database exception
     * @throws BusinessRuleException for operator save exceptions
     */
    protected UserMessage assignOperatorsToNullWorkgroup(Workgroup wg)
    throws DataAccessException, BusinessRuleException {
        UserMessage userMessage = null;
        List<Operator> operators = operatorManager.getPrimaryDAO().listForWorkgroup(wg);
        int numOperators = operators.size();

        for (Operator operator : operators) {
            // reassign operator to a null workgroup
            operator.setWorkgroup(null);
            operatorManager.save(operator);
        }

        if (numOperators > 0) {
            // build a message that say
            // x operators of workgroup <deleted name> do not belong to a workgroup
            userMessage = new UserMessage("workgroup.delete.reassign.operator.null",
                numOperators, wg.getGroupName());
        }

        return userMessage;
    }

    /**
     * This routine will get a list of operators that are assigned
     * to the workgroup being deleted and either assign them to
     * the default workgroup (if one exist) or pass the request on to
     * assignOperatorsToNullWorkgroup method.
     * @param wg being deleted
     * @return UserMessage that contains information for the GUI
     * @throws DataAccessException on any database exception
     * @throws BusinessRuleException on any operator save business rule exception
     */
    protected UserMessage assignOperatorsToDefaultWorkgroup(Workgroup wg)
    throws DataAccessException, BusinessRuleException {
        Workgroup defaultWG = this.findDefaultWorkgroup();
        UserMessage userMessage = null;

        //Is there a default workgroup?
        if (defaultWG == null) {
            userMessage = assignOperatorsToNullWorkgroup(wg);
        } else {
            // get all operators for the workgroup being deleted
            List<Operator> operators = operatorManager.getPrimaryDAO()
                .listForWorkgroup(wg);

            int numOperators = operators.size();

            for (Operator operator : operators) {
                // reassign operator to the default workgroup
                operator.setWorkgroup(defaultWG);
                operatorManager.save(operator);
            }

            if (numOperators > 0) {
                // build a message that say
                // x operators reassigned from workgroup <deleted name>
                // to workgroup <default name>
                userMessage = new UserMessage("workgroup.delete.reassign.operator.default", false,
                    numOperators, wg.getGroupName(), defaultWG.getGroupName());
            }
        }
        return userMessage;
    }

    /**
     * Set the task function manager.
     * @return The task function management service.
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    /**
     * Get the task function manager.
     * @param taskFunctionManager The task function management service.
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }


    /**
     *
     * Create and return a default workgroup with all task functions added.
     * NOTE:  This should only be called from the CREATE SITE feature.
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     * @return The newly created Workgroup.
     */
    public Workgroup createDefaultWorkgroup()
    throws DataAccessException, BusinessRuleException {
        Workgroup wg = new Workgroup();
        // TODO: internationalize group name if need be
        wg.setGroupName("All work types");
        wg.setIsDefault(true);
//        wg.setVersion(version) // hibernate handles this
        wg.setAutoAddRegions(true);
        wg.setCreatedDate(new Date());

        // loop through and add all functions
        List<TaskFunction> taskFunctions = getTaskFunctionManager().getAll();
        for (TaskFunction taskFunction : taskFunctions) {
            wg.addTaskFunction(taskFunction, null);  // set region to null
        }

        // call the protectedSave with instruction to
        // not worry about the default flag.
        this.protectedSave(wg, false);
        return wg;
    }

    /**
     * getter for the list of workgroups to be assigned to a operator.
     * @return list of Workgroups
     * @throws DataAccessException if any
     */
    public List<Workgroup> getWorkgroupPerSite() throws DataAccessException {
        List<Workgroup> workgroups = new ArrayList<Workgroup>();
        List<Workgroup> wg = new ArrayList<Workgroup>();
        workgroups = getPrimaryDAO().getAll();

        SiteContext siteContext = SiteContextHolder.getSiteContext();
        for (Workgroup w : workgroups) {
            if (siteContext.isInCurrentSite(w)) {
                wg.add(w);
            }
        }
        return wg;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 