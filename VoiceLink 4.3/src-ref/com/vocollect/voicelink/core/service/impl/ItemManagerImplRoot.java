/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.ItemDAO;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.service.ItemManager;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>Item</code> model object.
 * 
 * @author ddoubleday
 */
public abstract class ItemManagerImplRoot extends GenericManagerImpl<Item, ItemDAO>
    implements ItemManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ItemManagerImplRoot(ItemDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get item by number.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ItemManager#findItemByNumber()
     */
    public Item findItemByNumber(String itemNumber) throws DataAccessException {
        return getPrimaryDAO().findItemByNumber(itemNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Item item = get(id);
        return delete(item);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Item instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("item.delete.error.inUse");
            } else {
                throw ex;
            }
        }
        return null;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 