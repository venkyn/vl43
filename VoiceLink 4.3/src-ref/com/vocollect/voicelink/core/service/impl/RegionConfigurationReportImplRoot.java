/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Implementation of Region Configurations Report.
 * 
 * @author kudupi
 */
public abstract class RegionConfigurationReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    /**
     * Bean for report.
     * 
     * @author kudupi
     */
    public class RegionConfigurationReportItem {

        private String profileName;

        private String profileNumber;

        // Variables for Normal Profile Configurations
        private String nAssignmentIssuance;

        private String nAllowMultipleAssignments;

        private String nAllowPassAssignments;

        private String nAllowBaseItems;

        private String nCaseLabelCheckDigits;

        private String nPromptType;

        private String nQuantityVerification;

        private String nNumberOfSpokenDigits;

        private String nNumberOfAssignments;

        private String nPickingType;

        private String nBaseQuantityThreshold;

        private String nBaseWeightThreshold;

        private String nAllowSkipAisle;

        private String nAllowSkipSlot;

        private String nAllowRepickSkips;

        private String nSupressSpeakingQuantity;

        private String nAllowDelivery;

        private String nPrintLabels;

        private String nPreCreateContainers;

        private String nPromptForId;

        private String nPromptDeliveryOnContainerClose;

        private String nAllowMultipleOpen;

        private String nRequiresTargetContainers;

        private String nConfirmDelivery;

        private String nSpokenContainerDigits;

        private String nAllowGoBackToShorts;

        private String nAllowMarkOutShorts;

        private String nVerifyLocationWithHost;

        private String nAllowSignoffDuringSelection;

        private String nAllowReversePicking;

        private String nAllowSummaryPrompt;

        // Variable for Chase Profile Configurations
        private String cAssignmentIssuance;

        private String cAllowMultipleAssignments;

        private String cAllowPassAssignments;

        private String cAllowBaseItems;

        private String cCaseLabelCheckDigits;

        private String cPromptType;

        private String cQuantityVerification;

        private String cNumberOfSpokenDigits;

        private String cNumberOfAssignments;

        private String cPickingType;

        private String cBaseQuantityThreshold;

        private String cBaseWeightThreshold;

        private String cAllowSkipAisle;

        private String cAllowSkipSlot;

        private String cAllowRepickSkips;

        private String cSupressSpeakingQuantity;

        private String cAllowDelivery;

        private String cPrintLabels;

        private String cPreCreateContainers;

        private String cPromptForId;

        private String cPromptDeliveryOnContainerClose;

        private String cAllowMultipleOpen;

        private String cRequiresTargetContainers;

        private String cConfirmDelivery;

        private String cSpokenContainerDigits;

        private String cAllowGoBackToShorts;

        private String cAllowMarkOutShorts;

        private String cVerifyLocationWithHost;

        private String cAllowSignoffDuringSelection;

        private String cAllowReversePicking;

        private String cAllowSummaryPrompt;

        /**
         * @return profileName
         */
        public String getProfileName() {
            return profileName;
        }

        /**
         * @param profileName String
         */
        public void setProfileName(String profileName) {
            this.profileName = profileName;
        }

        /**
         * @return profileNumber
         */
        public String getProfileNumber() {
            return profileNumber;
        }

        /**
         * @param profileNumber String
         */
        public void setProfileNumber(String profileNumber) {
            this.profileNumber = profileNumber;
        }

        /**
         * @return nAssignmentIssuance
         */
        public String getnAssignmentIssuance() {
            return nAssignmentIssuance;
        }

        /**
         * @param nassignmentIssuance String
         */
        public void setnAssignmentIssuance(String nassignmentIssuance) {
            this.nAssignmentIssuance = nassignmentIssuance;
        }

        /**
         * @return nAllowMultipleAssignments
         */
        public String getnAllowMultipleAssignments() {
            return nAllowMultipleAssignments;
        }

        /**
         * @param nallowMultipleAssignments String
         */
        public void setnAllowMultipleAssignments(String nallowMultipleAssignments) {
            this.nAllowMultipleAssignments = nallowMultipleAssignments;
        }

        /**
         * @return nAllowPassAssignments
         */
        public String getnAllowPassAssignments() {
            return nAllowPassAssignments;
        }


        /**
         * @param nallowPassAssignments String
         */
        public void setnAllowPassAssignments(String nallowPassAssignments) {
            this.nAllowPassAssignments = nallowPassAssignments;
        }

        /**
         * @return nAllowBaseItems String
         */
        public String getnAllowBaseItems() {
            return nAllowBaseItems;
        }

        /**
         * @param nallowBaseItems String
         */
        public void setnAllowBaseItems(String nallowBaseItems) {
            this.nAllowBaseItems = nallowBaseItems;
        }

        /**
         * @return nCaseLabelCheckDigits String
         */
        public String getnCaseLabelCheckDigits() {
            return nCaseLabelCheckDigits;
        }

        /**
         * @param ncaseLabelCheckDigits String
         */
        public void setnCaseLabelCheckDigits(String ncaseLabelCheckDigits) {
            this.nCaseLabelCheckDigits = ncaseLabelCheckDigits;
        }

        /**
         * @return nPromptType String
         */
        public String getnPromptType() {
            return nPromptType;
        }

        /**
         * @param npromptType String
         */
        public void setnPromptType(String npromptType) {
            this.nPromptType = npromptType;
        }

        /**
         * @return nQuantityVerification String
         */
        public String getnQuantityVerification() {
            return nQuantityVerification;
        }

        /**
         * @param nquantityVerification String
         */
        public void setnQuantityVerification(String nquantityVerification) {
            this.nQuantityVerification = nquantityVerification;
        }

        /**
         * @return nNumberOfSpokenDigits String
         */
        public String getnNumberOfSpokenDigits() {
            return nNumberOfSpokenDigits;
        }

        /**
         * @param nnumberOfSpokenDigits String
         */
        public void setnNumberOfSpokenDigits(String nnumberOfSpokenDigits) {
            this.nNumberOfSpokenDigits = nnumberOfSpokenDigits;
        }

        /**
         * @return nNumberOfAssignments String
         */
        public String getnNumberOfAssignments() {
            return nNumberOfAssignments;
        }

        /**
         * @param nnumberOfAssignments String
         */
        public void setnNumberOfAssignments(String nnumberOfAssignments) {
            this.nNumberOfAssignments = nnumberOfAssignments;
        }

        /**
         * @return nPickingType String
         */
        public String getnPickingType() {
            return nPickingType;
        }

        /**
         * @param npickingType String
         */
        public void setnPickingType(String npickingType) {
            this.nPickingType = npickingType;
        }

        /**
         * @return nBaseQuantityThreshold
         */
        public String getnBaseQuantityThreshold() {
            return nBaseQuantityThreshold;
        }

        /**
         * @param nbaseQuantityThreshold String
         */
        public void setnBaseQuantityThreshold(String nbaseQuantityThreshold) {
            this.nBaseQuantityThreshold = nbaseQuantityThreshold;
        }

        /**
         * @return nBaseWeightThreshold String
         */
        public String getnBaseWeightThreshold() {
            return nBaseWeightThreshold;
        }

        /**
         * @param nbaseWeightThreshold String
         */
        public void setnBaseWeightThreshold(String nbaseWeightThreshold) {
            this.nBaseWeightThreshold = nbaseWeightThreshold;
        }

        /**
         * @return nAllowSkipAisle String
         */
        public String getnAllowSkipAisle() {
            return nAllowSkipAisle;
        }

        /**
         * @param nallowSkipAisle String
         */
        public void setnAllowSkipAisle(String nallowSkipAisle) {
            this.nAllowSkipAisle = nallowSkipAisle;
        }

        /**
         * @return nAllowSkipSlot String
         */
        public String getnAllowSkipSlot() {
            return nAllowSkipSlot;
        }

        /**
         * @param nallowSkipSlot String
         */
        public void setnAllowSkipSlot(String nallowSkipSlot) {
            this.nAllowSkipSlot = nallowSkipSlot;
        }

        /**
         * @return nAllowRepickSkips String
         */
        public String getnAllowRepickSkips() {
            return nAllowRepickSkips;
        }

        /**
         * @param nallowRepickSkips String
         */
        public void setnAllowRepickSkips(String nallowRepickSkips) {
            this.nAllowRepickSkips = nallowRepickSkips;
        }

        /**
         * @return nSupressSpeakingQuantity String
         */
        public String getnSupressSpeakingQuantity() {
            return nSupressSpeakingQuantity;
        }

        /**
         * @param nsupressSpeakingQuantity String
         */
        public void setnSupressSpeakingQuantity(String nsupressSpeakingQuantity) {
            this.nSupressSpeakingQuantity = nsupressSpeakingQuantity;
        }

        /**
         * @return nAllowDelivery String
         */
        public String getnAllowDelivery() {
            return nAllowDelivery;
        }

        /**
         * @param nallowDelivery String
         */
        public void setnAllowDelivery(String nallowDelivery) {
            this.nAllowDelivery = nallowDelivery;
        }

        /**
         * @return nPrintLabels String
         */
        public String getnPrintLabels() {
            return nPrintLabels;
        }

        /**
         * @param nprintLabels String
         */
        public void setnPrintLabels(String nprintLabels) {
            this.nPrintLabels = nprintLabels;
        }

        /**
         * @return nPreCreateContainers String
         */
        public String getnPreCreateContainers() {
            return nPreCreateContainers;
        }

        /**
         * @param npreCreateContainers String
         */
        public void setnPreCreateContainers(String npreCreateContainers) {
            this.nPreCreateContainers = npreCreateContainers;
        }

        /**
         * @return nPromptForId String
         */
        public String getnPromptForId() {
            return nPromptForId;
        }

        /**
         * @param npromptForId String
         */
        public void setnPromptForId(String npromptForId) {
            this.nPromptForId = npromptForId;
        }

        /**
         * @return nPromptDeliveryOnContainerClose String
         */
        public String getnPromptDeliveryOnContainerClose() {
            return nPromptDeliveryOnContainerClose;
        }

        /**
         * @param npromptDeliveryOnContainerClose String
         */
        public void setnPromptDeliveryOnContainerClose(String npromptDeliveryOnContainerClose) {
            this.nPromptDeliveryOnContainerClose = npromptDeliveryOnContainerClose;
        }

        /**
         * @return nAllowMultipleOpen String
         */
        public String getnAllowMultipleOpen() {
            return nAllowMultipleOpen;
        }

        /**
         * @param nallowMultipleOpen String
         */
        public void setnAllowMultipleOpen(String nallowMultipleOpen) {
            this.nAllowMultipleOpen = nallowMultipleOpen;
        }

        /**
         * @return nRequiresTargetContainers String
         */
        public String getnRequiresTargetContainers() {
            return nRequiresTargetContainers;
        }

        /**
         * @param nrequiresTargetContainers String
         */
        public void setnRequiresTargetContainers(String nrequiresTargetContainers) {
            this.nRequiresTargetContainers = nrequiresTargetContainers;
        }

        /**
         * @return nConfirmDelivery String
         */
        public String getnConfirmDelivery() {
            return nConfirmDelivery;
        }

        /**
         * @param nconfirmDelivery String
         */
        public void setnConfirmDelivery(String nconfirmDelivery) {
            this.nConfirmDelivery = nconfirmDelivery;
        }

        /**
         * @return nSpokenContainerDigits String
         */
        public String getnSpokenContainerDigits() {
            return nSpokenContainerDigits;
        }

        /**
         * @param nspokenContainerDigits String
         */
        public void setnSpokenContainerDigits(String nspokenContainerDigits) {
            this.nSpokenContainerDigits = nspokenContainerDigits;
        }

        /**
         * @return nAllowGoBackToShorts String
         */
        public String getnAllowGoBackToShorts() {
            return nAllowGoBackToShorts;
        }

        /**
         * @param nallowGoBackToShorts String
         */
        public void setnAllowGoBackToShorts(String nallowGoBackToShorts) {
            this.nAllowGoBackToShorts = nallowGoBackToShorts;
        }

        /**
         * @return nAllowMarkOutShorts String
         */
        public String getnAllowMarkOutShorts() {
            return nAllowMarkOutShorts;
        }

        /**
         * @param nallowMarkOutShorts String
         */
        public void setnAllowMarkOutShorts(String nallowMarkOutShorts) {
            this.nAllowMarkOutShorts = nallowMarkOutShorts;
        }

        /**
         * @return nVerifyLocationWithHost String
         */
        public String getnVerifyLocationWithHost() {
            return nVerifyLocationWithHost;
        }

        /**
         * @param nverifyLocationWithHost String
         */
        public void setnVerifyLocationWithHost(String nverifyLocationWithHost) {
            this.nVerifyLocationWithHost = nverifyLocationWithHost;
        }

        /**
         * @return nAllowSignoffDuringSelection String
         */
        public String getnAllowSignoffDuringSelection() {
            return nAllowSignoffDuringSelection;
        }

        /**
         * @param nallowSignoffDuringSelection String
         */
        public void setnAllowSignoffDuringSelection(String nallowSignoffDuringSelection) {
            this.nAllowSignoffDuringSelection = nallowSignoffDuringSelection;
        }

        /**
         * @return nAllowReversePicking String
         */
        public String getnAllowReversePicking() {
            return nAllowReversePicking;
        }

        /**
         * @param nallowReversePicking String
         */
        public void setnAllowReversePicking(String nallowReversePicking) {
            this.nAllowReversePicking = nallowReversePicking;
        }

        /**
         * @return nAllowSummaryPrompt String
         */
        public String getnAllowSummaryPrompt() {
            return nAllowSummaryPrompt;
        }

        /**
         * @param nallowSummaryPrompt String
         */
        public void setnAllowSummaryPrompt(String nallowSummaryPrompt) {
            this.nAllowSummaryPrompt = nallowSummaryPrompt;
        }

        /**
         * @return cAssignmentIssuance String
         */
        public String getcAssignmentIssuance() {
            return cAssignmentIssuance;
        }

        /**
         * @param cassignmentIssuance String
         */
        public void setcAssignmentIssuance(String cassignmentIssuance) {
            this.cAssignmentIssuance = cassignmentIssuance;
        }

        /**
         * @return cAllowMultipleAssignments String
         */
        public String getcAllowMultipleAssignments() {
            return cAllowMultipleAssignments;
        }

        /**
         * @param callowMultipleAssignments String
         */
        public void setcAllowMultipleAssignments(String callowMultipleAssignments) {
            this.cAllowMultipleAssignments = callowMultipleAssignments;
        }

        /**
         * @return cAllowPassAssignments String
         */
        public String getcAllowPassAssignments() {
            return cAllowPassAssignments;
        }

        /**
         * @param callowPassAssignments String
         */
        public void setcAllowPassAssignments(String callowPassAssignments) {
            this.cAllowPassAssignments = callowPassAssignments;
        }

        /**
         * @return cAllowBaseItems String
         */
        public String getcAllowBaseItems() {
            return cAllowBaseItems;
        }

        /**
         * @param callowBaseItems String
         */
        public void setcAllowBaseItems(String callowBaseItems) {
            this.cAllowBaseItems = callowBaseItems;
        }

        /**
         * @return cCaseLabelCheckDigits String
         */
        public String getcCaseLabelCheckDigits() {
            return cCaseLabelCheckDigits;
        }

        /**
         * @param ccaseLabelCheckDigits String
         */
        public void setcCaseLabelCheckDigits(String ccaseLabelCheckDigits) {
            this.cCaseLabelCheckDigits = ccaseLabelCheckDigits;
        }

        /**
         * @return cPromptType String
         */
        public String getcPromptType() {
            return cPromptType;
        }

        /**
         * @param cpromptType String
         */
        public void setcPromptType(String cpromptType) {
            this.cPromptType = cpromptType;
        }

        /**
         * @return cQuantityVerification String
         */
        public String getcQuantityVerification() {
            return cQuantityVerification;
        }

        /**
         * @param cquantityVerification String
         */
        public void setcQuantityVerification(String cquantityVerification) {
            this.cQuantityVerification = cquantityVerification;
        }

        /**
         * @return cNumberOfSpokenDigits String
         */
        public String getcNumberOfSpokenDigits() {
            return cNumberOfSpokenDigits;
        }

        /**
         * @param cnumberOfSpokenDigits String
         */
        public void setcNumberOfSpokenDigits(String cnumberOfSpokenDigits) {
            this.cNumberOfSpokenDigits = cnumberOfSpokenDigits;
        }

        /**
         * @return cNumberOfAssignments String
         */
        public String getcNumberOfAssignments() {
            return cNumberOfAssignments;
        }

        /**
         * @param cnumberOfAssignments String
         */
        public void setcNumberOfAssignments(String cnumberOfAssignments) {
            this.cNumberOfAssignments = cnumberOfAssignments;
        }

        /**
         * @return cPickingType String
         */
        public String getcPickingType() {
            return cPickingType;
        }

        /**
         * @param cpickingType String
         */
        public void setcPickingType(String cpickingType) {
            this.cPickingType = cpickingType;
        }

        /**
         * @return cBaseQuantityThreshold String
         */
        public String getcBaseQuantityThreshold() {
            return cBaseQuantityThreshold;
        }

        /**
         * @param cbaseQuantityThreshold String
         */
        public void setcBaseQuantityThreshold(String cbaseQuantityThreshold) {
            this.cBaseQuantityThreshold = cbaseQuantityThreshold;
        }

        /**
         * @return cBaseWeightThreshold String
         */
        public String getcBaseWeightThreshold() {
            return cBaseWeightThreshold;
        }

        /**
         * @param cbaseWeightThreshold String
         */
        public void setcBaseWeightThreshold(String cbaseWeightThreshold) {
            this.cBaseWeightThreshold = cbaseWeightThreshold;
        }

        /**
         * @return cAllowSkipAisle String
         */
        public String getcAllowSkipAisle() {
            return cAllowSkipAisle;
        }

        /**
         * @param callowSkipAisle String
         */
        public void setcAllowSkipAisle(String callowSkipAisle) {
            this.cAllowSkipAisle = callowSkipAisle;
        }

        /**
         * @return cAllowSkipSlot String
         */
        public String getcAllowSkipSlot() {
            return cAllowSkipSlot;
        }

        /**
         * @param callowSkipSlot String
         */
        public void setcAllowSkipSlot(String callowSkipSlot) {
            this.cAllowSkipSlot = callowSkipSlot;
        }

        /**
         * @return cAllowRepickSkips String
         */
        public String getcAllowRepickSkips() {
            return cAllowRepickSkips;
        }

        /**
         * @param callowRepickSkips String
         */
        public void setcAllowRepickSkips(String callowRepickSkips) {
            this.cAllowRepickSkips = callowRepickSkips;
        }

        /**
         * @return cSupressSpeakingQuantity String
         */
        public String getcSupressSpeakingQuantity() {
            return cSupressSpeakingQuantity;
        }

        /**
         * @param csupressSpeakingQuantity String
         */
        public void setcSupressSpeakingQuantity(String csupressSpeakingQuantity) {
            this.cSupressSpeakingQuantity = csupressSpeakingQuantity;
        }

        /**
         * @return cAllowDelivery String
         */
        public String getcAllowDelivery() {
            return cAllowDelivery;
        }

        /**
         * @param callowDelivery String
         */
        public void setcAllowDelivery(String callowDelivery) {
            this.cAllowDelivery = callowDelivery;
        }

        /**
         * @return cPrintLabels String
         */
        public String getcPrintLabels() {
            return cPrintLabels;
        }

        /**
         * @param cprintLabels String
         */
        public void setcPrintLabels(String cprintLabels) {
            this.cPrintLabels = cprintLabels;
        }

        /**
         * @return cPreCreateContainers String
         */
        public String getcPreCreateContainers() {
            return cPreCreateContainers;
        }

        /**
         * @param cpreCreateContainers String
         */
        public void setcPreCreateContainers(String cpreCreateContainers) {
            this.cPreCreateContainers = cpreCreateContainers;
        }

        /**
         * @return cPromptForId String
         */
        public String getcPromptForId() {
            return cPromptForId;
        }

        /**
         * @param cpromptForId String
         */
        public void setcPromptForId(String cpromptForId) {
            this.cPromptForId = cpromptForId;
        }

        /**
         * @return cPromptDeliveryOnContainerClose String
         */
        public String getcPromptDeliveryOnContainerClose() {
            return cPromptDeliveryOnContainerClose;
        }

        /**
         * @param cpromptDeliveryOnContainerClose String
         */
        public void setcPromptDeliveryOnContainerClose(String cpromptDeliveryOnContainerClose) {
            this.cPromptDeliveryOnContainerClose = cpromptDeliveryOnContainerClose;
        }

        /**
         * @return cAllowMultipleOpen String
         */
        public String getcAllowMultipleOpen() {
            return cAllowMultipleOpen;
        }

        /**
         * @param callowMultipleOpen String
         */
        public void setcAllowMultipleOpen(String callowMultipleOpen) {
            this.cAllowMultipleOpen = callowMultipleOpen;
        }

        /**
         * @return cRequiresTargetContainers String
         */
        public String getcRequiresTargetContainers() {
            return cRequiresTargetContainers;
        }

        /**
         * @param crequiresTargetContainers String
         */
        public void setcRequiresTargetContainers(String crequiresTargetContainers) {
            this.cRequiresTargetContainers = crequiresTargetContainers;
        }

        /**
         * @return cConfirmDelivery String
         */
        public String getcConfirmDelivery() {
            return cConfirmDelivery;
        }

        /**
         * @param cconfirmDelivery String
         */
        public void setcConfirmDelivery(String cconfirmDelivery) {
            this.cConfirmDelivery = cconfirmDelivery;
        }

        /**
         * @return cSpokenContainerDigits String
         */
        public String getcSpokenContainerDigits() {
            return cSpokenContainerDigits;
        }

        /**
         * @param cspokenContainerDigits String
         */
        public void setcSpokenContainerDigits(String cspokenContainerDigits) {
            this.cSpokenContainerDigits = cspokenContainerDigits;
        }

        /**
         * @return cAllowGoBackToShorts String
         */
        public String getcAllowGoBackToShorts() {
            return cAllowGoBackToShorts;
        }

        /**
         * @param callowGoBackToShorts String
         */
        public void setcAllowGoBackToShorts(String callowGoBackToShorts) {
            this.cAllowGoBackToShorts = callowGoBackToShorts;
        }

        /**
         * @return cAllowMarkOutShorts String
         */
        public String getcAllowMarkOutShorts() {
            return cAllowMarkOutShorts;
        }

        /**
         * @param callowMarkOutShorts String
         */
        public void setcAllowMarkOutShorts(String callowMarkOutShorts) {
            this.cAllowMarkOutShorts = callowMarkOutShorts;
        }

        /**
         * @return cVerifyLocationWithHost String
         */
        public String getcVerifyLocationWithHost() {
            return cVerifyLocationWithHost;
        }

        /**
         * @param cverifyLocationWithHost String
         */
        public void setcVerifyLocationWithHost(String cverifyLocationWithHost) {
            this.cVerifyLocationWithHost = cverifyLocationWithHost;
        }

        /**
         * @return cAllowSignoffDuringSelection String
         */
        public String getcAllowSignoffDuringSelection() {
            return cAllowSignoffDuringSelection;
        }

        /**
         * @param callowSignoffDuringSelection String
         */
        public void setcAllowSignoffDuringSelection(String callowSignoffDuringSelection) {
            this.cAllowSignoffDuringSelection = callowSignoffDuringSelection;
        }

        /**
         * @return cAllowReversePicking String
         */
        public String getcAllowReversePicking() {
            return cAllowReversePicking;
        }

        /**
         * @param callowReversePicking String
         */
        public void setcAllowReversePicking(String callowReversePicking) {
            this.cAllowReversePicking = callowReversePicking;
        }

        /**
         * @return cAllowSummaryPrompt String
         */
        public String getcAllowSummaryPrompt() {
            return cAllowSummaryPrompt;
        }

        /**
         * @param callowSummaryPrompt String
         */
        public void setcAllowSummaryPrompt(String callowSummaryPrompt) {
            this.cAllowSummaryPrompt = callowSummaryPrompt;
        }
    }

    // Managers for queries
    private SelectionRegionManager selectionRegionManager;

    /**
     * @return selectionRegionManager SelectionRegionManager
     */
    public SelectionRegionManager getSelectionRegionManager() {
        return selectionRegionManager;
    }

    /**
     * @param selectionRegionManager SelectionRegionManager
     */
    public void setSelectionRegionManager(SelectionRegionManager selectionRegionManager) {
        this.selectionRegionManager = selectionRegionManager;
    }

    /**
     * Overriding the getDataSource method of ReportDataSourceManager
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values)
        throws Exception {

        List<RegionConfigurationReportItem> regionConfigurationReportItemList = new 
                                                        ArrayList<RegionConfigurationReportItem>();
        // Capturing parameter values entered by the user.
        String region = (String) values.get("REGION");

        if (region == null) { // If all option of selection region is selected.
            List<SelectionRegion> listSelectionRegion = getSelectionRegionManager()
                .getPrimaryDAO().listAllRegionsOrderByNumber();
            for (SelectionRegion selectionRegion : listSelectionRegion) {
                regionConfigurationReportItemList
                    .add(populateRegionConfigurationItem(selectionRegion));
            }
        } else { // If a specific selection region is selected.
            SelectionRegion selectionRegion = getSelectionRegionManager()
                .findRegionByNumber(Integer.parseInt(region));
            regionConfigurationReportItemList
                .add(populateRegionConfigurationItem(selectionRegion));
        }

        return new JRBeanCollectionDataSource(regionConfigurationReportItemList);
    }

    /**
     * Method to populate RegionConfigurationReportItem
     * 
     * @param selectionRegion SelectionRegion
     * @return regionConfigurationReportItem
     */
    private RegionConfigurationReportItem populateRegionConfigurationItem(SelectionRegion selectionRegion) {

        RegionConfigurationReportItem regionConfigurationReportItem = new RegionConfigurationReportItem();

        regionConfigurationReportItem.setProfileName(selectionRegion.getName());
        regionConfigurationReportItem.setProfileNumber(Integer.toString(selectionRegion.getNumber()));
        // Extracting localized value of string "Yes"
        String valueYes = ResourceUtil.getLocalizedKeyValue("region.create.label.allow.base.items.yes"); 
        // Extracting localized value of string "No"        
        String valueNo = ResourceUtil.getLocalizedKeyValue("region.create.label.allow.base.items.no");
        
        // Setters for Normal Assignments
        regionConfigurationReportItem.setnAllowBaseItems(
            selectionRegion.getProfileNormalAssignment().isAllowBaseItems() ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowDelivery(
            selectionRegion.getProfileNormalAssignment().isRequireDeliveryPrompt() ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowGoBackToShorts(
            getLocalizedValue(selectionRegion.getUserSettingsNormal().getGoBackForShortsIndicator().getResourceKey()));
        regionConfigurationReportItem.setnAllowMarkOutShorts(
            getLocalizedValue(selectionRegion.getUserSettingsNormal().getAutoMarkOutShorts().getResourceKey()));
        regionConfigurationReportItem.setnAllowMultipleAssignments(
            selectionRegion.getProfileNormalAssignment().isAllowMultipleAssignments() ? valueYes : valueNo);
        if (selectionRegion.getProfileNormalAssignment().getContainerType() != null) {
            regionConfigurationReportItem.setnAllowMultipleOpen(
                selectionRegion.getProfileNormalAssignment().getContainerType().isAllowMultipleOpenContainers() 
                ? valueYes : valueNo);
            regionConfigurationReportItem.setnPreCreateContainers(
                selectionRegion.getProfileNormalAssignment().getContainerType().getAllowPreCreationOfContainers() 
                ? valueYes : valueNo);
            regionConfigurationReportItem.setnPromptDeliveryOnContainerClose(
                selectionRegion.getProfileNormalAssignment().getContainerType()
                    .isRequireDeliveryAfterClosed() ? valueYes : valueNo);
            regionConfigurationReportItem.setnPromptForId(
                selectionRegion.getProfileNormalAssignment().getContainerType()
                .isPromptOperatorForContainerID()  ? valueYes : valueNo);
            regionConfigurationReportItem.setnRequiresTargetContainers(
                selectionRegion.getProfileNormalAssignment().getContainerType()
                    .isRequireTargetContainers()  ? valueYes : valueNo);
        } else {
            regionConfigurationReportItem.setnAllowMultipleOpen("---");
            regionConfigurationReportItem.setnPreCreateContainers("---");
            regionConfigurationReportItem.setnPromptDeliveryOnContainerClose("---");
            regionConfigurationReportItem.setnPromptForId("---");
            regionConfigurationReportItem.setnRequiresTargetContainers("---");
        }
        regionConfigurationReportItem.setnAllowPassAssignments(
            selectionRegion.getProfileNormalAssignment().isAllowPassingAssignment()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowRepickSkips(
            selectionRegion.getUserSettingsNormal().isAllowRepickSkips()  ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowReversePicking(
            selectionRegion.getUserSettingsNormal().isAllowReversePicking()  ? valueYes : valueNo);
        regionConfigurationReportItem
            .setnAllowSignoffDuringSelection(
                selectionRegion.getUserSettingsNormal().isAllowSignOffAtAnyPoint()
                ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowSkipAisle(
            selectionRegion.getUserSettingsNormal().isAllowSkipAisle()  ? valueYes : valueNo);
        regionConfigurationReportItem.setnAllowSkipSlot(
            selectionRegion.getUserSettingsNormal().isAllowSkipSlot()  ? valueYes : valueNo);
        int nSummaryPromptType = selectionRegion.getUserSettingsNormal().getSummaryPromptIndicator().ordinal();
        if (nSummaryPromptType == 2) { // If summary prompt is a Configured Prompt
            regionConfigurationReportItem
                .setnAllowSummaryPrompt(selectionRegion.getUserSettingsNormal()
                    .getSummaryPrompt().getName());
        } else {
            regionConfigurationReportItem
                .setnAllowSummaryPrompt(getLocalizedValue(selectionRegion
                    .getUserSettingsNormal().getSummaryPromptIndicator()
                    .getResourceKey()));
        }
        regionConfigurationReportItem.setnAssignmentIssuance(
            selectionRegion.getProfileNormalAssignment().isAutoIssuance()
            ? ResourceUtil
                .getLocalizedKeyValue("region.create.label.assignment.issuance.automatic") : ResourceUtil
                .getLocalizedKeyValue("region.create.label.assignment.issuance.manual"));
        regionConfigurationReportItem.setnBaseQuantityThreshold(Integer
            .toString(selectionRegion.getUserSettingsNormal()
                .getBaseQuantityThreshold()));
        regionConfigurationReportItem.setnBaseWeightThreshold(Double
            .toString(selectionRegion.getUserSettingsNormal()
                .getBaseWeightThreshold()));
        regionConfigurationReportItem.setnCaseLabelCheckDigits(
            selectionRegion.getProfileNormalAssignment().isRequireCaseLabelCheckDigits()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setnConfirmDelivery(
            selectionRegion.getUserSettingsNormal().isRequireDeliveryConfirmation()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setnNumberOfAssignments(Integer
            .toString(selectionRegion.getUserSettingsNormal()
                .getMaximumAssignmentsAllowed()));
        if (selectionRegion.getUserSettingsNormal()
            .getWorkIdentifierRequestLength() == 0) {
            regionConfigurationReportItem.setnNumberOfSpokenDigits(ResourceUtil
                .getLocalizedKeyValue("region.spokenDigits.all"));
        } else {
            regionConfigurationReportItem.setnNumberOfSpokenDigits(Integer
                .toString(selectionRegion.getUserSettingsNormal()
                    .getWorkIdentifierRequestLength()));
        }
        regionConfigurationReportItem.setnPickingType(
            getLocalizedValue(selectionRegion.getUserSettingsNormal().getPickBaseItemsBy().getResourceKey()));
        regionConfigurationReportItem.setnPrintLabels(
            getLocalizedValue(selectionRegion.getProfileNormalAssignment().getPrintContainerLabelsIndicator()
            .getResourceKey()));
        regionConfigurationReportItem.setnPromptType(
            selectionRegion.getProfileNormalAssignment().isMultiplePickPrompt()
            ? ResourceUtil
                .getLocalizedKeyValue("region.create.label.prompt.type.multiple") : ResourceUtil
                .getLocalizedKeyValue("region.create.label.prompt.type.single"));
        regionConfigurationReportItem.setnQuantityVerification(
            selectionRegion.getProfileNormalAssignment().isRequireQuantityVerification()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setnSpokenContainerDigits(Integer
            .toString(selectionRegion.getUserSettingsNormal()
                .getNumberOfContainerDigitsSpeak()));
        regionConfigurationReportItem.setnSupressSpeakingQuantity(
                selectionRegion.getUserSettingsNormal().isSuppressQuantityOfOne()
                ? valueYes : valueNo);
        regionConfigurationReportItem.setnVerifyLocationWithHost(
            selectionRegion.getUserSettingsNormal().isVerifyReplenishment()  ? valueYes : valueNo);

        // Setter for Chase Assignments
        regionConfigurationReportItem.setcAllowBaseItems(selectionRegion
            .getProfileChaseAssignment().isAllowBaseItems() ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowDelivery(selectionRegion
            .getProfileChaseAssignment().isRequireDeliveryPrompt()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowGoBackToShorts(getLocalizedValue(selectionRegion
            .getUserSettingsChase().getGoBackForShortsIndicator().getResourceKey()));
        regionConfigurationReportItem.setcAllowMarkOutShorts(getLocalizedValue(selectionRegion
            .getUserSettingsChase().getAutoMarkOutShorts().getResourceKey()));
        regionConfigurationReportItem
            .setcAllowMultipleAssignments(selectionRegion
                .getProfileChaseAssignment().isAllowMultipleAssignments()
                ? valueYes : valueNo);
        if (selectionRegion.getProfileChaseAssignment().getContainerType() != null) {
            regionConfigurationReportItem.setcAllowMultipleOpen(selectionRegion
                .getProfileChaseAssignment().getContainerType()
                .isAllowMultipleOpenContainers()  ? valueYes : valueNo);
            regionConfigurationReportItem
                .setcPreCreateContainers(selectionRegion
                    .getProfileChaseAssignment().getContainerType()
                    .getAllowPreCreationOfContainers()  ? valueYes : valueNo);
            regionConfigurationReportItem
                .setcPromptDeliveryOnContainerClose(selectionRegion
                    .getProfileChaseAssignment().getContainerType()
                    .isRequireDeliveryAfterClosed()  ? valueYes : valueNo);
            regionConfigurationReportItem.setcPromptForId(selectionRegion
                .getProfileChaseAssignment().getContainerType()
                .isPromptOperatorForContainerID()  ? valueYes : valueNo);
            regionConfigurationReportItem
                .setcRequiresTargetContainers(selectionRegion
                    .getProfileChaseAssignment().getContainerType()
                    .isRequireTargetContainers()  ? valueYes : valueNo);
        } else {
            regionConfigurationReportItem
                .setcAllowMultipleOpen("---");
            regionConfigurationReportItem
                .setcPreCreateContainers("---");
            regionConfigurationReportItem
                .setcPromptDeliveryOnContainerClose("---");
            regionConfigurationReportItem.setcPromptForId("---");
            regionConfigurationReportItem
                .setcRequiresTargetContainers("---");
        }
        regionConfigurationReportItem.setcAllowPassAssignments(selectionRegion
            .getProfileChaseAssignment().isAllowPassingAssignment()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowRepickSkips(selectionRegion
            .getUserSettingsChase().isAllowRepickSkips()  ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowReversePicking(selectionRegion
            .getUserSettingsChase().isAllowReversePicking()  ? valueYes : valueNo);
        regionConfigurationReportItem
            .setcAllowSignoffDuringSelection(selectionRegion
                .getUserSettingsChase().isAllowSignOffAtAnyPoint()
                ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowSkipAisle(selectionRegion
            .getUserSettingsChase().isAllowSkipAisle()  ? valueYes : valueNo);
        regionConfigurationReportItem.setcAllowSkipSlot(selectionRegion
            .getUserSettingsChase().isAllowSkipSlot()  ? valueYes : valueNo);
        int cSummaryPromptType = selectionRegion.getUserSettingsChase().getSummaryPromptIndicator().ordinal();
        if (cSummaryPromptType == 2) { // If summary prompt is a Configured Prompt
            regionConfigurationReportItem
                .setcAllowSummaryPrompt(selectionRegion.getUserSettingsChase()
                    .getSummaryPrompt().getName());
        } else {
            regionConfigurationReportItem
                .setcAllowSummaryPrompt(getLocalizedValue(selectionRegion
                    .getUserSettingsChase().getSummaryPromptIndicator()
                    .getResourceKey()));
        }
        regionConfigurationReportItem.setcAssignmentIssuance(selectionRegion
            .getProfileChaseAssignment().isAutoIssuance()
            ? ResourceUtil
                .getLocalizedKeyValue("region.create.label.assignment.issuance.automatic") : ResourceUtil
                .getLocalizedKeyValue("region.create.label.assignment.issuance.manual"));
        regionConfigurationReportItem.setcBaseQuantityThreshold(Integer
            .toString(selectionRegion.getUserSettingsChase()
                .getBaseQuantityThreshold()));
        regionConfigurationReportItem.setcBaseWeightThreshold(Double
            .toString(selectionRegion.getUserSettingsChase()
                .getBaseWeightThreshold()));
        regionConfigurationReportItem.setcCaseLabelCheckDigits(selectionRegion
            .getProfileChaseAssignment().isRequireCaseLabelCheckDigits()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setcConfirmDelivery(selectionRegion
            .getUserSettingsChase().isRequireDeliveryConfirmation()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setcNumberOfAssignments(Integer
            .toString(selectionRegion.getUserSettingsChase()
                .getMaximumAssignmentsAllowed()));
        if (selectionRegion.getUserSettingsChase()
            .getWorkIdentifierRequestLength() == 0) {
            regionConfigurationReportItem.setcNumberOfSpokenDigits(ResourceUtil
                .getLocalizedKeyValue("region.spokenDigits.all"));
        } else {
            regionConfigurationReportItem.setcNumberOfSpokenDigits(Integer
                .toString(selectionRegion.getUserSettingsChase()
                    .getWorkIdentifierRequestLength()));
        }
        regionConfigurationReportItem.setcPickingType(getLocalizedValue(selectionRegion
            .getUserSettingsChase().getPickBaseItemsBy().getResourceKey()));
        regionConfigurationReportItem.setcPrintLabels(getLocalizedValue(selectionRegion
            .getProfileChaseAssignment().getPrintContainerLabelsIndicator().getResourceKey()
            ));
        regionConfigurationReportItem.setcPromptType(selectionRegion
            .getProfileChaseAssignment().isMultiplePickPrompt()
            ? ResourceUtil
                .getLocalizedKeyValue("region.create.label.prompt.type.multiple") : ResourceUtil
                .getLocalizedKeyValue("region.create.label.prompt.type.single"));
        regionConfigurationReportItem.setcQuantityVerification(selectionRegion
            .getProfileChaseAssignment().isRequireQuantityVerification()
            ? valueYes : valueNo);
        regionConfigurationReportItem.setcSpokenContainerDigits(Integer
            .toString(selectionRegion.getUserSettingsChase()
                .getNumberOfContainerDigitsSpeak()));
        regionConfigurationReportItem
            .setcSupressSpeakingQuantity(selectionRegion.getUserSettingsChase()
                .isSuppressQuantityOfOne()  ? valueYes : valueNo);
        regionConfigurationReportItem
            .setcVerifyLocationWithHost(selectionRegion.getUserSettingsChase()
                .isVerifyReplenishment()  ? valueYes : valueNo);

        return regionConfigurationReportItem;
    }
    
    
    /**
     * @param keyValue String
     * @return Localized Key Value
     */
    private String getLocalizedValue(String keyValue) {
        return ResourceUtil.getLocalizedKeyValue(keyValue);
    }

    /**
     * {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<RegionConfigurationReportItem> reportList = new ArrayList<RegionConfigurationReportItem>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /**
     * {@inheritDoc}
     * 
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * 
     * @param beanClass - class report is based on.
     */
    public RegionConfigurationReportImplRoot(Class<RegionConfigurationReportItem> beanClass) {
        super(RegionConfigurationReportItem.class);
    }

    /**
     * Constructor.
     */
    public RegionConfigurationReportImplRoot() {
        super(RegionConfigurationReportItem.class);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 