/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.LabelValuePair;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorPerformanceReportRoot;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Implementation of Operator performance Report.
 *
 * @author krishna.udupi
 */
public abstract class OperatorPerformanceReportImplRoot extends JRAbstractBeanDataSourceProvider
implements ReportDataSourceManager, OperatorPerformanceReportRoot {

    private static final Long HOURS = 3600000L;
    private static final Long MINUTES = 60000L;

    private static final int OPERATOR_ID = 0;
    private static final int OPERATOR_NAME = 1;
    private static final int OPERATOR_ACTION = 2;
    private static final int TOTAL_DURATION = 3;
    private static final int TOTAL_COUNT = 4;
    private static final int GOAL_RATE = 5;

    /**
     * Bean for report.
     *
     * @author krishna.udupi
     */
    public class OperatorPerformace
    implements Comparable<OperatorPerformace> {
        private String operatorId;
        private String operatorName;
        private Long hoursWorked;
        private Long minutesWorked;
        private String function;
        private Double percentOfGoal = 0.0;
        private Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
        private Map<Integer, Long> durations = new HashMap<Integer, Long>();
        private boolean reverseOrder = false;

        /**
         * Getter for the reverseOrder property.
         * @return boolean value of the property
         */
        public boolean isReverseOrder() {
            return reverseOrder;
        }


        /**
         * Setter for the reverseOrder property.
         * @param reverseOrder the new reverseOrder value
         */
        public void setReverseOrder(boolean reverseOrder) {
            this.reverseOrder = reverseOrder;
        }

        /**
         * Getter for the operatorId property.
         * @return String value of the property
         */
        public String getOperatorId() {
            return operatorId;
        }

        /**
         * Setter for the operatorId property.
         * @param operatorId the new operatorId value
         */
        public void setOperatorId(String operatorId) {
            this.operatorId = operatorId;
        }

        /**
         * Getter for the operatorName property.
         * @return String value of the property
         */
        public String getOperatorName() {
            return operatorName;
        }

        /**
         * Setter for the operatorName property.
         * @param operatorName the new operatorName value
         */
        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        /**
         * Getter for the percentOfGoal property.
         * @return Double value of the property
         */
        public Double getPercentOfGoal() {
            return percentOfGoal;
        }

        /**
         * Getter for the hoursWorked property.
         * @return Long value of the property
         */
        public Long getHoursWorked() {
            return hoursWorked;
        }

        /**
         * Setter for the hoursWorked property.
         * @param hoursWorked the new hoursWorked value
         */
        public void setHoursWorked(Long hoursWorked) {
            this.hoursWorked = hoursWorked;
        }

        /**
         * Getter for the minutesWorked property.
         * @return Long value of the property
         */
        public Long getMinutesWorked() {
            return minutesWorked;
        }

        /**
         * Setter for the minutesWorked property.
         * @param minutesWorked the new minutesWorked value
         */
        public void setMinutesWorked(Long minutesWorked) {
            this.minutesWorked = minutesWorked;
        }

        /**
         * Getter for the function property.
         * @return String value of the property
         */
        public String getFunction() {
            return function;
        }

        /**
         * Setter for the function property.
         * @param function the new function value
         */
        public void setFunction(String function) {
            this.function = function;
        }

        /**
         * Calculate and set the percent of goal.
         */
        public void setPercentOfGoal() {
            List<Integer> goalRates = new ArrayList<Integer>(counts.keySet());
            Integer totalCount = 0;
            Long totalDuration = 0L;
            Double rate = 0.0;
            Double weight = 0.0;
            // iterates the loop to get the total of goal rate and total time spent
            for (Integer goalRate : goalRates) {
                totalCount += counts.get(goalRate);
                totalDuration += durations.get(goalRate);
            }
            // iterates the loop to get the total percent of goal
            for (Integer goalRate : goalRates) {
                if (durations.get(goalRate) != 0) {
                    rate = Double.valueOf(counts.get(goalRate))
                        / (Double.valueOf(durations.get(goalRate)) / Double.valueOf(HOURS));
                }
                if (totalDuration != 0) {
                    weight = ((Double.valueOf(durations.get(goalRate))
                        / Double.valueOf(totalDuration)));
                }
                percentOfGoal += (rate / Double.valueOf(goalRate)) * weight;
            }
            setHoursWorked(totalDuration / HOURS);
            setMinutesWorked((totalDuration - (getHoursWorked() * HOURS)) / MINUTES);
        }

        /**
         * Add values to operators record.
         *
         * @param goalRate - goal rate to add
         * @param duration - duration to add
         * @param count - count to add
         */
        public void addValues(Integer goalRate, Long duration, Integer count) {

            //If goal rate in undefined or zero then ignore data
            if (goalRate == null || goalRate.equals(0)) {
                return;
            }

            //if duration is undefined or 0 then ignore data
            if (duration == null || duration.equals(0)) {
                return;
            }

            //if count is undefined set to 0
            if (count == null) {
                count = 0;
            }

            //Add counts
            Integer totalCount = counts.get(goalRate);
            if (totalCount == null) {
                counts.put(goalRate, count);
            } else {
                counts.put(goalRate, count + totalCount);
            }

            //Add duration
            Long totalDuration = durations.get(goalRate);
            if (totalDuration == null) {
                durations.put(goalRate, duration);
            } else {
                durations.put(goalRate, duration + totalDuration);
            }
        }

        /**
         * {@inheritDoc}
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        public int compareTo(OperatorPerformace o) {
            int result = 0;
            result = o.getFunction().compareTo(this.getFunction());
            if (result == 0) {
                result = o.getPercentOfGoal().compareTo(this.getPercentOfGoal());
                if (isReverseOrder()) {
                    result = result * -1;
                }
            }
            return result;
        }
    }


    private OperatorLaborManager operatorLaborManager;
    private SelectionRegionManager selRegionManager = null;
    private PtsRegionManager ptsRegionManager = null;
    private ReplenishmentRegionManager repRegionManager = null;
    private PutawayRegionManager putRegionManager = null;
    private LineLoadingRegionManager llRegionManager = null;    
    private CycleCountingRegionManager cycleCountingRegionManager = null;
    private LoadingRegionManager loadRegionManager = null;
    
    
    
    /**
     * Getter for the loadingRegionManager property.
     * @return loadingRegionManager value of the property
     */
    public LoadingRegionManager getLoadRegionManager() {
        return loadRegionManager;
    }

    /**
     * Setter for the loadingRegionManager property.
     * @param loadingRegionManager the new loadingRegionManager value
     */
    public void setLoadRegionManager(LoadingRegionManager loadRegionManager) {
        this.loadRegionManager = loadRegionManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values) throws Exception {

        //Parameter values
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        Long count = (Long) values.get("OPERATOR_COUNT");
        String sortOrder = (String) values.get("SORT_ORDER");
        // TODO need to make this generic so that overwriting this in a map is not needed.
        // This Hack to put the right value in for the sort order in lieu of the ENUM value.
        values.put("SORT_ORDER",
                ResourceUtil.getLocalizedKeyValue("selection.report.operatorPerformance.sortOrder" + sortOrder));
        Integer function = convertParamToLong((String) values
            .get("ACTION_TYPE"));
        String region = (String) values.get("REGION");
        Long regionId = null;
        boolean reverseSort = false;
        OperatorLaborActionType action = null;

        if (sortOrder.equals("1")) {
            reverseSort = true;
        }

        if (function != null) {
            action = OperatorLaborActionType.toEnum(function);
        }
        if (region != null) {
            regionId = Long.parseLong(region);
        }


        //get data
        List<OperatorPerformace> fullList = queryData(startTime, endTime,
              action, reverseSort, regionId);
       // iterates through the result of the query and calculate the performance based on percent of goal
        for (OperatorPerformace op : fullList) {
            op.setPercentOfGoal();
        }
        //calculateOverallPerformance(fullList);



       if (function == null) {
            values.put("ACTION_TYPE", ResourceUtil
                .getLocalizedKeyValue("report.all"));
        } else {
            values.put("ACTION_TYPE", ResourceUtil
                .getLocalizedKeyValue(
                    OperatorLaborActionType.toEnum(function).getResourceKey()));
        }



        return new JRBeanCollectionDataSource(sortAndReduceList(fullList,
            count));
    }

    /**
     * Sort list by function and performance, and reduce list to number of
     * operators requested for each function.
     *
     * @param fullList - full list of operators and function performance
     * @param count - count of how many operators per function
     * @return - sorted and reduced list
     */
    protected List<OperatorPerformace> sortAndReduceList(
                List<OperatorPerformace> fullList,
                Long count) {
        List<OperatorPerformace> reportList = new ArrayList<OperatorPerformace>();

        Collections.sort(fullList);
        Collections.reverse(fullList);

        Integer currentCount = 0;
        Double lastPerformance = 0.0;
        String currentFunction = "";

        // iterates through the list to check if the function is blank
        for (OperatorPerformace op : fullList) {
            if (!currentFunction.equals(op.getFunction())) {
                currentCount = 1;
                lastPerformance = 0.0;
                currentFunction = op.getFunction();
            }

            //if number of operators for function not reached yet
            //or current operator's performance is equal to previous operators
            if (currentCount <= count
                || op.getPercentOfGoal().equals(lastPerformance)) {
                reportList.add(op);
                lastPerformance = op.getPercentOfGoal();
                currentCount++;
            }
        }

        return reportList;
    }
    /**
     * Calculate the performane for each object in list.
     * @param fullList - list of operators and functions
     */
    protected void calculateOverallPerformance(List<OperatorPerformace> fullList) {
        for (OperatorPerformace op : fullList) {
            op.setPercentOfGoal();
        }
    }

    /**
     * Query performance data from database.
     *
     * @param startTime - start time
     * @param endTime - end time
     * @param function - function to query
     * @param reverseSort - should be sorted lowest to highest
     * @param region - region number
     * @return - full ist of objects.
     */
    protected List<OperatorPerformace> queryData(Date startTime,
                                               Date endTime,
                                               OperatorLaborActionType function,
                                               boolean reverseSort, Long region) {
        Map<String, OperatorPerformace> fullList =
            new HashMap<String, OperatorPerformace>();
        List<Object[]> results = getOperatorLaborManager()
            .listPerformanceCounts(startTime, endTime, function, region);
        // iterates through the result and set the values in bean for displaying it on UI
        for (Object[] objs : results) {
            String key = (String) objs[OPERATOR_ID] + "_&_"
                + ((OperatorLaborActionType) objs[OPERATOR_ACTION]).toString();
            OperatorPerformace op = fullList.get(key);
            if (op == null) {
                op = new OperatorPerformace();
                op.setOperatorId((String) objs[OPERATOR_ID]);
                op.setOperatorName((String) objs[OPERATOR_NAME]);
                op.setFunction(ResourceUtil
                    .getLocalizedKeyValue(
                        ((OperatorLaborActionType) objs[OPERATOR_ACTION])
                        .getResourceKey()));
                op.setReverseOrder(reverseSort);
            }
            op.addValues((Integer) objs[GOAL_RATE],
                (Long) objs[TOTAL_DURATION],
                (Integer) objs[TOTAL_COUNT]);
            fullList.put(key, op);
        }

        return new ArrayList<OperatorPerformace>(fullList.values());
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<OperatorPerformace> reportList = new ArrayList<OperatorPerformace>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /**
     * Convert value parameter to Integer checking for null.
     *
     * @param value - value to convert
     * @return - Integer or null
     */
    protected Integer convertParamToLong(String value) {
        if (value == null) {
            return null;
        } else {
            return Integer.valueOf(value);
        }

    }


    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * @param beanClass - class report is based on.
     */
    public OperatorPerformanceReportImplRoot(Class<OperatorPerformace> beanClass) {
        super(OperatorPerformace.class);
    }

    /**
     * Constructor.
     */
    public OperatorPerformanceReportImplRoot() {
        super(OperatorPerformace.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorPerformanceReportRoot#getFunctions()
     */
    public List<LabelValuePair> getFunctions() {
        List<LabelValuePair> values = new ArrayList<LabelValuePair>();
        OperatorLaborActionType[] enums = OperatorLaborActionType.values();

        for (OperatorLaborActionType myEnum : enums) {
            if (myEnum.toValue() > OperatorLaborActionType.Break.toValue()) {
                LabelValuePair lp = new LabelValuePair(ResourceUtil.getLocalizedKeyValue(myEnum.getResourceKey()),
                   String.valueOf(myEnum.toValue()));
                values.add(lp);
            }
        }

        return values;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorPerformanceReportRoot#getSortOrders()
     */
    public List<LabelValuePair> getSortOrders() {
        List<LabelValuePair> values = new ArrayList<LabelValuePair>();

        LabelValuePair lp = new LabelValuePair(
            ResourceUtil.getLocalizedKeyValue("selection.report.operatorPerformance.sortOrder1"),
            "1");
        values.add(lp);
        lp = new LabelValuePair(
            ResourceUtil.getLocalizedKeyValue("selection.report.operatorPerformance.sortOrder2"),
            "2");
        values.add(lp);

        return values;
    }
    
    /**
     * @return an array of regions for the performance report
     * @throws DataAccessException
     */
    public List<Region> getRegions() throws DataAccessException {
        ArrayList<Region> regions = null;
        regions = new ArrayList<Region>(selRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber());
        regions.addAll(new ArrayList<Region>(repRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber()));
        regions.addAll(new ArrayList<Region>(putRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber()));
        regions.addAll(new ArrayList<Region>(llRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber()));
        regions.addAll(new ArrayList<Region>(ptsRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber()));  
        regions.addAll(new ArrayList<Region>(cycleCountingRegionManager.getPrimaryDAO()
            .listAllRegionsOrderByNumber()));
        regions.addAll(new ArrayList<Region>(loadRegionManager.getPrimaryDAO()
                .listAllRegionsOrderByNumber()));
        return regions;
    }

    
    /**
     * Getter for the selRegionManager property.
     * @return the value of the property
     */
    public SelectionRegionManager getSelRegionManager() {
        return this.selRegionManager;
    }

    
    /**
     * Setter for the selRegionManager property.
     * @param selRegionManager the new selRegionManager value
     */
    public void setSelRegionManager(SelectionRegionManager selRegionManager) {
        this.selRegionManager = selRegionManager;
    }

    
    /**
     * Getter for the ptsRegionManager property.
     * @return the value of the property
     */
    public PtsRegionManager getPtsRegionManager() {
        return this.ptsRegionManager;
    }

    
    /**
     * Setter for the ptsRegionManager property.
     * @param ptsRegionManager the new ptsRegionManager value
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }
    
      
    /**
     * Getter for the repRegionManager property.
     * @return the value of the property
     */
    public ReplenishmentRegionManager getRepRegionManager() {
        return this.repRegionManager;
    }

    
    /**
     * Setter for the repRegionManager property.
     * @param repRegionManager the new repRegionManager value
     */
    public void setRepRegionManager(ReplenishmentRegionManager repRegionManager) {
        this.repRegionManager = repRegionManager;
    }

    
    /**
     * Getter for the putRegionManager property.
     * @return the value of the property
     */
    public PutawayRegionManager getPutRegionManager() {
        return this.putRegionManager;
    }

    
    /**
     * Setter for the putRegionManager property.
     * @param putRegionManager the new putRegionManager value
     */
    public void setPutRegionManager(PutawayRegionManager putRegionManager) {
        this.putRegionManager = putRegionManager;
    }

    
    /**
     * Getter for the llRegionManager property.
     * @return the value of the property
     */
    public LineLoadingRegionManager getLlRegionManager() {
        return this.llRegionManager;
    }

    
    /**
     * Setter for the llRegionManager property.
     * @param llRegionManager the new llRegionManager value
     */
    public void setLlRegionManager(LineLoadingRegionManager llRegionManager) {
        this.llRegionManager = llRegionManager;
    }

    
    /**
     * Getter for the ccRegionManager property.
     * @return CycleCountingRegionManager value of the property
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    
    /**
     * Setter for the ccRegionManager property.
     * @param ccRegionManager the new ccRegionManager value
     */
    public void setCycleCountingRegionManager(CycleCountingRegionManager ccRegionManager) {
        this.cycleCountingRegionManager = ccRegionManager;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 