/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.LocationDAO;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.replenishment.model.ReplenishPriority;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.service.PickManager;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>Location</code> model object.
 * 
 * @author ddoubleday
 */
public abstract class LocationManagerImplRoot
    extends GenericManagerImpl<Location, LocationDAO>
    implements LocationManager {
    
    private PickManager             pickManager;
    
    private ReplenishmentManager    replenishmentManager;

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;
    
    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }


    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }


    /**
     * Getter for the pickManager property.
     * @return PickManager value of the property
     */
    public PickManager getPickManager() {
        return pickManager;
    }

    
    /**
     * Setter for the pickManager property.
     * @param pickManager the new pickManager value
     */
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }

    
    /**
     * Getter for the replenishmentManager property.
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }

    
    /**
     * Setter for the replenishmentManager property.
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LocationManagerImplRoot(LocationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get specified location.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.LocationManager#findLocationFromID(java.lang.String)
     */
    public Location findLocationFromID(long locationID)
        throws DataAccessException {
        return getPrimaryDAO().findLocationFromID(locationID);
    }

    /**
     * Implementation to get specified location.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.LocationManager#findLocationByScanned(java.lang.String)
     */
    public Location findLocationByScanned(String locationID)
        throws DataAccessException {
        return getPrimaryDAO().findLocationByScanned(locationID);
    }
    
    /**
     * retrieves top 100 location scanned verification 
     * for auto complete feature of loading dock doors.
     * 
     * @param scannedVerification - value to retrieve by
     * @return the location, may be null
     * @throws DataAccessException - database exceptions
     */
    public List<String> listLocationsAutoComplete(String scannedVerification) 
        throws DataAccessException {
        final int resultLimit = 100;
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(resultLimit);
        return getPrimaryDAO().listLocationsAutoComplete(qd, scannedVerification.toLowerCase() + "%");
    }

    /**
     * Implementation to get specified location.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.LocationManager#findLocationBySpoken(java.lang.String)
     */
    public Location findLocationBySpoken(String locationID, String checkDigits)
        throws DataAccessException {
        return getPrimaryDAO().findLocationBySpoken(locationID, checkDigits);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Location item = get(id);
        return delete(item);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Location instance)
        throws BusinessRuleException, DataAccessException {
        try {
            super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("location.delete.error.inUse");
            } else {
                throw ex;
            }
        }
        return null;
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeReportShort(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void reportShort(Location location, Item item) 
    throws BusinessRuleException, DataAccessException {
        //If the location is locked due to invalid lot, we don't want to be changing
        //the status
        if (!location.getStatus(item).equals(LocationStatus.InvalidLot)) {

            LocationStatus ls = LocationStatus.Empty;

            validateReplenishAction(location, item);

            //Get next replenishment and if found, and available or unavailable
            //set to high priority. If in-progress we do not need to set anything
            Replenishment r = getReplenishmentManager().findNextReplenishment(
                location, item);
            if (r != null) {
                if (r.getStatus().equals(ReplenishStatus.Unavailable)
                    || r.getStatus().equals(ReplenishStatus.Available)) {
                    r.setPriority(ReplenishPriority.High);
                    ls = LocationStatus.Pending;
                } else {
                    ls = LocationStatus.InProgress;
                }
            }

            //Set status
            location.setStatus(ls, item);

            //Auto mark out shorts.
            if (ls.equals(LocationStatus.Empty)) {
                getPickManager().executeAutoMarkOutShorts(location, item);
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeReportShort(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeReportShort(Location location, Item item) 
    throws BusinessRuleException, DataAccessException {
        reportShort(location, item);
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeTriggerReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void triggerReplenishment(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        
        validateReplenishAction(location, item);
        
        Replenishment r = getReplenishmentManager().findNextReplenishment(
            location, item);
        if (r != null) {
            if (r.getStatus().equals(ReplenishStatus.Unavailable)
                || r.getStatus().equals(ReplenishStatus.Available)) {
                r.setPriority(ReplenishPriority.High);
            }
        }
        
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeTriggerReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeTriggerReplenishment(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        triggerReplenishment(location, item);
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeReportReplenished(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void reportReplenished(Location location, Item item)
    throws BusinessRuleException, DataAccessException {

        validateReplenishAction(location, item,
            "locationItem.reportReplen.error.itemNotMapped");
        if (!location.getStatus(item).equals(LocationStatus.InvalidLot)) {
            location.setStatus(LocationStatus.Replenished, item);

            Replenishment r = getReplenishmentManager().findNextReplenishment(
                location, item);
            if (r != null) {
                if (r.getPriority().equals(ReplenishPriority.High)) {
                    r.setPriority(ReplenishPriority.Normal);
                }
            }    
        } else {
              BusinessRuleException be = new BusinessRuleException(
                CoreErrorCode.LOCATION_WITH_INVALID_LOT,
                new UserMessage("location.setToReplenished.error.invalidLotStatus",
                     location.getScannedVerification()));
            throw be;
         }
    }

    
    /**
     * Sets invalid lot status to replenished.
     * 
     * @param location the location to be changed
     * @param item the item to be changed
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - DAO exceptions 
     */    
    public void clearInvalidLot(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        this.validateClearInvalidLotAction(location, item,
            "locationItem.invalidLot.error.invalidStatus");
        location.setStatus(LocationStatus.Replenished, item);
        Replenishment r = getReplenishmentManager().findNextReplenishment(
            location, item);
        if (r != null) {
            if (r.getPriority().equals(ReplenishPriority.High)) {
                r.setPriority(ReplenishPriority.Normal);
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeReportReplenished(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeReportReplenished(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        reportReplenished(location, item);
    }
    
    /**
     * Executes clearing of invalid lots.
     * 
     * @param location the location to be changed
     * @param item the item to be changed
     * @throws BusinessRuleException - Business exceptions
     * @throws DataAccessException - DAO exceptions 
     */    
    public void executeClearInvalidLot(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        clearInvalidLot(location, item);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeStartReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void startReplenishment(Location location, Item item)
    throws BusinessRuleException {
        
        // If the location is locked due to invalid lot, we don't want to be
        // changing the status
        if (!location.getStatus(item).equals(LocationStatus.InvalidLot)) {

            validateReplenishAction(location, item);

            // Only report in-progree if location is not already replenished.
            if (!location.getStatus(item).equals(LocationStatus.InvalidLot)
                && !location.getStatus(item).equals(LocationStatus.Replenished)) {
                location.setStatus(LocationStatus.InProgress, item);
            }
        }
        
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeStartReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeStartReplenishment(Location location, Item item)
    throws BusinessRuleException {
        if (location.getStatus(item).equals(LocationStatus.InvalidLot)) {
            BusinessRuleException be = new BusinessRuleException(
                CoreErrorCode.ITEM_LOCATION_ITEM_INVALID_LOT, new UserMessage(
                    "locationItem.replenAction.error.invalidLotStatus", item
                        .getNumber(), location.getScannedVerification()));
            throw be;
        }

        startReplenishment(location, item);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeCompleteReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void completeReplenishment(Location location, Item item)
    throws BusinessRuleException {
        validateReplenishAction(location, item);
        if (!location.getStatus(item).equals(LocationStatus.InvalidLot)) {
            location.setStatus(LocationStatus.Replenished, item);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LocationManagerRoot#executeCompleteReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeCompleteReplenishment(Location location, Item item)
    throws BusinessRuleException {
        completeReplenishment(location, item);
    }

    
    /**
     * Validate information for replenishment actions. 
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @throws BusinessRuleException - Business exceptions
     */
    protected void validateReplenishAction(Location location, 
                                           Item item) 
    throws BusinessRuleException {
        validateReplenishAction(location, item, 
            "locationItem.replenAction.error.itemNotMapped");
    }
    
    /**
     * Validate information for replenishment actions. 
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @param errorMessageKey - Error message key if default message is not wanted
     * @throws BusinessRuleException - Business exceptions
     */
    protected void validateClearInvalidLotAction(Location location, 
                                           Item item, String errorMessageKey) 
    throws BusinessRuleException {
        //Multiple items defined so item must be specified
        if (location.getItems().size() > 0) {
            //Item cannot be null
            if (item == null) {
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_MUST_BE_DEFINED,
                    new UserMessage("locationItem.replenAction.error.itemMustBeSpecified",
                        location.getScannedVerification()));
                
                logNotification(location, item, be);

                throw be;
            }
            
            if (location.getStatus(item).equals(LocationStatus.Unknown)) {
                // Create the exception so the notification can
                // retrieve the number
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED,
                    new UserMessage(errorMessageKey,
                        item.getNumber(), location.getScannedVerification()));

                logNotification(location, item, be);

                throw be;
            }
            
            if (!location.getStatus(item).equals(LocationStatus.InvalidLot)) {
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_NOT_INVALID_LOT,
                    new UserMessage("locationItem.clearLot.error.notInvalidLotStatus",
                        item.getNumber(), location.getScannedVerification()));
                throw be;
            }
            
        } else {
            if (!location.getStatus().equals(LocationStatus.InvalidLot)) {
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_NOT_INVALID_LOT,
                    new UserMessage("locationItem.clearLot.error.notInvalidLotStatus",
                         location.getScannedVerification()));
                throw be;
            }
        }
        
    }
    
    /**
     * Validate information for replenishment actions. 
     * 
     * @param location - Location where reported
     * @param item - Item shorted
     * @param errorMessageKey - Error message key if default message is not wanted
     * @throws BusinessRuleException - Business exceptions
     */
    protected void validateReplenishAction(Location location, 
                                           Item item, String errorMessageKey) 
    throws BusinessRuleException {
        //Multiple items defined so item must be specified
        if (location.getItems().size() > 0) {
            //Item cannot be null
            if (item == null) {
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_MUST_BE_DEFINED,
                    new UserMessage("locationItem.clearLot.error.itemMustBeSpecified",
                        location.getScannedVerification()));
                
                logNotification(location, item, be);

                throw be;
            }
            
            if (location.getStatus(item).equals(LocationStatus.Unknown)) {
                // Create the exception so the notification can
                // retrieve the number
                BusinessRuleException be = new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED,
                    new UserMessage(errorMessageKey,
                        item.getNumber(), location.getScannedVerification()));

                logNotification(location, item, be);

                throw be;
            }
         }  
    }
    
    /**
     * Utility to log a Location/Item status mapping error.
     * @param location being replenished
     * @param item being replenished
     * @param be exception being thrown
     */
    private void logNotification(Location location, Item item, 
                                 BusinessRuleException be) {

        String messageKey = null;
        
        if (be.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
            messageKey = "voicelink.notification.task.item.location.notmapped";
        } else if (be.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_INVALID_LOT) {
            messageKey = "voicelink.notification.task.item.location.notinvalidlot";            
        } else if (be.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_INVALID_LOT) {
            messageKey = "voicelink.notification.task.item.location.invalidlot";
        } else {
            messageKey = "voicelink.notification.replenish.location.status.unknown";
        }
        
        LOPArrayList lop = new LOPArrayList();
        if (item == null) {
            lop.add("task.itemNumber", new String("null"));
        } else {
            lop.add("task.itemNumber", item.getNumber().toString());
        }
        lop.add("task.locationScannedValue", 
            location.getScannedVerification());
        try {
            getVoicelinkNotificationUtil().createNotification(
                "notification.column.keyname.Process.GUIItemLocationStatus", 
                messageKey,
                NotificationPriority.CRITICAL,
                new Date(),
                be.getErrorCode().toString(),
                lop); 
        } catch (Exception ex) {
            // Do nothing -- we don't care about 
            // failures to post the notificaiton
        }

    }

    
    /**
     * This method is used in a location import validation.
     * It finds locations that have a different scannedVerification but have the
     * same combination of spokenVerification and checkDigits.  If one is found,
     * an import record should fail validation.
     * 
     * @param spokenVerification - spokenVerification to look for.
     * @param checkDigits - checkDigits to look for.
     * @param scannedVerification - scannedVerification (unique ID) not to find.
     * @return - location that blocks import, if any.
     */
    public Location findLocationImportBlockingCondition(
                                           String spokenVerification,
                                           String checkDigits,
                                           String scannedVerification) {

        return getPrimaryDAO().findLocationImportBlockingCondition(
            spokenVerification, checkDigits, scannedVerification);
    }    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 