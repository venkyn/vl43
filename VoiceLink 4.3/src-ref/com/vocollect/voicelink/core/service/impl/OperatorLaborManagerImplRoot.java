/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.dao.OperatorLaborByFunctionRegionDAO;
import com.vocollect.voicelink.core.dao.OperatorLaborByRegionDAO;
import com.vocollect.voicelink.core.dao.OperatorLaborDAO;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorFunctionLaborRoot;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborByFunctionRegion;
import com.vocollect.voicelink.core.model.OperatorLaborByRegion;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingRegionDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.putaway.dao.PutawayRegionDAO;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.puttostore.model.PtsOperatorLaborReport;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentRegionDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Additional service methods for the <code>OperatorLabor</code> model object.
 * 
 * @author pfunyak
 */
public abstract class OperatorLaborManagerImplRoot extends
    GenericManagerImpl<OperatorLabor, OperatorLaborDAO> implements
    OperatorLaborManager {

    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.00;

    private static final Double ONE_HUNDRED = 100.00;

    /** Region number for the region that represents "multiple". */
    public static final Integer MULTIPLE_REGION_NUMBER = -1;

    public String multipleRegionName = ResourceUtil
        .getLocalizedKeyValue("common.view.column.region.multiple");

    private static final int POS_ZERO = 0;

    private static final int POS_ONE = 1;

    private static final int POS_TWO = 2;

    private static final int POS_THREE = 3;

    private static final int POS_FOUR = 4;

    private static final int POS_FIVE = 5;

    private static final int POS_SIX = 6;

    private static final int HOURS_IN_DAY = 24;

    private static final int HOUR_TEN = 10;

    // this is here so we do not have to have multiple managers for aggreates
    private OperatorLaborByFunctionRegionDAO operatorLaborByFunctionRegionDAO;

    private OperatorLaborByRegionDAO operatorLaborByRegionDAO;

    private PutawayRegionDAO putawayRegionDAO;

    private ReplenishmentRegionDAO replenishmentRegionDAO;

    private CycleCountingRegionDAO cycleCountingRegionDAO;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Getter for the operatorLaborByRegionDAO property.
     * @return OperatorLaborByRegionDAO value of the property
     */
    public OperatorLaborByRegionDAO getOperatorLaborByRegionDAO() {
        return this.operatorLaborByRegionDAO;
    }

    /**
     * Setter for the operatorLaborByRegionDAO property.
     * @param operatorLaborByRegionDAO the new operatorLaborByRegionDAO value
     */
    public void setOperatorLaborByRegionDAO(OperatorLaborByRegionDAO operatorLaborByRegionDAO) {
        this.operatorLaborByRegionDAO = operatorLaborByRegionDAO;
    }

    /**
     * Getter for the operatorLaborByFunctionRegionDAO property.
     * @return OperatorLaborByFunctionRegionDAO value of the property
     */
    public OperatorLaborByFunctionRegionDAO getOperatorLaborByFunctionRegionDAO() {
        return this.operatorLaborByFunctionRegionDAO;
    }

    /**
     * Setter for the operatorLaborByFunctionRegionDAO property.
     * @param operatorLaborByFunctionRegionDAO the new
     *            operatorLaborByFunctionRegionDAO value
     */
    public void setOperatorLaborByFunctionRegionDAO(OperatorLaborByFunctionRegionDAO operatorLaborByFunctionRegionDAO) {
        this.operatorLaborByFunctionRegionDAO = operatorLaborByFunctionRegionDAO;
    }

    /**
     * Getter for the putawayRegionDAO property.
     * @return PutawayRegionDAO value of the property
     */
    public PutawayRegionDAO getPutawayRegionDAO() {
        return this.putawayRegionDAO;
    }

    /**
     * Setter for the putawayRegionDAO property.
     * @param putawayRegionDAO the new putawayRegionDAO value
     */
    public void setPutawayRegionDAO(PutawayRegionDAO putawayRegionDAO) {
        this.putawayRegionDAO = putawayRegionDAO;
    }

    /**
     * Getter for the replenishmentRegionDAO property.
     * @return ReplenishmentRegionDAO value of the property
     */
    public ReplenishmentRegionDAO getReplenishmentRegionDAO() {
        return this.replenishmentRegionDAO;
    }

    /**
     * Setter for the replenishmentRegionDAO property.
     * @param replenishmentRegionDAO the new replenishmentRegionDAO value
     */
    public void setReplenishmentRegionDAO(ReplenishmentRegionDAO replenishmentRegionDAO) {
        this.replenishmentRegionDAO = replenishmentRegionDAO;
    }

    /**
     * Getter for the cycleCountingRegionDAO property.
     * @return CycleCountingRegionDAO value of the property
     */
    public CycleCountingRegionDAO getCycleCountingRegionDAO() {
        return cycleCountingRegionDAO;
    }

    /**
     * Setter for the cycleCountingRegionDAO property.
     * @param cycleCountingRegionDAO the new cycleCountingRegionDAO value
     */
    public void setCycleCountingRegionDAO(CycleCountingRegionDAO cycleCountingRegionDAO) {
        this.cycleCountingRegionDAO = cycleCountingRegionDAO;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     * 
     */
    public OperatorLaborManagerImplRoot(OperatorLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#findOpenLaborRecord(java.lang.String)
     */
    @Override
    public OperatorLabor findOpenRecordByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().findOpenRecordByOperatorId(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#findOpenLaborRecord(java.lang.String)
     */
    @Override
    public List<OperatorLabor> listAllRecordsByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listAllRecordsByOperatorId(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listAllClosedBreakRecordsByOperatorId(long, java.util.Date, java.util.Date)
     */
    public List<OperatorLabor> listAllClosedBreakRecordsByOperatorId(long operatorId,
                                                                     Date timeWindowStartTime,
                                                                     Date timeWindowEndTime)
        throws DataAccessException {
        return getPrimaryDAO().listAllClosedBreakRecordsByOperatorId(
            operatorId, timeWindowStartTime, timeWindowEndTime);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listLaborByOperator(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> listLaborByOperator(ResultDataInfo rdi)
        throws DataAccessException {

        // This is a hack to get ALL of the operator labor records summarized
        // for a given operator.
        Object[] args = rdi.getQueryArgs();
        Date timeWindow = (Date) args[0];
        // First, get all sign on, sign off and break records and sum up the
        // total time.
        List<Object[]> data = this.getOperatorLaborByFunctionRegionDAO()
            .listOtherLaborByOperator(timeWindow);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        newList.addAll(buildOperatorLaborList(data));

        // Now get the "function" labor records grouped by function and region.
        data = this.getOperatorLaborByFunctionRegionDAO()
            .listLaborByFunctionRegion(timeWindow);
        newList.addAll(buildOperatorLaborList(data));

        data = this.getOperatorLaborByFunctionRegionDAO()
            .listLaborByFunctionAndNullRegion(timeWindow);
        newList.addAll(buildOperatorLaborList(data));

        // return the list.
        return newList;
    }

    /**
     * This method builds a list of Operator summary objects of type
     * OperatorLaborByFunctionRegion.
     * 
     * 
     * @param data the list of objects
     * @return a list of data objects
     * @throws DataAccessException - on Database error
     */
    protected ArrayList<DataObject> buildOperatorLaborList(List<Object[]> data)
        throws DataAccessException {

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());

        for (Object[] objArray : data) {
            OperatorLaborByFunctionRegion olObj = new OperatorLaborByFunctionRegion();
            olObj.setOperator((Operator) objArray[POS_ZERO]);
            // if filter type is "other", then region and action will always be
            // null
            // if filter type is "Selection" then the region will NEVER be null
            // If filter type is "Putaway", "Replenishment" or "Cycle Counting"
            // "LineLoading" then we
            // may or may not have a region.
            if ((OperatorLaborFilterType) objArray[POS_SIX] == OperatorLaborFilterType.Other) {
                olObj.setRegion(null);
                olObj.setActionType(null);
            } else if ((OperatorLaborFilterType) objArray[POS_SIX] == OperatorLaborFilterType.Selection) {
                olObj.setRegion((Region) objArray[POS_ONE]);
                olObj
                    .setActionType((OperatorLaborActionType) objArray[POS_TWO]);
            } else {
                if (objArray[POS_ONE] instanceof Region) {
                    olObj.setRegion((Region) objArray[POS_ONE]);
                } else {
                    olObj.setRegion(null);
                }
                olObj
                    .setActionType((OperatorLaborActionType) objArray[POS_TWO]);
            }
            olObj.setTotalQuantity((Integer) objArray[POS_THREE]);
            olObj.setTotalTime((Long) objArray[POS_FOUR]);
            olObj.setGoalRate((Integer) objArray[POS_FIVE]);
            // if we have a duration calculate the actual rate.
            if (olObj.getTotalTime() > 0) {
                olObj.setActualRate(new Double(olObj.getTotalQuantity()
                    / (olObj.getTotalTime() / MILLISEC_CONVERSION_FACTOR)));
            } else {
                olObj.setActualRate(new Double(0.0));
            }
            // if we know the region, set the site, get the goal rate and
            // calculate
            // the percent of goal if goal rate is not zero.
            if (olObj.getRegion() != null) {
                SiteContext siteContext = SiteContextHolder.getSiteContext();
                Site theSite = siteContext.getSite(olObj.getRegion());
                olObj.setSite(theSite);
                if ((olObj.getGoalRate() == null) || (olObj.getGoalRate() == 0)) {
                    olObj.setPercentOfGoal(new Double(0.0));
                } else {
                    olObj.setPercentOfGoal(new Double(olObj.getActualRate()
                        / olObj.getGoalRate() * ONE_HUNDRED));
                }
            } else {
                olObj.setSite(null);
                Double avgGoalRate = 0.0;

                // if we don't know the region, this means that we may be doing
                // put away, replenishment or cycle counting in multiple
                // regions. Calculate the percent of goal based on the average
                // goal rate FOR THE ENTIRE REGION.
                // TODO needs MORE refactoring...
                // region is null
                if (olObj.getRegion() == null) {
                    // We are doing Put Away
                    if (olObj.getActionType() == OperatorLaborActionType.PutAway) {
                        PutawayRegion pr = new PutawayRegion();
                        ResourceUtil.getLocalizedKeyValue("");
                        pr.setName(multipleRegionName);
                        pr.setNumber(MULTIPLE_REGION_NUMBER);
                        olObj.setGoalRate(this.getPutawayRegionDAO()
                            .avgGoalRate().intValue());
                        pr.setGoalRate(olObj.getGoalRate());
                        olObj.setRegion(pr);
                        avgGoalRate = this.getPutawayRegionDAO().avgGoalRate();
                    }
                    // We are doing Replenishment
                    if (olObj.getActionType() == OperatorLaborActionType.Replenishment) {
                        ReplenishmentRegion rr = new ReplenishmentRegion();
                        rr.setName(multipleRegionName);
                        rr.setNumber(MULTIPLE_REGION_NUMBER);
                        olObj.setGoalRate(this.getReplenishmentRegionDAO()
                            .avgGoalRate().intValue());
                        rr.setGoalRate(olObj.getGoalRate());
                        olObj.setRegion(rr);
                        avgGoalRate = this.getReplenishmentRegionDAO()
                            .avgGoalRate();
                    }
                    // We are doing Cycle Counting
                    if (olObj.getActionType() == OperatorLaborActionType.CycleCounting) {
                        CycleCountingRegion ccr = new CycleCountingRegion();
                        ccr.setName(multipleRegionName);
                        ccr.setNumber(MULTIPLE_REGION_NUMBER);
                        olObj.setGoalRate(this.getCycleCountingRegionDAO()
                            .avgGoalRate().intValue());
                        ccr.setGoalRate(olObj.getGoalRate());
                        olObj.setRegion(ccr);
                        avgGoalRate = this.getCycleCountingRegionDAO()
                            .avgGoalRate();
                    }
                }
                // If we are not doing put away, replenishment or cycle
                // counting, return 0 for the percent of goal
                if (avgGoalRate == 0) {
                    olObj.setPercentOfGoal(new Double(0.0));
                } else {
                    olObj.setPercentOfGoal(new Double(olObj.getActualRate()
                        / avgGoalRate * ONE_HUNDRED));
                    olObj.setGoalRate(avgGoalRate.intValue());
                }
            }
            olObj.setFilterType((OperatorLaborFilterType) objArray[POS_SIX]);
            newList.add(olObj);
        }
        return newList;
    }

    /**
     * Return the labor summary statistics by region and action type.
     * @param rdi the Result Data Info object. Pass the action type as a query
     *            parameter.
     * @return List of DataObjects
     * @throws DataAccessException on any failure.
     */
    @Override
    public List<DataObject> listLaborByRegionAndFilterType(ResultDataInfo rdi)
        throws DataAccessException {

        // This method fetches two result sets and builds the union of the sets.
        Object[] args = rdi.getQueryArgs();
        // Pass the parameter to the named query
        OperatorLaborFilterType filterType = (OperatorLaborFilterType) args[0];
        RegionType regionType = (RegionType) args[1];
        Date timeWindow = (Date) args[2];

        // get the list of "Empty statistics"
        List<Object[]> emptyList = this.getOperatorLaborByRegionDAO()
            .listEmptyLaborSummaryByRegion(filterType, regionType, timeWindow);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(
            emptyList.size());
        newList.addAll(buildLaborSummaryList(emptyList, filterType));
        // get the list of "real statistics"
        List<Object[]> statisticsList = this.getOperatorLaborByRegionDAO()
            .listLaborByRegionAndFilterType(filterType, timeWindow);
        newList.addAll(buildLaborSummaryList(statisticsList, filterType));
        List<Object[]> nullRegionList = this.getOperatorLaborByRegionDAO()
            .listLaborByFilterTypeAndNullRegion(filterType, timeWindow);
        newList.addAll(buildLaborSummaryList(nullRegionList, filterType));

        // return the list.
        return newList;
    }

    /**
     * Build an array list of data object (OperatorLaborByRegion objects) from
     * the given list of objects.
     * @param data - the list to use.
     * @param filterType - the filter type
     * 
     * @return - list
     * @throws DataAccessException - on db exception
     */
    private ArrayList<DataObject> buildLaborSummaryList(List<Object[]> data,
                                                        OperatorLaborFilterType filterType)
        throws DataAccessException {

        ArrayList<DataObject> returnList = new ArrayList<DataObject>(
            data.size());

        for (Object[] objArray : data) {
            OperatorLaborByRegion olObj = new OperatorLaborByRegion();

            if (objArray[POS_ZERO] instanceof Region) {
                olObj.setRegion((Region) objArray[POS_ZERO]);
                olObj.setId(olObj.getRegion().getId());
                SiteContext siteContext = SiteContextHolder.getSiteContext();
                Site theSite = siteContext.getSite(olObj.getRegion());
                olObj.setSite(theSite);
            } else {
                olObj.setRegion(null);
                olObj.setSite(null);
            }
            olObj.setTotalQuantity((Integer) objArray[POS_ONE]);
            olObj.setTotalTime((Long) objArray[POS_TWO]);
            if (olObj.getTotalTime() > 0) {
                olObj.setActualRate(new Double(olObj.getTotalQuantity()
                    / (olObj.getTotalTime() / MILLISEC_CONVERSION_FACTOR)));
            } else {
                olObj.setActualRate(new Double(0.0));
            }
            // if region is null then the record we are attempting to build is a
            // a put away, cycle counting or replenishment summary where an
            // operator signed on to multiple or all regions. We need to take
            // the average goal rate for ALL regions to compute the labor
            // statistics
            // TODO Again more refactoring
            if (olObj.getRegion() == null) {
                if (filterType == OperatorLaborFilterType.Putaway) {
                    olObj.setId(new Long(MULTIPLE_REGION_NUMBER));
                    PutawayRegion pr = new PutawayRegion();
                    pr.setName(multipleRegionName);
                    pr.setNumber(MULTIPLE_REGION_NUMBER);
                    olObj.setGoalRate(this.getPutawayRegionDAO().avgGoalRate()
                        .intValue());
                    pr.setGoalRate(olObj.getGoalRate());
                    olObj.setRegion(pr);
                }
                if (filterType == OperatorLaborFilterType.Replenishment) {
                    olObj.setId(new Long(MULTIPLE_REGION_NUMBER));
                    ReplenishmentRegion rr = new ReplenishmentRegion();
                    rr.setName(multipleRegionName);
                    rr.setNumber(MULTIPLE_REGION_NUMBER);
                    olObj.setGoalRate(this.getReplenishmentRegionDAO()
                        .avgGoalRate().intValue());
                    rr.setGoalRate(olObj.getGoalRate());
                    olObj.setRegion(rr);
                }
                if (filterType == OperatorLaborFilterType.CycleCounting) {
                    olObj.setId(new Long(MULTIPLE_REGION_NUMBER));
                    CycleCountingRegion ccr = new CycleCountingRegion();
                    ccr.setName(multipleRegionName);
                    ccr.setNumber(MULTIPLE_REGION_NUMBER);
                    olObj.setGoalRate(this.getCycleCountingRegionDAO()
                        .avgGoalRate().intValue());
                    ccr.setGoalRate(olObj.getGoalRate());
                    olObj.setRegion(ccr);
                }
            } else {
                olObj.setGoalRate(olObj.getRegion().getGoalRate());
            }
            if (olObj.getGoalRate() > 0) {
                olObj.setPercentOfGoal(new Double(olObj.getActualRate()
                    / olObj.getGoalRate() * ONE_HUNDRED));
            } else {
                olObj.setPercentOfGoal(new Double(0.0));
            }
            olObj.setNumberOfOperators((Long) objArray[POS_THREE]);
            returnList.add(olObj);
        }
        return returnList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        Object[] args = rdi.getQueryArgs();
        Date timeWindow = (Date) args[0];
        return this.getPrimaryDAO().listOperatorLabor(new QueryDecorator(rdi),
            timeWindow);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#openLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType,
     *      com.vocollect.voicelink.core.model.OperatorLaborRecordType)
     */
    @Override
    public void openLaborRecord(Date time,
                                Operator operator,
                                OperatorLaborActionType actionType)
        throws DataAccessException, BusinessRuleException {

        if (actionType == OperatorLaborActionType.Break) {
            throw new IllegalArgumentException();
        }
        if (actionType == OperatorLaborActionType.SignOn) {
            this.createSignOnSignOffRecord(time, operator,
                OperatorLaborActionType.SignOn);
        } else if (actionType == OperatorLaborActionType.SignOff) {
            this.createSignOnSignOffRecord(time, operator,
                OperatorLaborActionType.SignOff);
        } else if (actionType == OperatorLaborActionType.Selection) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.Selection,
                OperatorLaborFilterType.Selection);
        } else if (actionType == OperatorLaborActionType.LineLoading) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.LineLoading,
                OperatorLaborFilterType.LineLoading);
        } else if (actionType == OperatorLaborActionType.PutAway) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.PutAway,
                OperatorLaborFilterType.Putaway);
        } else if (actionType == OperatorLaborActionType.Replenishment) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.Replenishment,
                OperatorLaborFilterType.Replenishment);
        } else if (actionType == OperatorLaborActionType.PutToStore) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.PutToStore,
                OperatorLaborFilterType.PutToStore);
        } else if (actionType == OperatorLaborActionType.CycleCounting) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.CycleCounting,
                OperatorLaborFilterType.CycleCounting);
        } else if (actionType == OperatorLaborActionType.Loading) {
            this.createFunctionRecord(time, operator,
                OperatorLaborActionType.Loading,
                OperatorLaborFilterType.Loading);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#closeLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public OperatorLabor closeLaborRecord(Date endTime, Operator operator)
        throws DataAccessException, BusinessRuleException {

        return this.closeCurrentOpenRecord(endTime, operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#openLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType,
     *      com.vocollect.voicelink.core.model.OperatorLaborRecordType,
     *      com.vocollect.voicelink.core.model.BreakType)
     */
    @Override
    public void openBreakLaborRecord(Date time,
                                     Operator operator,
                                     BreakType breakType,
                                     OperatorLabor previousLabor)
        throws DataAccessException, BusinessRuleException {

        // throw exception if breaktype does not exist??
        OperatorBreakLabor bl = new OperatorBreakLabor();
        this.setBaseClassMembers(bl, time, operator,
            OperatorLaborActionType.Break, OperatorLaborFilterType.Other);
        bl.setBreakType(breakType);
        bl.setPreviousOperatorLabor(previousLabor);
        this.save(bl);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#closeBreakLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public OperatorBreakLabor closeBreakLaborRecord(Date time, Operator operator)
        throws DataAccessException, BusinessRuleException {

        // return the break record that was just closed.
        return ((OperatorBreakLabor) closeCurrentOpenRecord(time, operator));
    }

    /**
     * create a new signon/signoff record for this operator.
     * @param operator - operator object
     * @param time - record start time
     * @param actionType - type of record being created.
     * 
     * @throws DataAccessException - if failure.
     * @throws BusinessRuleException - if failure.
     */
    protected void createSignOnSignOffRecord(Date time,
                                             Operator operator,
                                             OperatorLaborActionType actionType)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor lr = new OperatorLabor();
        this.setBaseClassMembers(lr, time, operator, actionType,
            OperatorLaborFilterType.Other);
        this.save(lr);
    }

    /**
     * create a new Function record for this operator.
     * 
     * @param time - start time of break.
     * @param operator - operator that is taking the break.
     * @param actionType - type of labor record.
     * @param filterType - used by the filtering feature to filter records.
     * 
     * @throws DataAccessException - if failure.
     * @throws BusinessRuleException - if failure.
     * 
     */
    protected void createFunctionRecord(Date time,
                                        Operator operator,
                                        OperatorLaborActionType actionType,
                                        OperatorLaborFilterType filterType)
        throws DataAccessException, BusinessRuleException {

        OperatorFunctionLaborRoot fl = new OperatorFunctionLabor();
        this.setBaseClassMembers(fl, time, operator, actionType, filterType);

        // if the operator is only signed on to a single region then we can
        // track
        // the operators labor by region for putaway.
        if (((actionType == OperatorLaborActionType.PutAway)
            || (actionType == OperatorLaborActionType.Replenishment) || (actionType == OperatorLaborActionType.LineLoading))) {
            if (operator.getRegions().size() == 1) {
                fl.setRegion(operator.getRegions().iterator().next());
            } else {
                fl.setRegion(null);
            }
        } else {
            fl.setRegion(operator.getCurrentRegion());
        }
        fl.setCount(new Integer(0));
        fl.setActualRate(new Double(0.0));
        fl.setPercentOfGoal(new Double(0.0));
        this.save(fl);
    }

    /**
     * Close the currently opened labor record for this operator.
     * 
     * @param operator - operator object.
     * @param endTime - the time to use to set the end time for the labor
     *            record.
     * 
     * @return OperatorLabor - the object that was just closed.
     * 
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if save fails.
     * 
     */
    public OperatorLabor closeCurrentOpenRecord(Date endTime, Operator operator)
        throws DataAccessException, BusinessRuleException {

        // Find the currently opened labor record and close it.
        // There should only be one open record but there can be none.
        OperatorLabor ol = this.findOpenRecordByOperatorId(operator.getId());

        if (ol != null) {
            ol.setEndTime(endTime);
            ol.setDuration(endTime.getTime() - ol.getStartTime().getTime());
        }
        return ol;
    }

    /**
     * set the base class memeber for the OperatorLabor record.
     * 
     * @param laborRecord - operator object.
     * @param time - record start time.
     * @param operator - operator object.
     * @param actionType - type of action.
     * @param filterType - used to filter records
     */
    protected void setBaseClassMembers(OperatorLabor laborRecord,
                                       Date time,
                                       Operator operator,
                                       OperatorLaborActionType actionType,
                                       OperatorLaborFilterType filterType) {

        laborRecord.setOperator(operator);
        laborRecord.setActionType(actionType);
        laborRecord.setStartTime(time);
        laborRecord.setEndTime(null);
        laborRecord.setDuration(new Long(0));
        laborRecord.setFilterType(filterType);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.util.Date)
     */
    @Override
    public List<OperatorLabor> listOlderThan(QueryDecorator decorator, Date date)
        throws DataAccessException {
        return getPrimaryDAO().listOlderThan(decorator, date);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.util.Date)
     */
    @Override
    public int executePurge(QueryDecorator decorator, Date olderThan) {
        int returnRecords = 0;
        List<OperatorLabor> operatorLabor = null;

        // Get List of Operator Labor records to purge
        if (log.isDebugEnabled()) {
            log.debug("### Finding Operator Labor records to Purge :::");
        }
        try {
            operatorLabor = listOlderThan(decorator, olderThan);
            returnRecords = operatorLabor.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting transactional "
                + "Operator Labor records from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Operator Labor records for purge :::");
        }
        for (OperatorLabor ol : operatorLabor) {
            try {
                delete(ol);
            } catch (Throwable t) {
                log.warn(
                    "!!! Error purging transactional Operator Labor "
                        + ol.getId() + " from database: " + t, t);
            }
        }

        getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listBreaksLongerThan(java.util.Date,
     *      java.util.Date, java.lang.Long, java.lang.Long, java.lang.Long)
     */
    @Override
    public List<Object[]> listBreaksLongerThan(Date startTime,
                                               Date endTime,
                                               Long durationInMilliSeconds,
                                               Set<String> operatorId,
                                               String breakId,
                                               String operator)
        throws DataAccessException {
        return getPrimaryDAO().listBreaksLongerThan(startTime, endTime,
            durationInMilliSeconds, operatorId, breakId, operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listPerformanceCounts(java.util.Date,
     *      java.util.Date,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType)
     */
    @Override
    public List<Object[]> listPerformanceCounts(Date startTime,
                                                Date endTime,
                                                OperatorLaborActionType actionType,
                                                Long region) {
        return getPrimaryDAO().listPerformanceCounts(startTime, endTime,
            actionType, region);
    }

    /**
     * 
     * @param operatorId - the selected operator
     * @param startDate - the start of the date range
     * @param endDate - the end of the date range
     * @return - a list of objects
     * @throws DataAccessException - on db failure
     */
    @Override
    public List<Object[]> listLaborSummaryReportRecords(Long operatorId,
                                                        Date startDate,
                                                        Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(operatorId,
            startDate, endDate);
    }

    /**
     * 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @return - list of operators who have labor records with the given action
     *         type and date range.
     * @throws DataAccessException - on db failure.
     */
    @Override
    public List<Operator> listAllOperatorsActionBetweenDates(Date startDate,
                                                             Date endDate,
                                                             OperatorLaborActionType actionType)
        throws DataAccessException {
        return this.getPrimaryDAO().listAllOperatorsActionBetweenDates(
            startDate, endDate, actionType);
    }

    /**
     * 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param actionType - the requested actionType
     * @param regionNumber - ther requested region
     * @return - list of operators who have labor records with the given action
     *         type, region and date range.
     * @throws DataAccessException - on db failure.
     */
    @Override
    public List<Operator> listAllOperatorsActionRegionBetweenDates(Date startDate,
                                                                   Date endDate,
                                                                   OperatorLaborActionType actionType,
                                                                   Integer regionNumber)
        throws DataAccessException {

        return this.getPrimaryDAO().listAllOperatorsActionRegionBetweenDates(
            startDate, endDate, actionType, regionNumber);
    }

    /**
     * 
     * @param operatorId - the selected operator
     * @param startDate - the start of the date range
     * @param endDate - the end of the date range
     * @return - a list of objects
     * @throws DataAccessException - on db failure
     */
    @Override
    public List<Object[]> listLaborDetailReportRecords(Long operatorId,
                                                       Date startDate,
                                                       Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listLaborDetailReportRecords(operatorId,
            startDate, endDate);
    }

    /**
     * Class used by reports.
     */
    public class ActionTypesForReport {

        private String actionTypeForReportName;

        private String actionTypeForReportValue;

        /**
         * Constructor.
         * @param name - the name property of this action type
         * @param value - value property for this action type
         */
        ActionTypesForReport(String name, String value) {
            this.actionTypeForReportName = name;
            this.actionTypeForReportValue = value;
        }

        /**
         * @return - the action type value
         */
        public String getValue() {
            return this.actionTypeForReportValue;
        }

        /**
         * @return - the action type name
         */
        public String getName() {
            return this.actionTypeForReportName;
        }

    }

    /**
     * Method used to return a list of action types used in labor summary
     * reports.
     * 
     * @return - a list of action types used by reports
     */
    @Override
    public List<ActionTypesForReport> getActionTypesForReport() {

        List<ActionTypesForReport> actionTypeForReportsList = new ArrayList<ActionTypesForReport>();
        // create list of action types that are currently supported by the labor
        // summary reports.
        actionTypeForReportsList
            .add(new ActionTypesForReport(
                ResourceUtil
                    .getLocalizedKeyValue("selection.report.laborsummary.selectiononly"),
                "SELECTION_ONLY"));
        actionTypeForReportsList.add(new ActionTypesForReport(ResourceUtil
            .getLocalizedKeyValue("selection.report.laborsummary.replenonly"),
            "REPLENISHMENT_ONLY"));
        actionTypeForReportsList
            .add(new ActionTypesForReport(
                ResourceUtil
                    .getLocalizedKeyValue("selection.report.laborsummary.selectionincreplen"),
                "SELECTION_REPLENISHMENT"));
        actionTypeForReportsList
            .add(new ActionTypesForReport(
                ResourceUtil
                    .getLocalizedKeyValue("selection.report.laborsummary.replenincselection"),
                "REPLENISHMENT_SELECTION"));
        actionTypeForReportsList
            .add(new ActionTypesForReport(
                ResourceUtil
                    .getLocalizedKeyValue("selection.report.laborsummary.cyclecountingonly"),
                "CYCLECOUNTING_ONLY"));
        actionTypeForReportsList.add(new ActionTypesForReport(ResourceUtil
            .getLocalizedKeyValue("selection.report.laborsummary.loadingonly"),
            "LOADING_ONLY"));

        return actionTypeForReportsList;
    }

    /**
     * Class used by reports.
     */
    public class ShiftHoursForReport {

        private String name;

        private Integer value;

        /**
         * Constructor.
         * @param name - the name property of this action type
         * @param value - value property for this action type
         */
        ShiftHoursForReport(String name, Integer value) {
            this.name = name;
            this.value = value;
        }

        /**
         * @return - the action type value
         */
        public Integer getValue() {
            return this.value;
        }

        /**
         * @return - the action type name
         */
        public String getName() {
            return this.name;
        }

    }

    /**
     * Method used to return a list of action types used in labor summary
     * reports.
     * 
     * @return - a list of start hours used by reports
     */
    @Override
    public List<ShiftHoursForReport> getShiftStartHoursForReport() {

        List<ShiftHoursForReport> shiftHoursForReportList = new ArrayList<ShiftHoursForReport>();
        String shiftHourString;
        // create list of action types that are currently supported by the labor
        // summary reports.
        for (Integer i = 0; i < HOURS_IN_DAY; i++) {
            if (i < HOUR_TEN) {
                shiftHourString = "0" + i.toString() + ":00";
            } else {
                shiftHourString = i.toString() + ":00";
            }
            shiftHoursForReportList.add(new ShiftHoursForReport(
                shiftHourString, i));
        }
        return shiftHoursForReportList;
    }

    /**
     * Method used to return a list of shift hours used in labor reports.
     * 
     * @return - a list of shift hours used by reports
     */
    @Override
    public List<ShiftHoursForReport> getShiftLengthHoursForReport() {

        List<ShiftHoursForReport> shiftHoursForReportList = new ArrayList<ShiftHoursForReport>();
        String shiftHourString;
        // create list of action types that are currently supported by the labor
        // summary reports.
        for (Integer i = 1; i <= HOURS_IN_DAY; i++) {
            if (i < HOUR_TEN) {
                shiftHourString = " " + i.toString();
            } else {
                shiftHourString = i.toString();
            }
            shiftHoursForReportList.add(new ShiftHoursForReport(
                shiftHourString, i));
        }
        return shiftHoursForReportList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listLaborRecordsForPtsOperatorLaborReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.lang.String, java.lang.String, java.util.Set, java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<PtsOperatorLaborReport> listLaborRecordsForPtsOperatorLaborReport(QueryDecorator queryDecorator,
                                                                                  String regionNumber,
                                                                                  Set<String> operatorIdentifiers,
                                                                                  Date startDate,
                                                                                  Date endDate)
        throws DataAccessException {
        // TODO I think this is excess and could be cleanly pushed to the HQL
        // query.
        WhereClause whereClause = new WhereClause();

        // TODO this is a less than ideal way to do this
        // the real way to do this is to modify WhereClause
        // to check if a string is null or empty
        if (regionNumber != null && regionNumber.isEmpty()) {
            regionNumber = null;
        }
        whereClause.add(regionNumber, "r.number");

        whereClause.add(operatorIdentifiers, "o.common.operatorIdentifier");
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }

        return getPrimaryDAO().listLaborRecordsForPtsOperatorLaborReport(
            queryDecorator, startDate, endDate);

    }

    /**
     * @param operatorId - operator id
     * @param regionType - type of regions (selection, putaway, etc)
     * @return average goal rate for the region type.
     * @throws DataAccessException - any database exception
     */
    @Override
    public Double avgGoalRateForTypeAndOperator(Long operatorId,
                                                OperatorLaborActionType regionType)
        throws DataAccessException {
        List<Integer> listGoalRates = this.getPrimaryDAO()
            .listGoalRateForTypeAndOperator(operatorId, regionType);
        Integer sumGoalRate = 0;

        for (Integer goalRate : listGoalRates) {
            sumGoalRate += goalRate;
        }

        return sumGoalRate / (listGoalRates.size() * 1.0);
    }

    // Methods in Data Aggregators

    // Used for operator required calculations and goal rate calculations

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listLaborRecordInTimeWindow(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<OperatorLabor> listLaborRecordInTimeWindow(Date startTime,
                                                           Date endTime)
        throws DataAccessException {
        return getPrimaryDAO().listLaborRecordInTimeWindow(startTime, endTime);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listOperatorActualRateByRegion()
     */
    @Override
    public List<Map<String, Object>> listOperatorActualRateByRegion() {
        return this.getPrimaryDAO().listOperatorActualRateByRegion();
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listAllClosedBreakRecords(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<OperatorLabor> listAllClosedBreakRecords(Date timeWindowStartTime,
                                                         Date timeWindowEndTime)
        throws DataAccessException {
        return this.getPrimaryDAO().listAllClosedBreakRecords(
            timeWindowStartTime, timeWindowEndTime);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#listAllOpenRecords()
     */
    @Override
    public List<OperatorLabor> listAllOpenRecords() throws DataAccessException {
        return this.getPrimaryDAO().listAllOpenRecords();
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.OperatorLaborManagerRoot#getFirstSignOffTimeAfterBreak(java.lang.Long,
     *      java.util.Date)
     */
    @Override
    public Date getFirstSignOffTimeAfterBreak(Long operatorId, Date breakEndTime)
        throws DataAccessException {
        return this.getPrimaryDAO().minFirstSignOffTimeAfterBreak(operatorId,
            breakEndTime);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 