/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Implementation of Assignment Report.
 *
 * @author kudupi
 */
public abstract class AssignmentReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    // Managers for queries

    private ArchiveAssignmentManager archiveManager;

    private AssignmentManager assignmentManager;


    /**
     * Overriding the getDataSource method of ReportDataSourceManager
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values)
        throws Exception {

        //Capturing parameter values entered by the user.
        String workID = checkNull((String) values.get("WORK_ID"));
        String customerNumber = checkNull((String) values.get("CUSTOMER_NUMBER"));
        String route = checkNull((String) values.get("ROUTE"));

        Long assignmentNumber = null;
        if ((String) values.get("ASSIGNMENT_NUMBER") != null) {
            try {
                assignmentNumber = new Long((String) values.get("ASSIGNMENT_NUMBER"));
            } catch (NumberFormatException x) {
            }
        }
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        String siteId = (String) values.get("SITE");

        Set<String> allOperatorId = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            allOperatorId = null;
        }

        //Get a list of report objects from the archive db
        List<AssignmentReport> archiveList = getArchiveManager()
        .listArchiveAssignmentsForAssignmentReport(new QueryDecorator(),
            workID, allOperatorId, assignmentNumber, customerNumber, route,
            startTime, endTime, Long.parseLong(siteId));

        //Get a list of report objects from the transactional db
        List<AssignmentReport> assignmentList = getAssignmentManager()
        .listAssignmentsForAssignmentReport(new QueryDecorator(), workID, allOperatorId,
            assignmentNumber, customerNumber, route, startTime, endTime);

        //Add the two lists together
        assignmentList.addAll(archiveList);
        return new JRBeanCollectionDataSource(assignmentList);
    }

    /**
     * Convenience method for checking input parameters
     * @param str the string to check
     * @return the return string
     */
    private String checkNull(String str) {
        if (str != null) {
            return (str.trim().equalsIgnoreCase("") ? null : str);
        } else {
            return null;
        }
    }


    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<AssignmentReport> reportList = new ArrayList<AssignmentReport>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }

    /**
     * Constructor.
     * @param beanClass - class report is based on.
     */
    public AssignmentReportImplRoot(Class<AssignmentReport> beanClass) {
        super(AssignmentReport.class);
    }

    /**
     * Constructor.
     */
    public AssignmentReportImplRoot() {
        super(AssignmentReport.class);
    }

    /**
     * Getter for the archiveManager property.
     * @return the value of the property
     */
    public ArchiveAssignmentManager getArchiveManager() {
        return this.archiveManager;
    }


    /**
     * Setter for the archiveManager property.
     * @param archiveManager the new archiveManager value
     */
    public void setArchiveManager(ArchiveAssignmentManager archiveManager) {
        this.archiveManager = archiveManager;
    }


    /**
     * Getter for the assignmentManager property.
     * @return the value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return this.assignmentManager;
    }


    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 