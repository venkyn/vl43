/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * @author mraj
 *
 */
public class VehicleSafetyCheckReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;
    /**
     * @param beanClass Report Class that the report is based on
     */
    public VehicleSafetyCheckReportImplRoot(Class<VehicleSafetyCheckResponse> beanClass) {
        super(VehicleSafetyCheckResponse.class);
    }

    
    
    /**
     * Default Constructor.
     */
    public VehicleSafetyCheckReportImplRoot() {
        super(VehicleSafetyCheckResponse.class);
    }
    
    /**
     * @return the vehicleSafetyCheckResponseManager
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }


    
    /**
     * @param vehicleSafetyCheckResponseManager the vehicleSafetyCheckResponseManager to set
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }


    /* (non-Javadoc)
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    @Override
    public JRDataSource getDataSource(Map<String, Object> values)
        throws Exception {
        Date startDate = (Date) values.get("START_DATE");
        Date endDate = (Date) values.get("END_DATE");
        Integer vehicleType = (Integer) values.get("VEHICLE_TYPE");
        String vehicleNumber = (String) values.get("VEHICLE");
        @SuppressWarnings("unchecked")
        Set<String> operatorIdentifiers = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            operatorIdentifiers = null;
        }
        
        if (StringUtil.isNullOrEmpty(vehicleNumber)) {
            vehicleNumber = null;
        }
        
        List<VehicleSafetyCheckResponse> responses = getVehicleSafetyCheckResponseManager()
            .listVehicleSafetyCheckResponseForReport(new QueryDecorator(),
                vehicleType, vehicleNumber, operatorIdentifiers, startDate, endDate);
        return new JRBeanCollectionDataSource(responses);
    }

    /* (non-Javadoc)
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    @Override
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<VehicleSafetyCheckResponse> reportList = new ArrayList<VehicleSafetyCheckResponse>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /* (non-Javadoc)
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    @Override
    public void dispose(JRDataSource arg0) throws JRException {

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 