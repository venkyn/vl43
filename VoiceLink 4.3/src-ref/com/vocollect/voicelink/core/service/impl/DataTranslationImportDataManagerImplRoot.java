/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.DataTranslationImportDataDAO;
import com.vocollect.voicelink.core.model.DataTranslationImportData;
import com.vocollect.voicelink.core.service.DataTranslationImportDataManager;

import java.util.List;

/**
 * Service layer interface to the Data Translation  import.
 * @author dgold
 *
 */
public abstract class DataTranslationImportDataManagerImplRoot
    extends GenericManagerImpl<DataTranslationImportData, DataTranslationImportDataDAO>
    implements DataTranslationImportDataManager {

    /**
     * Default c'tor.
     * @param primaryDAO the DataTranslation dao
     */
    public DataTranslationImportDataManagerImplRoot(DataTranslationImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @param siteName name of the site
     * @return list of 0 or more DataTranslation items
     * @throws DataAccessException on database problems
     */
    public List<DataTranslationImportData> listDataTranslationBySiteName(String siteName)
            throws DataAccessException {
        return getPrimaryDAO().listDataTranslationBySiteName(siteName);

    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException for database problems.
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 