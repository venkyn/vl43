/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Implementation class for Put To Store License Report.
 *
 * @author kudupi
 */
public abstract class PTSLicenseReportImplRoot extends
        JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    // Manager for extracting transactional data for PTS License Report.
    private PtsLicenseManager ptsLicenseManager;

    // Manager for extracting archive data for PTS License Report.
    private ArchivePtsLicenseManager archivePtsLicenseManager;   
    
    /**
     * Parameterized Constructor.
     * @param beanClass - Bean class on which Report is based.
     */
    public PTSLicenseReportImplRoot(Class<PtsLicenseReport> beanClass) {
        super(PtsLicenseReport.class);
    }

    /**
     * Default Constructor.
     */
    public PTSLicenseReportImplRoot() {
        super(PtsLicenseReport.class);
    }    

    /**
     * Overriding the getDataSource method of ReportDataSourceManager
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values)
            throws Exception {

        // Capturing parameter values entered by the user.
        String licenseNumber = checkNull((String) values.get("LICENSE_NUMBER"));
        Date startTime = (Date) values.get("START_DATE");
        Date endTime = (Date) values.get("END_DATE");
        String siteId = checkNull((String) values.get("SITE"));
 
        Set<String> allOperatorIds = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            allOperatorIds = null;
        }        
        
        //Get a list of report objects from the archive db
        List<PtsLicenseReport> archiveList = getArchivePtsLicenseManager()
        .listArchiveLicensesForPtsLicenseReport(new QueryDecorator(), licenseNumber, 
            allOperatorIds, startTime, endTime, Long.parseLong(siteId));
        
        //Get a list of report objects from the transactional db
        List<PtsLicenseReport> licenseList = this.getPtsLicenseManager()
        .listLicensesForPtsLicenseReport(new QueryDecorator(), licenseNumber, allOperatorIds, startTime, endTime);

        //Add the two lists together        
        licenseList.addAll(archiveList);

        return new JRBeanCollectionDataSource(licenseList);
    }

    /**
     * {@inheritDoc}
     *
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {
        List<PtsLicenseReport> reportList = new ArrayList<PtsLicenseReport>();
        return new JRBeanCollectionDataSource(reportList);
    }

    /**
     * {@inheritDoc}
     *
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing
    }
    
    /**
     * Convenience method for checking input parameters
     * @param str the string to check
     * @return the return string
     */
    private String checkNull(String str) {
        if (str != null) {
            return (str.trim().equalsIgnoreCase("") ? null : str);
        } else {
            return null;
        }
    }
    
    /**
     * Getter for putsLicenseManager.
     * @return ptsLicenseManager of type PtsLicenseManager.
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return ptsLicenseManager;
    }

    /**
     * Setter for ptsLicenseManager.
     * @param ptsLicenseManager of type PtsLicenseManager.
     */
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }

    /**
     * Getter for archivePtsLicenseManager.
     * @return archivePtsLicenseManager of type ArchivePtsLicenseManager.
     */
    public ArchivePtsLicenseManager getArchivePtsLicenseManager() {
        return archivePtsLicenseManager;
    }

    /**
     * Setter for archivePtsLicenseManager.
     * @param archivePtsLicenseManager of type ArchivePtsLicenseManager.
     */
    public void setArchivePtsLicenseManager(
            ArchivePtsLicenseManager archivePtsLicenseManager) {
        this.archivePtsLicenseManager = archivePtsLicenseManager;
    }    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 