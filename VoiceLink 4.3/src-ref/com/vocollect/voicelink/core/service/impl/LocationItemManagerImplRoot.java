/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.BatchDataAccessException;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.LocationItemDAO;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationItem;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.LocationItemManager;
import com.vocollect.voicelink.core.service.LocationManager;

import java.util.List;


/**
 * Additional service methods for the <code>LocationItem</code> model object.
 *
 */
public abstract class LocationItemManagerImplRoot 
    extends GenericManagerImpl<LocationItem, LocationItemDAO> 
    implements LocationItemManager {

    private LocationManager locationManager = null;
    
    /**
     * {@inheritDoc}
     * @param instances
     * @return
     * @throws BusinessRuleException
     * @throws DataAccessException
     */
    @Override
    public Object save(List<LocationItem> instances) throws BusinessRuleException, DataAccessException {
        int i = 0;
        try {
            for (LocationItem li : instances) {
                save(li);
                i++;
            }
        } catch (Exception e) {
            throw new BatchDataAccessException(e, i);
        }

        return instances;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LocationItemManagerImplRoot(LocationItemDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Method to ensure location is updated properly when a new Item 
     * is add to a location, also to handle import logic based on action.
     * 
     * @param locationItem - locationItem that was edited/created
     * @return the saved locationItem
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    public Object save(LocationItem locationItem) 
    throws DataAccessException, BusinessRuleException {

        //Normal Save from UI creation
        if (locationItem.getImportAction() == null) {
            return executeCreateMapping(locationItem);
            
        //Save from import
        } else if (locationItem.getImportAction().equals("A")) {
            return importAdd(locationItem);
            
        //Delete from import
        } else if (locationItem.getImportAction().equals("D")) {
            return importDelete(locationItem);
            
        //Mark as replenished from import
        } else if (locationItem.getImportAction().equals("R")) {
            return importMarkReplenished(locationItem);
        }
        
        return locationItem;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(LocationItem instance) throws BusinessRuleException, DataAccessException {
        instance.getLocation().getItems().remove(instance);
        //If no items defined then we must be deleting the last item therefor
        if (instance.getLocation().getItems().size() == 0 
            && instance.getLocation().getStatus().equals(LocationStatus.Multiple)) {
            instance.getLocation().setStatus(LocationStatus.Replenished);
        }
        return super.delete(instance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id) throws BusinessRuleException, DataAccessException {
        return delete(get(id));
    }
    
    
    /**
     * add mapping from import.
     * 
     * @param locationItem - locationItem mapping to add
     * @return - locationItem object
     * @throws BusinessRuleException - Business rules
     * @throws DataAccessException - Database exceptions
     */
    protected Object importAdd(LocationItem locationItem) 
    throws BusinessRuleException, DataAccessException {

        //Make sure item is new
        if (!locationItem.isNew()) {
            throw new BusinessRuleException(
                CoreErrorCode.ITEM_LOCATION_ALREADY_EXISTS,
                new UserMessage("locationItem.import.error.mappingAlreadyExists"));
        }
        
        //Make sure item defined
        if (locationItem.getItem() == null) {
            throw new BusinessRuleException(
                CoreErrorCode.ITEM_LOCATION_ITEM_NOT_DEFINED,
                new UserMessage("locationItem.import.error.itemNotDefinedForAdd"));
        }
        
        //Set status to multiple
        locationItem.getLocation().setStatus(LocationStatus.Multiple);
        return super.save(locationItem);
    }
    
    /**
     * Delete from import.
     * 
     * @param locationItem - locationItem Mapping
     * @return - locationItem object
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Database exceptions
     */
    protected Object importDelete(LocationItem locationItem) 
    throws BusinessRuleException, DataAccessException {
        if (locationItem.isNew()) {
            throw new BusinessRuleException(
                CoreErrorCode.DELETING_ITEM_LOCATION_DOES_NOT_EXIST,
                new UserMessage("locationItem.import.error.doesNotExistForDelete"));
            
        }

        //Make sure item defined
        if (locationItem.getItem() == null) {
            throw new BusinessRuleException(
                CoreErrorCode.DELETING_ITEM_LOCATION_ITEM_NOT_DEFINED,
                new UserMessage("locationItem.import.error.itemNotDefinedForDelete"));
        }

        return delete(locationItem);
    }
    
    /**
     * Mark as replenished from import.
     *    If Item is defined and mapping does not exist in the system already then record is failed
     *    If item is defined and mapping does exist then mapping is marked as replenished
     *    If item is not defined and there are existing mappings for location then record is failed
     *    If item is not defined and there are no existing mappings then the location is marked as replenished
     * @param locationItem - locationItem Mapping
     * @return - locationItem object
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Database exceptions
     */
    protected Object importMarkReplenished(LocationItem locationItem) 
    throws BusinessRuleException, DataAccessException {


        //      Item was defined, must have a mapping
        if (!StringUtil.isNullOrEmpty(locationItem.getItemNumber())) {
            if (locationItem.isNew()) {     // New Mapping
                throw new BusinessRuleException(
                        CoreErrorCode.ITEM_LOCATION_REPLEN_NEW_MAPPING,
                        new UserMessage("locationItem.import.error.replenishNewLocationMapping"));
            } else if (!locationItem.getStatus().equals(LocationStatus.Replenished)) {
                locationItem.setStatus(LocationStatus.Replenished);
                return super.save(locationItem);
            }
        } else {  // Item not defined - find mapping by location
            Location location = locationItem.getLocation();
            if (location.getItems().size() <= 0) {  
                // New mapping  - set location to replenished
                if (!location.getStatus().equals(LocationStatus.Replenished)) {
                    location.setStatus(LocationStatus.Replenished, locationItem.getItem());
                    getLocationManager().save(location);
                    return locationItem;
                }
            } else { // existing mapping - fail
                throw new BusinessRuleException(
                        CoreErrorCode.ITEM_LOCATION_REPLEN_NO_MAPPING_NO_ITEM,
                        new UserMessage("locationItem.import.error.itemNotDefinedNoLocationMappingsExist"));
            }
        }

        return locationItem;
    }
    
    /**
     * Get the ItemLocation mapping by item number and location-scannedVerification.
     * @param itemNumber itemNumber
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public LocationItem findReplenishmentByItemAndLoc(String itemNumber, String scannedLocation)
        throws DataAccessException {
        return getPrimaryDAO().findReplenishmentByItemAndLoc(itemNumber, scannedLocation);
    }

    /**
     * Get the ItemLocation mapping by location-scannedVerification.
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public List<LocationItem> listReplenishmentByLocation(String scannedLocation)
        throws DataAccessException {
        return getPrimaryDAO().listReplenishmentByLocation(scannedLocation);
    }

     /**
     * Gets the value of locationManager.
     * @return the locationManager
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Sets the value of the locationManager.
     * @param locationManager the locationManager to set
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * add mapping from user interface.
     * 
     * @param locationItem - locationItem mapping to add
     * @return - locationItem object
     * @throws BusinessRuleException - Business rules
     * @throws DataAccessException - Database exceptions
     */
    public Object executeCreateMapping(LocationItem locationItem)
        throws BusinessRuleException, DataAccessException {

        // Make sure item and location exist
        if (locationItem.getItem() == null
            || locationItem.getLocation() == null) {
            throw new BusinessRuleException(
                CoreErrorCode.ITEM_LOCATION_ITEM_NOT_DEFINED, new UserMessage(
                    "locationItem.import.error.locitemNotFound"));
        }

        // does mapping already exist?
        LocationItem li = new LocationItem();
        li = findReplenishmentByItemAndLoc(
            locationItem.getItemNumber(), locationItem.getLocation()
                .getScannedVerification());

        // If the mapping exists then it can't be added again
        if (li != null) {
            // Make sure item is new
            if (!li.isNew()) {
                throw new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ALREADY_EXISTS,
                    new UserMessage(
                        "locationItem.import.error.mappingAlreadyExists"));
            }
            // Make sure item defined
            if (li.getItem() == null) {
                throw new BusinessRuleException(
                    CoreErrorCode.ITEM_LOCATION_ITEM_NOT_DEFINED,
                    new UserMessage(
                        "locationItem.import.error.itemNotDefinedForAdd"));
            }
        }

        // Set status to multiple
        locationItem.getLocation().setStatus(LocationStatus.Multiple);
        return super.save(locationItem);

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 