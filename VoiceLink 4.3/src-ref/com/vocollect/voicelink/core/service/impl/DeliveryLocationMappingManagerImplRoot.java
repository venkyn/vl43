/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.BaseAction;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.DeliveryLocationMappingDAO;
import com.vocollect.voicelink.core.dao.DeliveryLocationMappingSettingDAO;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingRoot;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingManager;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Specific manager for Delivery Location Mappings - essentially rules that can
 * define that for all assignments with a specific customer number or route,
 * send that assignment to a given delivery door.
 * 
 * @author Ben Northrop, Nayeem Sayed
 */
public class DeliveryLocationMappingManagerImplRoot extends
        GenericManagerImpl<DeliveryLocationMapping, DeliveryLocationMappingDAO>
        implements DeliveryLocationMappingManager {

    /**
     * Logging utility for debugging.
     */
    private static final Logger LOG = new Logger(BaseAction.class);

    /**
     * The delivery location mapping setting manager.
     */
    private DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager;

    /**
     * The DAO for the site specific mapping setting DAO.
     */
    private DeliveryLocationMappingSettingDAO deliveryLocationMappingSettingDAO;

    /**
     * The assignment manager to check reference of delivery location
     */
    private AssignmentManager assignmentManager;

    /**
     * @return the assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager
     *            the assignmentManager to set
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Constructor.
     * 
     * @param primaryDAO
     *            the DAO for this manager
     */
    public DeliveryLocationMappingManagerImplRoot(
            DeliveryLocationMappingDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Override the getAll() from GenericManager so to return the appropriate
     * type of DeliveryLocationMappings (e.g. customer number, route, etc.).
     * 
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingManagerRoot#getMappings()
     * @return List delivery location mappings.
     * @throws BusinessRuleException
     *             - if cannot get mapping type for site
     * @throws DataAccessException
     *             - any database exception.
     */
    public List<DeliveryLocationMapping> getMappings()
            throws DataAccessException, BusinessRuleException {

        DeliveryLocationMappingType mappingType = getDeliveryLocationMappingSettingManager()
                .getMappingType();

        List<DeliveryLocationMapping> dlm = new ArrayList<DeliveryLocationMapping>(
                getPrimaryDAO().listMappings(mappingType));

        return dlm;
    }

    /**
     * @return List of delivery location mappings
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public List<DeliveryLocationMapping> getAllMappings()
            throws DataAccessException, BusinessRuleException {

        List<DeliveryLocationMapping> dlm = new ArrayList<DeliveryLocationMapping>(
                getPrimaryDAO().listMappings(DeliveryLocationMappingType.Route));

        dlm.addAll(new ArrayList<DeliveryLocationMapping>(getPrimaryDAO()
                .listMappings(DeliveryLocationMappingType.CustomerNumber)));

        return dlm;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#getAll()
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
            throws DataAccessException {
        return this.getPrimaryDAO().listMappingsForFilter(
                new QueryDecorator(rdi));
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingManager#executeModifyDeliveryLocation(java.util.Collection,
     *      java.lang.String)
     */
    public void executeModifyDeliveryLocation(String oldMappingValue,
            String newDeliveryLocaiton, DeliveryLocationMappingType mappingType)
            throws DataAccessException {

        DeliveryLocationMapping mapping = getPrimaryDAO().findDeliveryLocation(
                oldMappingValue, mappingType);

        if (mapping == null) {
            throw new DataAccessException(
                    CoreErrorCode.DELIVERY_MAPPING_VALUES_NOT_FOUND);
        }

        mapping.setDeliveryLocation(newDeliveryLocaiton);

        getPrimaryDAO().save(mapping);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.DeliveryLocationMappingManagerRoot
     * #executeDeleteDeliveryLocation(java.lang.String,
     * com.vocollect.voicelink.core.model.DeliveryLocationMappingType)
     */
    @Override
    public void executeDeleteDeliveryLocation(String mappingValue,
            DeliveryLocationMappingType mappingType) throws DataAccessException {

        DeliveryLocationMapping mapping = getPrimaryDAO().findDeliveryLocation(
                mappingValue, mappingType);

        if (mapping == null) {
            throw new DataAccessException(
                    CoreErrorCode.DELIVERY_MAPPING_VALUES_NOT_FOUND);
        }

        getPrimaryDAO().delete(mapping.getId());
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingManager#executeModifyDeliveryLocation(java.util.Collection,
     *      java.lang.String)
     */
    public void executeModifyDeliveryLocation(
            Collection<Long> deliveryLocationMappingIds, String deliveryLocation)
            throws DataAccessException {
        for (Long id : deliveryLocationMappingIds) {
            DeliveryLocationMappingRoot deliveryLocationMapping = getPrimaryDAO()
                    .get(id);
            deliveryLocationMapping.setDeliveryLocation(deliveryLocation);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.lang.Object)
     */
    @Override
    public Object save(DeliveryLocationMapping mapping)
            throws BusinessRuleException, DataAccessException {
        if (mapping == null) {
            throw new InvalidParameterException("Mapping is null.");
        }
        validateMappingRuleDoesNotExist(mapping);
        return super.save(mapping);
    }

    /**
     * Assign new tag to pick.
     * 
     * @param mapping
     *            - mapping to assign tag to.
     */
    public void assignSiteTagsToMapping(DeliveryLocationMappingRoot mapping) {
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        siteContext.setSiteToCurrentSite(mapping);
    }

    /**
     * Validate that the rule does not exist.
     * 
     * @param mapping
     *            - mapping to be validated.
     * @throws BusinessRuleException
     *             - if mapping exists.
     * @throws DataAccessException
     *             - any database exception.
     */
    public void validateMappingRuleDoesNotExist(
            DeliveryLocationMappingRoot mapping) throws DataAccessException,
            BusinessRuleException {
        if (mapping == null) {
            throw new InvalidParameterException("mapping is null.");
        }

        int numMappings = getPrimaryDAO().countNumberOfMappings(
                mapping.getMappingValue(), mapping.getMappingType()).intValue();

        LOG.info("got number of mappings: " + numMappings);
        if (numMappings > 0) {
            throw new BusinessRuleException(
                    CoreErrorCode.DELIVERY_MAPPING_VALUES_ALREADY_EXIST);
        }
    }

    /**
     * Find the delivery location corresponding to the mapping value (e.g.
     * customer number, route) {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingManagerRoot#getDeliveryLocation(java.lang.String)
     */
    public String findDeliveryLocation(String mappingValue)
            throws DataAccessException {

        DeliveryLocationMappingType mappingType = getDeliveryLocationMappingSettingManager()
                .getMappingType();

        DeliveryLocationMapping mapping = getPrimaryDAO().findDeliveryLocation(
                mappingValue, mappingType);

        return mapping == null ? null : mapping.getDeliveryLocation();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.DeliveryLocationMappingManagerRoot
     * #isDeliveryLocationReferenced(java.lang.String)
     */
    @Override
    public boolean isDeliveryLocationReferenced(Collection<Long> deliveryLocationMappingIds)
            throws DataAccessException {

        for (Long id : deliveryLocationMappingIds) {
            DeliveryLocationMapping deliveryLocationMapping = getPrimaryDAO()
                    .get(id);
            List<Assignment> assignments = getAssignmentManager()
                    .listAssignmentsByDeliveryLocation(deliveryLocationMapping.getDeliveryLocation());

            for (Assignment assignment : assignments) {
                if (assignment.getStatus() == AssignmentStatus.Complete
                        || assignment.getStatus() == AssignmentStatus.InProgress
                        || assignment.getStatus() == AssignmentStatus.Short) {
                    return true;
                }
            }
        }
        

        return false;
    }

    /**
     * Getter for the deliveryLocationMappingSettingManager property.
     * 
     * @return DeliveryLocationMappingSettingManager value of the property
     */
    public DeliveryLocationMappingSettingManager getDeliveryLocationMappingSettingManager() {
        return this.deliveryLocationMappingSettingManager;
    }

    /**
     * Setter for the deliveryLocationMappingSettingManager property.
     * 
     * @param deliveryLocationMappingSettingManager
     *            the new deliveryLocationMappingSettingManager value
     */
    public void setDeliveryLocationMappingSettingManager(
            DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager) {
        this.deliveryLocationMappingSettingManager = deliveryLocationMappingSettingManager;
    }

    /**
     * Getter for the deliveryLocationMappingSettingDAO property.
     * 
     * @return DeliveryLocationMappingSettingDAO value of the property
     */
    public DeliveryLocationMappingSettingDAO getDeliveryLocationMappingSettingDAO() {
        return this.deliveryLocationMappingSettingDAO;
    }

    /**
     * Setter for the deliveryLocationMappingSettingDAO property.
     * 
     * @param deliveryLocationMappingSettingDAO
     *            the new deliveryLocationMappingSettingDAO value
     */
    public void setDeliveryLocationMappingSettingDAO(
            DeliveryLocationMappingSettingDAO deliveryLocationMappingSettingDAO) {
        this.deliveryLocationMappingSettingDAO = deliveryLocationMappingSettingDAO;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 