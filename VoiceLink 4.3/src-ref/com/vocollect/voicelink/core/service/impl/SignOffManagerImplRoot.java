/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>Operator</code> model object.
 *
 * @author ddoubleday
 */

public abstract class SignOffManagerImplRoot extends
    GenericManagerImpl<Operator, OperatorDAO> implements SignOffManager {

    private AssignmentManager    assignmentManager;

    private LaborManager laborManager;
    
    private VoicelinkNotificationUtil   voicelinkNotificationUtil;
    
    private ReplenishmentManager replenishmentManager;
    
    private PtsLicenseManager ptsLicenseManager;
    
    private CycleCountingAssignmentManager cycleCountingAssignmentManager;
    
    private LoadingRouteManager loadingRouteManager;
    
    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;
    
    private VehicleManager vehicleManager;
     
    /**
     * @return the vehicleSafetyCheckResponseManager
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }

    
    /**
     * @param vehicleSafetyCheckResponseManager the vehicleSafetyCheckResponseManager to set
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }
    
    /**
     * @return the vehicleManager
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }
    
    /**
     * @param vehicleManager the vehicleManager to set
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    /**
      * Getter for loadingRouteManager.
     * @return loadingRouteManager LoadingRouteManager.
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * Setter for loadingRouteManager.
     * @param loadingRouteManager LoadingRouteManager.
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * Getter for the replenishmentManager property.
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return this.replenishmentManager;
    }
    
    /**
     * Setter for the replenishmentManager property.
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }
    
    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }
    
    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }
        
    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }
    
    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

  
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public SignOffManagerImplRoot(OperatorDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.SignOffManagerRoot#executeSignOffOperatorFromUI(com.vocollect.voicelink.core.model.Operator, java.util.Date)
     */
    public void executeSignOffOperatorFromUI(Operator operator, Date signOffTime)
    throws DataAccessException, BusinessRuleException {
        try {
            executeSignOffOperator(operator, signOffTime);
        } catch (DataAccessException e) {
            throw e;
        } catch (BusinessRuleException e) {
            throw e;
        }
        
        // Create notification detail information array
        LOPArrayList lop = new LOPArrayList();
        lop.add("task.operator", operator.getOperatorIdentifier());
        
        // If successful - post notificaiton
        try {
            getVoicelinkNotificationUtil().createNotification(
                "notification.column.keyname.Process.GUISignOff", 
                "voicelink.notification.gui.operator.signedOff",
                NotificationPriority.INFORMATION,
                signOffTime,
                "0",
                lop); 
        } catch (Exception e) {
            // Do nothing
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.SignOffManager#signOffOperator(com.vocollect.voicelink.core.model.Operator, java.util.Date)
     */
    public void executeSignOffOperator(Operator operator, Date signOffTime) 
    throws DataAccessException, BusinessRuleException {

        if (operator.getStatus() == OperatorStatus.SignedOff) {
            throw new BusinessRuleException(
                CoreErrorCode.OPERATOR_ALREADY_SIGNED_OFF,
                new UserMessage("operator.signOff.message.already.signed.off",
                    operator.getOperatorIdentifier()));
        }
        //=====================================================
        //Clean up selection as needed
        assignmentManager.executeClearReservedAssignments(operator);
       
        //Second suspend and active assignments
        List<Assignment> assignments = assignmentManager
            .listActiveAssignments(operator);
       
        for (Assignment a : assignments) {
            if (a.getStatus().isInSet(AssignmentStatus.InProgress)) {
                a.stopAssignment(AssignmentStatus.Suspended, null);
            }
        }
       
        //=====================================================
        //Clean up fork apps 
        List<Replenishment> replenishments = replenishmentManager
            .listActiveReplenishment(operator);
        
        for (Replenishment r : replenishments) {
            r.setOperator(null);
            r.setStatus(ReplenishStatus.Available);
            replenishmentManager.save(r);
        }
        
        //=====================================================
        // Clean up for Loading App (Loading Route)
        getLoadingRouteManager()
            .executeChangeOperationForOperator(operator);
        
        //=====================================================
        //Clean up Put To Store as needed
        getPtsLicenseManager().executeClearReservedLicenses(operator);
       
        //Second suspend and active licenses
        ArrayList <PtsLicense> licenses = (ArrayList <PtsLicense>) getPtsLicenseManager()
            .listActiveLicenses(operator);
       
        for (PtsLicense l : licenses) {
            if (l.getStatus().isInSet(PtsLicenseStatus.InProgress)) {
                l.stopAssignment(PtsLicenseStatus.Suspended, null);
                l.setOperator(null);
            }
        }
        // Ungroup the suspended licenses so they can be picked up seperately.
        if (licenses.size() > 0) {
            getPtsLicenseManager().executeUngroupAssignments(licenses);
        }
        
        //=====================================================
        // CycleCounting cleanup
        getCycleCountingAssignmentManager().executeClearInProgressAssignment(
                operator.getId());
        
        //=====================================================
        // Vehicle Safety Check cleanup
        getVehicleSafetyCheckResponseManager().executeClearDefaultResponses(
                operator);
        getVehicleManager().deleteVehicleByOperator(operator);
        
        //=====================================================
        //Sign operator off
        operator.signOff(signOffTime);
        this.getLaborManager().openOperatorLaborRecord(signOffTime, 
                                                       operator, 
                                                       OperatorLaborActionType.SignOff);
   }


    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.SignOffManagerRoot#getLastLaborRecord(com.vocollect.voicelink.core.model.Operator)
     */
    public Date getEarliestSignOffTime(Operator operator) 
              throws DataAccessException, BusinessRuleException {
        
        if (operator.getStatus() == OperatorStatus.SignedOff) {
            throw new BusinessRuleException(
                CoreErrorCode.OPERATOR_ALREADY_SIGNED_OFF,
                new UserMessage("operator.signOff.message.already.signed.off",
                    operator.getOperatorIdentifier()));
        }
        // if the operator has a sign of time, use it
        if (operator.getSignOffTime() != null) {
            return operator.getSignOffTime();
        }
        return this.getLaborManager().getEarliestEndTime(operator);
    }

    
    /**
     * Getter for the ptsLicenseManager property.
     * @return PtsLicenseManager value of the property
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return this.ptsLicenseManager;
    }

    
    /**
     * Setter for the ptsLicenseManager property.
     * @param ptsLicenseManager the new ptsLicenseManager value
     */
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }

 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 