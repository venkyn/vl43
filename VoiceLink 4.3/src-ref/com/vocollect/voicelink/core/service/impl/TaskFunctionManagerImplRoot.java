/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.TaskFunctionDAO;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.TaskFunctionManager;

import java.util.List;


/**
 * Additional service methods for the <code>BreakType</code> model object.
 *
 * @author Ed Stoll
 */
public abstract class TaskFunctionManagerImplRoot extends
    GenericManagerImpl<TaskFunction, TaskFunctionDAO> 
    implements TaskFunctionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public TaskFunctionManagerImplRoot(TaskFunctionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.TaskFunctionManagerRoot#findByType(com.vocollect.voicelink.core.model.TaskFunctionType)
     */
    public TaskFunction findByType(TaskFunctionType type) throws DataAccessException {
        return getPrimaryDAO().findByType(type);
    }

    /**
     * Implementation to get a list of taskfunctions for the work group id.
     * @param workgroupId to search for the function number
     * @return List of task functions
     * @throws DataAccessException database error
     */
    public List<TaskFunction> listFunctionsByWorkgroup(Long workgroupId)
        throws DataAccessException {
        return getPrimaryDAO().listFunctionsByWorkgroup(workgroupId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.TaskFunctionManagerRoot#listAllOrderByNumberDesc()
     */
    public List<TaskFunction> listAllOrderByNumberDesc()
        throws DataAccessException {
        return getPrimaryDAO().listAllOrderByNumberDesc();
}
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 