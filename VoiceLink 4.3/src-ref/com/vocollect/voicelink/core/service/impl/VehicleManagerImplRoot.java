/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.VehicleDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleCategory;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

/**
 * @author mraj
 * 
 */
public class VehicleManagerImplRoot extends
    GenericManagerImpl<Vehicle, VehicleDAO> implements VehicleManager {
    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleManagerImplRoot(VehicleDAO primaryDAO) {
        super(primaryDAO);
    }

    
    /**
     * @return the vehicleSafetyCheckResponseManager
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }

    /**
     * @param vehicleSafetyCheckResponseManager the vehicleSafetyCheckResponseManager to set
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager 
                                                     vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }


    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.core.service.VehicleManagerRoot#
     * findVehicleByNumberAndType(java.lang.String, java.lang.Integer)
     */
    @Override
    public Vehicle findVehicleByNumberAndType(String vehicleNumber,
                                              Integer vehicleTypeNumber)
        throws DataAccessException {
        return getPrimaryDAO().findVehicleByNumberAndType(vehicleNumber,
            vehicleTypeNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.VehicleManagerRoot#findVehicleBySpokenNumberAndType(java.lang.String, java.lang.Integer)
     */
    @Override
    public Vehicle findVehicleBySpokenNumberAndType(String spokenVehicleNumber,
                                                    Integer vehicleTypeNumber)
        throws DataAccessException {
        return getPrimaryDAO().findVehicleBySpokenNumberAndType(spokenVehicleNumber,
            vehicleTypeNumber);
    }


    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleManagerRoot#findDefaultVehicleByType(java.lang.Integer)
     */
    @Override
    public Vehicle findDefaultVehicleByType(Integer vehicleTypeNumber)
        throws DataAccessException {
        return getPrimaryDAO().findDefaultVehicleByType(vehicleTypeNumber);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleManagerRoot#deleteVehicleByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public void deleteVehicleByOperator(Operator operator)
        throws DataAccessException {
        Vehicle vehicle = getPrimaryDAO().findVehicleByOperator(operator);
        if (vehicle != null && vehicle.getOperators().contains(operator)) {
            vehicle.getOperators().remove(operator);
            getPrimaryDAO().flushSession();
        }
    }
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.VehicleManagerRoot#findVehicleByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public Vehicle findVehicleByOperator(Operator operator)
        throws DataAccessException {
        return getPrimaryDAO().findVehicleByOperator(operator);
    }
    
    @Override
    public Object save(Vehicle instance) throws BusinessRuleException,
        DataAccessException {
        
        //Duplicate 'Default' vehicle numbers can exist
        if (instance.getVehicleCategory() == VehicleCategory.Default) {
            return super.save(instance);
        }
        
        boolean isNew = instance.isNew();
        
        Long numberUniquenessId = getPrimaryDAO().uniquenessByNumber(
            instance.getVehicleNumber());
        
        Long spokenNumberUniquenessId = getPrimaryDAO().uniquenessBySpokenNumber(
            instance.getSpokenVehicleNumber());

        //Check Vehicle Number is unique
        if (numberUniquenessId != null
            && (isNew || (!isNew && numberUniquenessId.longValue() != instance
                .getId().longValue()))) {
            throw new FieldValidationException("vehicle.vehicleNumber", instance
                .getVehicleNumber().toString(), new UserMessage(
                "vsc.vehicle.error.duplicateNumber", instance.getVehicleNumber()
                    .toString()));
        }
        
        
        //Check Spoken Vehicle Number is unique
        if (spokenNumberUniquenessId != null
            && (isNew || (!isNew && spokenNumberUniquenessId.longValue() != instance
                .getId().longValue()))) {
            throw new FieldValidationException("vehicle.spokenVehicleNumber", instance
                .getVehicleNumber().toString(), new UserMessage(
                "vsc.vehicle.error.duplicateSpokenNumber", instance.getSpokenVehicleNumber()
                    .toString()));
        }
        
        return super.save(instance);
    }

    @Override
    public Object delete(Vehicle instance) throws BusinessRuleException,
    DataAccessException {
        //Default vehicle delete not allowed
        if (instance.getVehicleCategory() == VehicleCategory.Default) {
            throw new BusinessRuleException(CoreErrorCode.DEFAULT_VEHICLE_DELETE_NOT_ALLOWED, new UserMessage(
                "vsc.vehicle.delete.default.vehicle.error", instance.getVehicleNumber()));
        }
        
        if (getVehicleSafetyCheckResponseManager()
            .countVehiclSafetyCheckResponsePerVehicle(instance).longValue() > 0) {
            throw new DataAccessException(
                CoreErrorCode.SAFETYCHECK_RESPONSES_EXISTS_FOR_VEHICLETYPE, new UserMessage(
                    "vsc.vehicle.safetyCheck.response.error.exist", instance.getVehicleNumber()), null);
        } else {
            return super.delete(instance);
        } 
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 