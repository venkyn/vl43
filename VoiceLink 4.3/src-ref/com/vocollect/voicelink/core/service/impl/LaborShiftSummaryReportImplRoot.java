/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;


import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.loading.service.ArchiveLoadingLaborManager;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;

import static com.vocollect.epp.util.ReportUtilities.TIME_ZONE;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_TIMEZONE;
/**
 * @author pfunyak, amracna
 * 
 */
public class LaborShiftSummaryReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager,
    LaborShiftSummaryReportRoot {

    // Parameters passed to the report

    private List<String> operatorIdParams;

    private String reportTypeParam;

    private Integer regionNumberParam;

    private Date startDateParam;

    private Date endDateParam;

    private Integer shiftStartParam;

    private Integer shiftLengthParam;

    // goal rate for replenishment
    private String replenishmentGoalRate;

    private String cycleCountingGoalRate;
    
    private String loadingGoalRate;

    // managers used to retrieve report data
    private OperatorLaborManager operatorLaborManager;

    private OperatorManager operatorManager;

    private AssignmentLaborManager assignmentLaborManager;

    private ArchiveAssignmentLaborManager archiveAssignmentLaborManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    private ArchiveCycleCountingLaborManager archiveCycleCountingLaborManager;

    private ReplenishmentManager replenishmentManager;

    private RegionManager regionManager;
    
    private LoadingRouteLaborManager loadingRouteLaborManager;
    
    private ArchiveLoadingLaborManager archiveLoadingLaborManager;


    // constants
    private static final Double MILLISEC_IN_AN_HOUR = 3600000.00;

    private static final Long MILLISEC_PER_DAY = 86400000L;

    // Used to find operator labor fields
    // 0 = ActionType, 1 = RegionNumber, 2 = RegionGoalRate, 3 = StartTime,
    // 4 = EndTime, 5 = Duration
    private static final Integer ACTION_TYPE_NDX = 0;

    private static final Integer REGION_NUMBER_NDX = 1;

    private static final Integer GOAL_RATE_NDX = 2;

    private static final Integer START_TIME_NDX = 3;

    private static final Integer END_TIME_NDX = 4;

    private static final Integer DURATION_NDX = 5;

    // format rate for report
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat(
        "###0.00");

    private static final Logger log = new Logger(
        LaborShiftSummaryReportImplRoot.class);

    // order labor records by startTime
    private static Comparator<Object> orderByStartTime = new Comparator<Object>() {

        public int compare(Object o1, Object o2) {
            Date date1 = null;
            Date date2 = null;

            if (o1 instanceof AssignmentLaborRecord) {
                date1 = ((AssignmentLaborRecord) o1).getStartDate();
                date2 = ((AssignmentLaborRecord) o2).getStartDate();
            } else if (o1 instanceof CycleCountingLaborRecord) {
                date1 = ((CycleCountingLaborRecord) o1).getStartDate();
                date2 = ((CycleCountingLaborRecord) o2).getStartDate();
            }

            if (date1.compareTo(date2) < 0) {
                return -1;
            } else if (date1.compareTo(date2) > 0) {
                return 1;
            }
            return 0;
        }
    };

    private static Comparator<Operator> orderOperatorsByName = new Comparator<Operator>() {

        public int compare(Operator operator1, Operator operator2) {
            if (operator1.getName() == null && operator2.getName() == null) {
                if (operator1.getOperatorIdentifier().compareTo(
                    operator2.getOperatorIdentifier()) < 0) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (operator1.getName() == null
                && operator2.getName() != null) {
                return -1;
            } else if (operator1.getName() != null
                && operator2.getName() == null) {
                return 1;
            } else if (operator1.getName().compareTo(operator2.getName()) < 0) {
                return -1;
            } else if (operator1.getName().compareTo(operator2.getName()) == 0) {
                if (operator1.getOperatorIdentifier().compareTo(
                    operator2.getOperatorIdentifier()) < 0) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                return 1;
            }
        }
    };

    /**
     * Get the parameters passed to the report. Make sure parameters are not
     * null before attempting to save.
     * @param values - map of parameters passed to the report
     * 
     */
    @SuppressWarnings("unchecked")
    private void getParameters(Map<String, Object> values) {

        if (values.get("OPERATOR_ID") != null) {
            values.get("OPERATOR_ID").toString();
        }
        if (values.get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL) != null) {
            this.operatorIdParams = new ArrayList<String>(
                ((Set<String>) values
                    .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL)));
        }
        if (values.get("START_DATE") != null) {
            this.startDateParam = (Date) values.get("START_DATE");
        }
        if (values.get("END_DATE") != null) {
            this.endDateParam = (Date) values.get("END_DATE");
        }
        if (values.get("SHIFT_START") != null) {
            this.shiftStartParam = (Integer) values.get("SHIFT_START");
        }
        if (values.get("SHIFT_LENGTH") != null) {
            this.shiftLengthParam = (Integer) values.get("SHIFT_LENGTH");
        }
        if (values.get("REPORT_TYPE") != null) {
            this.reportTypeParam = values.get("REPORT_TYPE").toString();
        }

        this.regionNumberParam = (Integer) values.get("REGION");

    }

    /**
     * Select operator objects from the Operator labor records based on the
     * given criteria.
     * 
     * @param operatorIds - List of operator or null
     * @param regionNumber - region number or null
     * @param startDate - start of report range
     * @param endDate - end report range
     * @param reportType - report type being requested
     * @return list of operators based on criteria
     */
    private List<Operator> getOperators(List<String> operatorIds,
                                        Integer regionNumber,
                                        Date startDate,
                                        Date endDate,
                                        String reportType) {

        OperatorLaborActionType operatorLaborActionType = null;
        if (reportType.compareTo("SELECTION_ONLY") == 0) {
            operatorLaborActionType = OperatorLaborActionType.Selection;
        } else if (reportType.compareTo("CYCLECOUNTING_ONLY") == 0) {
            operatorLaborActionType = OperatorLaborActionType.CycleCounting;
        } else if (reportType.compareTo("REPLENISHMENT_ONLY") == 0) {
            operatorLaborActionType = OperatorLaborActionType.Replenishment;
        } else if (reportType.compareTo("LOADING_ONLY") == 0) {
            operatorLaborActionType = OperatorLaborActionType.Loading;
        }

        try {
            // if operatorId has only one entry and that entry contains a
            // real operator id then we are requesting all operators.
            // if regionNumber is null then we are requesting all regions.

            // TODO: Fix this workaround when the code in
            // ReportUtilities.setReportOperatorIdentifiers is updated.
            // The current implemenation puts a single space in the operatorId2
            // list by default.
            if (operatorIds.size() == 1 && operatorIds.get(0) == " ") {
                // the user wants to report on all operators that meet the
                // "report on" criteria for the date range specified
                if (operatorLaborActionType == OperatorLaborActionType.Selection
                    || operatorLaborActionType == OperatorLaborActionType.CycleCounting
                    || operatorLaborActionType == OperatorLaborActionType.Loading) {
                    if (regionNumber == null) {
                        /*
                         * CSPGW-134 Only operators who at least did some
                         * selection in the specified period should be reported
                         * on.
                         */
                        // report on all operators that at least performed
                        // some selection in the specified time period
                        // select all operators who performed selection within
                        // the date range.
                        return this.getOperatorLaborManager()
                            .listAllOperatorsActionBetweenDates(startDate,
                                endDate, operatorLaborActionType);
                    } else {
                        // a region was spec'd - report on only selectors who at
                        // least did some selection
                        // in the spec'd region in the time period spec'd.
                        // select all operators who performed selection in the
                        // given region within the date range.
                        return this.getOperatorLaborManager()
                            .listAllOperatorsActionRegionBetweenDates(
                                startDate, endDate, operatorLaborActionType,
                                regionNumber);
                    }
                } else if (reportType.compareTo("REPLENISHMENT") == 0) {
                    // select all operators who performed replenishment within
                    // the date range.
                    return this.getOperatorLaborManager()
                        .listAllOperatorsActionBetweenDates(startDate, endDate,
                            operatorLaborActionType);
                } else if (reportType.compareTo("LOADING_ONLY") == 0) {
                    // select all operators who performed replenishment within
                    // the date range.
                    return this.getOperatorLaborManager()
                        .listAllOperatorsActionBetweenDates(startDate, endDate,
                            operatorLaborActionType);
                }
            } else {
                // we are requesting a specific operator
                List<Operator> operatorList = new ArrayList<Operator>();
                Operator operator;
                for (String operatorId : operatorIds) {
                    operator = this.getOperatorManager().findByIdentifier(
                        operatorId);
                    operatorList.add(operator);
                }
                return operatorList;
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    public JRDataSource getDataSource(Map<String, Object> values) {
        Boolean isInterval = (values.get("IS_INTERVAL") == null)
            ? Boolean.TRUE : (Boolean) values.get("IS_INTERVAL");
        TimeZone siteTz = SiteContextHolder.getSiteContext().getCurrentSite().getTimeZone();
        
        values.remove(JASPER_REPORT_TIMEZONE);
        values.put(TIME_ZONE, siteTz);
        // get the parameters passed to the report.
        this.getParameters(values);

        // calculate the number of shifts
        int numberOfShifts = this.getNumberOfShifts(this.startDateParam,
            this.endDateParam, this.shiftStartParam, this.shiftLengthParam);

        // get the goal rate for Replenishment from any replenishment region
        try {
            this.setReplenishmentGoalRate(this
                .getAverageGoalRate(RegionType.Replenishment));
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        try {
            this.setCycleCountingGoalRate(this
                .getAverageGoalRate(RegionType.CycleCounting));
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        
        try {
            this.setLoadingGoalRate(this
                .getAverageGoalRate(RegionType.Loading));
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        // calculate the overall start date of the entire period
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(this.startDateParam);
        
        Calendar endDate = Calendar.getInstance();
        endDate.setTime(this.endDateParam);
        
        Date serverStartDate;
        Date serverEndDate;
        
        Date siteStartDate;
        Date siteEndDate;
        
        siteStartDate = DateUtil.convertDateTimeZone(startDate,
            startDate.getTimeZone(), siteTz);
        startDate.setTime(siteStartDate);
        startDate.set(Calendar.HOUR_OF_DAY, this.shiftStartParam);
        startDate.set(Calendar.MINUTE, 0);
        
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate.getTime());
        serverStartDate = DateUtil.convertDateTimeZone(cal,
            siteTz, startDate.getTimeZone());
        
        // get the overall end date of the entire period
        siteEndDate = DateUtil.convertDateTimeZone(endDate,
            endDate.getTimeZone(), siteTz);
        siteEndDate = this.getOverallEndDate(this.startDateParam, siteEndDate,
            this.shiftStartParam, this.shiftLengthParam);

        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(siteEndDate);
        serverEndDate = DateUtil.convertDateTimeZone(cal1, siteTz,
            endDate.getTimeZone());

        // get the list of operators who have labor records meeting the criteria
        // in the overall date range.
        List<Operator> operList = this.getOperators(this.operatorIdParams,
            this.regionNumberParam, serverStartDate, serverEndDate,
            this.reportTypeParam);

        // if list is empty return empty collection to the report.
        if (operList == null || operList.isEmpty()) {
            log.debug("Empty Report!!! No Operators meet this criteria.");

            // CSPGW-122 set header information , so a blank page is not
            // returned.
            SummaryReportData blank = new SummaryReportData();
            blank
                .setOperatorName(ResourceUtil
                    .getLocalizedKeyValue("selection.report.laborsummary.nooperators"));
            blank
                .setOperatorID(ResourceUtil.getLocalizedKeyValue("report.all"));
            if (this.regionNumberParam == null) {
                blank.setRegionNumber(ResourceUtil
                    .getLocalizedKeyValue("report.all"));
            } else {
                blank.setRegionNumber(this.regionNumberParam.toString());
            }
            blank.setStartDate(siteStartDate);
            blank.setEndDate(siteEndDate);
            blank.setShiftStart(new Timestamp(startDate.getTime().getTime()));
            blank.setShiftEnd(new Timestamp(serverEndDate.getTime()));
            blank.setShiftLength(this.shiftLengthParam);

            ArrayList<SummaryReportData> emptyReport = new ArrayList<SummaryReportData>();
            emptyReport.add(blank);

            return (new JRBeanCollectionDataSource(emptyReport));

        }

        // sort the operators by employeeName, operatorId
        Collections.sort(operList, orderOperatorsByName);

        // list to hold report records
        List<SummaryReportData> reportRecordList = new ArrayList<SummaryReportData>();

        // loop through the operators and create the report records
        // each operator gets ONE SummaryReportData record which contains
        // header information and all of the shift details for the given date
        // range
        for (Operator operator : operList) {

            log.debug("Processing Operator: " + operator.getName() + ":"
                + operator.getOperatorIdentifier());

            SummaryReportData reportRecord = new SummaryReportData();
            // fill the record base information
            reportRecord.setOperatorName(operator.getName());
            reportRecord.setOperatorID(operator.getOperatorIdentifier());
            if (this.regionNumberParam == null) {
                reportRecord.setRegionNumber(ResourceUtil
                    .getLocalizedKeyValue("report.all"));
            } else {
                reportRecord.setRegionNumber(this.regionNumberParam.toString());
            }

            // set the date related fields for the report header
            // set start and end range for report
            reportRecord.setStartDate(siteStartDate);
            reportRecord.setEndDate(siteEndDate);
            // set the shift start and shift end times for the report
            reportRecord
                .setShiftStart(new Timestamp(startDate.getTime().getTime()));
            reportRecord.setShiftEnd(new Timestamp(siteEndDate.getTime()));
            reportRecord.setShiftLength(this.shiftLengthParam);

            // prime pump for the first set of detail records.
            // we need the shift start and end dates to call the db to fetch the
            // aggregates.
            // the shift start date for the first set of aggregates is the date
            // we just computed above.
            // the shift end time is simply the shift start date plus the shift
            // end time.
            Calendar shiftStartDate = Calendar.getInstance();
            shiftStartDate.setTime(startDate.getTime());

            Calendar shiftEndDate = Calendar.getInstance();
            // set the date to the shift start date
            shiftEndDate.setTime(startDate.getTime());
            // bump the date ahead to the end of the shift
            shiftEndDate.add(Calendar.HOUR_OF_DAY, this.shiftLengthParam);

            // create all detail records for all shifts for the current operator
            List<ShiftDetailRec> detailRecList = new ArrayList<ShiftDetailRec>();
            for (int shift = 1; shift <= numberOfShifts; shift++) {
                // List to hold the detail records
                detailRecList.addAll(this.createShiftDetailRecords(operator,
                    shiftStartDate, shiftEndDate, isInterval));
                Long nextStartShift = shiftStartDate.getTimeInMillis()
                    + MILLISEC_PER_DAY;
                Long nextEndShift = shiftEndDate.getTimeInMillis()
                    + MILLISEC_PER_DAY;
                shiftStartDate.setTimeInMillis(nextStartShift);
                shiftEndDate.setTimeInMillis(nextEndShift);
            }
            // add the list of detail records to the SubReportData object
            reportRecord.setDetails(new JRBeanCollectionDataSource(
                detailRecList));
            // add the report record to the summary data list
            reportRecordList.add(reportRecord);
        }
        // return the data source for jasper reports
        return new JRBeanCollectionDataSource(reportRecordList);
    }

    /**
     * @param startDate - the start date
     * @param endDate - the end date
     * @param shiftStartTime - the shift start time
     * @param shiftLength - the shift length
     * @return Date
     */
    private Date getOverallEndDate(Date startDate,
                                   Date endDate,
                                   Integer shiftStartTime,
                                   Integer shiftLength) {

        Calendar overallEndDate = Calendar.getInstance();

        // If the shift is non-overlapping, the overall end date is the end
        // date entered by the user with the shift start and shift length
        // added to it.
        // If the shift is overlapping (overlaps two days), the end date is the
        // end of the shift that started on the previous day

        Calendar startOfFirstShift = Calendar.getInstance();
        startOfFirstShift.setTime(startDate);
        startOfFirstShift.set(Calendar.HOUR_OF_DAY, shiftStartTime);

        Calendar endOfFirstShift = Calendar.getInstance();
        endOfFirstShift.setTime(startOfFirstShift.getTime());
        endOfFirstShift.add(Calendar.HOUR_OF_DAY, shiftLength);

        overallEndDate.setTime(endDate);
        overallEndDate.set(Calendar.HOUR_OF_DAY, shiftStartTime + shiftLength);

        return overallEndDate.getTime();
    }

    /**
     * @param startDate - the start date
     * @param endDate - the end date
     * @param shiftStartTime - the shift start time
     * @param shiftLength - the shift length
     * @return int
     */
    private int getNumberOfShifts(Date startDate,
                                  Date endDate,
                                  Integer shiftStartTime,
                                  Integer shiftLength) {

        Long intervals; // number of possible shift slots.

        // Get total millisecs in the reporting period
        Long mSecPerReport = new Long(endDate.getTime() - startDate.getTime());

        // determine the total number of shift slots that the report
        // encompasses.
        intervals = mSecPerReport / MILLISEC_PER_DAY;
        // include partial slots.
        if ((mSecPerReport % MILLISEC_PER_DAY) > 0) {
            intervals++;
        }

        return intervals.intValue();
    }


    
    /**
     * @throws DataAccessException
     * 
     */
    /**
     * @param operatorId operator Id
     * @param actionType action type  (loading, replenishment etc)
     * @return average goal rate
     * @throws DataAccessException
     */
    private String getAverageGoalRateByOperator(Long operatorId,
            OperatorLaborActionType actionType) throws DataAccessException {

        Double average = this.getOperatorLaborManager()
                .avgGoalRateForTypeAndOperator(operatorId, actionType);
        if (average != null) {
            return average.toString();
        } else {
            return null;
        }
    }

    /**
     * @param regionType Type of region
     * @return The string value of the average goal rate
     * @throws DataAccessException A dataacessexception on any issue
     */
    private String getAverageGoalRate(RegionType regionType)
        throws DataAccessException {
        Double average = this.getRegionManager().avgGoalRateForType(regionType);
        if (average != null) {
            return average.toString();
        } else {
            return null;
        }

    }

    /**
     * @param operator - the operator
     * @param shiftStartDate - the shift start date
     * @param shiftEndDate - the shift end date
     * @param isInterval - true if interval is selected from report launcher
     * @return List<ShiftDetailRec>
     */
    private List<ShiftDetailRec> createShiftDetailRecords(Operator operator,
                                                          Calendar shiftStartDate,
                                                          Calendar shiftEndDate, boolean isInterval) {
       
        Calendar recordStartDate = Calendar.getInstance();
        Calendar recordEndDate = Calendar.getInstance();
        
        recordStartDate.setTime(shiftStartDate.getTime());
        recordEndDate.setTime(shiftEndDate.getTime());
        
        Date shiftStart = recordStartDate.getTime();
        Date shiftEnd = recordEndDate.getTime();

        TimeZone siteTimeZone = SiteContextHolder.getSiteContext().getCurrentSite().getTimeZone();
        TimeZone systemTimeZone = Calendar.getInstance().getTimeZone();
        
        shiftStart = DateUtil.convertDateTimeZone(recordStartDate,
            siteTimeZone, systemTimeZone);
        shiftEnd = DateUtil.convertDateTimeZone(recordEndDate,
            siteTimeZone, systemTimeZone);
        
        Date displayShiftDate = shiftStartDate.getTime();
        
        // the processed list of details to return
        List<ShiftDetailRec> shiftDetailRecords = new ArrayList<ShiftDetailRec>();

        // get OperatorLabor data for the given operator and given shift
        List<ShiftDetailRec> unprocessedShiftDetailRecords = this
            .getOperatorLaborRecords(operator, shiftStart, shiftEnd, displayShiftDate);
        List<AssignmentLaborRecord> assignmentLaborRecords = this
            .getAssignmentLaborRecords(operator, shiftStart, shiftEnd);
        List<ReplenishmentRecord> replenishmentRecords = this
            .getReplenishmentRecords(operator, shiftStart, shiftEnd);
        List<CycleCountingLaborRecord> cycleCountingLaborRecords = this
            .getCycleCountingLaborRecords(operator, shiftStart, shiftEnd);
        List<LoadingRecord> loadingRecords = this
                .getLoadingRecords(operator, shiftStart, shiftEnd);

        // if no data, return an empty list
        if (unprocessedShiftDetailRecords.isEmpty()) {
            return unprocessedShiftDetailRecords;
        }

        adjustDetails(unprocessedShiftDetailRecords, assignmentLaborRecords,
            replenishmentRecords, cycleCountingLaborRecords, loadingRecords,
            shiftStart, shiftEnd);
        fillDetails(unprocessedShiftDetailRecords, assignmentLaborRecords,
            replenishmentRecords, cycleCountingLaborRecords, loadingRecords);
        aggregateDetails(shiftDetailRecords, unprocessedShiftDetailRecords);
        filterDetails(shiftDetailRecords, this.reportTypeParam,
            this.regionNumberParam);

        return shiftDetailRecords;
    }

    /**
     * @param unprocessedShiftDetailRecords - unprocessed shift detail list
     * @param assignmentLaborRecords - list of assignment labor records
     * @param replenishmentRecords - list of replenishment reports
     * @param cycleCountingLaborRecords list of cycle counting report objects
     * @param loadingRecords list of loading report objects
     * @param shiftStart - shift start date
     * @param shiftEnd - shift end date
     */
    private void adjustDetails(List<ShiftDetailRec> unprocessedShiftDetailRecords,
                               List<AssignmentLaborRecord> assignmentLaborRecords,
                               List<ReplenishmentRecord> replenishmentRecords,
                               List<CycleCountingLaborRecord> cycleCountingLaborRecords,
                               List<LoadingRecord> loadingRecords,
                               Date shiftStart,
                               Date shiftEnd) {
        // adjust the first record start time (and potentially duration)
        if (unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.SignOn
            || unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.SignOff
            || unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.Break) {
            // simply set the start time to the shift start time
            unprocessedShiftDetailRecords.get(0).setStartTime(shiftStart);
            // if end time is not null then set the duration, otherwise we'll
            // catch this when adjusting the last record
            // since if the first record is open it means it is also the last
            // record
            if (unprocessedShiftDetailRecords.get(0).getEndTime() != null) {
                unprocessedShiftDetailRecords.get(0).setDuration(
                    unprocessedShiftDetailRecords.get(0).getEndTime().getTime()
                        - unprocessedShiftDetailRecords.get(0).getStartTime()
                            .getTime());
            }
        } else if (unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.Selection) {
            // check to see if there's anything in assignmentLaborRecords
            if (!assignmentLaborRecords.isEmpty()) {
                // check to see if we should adjust the start time to the start
                // time of the first AL record
                // or the shift start time
                if (assignmentLaborRecords.get(0).getStartDate()
                    .compareTo(shiftStart) < 0) {
                    // set the start time of the shift detail record to the
                    // start time of first al record
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        assignmentLaborRecords.get(0).getStartDate());
                } else {
                    // set the start time to the shift start time
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        shiftStart);
                }

            } else {
                // set the start time to the shift start time
                unprocessedShiftDetailRecords.get(0).setStartTime(shiftStart);
            }
            // if end time is not null then set the duration, otherwise we'll
            // catch this when adjusting the last record
            // since if the first record is open it means it is also the last
            // record
            if (unprocessedShiftDetailRecords.get(0).getEndTime() != null) {
                unprocessedShiftDetailRecords.get(0).setDuration(
                    unprocessedShiftDetailRecords.get(0).getEndTime().getTime()
                        - unprocessedShiftDetailRecords.get(0).getStartTime()
                            .getTime());
            }
        } else if (unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.Replenishment) {
            // check to see if there's anything in assignmentLaborRecords
            if (!replenishmentRecords.isEmpty()) {
                // check to see if we should adjust the start time to the start
                // time of the first replenishment record
                // or the shift start time
                if (replenishmentRecords.get(0).getStartDate()
                    .compareTo(shiftStart) < 0) {
                    // set the start time of the shift detail record to the
                    // start time of first replenishment record
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        replenishmentRecords.get(0).getStartDate());
                } else {
                    // set the start time to the shift start time
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        shiftStart);
                }
            } else {
                // set the start time to the shift start time
                unprocessedShiftDetailRecords.get(0).setStartTime(shiftStart);
            }
            // if end time is not null then set the duration, otherwise we'll
            // catch this when adjusting the last record
            // since if the first record is open it means it is also the last
            // record
            if (unprocessedShiftDetailRecords.get(0).getEndTime() != null) {
                unprocessedShiftDetailRecords.get(0).setDuration(
                    unprocessedShiftDetailRecords.get(0).getEndTime().getTime()
                        - unprocessedShiftDetailRecords.get(0).getStartTime()
                            .getTime());
            }
        } else if (unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.CycleCounting) {
            // check to see if there's anything in assignmentLaborRecords
            if (!cycleCountingLaborRecords.isEmpty()) {
                // check to see if we should adjust the start time to the start
                // time of the first cycle counting record
                // or the shift start time
                if (cycleCountingLaborRecords.get(0).getStartDate()
                    .compareTo(shiftStart) < 0) {
                    // set the start time of the shift detail record to the
                    // start time of first cycle counting record
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        cycleCountingLaborRecords.get(0).getStartDate());
                } else {
                    // set the start time to the shift start time
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        shiftStart);
                }
            } else {
                // set the start time to the shift start time
                unprocessedShiftDetailRecords.get(0).setStartTime(shiftStart);
            }
            // if end time is not null then set the duration, otherwise we'll
            // catch this when adjusting the last record
            // since if the first record is open it means it is also the last
            // record
            if (unprocessedShiftDetailRecords.get(0).getEndTime() != null) {
                unprocessedShiftDetailRecords.get(0).setDuration(
                    unprocessedShiftDetailRecords.get(0).getEndTime().getTime()
                        - unprocessedShiftDetailRecords.get(0).getStartTime()
                            .getTime());
            }
        } else if (unprocessedShiftDetailRecords.get(0).getAction() == OperatorLaborActionType.Loading) {
            // check to see if there's anything in loading Records
            if (!loadingRecords.isEmpty()) {
                // check to see if we should adjust the start time to the start
                // time of the first loading record
                // or the shift start time
                if (loadingRecords.get(0).getStartDate()
                    .compareTo(shiftStart) < 0) {
                    // set the start time of the shift detail record to the
                    // start time of first loading record
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                            loadingRecords.get(0).getStartDate());
                } else {
                    // set the start time to the shift start time
                    unprocessedShiftDetailRecords.get(0).setStartTime(
                        shiftStart);
                }
            } else {
                // set the start time to the shift start time
                unprocessedShiftDetailRecords.get(0).setStartTime(shiftStart);
            }
            // if end time is not null then set the duration, otherwise we'll
            // catch this when adjusting the last record
            // since if the first record is open it means it is also the last
            // record
            if (unprocessedShiftDetailRecords.get(0).getEndTime() != null) {
                unprocessedShiftDetailRecords.get(0).setDuration(
                    unprocessedShiftDetailRecords.get(0).getEndTime().getTime()
                        - unprocessedShiftDetailRecords.get(0).getStartTime()
                            .getTime());
            }
        }

        // adjust the last record end time
        if (unprocessedShiftDetailRecords.get(
            unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.SignOn
            || unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.SignOff
            || unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.Break) {
            // simply set the end time to the shift end time
            unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).setEndTime(shiftEnd);
            unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).setDuration(
                unprocessedShiftDetailRecords
                    .get(unprocessedShiftDetailRecords.size() - 1).getEndTime()
                    .getTime()
                    - unprocessedShiftDetailRecords
                        .get(unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime().getTime());
        } else if (unprocessedShiftDetailRecords.get(
            unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.Selection) {
            // check to see if there's anything in assignmentLaborRecords
            if (!assignmentLaborRecords.isEmpty()) {
                // check to see if the last assignmentLaborRecord belongs to
                // this OL Selection record
                if (assignmentLaborRecords
                    .get(assignmentLaborRecords.size() - 1)
                    .getStartDate()
                    .compareTo(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime()) >= 0) {
                    // set the end time of the shift detail record to the end
                    // time of last al record
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        assignmentLaborRecords.get(
                            assignmentLaborRecords.size() - 1).getEndDate());
                } else {
                    // set the end time to the start time so we don't calculate
                    // excess down time
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime());
                }
            } else {
                // set the end time to the start time so we don't calculate
                // excess down time
                unprocessedShiftDetailRecords.get(
                    unprocessedShiftDetailRecords.size() - 1).setEndTime(
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime());
            }
            // recalculate the duration
            unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).setDuration(
                unprocessedShiftDetailRecords
                    .get(unprocessedShiftDetailRecords.size() - 1).getEndTime()
                    .getTime()
                    - unprocessedShiftDetailRecords
                        .get(unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime().getTime());
        } else if (unprocessedShiftDetailRecords.get(
            unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.Replenishment) {
            // check to see if there's anything in replenishmentRecords
            if (!replenishmentRecords.isEmpty()) {
                // check to see if the last replenishmentRecord belongs to this
                // OL Selection record
                if (replenishmentRecords
                    .get(replenishmentRecords.size() - 1)
                    .getStartDate()
                    .compareTo(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime()) >= 0) {
                    // set the end time of the shift detail record to the end
                    // time of the last replenishment record
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        replenishmentRecords.get(
                            replenishmentRecords.size() - 1).getEndDate());
                } else {
                    // set the end time to the start time so we don't calculate
                    // excess down time
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime());
                }
            } else {
                // set the end time to the start time so we don't calculate
                // excess down time
                unprocessedShiftDetailRecords.get(
                    unprocessedShiftDetailRecords.size() - 1).setEndTime(
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime());
            }
            // recalculate the duration
            unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).setDuration(
                unprocessedShiftDetailRecords
                    .get(unprocessedShiftDetailRecords.size() - 1).getEndTime()
                    .getTime()
                    - unprocessedShiftDetailRecords
                        .get(unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime().getTime());
        } else if (unprocessedShiftDetailRecords.get(
            unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.CycleCounting) {
            // check to see if there's anything in cycleCountingLaborRecords
            if (!cycleCountingLaborRecords.isEmpty()) {
                // check to see if the last replenishmentRecord belongs to this
                // OL Selection record
                if (cycleCountingLaborRecords
                    .get(cycleCountingLaborRecords.size() - 1)
                    .getStartDate()
                    .compareTo(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime()) >= 0) {
                    // set the end time of the shift detail record to the end
                    // time of the last replenishment record
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        cycleCountingLaborRecords.get(
                            cycleCountingLaborRecords.size() - 1).getEndDate());
                } else {
                    // set the end time to the start time so we don't calculate
                    // excess down time
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime());
                }
            } else {
                // set the end time to the start time so we don't calculate
                // excess down time
                unprocessedShiftDetailRecords.get(
                    unprocessedShiftDetailRecords.size() - 1).setEndTime(
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime());
            }
            // recalculate the duration
            unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).setDuration(
                unprocessedShiftDetailRecords
                    .get(unprocessedShiftDetailRecords.size() - 1).getEndTime()
                    .getTime()
                    - unprocessedShiftDetailRecords
                        .get(unprocessedShiftDetailRecords.size() - 1)
                        .getStartTime().getTime());
        } else if (unprocessedShiftDetailRecords.get(
                unprocessedShiftDetailRecords.size() - 1).getAction() == OperatorLaborActionType.Loading) {
                // check to see if there's anything in loading Records
                if (!loadingRecords.isEmpty()) {
                    // check to see if the last loading Record belongs to this
                    // OL Selection record
                    if (loadingRecords
                        .get(loadingRecords.size() - 1)
                        .getStartDate()
                        .compareTo(
                            unprocessedShiftDetailRecords.get(
                                unprocessedShiftDetailRecords.size() - 1)
                                .getStartTime()) >= 0) {
                        // set the end time of the shift detail record to the end
                        // time of the last loading record
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1).setEndTime(
                                    loadingRecords.get(
                                            loadingRecords.size() - 1).getEndDate());
                    } else {
                        // set the end time to the start time so we don't calculate
                        // excess down time
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1).setEndTime(
                            unprocessedShiftDetailRecords.get(
                                unprocessedShiftDetailRecords.size() - 1)
                                .getStartTime());
                    }
                } else {
                    // set the end time to the start time so we don't calculate
                    // excess down time
                    unprocessedShiftDetailRecords.get(
                        unprocessedShiftDetailRecords.size() - 1).setEndTime(
                        unprocessedShiftDetailRecords.get(
                            unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime());
                }
                // recalculate the duration
                unprocessedShiftDetailRecords.get(
                    unprocessedShiftDetailRecords.size() - 1).setDuration(
                    unprocessedShiftDetailRecords
                        .get(unprocessedShiftDetailRecords.size() - 1).getEndTime()
                        .getTime()
                        - unprocessedShiftDetailRecords
                            .get(unprocessedShiftDetailRecords.size() - 1)
                            .getStartTime().getTime());
            }

    }

    /**
     * @param shiftDetailRecords - list of shift details
     * @param reportType - the report type
     * @param regionNumber - the region number
     */
    private void filterDetails(List<ShiftDetailRec> shiftDetailRecords,
                               String reportType,
                               Integer regionNumber) {

        List<ShiftDetailRec> filteredList = new ArrayList<ShiftDetailRec>();

        for (ShiftDetailRec detail : shiftDetailRecords) {

            if (detail.getAction() == OperatorLaborActionType.SignOn
                || detail.getAction() == OperatorLaborActionType.SignOff
                || detail.getAction() == OperatorLaborActionType.Break) {
                // these details always get included
                filteredList.add(detail);
            } else if (reportType.compareTo("SELECTION_ONLY") == 0) {
                // check to see if the detail is for selection - keep only
                // selection
                if (detail.getAction() == OperatorLaborActionType.Selection) {
                    if (regionNumber == null) {
                        // user chose all selection regions so keep the detail
                        filteredList.add(detail);
                    } else if (detail.getRegionNumber().compareTo(regionNumber) == 0) {
                        // keep this detail because it is for the region the
                        // user chose
                        filteredList.add(detail);
                    }
                }
            } else if (reportType.compareTo("REPLENISHMENT_ONLY") == 0) {
                // check to see if the detail is for replenishment - keep only
                // replenishment
                if (detail.getAction() == OperatorLaborActionType.Replenishment) {
                    filteredList.add(detail);
                }

            } else if (reportType.compareTo("CYCLECOUNTING_ONLY") == 0) {
                // check to see if the detail is for replenishment - keep only
                // replenishment
                if (detail.getAction() == OperatorLaborActionType.CycleCounting) {
                    filteredList.add(detail);
                }

            } else if (reportType.compareTo("LOADING_ONLY") == 0) {
                // check to see if the detail is for loading - keep only
                // loading
                if (detail.getAction() == OperatorLaborActionType.Loading) {
                    filteredList.add(detail);
                }
            }  else if (reportType.compareTo("SELECTION_REPLENISHMENT") == 0
                || reportType.compareTo("REPLENISHMENT_SELECTION") == 0) {
                // keep all selection and replenishment details as well
                filteredList.add(detail);
            }
        }
        shiftDetailRecords.clear();
        shiftDetailRecords.addAll(filteredList);
    }

    /**
     * @param shiftDetailRecords - list of shift details
     * @param unprocessedShiftDetailRecords - list of unprocessed shift details
     */
    private void aggregateDetails(List<ShiftDetailRec> shiftDetailRecords,
                                  List<ShiftDetailRec> unprocessedShiftDetailRecords) {
        // iterate once over the list for each record aggregating the totals
        // into shiftDetailRecords
        for (ShiftDetailRec detail : unprocessedShiftDetailRecords) {
            // look for existing detail with this action/region in
            // shiftDetailRecords
            if (!shiftDetailRecords.isEmpty()) {
                int i = 0;
                boolean shiftDetailNotFound = true;
                while (shiftDetailNotFound && i < shiftDetailRecords.size()) {
                    ShiftDetailRec shiftDetail = shiftDetailRecords.get(i);
                    if ((shiftDetail.getAction() == detail.getAction() && (detail
                        .getAction() == OperatorLaborActionType.Replenishment
                        || detail.getAction() == OperatorLaborActionType.Break
                        || detail.getAction() == OperatorLaborActionType.SignOn || detail
                            .getAction() == OperatorLaborActionType.SignOff))) {

                        aggregate(detail, shiftDetail);

                        if (shiftDetail.getDuration() > 0
                            && shiftDetail.getAction() == OperatorLaborActionType.Replenishment) {
                            Double rate = new Double(
                                shiftDetail.getCount()
                                    / (shiftDetail.getDuration() / MILLISEC_IN_AN_HOUR));
                            shiftDetail.setRate(DECIMAL_FORMAT.format(rate));
                        } else if (shiftDetail.getAction() == OperatorLaborActionType.Replenishment) {
                            shiftDetail.setRate("0.00");
                        }
                        shiftDetail.setWorkingTime(shiftDetail.getWorkingTime()
                            + detail.getWorkingTime());
                        shiftDetailRecords.set(i, shiftDetail);
                        shiftDetailNotFound = false;

                    } else if ((shiftDetail.getAction() == detail.getAction()
                        && (detail.getAction() == OperatorLaborActionType.Selection) && shiftDetail
                        .getRegionNumber().compareTo(detail.getRegionNumber()) == 0)) {
                        // aggregate detail into shiftDetail only if this is a
                        // Selection detail. No other actions besides
                        // Replenishment and Selection are currently supported.

                        aggregate(detail, shiftDetail);

                        if (shiftDetail.getDuration() > 0) {
                            Double rate = new Double(
                                shiftDetail.getCount()
                                    / (shiftDetail.getDuration() / MILLISEC_IN_AN_HOUR));
                            shiftDetail.setRate(DECIMAL_FORMAT.format(rate));
                        } else {
                            shiftDetail.setRate("0.00");
                        }
                        shiftDetail.setWorkingTime(shiftDetail.getWorkingTime()
                            + detail.getWorkingTime());
                        shiftDetailRecords.set(i, shiftDetail);
                        shiftDetailNotFound = false;
                    } else if (shiftDetail.getAction() == detail.getAction()
                        && (detail.getAction() == OperatorLaborActionType.CycleCounting)) {

                        aggregate(detail, shiftDetail);

                        if (shiftDetail.getDuration() > 0
                            && shiftDetail.getAction() == OperatorLaborActionType.CycleCounting) {
                            Double rate = new Double(
                                shiftDetail.getCount()
                                    / (shiftDetail.getDuration() / MILLISEC_IN_AN_HOUR));
                            shiftDetail.setRate(DECIMAL_FORMAT.format(rate));
                        } else if (shiftDetail.getAction() == OperatorLaborActionType.CycleCounting) {
                            shiftDetail.setRate("0.00");
                        }
                        shiftDetail.setWorkingTime(shiftDetail.getWorkingTime()
                            + detail.getWorkingTime());
                        shiftDetailRecords.set(i, shiftDetail);
                        shiftDetailNotFound = false;
                    } else if (shiftDetail.getAction() == detail.getAction()
                            && (detail.getAction() == OperatorLaborActionType.Loading)) {

                            aggregate(detail, shiftDetail);

                            if (shiftDetail.getDuration() > 0
                                && shiftDetail.getAction() == OperatorLaborActionType.Loading) {
                                Double rate = new Double(
                                    shiftDetail.getCount()
                                        / (shiftDetail.getDuration() / MILLISEC_IN_AN_HOUR));
                                shiftDetail.setRate(DECIMAL_FORMAT.format(rate));
                            } else if (shiftDetail.getAction() == OperatorLaborActionType.Loading) {
                                shiftDetail.setRate("0.00");
                            }
                            shiftDetail.setWorkingTime(shiftDetail.getWorkingTime()
                                + detail.getWorkingTime());
                            shiftDetailRecords.set(i, shiftDetail);
                            shiftDetailNotFound = false;
                        }
                    i++;
                }
                // Only supporting Selection and Replenishment for the 4.0
                // release. In 4.1 we added cycle counting and loading so these
                // still apply
                if (shiftDetailNotFound
                    && (detail.getAction() != OperatorLaborActionType.PutAway)
                    && (detail.getAction() != OperatorLaborActionType.PutToStore)
                    && (detail.getAction() != OperatorLaborActionType.LineLoading)) {
                    // add the current detail into shiftDetailRecords
                    shiftDetailRecords.add(detail);
                }

            } else {
                if ((detail.getAction() != OperatorLaborActionType.PutAway)
                    && (detail.getAction() != OperatorLaborActionType.PutToStore)
                    && (detail.getAction() != OperatorLaborActionType.LineLoading)) {
                    // there are no shift detail records yet, so just add this
                    // detail to shiftDetailRecords
                    shiftDetailRecords.add(detail);
                }
            }
        }
    }

    /**
     * @param detail
     * @param shiftDetail
     */
    private void aggregate(ShiftDetailRec detail, ShiftDetailRec shiftDetail) {
        shiftDetail.setBreakTime(shiftDetail.getBreakTime()
            + detail.getBreakTime());
        shiftDetail.setDownTime(shiftDetail.getDownTime()
            + detail.getDownTime());
        shiftDetail.setDuration(shiftDetail.getDuration()
            + detail.getDuration());
        shiftDetail.setCount(shiftDetail.getCount() + detail.getCount());
        shiftDetail.setNonWorkingTime(shiftDetail.getNonWorkingTime()
            + detail.getNonWorkingTime());
    }

    /**
     * @param unprocessedShiftDetailRecords - list of unprocessed shift detail
     *            records
     * @param assignmentLaborRecords - list of assignment labor records
     * @param replenishmentRecords - list of replenishment records
     * @param cycleCountingLaborRecords - list of cycleCounting records
     * @param loadingRecords - list loadingRecords
     */
    private void fillDetails(List<ShiftDetailRec> unprocessedShiftDetailRecords,
                             List<AssignmentLaborRecord> assignmentLaborRecords,
                             List<ReplenishmentRecord> replenishmentRecords,
                             List<CycleCountingLaborRecord> cycleCountingLaborRecords, 
                             List<LoadingRecord> loadingRecords) {

        // iterate over the lists filling in the appropriate times and counts

        // index for assignmentLaborRecords
        int a = 0;
        // index for replenishmentRecords
        int r = 0;
        // index for cyclecountingRecords
        int c = 0;
        // index for loadingRecords
        int l = 0;

        for (ShiftDetailRec detail : unprocessedShiftDetailRecords) {
            OperatorLaborActionType action = detail.getAction();
            if ((action == OperatorLaborActionType.SignOn)
                || action == OperatorLaborActionType.SignOff) {
                detail.setNonWorkingTime(detail.getDuration());
            } else if (action == OperatorLaborActionType.Break) {
                detail.setBreakTime(detail.getDuration());
            } else if (action == OperatorLaborActionType.Selection) {
                // check for empty assignmentLaborRecords
                if (!assignmentLaborRecords.isEmpty()) {
                    Long workingTime = 0L;
                    Long count = 0L;
                    while (a < assignmentLaborRecords.size()
                        && assignmentLaborRecords.get(a).getEndDate()
                            .compareTo(detail.getEndTime()) <= 0) {
                        // while we're still on assignmentLaborRecords that are
                        // for this Selection record
                        // aggregate
                        workingTime += assignmentLaborRecords.get(a)
                            .getDuration();
                        count += assignmentLaborRecords.get(a)
                            .getQuantityPicked();
                        a++;
                    }
                    detail.update(workingTime, count);

                } else {
                    detail.updateDownTime();
                }

            } else if (action == OperatorLaborActionType.CycleCounting) {
                // check for empty assignmentLaborRecords
                if (!cycleCountingLaborRecords.isEmpty()) {
                    Long workingTime = 0L;
                    Long count = 0L;
                    while (c < cycleCountingLaborRecords.size()
                        && cycleCountingLaborRecords.get(c).getEndDate()
                            .compareTo(detail.getEndTime()) <= 0) {
                        // while we're still on assignmentLaborRecords that are
                        // for this Cycle Counting record
                        // aggregate
                        workingTime += cycleCountingLaborRecords.get(c)
                            .getDuration();
                        count += cycleCountingLaborRecords.get(c)
                            .getLocationsCounted();
                        c++;
                    }
                    detail.update(workingTime, count);

                } else {
                    detail.updateDownTime();
                }

            } else if (action == OperatorLaborActionType.Replenishment) {
                // check for empty replenishmentRecords
                if (!replenishmentRecords.isEmpty()) {
                    Long workingTime = 0L;
                    Long count = 0L;
                    while (r < replenishmentRecords.size()
                        && replenishmentRecords.get(r).getEndDate()
                            .compareTo(detail.getEndTime()) <= 0) {
                        // while we're still on replenishmentRecords that are
                        // for this replenishment record
                        // aggregate
                        workingTime += replenishmentRecords.get(r)
                            .getDuration();
                        count++;
                        r++;
                    }
                    detail.update(workingTime, count);

                } else {
                    // set the down time
                    detail.updateDownTime();
                }
            } else if (action == OperatorLaborActionType.Loading) {
                // check for empty Loading records
                if (!loadingRecords.isEmpty()) {
                    Long workingTime = 0L;
                    Long count = 0L;
                    while (l < loadingRecords.size()
                        && loadingRecords.get(l).getEndDate()
                            .compareTo(detail.getEndTime()) <= 0) {
                        // while we're still on assignmentLaborRecords that are
                        // for this Labor record
                        // aggregate
                        workingTime += loadingRecords.get(l)
                            .getDuration();
                        count += loadingRecords.get(l)
                            .getContainersLoaded();
                        l++;
                    }
                    detail.update(workingTime, count);

                } else {
                    detail.updateDownTime();
                }

            }
        }
    }

    /**
     * @param operator - the operator
     * @param shiftStart - the shift start date
     * @param shiftEnd - the shift end date
     * @param displayShiftDate - the display shift date
     * @return List<ShiftDetailRec>
     */
    private List<ShiftDetailRec> getOperatorLaborRecords(Operator operator,
                                                         Date shiftStart,
                                                         Date shiftEnd, Date displayShiftDate) {
        List<ShiftDetailRec> detailRecList = new ArrayList<ShiftDetailRec>();
        try {
            List<Object[]> data = this.getOperatorLaborManager()
                .listLaborSummaryReportRecords(operator.getId(), shiftStart,
                    shiftEnd);

            for (Object[] objArray : data) {
                ShiftDetailRec detail = this.buildDetailRecord(operator,
                    shiftStart, shiftEnd, objArray, displayShiftDate);
                detailRecList.add(detail);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return detailRecList;
    }

    /**
     * @param operator - the operator
     * @param shiftStartDate - the shift start date
     * @param shiftEndDate - the shift end date
     * @param objArray - an object array that contains he fields to be added to
     *            the detail record.
     * @param displayShiftDate - the displayShiftDate
     * @return the newly created shift detail record.
     */
    private ShiftDetailRec buildDetailRecord(Operator operator,
                                             Date shiftStartDate,
                                             Date shiftEndDate,
 Object[] objArray, Date displayShiftDate) {
        // 0 = ActionType, 1 = RegionNumber, 2 = RegionGoalRate, 3 = StartTime,
        // 4 = EndTime, 5 = Duration
        ShiftDetailRec detailRec = new ShiftDetailRec();
        detailRec.setOperatorID(operator.getId());
        detailRec.setShiftDate(displayShiftDate);
        detailRec
                .setAction((OperatorLaborActionType) objArray[ACTION_TYPE_NDX]);
        if (detailRec.getAction() == OperatorLaborActionType.Selection) {
            detailRec.setRegionNumber((Integer) objArray[REGION_NUMBER_NDX]);
            detailRec.setGoalRate(objArray[GOAL_RATE_NDX].toString());
        } else if (detailRec.getAction() == OperatorLaborActionType.Replenishment) {
            try {
                detailRec
                        .setGoalRate(getAverageGoalRateByOperator(
                                operator.getId(),
                                OperatorLaborActionType.Replenishment));
            } catch (DataAccessException de) {
                de.printStackTrace();
            }
        } else if (detailRec.getAction() == OperatorLaborActionType.CycleCounting) {
            detailRec.setGoalRate(this.getCycleCountingGoalRate());
        } else if (detailRec.getAction() == OperatorLaborActionType.Loading) {
            try {
                detailRec.setGoalRate(getAverageGoalRateByOperator(
                        operator.getId(), OperatorLaborActionType.Loading));
            } catch (DataAccessException de) {
                de.printStackTrace();
            }

        }
        detailRec.setStartTime((Date) objArray[START_TIME_NDX]);
        if (objArray[END_TIME_NDX] != null) {
            detailRec.setEndTime((Date) objArray[END_TIME_NDX]);
            detailRec.setDuration((Long) objArray[DURATION_NDX]);
        }
        // initialize all time buckets and the count to 0L
        detailRec.setBreakTime(0L);
        detailRec.setDownTime(0L);
        detailRec.setNonWorkingTime(0L);
        detailRec.setWorkingTime(0L);
        detailRec.setCount(0L);

        return detailRec;
    }

    /**
     * @param operator - the operator
     * @param shiftStart - the shift start date
     * @param shiftEnd - the shift end date
     * @return List<ReplenishmentRecord>
     */
    private List<ReplenishmentRecord> getReplenishmentRecords(Operator operator,
                                                              Date shiftStart,
                                                              Date shiftEnd) {

        List<ReplenishmentRecord> replenishmentRecords = new ArrayList<ReplenishmentRecord>();

        try {
            List<Object[]> replenishments = this.getReplenishmentManager()
                .listLaborSummaryReportRecords(operator.getId(), shiftStart,
                    shiftEnd);
            for (Object[] replen : replenishments) {
                ReplenishmentRecord replenishmentRecord = new ReplenishmentRecord();
                replenishmentRecord.setStartDate((Date) replen[0]);
                replenishmentRecord.setEndDate((Date) replen[1]);
                replenishmentRecord.setDuration(replenishmentRecord
                    .getEndDate().getTime()
                    - replenishmentRecord.getStartDate().getTime());
                replenishmentRecords.add(replenishmentRecord);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return replenishmentRecords;
    }
    
    /**
     * @param operator - the operator
     * @param shiftStart - the shift start date
     * @param shiftEnd - the shift end date
     * @return List<LoadingRecord>
     */
    private List<LoadingRecord> getLoadingRecords(Operator operator,
                                                              Date shiftStart,
                                                              Date shiftEnd) {

        List<LoadingRecord> loadingRecords = new ArrayList<LoadingRecord>();
        List<Object[]> loadings;

        try {
            // get records from archive database
            loadings = this.archiveLoadingLaborManager
                .listLaborSummaryReportRecords(operator.getOperatorIdentifier(),
                    shiftStart, shiftEnd);
            for (Object[] load : loadings) {
                LoadingRecord loadingRecord = new LoadingRecord();
                loadingRecord.setStartDate((Date) load[0]);
                loadingRecord.setEndDate((Date) load[1]);
                loadingRecord.setDuration((Long) load[3]);
                loadingRecord.setContainersLoaded((Long) load[2]);
                
                loadingRecords.add(loadingRecord);
            }
            loadings = this.getLoadingRouteLaborManager()
                .listLaborSummaryReportRecords(operator.getId(), shiftStart,
                    shiftEnd);
            for (Object[] load : loadings) {
                LoadingRecord loadingRecord = new LoadingRecord();
                
                
                               
                loadingRecord.setStartDate((Date) load[0]);
                loadingRecord.setEndDate((Date) load[1]);
                loadingRecord.setDuration((Long) load[3]);
                loadingRecord.setContainersLoaded((Long) load[2]);
                
                loadingRecords.add(loadingRecord);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return loadingRecords;
    }

    /**
     * @param operator - the operator
     * @param shiftStart - the shift start date
     * @param shiftEnd - the shift end date
     * @return List<AssignmentLaborRecord>
     */
    private List<AssignmentLaborRecord> getAssignmentLaborRecords(Operator operator,
                                                                  Date shiftStart,
                                                                  Date shiftEnd) {

        List<AssignmentLaborRecord> assignmentLaborRecords = new ArrayList<AssignmentLaborRecord>();
        List<Object[]> dataObjects;
        try {
            // get records from archive database
            dataObjects = this.archiveAssignmentLaborManager
                .listLaborSummaryReportRecords(
                    operator.getOperatorIdentifier(), shiftStart, shiftEnd);
            for (Object[] objArray : dataObjects) {
                AssignmentLaborRecord alRec = new AssignmentLaborRecord();
                alRec.setDuration((Long) objArray[0]);
                alRec.setStartDate((Date) objArray[1]);
                alRec.setEndDate((Date) objArray[2]);
                alRec.setQuantityPicked((Long) objArray[3]);
                assignmentLaborRecords.add(alRec);
            }
            // get records from transactional database
            dataObjects = this.assignmentLaborManager
                .listLaborSummaryReportRecords(operator.getId(), shiftStart,
                    shiftEnd);
            for (Object[] objArray : dataObjects) {
                AssignmentLaborRecord alRec = new AssignmentLaborRecord();
                alRec.setDuration((Long) objArray[0]);
                alRec.setStartDate((Date) objArray[1]);
                alRec.setEndDate((Date) objArray[2]);
                alRec.setQuantityPicked((Long) objArray[3]);
                assignmentLaborRecords.add(alRec);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        if (!assignmentLaborRecords.isEmpty()) {
            Collections.sort(assignmentLaborRecords, orderByStartTime);
        }
        return assignmentLaborRecords;
    }

    /**
     * @param operator - the operator
     * @param shiftStart - the shift start date
     * @param shiftEnd - the shift end date
     * @return List<AssignmentLaborRecord>
     */
    private List<CycleCountingLaborRecord> getCycleCountingLaborRecords(Operator operator,
                                                                        Date shiftStart,
                                                                        Date shiftEnd) {

        List<CycleCountingLaborRecord> cycleCountingLaborRecords = new ArrayList<CycleCountingLaborRecord>();
        List<Object[]> dataObjects;
        try {
            // get records from archive database
            dataObjects = this.archiveCycleCountingLaborManager
                .listLaborSummaryReportRecords(operator.getOperatorIdentifier(),
                    shiftStart, shiftEnd);
            for (Object[] objArray : dataObjects) {
                CycleCountingLaborRecord cclr = new CycleCountingLaborRecord();
                cclr.setDuration((Long) objArray[0]);
                cclr.setStartDate((Date) objArray[1]);
                cclr.setEndDate((Date) objArray[2]);
                cclr.setLocationsCounted((Long) objArray[3]);
                cycleCountingLaborRecords.add(cclr);
            }
            // get records from transactional database
            dataObjects = this.cycleCountingLaborManager
                .listLaborSummaryReportRecords(operator.getId(), shiftStart,
                    shiftEnd);
            for (Object[] objArray : dataObjects) {
                CycleCountingLaborRecord laborRec = new CycleCountingLaborRecord();
                laborRec.setDuration((Long) objArray[0]);
                laborRec.setStartDate((Date) objArray[1]);
                laborRec.setEndDate((Date) objArray[2]);
                laborRec.setLocationsCounted((Long) objArray[3]);
                cycleCountingLaborRecords.add(laborRec);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        if (!cycleCountingLaborRecords.isEmpty()) {
            Collections.sort(cycleCountingLaborRecords, orderByStartTime);
        }
        return cycleCountingLaborRecords;
    }

    /**
     * This bean holds all of the data for a particular operator.
     */
    public class SummaryReportData {

        private String operatorID;

        private String operatorName;

        private String regionNumber;

        private Date startDate;

        private Date endDate;

        private Timestamp shiftStart;

        private Timestamp shiftEnd;

        private Integer shiftLength;

        private JRBeanCollectionDataSource details;

        /**
         * @return the operatorID
         */
        public String getOperatorID() {
            return operatorID;
        }

        /**
         * @param operatorID the operatorID to set
         */
        public void setOperatorID(String operatorID) {
            this.operatorID = operatorID;
        }

        /**
         * @return the operatorName
         */
        public String getOperatorName() {
            return operatorName;
        }

        /**
         * @param operatorName the operatorName to set
         */
        public void setOperatorName(String operatorName) {
            this.operatorName = operatorName;
        }

        /**
         * @return the regionNumber
         */
        public String getRegionNumber() {
            return regionNumber;
        }

        /**
         * @param regionNumber the regionNumber to set
         */
        public void setRegionNumber(String regionNumber) {
            this.regionNumber = regionNumber;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @param endDate the endDate to set
         */
        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @param startDate the startDate to set
         */
        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        /**
         * @return the shiftEnd
         */
        public Timestamp getShiftEnd() {
            return shiftEnd;
        }

        /**
         * @param shiftEnd the shiftEnd to set
         */
        public void setShiftEnd(Timestamp shiftEnd) {
            this.shiftEnd = shiftEnd;
        }

        /**
         * @return the shiftStart
         */
        public Timestamp getShiftStart() {
            return shiftStart;
        }

        /**
         * @param shiftStart the shiftStart to set
         */
        public void setShiftStart(Timestamp shiftStart) {
            this.shiftStart = shiftStart;
        }

        /**
         * @return the details
         */
        public JRBeanCollectionDataSource getDetails() {
            return details;
        }

        /**
         * @param details the details to set
         */
        public void setDetails(JRBeanCollectionDataSource details) {
            this.details = details;
        }

        /**
         * @param shiftLength shift length
         */
        public void setShiftLength(Integer shiftLength) {
            this.shiftLength = shiftLength;
        }

        /**
         * @return shift length
         */
        public Integer getShiftLength() {
            return shiftLength;
        }
    }

    /**
     * This bean contains the data for one detail line for a particular
     * operator.
     */
    public class ShiftDetailRec {

        private Long operatorID;

        private Date shiftDate;

        private Date startTime;

        private Date endTime;

        private OperatorLaborActionType action;

        private Integer regionNumber;

        private Long duration;

        private Long downTime = -1L;

        private Long workingTime = -1L;

        private Long breakTime = -1L;

        private Long nonWorkingTime = -1L;

        private Long count = -1L;

        private String rate = "-";

        private String goalRate = "-";

        private JRBeanCollectionDataSource totals;

        /**
         * @param timeWorked total working time
         * @param recordCount total number of records
         */
        private void update(Long timeWorked, Long recordCount) {
            this.setWorkingTime(timeWorked);
            this.setDownTime(this.getDuration() - this.getWorkingTime());
            this.setCount(recordCount);
            if (this.getDuration() > 0) {
                Double laborRate = new Double(this.getCount()
                    / (this.getDuration() / MILLISEC_IN_AN_HOUR));
                this.setRate(DECIMAL_FORMAT.format(laborRate));
            }
        }

        /**
         * @param detail
         */
        public void updateDownTime() {
            this.setWorkingTime(0L);
            this.setDownTime(this.getDuration());
            this.setCount(0L);
            this.setRate("0.00");
        }

        /**
         * @return the operatorID
         */
        public Long getOperatorID() {
            return operatorID;
        }

        /**
         * @param operatorID the operatorID to set
         */
        public void setOperatorID(Long operatorID) {
            this.operatorID = operatorID;
        }

        /**
         * @return the action
         */
        public OperatorLaborActionType getAction() {
            return action;
        }

        /**
         * @param action the action to set
         */
        public void setAction(OperatorLaborActionType action) {
            this.action = action;
        }

        /**
         * @return the breakTime
         */
        public Long getBreakTime() {
            return breakTime;
        }

        /**
         * @param breakTime the breakTime to set
         */
        public void setBreakTime(Long breakTime) {
            this.breakTime = breakTime;
        }

        /**
         * @return the downTime
         */
        public Long getDownTime() {
            return downTime;
        }

        /**
         * @param downTime the downTime to set
         */
        public void setDownTime(Long downTime) {
            this.downTime = downTime;
        }

        /**
         * @return the goalRate
         */
        public String getGoalRate() {
            return goalRate;
        }

        /**
         * @param goalRate the goalRate to set
         */
        public void setGoalRate(String goalRate) {
            this.goalRate = goalRate;
        }

        /**
         * @return the nonWorkingTime
         */
        public Long getNonWorkingTime() {
            return nonWorkingTime;
        }

        /**
         * @param nonWorkingTime the nonWorkingTime to set
         */
        public void setNonWorkingTime(Long nonWorkingTime) {
            this.nonWorkingTime = nonWorkingTime;
        }

        /**
         * @return the rate
         */
        public String getRate() {
            return rate;
        }

        /**
         * @param rate the rate to set
         */
        public void setRate(String rate) {
            this.rate = rate;
        }

        /**
         * @return the regionNumber
         */
        public Integer getRegionNumber() {
            return regionNumber;
        }

        /**
         * @param regionNumber the regionNumber to set
         */
        public void setRegionNumber(Integer regionNumber) {
            this.regionNumber = regionNumber;
        }

        /**
         * @return the shiftDate
         */
        public Date getShiftDate() {
            return shiftDate;
        }

        /**
         * @param shiftDate the shiftDate to set
         */
        public void setShiftDate(Date shiftDate) {
            this.shiftDate = shiftDate;
        }

        /**
         * @return the totals
         */
        public JRBeanCollectionDataSource getTotals() {
            return totals;
        }

        /**
         * @param totals the totals to set
         */
        public void setTotals(JRBeanCollectionDataSource totals) {
            this.totals = totals;
        }

        /**
         * @return the workingTime
         */
        public Long getWorkingTime() {
            return workingTime;
        }

        /**
         * @param workingTime the workingTime to set
         */
        public void setWorkingTime(Long workingTime) {
            this.workingTime = workingTime;
        }

        /**
         * @return the count
         */
        public Long getCount() {
            return count;
        }

        /**
         * @param count the count to set
         */
        public void setCount(Long count) {
            this.count = count;
        }

        /**
         * @return the endTime
         */
        public Date getEndTime() {
            return endTime;
        }

        /**
         * @param endTime the endTime to set
         */
        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        /**
         * @return the startTime
         */
        public Date getStartTime() {
            return startTime;
        }

        /**
         * @param startTime the startTime to set
         */
        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        /**
         * @return the duration
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the duration to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }
    }

    /**
     * 
     * This bean contains the startdate, enddata and duration for the assignment
     * labor records.
     * 
     */
    private class AssignmentLaborRecord {

        private Date startDate;

        private Date endDate;

        private Long duration;

        private Long quantityPicked;

        /**
         * @return the quantityPicked
         */
        public Long getQuantityPicked() {
            return quantityPicked;
        }

        /**
         * @param quantityPicked the quantityPicked to set
         */
        public void setQuantityPicked(Long quantityPicked) {
            this.quantityPicked = quantityPicked;
        }

        /**
         * @return the duration
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the duration to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @param endDate the endDate to set
         */
        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @param startDate the startDate to set
         */
        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }
    }

    /**
     * 
     * This bean contains the startdate, enddata and duration for the cycle
     * counting labor records.
     * 
     */
    private class CycleCountingLaborRecord {

        private Date startDate;

        private Date endDate;

        private Long duration;

        private Long locationsCounted;

        /**
         * @return the locationsCounted
         */
        public Long getLocationsCounted() {
            return locationsCounted;
        }

        /**
         * @param locationsCounted the quantityPicked to set
         */
        public void setLocationsCounted(Long locationsCounted) {
            this.locationsCounted = locationsCounted;
        }

        /**
         * @return the duration
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the duration to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @param endDate the endDate to set
         */
        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @param startDate the startDate to set
         */
        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }
    }

    /**
     * 
     * This bean contains the startdate, enddata and duration for the assignment
     * labor records.
     * 
     */
    private class ReplenishmentRecord {

        private Date startDate;

        private Date endDate;

        private Long duration;

        /**
         * @return the duration
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the duration to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @param endDate the endDate to set
         */
        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @param startDate the startDate to set
         */
        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

    }
    
    /**
     * 
     * This bean contains the startdate, enddata and duration for the Loading
     * labor records.
     * 
     */
    private class LoadingRecord {

        private Date startDate;

        private Date endDate;

        private Long duration;
        
        private Long containersLoaded;

     

        /**
         * @return the containersLoaded
         */
        public Long getContainersLoaded() {
            return containersLoaded;
        }

        /**
         * @param containersLoaded
         */
        public void setContainersLoaded(Long containersLoaded) {
            this.containersLoaded = containersLoaded;
        }

        /**
         * @return the duration
         */
        public Long getDuration() {
            return duration;
        }

        /**
         * @param duration the duration to set
         */
        public void setDuration(Long duration) {
            this.duration = duration;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @param endDate the endDate to set
         */
        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @param startDate the startDate to set
         */
        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    public JRDataSource create(JasperReport arg0) throws JRException {

        List<SummaryReportData> reportRecordList = new ArrayList<SummaryReportData>();

        // create a report record
        SummaryReportData reportRecord = new SummaryReportData();
        reportRecord.setOperatorName("Joe Operator");

        // add the report record to the summary data list
        reportRecordList.add(reportRecord);
        return new JRBeanCollectionDataSource(reportRecordList);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.impl.LaborShiftSummaryReportRoot
     * #dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    public void dispose(JRDataSource arg0) throws JRException {
        // do nothing

    }

    /**
     * 
     * @param beanClass - object type returned by data provider
     */
    public LaborShiftSummaryReportImplRoot(Class<?> beanClass) {
        super(SummaryReportData.class);
    }

    /**
     * constructor.
     * 
     */
    public LaborShiftSummaryReportImplRoot() {
        super(SummaryReportData.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getOperatorLaborManager()
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setOperatorLaborManager(com.vocollect.voicelink.core.service.OperatorLaborManager)
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getOperatorManager()
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setOperatorManager(com.vocollect.voicelink.core.service.OperatorManager)
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getArchiveAssignmentLaborManager()
     */
    public ArchiveAssignmentLaborManager getArchiveAssignmentLaborManager() {
        return archiveAssignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setArchiveAssignmentLaborManager(com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager)
     */
    public void setArchiveAssignmentLaborManager(ArchiveAssignmentLaborManager archiveAssignmentLaborManager) {
        this.archiveAssignmentLaborManager = archiveAssignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getAssignmentLaborManager()
     */
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setAssignmentLaborManager(com.vocollect.voicelink.selection.service.AssignmentLaborManager)
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getReplenishmentManager()
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setReplenishmentManager(com.vocollect.voicelink.replenishment.service.ReplenishmentManager)
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getReplenishmentGoalRate()
     */
    public String getReplenishmentGoalRate() {
        return replenishmentGoalRate;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setReplenishmentGoalRate(java.lang.String)
     */
    public void setReplenishmentGoalRate(String replenishmentGoalRate) {
        this.replenishmentGoalRate = replenishmentGoalRate;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#getRegionManager()
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborShiftSummaryReportRoot#setRegionManager(com.vocollect.voicelink.core.service.RegionManager)
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * @return the cycleCountingLaborManager
     */
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager the cycleCountingLaborManager to set
     */
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * @return the archiveCycleCountingLaborManager
     */
    public ArchiveCycleCountingLaborManager getArchiveCycleCountingLaborManager() {
        return archiveCycleCountingLaborManager;
    }

    /**
     * @param archiveCycleCountingLaborManager the
     *            archiveCycleCountingLaborManager to set
     */
    public void setArchiveCycleCountingLaborManager(ArchiveCycleCountingLaborManager archiveCycleCountingLaborManager) {
        this.archiveCycleCountingLaborManager = archiveCycleCountingLaborManager;
    }
    
    
    /**
     * @return the loadingRouteLaborManager
     */
    public LoadingRouteLaborManager getLoadingRouteLaborManager() {
        return loadingRouteLaborManager;
    }

    /**
     * @param loadingRouteLaborManager the loadingRouteLaborManager to set
     */
    public void setLoadingRouteLaborManager(
            LoadingRouteLaborManager loadingRouteLaborManager) {
        this.loadingRouteLaborManager = loadingRouteLaborManager;
    }

    /**
     * @return the cycleCountingGoalRate
     */
    public String getCycleCountingGoalRate() {
        return cycleCountingGoalRate;
    }

    /**
     * @param cycleCountingGoalRate the cycleCountingGoalRate to set
     */
    public void setCycleCountingGoalRate(String cycleCountingGoalRate) {
        this.cycleCountingGoalRate = cycleCountingGoalRate;
    }
    
    /**
     * @return loadingGoalRate
     */
    public String getLoadingGoalRate() {
        return loadingGoalRate;
    }

    /**
     * @param loadingGoalRate the loadingGoalRate to set
     */
    public void setLoadingGoalRate(String loadingGoalRate) {
        this.loadingGoalRate = loadingGoalRate;
    }
    
    /**
     * @return the archiveLoadingLaborManager
     */
    public ArchiveLoadingLaborManager getArchiveLoadingLaborManager() {
        return archiveLoadingLaborManager;
    }

    /**
     * @param archiveLoadingLaborManager the archiveLoadingLaborManager to set
     */
    public void setArchiveLoadingLaborManager(
            ArchiveLoadingLaborManager archiveLoadingLaborManager) {
        this.archiveLoadingLaborManager = archiveLoadingLaborManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 