/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.errors.ReportErrorRoot;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.ReportPrinterMap;
import com.vocollect.voicelink.core.service.ReportPrinterManagerRoot;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.PrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.query.JRHibernateQueryExecuterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * {@inheritDoc}
 * @see com.voccom.vocollect.voicelink.lineloading.service.ReportPrinterManagerRoot
 * 
 * @author bnorthrop
 */
public class ReportPrinterManagerImplRoot implements ReportPrinterManagerRoot {

    // DATA MEMBERS
    /**
     * The mapping of printer numbers to the network addresses of printers -
     * defined in the applicationContext-reportPrinters.xml file.
     */
    private ReportPrinterMap reportPrinterMap;

    /**
     * The hibernate session factory, used to load the report data (injected
     * from Spring). Use this to get the current session (not to open a new
     * one).
     */
    private SessionFactory sessionFactory;

    /**
     * The default root in the filesystem (from the classpath) to the reports.
     */
    public static final String REPORT_ROOT = "reports/voicelink/";

    /**
     * Custom logger.
     */
    private static final Logger LOG = new Logger(
        ReportPrinterManagerImplRoot.class);

    /**
     * This is the core method used to print a report directly from a task.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.ReportPrinterManagerRoot#printReport(java.lang.String,
     *      int)
     */
    public void printReport(String reportName,
                            String printerNumber,
                            Map<String, String> parameterValues)
        throws VocollectException {

        String printerAddress = reportPrinterMap.getPrinter(printerNumber);

        JasperDesign reportTemplate = findReport(reportName);

        JasperReport compiledReport = compileReport(reportTemplate);

        JRParameter[] parameterArray = reportTemplate.getParameters();

        Map<String, JRParameter> parameterMap = buildParameterMap(parameterArray);

        Map<String, Object> values = loadParameterValues(parameterMap, parameterValues);

        printReport(printerAddress, compiledReport, values);

    }

    /**
     * Print the Jasper report, given the parameters.
     * 
     * @param printerAddress - the network address of the printer.
     * @param compiledReport - the compiled Jasper report template.
     * @param values - the parameter values to fill the report.
     * 
     * @throws VocollectException - if couldn't load or print.
     */
    private void printReport(String printerAddress,
                             JasperReport compiledReport,
                             Map<String, Object> values)
        throws VocollectException {
        
        Session session = getSessionFactory().getCurrentSession();
        values.put(JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION, session);

        JasperPrint reportResult = fillReport(compiledReport, values, session);

        printReport(printerAddress, reportResult);

    }

    /**
     * By default, use the HTML rendering of the report.
     * @param printerAddress - the network address of the printer to be used.
     * @param reportResult - the report with parameters already filled in.
     * @throws VocollectException - in case the print is not successful.
     */
    public void printReport(String printerAddress, JasperPrint reportResult)
        throws VocollectException {
        JRAbstractExporter exporter = null;

        try {

            exporter = new JRHtmlExporter();
            // Tell Jasper to not use empty images to space the report;
            // we would need a place to store these image files.
            exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, Boolean.FALSE);
            exporter.setParameter(JRHtmlExporterParameter.IMAGES_URI, "image?image=");
            exporter = new JRPrintServiceExporter();

            PrintServiceAttributeSet serviceAttributeSet = new HashPrintServiceAttributeSet();
            serviceAttributeSet.add(new PrinterName(printerAddress, null));

            exporter.setParameter(JRExporterParameter.JASPER_PRINT, reportResult);

            exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET,
                                  serviceAttributeSet);

            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);

            exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);

            exporter.exportReport();

        } catch (JRException ex) {
            ReportErrorRoot errorCode;
            if (ex.getMessage().contains("No suitable print service found.")) {
                errorCode = ReportError.JASPER_PRINTSERVICE_EXCEPTION;
            } else {
                errorCode = ReportError.JASPER_EXPORT_ERROR;
            } 
            LOG.error(ex.getMessage(), errorCode);
            throw new VocollectException(errorCode, ex);
        }
    }

    /**
     * Fill the report with the parameters assembled.
     * @param compiledReport - the jrxml file for the report.
     * @param values - the values of the parameters
     * @param session - the hibernate session for
     * @return JasperReport - the report.
     * @throws VocollectException - if there is an exception filling the report.
     */
    private JasperPrint fillReport(JasperReport compiledReport,
                                   Map<String, Object> values,
                                   Session session) throws VocollectException {
        // Pass in the parameters, and fill the report
        JasperPrint reportResult = null;
        try {
            reportResult = JasperFillManager.fillReport(compiledReport, values);
        } catch (JRException ex) {
            LOG.error(ex.getMessage(), ReportError.JASPER_FILL_ERROR);
            throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
        }
        return reportResult;
    }

    /**
     * Compile the Jasper report, given the template.
     * @param reportTemplate - the template of the report to be compiled.
     * @throws VocollectException if compilation does not succeed.
     * @return JasperReport - the compiled report.
     */
    private JasperReport compileReport(JasperDesign reportTemplate)
        throws VocollectException {
        try {
            return JasperCompileManager.compileReport(reportTemplate);
        } catch (JRException ex) {
            LOG.error(ex.getMessage(), ReportError.JASPER_COMPILE_ERROR);
            throw new VocollectException(ReportError.JASPER_COMPILE_ERROR, ex);
        }
    }

    /**
     * Helper function to put the JR parameters into the member variable Map.
     * This allows us constant time lookups while we iterate through the
     * database parameters.
     * @param parameterArray The array to convert
     * @return Map - parameterMap
     */
    private Map<String, JRParameter> buildParameterMap(JRParameter[] parameterArray) {
        // Create an empty hashmap
        Map<String, JRParameter> parameterMap = new HashMap<String, JRParameter>();
        if (parameterArray == null) {
            return parameterMap;
        }

        // For each array parameter, insert into the map
        for (int i = 0; i < parameterArray.length; i++) {
            JRParameter param = parameterArray[i];
            parameterMap.put(param.getName(), param);
        }

        return parameterMap;
    }

    /**
     * This function extracts the report path from the URL and verifies its
     * existence by attempting to open it. If successful, it will also save
     * parameters related to the report file itself.
     * 
     * @param reportName the name/address of the report.
     * @return boolean true implies report was found, otherwise false.
     * @throws VocollectException if the report could not be opened
     */
    private JasperDesign findReport(String reportName)
        throws VocollectException {

        JasperDesign source = null;
        try {
            LOG.debug("Loading report: " + REPORT_ROOT + reportName);
            source = JRXmlLoader.load(getClass().getClassLoader()
                .getResourceAsStream(REPORT_ROOT + reportName));

        } catch (JRException ex) {
            if (ex.getCause() instanceof FileNotFoundException) {
                LOG.error(ex.getMessage(), CoreErrorCode.REPORT_NOT_FOUND);
                throw new VocollectException(CoreErrorCode.REPORT_NOT_FOUND);
            } else {
                LOG.error(ex.getMessage(), ReportError.JASPER_LOAD_ERROR);
                throw new VocollectException(ReportError.JASPER_LOAD_ERROR, ex);
            }
        }

        return source;
    }

    /**
     * Get all the values to pass into the report. This will examine the report
     * parameters from the POST data from the form submit.
     * @param parameterMap - the parameters from the jasper template.
     * @param parameterValues - map of the parameter values.
     * @return Map - map of parameter values for the report
     */
    private Map<String, Object> loadParameterValues(Map<String, JRParameter> parameterMap,
                                                    Map<String, String> parameterValues) {

        Map<String, Object> values = new HashMap<String, Object>();
        for (String paramName : parameterMap.keySet()) {
            JRParameter parameter = parameterMap.get(paramName);

            // String value = servletRequest.getParameter(paramName);
            String value = ReportUtilities.getParameterStringValue(
                paramName, parameterValues);

            if (value == "" && parameter.getValueClass().equals(String.class)) {
                // Treat empty strings as null; don't pass them in. This
                // will force the report to use the default value.
                continue;
            }

            // Convert the String value into an object value
            values.put(paramName, ReportUtilities.getParameterValue(
                parameter, value));
        }

        return values;
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Getter for the printerMap property.
     * @return ReportPrinterMap value of the property
     */
    public ReportPrinterMap getReportPrinterMap() {
        return this.reportPrinterMap;
    }

    /**
     * Setter for the printerMap property.
     * @param reportPrinterMap the new printerMap value
     */
    public void setReportPrinterMap(ReportPrinterMap reportPrinterMap) {
        this.reportPrinterMap = reportPrinterMap;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 