/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.BreakTypeDAO;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.service.BreakTypeManager;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;


/**
 * Additional service methods for the <code>BreakType</code> model object.
 *
 * @author Ed Stoll
 */
public abstract class BreakTypeManagerImplRoot extends
    GenericManagerImpl<BreakType, BreakTypeDAO> implements BreakTypeManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public BreakTypeManagerImplRoot(BreakTypeDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.BreakTypeManager#listAll()
     */
    public List<BreakType> listAllOrderByNumber() throws DataAccessException {
        return getPrimaryDAO().listAllOrderByNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.BreakTypeManager#findByNumber(int)
     */
    public BreakType findByNumber(int breakNumber) throws DataAccessException {
        return getPrimaryDAO().findByNumber(breakNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        BreakType breakType = get(id);
        return delete(breakType);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(BreakType instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("breakType.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
     }
 
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 