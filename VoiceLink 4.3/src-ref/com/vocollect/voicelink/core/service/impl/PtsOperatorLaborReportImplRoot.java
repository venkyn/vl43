/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.puttostore.model.PtsOperatorLaborReport;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Implementation class for Put To Store Labor Report.
 * 
 * @author mlashinsky
 */
public class PtsOperatorLaborReportImplRoot extends
    JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {

    private OperatorLaborManager operatorLaborManager;

    /**
     * Constructor.
     * @param beanClass Report Class that the report is based on
     */
    public PtsOperatorLaborReportImplRoot(Class<PtsOperatorLaborReport> beanClass) {
        super(PtsOperatorLaborReport.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.ReportDataSourceManagerRoot#getDataSource(java.util.Map)
     */
    @Override
    public JRDataSource getDataSource(Map<String, Object> values)
        throws Exception {
        // Parameter values
        Date startDate = (Date) values.get("START_DATE");
        Date endDate = (Date) values.get("END_DATE");
        String regionNumber = (String) values.get("REGION_NUMBER");
        @SuppressWarnings("unchecked")
        Set<String> operatorIdentifiers = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            operatorIdentifiers = null;
        }

        List<PtsOperatorLaborReport> ptsOperatorLaborRecords = getOperatorLaborManager()
            .listLaborRecordsForPtsOperatorLaborReport(new QueryDecorator(),
                regionNumber, operatorIdentifiers, startDate, endDate);
        return new JRBeanCollectionDataSource(ptsOperatorLaborRecords);
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#create(net.sf.jasperreports.engine.JasperReport)
     */
    @Override
    public JRDataSource create(JasperReport arg0) throws JRException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     * @see net.sf.jasperreports.engine.JRDataSourceProvider#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    @Override
    public void dispose(JRDataSource arg0) throws JRException {
        // TODO Auto-generated method stub

    }

    /**
     * Setter for the laborManager property.
     * @param operatorLaborManager the new laborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 