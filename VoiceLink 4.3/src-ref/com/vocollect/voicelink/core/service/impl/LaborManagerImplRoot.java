/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl; 

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonDetail;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseDetail;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.putaway.service.LicenseDetailManager;
import com.vocollect.voicelink.putaway.service.LicenseManager;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.puttostore.service.PtsPutDetailManager;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import java.util.Date;
import java.util.List;

/**
 * Additional service methods for Labor Management.
 * 
 * @author pfunyak
 */

public abstract class LaborManagerImplRoot implements LaborManager {

    private static final Double ONE_HUNDRED = 100.00;

    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.00;

    private AssignmentLaborManager assignmentLaborManager;

    private OperatorLaborManager operatorLaborManager;

    private PutawayRegionManager putawayRegionManager;

    private ReplenishmentRegionManager replenishmentRegionManager;

    private CycleCountingRegionManager cycleCountingRegionManager;

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private LicenseManager licenseManager;

    private ReplenishmentManager replenishmentManager;

    private LicenseDetailManager licenseDetailManager;

    private ReplenishmentDetailManager replenishmentDetailManager;

    private CartonDetailManager cartonDetailManager;

    private PickDetailManager pickDetailManager;

    private PtsLicenseLaborManager ptsLicenseLaborManager;

    private PtsPutDetailManager ptsPutDetailManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    private LoadingRouteLaborManager loadingLaborManager;

    private LoadingContainerManager loadingContainerManager;

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManager#getAssignmentLaborManager()
     */
    @Override
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }

    /**
     * Setter for the assignmentLaborManager property.
     * 
     * @param assignmentLaborManager the new assignmentLaborManager value
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * 
     * @return OperatorLaborManager value of the property
     */
    @Override
    public OperatorLaborManager getOperatorLaborManager() {
        return this.operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * 
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * Getter for the putawayRegionManager property.
     * 
     * @return PutawayRegionManager value of the property
     */
    public PutawayRegionManager getPutawayRegionManager() {
        return this.putawayRegionManager;
    }

    /**
     * Setter for the putawayRegionManager property.
     * 
     * @param putawayRegionManager the new putawayRegionManager value
     */
    public void setPutawayRegionManager(PutawayRegionManager putawayRegionManager) {
        this.putawayRegionManager = putawayRegionManager;
    }

    /**
     * Getter for the licenseManager property.
     * 
     * @return LicenseManager value of the property
     */
    public LicenseManager getLicenseManager() {
        return this.licenseManager;
    }

    /**
     * Setter for the licenseManager property.
     * 
     * @param licenseManager the new licenseManager value
     */
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }

    /**
     * Getter for the licenseDetailManager property.
     * 
     * @return LicenseDetailManager value of the property
     */
    public LicenseDetailManager getLicenseDetailManager() {
        return this.licenseDetailManager;
    }

    /**
     * Setter for the licenseDetailManager property.
     * 
     * @param licenseDetailManager the new licenseDetailManager value
     */
    public void setLicenseDetailManager(LicenseDetailManager licenseDetailManager) {
        this.licenseDetailManager = licenseDetailManager;
    }

    /**
     * Getter for the replenishmentRegionManager property.
     * 
     * @return ReplenishmentRegionManager value of the property
     */
    public ReplenishmentRegionManager getReplenishmentRegionManager() {
        return this.replenishmentRegionManager;
    }

    /**
     * Setter for the replenishmentRegionManager property.
     * 
     * @param replenishmentRegionManager the new replenishmentRegionManager
     *            value
     */
    public void setReplenishmentRegionManager(ReplenishmentRegionManager replenishmentRegionManager) {
        this.replenishmentRegionManager = replenishmentRegionManager;
    }

    /**
     * @return the cycleCountingRegionManager
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * @param cycleCountingRegionManager the cycleCountingRegionManager to set
     */
    public void setCycleCountingRegionManager(CycleCountingRegionManager cycleCountingRegionManager) {
        this.cycleCountingRegionManager = cycleCountingRegionManager;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager the cycleCountingAssignmentManager
     *            to set
     */
    public void setCycleCountingAssignmentManager(CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * Getter for the replenishmentManager property.
     * 
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return this.replenishmentManager;
    }

    /**
     * Setter for the replenishmentManager property.
     * 
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * Getter for the replenishmentDetailManager property.
     * 
     * @return ReplenishmentDetailManager value of the property
     */
    public ReplenishmentDetailManager getReplenishmentDetailManager() {
        return this.replenishmentDetailManager;
    }

    /**
     * Setter for the replenishmentDetailManager property.
     * 
     * @param replenishmentDetailManager the new replenishmentDetailManager
     *            value
     */
    public void setReplenishmentDetailManager(ReplenishmentDetailManager replenishmentDetailManager) {
        this.replenishmentDetailManager = replenishmentDetailManager;
    }

    /**
     * Getter for the cartonDetailManager property.
     * 
     * @return CartonDetailManager value of the property
     */
    public CartonDetailManager getCartonDetailManager() {
        return this.cartonDetailManager;
    }

    /**
     * Setter for the cartonDetailManager property.
     * 
     * @param cartonDetailManager the new cartonDetailManager value
     */
    public void setCartonDetailManager(CartonDetailManager cartonDetailManager) {
        this.cartonDetailManager = cartonDetailManager;
    }

    /**
     * Getter for the pickDetailManager property.
     * 
     * @return PickDetailManager value of the property
     */
    public PickDetailManager getPickDetailManager() {
        return this.pickDetailManager;
    }

    /**
     * Setter for the pickDetailManager property.
     * 
     * @param pickDetailManager the new pickDetailManager value
     */
    public void setPickDetailManager(PickDetailManager pickDetailManager) {
        this.pickDetailManager = pickDetailManager;
    }

    /**
     * Getter for the ptsPutDetailManager property.
     * 
     * @return PtsPutDetailManager value of the property
     */
    public PtsPutDetailManager getPtsPutDetailManager() {
        return ptsPutDetailManager;
    }

    /**
     * Setter for the ptsPutDetailManager property.
     * 
     * @param ptsPutDetailManager the new ptsPutDetailManager value
     */
    public void setPtsPutDetailManager(PtsPutDetailManager ptsPutDetailManager) {
        this.ptsPutDetailManager = ptsPutDetailManager;
    }

    /**
     * @return the cycleCountingLaborManager
     */
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager the cycleCountingLaborManager to set
     */
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * @return the loadingLaborManager
     */
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }

    /**
     * @param loadingLaborManager the loadingLaborManager to set
     */
    public void setLoadingLaborManager(LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    /**
     * Getter for the loadingContainerManager property.
     * @return LoadingContainerManager value of the property
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * Setter for the loadingContainerManager property.
     * @param loadingContainerManager the new loadingContainerManager value
     */
    public void setLoadingContainerManager(LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#openOperatorLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType)
     */
    @Override
    public void openOperatorLaborRecord(Date startTime,
                                        Operator operator,
                                        OperatorLaborActionType actionType)
        throws DataAccessException, BusinessRuleException,
        IllegalArgumentException {

        OperatorLaborManager olManager = this.getOperatorLaborManager();
        OperatorLabor olClosedRecord = olManager.closeLaborRecord(startTime,
            operator);

        // if closed labor record was a selection labor record, close assignment
        // labor records
        this.getOperatorLaborManager().openLaborRecord(startTime, operator,
            actionType);
        if (olClosedRecord == null) {
            return;
        }

        // if selection labor record was closed, close assignment labor records.
        OperatorLaborActionType closedRecordActionType = olClosedRecord
            .getActionType();
        // FIXME this really needs to be refactored and would be best served
        // with a refactor to Fork Apps and LL labor
        if (closedRecordActionType == OperatorLaborActionType.Selection) {
            OperatorFunctionLabor sLabor = (OperatorFunctionLabor) olClosedRecord;
            AssignmentLaborManager alManager = this.getAssignmentLaborManager();
            List<AssignmentLabor> alRecords = alManager
                .listOpenRecordsByOperatorId(operator.getId());
            // Close the assignment labor records.
            for (AssignmentLabor al : alRecords) {
                this.getAssignmentLaborManager().closeLaborRecord(startTime,
                    al.getAssignment());
                sLabor.setCount(sLabor.getCount() + al.getQuantityPicked());
            }
            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);
            // if we closed a function labor record, we need to recalculate the
            // actual rate
            // and the percent of goal.
        } else if (closedRecordActionType
            .equals(OperatorLaborActionType.PutToStore)) {
            OperatorFunctionLabor ptsLabor = (OperatorFunctionLabor) olClosedRecord;
            PtsLicenseLaborManager pllManager = this
                .getPtsLicenseLaborManager();
            List<PtsLicenseLabor> pllRecords = pllManager
                .listOpenRecordsByOperatorId(operator.getId());
            // Close the assignment labor records.
            for (PtsLicenseLabor pll : pllRecords) {
                this.getPtsLicenseLaborManager().closeLaborRecord(startTime,
                    pll.getLicense());
                ptsLabor.setCount(ptsLabor.getCount() + pll.getQuantityPut());
            }
            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);
            // if we closed a function labor record, we need to recalculate the
            // actual rate
            // and the percent of goal.
        } else if ((closedRecordActionType == OperatorLaborActionType.Replenishment)
            || (closedRecordActionType == OperatorLaborActionType.PutAway)
            || (closedRecordActionType == OperatorLaborActionType.LineLoading)) {

            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);

        } else if (closedRecordActionType == OperatorLaborActionType.CycleCounting) {
            OperatorFunctionLabor sLabor = (OperatorFunctionLabor) olClosedRecord;
            CycleCountingLaborManager cclManager = this
                .getCycleCountingLaborManager();
            List<CycleCountingLabor> cycleCountingLaborRecords = cclManager
            // TODO should this be getId or getOperatorIdentifier?
                .listOpenRecordsByOperatorId(operator.getId());
            // Close the cc assignment labor records.
            for (CycleCountingLabor ccl : cycleCountingLaborRecords) {
                this.getCycleCountingLaborManager().closeLaborRecord(startTime,
                    ccl.getAssignment());
                sLabor.setCount(sLabor.getCount() + ccl.getLocationsCounted());
            }
            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);
            // if we closed a function labor record, we need to recalculate the
            // actual rate
        } else if (closedRecordActionType == OperatorLaborActionType.Loading) {
            closeLoadingLaborRecord(startTime, operator);
            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#openBreakLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.BreakType)
     */
    @Override
    public void openBreakLaborRecord(Date startTime,
                                     Operator operator,
                                     BreakType breakType)
        throws DataAccessException, BusinessRuleException {

        OperatorLaborManager olManager = this.getOperatorLaborManager();

        // START VLINK-3585
        // Verify that we do not already have a break labor record open.
        // If we do, ignore this request.
        OperatorLabor currentLaborRecord = olManager
            .findOpenRecordByOperatorId(operator.getId());
        if (currentLaborRecord.getActionType() == OperatorLaborActionType.Break) {
            return;
        }
        // END VLINK-3585
        // Close the currently opened labor record
        OperatorLabor olClosedRecord = olManager.closeLaborRecord(startTime,
            operator);
        // create a break labor record.
        olManager.openBreakLaborRecord(startTime, operator, breakType,
            olClosedRecord);
        OperatorBreakLabor breakLabor = (OperatorBreakLabor) olManager
            .findOpenRecordByOperatorId(operator.getId());
        OperatorLaborActionType olClosedRecordActionType = olClosedRecord
            .getActionType();
        // if we closed a selection labor record, close the assignment labor
        // records and associate
        // the assignment labor records with the break labor record so we can
        // open new assignment
        // labor records when the operator returns from their break.
        if (olClosedRecord.getActionType() == OperatorLaborActionType.Selection) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olClosedRecord;
            this.closeAssignmentLaborRecordsForBreak(startTime, operator,
                flRec, breakLabor);
            // if we closed another type of function labor record, we need to
            // recalculate
            // the actual rate and the percent of goal.
        } else if ((olClosedRecordActionType == OperatorLaborActionType.PutAway)
            || (olClosedRecordActionType == OperatorLaborActionType.Replenishment)
            || (olClosedRecordActionType == OperatorLaborActionType.LineLoading)) {

            this.calculateAggregateValues((OperatorFunctionLabor) olClosedRecord);
        } else if (olClosedRecord.getActionType() == OperatorLaborActionType.PutToStore) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olClosedRecord;
            this.closePtsLicenseLaborRecordsForBreak(startTime, operator,
                flRec, breakLabor);
        } else if (olClosedRecord.getActionType() == OperatorLaborActionType.CycleCounting) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olClosedRecord;
            this.closeCycleCountingLaborRecordsForBreak(startTime, operator,
                flRec, breakLabor);
        } else if (olClosedRecord.getActionType() == OperatorLaborActionType.Loading) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olClosedRecord;
            this.closeLoadingLaborRecordsForBreak(startTime, operator, flRec,
                breakLabor);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManager#closeOperatorBreakLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public void closeBreakLaborRecord(Date time, Operator operator)
        throws DataAccessException, BusinessRuleException {

        // close the open break labor record.
        OperatorLaborManager olManager = this.getOperatorLaborManager();
        OperatorBreakLabor breakLabor = olManager.closeBreakLaborRecord(time,
            operator);

        // if the previously opened operator labor contained in the break record
        // was a selection record,
        // open a new selection labor record and any assignment labor records
        // that need to be opened.
        if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.Selection) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.Selection);
            OperatorLabor selectionLabor = olManager
                .findOpenRecordByOperatorId(operator.getId());
            // if the list of assignmnet labor records is NOT empty loop through
            // the list and
            // create new assignment labor records.
            AssignmentLaborManager alManager = this.getAssignmentLaborManager();
            List<AssignmentLabor> alList = alManager
                .listClosedRecordsByBreakLaborId(breakLabor.getId());
            for (AssignmentLabor al : alList) {
                alManager.openLaborRecord(time, operator, al.getAssignment(),
                    selectionLabor);
            }
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.PutAway) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.PutAway);
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.Replenishment) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.Replenishment);
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.LineLoading) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.LineLoading);
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.PutToStore) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.PutToStore);
            OperatorLabor ptsLabor = olManager
                .findOpenRecordByOperatorId(operator.getId());
            // if the list of assignmnet labor records is NOT empty loop through
            // the list and
            // create new assignment labor records.
            PtsLicenseLaborManager llManager = this.getPtsLicenseLaborManager();
            List<PtsLicenseLabor> llList = llManager
                .listClosedRecordsByBreakLaborId(breakLabor.getId());
            for (PtsLicenseLabor ll : llList) {
                llManager.openLaborRecord(time, operator, ll.getLicense(),
                    ptsLabor);
            }
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.CycleCounting) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.CycleCounting);
            OperatorLabor operLaborRecord = olManager
                .findOpenRecordByOperatorId(operator.getId());

            CycleCountingLabor ccLabor = this.cycleCountingLaborManager
                .findClosedRecordsByBreakLaborId(breakLabor.getId());

            if (ccLabor != null) {
                CycleCountingAssignment ccAssignment = ccLabor.getAssignment();

                this.getCycleCountingLaborManager().openLaborRecord(time,
                    operator, ccAssignment, operLaborRecord);
            }
        } else if (breakLabor.getPreviousOperatorLabor().getActionType() == OperatorLaborActionType.Loading) {
            olManager.openLaborRecord(time, operator,
                OperatorLaborActionType.Loading);
            OperatorLabor loadingOperLabor = olManager
                .findOpenRecordByOperatorId(operator.getId());
            // if the list of loading labor records is NOT empty loop through
            // the list and
            // create new loading labor records.
            LoadingRouteLaborManager llManager = this.getLoadingLaborManager();
            List<LoadingRouteLabor> llList = llManager
                .listClosedRecordsByBreakLaborId(breakLabor.getId());
            for (LoadingRouteLabor ll : llList) {
                llManager.openLaborRecord(time, operator, ll.getRoute(),
                    loadingOperLabor);
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#openLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType,
     *      com.vocollect.voicelink.core.model.OperatorLaborRecordType)
     */
    @Override
    public void openAssignmentLaborRecord(Date startTime,
                                          Operator operator,
                                          Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        // Open an assignment labor record
        this.getAssignmentLaborManager().openLaborRecord(
            startTime,
            operator,
            assignment,
            this.getOperatorLaborManager().findOpenRecordByOperatorId(
                operator.getId()));
    }

    /**
     * @param startTime Start time
     * @param operator the operator
     * @param assignment the assignment
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Override
    public void openCycleCountingLaborRecord(Date startTime,
                                             Operator operator,
                                             CycleCountingAssignment assignment)
        throws DataAccessException, BusinessRuleException {

        cycleCountingLaborManager.openLaborRecord(startTime, operator,
            assignment, this.getOperatorLaborManager()
                .findOpenRecordByOperatorId(operator.getId()));
    }

    /**
     * @param startTime Start time
     * @param operator the operator
     * @param route the route
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Override
    public void openLoadingLaborRecord(Date startTime,
                                       Operator operator,
                                       LoadingRoute route)
        throws DataAccessException, BusinessRuleException {
        // Close any record, if already open
        closeLoadingLaborRecord(startTime, operator);

        loadingLaborManager.openLaborRecord(
            startTime,
            operator,
            route,
            this.getOperatorLaborManager().findOpenRecordByOperatorId(
                operator.getId()));
    }

    /**
     * Method to increment operator labor count and invoke loading labor update
     * for every container loaded and consolidated.
     * @param operator - Operator that is loading the route.
     * @param route - The route object.
     * @param status - The container load status sent by the terminal
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
    @Override
    public void updateLoadingLabor(Operator operator,
                                   LoadingRoute route,
                                   LoadingContainerStatus status)
        throws DataAccessException, BusinessRuleException {

        // Operator doesn't get credit for Un-loading containers
        if (LoadingContainerStatus.Unloaded.equals(status)) {
            return;
        }

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());

        // Update Count field of Operator labor record with every container load
        if (olRec.getActionType() == OperatorLaborActionType.Loading) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
        }

        loadingLaborManager.updateLaborRecord(operator, route);
    }

    /**
     * Method to close Loading labor and associated operator record.
     * @param startTime Start time
     * @param operator The operator
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    private void closeLoadingLaborRecord(Date startTime, Operator operator)
        throws DataAccessException, BusinessRuleException {
        LoadingRouteLaborManager llManager = this.getLoadingLaborManager();
        LoadingRouteLabor loadingRecord = llManager
            .findOpenRecordsByOperatorId(operator.getId());

        // Return if no open record found
        if (loadingRecord == null) {
            return;
        }

        // Close the loading route labor records.
        this.getLoadingLaborManager().closeLaborRecord(startTime,
            loadingRecord.getRoute(), operator);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManager#closeAssignmentLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.selection.model.Assignment)
     */
    @Override
    public void closeAssignmentLaborRecord(Date endTime, Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        // This method will usually be called from stop or pass assignment.
        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(assignment.getOperator().getId());

        AssignmentLabor alRec = this.getAssignmentLaborManager()
            .closeLaborRecord(endTime, assignment);

        if (olRec.getActionType() == OperatorLaborActionType.Selection) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + alRec.getQuantityPicked());
            flRec.setDuration(endTime.getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
    }

    /**
     * @param endTime the end time
     * @param assignment the Cycle Counting assignment
     * @param operator the Cycle Counting operator
     * @return the Cycle Counting labor record closed
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Override
    public CycleCountingLabor closeCycleCountingLaborRecord(Date endTime,
                                                            CycleCountingAssignment assignment,
                                                            Operator operator)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());
        if (olRec.getActionType() == OperatorLaborActionType.CycleCounting) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
            flRec.setDuration(assignment.getEndTime().getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }

        return cycleCountingLaborManager.closeLaborRecord(endTime, assignment);
    }

    /**
     * @param endTime the end time
     * @param operator Operator
     * @param functionLabor Operator Function record
     * @param breakLabor Operator Break labor record
     */
    private void closeCycleCountingLaborRecordsForBreak(Date endTime,
                                                        Operator operator,
                                                        OperatorFunctionLabor functionLabor,
                                                        OperatorBreakLabor breakLabor)
        throws DataAccessException, BusinessRuleException {

        // get the list of open assignment labor records.
        CycleCountingLaborManager cclManager = this
            .getCycleCountingLaborManager();
        List<CycleCountingLabor> cclRecords = cclManager
            .listOpenRecordsByOperatorId(operator.getId());
        // Close the cc assignment labor records, associate them with the break
        // labor record, and calculate running totals on operator cc assignement
        // labor record.
        for (CycleCountingLabor ccl : cclRecords) {
            ccl.setBreakLaborId(breakLabor.getId());
            this.getCycleCountingLaborManager().closeLaborRecord(endTime,
                ccl.getAssignment());
            functionLabor.setCount(functionLabor.getCount()
                + ccl.getLocationsCounted());
        }
        this.calculateAggregateValues(functionLabor);

    }

    /**
     * @param endTime the end time
     * @param route the Loading route
     * @param operator The operator
     * @return the Loading labor record closed
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Override
    public LoadingRouteLabor closeLoadingLaborRecord(Date endTime,
                                                     LoadingRoute route,
                                                     Operator operator)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());

        LoadingRouteLabor llRec = this.getLoadingLaborManager()
            .closeLaborRecord(endTime, route, operator);

        if (olRec.getActionType() == OperatorLaborActionType.Loading) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setDuration(endTime.getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
        return llRec;

    }

    /**
     * 
     * @param endTime - the time to use to set the end time of the assignment
     *            labor record
     * @param operator - the operator who we are closing the labor records for.
     * @param functionLabor - the function labor record that was just closed.
     * @param breakLabor - The open break labor record.
     * 
     * @throws DataAccessException - on db failure
     * @throws BusinessRuleException - on model object save failure
     * 
     */
    private void closeAssignmentLaborRecordsForBreak(Date endTime,
                                                     Operator operator,
                                                     OperatorFunctionLabor functionLabor,
                                                     OperatorBreakLabor breakLabor)
        throws DataAccessException, BusinessRuleException {
        // get the list of open assignment labor records.
        AssignmentLaborManager alManager = this.getAssignmentLaborManager();
        List<AssignmentLabor> alRecords = alManager
            .listOpenRecordsByOperatorId(operator.getId());
        // Close the assignment labor records, associate them with the break
        // labor record, and
        // calculate running totals on on operator selection labor record.
        for (AssignmentLabor al : alRecords) {
            al.setBreakLaborId(breakLabor.getId());
            this.getAssignmentLaborManager().closeLaborRecord(endTime,
                al.getAssignment());
            functionLabor.setCount(functionLabor.getCount()
                + al.getQuantityPicked());
        }
        this.calculateAggregateValues(functionLabor);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManager#updatePutAwayLaborStatistics()
     */

    @Override
    public void updatePutAwayLaborStatistics(Operator operator, License license)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());

        if (olRec.getActionType() == OperatorLaborActionType.PutAway) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
            flRec.setDuration(license.getPutTime().getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManager#updateReplenishmentLaborStatistics()
     */

    @Override
    public void updateReplenishmentLaborStatistics(Operator operator,
                                                   Replenishment replenishment)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());
        if (olRec.getActionType() == OperatorLaborActionType.Replenishment) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
            flRec.setDuration(replenishment.getEndTime().getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#updateLineLoadLaborStatistics(com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.lineloading.model.CartonDetail)
     */
    @Override
    public void updateLineLoadLaborStatistics(Operator operator, Carton carton)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());
        if (olRec.getActionType() == OperatorLaborActionType.LineLoading
            && carton.getStatus() == CartonStatus.Loaded) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
            flRec.setDuration(carton.getLoadTime().getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
    }

    /**
     * 
     * @param endTime - the time to use to set the end time of the assignment
     *            labor record
     * @param operator - the operator who we are closing the labor records for.
     * @param functionLabor - the function labor record that was just closed.
     * @param breakLabor - The open break labor record.
     * 
     * @throws DataAccessException - on db failure
     * @throws BusinessRuleException - on model object save failure
     * 
     */
    private void closeLoadingLaborRecordsForBreak(Date endTime,
                                                  Operator operator,
                                                  OperatorFunctionLabor functionLabor,
                                                  OperatorBreakLabor breakLabor)
        throws DataAccessException, BusinessRuleException {
        // get the list of open assignment labor records.
        LoadingRouteLaborManager lrlManager = this.getLoadingLaborManager();
        LoadingRouteLabor llRecord = lrlManager
            .findOpenRecordsByOperatorId(operator.getId());
        // Close the loading labor records, associate them with the break
        // labor record, and
        // calculate running totals on on operator loading labor record.
        // Performing a null check if an operator tries to take a break before a
        // route
        // is issued (No loading function labor record created yet)
        if (llRecord != null) {
            llRecord.setBreakLaborId(breakLabor.getId());
            this.getLoadingLaborManager().closeLaborRecord(endTime,
                llRecord.getRoute(), operator);
            functionLabor.setCount(llRecord.getContainersLoaded());
        }
        this.calculateAggregateValues(functionLabor);
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#updateLineLoadLaborStatistics(com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.lineloading.model.CartonDetail)
     */
    @Override
    public void updateCycleCountingLaborStatistics(Operator operator,
                                                   CycleCountingAssignment assignment)
        throws DataAccessException, BusinessRuleException {

        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());
        if (olRec.getActionType() == OperatorLaborActionType.CycleCounting) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + 1);
            flRec.setDuration(assignment.getEndTime().getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
    }

    /**
     * @param fl - the selection labor record used to compute the statistics.
     * 
     * @throws DataAccessException - for errors on db access
     */
    protected void calculateAggregateValues(OperatorFunctionLabor fl)
        throws DataAccessException {

        // if we have a duration calculate the actual rate.
        if (fl.getDuration() > 0) {
            fl.setActualRate(new Double(fl.getCount()
                / (fl.getDuration() / MILLISEC_CONVERSION_FACTOR)));
        } else {
            fl.setActualRate(new Double(0.0));
        }

        // if we know the region, get the goal rate and calculate the percent of
        // goal if goal rate is not zero.
        if (fl.getRegion() != null) {
            if (fl.getRegion().getGoalRate() == null
                || fl.getRegion().getGoalRate() == 0) {
                fl.setPercentOfGoal(new Double(0.0));
                fl.setGoalRate(0);
            } else {
                fl.setPercentOfGoal(new Double(fl.getActualRate()
                    / fl.getRegion().getGoalRate() * ONE_HUNDRED));
                fl.setGoalRate(fl.getRegion().getGoalRate());
            }
        } else {
            // if we don't know the region, this means that we may be doing
            // putaway or replenishment
            // in multiple regions. Calculate the percent of goal based on the
            // average goal rate.
            // If we are not doing putway or replenishment, return 0 for the
            // percent of goal
            Double avgGoalRate = 0.0;
            if ((fl.getRegion() == null)
                && (fl.getActionType() == OperatorLaborActionType.PutAway)) {
                avgGoalRate = this.getPutawayRegionManager().avgGoalRate();
            } else if ((fl.getRegion() == null)
                && (fl.getActionType() == OperatorLaborActionType.Replenishment)) {
                avgGoalRate = this.getReplenishmentRegionManager()
                    .avgGoalRate();
            } else if ((fl.getRegion() == null)
                && (fl.getActionType() == OperatorLaborActionType.CycleCounting)) {
                avgGoalRate = this.getCycleCountingRegionManager()
                    .avgGoalRate();
            }

            if ((avgGoalRate == null || avgGoalRate == 0)
                && (fl.getGoalRate() == null || fl.getGoalRate() == 0)) {
                fl.setPercentOfGoal(new Double(0.0));
            } else {
                if (fl.getGoalRate() == null || fl.getGoalRate() == 0) {
                    fl.setGoalRate(avgGoalRate.intValue());
                }

                fl.setPercentOfGoal(new Double(fl.getActualRate()
                    / fl.getGoalRate() * ONE_HUNDRED));
            }
        }
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#executeModifyEndTime(com.vocollect.voicelink.core.model.OperatorLabor,
     *      java.util.Date)
     */
    @Override
    public void executeModifyEndTime(OperatorLabor operatorLabor, Date endTime)
        throws DataAccessException, BusinessRuleException {

        // get the list of operator labor records for operator contained in the
        // operator labor record
        List<OperatorLabor> laborList = this.getOperatorLaborManager()
            .listAllRecordsByOperatorId(operatorLabor.getOperator().getId());

        OperatorLabor nextRecord = null;
        if (laborList.contains(operatorLabor)) {
            nextRecord = laborList.get(laborList.indexOf(operatorLabor) + 1);
        } else {
            throw new BusinessRuleException(
                CoreErrorCode.LABOR_DID_NOT_FIND_REQUESTED_RECORD,
                new UserMessage("labor.did.not.find.requested.record"));
        }
        if (nextRecord != null) {
            nextRecord.setStartTime(endTime);
            if (nextRecord instanceof OperatorFunctionLabor) {
                if (nextRecord.getEndTime() != null) {
                    nextRecord.setDuration(nextRecord.getEndTime().getTime()
                        - nextRecord.getStartTime().getTime());
                    this.calculateAggregateValues((OperatorFunctionLabor) nextRecord);
                }
            }
        }
        operatorLabor.setEndTime(endTime);
        operatorLabor.setDuration(endTime.getTime()
            - operatorLabor.getStartTime().getTime());

        if (operatorLabor instanceof OperatorFunctionLabor) {
            this.calculateAggregateValues((OperatorFunctionLabor) operatorLabor);
        }
        this.getOperatorLaborManager().save(operatorLabor);
        this.getOperatorLaborManager().save(nextRecord);

        // Re-saving the end time of Application Labor Record to synchronise
        // with Operator labor record
        OperatorLaborActionType actionType = operatorLabor.getActionType();
        if (actionType == OperatorLaborActionType.Selection) {

            List<AssignmentLabor> assignmentLabors = this.assignmentLaborManager
                .listAllRecordsByOperatorLaborId(operatorLabor.getId());

            if (assignmentLabors != null && assignmentLabors.size() > 0) {
                AssignmentLabor assignmentlabor = assignmentLabors
                    .get(assignmentLabors.size() - 1);
                assignmentlabor.setEndTime(endTime);

                this.assignmentLaborManager
                    .recalculateAggregatesAndSave(assignmentlabor);
            }

        } else if (actionType == OperatorLaborActionType.PutToStore) {

            List<PtsLicenseLabor> ptsLabors = this.ptsLicenseLaborManager
                .listAllRecordsByOperatorLaborId(operatorLabor.getId());

            if (ptsLabors != null && ptsLabors.size() > 0) {
                PtsLicenseLabor ptsLabor = ptsLabors.get(ptsLabors.size() - 1);
                ptsLabor.setEndTime(endTime);

                this.ptsLicenseLaborManager
                    .recalculateAggregatesAndSave(ptsLabor);
            }

        } else if (actionType == OperatorLaborActionType.Loading) {

            List<LoadingRouteLabor> loadingLabors = this.loadingLaborManager
                .listAllRecordsByOperatorLaborId(operatorLabor.getId());

            if (loadingLabors != null && loadingLabors.size() > 0) {
                LoadingRouteLabor loadingLabor = loadingLabors
                    .get(loadingLabors.size() - 1);

                loadingLabor.setEndTime(endTime);

                this.loadingLaborManager
                    .recalculateAggregatesAndSave(loadingLabor);
            }

        } else if (actionType == OperatorLaborActionType.CycleCounting) {

            List<CycleCountingLabor> cycleCountLabors = this.cycleCountingLaborManager
                .listAllRecordsByOperatorLaborId(operatorLabor.getId());

            if (cycleCountLabors != null && cycleCountLabors.size() > 0) {
                CycleCountingLabor cycleCountingLabor = cycleCountLabors
                    .get(cycleCountLabors.size() - 1);
                cycleCountingLabor.setEndTime(endTime);

                this.cycleCountingLaborManager
                    .recalculateAggregatesAndSave(cycleCountingLabor);
            }

        }

    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#getEarliestEndTime(java.lang.Long)
     */
    @Override
    public Date getEarliestEndTime(Long operatorLaborId)
        throws DataAccessException, BusinessRuleException {

        // This method will generally be called by the UI to allow a supervisor
        // to modify the end date of a function labor record after it has
        // already
        // been closed.
        // Find the open operator labor record.
        OperatorLabor olRec = this.getOperatorLaborManager().get(
            operatorLaborId);

        if (olRec.getEndTime() == null) {
            throw new BusinessRuleException(
                CoreErrorCode.LABOR_CANNOT_MODIFY_OPEN_LABOR_RECORD,
                new UserMessage("labor.cannot.modify.open.labor.record"));
        }

        Date returnDate = null;
        OperatorLaborActionType actionType = olRec.getActionType();
        if (actionType == OperatorLaborActionType.Selection) {
            returnDate = this.getEarliestSelectionDate(olRec.getOperator(),
                olRec);
            if (returnDate == null) {
                throw new BusinessRuleException(
                    CoreErrorCode.LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE,
                    new UserMessage(
                        "operatorLabor.error.editEndtime.noAssignment"));
            }
        } else if (actionType == OperatorLaborActionType.PutAway) {
            returnDate = this
                .getEarliestPutAwayDate(olRec.getOperator(), olRec);
        } else if (actionType == OperatorLaborActionType.Replenishment) {
            returnDate = this.getEarliestReplenishmentDate(olRec.getOperator(),
                olRec);
        } else if (actionType == OperatorLaborActionType.LineLoading) {
            returnDate = this.getEarliestLineLoadingDate(olRec.getOperator(),
                olRec);
        } else if (actionType == OperatorLaborActionType.PutToStore) {
            returnDate = this.getEarliestPutToStoreDate(olRec.getOperator(),
                olRec);
            if (returnDate == null) {
                throw new BusinessRuleException(
                    CoreErrorCode.LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE,
                    new UserMessage("operatorLabor.error.editEndtime.noLicense"));
            }
        } else if (actionType == OperatorLaborActionType.CycleCounting) {
            returnDate = this.getEarliestCycleCountingDate(olRec.getOperator(),
                olRec);
            if (returnDate == null) {
                throw new BusinessRuleException(
                    CoreErrorCode.LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE,
                    new UserMessage("operatorLabor.error.editEndtime.noLicense"));
            }
        } else if (actionType == OperatorLaborActionType.Loading) {
            returnDate = this
                .getEarliestLoadingDate(olRec.getOperator(), olRec);
            if (returnDate == null) {
                throw new BusinessRuleException(
                    CoreErrorCode.LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE,
                    new UserMessage("operatorLabor.error.editEndtime.noLicense"));
            }
        } else {
            throw new BusinessRuleException(
                CoreErrorCode.LABOR_CANNOT_MODIFY_LABOR_RECORD_TYPE,
                new UserMessage("labor.cannot.modify.labor.record.type"));
        }
        return returnDate;
    }

    /**
     * Get the earliest loading end date. This method attempts to find the
     * date/time of the last action the operator performed within the system
     * while performing selection.
     * 
     * @param operator that is performing the action
     * @param olRec - The operator labor record that is being modified.
     * 
     * @return the latest time we can find in the loading data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestLoadingDate(Operator operator, OperatorLabor olRec)
        throws DataAccessException {

        List<LoadingRouteLabor> llRecords = this.getLoadingLaborManager()
            .listAllRecordsByOperatorLaborId(olRec.getId());

        // Check if there are any associated Loading labor records.
        if (llRecords == null || llRecords.isEmpty()) {
            return olRec.getStartTime();
        }

        LoadingRouteLabor loadingLabor = llRecords.get(llRecords.size() - 1);
        if (loadingLabor.getContainersLoaded() == 0) {
            return loadingLabor.getStartTime();
        } else {
            if (loadingLabor.getEndTime() != null) {
                return loadingLabor.getEndTime();
            } else {
                Date loadTime = this.getLoadingContainerManager().maxEndTime(
                    operator.getId(), loadingLabor.getStartTime(),
                    loadingLabor.getEndTime());

                return loadTime;
            }

        }

    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#getEarliestEndTime(com.vocollect.voicelink.core.model.Operator)
     */
    @Override
    public Date getEarliestEndTime(Operator operator)
        throws DataAccessException, BusinessRuleException {

        // This method is called by the signoff manager when a operator is
        // signed off
        // by a supervisor from the UI. The labor record will be open at the
        // time of
        // this call.
        // Find the open operator labor record.
        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(operator.getId());

        Date returnDate = null;
        if (olRec != null) {
            OperatorLaborActionType actionType = olRec.getActionType();

            if (actionType == OperatorLaborActionType.Selection) {
                returnDate = this.getEarliestSelectionDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.PutAway) {
                returnDate = this.getEarliestPutAwayDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.Replenishment) {
                returnDate = this.getEarliestReplenishmentDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.LineLoading) {
                returnDate = this.getEarliestLineLoadingDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.CycleCounting) {
                returnDate = this.getEarliestCycleCountingDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.PutToStore) {
                returnDate = this.getEarliestPutToStoreDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.Loading) {
                returnDate = this.getEarliestLoadingDate(operator, olRec);
            } else if (actionType == OperatorLaborActionType.Break) {
                return olRec.getStartTime();
            } else if (actionType == OperatorLaborActionType.SignOn) {
                return olRec.getStartTime();
            } else if (actionType == OperatorLaborActionType.SignOff) {
                return olRec.getStartTime();
            }
        }
        return returnDate;
    }

    /**
     * Get the earliest selection end date. This method attempts to find the
     * date/time of the last action the operator performed within the system
     * while performing selection.
     * 
     * @param operator that is performing the action
     * @param olRec - The operator labor record that is being modified.
     * 
     * @return the latest time we can find in the replenishment data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestSelectionDate(Operator operator, OperatorLabor olRec)
        throws DataAccessException {

        // Find last assignment labor record if there is one for the given time
        // period.
        // The following call returns the records sorted from oldest to newest.
        List<AssignmentLabor> alRecords = this.getAssignmentLaborManager()
            .listAllRecordsByOperatorLaborId(olRec.getId());
        // We have an open operator labor record but no assignment labor records
        // return the start time of the operator labor record or null.
        // if the olRec count is zero then we can modify the record to the
        // startime of the
        // operator labor record. If olRec.count is not zero then we can not
        // modify the record.
        // We can get into this situation if the assignment labor records are
        // purged
        // before the operator labor records have been purged.
        if (alRecords == null || alRecords.size() <= 0) {
            if (olRec.getCount() == 0) {
                return olRec.getStartTime();
            } else {
                if (operator.getStatus() == OperatorStatus.SignedOn) {
                    return new Date();
                } else {
                    return null;
                }
            }
        }

        // See if we have a closed assignment labor record for this operator
        // labor record.
        // If we do, see if there are any pick details.
        // If there are pick details, use the end time of the closed assignment
        // labor record.
        // If there are no pick details, use the start time of the assignment
        // labor record.
        AssignmentLabor assignmentLabor = alRecords.get(alRecords.size() - 1);
        if (assignmentLabor != null && assignmentLabor.getEndTime() != null) {
            Date pickDetailTime = this.getPickDetailManager().maxEndTime(
                operator.getId(), assignmentLabor.getStartTime(),
                assignmentLabor.getEndTime());
            if (pickDetailTime != null) {
                // This first case is for getting the end time when signing off
                // the operator from the GUI.
                if (operator.getStatus() == OperatorStatus.SignedOn) {
                    return assignmentLabor.getEndTime();
                } else {
                    // this case is for when the end time is being adjusted.
                    if (assignmentLabor.getEndTime().getTime() == olRec
                        .getEndTime().getTime()) {
                        // if the operator was previously signed off from the
                        // gui then return the pickDetail time.
                        return pickDetailTime;
                    } else {
                        return assignmentLabor.getEndTime();
                    }
                }
            } else {
                return assignmentLabor.getStartTime();
            }
        }
        // If we got here, we have an open assignment labor record.
        // Find the last pick detail record for this assignment if any.
        Date lastPickDetailDate = this.getPickDetailManager().maxEndTime(
            operator.getId(), assignmentLabor.getStartTime(),
            assignmentLabor.getEndTime());
        // if we do not find any pick details, return the start time of the
        // assignment
        // labor record
        if (lastPickDetailDate == null) {
            return assignmentLabor.getStartTime();
        } else {
            return lastPickDetailDate;
        }
    }

    /**
     * @param operator being signed off
     * @param olRec operator labor record
     * @return the latest time we can find in the replenishment data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestPutAwayDate(Operator operator, OperatorLabor olRec)
        throws DataAccessException {

        Date retDate = null;
        List<License> licenses;

        // If the labor record is open the end time is not available so use just
        // the
        // start time to get labor records otherwise use end time as well to
        // define
        // range of labor records of interest.
        if (olRec.getEndTime() == null) {
            licenses = this.getLicenseManager()
                .listRecordsByOperatorGreaterThanDate(operator,
                    olRec.getStartTime());
        } else {
            licenses = this.getLicenseManager()
                .listMostRecentRecordsByOperator(operator,
                    olRec.getStartTime(), olRec.getEndTime());
        }

        // Get start date and return as end time if no other labor records
        // exist for this operator.
        Date startDate = olRec.getStartTime();
        if (licenses.size() == 0) {
            // there can be detail records even though the license record has no
            // put time.
            List<LicenseDetail> licenseDetails = this
                .getLicenseDetailManager()
                .listMostRecentRecordsByOperator(operator, olRec.getStartTime());
            if (licenseDetails.size() == 0) {
                retDate = startDate;
            } else {
                LicenseDetail licenseDetail = licenseDetails.get(0);
                if (licenseDetail == null) {
                    retDate = startDate;
                } else {
                    retDate = licenseDetail.getPutTime();
                }
            }
        } else {
            License license = licenses.get(0);

            // Check for completed licenses that completed after the
            // labor record's start time.
            if ((license.getStatus() == LicenseStatus.Complete)
                && (license.getPutTime().after(startDate))) {
                if (olRec.getEndTime() == null) {
                    retDate = license.getPutTime();
                } else {
                    retDate = olRec.getEndTime();
                    if (retDate.after(license.getPutTime())) {
                        retDate = license.getPutTime();
                    }
                }

                // Check for inprogress License records that started after
                // the labor record's start time.
            } else if ((license.getStatus() == LicenseStatus.InProgress)
                && (license.getStartTime().after(startDate))) {
                // Find Detail Records.... with the most recent activity at the
                // top
                // of the list.
                List<LicenseDetail> details = this.getLicenseDetailManager()
                    .listMostRecentRecordsByOperator(operator,
                        olRec.getStartTime());

                // Get the most recent one.
                LicenseDetail detail = details.get(0);
                if (detail == null) {
                    // None exist - return the Licenses' starttime.
                    retDate = license.getStartTime();
                } else if (detail.getStartTime().after(startDate)) {
                    // based upon putTIme -- return either the
                    // start time or the end time
                    if (detail.getPutTime() != null) {
                        retDate = detail.getPutTime();
                    } else {
                        retDate = detail.getStartTime();
                    }
                }
            }
        }
        return retDate;
    }

    /**
     * @param operator to search for
     * @param olRec operator labor record
     * @return the latest time we can find in the replenishment data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestReplenishmentDate(Operator operator,
                                              OperatorLabor olRec)
        throws DataAccessException {

        Date retDate = null;
        List<Replenishment> replenishments;
        if (olRec.getEndTime() == null) {
            replenishments = this.getReplenishmentManager()
                .listRecordsByOperatorGreaterThanDate(operator,
                    olRec.getStartTime());
        } else {
            replenishments = this.getReplenishmentManager()
                .listMostRecentRecordsByOperator(operator,
                    olRec.getStartTime(), olRec.getEndTime());
        }

        Date startDate = olRec.getStartTime();
        if (replenishments.size() == 0) {
            // there can be detail records even though the replen record has no
            // end time.
            List<ReplenishmentDetail> replenishmentDetails = this
                .getReplenishmentDetailManager()
                .listMostRecentRecordsByOperator(operator, olRec.getStartTime());
            if (replenishmentDetails.size() == 0) {
                retDate = startDate;
            } else {
                ReplenishmentDetail replenDetail = replenishmentDetails.get(0);
                if (replenDetail == null) {
                    retDate = startDate;
                } else {
                    retDate = replenDetail.getReplenishTime();
                }
            }

        } else {
            Replenishment replenishment = replenishments.get(0);

            // Check for completed replenishments that completed after the
            // labor record's start time.
            if ((replenishment.getStatus() == ReplenishStatus.Complete)
                && (replenishment.getEndTime().after(startDate))) {
                if (olRec.getEndTime() == null) {
                    retDate = replenishment.getEndTime();
                } else {
                    retDate = olRec.getEndTime();
                    if (retDate.after(replenishment.getEndTime())) {
                        retDate = replenishment.getEndTime();
                    }
                }

                // Check for inprogress replenishments record that started after
                // the labor record's start time.
            } else if ((replenishment.getStatus() == ReplenishStatus.InProgress)
                && (replenishment.getStartTime().after(startDate))) {

                // Find Detail Records....
                List<ReplenishmentDetail> details = this
                    .getReplenishmentDetailManager()
                    .listMostRecentRecordsByOperator(operator,
                        olRec.getStartTime());
                // Get the most recent one.
                ReplenishmentDetail detail = details.get(0);
                if (detail == null) {
                    // None exist - return the Replenishment's starttime.
                    retDate = replenishment.getStartTime();

                } else if (detail.getStartTime().after(startDate)) {
                    // based upon replenishTime -- return either the
                    // start time or the end time
                    if (detail.getReplenishTime() != null) {
                        retDate = detail.getReplenishTime();
                    } else {
                        retDate = detail.getStartTime();
                    }
                }
            }
        }
        return retDate;
    }

    /**
     * @param olRec operator labor record
     * @param operator being signed off
     * @return the latest time we can find in the replenishment data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestLineLoadingDate(Operator operator,
                                            OperatorLabor olRec)
        throws DataAccessException {

        Date retDate = null;
        List<CartonDetail> cartonDetails;
        if (olRec.getEndTime() == null) {
            cartonDetails = this.getCartonDetailManager()
                .listRecordsByOperatorGreaterThanDate(operator,
                    olRec.getStartTime());
        } else {
            cartonDetails = this.getCartonDetailManager()
                .listMostRecentRecordsByOperator(operator,
                    olRec.getStartTime(), olRec.getEndTime());
        }

        if (cartonDetails.size() == 0) {
            retDate = olRec.getStartTime();
        } else {
            CartonDetail detail = cartonDetails.get(0);
            retDate = detail.getActionTime();
        }
        return retDate;
    }

    /**
     * @param olRec operator labor record
     * @param operator being signed off
     * @return the latest time we can find in the replenishment data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestCycleCountingDate(Operator operator,
                                              OperatorLabor olRec)
        throws DataAccessException {

        // Get the list of Cycle Counting labor records for this operator labor
        // record.
        List<CycleCountingLabor> ccLoRecs = this.getCycleCountingLaborManager()
            .listAllRecordsByOperatorLaborId(olRec.getId());

        // Check to see if there are any cc labor records associated with this
        // operator
        // labor record.
        if (ccLoRecs.size() > 0) {
            // If we get here then we have cc labor records for this operator
            // labor record.
            // get the last one from the list and figure out what is going on.
            CycleCountingLabor ccLoRec = ccLoRecs.get(ccLoRecs.size() - 1);
            // if the endtime of the cc assignment is null then return the start
            // time of the
            // cc labor record.
            if (ccLoRec.getEndTime() == null) {
                return ccLoRec.getStartTime();
            }
            // if endtime is null you are being signed off from the GUI.
            // If the location counted is zero then nothing was counted.
            // return the start time of the cc labor record. this may not be a
            // valid usecase.
            // if something was counted, return the end time of the
            // cc labor record.
            if (olRec.getEndTime() == null) {
                if (ccLoRec.getLocationsCounted() == 0) {
                    return ccLoRec.getStartTime();
                } else {
                    return ccLoRec.getEndTime();
                }
            }
            // If the cc labor record end time equals the operator labor record
            // end time then
            // the operator was signed off from the GUI.
            // If the call to getLocationsCounted is zero then the operator did
            // not count anything.
            // Return the start time of the cc labor record.
            if ((ccLoRec.getEndTime().compareTo(olRec.getEndTime()) == 0)
                && (ccLoRec.getLocationsCounted() == 0)) {
                return ccLoRec.getStartTime();
            }

            // If the cc labor record end time equals the operator labor record
            // end time then
            // the operator was signed off from the GUI.
            // If the call to getLocationsCounted is one then the operator
            // counted the location.
            // Return the end time of the cc labor record.
            if ((ccLoRec.getEndTime().compareTo(olRec.getEndTime()) == 0)
                && (ccLoRec.getLocationsCounted() == 1)) {
                return ccLoRec.getEndTime();
            }

            // If the cc labor record end time does NOT equal the operator labor
            // record end time then
            // the operator was signed off from the GUI and completed a cc
            // assignment.
            // If the call to getLocationsCounted is one then the operator
            // counted the location.
            // Return the end time of the cc labor record.
            if ((ccLoRec.getEndTime().compareTo(olRec.getEndTime()) != 0)
                && (ccLoRec.getLocationsCounted() == 1)) {
                return ccLoRec.getEndTime();
            }

        }
        // There are no assignment labor records assoicated with the operator
        // labor record.
        // return the start time of the operator labor record.
        return olRec.getStartTime();
    }

    /**
     * Getter for the ptsLicenseLaborManager property.
     * 
     * @return PtsLicenseLaborManager value of the property
     */
    @Override
    public PtsLicenseLaborManager getPtsLicenseLaborManager() {
        return this.ptsLicenseLaborManager;
    }

    /**
     * Setter for the ptsLicenseLaborManager property.
     * 
     * @param ptsLicenseLaborManager the new ptsLicenseLaborManager value
     */
    public void setPtsLicenseLaborManager(PtsLicenseLaborManager ptsLicenseLaborManager) {
        this.ptsLicenseLaborManager = ptsLicenseLaborManager;
    }

    /**
     * Closes a PTS license labor record.
     * 
     * @param endTime - time the labor record is being closed.
     * @param license - the license to close
     * @throws DataAccessException - if db fails
     * @throws BusinessRuleException - if business logic fails
     */
    @Override
    public void closePtsLicenseLaborRecord(Date endTime, PtsLicense license)
        throws DataAccessException, BusinessRuleException {
        // This method will usually be called from stop or pass assignment in
        // the PTS application.
        OperatorLabor olRec = this.getOperatorLaborManager()
            .findOpenRecordByOperatorId(license.getOperator().getId());

        PtsLicenseLabor llRec = getPtsLicenseLaborManager().closeLaborRecord(
            endTime, license);

        if (olRec.getActionType() == OperatorLaborActionType.PutToStore) {
            OperatorFunctionLabor flRec = (OperatorFunctionLabor) olRec;
            flRec.setCount(flRec.getCount() + llRec.getQuantityPut());
            flRec.setDuration(endTime.getTime()
                - flRec.getStartTime().getTime());
            this.calculateAggregateValues(flRec);
        }
        return;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.OperatorLaborManager#openLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLaborActionType,
     *      com.vocollect.voicelink.core.model.OperatorLaborRecordType)
     */
    @Override
    public void openPtsLicenseLaborRecord(Date startTime,
                                          Operator operator,
                                          PtsLicense license)
        throws DataAccessException, BusinessRuleException {

        // Open a license labor record
        this.getPtsLicenseLaborManager().openLaborRecord(
            startTime,
            operator,
            license,
            this.getOperatorLaborManager().findOpenRecordByOperatorId(
                operator.getId()));
    }

    /**
     * 
     * @param endTime - the time to use to set the end time of the assignment
     *            labor record
     * @param operator - the operator who we are closing the labor records for.
     * @param functionLabor - the function labor record that was just closed.
     * @param breakLabor - The open break labor record.
     * 
     * @throws DataAccessException - on db failure
     * @throws BusinessRuleException - on model object save failure
     * 
     */
    private void closePtsLicenseLaborRecordsForBreak(Date endTime,
                                                     Operator operator,
                                                     OperatorFunctionLabor functionLabor,
                                                     OperatorBreakLabor breakLabor)
        throws DataAccessException, BusinessRuleException {
        // get the list of open assignment labor records.
        PtsLicenseLaborManager llManager = this.getPtsLicenseLaborManager();
        List<PtsLicenseLabor> llRecords = llManager
            .listOpenRecordsByOperatorId(operator.getId());
        // Close the assignment labor records, associate them with the break
        // labor record, and
        // calculate running totals on on operator selection labor record.
        for (PtsLicenseLabor ll : llRecords) {
            ll.setBreakLaborId(breakLabor.getId());
            this.getPtsLicenseLaborManager().closeLaborRecord(endTime,
                ll.getLicense());
            functionLabor.setCount(functionLabor.getCount()
                + ll.getQuantityPut());
        }
        this.calculateAggregateValues(functionLabor);
    }

    /**
     * Get the earliest put to store end date. This method attempts to find the
     * date/time of the last action the operator performed within the system
     * while performing put to store.
     * 
     * @param operator that is performing the action
     * @param olRec - The operator labor record that is being modified.
     * 
     * @return the latest time we can find in the put to store data
     * @throws DataAccessException on any database exception
     */
    private Date getEarliestPutToStoreDate(Operator operator,
                                           OperatorLabor olRec)
        throws DataAccessException {

        // Find last assignment labor record if there is one for the given time
        // period.
        // The following call returns the records sorted from oldest to newest.
        List<PtsLicenseLabor> llRecords = this.getPtsLicenseLaborManager()
            .listAllRecordsByOperatorLaborId(olRec.getId());
        // We have an open operator labor record but no license labor records
        // return the start time of the operator labor record or null.
        // if the olRec count is zero then we can modify the record to the
        // startime of the
        // operator labor record. If olRec.count is not zero then we can not
        // modify the record.
        // We can get into this situation if the license labor records are
        // purged
        // before the operator labor records have been purged.
        if (llRecords == null || llRecords.size() <= 0) {
            if (olRec.getCount() == 0) {
                return olRec.getStartTime();
            } else {
                if (operator.getStatus() == OperatorStatus.SignedOn) {
                    return new Date();
                } else {
                    return null;
                }
            }
        }

        // See if we have a closed pts license labor record for this operator
        // labor record.
        // If we do, see if there are any put details.
        // If there are put details, use the end time of the closed license
        // labor record.
        // If there are no put details, use the start time of the pts license
        // labor record.
        PtsLicenseLabor ptsLicenseLabor = llRecords.get(llRecords.size() - 1);
        if (ptsLicenseLabor != null && ptsLicenseLabor.getEndTime() != null) {
            Date putDetailTime = this.getPtsPutDetailManager().maxEndTime(
                operator.getId(), ptsLicenseLabor.getStartTime(),
                ptsLicenseLabor.getEndTime());
            if (putDetailTime != null) {
                // This first case is for getting the end time when signing off
                // the operator from the GUI.
                if (operator.getStatus() == OperatorStatus.SignedOn) {
                    return ptsLicenseLabor.getEndTime();
                } else {
                    // this case is for when the end time is being adjusted.
                    if (ptsLicenseLabor.getEndTime().getTime() == olRec
                        .getEndTime().getTime()) {
                        // if the operator was previously signed off from the
                        // gui then return the putDetail time.
                        return putDetailTime;
                    } else {
                        return ptsLicenseLabor.getEndTime();
                    }
                }
            } else {
                return ptsLicenseLabor.getStartTime();
            }
        }
        // If we got here, we have an open license labor record.
        // Find the last put detail record for this license if any.
        Date lastPutDetailDate = this.getPtsPutDetailManager().maxEndTime(
            operator.getId(), ptsLicenseLabor.getStartTime(),
            ptsLicenseLabor.getEndTime());
        // if we do not find any put details, return the start time of the
        // license
        // labor record
        if (lastPutDetailDate == null) {
            return ptsLicenseLabor.getStartTime();
        } else {
            return lastPutDetailDate;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#lastWorkOnLaborRecord(com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.core.model.OperatorLabor)
     */
    public Date lastWorkOnLaborRecord(Operator operator,
                                      OperatorLabor operatorLabor)
        throws DataAccessException {
        Date lastWorkDone = null;

        switch (operatorLabor.getActionType()) {
        case Selection:
            lastWorkDone = getEarliestSelectionDate(operator, operatorLabor);
            
            // If the last work done and labor data end time is same, that means
            // the operator is during the pick and not start or end of
            // assignment
            if (operatorLabor.getEndTime() != null
                && operatorLabor.getEndTime().equals(lastWorkDone)) {
                
                lastWorkDone = this.getPickDetailManager().maxEndTime(
                    operator.getId(), operatorLabor.getStartTime(), operatorLabor.getEndTime());
            }
            break;
        case PutAway:
            lastWorkDone = getEarliestPutAwayDate(operator, operatorLabor);
            break;

        case Replenishment:
            lastWorkDone = getEarliestReplenishmentDate(operator, operatorLabor);
            break;

        case LineLoading:
            lastWorkDone = getEarliestLineLoadingDate(operator, operatorLabor);
            break;

        case PutToStore:
            lastWorkDone = getEarliestPutToStoreDate(operator, operatorLabor);
            break;

        case Loading:
            lastWorkDone = getEarliestLoadingDate(operator, operatorLabor);
            break;

        case CycleCounting:
            lastWorkDone = getEarliestCycleCountingDate(operator, operatorLabor);
            break;

        default:
            break;
        }

        return lastWorkDone;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.LaborManagerRoot#nextWorkStartedOnLaborRecord(com.vocollect.voicelink.core.model.Operator, com.vocollect.voicelink.core.model.OperatorLabor)
     */
    public Date nextWorkStarted(Operator operator,
                                             OperatorLabor operatorLabor) {
        Date closestDateTime = null;
        try {
            closestDateTime = this.getPickDetailManager().minPickTime(
                operator.getId(), operatorLabor.getEndTime());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return closestDateTime;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 