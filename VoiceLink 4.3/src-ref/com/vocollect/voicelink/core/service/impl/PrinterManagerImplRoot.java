/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.dao.PrinterDAO;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.AbstractPrintServer;
import com.vocollect.voicelink.printserver.ChaseLabel;
import com.vocollect.voicelink.printserver.ContainerLabel;
import com.vocollect.voicelink.printserver.LabelData;
import com.vocollect.voicelink.printserver.PalletLabel;
import com.vocollect.voicelink.printserver.PrintJob;
import com.vocollect.voicelink.printserver.PrintServerFactory;
import com.vocollect.voicelink.printserver.PtsContainerLabel;
import com.vocollect.voicelink.printserver.ServerPrinter;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerType;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Additional service methods for the <code>Printer</code> model object.
 *
 * @author Ed Stoll
 */
public abstract class PrinterManagerImplRoot extends
    GenericManagerImpl<Printer, PrinterDAO> implements PrinterManager {

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PrinterManagerImplRoot(PrinterDAO primaryDAO) {
        super(primaryDAO);
    }

    
    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }
    
    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.PrinterManager#listAll()
     */
    public List<Printer> listAllOrderByNumber() throws DataAccessException {
        return getPrimaryDAO().listAllOrderByNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.PrinterManager#findByNumber(int)
     */
    public Printer findByNumber(int printerNumber) throws DataAccessException {
        return getPrimaryDAO().findByNumber(printerNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.PrinterManager#listAllEnabledPrinters()
     */
    public List<Printer> listAllEnabledPrinters() throws DataAccessException {
        return getPrimaryDAO().listAllEnabledPrinters();
    }
    
    /**
     * For each assignment, determine if you need to print pallet, 
     * container, or chase labels, building one print job request.
     * @param assignments - list of assignments to print pallet or 
     *                      container labels
     * @param printer - printer object
     * @throws BusinessRuleException - for certain conditions
     * 
     */
    public void generateAssignmentLabels(List<Assignment> assignments, 
                                         Printer printer) 
    throws BusinessRuleException {
        
        List<LabelData> labels = new ArrayList<LabelData>();
        
        // build the label objects
        for (Assignment assignment : assignments) {

            // Is this a chase assignment?
            // Chase assignments only print chase labels.
            // You cannot print Container labels for chase assignments.
            // Not sure there is a good reason for this rule other than you can't
            if (assignment.getType() == AssignmentType.Chase) {
                
                // Build Chase Labels for this assignment
                List<ChaseLabel> chaseLabels = 
                    getAssignmentChaseLabels(assignment);
                
                for (ChaseLabel chase : chaseLabels) {
                    labels.add(chase.getLabelData());
                }
                
            } else {

                // Does this assignment use Containers?
                ContainerType ct = assignment.getRegion().
                                    getProfileNormalAssignment().
                                    getContainerType();
                
                if ((ct == null) || (ct.getId() == null)) {
                    // This assignment prints pallet labels
                    labels.add(getPalletLabel(assignment).getLabelData());

                } else {
                    // Are there any containers for this assignment
                    if (assignment.getContainerCount() <= 0) {
                        // Specifically call String.valueOf on the assignment number
                        // so the digit is not comma-delimited in the message.
                        throw new BusinessRuleException(
                            CoreErrorCode.PRINT_ASSIGNMENT_DOESNOT_HAVE_CONTAINERS,
                            new UserMessage("print.assignment.error.nocontainers",
                                String.valueOf(assignment.getNumber())));            

                        // Throw exception of no containers to print
                    } else {
                        // Build Container labels for this assignment
                        List<ContainerLabel> clabels = 
                            getAssignmentContainerLabels(assignment);
                        
                        for (ContainerLabel cl : clabels) {
                            labels.add(cl.getLabelData());
                        }
                    }

                }
            }   // if assignment type
        }   // for each assignmet
        
        // If we have labels - print them
        if (labels.size() > 0) {
            submitPrintJob(labels, printer);
        } else {
            throw new BusinessRuleException(
                CoreErrorCode.PRINT_ASSIGNMENT_NO_LABELS,
                new UserMessage("print.assignment.error.nolabels"));            
        }
        return;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.PrinterManager#generateContainerLabels(java.util.List, com.vocollect.voicelink.core.model.Printer)
     */
    public void generateContainerLabels(List<Container> containers, Printer printer)
        throws BusinessRuleException {
            
        List<LabelData> labels = new ArrayList<LabelData>();
        
        for (Container container : containers) {
            // Build Container labels for this assignment
            ContainerLabel clabel = getContainerLabel(container);
            labels.add(clabel.getLabelData());
        }

        // If we have labels - print them
        if (labels.size() > 0) {
            submitPrintJob(labels, printer);
        }

        return;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.PrinterManager#generatePtsContainerLabels(java.util.List, com.vocollect.voicelink.core.model.Printer)
     */
    public void generatePtsContainerLabels(List<PtsContainer> ptsContainers, Printer printer)
        throws BusinessRuleException {
            
        List<LabelData> labels = new ArrayList<LabelData>();
        
        for (PtsContainer container : ptsContainers) {
            // Build Container labels
            PtsContainerLabel clabel = getPtsContainerLabel(container);
            labels.add(clabel.getLabelData());
        }

        // If we have labels - print them
        if (labels.size() > 0) {
            submitPrintJob(labels, printer);
        }

        return;
    }
    
    /**
     * For each pick, print a pick label (basically the same thing as a 
     * chase label).  The quantity to print for each pick will be based
     * upon the QuantityToPick.  Submit this as one print job request.
     * @param picks - list of picks to print
     * @param printer - printer object
     * @throws BusinessRuleException - on print server exceptions
     */
    public void generatePickLabels(List<Pick> picks, Printer printer) 
    throws BusinessRuleException {
        List<LabelData> labels = new ArrayList<LabelData>();
        
        for (Pick pick : picks) {
            // Build chase label for this pick
            ChaseLabel clabel = getChaseLabel(pick);
            labels.add(clabel.getLabelData());
        }

        // If we have labels - print them
        if (labels.size() > 0) {
            submitPrintJob(labels, printer);
        }

        return;
    }
    
    /**
     * @param labels - a List of LabelData
     * @param printer - destination printer object
     * @throws BusinessRuleException when print server throws an error.
     */
    protected void submitPrintJob(List<LabelData> labels, Printer printer) 
    throws BusinessRuleException {

        PrintJob job = null;
        
        try {
            job = new PrintJob(printer.getName(), labels);
        } catch (VocollectException e) {
            logNotification(e);
            throw new BusinessRuleException(e.getErrorCode());
        }
        
        try {
            AbstractPrintServer printServer = 
                PrintServerFactory.getPrintServer();
            printServer.submitJob(job);
            
        } catch (VocollectException e) {
            logNotification(e);
            throw new BusinessRuleException(e.getErrorCode());
        }

    }
        
    /**
     * @return list of Printer objects
     * @throws DataAccessException for any database exception
     * @throws VocollectException for any printserver exception
     */
    public List<Printer> getServerPrinters() 
        throws DataAccessException, VocollectException {
        //connect to AbstractPrintServer for this call
        AbstractPrintServer abstractServer = PrintServerFactory.getPrintServer();
        Map<String, ServerPrinter> mapServerPrinters = 
            abstractServer.getPrinters();
        List<Printer> serverPrinters = new ArrayList<Printer>();
        Printer p = null;

        Collection<String> keys = mapServerPrinters.keySet();
        for (String printerName : keys) {
            p = new Printer();
            p.setName(printerName);
            p.setIsEnabled(true);
            serverPrinters.add(p);
        }
        
        return serverPrinters;
    }

    /**
     * @param assignment - a container assignment that has containers.
     * @return a list of Containers.
     */
    protected List<ContainerLabel> getAssignmentContainerLabels(Assignment assignment) {
        List<ContainerLabel> labels = new ArrayList<ContainerLabel>();
        ContainerLabel cl = null;
        
        for (Container container : assignment.getContainers()) {
            cl = getContainerLabel(container);
            labels.add(cl);
        }
        return labels;
    }

    /**
     * @param assignment of the chase
     * @return - List of ChaseLabels
     */
    public List<ChaseLabel> getAssignmentChaseLabels(Assignment assignment) {
        List<ChaseLabel> labels = new ArrayList<ChaseLabel>();
        ChaseLabel cl = null;

        // build a chase label for each pick
        for (Pick pick : assignment.getPicks()) {
            cl = getChaseLabel(pick);
            labels.add(cl);
        }
        return labels;
    }

    
    /**
     * @param assignment for the pallet
     * @return a pallet label
     */
    protected PalletLabel getPalletLabel(Assignment assignment) {
        Short copies = 1;
        return new PalletLabel(assignment, copies);
    }
    
    /**
     * @param container - a container.
     * @return a container label.
     */
    protected ContainerLabel getContainerLabel(Container container) {
        Short copies = 1;
        return new ContainerLabel(container, copies);
    }

    /**
     * @param pick - a pick.
     * @return a chase label.
     */
    protected ChaseLabel getChaseLabel(Pick pick) {
        return new ChaseLabel(pick, 
            new Short((short) pick.getQuantityToPick()));
    }

    /**
     * @param container - a container.
     * @return a pts container label.
     */
    protected PtsContainerLabel getPtsContainerLabel(PtsContainer container) {
        Short copies = 1;
        return new PtsContainerLabel(container, copies);
    }
    
    /**
     * Utility to log a print server error.
     * @param ve exception being thrown
     */
    private void logNotification(VocollectException ve) {

        String messageKey = 
            "notification.column.keyname.Process.PrintServer";
        
        LOPArrayList lop = new LOPArrayList();
        lop.add("notification.printServer.errorCode",
            ve.getUserMessage().getKey());

        try {
            getVoicelinkNotificationUtil().createRenderType2Notification(
                "notification.column.keyname.Process.PrintServer", 
                messageKey,
                NotificationPriority.CRITICAL,
                new Date(),
                ve.getErrorCode().toString(),
                lop); 
        } catch (Exception ex) {
            // Do nothing -- we don't care about 
            // failures to post the notificaiton
        }

    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 