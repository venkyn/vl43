/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.LotDAO;
import com.vocollect.voicelink.core.model.Lot;

import java.util.Date;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Lot-related operations.
 * 
 * @author mnichols
 */
public interface LotManagerRoot extends 
    GenericManager<Lot, LotDAO>, DataProvider {

    /**
     * retrieves the lot by the number.
     * 
     * @param lotNumber - value to retrieve by
     * @return list of requested lots, list may be empty
     * @throws DataAccessException - database exceptions
     */
    Lot findLotByNumber(String lotNumber) 
        throws DataAccessException;

    /**
     * Find lot for item and location(optional).
     * 
     * @param lotNumber - lot number to find.
     * @param itemNumber - item lot belongs to 
     * @param locationId - location where stored (optional)
     * @return - lot number if found.
     */
    Lot findLotByNumberItemLocation(String lotNumber, 
                                    String itemNumber, 
                                    String locationId);
    
    /**
     * get count of lots.
     * 
     * @return - total lots in a site
     */
    Long countAll();

    
    //Methods called from web service
    /**
     * Method to Add or Update a lot number. If the lot already exists, then
     * the speakbale lot number and expiration date will be updated. 
     * If the lot number does not exist then it will be created. 
     * A lot is determined by the Site, Lot number, Item Number, and Location Number.
     *  
     * @param lotNumber - lot number to add
     * @param itemNumber - item number lot belongs to.
     * @param locationId - (Optional) Location where lot is stored
     * @param speakableLotNumber - A value representing the lot number and operator can speak
     * @param expirationDate - (Optional) expiration date of lot number
     * @throws BusinessRuleException - business rule exceptions 
     * @throws DataAccessException - Database exceptions
     */
    public void executeCreateUpdateLot(String lotNumber,
                                String itemNumber,
                                String locationId,
                                String speakableLotNumber,
                                Date expirationDate)
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Delete an existing Lot with the specified item and location.
     * @param lotNumber - lot number to delete
     * @param itemNumber - item lot belongs to
     * @param locationId - (Optional) location lot is stored at. 
     * @throws BusinessRuleException - business rule exceptions 
     * @throws DataAccessException - Database exceptions
     */
    public void executeDeleteLot(String lotNumber,
                          String itemNumber,
                          String locationId)
    throws BusinessRuleException, DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 