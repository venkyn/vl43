/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.service;
import com.vocollect.voicelink.core.importer.ImportSetupException;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.ApplicationContextAware;

/**
 * This class update for the import/export setup when a new site gets added or
 * delete.
 * @author Kalpna
 *
 */
public interface ImportSetupManagerRoot 
    extends ApplicationContextAware, BeanFactoryPostProcessor {

    /**
     * Add the import/export for new site.
     * @param siteName name of the site which is just added
     * @throws ImportSetupException wrapper for IO exceptions
     */
    public void addNewSite(String siteName) throws ImportSetupException;

    /**
     * Deletes the import/export for the deleted site.
     * @param siteName name of the site which is just deleted
     * @throws ImportSetupException wrapper for IO exceptions
     */
    public void deleteSite(String siteName) throws ImportSetupException;
    
    /**
     * Looks for import-setup.xml and export-setup.xml.  The files are re-built
     * (refreshed) by querying the sites table and first calling the deleteSite()
     * method for each site (catching all throwable and doing nothing on failure)
     * then calling addNewSite for each site.  During install or upgrade the
     * files begin with no site level data in them and only have either the 
     * <Exporters> </Exporters> or <Importers> </Importers> tags with an empty
     * line in between.  If this is to be done as a "field fix" it is recommended
     * that the users blank out the files in a similar way, although, the deleteSite()
     * calling will accomplish this automatically as well.
     * @throws ImportSetupException wrapper for IO exceptions, and DataAccessExcepiton.
     */
    public void refreshAllSites() throws ImportSetupException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 