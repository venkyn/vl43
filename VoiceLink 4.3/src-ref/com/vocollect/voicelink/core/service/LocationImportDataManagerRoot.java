/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.LocationImportDataDAO;
import com.vocollect.voicelink.core.model.LocationImportData;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for LocationImportData-related operations.
 * 
 * @author jtauberg
 */
public interface LocationImportDataManagerRoot 
    extends GenericManager<LocationImportData, LocationImportDataDAO>, DataProvider {
    
    /**
     * Get a list LocationImportData objects to import for the given site.
     * 
     * @param siteName - name of the site to import Items for.
     * @return - List of LocationImportData that have obj.siteName == siteName
     * @throws DataAccessException - Database Exception
     */
    public List<LocationImportData> listLocationBySiteName(String siteName) throws DataAccessException;

    /**
     * Update all ImportLocationData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 