/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.DeliveryLocationMappingDAO;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingRoot;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;

import java.util.Collection;
import java.util.List;

/**
 * Manager for DeliveryLocationMappings (i.e. rules for mapping assignments to
 * delivery doors).
 * 
 * @author bnorthrop
 */
public interface DeliveryLocationMappingManagerRoot extends
        GenericManager<DeliveryLocationMapping, DeliveryLocationMappingDAO>,
        DataProvider {

    /**
     * @param oldMappingValue
     *            The mapping whose location we want to modify
     * @param newDeliveryLocaiton
     *            The new value
     * @param mappingType
     *            the mapping type
     * @throws DataAccessException
     */
    public void executeModifyDeliveryLocation(String oldMappingValue,
            String newDeliveryLocaiton, DeliveryLocationMappingType mappingType)
            throws DataAccessException;

    /**
     * Modify the delivery locations for a set of DeliveryLocationMapping ids.
     * 
     * @param deliveryLocationMappingIds
     *            ids of the mappings to be changed.
     * @param deliveryLocation
     *            to change mappings to
     * @throws DataAccessException
     *             - exception in the database tier.
     */
    public void executeModifyDeliveryLocation(
            Collection<Long> deliveryLocationMappingIds, String deliveryLocation)
            throws DataAccessException;

    /**
     * @param mappingValue
     *            The mapping value to delete
     * @param mappingType
     *            Mapping type
     */
    public void executeDeleteDeliveryLocation(String mappingValue,
            DeliveryLocationMappingType mappingType) throws DataAccessException;

    /**
     * Return the delivery location given the mapping value (e.g. customer
     * number, route, etc.), or null if there is no mapping defined for that
     * value.
     * 
     * @param mappingValue
     *            - e.g. customer number, route, etc.
     * @return String delivery location.
     * @throws DataAccessException
     *             - exception in the database tier.f *
     */
    public String findDeliveryLocation(String mappingValue)
            throws DataAccessException;

    /**
     * Validate that the rule does not exist.
     * 
     * @param mapping
     *            - mapping to be validated.
     * @throws BusinessRuleException
     *             - if mapping exists.
     * @throws DataAccessException
     *             - any database exception.
     */
    public void validateMappingRuleDoesNotExist(
            DeliveryLocationMappingRoot mapping) throws DataAccessException,
            BusinessRuleException;

    /**
     * Get all the mappings for the current site.
     * 
     * @see com.vocollect.voicelink.core.service.DeliveryLocationMappingManagerRoot#getMappings()
     * @return List delivery location mappings.
     * @throws BusinessRuleException
     *             - if cannot get mapping type for site
     * @throws DataAccessException
     *             - any database exception.
     */
    public List<DeliveryLocationMapping> getMappings()
            throws DataAccessException, BusinessRuleException;

    /**
     * @return List of all the delivery location mappings
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public List<DeliveryLocationMapping> getAllMappings()
            throws DataAccessException, BusinessRuleException;

    /**
     * @param deliveryLocationMappingIds
     *            The delivery location in string
     * @return returns true if the delivery location is referenced by a
     *         completed and in-progress assignments
     */
    public boolean isDeliveryLocationReferenced(Collection<Long> deliveryLocationMappingIds)
            throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 