/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.PrinterDAO;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.printserver.ChaseLabel;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Printer-related operations.
 * 
 * @author Ed Stoll
 */
public interface PrinterManagerRoot extends 
    GenericManager<Printer, PrinterDAO>, DataProvider {

    /**
     * Retrieves all Printers ordered by ID string, or returns null if there are none.
     *
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
    List<Printer> listAllOrderByNumber() throws DataAccessException;
    
        
    /**
     * Retrieves the Printer object for the given Printer number or
     * or returns null if the Printer object is not found.
     * 
     * @param printerNumber - the requested Printer number
     *
     * @return the requested Printer, or null
     * @throws DataAccessException on any failure.
     */
    Printer findByNumber(int printerNumber) throws DataAccessException;
    
    /**
     * @return list of enabled printers
     * @throws DataAccessException on any database exception
     */
    List<Printer> listAllEnabledPrinters() throws DataAccessException;

    /**
     * @param assignments list of assignments
     * @param printer printer object
     */
    /**
     * @param assignments - list of assignments
     * @param printer - printer object
     * @throws BusinessRuleException - any business rule exception
     */
    void generateAssignmentLabels(List<Assignment> assignments, 
                                  Printer printer)
        throws BusinessRuleException;

    /**
     * @param containers to be printed
     * @param printer to print to
     * @throws BusinessRuleException for print server exceptions
     */
    void generateContainerLabels(List<Container> containers, Printer printer)
        throws BusinessRuleException;

    
    /**
     * @param ptsContainers to be printed
     * @param printer to print to
     * @throws BusinessRuleException for print server exceptions
     */
    void generatePtsContainerLabels(List<PtsContainer> ptsContainers, Printer printer)
        throws BusinessRuleException;

    
    /**
     * @param picks to print
     * @param printer to print to
     * @throws BusinessRuleException on print server exceptions
     */
    void generatePickLabels(List<Pick> picks, Printer printer)
        throws BusinessRuleException;
     
    /**
     * @param assignment to print chase labels
     * @return list of chase labels
     */
    List<ChaseLabel> getAssignmentChaseLabels(Assignment assignment);
    
    /**
     * @return the list of printers known to the server
     * @throws DataAccessException on any database exception
     * @throws VocollectException on print server exceptions
     */
    public List<Printer> getServerPrinters() 
        throws DataAccessException, VocollectException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 