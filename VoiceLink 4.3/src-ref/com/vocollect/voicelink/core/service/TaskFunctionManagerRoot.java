/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.TaskFunctionDAO;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for TaskFunction-related operations.
 * 
 * @author Mark Koenig
 */
public interface TaskFunctionManagerRoot extends 
    GenericManager<TaskFunction, TaskFunctionDAO>, DataProvider {

    /**
     * retrieves the item by the number.
     * 
     * @param type - value to retrieve by
     * @return requested task function, list may be empty
     * @throws DataAccessException - database exceptions
     */
    TaskFunction findByType(TaskFunctionType type) throws DataAccessException;
    
    /**
     * @param workgroupId the operator's workgroup id
     * @return list of Task Functions for a Workgroup
     * @throws DataAccessException - database exceptions
     */
    List<TaskFunction> listFunctionsByWorkgroup(Long workgroupId)
        throws DataAccessException;
    
    /**
     * @return list of task function in descending functionNumber order.
     * @throws DataAccessException - database exception
     */
    List<TaskFunction> listAllOrderByNumberDesc()
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 