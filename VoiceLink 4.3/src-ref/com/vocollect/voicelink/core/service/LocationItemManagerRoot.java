/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.LocationItemDAO;
import com.vocollect.voicelink.core.model.LocationItem;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Location/Item mapping-related operations.
 * 
 */
public interface LocationItemManagerRoot extends 
    GenericManager<LocationItem, LocationItemDAO>, DataProvider {

    /**
     * Get the ItemLocation mapping by item number and location-scannedVerification.
     * @param itemNumber itemNumber
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public LocationItem findReplenishmentByItemAndLoc(String itemNumber, String scannedLocation)
        throws DataAccessException;

    /**
     * Get the ItemLocation mapping by location-scannedVerification.
     * @param scannedLocation location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     */
    public List<LocationItem> listReplenishmentByLocation(String scannedLocation)
        throws DataAccessException;

    /**
     * Create a ItemLocation mapping (called from the GUI).
     * @param locationItem location identification
     * @return the mapping, if any
     * @throws DataAccessException on database exception.
     * @throws BusinessRuleException on database exception.
     */
    public Object executeCreateMapping(LocationItem locationItem)
        throws BusinessRuleException, DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 