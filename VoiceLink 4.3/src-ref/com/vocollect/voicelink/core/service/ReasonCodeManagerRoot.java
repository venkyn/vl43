/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.ReasonCodeDAO;
import com.vocollect.voicelink.core.model.ReasonCode;
import com.vocollect.voicelink.core.model.TaskFunctionType;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for TaskFunction-related operations.
 * 
 * @author sfahnestock
 */
public interface ReasonCodeManagerRoot extends 
    GenericManager<ReasonCode, ReasonCodeDAO>, DataProvider {

    /**
     * Retrieves all reason codes for the specified task application.
     *
     * @param taskFunctionType - the task function type to search by
     * @return the requested reason codes, or null
     * @throws DataAccessException on any failure.
     */
    List<ReasonCode> listByTaskFunctionType(TaskFunctionType taskFunctionType) throws DataAccessException;
 
    
    
    /**
     * Retrieves an reason code object by integer reason number or returns null  
     * if there is no such reason code.
     *
     * @param reasonNumber - the number of the License
     * @return the requested reason code object, or null
     * @throws DataAccessException on any failure.
     */
    public ReasonCode findByReasonNumber(int reasonNumber) throws DataAccessException;
    
}




*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 