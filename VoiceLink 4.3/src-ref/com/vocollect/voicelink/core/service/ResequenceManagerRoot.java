/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.GenericManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for resequence-related operations.
 * 
 * @author bnichols
 */
public interface ResequenceManagerRoot {
        
    /**
     * Assigns new sequence numbers to the assignments.
     * @param resequenceList the list of resequence IDs
     * @param results the result set
     * @param manager the manager instance for the model type
     * @throws DataAccessException on database failure
     * @throws BusinessRuleException on business rule violation
     */
    public void executeResequence(ArrayList<Long>  resequenceList,
                                  List<DataObject> results,
                                  GenericManager   manager)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param resequenceList the list of resequence IDs
     * @param results the result set
     * @return the reordered list of results
     */
    public List<DataObject> setupResequenceResults(ArrayList<Long> resequenceList,
                                                   List<DataObject> results);

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 