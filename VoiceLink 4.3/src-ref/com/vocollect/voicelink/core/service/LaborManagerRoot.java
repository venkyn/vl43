/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;

import java.util.Date;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Labor Tracking operations.
 */
public interface LaborManagerRoot {

    /**
     * Getter for the operator labor manager.
     * 
     * @return OperatorLaborManager
     */
    public OperatorLaborManager getOperatorLaborManager();

    /**
     * Getter for the assignment labor manager.
     * 
     * @return AssignmentLaborManager
     */
    public AssignmentLaborManager getAssignmentLaborManager();

    /**
     * Opens an Operator Labor record based on action type.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object.
     * @param actionType - the Operator Labor Record type.<br>
     *            SignOn<br>
     *            SignOff<br>
     *            Selection<br>
     *            PutAway<br>
     *            Replenishment<br>
     *            LineLoading
     * @throws DataAccessException for data access exceptions.
     * @throws BusinessRuleException for business rule exceptions.
     */
    public void openOperatorLaborRecord(Date startTime,
                                        Operator operator,
                                        OperatorLaborActionType actionType)
        throws DataAccessException, BusinessRuleException;

    /**
     * Opens an Operator break labor record.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object
     * @param breakType - BreakType object.
     * @throws DataAccessException on any failure.
     * @throws BusinessRuleException for business rule exceptions.
     */
    public void openBreakLaborRecord(Date startTime,
                                     Operator operator,
                                     BreakType breakType)
        throws DataAccessException, BusinessRuleException;

    /**
     * Closes a break record.
     * 
     * @param endTime - the start time of the labor record.
     * @param operator - the Operator object
     * @throws DataAccessException on any failure.
     * @throws BusinessRuleException - on save failure
     */
    public void closeBreakLaborRecord(Date endTime, Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * open an assignment labor record.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object
     * @param assignment - the assignment to open the labor record for.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if db save fails.
     */
    public void openAssignmentLaborRecord(Date startTime,
                                          Operator operator,
                                          Assignment assignment)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param startTime Start time
     * @param operator the operator
     * @param assignment the assignment
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public void openCycleCountingLaborRecord(Date startTime,
                                             Operator operator,
                                             CycleCountingAssignment assignment)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param startTime Start time
     * @param operator the operator
     * @param route the route
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public void openLoadingLaborRecord(Date startTime,
                                       Operator operator,
                                       LoadingRoute route)
        throws DataAccessException, BusinessRuleException;

    /**
     * close an assignment labor record.
     * 
     * @param endTime - the start time of the labor record.
     * @param assignment - the assignment to open the labor record for.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if db save fails.
     */
    public void closeAssignmentLaborRecord(Date endTime, Assignment assignment)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param endTime the end time
     * @param assignment the Cycle Counting assignment
     * @return the Cycle Counting labor record closed
     * @throws DataAccessException
     * @throws BusinessRuleException
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#closeLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment)
     */
    public CycleCountingLabor closeCycleCountingLaborRecord(Date endTime,
                                                            CycleCountingAssignment assignment,
                                                            Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * Method to close Loading labor record by updating <code>endTime</code>
     * 
     * @param endTime the end time
     * @param route the Loading route
     * @param operator the operator
     * @return the Loading labor record closed
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public LoadingRouteLabor closeLoadingLaborRecord(Date endTime,
                                                     LoadingRoute route,
                                                     Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * Update the labor statistics for the put away labor records.
     * 
     * @param operator - the operator that has the license
     * @param license - the license to update.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - on save failure
     */
    public void updatePutAwayLaborStatistics(Operator operator, License license)
        throws DataAccessException, BusinessRuleException;

    /**
     * Update the labor statistics for the replenishment labor records.
     * 
     * @param operator - the operator that has the replenishment
     * @param replenishment - the replenishment assignment to update.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - on save failure
     */
    public void updateReplenishmentLaborStatistics(Operator operator,
                                                   Replenishment replenishment)
        throws DataAccessException, BusinessRuleException;

    /**
     * Update the labor statistics for lineload labor records.
     * 
     * @param operator - the operator that has performed the lineload
     * @param carton - the carton details to update.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - on save failure
     */
    public void updateLineLoadLaborStatistics(Operator operator, Carton carton)
        throws DataAccessException, BusinessRuleException;

    /**
     * Update the labor statistics for cycle counting labor records.
     * 
     * @param operator - the operator that has performed the cycle counting
     * 
     * @param assignment - the cycle counting assignment
     * 
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - on save failure
     */
    public void updateCycleCountingLaborStatistics(Operator operator,
                                                   CycleCountingAssignment assignment)
        throws DataAccessException, BusinessRuleException;

    /**
     * Gets the earliest end time for a labor record.
     * 
     * @param operatorLaborId the id of the labor record
     * @return the earliest end time for the labor record
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if db get fails.
     */
    public Date getEarliestEndTime(Long operatorLaborId)
        throws DataAccessException, BusinessRuleException;

    /**
     * Gets the earliest end time for a labor record.
     * 
     * @param operator the operator to find the time for.
     * @return the earliest end time for the labor record
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if db get fails.
     */
    public Date getEarliestEndTime(Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * Updates the end time of a labor record.
     * 
     * @param operatorLabor the labor record to modify
     * @param endTime the new end time
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if db save fails.
     */
    public void executeModifyEndTime(OperatorLabor operatorLabor, Date endTime)
        throws DataAccessException, BusinessRuleException;

    /**
     * Closes a PTS license labor record.
     * 
     * @param endTime - time the labor record is being closed.
     * @param license - the license to close
     * @throws DataAccessException -if db error
     * @throws BusinessRuleException - if db save fails.
     */
    public void closePtsLicenseLaborRecord(Date endTime, PtsLicense license)
        throws DataAccessException, BusinessRuleException;

    /**
     * open a PTS license labor record.
     * 
     * @param startTime - the start time of the labor record.
     * @param operator - the Operator object
     * @param license - the licese to open the labor record for.
     * @throws DataAccessException - if db error.
     * @throws BusinessRuleException - if business logic fails.
     */
    public void openPtsLicenseLaborRecord(Date startTime,
                                          Operator operator,
                                          PtsLicense license)
        throws DataAccessException, BusinessRuleException;

    /**
     * Getter for the PTS license labor manager.
     * 
     * @return PtsLicenseLaborManager
     */
    public PtsLicenseLaborManager getPtsLicenseLaborManager();

    /**
     * Method to update Loading Labor record. This is normally invoked for every
     * container loaded.
     * @param operator - The operator loading the container
     * @param route - The loading route to which container belongs
     * @param status - The container load status sent by the terminal
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    public void updateLoadingLabor(Operator operator,
                                   LoadingRoute route,
                                   LoadingContainerStatus status)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param operator the operator for which last work is calculated
     * @param operatorLabor the operator labor record to consider
     * @return the last date with time he work
     * @throws DataAccessException when data access happens
     */
    public Date lastWorkOnLaborRecord(Operator operator,
                                      OperatorLabor operatorLabor)
        throws DataAccessException;
    
    /**
     * This method will be calculating the date and time at which, the operator started
     * working again after coming back from a break.
     * 
     * @param operator - the operator for which next work started is calculated
     * @param operatorLabor the operator labor record to consider
     * @return the next date with time he started working again
     * @throws DataAccessException when data access happens
     */
    public Date nextWorkStarted(Operator operator,
                                      OperatorLabor operatorLabor)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 