/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.dao.LocationItemImportDataDAO;
import com.vocollect.voicelink.core.model.LocationItemImportData;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for LocationItemImportData-related operations.
 * 
 * @author jtauberg
 */
public interface LocationItemImportDataManagerRoot 
    extends GenericManager<LocationItemImportData, LocationItemImportDataDAO>,
        DataProvider {
    
    /**
     * Get a list of LocationItemImportData objects to import for the given site.
     * 
     * @param siteName - name of the site to import LocationItems for.
     * @return - List of LocationItemImportData that have obj.siteName == siteName
     * @throws DataAccessException - Database Exception
     */
    public List<LocationItemImportData> listLocationItemBySiteName(String siteName)
        throws DataAccessException;

    /**
     * Update all LocationItemImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 