/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.scheduling;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Report;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.scheduling.ReportSchedulerJob;
import com.vocollect.epp.security.SecurityErrorCode;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.service.ReportManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.JasperReportWrapper;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.ReportWrapper;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.SystemConfigurationAction;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.service.OperatorTeamManager;

import static com.vocollect.epp.util.ReportUtilities.DATE_FORMAT;
import static com.vocollect.epp.util.ReportUtilities.END_DATE;
import static com.vocollect.epp.util.ReportUtilities.ENUM;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_ALL;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_IDENTIFIERS;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_IDENTIFIERS_ALL;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_TEAM;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_TEAM_ID;
import static com.vocollect.epp.util.ReportUtilities.PARAM_CURRENT_SITE_CONTEXT;
import static com.vocollect.epp.util.ReportUtilities.PARAM_CURRENT_USER;
import static com.vocollect.epp.util.ReportUtilities.REPORT_NAME;
import static com.vocollect.epp.util.ReportUtilities.SESSION_VALUE;
import static com.vocollect.epp.util.ReportUtilities.SITE;
import static com.vocollect.epp.util.ReportUtilities.START_DATE;
import static com.vocollect.epp.util.ReportUtilities.TIME_ZONE;
import static com.vocollect.epp.util.ReportUtilities.getReportValuesMap;
import static com.vocollect.epp.util.ReportUtilities.setReportOperatorIdentifiers;
import static com.vocollect.epp.util.ReportUtilities.setReportOperatorTeams;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_HIBERNATE_SESSION_PARAM;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_LOCALE;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_SOURCE_EXTENSION;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_TIMEZONE;
import static com.vocollect.epp.util.ReportWrapperRoot.REPORT_PATH_BASE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Multipart;
import javax.mail.Transport;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.acegisecurity.Authentication;


import org.acegisecurity.context.SecurityContextHolder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.i18n.LocaleContextHolder;


/**
 * This class implements scheduling of VoiceLink Reports.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 * @author kudupi
 */
public class VoiceLinkReportJobManagerImplRoot extends ReportSchedulerJob {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(VoiceLinkReportJobManagerImplRoot.class);

    private static final int BYTE_ARRAY_SIZE = 1024;

    private static final int DAYS_IN_A_WEEK = 7;

    private static final int DAILY_FREQUENCY = 1;

    private static final int WEEKLY_FREQUENCY = 2;

    private static final int MONTHLY_FREQUENCY = 3;

    public static final String LIST_ALL_DESCRIPTION = "report.parameter.all";
    
    private static final int FIVE_DIGIT_LOCALE_LENGTH = 5;
    
    private static final int TWO_DIGIT_LOCALE_LENGTH = 2;

    // Start date of the Report.
    private String startDate = null;

    // End date of the Report.
    private String endDate = null;

    // Report object for Scheduler.
    private Report report;

    // Manager for setting Notifications.
    private NotificationManager notificationManager;

    // Manager for accessing Reports related information.
    private ReportManager reportManager;

    // SystemProperty Manager to access system properties.
    private SystemPropertyManager systemPropertyManager;

    // OperatorTeam Manager to access operatorTeams properties.
    private OperatorTeamManager operatorTeamManager;

    // Context for Site.
    private SiteContext siteContext;

    /**
     * Getter for report property.
     * @return report - Report object.
     */
    public Report getReport() {
        return report;
    }

    /**
     * Setter for report property.
     * @param report the new Report value.
     */
    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * Getter for systemPropertyManager property.
     * @return systemPropertyManager - SystemPropertyManager object.
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for systemPropertyManager property.
     * @param systemPropertyManager the new SystemPropertyManager value.
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return notificationManager - NotificationManager object.
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }


    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new NotificationManager value.
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for operatorTeamManager property.
     * @return operatorTeamManager - OperatorTeamManager Object.
     */
    public OperatorTeamManager getOperatorTeamManager() {
        return operatorTeamManager;
    }

    /**
     * Setter for operatorTeamManager property.
     * @param operatorTeamManager the new OperatorTeamManager Object.
     */
    public void setOperatorTeamManager(OperatorTeamManager operatorTeamManager) {
        this.operatorTeamManager = operatorTeamManager;
    }

    /**
     * Getter for the siteContext property.
     * @return siteContext - SiteContext Object.
     */
    @Override
    public SiteContext getSiteContext() {
        return this.siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    @Override
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * Getter for the reportManager property.
     * @return reportManager - ReportManager object.
     */
    public ReportManager getReportManager() {
        return reportManager;
    }

    /**
     * Setter for the reportManager property.
     * @param reportManager the new ReportManager value.
     */
    public void setReportManager(ReportManager reportManager) {
        this.reportManager = reportManager;
    }

    /**
     * Getter for startDate property.
     * @return startDate String.
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Setter for startDate property.
     * @param startDate String.
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for endDate property.
     * @return endDate String.
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Setter for endDate property.
     * @param endDate String.
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * This Quartz job activates the Report Scheduling Process.
     * @param context describes details corresponding to the current invocation.
     * @throws JobExecutionException if an error occurs during job execution
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     * @author kudupi
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        try {
            log.debug("####REPORT SCHEDULE: Starting Report Scheduling Process:::");
            log.info("####REPORT SCHEDULE: Number of  Reports in the System = " + getReportManager().getAll().size());
            
            for (Report reportObj : getReportManager().getAll()) {
                SiteContext siteContextObj = SiteContextHolder.getSiteContext();
                Site siteValue = siteContextObj.getSite(reportObj);
                Date reportGenerationDate = reportObj.getTime();
                if (reportGenerationDate != null) {
                    log.info("#### REPORT SCHEDULER: Report Generation Date of the report "
                        + reportObj.getName() + " is " + reportGenerationDate);
                    Calendar siteCal = Calendar.getInstance();
                    Date siteDate = DateUtil.convertDateTimeZone(siteCal,
                        siteCal.getTimeZone(), siteValue.getTimeZone());
                    boolean startScheduler = checkForScheduleStart(siteDate,
                            reportObj.getFrequency().getValue());
                    
                    if (startScheduler) {
                        if (reportGenerationDate.compareTo(siteDate) <= 0) {
                            log.info("##### REPORT SCHEDULER: Report = " + reportObj.getName()
                                    + " is now scheduled to run and get generated.");
                            setReport(reportObj);
                            setSiteContext(siteContextObj);
                            log.info("##### REPORT SCHEDULER: Site ID = " + siteValue.getId()
                                    + " Site Name = " + siteValue.getName());
                            siteContextObj.setCurrentSite(siteValue);
                            siteContextObj.setFilterBySite(true);
                            SiteContextHolder.setSiteContext(siteContextObj);
                            File reportFile = new File(ReportSchedulerJob.class.getProtectionDomain().getCodeSource()
                                    .getLocation().getPath());
                            String filePath = reportFile.getCanonicalPath();
                            filePath = filePath.replaceAll("%20", " ");
                            String absolutePath = filePath.replace(filePath.substring(
                                    filePath.indexOf("WEB-INF") - 1, filePath.length()), REPORT_PATH_BASE);
                            String reportName = reportObj.getType().getReportTypeName();
                            if (log.isInfoEnabled()) {
                                log.info("#### REPORTSCHEDULERJOB: Generating report: "
                                        + reportName + " from " + absolutePath);
                            }
                            ReportWrapper reportWrapper = new JasperReportWrapper(
                                absolutePath, reportName + JASPER_REPORT_SOURCE_EXTENSION);

                            // Check and initialize if the report exists
                            if (!reportWrapper.reportExists(absolutePath, reportName
                                + JASPER_REPORT_SOURCE_EXTENSION)) {
                                log.warn("#### REPORTSCHEDULERJOB: Report definition " + reportName
                                    + JASPER_REPORT_SOURCE_EXTENSION + " NOT found at: "
                                    + absolutePath);
                            }

                            Map<String, Object> reportValues = getReportValues();
                            Set<ReportParametersDTO> reportParams = convertToDTO(reportObj
                                .getReportParameters());

                            // Build Parameters value map
                            Map<String, Object> values = getReportValuesMap(reportWrapper
                                .getParameters(), reportParams, reportValues);
                            Locale locale = convertStringToLocale(reportObj.getLanguage()); 
                            values.put(JASPER_REPORT_LOCALE, locale);
                            values.put(JASPER_REPORT_TIMEZONE, SiteContextHolder.getSiteContext().getCurrentSite()
                                .getTimeZone());
                            values.put(TIME_ZONE, Calendar.getInstance().getTimeZone());
                            LocaleContextHolder.setLocale(locale, true);
                            setReportOperatorIdentifiers(reportParams, values);
                            setReportOperatorTeams(reportParams, values);
                            setAllTeamOperators(values);

                            Object dataSource = getDataSource(values); //Set data source

                            // get render format
                            String renderFormat = reportObj.getFormat().toString();
                            
                            if (log.isDebugEnabled()) {
                                log.debug("Invoking report generation for report:" + reportName
                                    + ", format:" + renderFormat + ", language:"
                                    + reportObj.getLanguage());
                            }
                            
                            try {
                                values.put(START_DATE, getDisplayDateforInterval(true));
                                values.put(END_DATE, getDisplayDateforInterval(false));
                            } catch (Exception e) {
                                log.warn("Error parsing Start/end dates");
                            }
                            InputStream reportInputStream = reportWrapper.generateReport(
                                values, renderFormat, locale.getLanguage(), dataSource);
                            if (log.isInfoEnabled()) {
                                log.info("Report:" + reportObj.getName() + " generated.");
                            }
                            log.debug("#### REPORT SCHEDULER: Report Input Stream received successfully.");
                            try {
                                log.debug("#### REPORT SCHEDULER: Creating file from the Input Stream received.");
                                log.debug("#### Temp Path : "+System.getProperty("java.io.tmpdir"));
                                File reportOuputFile = new File(System.getProperty("java.io.tmpdir")+"/Report."
                                        + reportObj.getFormat().toString());
                                OutputStream out = new FileOutputStream(reportOuputFile);
                                byte[] buf = new byte[BYTE_ARRAY_SIZE];
                                int len;
                                if (reportInputStream != null) {
                                    log.debug("#### REPORT SCHEDULER: Report Input Stream is not null");
                                    while ((len = reportInputStream.read(buf)) > 0) {
                                        out.write(buf, 0, len);
                                    }
                                    out.close();
                                    reportInputStream.close();
                                    log.debug("#### REPORT SCHEDULER: File created successfully........... " +
                                                "Now sending email.");
                                    sendMail(reportObj, reportOuputFile);
                                    log.debug("#### REPORT SCHEDULER: Updating the time field.");
                                    reportObj.setLastRun(new Date());
                                    Date nextRun = incrementDate(reportObj.getTime(), 
                                            reportObj.getFrequency().toValue());
                                    reportObj.setTime(nextRun);
                                    log.debug("#### REPORT SCHEDULER: New Date field value = " + reportObj.getTime().toString());
                                    
                                    log.debug("#### REPORT SCHEDULER: Report object updated successfully.");
                                    List<Report> reportList = new ArrayList<Report>();
                                    reportList.add(reportObj);
                                    log.debug("#### REPORT SCHEDULER: Saving the updated report object.");
                                    reportManager.save(reportList);
                                    boolean fileDelete = reportOuputFile.delete();
                                    log.info("#### REPORT SCHEDULER: Temp report file created is deletion status is = "
                                            + fileDelete);
                                } else {
                                    log.warn("#### REPORT SCHEDULER: Report Input Stream is null.");
                                }
                            } catch (IOException ioe) {
                                ioe.printStackTrace();
                            }
                        } else {
                            log.info("##### REPORT SCHEDULER: Report = " + reportObj.getName() +
                                    " is not following currently under scheduling criteria.");
                        }
                    } else {
                        String message = (reportObj.getFrequency().getValue() == WEEKLY_FREQUENCY) ?
                                "Every Monday as it is a Weekly Report." : "" +
                                    "1st of Every Month as it is a Montly Report.";
                        log.info("#### REPORT SCHEDULER: " + reportObj.getName() +
                                " will be generated on " + message);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("Exception encountered during report scheduler "
                + "execution: " + e.getMessage());
            try {
                //Modify the siteContext currentTagId to point to the system "site"
                getSiteContext().setToSystemMode();
                SiteContextHolder.setSiteContext(getSiteContext());

                // Create Report Scheduler Notification
                generateFailureNotification(e);

            } catch (Exception exc) {
                log.error("Error creating Notification",
                    SystemErrorCode.NOTIFICATION_FAILURE, exc);
            }
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

    /**
     * @param isStartDate 
     *               - boolean value if the date is start date
     *
     * @return Date  value
     */
    private Date getDisplayDateforInterval(boolean isStartDate) {
        Date displayDate = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        if (isStartDate) {
            cal.setTime(this.report.getInterval().getStartDate());
        } else {
            cal.setTime(this.report.getInterval().getEndDate());
        }
        displayDate = DateUtil.convertDateTimeZone(cal, TimeZone.getDefault(),
                SiteContextHolder.getSiteContext().getCurrentSite()
                        .getTimeZone());
        return displayDate;
    }
    
    /**
     * Method to generate error notifications when report scheduler fails to
     * generate or email report
     * @param ex Exception encountered because of which job scheduler did not
     *            execute
     * @throws Exception thrown if notification could not be generated
     */
    private void generateFailureNotification(Exception ex) throws Exception {
       LOPArrayList lop = new LOPArrayList();
       lop.add("notification.report.scheduler.status",
           "notification.report.scheduler.status.email.fail");
        if ((ex instanceof JobExecutionException)) {
            JobExecutionException exceptionObject = (JobExecutionException)ex;
            if (exceptionObject.getMessage().contains("SMTP")) {
                lop.add("notification.exception.message", "notification.report.scheduler.smtp.configuration");
            } else if (exceptionObject.getMessage().contains(
                "Authentication Required")) {
               lop.add("notification.exception.message", "notification.report.scheduler.invalid.user");
            } else {
               lop.clear();
               lop.add("notification.report.scheduler.error.code", exceptionObject.getErrorCode());
               lop.add("notification.exception.message", "notification.report.scheduler.error.message");
           }
        }

        getNotificationManager().createNotification(
            "systemConfiguration.reportscheduler.title",
            "notification.report.scheduler.failed",
            NotificationPriority.WARNING, new Date(), "100",
            "notification.column.keyname.Application.2", lop);
    }

/**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    @Override
    protected void internalInterrupt() {

        // Indicate Purge Archive is failing in the log file
        log.debug("####REPORT SCHEDULER: Stopping Report Scheduler Process:::");

    }

    /**
     * Method to add identifiers of all operators to the values map for querying
     * archive database
     * @param values Parameter name-value map
     * @throws DataAccessException Thrown when operator team is not retrieved
     *             using operatorTeamManager
     */
    @SuppressWarnings("unchecked")
    private void setAllTeamOperators(Map<String, Object> values)
        throws DataAccessException {

        Set<String> operatorIdAll = new HashSet<String>();

        Set<String> operatorId = (Set<String>) values.get(OPERATOR_IDENTIFIERS);
        String op = (String) values.get(OPERATOR);
        if (!StringUtil.isNullOrEmpty(op)) {
            operatorIdAll.addAll(operatorId);
        }

        Long opTeam = (Long) values.get(OPERATOR_TEAM);
        if (opTeam != null) {
            Set<Long> operatorTeamIds = (Set<Long>) values
                .get(OPERATOR_TEAM_ID);
            for (Long operatorTeamId : operatorTeamIds) {
                OperatorTeam operatorTeam = this.operatorTeamManager
                    .get(operatorTeamId);
                Set<Operator> operators = operatorTeam.getOperators();
                for (Operator operator : operators) {
                    operatorIdAll.add(operator.getOperatorIdentifier());
                }
            }
        }

        String operatorIds = null;
        if (operatorIdAll.isEmpty()) {
            operatorIdAll.add(" ");
        } else {
            operatorIds = operatorIdAll.iterator().next();
        }
        values.put(OPERATOR_IDENTIFIERS_ALL, operatorIdAll);
        values.put(OPERATOR_ALL, operatorIds);
    }

    /**
     * Method to get the datasource object
     * @param values Map of report values <code>key</code> Parameter name <br>
     *            <code>value</code> Parameter value
     * @return Datasource object. Could be either of type
     *         ReportDataSourceManager or null
     * @throws VocollectException Thrown when error encountered while getting
     *             datasource of type ReportDataSourceManager or bean associated
     *             object is not of either type SessionFactory and
     *             ReportDataSourceManager
     */
    private Object getDataSource(Map<String, Object> values)
        throws VocollectException {

        if (log.isDebugEnabled()) {
            log.debug("#### REPORTSCHEDULERJOB :::: Get data source bean with bean id="
                    + this.report.getType().getSessionBeanId()
                + ", report=" + this.report.getName());
        }
        Object dataSource = getReportDataSourceBean();
        if (dataSource instanceof SessionFactory) {
            // Set hibernate session
            Session session = ((SessionFactory) dataSource).openSession();
            values.put(JASPER_HIBERNATE_SESSION_PARAM, session);
            dataSource = null;
            if (log.isInfoEnabled()) {
                log.info("#### REPORTSCHEDULERJOB :::: Hibernate session with id : "
                        + this.report.getType().getSessionBeanId()
                    + " to be used, report=" + this.report.getName());
            }
        } else if ((dataSource instanceof ReportDataSourceManager)) {
            try {
                dataSource = ((ReportDataSourceManager) dataSource)
                    .getDataSource(values);
                if (log.isInfoEnabled()) {
                    log.info("#### REPORTSCHEDULERJOB :::: Datasource = ReportDataSourceManager"
                        + " , report=" + this.report.getName());
                }
            } catch (Exception ex) {
                throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
            }
        } else {
            log.error(
                "#### REPORTSCHEDULERJOB :::: Unknown session id:"
                    + this.report.getType().getSessionBeanId() + " for report "
                    + this.report.getName(),
                ReportError.DATASOURCE_BEAN_UNKOWN_ERROR);
            throw new VocollectException(
                ReportError.DATASOURCE_BEAN_UNKOWN_ERROR);
        }

        return dataSource;
    }

    /**
     * Find the bean data source specified for report.
     * @return - Bean object obtained from applicationContext
     * @throws VocollectException - If required bean is not found in the
     *             applicationContext
     */
    private Object getReportDataSourceBean() throws VocollectException {
        // Get the hibernate session from our context
        if (!getApplicationContext().containsBean(this.report.getType()
            .getSessionBeanId())) {
            throw new VocollectException(
                ReportError.DATASOURCE_BEAN_NOTFOUND_ERROR);
        }
        return getApplicationContext().getBean(this.report.getType()
            .getSessionBeanId());
    }

    /**
     * Utility method to collect all user submitted parameters values in a map
     * of values to be used for invocation of report generation
     * @return Map of user submitted values. <code>key</code> Parameter name <br>
     *         <code>value</code> Parameter value
     */
    private Map<String, Object> getReportValues() {
        Map<String, Object> values = new HashMap<String, Object>();
        if (SiteContextHolder.getSiteContext() != null) {
            values.put(PARAM_CURRENT_SITE_CONTEXT, SiteContextHolder.getSiteContext().getCurrentSite().getName());
            values.put(SITE, SiteContextHolder.getSiteContext().getCurrentSite().getId().toString());
        }
        if (getCurrentUser() != null) {
            values.put(PARAM_CURRENT_USER, getCurrentUser().getName());
        }
        values.put(SESSION_VALUE, null);
        values.put(START_DATE, parseIntervalDate(true));
        values.put(END_DATE, parseIntervalDate(false));
        values.put(REPORT_NAME, this.report.getName());
        values.put(ENUM, null);
        return values;
    }

    /**
     * Helper function to get the start or end date from either the specified
     * date or the interval.
     * @param isStartDate - boolean to determine start or end
     * @return String - the interval date
     */
    private String parseIntervalDate(boolean isStartDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        if (isStartDate) {
           if (this.getStartDate() != null) {
               return (this.report.getInterval().getStartDate().toString() + "-00");
           } else {
               return (sdf.format(this.report.getInterval().getStartDate()));
           }
        } else {
            if (this.getEndDate() != null) {
                return (this.report.getInterval().getEndDate().toString() + "-00");
            } else {
                return (sdf.format(this.report.getInterval().getEndDate()));
            }
        }
    }

    /**
     * Method to convert ReportParameter object to ReportParametersDTO object, so
     * that it can be used in util classes
     * @param reportParameters Set of ReportParameter objects associated with the
     *            report object
     * @return Set of ReportParametersDTO objects adapted from ReportParameter
     *         objects
     */
    private Set<ReportParametersDTO> convertToDTO(Set<ReportParameter> reportParameters) {
        Set<ReportParametersDTO> paramList = new HashSet<ReportParametersDTO>(
            reportParameters.size());
        ReportParametersDTO param;
        for (ReportParameter reportParam : reportParameters) {

            param = new ReportParametersDTO();
            param.setDescription(reportParam.getDescriptiveText());
            param.setName(reportParam.getReportTypeParameter()
                .getParameterName());
            param.setValue(reportParam.getValue());
            param.setValueClass(reportParam.getClass());
            paramList.add(param);
        }

        return paramList;
    }

    /**
     * Get the current user of the system.
     *
     * @return the User associated with this session.
     */
    public User getCurrentUser() {
        try {
            Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
            if (auth != null) {
                log.info("##### ReportSchedulerJob :::: Current User got from context is = "
                        + ((User) auth.getPrincipal()).getName());
                return (User) auth.getPrincipal();
            } else {
                log.error(
                        "##### ReportSchedulerJob :::: Current user not found in context holder",
                        SecurityErrorCode.NO_CURRENT_USER);
                    return null;
            }
        } catch (Throwable t) {
            // Make sure no exceptions are thrown out of this, to avoid
            // causing Freemarker to barf.
            log.error(
                "##### ReportSchedulerJob :::: Unable to retrieve current user",
                SecurityErrorCode.NO_CURRENT_USER, t);
            return null;
        }
    }

    /**
     * This method increments the date and returns the date
     * value for next execution of the report based on the
     * frequency of the Report.
     *
     * @param date - Current Date of Execution.
     * @param duration - Frequency of the Report.
     * @return date - Next Date of Execution.
     */
    private Date incrementDate(Date date, int duration) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (duration == DAILY_FREQUENCY) {
            c.add(Calendar.DATE, 1);
        } else if (duration == WEEKLY_FREQUENCY) {
            c.add(Calendar.DATE, DAYS_IN_A_WEEK);
        } else if (duration == MONTHLY_FREQUENCY) {
            c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH) , c.get(Calendar.DATE));
            c.add(Calendar.DATE, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
        return c.getTime();
    }

    /**
     * This method decides if the scheduler should start generating a report.
     * If report frequency is:
     * 1) DAILY - Its always true.
     * 2) WEEKLY - Will check if the day is Monday.
     * (For WEEKLY frequency, reports are generated on every Monday).
     * 3) MONTHLY - Will check if the day is 1st day of the Month.
     * (For MONTHLY frequency, the reports are generated on the 1st day of the Month.)
     *
     * @param date - Current Date of Execution.
     * @param duration - Frequency of the Report.
     * @return startScheduleReport - boolean value of the result. If true, the report generation will start
     * .
     */
    private boolean checkForScheduleStart(Date date, int duration) {
        boolean startScheduleReport = false;
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        if (duration == DAILY_FREQUENCY) {
            startScheduleReport = true;
        } else if (duration == WEEKLY_FREQUENCY) {
            int weekday = c.get(Calendar.DAY_OF_WEEK);
            if (weekday == Calendar.MONDAY) {
                startScheduleReport = true;
            }
        } else if (duration == MONTHLY_FREQUENCY) {
            int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
            if (dayOfMonth == 1) {
                startScheduleReport = true;
            }
        }
        return startScheduleReport;
    }

    /**
     * This method will send report as mail to the recipients.
     * @param reportValue Report object.
     * @param file File object.
     */
    private void sendMail(Report reportValue, File file) throws JobExecutionException {
        log.debug("#### reportValue SCHEDULER: Inside sendMail method.");
        Properties props = System.getProperties();
        String smtpHostname = "";
        String fromAddress  = "";

        try {
            smtpHostname = getSystemPropertyManager().
            findByName(SystemConfigurationAction.SMTP_HOST).getValue();
            fromAddress  = getSystemPropertyManager().
            findByName(SystemConfigurationAction.SMTP_USERNAME).getValue();
        } catch (DataAccessException dae) {
            dae.printStackTrace();
        }
        log.debug("#### REPORT SCHEDULER: Value of smtpHostname = " + smtpHostname +
                " Value of fromAddress  = " + fromAddress);
        if ((smtpHostname != null && smtpHostname.trim().length() != 0) &&
                (fromAddress != null && fromAddress.trim().length() != 0)) {
            props.put("mail.smtp.host", smtpHostname);
            javax.mail.Session session = javax.mail.Session.getInstance(props, null);

            try {
                // create a message
                MimeMessage msg = new MimeMessage(session);
                msg.setFrom(new InternetAddress(fromAddress));
                if (reportValue.getEmails() != null && reportValue.getEmails() != "") {
                    String[] emailIDs = reportValue.getEmails().split(",");
                    InternetAddress[] address = new InternetAddress[emailIDs.length];
                    for (int i = 0; i < emailIDs.length; i++) {
                        address[i] = new InternetAddress(emailIDs[i]);
                    }
                    msg.setRecipients(Message.RecipientType.TO, address);
                    msg.setSubject(reportValue.getSubject(), "UTF-8");
                    
                    // create and fill the first message part
                    MimeBodyPart mbp1 = new MimeBodyPart();
                    mbp1.setText(reportValue.getName() + "", "UTF-8");

                    // create the second message part
                    MimeBodyPart mbp2 = new MimeBodyPart();

                    // attach the file to the message
                    FileDataSource fds = new FileDataSource(file);
                    mbp2.setDataHandler(new DataHandler(fds));
                    mbp2.setFileName(fds.getName());

                    // create the Multipart and add its parts to it
                    Multipart mp = new MimeMultipart();
                    mp.addBodyPart(mbp1);
                    mp.addBodyPart(mbp2);
                    
                    // add the Multipart to the message
                    msg.setContent(mp);

                    // set the Date: header
                    msg.setSentDate(new Date());

                    // send the message
                    Transport.send(msg);

                    log.debug("#### REPORT SCHEDULER: Email sent successfully.");
                } else {
                    log.warn(" In sendMail method. There are no send recipients" +
                             ". Not sending mail.");
                }
            } catch (MessagingException mex) {
                log.error("#### REPORT SCHEDULER: Error during Report Scheduler process while sending email.",
                        SystemErrorCode.EMAIL_FAILURE, mex);
                mex.printStackTrace();
                Exception ex = null;
                if ((ex = mex.getNextException()) != null) {
                    ex.printStackTrace();
                }
                throw new JobExecutionException("Email Exception - " + mex.getMessage());
            }
        } else {
            log.error("#### REPORT SCHEDULER: In send mail method. SMTP details not available. Not sending mail.",
                    SystemErrorCode.EMAIL_FAILURE);
            throw new JobExecutionException("SMTP details not available");
        }
    }
    
    /**
     * Method to make a locale from a string.
     * @param stringToConvert - String value from the report object
     * @return a Locale for the specified string.
     */
    public Locale convertStringToLocale(String stringToConvert) {
        String language;
        String country;
        Locale convertedLocale;

        if (stringToConvert.length() == TWO_DIGIT_LOCALE_LENGTH) {
            // 2 Digit Locale (xx)
            convertedLocale = new Locale(stringToConvert);
        } else if (stringToConvert.length() == FIVE_DIGIT_LOCALE_LENGTH) {
            String[] localeSplits = new String[2];
            // 4 Digit Locale (xx_YY)
            if (stringToConvert.contains("-")) {
                localeSplits = stringToConvert.split("-");
            } else if (stringToConvert.contains("_")) {
                localeSplits = stringToConvert.split("_");
            }
            language = localeSplits[0];
            country = localeSplits[1];
            convertedLocale = new Locale(language, country);
        } else {
            // Invalid locale default to normal
            convertedLocale = new Locale("en", "US");
        }
        return convertedLocale;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 