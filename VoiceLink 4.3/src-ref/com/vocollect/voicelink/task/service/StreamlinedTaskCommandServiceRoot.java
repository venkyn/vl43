/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.service;


/**
 * This interface streamlines the TaskCommandService interface by providing a
 * simplified executeCommand method.
 *
 * @author sthomas
 */
public interface StreamlinedTaskCommandServiceRoot {

    /**
     * Dispatches a TaskCommand, maps the values passed to its arguments,
     * and executes it.
     *
     * @param commandName - Command identifier.
     * @param values - Command arguments.
     * @return - The result (response) of the Command execution.
     * @throws Exception - Expect any type of exception.
     */
     String executeCommand(String commandName, String[] values) throws Exception;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 