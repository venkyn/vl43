/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.service;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommand;


/**
 *
 *
 * @author ddoubleday
 */
public interface TaskCommandTransactionalServiceRoot extends TaskCommandService {

    /**
     * Execute the specified command in a read-only transaction.
     * @param cmd the command to execute inside a read-only transaction.
     * @return the Command Response, or null if no response is required.
     * @throws Exception on unexpected error conditions.
     */
    Response executeReadOnlyCommand(TaskCommand cmd) throws Exception;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 