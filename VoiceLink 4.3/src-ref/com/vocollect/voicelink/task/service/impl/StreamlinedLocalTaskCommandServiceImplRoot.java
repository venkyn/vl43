/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.service.impl;

import com.vocollect.voicelink.task.command.CommandDispatcher;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommand;
import com.vocollect.voicelink.task.service.StreamlinedTaskCommandService;

import java.util.Formatter;
import java.util.List;

/**
 * Streamlines the LocalTaskCommandService interface by providing
 * a simplified executeCommand method.
 *
 * @author sthomas
 */
public class StreamlinedLocalTaskCommandServiceImplRoot
    extends LocalTaskCommandServiceImpl
    implements StreamlinedTaskCommandService {

    // [start] constants

    // The character used to separate formatting from field identifiers in the host response.
    private static final String FORMAT_SEPARATOR = ":";

    // The character(s) separating one field value to another in the response going to the client.
    private static final String FIELD_SEPARATOR  = ",";

    // [end]

    // [start] configurable values

    // [start] endOfRecord
    // The character sequence to send to the client to signify the end of record.
    private String endOfRecord = "\r\n";

    /**
     * Sets the character sequence to send to the client to signify the end of record.
     * @param eor - The end of record sequence.
     */
    public void setEndOfRecord(String eor) {
        this.endOfRecord = eor;
    }

    /**
     * Returns the character sequence to send to the client to signify the end of record.
     * @return The character sequence.
     */
    private String getEndOfRecord() {
        return this.endOfRecord;
    }
    // [end]

    // [start] endOfTransmission
    // The sequence of characters to send to the client to signify the end of the transmission.
    private String endOfTransmission = "\n\n";

    /**
     * Sets the characters indicating the end of transmission.
     * @param theEndOfTransmitSequence - The characters (e.g. "\n\n") to send for end of
     *            transmission.
     */
    public void setEndOfTransmission(String theEndOfTransmitSequence) {
        this.endOfTransmission = theEndOfTransmitSequence;
    }

    /**
     * Gets the characters indicating the end of transmission.
     * @return The characters (e.g. "\n\n") to send for end of transmission.
     */
    private String getEndOfTransmission() {
        return this.endOfTransmission;
    }
    // [end]

    // [start] confirmationByte
    // The single byte to send to the client after processing an ODR.
    private Byte confirmationByte = 0;

    /**
     * Sets the ODR confirmation byte used in conversing with the client.
     * @param theConfirmationByte - The single byte communicated to the client after processing an
     *            ODR.
     */
    public void setODRConfirmationByte(Byte theConfirmationByte) {
        this.confirmationByte = theConfirmationByte;
    }

    /**
     * Returns the ODR confirmation byte used in conversing with the client.
     * @return The single byte communicated to the client after processing an ODR.
     */
    private Byte getODRConfirmationByte() {
        return this.confirmationByte;
    }
    // [end]

    // [end] configurable values

    // [start] dispatcher
    private CommandDispatcher dispatcher = null;

    /**
     * Sets the object used to find business layer classes for handling
     * terminal/task requests.
     *
     * @param theDispatcher - The CommandDispatcher.
     */
    public void setDispatcher(CommandDispatcher theDispatcher) {
        this.dispatcher = theDispatcher;
    }

    /**
     * Retrieves the object used to find business layer classes for handling
     * terminal/task requests.
     *
     * @return The CommandDispatcher, or null if uninitialized.
     */
    private CommandDispatcher getDispatcher() {
        return this.dispatcher;
    }
    // [end]

    // [start] executeCommand
    /**
     * {@inheritDoc}
     * @throws Exception
     * @see com.vocollect.voicelink.proxy.tx.StreamlinedTaskCommandService#executeCommand(java.lang.String, java.lang.String[])
     */
    public String executeCommand(String commandName, String[] values) throws Exception {

        TaskCommand cmd = (TaskCommand) this.getDispatcher().dispatch(commandName);

        this.getDispatcher().mapArgumentsToCommand(values, cmd);

        Response response = this.executeCommand(cmd);

        return formatResponse(response);
    }
    // [end]

    // [start] formatResponse (and associated functions)
    /**
     * Directs the formatting of the Response object so that the result
     * is consumable by the terminal/task.
     * @param response - The Response object returned by the Command objects.
     * @return The formatted response data.
     */
    public String formatResponse(Response response) {

        StringBuilder responseBuilder = new StringBuilder();

        if (null != response) {

            // LUT response!

            // Formats the response data (if any) according to information given in the fields.
            List<ResponseRecord> records = response.getRecords();
            assert (null != records);
            String[] fieldInfo = response.getFields();
            assert (null != fieldInfo);

            // Loop through data and handle various formatting scenarios
            for (ResponseRecord record : records) {

                for (String field : fieldInfo) {

                    // format the individual field at this location
                    String value = this.formatRecordValue(field, record);

                    // append the formatted field value
                    responseBuilder.append(value).append(FIELD_SEPARATOR);

                }

                // Replace the extra comma at the end of the record
                //  with the record terminator.
                int lastCharIndex = responseBuilder.length() - 1;
                responseBuilder.deleteCharAt(lastCharIndex);
                responseBuilder.append(this.getEndOfRecord());

            }

            responseBuilder.append(this.getEndOfTransmission());

        } else {

            // ODR response!

            responseBuilder.setLength(0); // clear the response variable
            responseBuilder.append(this.getODRConfirmationByte());
        }

        return responseBuilder.toString();
    }

    /**
     * Formats a value in a ResponseRecord for consumption by the
     * terminal/task.
     *
     * @param field - The field descriptor for indexing into the record.
     * @param record - The ResponseRecord to extract from.
     * @return The formatted value to return to the client, or null if
     *          an error occurred during processing.
     */
    public String formatRecordValue(String field, ResponseRecord record) {

        // get the field identifier
        String[] fieldSplit = field.split(FORMAT_SEPARATOR);
        String name = fieldSplit[0];

        // get the value of the field
        Object value = record.nullSafeGet(name);
        assert (null != value);

        // perform special type conversions
        value = this.convertTypes(value);

        // check for and utilize any formatting information
        if (2 == fieldSplit.length) {

            // formatting available!
            String fmt = fieldSplit[1];
            value = this.customFormatValue(value, fmt);
        }

        // cleanup quotes and commas
        String result = this.cleanString(value);

        return result;
    }

    /**
     * Performs any data type conversion necessary for communication from the host/VL3 business
     * layer to the terminal.
     * @param value - The value to be converted
     * @return The converted value.
     */
    public Object convertTypes(Object value) {
        if (value instanceof Boolean) {
            Boolean booleanVal = (Boolean) value;
            value = (booleanVal ? 1 : 0);
        }
        return value;
    }

    /**
     * Formats value according the specifications in formatString. Default support is for <a
     * href='http://java.sun.com/j2se/1.5.0/docs/api/java/util/Formatter.html'>sprintf()-style
     * formatting</a>.
     *
     * @param value - The value to be formatted
     * @param formatString - The String specifying the formatting scheme.
     * @return The formatted value.
     */
    public Object customFormatValue(Object value, String formatString) {

        Object result = null;

        if (0 == value.toString().length()) {

            // Don't format an empty String.
            //  The task doesn't expect that and it causes problems.
            result = value;

        } else {

            Formatter sprintf = new Formatter();
            sprintf.format(formatString, value);
            result = sprintf.out().toString();

        }

        return result;
    }

    /**
     * Performs any final 'cleaning' steps on the value before it is sent to the client. Currently
     * this involves removing all double-quotes, then enclosing any fields containing commas in
     * double-quotes.
     *
     * @param value - The value to clean.
     * @return The cleaned value.
     */
    public String cleanString(Object value) {
        String cleanUp = value.toString();
        cleanUp = cleanUp.replaceAll("\"", "");
        if (-1 != cleanUp.indexOf(',')) {
            StringBuilder sb = new StringBuilder("\"");
            sb.append(cleanUp).append("\"");
            cleanUp = sb.toString();
        }
        return cleanUp;
    }
    // [end]

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 