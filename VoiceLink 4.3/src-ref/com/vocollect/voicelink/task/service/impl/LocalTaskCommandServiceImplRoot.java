/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommand;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskMessageInfo;
import com.vocollect.voicelink.task.command.TaskResponseRecord;
import com.vocollect.voicelink.task.service.TaskCommandService;
import com.vocollect.voicelink.task.service.TaskCommandTransactionalService;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import org.springframework.context.i18n.LocaleContextHolder;


/**
 * Implementation of the TaskCommandService that runs the command locally.
 *
 * @author ddoubleday
 */
public abstract class LocalTaskCommandServiceImplRoot implements TaskCommandService {

    private static final Logger log = new Logger(BaseTaskCommand.class);

    private static final int ODR_RETRIES = 10;
    private static final int LUT_RETRIES = 3;
    private static final long WAITLIMIT = 100;

    private TaskCommandTransactionalService serviceProxy;

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;

    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }

    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }


    /**
     * Delegates the execution of the command to a proxy service that runs it
     * with the right kind of transaction.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.service.TaskCommandService#executeCommand(com.vocollect.voicelink.task.command.TaskCommand)
     */
    public Response executeCommand(TaskCommand cmd) throws Exception {
        BaseTaskCommand baseCmd = (BaseTaskCommand) cmd;
        boolean isOdr = baseCmd.isODR();
        int retries = 1;
        Response returnResponse = null;

        //Retry ODR's 10 times, Luts will be tried 3 times
        if (isOdr) {
            retries = ODR_RETRIES;
        } else {
            retries = LUT_RETRIES;
        }

        while (retries > 0) {
            try {
                if (cmd.isReadOnly()) {
                    returnResponse = getServiceProxy().executeReadOnlyCommand(cmd);
                } else {
                    returnResponse = getServiceProxy().executeCommand(cmd);
                }
                //No need to retry anymore after successful execution
                retries = 0;

                //report command successful.
                baseCmd.reportSuccess();

            } catch (Throwable t) {

                //decrease the number of tries
                retries--;

                //If LUT and not a data access exception, then don't retry.
                if (!isOdr) {
                    if (t instanceof DataAccessException) {
                        //do nothing
                    } else {
                        retries = 0;
                    }
                }

                //Log Error
                logTaskErrors(baseCmd, t);

                //check if last try
                if (retries == 0) {
                    // Get the exception information
                    TaskMessageInfo messageInfo = buildMessageInfo(t, baseCmd);

                    // Post a notification
                    getVoicelinkNotificationUtil()
                        .postTaskNotification(messageInfo, baseCmd);

                    if (isOdr) {
                    	if (t instanceof TaskCommandException) {
                    		TaskCommandException tce = (TaskCommandException) t;
                    		if (tce.getErrorCode() == TaskErrorCode.DUPLICATE_COMMAND_CALL){
                    			retries = 0;
                    			log.fatal("Duplicate " + baseCmd.getDispatchName()
                    				+ " command detected ", 
                    				TaskErrorCode.GENERAL_SYSTEM_ERROR, 
                    				null);   
                    		}
                    	} else {
                    		//if odr, log retries exceed, response is null
                    		log.fatal("Command " + baseCmd.getDispatchName()
                    				+ " maximum retries exceeded ", 
                    				TaskErrorCode.GENERAL_SYSTEM_ERROR, 
                    				null);      
                    	}
                    } else {
                        //LUT build an error response
                        returnResponse = buildErrorResponse(messageInfo, baseCmd);
                    }
                } else {
                    //wait 100 millisecond before trying again
                    Thread.sleep(WAITLIMIT);
                }
            }
        }

        return returnResponse;
    }


    /**
     * Populate member variables based upon the exception.
     * @param t the exception being processed
     * @param baseCmd the task command being processed
     * @return TaskMessageInfo
     */
    protected TaskMessageInfo buildMessageInfo(Throwable t, BaseTaskCommand baseCmd) {
        TaskMessageInfo messageInfo = new TaskMessageInfo();

        try {
            if (t instanceof VocollectException) {
                VocollectException ve = (VocollectException) t;

                // Set the error code in the errorResponse object
                if (ve.getUserMessage() == null) {
                    // Use the default error code message.
                    messageInfo.setErrorCode(ve.getErrorCode(),
                        LocaleContextHolder.getLocale(),
                        baseCmd.getOperatorId(),
                        baseCmd.getSerialNumber());
                } else {
                    // Use the specified message, unless it is
                    // default message for the errorCode. (This allows
                    // for overriding with the phonetic message in this
                    // case.
                    String msgKey = ve.getUserMessage().getKey();
                    if (msgKey.equals(
                        ve.getErrorCode().getDefaultMessageKey())) {
                        // Allow for phonetic override by not using message,
                        // but do use the args.

                        // Now get the UserMessage values and
                        // Build LabelObjectPairs for the parameters
                        LOPArrayList lop = new LOPArrayList();
                        for (Object value : ve.getUserMessage().getValues()) {

                            // If the value contains the string "VoiceLink-"
                            // then build a task message resource string
                            // to put into the notification details
                            String sValue = value.toString();
                            if (sValue.contains("VoiceLink-")) {
                                sValue = sValue.replace("-", "");
                                value = "errorCode.message." + sValue;
                            }
                            lop.add("task.data", value);

                        }

                        messageInfo.setErrorCode(ve.getErrorCode(),
                            LocaleContextHolder.getLocale(),
                            baseCmd.getOperatorId(),
                            baseCmd.getSerialNumber(),
                            lop);
                    } else {
                        // The thrower of the error really wanted this
                        // specific message, so use it.
                        messageInfo.setErrorCode(ve.getErrorCode(),
                            LocaleContextHolder.getLocale(), ve.getUserMessage());
                    }
                }

            } else {
                buildGenericErrorMessage(messageInfo, baseCmd, t);
            }
        } catch (Throwable tFinal) {
            buildGenericErrorMessage(messageInfo, baseCmd, tFinal);
        }

        return messageInfo;
    }

    /**
     * Build generic message values.
     * @param messageInfo - will be altered by the call to setErrorCode
     * @param baseCmd Task command being processed
     * @param t - the thrown exception
     */
    protected void buildGenericErrorMessage(TaskMessageInfo messageInfo,
                                          BaseTaskCommand baseCmd,
                                          Throwable t) {

        // Now get the thrown exception info and
        // Build LabelObjectPairs for the parameters
        LOPArrayList lop = new LOPArrayList();
        lop.add("notification.exception.message", t.getLocalizedMessage());

        messageInfo.setErrorCode(
            TaskErrorCode.GENERAL_SYSTEM_ERROR,
            LocaleContextHolder.getLocale(),
            baseCmd.getOperatorId(),
            baseCmd.getSerialNumber(),
            lop);
    }


    /**
     * Build and error response record to return to task when
     * error is thrown from command.
     * @param info TaskCommandexceptionInfo object
     * @param baseCmd - task command object
     * @return response
     */
    protected Response buildErrorResponse(TaskMessageInfo info,
                                          BaseTaskCommand baseCmd) {
        // Construct and populate an error record for LUTs.
        ResponseRecord errorRecord = new TaskResponseRecord();
        Response response = baseCmd.getResponse();


        errorRecord.setErrorFields(info);
        response.setErrorRecord(errorRecord);

        return response;
    }

    /**
     * Log error that was thrown from a task command.
     *
     * @param cmd - command object being executed
     * @param t - error that was thrown
     */
    protected void logTaskErrors(BaseTaskCommand cmd, Throwable t) {
        if (t instanceof VocollectException) {
            VocollectException ve = (VocollectException) t;

            if (ve instanceof TaskCommandException) {
                if (ve.getCause() != null) {
                    logError(cmd, ve.getErrorCode(), t);
                } else if (log.isDebugEnabled()) {
                    // Don't dump stack traces for these, these are normal
                    // business rule failures.
                    log.debug("Command " + cmd.getDispatchName()
                        + " threw exception " + ve.toString());
                }
            } else {
                ErrorCode errorCode = TaskErrorCode.GENERAL_SYSTEM_ERROR;
                if (ve.getErrorCode() != null) {
                    errorCode = ve.getErrorCode();
                }
                log.error(cmd, errorCode, t);
            }
        } else {
            // Log unexpected non-VocollectException
            log.error(cmd, TaskErrorCode.GENERAL_SYSTEM_ERROR, t);
        }
    }

    /**
     * @param cmd the command logging the error
     * @param code the ErrorCode associated.
     * @param t the exception to dump the stack trace
     */
    private void logError(BaseTaskCommand cmd, ErrorCode code, Throwable t) {
        log.error("Command " + cmd.getDispatchName()
            + "threw unexpected exception " + t.toString(),
            code,
            t);
    }

    /**
     * Getter for the serviceProxy property.
     * @return TaskCommandTransactionalService value of the property
     */
    public TaskCommandTransactionalService getServiceProxy() {
        return this.serviceProxy;
    }

    /**
     * Setter for the serviceProxy property.
     * @param serviceProxy the new serviceProxy value
     */
    public void setServiceProxy(TaskCommandTransactionalService serviceProxy) {
        this.serviceProxy = serviceProxy;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 