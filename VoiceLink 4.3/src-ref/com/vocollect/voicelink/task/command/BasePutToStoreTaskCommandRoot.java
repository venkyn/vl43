/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsPutManager;

import java.util.List;


/**
 * Base task command object for put to store commands. Contains the most
 * commonly used service managers.
 *
 * @author estoll
 */
public abstract class BasePutToStoreTaskCommandRoot extends BaseCoreTaskCommand {

    private static final long serialVersionUID = -2267561764952674529L;

    private PtsLicenseManager       ptsLicenseManager;

    private PtsPutManager           ptsPutManager;

    private PtsContainerManager     ptsContainerManager;

    private Long                    groupNumber;

    private List <PtsLicense>       licenses;

    /**
     * Getter for the ptsLicenseManager property.
     * @return PtsLicenseManager value of the property
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return this.ptsLicenseManager;
    }

    /**
     * Setter for the ptsLicenseManager property.
     * @param ptsLicenseManager the new ptsLicenseManager value
     */
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }


    /**
     * Getter for the ptsPutManager property.
     * @return PtsPutManager value of the property
     */
    public PtsPutManager getPtsPutManager() {
        return ptsPutManager;
    }


    /**
     * Setter for the ptsPutManager property.
     * @param ptsPutManager the new ptsPutManager value
     */
    public void setPtsPutManager(PtsPutManager ptsPutManager) {
        this.ptsPutManager = ptsPutManager;
    }


    /**
     * @return the ptsContainerManager
     */
    public PtsContainerManager getPtsContainerManager() {
        return ptsContainerManager;
    }

    /**
     * @param ptsContainerManager the ptsContainerManager to set
     */
    public void setPtsContainerManager(PtsContainerManager ptsContainerManager) {
        this.ptsContainerManager = ptsContainerManager;
    }

    /**
     * @return the groupNumber
     */
    public Long getGroupNumber() {
        return groupNumber;
    }

    /**
     * @param groupNumber the groupNumber to set
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     *
     * @return the licenses
     * @throws DataAccessException - on database exception
     */
    public List<PtsLicense> getLicenses() throws DataAccessException  {
        if (this.licenses == null) {
            if (this.groupNumber != null) {
                this.licenses = this.getPtsLicenseManager().listLicensesInGroup(this.getGroupNumber());
            }
        }
        return this.licenses;
    }

    /**
     * @param licenses the licenses to set
     */
    public void setLicenses(List<PtsLicense> licenses) {
        this.licenses = licenses;
    }

    /**
     * Do standard validations and initializations for task commands
     * that occur in selection. This includes base task commands
     * validations as well as core task command validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exceptions
     */
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        super.doStandardValidations();
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 