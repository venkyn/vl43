/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;


/**
 *
 *
 * @author sfahnestock
 */
public abstract class BaseReplenishmentTaskCommandRoot extends BaseCoreTaskCommand {

    private static final long serialVersionUID = -2267561764952674529L;

    private ReplenishmentManager              replenishmentManager;

    private ReplenishmentDetailManager        replenishmentDetailManager;




    /**
     * Getter for the replenishmentDetailManager property.
     * @return ReplenishmentDetailManager value of the property
     */
    public ReplenishmentDetailManager getReplenishmentDetailManager() {
        return replenishmentDetailManager;
    }




    /**
     * Setter for the replenishmentDetailManager property.
     * @param replenishmentDetailManager the new replenishmentDetailManager value
     */
    public void setReplenishmentDetailManager(ReplenishmentDetailManager replenishmentDetailManager) {
        this.replenishmentDetailManager = replenishmentDetailManager;
    }




    /**
     * Getter for the replenishmentManager property.
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }




    /**
     * Setter for the replenishmentManager property.
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }



    /**
     * Do standard validations and intitializations for task commands
     * that occur in selection. This includes base task commands
     * validations as well as core task command validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exceptions
     * 
     */
    @Override
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        super.doStandardValidations();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 