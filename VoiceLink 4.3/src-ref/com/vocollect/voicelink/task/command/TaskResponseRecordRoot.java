/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.logging.Logger;

import java.io.Serializable;
import java.util.HashMap;


/**
 *
 *
 * @author ddoubleday
 */
public class TaskResponseRecordRoot extends HashMap<String, Object>
    implements ResponseRecord, Serializable {

    private static final Logger log = new Logger(TaskResponseRecord.class);

    private static final long serialVersionUID = 6893229645166797547L;

    /**
     * Constructor.
     */
    public TaskResponseRecordRoot() {
        initialize();
    }

    /**
     * Constructor.
     * @param initialCapacity initial storage size of the record.
     */
    public TaskResponseRecordRoot(int initialCapacity) {
        super(initialCapacity);
        initialize();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.ResponseRecord#getErrorCode()
     */
    public String getErrorCode() {
        return (String) get(ERROR_CODE_FIELD);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.ResponseRecord#getErrorMessage()
     */
    public String getErrorMessage() {
        return (String) get(ERROR_MESSAGE_FIELD);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.ResponseRecord#nullSafeGet(java.lang.String)
     */
    public Object nullSafeGet(String key) {
        Object o = super.get(key);
        if (o == null) {
            if (log.isDebugEnabled()) {
                log.debug("Field " + key + "not in map.");
            }
            return "";
        } else {
            return o;
        }
    }

    /**
     * @param info containing task messages and error codes
     */
    public void setErrorFields(TaskMessageInfo info) {
        put(ERROR_CODE_FIELD, info.getTaskReturnCode());
        put(ERROR_MESSAGE_FIELD, info.getTaskMessage());
    }

    /**
     * Initialize the record.
     */
    protected void initialize() {
        put(ERROR_CODE_FIELD, Integer.toString(SUCCESS_ERROR_CODE));
        put(ERROR_MESSAGE_FIELD, "");
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 