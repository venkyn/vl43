/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

/**
 * Base task command for cycle counting to hold common members and managers.
 * 
 * @author khazra
 * 
 */
@SuppressWarnings("serial")
public abstract class BaseCycleCountingTaskCommandRoot extends BaseCoreTaskCommand {

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;
    
    private CycleCountingRegionManager cycleCountingRegionManager;
    
    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }
    /**
     * @param cycleCountingAssignmentManager the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }
    /**
     * @return the cycleCountingRegionManager
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }
    /**
     * @param cycleCountingRegionManager the cycleCountingRegionManager to set
     */
    public void setCycleCountingRegionManager(
            CycleCountingRegionManager cycleCountingRegionManager) {
        this.cycleCountingRegionManager = cycleCountingRegionManager;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 