/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.ContainerManager;
import com.vocollect.voicelink.selection.service.PickManager;

import java.util.List;


/**
 * Base task command object for selection commands. Contains the most
 * commonly used service managers Assignment, Pick, and Container.
 *
 * @author mkoenig
 */
public abstract class BaseSelectionTaskCommandRoot extends BaseCoreTaskCommand {

    private static final long serialVersionUID = -2267561764952674529L;

    private AssignmentManager assignmentManager;

    private PickManager       pickManager;

    private ContainerManager  containerManager;

    private Long              groupNumber;

    private List<Assignment> assignments;



    /**
     * Getter for the assignments property.
     * @return List&lt;Assignment&gt; value of the property
     * @throws DataAccessException - Database exception
     */
    public List<Assignment> getAssignments() throws DataAccessException {
        if (this.assignments == null) {
            if (getGroupNumber() != null) {
                this.assignments =
                    getAssignmentManager().listAssignmentsInGroup(getGroupNumber());
            }
        }

        return assignments;
    }





    /**
     * Getter for the containerManager property.
     * @return ContainerManager value of the property
     */
    public ContainerManager getContainerManager() {
        return containerManager;
    }





    /**
     * Setter for the containerManager property.
     * @param containerManager the new containerManager value
     */
    public void setContainerManager(ContainerManager containerManager) {
        this.containerManager = containerManager;
    }





    /**
     * Getter for the pickManager property.
     * @return PickManager value of the property
     */
    public PickManager getPickManager() {
        return pickManager;
    }





    /**
     * Setter for the pickManager property.
     * @param pickManager the new pickManager value
     */
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }




    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }



    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }



    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Getter for the groupNumber property.
     * @return Long value of the property
     */
    public Long getGroupNumber() {
        return groupNumber;
    }



    /**
     * Setter for the groupNumber property.
     * @param groupNumber the new groupNumber value
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }


    /**
     * Standard check to ensure assignments are in-progress.
     *
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     */
    protected void checkAssignmentStatus()
    throws DataAccessException, TaskCommandException {
        if (getAssignments() != null) {
            for (Assignment a : getAssignments()) {
                if (!a.getStatus().equals(AssignmentStatus.InProgress)) {
                    throw new TaskCommandException(
                        TaskErrorCode.ASSIGNMENT_NOT_INPROGRESS);
                }
            }
        }
    }

    /**
     * Standard Check to ensure operator is correctly assignment
     * to assignments.
     *
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     */
    protected void checkAssignmentOperator()
    throws DataAccessException, TaskCommandException {
        if (getAssignments() != null) {
            for (Assignment a : getAssignments()) {
                if (!a.getOperator().equals(getOperator())) {
                    throw new TaskCommandException(
                        TaskErrorCode.ASSIGNMENT_WRONG_OPERATOR);
                }
            }
        }
    }

    /**
     * Do standard validations and intitializations for task commands
     * that occur in selection. This includes base task commands
     * validations as well as core task command validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exceptions
     */
    @Override
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        super.doStandardValidations();
        checkAssignmentStatus();
        checkAssignmentOperator();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 