/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import java.util.Locale;

import org.springframework.beans.propertyeditors.LocaleEditor;


/**
 * Custom Date property editor that allows for two date formats. The altFormat
 * will be tried if the main format doesn't work.
 * <p>
 * The text output format will always be determined by the primary data format.
 *
 * @author ddoubleday
 */
public class TaskCommandCustomLocaleEditor extends LocaleEditor {

    private static final String UNINITIALIZED_LOCALE = "@LanguageCode@";

    /**
     * Constructor.
     */
    public TaskCommandCustomLocaleEditor() {
        super();
    }

    /**
     * The altFormat will be attempted for the conversion if the primary format
     * fails.
     * {@inheritDoc}
     * @see org.springframework.beans.propertyeditors.CustomDateEditor#setAsText(java.lang.String)
     */
    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (UNINITIALIZED_LOCALE.equals(text)) {
            super.setAsText(Locale.US.toString());
        } else {
            super.setAsText(text);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 