/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingStopManager;


/**
 * Base task command for loading to hold common members and managers.
 * 
 * @author kudupi
 */
public abstract class BaseLoadingTaskCommandRoot extends BaseCoreTaskCommandRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 3548282903111740251L;
    
    private LoadingRegionManager loadingRegionManager;
    
    private LoadingRouteManager  loadingRouteManager;
    
    private LoadingStopManager   loadingStopManager;

    private LoadingContainerManager loadingContainerManager;

    /**
     * Getter for LoadingRegionManager.
     * @return loadingRegionManager.
     */
    public LoadingRegionManager getLoadingRegionManager() {
        return loadingRegionManager;
    }

    /**
     * Setter for LoadingRegionManager
     * @param loadingRegionManager LoadingRegionManager type.
     */
    public void setLoadingRegionManager(LoadingRegionManager loadingRegionManager) {
        this.loadingRegionManager = loadingRegionManager;
    }
    
    /**
     * Getter for LoadingRouteManager.
     * @return loadingRouteManager.
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * Setter for LoadingRouteManager.
     * @param loadingRouteManager LoadingRouteManager.
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }
    
    
    /**
     * Getter for LoadingStopManager.
     * @return loadingStopManager LoadingStopManager.
     */
    public LoadingStopManager getLoadingStopManager() {
        return loadingStopManager;
    }

    /**
     * Setter for LoadingStopManager.
     * @param loadingStopManager LoadingStopManager.
     */
    public void setLoadingStopManager(LoadingStopManager loadingStopManager) {
        this.loadingStopManager = loadingStopManager;
    }

    /**
     * Getter for LoadingContainerManager.
     * @return loadingContainerManager.
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * Setter for LoadingContainerManager.
     * @param loadingContainerManager LoadingContainerManager.
     */
    public void setLoadingContainerManager(
            LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 