/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;


import java.util.Date;


/**
 * The base class for all Task LUTs and ODRs.
 *
 * @author ddoubleday
 */
public interface TaskCommandRoot extends Command {

    /**
     * @return the time of the command
     */
    Date getCommandTime();

    /**
     * @return the operatorId specified in the command arguments.
     */
    String getOperatorId();

    /**
     * @return the device serial number specified in the command arguments.
     */
    String getSerialNumber();


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 