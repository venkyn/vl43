/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.LabelObjectPair;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.springframework.context.NoSuchMessageException;


/**
 * Object to retrieve and hold the Task Exception information
 * that is suitable for both a task response record and a
 * notification.
 *
 * @author estoll
 */
public class TaskMessageInfoRoot implements Serializable {

    private static final long serialVersionUID = -8141173174304298871L;

    private static final Logger log = new Logger(TaskMessageInfo.class);


    private static final String MESSAGE_NOT_FOUND_KEY
    = "errorCode.taskMessageNotFound";

    private static final String MESSAGE_NOT_FOUND_MESSAGE
        = "No error message available";

    private String errorCode;

    private String defaultMessageKey;

    private String taskReturnCode;

    private String errorMessage;

    private String taskMessage;

    private List<LabelObjectPair> messageParameters
        = new ArrayList<LabelObjectPair>();



    /**
     * Getter for the defaultMessageKey property.
     * @return String value of the property
     */
    public String getDefaultMessageKey() {
        return this.defaultMessageKey;
    }


    /**
     * Getter for the messageParameters property.
     * @return List of LabelObjectPair values
     */
    public List<LabelObjectPair> getMessageParameters() {
        return this.messageParameters;
    }


    /**
     * Getter for the errorCode property.
     * @return ErrorCode value of the property
     */
    public String getErrorCode() {
        return this.errorCode;
    }


    /**
     * Getter for the errorMessage property.
     * @return String value of the property
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }


    /**
     * Getter for the taskMessage property.
     * @return String value of the property
     */
    public String getTaskMessage() {
        return this.taskMessage;
    }


    /**
     * Getter for the taskReturnCode property.
     * @return ErrorCode value of the property
     */
    public String getTaskReturnCode() {
        return this.taskReturnCode;
    }

    /**
     * @param code the errorcode of the current exception
     * @param locale of the task
     * @param operatorIdentifier of the terminal operator
     * @param deviceSerialNumber of the device getting the message
     */
    public void setErrorCode(ErrorCode code,
                             Locale    locale,
                             String    operatorIdentifier,
                             String    deviceSerialNumber) {

        ErrorCode checkedCode = checkErrorCode(code);
        setTaskReturnCode(checkedCode, locale);
        setMessages(
            checkedCode, locale, operatorIdentifier, deviceSerialNumber);
        setArgList(operatorIdentifier, deviceSerialNumber, null);
    }

    /**
     * @param code the errorcode of the currect exception
     * @param locale of the task
     * @param operatorIdentifier of the terminal
     * @param deviceSerialNumber of the terminal
     * @param argList for the message
     */
    public void setErrorCode(ErrorCode code,
                             Locale    locale,
                             String    operatorIdentifier,
                             String    deviceSerialNumber,
                             List<LabelObjectPair> argList) {

        ErrorCode checkedCode = checkErrorCode(code);
        setTaskReturnCode(checkedCode, locale);

        // Determine size of args list
        int newArgsLength = 2 + argList.size();
        Object[] newArgs = new Object[newArgsLength];

        // First 2 are always OperatorId and Serial Number
        newArgs[0] = operatorIdentifier;
        newArgs[1] = deviceSerialNumber;

        int pos = 2;
        // Add in any argument values
        for (LabelObjectPair pair : argList) {
            newArgs[pos++] = pair.getValue();
        }

        setMessages(checkedCode, locale, newArgs);
        setArgList(operatorIdentifier, deviceSerialNumber, argList);
    }

    /**
     * @param code of the current exception
     * @param locale of the task
     * @param message to provide to the operator
     */
    public void setErrorCode(ErrorCode code, Locale locale, UserMessage message) {

        ErrorCode checkedCode = checkErrorCode(code);
        setTaskReturnCode(checkedCode, locale);

        if (message != null) {
            // A specific error message was requested. Resolve it.
            String theMessage = null;
            if (message.getValues() != null) {
                theMessage = resolveMessage(
                    message.getKey(), locale, message.getValues().toArray());
            } else {
                theMessage = resolveMessage(message.getKey(), locale);
            }
            taskMessage = theMessage;
            errorMessage = theMessage;

            // Now get the UserMessage values and
            // store them in messageParameters
            for (Object value : message.getValues()) {
                messageParameters.add(
                    new LabelObjectPair("task.data", value));
            }
        } else {
            // Just use the default message for the code.
            setMessages(checkedCode, locale);
        }
    }

    /**
     * Add the operator and serialnumber to the arglist and save
     * these as LabelObjectPairs in the messageParameters member.
     * @param operatorIdentifier running the task
     * @param deviceSerialNumber running the task
     * @param argList other parameters to the message
     */
    protected void setArgList(String operatorIdentifier,
                              String deviceSerialNumber,
                              List<LabelObjectPair> argList) {
        messageParameters.clear();

        // OperatorId is first
        messageParameters.add(
            new LabelObjectPair("task.operator", operatorIdentifier));
        // Serial Number is second
        messageParameters.add(
            new LabelObjectPair("task.serialNumber", deviceSerialNumber));

        if (argList != null) {
            // Then any others
            for (LabelObjectPair pair : argList) {
                messageParameters.add(pair);
            }
        }

    }

    /**
     * Extract the defaultMessageKey from an errorcode object.
     * @param code - Errorcode to get the default message key from
     */
    protected void setDefaultMessageKey(ErrorCode code) {
        defaultMessageKey = code.getDefaultMessageKey();
    }

    /**
     * @param code the error code
     * @param locale the locale for translation
     * @param args the arg list for message tokens.
     */
    protected void setMessages(ErrorCode code,
                               Locale    locale,
                               Object... args) {
        String message = null;
        String taskErrorMessage = null;

        if (code instanceof TaskErrorCode) {
            TaskErrorCode taskCode = (TaskErrorCode) code;
            try {
                message = ResourceUtil.getLocalizedMessage(taskCode
                    .getDefaultPhoneticMessageKey(), args, locale);
            } catch (NoSuchMessageException e) {
                // This is OK, we will use the default message if there
                // is no phonetic message.
                if (log.isDebugEnabled()) {
                    log.debug("No phonetic message for error code " + code);
                }
            }
            try {
                taskErrorMessage = ResourceUtil.getLocalizedMessage(taskCode
                    .getDefaultMessageKey(), args, locale);
            } catch (NoSuchMessageException e) {
                // This is OK, we will use the default message if there
                // is no phonetic message.
                if (log.isDebugEnabled()) {
                    log.debug("No error message for error code " + code);
                }

            }

        }
        // Save the message key
        setDefaultMessageKey(code);

        // Save the phonetic message
        if (message == null) {
            message = resolveMessage(code.getDefaultMessageKey(), locale, args);
        }
        taskMessage = message;

        // Save the task error message
        if (taskErrorMessage == null) {
            taskErrorMessage = resolveMessage(code.getDefaultMessageKey(),
                locale, args);
        }
        errorMessage = taskErrorMessage;
    }


    /**
     * Some error codes must be translated to task return codes.
     * This method does that.
     * @param code the real error code
     * @param locale the Locale to look in for the translated code.
     */
    protected void setTaskReturnCode(ErrorCode code, Locale locale) {
        // Check to see if there is a task return code associated with this
        // error code.

        String returnCode = null;
        if (code instanceof TaskErrorCode) {
            try {
                returnCode = ResourceUtil.getLocalizedMessage(
                    ((TaskErrorCode) code).getDefaultTaskReturnCodeMessageKey(),
                    null, locale);
            } catch (NoSuchMessageException e) {
                returnCode = null;
            }
        }

        // Set errorCode
        errorCode = Long.toString(code.getErrorCode());

        // Set the taskReturnCode
        if (returnCode != null) {
            taskReturnCode = returnCode;
        } else {
            taskReturnCode = Long.toString(code.getErrorCode());
        }
    }

    /**
     * Resolve the message key to a localized String.
     * @param messageKey the key.
     * @param locale Locale to resolve to.
     * @param args arguments to place at tokens in the message.
     * @return the localized message String.
     */
    private String resolveMessage(String    messageKey,
                                  Locale    locale,
                                  Object... args) {
        try {
            return ResourceUtil.getLocalizedMessage(messageKey, args, locale);
        } catch (NoSuchMessageException e) {
            return resolveFallbackMessage(messageKey, locale);
        }
    }

   /**
     * @param messageKey the key.
     * @param locale the locale.
     * @return the localized fallback message, or an English unlocalized
     * message if even this can't be found.
     */
    private String resolveFallbackMessage(String messageKey, Locale locale) {
        if (log.isDebugEnabled()) {
            log.debug("Message key '" + messageKey + "' not found");
        }
        // Return a default message if no message for the key was found,
        // and a fallback English string if even this wasn't found.
        return ResourceUtil.getLocalizedMessage(MESSAGE_NOT_FOUND_KEY,
            null, MESSAGE_NOT_FOUND_MESSAGE, locale);
    }


    /**
     * Return a default error code if the specified one is null.
     * @param code the error code
     * @return the original, or a default if it was null.
     */
    private ErrorCode checkErrorCode(ErrorCode code) {
        if (code != null) {
            return code;
        } else {
            return TaskErrorCode.GENERAL_SYSTEM_ERROR;
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 