/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import java.util.Map;


/**
 *
 *
 * @author ddoubleday
 */
public interface ResponseRecordRoot extends Map<String, Object> {

    /**
     * The error code that indicates success.
     */
    public static final int SUCCESS_ERROR_CODE = 0;

    /**
     * The name of the field where the error code will be stored (as a String).
     */
    public static final String ERROR_CODE_FIELD = "errorCode";

    /**
     * The name of the field where the (localized) error message will be stored.
     */
    public static final String ERROR_MESSAGE_FIELD = "errorMessage";

    /**
     * Get the value for the associated key, or an empty String if the key
     * isn't in the map.
     * @param key the key into the map.
     * @return the value, or the empty String.
     */
    public Object nullSafeGet(String key);

    /**
     * @param info containing task message information
     */
    void setErrorFields(TaskMessageInfo info);

    /**
     * @return the error code as a String (makes it easier to put in output.)
     * The String rep of the SUCCESS_ERROR_CODE will be returned if there is
     * no error.
     */
    String getErrorCode();

    /**
     * @return the localized error message, or the empty String if there is
     * no message.
     */
    String getErrorMessage();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 