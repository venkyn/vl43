/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for task commands. Although the
 * range for these error codes is 1000-1999, please use only 1000-1499 for the
 * predefined ones; 1500-1999 should be reserved for customization error codes.
 *
 * Recommended usage:
 *      Selection/Core use 1001-1100 customizations use 1501-1600
 *      Putaway use 1100-1125 customizations use 1601-1625
 *      Replensihement 1126-1150 customizations use 1626-1650
 *      LineLoading 1151-1175 customizations use 1651-1675
 *      << There is currently a gap 1176-1200 >> An app about the size
 *          of lineloading could use these >>
 *      PutToStore 1201-1250 customizations use 1701-1750
 *      Loading    1401-1450 customizations use 1801-1850
 *      
 * @author ddoubleday
 */
public class TaskErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND = 1000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND = 1999;

    /**
     * No error, just the base initialization.
     */
    public static final TaskErrorCodeRoot NO_ERROR = new TaskErrorCodeRoot();

    /**
     * A unknown error occurred.
     */
    public static final TaskErrorCode GENERAL_SYSTEM_ERROR = new TaskErrorCode(
        1000);

    // ======================================================================
    // Core & Selection error codes
    // Start with 1001
    // End with 1100
    // ======================================================================

    /**
     * Invalid Operator password specified.
     */
    public static final TaskErrorCode INVALID_PASSWORD = new TaskErrorCode(1001);

    /**
     * Operator not allowed on multiple terminals.
     */
    public static final TaskErrorCode NOT_ALLOWED_MULTIPLE_TERMINALS = new TaskErrorCode(
        1002);

    /**
     * No assignments found in region.
     */
    public static final TaskErrorCode NO_ASSIGNMENTS_FOUND = new TaskErrorCode(
        1003);

    /**
     * No assignments reserved for manual issuance.
     */
    public static final TaskErrorCode NO_ASSIGNMENTS_RESERVED = new TaskErrorCode(
        1004);

    /**
     * Invalid status for update status command.
     */
    public static final TaskErrorCode INVALID_STATUS = new TaskErrorCode(1005);

    /**
     * Aisle-slot indicator is invalid for updating status.
     */
    public static final TaskErrorCode INVALID_INDICATOR = new TaskErrorCode(
        1006);

    /**
     * Can not skip the entire assignment.
     */
    public static final TaskErrorCode CANNOT_SKIP_ENTIRE_ASSIGNMENT = new TaskErrorCode(
        1007);

    /**
     * No picks found for group number.
     */
    public static final TaskErrorCode NO_PICKS_IN_GROUP = new TaskErrorCode(
        1008);

    /**
     * Operator not currently signed in.
     */
    public static final TaskErrorCode OPERATOR_NOT_SINGED_IN = new TaskErrorCode(
        1009);

    /**
     * Operator not assigned to terminal.
     */
    public static final TaskErrorCode OPERATOR_NOT_ON_TERMINAL = new TaskErrorCode(
        1010);

    /**
     * Assignment is not in progress.
     */
    public static final TaskErrorCode ASSIGNMENT_NOT_INPROGRESS = new TaskErrorCode(
        1011);

    /**
     * Assignment Not assigned to operator.
     */
    public static final TaskErrorCode ASSIGNMENT_WRONG_OPERATOR = new TaskErrorCode(
        1012);

    /**
     * Picked ODR/LUT could not find pick for the given pick id.
     */
    public static final TaskErrorCode PICKED_ODR_PICK_NOT_FOUND = new TaskErrorCode(
        1013);

    /**
     * Picked ODR/LUT quantityPicked value was less than zero.
     */
    public static final TaskErrorCode PICKED_ODR_NEGATIVE_QUANTITY_PICKED_VALUE = new TaskErrorCode(
        1014);

    /**
     * Picked ODR/LUT pick status was not BaseItem, NotPicked, Skipped, Partial,
     * ShortsGoBack.
     */
    public static final TaskErrorCode PICKED_ODR_INVALID_PICK_STATUS = new TaskErrorCode(
        1015);

    /**
     * Picked ODR/LUT pick status was not BaseItem, NotPicked, Skipped, Partial,
     * ShortsGoBack.
     */
    public static final TaskErrorCode IN_PROGRESS_WORK_IN_REGION = new TaskErrorCode(
        1016);

    /**
     * Not all picks completed when stopping assignment.
     */
    public static final TaskErrorCode NOT_ALL_PICKS_PICKED = new TaskErrorCode(
        1018);

    /**
     * Invalid container number.
     */
    public static final TaskErrorCode INVALID_CONTAINER_NUMBER = new TaskErrorCode(
        1019);

    /**
     * Container already closed.
     */
    public static final TaskErrorCode CONTAINER_ALREADY_CLOSED = new TaskErrorCode(
        1020);

    /**
     * Container already exists.
     */
    public static final TaskErrorCode CONTAINER_ALREADY_EXISTS = new TaskErrorCode(
        1021);

    /**
     * Operator cannot have duplicate container numbers open.
     */
    public static final TaskErrorCode CONTAINER_NUMBER_DUPLICATE = new TaskErrorCode(
        1022);

    /**
     * Invalid region passed from task.
     */
    public static final TaskErrorCode INVALID_REGION_NUMBER = new TaskErrorCode(
        1023);

    /**
     * In progress work exists in another region.
     */
    public static final TaskErrorCode IN_PROGRESS_IN_OTHER_REGION = new TaskErrorCode(
        1024);

    /**
     * Not authorized for any regions.
     */
    public static final TaskErrorCode NOT_AUTHORIZED_ANY_REGIONS = new TaskErrorCode(
        1025);

    /**
     * Container not found in system.
     */
    public static final TaskErrorCode CONTAINER_NOT_FOUND = new TaskErrorCode(
        1026);

    /**
     * Container is empty.
     */
    public static final TaskErrorCode CONTAINER_EMPTY = new TaskErrorCode(1027);

    /**
     * Not authorized for the requested region.
     */
    public static final TaskErrorCode NOT_AUTHORIZED_FOR_REQUESTED_REGION = new TaskErrorCode(
        1028);

    /**
     * In progress work in a different function.
     */
    public static final TaskErrorCode IN_PROGRESS_WORK_IN_OTHER_FUNCTION = new TaskErrorCode(
        1029);

    /**
     * No assignments matching the given work id.
     */
    public static final TaskErrorCode WORK_REQUEST_FAILED = new TaskErrorCode(
        1030);

    /**
     * Multiple matching assignments for requested work id.
     */
    public static final TaskErrorCode MULT_MATCHING_ASSIGNMENTS = new TaskErrorCode(
        1031);

    /**
     * Operator assigned to a different region.
     */
    public static final TaskErrorCode ASSIGNED_TO_OTHER_REGION = new TaskErrorCode(
        1032);

    /**
     * Cannot pass a chase assignment.
     */
    public static final TaskErrorCode CANNOT_PASS_CHASE = new TaskErrorCode(
        1033);

    /**
     * The Specified location is not found.
     */
    public static final TaskErrorCode LOCATION_NOT_FOUND = new TaskErrorCode(
        1034);

    /**
     * The operator is not authorized for any functions.
     */
    public static final TaskErrorCode NOT_AUTHORIZED_FOR_ANY_FUNCTIONS = new TaskErrorCode(
        1035);

    /**
     * The operator is already signed in.
     */
    public static final TaskErrorCode OPERATOR_ALREADY_SIGNED_OFF = new TaskErrorCode(
        1036);

    /**
     * Operator assigned to a different region.
     */
    public static final TaskErrorCode ASSIGNED_TO_OTHER_REGION_AT_SIGNON = new TaskErrorCode(
        1037);

    /**
     * Duplicate command call detected.
     */
    public static final TaskErrorCode DUPLICATE_COMMAND_CALL = new TaskErrorCode(
        1038);

    /**
     * Site is undefined.
     */
    public static final TaskErrorCode SITE_UNDEFINED = new TaskErrorCode(1039);

    /**
     * Printer not found.
     */
    public static final TaskErrorCode PRINTER_NOT_FOUND = new TaskErrorCode(1040);

    /**
     * Printer is disabled.
     */
    public static final TaskErrorCode PRINTER_IS_DISABLED =
        new TaskErrorCode(1041);

    /**
     * Printing Container -- Container is not known.
     */
    public static final TaskErrorCode PRINT_UNKNOWN_CONTAINER =
        new TaskErrorCode(1042);

    public static final TaskErrorCode SERVER_PRINTER_NOT_FOUND =
        new TaskErrorCode(1043);


    /**
     * IDD Standard Check 6 -- Incompatible task version.
     */
    public static final TaskErrorCode WRONG_TASKVERSION =
        new TaskErrorCode(1044);

    // Task Version string must parse into 4 parts
    public static final TaskErrorCode INVALID_TASKVERSION_STRING =
        new TaskErrorCode(1045);

    // Task Version is unknown
    public static final TaskErrorCode UNKNOWN_TASKVERSION_STRING =
        new TaskErrorCode(1046);

    // Task Version compatibility min/max are not defined
    public static final TaskErrorCode UNDEFINED_TASKVERSION_COMPATIBILITY_RANGE =
        new TaskErrorCode(1047);

    // General PrintServer exception - will bundle message into
    // a task exception.
    public static final TaskErrorCode PRINTSERVER_EXCEPTION =
        new TaskErrorCode(1048);

    /**
     * The operator is not signed in to any regions.
     */
    public static final TaskErrorCode NOT_SIGNED_INTO_ANY_REGIONS = new TaskErrorCode(
        1049);

    /**
     * * No Authorized Regions are available for operator.
     */
    public static final TaskErrorCode NO_AUTHORIZED_REGIONS =
        new TaskErrorCode(1050);


    /**
     * AssignedTo Region is not in list of authorized regions.
     */
    public static final TaskErrorCode ASSIGNEDTO_REGION_WORKTYPE_MISSMATCH =
        new TaskErrorCode(1051);

    // Task Version is not defined in properties file
    public static final TaskErrorCode UNDEFINED_TASKVERSION_PROPERTIES =
        new TaskErrorCode(1052);

    /**
     *  The report printer for JasperReports is not configured correctly.
     */
    public static final TaskErrorCode JASPER_REPORT_PRINTESERVICE_EXCEPTION =
        new TaskErrorCode(1053);


    /**
     *  The failed to fill the Jasper report with the required parameters.
     */
    public static final TaskErrorCode JASPER_REPORT_COULD_NOT_BE_FILLED =
        new TaskErrorCode(1054);


    /**
     *  The Jasper report could not be compiled.
     */
    public static final TaskErrorCode JASPER_REPORT_COULD_NOT_BE_COMPILED =
        new TaskErrorCode(1055);

    /**
     *  The Lot does not exist.
     */
    public static final TaskErrorCode LOT_DOES_NOT_EXIST =
        new TaskErrorCode(1056);

    /**
     *  The Lot is expired.
     */
    public static final TaskErrorCode LOT_NUMBER_EXPIRED =
        new TaskErrorCode(1057);

    /**
     *  Lot Location is invalid.
     */
    public static final TaskErrorCode LOT_LOCATION_INVALID =
        new TaskErrorCode(1058);

    /**
     *  The Lot does not exist.
     */
    public static final TaskErrorCode LOT_ITEM_LOCATION_INVALID =
        new TaskErrorCode(1059);

    /**
     *  The Lot is expired.
     */
    public static final TaskErrorCode LOT_ITEM_NUMBER_EXPIRED =
        new TaskErrorCode(1060);

    /**
     * Putaway & PutToStore license errors.
     */
    public static final TaskErrorCode LICENSE_NOT_FOUND =
        new TaskErrorCode(1061);

    public static final TaskErrorCode LICENSE_NOT_AVAILABLE = new TaskErrorCode(
        1062);

    public static final TaskErrorCode NO_LICENSE_FOUND_FOR_OPERATOR = new TaskErrorCode(
        1063);

    public static final TaskErrorCode NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION = new TaskErrorCode(
        1064);

    /**
     * Vehicle safety check errors.
     */
    public static final TaskErrorCode VEHICLE_NOT_FOUND =
        new TaskErrorCode(1065);

    // ======================================================================
    // Putaway error codes
    // Start with 1101
    // End with 1125
    // ======================================================================
    /**
     * Not authorized for region.
     */
    public static final TaskErrorCode NOT_AUTH_FOR_REGION = new TaskErrorCode(
        1101);

    /**
     * Region not compatibale with previously request regions.
     */
    public static final TaskErrorCode NOT_COMP_WITH_PREV_REGION = new TaskErrorCode(
        1102);

    /**
     * All regions not allowed.
     */
    public static final TaskErrorCode ALL_REGIONS_NOT_ALLOWED = new TaskErrorCode(
        1103);

    /**
     * selected region no longer valid.
     */
    public static final TaskErrorCode REGIONS_NO_LONGER_VALID = new TaskErrorCode(
        1104);

    /**
     * Multiple licenses found.
     */
    public static final TaskErrorCode MULTIPLE_LICENSE_FOUND = new TaskErrorCode(
        1105);

    /**
     * License not found. Was 1106 -- but
     * since PUTTOSTORE also uses this message
     * it was moved to Core as 1061
     */

    /**
     * License already selected.
     */
    public static final TaskErrorCode LICENSE_ALREADY_SELECTED = new TaskErrorCode(
        1107);

    /**
     * License not available was 1108 -- but
     * since PUTTOSTORE also uses this message
     * it was moved to Core as 1062
     */


    /**
     * License not in region.
     */
    public static final TaskErrorCode LICENSE_NOT_IN_REGION =
        new TaskErrorCode(1109);


    /**
     * No License found was 1110 -- but
     * since PUTTOSTORE also uses this message
     * it was moved to Core as 1063
     */


    /**
     * License already complete.
     */
    public static final TaskErrorCode LICENSE_ALREADY_COMPLETE = new TaskErrorCode(
        1111);

    /**
     * * No Authorized Putaway Regions are available for operator.
     */
    public static final TaskErrorCode NO_AUTHORIZED_PUTAWAY_REGIONS =
        new TaskErrorCode(1112);


    // ======================================================================
    // Replenishment error codes
    // Start with 1126
    // End with 1150
    // ======================================================================
    /**
     * Not authorized for region.
     */
    public static final TaskErrorCode CANNOT_CHANGE_STATUS = new TaskErrorCode(
        1127);

    /**
     * Not authorized for region.
     */
    public static final TaskErrorCode REGION_NOT_FOUND = new TaskErrorCode(1128);

    /**
     * Work in another region.
     */
    public static final TaskErrorCode WORK_IN_OTHER_REGION = new TaskErrorCode(
        1129);

    /**
     * No work is available.
     */
    public static final TaskErrorCode NO_WORK_AVAILABLE = new TaskErrorCode(
        1130);

    /**
     * Replenishment not found.
     */
    public static final TaskErrorCode REPLEN_NOT_FOUND = new TaskErrorCode(1131);

    /**
     * Replenishment already complete.
     */
    public static final TaskErrorCode REPLEN_ALREADY_COMPLETE = new TaskErrorCode(
        1132);

    /**
     * * No Authorized Replenishment Regions are available for operator.
     */
    public static final TaskErrorCode NO_AUTHORIZED_REPLEN_REGIONS =
        new TaskErrorCode(1133);

    // ======================================================================
    // Line Loading error codes
    // Start with 1151
    // End with 1175
    // ======================================================================
    /**
     * No Cartons available for specified spur.
     */
    public static final TaskErrorCode NO_CARTONS_AVAILABLE = new TaskErrorCode(
        1151);

    /**
     * Carton was not found.
     */
    public static final TaskErrorCode CARTON_NOT_FOUND = new TaskErrorCode(
        1152);

    /**
     * Route Stop of carton is closed.
     */
    public static final TaskErrorCode CARTON_ROUTE_STOP_CLOSED = new TaskErrorCode(
        1153);

    /**
     * Carton not in valid singed into region.
     */
    public static final TaskErrorCode CARTON_NOT_IN_REGION = new TaskErrorCode(
        1154);

    /**
     * Carton is available on wrong spur.
     */
    public static final TaskErrorCode CARTON_AVAILABLE_ON_WRONG_SPUR = new TaskErrorCode(
        1155);

    /**
     * Carton is loaded on wrong spur.
     */
    public static final TaskErrorCode CARTON_LOADED_ON_WRONG_SPUR = new TaskErrorCode(
        1156);

    /**
     * Carton Already Loaded.
     */
    public static final TaskErrorCode CARTON_ALREADY_LOADED = new TaskErrorCode(
        1157);

    /**
     * Pallet Not Found.
     */
    public static final TaskErrorCode PALLET_NOT_FOUND = new TaskErrorCode(
        1158);

    /**
     * Pallet's route does not match carton's.
     */
    public static final TaskErrorCode PALLET_ROUTE_NOT_MATCH = new TaskErrorCode(
        1159);

    /**
     * Pallet's Stop does not match carton's.
     */
    public static final TaskErrorCode PALLET_STOP_NOT_MATCH = new TaskErrorCode(
        1160);

    /**
     * Pallet's Stop does not match carton's.
     */
    public static final TaskErrorCode PALLET_NOT_OPEN = new TaskErrorCode(
        1161);

    /**
     * All carton's for route stop are loaded.
     */
    public static final TaskErrorCode ROUTE_STOP_ALL_LOADED = new TaskErrorCode(
        1162);

    /**
     * Route Stop not fully loaded.
     */
    public static final TaskErrorCode ROUTE_STOP_NOT_LOADED = new TaskErrorCode(
        1163);


    /**
     *  The Pallet Manifest Jasper report was not found.
     */
    public static final TaskErrorCode JASPER_PALLET_MANIFEST_REPORT_NOT_FOUND =
        new TaskErrorCode(1164);


    // ======================================================================
    // PutToStore error codes
    // Start with 1201
    // End with 1250
    // ======================================================================

    /**
     *  License not in region.
     */
    public static final TaskErrorCode PTS_LICENSE_NOT_IN_REGION =
        new TaskErrorCode(1201);

    /**
     *  License reserved by another operator.
     */
    public static final TaskErrorCode PTS_LICENSE_RESERVED =
        new TaskErrorCode(1202);

    /**
     * Multiple matching licenses for requested license.
     */
    public static final TaskErrorCode PTS_LICENSES_MULTIPLE_MATCHING =
        new TaskErrorCode(1203);

    /**
     * No licenses matching the license value.
     */
    public static final TaskErrorCode PTS_LICENSE_REQUEST_FAILED =
        new TaskErrorCode(1204);

    /**
     * Too many licenses in group.
     */
    public static final TaskErrorCode PTS_TOO_MANY_LICNESES_IN_GROUP =
        new TaskErrorCode(1205);

    /**
     * No puts found for group.
     */
    public static final TaskErrorCode PTS_PUTS_NO_PUTS_IN_GROUP =
        new TaskErrorCode(1206);

    /**
     * Can not create new PTS Container, because number already exists (open).
     */
    public static final TaskErrorCode PTS_CONTAINER_OPEN_ALREADY_EXISTS =
        new TaskErrorCode(1207);

    /**
     * PTS Container can not be created, because location already has an open
     * container with same spoken container number.
     */
    public static final TaskErrorCode PTS_CONTAINER_OPEN_CONTAINER_NUMBER_DUPLICATE_FOR_LOCATION =
        new TaskErrorCode(1208);

    /**
     * Attempted to close a container and multiple open containers were found but
     * the container number was not specified.
     */
    public static final TaskErrorCode PTS_CONTAINER_CLOSE_NOT_SPCEIFIED_MULTIPLE_OPEN_FOUND =
        new TaskErrorCode(1209);

    /**
     * Operator attempted to close a container at a location and no open containers were found.
     */
    public static final TaskErrorCode PTS_CONTAINER_CLOSE_NOT_SPECIFIED_NO_OPEN_CONTAINER_AT_LOCATION
    = new TaskErrorCode(1210);

    /**
     *   Operator attempted to open or close a container and we found multiple open containers
     *   but multiple containers are not allowed.
     */
    public static final TaskErrorCode PTS_CONTAINER_FATAL_ERROR_MULTIPLE_OPENED_NOT_ALLOWED
    = new TaskErrorCode(1211);

    /**
     *   Operator attempted to close a container but it was not found at the location.
     */
    public static final TaskErrorCode PTS_CONTAINER_CLOSE_SPECIFIED_NOT_FOUND_OR_ALREADY_CLOSED
    = new TaskErrorCode(1212);

    /**
     * Operator no open containers at the location (review containers).
     */
    public static final TaskErrorCode PTS_CONTAINERS_NO_OPEN_AT_LOCATION =
        new TaskErrorCode(1213);

    /**
     * Operator specified a container to close and multiple open were found.
     */
    public static final TaskErrorCode PTS_CONTAINER_CLOSE_SPCEIFIED_MULTIPLE_OPEN_FOUND =
        new TaskErrorCode(1214);

    /**
     * Operator tried to print container labels at a location that does not exist.
     */
    public static final TaskErrorCode PTS_PRINT_LOCATION_NOT_FOUND =
        new TaskErrorCode(1216);

    /**
     * Operator tried to print container labels with no open containers.
     */
    public static final TaskErrorCode NO_OPEN_PTS_CONTAINERS_AT_LOCATION =
        new TaskErrorCode(1217);

    /**
     * No puts found for group.
     */
    public static final TaskErrorCode NOT_ALL_PUTS_PUT =
        new TaskErrorCode(1218);

    /**
     * Invalid status for update status command.
     */
    public static final TaskErrorCode INVALID_STATUS_FOR_PUT =
        new TaskErrorCode(1219);

    /**
     * Aisle-slot indicator is invalid for updating status.
     */
    public static final TaskErrorCode INVALID_INDICATOR_PUT =
        new TaskErrorCode(1220);

    /**
     * Can not skip the entire assignment.
     */
    public static final TaskErrorCode CANNOT_SKIP_ENTIRE_ASSIGNMENT_PTS =
        new TaskErrorCode(1221);


    /**
     * No container number was specified and no open containers were found.
     */
    public static final TaskErrorCode PTS_PUT_NONE_SPECIFIED_NO_OPEN_FOUND =
        new TaskErrorCode(1223);

    /**
     * No container number was specified and multiple open containers were found.
     */
    public static final TaskErrorCode PTS_PUT_NONE_SPECIFIED_MULTIPLE_FOUND =
        new TaskErrorCode(1224);

    /**
     * Container number was specified but it was not found.
     */
    public static final TaskErrorCode PTS_PUT_CONTAINER_SPECIFIED_NOT_FOUND =
        new TaskErrorCode(1225);


    /**
     * Container number was specified and multiple containers with the same number were found
     * at the location.
     */
    public static final TaskErrorCode PTS_PUT_CONTAINER_SPECIFIED_MULTIPLE_FOUND =
        new TaskErrorCode(1226);


    /**
     * Attempting to update a put record that could not be found in the system.
     */
    public static final TaskErrorCode PTS_PUT_UPDATING_UNKNOWN_RECORD =
        new TaskErrorCode(1227);

    /**
     * Attempting to update a put record that was already completed.
     */
    public static final TaskErrorCode PTS_PUT_ALREADY_COMPLETED =
        new TaskErrorCode(1228);

    /**
     * Cannot pass an assignment that has partial or skipped puts.
     */
    public static final TaskErrorCode PTS_LICENSE_INVALID_PASS_STATUS
    = new TaskErrorCode(1230);

    /**
     * Cannot reserve a license because a previously reserved license
     * has the same item number in it.
     */
    public static final TaskErrorCode PTS_VERIFY_LICENSE_DUPLICATE_ITEM
    = new TaskErrorCode(1231);

    /**
     * No licenses were found for the group number provided.
     */
    public static final TaskErrorCode PTS_NO_LICENSES_FOR_GROUP_NUMBER
    = new TaskErrorCode(1232);


    // ======================================================================
    // CycleCounting error codes
    // Start with 1301
    // End with 1350
    // ======================================================================

    /**
     *  License not in region.
     */
    public static final TaskErrorCode CC_NO_REGION_AUTHORIZED =
        new TaskErrorCode(1301);
    
    public static final TaskErrorCode CC_NOT_COMPAT_WITH_PREV_REGION =
            new TaskErrorCode(1302);
    
    public static final TaskErrorCode CC_ALL_REGION_NOT_ALLOWED =
            new TaskErrorCode(1303);
    
    public static final TaskErrorCode CC_REGION_NOT_FOUND =
            new TaskErrorCode(1304);
    
    public static final TaskErrorCode CC_INVALID_LOCATION =
            new TaskErrorCode(1305);
    
    public static final TaskErrorCode CC_INVALID_LOCATION_ITEM =
            new TaskErrorCode(1306);


    /**
     * Loading Error Codes
     * Start with 1401
     * End with 1450
     */
    public static final TaskErrorCode LOADING_ROUTE_NOT_FOUND =
        new TaskErrorCode(1401);
    
    public static final TaskErrorCode LOADING_ROUTE_ALREADY_COMPLETE =
        new TaskErrorCode(1402);
    
    public static final TaskErrorCode NO_LOADING_CONTAINERS_FOUND = 
        new TaskErrorCode(1403);
    
    public static final TaskErrorCode ALL_CONTAINERS_NOT_LOADED = 
        new TaskErrorCode(1404);
    
    public static final TaskErrorCode NO_LOADING_ROUTE_INPROGRESS = 
        new TaskErrorCode(1405);
    
    public static final TaskErrorCode LOADING_CONTAINER_NOT_LOADED = 
        new TaskErrorCode(1406);
    
    public static final TaskErrorCode NO_LOADING_MASTER_CONTAINER = 
        new TaskErrorCode(1407);
    
    public static final TaskErrorCode LOADING_CONTAINER_ERROR_MESSAGE = 
        new TaskErrorCode(1408);
    
    public static final TaskErrorCode LOADING_CANNOT_CONSOLIDATE = 
        new TaskErrorCode(1409);

    public static final TaskErrorCode MAX_CONSOLIDATION_ACHIEVED = 
        new TaskErrorCode(1410);
    
    public static final TaskErrorCode JASPER_LOAD_TRUCK_DIAGRAM_REPORT_NOT_FOUND =
        new TaskErrorCode(1411);    
    
    /**
     * Constructor.
     */
    private TaskErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected TaskErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(TaskErrorCodeRoot.NO_ERROR, err);
    }

    /**
     * @return the resource message key for a speakable message which is
     *         associated with this ErrorCode by default.
     */
    public String getDefaultPhoneticMessageKey() {
        String defaultKey = getDefaultMessageKey();
        return defaultKey.replace("message", "phoneticMessage");
    }

    /**
     * The code that is returned to the task in the event of a failure is not
     * the same as the TaskErrorCode value. The returned error code is less
     * granular. Most TaskErrorCodes will map to a taskReturnCode of "1". The
     * value is specified in a resource key to allow for user customization.
     * @return the resource message key for the code that should be returned to
     *         the task when this ErrorCode is raised.
     */
    public String getDefaultTaskReturnCodeMessageKey() {
        String defaultKey = getDefaultMessageKey();
        return defaultKey.replace("message", "taskReturnCode");
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 