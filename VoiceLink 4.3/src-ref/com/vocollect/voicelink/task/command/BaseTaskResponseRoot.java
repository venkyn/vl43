/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Define a response consisting of multiple records.
 * @author ddoubleday
 */
public class BaseTaskResponseRoot implements Response, Serializable {

    private static final long serialVersionUID = 6843023626761967611L;

    // The list of fields that should be in each response record.
    private String[] fields = new String[] {
        ResponseRecord.ERROR_CODE_FIELD,
        ResponseRecord.ERROR_MESSAGE_FIELD
    };

    // The list of records that make up the response.
    private List<ResponseRecord> records = new ArrayList<ResponseRecord>();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Response#addRecord(java.lang.Object)
     */
    public void addRecord(ResponseRecord record) {
        this.records.add(record);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Response#getFields()
     */
    public String[] getFields() {
        return this.fields;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Response#getRecords()
     */
    public List<ResponseRecord> getRecords() {
        return this.records;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Response#setErrorRecord(com.vocollect.voicelink.task.command.ResponseRecord)
     */
    public void setErrorRecord(ResponseRecord record) {
        this.records.clear();
        this.records.add(record);
    }


    /**
     * Setter for the fields property. The automatic error code and error
     * message fields will be appended to the list, so they don't need
     * to be configured in Spring.
     * @param fields the new fields value
     */
    public void setFields(String[] fields) {
        this.fields = new String[fields.length + 2];
        System.arraycopy(fields, 0, this.fields, 0, fields.length);
        this.fields[fields.length] = ResponseRecord.ERROR_CODE_FIELD;
        this.fields[fields.length + 1] = ResponseRecord.ERROR_MESSAGE_FIELD;
    }

    /**
     * Setter for the records property.
     * @param records the new records value
     */
    public void setRecords(List<ResponseRecord> records) {
        this.records = records;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 