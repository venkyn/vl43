/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import java.util.List;


/**
 * Interface for responses to VoiceLink task commands.
 *
 * @author ddoubleday
 */
public interface ResponseRoot {

    /**
     * Add a response record. The record is a map of Objects keyed by the
     * field name.
     * @param record the record to add
     */
    void addRecord(ResponseRecord record);

    /**
     * @return the array of field names included in each record of the response.
     */
    String[] getFields();

    /**
     * @return the list of records that are part of the response.
     */
    List<ResponseRecord> getRecords();

    /**
     * Set a single error record in the response. This will clear any
     * previously added records.
     * @param record the error record to add.
     */
    void setErrorRecord(ResponseRecord record);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 