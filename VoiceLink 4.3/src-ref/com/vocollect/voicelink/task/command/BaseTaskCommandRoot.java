/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.LabelObjectPair;
import com.vocollect.epp.util.LocaleAdapter;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Device;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.TerminalManager;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * The base class for all Task LUTs and ODRs.
 *
 * @author ddoubleday
 */
public abstract class BaseTaskCommandRoot implements TaskCommand, Serializable {


    /**
     * The format of the date string that is sent from the terminal.
     */
    public static final String TASK_COMMAND_DATE_FORMAT = "MM-dd-yy HH:mm:ss";

    /**
     * The alternate (psuedo-millisecond) format of the date string that
     * is sent from the terminal.
     */
    public static final String
        TASK_COMMAND_MILLI_DATE_FORMAT = "MM-dd-yy HH:mm:ss.SS";

    private static final Logger log = new Logger(BaseTaskCommand.class);

    private static final String[] COMMON_DISPATCH_ARGS = {
        "commandTimeString", "serialNumber", "operatorId"   };
    //Number of milliseconds in a second
    private static final Long MILLISECONDS_IN_SECOND = 1000L;


    //Map to track duplicate ODR transaction calls
    private static Map<String, Date> lastODRCommand = 
        Collections.synchronizedMap(new HashMap<String, Date>());

    //Map to track duplicate LUT transaction calls
    private static Map<String, Date> lastLUTCommand = 
        Collections.synchronizedMap(new HashMap<String, Date>());

    private static Map<String, String>  lastCommandName = new HashMap<String, String>();
    //Command times, initially received as string then converted
    //to date when timezone could be determined
    private String                  commandTimeString;

    private Date                    commandTime;

    // Since all task commands start with the same three arguments, this
    // shortcut allows us to not have to specify them for every command.
    private String[]                dispatchArgs         = COMMON_DISPATCH_ARGS;

    private String                  dispatchName;

    private String                  operatorId;

    private OperatorManager         operatorManager;

    private boolean                 readOnly = false;

    private Response                response;

    private String                  serialNumber;
    private TerminalManager         terminalManager;

    private Operator                operator;


    private Device                  terminal;

    private SystemPropertyManager   systemPropertyManager;

    private SiteContext             siteContext;

    private String                  taskVersion;

    private TaskVersionInfo         taskVersionInfo;

    private DataTranslationManager  dataTranslationManager;


    /**
     * Getter for the dataTranslationManager property.
     * @return DataTranslationManager value of the property
     */
    public DataTranslationManager getDataTranslationManager() {
        return dataTranslationManager;
    }


    /**
     * Setter for the dataTranslationManager property.
     * @param dataTranslationManager the new dataTranslationManager value
     */
    public void setDataTranslationManager(DataTranslationManager dataTranslationManager) {
        this.dataTranslationManager = dataTranslationManager;
    }


    /**
     * Getter for the taskVersionInfo property.
     * @return TaskVersionInfo value of the property
     */
    public TaskVersionInfo getTaskVersionInfo() {
        return this.taskVersionInfo;
    }



    /**
     * Setter for the taskVersionInfo property.
     * @param taskVersionInfo the new taskVersionInfo value
     */
    public void setTaskVersionInfo(TaskVersionInfo taskVersionInfo) {
        this.taskVersionInfo = taskVersionInfo;
    }


    /**
     * Getter for the taskVersion property.
     * @return String value of the property
     */
    public String getTaskVersion() {
        return this.taskVersion;
    }


    /**
     * Setter for the taskVersion property.
     * @param taskVersion the new taskVersion value
     */
    public void setTaskVersion(String taskVersion) {
        this.taskVersion = taskVersion;
    }


    /**
     * Getter for the commandTime property.
     * @return Date value of the property
     */
    public Date getCommandTime() {
        return commandTime;
    }



    /**
     * Setter for the commandTime property.
     * @param commandTime the new commandTime value
     */
    public void setCommandTime(Date commandTime) {
        this.commandTime = commandTime;
    }


    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }


    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * Class to be overridden by subclasses to do the actual work.
     * @return the command Response object.
     * @throws Exception in the general case.
     */
    protected abstract Response doExecute() throws Exception;

    /**
     * The execute method handles generic command housekeeping and delegates
     * specific work to subclasses.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Command#execute()
     */
    public Response execute() throws Exception {
        //Ensure starting with empty record sets
        Response r;

        try {
            if (getResponse() != null) {
                getResponse().getRecords().clear();
            }

            doStandardValidations();
            r = doExecute();
        } catch (Exception e) {
            getTerminalManager().getPrimaryDAO().clearSession();
            throw e;
        }

        return r;
    }


    /**
     * call to report command successfully sent to proxy so it will not
     * be retried.
     */
    public void reportSuccess() {
        //Save information when no errors thrown from execute
        if (isODR()) {
            lastODRCommand.put(getSerialNumber() + getDispatchName(), getCommandTime());
        } else {
            lastLUTCommand.put(getSerialNumber() + getDispatchName(), getCommandTime());           
        }

        lastCommandName.put(getSerialNumber(), getDispatchName());
    }


    /**
     * Getter for the commandTime property.
     * @return String value of the property
     */
    public String getCommandTimeString() {
        return this.commandTimeString;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Command#getDispatchArgs()
     */
    public String[] getDispatchArgs() {
        return this.dispatchArgs;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Command#getDispatchName()
     */
    public String getDispatchName() {
        return this.dispatchName;
    }

    /**
     * @return the Operator with the operatorId set in this class, or null if
     *         the operatorId hasn't been set, or if no such operator exists.
     * @throws DataAccessException on failure to access the database.
     */
    public Operator getOperator() throws DataAccessException {
        if (this.operator == null) {
            if (getOperatorId() != null) {
                this.operator =
                    getOperatorManager().findByIdentifier(getOperatorId());
            } else {
                log.warn("Operator ID was not set -- it should have been.");
            }
        }

        return this.operator;
    }

    /**
     * Getter for the operatorId property.
     * @return String value of the property
     */
    public String getOperatorId() {
        return this.operatorId;
    }

    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;
    }

    /**
     * Getter for the response property.
     * @return Response value of the property
     */
    public Response getResponse() {
        return this.response;
    }

    /**
     * Getter for the serialNumber property.
     * @return String value of the property
     */
    public String getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * @return the Device with the operatorId set in this class, or null if
     *         the operatorId hasn't been set, or if no such operator exists.
     * @throws DataAccessException on failure to access the database.
     */
    public Device getTerminal() throws DataAccessException {
        if (this.terminal == null) {
            if (getSerialNumber() != null) {
                boolean siteFilter = getSiteContext().isFilterBySite();
                getSiteContext().setFilterBySite(false);
                SiteContextHolder.setSiteContext(getSiteContext());
                this.terminal =
                    getTerminalManager().findBySerialNumber(getSerialNumber());

                getSiteContext().setFilterBySite(siteFilter);
            } else {
                log.warn("Device SN was not set -- it should have been.");
            }
        }

        return this.terminal;
    }


    /**
     * Getter for the terminalManager property.
     * @return TerminalManager value of the property
     */
    public TerminalManager getTerminalManager() {
        return this.terminalManager;
    }


    /**
     * Getter for the readOnly property.
     * @return boolean value of the property
     */
    public boolean isReadOnly() {
        return this.readOnly;
    }

    /**
     * @return whether or not this command is an ODR.
     */
    public boolean isODR() {
        return getResponse() == null;
    }
    
    /**
     * Setter for the commandTime property.
     * @param commandTimeString the new commandTimeString value
     */
    public void setCommandTimeString(String commandTimeString) {

        this.commandTimeString = commandTimeString;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Command#setDispatchArgs(java.lang.String[])
     */
    public void setDispatchArgs(String[] args) {
        if ((args != null) && (args.length > 0)) {
            // Add these on to the common set of args
            this.dispatchArgs = new String[COMMON_DISPATCH_ARGS.length
                + args.length];
            System.arraycopy(
                COMMON_DISPATCH_ARGS, 0, this.dispatchArgs, 0,
                COMMON_DISPATCH_ARGS.length);
            System.arraycopy(
                args, 0, this.dispatchArgs, COMMON_DISPATCH_ARGS.length,
                args.length);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.Command#setDispatchName(java.lang.String)
     */
    public void setDispatchName(String name) {
        this.dispatchName = name;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        if (this.operator == null) {
            this.operator = operator;
        } else {
            throw new IllegalArgumentException(
                "Cannot set Operator that is already set.");
        }
    }

    /**
     * Setter for the operatorId property.
     * @param operatorId the new operatorId value
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Setter for the readOnly property.
     * @param readOnly the new readOnly value
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * Setter for the response property.
     * @param response the new response value
     */
    public void setResponse(Response response) {
        this.response = response;
    }

    /**
     * Setter for the terminal property.
     * @param terminal the new terminal value
     */
    public void setTerminal(Device terminal) {
        if (this.terminal == null) {
            this.terminal = terminal;
        } else {
            throw new IllegalArgumentException(
                "Cannot set terminal that is already set");
        }
    }

    /**
     * Setter for the serialNumber property.
     * @param serialNumber the new serialNumber value
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Setter for the terminalManager property.
     * @param terminalManager the new terminalManager value
     */
    public void setTerminalManager(TerminalManager terminalManager) {
        this.terminalManager = terminalManager;
    }

    /**
     * @return a new Device with required fields populated.
     */
    protected Device makeNewTerminal() {
        Device term = new Device();

        term.setSerialNumber(getSerialNumber());

        return term;
    }

    /**
     * Verify task version string is properly formated
     * and that the compatibility number falls within the
     * min/max range.
     *
     * @throws TaskCommandException -
     * @throws DataAccessException on data error.
     */
    protected void checkTaskCompatibility()
        throws TaskCommandException, DataAccessException {
        final String versionStringDelimiter = "-";
        final int taskIdentifier = 0;
        final int taskCompatibilityPosition = 2;
        final int taskVersionParts = 4;

        String taskVersionLocal = getTaskVersion();

        // Get the taskversion either from the command parameter
        // or the terminal's current version.
        if (StringUtil.isNullOrEmpty(taskVersionLocal)
            && (getTerminal() != null)) {
            taskVersionLocal = getTerminal().getTaskVersion();
        }

        // if we still don't have a version string - error out.
        if (StringUtil.isNullOrEmpty(taskVersionLocal)) {
            throw new TaskCommandException(
                TaskErrorCode.UNKNOWN_TASKVERSION_STRING,
                getDispatchName());
        }

        // Parse the version string into pieces
        String[] tokens = taskVersionLocal.split(versionStringDelimiter);

        // Verify 4 segments of the task version string
        if (tokens.length != taskVersionParts) {
            throw new TaskCommandException(
                TaskErrorCode.INVALID_TASKVERSION_STRING,
                getDispatchName());
        }

        // Get the min/max based upon the task type
        String minString = this.taskVersionInfo.getMinCompatibility()
                .get(tokens[taskIdentifier]);

        String maxString = this.taskVersionInfo.getMaxCompatibility()
                .get(tokens[taskIdentifier]);

        // If values are null, the properties file is not set up correctly
        if ((minString == null) || (maxString == null)) {
            throw new TaskCommandException(
                TaskErrorCode.UNDEFINED_TASKVERSION_PROPERTIES,
                tokens[taskIdentifier]);
        }

        Double max = java.lang.Double.valueOf(maxString);
        Double min = java.lang.Double.valueOf(minString);

        // Verify the values are not null
        if ((min == null) || (max == null)) {
            throw new TaskCommandException(
                TaskErrorCode.UNDEFINED_TASKVERSION_COMPATIBILITY_RANGE,
                getDispatchName());
        }

        // The compatibility segment of the version string must
        // be in the range of the min/max
        Double taskCompatibility =
            java.lang.Double.valueOf(tokens[taskCompatibilityPosition]);

        if ((taskCompatibility < min)
            || (taskCompatibility > max)) {
            throw new TaskCommandException(
                TaskErrorCode.WRONG_TASKVERSION,
                taskVersionLocal);
        }
    }


    /**
     * Standard check for a duplicate function call.
     *
     * @throws TaskCommandException - Task Exception
     */
    protected void checkDuplicateCommandCall() throws TaskCommandException {
        
        Date lastDate;
        if (isODR()) {
            lastDate = lastODRCommand.get(getSerialNumber() + getDispatchName());
        } else {
            lastDate = lastLUTCommand.get(getSerialNumber() + getDispatchName());           
        }
        
        //There was a previous command
        if (lastDate != null) {

            long currentTime = getCommandTime().getTime();
            long lastTime = lastDate.getTime();
            long currentTimeInSeconds = currentTime / MILLISECONDS_IN_SECOND;
            long lastTimeInSeconds = lastTime / MILLISECONDS_IN_SECOND;

            // check time rounded to seconds.
            // if current command is older than the previously executed command
            // this is an invalid command.
            if (currentTimeInSeconds < lastTimeInSeconds) {
                throw new TaskCommandException(
                        TaskErrorCode.DUPLICATE_COMMAND_CALL,
                        getDispatchName());
            }

            // Now check time to the millisecond.
            // If time still equals, check command name.
            if (currentTime == lastTime) {
               String lastCommand = BaseTaskCommandRoot.lastCommandName.get(getSerialNumber());
                if (lastCommand == getDispatchName()) {
                    throw new TaskCommandException(
                            TaskErrorCode.DUPLICATE_COMMAND_CALL,
                            getDispatchName());
                }
            }
        }
    }

    /**
     * Set the locale from the terminal, or the default if the
     * terminal isn't defined.
     * @throws DataAccessException - Database Exception
     */
    protected void setResponseLocale() throws DataAccessException {
        if (getTerminal() != null) {
            // Use the Locale associated with the terminal (but potentially
            // adjusted to handle special cases.
            LocaleContextHolder.setLocale(
                LocaleAdapter.adjustLocale(getTerminal().getTaskLocale()));
        } else {
            // Use the default
            LocaleContextHolder.setLocale(LocaleAdapter.adjustLocale((String) null));
        }
    }


    /**
     * Do standard validations and intitializations for all task commands.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException  - Business rule exception
     */
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        setSiteContextForCommand();
        setResponseLocale();
        checkDuplicateCommandCall();
        checkTaskCompatibility();
    }

    /**
     * Set the site context for the command being executed based on the terminals
     * current site.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exception
     */
    protected void setSiteContextForCommand()
    throws DataAccessException, TaskCommandException {
        TimeZone timeZone = null;

        //Reset Site Context to terminals context
        if ((getTerminal() != null) && (getTerminal().getTags() != null)) {
            if (!getTerminal().getTags().isEmpty()) {
                // Limit access to sites OK for the user.
                getSiteContext().setContextSites(getTerminal());
                // Terminal can only ever have one site tag
                Site currentSite = getSiteContext().getSite(getTerminal());
                getSiteContext().setCurrentSite(currentSite);
                getSiteContext().setFilterBySite(true);

                SiteContextHolder.setSiteContext(getSiteContext());

                timeZone = currentSite.getTimeZone();
            }
        }

        //Set Command Time based on time zone
        setCommandTimeForTimeZone(timeZone);
    }


    /**
     * Set commandTime based on specified time zone.
     *
     * @param timeZone - time zone to set to
     * @throws TaskCommandException - Task Command Exception
     */
    protected void setCommandTimeForTimeZone(TimeZone timeZone)
    throws TaskCommandException {

        try {
            setCommandTime(convertDateString(TASK_COMMAND_MILLI_DATE_FORMAT,
                timeZone,
                getCommandTimeString()));
        } catch (ParseException e) {
            try {
                setCommandTime(convertDateString(TASK_COMMAND_DATE_FORMAT,
                    timeZone,
                    getCommandTimeString()));
            } catch (ParseException e1) {
                throw new TaskCommandException(
                        TaskErrorCode.GENERAL_SYSTEM_ERROR, e1);
            }
        }
    }

    /**
     * Converts a string to a date based on a format and time zone.
     *
     * @param format - Format of time string
     * @param timeZone - Time zone to convert to
     * @param date - Date string to convert
     * @return - String converted to date object
     * @throws ParseException - Parsing exception.
     */
    private Date convertDateString(String format,
                                   TimeZone timeZone,
                                   String date) throws ParseException {
        SimpleDateFormat formatter =
            new SimpleDateFormat(format);
        if (timeZone != null) {
            formatter.setTimeZone(timeZone);
        }

        return formatter.parse(date);

    }

    /**
     * inserts spaces between each character of the string. This method
     * is typically used for sending values to the task the the user wishes
     * to have spoken phonetically instead of as value. For example if the value
     * was 30, then without spaces it would be spoken as thirty, with spaces it
     * would be spoken as three zero.
     *
     * @param data - string to include spaces into.
     * @return - return string with inserted spaces
     */
    public String createPhoneticString(String data) {

        //if null string passed in the return null back
        if (data == null) {
            return null;
        } else {
            StringBuilder buffer = new StringBuilder();

            //loop through each character and add space
            for (char c : data.toCharArray()) {
                buffer.append(c);
                buffer.append(' ');
            }

            return buffer.toString().trim();
        }

    }

    /**
     * Create a TaskMessageInfo object.  The array of objects
     * should EXCLUDE the Device Locale, Operator ID & Device
     * Serial Number.
     * @param code errorcode of the message object to build
     * @param argList other parameters to the message
     * @return TaskMessageInfo object
     * @throws DataAccessException on database exceptions
     */
    public TaskMessageInfo createMessageInfo(ErrorCode code,
                               List<LabelObjectPair> argList)
    throws DataAccessException {
        TaskMessageInfo messageInfo = new TaskMessageInfo();

        messageInfo.setErrorCode(code, this.getTerminal().getTaskLocale(),
                                this.getOperatorId(),
                                this.getSerialNumber(),
                                argList);
        return messageInfo;
    }

    /**
     * Create a TaskMessageInfo object.  Use this method when
     * the message does not uses values other than operator id
     * or serial number.
     * @param code errorcode of the message object to build
     * @return TaskMessageInfo object
     * @throws DataAccessException on database exceptions
     */
    public TaskMessageInfo createMessageInfo(ErrorCode code)
    throws DataAccessException {
        TaskMessageInfo messageInfo = new TaskMessageInfo();

        messageInfo.setErrorCode(code, this.getTerminal().getTaskLocale(),
                                getOperatorId(),
                                getSerialNumber());
        return messageInfo;
    }

    /**
     * Translate the specified user data key into the current (thread) Locale.
     * @param key the user data to translate
     * @return the translation for the current Locale (specified by
     * <code>LocaleContextHolder</code>, or the original value if no match
     * @throws DataAccessException on failure to retrieve data.
     */
    protected String translateUserData(String key) throws DataAccessException {
        return getDataTranslationManager().translate(key);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 