/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.lineloading.service.PalletManager;
import com.vocollect.voicelink.lineloading.service.RouteStopManager;


/**
 * Base task command object for line loading commands. Contains the most
 * commonly used service managers Cartons, Carton Details, and Route Stops.
 *
 * @author mkoenig
 */
public abstract class BaseLineLoadingTaskCommandRoot extends BaseCoreTaskCommand {

    private PalletManager              palletManager;

    private CartonManager              cartonManager;

    private RouteStopManager           routeStopManager;

    private CartonDetailManager        cartonDetailManager;






    /**
     * Getter for the palletManager property.
     * @return PalletManager value of the property
     */
    public PalletManager getPalletManager() {
        return palletManager;
    }

    /**
     * Setter for the palletManager property.
     * @param palletManager the new palletManager value
     */
    public void setPalletManager(PalletManager palletManager) {
        this.palletManager = palletManager;
    }


    /**
     * Getter for the cartonDetailManager property.
     * @return CartonDetailManager value of the property
     */
    public CartonDetailManager getCartonDetailManager() {
        return cartonDetailManager;
    }


    /**
     * Setter for the cartonDetailManager property.
     * @param cartonDetailManager the new cartonDetailManager value
     */
    public void setCartonDetailManager(CartonDetailManager cartonDetailManager) {
        this.cartonDetailManager = cartonDetailManager;
    }


    /**
     * Getter for the cartonManager property.
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return cartonManager;
    }


    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }


    /**
     * Getter for the routeStopManager property.
     * @return RouteStopManager value of the property
     */
    public RouteStopManager getRouteStopManager() {
        return routeStopManager;
    }


    /**
     * Setter for the routeStopManager property.
     * @param routeStopManager the new routeStopManager value
     */
    public void setRouteStopManager(RouteStopManager routeStopManager) {
        this.routeStopManager = routeStopManager;
    }

    /**
     * Do standard validations and intitializations for task commands
     * that occur in selection. This includes base task commands
     * validations as well as core task command validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exception
     */
    @Override
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException{
        super.doStandardValidations();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 