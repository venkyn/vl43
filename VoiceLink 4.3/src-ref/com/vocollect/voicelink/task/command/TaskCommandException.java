/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;



/**
 * Exception that is thrown by TaskCommand objects on any error.
 *
 * @author ddoubleday
 */
public class TaskCommandException extends VocollectException {

    private static final long serialVersionUID = 5150062200301528041L;

    private Response response;

    /**
     * Constructor.
     * @param code the ErrorCode associated with the exception.
     */
    public TaskCommandException(ErrorCode code) {
        super(code);
    }

    /**
     * Constructor.
     * @param code the ErrorCode associated with the exception.
     * @param t the wrapped Throwable
     */
    public TaskCommandException(ErrorCode code,
                                Throwable     t) {
        super(code, t);
    }

    /**
     * Constructor.
     * @param code the ErrorCode associated with the exception.
     * @param messageArgs a varargs list of arguments to be inserted into
     * default message for the error code.
     */
    public TaskCommandException(ErrorCode code, Object...messageArgs) {
        super(code);
        // Set the message with arguments, overriding the default one.
        // Set the second parameter so we will not escape HTML.  This prevents us
        // from inserting &nbsp for spaces in localized message keys.
        setMessage(new UserMessage(code.getDefaultMessageKey(), false, messageArgs));
    }

    /**
     * Constructor.
     * @param code the ErrorCode associated with the exception.
     * @param msg a localizable message associated with the exception.
     * @param t the wrapped Throwable.
     */
    public TaskCommandException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

    /**
     * Constructor. Use when you want to throw an exception out of
     * transaction scope and carry along a Response object.
     * @param errorResponse the Response associated with the exception.
     */
    public TaskCommandException(Response errorResponse) {
        super();
        setResponse(errorResponse);
    }

    /**
     * Getter for the response property.
     * @return Response value of the property
     */
    public Response getResponse() {
        return this.response;
    }

    /**
     * Setter for the response property. One can set a Response object
     * into the exception so the exception can be thrown out of transaction
     * scope to trigger rollback, but still carry a Response object with it.
     * @param response the new response value
     */
    public void setResponse(Response response) {
        this.response = response;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 