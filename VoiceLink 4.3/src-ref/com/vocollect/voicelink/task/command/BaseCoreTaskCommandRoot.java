/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.OperatorStatus;


/**
 * Base task command object for selection commands. Contains the most
 * commonly used service managers Assignment, Pick, and Container.
 *
 * @author mkoenig
 */
public abstract class BaseCoreTaskCommandRoot extends BaseTaskCommand {

    private static final long serialVersionUID = -2267561764952674529L;

    private static final Logger log = new Logger(BaseCoreTaskCommand.class);

    /**
     * Standard check to ensure operator is signed in.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void checkOperatorStatus()
    throws DataAccessException, TaskCommandException {
        if (!getOperator().getStatus().equals(OperatorStatus.SignedOn)) {
            throw new TaskCommandException(TaskErrorCode.OPERATOR_NOT_SINGED_IN);
        }
    }

    /**
     * Standard check for operator being assigned to terminal.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void checkOperatorTerminal()
    throws DataAccessException, TaskCommandException {
        if (getOperator() == null
            || getTerminal().getOperator() == null
            || !getTerminal().getOperator().getOperatorIdentifier().
            equals(getOperator().getOperatorIdentifier())) {
            throw new TaskCommandException(TaskErrorCode.OPERATOR_NOT_ON_TERMINAL);
        } else {
        }
    }

    /**
     * Do standard validations and intitializations for task commands
     * that occur after signing on. This includes base task commands
     * validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exception
     */
    @Override
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        super.doStandardValidations();
        checkOperatorTerminal();
        checkOperatorStatus();
    }

    /**
     * Return the phonetic description (which falls back to the textual
     * description), or a placeholder value if there is no description.
     * @param item the Item object
     * @return the (potentially) translated (possibly phonetic) description,
     * if it exists; otherwise, the translated "No Description" string.
     */
    protected String getTranslatedItemPhoneticDescription(Item item) {

        // Include the "No Description" localized message if there is
        // no phonetic description (note the assumption that there is
        // no description either, because getPhoneticDescription()
        // returns getDescription() if there is no phonetic value.
        if (StringUtil.isNullOrEmpty(item.getPhoneticDescription())) {
            return ResourceUtil.getLocalizedKeyValue(
                           "item.label.description.noDescription");
        } else {
            try {
                return translateUserData(item.getPhoneticDescription());
            } catch (DataAccessException e) {
                log.error("Failed to retrieve translation",
                    SystemErrorCode.DATA_PROVIDER_FAILURE, e);
                // just return the untranslated value
                return item.getPhoneticDescription();
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 