/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.putaway.service.LicenseDetailManager;
import com.vocollect.voicelink.putaway.service.LicenseManager;


/**
 * Base task command object for selection commands. Contains the most
 * commonly used service managers Assignment, Pick, and Container.
 *
 * @author mkoenig
 */
public abstract class BasePutawayTaskCommandRoot extends BaseCoreTaskCommand {

    private static final long serialVersionUID = -2267561764952674529L;

    private LicenseManager              licenseManager;

    private LicenseDetailManager        licenseDetailManager;



    /**
     * Getter for the licenseDetailManager property.
     * @return LicenseDetailManager value of the property
     */
    public LicenseDetailManager getLicenseDetailManager() {
        return licenseDetailManager;
    }



    /**
     * Setter for the licenseDetailManager property.
     * @param licenseDetailManager the new licenseDetailManager value
     */
    public void setLicenseDetailManager(LicenseDetailManager licenseDetailManager) {
        this.licenseDetailManager = licenseDetailManager;
    }



    /**
     * Getter for the licenseManager property.
     * @return LicenseManager value of the property
     */
    public LicenseManager getLicenseManager() {
        return licenseManager;
    }



    /**
     * Setter for the licenseManager property.
     * @param licenseManager the new licenseManager value
     */
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }


    /**
     * Do standard validations and intitializations for task commands
     * that occur in selection. This includes base task commands
     * validations as well as core task command validations.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Business rule Exceptions
     */
    @Override
    protected void doStandardValidations()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        super.doStandardValidations();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 