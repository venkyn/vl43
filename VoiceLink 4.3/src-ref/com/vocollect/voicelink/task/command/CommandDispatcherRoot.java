/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Registry for name to bean name command mappings -- the name is what the
 * outside world uses, the bean name is what Spring knows.
 * <p>
 * The dispatcher is a Spring-managed singleton that is auto-populated with
 * all the Spring-managed TaskCommand beans. To dispatch to a specific command,
 * call the dispatch method, passing the configured "dispatchName" property
 * as the argument.
 *
 * @author Chris Winters
 * @author ddoubleday
 */
public class CommandDispatcherRoot
    implements ApplicationContextAware, BeanFactoryPostProcessor {

    private static Logger log = new Logger(CommandDispatcher.class);

    private Map<String, String> commandsByName =
        new HashMap<String, String>();

    private ApplicationContext      ctx;

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext appCtx)
        throws BeansException {
        this.ctx = appCtx;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory bf)
        throws BeansException {

        String[] cmds = ctx.getBeanNamesForType(Command.class);
        addCommands(cmds);
    }

    /**
     * Add the specified set of command names to the dispatcher registry.
     * @param cmds the names of the commands to register with dispatcher.
     */
    private void addCommands(String[] cmds) {
        for (String beanName : cmds) {
            Command cmdBean = (Command) ctx.getBean(beanName);
            commandsByName.put(cmdBean.getDispatchName(), beanName);
            if (log.isDebugEnabled()) {
                log.debug("Added bean '" + beanName + "' to dispatcher");
            }
        }
    }

    /**
     * Find the command mapped to a given command name.
     * @param commandName name of the command to lookup
     * @return command
     */
    public Command dispatch(String commandName) {
        return getCommandByName(commandName);
    }

    /**
     * @param commandName the Spring bean name to retrieve by.
     * @return the associated command, or throw an exception if the bean
     * couldn't be found or was not a <code>Command</code> bean.
     * @throws BeansException
     * @throws ClassCastException
     *
     */
    private Command getCommandByName(String commandName) {
        if (log.isDebugEnabled()) {
            log.debug("Getting command for name: " + commandName);
        }
        try {
            return (Command) ctx.getBean(this.commandsByName.get(commandName));
        } catch (BeansException e) {
            log.error(
                "Cannot get bean " + commandName + ": " + e.getMessage(),
                SystemErrorCode.CONFIGURATION_ERROR);
            throw e;
        } catch (ClassCastException e) {
            log.error(
                "Bean '" + commandName + "' is not a Command object",
                SystemErrorCode.CONFIGURATION_ERROR);
            throw e;
        }
    }

    /**
     * @param cmd the Command whose properties are being set.
     * @param values the values to map to properties of the Command
     * @throws IllegalStateException when an argument cannot be mapped
     */
    protected void mapArgs(Command cmd, List<PropertyValue> values)
        throws IllegalStateException {
        BeanWrapper wrapper = new BeanWrapperImpl(cmd);
        addCustomEditors(wrapper);
        for (PropertyValue value : values) {
            if (log.isDebugEnabled()) {
                log.debug("Mapping " + value.getName() + " => "
                          + value.getValue());
            }
            try {
                wrapper.setPropertyValue(value);
            } catch (BeansException e) {
                log.error(
                    "Failed to map property " + value.getName() + ": "
                    + e.getMessage(),
                    SystemErrorCode.CONFIGURATION_ERROR);
                throw new IllegalStateException(e.getMessage());
            }
        }
    }

    /**
     * Add custom property editors to the BeanWrapper object that will
     * map command arguments to setters in the command.
     * @param wrapper the BeanWrapper to add custom editors to.
     */
    protected void addCustomEditors(BeanWrapper wrapper) {
        wrapper.registerCustomEditor(
            Locale.class, "locale", new TaskCommandCustomLocaleEditor());
    }

    /**
     * Map arguments to a command object.
     *
     * <p>
     * Each command makes its arguments available via
     * {@link com.vocollect.tm.Command#getDispatchArgs()}. This method uses
     * that data to map the ordered arguments to the given command.
     * </p>
     *
     * @param args ordered arguments to assign to the given command
     * @param cmd command to which we map arguments
     * @throws IllegalArgumentException if the number of argument values is
     *             greater than the number of arguments expected by the command
     * @throws IllegalStateException if we cannot map an argument to the command --
     *             this may happen with mismatched argument and property names
     *             or if the named property is not accessible
     */
    public void mapArgumentsToCommand(String[] args, Command cmd)
        throws IllegalArgumentException, IllegalStateException {
        String[] propertyNames = cmd.getDispatchArgs();
        if (args.length != propertyNames.length) {
            String cmdName = cmd.getDispatchName();
            String errorMsg = "Invalid number of arguments for command "
                + cmdName + ", arg length = " + args.length
                + ", property length = " + propertyNames.length;
            log.error(errorMsg, SystemErrorCode.CONFIGURATION_ERROR);
            throw new IllegalArgumentException(errorMsg);
        }
        if (args.length > 0) {
            List<PropertyValue> values = new ArrayList<PropertyValue>();
            for (int i = 0; i < args.length; i++) {
                values.add(new PropertyValue(propertyNames[i], args[i]));
            }
            mapArgs(cmd, values);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 