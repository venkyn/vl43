/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import java.util.HashMap;
import java.util.Map;



/**
 *
 *
 * @author estoll
 */
public class TaskVersionInfoRoot {
    private String combinedTaskVersion;
    private String lineloadingTaskVersion;

    private Map <String, String> minCompatibility =
            new HashMap<String, String>();
    private Map <String, String> maxCompatibility =
        new HashMap<String, String>();

    /**
     * Getter for the combinedTaskVersion property.
     * @return String value of the property
     */
    public String getCombinedTaskVersion() {
        return this.combinedTaskVersion;
    }

    /**
     * Setter for the combinedTaskVersion property.
     * @param combinedTaskVersion the new combinedTaskVersion value
     */
    public void setCombinedTaskVersion(String combinedTaskVersion) {
        this.combinedTaskVersion = combinedTaskVersion;
    }

    /**
     * Getter for the lineloadingTaskVersion property.
     * @return String value of the property
     */
    public String getLineloadingTaskVersion() {
        return this.lineloadingTaskVersion;
    }

    /**
     * Setter for the lineloadingTaskVersion property.
     * @param lineloadingTaskVersion the new lineloadingTaskVersion value
     */
    public void setLineloadingTaskVersion(String lineloadingTaskVersion) {
        this.lineloadingTaskVersion = lineloadingTaskVersion;
    }

    /**
     * Getter for the maxCompatibility property.
     * @return Collection
     */
    public Map<String, String> getMaxCompatibility() {
        return this.maxCompatibility;
    }

    /**
     * Setter for the maxCompatibility property.
     * @param maxCompatibility the new maxCompatibility value
     */
    public void setMaxCompatibility(Map<String, String> maxCompatibility) {
        this.maxCompatibility = maxCompatibility;
    }

    /**
     * Getter for the minCompatibility property.
     * @return Collection
     */
    public Map<String, String> getMinCompatibility() {
        return this.minCompatibility;
    }

    /**
     * Setter for the minCompatibility property.
     * @param minCompatibility the new minCompatibility value
     */
    public void setMinCompatibility(Map<String, String> minCompatibility) {
        this.minCompatibility = minCompatibility;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 