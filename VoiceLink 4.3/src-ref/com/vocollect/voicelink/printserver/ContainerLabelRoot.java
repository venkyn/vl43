/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author estoll
 */
public class ContainerLabelRoot extends BaseLabel {
    private Container container;

    /**
     * Constructor.
     * @param container - container object
     * @param copies - copies to print
     */
    public ContainerLabelRoot(Container container, Short copies) {
        super("Container", copies);
        this.container = container;
        //Build the LabelData
        addData();
    }


    /**
     * Getter for the container property.
     * @return Container value of the property
     */
    public Container getContainer() {
        return this.container;
    }


    /**
     * Build the labeldata from the Container object.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.BaseLabel#addData()
     */
    @Override
    protected void addData() {
        Map<String, Object> labelDataMap = new HashMap<String, Object>();
        // Build the map for a Container Label (assignment and container data)
        Integer count = 1;

        // To resolve a lazy loading issue, we are digging into
        // all the properties that the label is accessing
        // TODO EdStoll - turn this all into Spring...

        Operator o = null;
        Customer c = null;

        for (Assignment a : container.getAssignments()) {
            // to get around a lazy loading issue
            o = a.getOperator();
            c = a.getCustomerInfo();

            if (c != null) {
                c.getCustomerNumber();
            }
            if (o != null) {
                o.getOperatorIdentifier();
            }
            a.getNumber();
            a.getRoute();
            a.getDeliveryLocation();
            a.getDeliveryDate();

            String key = "assignment" + count.toString();
            labelDataMap.put(key, a);
        }

        labelDataMap.put("container", container);
        //Build a LabelData Object and assign it to the LabelDataMember
        LabelData ld = new LabelData(this.getTypeName(), labelDataMap, this.getCopies());
        this.setLabelData(ld);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 