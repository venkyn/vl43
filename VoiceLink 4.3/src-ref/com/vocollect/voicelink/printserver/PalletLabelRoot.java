/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.Assignment;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author estoll
 */
public class PalletLabelRoot extends BaseLabel {
    private Assignment assignment;

    /**
     * Constructor.
     * @param assignment - assignment object
     * @param copies - copies to print
     */
    public PalletLabelRoot(Assignment assignment, Short copies) {
        super("Pallet", copies);
        this.assignment = assignment;
        //Build the LabelData
        addData();
    }


    /**
     * Getter for the assignment property.
     * @return assignment value of the property
     */
    public Assignment getassignment() {
        return this.assignment;
    }


    /**
     * Build the labeldata from the assignment object.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.BaseLabel#addData()
     */
    @Override
    protected void addData() {
        Map<String, Object> labelDataMap = new HashMap<String, Object>();

        // Lazy loading issue
        Customer customer = assignment.getCustomerInfo();
        Operator operator = assignment.getOperator();

        if (customer != null) {
            customer.getCustomerNumber();
        }

        if (operator != null) {
            operator.getOperatorIdentifier();
        }

        // Build the map for a Pallet Label (assignment data)
        labelDataMap.put("assignment", assignment);

        //Build a LabelData Object and assign it to the LabelDataMember
        LabelData ld = new LabelData(this.getTypeName(), labelDataMap, this.getCopies());
        this.setLabelData(ld);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 