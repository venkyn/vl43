/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import com.loftware.lps.LPSInvalidFieldDataException;
import com.loftware.lps.LPSPrintController;
import com.loftware.lps.LPSPrintJobResponse;
import com.loftware.lps.LPSPrintSummaryResponse;
import com.loftware.lps.LPSTemplate;
import com.loftware.lps.connector.LPSConnectorException;
import com.loftware.lps.connector.LPSConnectorRuntimeException;
import com.loftware.lps.connector.LPSDirectory;
import com.loftware.lps.connector.LPSDirectoryFactory;
import com.loftware.lps.connector.LPSLabel;
import com.loftware.lps.connector.LPSLabelDAO;
import com.loftware.lps.connector.LPSPrinter;
import com.loftware.lps.connector.LPSServer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;


/**
 * This is the Loftware print server object to be used by the VoiceLink application.
 * It is an implementation of the AbstractPrintServerClass.
 *
 * @author phanson
 */
public class LoftwarePrintServer extends AbstractPrintServer {

    // Logging object.
    private static final Logger log = new Logger(AbstractPrintServer.class);


    /*
     * Loftware Java API class used to interface between the client
     * application and Loftware Connector framework.
     */
    private LPSPrintController lpsController = null;

    /*
     * Loftware Java API class defining the  lookup services used by the
     * LPSConnector classes. A reference to the LPSDirectory object is
     * necessary to create a instance of any of the Data Access Interfaces
     */
    private LPSDirectory lpsDir = null;

    private Collection<LPSPrinter> lpsPrinters = null;

    /**
     * Constructor.  Package private for test purposes.  Otherwise use the
     * other constructor for the default XML file name.
     * @param xmlConfigFile The name of the XML configuration file, which
     *      should contain a list of labels and fields.  This is required
     *      because the field names we get from Loftware are in all caps,
     *      which will not work.
     */
    LoftwarePrintServer(String xmlConfigFile) {
        super(PrintServerType.LOFTWARE, xmlConfigFile);
        initialize();
    }

    /**
     *  Default Constructor, which uses the default XML file name.
     */
    public LoftwarePrintServer() {
        super(PrintServerType.LOFTWARE);
        initialize();
    }

    /**
     *  Initialize function called by both constructors.
     */
    private void initialize() {
        // Instantiate the Loftware Java API objects
        lpsController = new LPSPrintController();
        lpsDir = LPSDirectoryFactory.defaultDirectory();
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updatePrinters()
     */
    @Override
    protected void updatePrinters() throws VocollectException {

        PrintServerErrorCode error = null;

        // Get the parent Printers Map object.
        Map<String, ServerPrinter> printers = super.getPrinters(false);

        /*
         * Here we get the collection of printers configured in all available
         * Loftware Print Servers and create Vocollect ServerPrinter objects which
         * are then stored in the parent Printers Map object.
         */
        try {

            lpsPrinters = lpsController.getPrinters();
            // Any exceptions should be thrown above.
            printers.clear();
            if (lpsPrinters != null) {
                for (Iterator<LPSPrinter> iter = lpsPrinters.iterator(); iter.hasNext();) {
                   LPSPrinter lpsPrinter = iter.next();
                   // Shouldn't we use printerName instead of alias below ?
                   ServerPrinter voPrinter =
                       new ServerPrinter(lpsPrinter.getPrinterAlias(), null, -1);
                   voPrinter.setModelName(lpsPrinter.getPrinterType());
                   printers.put(voPrinter.getPrinterName(), voPrinter);
                }
            }

        } catch (LPSConnectorRuntimeException e) {
            error = PrintServerErrorCode.LPS_RUNTIME_EXCEPTION;
            throw new VocollectException(error, e);
          }
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateLabels()
     */
    @Override
    protected void updateLabels() throws VocollectException {
        // First, load the labels from the XML file.
        super.loadXmlConfig();

        // Now query the API for the label list, and throw an error
        // if there's a mismatch.  The field names should match, except
        // the API will return field names in all caps.

        PrintServerErrorCode error = null;

        // Get the parent Labels List object.
        List<LabelTemplate> labelTemplates = super.getLabels(false);

        // Array to hold each label's list of field names.
        List<String> labelFieldList = null;

        /*
         * Here we get the list of labels configured in the Loftware
         * Connector Framework and create Vocollect Label objects which
         * are then stored in the parent LabelTemplates List object.
         */
        try {
            // Loftware Java API Data Access Object for LPS labels
            LPSLabelDAO myLPSLabelDAO = lpsDir.newLPSLabelDAO();
            Collection<LPSLabel> lpsLabels = myLPSLabelDAO.findAll();
            if (lpsLabels.size() != labelTemplates.size()) {
                error = PrintServerErrorCode
                    .XML_SERVER_MISMATCH;
                throw new VocollectException(error);
            }

            /*
             * Loop through each label discovered in the findAll, grabbing
             * their fields, and build a Vocollect LabelTemplate object which
             * we then add to our List of LabelTemplates.  At the same time,
             * iterate through the label template list.
             */
            LabelTemplate labelTemplate;

            if (lpsLabels.size() > 0) {
                for (Iterator<LPSLabel> iter = lpsLabels.iterator(); iter.hasNext();) {
                    LPSLabel label = iter.next();
                    labelTemplate = getLabel(label.getName());
                    if (labelTemplate == null) {
                        PrintServerErrorCode code = PrintServerErrorCode.XML_SERVER_MISMATCH;
                        throw new VocollectException(code);
                    }

                    // Get the list of fields.
                    labelFieldList = getLabelFields(label.getName());

                    validateLabelFields(labelTemplate.getLabelFieldList(),
                        labelFieldList, labelTemplate.getLabelName());
                }
            }

        } catch (LPSConnectorException e) {
            error = getLPSConnectorError(e.severity());
            throw new VocollectException(error, e);
        } catch (LPSConnectorRuntimeException e) {
            error = PrintServerErrorCode.LPS_RUNTIME_EXCEPTION;
            throw new VocollectException(error, e);
        }
    }

    /**
     * Examine two field lists, one from the current template, which should
     * have been loaded from an XML file, and one from the Loftware API.
     * Note that we assume neither list is null.
     * @param templateFields    Current fields loaded from XML file
     * @param lpsFields         Loftware fields loaded through API
     * @param labelName         Name of the label, for the error message
     * @throws VocollectException The exception will contain a detailed error
     *      message if there is a validation problem.
     */
    private static void validateLabelFields(List<String> templateFields,
                                            List<String> lpsFields,
                                            String labelName)
    throws VocollectException {
        UserMessage error = null;
        PrintServerErrorCode code = null;
        if (templateFields.size() != lpsFields.size()) {
            code = PrintServerErrorCode
                .XML_SERVER_NUM_FIELDS;
            // Need array of strings for error message
            Object[] args = { templateFields.size(), labelName,
                lpsFields.size() };
            error = new UserMessage(code, args);
            throw new VocollectException(code, error);
        }

        // Loop through and validate field names.  Ignore case, because
        // we know it won't be correct.
        for (int i = 0; i < templateFields.size(); i++) {
            if (templateFields.get(i).compareToIgnoreCase(
                lpsFields.get(i)) != 0) {
                // Different field names
                code = PrintServerErrorCode
                    .XML_SERVER_FIELD_MISMATCH;
                // Array of parameters
                Object[] args = { templateFields.get(i), labelName,
                    lpsFields.get(i) };
                error = new UserMessage(code, args);
                throw new VocollectException(code, error);
            }
        }
    }

    /**
     * Returns the collection of print fields in a given label template.
     * @param labelName     Name of the label whose fields we want to list
     * @return              String List of field names
     * @throws VocollectException On getLabelFields error.
     */
    private List<String> getLabelFields(String labelName)
    throws VocollectException {

        PrintServerErrorCode error = null;
        List<String> labelFieldList = new ArrayList<String>();

        try {
            // Use the LPSPrintController method getServers to return a
            // collection of servers known to the Controller.
            Collection<LPSServer> allServers = null;
            allServers = lpsController.getServers();

            // Loop through each server and find the label template
            // of the passed in label name.
            for (Iterator<LPSServer> iter = allServers.iterator(); iter.hasNext();) {
                LPSServer server = iter.next();
                LPSTemplate lblTemplate = lpsController.templateForLabel(
                   labelName, server.getServerAddress());

                // Build the list of fields from the label template.
                List<Map<String, String>> fields = lblTemplate.getLabelFields();
                for (Iterator<Map<String, String>> fieldIter = fields.iterator(); fieldIter.hasNext();) {
                    Map<String, String> fldMap = fieldIter.next();
                    labelFieldList.add(fldMap.get("name"));
                }
            }
        } catch (LPSConnectorException e) {
            error = getLPSConnectorError(e.severity());
            throw new VocollectException(error, e);
        }

        return labelFieldList;

    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateJobQueue()
     */
    @Override
    protected void updateJobQueue() throws VocollectException {
        // Check the head of the queue, and shift to the history
        // collection until the head of the queue is not yet printed.
        PrintServerErrorCode error = null;
        int checkQueue = -1;
        PrintJob queueJob = null;
        Queue<PrintJob> jobQueue = getJobQueueSafe();

        do {
            // get the job at the head of the queue and check its status
            queueJob = jobQueue.peek();

            if (queueJob != null) {
                try {
                    LPSPrintJobResponse response =
                        lpsController.printStatusForJob(queueJob.getJobId(),
                            queueJob.getServerName());

                    // Set the job's status code based on the mapped LPS status codes.
                    setStatusCode(response.getStatusCode(), queueJob);

                    if (queueJob.getJobStatus() == PrintJob.
                        JobStatus.IN_SERVER_QUEUE) {
                        // Job is still pending, so all other jobs will be pending
                        // as well which means we exit the loop
                        checkQueue = 1;
                    } else {
                        // The job is no longer pending, so we take it out of the
                        // job queue and put it into the job history
                        jobHistoryAdd(jobQueue.remove());
                    }

                 } catch (LPSConnectorException e) {
                     error = getLPSConnectorError(e.severity());
                     throw new VocollectException(error, e);
                 }
            }
        } while (checkQueue == -1);

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#submitJob(com.vocollect.voicelink.printserver.PrintJob)
     */
    @Override
    protected synchronized long processJob(PrintJob newJob) throws VocollectException {

        PrintServerErrorCode error = null;
        LPSPrintSummaryResponse  prtSumRsp = null;

        String printerName = newJob.getPrinterName();
        List<LabelData> labelDataList = newJob.getLabelList();

        // Build a collection of templates based on the labels in the
        // LabelData list.
        Collection<LPSTemplate> templateCollection =
            getTemplateCollection(printerName, labelDataList, newJob);

        try {
            prtSumRsp = lpsController.printJob(templateCollection, printerName,
                "Loftware Print Job", 0, 0, false);
        } catch (LPSConnectorException e) {
            // Set job status and put into job history
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            jobHistoryAdd(newJob);
            error = getLPSConnectorError(e.severity());
            throw new VocollectException(error, e);
        }

        // The print summary response object should only contain a max of
        // 1 print job response, as the Loftware documentation indicates
        // that multiple responses will only be created when using multiple
        // printers.  We only use one printer per job.
        assert prtSumRsp.getPrintJobResponses().size() == 1 : "Should have exactly 1 print job response.";

        LPSPrintJobResponse response = (LPSPrintJobResponse)
            prtSumRsp.getPrintJobResponses().get(0);

        // Set the job's status code based on the mapped LPS status codes.
        setStatusCode(response.getStatusCode(), newJob);

        // Set the job's server name; this will be used later to retrieve
        // the status of the job at a later time.
        newJob.setServerName(response.getServerName());

        // If we get to this point, add to the job queue
        getJobQueueSafe().add(newJob);

        // Return the new job ID
        return response.getJobNumber();
    }

    /**
     * Returns the collection of label templates with print data mapped to the
     * template label fields.
     * @param printerName       LPS alias name of the printer handling the job
     * @param labelDataList     collection of LabelData objects
     * @param newJob        The print job submitted to the server
     * @return                  collection of label templates
     * @throws VocollectException On getTemplateCollection error.
     */
    private Collection<LPSTemplate> getTemplateCollection(String printerName,
        List<LabelData> labelDataList, PrintJob newJob) throws VocollectException {

        PrintServerErrorCode error = null;
        Collection<LPSTemplate> templateCollection = new ArrayList<LPSTemplate>();
        LPSTemplate lpsLabel = null;
        LabelTemplate labelConfig = null;

        // Loop for each label
        Iterator<LabelData> labels = labelDataList.iterator();
        while (labels.hasNext()) {
            LabelData voLabelData = labels.next();

            // The Vocollect Label data does not have the loftware
            // ".lwl" as part of the label name.
            String labelName = voLabelData.getLabelName() + ".lwl";

            try {
                // Create an LPS label
                lpsLabel = lpsController.templateForLabel(labelName, null,
                    printerName);

                List<Map<String, String>> lpsFields = lpsLabel.getLabelFields();

                // Get the corresponding LabelTemplate from our member variable
                labelConfig = super.getLabel(labelName);
                if (labelConfig == null
                    || labelConfig.getLabelFieldList().size()
                    != lpsFields.size()) {
                    error = PrintServerErrorCode
                        .XML_SERVER_MISMATCH;
                    throw new VocollectException(error);
                }
                String templateFieldName;   // From XML / LabelTemplate object
                String lpsFieldName;        // From Loftware

                // Loop through each label field of the LPS label, and get the
                // field name from the labelConfig.
                for (int i = 0; i < lpsFields.size(); i++) {
                    Map<String, String> fldMap = lpsFields.get(i);
                    templateFieldName = labelConfig.getLabelFieldList().get(i);

                    // Note:  Loftware returns field names in all uppercase;
                    // do a case-insensitive check
                    lpsFieldName = fldMap.get("name");
                    int maxLength = Integer.parseInt(fldMap.get("length"));
                    if (templateFieldName.compareToIgnoreCase(lpsFieldName)
                        != 0) {
                        // Mismatched fields
                        error = PrintServerErrorCode
                            .XML_SERVER_FIELD_MISMATCH;
                        Object[] args = { templateFieldName, labelName,
                            lpsFieldName };
                        UserMessage message = new UserMessage(error, args);
                        throw new VocollectException(error, message);
                    }

                    // Put the field name in Freemarker format
                    String freemarkerFieldName = "${" + templateFieldName + "}";
                    String fieldValue = Utilities.processTemplate(
                        freemarkerFieldName, voLabelData);

                    if (fieldValue.length() > maxLength) {
                        // Log this condition and truncate the data
                        log.error("Label [" + labelName
                                    + "] Field [" + templateFieldName
                                    + "] has been truncated",
                                    PrintServerErrorCode.DATA_FIELD_LENGTH);

                        fieldValue = fieldValue.substring(0, maxLength);
                    }


                    // Put field value into LPS template
                    lpsLabel.setData(templateFieldName, fieldValue);
                }

            } catch (LPSInvalidFieldDataException ex) {
                error = PrintServerErrorCode
                    .DATA_FIELD_LENGTH;
                throw new VocollectException(error, ex);
            } catch (LPSConnectorException e) {
                // Set job status and put into job history
                newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                jobHistoryAdd(newJob);
                error = getLPSConnectorError(e.severity());
                throw new VocollectException(error, e);
            }

            lpsLabel.setQuantity(voLabelData.getCopies());

            templateCollection.add(lpsLabel);

        }

        return templateCollection;

    }

    /**
     * Mapping of the Loftware LSPPrintJobResponse status code to the Vocollect
     * PrintJob status code and setting that code in the PrintJob object.
     * @param statusCode    LPSPrintJobReponse status code
     * @param newJob        printJob submitted to the processJob request
     * @throws VocollectException Updating the status may trigger a callback
     *      function, which could in turn throw an error.
     */
    private void setStatusCode(int statusCode, PrintJob newJob)
    throws VocollectException {

        switch(statusCode) {
            case LPSPrintJobResponse.STAT_PRINTED:
                newJob.setJobStatus(PrintJob.JobStatus.PRINT_SUCCESSFUL);
                break;
            case LPSPrintJobResponse.STAT_PRINTED_WITH_ERRORS:
                newJob.setJobStatus(PrintJob.JobStatus.PRINTED_WITH_ERRORS);
                break;
            case LPSPrintJobResponse.STAT_PRINTERERROR:
                newJob.setJobStatus(PrintJob.JobStatus.PRINTER_ERROR);
                break;
            case LPSPrintJobResponse.STAT_CRIT_FAIL:
                newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                break;
            case LPSPrintJobResponse.STAT_PENDING:
                newJob.setJobStatus(PrintJob.JobStatus.IN_SERVER_QUEUE);
                break;
            case LPSPrintJobResponse.STAT_DELETED:
                newJob.setJobStatus(PrintJob.JobStatus.DELETED);
                break;
            case LPSPrintJobResponse.STAT_SPOOLED:
                newJob.setJobStatus(PrintJob.JobStatus.SPOOLED_TO_PRINTER);
                break;
            default:
                newJob.setJobStatus(PrintJob.JobStatus.UNKNOWN_JOB);
                break;

        }

    }

    /**
     * Checks the LPSConnectorError severity level to determine if it is a
     * critical error requiring handling.
     * @param severity    LPSConnectorException severity level
     * @return            errorCode mapped to a PrintServerErrorCode
     *
     */
    private PrintServerErrorCode getLPSConnectorError(int severity) {
        PrintServerErrorCode errorCode = null;
        switch(severity) {
            case LPSConnectorException.SEVERITY_LEVEL_ERROR:
                errorCode = PrintServerErrorCode.LPS_CONNECTOR_EXCEPTION_SEVERE;
                break;
            case LPSConnectorException.SEVERITY_LEVEL_PRINTER_ERROR:
                errorCode = PrintServerErrorCode.LPS_CONNECTOR_EXCEPTION_PRINTER;
                break;
            default:
                // The above error levels should be all that's ever returned
                // by an exception.
                assert false : "Invalid exception severity from Loftware";
                break;
        }

        return errorCode;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#shutdown()
     */
    @Override
    public void shutdown() {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 