/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;

import java.util.Iterator;
import java.util.List;


/**
 * This is the NiceWatch print server object to be used by the VoiceLink application.
 * It is an implementation of the AbstractPrintServerClass.
 *
 * @author phanson
 */
public class NiceWatchPrintServer extends AbstractPrintServer {

    //private final String xmlConfigFileName = "NiceWatchPrintServer.xml";
    private int nextJobId = 1;

    // This holds variable names defined in the NiceWatch Trigger which
    // handles the TCP/IP print request; these are a standard set of variables
    // which do not need to be defined in each NiceLabel label's text database
    // definition
    private final String templateHeaderPrefix =
        "PrinterName,LabelName,NumberOfCopies,";

    /**
     * Constructor.  Package private for test purposes.  Otherwise use the
     * other constructor for the default XML file name.
     * @param xmlConfigFile The name of the XML configuration file, which
     *      should contain a list of labels and fields.  The list of labels
     *      should correspond to labels defined in the NiceWatch print label
     *      TCP/IP triggers
     */
    NiceWatchPrintServer(String xmlConfigFile) {
        super(PrintServerType.NICEWATCH, xmlConfigFile);

    }

    /**
     *  Default Constructor, which uses the default XML file name.
     */
    public NiceWatchPrintServer() {
        super(PrintServerType.NICEWATCH);

    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updatePrinters()
     */
    protected void updatePrinters() {
        loadXmlConfig();
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateLabels()
     */
    protected void updateLabels() {
        loadXmlConfig();
    }

    /**
     * Our job queue will always be up to date, because when jobs finish
     * processing, they will update the queues automatically.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateJobQueue()
     */
    protected void updateJobQueue() {
        return;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#submitJob(com.vocollect.voicelink.printserver.PrintJob)
     */
    @Override
    protected long processJob(PrintJob newJob) throws VocollectException {

        // Synchronize access to jobID to prevent duplicates
        synchronized (this) {
            // Set the job id
            newJob.setJobId(nextJobId);

            // Increment the nextJobId
            if (nextJobId == java.lang.Integer.MAX_VALUE) {
                // Roll over to 1 again.
                nextJobId = 1;
            } else {
                nextJobId++;
            }
        }

        // Get the printer
        String printerName = newJob.getPrinterName();

        // Get the list of labels that will be printed in the job
        List<LabelData> labelDataList = newJob.getLabelList();

        // Validate the list of labels and label fields against the list of
        // label templates from our XML configuration file
        Utilities.validateLabelList(labelDataList, this);

        // Iterate through the list of label data objects and using the label
        // type name get the LabelTemplate for the label
        Iterator<LabelData> labels = labelDataList.iterator();
        while (labels.hasNext()) {

            LabelData voLabelData = labels.next();
            String labelName = voLabelData.getLabelName(); // get the name

            // Get the template for the label data
            LabelTemplate labelTemplate = super.getLabel(labelName);

            // Now we get the list of label fields from the template
            // to build our Header Line and Freemarker template
            List fieldList = labelTemplate.getLabelFieldList();

            // A string to hold the header line of field names
            StringBuilder headerString = new StringBuilder();

            // A string to hold the Freemarker field name template
            StringBuilder freeMarkerTemplate = new StringBuilder();

            // Append the printer name, label name, and copies to each
            // string
            headerString.append(templateHeaderPrefix);

            freeMarkerTemplate.append("\"" + printerName + "\"");
            freeMarkerTemplate.append(",\"" + labelName + "\"");
            freeMarkerTemplate.append("," + voLabelData.getCopies());

            // Now we iterate through the list of fields for the given
            // label and append those to our header string and also to
            // our Freemarker string with the appropriate Freemarker
            // markup code.
            Iterator fieldListIter = fieldList.iterator();
            while (fieldListIter.hasNext()) {
                String fieldName = (String) fieldListIter.next();
                headerString.append("," + fieldName);

                // Since each field name is delimited by quotes in the
                // NiceLabel text database, we append the quotation
                // symbol along with the Freemarker.
                freeMarkerTemplate.append(",\"${" + fieldName + "}\"");

            }

            // Parent function which utilizes Freemarker to map the
            // label data values to a field template and return a populated
            // field string.
            String dataString = Utilities.processTemplate(
                freeMarkerTemplate.toString(), voLabelData);

            // Now we ship the print statements to a NiceWatch monitored
            // port where a communication trigger will initiate a print
            // job.
            Utilities.sendPrintJob(headerString.toString(), dataString,
                this.getServerHostName(), labelTemplate.getPrintPort(), newJob);

        } // end of while loop

        //  Set job status and put into job history
        newJob.setJobStatus(PrintJob.JobStatus.PRINT_SUCCESSFUL);
        jobHistoryAdd(newJob);

        return newJob.getJobId();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#shutdown()
     */
    @Override
    public void shutdown() {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 