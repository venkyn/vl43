/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;
import java.util.Map;

/**
 * This class contains all the data needed to print a label.  It should
 * correspond to a LabelTemplate object returned from the print server,
 * but the fields are stored differently.
 *
 * @author cblake
 */
public class LabelData {
    /**
     * Label type name that corresponds to LabelTemplate object.
     */
    private String labelName;

    /**
     * A map of label data objects.  The keys correspond to object names,
     * and the values correspond to object instances.  For instance, the key
     * could be "Assignment," and the value would be an Assignment object.
     * Data will be retrieved through reflection by calling accessors through
     * the getFieldName convention.
     */
    private Map<String, Object> labelDataMap;

    /**
     * # of copies to print of this label.
     */
    private short copies = 1;   // Default # of copies

    /**
     * Constructor.
     * @param labelName     Name of the type of label
     * @param labelDataMap      Mapping of fields and data in the label
     * @param copies            Number of copies of the label to print
     */
    public LabelData(final String labelName,
                     final Map<String, Object> labelDataMap,
                 short copies) {
        this.labelName = labelName;
        this.labelDataMap = labelDataMap;
        this.copies = copies;
    }

    /**
     *
     * @return The label type name
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     *
     * @return The map of data objects for this label.  Each data object
     * contains all the relevant fields, accessible through standard
     * getX convention.
     */
    public Map<String, Object> getLabelDataMap() {
        return labelDataMap;
    }

    /**
     * @return  number of Label's copies
     */
    public short getCopies() {
        return copies;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 