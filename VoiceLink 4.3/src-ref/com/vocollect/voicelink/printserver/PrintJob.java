/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * This represents a print job sent to the print server.
 *
 * @author phanson
 */
public class PrintJob {
    /**
     * An enumeration of potential job statuses.  Of the error messages,
     * only one is a fatal error:  PRINTER_ERROR.  If a fatal error is
     * generated during the print job submission, prior to returning, then
     * an exception will be thrown with an error code explaining the more
     * detailed message.
     *
     * @author cblake
     */
    public enum JobStatus {
        NEW,
        IN_SERVER_QUEUE,
        SPOOLED_TO_PRINTER,
        PRINT_SUCCESSFUL,
        PRINTED_WITH_ERRORS,
        PRINTER_ERROR,
        PRINT_FAILED,
        DELETED,
        UNKNOWN_JOB
    }

    private long jobId;
    private String printerName;
    private String serverName;
    private List<LabelData> labelList;
    private JobStatus jobStatus;
    private String errorMessage = "";
    private ErrorCode errorCode = null;
    private ServerPrinter jobPrinter = null;  // Initialized on job submission

    /**
     * The constructor can pass in an object which will be called upon job
     * completion.
     */
    private Object callBackObject;

    /**
     * This is the name of the method to be used in the callback.
     */
    private String callBackMethodName;

    private Method callBackMethod = null;

    /**
     * Constructor.  This is used when there is no callback function.
     * @param printerName   Name of the printer receiving the job
     * @param labelList     List of labels to be printed by the job
     * @throws VocollectException   if the constructor has bad arguments.
     */
    public PrintJob(final String printerName, final List<LabelData> labelList)
    throws VocollectException {
        this(printerName, labelList, null, "");
    }

    /**
     * Constructor.
     * @param printerName   Name of the printer receiving the job
     * @param labelList     List of labelData objects to be printed
     * @param callBackObject    An object containing a callback method to
     *      be invoked upon job completion.  If null, the callback will not
     *      be invoked.
     * @param callBackMethodName    The name of the method to invoke in the
     *      callback object.  Should be empty string when the callback object
     *      is null and not used.  This allows the calling class to trigger
     *      its own code handling.
     * @throws VocollectException   If the callback object and method are
     *      not setup properly, we will throw an appropriate error code.
     */
    public PrintJob(final String printerName, final List<LabelData> labelList,
                    final Object callBackObject,
                    final String callBackMethodName)
    throws VocollectException {
        this.jobId = -1;    // jobId will be set when submitted
        this.printerName = printerName;
        this.labelList = labelList;
        // Default the status to NEW until the job is submitted
        this.jobStatus = JobStatus.NEW;
        this.callBackObject = callBackObject;
        this.callBackMethodName = callBackMethodName;
        if (callBackObject != null) {
            // Validate the callback method name
            PrintServerErrorCode error = null;
            if (callBackMethodName == null) {
                error = PrintServerErrorCode.NULL_CALLBACK_METHOD_NAME;
                throw new VocollectException(error);
            }
            Class<?> callBackClass = callBackObject.getClass();
            try {
                callBackMethod = callBackClass.getMethod(
                    this.callBackMethodName);
            } catch (NoSuchMethodException ex) {
                error = PrintServerErrorCode.INVALID_CALLBACK_METHOD;
                throw new VocollectException(error);
            }
        }
        this.errorMessage = null;
        this.serverName = null;
    }

    /**
     * @return  Print job's identification number
     */
    public long getJobId() {
        return jobId;
    }

    /**
     * Mutator method should only be accessible within the package.
     * @param jobId New JobID value.  Typically updated during submitJob().
     */
    void setJobId(long jobId) {
        this.jobId = jobId;
    }

    /**
     * @return  Print job's printer name
     */
    public String getPrinterName() {
        return printerName;
    }

    /**
     * @return  Print job's server name
     */
    public String getServerName() {
        return serverName;
    }

    /**
     * @param  serverName Print job's server name.
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }


    /**
     * @return  List of labels available to the printer
     */
    public List<LabelData> getLabelList() {
        return labelList;
    }

    /**
     * @return  Print job's status.  The actual values returned depend on
     * the print server being used.  The sequence of statuses on a valid
     * print job with no errors would be:
     *  NEW (when created for submission by VoiceLink)
     *  IN_SERVER_QUEUE (Print server hasn't yet sent to printer)
     *  SPOOLED_TO_PRINTER (job has been sent to printer)
     *  PRINT_SUCCESSFUL (job has completed)
     *  UNKNOWN_JOB (after 100 subsequent jobs have been submitted, and the
     *      job has been purged from the history)
     *
     *  PRINTER_ERROR should indicate problems like paper jam, out of paper,
     *      etc.
     *  DELETED indicates that an operator has deleted the job from the server
     *      or printer.
     */
    public JobStatus getJobStatus() {
        return jobStatus;
    }

    /**
     * @return  Any error message pertaining to this print job.  This will
     * only be set if it comes from the print server directly, in which
     * case it is presumably already localized.
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Package protected setter of the print job error message.
     * @param errorMessage  Any new error message for the print job.
     */
    void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * The print server may set error codes in here if there is
     * a problem when printing.
     * @return  An error code encountered during printing, or null if
     *      there has been no error.
     */
    public ErrorCode getErrorCode() {
        return errorCode;
    }

    /**
     *
     * @param errorCode New print server error code encountered while printing.
     */
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Mutator method.  Only accessible within this package, because
     * only the print servers should be modifying job status.  When a job
     * status is changed to be completed (failure or success), we must
     * invoke the callback method if needed.  Any error encountered when
     * invoking the function will be set in the print job object.
     *
     * Note that the error code should be set prior to this function being
     * called, because this initiates the callback.  The error code should be
     * updated when the callback is invoked.
     *
     * @param status    The new status to apply to this job.
     */
    void setJobStatus(JobStatus status) {
        if (!isJobComplete(this.jobStatus) && isJobComplete(status)) {
            // Job status has transitioned from an incomplete status
            // to a complete status, so call the callback method.
            if (callBackMethod != null) {
                try {
                    callBackMethod.invoke(callBackObject);
                } catch (IllegalAccessException ex) {
                    PrintServerErrorCode error = PrintServerErrorCode
                        .CALLBACK_ACCESS_ERROR;
                    setErrorCode(error);
                } catch (InvocationTargetException ex) {
                    PrintServerErrorCode error = PrintServerErrorCode
                        .CALLBACK_EXCEPTION;
                    setErrorCode(error);
                }

            }
        }

        // Update the member variable
        this.jobStatus = status;
    }

    /**
     * This utility method is used to test whether the given job status
     * represents completion or not.  Fatal errors are terminal, so they
     * will cause a return value of true.
     * @param status    The job status to examine
     * @return          Whether or not this job is done processing
     */
    public static boolean isJobComplete(JobStatus status) {
        switch (status) {
        case NEW:
        case IN_SERVER_QUEUE:
        case SPOOLED_TO_PRINTER:
        case PRINTER_ERROR:
            // These statuses indicate that the job is not done processing
            return false;
        default:
            return true;
        }
    }

    /**
     * Get the printer object;  not known until job is submitted.  Null
     * value indicates an error.  Only accessible in package.
     * @return  The printer object with which this job will be printed
     */
    ServerPrinter getJobPrinter() {
        return jobPrinter;
    }

    /**
     * Package protected setter.  The print server will find the needed
     * printer object and set it here.
     * @param printer   The new associated printer object
     */
    void setJobPrinter(ServerPrinter printer) {
        this.jobPrinter = printer;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 