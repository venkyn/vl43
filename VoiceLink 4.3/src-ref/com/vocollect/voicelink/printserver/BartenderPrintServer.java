/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;

import java.util.Iterator;
import java.util.List;


/**
 * This is the Bartender print server object to be used by the VoiceLink application.
 * It is an implementation of the AbstractPrintServerClass.
 *
 * @author phanson
 */
public class BartenderPrintServer extends AbstractPrintServer {

    //private final String xmlConfigFileName = "BartenderPrintServer.xml";
    private int nextJobId = 1;

     /**
     * Constructor.  Package private for test purposes.  Otherwise use the
     * other constructor for the default XML file name.
     * @param xmlConfigFile The name of the XML configuration file, which
     *      should contain a list of labels and fields.  The list of labels
     *      should correspond to labels defined in the NiceWatch print label
     *      TCP/IP triggers
     */
    BartenderPrintServer(String xmlConfigFile) {
        super(PrintServerType.BARTENDER, xmlConfigFile);

    }

    /**
     *  Default Constructor, which uses the default XML file name.
     */
    public BartenderPrintServer() {
        super(PrintServerType.BARTENDER);

    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updatePrinters()
     */
    protected void updatePrinters() {
        loadXmlConfig();
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateLabels()
     */
    protected void updateLabels() {
        loadXmlConfig();
    }

    /**
     * Our job queue will always be up to date, because when jobs finish
     * processing, they will update the queues automatically.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateJobQueue()
     */
    protected void updateJobQueue() {
        return;
    }

    /**
     * If an exception is thrown in this code, the abstract print server will
     * automatically add the job to the job history.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#submitJob(com.vocollect.voicelink.printserver.PrintJob)
     */
    @Override
    protected long processJob(PrintJob newJob)
    throws VocollectException {
        final int headerBufferSize = 1024;
        final int templateBufferSize = 1024;

        int labelCounter = 1;

        // The job ID must be set & updated in a thread safe way, to
        // ensure multiple jobs don't get the same JobID.
        synchronized (this) {
            // Set the job id
            newJob.setJobId(nextJobId);

            // Increment the nextJobId
            if (nextJobId == java.lang.Integer.MAX_VALUE) {
                // Roll over to 1 again.
                nextJobId = 1;
            } else {
                nextJobId++;
            }
        }

        // Get the printer
        String printerName = newJob.getPrinterName();

        // Get the list of labels that will be printed in the job
        List<LabelData> labelDataList = newJob.getLabelList();

        // Validate the list of labels and label fields against the list of
        // label templates from our XML configuration file
        Utilities.validateLabelList(labelDataList, this);

        // Iterate through the list of label data objects and using the label
        // type name get the LabelTemplate for the label
        Iterator<LabelData> labels = labelDataList.iterator();
        while (labels.hasNext()) {

            LabelData voLabelData = labels.next();
            String labelName = voLabelData.getLabelName(); // get the name

            // Get the template for the label data
            LabelTemplate labelTemplate = super.getLabel(labelName);

            // Now we get the list of label fields from the template
            // to build our Header Line and Freemarker template
            List<String> fieldList = labelTemplate.getLabelFieldList();


            // A string to hold the Commander script
            String commandScript =
                "%BTW% /AF=" + labelName + " /D=<Trigger File Name> "
                + " /PRN=\"" + printerName + "\" /R=3 /DbTextHeader=3 /C="
                + voLabelData.getCopies() + " /P /DD\r\n%END%\r\n";

            // A string to hold the header line of field names
            StringBuilder headerString =
                new StringBuilder(headerBufferSize);

            // A string to hold the Freemarker field name template
            StringBuilder freeMarkerTemplate =
                new StringBuilder(templateBufferSize);

            // Now we iterate through the list of fields for the given
            // label and append those to our header string and also to
            // our Freemarker string with the appropriate Freemarker
            // markup code.
            Iterator<String> fieldListIter = fieldList.iterator();
            while (fieldListIter.hasNext()) {
                String fieldName = fieldListIter.next();
                headerString.append("," + fieldName);

                // Since each field name is delimited by quotes in the
                // NiceLabel text database, we append the quotation
                // symbol along with the Freemarker.
                freeMarkerTemplate.append(",\"${" + fieldName + "}\"");

            }

            headerString.deleteCharAt(0);
            freeMarkerTemplate.deleteCharAt(0);

            // Parent function which utilizes Freemarker to map the
            // label data values to a field template and return a populated
            // field string.
            String dataString = Utilities.processTemplate(
                freeMarkerTemplate.toString(), voLabelData);

            // Build the file name
            // The filename has the job number followed by an underscore
            // then the label number of count.
            String fileName = this.getFileDropLocation()
            + "BartenderPrintJob_" + String.valueOf(newJob.getJobId())
            + "_" + String.valueOf(labelCounter) + ".txt";

            // Build the data string for the file
            String fileData = commandScript + headerString + "\r\n" + dataString;

            // Now we ship the print statements to a Bartender Commander monitored
            // file share where a Commander task will initiate a print job.
            Utilities.sendPrintJob(fileName, fileData, newJob, this);

            labelCounter++;

        } // end of while loop

        //  Set job status and put into job history
        newJob.setJobStatus(PrintJob.JobStatus.PRINT_SUCCESSFUL);
        jobHistoryAdd(newJob);

        return newJob.getJobId();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#shutdown()
     */
    @Override
    public void shutdown() {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 