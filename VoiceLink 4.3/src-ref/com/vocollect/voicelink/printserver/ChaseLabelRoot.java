/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Pick;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author estoll
 */
public class ChaseLabelRoot extends BaseLabel {
    private Pick pick;

    /**
     * Constructor.
     * @param pick - pick object
     * @param copies - copies to print
     */
    public ChaseLabelRoot(Pick pick, Short copies) {
        super("Chase", copies);
        this.pick = pick;
        //Build the LabelData
        addData();
    }


    /**
     * Getter for the pick property.
     * @return Pick value of the property
     */
    public Pick getPick() {
        return this.pick;
    }


    /**
     * Build the labeldata from the Pick object.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.BaseLabel#addData()
     */
    @Override
    protected void addData() {
        Map<String, Object> labelDataMap = new HashMap<String, Object>();
        // access the item and location information so Hibernate loads
        // the data before we get passed off to the Printer thread.

        Assignment assignment = null;
        Assignment originalAssignment = null;

        //Check assignment type
        if (pick.getAssignment().getType() == AssignmentType.Chase) {
            originalAssignment = pick.getOriginalPick().getAssignment();
            assignment = pick.getAssignment();
        } else {
            assignment = pick.getAssignment();
            originalAssignment = pick.getAssignment();
        }

        Operator operator = assignment.getOperator();
        Operator originalOperator = originalAssignment.getOperator();

        Customer customer = assignment.getCustomerInfo();
        Customer originalCustomer = originalAssignment.getCustomerInfo();

        Location location = pick.getLocation();
        Item item = pick.getItem();
        // TODO EdStoll - turn this all into Spring...

        if (customer != null) {
            customer.getCustomerNumber();
        }
        if (originalCustomer != null) {
            originalCustomer.getCustomerNumber();
        }

        if (item != null) {
            pick.getItem().getUpc();
            pick.getItem().getDescription();
        }

        if (location != null) {
            pick.getLocation().getAisle();
            pick.getLocation().getSlot();
        }

        if (operator != null) {
            operator.getOperatorIdentifier();
        }
        if (originalOperator != null) {
            originalOperator.getOperatorIdentifier();
        }

        assignment.getNumber();
        originalAssignment.getNumber();
        assignment.getDeliveryDate();
        originalAssignment.getDeliveryDate();
        assignment.getDeliveryLocation();
        originalAssignment.getDeliveryLocation();

        // Build the map for a Chase Label (assignment and pick data)
        labelDataMap.put("assignment", assignment);
        labelDataMap.put("originalAssignment", originalAssignment);
        labelDataMap.put("pick", pick);

        //Build a LabelData Object and assign it to the LabelDataMember
        LabelData ld = new LabelData(this.getTypeName(), labelDataMap, this.getCopies());
        this.setLabelData(ld);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 