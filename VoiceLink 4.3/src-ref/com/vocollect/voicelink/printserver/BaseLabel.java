/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;


/**
 *
 *
 * @author estoll
 */
public abstract class BaseLabel {
    private LabelData labelData;
    private short copies;
    private String typeName;

    /**
     * Constructor.
     * @param typeName - Chase, Container or Pallet
     * @param copies - number of copies to print
     */
    public BaseLabel(String typeName, short copies) {
        this.typeName = typeName;
        this.copies = copies;
    }

    /**
     * Getter for the copies property.
     * @return short value of the property
     */
    public short getCopies() {
        return this.copies;
    }

    /**
     * Setter for the copies property.
     * @param copies the new copies value
     */
    public void setCopies(short copies) {
        this.copies = copies;
    }

    /**
     * Getter for the labelData property.
     * @return LabelData value of the property
     */
    public LabelData getLabelData() {
        return this.labelData;
    }

    /**
     * Setter for the labelData property.
     * @param labelData the new labelData value
     */
    public void setLabelData(LabelData labelData) {
        this.labelData = labelData;
    }

    /**
     * Getter for the typeName property.
     * @return String value of the property
     */
    public String getTypeName() {
        return this.typeName;
    }

    /**
     * Setter for the typeName property.
     * @param typeName the new typeName value
     */
    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    /**
     * Execute this method to populate labelData.
     */
    protected abstract void addData();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 