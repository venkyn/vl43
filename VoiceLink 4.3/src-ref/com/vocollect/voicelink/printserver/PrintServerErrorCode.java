/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for print server. Although the
 * range for these error codes is 8000-8999, please use only 8000-8499 for
 * the predefined ones; 3500-3999 should be reserved for customization error
 * codes.
 *
 * @author mkoenig
 */
public final class PrintServerErrorCode extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 8000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 8999;

    /**
     * No error, just the base initialization.
     */
    public static final PrintServerErrorCode NO_ERROR
        = new PrintServerErrorCode();


    /** When submitting a print job, the job object was null. */
    public static final PrintServerErrorCode PRINT_JOB_NULL =
        new PrintServerErrorCode(8001);

    /** When submitting a print job, a matching printer couldn't be found. */
    public static final PrintServerErrorCode PRINTER_NOT_FOUND =
        new PrintServerErrorCode(8002);

    /** Loftware Print Server error messages. */
    public static final PrintServerErrorCode LPS_RUNTIME_EXCEPTION =
        new PrintServerErrorCode(8003);
    public static final PrintServerErrorCode LPS_CONNECTOR_EXCEPTION_SEVERE =
        new PrintServerErrorCode(8004);
    public static final PrintServerErrorCode LPS_CONNECTOR_EXCEPTION_PRINTER =
        new PrintServerErrorCode(8005);

    /**
     * A callback object was specified in the PrintJob constructor, but the
     * callback method name is null.
     */
    public static final PrintServerErrorCode NULL_CALLBACK_METHOD_NAME =
        new PrintServerErrorCode(8006);

    /**
     * The callback method name specified in the PrintJob constructor could
     * not be found in the callback object.
     */
    public static final PrintServerErrorCode INVALID_CALLBACK_METHOD =
        new PrintServerErrorCode(8007);

    /** When invoking the callback method, we got an exception. */
    public static final PrintServerErrorCode CALLBACK_EXCEPTION =
        new PrintServerErrorCode(8008);

    /** Unable to access the callback method when trying to invoke. */
    public static final PrintServerErrorCode CALLBACK_ACCESS_ERROR =
        new PrintServerErrorCode(8009);

    /** IO error parsing template body with Freemarker. */
    public static final PrintServerErrorCode TEMPLATE_IO =
        new PrintServerErrorCode(8010);

    /** Template field not found in Freemarker data source.  Template
     * file may not be found. */
    public static final PrintServerErrorCode BAD_TEMPLATE_FIELD =
        new PrintServerErrorCode(8011);

    /** General Freemarker template error. */
    public static final PrintServerErrorCode PROCESS_TEMPLATE =
        new PrintServerErrorCode(8012);

    /** The server configuration file contains an invalid IP address.
     * An IP address must be specified as a list of 4 numbers.  Each
     * number must be between 0 and 255.
     */
    public static final PrintServerErrorCode INVALID_IP_ADDRESS =
        new PrintServerErrorCode(8013);

    /** The XML server configuration file does not match the configuration
     * of the print server itself.  This may be because Loftware labels and
     * field names don't match the XML configuration.
     */
    public static final PrintServerErrorCode XML_SERVER_MISMATCH =
        new PrintServerErrorCode(8014);

    /** The XML server configuration indicates a different # of
     * label fields than the print server.
     */
    public static final PrintServerErrorCode XML_SERVER_NUM_FIELDS =
        new PrintServerErrorCode(8015);

    /** The XML server configuration has a different field than
     * the print server.
     */
    public static final PrintServerErrorCode XML_SERVER_FIELD_MISMATCH =
        new PrintServerErrorCode(8016);

    /** One of the data fields exceeds the maximum field length. */
    public static final PrintServerErrorCode DATA_FIELD_LENGTH =
        new PrintServerErrorCode(8017);

    /** Unable to find the label template from the label name and
     * printer name.
     */
    public static final PrintServerErrorCode LABEL_NOT_FOUND =
        new PrintServerErrorCode(8018);

    /** The label's Freemarker template file could not be found. */
    public static final PrintServerErrorCode TEMPLATE_FILE_NOT_FOUND =
        new PrintServerErrorCode(8019);

    /** Network Print Server error messages. */
    public static final PrintServerErrorCode NETWORK_UNKNOWN_HOST =
        new PrintServerErrorCode(8020);
    public static final PrintServerErrorCode NETWORK_IO =
        new PrintServerErrorCode(8021);

    /** Server Resources error message. */
    public static final PrintServerErrorCode SERVER_PROPERTIES_NOTFOUND =
        new PrintServerErrorCode(8030);
    public static final PrintServerErrorCode SERVER_PROPERTIES_NULL_SERVERTYPE =
        new PrintServerErrorCode(8031);
    public static final PrintServerErrorCode SERVER_PROPERTIES_INVALID_SERVERTYPE =
        new PrintServerErrorCode(8032);
    public static final PrintServerErrorCode SERVER_PROPERTIES_NULL_IP =
        new PrintServerErrorCode(8033);
    public static final PrintServerErrorCode SERVER_PROPERTIES_NULL_FILEDROP =
        new PrintServerErrorCode(8034);

    // Vocollect print server errors
    /** The template header file specified is invalid. */
    public static final PrintServerErrorCode BAD_TEMPLATE_HEADER_FILE =
        new PrintServerErrorCode(8035);
    public static final PrintServerErrorCode BAD_TEMPLATE_FORMAT_FILE =
        new PrintServerErrorCode(8036);
    public static final PrintServerErrorCode BAD_TEMPLATE_BODY_FILE =
        new PrintServerErrorCode(8037);
    public static final PrintServerErrorCode BAD_TEMPLATE_SEPARATOR_FILE =
        new PrintServerErrorCode(8038);
    public static final PrintServerErrorCode BAD_TEMPLATE_FOOTER_FILE =
        new PrintServerErrorCode(8039);

    /** Unable to retrieve Printer List */
    public static final PrintServerErrorCode PRINTER_LIST_UNAVAILABLE =
        new PrintServerErrorCode(8040);

    /**
     * Constructor.
     */
    private PrintServerErrorCode() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected PrintServerErrorCode(long err) {
        // The error is defined in the TaskErrorCode context.
        super(PrintServerErrorCode.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 