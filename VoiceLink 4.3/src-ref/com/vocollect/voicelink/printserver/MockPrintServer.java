/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;
import com.vocollect.epp.exceptions.VocollectException;

/**
 * This is a dummy instance of AbstractPrintServer, to be used for
 * testing.  It will handle all the functions without errors, but
 * won't actually do any work.
 *
 * @author cblake
 */
public class MockPrintServer extends AbstractPrintServer {
    private long nextJobId = 1;

    /**
     *
     * Constructor.
     * @param xmlConfigFile The XML configuration file to use.
     */
    protected MockPrintServer(String xmlConfigFile) {
        super(AbstractPrintServer.PrintServerType.MOCK, xmlConfigFile);
        initialize();
    }

    /**
     * Convenience constructor that loads the default XML file.
     */
    public MockPrintServer() {
        super(AbstractPrintServer.PrintServerType.MOCK);
        initialize();
    }

    /** Constructor subroutine. */
    private void initialize() {
        // Add a dummy printer
        final String printerName = "FirstPrinter";
        final String hostName = "192.168.1.29";
        final int portNumber = 11223;
        ServerPrinter printer = null;
        try {
            printer = new ServerPrinter(printerName, hostName, portNumber);
        } catch (VocollectException ex) {
            // This should only happen with an invalid IP.. no problem.
        }
        this.getPrintersSafe().put(printerName, printer);
        final String labelName = "FirstLabel";
        final LabelTemplate label = new LabelTemplate(labelName, null);
        this.getLabelsSafe().add(label);
    }

    /**
     * No updating needed.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updatePrinters()
     */
    @Override
    protected void updatePrinters() {
        return;
    }

    /**
     * No updating needed.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateLabels()
     */
    @Override
    protected void updateLabels() {
        return;
    }

    /**
     * No updating needed.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateJobQueue()
     */
    @Override
    protected void updateJobQueue() {
        return;
    }

    /**
     * In this simple mock class, we just throw the new job directly into
     * history and act like it printed successfully.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#submitJob(com.vocollect.voicelink.printserver.PrintJob)
     */
    @Override
    protected long processJob(PrintJob newJob) throws VocollectException {
        newJob.setJobId(nextJobId);
        newJob.setJobStatus(PrintJob.JobStatus.PRINT_SUCCESSFUL);
        if (nextJobId == Long.MAX_VALUE) {
            // wrap around to 1 again.
            nextJobId = 1;
        } else {
            // Iterate to next job ID
            nextJobId++;
        }

        // Send straight to job history
        jobHistoryAdd(newJob);
        return newJob.getJobId();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#shutdown()
     */
    @Override
    public void shutdown() {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 