/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateScalarModel;


import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


/**
 * A general utilities class for the print server classes, with all
 * static methods.
 *
 * @author cblake
 */
public final class Utilities {
    /**
     * Private constructor; should never be instantiated.
     */
    private Utilities() {

    }

    /**
     * Process the given template text using Freemarker; all the fields
     * using the syntax ${Object.attribute} will be replaced by using the
     * data objects in the label.
     * @param reader  A reader of a Freemarker template.  Could be a string
     *      or a file stream.
     * @param label A label that contains a field data map to be used in
     *      processing of the templateText parameter
     * @return  The processed body result, with data in place of variables
     * @throws VocollectException When Freemarker throws an exception
     */
    private static String processTemplate(Reader reader, LabelData label)
    throws VocollectException {
        // Create Freemarker configuration
        Configuration config = new Configuration();
        config.setNumberFormat("0.######");
        config.setDateTimeFormat("dd/MM/yyyy");

        // Tell Freemarker to ignore errors - particularly we have a problem
        // printing assignment.operator.operatorid for assignmments that
        // have not been issued -- so we tell Freemarker ignore errors.
        // Freemarker will still log the error - so a developer working on
        // labels can figure out why data isn't printing by looking at the
        // log file.
        config.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);


        DefaultObjectWrapper wrapper = new DefaultObjectWrapper();
        // We need NULL values to be interpreted as empty strings; otherwise
        // Freemarker will throw an exception when handling NULLs.
        wrapper.setNullModel(TemplateScalarModel.EMPTY_STRING);
        config.setObjectWrapper(wrapper);

        java.io.Writer resultWriter = null;
        PrintServerErrorCode error = null;

        try {
            // Create the template object from our string reader
            freemarker.template.Template template = new Template(
                "VocollectRunTimeTemplate", reader, config);
            // Send output to a string buffer in a string writer
            resultWriter = new java.io.StringWriter();
            template.process(label.getLabelDataMap(), resultWriter);
        } catch (java.io.IOException ex) {
            error = PrintServerErrorCode.TEMPLATE_IO;
            throw new VocollectException(error, ex);
        } catch (freemarker.core.InvalidReferenceException ex) {
            error = PrintServerErrorCode.BAD_TEMPLATE_FIELD;
            throw new VocollectException(error, ex);
        } catch (freemarker.template.TemplateException ex) {
            error = PrintServerErrorCode.PROCESS_TEMPLATE;
            throw new VocollectException(error, ex);
        }

        return resultWriter.toString();
    }

    /**
     * Convenience method when processing a String instead of a file-based
     * Freemarker template.
     * @param templateText  The text to be treated as a Freemarker template
     * @param label The label data to be printed
     * @return  The resulting processed string, with data values
     * @throws VocollectException   On error
     */
    protected static String processTemplate(String templateText,
                                            LabelData label)
    throws VocollectException {
        // Put the template text into a reader for processing
        java.io.Reader reader = new java.io.StringReader(templateText);
        return processTemplate(reader, label);
    }

    /**
     * Open a file and read the contents into a string.  This will treat
     * the file as having UTF-8 encoding, which is backwards compatible with
     * standard ASCII.
     *
     * @param fileName  Name of file to read
     * @param caller - caller object of that has spring context
     * @return  The file body string
     * @throws VocollectException   On IO error
     */
    public static String readFile(final String fileName, Object caller)
    throws VocollectException {
        InputStream stream = caller.getClass().getClassLoader()
                    .getResourceAsStream(fileName);
        String data = "";
        final String charset = "UTF-8";
        try {
            final int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            while ((bytesRead = stream.read(buffer)) > 0) {
                data += new String(buffer, 0, bytesRead, charset);
            }
        } catch (IOException ex) {
            throw new VocollectException(PrintServerErrorCode.TEMPLATE_IO, ex);
        }

        return data;
    }


    /**
     * Opens a socket on the given port on the network over TCP/IP.
     * With NiceWare, this is used to fire a TCP/IP communication-based
     * trigger which initiates a print job.  This can also be used to
     * send data directly to a printer.
     *
     * This must be synchronized to prevent potential collisions when sending
     * data to the same place on the network.
     *
     * Note:  we cannot add job directly to history here.  The vocollect print
     * server will have to first remove it from the job queue.
     *
     * @param headerString  The list of label field names/variables
     * @param dataString    The mapped data values for the label fields/variables
     * @param serverHostName    The host name of the server.  May be an IP
     *      address.
     * @param port     The port on the print server
     * @param newJob        The print job submitted to the server
     * @throws VocollectException On sendPrintJob error.
     */
    public static synchronized void sendPrintJob(String headerString,
                                                 String dataString,
                                                 String serverHostName,
                                                 int port,
                                                 PrintJob newJob)
    throws VocollectException {

        PrintServerErrorCode error = null;
        Socket outSocket = null;
        PrintStream out = null;

        try {
            outSocket = new Socket(serverHostName, port);
            out = new PrintStream(outSocket.getOutputStream(), false, "UTF-8");

            if (headerString.length() > 0) {
                out.println(headerString);
            }
            out.println(dataString);
            out.close();
            outSocket.close();

        } catch (UnknownHostException e) {
            //  Set job status
            error = PrintServerErrorCode.NETWORK_UNKNOWN_HOST;
            newJob.setErrorCode(error);
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            throw new VocollectException(error, e);
        } catch (java.net.ConnectException e) {
            //  Set job status
            error = PrintServerErrorCode.NETWORK_IO;
            newJob.setErrorCode(error);
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            throw new VocollectException(error, e);
        } catch (IOException e) {
            //  Set job status
            error = PrintServerErrorCode.NETWORK_IO;
            newJob.setErrorCode(error);
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            throw new VocollectException(error, e);
        }

    }

    /**
     * With Bartender, this is used to write a file to a shared location
     * where a Commander task monitoring that file share initiates a print job.
     *
     * SubmitJob will automatically add failed jobs to the history; don't do
     * it in here.
     *
     * @param fileName is the complete path and name of the file to be dropped
     * @param fileData is the data string to be written to the file
     * @param newJob is the print job submitted to the server
     * @param server is a print server reference for adding job to the
     *      history when an exception is thrown.
     * @throws VocollectException On sendPrintJob error.
     */
    public static void sendPrintJob(String fileName, String fileData,
                                    PrintJob newJob, AbstractPrintServer server)
    throws VocollectException {

        PrintWriter out = null;


        try {

            out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(fileName), "UTF8")));
            out.write(fileData);

            if (out.checkError()) {
                // Set job status and put into job history
                PrintServerErrorCode error = PrintServerErrorCode.NETWORK_IO;
                newJob.setErrorCode(error);
                newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                throw new VocollectException(error);
            }

        } catch (IOException e) {
            //  Set job status and put into job history
            PrintServerErrorCode error = PrintServerErrorCode.NETWORK_IO;
            newJob.setErrorCode(error);
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            throw new VocollectException(error, e);
        } finally {
            if (out != null) {
                out.close();
            }

        }

    }

    /**
     * This method will validate the submitted list of labels and their fields
     * against the XML Configuration file.
     * @param labelDataList List of LabelData objects.
     * @param impServer Instance of the implemented print server
     * @throws VocollectException   If label cannot be found in the XML
     *          Configuration file an exception will be thrown
     */
    public static void validateLabelList(List<LabelData> labelDataList,
                                         AbstractPrintServer impServer)
    throws VocollectException {

        LabelTemplate labelConfig = null;
        PrintServerErrorCode error = null;

        // Loop for each label
        Iterator<LabelData> labels = labelDataList.iterator();
        while (labels.hasNext()) {

            LabelData voLabelData = labels.next();

            String labelName = voLabelData.getLabelName();

            // Get the corresponding LabelTemplate from our member variable
            labelConfig = impServer.getLabel(labelName);
            if (labelConfig == null) {
                error = PrintServerErrorCode
                    .XML_SERVER_MISMATCH;
                throw new VocollectException(error);
            }
        }

    }

    /**
     * Takes in a text string and using regex classes does a search and replace
     * using the passed in parameters.
     *
     * @param formatText - unparsed label body string
     * @param searchText - text to search
     * @param replacementText - text to replace found text
     * @return  The parsed body string
     * @throws VocollectException   On syntax error and argument error
     */
    public static String replaceText(String formatText, String searchText,
                                     String replacementText)
    throws VocollectException {

        try {
            Pattern p = Pattern.compile(searchText);
            Matcher m = p.matcher(formatText);
            boolean b = m.find();
            if (b) {
                formatText = m.replaceAll(replacementText);
            }

        } catch (PatternSyntaxException patEx) {
            throw new VocollectException(PrintServerErrorCode.PROCESS_TEMPLATE, patEx);
        } catch (IllegalArgumentException argEx) {
            throw new VocollectException(PrintServerErrorCode.PROCESS_TEMPLATE, argEx);
        }

        return formatText;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 