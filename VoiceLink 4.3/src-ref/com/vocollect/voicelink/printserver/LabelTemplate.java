/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;

import java.util.List;

/**
 * This represents a label template available to the print server.
 *
 * @author cblake
 */
public class LabelTemplate {
    /**
     * The name of this label.  Should be unique for the print server, except
     * for the Vocollect Print Server, in which case it should be unique for
     * its printer.
     */
    private String labelName;

    /** The name of the printer that this label belongs to.  This is only
     * applicable to Vocollect print server.
     */
    private List<String> labelPrinterNames;

    /** Contains variables to be replaced by data variables. */
    private String labelTemplateFormat = "";

    /**
     * If applicable, the printer port mapped to this label.
     */
    private int printPort = -1;

    /**
     * A collection of label field names that are used by this template.
     */
    private List<String> labelFieldList;


    /**
     * Default constructor for use with XML loading.
     */
    public LabelTemplate() {
        // Construct an empty list object
        this.labelFieldList = new java.util.ArrayList<String>();
        this.labelPrinterNames = new java.util.ArrayList<String>();
    }

    /**
     * Constructor.
     * @param labelName     Name of the type of label
     * @param labelFieldList    List of fields in the label
     */
    public LabelTemplate(final String labelName,
                         final List<String> labelFieldList) {
        this.labelName = labelName;
        if (labelFieldList == null) {
            // No field list specified; create an empty list
            this.labelFieldList = new java.util.ArrayList<String>();
        } else {
            this.labelFieldList = labelFieldList;
        }
    }

    /**
     * Constructor.
     * @param labelName     Name of the type of label
     * @param labelFieldList    List of fields in the label
     * @param labelPort         Port associated with the label
     */
    public LabelTemplate(final String labelName,
                         final List<String> labelFieldList,
                         final int labelPort) {
        this.labelName = labelName;
        this.labelFieldList = labelFieldList;
    }

    /**
     * @return  Label's name identifier
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     * Setter method to be used when loading XML configuration.
     * @param labelName The new label type name.
     */
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    /**
     * Required for Vocollect print servers, because the same label on a
     * different printer will result in a different label template.
     * @return  The name of the printer affiliated with this label.
     */
    public List<String> getLabelPrinterNames() {
        return labelPrinterNames;
    }

    /**
     * Only called by XML Spring bean builder.
     * @param labelPrinterNames  New label printer name
     */
    public void setLabelPrinterNames(List<String> labelPrinterNames) {
        this.labelPrinterNames = labelPrinterNames;
    }

    /**
     * Get the format string to appear at the top of the label, once
     * per print job.
     * @return  The label string
     */
    public String getLabelTemplateFormat() {
        return labelTemplateFormat;
    }

    /**
     * Update the label format template by reading in the given file.
     * @param labelTemplateFormatFile   The file to read in
     * @throws VocollectException   If the file can't be found
     */
    public void setLabelTemplateFormat(String labelTemplateFormatFile)
    throws VocollectException {
        try {
            this.labelTemplateFormat = Utilities.readFile(
                labelTemplateFormatFile, this);
        } catch (VocollectException ex) {
            throw new VocollectException(PrintServerErrorCode
                .BAD_TEMPLATE_FORMAT_FILE, ex);
        }
    }

    /**
     * Only applicable to NiceWatch label printing over TCP/IP.  This is
     * needed to map to a configured NiceWatch TCP/IP port trigger; each label
     * can be triggered by a different port.
     * @return  The port associated with printing this label.
     */
    public int getPrintPort() {
        return printPort;
    }

    /**
     * Setter method.
     * @param printPort New print label port
     */
    public void setPrintPort(int printPort) {
        this.printPort = printPort;
    }

    /**
     * @return  Label's field mapping
     */
    public List<String> getLabelFieldList() {
        return labelFieldList;
    }

    /**
     * Setter method for XML loading.
     * @param labelFieldList    New label field list.  If this is null,
     *      we'll just clear the list; the member variable should never
     *      be null.
     */
    public void setLabelFieldList(List<String> labelFieldList) {
        if (labelFieldList == null) {
            this.labelFieldList.clear();
            return;
        }

        this.labelFieldList = labelFieldList;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 