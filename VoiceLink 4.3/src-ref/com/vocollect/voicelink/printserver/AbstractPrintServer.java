/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;
import com.vocollect.epp.exceptions.VocollectException;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * This is the print server object to be used by the VoiceLink application.
 * It is implemented by one of the four print servers.
 *
 * @author cblake
 */
public abstract class AbstractPrintServer {
    /**
     * An enumeration of the different print server modes.  Corresponds
     * to the subclass instance.  Must be specified during construction.
     * the MOCK server type should only be used for unit tests.
     *
     * @author cblake
     */
    public enum PrintServerType {
        VOCOLLECT,
        LOFTWARE,
        BARTENDER,
        NICEWATCH,
        MOCK    // Mock server used for testing
    }

    protected static final int MAX_HISTORY_LENGTH = 100;
    public static final String SERVER_CONFIG_FILE = "PrintServer.xml";
    public static final String SERVER_CONFIG_XML_ID    = "serverConfig";

    private final PrintServerType serverType;
    private final String xmlConfigFile;
    private String serverHostName = null;
    private String fileDropLocation = "";

    /** Map of printer names (String) to ServerPrinter objects. */
    private Map<String, ServerPrinter> printerMap;
    private List<LabelTemplate> labelList;
    private Queue<PrintJob> jobQueue;

    /** Limited to a certain size for memory reasons. */
    private Queue<PrintJob> jobHistory;

    /**
     * This method ensures to free up resources when we need to shutdown the
     * print servers
     */
    public abstract void shutdown();

    /**
     * Can only be instantiated by subclass which specifies a
     * print server type.
     * Constructor.
     * @param serverType    The type of print server being constructed.
     * @param xmlConfigFile The path and file name to the XML configuration
     *      file used to indicate the printers and labels for the print server.
     */
    protected AbstractPrintServer(PrintServerType serverType,
                                  String xmlConfigFile) {
        this.serverType = serverType;

        printerMap = new java.util.HashMap <String, ServerPrinter>();
        labelList = new java.util.ArrayList<LabelTemplate>();
        this.xmlConfigFile = xmlConfigFile;
        // The queue's must use a concurrent queue object to ensure
        // that multi-threaded queue access is synchronized.  Their iterators
        // will never throw a concurrent modification exception.
        jobQueue = new java.util.concurrent.ConcurrentLinkedQueue<PrintJob>();
        jobHistory = new java.util.concurrent.ConcurrentLinkedQueue<PrintJob>();
    }

    /**
     * Convenience constructor; uses the default XML file name.
     * @param serverType    The type of server being created.
     */
    protected AbstractPrintServer(PrintServerType serverType) {
        this(serverType, SERVER_CONFIG_FILE);
    }

    /**
    *
    * @return the current print server type
    */
   public PrintServerType getServerType() {
       return serverType;
   }

   /**
    * Get the print server IP address.  This should be null, except for
    * the NiceWatch print server, which uses TCP/IP file drops.
    * @return   The print server IP address.  An array of 4 values,
    *       from 0 to 255.
    */
   public String getServerHostName() {
       return this.serverHostName;
   }

   /**
    * Validate and set the server IP.  Should be called by child classes.
    * @param serverHostName New server IP address.  Must be an array of
    *       four values from 0 to 255 (unsigned bytes).
    * @throws VocollectException If the IP is invalid.
    */
   protected void setServerHostName(String serverHostName)
   throws VocollectException {
       this.serverHostName = serverHostName;
   }

   /**
    *
    * @return Location for file drop printing.  Only used by SeaGull.  This
    *   must correspond to a mapped drive that is monitored by the print
    *   server.
    */
   public String getFileDropLocation() {
       return this.fileDropLocation;
   }

   /**
    * Should only be called by a subclass during initialization.
    * @param fileDropLocation New file drop location
    */
   protected void setFileDropLocation(String fileDropLocation) {
       this.fileDropLocation = fileDropLocation;
   }

    /**
     * Return the list of printer objects that are currently defined
     * for this print server.  It may cause the configuration file to
     * be reloaded every time.  Subclasses may override this method to
     * update the member variable each time.
     * @param updatePrinters    Whether we want to update the list of printers
     *      through the print server implementation class.  This will cause
     *      the print server to re-query over the network or re-read the
     *      configuration files.
     * @return  List of printer objects.  The key is the printer name.
     * @throws VocollectException if the update function has problems
     */
    public Map<String, ServerPrinter> getPrinters(boolean updatePrinters)
    throws VocollectException {
        if (updatePrinters) {
            updatePrinters();
        }

        return printerMap;
    }

    /**
     * Convenience method.
     * @return  List of printer objects.  The key is the printer name.  This
     *      will always cause the subclass to update the list of printers.
     * @throws VocollectException on error updating printer list
     */
    public Map<String, ServerPrinter> getPrinters() throws VocollectException {
        return getPrinters(true);
    }

    /**
     * Subclasses may want to access the printer map without dealing
     * with exceptions by avoiding the update.
     * @return  List of printers currently in our collection.
     */
    protected Map<String, ServerPrinter> getPrintersSafe() {
        return printerMap;
    }

    /**
     * Must be implemented by the subclass.  Will update our list of printers
     * from the print server.  If this query is not possible, it should just
     * return immediately.
     * @throws VocollectException On updatePrinters error.  This may occur if
     *      there are network problems when trying to print.
     */
    protected abstract void updatePrinters() throws VocollectException;

    /**
     * May be overridden and called from subclass.  Note that these label
     * objects will only have the label type name specified, because that
     * is all we need to pass on to the VoiceLink UI.
     * @param updateLabels Whether or not to update the list of labels from
     *      the print server.
     * @return  List of labels currently available from this print server
     * @throws VocollectException On getLabels error.
     */
    public List<LabelTemplate> getLabels(boolean updateLabels)
    throws VocollectException {
        if (updateLabels) {
            updateLabels();
        }

        return labelList;
    }

    /**
     * Return the list of labels without querying the print server.  This
     * precludes the need for exception handling.
     * @return  List of current labels
     */
    protected List<LabelTemplate> getLabelsSafe() {
        return labelList;
    }

    /**
     * Convenience method.
     * @return  List of label objects.  The key is the label's name.
     * @throws VocollectException   If there's a problem updating the labels.
     */
    public List<LabelTemplate> getLabels() throws VocollectException {
        return getLabels(true);
    }

    /**
     * Search through our collection of labels by label name.
     * @param labelName Label type name that we're looking for.
     * @return  The matching label template object, or null if not found.
     */
    public LabelTemplate getLabel(String labelName) {
        List<LabelTemplate> labels = getLabelsSafe();
        LabelTemplate label = null;
        Iterator<LabelTemplate> iter = labels.iterator();
        while (iter.hasNext()) {
            label = iter.next();
            if (label.getLabelName().compareToIgnoreCase(labelName)
                == 0) {
                // Found our label
                return label;
            }
        }

        return null;    // Couldn't find label
    }

    /**
     * Update the collection of labels available from this print server.
     * Must be implemented by subclass.  Should simply return immediately
     * if this information cannot be updated automatically.
     * @throws VocollectException On updateLabels error.
     */
    protected abstract void updateLabels() throws VocollectException;

    /**
     * Note that for some print servers, this queue may always be empty.
     * Third party print servers will probably receive the job submission
     * directly by the submit method.
     * @return  Current queue of print jobs that haven't yet been
     * submitted to the print server.
     * @throws VocollectException On getJobQueue error.
     */
    public Queue<PrintJob> getJobQueue() throws VocollectException {
        updateJobQueue();
        return jobQueue;
    }

    /**
     * Return a straight reference to the job queue without calling an
     * update.  This avoids throwing a potential exception.
     * @return  Current job queue member variable
     */
    protected Queue<PrintJob> getJobQueueSafe() {
        return jobQueue;
    }

    /**
     * The print server implementation class must update the job queue.
     * Some print servers may be incapable of performing an update, in
     * which case they should just return immediately.
     *
     * This method should query the status of the head of the queue and check
     * the job status.  If the job has completed (error or no), it should be
     * popped from the queue and pushed to the history, then the head should
     * be checked again.
     * @throws VocollectException On updateJobQueue error.
     */
    protected abstract void updateJobQueue() throws VocollectException;

    /**
     * Add the given job to the job history collection.  If the collection
     * would exceed the maximum size, purge the oldest job from the history.
     * This list should be automatically updated by updateJobQueue().
     * @param job   Job to add to the history collection.
     */
    public void jobHistoryAdd(PrintJob job) {
        while (jobHistory.size() >= MAX_HISTORY_LENGTH) {
            jobHistory.remove();
        }
        jobHistory.add(job);
    }

    /**
     * Query the specified job's status.  Not all print servers support
     * all values for the job status.  For print servers performing a
     * file drop, this will return success right away, as long as the
     * data was sent across the network.
     * @param jobId The jobID of the job to query
     * @return  The job status
     * @throws VocollectException On jobStatus error.
     */
    public final PrintJob.JobStatus jobStatus(long jobId)
    throws VocollectException {
        // Update the job queue, then query for the job status.
        updateJobQueue();

        // Lookup job in queue
        Iterator<PrintJob> jobs = jobQueue.iterator();
        PrintJob job = null;
        while (jobs.hasNext()) {
            job = jobs.next();
            if (job.getJobId() == jobId) {
                // Found a match in the queue
                return job.getJobStatus();
            }
        }

        // Lookup in job history
        jobs = jobHistory.iterator();
        while (jobs.hasNext()) {
            job = jobs.next();
            if (job.getJobId() == jobId) {
                // Found a match in history (already printed).
                // We don't know if there were problems
                return job.getJobStatus();
            }
        }

        // If we get down here, then the job is neither in the queue
        // nor in the history.  It may be an old job.
        return PrintJob.JobStatus.UNKNOWN_JOB;
    }

    /**
     * This handles common error checking and processing that is used
     * regardless of the print server implementation.  We must make sure
     * that the print job's printer and labels can be found.
     *
     * This method will automatically update the jobID and job status,
     * but the subclass processJob method must add this to the queue
     * or history.
     * @param newJob    New print job
     * @return  New jobId from the processNewJob method
     * @throws VocollectException   On submission error.
     */
    public long submitJob(final PrintJob newJob) throws VocollectException {
        PrintServerErrorCode error = null;

        // Make sure job isn't null
        if (newJob == null) {
            error = PrintServerErrorCode.PRINT_JOB_NULL;
            throw new VocollectException(error);
        }

        // Find printer; note that this will automatically update the
        // list of printers.
        ServerPrinter jobPrinter = getPrinters().get(newJob.getPrinterName());
        if (jobPrinter == null) {
            error = PrintServerErrorCode.PRINTER_NOT_FOUND;
            newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
            throw new VocollectException(error);
        } else {
            // Found the printer; update the print job object
            newJob.setJobPrinter(jobPrinter);
        }

        // We passed validation if we got down here; set status to IN_QUEUE
        newJob.setJobStatus(PrintJob.JobStatus.IN_SERVER_QUEUE);

        // Update the list of labels for use in job processing
        updateLabels();

        // Get jobID from the protected worker function, then return it.
        try {
            newJob.setJobId(processJob(newJob));
        } catch (VocollectException ex) {
            // Problem in processing; send straight to the history.
            // The class that threw the exception should have set the error
            // code and job status.
            jobHistoryAdd(newJob);
            throw ex;
        }
        return newJob.getJobId();
    }

    /**
     * This is the most important overridable method.  For the third
     * party print servers, it will submit the new job to the print
     * server's queue.  If there is an error before we return, an exception
     * will be thrown, and the job status will be set to PRINT_FAILED.
     * @param newJob    The print job to submit to the print server.
     * @return  New job ID for tracking the print job.
     * @throws VocollectException If there is a problem submitting the job.
     */
    protected abstract long processJob(PrintJob newJob)
        throws VocollectException;


    /**
     * Read in our XML configuration file and load up a list of printers
     * and labels.  Every print server currently requires this XML file
     * in order to fully initialize the list of printers and labels.
     * However, for flexibility reasons, we will not call this unless
     * requested by a subclass.
     *
     * This is typically needed to define a list of labels and label fields
     * at a minimum.
     */
    protected void loadXmlConfig() {
        /* Load the server configuration XML file.  The amount of detail
         * in this file varies depending on the print server.  The Printers
         * collection will only be reloaded if the configuration specified
         * some values.
         */

        // estoll -- changed B3 Code from a FileStream to an ClassPathResource
        ClassPathResource resource = new ClassPathResource(this.xmlConfigFile,
            this.getClass().getClassLoader());
        // END of estoll Change

        BeanFactory factory = new XmlBeanFactory(resource);

        // Extract the top-level server config object from XML
        ServerConfig config = (ServerConfig) factory.getBean(
            SERVER_CONFIG_XML_ID);

        if (config.getLabelList() != null) {
            // A label list was specified
            labelList = config.getLabelList();
        }
        if (config.getPrinterMap() != null) {
            // A printer map was specified
            printerMap = config.getPrinterMap();
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 