/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;
import java.util.List;
import java.util.Map;


/**
 * This is a JavaBean class that will be loaded from XML files.  It will
 * contain a collection of printers and label templates.  The amount of
 * detail in this data depends on what is required for the specific
 * print server.
 *
 * This container class will simply be used to extract the printers and
 * labels to populate the print server object itself.  Once this has been
 * done, it will be discarded.
 *
 * @author cblake
 */
public class ServerConfig {
    private Map<String, ServerPrinter> printerMap;
    private List<LabelTemplate> labelList;

    /**
     * Constructor.
     */
    public ServerConfig() {

    }

    /**
     * @return  Map of printer names to printer objects
     */
    public Map<String, ServerPrinter> getPrinterMap() {
        return printerMap;
    }

    /**
     * @param printerMap New map of printer names to printer objects
     */
    public void setPrinterMap(Map<String, ServerPrinter> printerMap) {
        this.printerMap = printerMap;
    }

    /**
     * @return  List of label definitions, with field lists
     */
    public List<LabelTemplate> getLabelList() {
        return labelList;
    }

    /**
     * @param labelList New list of label definitions
     */
    public void setLabelList(List<LabelTemplate> labelList) {
        this.labelList = labelList;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 