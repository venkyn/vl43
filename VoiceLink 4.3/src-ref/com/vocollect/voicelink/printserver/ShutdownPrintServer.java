/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

/**
 * This class listenes to the context close event and shuts down the the printer
 * server.
 * @author khazra
 *
 */
public class ShutdownPrintServer implements ApplicationListener {

    /**
     * Logging object.
     */
    private static final Logger log = new Logger(ShutdownPrintServer.class);

    /**
     *   @param event - event to check for.
     *
     */

    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof ContextClosedEvent) {
            try {
                AbstractPrintServer printServer = PrintServerFactory
                    .getPrintServer();

                printServer.shutdown();

            } catch (VocollectException e) {
                log.info(e.getErrorCode());
            }
        }

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 