/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 *
 *
 * @author cblake
 */
public class VocollectPrintServer extends AbstractPrintServer {
    private int nextJobId = 1;
    private ExecutorService printExecutor = null;
    /** # of milliseconds to wait at the end of each print job execution.
     * This prevents us from overflowing the printer memory buffer.  In
     * testing on the Zebra 105SL, a delay of 500ms caused repeated IO
     * errors, whereas 1000ms works without a problem.
     */
    public static final long PRINT_DELAY = 1000;

    private String exportFile;
    private String labelHeader;
    private String exportBody;
    private String labelBody;

    private static final String START_MARKER = "\\x1B\\x1B\\x23003999";
    private static final String END_MARKER = "\\x1B\\x1B\\x23003998";
    private static final String QUANTITY_MARKER = "\\x1B\\x1B\\x23009997LEN000";
    private static final String FREEMARKER_QUANTITY = "\\${copies}";

    private static final String VOCOLLECT_START_MARKER = "<DATA_BODY>";
    private static final String VOCOLLECT_END_MARKER = "</DATA_BODY>";
    private static final String VOCOLLECT_QUANTITY_MARKER = "<COPIES/>";


    /**
     * Constructor.
     * @param serverType
     * @param xmlConfigFile The path and file name of the XML configuration
     *      file to use.  This constructor should only be used for testing;
     *      it is assumed we normally use the default config file.
     */
    VocollectPrintServer(String xmlConfigFile) {
        super(PrintServerType.VOCOLLECT, xmlConfigFile);
        initialize();
    }

    /**
     * This is the public constructor, which will automatically load the
     * default XML file.
     * Constructor.
     */
    public VocollectPrintServer() {
        super(PrintServerType.VOCOLLECT);
        initialize();
    }

    /**
     * Common subroutine for all constructors.
     */
    private void initialize() {
        // Create the executor for an independent thread; this thread
        // will process all the jobs sequentially.
        printExecutor = Executors.newSingleThreadExecutor();

        // TODO:  validate the printer & label objects as appropriate for
        // this print server.  All printers need host names and ports,
        // and all labels need printer names and template files.
    }

    /**
     * Lookup a label template for the given printer name and label name.
     * In this Vocollect print server, label names do not have to be unique
     * except for their printer.  This is because every printer needs its
     * own set of templates.  Note that this does not refresh the list of
     * labels before returning a result, in order to avoid throwing an
     * exception.
     * @param printerName   The printer name for the print job
     * @param labelName The name of the label to find.
     * @return The label with the matching name and matching printer name.
     *      If not found, null is returned.
     */
    protected LabelTemplate getLabel(String printerName, String labelName) {
        List<LabelTemplate> labels = this.getLabelsSafe();
        LabelTemplate label = null;
        Iterator<LabelTemplate> iter = labels.iterator();
        List<String> printers = null;
        while (iter.hasNext()) {
            label = iter.next();
            if (label.getLabelName().compareToIgnoreCase(labelName) == 0) {
                // We found a matching label name; loop through the label
                // printers
                printers = label.getLabelPrinterNames();
                for (int i = 0; i < printers.size(); i++) {
                    if (printers.get(i).compareToIgnoreCase(printerName) == 0) {
                        return label;
                    }
                }
            }
        }

        return null;    // Label not found
    }

    /**
     * Update printer list by reloading the XML file.  This will also
     * update the list of labels.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#getPrinters()
     */
    protected void updatePrinters() {
        updateConfig();
    }

    /**
     * Update label list by reloading the XML config file.  This will
     * also update the list of printers.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateLabels()
     */
    protected void updateLabels() {
        updateConfig();
    }

    /**
     * Update the Vocollect print server configuration from the XML file.
     * This will reload the list of printers and labels.  This can be called
     * directly instead of calling both updateLabels() and updatePrinters(),
     * which would be redundant.
     */
    private void updateConfig() {
        loadXmlConfig();
    }

    /**
     * Our job queue will always be up to date, because when jobs finish
     * processing, they will update the queues automatically.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#updateJobQueue()
     */
    protected void updateJobQueue() {
        return;
    }

    /**
     * The Vocollect print server will manage the processing queue in a
     * separate thread, so print jobs will be submitted to the printer
     * asynchronously.  This method is synchronized to make sure that
     * jobs don't get out of order between retrieving the jobID and adding
     * to the job queues.
     *
     * Note that we do not need a definition of the field names in the XML
     * file for the Vocollect print server, since it will get field names
     * directly through the FTL template file.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#processJob(com.vocollect.voicelink.printserver.PrintJob)
     */
    protected synchronized long processJob(PrintJob newJob)
    throws VocollectException {
        // Update the status and JobID
        newJob.setJobStatus(PrintJob.JobStatus.IN_SERVER_QUEUE);
        newJob.setJobId(nextJobId);

        // Increment the nextJobId
        if (nextJobId >= java.lang.Integer.MAX_VALUE) {
            // Roll over to 1 again.
            nextJobId = 1;
        } else {
            nextJobId++;
        }

        // Add the job to our queue
        this.getJobQueue().add(newJob);

        // Add to our thread executor
        ProcessJob threadJob = new ProcessJob(newJob);
        printExecutor.execute(threadJob);

        return newJob.getJobId();
    }

    /**
     * This class will be instantiated for every print job.  It will send
     * the data over the network to the printer's IP address, update job
     * status, call the callback function, and exit.
     *
     * It is assumed that each job will be processed sequentially, so each
     * job will assume it is at the head of the queue when run() is called.
     * This will work even when using multiple threads, as long as all jobs
     * are processed in one thread.
     *
     * Once a job has completed, the following postconditions should be true:
     *      - the print job status has been set to a terminal status
     *          (failure or success)
     *      - the callback function will be called, if needed
     *          (this is done by the setJobStatus method)
     *      - the job will be moved from the job queue to the job history
     * @author cblake
     */
    private class ProcessJob implements Runnable {
        private final PrintJob thisJob;
        private final ServerPrinter printer;

        /**
         * Constructor.  Store the print job in a member variable, and
         * validate the print job by checking for the printer and label.
         * @param newJob    The job we are processing
         * @throws VocollectException if job cannot be submitted
         */
        public ProcessJob(PrintJob newJob) throws VocollectException {
            this.thisJob = newJob;

            // Update the list of printers & labeltemplates once, then
            // verify the data.
            updateConfig();

            PrintServerErrorCode error = null;
            VocollectException exception = null;
            printer = getPrintersSafe().get(newJob.getPrinterName());
            if (printer == null) {
                error = PrintServerErrorCode
                    .PRINTER_NOT_FOUND;
                newJob.setErrorCode(error);
                newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                throw exception;
            }

            Iterator<LabelData> iter = thisJob.getLabelList().iterator();
            LabelData record = null;
            LabelTemplate template = null;

            while (iter.hasNext()) {
                record = iter.next();
                template = getLabel(thisJob.getPrinterName(),
                    record.getLabelName());
                if (template == null) {
                    error = PrintServerErrorCode
                        .LABEL_NOT_FOUND;
                    newJob.setErrorCode(error);
                    newJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                    throw exception;
                }
            }
        }

        /**
         *
         * {@inheritDoc}
         * @see java.lang.Runnable#run()
         */
        public void run() {
            // If any errors were found during construction, then the
            // print job will already be marked as complete.
            if (thisJob.getJobStatus() == PrintJob.JobStatus.PRINT_FAILED) {
                moveJobToHistory();
                return;
            }

            // Loop through the labels to print
            Iterator<LabelData> labels = thisJob.getLabelList().iterator();
            LabelData record = null;

            String currentLabelName = null;
            String printCommands = "";
            // First, read in the header file

            while (labels.hasNext()) {
                record = labels.next();

                // If the labe name changes -- we need to get the template
                if (!record.getLabelName().equals(currentLabelName)) {

                    // Get the format from the first label template & print it once
                    LabelTemplate template = getLabel(thisJob.getPrinterName(),
                        record.getLabelName());

                    // Get the file stored for this label
                    exportFile = template.getLabelTemplateFormat();

                    // Check to see if the file is a valid PNL file from a
                    // Nicewatch Label export (i.e. it contains the special
                    // marker text) or a txt file using Vocollect Markers
                    if (isValidPnl(exportFile)) {

                        // Now we split the initial file into two strings; a
                        // header string and a body string
                        labelHeader = getLabelHeader(exportFile);
                        exportBody = getLabelBody(exportFile);

                        // Now we parse the labelBody string and remove the start
                        // of page marker and the end of page marker; we also replace
                        // the print quantity marker with a Freemarker string
                        try {
                            labelBody = replaceMarkerText(exportBody);
                        } catch (VocollectException ex) {
                            // Problem parsing the label; Stop printing
                            // It is important that we handle this exception here;
                            // otherwise the abstract print server will try to
                            // add the job to the history.  We also need to remove it
                            // from the job queue.
                            thisJob.setErrorCode(ex.getErrorCode());
                            thisJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                            moveJobToHistory();
                            return;
                        }

                        printCommands += labelHeader;

                    } else {
                        // if it is not a valid PNL file, then we assume that it
                        // is a valid FLT file, ready for processing
                        labelBody = exportFile;
                    }

                }

                // Set currentLabelName
                currentLabelName = record.getLabelName();

                // Get the text to send, then send it.
                try {
                    // Add body text
                    printCommands += generatePrintCommands(record, labelBody);
                } catch (VocollectException ex) {
                    // Problem finding the label; Stop printing
                    // It is important that we handle this exception here;
                    // otherwise the abstract print server will try to
                    // add the job to the history.  We also need to remove it
                    // from the job queue.
                    thisJob.setErrorCode(ex.getErrorCode());
                    thisJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                    moveJobToHistory();
                    return;
                }
            }   // End while


            // Try to send file over network
            try {
                Utilities.sendPrintJob("", printCommands,
                    printer.getHostName(), printer.getPortNumber(),
                    thisJob);
            } catch (VocollectException ex) {
                thisJob.setErrorCode(ex.getErrorCode());
                thisJob.setJobStatus(PrintJob.JobStatus.PRINT_FAILED);
                moveJobToHistory();
                return;
            }

            // If we get down here, everything was successful
            thisJob.setJobStatus(PrintJob.JobStatus.PRINT_SUCCESSFUL);
            moveJobToHistory();

            // Wait for a little while to avoid overflowing the printer.
            // Must be synchronized in order to call wait(), even though
            // only one job should be printed at a time.
            synchronized (this) {
                try {
                    wait(PRINT_DELAY);
                } catch (InterruptedException ex) {
                    // If interrupted, just return
                }
            }
        }

        /**
         * Figure out the actual printer command text that will be sent
         * to the label printer.  Will use Freemarker to do search and
         * replace from the body template.  This is called for every label
         * record in the print job.
         *
         * @param record The print job label to generate
         * @param bodyTemplate The Freemarked label body string
         * @return      The final print command string
         * @throws VocollectException On error updating the label list
         */
        private String generatePrintCommands(LabelData record, String bodyTemplate)
        throws VocollectException {

            // Take the # of copies from this record, and add to the field
            // data map.  This will allow the use of a ${copies} variable
            // in the template file.
            final String copiesVariableName = "copies";
            record.getLabelDataMap().put(copiesVariableName,
                record.getCopies());

            String commands;
            // Process template with Freemarker
            // First, open up the freemarker template file
            commands = Utilities.processTemplate(bodyTemplate, record);

            return commands;
        }

        /**
         * This subroutine will simply pop this job (we assume we are the
         * head of the queue) off the job queue and push it onto the job
         * history.  Called whenver the job should be marked as complete.
         */
        private void moveJobToHistory() {
            // Assert that the head of the queue is our print job
            PrintJob finishedJob = getJobQueueSafe().peek();
            assert finishedJob == thisJob : "Vocollect job queue out of order";
            // Remove print job from queue and move to history
            jobHistoryAdd(getJobQueueSafe().remove());
        }

        /**
         * This subroutine will check the template file for a valid PNL marker
         * to determine if we should process it as a PNL file or an FLT file.
         * @param pnlFile The raw .PNL file for the label
         * @return true if it is valid PNL or false
         */
        private boolean isValidPnl(String pnlFile) {
            boolean bReturn = false;

            if (pnlFile == null) {
                bReturn = false;
            } else {

                Pattern p = Pattern.compile(START_MARKER);
                Matcher m = p.matcher(pnlFile);
                boolean b = m.find();

                if (!b) {
                    // Check to see if we are using the Vocollect markers.
                    p = Pattern.compile(VOCOLLECT_START_MARKER);
                    m = p.matcher(pnlFile);
                    b = m.find();
                }

                bReturn = b;
            }
           return bReturn;
        }

        /**
         * This subroutine will strip the first line of Print commands from the
         * exported .PNL file up to the start of page marker.
         * @param pnlFile The raw .PNL file for the label
         * @return The set of Print commands which constitute the header
         */
        private String getLabelHeader(String pnlFile) {
            String header = "";

            if (pnlFile != null) {

                Pattern p = Pattern.compile(START_MARKER);
                Matcher m = p.matcher(pnlFile);
                boolean b = m.find();
                if (b) {
                    header = pnlFile.substring(0, m.start() - 1);
                } else {
                    // Look for the Vocollect Markers
                    Pattern p2 = Pattern.compile(VOCOLLECT_START_MARKER);
                    Matcher m2 = p2.matcher(pnlFile);
                    boolean b2 = m2.find();
                    if (b2) {
                        header = pnlFile.substring(0, m2.start() - 1);
                    }

                }
            }

            return header;
        }

        /**
         * This subroutine will get the body of Print commands from the
         * exported .PNL file from the start of page marker.
         * @param pnlFile The raw .PNL file for the label
         * @return The set of Print commands which constitute the label body
         */
        private String getLabelBody(String pnlFile) {
            String body = "";

            if (pnlFile != null) {

                Pattern p = Pattern.compile(START_MARKER);
                Matcher m = p.matcher(pnlFile);
                boolean b = m.find();
                if (b) {
                    body = pnlFile.substring(m.start());
                } else {
                    // Check for the Vocollect Markers
                    Pattern p2 = Pattern.compile(VOCOLLECT_START_MARKER);
                    Matcher m2 = p2.matcher(pnlFile);
                    boolean b2 = m2.find();
                    if (b2) {
                        body = pnlFile.substring(m2.start());
                    }
                }
            }

            return body;

        }

        /**
         * This subroutine will parse the raw .PNL text in the body string
         * and either remove or replace the special marker text.  This routine
         * processes for both the NiceLabel export format as well as the
         * Vocollect file format.
         * @param pnlBody The raw .PNL body string for the label
         * @return The parsed label body
         * @throws VocollectException if parsing fails
         */
        private String replaceMarkerText(String pnlBody)
        throws VocollectException {
            String replacementText;

            replacementText = "";
            pnlBody = Utilities.replaceText(pnlBody, START_MARKER, replacementText);
            pnlBody = Utilities.replaceText(pnlBody, VOCOLLECT_START_MARKER, replacementText);

            replacementText = FREEMARKER_QUANTITY;
            pnlBody = Utilities.replaceText(pnlBody, QUANTITY_MARKER, replacementText);
            pnlBody = Utilities.replaceText(pnlBody, VOCOLLECT_QUANTITY_MARKER, replacementText);
            replacementText = "";
            pnlBody = Utilities.replaceText(pnlBody, END_MARKER, replacementText);
            pnlBody = Utilities.replaceText(pnlBody, VOCOLLECT_END_MARKER, replacementText);

            return pnlBody;
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.AbstractPrintServer#shutdown()
     */
    @Override
    public void shutdown() {
        printExecutor.shutdown();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 