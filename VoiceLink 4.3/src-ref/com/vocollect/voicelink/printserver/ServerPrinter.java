/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;
import com.vocollect.epp.exceptions.VocollectException;

/**
 * This represents a print server's available printer.
 *
 * @author cblake
 */
public class ServerPrinter {
    private String printerName;
    /**
     * Note that an IP address is an array of 4 unsigned bytes.  Java does
     * not provide the unsigned byte data type, so some numbers may appear
     * to be negative.
     */
    private String hostName;
    private int portNumber;     // -1 when not applicable

    /** The label template print header.  Contains
     * print commands at the beginning of a print job. */
    private String templateHeader = "";

    /** The printer label separator text. */
    private String templateSeparator = "";

    /** The printer label footer text. */
    private String templateFooter;

    private String manufacturerName = "";
    private String modelName = "";

    /**
     * Constructor.
     * @param printerName   Name of the printer; used as unique identifier
     * @param hostName      ServerPrinter host name (may be an IP address)
     * @param portNumber    ServerPrinter's network port number
     * @throws VocollectException If the ip address is invalid.
     */
    public ServerPrinter(final String printerName, final String hostName,
                   int portNumber)
    throws VocollectException {
        this.printerName = printerName;
        // IP address must go through setter method to validate
        setHostName(hostName);
        this.portNumber = portNumber;
    }

    /**
     * Package protected JavaBean constructor to be loaded by XML.
     * Constructor.
     */
    ServerPrinter() {

    }

    /**
     * JavaBean mutator.  Should only be called when loading XML
     * @param printerName   New printer name
     */
    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    /**
     * @return  ServerPrinter's name identifier
     */
    public String getPrinterName() {
        return printerName;
    }

    /**
     * Note:  this field is only useful for the Vocollect print server.
     * All other print servers will track printer information themselves.
     * @return  Network IP address
     */
    public String getHostName() {
        return hostName;
    }

    /**
     * Setter for java bean.  Will validate to make sure it is a legal
     * IP address.
     * @param hostName New ip address for printer.
     * @throws VocollectException if IP address is invalid.
     */
    public void setHostName(String hostName) throws VocollectException {
        this.hostName = hostName;
    }

    /**
     * This field is only useful for the Vocollect print server.
     * @return  Network port number
     */
    public int getPortNumber() {
        return portNumber;
    }

    /**
     *
     * @param portNumber    New printer port number.
     */
    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    /**
     * Get the label template header; this contains the
     * list of print commands to be printed at the beginning of every
     * print job.
     * @return  The header text
     */
    public String getTemplateHeader() {
        return templateHeader;
    }

    /**
     * Update the template header by reading in the specified file name.
     * @param templateHeaderFile   New file name
     * @throws VocollectException When the file can't be read
     */
    public void setTemplateHeader(String templateHeaderFile)
    throws VocollectException {
        try {
            this.templateHeader = Utilities.readFile(
                templateHeaderFile, this);
        } catch (VocollectException ex) {
            throw new VocollectException(PrintServerErrorCode
                .BAD_TEMPLATE_HEADER_FILE, ex);
        }
    }


    /**
     * The label template separator between labels within the same print job.
     * For Vocollect print server only.
     * @return  Template separator text
     */
    public String getTemplateSeparator() {
        return templateSeparator;
    }

    /**
     * Update the template separator text by reading from a file.
     * @param templateSeparatorFile    New file name
     * @throws VocollectException When the separator file can't be read
     */
    public void setTemplateSeparator(String templateSeparatorFile)
    throws VocollectException {
        try {
            this.templateSeparator = Utilities.readFile(
                templateSeparatorFile, this);
        } catch (VocollectException ex) {
            throw new VocollectException(PrintServerErrorCode
                .BAD_TEMPLATE_SEPARATOR_FILE, ex);
        }
    }

    /**
     * The label template footer at the end of the print job.  For Vocollect
     * print server only.
     * @return The footer text
     */
    public String getTemplateFooter() {
        return templateFooter;
    }

    /**
     * Update the footer text by loading from a file.
     * @param templateFooterFile New file name
     * @throws VocollectException When footer file can't be read
     */
    public void setTemplateFooter(String templateFooterFile)
    throws VocollectException {
        try {
            this.templateFooter = Utilities.readFile(
                templateFooterFile, this);
        } catch (VocollectException ex) {
            throw new VocollectException(PrintServerErrorCode
                .BAD_TEMPLATE_FOOTER_FILE, ex);
        }
    }

    /**
     *
     * @param manufacturerName  New printer manufacturer name
     */
    public void setManufacturerName(final String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    /**
     *
     * @return  ServerPrinter manufacturer name
     */
    public String getManufacturerName() {
        return manufacturerName;
    }

    /**
     *
     * @param modelName New printer model name
     */
    public void setModelName(final String modelName) {
        this.modelName = modelName;
    }

    /**
     *
     * @return  ServerPrinter model name
     */
    public String getModelName() {
        return modelName;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 