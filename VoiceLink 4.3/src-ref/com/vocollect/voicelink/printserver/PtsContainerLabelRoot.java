/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 * @author treed
 */
public class PtsContainerLabelRoot extends BaseLabel {

    // Container Number to display on the label
    private String containerNumber;

    // Customer Location that contains delivery location, route, and customer info.
    private PtsCustomerLocation ptsCustomerLocation;

    /**
     * Constructor.
     * @param container - PTS container
     * @param copies - copies to print
     */
    public PtsContainerLabelRoot(PtsContainer container, Short copies) {
        super("PutToStoreContainer", copies);

        this.containerNumber = container.getContainerNumber();
        this.ptsCustomerLocation = container.getCustomer();
        //Build the LabelData
        addData();
    }


    /**
     * Getter for the container property.
     * @return Container value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }



    /**
     * Getter for the location property.
     * @return PtsCustomerLocation value of the property
     */
    public PtsCustomerLocation getPtsCustomerLocation() {
        return this.ptsCustomerLocation;
    }


    /**
     * Build the labeldata from the Container object.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.printserver.BaseLabel#addData()
     */
    @Override
    protected void addData() {
        Map<String, Object> labelDataMap = new HashMap<String, Object>();
        // Build the map for a Container Label

        // Get around lazy loading issue
        Customer c = getPtsCustomerLocation().getCustomerInfo();
        c.getCustomerNumber();
        c.getCustomerName();
        c.getCustomerAddress();

        getPtsCustomerLocation().getDeliveryLocation();
        getPtsCustomerLocation().getRoute();

        labelDataMap.put("customerLocation", ptsCustomerLocation);
        labelDataMap.put("containerNumber", getContainerNumber());

        //Build a LabelData Object and assign it to the LabelDataMember
        LabelData ld = new LabelData(this.getTypeName(), labelDataMap, this.getCopies());
        this.setLabelData(ld);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 