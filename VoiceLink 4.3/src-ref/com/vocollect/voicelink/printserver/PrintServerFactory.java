/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.printserver.AbstractPrintServer.PrintServerType;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Factory class which returns a PrintServer object.
 *
 * @author phanson
 */
public final class PrintServerFactory {

    //private static Properties serverProps = null;
    //private static PrintServerErrorCode error = null;
    //private static String serverType = null;
    private static final String SERVER_TYPE = "printServer.Type";
    private static final String HOST_NAME = "printServer.HostName";
    private static final String FILE_DROP_NAME = "printServer.FileDropLocation";
    private static final String SERVER_PROPERTIES =
        "PrintServerConfiguration";
    private static AbstractPrintServer abstractServer;


    /**
     * Constructor.
     *
     */
    private PrintServerFactory() {


    }

    /**
     * @return static abstractServer instance
     * @throws VocollectException on errors
     */
    public static AbstractPrintServer getPrintServer()
    throws VocollectException {
        if (abstractServer == null) {
            abstractServer = getNewPrintServer();
        }
        return abstractServer;
    }


    /**
     * Queries the PrintServerConfiguration properties to determine the Print
     * Server type to instantiate and then sets implementation specific fields
     * where required.
     * @return  AbstractPrintServer object
     * @throws VocollectException If server properties file contains an invalid
     *      server type, an exception is thrown
     */
    public static AbstractPrintServer getNewPrintServer()
    throws VocollectException {

        ResourceBundle rsb = null;

        try {
            rsb = ResourceBundle.getBundle(SERVER_PROPERTIES);
        } catch (NullPointerException e) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NOTFOUND;
            throw new VocollectException(error, e);
        } catch (MissingResourceException e) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NOTFOUND;
            throw new VocollectException(error, e);
        }

        String serverType = rsb.getString(SERVER_TYPE);

        if (serverType == null) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NULL_SERVERTYPE;
            throw new VocollectException(error);
        }

        AbstractPrintServer printServer = null;

        switch (PrintServerType.valueOf(serverType.toUpperCase())) {
        case BARTENDER :
            printServer = new BartenderPrintServer();
            String fileDropName = rsb.getString(FILE_DROP_NAME);
            if (fileDropName == null) {
                PrintServerErrorCode error = PrintServerErrorCode
                    .SERVER_PROPERTIES_NULL_FILEDROP;
                throw new VocollectException(error);
            } else {
                printServer.setFileDropLocation(fileDropName);
            }
            break;

        case LOFTWARE :
            printServer = new LoftwarePrintServer();
            break;

        case NICEWATCH :
            printServer = new NiceWatchPrintServer();
            String hostName = rsb.getString(HOST_NAME);
            if (hostName == null) {
                PrintServerErrorCode error = PrintServerErrorCode
                    .SERVER_PROPERTIES_NULL_IP;
                throw new VocollectException(error);
            } else {
                printServer.setServerHostName(hostName);
            }
            break;

        case VOCOLLECT :
            printServer = new VocollectPrintServer();
            break;

        case MOCK :
            printServer = new MockPrintServer();
            break;

        default :
            // Shouldn't get here; means that the properties file is invalid
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_INVALID_SERVERTYPE;
            throw new VocollectException(error);

        }

        return printServer;
    }

    /**
     * For testing, passes in the test XML config file
     * Queries the PrintServerConfiguration properties to determine the Print
     * Server type to instantiate and then sets implementation specific fields
     * where required.
     * @param propertiesFile test properties file
     * @param xmlConfigFile test configuration file
     * @return  AbstractPrintServer object
     * @throws VocollectException If server properties file contains an invalid
     *      server type, an exception is thrown
     */
    public static AbstractPrintServer getNewPrintServer(String propertiesFile,
                                                        String xmlConfigFile)
    throws VocollectException {

        ResourceBundle rsb = null;

        try {
            rsb = ResourceBundle.getBundle(propertiesFile);
        } catch (NullPointerException e) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NOTFOUND;
            throw new VocollectException(error, e);
        } catch (MissingResourceException e) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NOTFOUND;
            throw new VocollectException(error, e);
        }

        String serverType = rsb.getString(SERVER_TYPE);

        if (serverType == null) {
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_NULL_SERVERTYPE;
            throw new VocollectException(error);
        }

        AbstractPrintServer printServer = null;

        switch (PrintServerType.valueOf(serverType.toUpperCase())) {
        case BARTENDER :
            printServer = new BartenderPrintServer(xmlConfigFile);
            String fileDropName = rsb.getString(FILE_DROP_NAME);
            if (fileDropName == null) {
                PrintServerErrorCode error = PrintServerErrorCode
                    .SERVER_PROPERTIES_NULL_FILEDROP;
                throw new VocollectException(error);
            } else {
                printServer.setFileDropLocation(fileDropName);
            }
            break;

        case LOFTWARE :
            printServer = new LoftwarePrintServer(xmlConfigFile);
            break;

        case NICEWATCH :
            printServer = new NiceWatchPrintServer(xmlConfigFile);
            String hostName = rsb.getString(HOST_NAME);
            if (hostName == null) {
                PrintServerErrorCode error = PrintServerErrorCode
                    .SERVER_PROPERTIES_NULL_IP;
                throw new VocollectException(error);
            } else {
                printServer.setServerHostName(hostName);
            }
            break;

        case VOCOLLECT :
            printServer = new VocollectPrintServer(xmlConfigFile);
            break;

        case MOCK :
            printServer = new MockPrintServer(xmlConfigFile);
            break;

        default :
            // Shouldn't get here; means that the properties file is invalid
            PrintServerErrorCode error = PrintServerErrorCode
                .SERVER_PROPERTIES_INVALID_SERVERTYPE;
            throw new VocollectException(error);

        }

        return printServer;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 