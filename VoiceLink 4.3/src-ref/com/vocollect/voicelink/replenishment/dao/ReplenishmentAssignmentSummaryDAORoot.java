/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.replenishment.model.ReplenishmentAssignmentSummary;

import java.util.Date;
import java.util.List;


/**
 *
 *
 * @author pfunyak
 */
public interface ReplenishmentAssignmentSummaryDAORoot extends GenericDAO<ReplenishmentAssignmentSummary> {


    /**
     * Find assignment summary counts for replenishment assignments.
     * @param timeWindow - used to determine how much data to include
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listAssignmentCountsByRegionAndStatus(Date timeWindow)  throws DataAccessException;


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 