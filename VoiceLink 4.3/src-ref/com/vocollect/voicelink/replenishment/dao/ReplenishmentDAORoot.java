/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 *
 *
 * @author sfahnestock
 */
public interface ReplenishmentDAORoot extends GenericDAO<Replenishment> {

    /**
     * retrieves a list of replenishments for the operator.
     *
     * @param operatorId - the operator to find the list for
     * @param queryDecorator - extra query instructions
     * @return list of requested replenishments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listOperatorReplenishment(QueryDecorator queryDecorator,
                                                  Long operatorId)
        throws DataAccessException;

    /**
     * Get count of replenishments in other regions.
     *
     * @param operatorId - the operator
     * @return - the count of replenishments in other regions
     * @throws DataAccessException - database exception
     */
    Number countOperatorReplenishments(Long operatorId) throws DataAccessException;

    /**
     * Get count of replenishments in other regions.
     *
     * @param replenNumber - the number to search by
     * @return - the replenishment, may be null
     * @throws DataAccessException - database exception
     */
    Replenishment findReplenishmentByNumber(String replenNumber) throws DataAccessException;


    /**
     * Find next replenishment for a location.
     *
     * @param queryDecorator - query decorator to select top1
     * @param location - location to find replenishment for
     * @return - list of replenishements for location.
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listReplenishmentByLocation(
        QueryDecorator queryDecorator,
        Location location)
        throws DataAccessException;


    /**
     * Find next replenishment for a location/item combination.
     *
     * @param queryDecorator - query decorator to select top1
     * @param location - location to find replenishment for
     * @param item - item to find replenishment for
     * @return - list of replenishements for location.
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listReplenishmentByLocationAndItem(
        QueryDecorator queryDecorator,
        Location location,
        Item item)
        throws DataAccessException;

    /**
     * Get all assignments available for resequence.
     *
     * @param decorator - additional query instructions.
     * @return - list of assignments
     * @throws DataAccessException - database exception
     */
    List<DataObject> listResequenceAssignments(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get all assignments available for resequence in the
     * specified region.
     *
     * @param regionId - long representing id of the region.
     * @return - list of assignments
     * @throws DataAccessException - database exception
     */
    List<DataObject> listResequenceAssignmentsByRegion(Long regionId)
        throws DataAccessException;

    /**
     * Gets all Replenishments older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the ReplenishmentStatus used in the query
     * @return a list of Replenishments
     * @throws DataAccessException Database failure
     */
    List<Replenishment> listOlderThan(QueryDecorator decorator,
        Date date, ReplenishStatus status) throws DataAccessException;

    /**
     * @param operator - the operator
     * @return list of replenishment records that are inprogress for the operator
     * @throws DataAccessException for any database exception
     */
    List<Replenishment> listActiveReplenishment(Operator operator)
                                                throws DataAccessException;

    /**
     * Get all InProgress and Completed Replenishment records
     * for this operator.
     * @param operator to search
     * @param startDate - start date range
     * @param endDate - end date range
     * @return list of replenishment InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<Replenishment> listMostRecentRecordsByOperator(Operator operator,
                                                        Date startDate,
                                                        Date endDate)
                                                        throws DataAccessException;



    /**
     * Get all InProgress and Completed Replenishment records
     * for this operator greater than startDate.
     * @param operator to search
     * @param startDate - start date range
     * @return list of replenishment InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<Replenishment> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                             Date startDate)
                                                             throws DataAccessException;





    /**
     * @param decorator extra instructions for the query
     * @return list of maps.  Each map contains the fields for a completed
     * replenishment, the object.
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedReplenishments(QueryDecorator decorator)
    throws DataAccessException;

    /**
     * @param operatorId - the operator ID
     * @param startDate - the start date
     * @param endDate - the end date
     * @return List<Object[]>
     * @throws DataAccessException
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorId, Date startDate,
        Date endDate) throws DataAccessException;

    /**
     * @param operatorId - the operator ID
     * @param startDate - the start date
     * @param endDate - the end date
     * @return List<Object[]>
     * @throws DataAccessException
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId,
            Date startDate, Date endDate) throws DataAccessException;
    
    /**
     * @param operatorId - the operator ID
     * @param olStartDate - the operator labor start date
     * @param olEndDate - the operator labor end date
     * @param startDate - the start date
     * @param endDate - the end date
     * @return List<Object[]>
     * @throws DataAccessException
     */
    List<Object[]> listReplenishmentBetweenDates(Long operatorId, Date olStartDate, 
            Date olEndDate, Date startDate, 
            Date endDate) throws DataAccessException;

    /**
     * @param operatorId - the operator ID
     * @param startDate - the start date
     * @param endDate - the end date
     * @param regionNumber - the region number
     * @return List<Object[]>
     * @throws DataAccessException
     */
List<Object[]> listReplenishmentForRegionBetweenDates(Long operatorId, Date startDate, 
                     Date endDate, Integer regionNumber)
                     throws DataAccessException;

    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 