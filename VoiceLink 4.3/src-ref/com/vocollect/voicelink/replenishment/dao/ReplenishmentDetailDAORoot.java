/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;

import java.util.Date;
import java.util.List;


/**
 *
 *
 * @author sfahnestock
 */
public interface ReplenishmentDetailDAORoot extends GenericDAO<ReplenishmentDetail> {

    /**
     * Get all replenishment details.
     * @param decorator - additional query instructions.
     * @return the List of pick details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listReplenishmentDetails(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get all replenished detail records for an operator ordered by replenishTime.
     * @param operator to work on
     * @param startDate the starting date for the query.
     * @return replenishmentDetail records
     * @throws DataAccessException for any database exception
     */
    List<ReplenishmentDetail> listMostRecentRecordsByOperator(Operator operator,
                                                              Date startDate)
                                                              throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 