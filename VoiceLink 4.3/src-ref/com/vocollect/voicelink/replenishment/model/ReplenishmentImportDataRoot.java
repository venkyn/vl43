/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;



/**
 * @author jtauberg
 *
 * Image of a Replenishment that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the
 * tool to do it. Additionally, any methods we need to override exist in the body.
 */
public class ReplenishmentImportDataRoot  extends BaseModelObject implements Importable {

    private static final long serialVersionUID = -1368585313377664107L;

    private ImportableImpl impl = new ImportableImpl();

    private Replenishment myModel = new Replenishment();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        return this.myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setStatus(int)
     */
    public void setImportStatus(int importStatus) {
        impl.setImportStatus(importStatus);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }

    //Helper methods for setting and getting complex members

    /**
     * Set the number in the region. This will be used to look up a region in the database.
     * @param number the number to set the regionnumber to.
     */
    public void setRegionNumber(String number) {
        ReplenishmentRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new ReplenishmentRegion();
            this.setRegion(tempRegion);
        }
        tempRegion.setNumber(new Integer(number));
    }


    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public String getRegionNumber() {
        ReplenishmentRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new ReplenishmentRegion();
            this.setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return tempRegion.getNumber().toString();
    }


    /**
     * Set the number in the Item. This will be used to look up an Item in the database.
     * @param itemId the ID to set the Item.number to.
     */
    public void setItemId(String itemId) {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        tempItem.setNumber(itemId);
    }


    /**
     * Get the Item Id (Item.number).
     * @return the Item Id (Item.number a String).
     */
    public String getItemId() {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        if (null == tempItem.getNumber()) {
            return null;
        }
        return tempItem.getNumber();
    }



    /**
     * Set the DestinationLocation (scannedVerification) in the toLocation.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the toLocation.scannedVerification to.
     */
    public void setDestinationLocation(String scannedVerification) {
        Location tempLocation = myModel.getToLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setToLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }


    /**
     * Get the DestinationLocation's id (scannedVerification).
     * @return the DestinationLocation ID (toLocation.scannedVerification) (String).
     */
    public String getDestinationLocation() {
        Location tempLocation = myModel.getToLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setToLocation(tempLocation);
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }


    /**
     * Set the ReserverLocation (scannedVerification) in the FromLocation.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the FromLocation.scannedVerification to.
     */
    public void setReserveLocation(String scannedVerification) {
        Location tempLocation = myModel.getFromLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setFromLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }


    /**
     * Get the ReserveLocation's id (scannedVerification).
     * @return the ReserveLocation ID (fromLocation.scannedVerification) (String).
     */
    public String getReserveLocation() {
        Location tempLocation = myModel.getFromLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setFromLocation(tempLocation);
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }



    //Delegates from Replenishment model below


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getGoalTime()
     */
    public String getGoalTime() {
        return myModel.getGoalTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getLicenseNumber()
     */
    public String getLicenseNumber() {
        return myModel.getLicenseNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getRegion()
     */
    public ReplenishmentRegion getRegion() {
        return myModel.getRegion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getReplenishQuantity()
     */
    public int getReplenishQuantity() {
        return myModel.getReplenishQuantity();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getScannedValidation()
     */
    public String getScannedValidation() {
        return myModel.getScannedValidation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getSpokenValidation()
     */
    public String getSpokenValidation() {
        return myModel.getSpokenValidation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getStatus()
     */
    public ReplenishStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getType()
     */
    public ReplenishType getType() {
        return myModel.getType();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#isPromptLicense()
     */
    public boolean isPromptLicense() {
        return myModel.isPromptLicense();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setGoalTime(java.lang.String)
     */
    public void setGoalTime(String goalTime) {
        myModel.setGoalTime(goalTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setLicenseNumber(java.lang.String)
     */
    public void setLicenseNumber(String licenseNumber) {
        myModel.setLicenseNumber(licenseNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setPromptLicense(boolean)
     */
    public void setPromptLicense(boolean promptLicense) {
        myModel.setPromptLicense(promptLicense);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setRegion(com.vocollect.voicelink.replenishment.model.ReplenishmentRegion)
     */
    public void setRegion(ReplenishmentRegion region) {
        myModel.setRegion(region);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setReplenishQuantity(int)
     */
    public void setReplenishQuantity(int replenishQuantity) {
        myModel.setReplenishQuantity(replenishQuantity);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setScannedValidation(java.lang.String)
     */
    public void setScannedValidation(String scannedValidation) {
        myModel.setScannedValidation(scannedValidation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setSpokenValidation(java.lang.String)
     */
    public void setSpokenValidation(String spokenValidation) {
        myModel.setSpokenValidation(spokenValidation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setStatus(com.vocollect.voicelink.replenishment.model.ReplenishStatus)
     */
    public void setStatus(ReplenishStatus status) {
        myModel.setStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setToLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setToLocation(Location toLocation) {
        myModel.setToLocation(toLocation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setType(com.vocollect.voicelink.replenishment.model.ReplenishType)
     */
    public void setType(ReplenishType type) {
        myModel.setType(type);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getToLocation()
     */
    public Location getToLocation() {
        return myModel.getToLocation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#getFromLocation()
     */
    public Location getFromLocation() {
        return myModel.getFromLocation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.model.ReplenishmentRoot#setFromLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setFromLocation(Location fromLocation) {
        myModel.setFromLocation(fromLocation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 