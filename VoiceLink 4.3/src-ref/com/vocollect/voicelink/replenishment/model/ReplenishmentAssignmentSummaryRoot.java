/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.Region;

import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 * @author pfunyak
 */
public class ReplenishmentAssignmentSummaryRoot extends BaseModelObject
    implements Taggable {

    //
    private static final long serialVersionUID = -6668204437442898204L;

    private Site site;

    private Region region;

    private Integer totalAssignments;

    private Integer inProgressAssignments;

    private Integer availableAssignments;

    private Integer completedAssignments;

    private Integer nonCompletedAssignments;

    private Set<Tag> tags = new HashSet<Tag>();

    /**
     * Getter for the region property.
     * @return Region value of the property
     */
    public Region getRegion() {
        return this.region;
    }

    /**
     * Getter for the site property.
     * @return Site value of the property
     */

    public Site getSite() {
        return this.site;
    }

    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(Region region) {
        this.region = region;
    }

    /**
     * Getter for the totalAssignments property.
     * @return Integer value of the property
     */
    public Integer getTotalAssignments() {
        return this.totalAssignments;
    }

    /**
     * Setter for the totalAssignments property.
     * @param totalAssignments the new totalAssignments value
     */
    public void setTotalAssignments(Integer totalAssignments) {
        this.totalAssignments = totalAssignments;
    }

    /**
     * Getter for the inProgressAssignments property.
     * @return Integer value of the property
     */
    public Integer getInProgressAssignments() {
        return this.inProgressAssignments;
    }

    /**
     * Setter for the inProgressAssignments property.
     * @param inProgressAssignments the new inProgressAssignments value
     */
    public void setInProgressAssignments(Integer inProgressAssignments) {
        this.inProgressAssignments = inProgressAssignments;
    }

    /**
     * Getter for the availableAssignments property.
     * @return Integer value of the property
     */
    public Integer getAvailableAssignments() {
        return this.availableAssignments;
    }

    /**
     * Setter for the availableAssignments property.
     * @param availableAssignments the new availableAssignments value
     */
    public void setAvailableAssignments(Integer availableAssignments) {
        this.availableAssignments = availableAssignments;
    }

    /**
     * Getter for the completedAssignments property.
     * @return Integer value of the property
     */
    public Integer getCompletedAssignments() {
        return this.completedAssignments;
    }

    /**
     * Setter for the completedAssignments property.
     * @param completedAssignments the new completedAssignments value
     */
    public void setCompletedAssignments(Integer completedAssignments) {
        this.completedAssignments = completedAssignments;
    }

    /**
     * Getter for the nonCompletedAssignments property.
     * @return Integer value of the property
     */
    public Integer getNonCompletedAssignments() {
        return this.nonCompletedAssignments;
    }

    /**
     * Setter for the nonCompletedAssignments property.
     * @param nonCompletedAssignments the new nonCompletedAssignments value
     */
    public void setNonCompletedAssignments(Integer nonCompletedAssignments) {
        this.nonCompletedAssignments = nonCompletedAssignments;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObjectRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getId()
     */
    public Long getId() {
        // Theres no such thing as an ID for an aggregate, so use region.
        if (getRegion() == null) {
            return null;
        } else {
            return getRegion().getId();
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReplenishmentAssignmentSummary)) {
            return false;
        }
        final ReplenishmentAssignmentSummaryRoot other = (ReplenishmentAssignmentSummaryRoot) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 