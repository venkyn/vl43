/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.voicelink.core.model.Region;

import java.util.Set;

/**
 *
 * @author sfahnestock
 */
public class ReplenishmentRegionRoot extends Region {

    private static final long serialVersionUID = 4953348240151978588L;

    private boolean             allowCancelLicense;

    private boolean             allowOverrideLocation;

    private boolean             allowOverridePickUpQty;

    private boolean             allowPartialPut;

    private boolean             capturePickUpQty;

    private boolean             capturePutQty;

    private Integer             licDigitsTaskSpeaks;

    private int                 locDigitsOperSpeaks;

    private int                 checkDigitsOperSpeaks;

    private String              exceptionLocation;

    private boolean             verifySpokenLicenseLocation;

    private Set<Replenishment>  replenishments;


    /**
     * Getter for the replenishments property.
     * @return List&lt;Replenishment&lt; value of the property
     */
    public Set<Replenishment> getReplenishments() {
        return replenishments;
    }


    /**
     * Setter for the replenishments property.
     * @param replenishments the new replenishments value
     */
    public void setReplenishments(Set<Replenishment> replenishments) {
        this.replenishments = replenishments;
    }


    /**
     * Getter for the allowCancelLicense property.
     * @return boolean value of the property
     */
    public boolean isAllowCancelLicense() {
        return allowCancelLicense;
    }


    /**
     * Setter for the allowCancelLicense property.
     * @param allowCancelLicense the new allowCancelLicense value
     */
    public void setAllowCancelLicense(boolean allowCancelLicense) {
        this.allowCancelLicense = allowCancelLicense;
    }


    /**
     * Getter for the allowOverrideLocation property.
     * @return boolean value of the property
     */
    public boolean isAllowOverrideLocation() {
        return allowOverrideLocation;
    }


    /**
     * Setter for the allowOverrideLocation property.
     * @param allowOverrideLocation the new allowOverrideLocation value
     */
    public void setAllowOverrideLocation(boolean allowOverrideLocation) {
        this.allowOverrideLocation = allowOverrideLocation;
    }



    /**
     * Getter for the allowOverridePickupQty property.
     * @return boolean value of the property
     */
    public boolean isAllowOverridePickUpQty() {
        return allowOverridePickUpQty;
    }


    /**
     * Setter for the allowOverridePickupQty property.
     * @param allowOverridePickUpQty the new allowOverridePickupQty value
     */
    public void setAllowOverridePickUpQty(boolean allowOverridePickUpQty) {
        this.allowOverridePickUpQty = allowOverridePickUpQty;
    }


    /**
     * Getter for the allowPartialPut property.
     * @return boolean value of the property
     */
    public boolean isAllowPartialPut() {
        return allowPartialPut;
    }


    /**
     * Setter for the allowPartialPut property.
     * @param allowPartialPut the new allowPartialPut value
     */
    public void setAllowPartialPut(boolean allowPartialPut) {
        this.allowPartialPut = allowPartialPut;
    }



    /**
     * Getter for the capturePickUpQty property.
     * @return boolean value of the property
     */
    public boolean isCapturePickUpQty() {
        return capturePickUpQty;
    }


    /**
     * Setter for the capturePickUpQty property.
     * @param capturePickUpQty the new capturePickUpQty value
     */
    public void setCapturePickUpQty(boolean capturePickUpQty) {
        this.capturePickUpQty = capturePickUpQty;
    }


    /**
     * Getter for the capturePutQty property.
     * @return boolean value of the property
     */
    public boolean isCapturePutQty() {
        return capturePutQty;
    }


    /**
     * Setter for the capturePutQty property.
     * @param capturePutQty the new capturePutQty value
     */
    public void setCapturePutQty(boolean capturePutQty) {
        this.capturePutQty = capturePutQty;
    }


    /**
     * Getter for the checkDigitsOperSpeak property.
     * @return int value of the property
     */
    public int getCheckDigitsOperSpeaks() {
        return checkDigitsOperSpeaks;
    }


    /**
     * Setter for the checkDigitsOperSpeak property.
     * @param checkDigitsOperSpeaks the new checkDigitsOperSpeak value
     */
    public void setCheckDigitsOperSpeaks(int checkDigitsOperSpeaks) {
        this.checkDigitsOperSpeaks = checkDigitsOperSpeaks;
    }


    /**
     * Getter for the exceptionLocation property.
     * @return String value of the property
     */
    public String getExceptionLocation() {
        return exceptionLocation;
    }


    /**
     * Setter for the exceptionLocation property.
     * @param exceptionLocation the new exceptionLocation value
     */
    public void setExceptionLocation(String exceptionLocation) {
        this.exceptionLocation = exceptionLocation;
    }


    /**
     * Getter for the licDigitsTaskSpeaks property.
     * @return int value of the property
     */
    public Integer getLicDigitsTaskSpeaks() {
        return licDigitsTaskSpeaks;
    }


    /**
     * Setter for the licDigitsTaskSpeaks property.
     * @param licDigitsTaskSpeaks the new licDigitsTaskSpeaks value
     */
    public void setLicDigitsTaskSpeaks(Integer licDigitsTaskSpeaks) {
        this.licDigitsTaskSpeaks = licDigitsTaskSpeaks;
    }


    /**
     * Getter for the locDigitsOperSpeak property.
     * @return int value of the property
     */
    public int getLocDigitsOperSpeaks() {
        return locDigitsOperSpeaks;
    }


    /**
     * Setter for the locDigitsOperSpeak property.
     * @param locDigitsOperSpeaks the new locDigitsOperSpeak value
     */
    public void setLocDigitsOperSpeaks(int locDigitsOperSpeaks) {
        this.locDigitsOperSpeaks = locDigitsOperSpeaks;
    }


    /**
     * Getter for the verifySpokenLicenseLocation property.
     * @return boolean value of the property
     */
    public boolean isVerifySpokenLicenseLocation() {
        return verifySpokenLicenseLocation;
    }


    /**
     * Setter for the verifySpokenLicenseLocation property.
     * @param verifySpokenLicenseLocation the new verifySpokenLicenseLocation value
     */
    public void setVerifySpokenLicenseLocation(boolean verifySpokenLicenseLocation) {
        this.verifySpokenLicenseLocation = verifySpokenLicenseLocation;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 