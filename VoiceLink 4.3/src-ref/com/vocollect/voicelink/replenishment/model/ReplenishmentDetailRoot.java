/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.ReasonCode;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentDetailRoot extends CommonModelObject
    implements Serializable, Taggable {

    private static final long serialVersionUID = 3231495093487133756L;

    private ReplenishDetailStatus   status;

    private Location                replenishLocation;

    private int                     quantity;

    private Operator                operator;

    private Date                    startTime;

    private Date                    replenishTime;

    private ReasonCode              reason;

    private Replenishment           replenishment;

    private ExportStatus            exportStatus = ExportStatus.NotExported;

    private Set<Tag> tags;



    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }


    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }



    /**
     * Getter for the replenishment property.
     * @return Replenishment value of the property
     */
    public Replenishment getReplenishment() {
        return replenishment;
    }



    /**
     * Setter for the replenishment property.
     * @param replenishment the new replenishment value
     */
    public void setReplenishment(Replenishment replenishment) {
        this.replenishment = replenishment;
    }


    /**
     * Getter for the reason property.
     * @return ReasonCode value of the property
     */
    public ReasonCode getReason() {
        return reason;
    }


    /**
     * Setter for the reason property.
     * @param reason the new reason value
     */
    public void setReason(ReasonCode reason) {
        this.reason = reason;
    }

    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the quantity property.
     * @return int value of the property
     */
    public int getQuantity() {
        return quantity;
    }


    /**
     * Setter for the quantity property.
     * @param quantity the new quantity value
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    /**
     * Getter for the replenishLocation property.
     * @return Location value of the property
     */
    public Location getReplenishLocation() {
        return replenishLocation;
    }


    /**
     * Setter for the replenishLocation property.
     * @param replenishLocation the new replenishLocation value
     */
    public void setReplenishLocation(Location replenishLocation) {
        this.replenishLocation = replenishLocation;
    }


    /**
     * Getter for the replenishTime property.
     * @return Date value of the property
     */
    public Date getReplenishTime() {
        return replenishTime;
    }


    /**
     * Setter for the replenishTime property.
     * @param replenishTime the new replenishTime value
     */
    public void setReplenishTime(Date replenishTime) {
        this.replenishTime = replenishTime;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the status property.
     * @return ReplenishDetailStatus value of the property
     */
    public ReplenishDetailStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(ReplenishDetailStatus status) {
        this.status = status;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ReplenishmentDetail)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReplenishmentDetail other = (ReplenishmentDetail) obj;
        if (operator == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!operator.equals(other.getOperator())) {
            return false;
        }
        if (replenishTime == null) {
            if (other.getReplenishTime() != null) {
                return false;
            }
        } else if (!replenishTime.equals(other.getReplenishTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((replenishTime == null) ? 0 : replenishTime.hashCode());
        return result;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 