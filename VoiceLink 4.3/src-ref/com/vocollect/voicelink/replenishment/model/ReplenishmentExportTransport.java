/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.replenishment.model;


import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.ExportTransportObject;



/**
 * @author jtauberg
 *
 */
public class ReplenishmentExportTransport extends ExportTransportObject {

    /**
     * Default Constructor.
     *
     */
    public ReplenishmentExportTransport() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToInProgress(Object object) {
        try {
            //Loop through the replenishment details and set them all to InProgress.
            for (ReplenishmentDetail rd : ((Replenishment) object).getReplenishDetails()) {
                rd.setExportStatus(ExportStatus.InProgress);
            }
            return;
        } catch (ClassCastException e) {
            // Expected
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToExported(Object object) {
        try {
            //Loop through the replenishment details and set them all to exported.
            for (ReplenishmentDetail rd : ((Replenishment) object).getReplenishDetails()) {
                rd.setExportStatus(ExportStatus.Exported);
            }
            return;
        } catch (ClassCastException e) {
            // Expected
        }
    }


    /**
     * {@inheritDoc}
     */
    public void updateExportToInProgress(Replenishment object) {
        //Loop through the replenishment details and set them all to InProgress.
        for (ReplenishmentDetail rd : object.getReplenishDetails()) {
            rd.setExportStatus(ExportStatus.InProgress);
        }
    }


    /**
     * {@inheritDoc}
     */
    public void updateExportToExported(Replenishment object) {
        //Loop through the replenishment details and set them all to exported.
        for (ReplenishmentDetail rd : object.getReplenishDetails()) {
            rd.setExportStatus(ExportStatus.Exported);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 