/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;

/**
 * This class is a component that groups together the fields that
 * make up the order of issuance for replenishments.
 */
public class IssuanceOrderRoot implements Serializable {

    //
    private static final long serialVersionUID = 5269184521548015390L;

    private ReplenishPriority           priority = ReplenishPriority.Normal;

    private Long                        sequenceNumber = new Long(-1);

    /**
     * Getter for the priority property.
     * @return ReplenishPriority value of the property
     */
    public ReplenishPriority getPriority() {
        return priority;
    }

    /**
     * Setter for the priority property.
     * @param priority the new priority value
     */
    public void setPriority(ReplenishPriority priority) {
        this.priority = priority;
    }


    /**
     * Getter for the sequenceNumber property.
     * @return Long value of the property
     */
    public Long getSequenceNumber() {
        return sequenceNumber;
    }


    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
    /**
     * Overriding the toString to take care of displaying Issuance Order
     * in the table component and toop tip.
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {

        String issuanceOrderString = "";
        if (getPriority() != null) {
            issuanceOrderString += ResourceUtil.getLocalizedEnumName(getPriority()) + "-";
        }
        if (getSequenceNumber() != null) {
            issuanceOrderString += getSequenceNumber() + "-";
        }

        int descrLength = issuanceOrderString.length();
        if (issuanceOrderString.charAt(descrLength - 1) == '-') {
            issuanceOrderString = issuanceOrderString.substring(0, descrLength - 1);
        }

        return issuanceOrderString;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 