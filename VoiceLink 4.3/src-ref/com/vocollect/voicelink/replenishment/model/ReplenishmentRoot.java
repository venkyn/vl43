/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Resequencable;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentRoot extends CommonModelObject
implements Serializable, Taggable, Resequencable {

    private static final long serialVersionUID = 4953348240151978588L;

    private String                      number;

    private ReplenishType               type;

   //Original Sequence # is defaulted to -1 so that it can be set when
   // setSequenceNumber is called.
    private Long                        originalSequenceNumber = -1L;

    private ReplenishmentRegion         region;

    private Date                        dateReceived;

    private String                      licenseNumber;

    private boolean                     promptLicense;

    private String                      scannedValidation;

    private String                      spokenValidation;

    private Location                    fromLocation;

    private Location                    toLocation;

    private ReplenishStatus             status;

    private int                         replenishQuantity;

    private int                         totalReplenishedQuantity;

    private Operator                    operator;

    private Date                        startTime;

    private Date                        endTime;

    private Item                        item;

    private String                      goalTime;

    private int                         detailCount = 0;

    private List<ReplenishmentDetail>   replenishDetails;

    private Set<Tag>                    tags;

    private Long                        newSequenceNumber;

    // The description that comprise the issuanceOrder.
    private IssuanceOrder               issuanceOrder;


    /**
     * Getter for the new sequence number property.
     * This property is not persisted.  Used for resequencing.
     * @return Long value of the property
     */
    public Long getNewSequenceNumber() {
        return newSequenceNumber;
    }

    /**
     * Setter for the new sequence number property.
     * This property is not persisted.  Used for resequencing.
     * @param newSequenceNumber the new sequence number value
     */
    public void setNewSequenceNumber(Long newSequenceNumber) {
        this.newSequenceNumber = newSequenceNumber;
    }

    /**
     * Getter for the IssuanceOrder property.
     * @return IssuanceOrder value of the property
     */
    public IssuanceOrder getIssuanceOrder() {
        if (this.issuanceOrder != null) {
            return this.issuanceOrder;
          } else {
            return new IssuanceOrder();
          }
    }

    /**
     * Setter for the IssuanceOrder property.
     * @param issuanceOrder new value for the property
     */
    public void setIssuanceOrder(IssuanceOrder issuanceOrder) {
        this.issuanceOrder = issuanceOrder;
    }

    /**
     * Getter for the detailCount property.
     * @return int value of the property
     */
    public int getDetailCount() {
        return detailCount;
    }

    /**
     * Setter for the detailCount property.
     * @param detailCount the new detailCount value
     */
    public void setDetailCount(int detailCount) {
        this.detailCount = detailCount;
    }



    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the replenishDetails property.
     * @return List&lt;ReplenishmentDetail&lt; value of the property
     */
    public List<ReplenishmentDetail> getReplenishDetails() {
        return replenishDetails;
    }

    /**
     * Setter for the replenishDetails property.
     * @param replenishDetails the new replenishDetails value
     */
    public void setReplenishDetails(List<ReplenishmentDetail> replenishDetails) {
        this.replenishDetails = replenishDetails;
    }



    /**
     * Getter for the status property.
     * @return ReplenishStatus value of the property
     */
    public ReplenishStatus getStatus() {
        if (status == null) {
            this.status = ReplenishStatus.Available;
        }
        return status;
    }



    /**
     * This is special method  for importer to not allow to change status from
     * in-progress, Complete or canceled.
     * Setter for the status property.
     * @param replenStatus the new status value
     */
    public void setStatusForImport(ReplenishStatus replenStatus) {
        if (this.status == ReplenishStatus.Available || this.status == ReplenishStatus.Unavailable) {
            setStatus(replenStatus);
        }
    }


    /**
     * Getter for the status property.
     * @return ReplenishStatus value of the property
     */
    public ReplenishStatus getStatusForImport() {
        if (status == null) {
            this.status = ReplenishStatus.Available;
        }
        return status;
    }



    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(ReplenishStatus status) {
        this.status = status;
    }

    /**
     * Getter for the dateReceived property.
     * @return Date value of the property
     */
    public Date getDateReceived() {
        return dateReceived;
    }


    /**
     * Setter for the dateReceived property.
     * @param dateReceived the new dateReceived value
     */
    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }


    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return endTime;
    }


    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }


    /**
     * Getter for the fromLocation property.
     * @return Location value of the property
     */
    public Location getFromLocation() {
        return fromLocation;
    }


    /**
     * Setter for the fromLocation property.
     * @param fromLocation the new fromLocation value
     */
    public void setFromLocation(Location fromLocation) {
        this.fromLocation = fromLocation;
    }


    /**
     * Getter for the goalTime property.
     * @return String value of the property
     */
    public String getGoalTime() {
        return goalTime;
    }


    /**
     * Setter for the goalTime property.
     * @param goalTime the new goalTime value
     */
    public void setGoalTime(String goalTime) {
        this.goalTime = goalTime;
    }


    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }


    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }


    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }


    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }


    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the originalSequenceNumber property.
     * @return Long value of the property
     */
    public Long getOriginalSequenceNumber() {
        return originalSequenceNumber;
    }


    /**
     * Setter for the originalSequenceNumber property.
     * @param originalSequenceNumber the new originalSequenceNumber value
     */
    public void setOriginalSequenceNumber(Long originalSequenceNumber) {
        this.originalSequenceNumber = originalSequenceNumber;
    }


    /**
     * Getter for the promptLicense property.
     * @return boolean value of the property
     */
    public boolean isPromptLicense() {
        return promptLicense;
    }


    /**
     * Setter for the promptLicense property.
     * @param promptLicense the new promptLicense value
     */
    public void setPromptLicense(boolean promptLicense) {
        this.promptLicense = promptLicense;
    }


    /**
     * Getter for the region property.
     * @return ReplenishRegion value of the property
     */
    public ReplenishmentRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(ReplenishmentRegion region) {
        this.region = region;
    }


    /**
     * Getter for the replenishQuantity property.
     * @return int value of the property
     */
    public int getReplenishQuantity() {
        return replenishQuantity;
    }


    /**
     * Setter for the replenishQuantity property.
     * @param replenishQuantity the new replenishQuantity value
     */
    public void setReplenishQuantity(int replenishQuantity) {
        this.replenishQuantity = replenishQuantity;
    }


    /**
     * Getter for the scannedValidation property.
     * @return String value of the property
     */
    public String getScannedValidation() {
        return scannedValidation;
    }


    /**
     * Setter for the scannedValidation property.
     * @param scannedValidation the new scannedValidation value
     */
    public void setScannedValidation(String scannedValidation) {
        this.scannedValidation = scannedValidation;
    }

    /**
     * Getter for the spokenValidation property.
     * @return String value of the property
     */
    public String getSpokenValidation() {
        return spokenValidation;
    }


    /**
     * Setter for the spokenValidation property.
     * @param spokenValidation the new spokenValidation value
     */
    public void setSpokenValidation(String spokenValidation) {
        this.spokenValidation = spokenValidation;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the toLocation property.
     * @return Location value of the property
     */
    public Location getToLocation() {
        return toLocation;
    }


    /**
     * Setter for the toLocation property.
     * @param toLocation the new toLocation value
     */
    public void setToLocation(Location toLocation) {
        this.toLocation = toLocation;
    }


    /**
     * Getter for the totalReplenishedQuantity property.
     * @return int value of the property
     */
    public int getTotalReplenishedQuantity() {
        return totalReplenishedQuantity;
    }


    /**
     * Setter for the totalReplenishedQuantity property.
     * @param totalReplenishedQuantity the new totalReplenishedQuantity value
     */
    public void setTotalReplenishedQuantity(int totalReplenishedQuantity) {
        this.totalReplenishedQuantity = totalReplenishedQuantity;
    }


    /**
     * Getter for the type property.
     * @return ReplenishType value of the property
     */
    public ReplenishType getType() {
        if (type == null) {
            this.type = ReplenishType.Replenishment;
        }
        return type;
    }


    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(ReplenishType type) {
        this.type = type;
    }

    /**
     * Sets the status to complete.
     *
     * @throws TaskCommandException - task exception
     */
    public void setComplete() throws TaskCommandException {
        if (getStatus().equals(ReplenishStatus.Complete)) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_CHANGE_STATUS);
        } else {
            setStatus(ReplenishStatus.Complete);
        }
    }

    /**
     * Sets the status to in progress.
     *
     * @throws TaskCommandException - task exception
     */
    public void setInProgress() throws TaskCommandException {
        if (getStatus().equals(ReplenishStatus.Complete)) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_CHANGE_STATUS);
        } else {
            setStatus(ReplenishStatus.InProgress);
        }
    }

    /**
     * Sets the status to available.
     *
     * @throws TaskCommandException - task exception
     */
    public void setAvailable() throws TaskCommandException {
        if (getStatus().equals(ReplenishStatus.Complete)) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_CHANGE_STATUS);
        } else {
            setStatus(ReplenishStatus.Available);
        }
    }

    /**
     * Sets the status to unavailable.
     *
     * @throws TaskCommandException - task exception
     */
    public void setUnavailable() throws TaskCommandException {
        if (getStatus().equals(ReplenishStatus.Complete)) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_CHANGE_STATUS);
        } else {
            setStatus(ReplenishStatus.Unavailable);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof License)) {
            return false;
        }
        final License other = (License) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.number == null
        ? 0 : this.number.hashCode();
    }

    /*
     * These methods delegate to the IssuanceOrder in order to make
     * the Import simpler. Depending on design enhancements in the import,
     * these may be removed.
     * TODO Remove the delegates or this comment after refactoring the Import
     */

    /**
     * Delegate to the IssuanceOrder method.
     * {@inheritDoc}
     * @see IssuanceOrder#getPriority()
     */
    public ReplenishPriority getPriority() {
        if (null == issuanceOrder) {
            issuanceOrder = new IssuanceOrder();
        }
        return issuanceOrder.getPriority();
    }

    /**
     * Delegate to the IssuanceOrder method.
     * {@inheritDoc}
     * @see IssuanceOrder#getSequenceNumber()
     */
    public Long getSequenceNumber() {
        if (null == issuanceOrder) {
            issuanceOrder = new IssuanceOrder();
        }
        return issuanceOrder.getSequenceNumber();
    }

    /**
     * Delegate to the IssuanceOrder method.
     * {@inheritDoc}
     * @see IssuanceOrder#setPriority(ReplenishPriority)
     */
    public void setPriority(ReplenishPriority priority) {
        if (null == issuanceOrder) {
            issuanceOrder = new IssuanceOrder();
        }
        issuanceOrder.setPriority(priority);
    }

    /**
     * Delegate to the SequenceNumber method.
     * {@inheritDoc}
     * @see IssuanceOrder#setSequenceNumber(Long)
     */
    public void setSequenceNumber(Long sequenceNumber) {
        if (null == issuanceOrder) {
            issuanceOrder = new IssuanceOrder();
        }
        issuanceOrder.setSequenceNumber(sequenceNumber);
        //Original Seq is initially -1, if it wasn't set yet, set it.
        if (getOriginalSequenceNumber() == -1L) {
            setOriginalSequenceNumber(getSequenceNumber());
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getNumber();
    }


  }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 