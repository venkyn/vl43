/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.ReasonCodeManager;
import com.vocollect.voicelink.replenishment.model.ReplenishDetailStatus;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.ReplenishType;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.task.command.BaseReplenishmentTaskCommand;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentLicenseCmdRoot extends BaseReplenishmentTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private String                  replenishmentNumber;

    private Integer                 quantityPut;

    // This needs to be a String because an empty String is sent by the
    // task when the replenishment is canceled.
    // If it is not-null or empty, the data must be convertable to a long number.
    private String                  locationPk;

    private Integer                 replenishIndicator;

    private String                  reasonCode;

    private String                  startTime;

    private Replenishment           receivedReplenishment;

    private ReplenishmentDetail     newDetail = new ReplenishmentDetail();

    private LocationManager         locationManager;

    private LaborManager            laborManager;

    private ReasonCodeManager       reasonCodeManager;

    /**
     * Getter for the reason code  property.
     * @return Integer value of the property
     */
    public Integer getReasonCodeAsInteger() {
        if (StringUtil.isNullOrEmpty(this.reasonCode)) {
            return null;
        } else {
            return Integer.parseInt(this.reasonCode);
        }
    }


    /**
     * Getter for the replenishIndicator property.
     * @return int value of the property
     */
    public Integer getReplenishIndicator() {
        return replenishIndicator;
    }

    /**
     * Setter for the replenishIndicator property.
     * @param replenishIndicator the new replenishIndicator value
     */
    public void setReplenishIndicator(Integer replenishIndicator) {
     this.replenishIndicator = replenishIndicator;
    }


    /**
     * Getter for the locationPk property.
     * @return String value of the property
     */
    public String getLocationPk() {
        return this.locationPk;
    }


    /**
     * Setter for the locationPk property.
     * @param locationPk the new locationPk value
     */
    public void setLocationPk(String locationPk) {
        this.locationPk = locationPk;
    }


    /**
     * Get the container for this pick. This will be retrieved
     * from the database via the containerID specified in the command.
     * @return the Container, or null if no containerID was specified.
     * @throws DataAccessException on failure to retrieve.
     */
    public Long  getLocationPkAsLong() throws DataAccessException {
        if (StringUtil.isNullOrEmpty(this.locationPk)) {
            return null;
        } else {
            return Long.parseLong(this.getLocationPk());
        }
    }

    /**
     * Getter for the reasonCodeManager property.
     * @return ReasonCodeManager value of the property
     */
    public ReasonCodeManager getReasonCodeManager() {
        return this.reasonCodeManager;
    }


    /**
     * Setter for the reasonCodeManager property.
     * @param reasonCodeManager the new reasonCodeManager value
     */
    public void setReasonCodeManager(ReasonCodeManager reasonCodeManager) {
        this.reasonCodeManager = reasonCodeManager;
    }


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the newDetail property.
     * @return ReplenishmentDetail value of the property
     */
    public ReplenishmentDetail getNewDetail() {
        return newDetail;
    }

    /**
     * Setter for the newDetail property.
     * @param newDetail the new newDetail value
     */
    public void setNewDetail(ReplenishmentDetail newDetail) {
        this.newDetail = newDetail;
    }

    /**
     * Getter for the receivedReplenishment property.
     * @return Replenishment value of the property
     */
    public Replenishment getReceivedReplenishment() {
        return receivedReplenishment;
    }

    /**
     * Setter for the receivedReplenishment property.
     * @param receivedReplenishment the new receivedReplenishment value
     */
    public void setReceivedReplenishment(Replenishment receivedReplenishment) {
        this.receivedReplenishment = receivedReplenishment;
    }


    /**
     * Getter for the quantityPut property.
     * @return int value of the property
     */
    public Integer getQuantityPut() {
        return quantityPut;
    }

    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(Integer quantityPut) {
        this.quantityPut = quantityPut;
    }

    /**
     * Getter for the reasonCode property.
     * @return String value of the property
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Setter for the reasonCode property.
     * @param reasonCode the new reasonCode value
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Getter for the replenishmentNumber property.
     * @return String value of the property
     */
    public String getReplenishmentNumber() {
        return replenishmentNumber;
    }

    /**
     * Setter for the replenishmentNumber property.
     * @param replenishmentNumber the new replenishmentNumber value
     */
    public void setReplenishmentNumber(String replenishmentNumber) {
        this.replenishmentNumber = replenishmentNumber;
    }

    /**
     * Getter for the startTime property.
     * @return String value of the property
     */
    public String getStartTime() {
        return startTime;

    }

    /**
     * Getter for the startTime property.
     * @return String value of the property
     */
    public Date getStartTimeAsDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(
            BaseTaskCommand.TASK_COMMAND_DATE_FORMAT);

        try {
            return formatter.parse(getStartTime());
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        // Actions
        getReplenishment();
        validateReplenishmentExists();
        validateReplenishmentNotComplete();
        createNewDetail();
        updateMainReplenishment();
        updateLocations();

        // Response
        buildResponse();
        return getResponse();
    }

    /**
     * Gets the retrieved replenishment.
     *
     * @throws DataAccessException - database exception
     */
    protected void getReplenishment() throws DataAccessException {
        setReceivedReplenishment(getReplenishmentManager()
            .findReplenishmentByNumber(getReplenishmentNumber()));
    }

    /**
     * Validates that the requested replenishment exists.
     *
     * @throws TaskCommandException - task exception
     */
    protected void validateReplenishmentExists() throws TaskCommandException {
        if (getReceivedReplenishment() == null) {
            // Throw replenishment not found error
            throw new TaskCommandException(
                TaskErrorCode.REPLEN_NOT_FOUND, getReplenishmentNumber());
        }
    }

    /**
     * Validates that the requested replenishment is not complete.
     *
     * @throws TaskCommandException - task exception
     */
    protected void validateReplenishmentNotComplete()
        throws TaskCommandException {
        if (getReceivedReplenishment().getStatus().equals(
            ReplenishStatus.Complete)) {
            // Throw replenishment already complete error
            throw new TaskCommandException(
                TaskErrorCode.REPLEN_ALREADY_COMPLETE, getReplenishmentNumber());
        }
    }

    /**
     * Creates the new replenishment detail for license.
     *
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     */
    protected void createNewDetail() throws DataAccessException, BusinessRuleException {

        ReplenishDetailStatus detailStatus = ReplenishDetailStatus.toEnum(getReplenishIndicator());
        Replenishment receivedReplen = this.getReceivedReplenishment();
        ReplenishmentDetail detail = this.getNewDetail();

        // Note that even if we cancel the replenishment, we set the location to the
        // ToLocation because the task does not send the location in this case.
        if (detailStatus == ReplenishDetailStatus.Replenished) {
            detail.setStatus(ReplenishDetailStatus.Replenished);
            detail.setReplenishLocation(getLocationManager().get(getLocationPkAsLong()));
        } else if (detailStatus == ReplenishDetailStatus.Canceled)  {
            detail.setStatus(ReplenishDetailStatus.Canceled);
            detail.setReplenishLocation(receivedReplen.getToLocation());
        } else if (detailStatus == ReplenishDetailStatus.Partial) {
            detail.setStatus(ReplenishDetailStatus.Replenished);
            detail.setReplenishLocation(getLocationManager().get(getLocationPkAsLong()));
        }

        if (detailStatus == ReplenishDetailStatus.Canceled) {
            detail.setQuantity(0);
        } else {
            detail.setQuantity(getQuantityPut());

        }
        detail.setOperator(getOperator());
        detail.setStartTime(getStartTimeAsDate());
        detail.setReplenishTime(this.getCommandTime());
        detail.setReplenishment(receivedReplen);

        // add the reason code if any.
        if (StringUtil.isNullOrEmpty(this.getReasonCode())) {
            detail.setReason(null);
        } else {
            detail.setReason(this.getReasonCodeManager().findByReasonNumber(
                this.getReasonCodeAsInteger()));
        }
        // add to replenishment detail count and add detail record for this replenishment.
        receivedReplen.setDetailCount(receivedReplen.getDetailCount() + 1);
        receivedReplen.getReplenishDetails().add(detail);

        this.setNewDetail(detail);
        getReplenishmentDetailManager().save(getNewDetail());
    }

    /**
     * Updates the main replenishment.
     * @throws DataAccessException - on db exception
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void updateMainReplenishment() throws DataAccessException, BusinessRuleException {

        Replenishment receivedReplen = getReceivedReplenishment();
        ReplenishDetailStatus detailStatus = ReplenishDetailStatus.toEnum(getReplenishIndicator());

        if (receivedReplen.getStartTime() == null) {
            receivedReplen.setStartTime(getStartTimeAsDate());
        }

        if (detailStatus == ReplenishDetailStatus.Replenished) {
            receivedReplen.setStatus(ReplenishStatus.Complete);
            receivedReplen.setEndTime(this.getCommandTime());
            // count the completed replenishment.
            this.getLaborManager()
                 .updateReplenishmentLaborStatistics(getOperator(), receivedReplen);
        } else if (detailStatus == ReplenishDetailStatus.Canceled) {
            // If detail count is zero then there were no prior partials so nothing
            // was actually put. The assignment status should be canceled.
            if (receivedReplen.getDetailCount() == 1) {
                receivedReplen.setStatus(ReplenishStatus.Canceled);
                receivedReplen.setTotalReplenishedQuantity(0);
            } else {
                receivedReplen.setStatus(ReplenishStatus.Complete);
           }
           receivedReplen.setEndTime(this.getCommandTime());
        }

        if (detailStatus == ReplenishDetailStatus.Replenished
            || detailStatus == ReplenishDetailStatus.Partial) {
            receivedReplen.setTotalReplenishedQuantity(receivedReplen.getTotalReplenishedQuantity()
                                                       + getQuantityPut());
        }
        receivedReplen.setOperator(getOperator());
    }

    /**
     * Update location as being replenished
     *
     * Updates the locations.
     * @throws DataAccessException - Database Locations
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void updateLocations()
    throws DataAccessException, BusinessRuleException {

        // Report the location that was actually replenished
        Replenishment receivedReplen = this.getReceivedReplenishment();
        if ((receivedReplen.getType() == ReplenishType.Replenishment) && (getQuantityPut() > 0)) {

            Location l = null;
            // if the replenishment has been canceled, mark the toLocation as replenished.
            // otherwise mark the location that was passed as replenished.
            if (ReplenishDetailStatus.toEnum(getReplenishIndicator()) == ReplenishDetailStatus.Canceled) {
                l =  this.getReceivedReplenishment().getToLocation();
            } else {
               l = getLocationManager().get(getLocationPkAsLong());
            }
            try {
                getLocationManager().reportReplenished(l, receivedReplen.getItem());

            } catch (BusinessRuleException e) {
                if (e.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
                    // Do nothing --
                    // we don't want the device to know about this
                    // LocationManager will have posted a Notificaiton
                    // about this.
                } else {
                    throw e;
                }
            }
        }

        //      If replenishment is complete then report as completed
        if (receivedReplen.getStatus() == ReplenishStatus.Complete) {
            try {
                getLocationManager().completeReplenishment(receivedReplen.getToLocation(),
                                                           receivedReplen.getItem());
            } catch (BusinessRuleException e) {
                if (e.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
                    // Do nothing --
                    // we don't want the device to know about this
                    // LocationManager will have posted a Notificaiton
                    // about this.
                } else {
                    throw e;
                }
            }
        }
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        // Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build Response Record.
     *
     * @return - return response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();

        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 