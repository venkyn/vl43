/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.ReplenishType;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.task.command.BaseReplenishmentTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 *
 * @author sfahnestock
 */
public class GetReplenishmentCmdRoot extends BaseReplenishmentTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private Replenishment       operatorReplenishment;

    private LocationManager     locationManager;


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }


    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }


    /**
     * Getter for the operatorReplenishment property.
     * @return Replenishment value of the property
     */
    public Replenishment getOperatorReplenishment() {
        return operatorReplenishment;
    }


    /**
     * Setter for the operatorReplenishment property.
     * @param operatorReplenishment the new operatorReplenishment value
     */
    public void setOperatorReplenishment(Replenishment operatorReplenishment) {
        this.operatorReplenishment = operatorReplenishment;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        findOperatorReplenishment();
        buildResponse();
        return getResponse();
    }

    /**
     * Finds the operator's top replenishment.
     *
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     * @throws BusinessRuleException - Business rule exceptions
     */
    protected void findOperatorReplenishment()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);
        List<Replenishment> replenishments =
            getReplenishmentManager().
                listOperatorReplenishment(queryDecorator, getOperator().getId());

        //If no replenishments are found
        if (replenishments.isEmpty()) {
            int count = getReplenishmentManager().countOperatorReplenishments(getOperator().getId());
            if (count > 0) {
                //Throw work in another region exception
                throw new TaskCommandException(TaskErrorCode.WORK_IN_OTHER_REGION);
            } else {
                //Throw no work at all exception
                throw new TaskCommandException(TaskErrorCode.NO_WORK_AVAILABLE);
            }
        } else {
            //Set the return replenishment and update
            setOperatorReplenishment(replenishments.get(0));
            getOperatorReplenishment().setOperator(getOperator());
            getOperatorReplenishment().setStatus(ReplenishStatus.InProgress);
            getOperatorReplenishment().setStartTime(this.getCommandTime());
            if (getOperatorReplenishment().getType().equals(ReplenishType.Replenishment)) {
                try {
                    getLocationManager().startReplenishment(
                        getOperatorReplenishment().getToLocation(),
                        getOperatorReplenishment().getItem());
                } catch (BusinessRuleException e) {
                    if (e.getErrorCode()
                        == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
                        // Do nothing --
                        // we don't want the device to know about this
                        // LocationManager will have posted a Notificaiton
                        // about this.
                    } else {
                        throw e;
                    }
                }
            }

        }
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord(getOperatorReplenishment()));
    }

    /**
     * Build Response Record.
     *
     * @param r - the replenishment to send
     * @return - return response record
     * @throws DataAccessException - Dtaabase Execeptions
     */
    protected ResponseRecord buildResponseRecord(Replenishment r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("replenishmentNumber", r.getNumber());
        record.put("license", r.getLicenseNumber());
        record.put("promptLicense", r.isPromptLicense());
        record.put("regionNumber", r.getRegion().getNumber());
        record.put("itemNumber", r.getItem().getNumber());
        record.put("itemDescription",
            getTranslatedItemPhoneticDescription(r.getItem()));
        record.put("quantity", r.getReplenishQuantity());
        record.put("fromLocationPreAisle",
                   translateUserData(r.getFromLocation().getPreAisle()));
        record.put("fromLocationAisle", r.getFromLocation().getAisle());
        record.put("fromLocationPostAisle",
                   translateUserData(r.getFromLocation().getPostAisle()));
        record.put("fromLocationSlot", r.getFromLocation().getSlot());
        if (!StringUtil.isNullOrEmpty(r.getSpokenValidation())) {
            record.put("replenSpokenValidation", r.getSpokenValidation());
        } else if (!r.getFromLocation().getCheckDigits().isEmpty()) {
                record.put("replenSpokenValidation", r.getFromLocation().getCheckDigits());
            } else {
                record.put("replenSpokenValidation", r.getFromLocation().getSpokenVerification());
        }
        if (!StringUtil.isNullOrEmpty(r.getScannedValidation())) {
            record.put("replenScannedValidation", r.getScannedValidation());
        } else {
            record.put("replenScannedValidation", r.getFromLocation().getScannedVerification());
        }
        record.put("toLocationPreAisle",
                   translateUserData(r.getToLocation().getPreAisle()));
        record.put("toLocationAisle", r.getToLocation().getAisle());
        record.put("toLocationPostAisle",
                   translateUserData(r.getToLocation().getPostAisle()));
        record.put("toLocationSlot", r.getToLocation().getSlot());
        record.put("toLocationCheckDigits", r.getToLocation().getCheckDigits());
        record.put("toLocationScannedLocation", r.getToLocation().getScannedVerification());
        record.put("toLocationPk", r.getToLocation().getId());
        record.put("goalTime", r.getGoalTime());

        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 