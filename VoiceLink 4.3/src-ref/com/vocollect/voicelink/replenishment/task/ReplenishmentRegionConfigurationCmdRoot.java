/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.task.command.BaseReplenishmentTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentRegionConfigurationCmdRoot
    extends BaseReplenishmentTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private ReplenishmentRegionManager  replenishmentRegionManager;

    private List<ReplenishmentRegion>   returnRegions = new ArrayList<ReplenishmentRegion>();

    private LaborManager                laborManager;


    /**
     * Getter for the returnRegions property.
     * @return List&lt;ReplenishmentRegion&gt; value of the property
     */
    public List<ReplenishmentRegion> getReturnRegions() {
        return returnRegions;
    }


    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<ReplenishmentRegion> returnRegions) {
        this.returnRegions = returnRegions;
    }


    /**
     * Getter for the replenishmentRegionManager property.
     * @return ReplenishmentRegionManager value of the property
     */
    public ReplenishmentRegionManager getReplenishmentRegionManager() {
        return replenishmentRegionManager;
    }


    /**
     * Setter for the replenishmentRegionManager property.
     * @param replenishmentRegionManager the new replenishmentRegionManager value
     */
    public void setReplenishmentRegionManager(ReplenishmentRegionManager replenishmentRegionManager) {
        this.replenishmentRegionManager = replenishmentRegionManager;
    }


    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }


    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildReturnRegionList();
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.REGIONS_NO_LONGER_VALID);
        }
        setOperatorLastLocation();
        buildResponse();
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                                this.getOperator(), OperatorLaborActionType.Replenishment);
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return

        for (ReplenishmentRegion r : getReturnRegions()) {
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build response record.
     *
     * @param r - region to build record for
     * @return - response record.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(ReplenishmentRegion r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        record.put("allowCancelLicense", r.isAllowCancelLicense());
        record.put("allowOverrideLocation", r.isAllowOverrideLocation());
        record.put("allowOverridePickUpQty", r.isAllowOverridePickUpQty());
        record.put("allowPartialPut", r.isAllowPartialPut());
        record.put("capturePickUpQty", r.isCapturePickUpQty());
        record.put("capturePutQty", r.isCapturePutQty());
        record.put("licDigitsTaskSpeaks", r.getLicDigitsTaskSpeaks());
        record.put("locDigitsOperSpeak", r.getLocDigitsOperSpeaks());
        record.put("checkDigitsOperSpeak", r.getCheckDigitsOperSpeaks());
        record.put("exceptionLocation", translateUserData(r.getExceptionLocation()));
        record.put("verifySpokenLicenseLocation", r.isVerifySpokenLicenseLocation());
        return record;
    }

    /**
     * Build a list of regions to return.
     *
     * @throws DataAccessException - Database exception
     */
    protected void buildReturnRegionList() throws DataAccessException {
        Set<Region> regions = getOperator().getRegions();

        for (Region r : regions) {
            if (r.getType().equals(RegionType.Replenishment)) {
                getReturnRegions().add((ReplenishmentRegion) r);
            }
        }
    }

    /**
     * Sets the operators last location to null.
     *
     * @throws DataAccessException - database exception
     */
    protected void setOperatorLastLocation() throws DataAccessException {
        getOperator().setLastLocation(null);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 