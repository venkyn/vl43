/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.task.command.BaseReplenishmentTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 *
 * @author sfahnestock
 */
public class RequestReplenishmentRegionCmdRoot extends BaseReplenishmentTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private boolean               allRegions;

    private String                regionNumber;

    private RegionManager         regionManager;

    private ReplenishmentRegionManager  replenishmentRegionManager;

    /**
     * Getter for the replenishmentRegionManager property.
     * @return ReplenishmentRegionManager value of the property
     */
    public ReplenishmentRegionManager getReplenishmentRegionManager() {
        return replenishmentRegionManager;
    }



    /**
     * Setter for the replenishmentRegionManager property.
     * @param replenishmentRegionManager the new replenishmentRegionManager value
     */
    public void setReplenishmentRegionManager(ReplenishmentRegionManager replenishmentRegionManager) {
        this.replenishmentRegionManager = replenishmentRegionManager;
    }


    /**
     * Getter for the allRegions property.
     * @return boolean value of the property
     */
    public boolean getAllRegions() {
        return allRegions;
    }


    /**
     * Setter for the allRegions property.
     * @param allRegions the new allRegions value
     */
    public void setAllRegions(boolean allRegions) {
        this.allRegions = allRegions;
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public Integer getRegionNumberInt() {
        if (!StringUtil.isNullOrEmpty(getRegionNumber())) {
            return Integer.valueOf(getRegionNumber());
        } else {
            return null;
        }
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public String getRegionNumber() {
        return regionNumber;
    }


    /**
     * Setter for the regionNumber property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(String regionNumber) {
        this.regionNumber = regionNumber;
    }


    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }



    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }




    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        validateAndSaveRequest();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * Validate and signs operator into regions.
     *
     * @throws DataAccessException - Database excpetions
     * @throws TaskCommandException - Task Command Exceptions
     */
    protected void validateAndSaveRequest()
    throws DataAccessException, TaskCommandException {
        ReplenishmentRegion       region;
        //Get a list of all regions operator is authorized for
        List<Region> operatorAuthorizedRegions =
            getRegionManager().listAuthorized(
                TaskFunctionType.Replenishment, getOperator().getWorkgroup().getId());

        //if no authorized regions available for operator
        if (operatorAuthorizedRegions.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NO_AUTHORIZED_REGIONS, getOperator().getId());
        }

        //If region parameter is not null
        if (!(getRegionNumber().equals(""))) {
            //Get the region requested
            region = this.getReplenishmentRegionManager().findRegionByNumber(getRegionNumberInt());
            //If region specified is not found
            if (region == null) {
                throw new TaskCommandException(TaskErrorCode.REGION_NOT_FOUND, getRegionNumber());
            }
            //If region found but not in authorized regions
            if (!(operatorAuthorizedRegions.contains(region))) {
                throw new TaskCommandException(TaskErrorCode.NOT_AUTH_FOR_REGION, getRegionNumber());
            }
            //Sign into region
            getOperator().getRegions().add(region);
        }

        //If all regions requested
        if (getAllRegions()) {
            // Sign operator into all replenishment regions they are authorized for
            List<ReplenishmentRegion> allReplenRegions = getReplenishmentRegionManager().getAll();
            // There is no listAuthorized() function for the ReplenishmentRegionManager. Therefore, we
            // must get all replenishment regions and loop through to check if operator is
            // authorized for each region.
            for (Region r : allReplenRegions) {
                if (operatorAuthorizedRegions.contains(r)) {
                    getOperator().getRegions().add(r);
                }
            }
            //If no REPLENISHMENT regions are authorized for operator
            if (getOperator().getRegions().isEmpty()) {
                throw new TaskCommandException(TaskErrorCode.NO_AUTHORIZED_REPLEN_REGIONS, getOperator().getId());
            }
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 