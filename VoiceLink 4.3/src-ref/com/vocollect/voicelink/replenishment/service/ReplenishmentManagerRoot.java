/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * 
 * @author sfahnestock
 */
public interface ReplenishmentManagerRoot extends
    GenericManager<Replenishment, ReplenishmentDAO>, DataProvider {

    /**
     * retrieves a list of replenishments for the operator.
     * 
     * @param operatorId - the operator to find the list for
     * @param queryDecorator - extra query instructions
     * @return list of requested replenishments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listOperatorReplenishment(QueryDecorator queryDecorator,
                                                  Long operatorId)
        throws DataAccessException;

    /**
     * Get count of replenishments in other regions.
     * 
     * @param operatorId - the operator
     * @return - the count of replenishments in other regions
     * @throws DataAccessException - database exception
     */
    int countOperatorReplenishments(Long operatorId) throws DataAccessException;

    /**
     * Get count of replenishments in other regions.
     * 
     * @param replenNumber - the number to search by
     * @return - the replenishment, may be null
     * @throws DataAccessException - database exception
     */
    Replenishment findReplenishmentByNumber(String replenNumber)
        throws DataAccessException;

    /**
     * Find next replenishment for a location.
     * 
     * @param queryDecorator - query decorator to select top1
     * @param location - location to find replenishment for
     * @return - list of replenishements for location.
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listReplenishmentByLocation(QueryDecorator queryDecorator,
                                                    Location location)
        throws DataAccessException;

    /**
     * Find next replenishment for a location/item combination.
     * 
     * @param queryDecorator - query decorator to select top1
     * @param location - location to find replenishment for
     * @param item - item to find replenishment for
     * @return - list of replenishements for location.
     * @throws DataAccessException - database exceptions
     */
    List<Replenishment> listReplenishmentByLocationAndItem(QueryDecorator queryDecorator,
                                                           Location location,
                                                           Item item)
        throws DataAccessException;

    /**
     * Find the next replenishmentto issue out for a location or location/item.
     * 
     * @param location - location to get replenishment for
     * @param item - item to get replenishment for
     * @return - return replenishment if found otherwise null
     * @throws DataAccessException - database exceptions
     */
    public Replenishment findNextReplenishment(Location location, Item item)
        throws DataAccessException;

    /**
     * Get the summary counts of replenishment assignments.
     * 
     * @param rdi - ResultDataInfo object.
     * @return - return list of summary information
     * @throws DataAccessException - database exceptions
     */
    List<DataObject> listAssignmentCountsByRegionAndStatus(ResultDataInfo rdi)
        throws DataAccessException;

    /**
     * Update the status of the replenishment assignment.
     * 
     * @param replenishment - replenishment assignment to change
     * @param status - the status to change to.
     * 
     * @throws BusinessRuleException - if status can not be changed
     * @throws DataAccessException - on database exceptions
     */
    public void executeUpdateStatus(Replenishment replenishment,
                                    ReplenishStatus status)
        throws BusinessRuleException, DataAccessException;

    /**
     * get list of assignments available to resequence from the given region.
     * 
     * @param regionId - id of the region to resequence
     * @return - return list of assignments
     * @throws DataAccessException - database exceptions
     */
    public List<DataObject> listResequenceAssignmentsByRegion(Long regionId)
        throws DataAccessException;

    /**
     * Resequence the list of replenishment items for the given region.
     * 
     * @param resequenceList - list of assignments to resequence
     * @param regionId - the region that contains the assignments.
     * @throws DataAccessException - on Database execption
     * @throws BusinessRuleException - on Business rule execption
     */
    public void executeResequence(ArrayList<Long> resequenceList, Long regionId)
        throws DataAccessException, BusinessRuleException;

    /**
     * Gets all Replenishments older than the date specified.
     * 
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the ReplenishmentStatus used in the query
     * @return a list of Replenishments
     * @throws DataAccessException Database failure
     */
    public List<Replenishment> listOlderThan(QueryDecorator decorator,
                                             Date date,
                                             ReplenishStatus status)
        throws DataAccessException;

    /**
     * @param operator - the operator
     * @return list of replenishment records that are inprogress for the
     *         operator
     * @throws DataAccessException on any database exception
     */
    public List<Replenishment> listActiveReplenishment(Operator operator)
        throws DataAccessException;

    /**
     * @param operator - the operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of Inprogress or Complete replenishment records for the
     *         operator
     * @throws DataAccessException on any database exception
     */
    public List<Replenishment> listMostRecentRecordsByOperator(Operator operator,
                                                               Date startDate,
                                                               Date endDate)
        throws DataAccessException;

    /**
     * @param operator - the operator
     * @param startDate - start of date range
     * @return list of Inprogress or Complete replenishment records for the
     *         operator
     * @throws DataAccessException on any database exception
     */
    public List<Replenishment> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                                    Date startDate)
        throws DataAccessException;

    /**
     * Purges assignments based on date and status.
     * 
     * @param decorator - query decorator
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of assignments purged
     */
    public int executePurge(QueryDecorator decorator,
                            ReplenishStatus status,
                            Date olderThan);

    /**
     * Create a "canceled" detail record for the given replenishment.
     * 
     * @param replenishment - the replenishment that is being canceled
     * 
     * @throws BusinessRuleException - on business rule exception
     * @throws DataAccessException - on database access error
     */
    public void executeUpdateReplenishment(Replenishment replenishment)
        throws BusinessRuleException, DataAccessException;

    /**
     * Get Replenishment Labor Summary records
     * 
     * @param operatorId the operator ID
     * @param startDate the start date
     * @param endDate the end date
     * @return List<Object[]>
     * @throws DataAccessException
     */

    List<Object[]> listLaborSummaryReportRecords(Long operatorId,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;

    
    /**
     *  
     * @param operatorId
     * @param startDate
     * @param endDate
     * @return
     * @throws DataAccessException
     */
    
    List<Object[]> listLaborDetailReportRecords(Long operatorId,
            Date startDate, Date endDate) throws DataAccessException;
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 