/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.voicelink.replenishment.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentImportDataDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishmentImportData;
import com.vocollect.voicelink.replenishment.service.ReplenishmentImportDataManager;

import java.util.List;


/**
 * Additional service methods for the <code>ReplenishmentImportData</code> model object.
 * @author jtauberg
 */
public abstract class ReplenishmentImportDataManagerImplRoot extends
        GenericManagerImpl<ReplenishmentImportData, ReplenishmentImportDataDAO>
        implements ReplenishmentImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentImportDataManagerImplRoot(ReplenishmentImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     */
    public List<ReplenishmentImportData> listReplenishmentBySiteName(String siteName)
        throws DataAccessException {
            return getPrimaryDAO().listReplenishmentBySiteName(siteName);
    }

    /**
     *  {@inheritDoc}
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 