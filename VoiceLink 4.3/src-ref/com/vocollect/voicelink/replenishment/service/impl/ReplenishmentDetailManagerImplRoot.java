/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentDetailDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;

import java.util.Date;
import java.util.List;


/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentDetailManagerImplRoot
    extends GenericManagerImpl<ReplenishmentDetail, ReplenishmentDetailDAO>
    implements ReplenishmentDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentDetailManagerImplRoot(ReplenishmentDetailDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
      protected List<DataObject> getAllData(ResultDataInfo rdi) throws DataAccessException {
            return this.getPrimaryDAO().
            listReplenishmentDetails(new QueryDecorator(rdi));
    }

      /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public List<ReplenishmentDetail> listMostRecentRecordsByOperator(Operator operator,
                                                                     Date startDate)
      throws DataAccessException {
          return this.getPrimaryDAO().listMostRecentRecordsByOperator(operator, startDate);
      }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 