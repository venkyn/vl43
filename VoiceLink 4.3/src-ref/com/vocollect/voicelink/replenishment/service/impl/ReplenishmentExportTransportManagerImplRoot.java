/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.replenishment.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentDAO;
import com.vocollect.voicelink.replenishment.service.ReplenishmentExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;


/**
 * @author jtauberg
 *
 */
public abstract class ReplenishmentExportTransportManagerImplRoot extends
    ReplenishmentManagerImpl implements ReplenishmentExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentExportTransportManagerImplRoot(ReplenishmentDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * {@inheritDoc}
     * @return list of maps.  Each map contains the fields for completed replenishment,
     *         the object (where obj.status=3 Completed).
     * @throws DataAccessException
     */
    public List<Map<String, Object>> listCompletedReplenishments()
    throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        return formatDatesInListOfMap(
            getPrimaryDAO().listCompletedReplenishments(qd));
    }


    /**
     * Formats the date.
     * @param inputList in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>> formatDatesInListOfMap(List<Map<String, Object>> inputList) {
        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map <String, Object> m : inputList) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if (null == myObj) {
                    continue;
                }
                if (myObj instanceof java.util.Date) {
                    //It is a date!
                    if (myObj == null) {
                        // If null replace with "No Timestamp"
                        formattedDateTime = "No Timestamp";
                    } else {
                        //it isn't null so format as Vocollect yyyyMMddHHmmss
                        formattedDateTime = sdf.format(myObj);
                    } // end if null
                    //Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } //end if class is date
            } // end for obj
            if (((m.get("PutStatus")).toString()).equals("Canceled")) {
                m.put("PutLocation", null);
            }
        } //end for map
        return inputList;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 