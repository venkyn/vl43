/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.replenishment.ReplenishmentErrorCode;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentRegionDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

/**
 *
 *
 * @author sfahnestock
 */
public class ReplenishmentRegionManagerImplRoot
    extends GenericManagerImpl<ReplenishmentRegion, ReplenishmentRegionDAO>
    implements ReplenishmentRegionManager {

    private WorkgroupManager workgroupManager;

    private RegionManager regionManager;

    private OperatorDAO operatorDAO;

    /**
     * Getter for the regionManager property.
     *
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the regionManager property.
     *
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the workgroupManager property.
     *
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     *
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * @return the operatorDAO
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }

    /**
     * @param operatorDAO the operatorDAO to set
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

    /**
     * Constructor.
     *
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentRegionManagerImplRoot(ReplenishmentRegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(ReplenishmentRegion region)
        throws BusinessRuleException, DataAccessException {

        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the replenishment task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        }

        return super.save(region);
    }

    /**
     * Take the passed in Replenishment Region and add it to all the workgroups
     * where AutoAddRegions is turned on and the task function is a
     * replenishment task function.
     *
     * @param region - a replenishment region
     * @throws DataAccessException - any database exception
     */
    protected void addToWorkgroups(ReplenishmentRegion region)
        throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.Replenishment) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public ReplenishmentRegion findRegionByNumber(int regionNumber)
        throws DataAccessException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }

    /**
     *
     * @return the average of the goal rates for all replenishment regions
     * @throws DataAccessException - database failure
     */
    public Double avgGoalRate() throws DataAccessException {
        return getPrimaryDAO().avgGoalRate();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManagerRoot#uniquenessByName(java.lang.String)
     */
    public Long uniquenessByName(String regionName) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByName(regionName);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManagerRoot#uniquenessByNumber(int)
     */
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByNumber(regionNumber);
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.replenishment.model.ReplenishmentRegion)
     */
    public void executeValidateEditRegion(ReplenishmentRegion region)
        throws DataAccessException, BusinessRuleException {
        // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            if (!verifyNoOperatorsSignedIn(region)) {
                throw new BusinessRuleException(
                    ReplenishmentErrorCode.REGION_OPERATORS_SIGNED_IN,
                    new UserMessage(
                        "replenishmentRegion.edit.error.operatorsSignedIn"));
            }
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManagerRoot#executeValidateDeleteRegion(com.vocollect.voicelink.replenishment.model.ReplenishmentRegion)
     */
    public void executeValidateDeleteRegion(ReplenishmentRegion region)
        throws DataAccessException, BusinessRuleException {
        // verify that no operators are signed into the region before deleteing
        if (!verifyNoOperatorsSignedIn(region)) {
            throw new BusinessRuleException(ReplenishmentErrorCode.REGION_OPERATORS_SIGNED_IN,
                new UserMessage("replenishmentRegion.delete.error.operatorsSignedIn"));
          }
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(ReplenishmentRegion instance)
        throws BusinessRuleException, DataAccessException {
        try {
            executeValidateDeleteRegion(instance);
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("replenishmentRegion.delete.error.inUse");
            } else {
                throw ex;
        }
    }
        return null;
    }


    /**
     * There can be no operators signed into the region when editing or
     * deleting.
     *
     * @param region - to be persisted or deleted
     * @return true if no operators are signed into region otherwise return
     *         false
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected boolean verifyNoOperatorsSignedIn(ReplenishmentRegion region)
        throws BusinessRuleException, DataAccessException {

        if (this.getOperatorDAO().
                countNumberOfOperatorsSignedIn(region).intValue() > 0) {
            return false;
        }
        return true;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 