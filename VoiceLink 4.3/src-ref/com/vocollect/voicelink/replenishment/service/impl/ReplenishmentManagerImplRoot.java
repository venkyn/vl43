/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.ResequenceManager;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.replenishment.ReplenishmentErrorCode;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentAssignmentSummaryDAO;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishDetailStatus;
import com.vocollect.voicelink.replenishment.model.ReplenishPriority;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentAssignmentSummary;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 * @author sfahnestock
 */
public class ReplenishmentManagerImplRoot
    extends GenericManagerImpl<Replenishment, ReplenishmentDAO>
    implements ReplenishmentManager {

    private static final int REGION = 0;
    private static final int TOTAL_REPLENISHMENTS = 1;
    private static final int IN_PROGRESS_REPLENISHMENTS = 2;
    private static final int AVAILABLE_REPLENISHMENTS = 3;
    private static final int COMPLETED_REPLENISHMENTS = 4;
    private static final int NON_COMPLETED_REPLENISHMENTS = 5;

    // this is here so we do not have to have a seperate manager for the summary method.
    private ReplenishmentAssignmentSummaryDAO   replenishmentAssignmentSummaryDAO;
    private ReplenishmentDetailManager          replenishmentDetailManager;
    private ResequenceManager                   resequenceManager;


    // Declare and instantiate a logger
    private static final Logger log = new Logger(VoiceLinkPurgeArchiveManagerImpl.class);


    /**
     * Getter for the replenishmentDetailManager property.
     * @return ReplenishmentDetailManager value of the property
     */
    public ReplenishmentDetailManager getReplenishmentDetailManager() {
        return this.replenishmentDetailManager;
    }


    /**
     * Setter for the replenishmentDetailManager property.
     * @param replenishmentDetailManager the new replenishmentDetailManager value
     */
    public void setReplenishmentDetailManager(ReplenishmentDetailManager replenishmentDetailManager) {
        this.replenishmentDetailManager = replenishmentDetailManager;
    }

    /**
     * Getter for the resequenceManager property.
     * @return ResequenceManager value of the property
     */
    public ResequenceManager getResequenceManager() {
        return this.resequenceManager;
    }

    /**
     * Setter for the resequenceManager property.
     * @param resequenceManager the new resequenceManager value
     */
    public void setResequenceManager(ResequenceManager resequenceManager) {
        this.resequenceManager = resequenceManager;
    }

    /**
     * Getter for the replenishmentAssignmentSummaryDAO property.
     * @return ReplenishmentAssignmentSummaryDAO value of the property
     */
    public ReplenishmentAssignmentSummaryDAO getReplenishmentAssignmentSummaryDAO() {
        return this.replenishmentAssignmentSummaryDAO;
    }


    /**
     * Setter for the replenishmentAssignmentSummaryDAO property.
     * @param replenishmentAssignmentSummaryDAO the new replenishmentAssignmentSummaryDAO value
     */
    public void setReplenishmentAssignmentSummaryDAO(
                   ReplenishmentAssignmentSummaryDAO replenishmentAssignmentSummaryDAO) {
        this.replenishmentAssignmentSummaryDAO = replenishmentAssignmentSummaryDAO;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentManagerImplRoot(ReplenishmentDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get list of operators replenishments.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ReplenishmentManager#listOperatorReplenishments(java.lang.String)
     */
    public List<Replenishment> listOperatorReplenishment(QueryDecorator queryDecorator,
                                                         Long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listOperatorReplenishment(queryDecorator, operatorId);
    }

    /**
     * Implementation to get count of operators replenishments in other regions.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ReplenishmentManager#listOperatorReplenishments(java.lang.String)
     */
    public int countOperatorReplenishments(Long operatorId) throws DataAccessException {
        return convertNumberToInt(getPrimaryDAO().countOperatorReplenishments(operatorId));
    }

    /**
     * Implementation to get the replenishment by number.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ReplenishmentManager#findReplenishmentByNumber(java.lang.String)
     */
    public Replenishment findReplenishmentByNumber(String operatorId) throws DataAccessException {
        return getPrimaryDAO().findReplenishmentByNumber(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(Replenishment instance) throws BusinessRuleException, DataAccessException {

        //If adding a new item see if status of location should be changed.
        if (instance.isNew()) {
            if (instance.getToLocation().getStatus(instance.getItem()) == LocationStatus.Empty) {
                instance.getToLocation().setStatus(LocationStatus.Pending, instance.getItem());
                instance.setPriority(ReplenishPriority.High);
            }
        }
        return super.save(instance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(List<Replenishment> instances) throws BusinessRuleException, DataAccessException {

        for (Replenishment instance : instances) {
            //If adding a new item see if status of location should be changed.
            if (instance.isNew()) {
                if (instance.getToLocation().getStatus(instance.getItem()) == LocationStatus.Empty) {
                    instance.getToLocation().setStatus(LocationStatus.Pending, instance.getItem());
                    instance.setPriority(ReplenishPriority.High);
                }
            }
        }
        this.getPrimaryDAO().save(instances);

        boolean newObject = false;
        for (Replenishment instance : instances) {
            if (instance.getSequenceNumber() == -1L) {
                instance.setSequenceNumber(instance.getId());
                newObject = true;
            }
        }

        if (newObject) {
            this.getPrimaryDAO().save(instances);
        }

        return instances;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#findReplenishmentByLocation(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.core.model.Location)
     */
    public List<Replenishment> listReplenishmentByLocation(
        QueryDecorator queryDecorator,
        Location location) throws DataAccessException {
        return getPrimaryDAO().listReplenishmentByLocation(queryDecorator,
            location);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#findReplenishmentByLocationAndItem(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public List<Replenishment> listReplenishmentByLocationAndItem(QueryDecorator queryDecorator,
                                                                  Location location, Item item)
                                                                  throws DataAccessException {
        return getPrimaryDAO().listReplenishmentByLocationAndItem(queryDecorator, location, item);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#findNextReplenishment(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public Replenishment findNextReplenishment(Location location, Item item)
                                               throws DataAccessException {

        List<Replenishment> replens = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        if (location.getItems().size() == 0) {
            replens = listReplenishmentByLocation(queryDecorator, location);
        } else {
            replens = listReplenishmentByLocationAndItem(queryDecorator, location, item);
        }

        if (replens == null) {
            return null;
        } else if (replens.size() == 0) {
            return null;
        } else {
            return replens.get(0);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listAssignmentCountsByRegionAndStatus()
     */
    public List<DataObject> listAssignmentCountsByRegionAndStatus(ResultDataInfo rdi) throws DataAccessException {
        Object[] queryArgs = rdi.getQueryArgs();
        // Retrieve it all.
        List<Object[]> data = this.getReplenishmentAssignmentSummaryDAO()
                                  .listAssignmentCountsByRegionAndStatus((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            ReplenishmentAssignmentSummary summaryObject = new ReplenishmentAssignmentSummary();
            summaryObject.setRegion((ReplenishmentRegion) objArray[REGION]);
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            Site theSite =  siteContext.getSite(summaryObject.getRegion());
            summaryObject.setSite(theSite);
            summaryObject.setTotalAssignments(
                convertNumberToInt(objArray[TOTAL_REPLENISHMENTS]));
            summaryObject.setInProgressAssignments(
                convertNumberToInt(objArray[IN_PROGRESS_REPLENISHMENTS]));
            summaryObject.setAvailableAssignments(
                convertNumberToInt(objArray[AVAILABLE_REPLENISHMENTS]));
            summaryObject.setCompletedAssignments(
                convertNumberToInt(objArray[COMPLETED_REPLENISHMENTS]));
            summaryObject.setNonCompletedAssignments(
                convertNumberToInt(objArray[NON_COMPLETED_REPLENISHMENTS]));
            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * Update the status of the replenishment assignment.
     *
     * @param  replenishment - replenishment assignment to change
     * @param  status - the status to change to.
     *
     * @throws BusinessRuleException - if status can not be changed
     * @throws DataAccessException - on database exceptions
     */
    public void executeUpdateStatus(Replenishment replenishment,  ReplenishStatus status)
            throws BusinessRuleException, DataAccessException {

        if ((replenishment.getStatus() == ReplenishStatus.Available)
            || ((replenishment.getStatus() == ReplenishStatus.Unavailable)
            && (replenishment.getStatus() != status))) {
             replenishment.setStatus(status);
        } else  {
            throw new BusinessRuleException(
                ReplenishmentErrorCode.REPLENISHMENT_CANNOT_CHANGE_STATUS,
                    new UserMessage("replenishment.updateStatus.error.cannot.update.status"));
        }
    }

    /**
     * Update the given assignment.
     * This method is to be called by the UI to change the assignment status or
     * to assign an operator to the assignment during the edit function.
     *
     * If the replenishment was canceled, this method will also create a "Canceled"
     * detail record and update the location status to replenished.
     *
     * @param replenishment -  the replenishment that is being canceled
     *
     * @throws BusinessRuleException -  on business rule exception
     * @throws DataAccessException - on database access error
     */
    @SuppressWarnings("unchecked")
    public void executeUpdateReplenishment(Replenishment replenishment)
                                           throws BusinessRuleException, DataAccessException {

        if (replenishment.getStatus() == ReplenishStatus.Canceled) {
            ReplenishmentDetail detail = new ReplenishmentDetail();
            Date theDate = new Date();
            detail.setStartTime(theDate);
            detail.setReplenishTime(theDate);
            detail.setQuantity(0);
            detail.setReason(null);
            detail.setOperator(replenishment.getOperator());
            detail.setReplenishLocation(replenishment.getToLocation());
            detail.setReplenishment(replenishment);
            detail.setExportStatus(ExportStatus.NotExported);
            detail.setStatus(ReplenishDetailStatus.Canceled);
            this.getReplenishmentDetailManager().save(detail);
            if (replenishment.getDetailCount() == 0) {
                List newDetails = new ArrayList<ReplenishmentDetail>();
                replenishment.setReplenishDetails(newDetails);
              }
            replenishment.setDetailCount(replenishment.getDetailCount() + 1);
            replenishment.setStartTime(theDate);
            replenishment.setEndTime(theDate);
            replenishment.getReplenishDetails().add(detail);
        }
        this.save(replenishment);
    }

    /**
     * get list of assignments available to resequence from
     * the given region.
     *
     * @param regionId - id of the region to resequence
     * @return - return list of assignments
     * @throws DataAccessException - database exceptions
     */
    public List<DataObject> listResequenceAssignmentsByRegion(Long regionId) throws DataAccessException {
        return this.getPrimaryDAO().listResequenceAssignmentsByRegion(regionId);
    }

    /**
     * get list of assignments available to resequence from
     * the given region.
     *
     * @param rdi - ResultDataInfo to reqsequence
     * @return - return list of assignments
     * @throws DataAccessException - database exceptions
     */
    @SuppressWarnings("unchecked")
    public List<DataObject> getResequenceData(ResultDataInfo rdi) throws DataAccessException {
        Column col = rdi.getSortColumnObject();
        col.setSortType(ColumnSortType.Normal);
        rdi.setSortColumnObject(col);
        ArrayList<Long> resequenceList = (ArrayList<Long>) (rdi.getQueryArgs()[0]);
        List<DataObject> results = this.getPrimaryDAO().listResequenceAssignments(new QueryDecorator(rdi));
        return getResequenceManager().setupResequenceResults(resequenceList, results);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#executeResequence(java.util.ArrayList, java.lang.Long)
     */

    public void executeResequence(ArrayList<Long> resequenceList, Long regionId)
        throws DataAccessException, BusinessRuleException {
        List<DataObject> results = this.getPrimaryDAO().listResequenceAssignmentsByRegion(regionId);
        getResequenceManager().executeResequence(resequenceList, results, this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date, com.vocollect.voicelink.replenishment.model.ReplenishStatus)
     */
    public List<Replenishment> listOlderThan(QueryDecorator decorator,
        Date date, ReplenishStatus status) throws DataAccessException {
        return getPrimaryDAO().listOlderThan(decorator, date, status);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listActiveReplenishment(java.lang.String)
     */
    public List<Replenishment> listActiveReplenishment(Operator operator)
    throws DataAccessException {
        return this.getPrimaryDAO().listActiveReplenishment(operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public List<Replenishment> listMostRecentRecordsByOperator(Operator operator,
                                                               Date startDate,
                                                               Date endDate)
    throws DataAccessException {
        return this.getPrimaryDAO().listMostRecentRecordsByOperator(operator, startDate, endDate);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public List<Replenishment> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                                        Date startDate)
    throws DataAccessException {
        return this.getPrimaryDAO().listRecordsByOperatorGreaterThanDate(operator, startDate);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.replenishment.model.ReplenishStatus, java.util.Date)
     */
    public int executePurge(QueryDecorator decorator, ReplenishStatus status, Date olderThan) {
        int returnRecords = 0;
        List<Replenishment> replens = null;

        //Get List of Replenishment Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Finding Replenishment Assignments - " + status.toString() + " to Purge :::");
        }
        try {
            replens = listOlderThan(decorator, olderThan, status);
            returnRecords = replens.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting TRANSACTIONAL "
                + "REPLENISHMENT ASSIGNMENTS - " + status.toString() + " from database to purge: " + e
                );
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                            + " Replenishment Assignments - " + status.toString() + " for purge :::");
            log.debug("### Starting Replenishment Purge Process :::");
        }
        for (Replenishment r : replens) {
            try {
                delete(r);
            } catch (Throwable t) {
                log.warn("!!! Error purging transactional replenishment "
                    + r.getNumber() + " from database: " + t
                    , t);
            }
        }

        getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listLaborSummaryReportRecords(java.lang.Long, java.util.Date, java.util.Date)
     */
    public List<Object[]> listLaborSummaryReportRecords(Long operatorId, Date startDate,
            Date endDate) throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(
            operatorId, startDate, endDate);
    }

    public List<Object[]> listLaborDetailReportRecords(Long operatorId, Date startDate, Date endDate) throws DataAccessException{
        
        return this.getPrimaryDAO().listLaborDetailReportRecords(operatorId, startDate, endDate);
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 