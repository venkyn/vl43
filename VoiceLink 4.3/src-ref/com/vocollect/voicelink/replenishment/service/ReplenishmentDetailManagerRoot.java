/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.replenishment.dao.ReplenishmentDetailDAO;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;

import java.util.Date;
import java.util.List;


/**
 *
 *
 * @author sfahnestock
 */
public interface ReplenishmentDetailManagerRoot
    extends GenericManager<ReplenishmentDetail, ReplenishmentDetailDAO>, DataProvider {


    /**
     * @param operator - the operator
     * @param startDate - end date must be greater than start date
     * @return list of Inprogress or Complete replenishment records for the operator
     * @throws DataAccessException on any database exception
     */
    public List<ReplenishmentDetail> listMostRecentRecordsByOperator(Operator operator,
                                                                     Date startDate)
    throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 