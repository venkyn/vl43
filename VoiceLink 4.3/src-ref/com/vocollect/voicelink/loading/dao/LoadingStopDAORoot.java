/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingStop;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author pfunyak
 * 
 */
public interface LoadingStopDAORoot extends GenericDAO<LoadingStop> {

    /**
     * Method to find loading stop
     * @param stopNumber Loading stop number
     * @param route Loading route
     * @return the loading stop object
     * @throws DataAccessException - database failure
     */
    public List<LoadingStop> listStopByNumber(Long stopNumber,
                                              LoadingRoute route)
        throws DataAccessException;
    
    /**
     * Method to list loading stops on route basis. 
     * @param route Loading route
     * @param operatorID operator working on that Route.
     * @return the loading stop object
     * @throws DataAccessException - database failure
     */
    public List<LoadingStop> listStopsByRoute(LoadingRoute route, Long operatorID)
        throws DataAccessException;    
    
    
    /**
     * @param customerNumber The customer number
     * @param route 
     * @param departureDateTime 
     * @param loadingRegionNumber 
     * @return The loading stop for the customer
     */
    LoadingStop findStopByCustomer(String route,
            Integer loadingRegionNumber, Date departureDateTime,
            String customerNumber);
    
    //Following queries are for exports.
    
    /**
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @param qd - query decorator for number of results
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedStops(QueryDecorator qd)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 