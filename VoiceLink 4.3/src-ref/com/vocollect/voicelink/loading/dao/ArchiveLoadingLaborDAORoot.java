/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import java.util.Date;
import java.util.List;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.model.ArchiveLoadingLabor;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;

/**
 * 
 *
 * @author yarora
 */
public interface ArchiveLoadingLaborDAORoot extends GenericDAO<ArchiveLoadingLabor> {
    
    
    /**
     * Get Labor Summary Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
    
    /**
     * Get Labor Detail Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(String operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 