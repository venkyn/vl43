/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.loading.model.ArchiveLoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;

import java.util.Date;

/**
 * @author mraj
 * 
 */
public interface ArchiveLoadingRouteDAORoot extends
    GenericDAO<ArchiveLoadingRoute> {

    /**
     * Gets a list of archive routes based on date and status.
     * @param createdDate date criteria
     * @param status status criteria
     * @return a list of archived routes
     * @throws DataAccessException - database exception
     */
    Integer updateOlderThan(Date createdDate, LoadingRouteStatus status)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 