/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Loading Route DAO Root interface for accessing data layer operations.
 * @author mraj
 * 
 */
public interface LoadingRouteDAORoot extends GenericDAO<LoadingRoute> {

    /**
     * retrieves the loadingRoute of the specified routeID.
     * 
     * @param routeID - value to retrieve by
     * @return loadingRoute object
     * @throws DataAccessException - database exceptions
     */
    LoadingRoute findLoadingRouteFromID(long routeID) 
        throws DataAccessException;
    
    /**
     * Method to fetch routes matching criteria.
     * @param decorator QueryDecorator object
     * @return List of all Loading route objects fetched
     * @throws DataAccessException - dae
     */
    public List<DataObject> listRoutes(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Method to find record matching criteria of route, stop number and region.
     * @param route Lading route
     * @param departureDateTime departureDateTime
     * @param region Loading region
     * @return selected LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listImportBlockingCondition(String route, Date departureDateTime, LoadingRegion region)
        throws DataAccessException;
    
    /**
     * Method to find record matching criteria of route, stop number and region.
     * @param route Lading route
     * @param departureDateTime departureDateTime
     * @param region Loading region
     * @return selected LoadingRoute object
     */
    List<LoadingRoute> listImportBlockingConditionWithStatus(String route,
        Date departureDateTime,
        LoadingRegion region);
    /**
     * Method to get list of loading routes.
     * @param routeNumber Loading route
     * @param regionNumber region number in which operator is signed in
     * @return list of LoadingRoutes
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listLoadingRouteByNumber(String routeNumber, Integer regionNumber)
        throws DataAccessException;
    
    /**
     * Method to get in progress loading route.
     * @param operatorID operator to which the route is assigned.
     * @param regionNumber region number in which operator is signed in
     * @return list of LoadingRoutes
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listInProgressLoadingRoute(Long operatorID, Integer regionNumber)
        throws DataAccessException;
    
    /**
     * Method to get active loading route.
     * @param operatorID operator to which the route is assigned.
     * @return list of LoadingRoutes
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listActiveLoadingRoute(Long operatorID)
        throws DataAccessException;    
    
    /**
     * Method to get Available loading route .
     * @param regionNumber Loading region
     * @return LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listAvailableLoadingRoute(Integer regionNumber)
        throws DataAccessException;

    
    /**
     * @param route - the route to find
     * @param loadingRegionNumber - the region number to find
     * @param departureDateTime - the departure date time to find
     * @param customerNumber - the customer number to find
     * @return Loading Route object found, if any
     * @throws DataAccessException - dae
     */
    public LoadingRoute findLoadingRoute(String route,
                                         Integer loadingRegionNumber,
                                         Date departureDateTime,
                                         String customerNumber)
        throws DataAccessException;
    
    
    //Following queries are for exports.
    
    /**
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @param qd - query decorator for number of results
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedRoutes(QueryDecorator qd)
        throws DataAccessException;

    /** 
     * Get list of labor records to export.
     * 
     * @return the list of labor records to export
     * @param qd - query decorator for number of results
     * @throws DataAccessException - Database failure
     */
    List<Map<String, Object>> listExportLaborRecords(QueryDecorator qd) 
    throws DataAccessException; 

    /**
     * Mark route records as exported.
     *  
     * @param qd - query decorator for number of results
     * @return list of export finish
     * @throws DataAccessException on database error
     */
    List<Map<String, Object>> listExportFinish(QueryDecorator qd) throws DataAccessException; 

    /**
     * Updates Routes that are ready to be purged or archived.
     * 
     * @param purgeable
     *            - whether to mark for archive(1) or purge (2)
     * @param date
     *            - Date to find routes older than
     * @param status
     *            - status of routes to find
     * @return - number of records updated
     * @throws DataAccessException
     *             - database exceptions
     */
    Integer updateOlderThan(Integer purgeable, Date date,
            LoadingRouteStatus status) throws DataAccessException;

    /**
     * Get a specified number of routes to archive.
     * 
     * @param queryDecorator
     *            - query decorator for number of row to retrieve
     * @return - list of routes
     * @throws DataAccessException
     *             - database exceptions
     */
    List<LoadingRoute> listArchive(QueryDecorator queryDecorator)
            throws DataAccessException;
    
    /**
     * Get a specified number of routes to purge.
     * 
     * @param queryDecorator
     *            - query decorator for number of row to retrieve
     * @return - list of routes
     * @throws DataAccessException
     *             - database exceptions
     */
    List<LoadingRoute> listPurge(QueryDecorator queryDecorator)
            throws DataAccessException;
    
    /**
     * Get routes by number and departure date.
     * 
     * @param route - loading route
     * @param departureDateTime - loading departureDateTime
     * @return - list of routes
     * @throws DataAccessException
     *             - database exceptions
     */
    List<LoadingRoute> listRouteByNumberAndDDT(String route, Date departureDateTime)
            throws DataAccessException;
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 