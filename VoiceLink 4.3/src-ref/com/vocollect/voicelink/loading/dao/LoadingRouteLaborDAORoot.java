/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;

import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author ktanneru
 */
public interface LoadingRouteLaborDAORoot extends GenericDAO<LoadingRouteLabor> {
    /**
     * Retrieves the open route labor record for the given route. or
     * returns null if there is no open route labor record.
     * 
     * @param routeId - The Id of the route.
     * @param operator - The operator
     * @return The requested route labor record or null
     * @throws DataAccessException on any failure.
     */
    LoadingRouteLabor findOpenRecordByRouteId(long routeId, Operator operator)
        throws DataAccessException;

    /**
     * Retrieves the open route labor records for the given operator or
     * returns null if there is no open route labor records.
     * 
     * @param operatorLaborId - the ID of the Operator
     * @return the requested list of route labor records or null
     * @throws DataAccessException on any failure.
     */
    List<LoadingRouteLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException;
    
    /**
     * Retrieves the open route labor records for the given operator or
     * returns null if there is no open route labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
     LoadingRouteLabor  findOpenRecordsByOperatorId(long operatorId) 
                                                        throws DataAccessException;
    

    /**
     * Retrieves the list of closed loading labor record for the given
     * Operator Labor Id returns null is there are no loading labor record
     * for the given Operator Labor Id.
     * 
     * @param breakLaborId - The Id of the Operator break labor record
     * @return The requested list of loading labor records closed for this
     *         break or null
     * @throws DataAccessException on any failure.
     */

    List<LoadingRouteLabor> listClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException;
    
    /**
     * Get Labor Summary Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
    
    /**
     * Get Labor Detail Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 