/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingTruckDiagramRow;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Loading Container DAO root interface for accessing data layer operations
 * 
 * @author mraj
 * 
 */
public interface LoadingContainerDAORoot extends GenericDAO<LoadingContainer> {

    /**
     * Method to get count of Container based on stop.
     * 
     * @param containerNumber Loading spoken Container
     * @param stop LoadingStop reference
     * @return Number of Containers in the stop.
     * @throws DataAccessException
     */
    public Number countLoadingContainerByStop(String containerNumber,
                                              LoadingStop stop)
        throws DataAccessException;

    /**
     * Method to get the list of loadingContainers by container number, route
     * number and region.
     * 
     * @param route LoadingRoute
     * @param loadingRegion LoadingRegion
     * @return list of LoadingContainer
     * @throws DataAccessException
     */
    public List<LoadingContainer> listAvailableLoadingContainersForRoute(LoadingRoute route,
                                                                         LoadingRegion loadingRegion)
        throws DataAccessException;

    /**
     * Method to get the Loading Containers based on Spoken Container Number.
     * 
     * @param containerNumber String
     * @param operatorID Long
     * @param routeNumber String.
     * @param regionNumber Integer
     * @return List<LoadingContainer>
     */
    public List<LoadingContainer> listLoadingContainerBySpokenNumber(String containerNumber,
                                                                     Long operatorID,
                                                                     String routeNumber,
                                                                     Integer regionNumber);

    /**
     * Method to get loading container Object
     * 
     * @param containerNumber Loading Container
     * @param route LoadingRoute
     * @param operatorID operator
     * @param loadingContainerStatus - String status of the container.
     * @return List of LoadingContainer
     * @throws DataAccessException
     */
    public LoadingContainer findLoadingContainerByNumber(String containerNumber,
                                                         LoadingRoute route,
                                                         Long operatorID,
                                                         List<LoadingContainerStatus> loadingContainerStatus)
        throws DataAccessException;

    /**
     * @param containerNumber Loading container number
     * @param containerStatus Status of the container
     * @return The target container
     */
    LoadingContainer findLoadingContainerByNumberAndStatus(String containerNumber,
                                                           List<LoadingContainerStatus> containerStatus);

    /**
     * Method to get count of Master Container.
     * @param masterContainer Loading Master Container
     * @param routeNumber Loading route
     * @param regionNumber region number in which operator is signed in
     * @return Number of Master Containers.
     * @throws DataAccessException
     */
    public Number countNumberOfMasterCont(String masterContainer,
                                          String routeNumber,
                                          Integer regionNumber)
        throws DataAccessException;

    /**
     * Method to find existing containers with matching criteria
     * 
     * @param containerNumber - the containerNumber
     * @return Loading container if found and null not found
     * @throws DataAccessException
     */
    public LoadingContainer findImportBlockingCondition(String containerNumber)
        throws DataAccessException;

    /**
     * @return list of maps. Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @param qd - query decorator for number of results
     * @throws DataAccessException .
     */
    public List<Map<String, Object>> listLoadedContainers(QueryDecorator qd)
        throws DataAccessException;

    /**
     * Method to find if any containers exist for the route with custom
     * positions
     * @param routeId Id of the Completed route, for which configuration is to
     *            be found out.
     * @param loadPositions list of standard container load positions
     * @return Number of containers with custom positions (2 digits positions)
     * @throws DataAccessException
     */
    public Long countContainerWithCustomPos(long routeId,
                                            List<Integer> loadPositions)
        throws DataAccessException;

    /**
     * @param routeId Id of the route for which containers are to be fetched for
     *            displaying on truck diagram
     * @return list of the <code> LoadingTruckDiagramRow</code> objects to be
     *         displayed on truck diagram
     * @throws DataAccessException
     */
    public List<LoadingTruckDiagramRow> listTruckLoadedContainers(long routeId)
        throws DataAccessException;

    /**
     * Find the time of the last loading record for the given operator that is
     * greater than or equal the minTime.
     * 
     * @param operatorId - the operator to search.
     * @param minTime - the date/time returned must be greater or equal to
     *            minTime
     * @param maxTime - the date/time returned must be less or equal to maxTime
     * 
     * @return Date - the maximum end time from the loading labor record.
     * 
     * @throws DataAccessException - database exceptions
     */
    public Date maxEndTime(long operatorId, Date minTime, Date maxTime)
        throws DataAccessException;

    /**
     * Find the sum of consolidated containers weight based on
     * masterContainerNumber.
     * 
     * @param masterContainerNumber - value of master container number.
     * @param routeId routeID value long.
     * @return Double - sum of consolidated containers weight.
     * 
     * @throws DataAccessException - database exceptions
     */
    public List<LoadingContainer> listConsolidatedContainers(String masterContainerNumber,
                                                         long routeId)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 