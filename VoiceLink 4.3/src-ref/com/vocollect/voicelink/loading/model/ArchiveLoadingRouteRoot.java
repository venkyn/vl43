/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author mraj
 *
 */
public class ArchiveLoadingRouteRoot extends ArchiveModelObject implements
    Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = -1481014830713708604L;

    private String number;

    private LoadingRegion region;

    private Date departureDateTime;
    
    private LoadingRouteType type;
    
    private LoadingRouteStatus status;

    private Location dockDoor;

    private String trailer;
    
    private String note;
    
    private boolean captureLoadPosition;
    
    private Double expectedCube;
    
    private Double expectedWeight;
    
    private Date startTime;

    private Date endTime;
    
    private Date createdDate;

    private ExportStatus exportStatus;
    
    private Site site;
    
    private Set<ArchiveLoadingStop> stops = new HashSet<ArchiveLoadingStop>();
    
    private List<ArchiveLoadingLabor> labor = new ArrayList<ArchiveLoadingLabor>();
    
    
    /**
     * 
     */
    public ArchiveLoadingRouteRoot() {
        
    }

    /**
     * 
     * @param route Loading route object to be archived
     */
    public ArchiveLoadingRouteRoot(LoadingRoute route) {
        if (route == null) {
            return;
        }

        for (LoadingRouteLabor ll : route.getLabor()) {
            getLabor().add(
                new ArchiveLoadingLabor(ll, (ArchiveLoadingRoute) this));
        }

        for (LoadingStop stop : route.getStops()) {
            getStops().add(
                new ArchiveLoadingStop(stop, (ArchiveLoadingRoute) this));
        }

        for (Tag t : route.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }

        this.setNumber(route.getNumber());
        this.setRegion(route.getRegion());
        this.setDepartureDateTime(route.getDepartureDateTime());
        this.setType(route.getType());
        this.setStatus(route.getStatus());
        this.setDockDoor(route.getDockDoor());
        this.setTrailer(route.getTrailer());
        this.setNote(route.getNote());
        this.setCaptureLoadPosition(route.getCaptureLoadPosition().longValue() == 0
            ? false : true);
        this.setExpectedCube(route.getExpectedCube());
        this.setExpectedWeight(route.getExpectedWeight());
        this.setStartTime(route.getStartTime());
        this.setEndTime(route.getEndTime());
        this.setCreatedDate(new Date());
        this.setExportStatus(route.getExportStatus());
    }
    
    
    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    
    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    
    /**
     * @return the region
     */
    public LoadingRegion getRegion() {
        return region;
    }

    
    /**
     * @param region the region to set
     */
    public void setRegion(LoadingRegion region) {
        this.region = region;
    }

    
    /**
     * @return the departureDateTime
     */
    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    
    /**
     * @param departureDateTime the departureDateTime to set
     */
    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    
    /**
     * @return the type
     */
    public LoadingRouteType getType() {
        return type;
    }

    
    /**
     * @param type the type to set
     */
    public void setType(LoadingRouteType type) {
        this.type = type;
    }

    
    /**
     * @return the status
     */
    public LoadingRouteStatus getStatus() {
        return status;
    }

    
    /**
     * @param status the status to set
     */
    public void setStatus(LoadingRouteStatus status) {
        this.status = status;
    }

    
    /**
     * @return the dockDoor
     */
    public Location getDockDoor() {
        return dockDoor;
    }

    
    /**
     * @param dockDoor the dockDoor to set
     */
    public void setDockDoor(Location dockDoor) {
        this.dockDoor = dockDoor;
    }

    
    /**
     * @return the trailer
     */
    public String getTrailer() {
        return trailer;
    }

    
    /**
     * @param trailer the trailer to set
     */
    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    
    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    
    /**
     * @return the captureLoadPosition
     */
    public boolean isCaptureLoadPosition() {
        return captureLoadPosition;
    }

    
    /**
     * @param captureLoadPosition the captureLoadPosition to set
     */
    public void setCaptureLoadPosition(boolean captureLoadPosition) {
        this.captureLoadPosition = captureLoadPosition;
    }

    
    /**
     * @return the expectedCube
     */
    public Double getExpectedCube() {
        return expectedCube;
    }

    
    /**
     * @param expectedCube the expectedCube to set
     */
    public void setExpectedCube(Double expectedCube) {
        this.expectedCube = expectedCube;
    }

    
    /**
     * @return the expectedWeight
     */
    public Double getExpectedWeight() {
        return expectedWeight;
    }

    
    /**
     * @param expectedWeight the expectedWeight to set
     */
    public void setExpectedWeight(Double expectedWeight) {
        this.expectedWeight = expectedWeight;
    }

    
    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    
    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    
    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    
    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    
    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    
    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    
    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }

    
    /**
     * @param site the site to set
     */
    public void setSite(Site site) {
        this.site = site;
    }

    
    /**
     * @return the stops
     */
    public Set<ArchiveLoadingStop> getStops() {
        return stops;
    }

    
    /**
     * @param stops the stops to set
     */
    public void setStops(Set<ArchiveLoadingStop> stops) {
        this.stops = stops;
    }
    
    /**
     * @return the labor
     */
    public List<ArchiveLoadingLabor> getLabor() {
        return labor;
    }
    
    /**
     * @param labor the labor to set
     */
    public void setLabor(List<ArchiveLoadingLabor> labor) {
        this.labor = labor;
    }

    
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ArchiveLoadingRouteRoot)) {
            return false;
        }
        final ArchiveLoadingRouteRoot other = (ArchiveLoadingRouteRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }

        return true;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().intValue());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 