/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author pfunyak
 *
 */
public class LoadingStopRoot extends CommonModelObject
                             implements Serializable, Taggable { 

    private static final long serialVersionUID = 2093173763866938485L;
    
    private Long number;
    
    private LoadingRoute route;
    
    private List <LoadingContainer> containers = new ArrayList<LoadingContainer>();
    
    private Integer expectedNumberOfContainers = new Integer(0);
    
    private Integer actualNumberOfContainers = 0;
    
    private Integer selOpenContainerCount = 0;
    
    private Integer containerCount = 0;
    
    private Integer numberOfImportedContainers = 0;
    
    private LoadingStopStatus status = LoadingStopStatus.NotLoaded;
    
    // Operator that is currently working on the stop.
    private Operator operator;
    
    // Time when Stop was initially started.
    private Date startTime;
    
    // Time when Stop was fully completed.
    private Date endTime;
    
    private int assignmentCount = 0;
    
    private int licenseCount = 0;

    private String customerNumber;
    
    private int availableContainerCount = 0;

    private String note = "";
    
    private ExportStatus exportStatus = ExportStatus.NotExported;
    
    private Set<Tag> tags;
    
    
    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        this.number = number;
    }
    
    /**
     * Getter for Available Container count.
     * @return availableContainerCount.
     */
    public int getAvailableContainerCount() {
        return availableContainerCount;
    }

    /**
     * Setter for Available Container count.
     * @param availableContainerCount int.
     */
    public void setAvailableContainerCount(int availableContainerCount) {
        this.availableContainerCount = availableContainerCount;
    }

    /**
     * @return the route
     */
    public LoadingRoute getRoute() {
        return route;
    }

    /**
     * @param route the route to set
     */
    public void setRoute(LoadingRoute route) {
        this.route = route;
    }
    
    
    /**
     * @return the containers
     */
    public List<LoadingContainer> getContainers() {
        return containers;
    }

    /**
     * @param containers the containers to set
     */
    public void setContainers(List<LoadingContainer> containers) {
        this.containers = containers;
    }

    /**
     * @return the expectedNumberOfContainers
     */
    public Integer getExpectedNumberOfContainers() {
        return expectedNumberOfContainers;
    }

    
    /**
     * @param expectedNumberOfContainers the expectedNumberOfContainers to set
     */
    public void setExpectedNumberOfContainers(Integer expectedNumberOfContainers) {
        this.expectedNumberOfContainers = expectedNumberOfContainers;
    }

    
    /**
     * @param actualNumberOfContainers the actualNumberOfContainers to set
     */
    public void setActualNumberOfContainers(Integer actualNumberOfContainers) {
        this.actualNumberOfContainers = actualNumberOfContainers;
    }

    /**
     * @return the actualNumberOfContainers
     */
    public Integer getActualNumberOfContainers() {
        return this.containerCount + this.selOpenContainerCount;
    }

    
    /**
     * @return the selOpenContainerCount
     */
    public Integer getSelOpenContainerCount() {
        return selOpenContainerCount;
    }

    /**
     * @param selOpenContainerCount the selOpenContainerCount to set
     */
    public void setSelOpenContainerCount(Integer selOpenContainerCount) {
        this.selOpenContainerCount = selOpenContainerCount;
    }

    /**
     * @return the containerCount
     */
    public Integer getContainerCount() {
        return containerCount;
    }

    /**
     * @param containerCount the containerCount to set
     */
    public void setContainerCount(Integer containerCount) {
        this.containerCount = containerCount;
    }

    /**
     * @param numberOfImportedContainers the numberOfImportedContainers to set
     */
    public void setNumberOfImportedContainers(Integer numberOfImportedContainers) {
        this.numberOfImportedContainers = numberOfImportedContainers;
    }

    /**
     * @return the numberOfImportedContainers
     */
    public Integer getNumberOfImportedContainers() {
        
        for (int i = 0; i < this.containers.size(); i++) {
            if (this.containers.get(i).getType() == LoadingContainerType.Imported) {
                this.numberOfImportedContainers++;
            }
        }
        return numberOfImportedContainers;
    }
    


    /**
     * @return the status
     */
    public LoadingStopStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(LoadingStopStatus status) {
        this.status = status;
    }

    /**
     * Getter for operator.
     * @return operator Operator.
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Setter for operator.
     * @param operator Operator.
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for startTime.
     * @return startTime Date.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Setter for startTime.
     * @param startTime Date.
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for endTime.
     * @return endTime Date.
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Setter for endTime.
     * @param endTime Date.
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    /**
     * @param actualNumberOfContainers the actualNumberOfContainers to set
     */
    public void setActualNumberOfContainers(int actualNumberOfContainers) {
        this.actualNumberOfContainers = actualNumberOfContainers;
    }

    /**
     * @param numberOfImportedContainers the numberOfImportedContainers to set
     */
    public void setNumberOfImportedContainers(int numberOfImportedContainers) {
        this.numberOfImportedContainers = numberOfImportedContainers;
    }

    /**
     * @return the assignmentCount
     */
    public int getAssignmentCount() {
        return assignmentCount;
    }

    /**
     * @param assignmentCount the assignmentCount to set
     */
    public void setAssignmentCount(int assignmentCount) {
        this.assignmentCount = assignmentCount;
    }

    /**
     * @return the licenseCount
     */
    public int getLicenseCount() {
        return licenseCount;
    }

    /**
     * @param licenseCount the licenseCount to set
     */
    public void setLicenseCount(int licenseCount) {
        this.licenseCount = licenseCount;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
    

    /**
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    
    /**
     * @param customerNumber the customerNumber to set
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * Method to allow import failure messages with descriptive field name when
     * LoadingStop.number field value is invalid <BR>
     * Use <code>getNumber()</code> instead<BR>
     * TODO: refactor LoadingStop.number to LoadingStop.stopNumber
     *   
     * @return the stopNumber
     */
    @Deprecated
    public Long getStopNumber() {
        return this.number;
    }

    
    /**
     * Method to allow import failure messages with descriptive field name when
     * LoadingStop.number field value is invalid<BR>
     * Use <code>setNumber(Long)</code> instead<BR>
     * TODO: refactor LoadingStop.number to LoadingStop.stopNumber
     * 
     * @param stopNumber the stopNumber to set
     */
    @Deprecated
    public void setStopNumber(Long stopNumber) {
        this.number = stopNumber;
    }

    
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @see com.vocollect.epp.model.Taggable#getTags()
     * @return set of tags
     */
    @Override
    public Set<Tag> getTags() {
        return this.tags;
    }

    /** 
     * @param tags -  the tags associated with the objects
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @param obj - the object to check
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     * 
     * @return true if objects are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LoadingStopRoot other = (LoadingStopRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }
        return true;
    }
    
    

    /**
     * @see java.lang.Object#hashCode()
     * @return hashCode
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((route == null) ? 0 : route.hashCode());
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        return result;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 