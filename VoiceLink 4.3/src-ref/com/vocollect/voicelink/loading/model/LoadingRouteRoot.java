/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;

import static com.vocollect.voicelink.loading.model.LoadingRouteStatus.Available;
import static com.vocollect.voicelink.loading.model.LoadingRouteType.Dedicated;
import static com.vocollect.voicelink.loading.model.LoadingRouteType.MultiStop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Model object for Loading Route
 * 
 * @author mraj
 * 
 */
public class LoadingRouteRoot extends CommonModelObject implements
        Serializable, Taggable {

    private static final long serialVersionUID = -8217808411959761337L;

    private String number;

    private Set<Tag> tags;

    private Set<LoadingStop> stops = new HashSet<LoadingStop>();

    private Location dockDoor;

    private LoadingRouteType type = Dedicated;

    private String trailer = "";

    // Operators who are currently working on the route, operators who
    // completed
    // the route, or the ones who are assigned to the route
    private Set<Operator> operators;

    // Time Route was initially started
    private Date startTime;

    // Time when Route was fully completed
    private Date endTime;

    private LoadingRouteStatus status = Available;

    private int availableStopCount = 0;

    private int availableRouteContainerCount = 0;

    private int routeContainerCount = 0;

    private Date departureDateTime;

    private Integer expectedNumberOfContainers = 0;

    private Integer actualNumberOfContainers = 0;
    
    private Integer loadedNumberOfContainers = 0;

    private Integer assignmentCount = 0;
    
    private Long captureLoadPosition;
    
    private String captureLoadPositionStringValue; 

    private Double expectedCube = 0.0;

    private Double expectedWeight = 0.0;
    
    private Double truckWeight; 
    
    private LoadingRegion region;

    private String note = "None";

    private ExportStatus exportStatus = ExportStatus.NotExported;
    
    // labor records belonging to this assignment (Added for archive purposes)
    private List<LoadingRouteLabor> labor = new ArrayList<LoadingRouteLabor>();
    
    private int purgeable;
    
    /**
     * @return the truckWeight
     */
    public Double getTruckWeight() {
        return truckWeight;
    }

    /**
     * @param truckWeight the truckWeight to set
     */
    public void setTruckWeight(Double truckWeight) {
        this.truckWeight = truckWeight;
    }

    /**
     * @return the stops
     */
    public Set<LoadingStop> getStops() {
        return stops;
    }

    /**
     * @param stops
     *            the stops to set
     */
    public void setStops(Set<LoadingStop> stops) {
        this.stops = stops;
    }

    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    /**
     * @param note
     *            the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
    

    /**
     * @return the captureLoadPositionStringValue
     */
    public String getCaptureLoadPositionStringValue() {
        if (this.getCaptureLoadPosition() == 0L) {
            return ResourceUtil.getLocalizedKeyValue("loading.route.captureload.false");
        } else {
            return ResourceUtil.getLocalizedKeyValue("loading.route.captureload.true");
        }        
    }

    /**
     * @param captureLoadPositionStringValue the captureLoadPositionStringValue to set
     */
    public void setCaptureLoadPositionStringValue(
            String captureLoadPositionStringValue) {
        this.captureLoadPositionStringValue = captureLoadPositionStringValue;
    }    

    /**
     * @return the actualNumberOfContainers
     */
    public Integer getActualNumberOfContainers() {
        this.actualNumberOfContainers = 0;
        
        if (this.stops != null) {
            for (LoadingStop ls : this.stops) {
                if (ls.getActualNumberOfContainers() != null) {
                    this.actualNumberOfContainers += ls
                            .getActualNumberOfContainers();
                }
            }
        }
        return this.actualNumberOfContainers;
    }

    /**
     * @param actualNumberOfContainers
     *            the actualNumberOfContainers to set
     */
    public void setActualNumberOfContainers(Integer actualNumberOfContainers) {
        this.actualNumberOfContainers = actualNumberOfContainers;
    }
    
    /**
     * Getter for the loadedNumberOfContainers property.
     * @return Integer value of the property
     */
    public Integer getLoadedNumberOfContainers() {
        return loadedNumberOfContainers;
    }

    
    /**
     * Setter for the loadedNumberOfContainers property.
     * @param loadedNumberOfContainers the new loadedNumberOfContainers value
     */
    public void setLoadedNumberOfContainers(Integer loadedNumberOfContainers) {
        this.loadedNumberOfContainers = loadedNumberOfContainers;
    }

    /**
     * Getter for the assignmentCount property.
     * @return Integer value of the property
     */
    public Integer getAssignmentCount() {
        return assignmentCount;
    }

    
    /**
     * Setter for the assignmentCount property.
     * @param assignmentCount the new assignmentCount value
     */
    public void setAssignmentCount(Integer assignmentCount) {
        this.assignmentCount = assignmentCount;
    }

    /**
     * Getter for captureLoadPosition.
     * 
     * @return captureLoadPosition Long.
     */
    public Long getCaptureLoadPosition() {
        return captureLoadPosition;
    }

    /**
     * Setter for captureLoadPosition.
     * 
     * @param captureLoadPosition
     *            Long.
     */
    public void setCaptureLoadPosition(Long captureLoadPosition) {
        this.captureLoadPosition = captureLoadPosition;
    }

    /**
     * @return the type
     */
    public LoadingRouteType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(LoadingRouteType type) {
        this.type = type;
    }

    /**
     * Getter for dockDoor.
     * 
     * @return dockDoor Location.
     */
    public Location getDockDoor() {
        return dockDoor;
    }

    /**
     * Setter for dockDoor.
     * 
     * @param dockDoor
     *            Location.
     */
    public void setDockDoor(Location dockDoor) {
        this.dockDoor = dockDoor;
    }

    /**
     * @return the trailer
     */
    public String getTrailer() {
        return trailer;
    }

    /**
     * @param trailer
     *            the trailer to set
     */
    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    /**
     * Getter for Set of Operators.
     * 
     * @return operators.
     */
    public Set<Operator> getOperators() {
        return operators;
    }

    /**
     * Setter for Set of Operators.
     * 
     * @param operators
     *            Set<Operators>
     */
    public void setOperators(Set<Operator> operators) {
        this.operators = operators;
    }

    /**
     * Getter for startTime.
     * 
     * @return startTime Date.
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Setter for startTime.
     * 
     * @param startTime
     *            Date.
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for endTime.
     * 
     * @return endTime Date.
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * Setter for endTime.
     * 
     * @param endTime
     *            Date.
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the status
     */
    public LoadingRouteStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(LoadingRouteStatus status) {
        this.status = status;
    }

    /**
     * Getter for availableStopCount.
     * 
     * @return the availableStopCount
     */
    public int getAvailableStopCount() {
        return availableStopCount;
    }

    /**
     * Setter for availableStopCount.
     * 
     * @param availableStopCount
     *            the availableStopCount to set
     */
    public void setAvailableStopCount(int availableStopCount) {
        this.availableStopCount = availableStopCount;
    }

    /**
     * Getter for availableRouteContainerCount
     * 
     * @return the availableRouteContainerCount
     */
    public int getAvailableRouteContainerCount() {
        return availableRouteContainerCount;
    }

    /**
     * Setter for availableRouteContainerCount
     * 
     * @param availableRouteContainerCount
     *            the availableRouteContainerCount to set
     */
    public void setAvailableRouteContainerCount(int availableRouteContainerCount) {
        this.availableRouteContainerCount = availableRouteContainerCount;
    }

    /**
     * @return the routeContainerCount
     */
    public int getRouteContainerCount() {
        return routeContainerCount;
    }

    /**
     * @param routeContainerCount
     *            the routeContainerCount to set
     */
    public void setRouteContainerCount(int routeContainerCount) {
        this.routeContainerCount = routeContainerCount;
    }

    /**
     * @return the departureDateTime
     */
    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    /**
     * @param departureDateTime
     *            the departureDateTime to set
     */
    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    /**
     * @return the route
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number
     *            the route to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the totalContainers
     */
    public Integer getExpectedNumberOfContainers() {
        return expectedNumberOfContainers;
    }

    /**
     * @param expectedNumberOfContainers
     *            the totalContainers to set
     */
    public void setExpectedNumberOfContainers(Integer expectedNumberOfContainers) {
        this.expectedNumberOfContainers = expectedNumberOfContainers;
    }

    /**
     * @return the expectedCube
     */
    public Double getExpectedCube() {
        return expectedCube;
    }

    /**
     * @param expectedCube
     *            the expectedCube to set
     */
    public void setExpectedCube(Double expectedCube) {
        this.expectedCube = expectedCube;
    }

    /**
     * @return the expectedWeight
     */
    public Double getExpectedWeight() {
        return expectedWeight;
    }

    /**
     * @param expectedWeight
     *            the expectedWeight to set
     */
    public void setExpectedWeight(Double expectedWeight) {
        this.expectedWeight = expectedWeight;
    }

    /**
     * @return the region
     */
    public LoadingRegion getRegion() {
        return region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(LoadingRegion region) {
        this.region = region;
    }

    
    /**
     * @return the labor
     */
    public List<LoadingRouteLabor> getLabor() {
        return labor;
    }

    
    /**
     * @param labor the labor to set
     */
    public void setLabor(List<LoadingRouteLabor> labor) {
        this.labor = labor;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus
     *            the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @return the purgeable
     */
    public int getPurgeable() {
        return purgeable;
    }

    
    /**
     * @param purgeable the purgeable to set
     */
    public void setPurgeable(int purgeable) {
        this.purgeable = purgeable;
    }

    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Method to add and associate a new stop object with the route. This is
     * called from the route and stop import process
     * 
     * @param newStop
     *            - the stop to add to the route
     */
    public void addStop(LoadingStop newStop) {
        getStops().add(newStop);
        newStop.setRoute((LoadingRoute) this);

        // Determine the type of route based on number of stops
        if (getStops().size() > 1) {
            this.type = MultiStop;
        }
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LoadingRouteRoot)) {
            return false;
        }
        final LoadingRouteRoot other = (LoadingRouteRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().intValue());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 