/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Loading Stop data to be imported
 * @author mraj
 * 
 */
public class LoadingStopImportDataRoot extends BaseModelObject implements
    Importable {

    /**
     * 
     */
    private static final long serialVersionUID = -8599025171150382954L;
    private ImportableImpl impl    = new ImportableImpl();
    private LoadingStop     myModel = new LoadingStop();
    private LoadingRouteImportData parent = null;
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isInProgress()
     */
    @Override
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setInProgress(boolean)
     */
    @Override
    public void setInProgress(boolean inProgress) {
        impl.setInProgress(inProgress);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setCompleted(boolean)
     */
    @Override
    public void setCompleted(boolean completed) {
        impl.setCompleted(completed);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportStatus()
     */
    @Override
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportStatus(int)
     */
    @Override
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#convertToModel()
     */
    @Override
    public Object convertToModel() throws VocollectException {
        return myModel;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportID()
     */
    @Override
    public Long getImportID() {
        return impl.getImportID();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportID(java.lang.Long)
     */
    @Override
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getSiteName()
     */
    @Override
    public String getSiteName() {
        return impl.getSiteName();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setSiteName(java.lang.String)
     */
    @Override
    public void setSiteName(String siteName) {
        impl.setSiteName(siteName);
    }

    
    /**
     * @return the parent
     */
    public LoadingRouteImportData getParent() {
        return parent;
    }

    
    /**
     * @param parent the parent to set
     */
    public void setParent(LoadingRouteImportData parent) {
        this.parent = parent;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    public Long getId() {
        return myModel.getId();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getNumber()
     */
    public Long getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setNumber(java.lang.String)
     */
    public void setNumber(Long number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getRoute()
     */
    public LoadingRoute getRoute() {
        return myModel.getRoute();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#toString()
     */
    public String toString() {
        return myModel.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setRoute(com.vocollect.voicelink.loading.model.LoadingRoute)
     */
    public void setRoute(LoadingRoute route) {
        myModel.setRoute(route);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getContainers()
     */
    public List<LoadingContainer> getContainers() {
        return myModel.getContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setContainers(java.util.Set)
     */
    public void setContainers(List<LoadingContainer> containers) {
        myModel.setContainers(containers);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteStop#getCustomerNumber()
     */
    public String getCustomerNumber() {
        return myModel.getCustomerNumber();
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteStop#setCustomerNumber(java.lang.String)
     */
    public void setCustomerNumber(String customerNumber) {
        myModel.setCustomerNumber(customerNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getExpectedNumberOfContainers()
     */
    public Integer getExpectedNumberOfContainers() {
        return myModel.getExpectedNumberOfContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setExpectedNumberOfContainers(int)
     */
    public void setExpectedNumberOfContainers(Integer expectedNumberOfContainers) {
        myModel.setExpectedNumberOfContainers(expectedNumberOfContainers);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getNote()
     */
    public String getNote() {
        return myModel.getNote();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setNote(java.lang.String)
     */
    public void setNote(String note) {
        myModel.setNote(note);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 