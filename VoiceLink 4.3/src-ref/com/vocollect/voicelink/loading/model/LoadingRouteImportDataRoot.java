/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Location;

import java.util.Date;
import java.util.Set;

/**
 * Loading route to be imported
 * @author mraj
 * 
 */
public class LoadingRouteImportDataRoot extends BaseModelObject implements
    Importable {


    private static final long serialVersionUID = -7244995431733294012L;

    private ImportableImpl impl = new ImportableImpl();
    
    private LoadingRoute     myModel = new LoadingRoute();
    
    private Set<LoadingStopImportData> importableStopData;
    
    /**
     * Set the number in the region. This will be used to look up a region in
     * the database.
     * @param number the number to set the regionnumber to. Data type of this
     *            argument is set to Long so that database field is created as
     *            numeric(19,0) instead of int. Advantage of numeric over int is
     *            that in case of SQL server value of '' would be treated as
     *            NULL and rejected. An int datatype inserts 0 by default for ''
     *            value
     */
    public void setRegionNumber(Long number) {
        LoadingRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new LoadingRegion();
            setRegion(tempRegion);
        }
        tempRegion.setNumber(new Integer(number.intValue()));
    }

    /**
     * Get the region number.
     * @return the region number as a string. Data type of return value is set
     *         to Long so that database field is created as numeric(19,0)
     *         instead of int. Advantage of numeric over int is that in case of
     *         SQL server value of '' would be treated as NULL and rejected. An
     *         int datatype inserts 0 by default for '' value
     */
    public Long getRegionNumber() {
        LoadingRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new LoadingRegion();
            setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return new Long(tempRegion.getNumber());
    }
    
    
    /**
     * @return the importableStopData
     */
    public Set<LoadingStopImportData> getImportableStopData() {
        return importableStopData;
    }


    
    /**
     * @param importableStopData the importableStopData to set
     */
    public void setImportableStopData(Set<LoadingStopImportData> importableStopData) {
        this.importableStopData = importableStopData;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    public Long getId() {
        return this.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#toString()
     */
    public String toString() {
        return myModel.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getStops()
     */
    public Set<LoadingStop> getStops() {
        return myModel.getStops();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setStops(java.util.Set)
     */
    public void setStops(Set<LoadingStop> stops) {
        myModel.setStops(stops);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getNote()
     */
    public String getNote() {
        return myModel.getNote();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setNote(java.lang.String)
     */
    public void setNote(String note) {
        myModel.setNote(note);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setCaptureLoadPosition(Long)
     */
    public void setCaptureLoadPosition(Long captureLoadPosition) {
        myModel.setCaptureLoadPosition(captureLoadPosition);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getCaptureLoadPosition()
     */
    public Long getCaptureLoadPosition()  {
        return myModel.getCaptureLoadPosition();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getActualNumberOfContainers()
     */
    public int getActualNumberOfContainers() {
        return myModel.getActualNumberOfContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setActualNumberOfContainers(int)
     */
    public void setActualNumberOfContainers(int actualNumberOfContainers) {
        myModel.setActualNumberOfContainers(actualNumberOfContainers);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getType()
     */
    public LoadingRouteType getType() {
        return myModel.getType();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setType(com.vocollect.voicelink.loading.model.LoadingRouteType)
     */
    public void setType(LoadingRouteType type) {
        if (null != type) {
            myModel.setType(type);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getDockDoor()
     */
    public Location getDockDoor() {
        return myModel.getDockDoor();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setDockDoor(java.lang.Long)
     */
    public void setDockDoor(Location dockDoor) {
        myModel.setDockDoor(dockDoor);
    }

    
    /**
     * Get the Dock Door's Location's id (scannedVerification).
     * @return dockDoor String.
     */
    public String getDockDoorLocationId() {
        Location tempLocation = myModel.getDockDoor();
        if (null == tempLocation
                || null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }

    /**
     * Method to set the DockDoor's location. This will further fetch the
     * corresponding location object from DB.
     * 
     * @param dockDoorLocation String.
     */
    public void setDockDoorLocationId(String dockDoorLocation) {
        Location tempLocation = myModel.getDockDoor();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setDockDoor(tempLocation);
        }
        tempLocation.setScannedVerification(dockDoorLocation);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getTrailer()
     */
    public String getTrailer() {
        return myModel.getTrailer();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setTrailer(java.lang.Long)
     */
    public void setTrailer(String trailer) {
        myModel.setTrailer(trailer);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getStatus()
     */
    public LoadingRouteStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setStatus(com.vocollect.voicelink.loading.model.LoadingRouteStatus)
     */
    public void setStatus(LoadingRouteStatus status) {
        myModel.setStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getDepartureDateTime()
     */
    public Date getDepartureDateTime() {
        return myModel.getDepartureDateTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setDepartureDateTime(java.util.Date)
     */
    public void setDepartureDateTime(Date departureDateTime) {
        myModel.setDepartureDateTime(departureDateTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getExpectedNumberOfContainers()
     */
    public int getExpectedNumberOfContainers() {
        return myModel.getExpectedNumberOfContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setExpectedNumberOfContainers(int)
     */
    public void setExpectedNumberOfContainers(int expectedNumberOfContainers) {
        myModel.setExpectedNumberOfContainers(expectedNumberOfContainers);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getExpectedCube()
     */
    public Double getExpectedCube() {
        return myModel.getExpectedCube();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setExpectedCube(java.lang.Double)
     */
    public void setExpectedCube(Double expectedCube) {
        myModel.setExpectedCube(expectedCube);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getExpectedWeight()
     */
    public Double getExpectedWeight() {
        return myModel.getExpectedWeight();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setExpectedWeight(java.lang.Double)
     */
    public void setExpectedWeight(Double expectedWeight) {
        myModel.setExpectedWeight(expectedWeight);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getRegion()
     */
    public LoadingRegion getRegion() {
        return myModel.getRegion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#addStop(com.vocollect.voicelink.loading.model.LoadingStop)
     */
    public void addStop(LoadingStop newStop) {
        myModel.addStop(newStop);
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setRegion(com.vocollect.voicelink.loading.model.LoadingRegion)
     */
    public void setRegion(LoadingRegion region) {
        myModel.setRegion(region);
    }
    
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isInProgress()
     */
    @Override
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setInProgress(boolean)
     */
    @Override
    public void setInProgress(boolean inProgress) {
        impl.setInProgress(inProgress);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setCompleted(boolean)
     */
    @Override
    public void setCompleted(boolean completed) {
        impl.setCompleted(completed);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportStatus()
     */
    @Override
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportStatus(int)
     */
    @Override
    public void setImportStatus(int newStatus) {
        impl.setImportStatus(newStatus);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#convertToModel()
     */
    @Override
    public Object convertToModel() throws VocollectException {
        for (LoadingStopImportData stop : this.importableStopData) {
            myModel.addStop((LoadingStop) stop.convertToModel());
        }
        return myModel;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportID()
     */
    @Override
    public Long getImportID() {
        return impl.getImportID();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportID(java.lang.Long)
     */
    @Override
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getSiteName()
     */
    @Override
    public String getSiteName() {
        return impl.getSiteName();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setSiteName(java.lang.String)
     */
    @Override
    public void setSiteName(String siteName) {
        impl.setSiteName(siteName);
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 