/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;


import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Model object for Loading Containers
 * 
 * @author mraj
 *
 */
public class LoadingContainerRoot extends CommonModelObject implements  Serializable, Taggable {

    private static final long serialVersionUID = 8010564256900948310L;
    
    // This is the route object that the container is associated with.
    private LoadingRoute route;

    // This is the stop object that the container is associated with.
    private LoadingStop stop;
    
    // The departure date and time of the route/stop that this container belongs to.
    private Date departureDateTime;
    
    // The region of the route/stop
    private LoadingRegion region;

    private String stopNumber;
    
    // This will contain either the Selection container number, the PTS Container number
    // or a container number that was imported from the WMS.
    private String containerNumber;
    
    // Holds the value of the master container number.
    private String masterContainerNumber;

    // The type of container being stored in the "theContainer" variable.
    // User must request the type of container to know what they can do with it.
    private LoadingContainerType type = LoadingContainerType.Unknown;
    
    private LoadingContainerStatus status = LoadingContainerStatus.NotLoaded;
    
    private ExportStatus exportStatus = ExportStatus.NotExported;
    
    // I don't think that these values should be named "expected"
    // They should contain the actual weight and cube size.
    // If they come from VoiceLink, we can set these values because we will
    // know the values. If they are imported the WMS should be able to provide
    // this information to us.
    private Double expectedCube;

    private Double expectedWeight;
    
    
    // I'm not sure if we need this here.  If we need to see who actually
    // loaded the container, we may have to look at labor information since
    // multiple operators could have moved this container.
    private Operator operator;
    
    private Integer loadPosition;
   
    private Date loadTime;

    private Set<Tag> tags;
    
    
    /**
     * @return the route
     */
    public LoadingRoute getRoute() {
        return route;
    }

    /**
     * @param route the route to set
     */
    public void setRoute(LoadingRoute route) {
        this.route = route;
    }

    /**
     * @return the stop
     */
    public LoadingStop getStop() {
        return stop;
    }

    /**
     * @param stop the stop to set
     */
    public void setStop(LoadingStop stop) {
        this.stop = stop;
    }

    /**
     * @return the departureDateTime
     */
    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    /**
     * @param departureDateTime the departureDateTime to set
     */
    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }
    
    /**
     * @return the region
     */
    public LoadingRegion getRegion() {
        return region;
    }

    /**
     * @param region the region to set
     */
    public void setRegion(LoadingRegion region) {
        this.region = region;
    }

    /**
     * @return the stopNumber
     */
    public String getStopNumber() {
        return stopNumber;
    }

    /**
     * @param stopNumber the stopNumber to set
     */
    public void setStopNumber(String stopNumber) {
        this.stopNumber = stopNumber;
    }

    /**
     * @return the containerNumber
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * @param containerNumber the containerNumber to set
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for masterContainerNumber.
     * @return masterContainerNumber String.
     */
    public String getMasterContainerNumber() {
        return masterContainerNumber;
    }

    /**
     * Setter for masterContainerNumber.
     * @param masterContainerNumber String.
     */
    public void setMasterContainerNumber(String masterContainerNumber) {
        this.masterContainerNumber = masterContainerNumber;
    }


    /**
     * @return the type
     */
    public LoadingContainerType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(LoadingContainerType type) {
        this.type = type;
    }

    /**
     * @return the status
     */
    public LoadingContainerStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(LoadingContainerStatus status) {
        this.status = status;
    }


    /**
     * @return the expectedCube
     */
    public Double getExpectedCube() {
        return expectedCube;
    }

    /**
     * @param expectedCube the expectedCube to set
     */
    public void setExpectedCube(Double expectedCube) {
        this.expectedCube = expectedCube;
    }

    /**
     * @return the expectedWeight
     */
    public Double getExpectedWeight() {
        return expectedWeight;
    }

    /**
     * @param expectedWeight the expectedWeight to set
     */
    public void setExpectedWeight(Double expectedWeight) {
        this.expectedWeight = expectedWeight;
    }

    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    
    
    /**
     * Getter for loadTime.
     * @return loadTime Date.
     */
    public Date getLoadTime() {
        return loadTime;
    }

    /**
     * Setter for loadTime.
     * @param loadTime Date.
     */
    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    /**
     * @return the loadPosition
     */
    public Integer getLoadPosition() {
        return loadPosition;
    }

    /**
     * @param loadPosition the loadPosition to set
     */
    public void setLoadPosition(Integer loadPosition) {
        this.loadPosition = loadPosition;
    }

    
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @return the tags
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags the tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LoadingContainerRoot)) {
            return false;
        }
        final LoadingContainerRoot other = (LoadingContainerRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().intValue());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 