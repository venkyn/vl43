/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * Model object class for Loading Summary Screens.
 * 
 * @author kudupi
 */
public class LoadingSummaryRoot extends BaseModelObject implements
    Serializable, Taggable {

    /**
     * 
     */
    private static final long serialVersionUID = 6230203835937048373L;

    private LoadingRegion region;

    private Long id;

    private int totalAssignments;

    private int inProgress;

    private int available;

    private int complete;

    private int nonComplete;

    private int operatorsWorkingIn;

    private int operatorsAssigned;

    private int totalRoutes;

    private int totalRoutesCompleted;

    private int totalRoutesRemaining;

    private Double estimatedCompleted;

    private Site site = null; // cache the site to improve performance

    /**
     * @return the region
     */
    public LoadingRegion getRegion() {
        return region;
    }
    
    /**
     * @param region the region to set
     */
    public void setRegion(LoadingRegion region) {
        this.region = region;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the totalAssignments
     */
    public int getTotalAssignments() {
        return totalAssignments;
    }

    /**
     * @param totalAssignments the totalAssignments to set
     */
    public void setTotalAssignments(int totalAssignments) {
        this.totalAssignments = totalAssignments;
    }

    /**
     * @return the inProgress
     */
    public int getInProgress() {
        return inProgress;
    }

    /**
     * @param inProgress the inProgress to set
     */
    public void setInProgress(int inProgress) {
        this.inProgress = inProgress;
    }

    /**
     * @return the available
     */
    public int getAvailable() {
        return available;
    }

    /**
     * @param available the available to set
     */
    public void setAvailable(int available) {
        this.available = available;
    }

    /**
     * @return the complete
     */
    public int getComplete() {
        return complete;
    }

    /**
     * @param complete the complete to set
     */
    public void setComplete(int complete) {
        this.complete = complete;
    }

    /**
     * @return the nonComplete
     */
    public int getNonComplete() {
        return nonComplete;
    }

    /**
     * @param nonComplete the nonComplete to set
     */
    public void setNonComplete(int nonComplete) {
        this.nonComplete = nonComplete;
    }

    /**
     * @return the operatorsWorkingIn
     */
    public int getOperatorsWorkingIn() {
        return operatorsWorkingIn;
    }

    /**
     * @param operatorsWorkingIn the operatorsWorkingIn to set
     */
    public void setOperatorsWorkingIn(int operatorsWorkingIn) {
        this.operatorsWorkingIn = operatorsWorkingIn;
    }

    /**
     * @return the operatorsAssigned
     */
    public int getOperatorsAssigned() {
        return operatorsAssigned;
    }

    /**
     * @param operatorsAssigned the operatorsAssigned to set
     */
    public void setOperatorsAssigned(int operatorsAssigned) {
        this.operatorsAssigned = operatorsAssigned;
    }

    /**
     * @return the totalRoutes
     */
    public int getTotalRoutes() {
        return totalRoutes;
    }

    /**
     * @param totalRoutes the totalRoutes to set
     */
    public void setTotalRoutes(int totalRoutes) {
        this.totalRoutes = totalRoutes;
    }

    /**
     * @return the totalRoutesCompleted
     */
    public int getTotalRoutesCompleted() {
        return totalRoutesCompleted;
    }

    /**
     * @param totalRoutesCompleted the totalRoutesCompleted to set
     */
    public void setTotalRoutesCompleted(int totalRoutesCompleted) {
        this.totalRoutesCompleted = totalRoutesCompleted;
    }

    /**
     * @return the totalRoutesRemaining
     */
    public int getTotalRoutesRemaining() {
        return totalRoutesRemaining;
    }

    /**
     * @param totalRoutesRemaining the totalRoutesRemaining to set
     */
    public void setTotalRoutesRemaining(int totalRoutesRemaining) {
        this.totalRoutesRemaining = totalRoutesRemaining;
    }

    /**
     * @return the estimatedCompleted
     */
    public Double getEstimatedCompleted() {
        return estimatedCompleted;
    }

    /**
     * @param estimatedCompleted the estimatedCompleted to set
     */
    public void setEstimatedCompleted(Double estimatedCompleted) {
        this.estimatedCompleted = estimatedCompleted;
    }

    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(Site site) {
        this.site = site;
    }
    
    /* (non-Javadoc)
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    @Override
    public Set<Tag> getTags() {
        return region.getTags();
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    @Override
    public void setTags(Set<Tag> tags) {
        region.setTags(tags);
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof LoadingSummaryRoot) {
            return false;
        }
        final LoadingSummaryRoot other = (LoadingSummaryRoot) obj;
        if (region == null) {
            if (other.region != null) {
                return false;
            }
        } else if (!region.equals(other.region)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        return result;
    }    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 