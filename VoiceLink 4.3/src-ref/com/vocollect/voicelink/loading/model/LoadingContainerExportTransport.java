/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.ExportTransportObject;


/**
 * @author mraj
 *
 */
public class LoadingContainerExportTransport extends ExportTransportObject {

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.model.ExportTransportObject#updateExportToInProgress(java.lang.Object)
     */
    @Override
    public void updateExportToInProgress(Object object) {
        if (object instanceof LoadingContainer) {
            ((LoadingContainer) object).setExportStatus(ExportStatus.InProgress);
        }
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.model.ExportTransportObject#updateExportToExported(java.lang.Object)
     */
    @Override
    public void updateExportToExported(Object object) {
        if (object instanceof LoadingContainer) {
            ((LoadingContainer) object).setExportStatus(ExportStatus.Exported);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 