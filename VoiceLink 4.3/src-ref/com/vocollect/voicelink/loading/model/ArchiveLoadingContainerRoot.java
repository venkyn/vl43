/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.Date;

/**
 * Model Object representing Archive Loading container
 * @author mraj
 * 
 */
public class ArchiveLoadingContainerRoot extends ArchiveModelObject implements
    Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8851418300460589509L;

    private String number;

    private ArchiveLoadingStop stop;

    private String masterContainerNumber;

    private LoadingContainerType type;

    private LoadingContainerStatus status;

    private Double expectedCube;

    private Double expectedWeight;

    private String operatorIdentifier;
    
    private String operatorName;

    private Integer loadPosition;

    private Date loadTime;

    private Date createdDate;

    private ExportStatus exportStatus;

    private Site site;

    /**
     * Default constructor
     */
    public ArchiveLoadingContainerRoot() {

    }

    /**
     * @param container The Loading container object to be archived
     * @param arStop The loading Archive stop object
     */
    public ArchiveLoadingContainerRoot(LoadingContainer container,
                                       ArchiveLoadingStop arStop) {
        this.setNumber(container.getContainerNumber());
        this.setStop(arStop);
        this.setMasterContainerNumber(container.getMasterContainerNumber());
        this.setType(container.getType());
        this.setStatus(container.getStatus());
        this.setExpectedCube(container.getExpectedCube());
        this.setExpectedWeight(container.getExpectedWeight());
        this.setLoadPosition(container.getLoadPosition());
        this.setLoadTime(container.getLoadTime());
        this.setCreatedDate(new Date());
        this.setExportStatus(container.getExportStatus());
        
        // Operator can be null when we have a cancelled container
        if (container.getOperator() != null) {
            this.setOperatorName(container.getOperator().getName());
            this.setOperatorIdentifier(container.getOperator()
                .getOperatorIdentifier());
        } else {
            this.setOperatorName(null);
            this.setOperatorIdentifier(null);
        }
        
        for (Tag t : container.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the stop
     */
    public ArchiveLoadingStop getStop() {
        return stop;
    }

    /**
     * @param stop the stop to set
     */
    public void setStop(ArchiveLoadingStop stop) {
        this.stop = stop;
    }

    /**
     * @return the masterContainerNumber
     */
    public String getMasterContainerNumber() {
        return masterContainerNumber;
    }

    /**
     * @param masterContainerNumber the masterContainerNumber to set
     */
    public void setMasterContainerNumber(String masterContainerNumber) {
        this.masterContainerNumber = masterContainerNumber;
    }

    /**
     * @return the type
     */
    public LoadingContainerType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(LoadingContainerType type) {
        this.type = type;
    }

    /**
     * @return the status
     */
    public LoadingContainerStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(LoadingContainerStatus status) {
        this.status = status;
    }

    /**
     * @return the expectedCube
     */
    public Double getExpectedCube() {
        return expectedCube;
    }

    /**
     * @param expectedCube the expectedCube to set
     */
    public void setExpectedCube(Double expectedCube) {
        this.expectedCube = expectedCube;
    }

    /**
     * @return the expectedWeight
     */
    public Double getExpectedWeight() {
        return expectedWeight;
    }

    /**
     * @param expectedWeight the expectedWeight to set
     */
    public void setExpectedWeight(Double expectedWeight) {
        this.expectedWeight = expectedWeight;
    }


    
    /**
     * @return the operatorIdentifier
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    
    /**
     * @param operatorIdentifier the operatorIdentifier to set
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    
    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    
    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the loadPosition
     */
    public Integer getLoadPosition() {
        return loadPosition;
    }

    /**
     * @param loadPosition the loadPosition to set
     */
    public void setLoadPosition(Integer loadPosition) {
        this.loadPosition = loadPosition;
    }

    /**
     * @return the loadTime
     */
    public Date getLoadTime() {
        return loadTime;
    }

    /**
     * @param loadTime the loadTime to set
     */
    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }

    /**
     * @param site the site to set
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ArchiveLoadingContainerRoot)) {
            return false;
        }
        final ArchiveLoadingContainerRoot other = (ArchiveLoadingContainerRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }

        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().intValue());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 