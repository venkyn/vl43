/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;

import java.util.Set;

/**
 * Loading Regions Model Object.
 * 
 * @author kudupi
 */
public class LoadingRegionRoot extends Region {

    /**
     * 
     */
    private static final long serialVersionUID = 6255254869416122450L;

    // Determines the type of Issuance (Manual - 1)
    private LoadingRouteIssuance loadingRouteIssuance = 
        LoadingRouteIssuance.Manual;
    
    // This value has to set to something that does not fail the import
    // validation if the object is initialized without new
    private static final int DIGITS_OPER_SPEAKS = 0;
    
    // The number of digits an operator must speak to specify a license plate number.
    private Integer             containerIDDigitsOperSpeaks = DIGITS_OPER_SPEAKS;
    
    // The number of digits operator must speak to capture a trailer ID.
    private Integer             trailerIDDigitsOperSpeaks = DIGITS_OPER_SPEAKS;
    
    // The maximum number of licenses that can be consolidated at a time.
    private Integer             maxConsolidationOfContainers;

    //String representation of region number
    private String      regionNumberStr;
    
  //Set of assignments within region
    private Set<LoadingRoute>  loadingRoutes;    
    
    /**
     * Getter for LoadingRoutes set
     * @return the loadingRoutes
     */
    public Set<LoadingRoute> getLoadingRoutes() {
        return loadingRoutes;
    }

    
    /**
     * Setter for LoadingRoutes set
     * @param loadingRoutes the loadingRoutes to set
     */
    public void setLoadingRoutes(Set<LoadingRoute> loadingRoutes) {
        this.loadingRoutes = loadingRoutes;
    }

    /**
     * Getter for LoadingRouteIssuance
     * @return loadingRouteIssuance enum value.
     */
    public LoadingRouteIssuance getLoadingRouteIssuance() {
        return loadingRouteIssuance;
    }

    /**
     * Setter for LoadingRouteIssuance
     * @param loadingRouteIssuance Loading Route Issuance enum value.
     */
    public void setLoadingRouteIssuance(LoadingRouteIssuance loadingRouteIssuance) {
        this.loadingRouteIssuance = loadingRouteIssuance;
    }

    /**
     * Getter for containerIDDigitsOperSpeaks
     * @return containerIDDigitsOperSpeaks integer value
     */
    public Integer getContainerIDDigitsOperSpeaks() {
        return containerIDDigitsOperSpeaks;
    }

    /**
     * Setter for containerIDDigitsOperSpeaks
     * @param containerIDDigitsOperSpeaks integer.
     */
    public void setContainerIDDigitsOperSpeaks(Integer containerIDDigitsOperSpeaks) {
        this.containerIDDigitsOperSpeaks = containerIDDigitsOperSpeaks;
    }

    /**
     * Getter for TrailerID Digits Operator Speaks.
     * @return trailerIDDigitsOperSpeaks integer value
     */
    public Integer getTrailerIDDigitsOperSpeaks() {
        return trailerIDDigitsOperSpeaks;
    }

    /**
     * Setter for trailerIDDigitsOperSpeaks
     * @param trailerIDDigitsOperSpeaks integer.
     */
    public void setTrailerIDDigitsOperSpeaks(Integer trailerIDDigitsOperSpeaks) {
        this.trailerIDDigitsOperSpeaks = trailerIDDigitsOperSpeaks;
    }

    /**
     * Getter for maxConsolidationOfContainers
     * @return maxConsolidationOfContainers integer value
     */
    public Integer getMaxConsolidationOfContainers() {
        return maxConsolidationOfContainers;
    }

    /**
     * Setter for maxConsolidationOfContainers
     * @param maxConsolidationOfContainers integer value
     */
    public void setMaxConsolidationOfContainers(Integer maxConsolidationOfContainers) {
        this.maxConsolidationOfContainers = maxConsolidationOfContainers;
    }

    /**
     * Method to return region number as an object depending upon the current
     * value. This is called from the import framework while importing
     * assignments to cater both the scnearios when the Loading Region is
     * present or not.
     * @return the regionNumberStr
     */
    public Object getRegionNumberStr() {
        if (getNumber() == null || getNumber().toString().equals("0")) {
            return "";
        } else {
            return getNumber();
        }
    }

    
    /**
     * @param regionNumberStr the regionNumberStr to set
     */
    public void setRegionNumberStr(String regionNumberStr) {
        if (StringUtil.isNullOrEmpty(regionNumberStr)
            || regionNumberStr.equals("0")) {
            this.setNumber(0);
        } else {
            this.setNumber(new Integer(regionNumberStr.trim()));
        }
        this.regionNumberStr = regionNumberStr;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 