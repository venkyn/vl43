/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorLabor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author mraj
 * 
 */
public class ArchiveLoadingLaborRoot extends ArchiveModelObject implements
    Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -4961288854182475321L;

    private ArchiveLoadingRoute route;

    private String operatorIdentifier;

    private String operatorName;

    private OperatorLabor operatorLabor;

    private Long breakLaborId;

    private Date startTime;

    private Date endTime;

    private Long duration;

    private Integer containersLoaded;

    private Double actualRate;

    private Double percentOfGoal;

    private ExportStatus exportStatus;

    /**
     * @param labor Loading labor object to be archived
     * @param arRoute Archive Loading Route object 
     */
    public ArchiveLoadingLaborRoot(LoadingRouteLabor labor,
                                   ArchiveLoadingRoute arRoute) {
        this.setRoute(arRoute);
        this.setOperatorIdentifier(labor.getOperator().getOperatorIdentifier());
        this.setOperatorName(labor.getOperator().getName());
        this.setBreakLaborId(labor.getBreakLaborId());
        this.setStartTime(labor.getStartTime());
        this.setEndTime(labor.getEndTime());
        this.setDuration(labor.getDuration());
        this.setContainersLoaded(labor.getContainersLoaded());
        this.setActualRate(labor.getActualRate());
        this.setPercentOfGoal(labor.getPercentOfGoal());
        this.setExportStatus(labor.getExportStatus());
    }

    /**
     * @return the route
     */
    public ArchiveLoadingRoute getRoute() {
        return route;
    }

    /**
     * @param route the route to set
     */
    public void setRoute(ArchiveLoadingRoute route) {
        this.route = route;
    }

    /**
     * @return the operatorIdentifier
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    /**
     * @param operatorIdentifier the operatorIdentifier to set
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the operatorLabor
     */
    public OperatorLabor getOperatorLabor() {
        return operatorLabor;
    }

    /**
     * @param operatorLabor the operatorLabor to set
     */
    public void setOperatorLabor(OperatorLabor operatorLabor) {
        this.operatorLabor = operatorLabor;
    }

    
    /**
     * @return the breakLaborId
     */
    public Long getBreakLaborId() {
        return breakLaborId;
    }

    
    /**
     * @param breakLaborId the breakLaborId to set
     */
    public void setBreakLaborId(Long breakLaborId) {
        this.breakLaborId = breakLaborId;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * @return the containersLoaded
     */
    public Integer getContainersLoaded() {
        return containersLoaded;
    }

    /**
     * @param containersLoaded the containersLoaded to set
     */
    public void setContainersLoaded(Integer containersLoaded) {
        this.containersLoaded = containersLoaded;
    }

    /**
     * @return the actualRate
     */
    public Double getActualRate() {
        return actualRate;
    }

    /**
     * @param actualRate the actualRate to set
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    /**
     * @return the percentOfGoal
     */
    public Double getPercentOfGoal() {
        return percentOfGoal;
    }

    /**
     * @param percentOfGoal the percentOfGoal to set
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ArchiveLoadingLaborRoot)) {
            return false;
        }
        final ArchiveLoadingLaborRoot other = (ArchiveLoadingLaborRoot) obj;
        if (getOperatorIdentifier() == null) {
            if (other.getOperatorIdentifier() != null) {
                return false;
            }
        } else if (!getOperatorIdentifier().equals(
            other.getOperatorIdentifier())) {
            return false;
        }
        if (getStartTime() == null) {
            if (other.getStartTime() != null) {
                return false;
            }
        } else if (!getStartTime().equals(other.getStartTime())) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
            * result
            + ((getOperatorIdentifier() == null) ? 0 : getOperatorIdentifier()
                .hashCode());
        result = prime * result
            + ((startTime == null) ? 0 : startTime.hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 