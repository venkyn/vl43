/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

/**
 * @author mraj
 * 
 */
public class LoadingTruckDiagramRowRoot {

    private String routeNumber;

    private String trailer;

    private String dockDoor;

    private Double truckWeight;

    private String containerNumberLeft;

    private String customerNumberLeft;

    private Double containerWeightLeft;

    private String containerNumberRight;

    private String customerNumberRight;

    private Double containerWeightRight;

    private Integer containerPosition;

    /**
     * Constructor.
     */
    public LoadingTruckDiagramRowRoot() {
    }

    /**
     * Constructor.
     * @param routeNumber number of the route
     * @param trailer trailer number of the route
     * @param dockDoor dock door location of the route
     * @param truckWeight summation of all the containers loaded on the truck
     * @param containerNumberLeft container number
     * @param customerNumberLeft customer number
     * @param containerWeightLeft container weight
     * @param containerPosition loaded container position
     */
    public LoadingTruckDiagramRowRoot(String routeNumber,
                                      String trailer,
                                      String dockDoor,
                                      Double truckWeight,
                                      String containerNumberLeft,
                                      String customerNumberLeft,
                                      Double containerWeightLeft,
                                      Integer containerPosition) {
        this.routeNumber = routeNumber;
        this.trailer = trailer;
        this.dockDoor = dockDoor;
        this.truckWeight = truckWeight;
        this.containerNumberLeft = containerNumberLeft;
        this.customerNumberLeft = customerNumberLeft;
        this.containerWeightLeft = containerWeightLeft;
        this.containerPosition = containerPosition;
    }

    /**
     * @return the routeNumber
     */
    public String getRouteNumber() {
        return routeNumber;
    }

    /**
     * @param routeNumber the routeNumber to set
     */
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    /**
     * @return the trailer
     */
    public String getTrailer() {
        return trailer;
    }

    /**
     * @param trailer the trailer to set
     */
    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    /**
     * @return the dockDoor
     */
    public String getDockDoor() {
        return dockDoor;
    }

    /**
     * @param dockDoor the dockDoor to set
     */
    public void setDockDoor(String dockDoor) {
        this.dockDoor = dockDoor;
    }

    /**
     * @return the truckWeight
     */
    public Double getTruckWeight() {
        return truckWeight;
    }

    /**
     * @param truckWeight the truckWeight to set
     */
    public void setTruckWeight(Double truckWeight) {
        this.truckWeight = truckWeight;
    }

    /**
     * @return the containerNumberLeft
     */
    public String getContainerNumberLeft() {
        return containerNumberLeft;
    }

    /**
     * @param containerNumberLeft the containerNumberLeft to set
     */
    public void setContainerNumberLeft(String containerNumberLeft) {
        this.containerNumberLeft = containerNumberLeft;
    }

    /**
     * @return the customerNumberLeft
     */
    public String getCustomerNumberLeft() {
        return customerNumberLeft;
    }

    /**
     * @param customerNumberLeft the customerNumberLeft to set
     */
    public void setCustomerNumberLeft(String customerNumberLeft) {
        this.customerNumberLeft = customerNumberLeft;
    }

    /**
     * @return the getContainerWeightLeft
     */
    public Double getContainerWeightLeft() {
        return containerWeightLeft;
    }

    /**
     * @param containerWeightLeft the containerWeightLeft to set
     */
    public void setContainerWeightLeft(Double containerWeightLeft) {
        this.containerWeightLeft = containerWeightLeft;
    }

    /**
     * Getter for the containerNumberRight property.
     * @return String value of the property
     */
    public String getContainerNumberRight() {
        return containerNumberRight;
    }

    /**
     * Setter for the containerNumberRight property.
     * @param containerNumberRight the new containerNumberRight value
     */
    public void setContainerNumberRight(String containerNumberRight) {
        this.containerNumberRight = containerNumberRight;
    }

    /**
     * Getter for the customerNumberRight property.
     * @return String value of the property
     */
    public String getCustomerNumberRight() {
        return customerNumberRight;
    }

    /**
     * Setter for the customerNumberRight property.
     * @param customerNumberRight the new customerNumberRight value
     */
    public void setCustomerNumberRight(String customerNumberRight) {
        this.customerNumberRight = customerNumberRight;
    }

    /**
     * Getter for the containerWeightRight property.
     * @return Double value of the property
     */
    public Double getContainerWeightRight() {
        return containerWeightRight;
    }

    /**
     * Setter for the containerWeightRight property.
     * @param containerWeightRight the new containerWeightRight value
     */
    public void setContainerWeightRight(Double containerWeightRight) {
        this.containerWeightRight = containerWeightRight;
    }

    /**
     * @return the containerPosition
     */
    public Integer getContainerPosition() {
        return containerPosition;
    }

    /**
     * @param containerPosition the containerPosition to set
     */
    public void setContainerPosition(Integer containerPosition) {
        this.containerPosition = containerPosition;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 