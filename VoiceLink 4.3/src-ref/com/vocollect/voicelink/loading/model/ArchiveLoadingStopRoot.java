/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Model object representing a Archive Loading Stop.
 * @author mraj
 *
 */
public class ArchiveLoadingStopRoot extends ArchiveModelObject implements
    Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = -71496067670798942L;

    private Long number;
    
    private ArchiveLoadingRoute route;
    
    private Integer expectedNumberOfContainers;
    
    private int actualNumberOfContainers;
    
    private LoadingStopStatus status;
    
    private Date startTime;
    
    private Date endTime;
    
    private String customerNumber;
    
    private boolean captureLoadPosition;
    
    private String note;
    
    private ExportStatus exportStatus;
    
    private Date createdDate;
    
    private List <ArchiveLoadingContainer> containers = new ArrayList<ArchiveLoadingContainer>();
    
    private Site site;
    
   
    /**
     * @param stop Loading Stop object to be archived
     * @param arRoute associated Archive Loading route object 
     */
    public ArchiveLoadingStopRoot(LoadingStop stop, ArchiveLoadingRoute arRoute) {
        
        for (LoadingContainer container : stop.getContainers()) {
            getContainers().add(new ArchiveLoadingContainer(container, (ArchiveLoadingStop) this));
        }
        
        for (Tag t : stop.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }
        
        this.setNumber(stop.getNumber());
        this.setRoute(arRoute);
        this.setExpectedNumberOfContainers(stop.getExpectedNumberOfContainers());
        this.setActualNumberOfContainers(stop.getActualNumberOfContainers());
        this.setStatus(stop.getStatus());
        this.setStartTime(stop.getStartTime());
        this.setEndTime(stop.getEndTime());
        this.setCustomerNumber(stop.getCustomerNumber());
        this.setNote(stop.getNote());
        this.setExportStatus(stop.getExportStatus());
        this.setCreatedDate(new Date());
    }


    /**
     * @return the number
     */
    public Long getNumber() {
        return number;
    }

    
    /**
     * @param number the number to set
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    
    /**
     * @return the route
     */
    public ArchiveLoadingRoute getRoute() {
        return route;
    }

    
    /**
     * @param route the route to set
     */
    public void setRoute(ArchiveLoadingRoute route) {
        this.route = route;
    }

    
    /**
     * @return the expectedNumberOfContainers
     */
    public Integer getExpectedNumberOfContainers() {
        return expectedNumberOfContainers;
    }

    
    /**
     * @param expectedNumberOfContainers the expectedNumberOfContainers to set
     */
    public void setExpectedNumberOfContainers(Integer expectedNumberOfContainers) {
        this.expectedNumberOfContainers = expectedNumberOfContainers;
    }

    
    /**
     * @return the actualNumberOfContainers
     */
    public int getActualNumberOfContainers() {
        return actualNumberOfContainers;
    }

    
    /**
     * @param actualNumberOfContainers the actualNumberOfContainers to set
     */
    public void setActualNumberOfContainers(int actualNumberOfContainers) {
        this.actualNumberOfContainers = actualNumberOfContainers;
    }

    
    /**
     * @return the status
     */
    public LoadingStopStatus getStatus() {
        return status;
    }

    
    /**
     * @param status the status to set
     */
    public void setStatus(LoadingStopStatus status) {
        this.status = status;
    }

    
    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    
    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    
    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    
    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    
    /**
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    
    /**
     * @param customerNumber the customerNumber to set
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    
    /**
     * @return the captureLoadPosition
     */
    public boolean isCaptureLoadPosition() {
        return captureLoadPosition;
    }

    
    /**
     * @param captureLoadPosition the captureLoadPosition to set
     */
    public void setCaptureLoadPosition(boolean captureLoadPosition) {
        this.captureLoadPosition = captureLoadPosition;
    }

    
    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }

    
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }

    
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    
    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    
    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the containers
     */
    public List<ArchiveLoadingContainer> getContainers() {
        return containers;
    }


    /**
     * @param containers the containers to set
     */
    public void setContainers(List<ArchiveLoadingContainer> containers) {
        this.containers = containers;
    }


    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }

    
    /**
     * @param site the site to set
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ArchiveLoadingStopRoot other = (ArchiveLoadingStopRoot) obj;
        if (getId() != other.getId()) {
            return false;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((route == null) ? 0 : route.hashCode());
        result = prime * result + ((number == null) ? 0 : number.hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 