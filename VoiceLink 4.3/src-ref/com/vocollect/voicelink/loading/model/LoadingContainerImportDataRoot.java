/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

import java.util.Date;


/**
 * Loading Container data to be imported
 * @author mraj
 *
 */
public class LoadingContainerImportDataRoot extends BaseModelObject implements
    Importable {
    
    
    /**
     * 
     */
    private static final long serialVersionUID = 6665690492808532867L;
    
    private ImportableImpl impl    = new ImportableImpl();
    
    private LoadingContainer     myModel = new LoadingContainer();
    
    private LoadingStopImportData parent = null;

    /**
     * Set the number in the region. This will be used to look up a region in
     * the database.
     * @param number the number to set the regionnumber to. Data type of this
     *            argument is set to Long so that database field is created as
     *            numeric(19,0) instead of int. Advantage of numeric over int is
     *            that in case of SQL server value of '' would be treated as
     *            NULL and rejected. An int datatype inserts 0 by default for ''
     *            value
     */
    public void setRegionNumber(Long number) {
        LoadingRegion tempRegion = null;
        if (getStop() != null && getStop().getRoute() != null
            && getStop().getRoute().getRegion() != null) {
            tempRegion = getStop().getRoute().getRegion();
        }

        if (null == tempRegion) {
            tempRegion = new LoadingRegion();
            getStop().getRoute().setRegion(tempRegion);
        }

        tempRegion.setNumber(new Integer(number.intValue()));
    }

    /**
     * Get the region number.
     * @return the region number as a string. Data type of return value is set
     *         to Long so that database field is created as numeric(19,0)
     *         instead of int. Advantage of numeric over int is that in case of
     *         SQL server value of '' would be treated as NULL and rejected. An
     *         int datatype inserts 0 by default for '' value
     */
    public Long getRegionNumber() {
        LoadingRegion tempRegion = null;
        if (getStop() != null && getStop().getRoute() != null
            && getStop().getRoute().getRegion() != null) {
            tempRegion = getStop().getRoute().getRegion();
        } else {
            return null;
        }

        if (null == tempRegion || null == tempRegion.getNumber()) {
            return null;
        }
        return new Long(tempRegion.getNumber());
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#getDepartureDateTime()
     */
    public Date getDepartureDateTime() {
        if (getStop() != null && getStop().getRoute() != null) {
            return getStop().getRoute().getDepartureDateTime();
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingRouteRoot#setDepartureDateTime(java.util.Date)
     */
    public void setDepartureDateTime(Date departureDateTime) {
        if (getStop() != null && getStop().getRoute() != null) {
            getStop().getRoute()
                .setDepartureDateTime(departureDateTime);
        }
    }
    
    /**
     * Set the route number. This will be used to look up a route in the database.
     * @param number the number to set the routenumber to.
     */
    public void setRouteNumber(String number) {
        if (getStop() != null) {
            LoadingRoute route = new LoadingRoute();
            route.setNumber(number);
            getStop().setRoute(route);
        }
    }

    /**
     * Get the Route number.
     * @return the route number as a string.
     */
    public String getRouteNumber() {
        if (getStop() != null && getStop().getRoute() != null) {
            return getStop().getRoute().getNumber();
        } else {
            return null;
        }
    }
    
    /**
     * Set the stop number. This will be used to look up a stop in the database.
     * @param number the number to set the stop number to.
     */
    public void setStopNumber(Long number) {
        LoadingStop stop = getStop();
        if (null == stop) {
            stop = new LoadingStop();
            setStop(stop);
        }
        stop.setNumber(number);
    }

    /**
     * Get the Stop number.
     * @return the stop number as a string.
     */
    public Long getStopNumber() {
        if (myModel.getStop() != null) {
            return myModel.getStop().getNumber();
        } else {
            return null;
        }
    }
    
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isInProgress()
     */
    @Override
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setInProgress(boolean)
     */
    @Override
    public void setInProgress(boolean inProgress) {
        impl.setInProgress(inProgress);

    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setCompleted(boolean)
     */
    @Override
    public void setCompleted(boolean completed) {
        impl.setCompleted(completed);

    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportStatus()
     */
    @Override
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportStatus(int)
     */
    @Override
    public void setImportStatus(int status) {
        impl.setImportStatus(status);

    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#convertToModel()
     */
    @Override
    public Object convertToModel() throws VocollectException {
        return myModel;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getImportID()
     */
    @Override
    public Long getImportID() {
        return impl.getImportID();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setImportID(java.lang.Long)
     */
    @Override
    public void setImportID(Long importID) {
        impl.setImportID(importID);

    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#getSiteName()
     */
    @Override
    public String getSiteName() {
        return impl.getSiteName();
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setSiteName(java.lang.String)
     */
    @Override
    public void setSiteName(String siteName) {
        impl.setSiteName(siteName);

    }

    
    /**
     * @return the parent
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#getParent()
     */
    public LoadingStopImportData getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#setParent(com.vocollect.voicelink.loading.model.LoadingStopImportData)
     */
    public void setParent(LoadingStopImportData parent) {
        this.parent = parent;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#getId()
     */
    public Long getId() {
        return myModel.getId();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingStopImportDataRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }



    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return myModel.toString();
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#getStop()
     */
    public LoadingStop getStop() {
        return myModel.getStop();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#setStop(com.vocollect.voicelink.loading.model.LoadingStop)
     */
    public void setStop(LoadingStop stop) {
        myModel.setStop(stop);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#getContainerNumber()
     */
    public String getContainerNumber() {
        return myModel.getContainerNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#setContainerNumber(java.lang.String)
     */
    public void setContainerNumber(String containerNumber) {
        myModel.setContainerNumber(containerNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#getExpectedCube()
     */
    public Double getExpectedCube() {
        return myModel.getExpectedCube();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#setExpectedCube(java.lang.Double)
     */
    public void setExpectedCube(Double expectedCube) {
        myModel.setExpectedCube(expectedCube);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#getExpectedWeight()
     */
    public Double getExpectedWeight() {
        return myModel.getExpectedWeight();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.model.LoadingContainerRoot#setExpectedWeight(java.lang.Double)
     */
    public void setExpectedWeight(Double expectedWeight) {
        myModel.setExpectedWeight(expectedWeight);
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    public boolean isNew() {
        return myModel.isNew();
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 