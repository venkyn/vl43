/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 * Loading Request Route Command Implementation.
 * 
 * @author kudupi
 */
public class LoadingRequestRouteCmdRoot extends BaseLoadingTaskCommand {
    
    /**
     * 
     */
    private static final long serialVersionUID = 6256404012788127196L;

    private LoadingRegion        loadingRegion;
    
    private LaborManager         laborManager;
    
    private String               routeNumber;
    
    private LoadingRoute         loadingRoute;
    
    private static final int     SUMMARY_PROMPT_ARGS = 3;
    
    private static final Semaphore SYNC_SINGLE_THREAD = new Semaphore(1, true);
    
    /**
     * Getter for routeNumber.
     * @return routeNumber String.
     */
    public String getRouteNumber() {
        return routeNumber;
    }

    /**
     * Setter for routeNumber.
     * @param routeNumber String.
     */
    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }
    
    /**
     * Getter for LoadingRoute.
     * @return loadingRoute.
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }

    /**
     * Setter for LoadingRoute.
     * @param loadingRoute LoadingRoute.
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }


    /**
     * Getter for LoadingRegion.
     * @return loadingRegion LoadingRegion.
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }

    /**
     * Setter for LoadingRegion.
     * @param loadingRegion LoadingRegion.
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }

    /**
     * Getter for LaborManager.
     * @return laborManager LaborManager.
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for LaborManager.
     * @param laborManager LaborManager.
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    @Override
    protected Response doExecute() throws Exception {
        // Actions
        setLoadingRegion(getLoadingRegionManager().get(getOperator()
                .getCurrentRegion().getId()));
        getLoadingRouteObj();
        validateLoadingRouteExists();
        updateLoadingRoute();
        openLaborRecord();

        // Response
        buildResponse();
        return getResponse();
    }

    /**
     * Gets the LoadingRoute object.
     *
     * @throws DataAccessException - database exception
     * @throws InterruptedException 
     */
    protected void getLoadingRouteObj() throws DataAccessException, InterruptedException {
         
            Integer regionNumber = getLoadingRegion().getNumber();
            List<LoadingRoute> lr = null;
            // This part of logic is for Manual Issuance of Routes.
            if ((getRouteNumber() != null) && (!getRouteNumber().equals(""))) {
                lr = getLoadingRouteManager()
                        .listLoadingRouteByNumber(getRouteNumber(), regionNumber);
                if (lr.isEmpty()) {
                    setLoadingRoute(null);
                } else {
                    setLoadingRoute(lr.get(0));
                }
            } else { // This part of logic is for Automatic Issuance of Routes.
                // For VoiceLink4.1 we are not implementing Automatic Issuance of Routes.
                SYNC_SINGLE_THREAD.acquire();
                try {
                    lr = getLoadingRouteManager().listAvailableLoadingRoute(regionNumber);
                    if (lr.isEmpty()) {
                        setLoadingRoute(null);
                    } else {
                        setLoadingRoute(lr.get(0));
                    }
                } finally {
                    SYNC_SINGLE_THREAD.release();
                   }
                }
    }
    
    /**
     * Validates that the requested loadingRoute exists.
     *
     * @throws TaskCommandException - task exception
     */
    protected void validateLoadingRouteExists() throws TaskCommandException {
        if (getLoadingRoute() == null) {
            // Throw loading route not found error
            throw new TaskCommandException(
                TaskErrorCode.LOADING_ROUTE_NOT_FOUND, getRouteNumber());
        }
    }
    
    /**
     * Updates the Loading Route.
     * @throws DataAccessException - on db exception
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void updateLoadingRoute() throws DataAccessException
        , BusinessRuleException {
        getLoadingRouteManager().executeChangeOperationForOperator(getOperator());
        LoadingRoute loadingRouteObj = getLoadingRoute();
        loadingRouteObj.setStatus(LoadingRouteStatus.InProgress);
        Set<Operator> operators = loadingRouteObj.getOperators();
        operators.add(getOperator());
        loadingRouteObj.setOperators(operators);
        if (loadingRouteObj.getStartTime() == null) {
            loadingRouteObj.setStartTime(getCommandTime());
        }
    }

    /**
     * updates the operator's labor records.
     * 
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     */
    protected void openLaborRecord() throws DataAccessException, BusinessRuleException {
        // create loading labor record for each route
        this.getLaborManager().openLoadingLaborRecord(
            this.getCommandTime(), this.getOperator(), getLoadingRoute());
    }
    
    /**
     * build the response object to send back to terminal.
     * @throws DataAccessException - Database Exception
     */
    protected void buildResponse() throws DataAccessException {
        // Build response
        getResponse().addRecord(buildResponseRecord(getLoadingRoute()));
    }
    
    /**
     * Builds the response record.
     * 
     * @param lr - the loading route to use for the response
     * @return record - the response record
     * @throws DataAccessException - Database Exception
     */
    protected ResponseRecord buildResponseRecord(LoadingRoute lr)
        throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("routeID", lr.getId());
        record.put("routeNumber", lr.getNumber());
        if (lr.getTrailer() != null && lr.getTrailer() != "") {
            record.put("captureTrailerID", "1");
            record.put("trailerID", lr.getTrailer().substring(lr.getTrailer().length() - 
                    lr.getRegion().getTrailerIDDigitsOperSpeaks()));
        } else {
            record.put("captureTrailerID", "0");
            record.put("trailerID", "");
        }
        record.put("doorNumber", lr.getDockDoor().getScannedVerification());
        if ((lr.getDockDoor().getCheckDigits() != null) && 
                (!lr.getDockDoor().getCheckDigits().equals(""))) {
            record.put("doorCheckDigits", lr.getDockDoor().getCheckDigits());
        } else {
            record.put("doorCheckDigits", "");
        }
        record.put("summaryPrompt", buildSummaryPrompt(lr));
        return record;
    }
    
    
    /**
     * Method to build the Summary Prompt.
     * @param lr loadingRoute object.
     * @return summaryPrompt String.
     */
    private String buildSummaryPrompt(LoadingRoute lr) throws DataAccessException {
        String summaryPrompt = "";
        Object[] args = new Object[SUMMARY_PROMPT_ARGS];
        args[0] = lr.getNumber();
        args[1] = lr.getStops().size();
        args[2] = lr.getAvailableStopCount();
        summaryPrompt = summaryPrompt + ResourceUtil.getLocalizedMessage("task.loading.requestroute.summaryprompt", 
                args, getTerminal().getTaskLocale());
        return summaryPrompt;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 