/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * Loading Valid Regions Task Command Class.
 * 
 * @author kudupi
 */
public class LoadingValidRegionsCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -7107863995290414087L;
    
    private RegionManager regionManager;
    
    private TaskFunctionManager taskFunctionManager;
    
    private List<Region> returnRegions;
    
    /**
     * Getter for regionManager.
     * @return regionManager RegionManager.
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for regionManager.
     * @param regionManager RegionManager.
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for taskFunctionManager.
     * @return taskFunctionManager TaskFunctionManager.
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    /**
     * Setter for taskFunctionManager.
     * @param taskFunctionManager TaskFunctionManager.
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * Getter for returnRegions.
     * @return returnRegions List<Regions>.
     */
    public List<Region> getReturnRegions() {
        return returnRegions;
    }

    /**
     * Setter for returnRegions.
     * @param returnRegions List<Regions>.
     */
    public void setReturnRegions(List<Region> returnRegions) {
        this.returnRegions = returnRegions;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //Get a list of authorized regions
        getAuthorizedRegions();
        resetOperatorInfo();
        executeChangeRegionForOperator();
        buildResponse();
        // set the operators work type
        setOperatorWorkType();
        return getResponse();
    }
    
    /**
     * Get List of region authorized to work in.
     * 
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - on failure to find any authorized regions for
     *                                the given operator.
     **/
    protected void getAuthorizedRegions() throws DataAccessException, TaskCommandException {
        
        // Get list of authorized regions
        this.setReturnRegions(getRegionManager().listAuthorized(
                TaskFunctionType.Loading,
                getOperator().getWorkgroup().getId()));
        
      //If no regions found then throw error not no regions found for function
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION,
                    ResourceUtil.getLocalizedKeyValue(TaskFunctionType.Loading.getResourceKey()));
        }
    }
    
    /**
     * Set operators work type to Cycle Counting, clears previous regions and
     * starts Cycle Counting labor record.
     * 
     * @throws DataAccessException
     *             - database exceptions
     */
    protected void resetOperatorInfo() throws DataAccessException {
        // Get task function for cycle counting
        TaskFunction taskFunction = getTaskFunctionManager().findByType(
                TaskFunctionType.Loading);
        getOperator().setCurrentWorkType(taskFunction);
        
        // clear operators regions before allowing them to choose.
        // since operators can not be assigned to a region via the UI
        // we need to set the assigned region property to null
        getOperator().setAssignedRegion(null);
        getOperator().setCurrentRegion(null);
        getOperator().getRegions().clear();
    }
    
    /**
     * Method to update the Route statuses on Change Region.
     * If Multiple operators are on a Route, then Route remains in
     * In Progress state. If Single operator is working on a Route,
     * Route status is updated to Suspended.
     */
    protected void executeChangeRegionForOperator() throws DataAccessException, 
    BusinessRuleException {
        getLoadingRouteManager()
        .executeChangeOperationForOperator(getOperator());
    }

    /**
     * Populate the response.
     * 
     * @throws DataAccessException
     *             - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (Region r : getReturnRegions()) {
            // Create a return record, populate it, and add to response
            getResponse().addRecord(buildResponseRecord(r));
        }
    }


    /**
     * Build Response Record.
     * 
     * @param r - region to build record for
     * @return - response record
     * @throws DataAccessException - Database exceptions
     */
    protected ResponseRecord buildResponseRecord(Region r)
            throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        return record;
    }

    /**
     * Assign the operator to the putaway work type for UI display.
     * @throws DataAccessException - on database exception
     */
    protected void setOperatorWorkType() throws DataAccessException {
        TaskFunction taskFunction =
            this.getTaskFunctionManager().findByType(TaskFunctionType.Loading);
        this.getOperator().setCurrentWorkType(taskFunction);

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 