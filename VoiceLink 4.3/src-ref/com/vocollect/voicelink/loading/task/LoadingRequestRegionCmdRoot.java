/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;

import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * Loading Request Region Command Implementation.
 * 
 * @author kudupi
 */
public class LoadingRequestRegionCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = 5022064612795887544L;
    
    private Integer regionNumber;
    
    private RegionManager regionManager;
    
    private TaskFunctionManager taskFunctionManager;

    private LaborManager laborManager;
    
    /**
     * Getter for regionNumber.
     * @return the regionNumber
     */
    public Integer getRegionNumber() {
        return regionNumber;
    }

    /**
     * Setter for regionNumber.
     * @param regionNumber
     *            the regionNumber to set
     */
    public void setRegionNumber(Integer regionNumber) {
        this.regionNumber = regionNumber;
    }
    
    /**
     * Getter for regionManager.
     * @return the regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for regionManager.
     * @param regionManager
     *            the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for taskFunctionManager.
     * @return the taskFunctionManager
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    /**
     * Setter for taskFunctionManager.
     * @param taskFunctionManager
     *            the taskFunctionManager to set
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * @param laborManager the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        validateAndSaveRequest();
        buildResponse();
        return getResponse();
    }
    
    /**
     * Populate the response to be sent back to the device.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Create and return a response record.
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }
    
    /**
     * updates the operator's labor records.
     * 
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     */
    protected void openLaborRecord() throws DataAccessException, BusinessRuleException {
        
    
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                                                       this.getOperator(),
                                                       OperatorLaborActionType.Loading);
                                               
    }
    /**
     * Validate and signs operator into regions.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Database Exception
     */
    protected void validateAndSaveRequest()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        LoadingRegion region = verifyPassedInRegion();

        List<Region> authRegions = getRegionManager().listAuthorized(
            TaskFunctionType.Loading, getOperator().getWorkgroup().getId());

        if (authRegions.contains(region)) {
            getOperator().getRegions().add(region);
//            this.getOperator().setCurrentRegion(region);
        } else {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION,
                TaskFunctionType.Loading, getRegionNumber());
        }
    }

    /**
     * Verifies that the passed in region is valid.
     * 
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     * @throws BusinessRuleException - Business Rule Exception
     * @return - valid loading region
     */
    protected LoadingRegion verifyPassedInRegion()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        LoadingRegion region = getLoadingRegionManager()
            .findRegionByNumber(getRegionNumber());

        if (region == null) {
            // Raise region not found error
            throw new TaskCommandException(TaskErrorCode.INVALID_REGION_NUMBER, getRegionNumber());
        }

        return region;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 