/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Loading Request Container Command Implementation.
 * 
 * @author kudupi
 */
public class LoadingRequestContainerCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -4949927480085520594L;

    private LoadingRegion loadingRegion;

    private LaborManager laborManager;

    private String containerNumber;

    private String status;

    private String partial;

    private LoadingRoute loadingRoute;

    private LoadingStop loadingStop;

    private List<LoadingContainer> listLoadingContainers;

    /**
     * Getter for List Loading Containers.
     * 
     * @return listLoadingContainers.
     */
    public List<LoadingContainer> getListLoadingContainers() {
        return listLoadingContainers;
    }

    /**
     * Setter for List Loading Containers.
     * 
     * @param listLoadingContainers
     *            LoadingContainers list.
     */
    public void setListLoadingContainers(
            List<LoadingContainer> listLoadingContainers) {
        this.listLoadingContainers = listLoadingContainers;
    }

    /**
     * Getter for loadingRoute
     * 
     * @return loadingRoute LoadingRoute.
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }

    /**
     * Setter for loadingRoute
     * 
     * @param loadingRoute
     *            LoadingRoute.
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }

    /**
     * Getter for LoadingRegion.
     * 
     * @return loadingRegion LoadingRegion.
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }

    /**
     * Getter for LoadingStop.
     * 
     * @return loadingStop LoadingStop.
     */
    public LoadingStop getLoadingStop() {
        return loadingStop;
    }

    /**
     * Setter for LoadingStop.
     * 
     * @param loadingStop
     *            LoadingStop.
     */
    public void setLoadingStop(LoadingStop loadingStop) {
        this.loadingStop = loadingStop;
    }

    /**
     * Setter for LoadingRegion.
     * 
     * @param loadingRegion
     *            LoadingRegion.
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }

    /**
     * Getter for LaborManager.
     * 
     * @return laborManager LaborManager.
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for laborManager.
     * 
     * @param laborManager
     *            LaborManager.
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for containerNumber spoken by the operator.
     * 
     * @return the containerNumber
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * Setter for containerNumber spoken by the operator.
     * 
     * @param containerNumber
     *            the containerNumber to set
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for container Status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter for container Status.
     * 
     * @param status
     *            the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter for Partial value. If partial = 0 direct load in Selection or
     * scanned Container ID If partial = 1 partial or spoken Container ID
     * 
     * @return partial String.
     */
    public String getPartial() {
        return partial;
    }

    /**
     * Setter for Partial Value. If partial = 0 direct load in Selection or
     * scanned Container ID If partial = 1 partial or spoken Container ID
     * 
     * @param partial
     *            String.
     */
    public void setPartial(String partial) {
        this.partial = partial;
    }

    @Override
    protected Response doExecute() throws Exception {
        // Actions
        LoadingRegion region = null;

        boolean directLoad = false;
        try {
            region = getLoadingRegionManager().get(
                    getOperator().getCurrentRegion().getId());

        } catch (EntityNotFoundException ex) {

            List<LoadingContainerStatus> containerStatus = new ArrayList<LoadingContainerStatus>();

            containerStatus.add(LoadingContainerStatus.NotLoaded);

            LoadingContainer loadingContainer = getLoadingContainerManager()
                    .findLoadingContainerByNumberAndStatus(
                            getContainerNumber(), containerStatus);

            this.listLoadingContainers = new ArrayList<LoadingContainer>();
            this.listLoadingContainers.add(loadingContainer);
            directLoad = true;

            region = loadingContainer.getStop().getRoute().getRegion();
            getLoadingRouteManager().executeChangeOperationForOperator(getOperator());
            Set<Operator> operators = new HashSet<Operator>();
            
            //Check if there are operators working on this route
            if (loadingContainer.getStop().getRoute().getOperators() != null
                && loadingContainer.getStop().getRoute().getOperators()
                    .size() > 0) {
                operators = loadingContainer.getStop().getRoute()
                    .getOperators();
            }
            
            operators.add(getOperator());

            loadingContainer.getStop().getRoute().setOperators(operators);

            LoadingRoute route = loadingContainer.getStop().getRoute();

            if (route.getStatus() == LoadingRouteStatus.Available
                    || route.getStatus() == LoadingRouteStatus.Suspended) {
                loadingContainer.getStop().getRoute()
                        .setStatus(LoadingRouteStatus.InProgress);
                loadingContainer.getStop().getRoute()
                        .setStartTime(getCommandTime());
            }
        }

        setLoadingRegion(region);

        getInProgressLoadingRoute();
        verifyContainer();

        if (!directLoad) {
            getLoadingContainers();
        }

        buildResponse();
        return getResponse();
    }

    /**
     * Getting the In Progress loading route based on Operator.
     * 
     * @throws DataAccessException
     * @throws TaskCommandException
     */
    private void getInProgressLoadingRoute() throws DataAccessException,
            TaskCommandException {
        // Query for extracting the In Progress route for an operator.
        List<LoadingRoute> loadingRouteList = getLoadingRouteManager()
                .listActiveLoadingRoute(getOperator().getId());
        if (!loadingRouteList.isEmpty()) {
            setLoadingRoute(getLoadingRouteManager().listActiveLoadingRoute(
                    getOperator().getId()).get(0));
        } else {
            throw new TaskCommandException(
                    TaskErrorCode.NO_LOADING_ROUTE_INPROGRESS, getOperator()
                            .getOperatorIdentifier());
        }
    }

    /**
     * Method to verify if the container exists.
     * 
     * @throws Exception
     */
    private void verifyContainer() throws Exception {
        if (!getLoadingContainerManager().isContainerExist(getOperator(),
                getContainerNumber(), getLoadingRoute(), getLoadingRegion())) {
            throw new TaskCommandException(
                    TaskErrorCode.NO_LOADING_CONTAINERS_FOUND,
                    getContainerNumber(), getLoadingRoute().getNumber());
        }
    }

    /**
     * This method will return all Loading Containers on Stop basis in an In
     * Progress Route based on containerNumber spoken by the operator. Apart
     * from the spoken containerNumber, this method will also return all the new
     * created containers.
     * 
     * @throws DataAccessException
     * @throws TaskCommandException
     */
    protected void getLoadingContainers() throws DataAccessException,
            TaskCommandException, InterruptedException {
        List<LoadingContainer> loadingContList = getLoadingContainerManager()
                .listAvailableLoadingContainersForRoute(getLoadingRoute(),
                        getLoadingRegion());
        setListLoadingContainers(loadingContList);
    }

    /**
     * Build the list of picks to return to doExecute().
     * 
     * @throws DataAccessException
     *             - Database Exceptions
     * 
     */
    protected void buildResponse() throws DataAccessException {
        for (LoadingContainer lc : getListLoadingContainers()) {
            getResponse().addRecord(buildResponseRecord(lc));
        }
    }

    /**
     * Builds the response record.
     * 
     * @param lcObj
     *            - the pick to use for the response
     * @return record - the response record
     * @throws DataAccessException
     *             - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(LoadingContainer lcObj)
            throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("routeID", getLoadingRoute().getId());
        record.put("containerNumber", lcObj.getContainerNumber());
        int containerIDDigitsOperSpeaks = lcObj.getStop().getRoute()
                .getRegion().getContainerIDDigitsOperSpeaks();
        // Logic for saving the spoken container number based on loading region
        // setting.

        String spokenContainerNumber = null;

        if (lcObj.getContainerNumber().length() >= containerIDDigitsOperSpeaks) {
            spokenContainerNumber = lcObj.getContainerNumber().substring(
                    lcObj.getContainerNumber().length()
                            - containerIDDigitsOperSpeaks);
        } else {
            spokenContainerNumber = lcObj.getContainerNumber();
        }

        record.put("partialContainerNumber", spokenContainerNumber);
        if (lcObj.getStatus() == LoadingContainerStatus.NotLoaded
                || lcObj.getStatus() == LoadingContainerStatus.Unloaded
                || lcObj.getStatus() == LoadingContainerStatus.InProgress) {
            record.put("status", "N"); // Setting the status to NOT LOADED.
        } else if (lcObj.getStatus() == LoadingContainerStatus.Loaded) {
            record.put("status", "L"); // Setting the status to LOADED.
        } else if (lcObj.getStatus() == LoadingContainerStatus.Consolidated) {
            record.put("status", "C"); // Setting the status to CONSOLIDATED.
        } else {
            record.put("status", "X"); // Setting the status as X for CANCELLED containers.
        }
        // If 1 - Operator will capture the load position
        // If 0 - Operator will not capture the load position
        record.put("captureLoadPosition", lcObj.getStop().getRoute()
                .getCaptureLoadPosition());

        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 