/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingStopStatus;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.Set;

/**
 * Command class for Loading Complete Route.
 * 
 * @author kudupi
 */
public class LoadingCompleteRouteCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = 4338586365651712510L;

    private LaborManager laborManager;

    private String routeID;

    private LoadingRoute loadingRoute;

    /**
     * Getter for loadingRoute.
     * 
     * @return loadingRoute LoadingRoute.
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }

    /**
     * Setter for loadingRoute.
     * 
     * @param loadingRoute
     *            LoadingRoute.
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }

    /**
     * Getter for laborManager.
     * 
     * @return laborManager LaborManager.
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for laborManager.
     * 
     * @param laborManager
     *            LaborManager.
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for routeID.
     * 
     * @return routeID String.
     */
    public String getRouteID() {
        return routeID;
    }

    /**
     * Setter for routeID.
     * 
     * @param routeID
     *            String.
     */
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    @Override
    protected Response doExecute() throws Exception {
        processCompleteRoute();
        closeLaborRecord();
        buildResponse();
        return getResponse();
    }

    /**
     * Method to check if number of containers loaded equals actual number of
     * containers for all the stops. Only then the operator can complete the
     * route.
     * 
     * @throws DataAccessException
     * @throws BusinessRuleException
     * @throws TaskCommandException
     */
    protected void processCompleteRoute() throws DataAccessException,
            BusinessRuleException, TaskCommandException {
        LoadingRoute lrObj = getLoadingRouteManager().findLoadingRouteFromID(
                Long.parseLong(getRouteID()));

        if (lrObj != null) {
            setLoadingRoute(lrObj);
            if (getLoadingRoute().getAvailableRouteContainerCount() == 0) {
                
                //If actual number of containers are same as the containers in the route
                if (getLoadingRoute().getRouteContainerCount() >= getLoadingRoute()
                        .getActualNumberOfContainers()) {
                    
                    Set<LoadingStop> stops = getLoadingRoute().getStops();
                    
                    for (LoadingStop stop : stops) {
                        stop.setStatus(LoadingStopStatus.Loaded);
                    }
                    
                    getLoadingRoute().setStatus(LoadingRouteStatus.Complete);
                    getLoadingRoute().setEndTime(getCommandTime());
                } else {
                    throw new TaskCommandException(
                            TaskErrorCode.ALL_CONTAINERS_NOT_LOADED,
                            getLoadingRoute().getNumber());
                }
            } else {
                throw new TaskCommandException(
                        TaskErrorCode.ALL_CONTAINERS_NOT_LOADED,
                        getLoadingRoute().getNumber());
            }
        } else {
            throw new TaskCommandException(
                    TaskErrorCode.LOADING_ROUTE_NOT_FOUND);
        }
    }

    /**
     * Method to close already open labor records associated with this route
     * 
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    protected void closeLaborRecord() throws DataAccessException,
            BusinessRuleException {

        // close labor records
        this.getLaborManager().closeLoadingLaborRecord(getCommandTime(),
                getLoadingRoute(), getOperator());
    }

    /**
     * Builds the response.
     * 
     * @throws DataAccessException
     *             - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     * 
     * @return record - the response record
     * @throws DataAccessException
     *             - database exception
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        // Check operator's assigned to region
        if (getOperator().getAssignedRegion() != null) {
            if (!(getOperator().getAssignedRegion().equals(getOperator()
                    .getCurrentRegion()))) {

                // Build LabelObjectPairs for the parameters
                LOPArrayList lop = new LOPArrayList();
                lop.add("task.regionNumber", getOperator().getAssignedRegion()
                        .getNumber());

                lop.add("task.regionName", getOperator().getAssignedRegion()
                        .getName());

                // Create a TaskMessageInfo object and pass
                // it to the setErrorFields method.
                record.setErrorFields(createMessageInfo(
                        TaskErrorCode.ASSIGNED_TO_OTHER_REGION, lop));
            }
        }
        return record;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 