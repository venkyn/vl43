/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.loading.model.LoadPosition;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingStopStatus;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 * Command class for Loading Update Container Command.
 * 
 * @author kudupi
 */
public class LoadingUpdateContainerCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -596044983082769010L;

    private LoadingRegion loadingRegion;

    private LaborManager laborManager;

    private String status;

    private String containerNumber;

    private String masterContainerNumber;

    private String capturePosition;

    private LoadingRoute loadingRoute;

    private LoadingStop loadingStop;

    private String trailerID;

    private LoadingContainer loadingContainer;

    private LoadingContainer loadingMasterContainer;

    private static final Semaphore SYNC_SINGLE_THREAD = new Semaphore(1, true);

    private static final Logger log = new Logger(
        LoadingUpdateContainerCmdRoot.class);

    /**
     * Getter for containerNumber
     * 
     * @return the containerNumber
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * Setter for containerNumber
     * 
     * @param containerNumber the containerNumber to set
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for masterContainerNumber
     * 
     * @return the masterContainerNumber
     */
    public String getMasterContainerNumber() {
        return masterContainerNumber;
    }

    /**
     * Setter for masterContainerNumber
     * 
     * @param masterContainerNumber the masterContainerNumber to set
     */
    public void setMasterContainerNumber(String masterContainerNumber) {
        this.masterContainerNumber = masterContainerNumber;
    }

    /**
     * Getter for status.
     * 
     * @return status String.
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter for status.
     * 
     * @param status String.
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Getter for capturePosition.
     * 
     * @return capturePosition String.
     */
    public String getCapturePosition() {
        return capturePosition;
    }

    /**
     * Setter for capturePosition.
     * 
     * @param capturePosition String.
     */
    public void setCapturePosition(String capturePosition) {
        this.capturePosition = capturePosition;
    }

    /**
     * Getter for loadingRoute.
     * 
     * @return loadingRoute LoadingRoute.
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }

    /**
     * Setter for loadingRoute.
     * 
     * @param loadingRoute LoadingRoute.
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }

    /**
     * Getter for LoadingStop.
     * 
     * @return loadingStop LoadingStop.
     */
    public LoadingStop getLoadingStop() {
        return loadingStop;
    }

    /**
     * Setter for LoadingStop.
     * 
     * @param loadingStop LoadingStop.
     */
    public void setLoadingStop(LoadingStop loadingStop) {
        this.loadingStop = loadingStop;
    }

    /**
     * Getter for trailerID.
     * 
     * @return trailerID String.
     */
    public String getTrailerID() {
        return trailerID;
    }

    /**
     * Setter for trailerID.
     * 
     * @param trailerID String.
     */
    public void setTrailerID(String trailerID) {
        this.trailerID = trailerID;
    }

    /**
     * Getter for loadingRegion.
     * 
     * @return loadingRegion LoadingRegion.
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }

    /**
     * Setter for loadingRegion.
     * 
     * @param loadingRegion LoadingRegion.
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }

    /**
     * Getter for laborManager.
     * 
     * @return laborManager LaborManager.
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for laborManager.
     * 
     * @param laborManager LaborManager.
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    @Override
    protected Response doExecute() throws Exception {
        LoadingRegion region = null;

        try {
            region = getLoadingRegionManager().get(
                getOperator().getCurrentRegion().getId());

        } catch (EntityNotFoundException ex) {

            List<LoadingContainerStatus> containerStatus = new ArrayList<LoadingContainerStatus>();

            containerStatus.add(LoadingContainerStatus.NotLoaded);

            this.loadingContainer = getLoadingContainerManager()
                .findLoadingContainerByNumberAndStatus(getContainerNumber(),
                    containerStatus);

            region = loadingContainer.getRegion();

            Set<Operator> operators = new HashSet<Operator>();

            //Check if there are operators working on this route
            if (this.loadingContainer.getStop().getRoute().getOperators() != null
                && this.loadingContainer.getStop().getRoute().getOperators()
                    .size() > 0) {
                operators = this.loadingContainer.getStop().getRoute()
                    .getOperators();
            }

            operators.add(getOperator());

            this.loadingContainer.getStop().getRoute().setOperators(operators);

            LoadingRoute route = this.loadingContainer.getStop().getRoute();

            if (route.getStatus() == LoadingRouteStatus.Available
                || route.getStatus() == LoadingRouteStatus.Suspended) {
                this.loadingContainer.getStop().getRoute()
                    .setStatus(LoadingRouteStatus.InProgress);
                this.loadingContainer.getStop().getRoute()
                    .setStartTime(getCommandTime());
            }
        }

        setLoadingRegion(region);

        try {
            SYNC_SINGLE_THREAD.acquire();
            getInProgressLoadingRoute();
            validateContainer();
            updateContainerStatuses();
            updateLoadingStopStatus();
        } finally {
            SYNC_SINGLE_THREAD.release();
        }
        updateLabor();
        buildResponse();
        return getResponse();
    }

    /**
     * Getting the In Progress loading route based on Operator.
     * 
     * @throws DataAccessException
     * @throws TaskCommandException
     */
    private void getInProgressLoadingRoute() throws DataAccessException,
        TaskCommandException {
        // Query for extracting the In Progress route for an operator.
        List<LoadingRoute> loadingRouteList = getLoadingRouteManager()
            .listActiveLoadingRoute(getOperator().getId());
        if (!loadingRouteList.isEmpty()) {
            setLoadingRoute(getLoadingRouteManager().listActiveLoadingRoute(
                getOperator().getId()).get(0));
        } else {
            throw new TaskCommandException(
                TaskErrorCode.NO_LOADING_ROUTE_INPROGRESS, getOperator()
                    .getOperatorIdentifier());
        }
    }

    /**
     * Method to validate the container thats being
     * Loaded/Unloaded/Consolidated.
     */
    protected void validateContainer() throws DataAccessException,
        TaskCommandException {

        List<LoadingContainerStatus> containerStatusList = new ArrayList<LoadingContainerStatus>();
        if (this.getStatus().equals("L")) { // Code Logic for Loading a
                                            // Container.
            containerStatusList.add(LoadingContainerStatus.NotLoaded);
            containerStatusList.add(LoadingContainerStatus.Unloaded);
            containerStatusList.add(LoadingContainerStatus.InProgress);
            this.loadingContainer = loadUnloadContainer(containerStatusList);
        } else if (this.getStatus().equals("N")) { // Code Logic for Unloading a
                                                   // Container.
            // Checking if container is loaded (Only then unloading of the
            // container can be done).
            containerStatusList.add(LoadingContainerStatus.Loaded);
            this.loadingContainer = loadUnloadContainer(containerStatusList);
        } else if (this.getStatus().equals("C")) { // Code Logic for
                                                   // Consolidating a Container.
            containerStatusList.add(LoadingContainerStatus.NotLoaded);
            containerStatusList.add(LoadingContainerStatus.Unloaded);
            containerStatusList.add(LoadingContainerStatus.InProgress);
            this.loadingContainer = consolidateContainer(containerStatusList);
        }
        if (this.loadingContainer == null) {
            if (this.getStatus().equals("N")) {
                throw new TaskCommandException(
                    TaskErrorCode.LOADING_CONTAINER_NOT_LOADED,
                    spokenContainerNumber(getContainerNumber()),
                    getLoadingRoute().getNumber());
            } else {
                throw new TaskCommandException(
                    TaskErrorCode.LOADING_CONTAINER_ERROR_MESSAGE,
                    spokenContainerNumber(getContainerNumber()),
                    getLoadingRoute().getNumber());
            }
        }
        if ((getStatus().equals("C"))
            && (!(this.loadingMasterContainer.getStop().getId() == this.loadingContainer
                .getStop().getId()))) {
            throw new TaskCommandException(
                TaskErrorCode.LOADING_CANNOT_CONSOLIDATE,
                spokenContainerNumber(getMasterContainerNumber()),
                spokenContainerNumber(getContainerNumber()));
        }
    }

    /**
     * Method to Load/Unload a Container.
     * 
     * @param containerStatusList List<LoadingContainerStatus>
     * @return loadingContainer LoadingContainer Object.
     * @throws DataAccessException - {@link DataAccessException}
     */
    private LoadingContainer loadUnloadContainer(List<LoadingContainerStatus> containerStatusList)
        throws DataAccessException {

        this.loadingContainer = getLoadingContainerManager()
            .findLoadingContainerByNumber(getContainerNumber(),
                getLoadingRoute(), getOperator().getId(), containerStatusList);
        return this.loadingContainer;
    }

    /**
     * Method to Consolidate a Container.
     * 
     * @param containerStatusList List<LoadingContainerStatus>
     * @return loadingContainer LoadingContainer Object.
     * @throws DataAccessException - {@link DataAccessException}
     * @throws TaskCommandException - {@link TaskCommandException}
     */
    private LoadingContainer consolidateContainer(List<LoadingContainerStatus> containerStatusList)
        throws DataAccessException, TaskCommandException {
        // Checking if Master Container Exists and is already not loaded.
        this.loadingMasterContainer = getLoadingContainerManager()
            .findLoadingContainerByNumber(getMasterContainerNumber(),
                getLoadingRoute(), getOperator().getId(), containerStatusList);
        if (loadingMasterContainer == null) {
            throw new TaskCommandException(
                TaskErrorCode.NO_LOADING_MASTER_CONTAINER,
                spokenContainerNumber(getMasterContainerNumber()),
                getLoadingRoute().getNumber());
        } else {
            int masterContainerCount = getLoadingContainerManager()
                .countNumberOfMasterCont(
                    this.loadingMasterContainer.getContainerNumber(),
                    getLoadingRoute().getNumber(),
                    getLoadingRegion().getNumber());
            if (masterContainerCount == (getLoadingRegion()
                .getMaxConsolidationOfContainers() - 1)) {
                throw new TaskCommandException(
                    TaskErrorCode.MAX_CONSOLIDATION_ACHIEVED,
                    spokenContainerNumber(getMasterContainerNumber()));
            }
            // Checking if consolidated container exists
            containerStatusList.clear();
            containerStatusList.add(LoadingContainerStatus.NotLoaded);
            containerStatusList.add(LoadingContainerStatus.Unloaded);
            this.loadingContainer = getLoadingContainerManager()
                .findLoadingContainerByNumber(getContainerNumber(),
                    getLoadingRoute(), getOperator().getId(),
                    containerStatusList);
        }
        return this.loadingContainer;
    }

    /**
     * Method to extract the spoken container number for Task phonetic error
     * messages.
     * @param containerNumberValue String.
     * @return spokenContainerNumber String.
     */
    private String spokenContainerNumber(String containerNumberValue) {
        String spokenContainerNumber = null;
        if (getLoadingRegion() != null) {
            int containerIDDigitsOperSpeaks = getLoadingRegion()
                .getContainerIDDigitsOperSpeaks();
            if (containerNumberValue.length() >= containerIDDigitsOperSpeaks) {
                spokenContainerNumber = containerNumberValue
                    .substring(containerNumberValue.length()
                        - containerIDDigitsOperSpeaks);
            } else {
                spokenContainerNumber = containerNumberValue;
            }
        } else {
            spokenContainerNumber = containerNumberValue;
        }
        return spokenContainerNumber;
    }

    /**
     * Method to update the statuses of Containers and Stops.
     * 
     * @throws DataAccessException
     */
    protected void updateContainerStatuses() throws DataAccessException {
        if (getStatus().equals("L")) {
            this.loadingContainer.setStatus(LoadingContainerStatus.Loaded);
        } else if (getStatus().equals("C")) {
            this.loadingContainer
                .setStatus(LoadingContainerStatus.Consolidated);
            this.loadingContainer
                .setMasterContainerNumber(getMasterContainerNumber());
            this.loadingMasterContainer
                .setStatus(LoadingContainerStatus.InProgress);
        } else {
            this.loadingContainer.setStatus(LoadingContainerStatus.Unloaded);
        }
        this.loadingContainer.setOperator(getOperator());
        this.loadingContainer.setLoadTime(getCommandTime());

        // Persist load position
        Integer loadPosition = null;

        if (LoadPosition.LEFT.toString().equalsIgnoreCase(getCapturePosition())) {
            loadPosition = LoadPosition.LEFT.toValue();
        } else if (LoadPosition.RIGHT.toString().equalsIgnoreCase(
            getCapturePosition())) {
            loadPosition = LoadPosition.RIGHT.toValue();
        } else {
            try {
                loadPosition = Integer.parseInt(getCapturePosition());
            } catch (NumberFormatException ex) {
                if (getCapturePosition() != null
                    && getCapturePosition().length() > 0) {
                    log.warn("Invalid load position received : "
                        + getCapturePosition());
                }
            }
        }
        this.loadingContainer.setLoadPosition(loadPosition);
    }

    /**
     * Method to update Loading Stop statuses.
     * 
     * @throws DataAccessException
     */
    private void updateLoadingStopStatus() throws DataAccessException {
        // Updating the statuses of LoadingStop object
        setLoadingStop(this.loadingContainer.getStop());
        if (getLoadingStop().getStatus() == LoadingStopStatus.NotLoaded 
            && getLoadingStop().getAvailableContainerCount() > 1) {
            getLoadingStop().setStatus(LoadingStopStatus.InProgress);
            getLoadingStop().setStartTime(getCommandTime());
            getLoadingStop().setOperator(getOperator());
        } else if (getLoadingStop().getStatus() == LoadingStopStatus.NotLoaded
            && getLoadingStop().getAvailableContainerCount() == 1) {
            getLoadingStop().setStatus(LoadingStopStatus.Loaded);
            getLoadingStop().setStartTime(getCommandTime());
            getLoadingStop().setEndTime(getCommandTime());
            getLoadingStop().setOperator(getOperator());
        } else if (getLoadingStop().getStatus() == LoadingStopStatus.InProgress
            && getLoadingStop().getAvailableContainerCount() == 1
            && this.loadingContainer.getStatus() == LoadingContainerStatus.Loaded) {
            getLoadingStop().setStatus(LoadingStopStatus.Loaded);
            getLoadingStop().setEndTime(getCommandTime());
            getLoadingStop().setOperator(getOperator());
        } else if (getLoadingStop().getStatus() == LoadingStopStatus.Loaded
            && getStatus().equals("N")) {
            getLoadingStop().setStatus(LoadingStopStatus.InProgress);
        } else if (getLoadingStop().getStatus() == LoadingStopStatus.InProgress
            && getLoadingStop().getAvailableContainerCount() == getLoadingStop()
                .getContainers().size() - 1
            && this.loadingContainer.getStatus() == LoadingContainerStatus.Unloaded) {
            getLoadingStop().setStatus(LoadingStopStatus.NotLoaded);
        }
    }

    /**
     * Method to update labor when a container is loaded
     * 
     * @throws DataAccessException - database exception
     */
    protected void updateLabor() throws DataAccessException,
        BusinessRuleException {
        this.getLaborManager().updateLoadingLabor(getOperator(),
            getLoadingRoute(), getContainerStatusEnum());
    }

    /**
     * Builds the response.
     * 
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Utility method to the enum value corresponding to container status sent
     * by terminal
     * 
     * @return LoadingContainerStatus enum corresponding to container status
     */
    private LoadingContainerStatus getContainerStatusEnum() {
        if (getStatus().equals("L")) {
            return LoadingContainerStatus.Loaded;
        } else if (getStatus().equals("N")) {
            return LoadingContainerStatus.Unloaded;
        } else {
            return LoadingContainerStatus.Consolidated;
        }
    }

    /**
     * Builds the response record.
     * 
     * @return record - the response record
     * @throws DataAccessException
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        if (getLoadingRoute().getAvailableRouteContainerCount() == 1
            && this.loadingContainer.getStatus() == LoadingContainerStatus.Loaded) {
            record.put("routeComplete", 1);
        } else {
            record.put("routeComplete", 0);
        }
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 