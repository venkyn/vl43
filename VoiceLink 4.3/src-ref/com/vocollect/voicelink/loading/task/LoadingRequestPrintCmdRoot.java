/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ApplicationContextHolder;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.ReportPrinterMap;
import com.vocollect.voicelink.core.service.ReportPrinterManager;
import com.vocollect.voicelink.loading.events.PrintEvent;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Command class for Loading Request Print Task Command.
 * 
 * @author kudupi
 */
public class LoadingRequestPrintCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = 3717993132034393732L;

    private static final Logger log = new Logger(
        LoadingRequestPrintCmdRoot.class);

    private String routeID;

    // Printer Number spoken by the operator.
    private Integer printerNumber;

    private ReportPrinterManager reportPrinterManager;
    
    
    public static final String IMAGEPATH = "IMAGEPATH";
    
    private PrintEvent<LoadingRoute> printEvent;
    

    

    /**
     * @return printEvent reference
     */
    public PrintEvent<LoadingRoute> getPrintEvent() {
        return printEvent;
    }

    /**
     * @param printEvent printEvent
     */
    public void setPrintEvent(PrintEvent<LoadingRoute> printEvent) {
        this.printEvent = printEvent;
    }

  
    

    /**
     * The mapping of printer numbers to the network addresses of printers -
     * defined in the applicationContext-reportPrinters.xml file.
     */
    private ReportPrinterMap reportPrinterMap;

    /**
     * Getter for routeID.
     * @return routeID.
     */
    public String getRouteID() {
        return routeID;
    }

    /**
     * Setter for routeID.
     * @param routeID String
     */
    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    /**
     * Getter for printerNumber.
     * @return printerNumber.
     */
    public Integer getPrinterNumber() {
        return printerNumber;
    }

    /**
     * Setter for printerNumber.
     * @param printerNumber Integer.
     */
    public void setPrinterNumber(Integer printerNumber) {
        this.printerNumber = printerNumber;
    }

    /**
     * Getter for reportPrinterManager.
     * @return reportPrinterManager.
     */
    public ReportPrinterManager getReportPrinterManager() {
        return reportPrinterManager;
    }

    /**
     * Setter for reportPrinterManager.
     * @param reportPrinterManager ReportPrinterManager.
     */
    public void setReportPrinterManager(ReportPrinterManager reportPrinterManager) {
        this.reportPrinterManager = reportPrinterManager;
    }

    /**
     * Getter for the printerMap property.
     * @return ReportPrinterMap value of the property
     */
    public ReportPrinterMap getReportPrinterMap() {
        return this.reportPrinterMap;
    }

    /**
     * Setter for the printerMap property.
     * @param reportPrinterMap the new printerMap value
     */
    public void setReportPrinterMap(ReportPrinterMap reportPrinterMap) {
        this.reportPrinterMap = reportPrinterMap;
    }

    @Override
    protected Response doExecute() throws Exception {
        printReport();
        buildResponse();
        return getResponse();
    }

    /**
     * Print Loading Route report directly from the task. Pass the printer
     * number in, which is then resolved in the
     * applicationContext-voicelink-reportPrinters.xml configuration file.
     * @throws VocollectException - vocollect exceptions
     */

    private void printReport() throws VocollectException {

        if (getPrinterNumber() != null) {
            try {
                LoadingRoute route = (LoadingRoute)getLoadingRouteManager().get(Long.parseLong(getRouteID()));
                publishPrintEvent(printerNumber, route);
            } catch (Exception e) {
                throw new VocollectException(TaskErrorCode.GENERAL_SYSTEM_ERROR);
               
            }
        }
    }


    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Builds the response record.
     * 
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        return record;
    }
    
    
    /**
     * Publishes the print event
     * @param printerNum Integer
     * @param routeObj route Object
     * @throws DataAccessException 
     */
    protected void publishPrintEvent(Integer printerNum, LoadingRoute routeObj) throws DataAccessException {
        log.debug("LoadingRequestPrintCmdRoot: Inside publishPrintEvent method");
        printEvent.setObject(routeObj);
        printEvent.setPrinterNumber(printerNum);
        printEvent.setSiteContext(SiteContextHolder.getSiteContext());
        printEvent.setOperator(getOperator());
        printEvent.setLocale(LocaleContextHolder.getLocale());
        ApplicationContextHolder.getApplicationContext().publishEvent(
                printEvent);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 