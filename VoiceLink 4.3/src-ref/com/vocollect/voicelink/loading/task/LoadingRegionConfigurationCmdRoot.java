/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;

import com.vocollect.voicelink.task.command.BaseLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Loading Region Configuration Command Implementation.
 * 
 * @author kudupi
 */
public class LoadingRegionConfigurationCmdRoot extends BaseLoadingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -8136937399368504815L;

    private LoadingRegionManager  loadingRegionManager;

    private List<LoadingRegion>   returnRegions = new ArrayList<LoadingRegion>();

    private LaborManager          laborManager;


    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the returnRegions property.
     * @return List&lt;LoadingRegion&gt; value of the property
     */
    public List<LoadingRegion> getReturnRegions() {
        return returnRegions;
    }

    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<LoadingRegion> returnRegions) {
        this.returnRegions = returnRegions;
    }

    /**
     * Getter for the loadingRegionManager property.
     * @return LoadingRegionManager value of the property
     */
    public LoadingRegionManager getLoadingRegionManager() {
        return loadingRegionManager;
    }

    /**
     * Setter for the loadingRegionManager property.
     * @param loadingRegionManager the new loadingRegionManager value
     */
    public void setLoadingRegionManager(LoadingRegionManager loadingRegionManager) {
        this.loadingRegionManager = loadingRegionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildReturnRegionList();
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.REGIONS_NO_LONGER_VALID);
        }
        setOperatorLastLocation();
        buildResponse();
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                               this.getOperator(), OperatorLaborActionType.Loading);

        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return

        for (LoadingRegion r : getReturnRegions()) {
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build response record.
     *
     * @param r - region to build record for LoadingRegion
     * @return - response record.
     * @throws DataAccessException - Database exceptions
     */
    protected ResponseRecord buildResponseRecord(LoadingRegion r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        record.put("loadingRouteIssuance", r.getLoadingRouteIssuance().toValue());
        record.put("containerIDDigitsOperSpeaks", r.getContainerIDDigitsOperSpeaks());
        record.put("trailerIDDigitsOperSpeaks", r.getTrailerIDDigitsOperSpeaks());
        record.put("maxConsolidationOfContainers", r.getMaxConsolidationOfContainers());
        
        getOperator().setCurrentRegion(r);
        return record;
    }

    /**
     * Build a list of regions to return.
     *
     * @throws DataAccessException - Database exception
     */
    protected void buildReturnRegionList() throws DataAccessException {
        Set<Region> regions = getOperator().getRegions();

        for (Region r : regions) {
            if (regions.size() == 1) {
                getReturnRegions().add((LoadingRegion) r);
            }
        }

        if (getReturnRegions().isEmpty() && !regions.isEmpty()) {
            getReturnRegions().add((LoadingRegion) regions.iterator().next());
        }

    }

    /**
     * Sets the operators last location to null.
     *
     * @throws DataAccessException - database exception
     */
    protected void setOperatorLastLocation() throws DataAccessException {
        getOperator().setLastLocation(null);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 