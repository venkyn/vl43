/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.events;


import com.vocollect.epp.util.SiteContext;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Locale;
import org.springframework.context.ApplicationEvent;


/**
 * @author yarora
 *
 * @param <T>
 */
public abstract class PrintEventRoot<T> extends ApplicationEvent {

 


 /**
 * constructer
 */
public PrintEventRoot() {
        super("printEvent");
    }



private static final long serialVersionUID = -3676653102659770003L;
    
   
    
    private T object;
 // Printer Number spoken by the operator.
    private Integer printerNumber;
    
    private SiteContext siteContext;
    
    private Operator operator;
    
    private Locale locale;

    /**
     * Getter for the locale property.
     * @return Locale value of the property
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Setter for the locale property.
     * @param locale the new locale value
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    /**
     * getter for siteContext
     * @return siteContext
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }



    /**
     * setter for siteContext
     * @param siteContext siteContext object
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }



    /**
     * @return object
     */
    public T getObject() {
        return object;
    }



    /**
     * @param object generic object
     */
    public void setObject(T object) {
        this.object = object;
    }



    /**
     * @return printerNumber printerNumber
     */
    public Integer getPrinterNumber() {
        return printerNumber;
    }



    /**
     * @param printerNumber printerNumber
     */
    public void setPrinterNumber(Integer printerNumber) {
        this.printerNumber = printerNumber;
    }



    /**
     * getter for operator
     * @return operator
     */
    public Operator getOperator() {
        return operator;
    }



    /**
     * setter for operator
     * @param operator operator object
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 