/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.loading.dao.LoadingRouteLaborDAO;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;

import java.util.Date;
import java.util.List;

/**
 * 
 * 
 * @author ktanneru
 */
public interface LoadingRouteLaborManagerRoot extends
    GenericManager<LoadingRouteLabor, LoadingRouteLaborDAO>, DataProvider {

    /**
     * 
     * @param startTime - Route Start time.
     * @param operator - Operator that is loading the route.
     * @param route - The route object.
     * @param operLaborRecord - The operator labor to associate the route labor.
     * 
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
    public void openLaborRecord(Date startTime,
                         Operator operator,
                         LoadingRoute route,
                         OperatorLabor operLaborRecord)
        throws DataAccessException, BusinessRuleException;

    /**
     * 
     * @param operator - Operator that is loading the route.
     * @param route - The route object.
     * 
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
    public void updateLaborRecord(Operator operator,
                         LoadingRoute route)
        throws DataAccessException, BusinessRuleException;
    
    /**
     * @param endTime - Route Start time.
     * @param route - The route object.
     * @param operator - The operator
     * @return loading route
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
    public LoadingRouteLabor closeLaborRecord(Date endTime, LoadingRoute route, Operator operator)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param al - Assignment Labor record to recalculate values and save
     * 
     * @throws DataAccessException - if db failure
     * @throws BusinessRuleException - if save fails.
     */
    public void recalculateAggregatesAndSave(LoadingRouteLabor al) throws DataAccessException, BusinessRuleException;
    
    /**
     * Retrieves the open loading labor record for the given route. or returns
     * null if there is no open route labor record.
     * 
     * @param routeId - The Id of the assignment.
     * @param operator - The operator
     * @return The requested loading labor record or null
     * @throws DataAccessException on any failure.
     */
    LoadingRouteLabor findOpenRecordByRouteId(long routeId, Operator operator)
        throws DataAccessException;
    
    /**
     * Retrieves the open loading route labor record for the given operator or
     * returns null if there are no open loading route labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    LoadingRouteLabor findOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException;


    /**
     * Retrieves the open loading route labor records for the given operator or
     * returns null if there is no open loading route labor records.
     * 
     * @param operatorLaborId - the ID of the Operator
     * @return the requested list of loading route labor records or null
     * @throws DataAccessException on any failure.
     */
    List<LoadingRouteLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException;

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.AssignmentLaborManager#findOpenLaborRecord(java.lang.String)
     */
     List<LoadingRouteLabor> listClosedRecordsByBreakLaborId(long breakLaborId) throws DataAccessException;
     
     /**
      * Returns a list of report objects based on OperatorId and a date range.
      * 
      * @param operatorIdentifier - operator
      * @param startDate - start of date range
      * @param endDate - end of date range
      * @return - list of objects
      * @throws DataAccessException - on db failure
      */
     List<Object[]> listLaborSummaryReportRecords(Long operatorIdentifier,
                                                  Date startDate,
                                                  Date endDate)
         throws DataAccessException;
     
     /**
      * Returns a list of report objects based on OperatorId and a date range.
      * 
      * @param operatorId - operator 
      * @param startDate - start of date range
      * @param endDate - end of date range
      * @return list of object 
      * @throws DataAccessException on db failure
      */
     List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                                 Date startDate, Date endDate) 
                                                 throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 