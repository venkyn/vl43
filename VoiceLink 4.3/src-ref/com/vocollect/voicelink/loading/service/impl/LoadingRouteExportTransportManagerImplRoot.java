/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.loading.dao.LoadingRouteDAO;
import com.vocollect.voicelink.loading.service.LoadingRouteExportTransportManagerRoot;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author mraj
 *
 */
public abstract class LoadingRouteExportTransportManagerImplRoot extends
    LoadingRouteManagerImpl implements LoadingRouteExportTransportManagerRoot {
    
    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;
    
    /**
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRouteExportTransportManagerImplRoot(LoadingRouteDAO primaryDAO) {
        super(primaryDAO);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteExportTransportManagerRoot#listCompletedRoutes()
     */
    @Override
    public List<Map<String, Object>> listCompletedRoutes()
        throws DataAccessException {

        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Map<String, Object>> records = getPrimaryDAO()
            .listCompletedRoutes(qd);

        for (Map<String, Object> record : records) {
            record.put("ExportTime", new Date());
        }

        return formatDatesInListOfMap(records);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteExportTransportManagerRoot#listExportFinish()
     */
    @Override
    public List<Map<String, Object>> listExportFinish()
        throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        return getPrimaryDAO().listExportFinish(qd);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteExportTransportManagerRoot#listExportRouteLabor()
     */
    @Override
    public List<Map<String, Object>> listExportRouteLabor()
        throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);
        List<Map<String, Object>> records = this.getPrimaryDAO().listExportLaborRecords(qd);
        
        return formatDatesInListOfMap(records);
    }

    /**
     * Formats the date.
     * @param inputList in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>> formatDatesInListOfMap(List<Map<String, Object>> inputList) {
        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;
        
        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map <String, Object> m : inputList) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if ((myObj != null) && (myObj instanceof java.util.Date)) {
                    // It is a date!
                    formattedDateTime = sdf.format(myObj);
                    // Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } // end if class is date
            } // end for obj

        } //end for map
        return inputList;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 