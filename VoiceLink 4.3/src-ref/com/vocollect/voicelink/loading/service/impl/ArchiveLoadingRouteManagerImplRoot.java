/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.loading.dao.ArchiveLoadingRouteDAO;
import com.vocollect.voicelink.loading.model.ArchiveLoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.service.ArchiveLoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;

import java.util.Date;


/**
 * Manager class to implement archive operations on ArchiveLoadingRoute object
 * @author mraj
 *
 */
public class ArchiveLoadingRouteManagerImplRoot extends
    GenericManagerImpl<ArchiveLoadingRoute, ArchiveLoadingRouteDAO> implements
    ArchiveLoadingRouteManager {

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    private LoadingRouteManager loadingRouteManager;

    
    /**
     * @return the loadingRouteManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    
    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * Passes the ArchiveLoadingRouteDAO as the primaryDAO. Constructor.
     * @param primaryDAO ArchiveLoadingRouteDAO object
     */
    public ArchiveLoadingRouteManagerImplRoot(ArchiveLoadingRouteDAO primaryDAO) {
        super(primaryDAO);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.ArchiveLoadingRouteManagerRoot#updateOlderThan(java.util.Date, com.vocollect.voicelink.loading.model.LoadingRouteStatus)
     */
    @Override
    public Integer updateOlderThan(Date createdDate, LoadingRouteStatus status)
        throws DataAccessException {
        return getPrimaryDAO().updateOlderThan(createdDate, status);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.ArchiveLoadingRouteManagerRoot#executePurge(com.vocollect.voicelink.loading.model.LoadingRouteStatus, java.util.Date)
     */
    @Override
    public int executePurge(LoadingRouteStatus status, Date olderThan) {
        int returnRecords = 0;

        // Get List of Archive Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Fetching Archive Routes to Purge :::");
        }
        try {
            returnRecords = updateOlderThan(olderThan, status);
        } catch (DataAccessException e) {
            log.warn("Error getting Archive "
                + "Routes from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Archive Routes for Purge :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 