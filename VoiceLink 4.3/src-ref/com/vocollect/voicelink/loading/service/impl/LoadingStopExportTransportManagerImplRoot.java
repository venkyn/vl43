/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.loading.dao.LoadingStopDAO;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.service.LoadingStopExportTransportManagerRoot;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 * @author mraj
 * 
 */
public abstract class LoadingStopExportTransportManagerImplRoot extends
        LoadingStopManagerImpl implements LoadingStopExportTransportManagerRoot {

    // Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * @param primaryDAO
     *            the DAO for this manager
     */
    public LoadingStopExportTransportManagerImplRoot(LoadingStopDAO primaryDAO) {
        super(primaryDAO);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingStopExportTransportManagerRoot
     * #listCompletedStops()
     */
    @Override
    public List<Map<String, Object>> listCompletedStops()
            throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Map<String, Object>> records = getPrimaryDAO().listCompletedStops(
                qd);

        updateActualNoOfContainers(records);

        return formatDatesInListOfMap(records);
    }

    /**
     * @param inputList
     *            The input records to process
     */
    private void updateActualNoOfContainers(List<Map<String, Object>> inputList) {

        for (Map<String, Object> m : inputList) {
            Object myObj = m.get("source");

            if (myObj instanceof LoadingStop) {
                LoadingStop stop = (LoadingStop) myObj;
                m.put("ActualNumberOfContainers",
                        stop.getContainerCount());
            }
        }

    }

    /**
     * Formats the date.
     * 
     * @param inputList
     *            in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>> formatDatesInListOfMap(
            List<Map<String, Object>> inputList) {
        // Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        // Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map<String, Object> m : inputList) {
            // Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                // Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if ((myObj != null) && (myObj instanceof java.util.Date)) {
                    // It is a date!
                    formattedDateTime = sdf.format(myObj);
                    // Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } // end if class is date
            } // end for obj

        } // end for map
        return inputList;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 