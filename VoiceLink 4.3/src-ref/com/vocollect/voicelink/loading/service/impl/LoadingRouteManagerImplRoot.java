/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.dao.LoadingContainerDAO;
import com.vocollect.voicelink.loading.dao.LoadingRouteDAO;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;

import java.util.Date;
import java.util.List;

/**
 * Implementation class for business layer of Loading route
 * 
 * @author mraj
 * 
 */
/**
 * @author kudupi
 * 
 */
public abstract class LoadingRouteManagerImplRoot extends
        GenericManagerImpl<LoadingRoute, LoadingRouteDAO> implements
        LoadingRouteManager {

    private LoadingContainerDAO loadingContainerDAO;

    /**
     * Constructor.
     * 
     * @param primaryDAO
     *            the DAO for this manager
     */
    public LoadingRouteManagerImplRoot(LoadingRouteDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get specified loadingRoute.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#findLoadingRouteFromID(java.lang.Long)
     */
    public LoadingRoute findLoadingRouteFromID(long routeID)
            throws DataAccessException {
        return getPrimaryDAO().findLoadingRouteFromID(routeID);
    }

    /**
     * Implementation to get the list of loadingRoutes by route number and
     * region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#findLoadingRouteByNumber(java.lang.String)
     */
    public List<LoadingRoute> listLoadingRouteByNumber(String routeNumber,
            Integer regionNumber) throws DataAccessException {
        return getPrimaryDAO().listLoadingRouteByNumber(routeNumber,
                regionNumber);
    }

    /**
     * Implementation to get the in progress loadingRoutes by operator and
     * region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#listInProgressLoadingRoute(operator,
     *      java.lang.integer)
     */
    public List<LoadingRoute> listInProgressLoadingRoute(Long operatorID,
            Integer regionNumber) throws DataAccessException {
        return getPrimaryDAO().listInProgressLoadingRoute(operatorID,
                regionNumber);
    }

    /**
     * Implementation to get the active loadingRoute by operator.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#listInProgressLoadingRoute(operator)
     */
    public List<LoadingRoute> listActiveLoadingRoute(Long operatorID)
            throws DataAccessException {
        return getPrimaryDAO().listActiveLoadingRoute(operatorID);
    }

    /**
     * Implementation to get the list of Available loadingRoutes by region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#findAvailableLoadingRoute(java.lang.String)
     */
    public List<LoadingRoute> listAvailableLoadingRoute(Integer regionNumber)
            throws DataAccessException {
        return getPrimaryDAO().listAvailableLoadingRoute(regionNumber);
    }

    /**
     * In case of Change Route. Need to make the already working route into
     * suspended state if only one operator is working else leave it in In
     * Progress state.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingRouteManager#executeChangeRouteOperator(operator,
     *      java.lang.integer)
     */
    public void executeChangeOperationForOperator(Operator operatorId)
            throws DataAccessException, BusinessRuleException {
        List<LoadingRoute> loadingRouteInProgressList = getPrimaryDAO()
                .listActiveLoadingRoute(operatorId.getId());
        if (!loadingRouteInProgressList.isEmpty()) {
            LoadingRoute loadingRouteInProgress = loadingRouteInProgressList
                    .get(0);
            if ((loadingRouteInProgress.getOperators().size() == 1)
                    && (loadingRouteInProgress.getStatus() != LoadingRouteStatus.Complete)) {
                loadingRouteInProgress.setStatus(LoadingRouteStatus.Suspended);
            }
            loadingRouteInProgress.getOperators().remove(operatorId);
            getPrimaryDAO().save(loadingRouteInProgress);
        }
    }

    /**
     * Method to validate if the Route can be edited with the new values or not.
     * @param loadingRoute LoadingRoute.
     * @param departureDateValue Date.
     * @param loadingRegion LoadingRegion.
     * @return boolean value.
     * @throws DataAccessException - dae
     */
    public boolean validateEditRoute(LoadingRoute loadingRoute,
            Date departureDateValue, LoadingRegion loadingRegion)
            throws DataAccessException {
        boolean valid = true;
        if (!departureDateValue.equals(loadingRoute.getDepartureDateTime())) {
            List<LoadingRoute> loadingRouteList = getPrimaryDAO()
            .listImportBlockingCondition(loadingRoute.getNumber(),
                    departureDateValue, loadingRegion);
            if (loadingRouteList.size() == 1) {
                valid = false;
            }
        }
        getPrimaryDAO().clearSession();
        return valid;
    }

    /**
     * Method to find record matching criteria of route, stop number and region.
     * 
     * @param route
     *            Lading route
     * @param departureDateTime
     *            departureDateTime
     * @param region
     *            Loading region
     * @return selected LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listImportBlockingCondition(String route,
            Date departureDateTime, LoadingRegion region)
            throws DataAccessException {
        return this.getPrimaryDAO().listImportBlockingCondition(route,
                departureDateTime, region);
    }
    
    /**
     * Method to find record matching criteria of route, stop number and region.
     * 
     * @param route
     *            Lading route
     * @param departureDateTime
     *            departureDateTime
     * @param region
     *            Loading region
     * @return selected LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listImportBlockingConditionWithStatus(String route,
            Date departureDateTime, LoadingRegion region)
            throws DataAccessException {
        return this.getPrimaryDAO().listImportBlockingConditionWithStatus(route,
                departureDateTime, region);
    }

    /**
     * Method to ensure all route in a group are updated the same when an route
     * was edited from the UI.
     * 
     * @param route
     *            - route that was edited
     * @return the saved assignment
     * @throws DataAccessException
     *             - Database Exceptions
     * @throws BusinessRuleException
     *             - Business rule exceptions
     */
    @Override
    public Object save(LoadingRoute route) throws DataAccessException,
            BusinessRuleException {
        if (route.isNew()) {
            for (LoadingStop stop : route.getStops()) {
                // if the object is being created and is a taggable object
                // get the current site from the context and add it to the
                // object
                SiteContext siteContext = SiteContextHolder.getSiteContext();
                siteContext.setSiteToCurrentSite(stop);
            }
        }

        super.save(route);
        return route;
    }

    /**
     * Method to ensure all routes in a group are updated the same when routes
     * are Imported.
     * 
     * @param routes
     *            - route that was edited
     * @return saved Object
     * @throws DataAccessException
     *             - Database Exceptions
     * @throws BusinessRuleException
     *             - Business rule exceptions
     */
    @Override
    public Object save(List<LoadingRoute> routes) throws DataAccessException,
            BusinessRuleException {
        for (LoadingRoute route : routes) {
            if (route.isNew()) {
                for (LoadingStop stop : route.getStops()) {
                    // if the object is being created and is a taggable object
                    // get the current site from the context and add it to the
                    // object
                    SiteContext siteContext = SiteContextHolder
                            .getSiteContext();
                    siteContext.setSiteToCurrentSite(stop);
                }
            }
        }

        this.getPrimaryDAO().save(routes);
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
            throws DataAccessException {
        return this.getPrimaryDAO().listRoutes(new QueryDecorator(rdi));
    }

    /**
     * @return the loadingContainerDAO
     */
    public LoadingContainerDAO getLoadingContainerDAO() {
        return loadingContainerDAO;
    }

    /**
     * @param loadingContainerDAO
     *            the loadingContainerDAO to set
     */
    public void setLoadingContainerDAO(LoadingContainerDAO loadingContainerDAO) {
        this.loadingContainerDAO = loadingContainerDAO;
    }

    /**
     * This method is used in assignment import validation. It finds loading
     * routes that have matching route number, loading region, departure date
     * time and customer number. If one is not found, an import record should
     * fail validation.
     * 
     * @param route
     *            - loading route to look for.
     * @param loadingRegionNumber
     *            - loading RegionNumber to look for.
     * @param departureDateTime
     *            - loading departureDateTime to look for.
     * @param customerNumber
     *            - customerNumber to look for
     * @return - loadingRoute that blocks import, if any.
     * @throws DataAccessException
     *             -.
     */
    public LoadingRoute findLoadingRoute(String route,
            Integer loadingRegionNumber, Date departureDateTime,
            String customerNumber) throws DataAccessException {
        if (loadingRegionNumber == null && departureDateTime == null) {
            return new LoadingRoute();
        }

        return this.getPrimaryDAO().findLoadingRoute(route,
                loadingRegionNumber, departureDateTime, customerNumber);
    }
    
    @Override
    public List<LoadingRoute> listRouteByNumberAndDDT(String route, Date departureDateTime)
        throws DataAccessException {
        return this.getPrimaryDAO().listRouteByNumberAndDDT(route, departureDateTime);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 