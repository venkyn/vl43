/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.dao.LoadingContainerDAO;
import com.vocollect.voicelink.loading.model.LoadPosition;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingTruckDiagramRow;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Implementation class for business layer of Loading route
 * 
 * @author mraj
 * 
 */
public abstract class LoadingContainerManagerImplRoot extends
    GenericManagerImpl<LoadingContainer, LoadingContainerDAO> implements
    LoadingContainerManager {

    private LoadingRouteManager loadingRouteManager;

    /**
     * Constructor.
     * 
     * @param primaryDAO the DAO for this manager
     */
    public LoadingContainerManagerImplRoot(LoadingContainerDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for loadingRouteManager.
     * @return loadingRouteManager LoadingRouteManager.
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * Setter for loadingRouteManager.
     * @param loadingRouteManager LoadingRouteManager.
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * Implementation to get the count of Loading Containers based on Stop,
     * route number and region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManager#countLoadingContainerByStop
     *      (java.lang.String, Stop)
     */
    public Integer countLoadingContainerByStop(String containerNumber,
                                               LoadingStop stop)
        throws DataAccessException {
        return convertNumberToInt(getPrimaryDAO().countLoadingContainerByStop(
            "%" + containerNumber, stop));
    }

    /**
     * Implementation to get the list of loadingContainers by container number,
     * stop, route number and region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManager#listAvailableLoadingContainersForRoute
     *      (java.lang.String, Operator, java.lang.String, java.lang.Integer)
     */
    public List<LoadingContainer> listAvailableLoadingContainersForRoute(LoadingRoute route,
                                                                         LoadingRegion loadingRegion)
        throws DataAccessException {
        return getPrimaryDAO().listAvailableLoadingContainersForRoute(
            route, loadingRegion);
    }

    /**
     * Implementation to get the LoadingContainer Object by container number,
     * route, operator, and loadingContainerStatus.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManager#findLoadingContainerByNumber
     *      (java.lang.String, route, operator, java.lang.String,
     *      java.lang.String)
     */
    public LoadingContainer findLoadingContainerByNumber(String containerNumber,
                                                         LoadingRoute route,
                                                         Long operatorID,
                                                         List<LoadingContainerStatus> loadingContainerStatus)
        throws DataAccessException {
        return getPrimaryDAO().findLoadingContainerByNumber(
            containerNumber, route, operatorID, loadingContainerStatus);
    }

    /**
     * Method to find cotainer by Number and Status
     * @param containerNumber Number of Loading container to be found
     * @param containerStatus Status of Loading container to be found
     * @return LoadingContainer object
     */
    public LoadingContainer findLoadingContainerByNumberAndStatus(String containerNumber,
                                                                  List<LoadingContainerStatus> containerStatus) {
        return getPrimaryDAO().findLoadingContainerByNumberAndStatus(
            containerNumber, containerStatus);
    }

    /**
     * Implementation to check if the container exists in LoadingContainers
     * 
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManager#isContainerExist
     *      (Operator, java.lang.String, LoadingRoute, LoadingRegion)
     */
    public boolean isContainerExist(Operator operatorID,
                                    String containerNumber,
                                    LoadingRoute routeNumber,
                                    LoadingRegion regionNumber)
        throws DataAccessException {
        List<LoadingContainer> lc = getPrimaryDAO()
            .listLoadingContainerBySpokenNumber(
                "%" + containerNumber, operatorID.getId(),
                routeNumber.getNumber(), regionNumber.getNumber());
        return lc.size() > 0;
    }

    /**
     * Implementation to get the count of Loading Master Containers Object by
     * master container number, route number and region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManager#countNumberOfMasterCont
     *      (java.lang.String, java.lang.String, java.lang.Integer)
     */
    public Integer countNumberOfMasterCont(String masterContainer,
                                           String routeNumber,
                                           Integer regionNumber)
        throws DataAccessException {
        return convertNumberToInt(getPrimaryDAO().countNumberOfMasterCont(
            masterContainer, routeNumber, regionNumber));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.lang.
     * Object)
     */
    @Override
    public Object save(LoadingContainer loadContainer)
        throws BusinessRuleException, DataAccessException {

        // Fail if container is being imported for Route which is already
        // complete
        if (loadContainer.isNew()
            && loadContainer.getStop().getRoute().getStatus() == LoadingRouteStatus.Complete) {
            throw new BusinessRuleException(null, new UserMessage(
                ResourceUtil.getLocalizedMessage(
                    "import.fail.dueto.status",
                    new Object[] {
                        "Route", LoadingRouteStatus.Complete,
                        loadContainer.getContainerNumber() },
                    LocaleContextHolder.getLocale())));
        }

        this.getPrimaryDAO().save(loadContainer);
        return loadContainer;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.util.
     * List)
     */
    @Override
    public Object save(List<LoadingContainer> loadContainers)
        throws BusinessRuleException, DataAccessException {
        this.getPrimaryDAO().save(loadContainers);
        return null;
    }

    /**
     * Method to find existing containers with matching criteria
     * 
     * @param containerNumber - the containerNumber
     * @return Loading container if found and null not found
     * @throws DataAccessException
     */
    public LoadingContainer findImportBlockingCondition(String containerNumber)
        throws DataAccessException {
        return this.getPrimaryDAO()
            .findImportBlockingCondition(containerNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManagerRoot#isTruckConfigurationCustom(long)
     */
    public boolean isTruckConfigurationCustom(long routeId)
        throws DataAccessException {
        List<Integer> loadPositions = new ArrayList<Integer>(2);
        loadPositions.add(LoadPosition.LEFT.toValue());
        loadPositions.add(LoadPosition.RIGHT.toValue());

        Long count = this.getPrimaryDAO().countContainerWithCustomPos(
            routeId, loadPositions);

        return count != null && count.longValue() > 0;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManagerRoot#listTruckLoadedContainers(long,
     *      boolean)
     */
    public List<LoadingTruckDiagramRow> listTruckLoadedContainers(long routeId,
                                                                  boolean isTruckConfigurationCustom)
        throws DataAccessException {

        LoadingRoute loadingRoute = getLoadingRouteManager()
            .findLoadingRouteFromID(routeId);
        List<LoadingTruckDiagramRow> loadedContainersList = this
            .getPrimaryDAO().listTruckLoadedContainers(routeId);
        List<LoadingTruckDiagramRow> formattedContainerList = new ArrayList<LoadingTruckDiagramRow>();

        if (isTruckConfigurationCustom) {
            // Return the obtained list if truck is loaded with custom positions
            formattedContainerList = loadedContainersList;
            if (formattedContainerList.isEmpty()) {
                LoadingTruckDiagramRow containerRow = new LoadingTruckDiagramRow();
                containerRow.setRouteNumber(loadingRoute.getNumber());
                containerRow.setDockDoor(loadingRoute.getDockDoor()
                    .getScannedVerification());
                containerRow.setTrailer(loadingRoute.getTrailer());
                formattedContainerList.add(containerRow);
            } else {
                formattedContainerList = processTruckWeight(formattedContainerList, routeId);
            }
        } else {

            formattedContainerList = new ArrayList<LoadingTruckDiagramRow>();
            List<LoadingTruckDiagramRow> containerListLeft = new ArrayList<LoadingTruckDiagramRow>();
            List<LoadingTruckDiagramRow> containerListRight = new ArrayList<LoadingTruckDiagramRow>();

            // Seperate the container list in Left/right
            for (LoadingTruckDiagramRow loadingContainer : loadedContainersList) {
                if (loadingContainer.getContainerPosition().intValue() == LoadPosition.LEFT
                    .toValue()) {
                    containerListLeft.add(loadingContainer);
                } else {
                    containerListRight.add(loadingContainer);
                }
            }

            int totalContainers = loadedContainersList.size();
            Iterator<LoadingTruckDiagramRow> leftItr = containerListLeft
                .iterator();
            Iterator<LoadingTruckDiagramRow> rightItr = containerListRight
                .iterator();
            LoadingTruckDiagramRow containerRow = null;
            // create the row objects to be returned
            do {
                containerRow = new LoadingTruckDiagramRow();
                
                LoadingTruckDiagramRow loadingContainerLeft = null;
                LoadingTruckDiagramRow loadingContainerRight = null;

                if (leftItr.hasNext()) {
                    loadingContainerLeft = leftItr.next();
                }

                if (rightItr.hasNext()) {
                    loadingContainerRight = rightItr.next();
                }

                containerRow.setRouteNumber(loadingRoute.getNumber());
                containerRow.setDockDoor(loadingRoute.getDockDoor()
                    .getScannedVerification());
                containerRow.setTrailer(loadingRoute.getTrailer());
                containerRow.setTruckWeight(loadingRoute.getTruckWeight());

                if (loadingContainerLeft == null
                    && loadingContainerRight == null) {
                    break;
                }

                if (loadingContainerRight != null) {
                    containerRow.setContainerNumberRight(loadingContainerRight
                        .getContainerNumberLeft());
                    containerRow.setCustomerNumberRight(loadingContainerRight
                        .getCustomerNumberLeft());
                    containerRow.setContainerWeightRight(loadingContainerRight
                        .getContainerWeightLeft());
                    totalContainers--;
                }

                if (loadingContainerLeft != null) {
                    containerRow.setContainerNumberLeft(loadingContainerLeft
                        .getContainerNumberLeft());
                    containerRow.setCustomerNumberLeft(loadingContainerLeft
                        .getCustomerNumberLeft());
                    containerRow.setContainerWeightLeft(loadingContainerLeft
                        .getContainerWeightLeft());
                    totalContainers--;
                }

                formattedContainerList.add(containerRow);
            } while (totalContainers > 0);

            if (formattedContainerList.isEmpty()) {
                formattedContainerList.add(containerRow);
            } else {
                formattedContainerList = processTruckWeight(formattedContainerList, routeId);
            }
        }
        return formattedContainerList;
    }

    /**
     * Method to calculate truck diagram list.
     * @param truckDiagramList List<LoadingTruckDiagramRow>
     * @param routeId RouteID value long
     * @return truckContainersList List<LoadingTruckDiagramRow>
     */
    private List<LoadingTruckDiagramRow> processTruckWeight(List<LoadingTruckDiagramRow> truckDiagramList, long routeId)
        throws DataAccessException {
        List<LoadingTruckDiagramRow> truckContainersList = new ArrayList<LoadingTruckDiagramRow>();
        List<LoadingTruckDiagramRow> finaltruckContainersList = new ArrayList<LoadingTruckDiagramRow>();
        Double truckWeight = 0.0;
        for (LoadingTruckDiagramRow loadTruckDiagramRow : truckDiagramList) {
            Double newExpectedWeight = 0.0;
            if (!StringUtil.isNullOrEmpty(loadTruckDiagramRow
                .getContainerNumberLeft())) {
                newExpectedWeight = this
                    .expectedWeightConsolidated(loadTruckDiagramRow
                        .getContainerNumberLeft().trim(), routeId);
                if (newExpectedWeight != null) {
                    newExpectedWeight = newExpectedWeight
                        + loadTruckDiagramRow.getContainerWeightLeft();
                } else {
                    newExpectedWeight = loadTruckDiagramRow
                        .getContainerWeightLeft();
                }
                loadTruckDiagramRow.setContainerWeightLeft(newExpectedWeight);
                truckWeight = truckWeight + newExpectedWeight;
            } 
            if (!StringUtil.isNullOrEmpty(loadTruckDiagramRow
                .getContainerNumberRight())) {
                newExpectedWeight = this
                    .expectedWeightConsolidated(loadTruckDiagramRow
                        .getContainerNumberRight().trim(), routeId);
                if (newExpectedWeight != null) {
                    newExpectedWeight = newExpectedWeight
                        + loadTruckDiagramRow.getContainerWeightRight();
                } else {
                    newExpectedWeight = loadTruckDiagramRow
                        .getContainerWeightRight();
                }
                loadTruckDiagramRow.setContainerWeightRight(newExpectedWeight);
                truckWeight = truckWeight + newExpectedWeight;
            }
            loadTruckDiagramRow.setTruckWeight(truckWeight);
            truckContainersList.add(loadTruckDiagramRow);
        }
        for (LoadingTruckDiagramRow truckRow : truckContainersList) {
            truckRow.setTruckWeight(truckWeight);
            finaltruckContainersList.add(truckRow);
        }
        return finaltruckContainersList;
    }

    /**
     * Method to calculate the summation of expected weight of
     * Consolidated containers based on master container number.
     * @param masterContainerNumber String
     * @param routeId routeId value long
     * @return consolidatedExpectedWeight Double.
     * @throws DataAccessException
     */
    public Double expectedWeightConsolidated(String masterContainerNumber, long routeId)
        throws DataAccessException {
        Double consolidatedExpectedWeight = 0.0;
        List<LoadingContainer> consolidateContainers = this.getPrimaryDAO().listConsolidatedContainers(
            masterContainerNumber, routeId);
        for (LoadingContainer loadCont : consolidateContainers) {
            consolidatedExpectedWeight = consolidatedExpectedWeight + loadCont.getExpectedWeight();
        }
        return consolidatedExpectedWeight;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingContainerManagerRoot#maxEndTime(long,
     *      java.util.Date, java.util.Date)
     */
    public Date maxEndTime(long operatorId, Date minTime, Date maxTime)
        throws DataAccessException {
        return this.getPrimaryDAO().maxEndTime(operatorId, minTime, maxTime);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 