/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.loading.dao.LoadingStopImportDataDAO;
import com.vocollect.voicelink.loading.model.LoadingStopImportData;
import com.vocollect.voicelink.loading.service.LoadingStopImportDataManager;


/**
 * Standard implementation of the Importable Loading Stop data manager implementation.
 * 
 * @author mraj
 *
 */
public abstract class LoadingStopImportDataManagerImplRoot extends
    GenericManagerImpl<LoadingStopImportData, LoadingStopImportDataDAO>
    implements LoadingStopImportDataManager {
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingStopImportDataManagerImplRoot(LoadingStopImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingStopImportDataManagerRoot#updateToComplete()
     */
    @Override
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 