/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.loading.LoadingErrorCode;
import com.vocollect.voicelink.loading.dao.LoadingRegionDAO;
import com.vocollect.voicelink.loading.dao.LoadingSummaryDAO;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingSummary;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.exception.ConstraintViolationException;

/**
 * Additional service methods for the <code>LoadingRegion</code> model object.
 * 
 * @author kudupi
 */
public abstract class LoadingRegionManagerImplRoot extends
    GenericManagerImpl<LoadingRegion, LoadingRegionDAO>
    implements LoadingRegionManager {
    
    //All Summaries.
    private static final int REGION = 0;

    //Route summary query positions
    private static final int TOTAL_ASSIGNMENTS = 1;
    private static final int INPROGRESS = 2;
    private static final int AVAILABLE = 3;
    private static final int COMPLETE = 4;
    private static final int NON_COMPLETE = 5;
    private static final int ASSIGN_SITE = 6;

    //Current work summary query positions
    private static final int OPERATOR_IN_REGION = 1;
    private static final int OPERATOR_ASSIGNED = 2;
    private static final int TOTAL_ROUTES = 3;
    private static final int TOTAL_ROUTES_COMPLETED = 4;
    private static final int TOTAL_ROUTES_REMAINING = 5;
    private static final int CURRENT_WORK_SITE = 6;
    
    private WorkgroupManager workgroupManager;
    
    private RegionManager regionManager;

    private OperatorDAO operatorDAO;
    
    private LoadingSummaryDAO loadingSummaryDAO;
    
    /**
     * Getter for WorkGroupManager.
     * @return workgroupManager WorkgroupManager.
     */
    public WorkgroupManager getWorkgroupManager() {
        return workgroupManager;
    }

    /**
     * Setter for workgroupManager.
     * @param workgroupManager WorkgroupManager.
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * Getter for regionManager.
     * @return regionManager RegionManager.
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for regionManager.
     * @param regionManager RegionManager.
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for operatorDAO.
     * @return operatorDAO OperatorDAO.
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }

    /**
     * Setter for opertaorDAO.
     * @param operatorDAO OperatorDAO.
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }
    
    /**
     * @return the loadingSummaryDAO
     */
    public LoadingSummaryDAO getLoadingSummaryDAO() {
        return loadingSummaryDAO;
    }

    
    /**
     * @param loadingSummaryDAO the loadingSummaryDAO to set
     */
    public void setLoadingSummaryDAO(LoadingSummaryDAO loadingSummaryDAO) {
        this.loadingSummaryDAO = loadingSummaryDAO;
    }
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRegionManagerImplRoot(LoadingRegionDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     */
    public LoadingRegion findRegionByNumber(int regionNumber)
                            throws DataAccessException, BusinessRuleException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.loadingRegionManagerRoot#uniquenessByName(java.lang.String)
     */
    public Long uniquenessByName(String regionName) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByName(regionName);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.loading.service.loadingRegionManagerRoot#uniquenessByNumber(int)
     */
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByNumber(regionNumber);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        LoadingRegion loadingRegion = get(id);
        return delete(loadingRegion);
    }
    
    /**
    *
    * {@inheritDoc}
    * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
    */
   @Override
   public Object delete(LoadingRegion instance)
       throws BusinessRuleException, DataAccessException {
       try {
           executeValidateDeleteRegion(instance);
           return super.delete(instance);
       } catch (DataAccessException ex) {
           if (ex.getCause() instanceof ConstraintViolationException) {
               throwBusinessRuleException("loadingRegion.delete.error.inUse");
           } else {
               throw ex;
           }
       }
       return null;
   }

   /**
    * Take the passed in Loading Region and add it to all the
    * Workgroups where AutoAddRegions is turned on and the task
    * function is a loading task function.
    * @param region - a loading region
    * @throws DataAccessException - any database exception
    */
   protected void addToWorkgroups(LoadingRegion region)
       throws DataAccessException {

       List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
       for (Workgroup workgroup : wgs) {
           for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
               if (wgf.getTaskFunction().getRegionType() == RegionType.Loading) {
                   wgf.getRegions().add(region);
               }
           }
       }
   }
   
   /**
    * {@inheritDoc}
    * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
    */
   @Override
   public Object save(LoadingRegion region)
       throws BusinessRuleException, DataAccessException {
       getRegionManager().verifyUniqueness(region);
       // Business rule: If the region being saved is new,
       // cycle through the Workgroups where autoaddregions is true
       // and save the regions to all the loading task functions
       if (region.isNew()) {
           addToWorkgroups(region);
       }
       return super.save(region);
   }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.loading.model.LoadingRegion)
     */
    public void executeValidateEditRegion(LoadingRegion region) throws DataAccessException, BusinessRuleException {

        // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            if (!verifyNoOperatorsSignedIn(region)) {
                throw new BusinessRuleException(LoadingErrorCode.REGION_OPERATORS_SIGNED_IN,
                    new UserMessage("loadingRegion.edit.error.operatorsSignedIn"));
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.loading.model.LoadingRegion)
     */
    public void executeValidateDeleteRegion(LoadingRegion region)
                                   throws DataAccessException, BusinessRuleException {

        // Verify that no operators are signed into the region before deleting
        if (!verifyNoOperatorsSignedIn(region)) {
            throw new BusinessRuleException(LoadingErrorCode.REGION_OPERATORS_SIGNED_IN,
                new UserMessage("loadingRegion.delete.error.operatorsSignedIn"));
           }
    }
    
    /**
     * There can be no operators signed into the region when editing or deleting.
     *
     * @param region - to be persisted or deleted
     * @return true if no operators are signed into region otherwise return false
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    public boolean verifyNoOperatorsSignedIn(LoadingRegion region)
        throws BusinessRuleException, DataAccessException {

        if (this.getOperatorDAO().
                countNumberOfOperatorsSignedIn(region).intValue() > 0) {
           return false;
        }
        return true;
    }
    
    /**
     * Method to get the list of Data object for Loading Route Summary.
     * @param rdi ResultDataInfo
     * @return List<DataObject> newList.
     * @throws DataAccessException
     */
    public List<DataObject> listLoadingRouteSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getLoadingSummaryDAO().listLoadingRouteSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            LoadingSummary summaryObject = new LoadingSummary();

            summaryObject.setRegion((LoadingRegion) objArray[REGION]);
            summaryObject.setTotalAssignments(convertNumberToInt(objArray[TOTAL_ASSIGNMENTS]));
            summaryObject.setInProgress(convertNumberToInt(objArray[INPROGRESS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setComplete(convertNumberToInt(objArray[COMPLETE]));
            summaryObject.setNonComplete(convertNumberToInt(objArray[NON_COMPLETE]));
            summaryObject.setSite((Site) objArray[ASSIGN_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            newList.add(summaryObject);
        }
        return newList;
    }
    
    /**
     * Method to get the list of Data object for Loading Current Work Summary.
     * @param rdi ResultDataInfo
     * @return List<DataObject> newList.
     * @throws DataAccessException
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getLoadingSummaryDAO().listCurrentWorkSummary((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            LoadingSummary summaryObject = new LoadingSummary();

            summaryObject.setRegion((LoadingRegion) objArray[REGION]);
            summaryObject.setOperatorsWorkingIn(convertNumberToInt(objArray[OPERATOR_IN_REGION]));
            summaryObject.setOperatorsAssigned(convertNumberToInt(objArray[OPERATOR_ASSIGNED]));
            summaryObject.setTotalRoutes(convertNumberToInt(objArray[TOTAL_ROUTES]));
            summaryObject.setTotalRoutesCompleted(convertNumberToInt(objArray[TOTAL_ROUTES_COMPLETED]));
            summaryObject.setTotalRoutesRemaining(convertNumberToInt(objArray[TOTAL_ROUTES_REMAINING]));
            summaryObject.setSite((Site) objArray[CURRENT_WORK_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            Double estimatedCompleted = 0.0;

            if ((summaryObject.getRegion().getGoalRate() != 0)
                && (summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn() != 0)) {
                int totalRemainingContainers = 0;
                for (LoadingRoute loadingRoute : summaryObject.getRegion().getLoadingRoutes()) {
                    totalRemainingContainers = totalRemainingContainers +
                                loadingRoute.getAvailableRouteContainerCount();
                }
                Double totalRemainContainers = new Double(totalRemainingContainers);
                Double operators = new Double(summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn());
                Double goalRate = new Double(summaryObject.getRegion().getGoalRate());
                // Calculation is based on total remaining containers in the region as
                // Goal Rate in loading = (Containers/Hr)
                estimatedCompleted = totalRemainContainers / goalRate / operators;
            }

            //Round to 2 decimal places and set property.
            summaryObject.setEstimatedCompleted(new BigDecimal(estimatedCompleted)
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            newList.add(summaryObject);
        }
        return newList;
    }    


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 