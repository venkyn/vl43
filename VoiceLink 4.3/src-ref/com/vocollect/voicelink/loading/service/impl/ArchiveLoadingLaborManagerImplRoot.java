/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.loading.dao.ArchiveLoadingLaborDAO;
import com.vocollect.voicelink.loading.model.ArchiveLoadingLabor;
import com.vocollect.voicelink.loading.service.ArchiveLoadingLaborManager;

/**
 * 
 * 
 * @author yarora
 */
public abstract class ArchiveLoadingLaborManagerImplRoot extends
    GenericManagerImpl<ArchiveLoadingLabor, ArchiveLoadingLaborDAO> implements
    ArchiveLoadingLaborManager {
    
    private static final Double ONE_HUNDRED = 100.0;
    
    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.0;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchiveLoadingLaborManagerImplRoot(ArchiveLoadingLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
    
    
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 