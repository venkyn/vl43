/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.loading.dao.LoadingStopDAO;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.service.LoadingStopManager;

import java.util.Date;
import java.util.List;

/**
 * @author mraj
 * 
 */
public abstract class LoadingStopManagerImplRoot extends
        GenericManagerImpl<LoadingStop, LoadingStopDAO> implements
        LoadingStopManager {

    /**
     * Constructor.
     * 
     * @param primaryDAO
     *            the DAO for this manager
     */
    public LoadingStopManagerImplRoot(LoadingStopDAO primaryDAO) {
        super(primaryDAO);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingStopManagerRoot#
     * findStopByNumber(java.lang.String, java.lang.String, java.util.Date, int)
     */
    @Override
    public List<LoadingStop> listStopByNumber(Long stopNumber,
            LoadingRoute route) throws DataAccessException {
        return getPrimaryDAO().listStopByNumber(stopNumber, route);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.loading.service.LoadingStopManagerRoot#
     * findStopsByRoute(route, java.lang.Long)
     */
    @Override
    public List<LoadingStop> listStopsByRoute(LoadingRoute route,
            Long operatorID) throws DataAccessException {
        return getPrimaryDAO().listStopsByRoute(route, operatorID);
    }
    
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingStopManagerRoot#findStopByCustomer(java.lang.String)
     */
    @Override
    public LoadingStop findStopByCustomer(String route,
            Integer loadingRegionNumber, Date departureDateTime,
            String customerNumber) throws DataAccessException {
        return getPrimaryDAO().findStopByCustomer(
                route, loadingRegionNumber, departureDateTime, customerNumber);
    }

    /**
     * return actual number of loading containers that were created for this
     * stop
     * 
     * @return 0
     */
    @Override
    public int getActualContainerCount() {
        return 0;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 