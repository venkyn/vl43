/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.loading.dao.LoadingRouteLaborDAO;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;

import java.util.Date;
import java.util.List;

/**
 * 
 * 
 * @author ktanneru
 */
public abstract class LoadingRouteLaborManagerImplRoot extends
    GenericManagerImpl<LoadingRouteLabor, LoadingRouteLaborDAO> implements
    LoadingRouteLaborManager {
    
    private static final Double ONE_HUNDRED = 100.0;
    
    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.0;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRouteLaborManagerImplRoot(LoadingRouteLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * openLaborRecord(java.util.Date,
     * com.vocollect.voicelink.core.model.Operator,
     * com.vocollect.voicelink.loading.model.LoadingRoute,
     * com.vocollect.voicelink.core.model.OperatorLabor)
     */
    @Override
    public void openLaborRecord(Date startTime,
                         Operator operator,
                         LoadingRoute route,
                         OperatorLabor operLaborRecord)
            throws DataAccessException, BusinessRuleException {

        // close any open labor records for this assignment.
        // this is to handle the rare case of where the operator gets out of
        // radio range and get assignment is called for a second (or more) time.
        this.closeLaborRecord(startTime, route, operator);
        
        LoadingRouteLabor loadingRouteLabor = new LoadingRouteLabor();
        loadingRouteLabor.setRoute(route);
        loadingRouteLabor.setOperator(operator);
        loadingRouteLabor.setStartTime(startTime);
        loadingRouteLabor.setContainersLoaded(0);
        loadingRouteLabor.setOperatorLabor(operLaborRecord);
        loadingRouteLabor.setActualRate(0.0);
        loadingRouteLabor.setPercentOfGoal(0.0);
        loadingRouteLabor.setEndTime(null);
        loadingRouteLabor.setDuration(new Long(0));
        
        this.save(loadingRouteLabor);
    }

    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * updateLaborRecord(com.vocollect.voicelink.core.model.Operator,
     * com.vocollect.voicelink.loading.model.LoadingRoute)
     */
    @Override
    public void updateLaborRecord(Operator operator,
                                  LoadingRoute route)
        throws DataAccessException, BusinessRuleException {
        long routeId = route.getId();
        
        // Note: if multiple operator per route to be implemented, then this would
        // require OperatorId additionally in the search criteria
        LoadingRouteLabor loadingRouteLabor = this
            .findOpenRecordByRouteId(routeId, operator);

        //Increment total loaded containers with every update
        if (loadingRouteLabor != null) {
            loadingRouteLabor.setContainersLoaded(loadingRouteLabor
                .getContainersLoaded() + 1);
            this.save(loadingRouteLabor);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#recalculateAggregatesAndSave(com.vocollect.voicelink.loading.model.loadingRoutLabor)
     */
    public void recalculateAggregatesAndSave(LoadingRouteLabor al) throws DataAccessException, BusinessRuleException {
    	Date startTime = al.getStartTime();
    	Date endTime =al.getEndTime();
        al.setDuration(new Long(endTime.getTime() - startTime.getTime()));
        if (al.getDuration() > 0) {
            al.setActualRate(new Double(al.getContainersLoaded()
                / (al.getDuration() / MILLISEC_CONVERSION_FACTOR)));
            al.setPercentOfGoal(new Double(al.getActualRate()
                / (al.getRoute().getRegion().getGoalRate()) * ONE_HUNDRED));
        }
        this.save(al);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * closeLaborRecord(java.util.Date,
     * com.vocollect.voicelink.loading.model.LoadingRoute)
     */
    @Override
    public LoadingRouteLabor closeLaborRecord(Date endTime,
                                              LoadingRoute route,
                                              Operator operator)
        throws DataAccessException,
            BusinessRuleException {

        long routeId = route.getId();
        LoadingRouteLabor lrl = this.findOpenRecordByRouteId(routeId, operator);
        
        if (lrl != null) {
            Date startTime = lrl.getStartTime();
            lrl.setEndTime(endTime);
            lrl.setDuration(new Long(endTime.getTime() - startTime.getTime()));
            if (lrl.getDuration() > 0) {
                lrl.setActualRate(new Double(lrl.getContainersLoaded()
                    / (lrl.getDuration() / MILLISEC_CONVERSION_FACTOR)));
                lrl.setPercentOfGoal(new Double(lrl.getActualRate()
                    / (route.getRegion().getGoalRate()) * ONE_HUNDRED));
            }
            this.save(lrl);
        }
        return lrl;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * listAllRecordsByOperatorLaborId(long)
     */
    @Override
    public List<LoadingRouteLabor> listAllRecordsByOperatorLaborId(
            long operatorLaborId) throws DataAccessException {
        return getPrimaryDAO().listAllRecordsByOperatorLaborId(operatorLaborId);
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * findOpenRecordByRouteId(long)
     */
    @Override
    public LoadingRouteLabor findOpenRecordByRouteId(long routeId, Operator operator)
            throws DataAccessException {
        return getPrimaryDAO().findOpenRecordByRouteId(routeId, operator);
    }
    
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#
     * listOpenRecordsByOperatorId(long)
     */
    @Override
    public LoadingRouteLabor findOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().findOpenRecordsByOperatorId(operatorId);
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#listOpenRecordsByOperatorLaborId(long)
     */
    public List<LoadingRouteLabor> listClosedRecordsByBreakLaborId(long breakLaborId) throws DataAccessException {
        return getPrimaryDAO().listClosedRecordsByBreakLaborId(breakLaborId);
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 