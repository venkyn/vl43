/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.loading.dao.LoadingContainerImportDataDAO;
import com.vocollect.voicelink.loading.model.LoadingContainerImportData;
import com.vocollect.voicelink.loading.service.LoadingContainerImportDataManager;

import java.util.List;

/**
 * Standard implementation of the Importable Loading Container data manager implementation.
 * 
 * @author mraj
 * 
 */
public abstract class LoadingContainerImportDataManagerImplRoot
    extends
    GenericManagerImpl<LoadingContainerImportData, LoadingContainerImportDataDAO>
    implements LoadingContainerImportDataManager {

    private static final int RECORDS_TO_QUERY = 10;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingContainerImportDataManagerImplRoot(LoadingContainerImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    public List<LoadingContainerImportData> listContainerBySiteName(String siteName)
        throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(RECORDS_TO_QUERY);
        return getPrimaryDAO().listContainerBySiteName(qd, siteName);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.loading.service.LoadingContainerImportDataManagerRoot
     * #updateToComplete()
     */
    @Override
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 