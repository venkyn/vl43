/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.loading.model.ArchiveLoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.service.ArchiveLoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingRouteToArchiveManager;

import java.util.Date;
import java.util.List;

/**
 * @author mraj
 *
 */
public class LoadingRouteToArchiveManagerImplRoot implements
    LoadingRouteToArchiveManager {
    
    private LoadingRouteManager loadingRouteManager;

    private ArchiveLoadingRouteManager archiveLoadingRouteManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);
    
    
    /**
     * @return the routeManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    
    /**
     * @param loadingRouteManager the routeManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    
    /**
     * @return the archiveLoadingRouteManager
     */
    public ArchiveLoadingRouteManager getArchiveLoadingRouteManager() {
        return archiveLoadingRouteManager;
    }

    
    /**
     * @param archiveLoadingRouteManager the archiveRouteManager to set
     */
    public void setArchiveLoadingRouteManager(ArchiveLoadingRouteManager archiveLoadingRouteManager) {
        this.archiveLoadingRouteManager = archiveLoadingRouteManager;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteToArchiveManagerRoot#executeMarkForPurge(com.vocollect.voicelink.loading.model.LoadingRouteStatus, java.util.Date, boolean)
     */
    @Override
    public Integer executeMarkForPurge(LoadingRouteStatus status,
                                       Date olderThan,
                                       boolean archive) {
        Integer recordsMarked = 0;

        //Mark all routes to be purged
        if (log.isDebugEnabled()) {
            log.debug("### Finding Loading " + status.toString()
                + " Route to Purge :::");
        }
        try {
            recordsMarked = getLoadingRouteManager().getPrimaryDAO().updateOlderThan(
                archive ? 1 : 2, olderThan, status);
        } catch (DataAccessException e) {
            log.warn("!!! Error marking transactional "
                + "Loading " + status.toString() + " Route for Purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + recordsMarked
                + " Loading " + status.toString() + " Route for Purge :::");
        }
        return recordsMarked;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteToArchiveManagerRoot#executeArchive(com.vocollect.epp.dao.hibernate.finder.QueryDecorator)
     */
    @Override
    public int executeArchive(QueryDecorator decorator) {
        int returnRecords = 0;

        //Get List of routes to Archive
        if (log.isDebugEnabled()) {
            log.debug("### Finding Transactional Routes to Archive :::");
        }
        try {
            List<LoadingRoute> routes = getLoadingRouteManager()
                .getPrimaryDAO().listArchive(decorator);
            returnRecords = routes.size();

            for (LoadingRoute route : routes) {
                getArchiveLoadingRouteManager().save(new ArchiveLoadingRoute(route));
                route.setPurgeable(2);
                getLoadingRouteManager().save(route);
            }

        } catch (DataAccessException e) {
            log.warn("!!! Error getting Selection "
                + "Loading routes from database to purge: " + e);
            return 0;
        } catch (BusinessRuleException e) {
            log.warn("!!! Error creating Archive Loading Route: "
                + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Archived " + returnRecords
                + " Loading Routes ");
            log.debug("### Loading Routes Archive Process Complete :::");
        }

        getLoadingRouteManager().getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.loading.service.LoadingRouteToArchiveManagerRoot#executePurgeAssignment()
     */
    @Override
    public int executePurge(QueryDecorator queryDecorator) {
        int returnRecords = 0;

        List<LoadingRoute> routes = null;

        try {
            routes = getLoadingRouteManager().getPrimaryDAO().listPurge(
                queryDecorator);

            returnRecords = routes.size();
        } catch (DataAccessException e) {
            log.warn("!!!Error: DataAccessException purging transactional "
                + "Loading Routes from database: " + e);
        }

        for (LoadingRoute route : routes) {
            try {
                getLoadingRouteManager().delete(route);

            } catch (BusinessRuleException e) {
                log.warn("!!!Error: BusinessRuleException purging transactional "
                    + "Loading Routes from database: " + e);
            } catch (DataAccessException e) {
                log.warn("!!!Error: DataAccessException purging transactional "
                    + "Loading Routes from database: " + e);

            }

        }
        if (log.isDebugEnabled()) {
            log.debug("### Purged " + returnRecords
                + " rows from the database.");
            log.debug("### Loading Routes Purge Process Complete :::");
        }
        return returnRecords;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 