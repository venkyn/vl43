/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.loading.dao.LoadingRouteImportDataDAO;
import com.vocollect.voicelink.loading.dao.LoadingStopImportDataDAO;
import com.vocollect.voicelink.loading.model.LoadingRouteImportData;
import com.vocollect.voicelink.loading.service.LoadingRouteImportDataManager;

import java.util.List;
/**
 * @author mraj
 * 
 */
public abstract class LoadingRouteImportDataManagerImplRoot extends
    GenericManagerImpl<LoadingRouteImportData, LoadingRouteImportDataDAO>
    implements LoadingRouteImportDataManager {

    private static final int RECORDS_TO_QUERY = 10;

    private LoadingStopImportDataDAO loadingStopImportDataDAO = null;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRouteImportDataManagerImplRoot(LoadingRouteImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    public List<LoadingRouteImportData> listRouteBySiteName(String siteName)
        throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(RECORDS_TO_QUERY);
        return getPrimaryDAO().listRouteBySiteName(qd, siteName);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
        getLoadingStopImportDataDAO().updateToComplete();
    }
    
    /**
     * @return the loadingStopImportDataDAO
     */
    public LoadingStopImportDataDAO getLoadingStopImportDataDAO() {
        return loadingStopImportDataDAO;
    }

    /**
     * @param loadingStopImportDataDAO the loadingStopImportDataDAO to set
     */
    public void setLoadingStopImportDataDAO(LoadingStopImportDataDAO loadingStopImportDataDAO) {
        this.loadingStopImportDataDAO = loadingStopImportDataDAO;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 