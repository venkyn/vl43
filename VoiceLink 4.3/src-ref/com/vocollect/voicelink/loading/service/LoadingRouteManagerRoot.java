/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.dao.LoadingRouteDAO;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;

import java.util.Date;
import java.util.List;
/**
 * Business Service Interface to handle communication between presentation and
 * persistence layer for loading route related operations.
 * 
 * @author mraj
 * 
 */
public interface LoadingRouteManagerRoot extends
    GenericManager<LoadingRoute, LoadingRouteDAO>, DataProvider {
    
    /**
     * retrieves the loadingRoute of the specified routeID.
     * 
     * @param routeID - value to retrieve by
     * @return loadingRoute object
     * @throws DataAccessException - database exceptions
     */
    LoadingRoute findLoadingRouteFromID(long routeID) 
        throws DataAccessException;

    /**
     * Method to find record matching criteria of route, stop number and region.
     * @param route Lading route
     * @param departureDateTime departureDateTime
     * @param region loading region 
     * @return selected LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listImportBlockingCondition(String route, Date departureDateTime, LoadingRegion region)
        throws DataAccessException;
    
    /**
     * Method to find record matching criteria of route, stop number and region.
     * @param route Lading route
     * @param departureDateTime departureDateTime
     * @param region loading region
     * @return selected LoadingRoute object
     * @throws DataAccessException - dae
     */
    public List<LoadingRoute> listImportBlockingConditionWithStatus(String route,
                                                                    Date departureDateTime,
                                                                    LoadingRegion region)
        throws DataAccessException;
    
    /**
     * Get loadingRoute based on routeNumber and regionNumber.
     * 
     * @param routeNumber - the route number to search by.
     * @param regionNumber - the region in which operator signed in.
     * @return - the List of LoadingRoute, may be null
     * @throws DataAccessException - database exception
     */
    public List<LoadingRoute> listLoadingRouteByNumber(String routeNumber, Integer regionNumber)
        throws DataAccessException; 
    
    /**
     * Get In progress loadingRoute based on operatorID and regionNumber.
     * 
     * @param operatorID - the operatorID.
     * @param regionNumber - the region in which operator signed in.
     * @return - the List of LoadingRoute, may be null
     * @throws DataAccessException - database exception
     */
    public List<LoadingRoute> listInProgressLoadingRoute(Long operatorID, Integer regionNumber)
        throws DataAccessException; 
    
    /**
     * Method to validate if the Route can be edited with the new values or not.
     * @param loadingRoute LoadingRoute.
     * @param departureDateValue Date.
     * @param loadingRegion LoadingRegion.
     * @return boolean value.
     * @throws DataAccessException - dae
     */
    public boolean validateEditRoute(LoadingRoute loadingRoute,
            Date departureDateValue, LoadingRegion loadingRegion)
            throws DataAccessException;    
    
    /**
     * Get Active loadingRoute based on operatorID.
     * 
     * @param operatorID - the operatorID.
     * @return - the List of LoadingRoute, may be null
     * @throws DataAccessException - database exception
     */
    public List<LoadingRoute> listActiveLoadingRoute(Long operatorID)
        throws DataAccessException; 
    
    /**
     * Get Available loadingRoute based on region.
     * 
     * @param regionNumber - the region number to search by
     * @return - the List of LoadingRoute, may be null
     * @throws DataAccessException - database exception
     */
    public List<LoadingRoute> listAvailableLoadingRoute(Integer regionNumber)
        throws DataAccessException; 

    
    /**
     * This method is used in assignment import validation. It finds loading
     * routes that have matching route number, loading region, departure date
     * time and customer number. If one is not found, an import record should
     * fail validation.
     * 
     * @param route - loading route to look for.
     * @param loadingRegionNumber - loading RegionNumber to look for.
     * @param departureDateTime - loading departureDateTime to look for.
     * @param customerNumber - customerNumber to look for
     * @return - loadingRoute that blocks import, if any.
     * @throws DataAccessException -.
     */
    public LoadingRoute findLoadingRoute(String route,
                                         Integer loadingRegionNumber,
                                         Date departureDateTime,
                                         String customerNumber)
        throws DataAccessException;
    
    /**
     * In case of Change Route. Need to make the already working route into 
     * suspended state if only one operator is working else leave it in 
     * In Progress state.
     * @param operator Operator.
     * @throws DataAccessException
     */
    public void executeChangeOperationForOperator(Operator operator) 
        throws DataAccessException, BusinessRuleException;
    
    /**
     * Get routes by number and departure date.
     * 
     * @param route - loading route
     * @param departureDateTime - loading departureDateTime
     * @return - list of routes
     * @throws DataAccessException
     *             - database exceptions
     */
    public List<LoadingRoute> listRouteByNumberAndDDT(String route, Date departureDateTime)
            throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 