/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.loading.dao.LoadingRouteImportDataDAO;
import com.vocollect.voicelink.loading.model.LoadingRouteImportData;

import java.util.List;


/**
 * @author mraj
 *
 */
public interface LoadingRouteImportDataManagerRoot extends
    GenericManager<LoadingRouteImportData, LoadingRouteImportDataDAO>, DataProvider {

    /**
     * Get the list of importable loading routes by site name.
     * @param siteName name of the site to use
     * @return list of all importable loading routes for the site.
     * @throws DataAccessException for database errors.
     */
    public List<LoadingRouteImportData> listRouteBySiteName(String siteName) throws DataAccessException;
    
    /**
     * Update all LoadingRouteImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 