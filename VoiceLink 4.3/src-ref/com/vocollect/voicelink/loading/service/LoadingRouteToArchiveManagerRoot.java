/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;

import java.util.Date;


/**
 * Interface for method required to Archive LoadingRoute graph of objects 
 * @author mraj
 *
 */
public interface LoadingRouteToArchiveManagerRoot {

    /**
     * Mark route records for purging(2) or archiving(1).
     * @param status - status to look for
     * @param olderThan - how old to look for
     * @param archive - whether or not to mark for archive
     * @return - number archived
     */
    public Integer executeMarkForPurge(LoadingRouteStatus status, 
                                       Date olderThan,
                                       boolean archive);
    
    /**
     * Archive specified number of loading routes.
     * 
     * @param decorator - number of routes to list and archive
     * @return - number archived.
     */
    public int executeArchive(QueryDecorator decorator);

    /**
     * Purges routes based on date and status.
     * 
     * @param decorator - number of routes to list and archive
     * @return - number purged.
     */
    public int executePurge(QueryDecorator decorator);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 