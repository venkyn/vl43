/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.loading.dao.LoadingStopDAO;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingStop;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between presentation and
 * persistence layer for loading stop related operations.
 * 
 * @author mraj
 * 
 */
public interface LoadingStopManagerRoot extends
        GenericManager<LoadingStop, LoadingStopDAO>, DataProvider {

    /**
     * Method to find loading stop
     * 
     * @param stopNumber
     *            Loading stop number
     * @param route
     *            Loading route
     * @return the loading stop object
     * @throws DataAccessException
     *             - database failure
     */
    public List<LoadingStop> listStopByNumber(Long stopNumber,
            LoadingRoute route) throws DataAccessException;

    /**
     * Method to list loading stops on route basis.
     * 
     * @param route
     *            Loading route
     * @param operatorID
     *            operator working on that Route.
     * @return the loading stop object
     * @throws DataAccessException
     *             - database failure
     */
    public List<LoadingStop> listStopsByRoute(LoadingRoute route,
            Long operatorID) throws DataAccessException;

    /**
     * @param customerNumber
     *            The customer number
     * @return The stop for the customer
     */
    public LoadingStop findStopByCustomer(String route,
            Integer loadingRegionNumber, Date departureDateTime,
            String customerNumber) throws DataAccessException;

    /**
     * 
     * @return the number of containers that were actually created for this stop
     */
    public int getActualContainerCount();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 