/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import java.util.Date;
import java.util.List;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.loading.dao.ArchiveLoadingLaborDAO;
import com.vocollect.voicelink.loading.model.ArchiveLoadingLabor;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;

/**
 * 
 * 
 * @author yarora
 */
public interface ArchiveLoadingLaborManagerRoot extends
    GenericManager<ArchiveLoadingLabor, ArchiveLoadingLaborDAO>, DataProvider {


     
     /**
      * Returns a list of report objects based on OperatorId and a date range.
      * 
      * @param operatorIdentifier - operator
      * @param startDate - start of date range
      * @param endDate - end of date range
      * @return - list of objects
      * @throws DataAccessException - on db failure
      */
     List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                  Date startDate,
                                                  Date endDate)
         throws DataAccessException;
     
     /**
      * Returns a list of report objects based on OperatorId and a date range.
      * 
      * @param operatorId - operator 
      * @param startDate - start of date range
      * @param endDate - end of date range
      * @return list of object 
      * @throws DataAccessException on db failure
      */
     List<Object[]> listLaborDetailReportRecords(String operatorId, 
                                                 Date startDate, Date endDate) 
                                                 throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 