/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.dao.LoadingContainerDAO;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingTruckDiagramRow;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between presentation and
 * persistence layer for loading container related operations.
 * 
 * @author mraj
 * 
 */
public interface LoadingContainerManagerRoot extends
        GenericManager<LoadingContainer, LoadingContainerDAO>, DataProvider {

    /**
     * Method to get count of Container based on stop.
     * 
     * @param containerNumber
     *            Loading spoken Container
     * @param stop
     *            LoadingStop reference
     * @return Number of Containers in the stop.
     * @throws DataAccessException
     */
    public Integer countLoadingContainerByStop(String containerNumber,
            LoadingStop stop) throws DataAccessException;

    /**
     * Get the list of loadingContainers by container number,route number and
     * region.
     * 
     * @param route
     *            - LoadingRoute.
     * @param loadingRegion
     *            - LoadingRegion.
     * @return - the List of LoadingContainer, may be null
     * @throws DataAccessException
     *             - database exception
     */
    public List<LoadingContainer> listAvailableLoadingContainersForRoute(
            LoadingRoute route, LoadingRegion loadingRegion)
            throws DataAccessException;

    /**
     * Get loadingContainer based on containerNumber for Loading, unloading and
     * Consolidating.
     * 
     * @param containerNumber
     *            - the container number which is to be returned.
     * @param route
     *            - LoadingRoute
     * @param operatorID
     *            - operator who is associated to the route.
     * @param loadingContainerStatus
     *            - the status of the container.
     * @return - the list LoadingContainer.
     * @throws DataAccessException
     *             - database exception
     */
    public LoadingContainer findLoadingContainerByNumber(
            String containerNumber, LoadingRoute route, Long operatorID,
            List<LoadingContainerStatus> loadingContainerStatus)
            throws DataAccessException;
    
    
    /**
     * @param containerNumber Container number
     * @param containerStatus Status
     * @return The target container number
     */
    public LoadingContainer findLoadingContainerByNumberAndStatus(String containerNumber,
            List<LoadingContainerStatus> containerStatus);


    /**
     * Method to check if the containerExists.
     * 
     * @param operator
     *            Operator
     * @param containerNumber
     *            String
     * @param loadingRoute
     *            LoadingRoute
     * @param loadingRegion
     *            LoadingRegion
     * @return boolean (True if exists else False)
     * @throws DataAccessException
     */
    public boolean isContainerExist(Operator operator, String containerNumber,
            LoadingRoute loadingRoute, LoadingRegion loadingRegion)
            throws DataAccessException;

    /**
     * Method to get count of Master Container.
     * 
     * @param masterContainer
     *            Loading Master Container
     * @param routeNumber
     *            Loading route
     * @param regionNumber
     *            region number in which operator is signed in
     * @return Number of Master Containers.
     * @throws DataAccessException
     */
    public Integer countNumberOfMasterCont(String masterContainer,
            String routeNumber, Integer regionNumber)
            throws DataAccessException;

    /**
     * Method to find existing containers with matching criteria
     * 
     * @param containerNumber
     *            - the containerNumber
     * @return Loading container if found and null not found
     * @throws DataAccessException
     */
    public LoadingContainer findImportBlockingCondition(String containerNumber)
            throws DataAccessException;
    

    /**
     * Method to find if any containers exist for the route with custom
     * positions
     * @param routeId Id of the Completed route, for which configuration is to
     *            be found out.
     * @return true, if custom positions found, false if standard 
     * @throws DataAccessException
     */
    public boolean isTruckConfigurationCustom(long routeId)
        throws DataAccessException;
    
    /**
     * @param routeId Id of the route for which containers are to be fetched
     * @param isTruckConfigurationCustom false if containers are loaded as left and right. Else
     *            true
     * @return list of the <code> LoadingTruckDiagramRow</code> objects to be
     *         displayed on truck diagram
     * @throws DataAccessException
     */
    public List<LoadingTruckDiagramRow> listTruckLoadedContainers(long routeId,
                                                               boolean isTruckConfigurationCustom)
        throws DataAccessException;
    
    /**
     * Find the time of the last container record for the given operator
     * that is greater than or equal the minTime. 
     *   
     * @param operatorId - the operator to search.
     * @param minTime - the date/time returned must be greater or equal to minTime
     * @param maxTime - the date/time returned must be less or equal to maxTime
     * 
     * @return Date - the maximum end time from the loading labor record. 
     * 
     * @throws DataAccessException - database exceptions
     */
     public Date maxEndTime(long operatorId, Date minTime, Date maxTime) throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 