/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * @author mraj
 * 
 */
public interface LoadingRouteExportTransportManagerRoot extends
    LoadingRouteManager {

    /**
     * @return list of maps. Each map contains the fields for a completed route,
     *         the object, and the matchingStatus of '1,2'.
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedRoutes() throws DataAccessException;

    /**
     * 
     * @return list of export finish
     * @throws DataAccessException on database error.
     */
    public List<Map<String, Object>> listExportFinish()
        throws DataAccessException;

    /**
     * get route labor records to export.
     * 
     * @return list of export labor records
     * @throws DataAccessException - database errors
     */
    public List<Map<String, Object>> listExportRouteLabor()
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 