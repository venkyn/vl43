/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.loading.dao.LoadingRegionDAO;
import com.vocollect.voicelink.loading.model.LoadingRegion;

import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for loading region related operations.
 * 
 * @author kudupi
 */
public interface LoadingRegionManagerRoot 
    extends GenericManager<LoadingRegion, LoadingRegionDAO>, DataProvider {
    
    /**
     * Find the Loading region, given the region number.
     * @param regionNumber reference to the region
     * @return the loading region referenced, or null if not found
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    public LoadingRegion findRegionByNumber(int regionNumber)
    throws DataAccessException, BusinessRuleException;
    
    /**
     * Finds if the supplied region name is unique.
     * 
     * @param regionName
     *            to search
     * @return the record id if found, null if not found
     * @throws DataAccessException
     */
    public Long uniquenessByName(String regionName) throws DataAccessException;

    /**
     * Finds if the supplied region number is unique.
     * 
     * @param regionNumber
     *            to search
     * @return the record id if found, null if not found
     * @throws DataAccessException
     */
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException;
    
    /**
     * Validate the business rules for editing a loading region region.
     * @param region - region to edit
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    void executeValidateEditRegion(LoadingRegion region)
        throws DataAccessException, BusinessRuleException;
    
    /**
     * Loading Route Summary query.
     * 
     * @param rdi ResultDataInfo
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<DataObject> listLoadingRouteSummary(ResultDataInfo rdi)  
    throws DataAccessException;

    /**
     * Loading Current Work Summary query.
     * 
     * @param rdi ResultDataInfo
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi) 
    throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 