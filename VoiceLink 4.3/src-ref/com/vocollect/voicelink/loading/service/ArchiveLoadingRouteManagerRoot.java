/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.loading.dao.ArchiveLoadingRouteDAO;
import com.vocollect.voicelink.loading.model.ArchiveLoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;

import java.util.Date;

/**
 * @author mraj
 * 
 */
public interface ArchiveLoadingRouteManagerRoot extends
    GenericManager<ArchiveLoadingRoute, ArchiveLoadingRouteDAO>, DataProvider {

    /**
     * deletes archive routes.
     * 
     * @param createdDate - date to check for
     * @param status - status of routes
     * @return - Number of row deleted
     * @throws DataAccessException - Database Access Exceptions
     */
    Integer updateOlderThan(Date createdDate, LoadingRouteStatus status)
        throws DataAccessException;

    /**
     * Purges routes based on date and status.
     * 
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of routes purged
     */
    public int executePurge(LoadingRouteStatus status, Date olderThan);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 