/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.event.listeners;

import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.JasperReportWrapper;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.ReportWrapper;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_LOCALE;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_SOURCE_EXTENSION;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_TIMEZONE;
import static com.vocollect.epp.util.ReportWrapperRoot.REPORT_PATH_BASE;

import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.ReportPrinterMap;
import com.vocollect.voicelink.core.service.ReportPrinterManager;
import com.vocollect.voicelink.loading.events.PrintEvent;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingTruckDiagramRow;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.task.LoadingRequestPrintCmdRoot;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.context.ApplicationListener;
import org.springframework.context.i18n.LocaleContextHolder;



/**
 * @author yarora
 *
 */
@SuppressWarnings("unused")
public class TruckDiagramPrintEventListenerRoot implements
ApplicationListener<PrintEvent<LoadingRoute>> {

    private VoicelinkNotificationUtil voicelinkNotificationUtil;
    /**
     * The mapping of printer numbers to the network addresses of printers -
     * defined in the applicationContext-reportPrinters.xml file.
     */
    private ReportPrinterMap reportPrinterMap;
    
    private ReportPrinterManager reportPrinterManager;
    
    private LoadingContainerManager loadingContainerManager;
    
    private final String imagePath = "/images/vocollect_icon_32x32.gif";
    
    public static final String IMAGEPATH = "IMAGEPATH";
    
    private static final Logger log = new Logger(TruckDiagramPrintEventListenerRoot.class);
    

    /**
     * Getter for the voicelinkNotificationUtil property.
     * 
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }

    /**
     * Setter for the voicelinkNotificationUtil property.
     * 
     * @param voicelinkNotificationUtil
     *            the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(
            VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }
    /**
     * @return reportPrinterManager
     */
    public ReportPrinterManager getReportPrinterManager() {
        return reportPrinterManager;
    }

    /**
     * @param reportPrinterManager reportPrinterManager object
     */
    public void setReportPrinterManager(ReportPrinterManager reportPrinterManager) {
        this.reportPrinterManager = reportPrinterManager;
    }

    /**
     * @return reportPrinterMap
     */
    public ReportPrinterMap getReportPrinterMap() {
        return reportPrinterMap;
    }

    /**
     * @param reportPrinterMap reportPrinterMap object
     */
    public void setReportPrinterMap(ReportPrinterMap reportPrinterMap) {
        this.reportPrinterMap = reportPrinterMap;
    }

    /**
     * @return loadingContainerManager
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * @param loadingContainerManager loadingContainerManager object
     */
    public void setLoadingContainerManager(
            LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }
    
    

    @Override
    public void onApplicationEvent(PrintEvent<LoadingRoute> evt) {
        
        
        LoadingRoute route = evt.getObject();
        try {
            SiteContextHolder.setSiteContext(evt.getSiteContext());
            SiteContext siteContext = evt.getSiteContext();
            LocaleContextHolder.setLocale(evt.getLocale());
            String printerAddress = reportPrinterMap.getPrinter(Integer
                    .toString(evt.getPrinterNumber()));
            JasperPrint jasperPrint = generateTruckDiagram(route.getId(), siteContext);
            
            this.reportPrinterManager.printReport(
                    printerAddress, jasperPrint);
        } catch (VocollectException ve) {
            String processKey = "notification.column.keyname.Process.PrintingTruckDiagram";
            String messageKey = "errorCode.message.VoiceLink";
            String errorNumber = null;
            LOPArrayList lop = new LOPArrayList();
            if (evt.getOperator() == null) {
                lop.add("task.operator", new String("null"));
            } else {
                lop.add("task.operator", evt.getOperator().getOperatorIdentifier());
            }
            lop.add("task.route", route.getNumber());
            if (ve.getErrorCode() == CoreErrorCode.PRINTER_NOT_FOUND) {
                errorNumber = Long.toString((TaskErrorCode.PRINTER_NOT_FOUND.getErrorCode()));
                messageKey = messageKey + errorNumber;
            } else if (ve.getErrorCode() == ReportError.JASPER_PRINTSERVICE_EXCEPTION) {
                errorNumber = Long.toString(TaskErrorCode.JASPER_REPORT_PRINTESERVICE_EXCEPTION.getErrorCode());
                messageKey = messageKey + errorNumber;
            } else if (ve.getErrorCode() == ReportError.JASPER_LOAD_ERROR
                || ve.getErrorCode() == CoreErrorCode.REPORT_NOT_FOUND
                || ve.getErrorCode() == ReportError.BAD_REPORT_NAME) {
                errorNumber = Long.toString(TaskErrorCode.JASPER_LOAD_TRUCK_DIAGRAM_REPORT_NOT_FOUND.getErrorCode());
                messageKey = messageKey + errorNumber;
            } else if (ve.getErrorCode() == ReportError.JASPER_COMPILE_ERROR) {
                errorNumber = Long.toString(TaskErrorCode.JASPER_REPORT_COULD_NOT_BE_COMPILED.getErrorCode());
                messageKey = messageKey + errorNumber;
            } else if (ve.getErrorCode() == ReportError.JASPER_FILL_ERROR) {
                errorNumber = Long.toString(TaskErrorCode.JASPER_REPORT_COULD_NOT_BE_FILLED.getErrorCode());
                messageKey = messageKey + errorNumber;
            } else {
                errorNumber = Long.toString(TaskErrorCode.PRINTSERVER_EXCEPTION.getErrorCode());
                messageKey = "notification.column.Message.PrintServerError";
            }
            try {
                
                getVoicelinkNotificationUtil()
                 .createNotification(
                         processKey,
                         messageKey,
                         NotificationPriority.CRITICAL, new Date(),
                         errorNumber, lop);
                 } catch (Exception e) {
                  // Do nothing -- we don't care about
                     // failures to post the notificaiton
                     
                 }
        }
    }
    
    
    /**
     * Method implementation to generate the truck diagram.
     * @param routeId routeID
     * @param siteContext siteContext
     * @return jasperPrint JasperPrint
     * @throws VocollectException 
     */
    private JasperPrint generateTruckDiagram(Long routeId, SiteContext siteContext) throws VocollectException {
        boolean isTruckLoadCustom = getLoadingContainerManager()
            .isTruckConfigurationCustom(routeId);
        String reportName = isTruckLoadCustom
            ? "LoadingTruckDiagramList" : "LoadingTruckDiagram";
        File reportFile = new File(LoadingRequestPrintCmdRoot.class
            .getProtectionDomain().getCodeSource().getLocation().getPath());
        String filePath = null;
        try {
            filePath = reportFile.getCanonicalPath();
        } catch (IOException e) {
            throw new VocollectException(ReportError.REPORT_NOT_FOUND);
        }
        String absolutePath = filePath.replace(
            filePath.substring(
                filePath.indexOf("WEB-INF") - 1, filePath.length()),
            REPORT_PATH_BASE);
        
        String absoluteImagePath = filePath.replace(
            filePath.substring(
                filePath.indexOf("WEB-INF") - 1, filePath.length()),
                imagePath);
        try {
            absolutePath = URLDecoder.decode(absolutePath, "UTF-8");
            absoluteImagePath = URLDecoder.decode(absoluteImagePath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new VocollectException(ReportError.REPORT_NOT_FOUND);
        }
        if (log.isInfoEnabled()) {
            log.info("Generating report: " + reportName + " from "
                + absolutePath);
        }
        log.info("Image path is : " + absoluteImagePath);
        ReportWrapper reportWrapper = new JasperReportWrapper(
            absolutePath, reportName + JASPER_REPORT_SOURCE_EXTENSION);
        // Check and initialize if the report exists
        if (!reportWrapper.reportExists(absolutePath, reportName
            + JASPER_REPORT_SOURCE_EXTENSION)) {
            log.warn("Report definition " + reportName
                + JASPER_REPORT_SOURCE_EXTENSION + " NOT found at: "
                + absolutePath);
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put(JASPER_REPORT_LOCALE, LocaleContextHolder.getLocale());
        values.put(JASPER_REPORT_TIMEZONE, siteContext.getCurrentSite().getTimeZone());
        values.put(IMAGEPATH, absoluteImagePath);
        String reportFormat = "HTML";
        List<LoadingTruckDiagramRow> containerList = getLoadingContainerManager()
            .listTruckLoadedContainers(
                routeId, isTruckLoadCustom);
        Object dataSource = new JRBeanCollectionDataSource(containerList);

        JasperPrint jasperPrint = reportWrapper.generateReportForTask(
            values, reportFormat,
            LocaleContextHolder.getLocale().getLanguage(), dataSource);
        return jasperPrint;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 