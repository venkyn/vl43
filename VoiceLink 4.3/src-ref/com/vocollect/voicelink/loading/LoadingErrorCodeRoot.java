/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for loading business. Although the
 * range for these error codes is 11000-11999, please use only 11000-11499 for
 * the predefined ones; 11500-11999 should be reserved for customization error
 * codes.
 * 
 * @author kudupi
 */
public class LoadingErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND = 11000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND = 11999;

    /**
     * No error, just the base initialization.
     */
    public static final LoadingErrorCodeRoot NO_ERROR = new LoadingErrorCodeRoot();

    /**
     * Regions with operators loaded cannot be edited or deleted.
     */
    public static final LoadingErrorCode REGION_OPERATORS_SIGNED_IN = new LoadingErrorCode(
            11000);

    
    public static final LoadingErrorCode ROUTE_STATUS_INVALID
    = new LoadingErrorCode(11001);
    
    public static final LoadingErrorCode CONTAINER_STATUS_INVALID
    = new LoadingErrorCode(11002);

    public static final LoadingErrorCode ROUTE_AVAILABLE_ERROR_FOR_PRINT = new LoadingErrorCode(
            11003);

    /**
     * Constructor.
     */
    private LoadingErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * 
     * @param err
     *            - error to be logged
     */
    protected LoadingErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(LoadingErrorCodeRoot.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 