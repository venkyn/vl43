/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.util;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.impl.NotificationManagerImpl;
import com.vocollect.epp.util.LabelObjectPair;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskMessageInfo;

import java.util.Date;
import java.util.List;
import java.util.Properties;


/**
 *
 *
 * @author estoll
 */
public abstract class VoicelinkNotificationUtilRoot {

    private static final Logger log = new Logger(NotificationManagerImpl.class);

    // All 98 return codes are CRITICAL -- this is a Vocollect Rule
    private static final String TASK_CRITICAL_ERROR = "98";

    // 92 return codes are identified by the partner/customer as critical
    // for notification purposes but are still normal errors to the task
    private static final String TASK_MAKE_CRITICAL_ERROR = "92";

    // 91 return codes are identified by the partner/customer as warning
    // for notification purposes but are still normal errors to the task
    private static final String TASK_MAKE_WARNING_ERROR = "91";

    // 90 return codes are identified by the partner/customer as NO
    // notification - but are still normal errors to the task
    private static final String TASK_NO_NOTIFICATION_ERROR = "90";


    private Properties serverProperties;

    private NotificationManager notificationManager;



    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }


    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the serverProperties property.
     * @return Properties value of the property
     */
    public Properties getServerProperties() {
        return this.serverProperties;
    }

    /**
     * Setter for the serverProperties property.
     * @param serverProperties the new serverProperties value
     */
    public void setServerProperties(Properties serverProperties) {
        this.serverProperties = serverProperties;
    }

    /**
     * Helper to build a notificaiton object with the server name set
     * and the application set to VoiceLink.
     * @return a basic VoiceLink notification object
     */
    public Notification voiceLinkNotification() {
        Notification n = new Notification();

        String host = serverProperties.getProperty("server.name");
        if (host == null) {
            host = "notification.column.Host.unknown";
        }

        n.setHost(host);
        n.setApplication("notification.column.keyname.Application.VoiceLink");
        return n;
    }


    /**
     * Create a Notification with detail records that have a render
     * type = 2, resource key.
     * @param processKey - resource key of the process
     * @param messageKey - resource key of the message
     * @param priority - notification priority level
     * @param timeOfAction - timestamp of the notification
     * @param errorNumber - String representation of an error number
     * @param argDetails - LabelObjectPair of notificaiton detail information
     * @throws Exception - all exceptions
     */
    public void createRenderType2Notification(String processKey,
                                      String messageKey,
                                      NotificationPriority priority,
                                      Date timeOfAction,
                                      String errorNumber,
                                      List<LabelObjectPair> argDetails)
    throws Exception {
        Notification n = voiceLinkNotification();
        n.setProcess(processKey);
        n.setMessage(messageKey);
        n.setPriority(priority);
        n.setCreationDateTime(timeOfAction);
        n.setErrorNumber(errorNumber);

        // Save any parameters in the notificationDetails
        if (argDetails.size() > 0) {
            createNotificationDetails2(n, argDetails);
        }

        // If failing to save the notification throws an error
        // we need to catch this, log it, and allow the task
        // to get the error message back it was processing.
        getNotificationManager().createNotificationAndEmail(n);

    }

    /**
     * Create a Notification.
     * @param processKey - resource key of the process
     * @param messageKey - resource key of the message
     * @param priority - notification priority level
     * @param timeOfAction - timestamp of the notification
     * @param errorNumber - String representation of an error number
     * @param argDetails - LabelObjectPair of notificaiton detail information
     * @throws Exception - all exceptions
     */
    public void createNotification(String processKey,
                                      String messageKey,
                                      NotificationPriority priority,
                                      Date timeOfAction,
                                      String errorNumber,
                                      List<LabelObjectPair> argDetails)
    throws Exception {
        Notification n = voiceLinkNotification();
        n.setProcess(processKey);
        n.setMessage(messageKey);
        n.setPriority(priority);
        n.setCreationDateTime(timeOfAction);
        n.setErrorNumber(errorNumber);

        // Save any parameters in the notificationDetails
        if (argDetails.size() > 0) {
            createNotificationDetails(n, argDetails);
        }

        // If failing to save the notification throws an error
        // we need to catch this, log it, and allow the task
        // to get the error message back it was processing.
        getNotificationManager().createNotificationAndEmail(n);

    }

    /**
     * Create and save a notification based upon the contents
     * of a TaskMessageInfo object.
     * @param messageInfo object
     * @param baseCmd - task command object
     */
    public void postTaskNotification(TaskMessageInfo messageInfo,
                                     BaseTaskCommand baseCmd) {
        String errorCode = "";
        String errorMessage = "";

        // Verify member variables are not null
        // if either are null - set values so developer can see the
        // ills of their ways.
        if ((messageInfo.getErrorCode() == null)
            || (messageInfo.getErrorMessage() == null)) {
            errorCode = "XX";
            errorMessage = "notification.column.Message.TaskMessageError";
        } else {
            errorCode = messageInfo.getErrorCode();
            errorMessage = messageInfo.getDefaultMessageKey();
        }

        // first verify that the task return code does not equal "90"
        // "90" return codes are set by the partner/customer and they
        // indicate that NO Notification is to be posted
        if (messageInfo.getTaskReturnCode()
            .compareTo(TASK_NO_NOTIFICATION_ERROR) != 0) {

            Notification n = voiceLinkNotification();

            n.setProcess("notification.column.keyname.Process.Task");

            // 98 = Critical (Vocollect Rule)
            // 92 = Critical -- Partner or customer dictates this
            // 91 = Warning -- Partner or customer dictates this
            // All others are warning (Vocollect rule)
            if ((messageInfo.getTaskReturnCode()
                .compareTo(TASK_CRITICAL_ERROR) == 0)
                || (messageInfo.getTaskReturnCode()
                    .compareTo(TASK_MAKE_CRITICAL_ERROR) == 0)) {
                n.setPriority(NotificationPriority.CRITICAL);
            } else if (messageInfo.getTaskReturnCode()
                .compareTo(TASK_MAKE_WARNING_ERROR) == 0) {
                n.setPriority(NotificationPriority.WARNING);
            } else {
                n.setPriority(NotificationPriority.WARNING);
            }

            n.setErrorNumber(errorCode);
            n.setMessage(errorMessage);
            n.setCreationDateTime(baseCmd.getCommandTime());

            // Save any parameters in the notificationDetails
            createNotificationDetails2(n, messageInfo.getMessageParameters());

            // If failing to save the notification throws an error
            // we need to catch this, log it, and allow the task
            // to get the error message back it was processing.
            try {
                getNotificationManager().createNotificationAndEmail(n);
            } catch (Exception e) {
                logNotificationError(e, messageInfo, baseCmd);
            }
        }
    }

    /**
     * General purpose saveNotification object.
     * @param n - fully populated notification object.
     */
    public void saveNotification(Notification n) {
        try {
            getNotificationManager().createNotificationAndEmail(n);
        } catch (Exception e) {
            log.info("Class VoicelinkNotificationUtil.saveNotification "
                + " threw unexpected exception while posting notification ["
                + e.toString() + "]");
        }

    }

    /*
     * Private methods
     */

    /**
     * Extract the message parameters from TaskMessageInfo and save each
     * as a NotificationDetail object of the Notification pass.
     * @param n Notification Object
     * @param argDetails - LabelObjectPair list of notification details
     */
    protected void createNotificationDetails(Notification n,
                                             List<LabelObjectPair> argDetails) {
        createDetails(n, argDetails, "1");
    }

    /**
     * Extract the message parameters from TaskMessageInfo and save each
     * as a NotificationDetail object of the Notification pass - set the
     * render type = 2, resource key.
     * @param n Notification Object
     * @param argDetails - LabelObjectPair list of notification details
     */
    protected void createNotificationDetails2(Notification n,
                                             List<LabelObjectPair> argDetails) {

        createDetails(n, argDetails, "2");
    }

    /**
     * Extract the message parameters from TaskMessageInfo and save each
     * as a NotificationDetail object of the Notification pass.
     * @param n Notification Object
     * @param argDetails - LabelObjectPair list of notification details
     * @param renderType specifies the method of rendering the details.
     */
    protected void createDetails(Notification n,
                                 List<LabelObjectPair> argDetails,
                                 String renderType) {

        NotificationDetail detail = null;
        Integer order = 0;

        for (LabelObjectPair lop : argDetails) {
            detail = new NotificationDetail();
            detail.setOrdering(order.toString());
            detail.setKey(lop.getLabel());
            if (lop.getValue() != null) {
                detail.setValue(lop.getValue().toString());
            } else {
                detail.setValue("null");
            }
            detail.setRenderType(renderType);
            n.getDetails().add(detail);

            // Increment Order
            order++;
        }

    }



    /**
     * Log error that was thrown while trying to post notification.
     *
     * @param t - error that was thrown
     * @param messageInfo - TaskMessagInfo object.
     * @param baseCmd - Task command object.
     */
    protected void logNotificationError(Throwable t,
                                        TaskMessageInfo messageInfo,
                                        BaseTaskCommand baseCmd) {
        Throwable stackDump = null;
        if (t instanceof VocollectException) {
            VocollectException ve = (VocollectException) t;

            if (ve instanceof TaskCommandException) {
                if (ve.getCause() != null) {
                    log.error("Command "
                        + baseCmd.getDispatchName()
                        + " threw unexpected exception while posting notification"
                        + t.toString(), ve.getErrorCode(), t);
                } else if (log.isDebugEnabled()) {
                    // Don't dump stack traces for these, these are normal
                    // business rule failures.
                    log.debug("Command "
                        + baseCmd.getDispatchName()
                        + " threw exception while posting notification "
                        + ve.toString());
                }
            } else {
                ErrorCode code = TaskErrorCode.GENERAL_SYSTEM_ERROR;
                if (ve.getErrorCode() != null) {
                    code = ve.getErrorCode();
                }
                log.error(baseCmd, code,
                    stackDump);
            }
        } else {
            // Log unexpected non-VocollectException
            log.error(baseCmd,
                TaskErrorCode.GENERAL_SYSTEM_ERROR, t);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 