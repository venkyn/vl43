/*
 * Copyright (c) 2007 Vocollect, Inc.
 * Pittsburgh, PA 15235
 * All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.util;

/**
 * Format the time as HH:MM:SS for display in reports.
 * NOTE : Hours can be greater than 24.
 * @author amracna
 */
public final class ReportTimeFormatter {
    
    private static final long TEN = 10;
    private static final long FIVE_HUNDRED = 500;
    // 1000
    private static final long MILLISECS_PER_SEC = 1000;
    // 60000
    private static final long MILLISECS_PER_MINUTE = (60 * MILLISECS_PER_SEC);
    // 36000000
    private static final long MILLISECS_PER_HOUR = 60 * MILLISECS_PER_MINUTE;
    private static final long MAX_SECONDS = 59;
    private static final long MAX_MINUTES = 59;
    
    /**
     * Hide the default constructor for public access.
     * Constructor.
     */
    private ReportTimeFormatter() {
        
    }
    
    /**
     * 
     * @param milliseconds - value to format
     * @return formatted string
     * 
     */
     public static String formatTime(Long milliseconds) {

         // TODO verify format for internationization issues.
         long remainder;
         if (milliseconds == null) {
             remainder = 0;
         } else {
           remainder = milliseconds.longValue();    
         }

         StringBuffer output = new StringBuffer("");

         Long hours = new Long(remainder / MILLISECS_PER_HOUR);
         remainder = remainder % MILLISECS_PER_HOUR;
         Long minutes = new Long(remainder / MILLISECS_PER_MINUTE);
         remainder = remainder % MILLISECS_PER_MINUTE;
         Long seconds = new Long(remainder / MILLISECS_PER_SEC);
         remainder = remainder % MILLISECS_PER_SEC;
        
         if (remainder >= FIVE_HUNDRED) {
             
             // check for seconds rollover
             if (seconds == MAX_SECONDS) {
                 seconds = 0L;
                 //check for minutes rollover
                 if (minutes == MAX_MINUTES) {
                     minutes = 0L;
                     hours++;
                 } else {
                     minutes++;
                 }
             } else {
                 seconds++;
             }
         }
         if (hours < TEN) {
             output.append("0");
         }
         output.append(hours.toString());
         output.append(":");
         if (minutes < TEN) {
             output.append("0");
         }
         output.append(minutes.toString());
         output.append(":");
         if (seconds < TEN) {
             output.append("0");
         }
         output.append(seconds.toString());
         return output.toString();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 