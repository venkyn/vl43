/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.lineloading.model.CartonDetail;

import java.util.Date;
import java.util.List;

/**
 * Line Loading Carton Detail Access Object (DAO) interface.
 *
 */
public interface CartonDetailDAORoot extends GenericDAO<CartonDetail> {

    /**
     * Get all cartonsDetails associated with any of the specified cartons.
     * @param decorator - additional query instructions.
     * @return the List of cartons for the pallets.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listCartonDetails(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get all carton details for the given operator in the given date range.
     * @param operator - opearator to search
     * @param startDate - start date for date range
     * @param endDate - end date for date range.
     * @return list of carton details for the given operator
     * @throws DataAccessException on database error
     */
    List<CartonDetail> listMostRecentRecordsByOperator(Operator operator,
                                                       Date startDate,
                                                       Date endDate)
                                                       throws DataAccessException;


    /**
     * Get all carton details for the given operator where action time is
     * greater than the given start date.
     * @param operator - opearator to search
     * @param startDate - start date for date range
     * @return list of carton details for the given operator
     * @throws DataAccessException on database error
     */
    List<CartonDetail> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                            Date startDate)
                                                            throws DataAccessException;


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 