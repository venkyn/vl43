/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;

import java.util.Date;
import java.util.List;


/**
 * Line Loading Route Stop Access Object (DAO) interface.
 *
 */
public interface RouteStopDAORoot extends GenericDAO<RouteStop> {

    /**
     * retrieves the routeStop by the number.
     *
     * @param route - value to retrieve by
     * @param stop - value to retrieve by
     * @param wave - value to retrieve by
     * @return requested routestop
     * @throws DataAccessException - database exceptions
     */
    RouteStop findRouteStopByRouteStopWave(String route, String stop, String wave)
        throws DataAccessException;

    /**
     * Gets all RouteStops older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the RouteStopStatus used in the query
     * @return a list of RouteStops
     * @throws DataAccessException Database failure
     */
    List<RouteStop> listOlderThan(QueryDecorator decorator,
        Date date, RouteStopStatus status) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 