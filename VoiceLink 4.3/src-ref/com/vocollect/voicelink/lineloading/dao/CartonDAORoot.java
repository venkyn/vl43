/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.RouteStop;

import java.util.List;
import java.util.Map;


/**
 * Line Loading Carton Access Object (DAO) interface.
 *
 */
public interface CartonDAORoot extends GenericDAO<Carton> {


    /**
     * Get count of cartons available to be loaded for spur/region.
     *
     * @param region - Region to count for
     * @param spur - Spur to Count for
     * @return - total number of available cartons for spur/region
     * @throws DataAccessException - database exceptions
     */
    public Number countCartonsForSpur(Region region,
                                   String spur)
        throws DataAccessException;

    /**
     * Get count of cartons not loaded for route stop.

     * @param routeStop - route stop to count
     * @return - total number of carton not loaded for route/stop
     * @throws DataAccessException - database exceptions
     */
    public Number countCartonsRemainingForRouteStop(RouteStop routeStop)
        throws DataAccessException;


    /**
     * Find carton by number.
     *
     * @param number - Number of carton to find
     * @return - Carton
     * @throws DataAccessException - Database exception
     */
    public Carton findCartonByNumber(String number)
        throws DataAccessException;

    /**
     * Get all cartons associated with any of the specified pallets.
     * @param decorator - additional query instructions.
     * @return the List of cartons for the pallets.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listCartons(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * @param decorator - additional query instructions.
     * @return the list of lineloading to export
     * @throws DataAccessException - Database failure
     */
    List<Map<String, Object>> listExportLineloadingRecords(QueryDecorator decorator)
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 