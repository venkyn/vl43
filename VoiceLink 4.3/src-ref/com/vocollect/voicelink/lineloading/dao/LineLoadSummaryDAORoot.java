/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.lineloading.model.LineLoadSummary;

import java.util.Date;
import java.util.List;

/**
 * Line Loading Carton Detail Access Object (DAO) interface.
 *
 */
public interface LineLoadSummaryDAORoot extends GenericDAO<LineLoadSummary> {

    /**
     * Find line loading summary by wave information.
     * @param qd the QueryDecorator with filter info to pass on
     * @param timeWindow the time window in which to look for the info.
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSummaryByWave(QueryDecorator qd, Date timeWindow)  throws DataAccessException;

    /**
     * Find line loading summary by wave and selection region information.
     * @param timeWindow the time window in which to look for the info.
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSummaryByWaveAndPickingRegion(Date timeWindow)  throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 