/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 * @author mkoenig
 */
public class PalletRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -4163823034861799759L;

    private String number;

    private RouteStop routeStop;

    private PalletStatus status = PalletStatus.Opened;

    private Set<Carton> cartons = null;

    private int cartonCount = 0;

    private String spur;

    private Set<Tag> tags;





    /**
     * Getter for the spur property.
     * @return String value of the property
     */
    public String getSpur() {
        return spur;
    }





    /**
     * Setter for the spur property.
     * @param spur the new spur value
     */
    public void setSpur(String spur) {
        this.spur = spur;
    }




    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }




    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the cartonCount property.
     * @return int value of the property
     */
    public int getCartonCount() {
        return cartonCount;
    }


    /**
     * Setter for the cartonCount property.
     * @param cartonCount the new cartonCount value
     */
    public void setCartonCount(int cartonCount) {
        this.cartonCount = cartonCount;
    }



    /**
     * Getter for the cartons property.
     * @return Set&lt;Carton&gt; value of the property
     */
    public Set<Carton> getCartons() {
        if (cartons == null) {
            cartons = new HashSet<Carton>();
        }
        return cartons;
    }



    /**
     * Setter for the cartons property.
     * @param cartons the new cartons value
     */
    public void setCartons(Set<Carton> cartons) {
        this.cartons = cartons;
    }


    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the routeStop property.
     * @return RouteStop value of the property
     */
    public RouteStop getRouteStop() {
        return routeStop;
    }


    /**
     * Setter for the routeStop property.
     * @param routeStop the new routeStop value
     */
    public void setRouteStop(RouteStop routeStop) {
        this.routeStop = routeStop;
    }


    /**
     * Getter for the status property.
     * @return PalletStatus value of the property
     */
    public PalletStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(PalletStatus status) {
        this.status = status;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PalletRoot)) {
            return false;
        }
        final PalletRoot other = (PalletRoot) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return this.number == null ? 0 : this.number.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getNumber();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 