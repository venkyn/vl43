/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import java.io.Serializable;


/**
 * Pallet layout definition for line loading labels.
 *
 */
public class PalletLabelLayoutRoot implements Serializable {

    //
    private static final long serialVersionUID = -2391513510847174027L;
    
    private static final int STOPSTART_DEFAULT = 3;

    private Integer palletIDStart = 1;

    private Integer palletIDLength = 1;

    private Integer routeStart = 2;

    private Integer routeLength = 1;

    private Integer stopStart = STOPSTART_DEFAULT;

    private Integer stopLength = 1;


    /**
     * Getter for the palletIDLength property.
     * @return Integer value of the property
     */
    public Integer getPalletIDLength() {
        return palletIDLength;
    }


    /**
     * Setter for the palletIDLength property.
     * @param palletIDLength the new palletIDLength value
     */
    public void setPalletIDLength(Integer palletIDLength) {
        this.palletIDLength = palletIDLength;
    }


    /**
     * Getter for the palletIDStart property.
     * @return Integer value of the property
     */
    public Integer getPalletIDStart() {
        return palletIDStart;
    }


    /**
     * Setter for the palletIDStart property.
     * @param palletIDStart the new palletIDStart value
     */
    public void setPalletIDStart(Integer palletIDStart) {
        this.palletIDStart = palletIDStart;
    }


    /**
     * Getter for the routeLength property.
     * @return Integer value of the property
     */
    public Integer getRouteLength() {
        return routeLength;
    }


    /**
     * Setter for the routeLength property.
     * @param routeLength the new routeLength value
     */
    public void setRouteLength(Integer routeLength) {
        this.routeLength = routeLength;
    }


    /**
     * Getter for the routeStart property.
     * @return Integer value of the property
     */
    public Integer getRouteStart() {
        return routeStart;
    }


    /**
     * Setter for the routeStart property.
     * @param routeStart the new routeStart value
     */
    public void setRouteStart(Integer routeStart) {
        this.routeStart = routeStart;
    }


    /**
     * Getter for the stopLength property.
     * @return Integer value of the property
     */
    public Integer getStopLength() {
        return stopLength;
    }


    /**
     * Setter for the stopLength property.
     * @param stopLength the new stopLength value
     */
    public void setStopLength(Integer stopLength) {
        this.stopLength = stopLength;
    }


    /**
     * Getter for the stopStart property.
     * @return Integer value of the property
     */
    public Integer getStopStart() {
        return stopStart;
    }


    /**
     * Setter for the stopStart property.
     * @param stopStart the new stopStart value
     */
    public void setStopStart(Integer stopStart) {
        this.stopStart = stopStart;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 