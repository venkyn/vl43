/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author mkoenig
 */
public class CartonRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -1601581123319121265L;

    private String number;

    private RouteStop routeStop;

    private CartonType cartonType = CartonType.Case;

    private CartonStatus status = CartonStatus.Available;

    private CartonPickStatus pickStatus = CartonPickStatus.NotPicked;

    private int detailCount = 0;

    private Pallet pallet;

    private Operator operator;

    private Date loadTime;

    private String spur;

    private List<CartonDetail> details = null;

    private Set<Tag> tags;

    private ExportStatus exportStatus = ExportStatus.NotExported;

    /**
     * Getter for the exportStatus.
     * @return exportStatus of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */

    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }
    /**
     * Getter for the spur property.
     * @return String value of the property
     */
    public String getSpur() {
        return spur;
    }


    /**
     * Setter for the spur property.
     * @param spur the new spur value
     */
    public void setSpur(String spur) {
        this.spur = spur;
    }


    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }




    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the pallet property.
     * @return Pallet value of the property
     */
    public Pallet getPallet() {
        return pallet;
    }



    /**
     * Setter for the pallet property.
     * @param pallet the new pallet value
     */
    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }


    /**
     * Getter for the cartonType property.
     * @return CartonType value of the property
     */
    public CartonType getCartonType() {
        return cartonType;
    }


    /**
     * Setter for the cartonType property.
     * @param cartonType the new cartonType value
     */
    public void setCartonType(CartonType cartonType) {
        this.cartonType = cartonType;
    }


    /**
     * Getter for the detailCount property.
     * @return int value of the property
     */
    public int getDetailCount() {
        return detailCount;
    }


    /**
     * Setter for the detailCount property.
     * @param detailCount the new detailCount value
     */
    public void setDetailCount(int detailCount) {
        this.detailCount = detailCount;
    }


    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the pickStatus property.
     * @return CartonPickStatus value of the property
     */
    public CartonPickStatus getPickStatus() {
        return pickStatus;
    }


    /**
     * Setter for the pickStatus property.
     * @param pickStatus the new pickStatus value
     */
    public void setPickStatus(CartonPickStatus pickStatus) {
        this.pickStatus = pickStatus;
    }


    /**
     * Getter for the routeStop property.
     * @return RouteStop value of the property
     */
    public RouteStop getRouteStop() {
        return routeStop;
    }


    /**
     * Setter for the routeStop property.
     * @param routeStop the new routeStop value
     */
    public void setRouteStop(RouteStop routeStop) {
        this.routeStop = routeStop;
    }


    /**
     * Getter for the status property.
     * @return CartonStatus value of the property
     */
    public CartonStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    private void setStatus(CartonStatus status) {
        this.status = status;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CartonRoot)) {
            return false;
        }
        final CartonRoot other = (CartonRoot) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return this.number == null ? 0 : this.number.hashCode();
    }




    /**
     * Getter for the loadTime property.
     * @return Date value of the property
     */
    public Date getLoadTime() {
        return loadTime;
    }




    /**
     * Setter for the loadTime property.
     * @param loadTime the new loadTime value
     */
    public void setLoadTime(Date loadTime) {
        this.loadTime = loadTime;
    }




    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }




    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }




    /**
     * Getter for the details property.
     * @return List&lt;CartonDetail&gt; value of the property
     */
    public List<CartonDetail> getDetails() {
        if (details == null) {
            details = new ArrayList<CartonDetail>();
        }
        return details;
    }




    /**
     * Setter for the details property.
     * @param details the new details value
     */
    public void setDetails(List<CartonDetail> details) {
        this.details = details;
    }


    /**
     * Create a new carton detail record and update carton information.
     *
     * @param action - action to perform
     * @param newPallet - pallet loaded on NULL if not loaded
     * @param newOperator - operator performing action NULL if no operator
     * @param actionTime - time action took place
     * @return - return carton detail created
     */
    public CartonDetail addCartonDetail(CartonDetailAction action,
                                        Pallet newPallet,
                                        Operator newOperator,
                                        Date actionTime) {
        CartonDetail cartonDetail = new CartonDetail();

        cartonDetail.setAction(action);
        cartonDetail.setPallet(newPallet);
        cartonDetail.setOperator(newOperator);
        cartonDetail.setActionTime(actionTime);
        cartonDetail.setCarton((Carton) this);

        //Set Carton Status based on detail being added
        switch (action) {
        case InProgress:
            setStatus(CartonStatus.InProgress);
            break;
        case Loaded:
            setStatus(CartonStatus.Loaded);
            break;
        case Moved:
            setStatus(CartonStatus.Loaded);
            break;
        case SetAside:
            setStatus(CartonStatus.SetAside);
            break;
        case Shorted:
            setStatus(CartonStatus.Shorted);
            break;
        default:
            break;
        }

        //If carton is currently on a pallet decrement that pallets carton count
        //since the carton will be set aside or loaded to another pallet
        //(possibly the same pallet)
        if (getPallet() != null) {
            getPallet().setCartonCount(getPallet().getCartonCount() - 1);
        }
        if (newPallet != null) {
            newPallet.getCartons().add((Carton) this);
            newPallet.setCartonCount(newPallet.getCartonCount() + 1);
        }
        setPallet(newPallet);

        //set operator of carton
        if (newOperator != null) {
            this.setOperator(newOperator);
        }
        setLoadTime(actionTime);

        setDetailCount(getDetailCount() + 1);
        tagCartonDetails(cartonDetail);
        getDetails().add(cartonDetail);

        return cartonDetail;
    }

    /**
     * Apply site tag to carton detail information.
     *
     * @param detail - detail information to tag
     */
    private void tagCartonDetails(CartonDetail detail) {
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        siteContext.setSiteToCurrentSite(detail);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getNumber();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 