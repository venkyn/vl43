/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 *
 * @author mkoenig
 */
public class RouteStopRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -4478862187446550834L;

    private String route;

    private String stop;

    private LineLoadingRegion region;

    private String wave;

    private String spur;

    private int totalCartons = 0;

    private int availableCartons = 0;

    private int notPickedCartons = 0;

    private int setAsideOrShortedCartons = 0;

    private int missingCartons = 0;

    private RouteStopStatus status = RouteStopStatus.Opened;

    private Date dateClosed;

    private Set<Carton> cartons = null;

    private Set<Pallet> pallets = null;

    private Set<Tag> tags;

    private Date    dateOpened = new Date();





    /**
     * Getter for the dateOpened property.
     * @return Date value of the property
     */
    public Date getDateOpened() {
        return dateOpened;
    }



    /**
     * Setter for the dateOpened property.
     * @param dateOpened the new dateOpened value
     */
    public void setDateOpened(Date dateOpened) {
        this.dateOpened = dateOpened;
    }


    /**
     * Getter for the notPickedCartons property.
     * @return int value of the property
     */
    public int getNotPickedCartons() {
        return notPickedCartons;
    }


    /**
     * Setter for the notPickedCartons property.
     * @param notPickedCartons the new notPickedCartons value
     */
    public void setNotPickedCartons(int notPickedCartons) {
        this.notPickedCartons = notPickedCartons;
    }



    /**
     * Getter for the availableCartons property.
     * @return int value of the property
     */
    public int getAvailableCartons() {
        return availableCartons;
    }



    /**
     * Setter for the availableCartons property.
     * @param availableCartons the new availableCartons value
     */
    public void setAvailableCartons(int availableCartons) {
        this.availableCartons = availableCartons;
    }


    /**
     * Getter for the setAsideOrShortedCartons property.
     * @return int value of the property
     */
    public int getSetAsideOrShortedCartons() {
        return setAsideOrShortedCartons;
    }



    /**
     * Setter for the setAsideOrShortedCartons property.
     * @param setAsideOrShortedCartons the new setAsideOrShortedCartons value
     */
    public void setSetAsideOrShortedCartons(int setAsideOrShortedCartons) {
        this.setAsideOrShortedCartons = setAsideOrShortedCartons;
    }

    /**
     * Getter for the totalCartons property.
     * @return int value of the property
     */
    public int getTotalCartons() {
        return totalCartons;
    }


    /**
     * Setter for the totalCartons property.
     * @param totalCartons the new totalCartons value
     */
    public void setTotalCartons(int totalCartons) {
        this.totalCartons = totalCartons;
    }



    /**
     * Getter for the loadedCartons property.
     * @return int value of the property
     */
    public int getLoadedCartons() {
        return getTotalCartons() - (getAvailableCartons()
            + getSetAsideOrShortedCartons());
    }



    /**
     * Getter for the missingCartons property.
     * @return int value of the property
     */
    public int getMissingCartons() {
        return missingCartons;
    }




    /**
     * Setter for the missingCartons property.
     * @param missingCartons the new missingCartons value
     */
    public void setMissingCartons(int missingCartons) {
        this.missingCartons = missingCartons;
    }



    /**
     * Getter for the pickedCartons property.
     * @return int value of the property
     */
    public int getPickedCartons() {
        return getTotalCartons() - getNotPickedCartons();
    }




    /**
     * Getter for the spur property.
     * @return String value of the property
     */
    public String getSpur() {
        return spur;
    }


    /**
     * Setter for the spur property.
     * @param spur the new spur value
     */
    public void setSpur(String spur) {
        this.spur = spur;
    }

    /**
     * Getter for the pallets property.
     * @return Set&lt;Pallet&gt; value of the property
     */
    public Set<Pallet> getPallets() {
        if (pallets == null) {
            pallets = new HashSet<Pallet>();
        }
        return pallets;
    }


    /**
     * Setter for the pallets property.
     * @param pallets the new pallets value
     */
    public void setPallets(Set<Pallet> pallets) {
        this.pallets = pallets;
    }


    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }




    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the cartons property.
     * @return Set&lt;Carton&gt; value of the property
     */
    public Set<Carton> getCartons() {
        if (cartons == null) {
            cartons = new HashSet<Carton>();
        }
        return cartons;
    }



    /**
     * Setter for the cartons property.
     * @param cartons the new cartons value
     */
    public void setCartons(Set<Carton> cartons) {
        this.cartons = cartons;
    }


    /**
     * Getter for the dateClosed property.
     * @return Date value of the property
     */
    public Date getDateClosed() {
        return dateClosed;
    }


    /**
     * Setter for the dateClosed property.
     * @param dateClosed the new dateClosed value
     */
    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }


    /**
     * Getter for the region property.
     * @return LineLoadingRegion value of the property
     */
    public LineLoadingRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(LineLoadingRegion region) {
        this.region = region;
    }


    /**
     * Getter for the route property.
     * @return String value of the property
     */
    public String getRoute() {
        return route;
    }


    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }


    /**
     * Getter for the status property.
     * @return RouteStopStatus value of the property
     */
    public RouteStopStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(RouteStopStatus status) {
        this.status = status;
    }


    /**
     * Getter for the stop property.
     * @return String value of the property
     */
    public String getStop() {
        return stop;
    }


    /**
     * Setter for the stop property.
     * @param stop the new stop value
     */
    public void setStop(String stop) {
        this.stop = stop;
    }


    /**
     * Getter for the wave property.
     * @return String value of the property
     */
    public String getWave() {
        return wave;
    }


    /**
     * Setter for the wave property.
     * @param wave the new wave value
     */
    public void setWave(String wave) {
        this.wave = wave;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RouteStopRoot)) {
            return false;
        }
        final RouteStopRoot other = (RouteStopRoot) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getRoute();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 