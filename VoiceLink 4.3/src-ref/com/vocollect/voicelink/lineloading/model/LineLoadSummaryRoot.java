/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.selection.model.SelectionRegion;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * This model is used for both summary by wave and summary by wave and region
 * since the only different is the region which will be null if only doing
 * summary by wave.
 *
 * @author mkoenig
 */
public class LineLoadSummaryRoot extends BaseModelObject implements
    Serializable, Taggable {

    //
    private static final long serialVersionUID = 2595948325436159393L;

    private RouteStop routeStop;

    private SelectionRegion selectionRegion;

    private Date dateOpened;

    private String wave;

    private int totalCartons = 0;

    private int available = 0;

    private int loaded = 0;

    private int picked = 0;

    private int missing = 0;

    private int pickShorted = 0;

    private int setAsideOrShorted = 0;

    private int notPicked = 0;

    private Site site;

    private Long id;

    /**
     * Getter for the routeStop property.
     * @return RouteStop value of the property
     */
    public RouteStop getRouteStop() {
        return routeStop;
    }

    /**
     * Setter for the routeStop property.
     * @param routeStop the new routeStop value
     */
    public void setRouteStop(RouteStop routeStop) {
        this.routeStop = routeStop;
    }

    /**
     * Getter for the loaded property.
     * @return int value of the property
     */
    public int getLoaded() {
        return loaded;
    }

    /**
     * Setter for the loaded property.
     * @param loaded the new loaded value
     */
    public void setLoaded(int loaded) {
        this.loaded = loaded;
    }

    /**
     * Getter for the missing property.
     * @return int value of the property
     */
    public int getMissing() {
        return missing;
    }

    /**
     * Setter for the missing property. If the input is less than zero, missing
     * is set to zero.
     * @param missing the new missing value
     */
    public void setMissing(int missing) {
        if (missing > 0) {
            this.missing = missing;
        } else {
            this.missing = 0;
        }
    }

    /**
     * Getter for the picked property.
     * @return int value of the property
     */
    public int getPicked() {
        return picked;
    }

    /**
     * Setter for the picked property.
     * @param picked the new picked value
     */
    public void setPicked(int picked) {
        this.picked = picked;
    }

    /**
     * Getter for the selectionRegion property.
     * @return SelectionRegion value of the property
     */
    public SelectionRegion getSelectionRegion() {
        return selectionRegion;
    }

    /**
     * Setter for the selectionRegion property.
     * @param selectionRegion the new selectionRegion value
     */
    public void setSelectionRegion(SelectionRegion selectionRegion) {
        this.selectionRegion = selectionRegion;
    }

    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return routeStop.getTags();
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        //Do nothing since tag are based on routeStop and should not be set here.
    }

    /**
     * Getter for the notPicked property.
     * @return int value of the property
     */
    public int getNotPicked() {
        return notPicked;
    }

    /**
     * Setter for the notPicked property.
     * @param notPicked the new notPicked value
     */
    public void setNotPicked(int notPicked) {
        this.notPicked = notPicked;
    }

    /**
     * Getter for the available property.
     * @return int value of the property
     */
    public int getAvailable() {
        return available;
    }

    /**
     * Setter for the available property.
     * @param available the new available value
     */
    public void setAvailable(int available) {
        this.available = available;
    }

    /**
     * Getter for the dateOpened property.
     * @return Date value of the property
     */
    public Date getDateOpened() {
        return dateOpened;
    }

    /**
     * Setter for the dateOpened property.
     * @param dateOpened the new dateOpened value
     */
    public void setDateOpened(Date dateOpened) {
        this.dateOpened = dateOpened;
    }

    /**
     * Getter for the pickShorted property.
     * @return int value of the property
     */
    public int getPickShorted() {
        return pickShorted;
    }

    /**
     * Setter for the pickShorted property.
     * @param pickShorted the new pickShorted value
     */
    public void setPickShorted(int pickShorted) {
        this.pickShorted = pickShorted;
    }

    /**
     * Getter for the setAsideOrShorted property.
     * @return int value of the property
     */
    public int getSetAsideOrShorted() {
        return setAsideOrShorted;
    }

    /**
     * Setter for the setAsideOrShorted property.
     * @param setAsideOrShorted the new setAsideOrShorted value
     */
    public void setSetAsideOrShorted(int setAsideOrShorted) {
        this.setAsideOrShorted = setAsideOrShorted;
    }

    /**
     * Getter for the totalCartons property.
     * @return int value of the property
     */
    public int getTotalCartons() {
        return totalCartons;
    }

    /**
     * Setter for the totalCartons property.
     * @param totalCartons the new totalCartons value
     */
    public void setTotalCartons(int totalCartons) {
        this.totalCartons = totalCartons;
    }

    /**
     * Getter for the wave property.
     * @return String value of the property
     */
    public String getWave() {
        return wave;
    }

    /**
     * Setter for the wave property.
     * @param wave the new wave value
     */
    public void setWave(String wave) {
        this.wave = wave;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LineLoadSummaryRoot)) {
            return false;
        }
        final LineLoadSummaryRoot other = (LineLoadSummaryRoot) obj;
        if (selectionRegion == null) {
            if (other.selectionRegion != null) {
                return false;
            }
        } else if (!selectionRegion.equals(other.selectionRegion)) {
            return false;
        }
        if (dateOpened == null) {
            if (other.dateOpened != null) {
                return false;
            }
        } else if (!dateOpened.equals(other.dateOpened)) {
            return false;
        }
        if (wave == null) {
            if (other.wave != null) {
                return false;
            }
        } else if (!wave.equals(other.wave)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((selectionRegion == null) ? 0 : selectionRegion.hashCode());
        result = prime * result
            + ((dateOpened == null) ? 0 : dateOpened.hashCode());
        result = prime * result + ((wave == null) ? 0 : wave.hashCode());
        return result;
    }

    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return this.site;
    }

    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 