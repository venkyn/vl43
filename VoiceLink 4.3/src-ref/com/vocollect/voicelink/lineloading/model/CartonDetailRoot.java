/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 *
 *
 * @author mkoenig
 */
public class CartonDetailRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -6356123926165148504L;

    private Carton carton;

    private Pallet pallet;

    private Operator operator;

    private Date actionTime;

    private CartonDetailAction action;

    private Set<Tag> tags;



    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        if (tags == null) {
            tags = new HashSet<Tag>();
        }
        return tags;
    }




    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }



    /**
     * Getter for the action property.
     * @return CartonDetailAction value of the property
     */
    public CartonDetailAction getAction() {
        return action;
    }


    /**
     * Setter for the action property.
     * @param action the new action value
     */
    public void setAction(CartonDetailAction action) {
        this.action = action;
    }


    /**
     * Getter for the actionTime property.
     * @return Date value of the property
     */
    public Date getActionTime() {
        return actionTime;
    }


    /**
     * Setter for the actionTime property.
     * @param actionTime the new actionTime value
     */
    public void setActionTime(Date actionTime) {
        this.actionTime = actionTime;
    }


    /**
     * Getter for the carton property.
     * @return Carton value of the property
     */
    public Carton getCarton() {
        return carton;
    }


    /**
     * Setter for the carton property.
     * @param carton the new carton value
     */
    public void setCarton(Carton carton) {
        this.carton = carton;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the pallet property.
     * @return Pallet value of the property
     */
    public Pallet getPallet() {
        return pallet;
    }


    /**
     * Setter for the pallet property.
     * @param pallet the new pallet value
     */
    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CartonDetailRoot)) {
            return false;
        }
        final CartonDetailRoot other = (CartonDetailRoot) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 