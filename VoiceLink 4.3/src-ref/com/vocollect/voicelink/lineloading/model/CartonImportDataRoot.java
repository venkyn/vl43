/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;

import java.util.Date;

/**
 * @author dgold
 *
 */
public class CartonImportDataRoot extends BaseModelObject implements Importable {

    private static final long serialVersionUID = -5642731818106575021L;

    private Carton myModel = new Carton();

    private ImportableImpl impl = new ImportableImpl();


    /**
     * Gets the value of regionNumber.
     * @return the regionNumber
     */
    public String getRegionNumber() {
        getRouteStop(); // This assures there is a route-stop object.
        LineLoadingRegion region = myModel.getRouteStop().getRegion();
        if (null == region) {
            region = new LineLoadingRegion();
            myModel.getRouteStop().setRegion(region);
        }
        return region.getNumber().toString();
    }


    /**
     * Sets the value of the regionNumber.
     * @param regionNumber the regionNumber to set
     */
    public void setRegionNumber(String regionNumber) {
        getRouteStop(); // This assures there is a route-stop object.
        LineLoadingRegion region = myModel.getRouteStop().getRegion();
        if (null == region) {
            region = new LineLoadingRegion();
            myModel.getRouteStop().setRegion(region);
        }
        region.setNumber(Integer.valueOf(regionNumber));
    }


    /**
     * Gets the value of routeNumber.
     * @return the routeNumber
     */
    public String getRouteNumber() {
        RouteStop routeStop = getRouteStop();
        return routeStop.getRoute();
    }


    /**
     * Sets the value of the routeNumber.
     * @param routeNumber the routeNumber to set
     */
    public void setRouteNumber(String routeNumber) {
        getRouteStop().setRoute(routeNumber);
    }


    /**
     * Gets the value of stopNumber.
     * @return the stopNumber
     */
    public String getStopNumber() {
        return getRouteStop().getStop();
    }


    /**
     * Sets the value of the stopNumber.
     * @param stopNumber the stopNumber to set
     */
    public void setStopNumber(String stopNumber) {
        getRouteStop().setStop(stopNumber);
    }


    /**
     * Gets the value of waveNumber.
     * @return the waveNumber
     */
    public String getWaveNumber() {
        return getRouteStop().getWave();
    }


    /**
     * Sets the value of the waveNumber.
     * @param waveNumber the waveNumber to set
     */
    public void setWaveNumber(String waveNumber) {
        getRouteStop().setWave(waveNumber);
    }


    /**
     * {@inheritDoc}
     * @return
     * @throws VocollectException
     */
    public Object convertToModel() throws VocollectException {
        return myModel;
    }


    /**
     * {@inheritDoc}
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CartonImportDataRoot)) {
            return false;
        }
        final CartonImportDataRoot other = (CartonImportDataRoot) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public int hashCode() {
        return this.myModel.getNumber() == null ? 0 : this.myModel.getNumber().hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getCartonType()
     */
    public CartonType getCartonType() {
        return myModel.getCartonType();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getLoadTime()
     */
    public Date getLoadTime() {
        return myModel.getLoadTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getPickStatus()
     */
    public CartonPickStatus getPickStatus() {
        return myModel.getPickStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getRouteStop()
     */
    public RouteStop getRouteStop() {
        RouteStop routeStop = myModel.getRouteStop();
        if (null == routeStop) {
            routeStop = new RouteStop();
            myModel.setRouteStop(routeStop);
        }
        return myModel.getRouteStop();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getSpur()
     */
    public String getSpur() {
        return myModel.getSpur();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#getStatus()
     */
    public CartonStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#setCartonType(com.vocollect.voicelink.lineloading.model.CartonType)
     */
    public void setCartonType(CartonType cartonType) {
        myModel.setCartonType(cartonType);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#setPickStatus(com.vocollect.voicelink.lineloading.model.CartonPickStatus)
     */
    public void setPickStatus(CartonPickStatus pickStatus) {
        myModel.setPickStatus(pickStatus);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#setRouteStop(com.vocollect.voicelink.lineloading.model.RouteStop)
     */
    public void setRouteStop(RouteStop routeStop) {
        myModel.setRouteStop(routeStop);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.model.CartonRoot#setSpur(java.lang.String)
     */
    public void setSpur(String spur) {
        myModel.setSpur(spur);
        getRouteStop().setSpur(spur);
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 