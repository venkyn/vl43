/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.voicelink.core.model.Region;


/**
 * Line Loading Region.
 *
 * @author mkoenig
 */
public class LineLoadingRegionRoot extends Region {

    //
    private static final long serialVersionUID = -5994189553811617628L;

    private PalletIdentificationMode palletIdentificationMode =
        PalletIdentificationMode.ScanPalletForEveryCarton;

    private boolean allowCloseStopFromTask = false;

    private boolean autoPrintManifestReport = true;

    private PalletLabelLayout palletLabelLayout = new PalletLabelLayout();


    /**
     * Getter for the allowCloseStopFromTask property.
     * @return boolean value of the property
     */
    public boolean getAllowCloseStopFromTask() {
        return allowCloseStopFromTask;
    }


    /**
     * Setter for the allowCloseStopFromTask property.
     * @param allowCloseStopFromTask the new allowCloseStopFromTask value
     */
    public void setAllowCloseStopFromTask(boolean allowCloseStopFromTask) {
        this.allowCloseStopFromTask = allowCloseStopFromTask;
    }


    /**
     * Getter for the autoPrintManifestReport property.
     * @return boolean value of the property
     */
    public boolean getAutoPrintManifestReport() {
        return autoPrintManifestReport;
    }


    /**
     * Setter for the autoPrintManifestReport property.
     * @param autoPrintManifestReport the new autoPrintManifestReport value
     */
    public void setAutoPrintManifestReport(boolean autoPrintManifestReport) {
        this.autoPrintManifestReport = autoPrintManifestReport;
    }


    /**
     * Getter for the palletIdentificationMode property.
     * @return PalletIdentificationMode value of the property
     */
    public PalletIdentificationMode getPalletIdentificationMode() {
        return palletIdentificationMode;
    }


    /**
     * Setter for the palletIdentificationMode property.
     * @param palletIdentificationMode the new palletIdentificationMode value
     */
    public void setPalletIdentificationMode(PalletIdentificationMode palletIdentificationMode) {
        this.palletIdentificationMode = palletIdentificationMode;
    }


    /**
     * Getter for the palletLabelLayout property.
     * @return PalletLabelLayout value of the property
     */
    public PalletLabelLayout getPalletLabelLayout() {
        return palletLabelLayout;
    }


    /**
     * Setter for the palletLabelLayout property.
     * @param palletLabelLayout the new palletLabelLayout value
     */
    public void setPalletLabelLayout(PalletLabelLayout palletLabelLayout) {
        this.palletLabelLayout = palletLabelLayout;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 