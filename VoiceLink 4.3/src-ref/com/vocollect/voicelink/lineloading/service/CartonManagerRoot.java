/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.lineloading.dao.CartonDAO;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonType;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.RouteStop;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Lineloading-related operations.
 *
 */
public interface CartonManagerRoot
extends GenericManager<Carton, CartonDAO>, DataProvider {

    /**
     * Get count of cartons available to be loaded for spur/region.
     *
     * @param region - Region to count for
     * @param spur - Spur to Count for
     * @return - total number of available cartons for spur/region
     * @throws DataAccessException - database exceptions
     */
    public int countCartonsForSpur(Region region,
                                   String spur)
        throws DataAccessException;

    /**
     * Get count of cartons not loaded for route stop.

     * @param routeStop - route stop to count
     * @return - total number of carton not loaded for route/stop
     * @throws DataAccessException - database exceptions
     */
    public int countCartonsRemainingForRouteStop(RouteStop routeStop)
    throws DataAccessException;

    /**
     * Find carton by number.
     *
     * @param number - Number of carton to find
     * @return - Carton
     * @throws DataAccessException - Database exception
     */
    public Carton findCartonByNumber(String number)
    throws DataAccessException;

    /**
     * Update a carton's pick status. Usually called from selection.
     *
     * @param cartonNumber - Carton number to update
     * @param expectedType - type of carton expected
     * @param quantityPicked - quantity that was picked to carton
     * @throws DataAccessException - database exceptions
     */
    public void executeUpdatePickStatus(String cartonNumber,
                                        CartonType expectedType,
                                        int quantityPicked)
    throws DataAccessException;

    /**
     * Manually load a carton to specified pallet.
     *
     * @param carton - carton to load.
     * @param pallet - pallet to load on.
     * @param operator - operator who loaded the carton
     * @throws BusinessRuleException - Business rule exceptions
     */
    public void executeManualLoad(Carton carton, Pallet pallet, Operator operator)
    throws BusinessRuleException;


    /**
     * Build a list of summary object to display for line loading summry by wave.
     *
     * @param rdi - input
     * @return - list of summary objects
     * @throws DataAccessException - database exceptions
     */
    public List<DataObject> listSummaryByWave(ResultDataInfo rdi)
    throws DataAccessException;

    /**
     * Build a list of summary object to display for line loading summry
     * by wave and selection region.
     *
     * @param rdi - input
     * @return - list of summary objects
     * @throws DataAccessException - database exceptions
     */
    public List<DataObject> listSummaryByWaveAndRegion(ResultDataInfo rdi)
    throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 