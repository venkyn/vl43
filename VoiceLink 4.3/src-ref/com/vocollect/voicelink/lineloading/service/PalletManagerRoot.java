/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.lineloading.dao.PalletDAO;
import com.vocollect.voicelink.lineloading.model.Pallet;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Lineloading-related operations.
 *
 */
public interface PalletManagerRoot
extends GenericManager<Pallet, PalletDAO>, DataProvider {

    /**
     * Find Pallet by number.
     *
     * @param number - Number of pallet to find
     * @return - pallet
     * @throws DataAccessException - Database exception
     */
    public Pallet findPalletByNumber(String number)
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 