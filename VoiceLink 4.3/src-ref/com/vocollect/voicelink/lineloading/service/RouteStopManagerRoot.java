/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.lineloading.dao.RouteStopDAO;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;

import java.util.Date;
import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Lineloading-related operations.
 *
 */
public interface RouteStopManagerRoot
extends GenericManager<RouteStop, RouteStopDAO>, DataProvider {


    /**
     * Determines if all cartons for route stop have been loaded or not.
     *
     * @param routeStop - routeStop to check
     * @return - true if all loaded
     * @throws DataAccessException - database exceptions
     */
    public boolean allCartonsLoaded(RouteStop routeStop)
    throws DataAccessException;

    /**
     * Deterimes if all cartons loaded by calling allCartonsLoaded. If not all
     * loaded an error is thrown.
     *
     * @param routeStop - route stop to check
     * @throws DataAccessException - database exceptions.
     * @throws BusinessRuleException - business rule exceptions.
     */
    public void checkAllCartonsLoaded(RouteStop routeStop)
    throws DataAccessException, BusinessRuleException;

    /**
     * Close a route stop.
     *
     * @param routeStop - routeStop to close
     * @param dateClosed - date route stop is closed
     */
    public void executeCloseRouteStop(RouteStop routeStop, Date dateClosed);


    /**
     * Verify route stop and print manifest report.
     *
     * @param routeStop - route stop to print.
     * @throws BusinessRuleException - Business rule exceptions
     */
    public void executePrintManifestReport(RouteStop routeStop)
    throws BusinessRuleException;

    /**
     * retrieves the routeStop by the number.
     *
     * @param route - value to retrieve by
     * @param stop - value to retrieve by
     * @param wave - value to retrieve by
     * @return requested routestop
     * @throws DataAccessException - database exceptions
     */
    RouteStop findRouteStopByRouteStopWave(String route, String stop, String wave)
        throws DataAccessException;

    /**
     * Gets all RouteStops older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the RouteStopStatus used in the query
     * @return a list of RouteStops
     * @throws DataAccessException Database failure
     */
    public List<RouteStop> listOlderThan(QueryDecorator decorator,
        Date date, RouteStopStatus status) throws DataAccessException;


    /**
     * Purges Route Stops based on date and status.
     *
     * @param decorator - query decorator
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of Route Stops purged
     */
    public int executePurge(QueryDecorator decorator,
                            RouteStopStatus status,
                            Date olderThan);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 