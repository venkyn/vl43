/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 *
 * @author vsubramani
 *
 */
public interface LineloadingExportTransportManagerRoot extends CartonManager {

    /**
     * @return list of assignment labor export records
     * @throws DataAccessException - database failure
     */
    public List<Map<String, Object>> listExportLineloading()
    throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 