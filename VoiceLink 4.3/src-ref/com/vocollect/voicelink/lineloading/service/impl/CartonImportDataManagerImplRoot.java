/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.lineloading.dao.CartonImportDataDAO;
import com.vocollect.voicelink.lineloading.model.CartonImportData;
import com.vocollect.voicelink.lineloading.service.CartonImportDataManager;

import java.util.List;

/**
 * @author dgold
 *
 */
public abstract class CartonImportDataManagerImplRoot extends
        GenericManagerImpl<CartonImportData, CartonImportDataDAO> implements CartonImportDataManager {

    /**
     * default constructor.
     * @param primaryDAO The primary DAO, called to access DB records.
     */
    public CartonImportDataManagerImplRoot(CartonImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    public List<CartonImportData> listCartonBySiteName(String siteName)
            throws DataAccessException {
        return getPrimaryDAO().listCartonBySiteName(siteName);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 