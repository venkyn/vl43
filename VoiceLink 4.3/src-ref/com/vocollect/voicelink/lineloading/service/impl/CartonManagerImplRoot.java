/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.lineloading.LineLoadingErrorCode;
import com.vocollect.voicelink.lineloading.dao.CartonDAO;
import com.vocollect.voicelink.lineloading.dao.LineLoadSummaryDAO;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonDetailAction;
import com.vocollect.voicelink.lineloading.model.CartonPickStatus;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.lineloading.model.CartonType;
import com.vocollect.voicelink.lineloading.model.LineLoadSummary;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.PalletStatus;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.selection.model.SelectionRegion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Additional service methods for the <code>Carton</code> model object.
 *
 */
public abstract class CartonManagerImplRoot extends
    GenericManagerImpl<Carton, CartonDAO> implements CartonManager {

    private LineLoadSummaryDAO lineLoadSummaryDAO;

    private static final int ROUTE_STOP = 0;

    private static final int DATE_OPENED = 1;

    private static final int WAVE = 2;

    private static final int TOTAL_CARTONS = 3;

    private static final int AVAILABLE = 4;

    private static final int SET_ASIDE = 5;

    private static final int LOADED = 6;

    private static final int PICKED = 7;

    private static final int NOT_PICKED = 8;

    private static final int PICK_SHORTED = 9;

    private static final int MISSING = 10;

    private static final int SEL_REGION = 11;

    /**
     * Getter for the lineLoadSummaryDAO property.
     * @return LineLoadSummaryDAO value of the property
     */
    public LineLoadSummaryDAO getLineLoadSummaryDAO() {
        return lineLoadSummaryDAO;
    }

    /**
     * Setter for the lineLoadSummaryDAO property.
     * @param lineLoadSummaryDAO the new lineLoadSummaryDAO value
     */
    public void setLineLoadSummaryDAO(LineLoadSummaryDAO lineLoadSummaryDAO) {
        this.lineLoadSummaryDAO = lineLoadSummaryDAO;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CartonManagerImplRoot(CartonDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#countCartonsForSpur(com.vocollect.voicelink.core.model.Region,
     *      java.lang.String)
     */
    public int countCartonsForSpur(Region region, String spur)
        throws DataAccessException {
        return convertNumberToInt(getPrimaryDAO().countCartonsForSpur(region, spur));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#findCartonByNumber(java.lang.String)
     */
    public Carton findCartonByNumber(String number) throws DataAccessException {
        return getPrimaryDAO().findCartonByNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#countCartonsRemainingForRouteStop(com.vocollect.voicelink.lineloading.model.RouteStop)
     */
    public int countCartonsRemainingForRouteStop(RouteStop routeStop)
        throws DataAccessException {

        return convertNumberToInt(
            getPrimaryDAO().countCartonsRemainingForRouteStop(routeStop));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#executeUpdatePickStatus(java.lang.String,
     *      com.vocollect.voicelink.lineloading.model.CartonType, int)
     */
    public void executeUpdatePickStatus(String cartonNumber,
                                        CartonType expectedType,
                                        int quantityPicked)
        throws DataAccessException {
        Carton carton = findCartonByNumber(cartonNumber);

        if ((carton != null) && carton.getCartonType().equals(expectedType)) {
            // If no quantity and status is not picked, then set to short
            if ((quantityPicked == 0)
                && carton.getPickStatus().equals(CartonPickStatus.NotPicked)) {
                carton.setPickStatus(CartonPickStatus.Shorted);
            } else if (quantityPicked > 0) {
                carton.setPickStatus(CartonPickStatus.Picked);
            }
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        return this.getPrimaryDAO().listCartons(new QueryDecorator(rdi));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#executeManualLoad(com.vocollect.voicelink.lineloading.model.Carton,
     *      com.vocollect.voicelink.lineloading.model.Pallet)
     */
    public void executeManualLoad(Carton carton, Pallet pallet, Operator operator)
        throws BusinessRuleException {

        // Carton cannot be shorted
        if (carton.getStatus().equals(CartonStatus.Shorted)) {
            throw new BusinessRuleException(
                LineLoadingErrorCode.CARTON_CANNOT_LOAD_SHORT, new UserMessage(
                    "lineload.manualLoad.error.cartonsShort"));
        }

        // pallet cannot be closed
        if (pallet.getStatus().equals(PalletStatus.Closed)) {
            throw new BusinessRuleException(
                LineLoadingErrorCode.CARTON_CANNOT_LOAD_CLOSED_PALLET,
                new UserMessage("lineload.manualLoad.error.palletClose"));
        }

        // Load carton on specified pallet
        carton.addCartonDetail(
            CartonDetailAction.Loaded, pallet, operator, new Date());

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#listSummaryByWave(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> listSummaryByWave(ResultDataInfo rdi)
        throws DataAccessException {

        Object[] queryArgs = rdi.getQueryArgs();
        // Retrieve it all.
        List<Object[]> data =
            getLineLoadSummaryDAO().listSummaryByWave(new QueryDecorator(rdi), (Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            LineLoadSummary summaryObject = new LineLoadSummary();


            summaryObject.setRouteStop((RouteStop) objArray[ROUTE_STOP]);
            summaryObject.setDateOpened((Date) objArray[DATE_OPENED]);
            summaryObject.setWave((String) objArray[WAVE]);
            summaryObject.setTotalCartons(convertNumberToInt(objArray[TOTAL_CARTONS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setSetAsideOrShorted(convertNumberToInt(objArray[SET_ASIDE]));
            summaryObject.setLoaded(convertNumberToInt(objArray[LOADED]));
            summaryObject.setPicked(convertNumberToInt(objArray[PICKED]));
            summaryObject.setNotPicked(convertNumberToInt(objArray[NOT_PICKED]));
            summaryObject.setPickShorted(convertNumberToInt(objArray[PICK_SHORTED]));
            summaryObject.setMissing(convertNumberToInt(objArray[MISSING]));
            summaryObject.setId(summaryObject.getRouteStop().getId());
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            Site theSite = siteContext.getSite(summaryObject.getRouteStop());
            summaryObject.setSite(theSite);

            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonManagerRoot#listSummaryByWaveAndRegion(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> listSummaryByWaveAndRegion(ResultDataInfo rdi)
        throws DataAccessException {

        Object[] queryArgs = rdi.getQueryArgs();
        // Retrieve it all.
        List<Object[]> data = getLineLoadSummaryDAO()
            .listSummaryByWaveAndPickingRegion((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            LineLoadSummary summaryObject = new LineLoadSummary();

            summaryObject.setRouteStop((RouteStop) objArray[ROUTE_STOP]);
            summaryObject.setDateOpened((Date) objArray[DATE_OPENED]);
            summaryObject.setWave((String) objArray[WAVE]);
            summaryObject.setTotalCartons(convertNumberToInt(objArray[TOTAL_CARTONS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setSetAsideOrShorted(convertNumberToInt(objArray[SET_ASIDE]));
            summaryObject.setLoaded(convertNumberToInt(objArray[LOADED]));
            summaryObject.setPicked(convertNumberToInt(objArray[PICKED]));
            summaryObject.setNotPicked(convertNumberToInt(objArray[NOT_PICKED]));
            summaryObject.setPickShorted(convertNumberToInt(objArray[PICK_SHORTED]));
            summaryObject.setMissing(convertNumberToInt(objArray[MISSING]));
            summaryObject
                .setSelectionRegion((SelectionRegion) objArray[SEL_REGION]);
            summaryObject.setId(summaryObject.getRouteStop().getId());
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            Site theSite = siteContext.getSite(summaryObject.getRouteStop());
            summaryObject.setSite(theSite);

            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.lang.Object)
     */
    @Override
    public Object save(Carton instance) throws BusinessRuleException, DataAccessException {

        //Set route stop's spur
        setRouteStopSpur(instance);

        return super.save(instance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.util.List)
     */
    @Override
    public Object save(List<Carton> instances) throws BusinessRuleException, DataAccessException {

        for (Carton instance : instances) {
            //Set route stop's spur
            setRouteStopSpur(instance);
        }

        return super.save(instances);
    }

    /**
     * When a carton is new and being added to a route stop, this method
     * will set the route stop's spur accodingly.
     *
     * @param instance - carton to set route stop spur for
     */
    private void setRouteStopSpur(Carton instance) {
        //If savng a new carton
        if (instance.isNew()) {
            //Check if route stop spur is not null and not equal to carton
            //then set to multiple
            if ((instance.getRouteStop().getSpur() != null)
                && !instance.getRouteStop().getSpur().equals(instance.getSpur())) {
                instance.getRouteStop().setSpur("Multiple");

            //Else if route stop's spur is null set to carton's route stop
            } else {
                if (instance.getRouteStop().getSpur() == null) {
                    instance.getRouteStop().setSpur(instance.getSpur());
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 