/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.lineloading.dao.PalletDAO;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.service.PalletManager;


/**
 * Additional service methods for the <code>Pallet</code> model object.
 *
 */
public abstract class PalletManagerImplRoot extends
    GenericManagerImpl<Pallet, PalletDAO>
    implements PalletManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PalletManagerImplRoot(PalletDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.PalletManagerRoot#findPalletByNumber(java.lang.String)
     */
    public Pallet findPalletByNumber(String number) throws DataAccessException {
        return getPrimaryDAO().findPalletByNumber(number);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 