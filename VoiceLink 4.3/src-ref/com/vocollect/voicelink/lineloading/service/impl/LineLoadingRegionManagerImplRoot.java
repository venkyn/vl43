/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.lineloading.LineLoadingErrorCode;
import com.vocollect.voicelink.lineloading.dao.LineLoadingRegionDAO;
import com.vocollect.voicelink.lineloading.model.LineLoadingRegion;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;


/**
 * Additional service methods for the <code>LineLoadingRegion</code> model object.
 *
 */
public abstract class LineLoadingRegionManagerImplRoot extends
    GenericManagerImpl<LineLoadingRegion, LineLoadingRegionDAO>
    implements LineLoadingRegionManager {

    private WorkgroupManager workgroupManager;
    private RegionManager regionManager;

    private OperatorDAO operatorDAO;

    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }


    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }


    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LineLoadingRegionManagerImplRoot(LineLoadingRegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * {@inheritDoc}
     */
    public LineLoadingRegion findRegionByNumber(int regionNumber)
                            throws DataAccessException, BusinessRuleException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        LineLoadingRegion lineLoadingRegion = get(id);
        return delete(lineLoadingRegion);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(LineLoadingRegion instance)
        throws BusinessRuleException, DataAccessException {
        try {
            executeValidateDeleteRegion(instance);
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("lineLoadingRegion.delete.error.inUse");
            } else {
                throw ex;
        }
    }
        return null;
    }

    /**
     * Take the passed in LineLoading Region and add it to all the
     * workgroups where AutoAddRegions is turned on and the task
     * function is a replenishment task function.
     * @param region - a lineloading region
     * @throws DataAccessException - any database exception
     */
    protected void addToWorkgroups(LineLoadingRegion region)
        throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.LineLoading) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(LineLoadingRegion region)
        throws BusinessRuleException, DataAccessException {
        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the replenishment task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        }

        return super.save(region);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LineLoadingRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.selection.model.LineLoadingRegion)
     */
    public void executeValidateEditRegion(LineLoadingRegion region) throws DataAccessException, BusinessRuleException {

        // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            if (!verifyNoOperatorsSignedIn(region)) {
                throw new BusinessRuleException(LineLoadingErrorCode.REGION_OPERATORS_SIGNED_IN,
                    new UserMessage("lineLoadingRegion.edit.error.operatorsSignedIn"));
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LineLoadingManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.selection.model.LineLoadingRegion)
     */
    public void executeValidateDeleteRegion(LineLoadingRegion region)
                                   throws DataAccessException, BusinessRuleException {

        // verify that no operators are signed into the region before deleteing
        if (!verifyNoOperatorsSignedIn(region)) {
            throw new BusinessRuleException(LineLoadingErrorCode.REGION_OPERATORS_SIGNED_IN,
                new UserMessage("lineLoadingRegion.delete.error.operatorsSignedIn"));
           }
    }

    /**
     * There can be no operators signed into the region when editing or deleting.
     *
     * @param region - to be persisted or deleted
     * @return true if no operators are signed into region otherwise return false
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    public boolean verifyNoOperatorsSignedIn(LineLoadingRegion region)
        throws BusinessRuleException, DataAccessException {

        if (this.getOperatorDAO().
                countNumberOfOperatorsSignedIn(region).intValue() > 0) {
           return false;
        }
        return true;
    }



    /**
     * Getter for the operatorDAO property.
     * @return OperatorDAO value of the property
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }



    /**
     * Setter for the operatorDAO property.
     * @param operatorDAO the new operatorDAO value
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 