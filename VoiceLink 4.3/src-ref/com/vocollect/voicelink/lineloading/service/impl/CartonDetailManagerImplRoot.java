/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.lineloading.dao.CartonDetailDAO;
import com.vocollect.voicelink.lineloading.model.CartonDetail;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>CartonDetail</code> model object.
 *
 */

/**
 *
 *
 * @author pfunyak
 */
public abstract class CartonDetailManagerImplRoot extends
    GenericManagerImpl<CartonDetail, CartonDetailDAO>
    implements CartonDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CartonDetailManagerImplRoot(CartonDetailDAO primaryDAO) {
        super(primaryDAO);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
            return this.getPrimaryDAO().
                listCartonDetails(new QueryDecorator(rdi));
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.CartonDetailManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator, java.util.Date, java.util.Date)
     */
    public List<CartonDetail> listMostRecentRecordsByOperator(Operator operator,
                                                              Date startDate,
                                                              Date endDate)
                                                       throws DataAccessException {

        return this.getPrimaryDAO().listMostRecentRecordsByOperator(operator, startDate, endDate);
    }

   /**
    *
    * {@inheritDoc}
    * @see com.vocollect.voicelink.lineloading.service.CartonDetailManagerRoot#listRecordsByOperatorGreaterThanDate(com.vocollect.voicelink.core.model.Operator, java.util.Date)
    */

    public List<CartonDetail>listRecordsByOperatorGreaterThanDate(Operator operator,
                                                                  Date startDate)
                                                                  throws DataAccessException {
        return this.getPrimaryDAO().listRecordsByOperatorGreaterThanDate(operator, startDate);
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 