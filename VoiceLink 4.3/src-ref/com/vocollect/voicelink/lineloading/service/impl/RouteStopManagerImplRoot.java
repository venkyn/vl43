/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.lineloading.LineLoadingErrorCode;
import com.vocollect.voicelink.lineloading.dao.RouteStopDAO;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonDetailAction;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.PalletStatus;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.lineloading.service.RouteStopManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>RouteStop</code> model object.
 *
 */
public abstract class RouteStopManagerImplRoot extends
    GenericManagerImpl<RouteStop, RouteStopDAO>
    implements RouteStopManager {

    private CartonManager   cartonManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(VoiceLinkPurgeArchiveManagerImpl.class);




    /**
     * Getter for the cartonManager property.
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return cartonManager;
    }


    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public RouteStopManagerImplRoot(RouteStopDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#allCartonsLoaded(com.vocollect.voicelink.lineloading.model.RouteStop)
     */
    public boolean allCartonsLoaded(RouteStop routeStop)
    throws DataAccessException {
        int count = getCartonManager()
            .countCartonsRemainingForRouteStop(routeStop);

        return (count == 0);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#checkAllCartonsLoaded(com.vocollect.voicelink.lineloading.model.RouteStop)
     */
    public void checkAllCartonsLoaded(RouteStop routeStop)
    throws DataAccessException, BusinessRuleException {
        if (!allCartonsLoaded(routeStop)) {
            throw new BusinessRuleException(
                LineLoadingErrorCode.ROUTE_STOP_CARTONS_NOT_LOADED,
                new UserMessage("lineload.closeRouteStop.error.cartonsNotLoaded"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#executeCloseRouteStop(com.vocollect.voicelink.lineloading.model.RouteStop, java.util.Date)
     */
    public void executeCloseRouteStop(RouteStop routeStop, Date dateClosed) {

        //Close all pallets for route stop
        for (Pallet p : routeStop.getPallets()) {
            p.setStatus(PalletStatus.Closed);
        }

        //Short any unloaded cartons for route stop
        for (Carton c : routeStop.getCartons()) {
            if (!c.getStatus().equals(CartonStatus.Loaded)) {
                c.addCartonDetail(CartonDetailAction.Shorted,
                                    null,
                                    null,
                                    dateClosed);
            }
        }

        //close route stop
        routeStop.setStatus(RouteStopStatus.Closed);
        routeStop.setDateClosed(dateClosed);
    }

    /**
     * {@inheritDoc}
     * @throws BusinessRuleException
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#executePrintManifestReport(com.vocollect.voicelink.lineloading.model.RouteStop)
     */
    public void executePrintManifestReport(RouteStop routeStop)
    throws BusinessRuleException {

        //Check that route/stop has been closed
        if (!routeStop.getStatus().equals(RouteStopStatus.Closed)) {
            throw new BusinessRuleException(
                LineLoadingErrorCode.ROUTE_STOP_NOT_CLOSED,
                new UserMessage("lineload.printManifest.error.routeStopNotClose"));
        }

        //TODO: Print manifest report here.
    }

    /**
     * Implementation to get routestop by number.
     *
     * {@inheritDoc}
     *
     */
    public RouteStop findRouteStopByRouteStopWave(String route, String stop, String wave) throws DataAccessException {
        return getPrimaryDAO().findRouteStopByRouteStopWave(route, stop, wave);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date, com.vocollect.voicelink.lineloading.model.RouteStopStatus)
     */
    public List<RouteStop> listOlderThan(QueryDecorator decorator,
        Date date, RouteStopStatus status) throws DataAccessException {
        return getPrimaryDAO().listOlderThan(decorator, date, status);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.lineloading.service.RouteStopManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.lineloading.model.RouteStopStatus, java.util.Date)
     */
    public int executePurge(QueryDecorator decorator,
                            RouteStopStatus status, Date olderThan) {
        int returnRecords = 0;
        List<RouteStop> routeStops = null;

        try {
            routeStops = listOlderThan(decorator, olderThan, status);
            returnRecords = routeStops.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting transactional "
                + "RouteStops from database to purge: " + e
                );
            return 0;
        }

        for (RouteStop rs : routeStops) {
            try {
                if (log.isDebugEnabled()) {
                    log.debug("### Found Route Stop - " + status.toString() + " to Purge :::");
                }
                delete(rs);
            } catch (Throwable t) {
                log.warn("!!! Error purging transactional Route Stop "
                    + rs.getRoute() + "-" + rs.getStop()
                    + " from database: " + t
                    , t);
            }
        }

        getPrimaryDAO().clearSession();
        return returnRecords;
    }


}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 