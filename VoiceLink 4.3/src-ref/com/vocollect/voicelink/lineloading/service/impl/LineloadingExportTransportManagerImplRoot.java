/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.lineloading.dao.CartonDAO;
import com.vocollect.voicelink.lineloading.service.LineloadingExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vsubramani
 *
 */
public abstract class LineloadingExportTransportManagerImplRoot extends CartonManagerImpl
        implements LineloadingExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LineloadingExportTransportManagerImplRoot(CartonDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     */
    public List<Map<String, Object>> listExportLineloading() throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Map<String, Object>> rawData =
            this.getPrimaryDAO().listExportLineloadingRecords(qd);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        for (Map <String, Object> m : rawData) {
            for (String keyObj : m.keySet()) {
                myObj = m.get(keyObj);
                if (null != myObj && myObj instanceof java.util.Date) {
                    formattedDateTime = sdf.format(myObj);
                    m.put(keyObj, formattedDateTime);
                }
            }
        }
        return rawData;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 