/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.lineloading.dao.CartonImportDataDAO;
import com.vocollect.voicelink.lineloading.model.CartonImportData;

import java.util.List;

/**
 * Service layer interface for carton (lineloading) import.
 * @author dgold
 *
 */
public interface CartonImportDataManagerRoot
    extends GenericManager<CartonImportData, CartonImportDataDAO>, DataProvider {

    /**
     * Get the list of importable Cartons by site name.
     * @param siteName name of the site to use
     * @return list of all importable Cartons for the site.
     * @throws DataAccessException for database errors.
     */
    public List<CartonImportData> listCartonBySiteName(String siteName) throws DataAccessException;

    /**
     * Update all CartonImportData objects to status=complete.
     * @throws DataAccessException on database error.
     */
    public void updateToComplete() throws DataAccessException;


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 