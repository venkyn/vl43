/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.lineloading.dao.LineLoadingRegionDAO;
import com.vocollect.voicelink.lineloading.model.LineLoadingRegion;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Lineloading-related operations.
 *
 */
public interface LineLoadingRegionManagerRoot
extends GenericManager<LineLoadingRegion, LineLoadingRegionDAO>, DataProvider {

    /**
     * Find the Line Loading region, given the region number.
     * @param regionNumber reference to the region
     * @return the selection region referenced, or null if not found
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    public LineLoadingRegion findRegionByNumber(int regionNumber)
    throws DataAccessException, BusinessRuleException;

    /**
     * Validate the business rules for editing a lineloading region region.
     * @param region - region to edit
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    void executeValidateEditRegion(LineLoadingRegion region)
        throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 