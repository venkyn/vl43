/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for line loading business. Although the
 * range for these error codes is 7000-7999, please use only 7000-7499 for
 * the predefined ones; 7500-7999 should be reserved for customization error
 * codes.
 *
 * @author mkoenig
 */
public class LineLoadingErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 7000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 7999;

    /**
     * No error, just the base initialization.
     */
    public static final LineLoadingErrorCodeRoot NO_ERROR
        = new LineLoadingErrorCodeRoot();



    /**
     * Route stop must be closed for printing manifest.
     */
    public static final LineLoadingErrorCode ROUTE_STOP_NOT_CLOSED
    = new LineLoadingErrorCode(7000);


    /**
     * Not all pallets loaded.
     */
    public static final LineLoadingErrorCode ROUTE_STOP_CARTONS_NOT_LOADED
    = new LineLoadingErrorCode(7001);

    /**
     * Cartons with status of short cannot be loaded.
     */
    public static final LineLoadingErrorCode CARTON_CANNOT_LOAD_SHORT
    = new LineLoadingErrorCode(7002);

    /**
     * Cartons with status of short cannot be loaded.
     */
    public static final LineLoadingErrorCode CARTON_CANNOT_LOAD_CLOSED_PALLET
    = new LineLoadingErrorCode(7003);

    /**
     * Regions with operators loaded cannot be edited or deleted.
     */
    public static final LineLoadingErrorCode REGION_OPERATORS_SIGNED_IN
    = new LineLoadingErrorCode(7004);


    /**
     * Constructor.
     */
    private LineLoadingErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected LineLoadingErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(LineLoadingErrorCodeRoot.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 