/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 *
 */
public class ConfirmSpurCmdRoot extends BaseLineLoadingTaskCommand {

    //
    private static final long serialVersionUID = -6315515355083082190L;

    private String  spur;



    /**
     * Getter for the spur property.
     * @return String value of the property
     */
    public String getSpur() {
        return spur;
    }



    /**
     * Setter for the spur property.
     * @param spur the new spur value
     */
    public void setSpur(String spur) {
        this.spur = spur;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        validateSpur();

        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record.
     *
     * @return - response record.
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();

        record.put("spur", getSpur());
        return record;
    }

    /**
     * Validates that there are cartons available on the specified spur.
     *
     * @throws Exception - any exception
     */
    protected void validateSpur() throws Exception {
        Integer cartonCount = 0;

        //Sum cartons for all regions/spur operator is signed into
        for (Region r : getOperator().getRegions()) {
            cartonCount += getCartonManager().countCartonsForSpur(r, getSpur());
        }

        //If no cartons found then throw error.
        if (cartonCount == 0) {
            throw new TaskCommandException(
                TaskErrorCode.NO_CARTONS_AVAILABLE, getSpur());
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 