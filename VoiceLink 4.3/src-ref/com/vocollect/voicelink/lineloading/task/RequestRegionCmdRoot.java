/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.lineloading.model.LineLoadingRegion;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 *
 *
 * @author mkoenig
 */
public class RequestRegionCmdRoot extends BaseLineLoadingTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private LineLoadingRegionManager      lineLoadingRegionManager;

    private String                        regionNumber;

    private RegionManager                 regionManager;



    /**
     * Getter for the lineLoadingRegionManager property.
     * @return LineLoadingRegionManager value of the property
     */
    public LineLoadingRegionManager getLineLoadingRegionManager() {
        return lineLoadingRegionManager;
    }



    /**
     * Setter for the lineLoadingRegionManager property.
     * @param lineLoadingRegionManager the new lineLoadingRegionManager value
     */
    public void setLineLoadingRegionManager(LineLoadingRegionManager lineLoadingRegionManager) {
        this.lineLoadingRegionManager = lineLoadingRegionManager;
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public Integer getRegionNumberInt() {
        if (!StringUtil.isNullOrEmpty(getRegionNumber())) {
            return Integer.valueOf(getRegionNumber());
        } else {
            return null;
        }
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public String getRegionNumber() {
        return regionNumber;
    }


    /**
     * Setter for the regionNumber property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(String regionNumber) {
        this.regionNumber = regionNumber;
    }



    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }



    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }




    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        validateAndSaveRequest();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * Validate and signs operator into regions.
     *
     * @throws DataAccessException - Database excpetions
     * @throws TaskCommandException - Task Command Exceptions
     * @throws BusinessRuleException - Database Exception
     */
    protected void validateAndSaveRequest()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        LineLoadingRegion region = verifyPassedInRegion();

        List<Region> authRegions = getRegionManager().listAuthorized(
            TaskFunctionType.LineLoading, getOperator().getWorkgroup().getId());

        if (authRegions.contains(region)) {
            getOperator().getRegions().add(region);
        } else {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION,
                TaskFunctionType.LineLoading, getRegionNumber());
        }
    }

    /**
     * Verifies that the passed in region is valid.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     * @throws BusinessRuleException - Business Rule Exception
     * @return - valid line loading region
     */
    protected LineLoadingRegion verifyPassedInRegion()
        throws DataAccessException, TaskCommandException, BusinessRuleException {
        LineLoadingRegion region = getLineLoadingRegionManager()
            .findRegionByNumber(getRegionNumberInt());

        if (region == null) {
            // Raise region not found error
            throw new TaskCommandException(TaskErrorCode.INVALID_REGION_NUMBER, getRegionNumber());
        }

        return region;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 