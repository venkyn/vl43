/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonDetailAction;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.PalletStatus;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 *
 */
public class PalletCmdRoot extends BaseLineLoadingTaskCommand {

    //
    private static final long serialVersionUID = -295530645777004218L;

    private static final String MULTIPLE_SPURS = "Multiple";

    private String          palletNumber;

    private String          cartonNumber;

    private boolean         newPallet;

    private Carton          carton;

    private Pallet          pallet;

    private TaskErrorCode   taskError;

    private LaborManager    laborManager;




    /**
     * Getter for the taskError property.
     * @return TaskErrorCode value of the property
     */
    public TaskErrorCode getTaskError() {
        return taskError;
    }


    /**
     * Setter for the taskError property.
     * @param taskError the new taskError value
     */
    public void setTaskError(TaskErrorCode taskError) {
        this.taskError = taskError;
    }



    /**
     * Getter for the carton property.
     * @return Carton value of the property
     */
    public Carton getCarton() {
        return carton;
    }



    /**
     * Setter for the carton property.
     * @param carton the new carton value
     */
    public void setCarton(Carton carton) {
        this.carton = carton;
    }



    /**
     * Getter for the pallet property.
     * @return Pallet value of the property
     */
    public Pallet getPallet() {
        return pallet;
    }


    /**
     * Setter for the pallet property.
     * @param pallet the new pallet value
     */
    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }



    /**
     * Getter for the cartonNumber property.
     * @return String value of the property
     */
    public String getCartonNumber() {
        return cartonNumber;
    }




    /**
     * Setter for the cartonNumber property.
     * @param cartonNumber the new cartonNumber value
     */
    public void setCartonNumber(String cartonNumber) {
        this.cartonNumber = cartonNumber;
    }




    /**
     * Getter for the newPallet property.
     * @return boolean value of the property
     */
    public boolean isNewPallet() {
        return newPallet;
    }




    /**
     * Setter for the newPallet property.
     * @param newPallet the new newPallet value
     */
    public void setNewPallet(boolean newPallet) {
        this.newPallet = newPallet;
    }




    /**
     * Getter for the palletNumber property.
     * @return String value of the property
     */
    public String getPalletNumber() {
        return palletNumber;
    }




    /**
     * Setter for the palletNumber property.
     * @param palletNumber the new pallet value
     */
    public void setPalletNumber(String palletNumber) {
        this.palletNumber = palletNumber;
    }




    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }



    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        //validate and update carton
        validateCarton();

        validatePallet();

        updatePallet();

        updateCarton();

        getCartonManager().save(getCarton());
        getPalletManager().save(getPallet());

        checkAllLoaded();

        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record.
     *
     * @return - response record.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        if (getTaskError() != null && getTaskError().equals(TaskErrorCode.ROUTE_STOP_ALL_LOADED)) {

            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.cartonRoute", getCarton().getRouteStop().getRoute());
            lop.add("task.cartonStop", getCarton().getRouteStop().getStop());

            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                getTaskError(), lop));
        }
        return record;
    }


    /**
     * Marks carton as loaded on pallet.
     *
     * @throws DataAccessException - database exceptions
     * @throws BusinessRuleException - business rule violation
     *
     */
    protected void updateCarton()throws DataAccessException, BusinessRuleException {

        getCarton().addCartonDetail(CartonDetailAction.Loaded, getPallet(),
                                    getOperator(), getCommandTime());
        this.updateLaborStatistics();
    }

    /**
     * Update labor statistics.
     * @throws DataAccessException on database exception
     * @throws BusinessRuleException on business rule violation
     */
    protected void updateLaborStatistics() throws DataAccessException, BusinessRuleException  {
        this.getLaborManager().updateLineLoadLaborStatistics(this.getOperator(), this.getCarton());
   }


    /**
     * Update the Pallets spur.
     */
    protected void updatePallet() {
        if (!getCarton().getSpur().equals(getPallet().getSpur())) {
            if (getPallet().getSpur() == null) {
                getPallet().setSpur(getCarton().getSpur());
            } else {
                getPallet().setSpur(MULTIPLE_SPURS);
            }

        }
    }

    /**
     * Find and validate pallet specified in command.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void validatePallet()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        setPallet(getPalletManager().findPalletByNumber(getPalletNumber()));

        validatePalletExists();

        validatePalletOpen();

}



    /**
     * validate pallet's stop matches carton's stop.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validatePalletStop() throws TaskCommandException {
        int stopStart = getCarton().getRouteStop().getRegion()
            .getPalletLabelLayout().getStopStart() - 1;
        int stopEnd = stopStart + getCarton().getRouteStop()
            .getRegion().getPalletLabelLayout().getStopLength();
        String palletNumberStop = "";
        if (getPalletNumber().length() >= stopEnd) {
            palletNumberStop = getPalletNumber().substring(stopStart, stopEnd);
        }

        if (!getCarton().getRouteStop().getStop().trim()
            .equals(palletNumberStop.trim())) {
            throw new TaskCommandException(
                TaskErrorCode.PALLET_STOP_NOT_MATCH,
                getPalletNumber(), getCartonNumber());
        }
    }



    /**
     * validate pallet's route matches carton's route.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validatePalletRoute() throws TaskCommandException {
        int routeStart = getCarton().getRouteStop().getRegion()
            .getPalletLabelLayout().getRouteStart() - 1;
        int routeEnd = routeStart + getCarton().getRouteStop()
            .getRegion().getPalletLabelLayout().getRouteLength();
        String palletNumberRoute = "";
        if (getPalletNumber().length() >= routeEnd) {
            palletNumberRoute = getPalletNumber().substring(routeStart, routeEnd);
        }

        if (!getCarton().getRouteStop().getRoute().trim()
            .equals(palletNumberRoute.trim())) {
            throw new TaskCommandException(
                TaskErrorCode.PALLET_ROUTE_NOT_MATCH,
                getPalletNumber(), getCartonNumber());
        }
    }



    /**
     * validate pallet is open.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validatePalletOpen() throws TaskCommandException {
        if (!getPallet().getStatus().equals(PalletStatus.Opened)) {
            throw new TaskCommandException(
                TaskErrorCode.PALLET_NOT_OPEN,
                getPalletNumber());
        }
    }



    /**
     * validate pallet exists or create new pallet.
     *
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Database exception.
     * @throws TaskCommandException - task Command Exception
     */
    protected void validatePalletExists()
    throws BusinessRuleException, DataAccessException, TaskCommandException {
        if (getPallet() == null) {
            if (isNewPallet()) {
                setPallet(new Pallet());
                getPallet().setNumber(getPalletNumber());
                getPallet().setRouteStop(getCarton().getRouteStop());
                getPallet().setStatus(PalletStatus.Opened);
                getPalletManager().save(getPallet());
            } else {
                throw new TaskCommandException(
                    TaskErrorCode.PALLET_NOT_FOUND,
                    getPalletNumber());
            }
        }
    }

    /**
     * Find and validate carton specified in command.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     */
    protected void validateCarton()
    throws DataAccessException, TaskCommandException {
        setCarton(getCartonManager().findCartonByNumber(getCartonNumber()));

        validateCartonFound();

        validateCartonRouteStopOpen();

        validatePalletStop();

        validatePalletRoute();
    }


    /**
     * Validate carton was found.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validateCartonFound()
    throws TaskCommandException {
        //Validate carton found
        if (getCarton() == null) {
            throw new TaskCommandException(TaskErrorCode.CARTON_NOT_FOUND,
                getCartonNumber());
        }
    }

    /**
     * Validate carton's route stop is not closed.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validateCartonRouteStopOpen()
    throws TaskCommandException {
        //Validate carton's route stop is not closed
        if (getCarton().getRouteStop().getStatus().equals(RouteStopStatus.Closed)) {
            throw new TaskCommandException(
                TaskErrorCode.CARTON_ROUTE_STOP_CLOSED,
                getCartonNumber());
        }
    }

    /**
     * Check if there are any more carton to load for route stop.
     *
     * @throws DataAccessException - Database Exceptions
     */
    protected void checkAllLoaded() throws DataAccessException {
        Integer remainingCartons = getCartonManager()
            .countCartonsRemainingForRouteStop(getCarton().getRouteStop());


        if (remainingCartons == 0) {
            setTaskError(TaskErrorCode.ROUTE_STOP_ALL_LOADED);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 