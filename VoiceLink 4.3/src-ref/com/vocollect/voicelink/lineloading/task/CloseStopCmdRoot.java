/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.service.ReportPrinterManager;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 *
 */
public class CloseStopCmdRoot extends BaseLineLoadingTaskCommand {

    //
    private static final long serialVersionUID = -8435204890372729366L;

    private String palletNumber;

    private String printerNumber;

    private Pallet pallet;

    private int palletCount = 0;

    private ReportPrinterManager reportPrinterManager;

    /**
     * Getter for the palletCount property.
     * @return int value of the property
     */
    public int getPalletCount() {
        return palletCount;
    }

    /**
     * Setter for the palletCount property.
     * @param palletCount the new palletCount value
     */
    public void setPalletCount(int palletCount) {
        this.palletCount = palletCount;
    }

    /**
     * Getter for the pallet property.
     * @return Pallet value of the property
     */
    public Pallet getPallet() {
        return pallet;
    }

    /**
     * Setter for the pallet property.
     * @param pallet the new pallet value
     */
    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    /**
     * Getter for the palletNumber property.
     * @return String value of the property
     */
    public String getPalletNumber() {
        return palletNumber;
    }

    /**
     * Setter for the palletNumber property.
     * @param palletNumber the new palletNumber value
     */
    public void setPalletNumber(String palletNumber) {
        this.palletNumber = palletNumber;
    }

    /**
     * Getter for the printerNumber property.
     * @return String value of the property
     */
    public String getPrinterNumber() {
        return printerNumber;
    }

    /**
     * Setter for the printerNumber property.
     * @param printerNumber the new printerNumber value
     */
    public void setPrinterNumber(String printerNumber) {
        this.printerNumber = printerNumber;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        validatePallet();

        validateRouteStop();

        closeRouteStop();

        printManifestReport();

        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        // Create a return record, no fields to return

        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record.
     *
     * @return - response record.
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        record.put("route", getPallet().getRouteStop().getRoute());
        record.put("stop", getPallet().getRouteStop().getStop());
        record.put("palletCount", getPallet().getRouteStop().getPallets()
            .size());

        return record;
    }

    /**
     * Find and validate pallet specified in command.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void validatePallet() throws DataAccessException,
        TaskCommandException, BusinessRuleException {
        setPallet(getPalletManager().findPalletByNumber(getPalletNumber()));

        validatePalletExists();

    }

    /**
     * validate pallet exists or create new pallet.
     *
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Database exception.
     * @throws TaskCommandException - task Command Exception
     */
    protected void validatePalletExists() throws BusinessRuleException,
        DataAccessException, TaskCommandException {
        if (getPallet() == null) {
            throw new TaskCommandException(
                TaskErrorCode.PALLET_NOT_FOUND, getPalletNumber());
        }
    }

    /**
     * Validate all cartons are loaded for route stop.
     *
     * @throws DataAccessException - database exceptions
     * @throws TaskCommandException - Task Command Exceptions
     */
    protected void validateRouteStop() throws DataAccessException,
        TaskCommandException {
        if (!getRouteStopManager().allCartonsLoaded(getPallet().getRouteStop())) {
            throw new TaskCommandException(
                TaskErrorCode.ROUTE_STOP_NOT_LOADED, getPallet().getRouteStop()
                    .getRoute(), getPallet().getRouteStop().getStop());
        }
    }

    /**
     * Close all pallets for route stop and close route stop.
     */
    protected void closeRouteStop() {
        getRouteStopManager().executeCloseRouteStop(
            getPallet().getRouteStop(), getCommandTime());
    }

    /**
     * Getter for the printerNumber as integer property.
     * @return Long value of the property
     */
    public Integer getPrinterNumberInt() {
        if (StringUtil.isNullOrEmpty(this.printerNumber)) {
            return null;
        } else {
            return Integer.parseInt(this.printerNumber);
        }
    }

    /**
     * Print manifest report directly from the task. Pass the printer number in,
     * which is then resolved in the
     * applicationContext-voiceline-reportPrinters.xml configuration file.
     * @throws VocollectException - vocollect exceptions
     */

    private void printManifestReport() throws VocollectException {

        if (getPrinterNumberInt() != null) {
            try {
                Map<String, String> parameterMap = new HashMap<String, String>();
                parameterMap.put("SESSION_VALUE", getPallet().getRouteStop().getId().toString());
                this.reportPrinterManager.printReport("PalletManifest.jrxml", printerNumber, parameterMap);
            } catch (VocollectException ve) {
                if (ve.getErrorCode() == CoreErrorCode.PRINTER_NOT_FOUND) {
                    throw new TaskCommandException(TaskErrorCode.PRINTER_NOT_FOUND, getPrinterNumber());
                } else if (ve.getErrorCode() == ReportError.JASPER_PRINTSERVICE_EXCEPTION) {
                    throw new TaskCommandException(TaskErrorCode.JASPER_REPORT_PRINTESERVICE_EXCEPTION,
                                                   getPrinterNumber());
                } else if (ve.getErrorCode() == ReportError.JASPER_LOAD_ERROR
                       || ve.getErrorCode() == CoreErrorCode.REPORT_NOT_FOUND) {
                    throw new TaskCommandException(TaskErrorCode.JASPER_PALLET_MANIFEST_REPORT_NOT_FOUND);
                } else if (ve.getErrorCode() == ReportError.JASPER_COMPILE_ERROR) {
                    throw new TaskCommandException(TaskErrorCode.JASPER_REPORT_COULD_NOT_BE_COMPILED);
                } else if (ve.getErrorCode() == ReportError.JASPER_FILL_ERROR) {
                    throw new TaskCommandException(TaskErrorCode.JASPER_REPORT_COULD_NOT_BE_FILLED);
                } else {
                    throw ve;
                }
            }
        }
    }

    /**
     * Getter for the reportPrinterManager property.
     * @return ReportPrinterManager value of the property
     */
    public ReportPrinterManager getReportPrinterManager() {
        return this.reportPrinterManager;
    }

    /**
     * Setter for the reportPrinterManager property.
     * @param reportPrinterManager the new reportPrinterManager value
     */
    public void setReportPrinterManager(ReportPrinterManager reportPrinterManager) {
        this.reportPrinterManager = reportPrinterManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 