/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.lineloading.model.LineLoadingRegion;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author mkoenig
 */
public class RegionConfigurationCmdRoot extends BaseLineLoadingTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private LineLoadingRegionManager  lineLoadingRegionManager;

    private List<LineLoadingRegion>   returnRegions =
        new ArrayList<LineLoadingRegion>();

    private LaborManager              laborManager;




    /**
     * Getter for the lineLoadingRegionManager property.
     * @return LineLoadingRegionManager value of the property
     */
    public LineLoadingRegionManager getLineLoadingRegionManager() {
        return lineLoadingRegionManager;
    }



    /**
     * Setter for the lineLoadingRegionManager property.
     * @param lineLoadingRegionManager the new lineLoadingRegionManager value
     */
    public void setLineLoadingRegionManager(LineLoadingRegionManager lineLoadingRegionManager) {
        this.lineLoadingRegionManager = lineLoadingRegionManager;
    }



    /**
     * Getter for the returnRegions property.
     * @return List$lt;LineLoadingRegion&gt; value of the property
     */
    public List<LineLoadingRegion> getReturnRegions() {
        return returnRegions;
    }



    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<LineLoadingRegion> returnRegions) {
        this.returnRegions = returnRegions;
    }



    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }




    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildReturnRegionList();
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.REGIONS_NO_LONGER_VALID);
        }
        setOperatorLastLocation();
        openLaborRecord();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        //Loop through regions for line loading that user signed into
        for (LineLoadingRegion r : getReturnRegions()) {
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build response record.
     *
     * @param r - region to build record for
     * @return - response record.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(LineLoadingRegion r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        record.put("allowCloseRouteStop", r.getAllowCloseStopFromTask());
        record.put("allowPrintManifest", r.getAutoPrintManifestReport());

        return record;
    }

    /**
     * Build a list of regions to return.
     *
     * @throws DataAccessException - Database exception
     */
    protected void buildReturnRegionList() throws DataAccessException {
        Set<Region> regions = getOperator().getRegions();

        for (Region r : regions) {
            getReturnRegions().add((LineLoadingRegion) r);
        }

    }

    /**
     * Sets the operators last location to null.
     *
     * @throws DataAccessException - database exception
     */
    protected void setOperatorLastLocation() throws DataAccessException {
        getOperator().setLastLocation(null);
    }


    /**
     * Open a line loading labor record.
     * @throws DataAccessException on Database Error
     * @throws BusinessRuleException on BusinessRuleException
     */
    protected void openLaborRecord() throws DataAccessException, BusinessRuleException {

        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                       this.getOperator(), OperatorLaborActionType.LineLoading);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 