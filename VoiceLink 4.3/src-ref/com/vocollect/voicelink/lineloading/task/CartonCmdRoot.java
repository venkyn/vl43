/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonDetailAction;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 *
 */
public class CartonCmdRoot extends BaseLineLoadingTaskCommand {

    //
    private static final long serialVersionUID = -1566330430714420641L;

    private String      spur;

    private String      cartonNumber;

    private boolean     setAside;

    private TaskErrorCode taskError;

    private Carton      carton;




    /**
     * Getter for the carton property.
     * @return Carton value of the property
     */
    public Carton getCarton() {
        return carton;
    }





    /**
     * Setter for the carton property.
     * @param carton the new carton value
     */
    public void setCarton(Carton carton) {
        this.carton = carton;
    }




    /**
     * Getter for the taskError property.
     * @return TaskErrorCode value of the property
     */
    public TaskErrorCode getTaskError() {
        return taskError;
    }




    /**
     * Setter for the taskError property.
     * @param taskError the new taskError value
     */
    public void setTaskError(TaskErrorCode taskError) {
        this.taskError = taskError;
    }



    /**
     * Getter for the cartonNumber property.
     * @return String value of the property
     */
    public String getCartonNumber() {
        return cartonNumber;
    }



    /**
     * Setter for the cartonNumber property.
     * @param cartonNumber the new carton value
     */
    public void setCartonNumber(String cartonNumber) {
        this.cartonNumber = cartonNumber;
    }



    /**
     * Getter for the setAside property.
     * @return boolean value of the property
     */
    public boolean getSetAside() {
        return setAside;
    }



    /**
     * Setter for the setAside property.
     * @param setAside the new setAside value
     */
    public void setSetAside(boolean setAside) {
        this.setAside = setAside;
    }



    /**
     * Getter for the spur property.
     * @return String value of the property
     */
    public String getSpur() {
        return spur;
    }



    /**
     * Setter for the spur property.
     * @param spur the new spur value
     */
    public void setSpur(String spur) {
        this.spur = spur;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {

        //validate and update carton
        validateCarton();

        updateCarton();

        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record.
     *
     * @return - response record.
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("route", getCarton().getRouteStop().getRoute());
        record.put("stop", getCarton().getRouteStop().getStop());

        if (getTaskError() == TaskErrorCode.CARTON_AVAILABLE_ON_WRONG_SPUR) {

            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.cartonNumber",
                getCartonNumber());
            lop.add("task.spur", getSpur());
            lop.add("task.cartonSpur", getCarton().getSpur());

            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                getTaskError(), lop));
        }
        return record;
    }

    /**
     * Marks carton as in-progress or set aside.
     *
     * @throws DataAccessException - database exceptions
     */
    protected void updateCarton() throws DataAccessException {
        if (getTaskError() == null) {
            if (getSetAside()) {
                getCarton().addCartonDetail(CartonDetailAction.SetAside,
                    null, getOperator(), getCommandTime());
            } else {
                getCarton().addCartonDetail(CartonDetailAction.InProgress,
                    null, getOperator(), getCommandTime());
            }
        }
    }

    /**
     * Find and validate carton specified in command.
     *
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void validateCarton()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        setCarton(getCartonManager().findCartonByNumber(getCartonNumber()));

        validateCartonFound();

        validateCartonRouteStopOpen();

        validateCartonNotLoaded();

        validateCartonSpur();
    }




    /**
     * Validate carton was on the correct spur.
     *
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void validateCartonSpur()
    throws DataAccessException, BusinessRuleException {
        if (!getCarton().getSpur().equals(getSpur())
            && getCarton().getStatus().equals(CartonStatus.Available)) {
          setTaskError(TaskErrorCode.CARTON_AVAILABLE_ON_WRONG_SPUR);
          getCarton().addCartonDetail(CartonDetailAction.SetAside,
              null, getOperator(), getCommandTime());
          getCartonManager().save(getCarton());

        }
    }




    /**
     * Validate carton is not already loaded.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validateCartonNotLoaded()
    throws TaskCommandException {
        //Validate carton is not loaded
        if (getCarton().getStatus().equals(CartonStatus.Loaded)
            && !getSetAside()) {
            if (getCarton().getSpur().equals(getSpur())) {
                throw new TaskCommandException(
                    TaskErrorCode.CARTON_ALREADY_LOADED,
                    getCartonNumber());
            } else {
                throw new TaskCommandException(
                    TaskErrorCode.CARTON_LOADED_ON_WRONG_SPUR,
                    getCartonNumber(), getSpur(),
                    getCarton().getSpur());
            }
        }
    }




    /**
     * Validate carton's route stop is not closed.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validateCartonRouteStopOpen()
    throws TaskCommandException {
        //Validate carton's route stop is not closed
        if (getCarton().getRouteStop().getStatus().equals(RouteStopStatus.Closed)) {
            throw new TaskCommandException(
                TaskErrorCode.CARTON_ROUTE_STOP_CLOSED,
                getCartonNumber());
        }
    }




    /**
     * Validate carton was found.
     *
     * @throws TaskCommandException - task Command Exception
     */
    protected void validateCartonFound()
    throws TaskCommandException {
        //Validate carton found
        if (getCarton() == null) {
            throw new TaskCommandException(TaskErrorCode.CARTON_NOT_FOUND,
                getCartonNumber());
        }
    }

    /**
     * Validate carton Region.
     *
     * @throws TaskCommandException - task Command Exception
     * @throws DataAccessException - Database Exceptions
     */
    protected void validateCartonRegion()
    throws TaskCommandException, DataAccessException {
        //Validate carton found
        if (getOperator().getRegions().contains(
            getCarton().getRouteStop().getRegion())) {
            throw new TaskCommandException(TaskErrorCode.CARTON_NOT_IN_REGION,
                getCartonNumber());
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 