/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.task.command.BaseLineLoadingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 *
 * @author mkoenig
 */
public class ValidRegionsCmdRoot extends BaseLineLoadingTaskCommand {

    //
    private static final long serialVersionUID = -6701602702027934374L;

    private RegionManager                 regionManager;

    private TaskFunctionManager           taskFunctionManager;

    private List<Region>                  returnRegions;




    /**
     * Getter for the taskFunctionManager property.
     * @return TaskFunctionManager value of the property
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }





    /**
     * Setter for the taskFunctionManager property.
     * @param taskFunctionManager the new taskFunctionManager value
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }




    /**
     * Getter for the returnRegions property.
     * @return List&lt;Region&gt; value of the property
     */
    public List<Region> getReturnRegions() {
        return returnRegions;
    }




    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<Region> returnRegions) {
        this.returnRegions = returnRegions;
    }



    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }



    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //Get a list of authorized regions
        getAuthorizedRegions();

        //If no regions found then throw error not authorized for any regions
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_ANY_REGIONS);
        }

        resetOperatorInfo();

        buildResponse();

        return getResponse();
    }

    /**
     * Set operators work type to Line Loading, clears previous regions
     * and starts line loading labor record.
     *
     * @throws DataAccessException - database exceptions
     */
    protected void resetOperatorInfo() throws DataAccessException {
        TaskFunction taskFunction = getTaskFunctionManager()
            .findByType(TaskFunctionType.LineLoading);
        getOperator().setCurrentWorkType(taskFunction);

        getOperator().getRegions().clear();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (Region r : getReturnRegions()) {
            //Create a return record, populate it, and add to response
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build Response Record.
     *
     * @param r - region to build record for
     * @return - response record
     * @throws DataAccessException - Database exceptions
     */
    protected ResponseRecord buildResponseRecord(Region r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));

        return record;
    }
    /**
     * Get List of region authorized to work in.
     *
     * @throws DataAccessException - database exception
     **/
    protected void getAuthorizedRegions() throws DataAccessException {
        //Get list of authorized regions
        setReturnRegions(getRegionManager().listAuthorized(
            TaskFunctionType.LineLoading, getOperator().getWorkgroup().getId()));
    }

  }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 