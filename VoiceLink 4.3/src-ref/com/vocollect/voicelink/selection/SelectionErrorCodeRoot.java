/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for selection business. Although the
 * range for these error codes is 3000-3999, please use only 3000-3499 for
 * the predefined ones; 3500-3999 should be reserved for customization error
 * codes.
 * 
 * @author mkoenig
 */
public class SelectionErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 3000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 3999;

    /** 
     * No error, just the base initialization.
     */
    public static final SelectionErrorCodeRoot NO_ERROR
        = new SelectionErrorCodeRoot();


    
    /** 
     * Assignment not available to be issued.
     */
    public static final SelectionErrorCode ASSIGNMENT_NOT_AVAILABLE
    = new SelectionErrorCode(3000);
    
    /** 
     * Assignment Already in Progress.
     */
    public static final SelectionErrorCode ASSIGNMENT_ALREADY_IN_PROGRESS
    = new SelectionErrorCode(3001);
    
    /** 
     * Assignment must be in group to be issued.
     */
    public static final SelectionErrorCode ASSIGNMENT_NOT_IN_GROUP
    = new SelectionErrorCode(3002);
    
    /** 
     * Assignment cannot be completed from current status.
     */
    public static final SelectionErrorCode ASSIGNMENT_INVALID_STOP_STATUS
    = new SelectionErrorCode(3003);

    /** 
     * Not all picks are picked and cannot be completed.
     */
    public static final SelectionErrorCode ASSIGNMENT_INVALID_STATUS_CHANGE
    = new SelectionErrorCode(3004);
    
    /** 
     * Not all picks are picked and cannot be completed.
     */
    public static final SelectionErrorCode ASSIGNMENT_PICKS_NOT_COMPLETED
    = new SelectionErrorCode(3005);
    
    /** 
     * Cannot adjust a chase assignment pick.
     */
    public static final SelectionErrorCode PICK_CANNOT_ADJUST_CHASE
    = new SelectionErrorCode(3006);
    
    /** 
     * Cannot adjust pick unless status is short.
     */
    public static final SelectionErrorCode PICK_CANNOT_ADJUST_UNLESS_SHORT
    = new SelectionErrorCode(3007);
    
    /** 
     * Cannot markout a chase pick.
     */
    public static final SelectionErrorCode PICK_CANNOT_MARKOUT_CHASE
    = new SelectionErrorCode(3008);
    
    /** 
     * Cannot add a chase pick to chase assignment.
     */
    public static final SelectionErrorCode PICK_CANNOT_ADD_CHASE
    = new SelectionErrorCode(3009);
    
    /** 
     * pick must be short to add to chase assignment.
     */
    public static final SelectionErrorCode PICK_MUST_BE_SHORT
    = new SelectionErrorCode(3010);
    
    /** 
     * cannot add chase pick to normal assignment.
     */
    public static final SelectionErrorCode PICK_CANNOT_ADD_CHASE_TO_NORMAL
    = new SelectionErrorCode(3011);
    
    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode PICK_CANNOT_ADD_ASSIGNMENT
    = new SelectionErrorCode(3012);
    
    /** 
     * Picks already complete cannot add more details.
     */
    public static final SelectionErrorCode PICK_ALREADY_COMPLETE
    = new SelectionErrorCode(3013);
    
    /** 
     * Invalid pick status change.
     */
    public static final SelectionErrorCode PICK_INVALID_STATUS_CHANGE
    = new SelectionErrorCode(3014);
    
    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode PICK_INVALID_REGION
    = new SelectionErrorCode(3015);
    
    /**
     * Cannot create a manual pick with quantity greater than quantityToPick
     * plus quantityPicked.
     */
    public static final SelectionErrorCode PICK_INVALID_MANUAL_PICK_QUANTITY
    = new SelectionErrorCode(3016);
    
    /** 
     * Cannot cancel pick unless assignment is Available, Unavilable, or Suspended.
     */
    public static final SelectionErrorCode PICK_CANNOT_BE_CANCELED_INPROGRESS
    = new SelectionErrorCode(3017);
    
    /** 
     * Cannot cancel pick unless assignment is Available, Unavilable, or Suspended.
     */
    public static final SelectionErrorCode PICK_CANNOT_BE_CANCELED_OPERATOR
    = new SelectionErrorCode(3018);
    
    /** 
     * Cannot cancel pick unless assignment is Available, Unavilable, or Suspended.
     */
    public static final SelectionErrorCode PICK_CANNOT_BE_CANCELED_INVALID_STATUS
    = new SelectionErrorCode(3019);
    
    //===================================================================
    //Grouping Errors
    //===================================================================
    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_2_ASSIGNMENTS_REQUIRED
    = new SelectionErrorCode(3020);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_INVALID_ASSIGNMENT_STATUS
    = new SelectionErrorCode(3021);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_NOT_AUTO_ISSUANCE
    = new SelectionErrorCode(3022);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_OPERATOR_ASSIGNED
    = new SelectionErrorCode(3023);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_NOT_SAME_REGION
    = new SelectionErrorCode(3024);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_NO_CASE_LABEL_CD
    = new SelectionErrorCode(3025);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_NOT_SAME_TYPE
    = new SelectionErrorCode(3026);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_REGION_NOT_ALLOW_MULT
    = new SelectionErrorCode(3027);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode GROUP_TOO_MANY_ASSIGNMENTS
    = new SelectionErrorCode(3028);

    //===================================================================
    //Ungrouping Errors
    //===================================================================

    /** 
     * cannot Ungroup with invalid status.
     */
    public static final SelectionErrorCode UNGROUP_INVALID_STATUS
    = new SelectionErrorCode(3040);

    /** 
     * Cannot ungroup if not in group.
     */
    public static final SelectionErrorCode UNGROUP_NOT_ALL_IN_GROUP
    = new SelectionErrorCode(3041);

    /** 
     * cannot ungroup is not all in same group.
     */
    public static final SelectionErrorCode UNGROUP_NOT_ALL_SAME_GROUP
    = new SelectionErrorCode(3042);

    /** 
     * Cannot ungroup unless manual region.
     */
    public static final SelectionErrorCode UNGROUP_TASK_REGION_NOT_MANUAL
    = new SelectionErrorCode(3043);

    /** 
     * cannot ungroup unless all assignment in group selected.
     */
    public static final SelectionErrorCode UNGROUP_TASK_NOT_ALL_ASSIGNMENTS
    = new SelectionErrorCode(3044);

    /** 
     * cannot ungroup is not all in same group.
     */
    public static final SelectionErrorCode UNGROUP_NO_OPERATOR
    = new SelectionErrorCode(3045);

    //===================================================================
    //Splitting Errors
    //===================================================================
    
    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_INVALID_ASSIGNMENT_STATUS
    = new SelectionErrorCode(3050);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_OPERATOR_ASSIGNED
    = new SelectionErrorCode(3051);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_IN_GROUP
    = new SelectionErrorCode(3052);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_INVALID_PICK_STATUS
    = new SelectionErrorCode(3053);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_MAX_TIMES
    = new SelectionErrorCode(3054);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_NOT_ALL_PICKS_ALLOWED
    = new SelectionErrorCode(3055);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_CASE_LABEL_CD
    = new SelectionErrorCode(3056);

    /** 
     * cannot add to assignment with invalid status.
     */
    public static final SelectionErrorCode SPLIT_TARGET_CONTAINERS
    = new SelectionErrorCode(3057);

    //===================================================================
    //Assignment Edit Errors
    //===================================================================

    /** 
     * cannot edit an assignment in progress.
     */
    public static final SelectionErrorCode EDIT_ASSIGNMENT_INVALID_STATUS
    = new SelectionErrorCode(3060);

    //===================================================================
    //Region Errors
    //===================================================================

    /**
     * cannot edit a region when operators are signed in.
     */
    public static final SelectionErrorCode REGION_OPERATORS_SIGNED_IN 
    = new SelectionErrorCode(3070);
    
    /**
     * cannot have assignments with status in-progress, suspended,
     * or passed.
     */
    public static final SelectionErrorCode REGION_INVALID_ASSIGNMENT_STATUS
    = new SelectionErrorCode(3071);
    
    /**
     * cannot edit a region if work ids are shorter than the request
     * digits spoken.
     */
    public static final SelectionErrorCode REGION_WORK_IDS_TOO_SHORT
    = new SelectionErrorCode(3072); 
    
    /**
     * cannot edit a region if you set the max assignments lower
     * than the existing max assignments.
     */
    public static final SelectionErrorCode REGION_INVALID_MAX_ASSIGNMENTS
    = new SelectionErrorCode(3073);
    
    /**
     * cannot edit a region to manual if there are available or unavailable
     * assignments.
     */
    public static final SelectionErrorCode REGION_INVALID_ISSUANCE
    = new SelectionErrorCode(3074);
    
    /**
     * cannot edit a region if work ids for normal and chase assignment are 
     * not equal for manual issuance.
     */
    public static final SelectionErrorCode REGION_WORK_IDS_NOT_EQUAL
    = new SelectionErrorCode(3075); 
    
    
    /**
     * cannot set end date for labor record to the requested date.
     */
    public static final SelectionErrorCode REGION_DUPLICATE_NUMBER
    = new SelectionErrorCode(3076); 
    
    /**
     * cannot set end date for labor record to the requested date.
     */
    public static final SelectionErrorCode REGION_DUPLICATE_NAME
    = new SelectionErrorCode(3077); 
    
    /**
     * Constructor.
     */
    private SelectionErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected SelectionErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(SelectionErrorCodeRoot.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 