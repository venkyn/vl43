/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.events;

import java.util.Date;

import org.springframework.context.ApplicationEvent;

/**
 * @author yarora
 *
 */
public abstract class ShortEventRoot extends ApplicationEvent {

    private static final long serialVersionUID = -3676653102659770003L;
    private Date pickTime;
    private Long originalAssignmentNo;
    private String locationID;
    private String itemNo;
    private String operatorID;
    private Integer pickedQty;
    private Long siteId;
    
    
    /**
     * 
     */
    public ShortEventRoot() {
        super("shortEvent");
    }


    /**
     * @return the pickTime
     */
    public Date getPickTime() {
        return pickTime;
    }


    /**
     * @param pickTime the pickTime to set
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }


    /**
     * @return the originalAssignmentNo
     */
    public Long getOriginalAssignmentNo() {
        return originalAssignmentNo;
    }


    /**
     * @param originalAssignmentNo the originalAssignmentNo to set
     */
    public void setOriginalAssignmentNo(Long originalAssignmentNo) {
        this.originalAssignmentNo = originalAssignmentNo;
    }


    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }


    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }


    /**
     * @return the itemNo
     */
    public String getItemNo() {
        return itemNo;
    }


    /**
     * @param itemNo the itemNo to set
     */
    public void setItemNo(String itemNo) {
        this.itemNo = itemNo;
    }


    /**
     * @return the operatorID
     */
    public String getOperatorID() {
        return operatorID;
    }


    /**
     * @param operatorID the operatorID to set
     */
    public void setOperatorID(String operatorID) {
        this.operatorID = operatorID;
    }


    /**
     * @return the pickedQty
     */
    public Integer getPickedQty() {
        return pickedQty;
    }


    /**
     * @param pickedQty the pickedQty to set
     */
    public void setPickedQty(Integer pickedQty) {
        this.pickedQty = pickedQty;
    }


    /**
     * @return the siteId
     */
    public Long getSiteId() {
        return siteId;
    }


    /**
     * @param siteId the siteId to set
     */
    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }
    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 