/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.selection.model.ArchivePick;

import java.util.Date;
import java.util.List;

/**
 * DAO for accessing Archived Picks.
 *
 */
public interface ArchivePickDAORoot extends GenericDAO<ArchivePick> {
    
    /**
     * Get counts of location/item that where shorted.
     * 
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @param siteId - site to search for.
     * @return - list of arrarys containing location, item, count, last date shorted
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listShortedLocations(Date startTime, Date endTime, Long siteId)
    throws DataAccessException;
    
    /**
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @param siteId - site to search for.
     * @return - list of arrarys containing location, items picked, number of visits
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSlotVisitLocations(Date startTime, Date endTime, Long siteId)
    throws DataAccessException;
    
    /**
     * @param siteId - site to search for.
     * @return - list of arrarys containing location, items picked, number of visits
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSlotVisitLocationsBySiteId(Long siteId)
    throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 