/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.selection.model.ArchiveContainer;
import com.vocollect.voicelink.selection.model.ContainerReport;

import java.util.Date;
import java.util.List;

/**
 *
 *
 *
 * @author mraj
 */
public interface ArchiveContainerDAORoot extends GenericDAO<ArchiveContainer> {

    /**
     * Gets a list of archive container to populate in Container modified report
     * @param queryDecorator - the query decorator
     * @param startDate - the start date of the report
     * @param endDate - the end date of the report
     * @param siteId - the site id for the report
     * @return List<ArchiveContainer> - List of Archive Containers.
     * @throws DataAccessException
     */
    List<ContainerReport> listArchiveContainersForContainerReport(QueryDecorator queryDecorator,
                                                                   Date startDate,
                                                                   Date endDate,
                                                                   Long siteId)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 