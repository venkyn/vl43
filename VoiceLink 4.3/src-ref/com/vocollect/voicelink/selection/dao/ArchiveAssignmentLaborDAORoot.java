/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.selection.model.ArchiveAssignmentLabor;

import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author treed
 */
public interface ArchiveAssignmentLaborDAORoot extends GenericDAO<ArchiveAssignmentLabor> {
   
    /**
     * 
     * @param operatorIdentifier  - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(String operatorIdentifier, 
                                                Date startDate, Date endDate) 
                                                throws DataAccessException;
    /**
     * 
     * @param operatorIdentifier  - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier, 
                                                Date startDate, Date endDate) 
                                                throws DataAccessException;
    
    /**
     * 
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDurationByOperRegionDates(String operatorIdentifier, 
                                                      Integer regionNumber,
                                                      Date startDate, Date endDate)
                                                      throws DataAccessException;

    /**
     * Sum the quantity picked field of the assignment labor records by operator
     * labor id and date range.
     * 
     * @param operatorLaborId - OperatorLabor record Id.
     * @param startTime - start of date range.
     * @param endTime - end of date range
     * 
     * @return Number - the sum of the quantity picked field that meets the the
     *         criteria. Can be zero.
     * 
     * @throws DataAccessException - database exceptions
     */
    Number sumQuantityPickedBetweenDates(Long operatorLaborId, Date startTime, Date endTime) 
                                          throws DataAccessException;
     
    

    /**
     * 
     * @param queryDecorator  - query decorator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @param siteId - the site
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listSelectionAssignmentLaborReportRecords(QueryDecorator queryDecorator, 
                                                Date startDate, Date endDate, Long siteId) 
                                                throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 