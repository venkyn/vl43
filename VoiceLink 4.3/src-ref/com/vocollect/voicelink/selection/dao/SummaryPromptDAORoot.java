/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.selection.model.SummaryPrompt;

import java.util.List;


/**
 * SummaryPrompt Data Access Object (DAO) interface. 
 *
 */
public interface SummaryPromptDAORoot extends GenericDAO<SummaryPrompt> {
    
    /**
     * List all summary prompts.
     * @param decorator - additional query instructions.
     * @return - list of summary prompts
     * @throws DataAccessException - Database Exception
     */
    List<SummaryPrompt> listSummaryPrompts(QueryDecorator decorator)
        throws DataAccessException;

    
    /**
     * Returns the Long of the id, or null if not found.
     * @param name the name to look for
     * @throws DataAccessException - indicates database error
     * @return summaryprompt id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 