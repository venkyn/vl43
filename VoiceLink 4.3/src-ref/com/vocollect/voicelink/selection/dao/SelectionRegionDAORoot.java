/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegion;

/**
 * Selection Region Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface SelectionRegionDAORoot extends GenericDAO<SelectionRegion> {

    
    /**
     * Count the number of Assignments with a given status for a given region.
     * @param region - the region query is based on
     * @param status - the assignment status query is based on
     * @throws DataAccessException - indicates database error
     * @return the number of assignments
     */
    public Number countNumberOfAssignments(Region region,
                                           AssignmentStatus status)
        throws DataAccessException;

    /**
     * Count the number of Assignments with a given status for a given region.
     * Note: this should overload the countNumberOfAssignments() method, but
     * overloading is not possible in this framework.
     * @param region - the region query is based on
     * @param status - the assignment status query is based on
     * @param assignmentType - chase or normal
     * @throws DataAccessException - indicates database error
     * @return the number of assignments
     */
    public Number countNumberOfAssignmentsInGroup(Region region,
                                                  AssignmentStatus status,
                                                  AssignmentType assignmentType)
        throws DataAccessException;

    /**
     * Get the number of assignments in the assignment group with the most
     * assignments, for a given region.
     * @param region - the region query is based on
     * @param assignmentType - the assignment type (chase or normal)
     * @throws DataAccessException - indicates database error
     * @return the max number of assignments
     */
    public Number maxAssignmentsInAnyGroup(Region region,
                                            AssignmentType assignmentType)
        throws DataAccessException;

    /**
     * Get the minimum length work identifier for an assignment marked either
     * available or unavailable, for a given region.
     * @param region - the region query is based on
     * @param assignmentType - chase or normal
     * @throws DataAccessException - indicates database error
     * @return the min length of a work ID.
     */
    public Number minLengthWorkId(Region region, AssignmentType assignmentType)
        throws DataAccessException;
    
    
    /**
     * Set partial work id to work id when number of digits to speak is all.
     * 
     * @param region - region being updated
     * @return - number of rows updated
     * @throws DataAccessException - database exception
     */
    public Integer updatePartialWorkIdAll(SelectionRegion region)
        throws DataAccessException;

    /**
     * Set the partial work ID to the last (x) digits of work id.
     * 
     * @param size1 - number of digits to set work id to
     * @param size2 - number of digits to set work id to (repeated for 
     * auto-parameter mapping reasons)
     * @param region - region being updated
     * @return - number of rows updated
     * @throws DataAccessException - database exception
     */
    public int updateCustomPartialWorkIdPartial(int size1, int size2, Long region)
        throws DataAccessException;
    
    /**
     * @param regionNumber the number of the region
     * @return the region
     * @throws DataAccessException - database failure
     */
    public SelectionRegion findRegionByNumber(int regionNumber) 
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionNumber - the region number to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByNumber(int regionNumber)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionName - the name of the region to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByName(String regionName)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 