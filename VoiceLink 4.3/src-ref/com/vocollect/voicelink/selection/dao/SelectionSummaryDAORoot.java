/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.selection.model.SelectionSummary;

import java.util.Date;
import java.util.List;

/**
 * Selection Summary Access Object (DAO) interface.
 * 
 */
public interface SelectionSummaryDAORoot extends GenericDAO<SelectionSummary> {

    /**
     * Selection Assignment Summary query.
     * 
     * @param timeWindow the date that the data should be greater than
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listAssignmentSummary(Date timeWindow)  
    throws DataAccessException;

    /**
     * Selection Current Work Summary query.
     * 
     * @param timeWindow the date that the data should be greater than
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listCurrentWorkSummary(Date timeWindow)  throws DataAccessException;
    
    /**
     * Selection shorts summary query.
     * 
     * @param timeWindow the date that the data should be greater than
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listShortsSummary(Date timeWindow)  throws DataAccessException;
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 