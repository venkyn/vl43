/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.selection.model.Pick;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Picks Data Access Object (DAO) interface.
 * 
 * @author Dennis Doubleday
 */
public interface PickDAORoot extends GenericDAO<Pick> {
    /**
     * @param decorator - additional query instructions.
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<DataObject> listShorts(QueryDecorator decorator) throws DataAccessException;
    
    /**
     * Get all picks associated with any of the specified assignments.
     * @param decorator - additional query instructions.
     * @return the List of picks for the assignments.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listPicks(QueryDecorator decorator)
        throws DataAccessException;
    
    /**
     * retrieves a list of picks from assignments with the 
     * specified group number.
     * 
     * @param groupNumber - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Pick> listGroupsPicks(Long groupNumber) 
        throws DataAccessException;
    
    
    /**
     * Find shorts to automarkout for a specified location.
     * 
     * @param location - location to find for
     * @return - list of picks to auto mark out
     * @throws DataAccessException - Database Exception
     */
    List<Pick> listShortsForLocation(Location location) 
        throws DataAccessException;

    /**
     * Find shorts to automarkout for a specified location and item.
     * 
     * @param location - location to find for
     * @param item - item to find for
     * @return - list of picks to auto mark out
     * @throws DataAccessException - Database Exception
     */
    List<Pick> listShortsForLocationAndItem(Location location, Item item) 
        throws DataAccessException;
    


    //Following queries are for exports.
    
    /**
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @param qd - query decorator for number of results
     * @throws DataAccessException .
     */
    List<Map<String, Object>> listCompletedPicks(QueryDecorator qd)
        throws DataAccessException;

    /** 
     * Get list of labor records to export.
     * 
     * @return the list of labor records to export
     * @param qd - query decorator for number of results
     * @throws DataAccessException - Database failure
     */
    List<Map<String, Object>> listExportLaborRecords(QueryDecorator qd) 
    throws DataAccessException; 

    /**
     * Mark assignment records as exported.
     *  
     * @param qd - query decorator for number of results
     * @return list of export finish
     * @throws DataAccessException on database error
     */
    List<Map<String, Object>> listExportFinish(QueryDecorator qd) throws DataAccessException; 

    
    
    /**
     * @return list of maps.  Each map contains the fields for a picks file picked record,
     *         the object, and the matchingStatus.
     * @param qd - query decorator for number of results
     * @throws DataAccessException . 
     */
    List<Map<String, Object>> listPickedPicks(QueryDecorator qd)
        throws DataAccessException;


    /**
     * Updates pick details that are ready to be exported.
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updatePickedPicks() 
    throws DataAccessException;

    /**
     * Get counts of location/item that where shorted.
     * 
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @return - list of arrarys containing location, item, count, last date shorted
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listShortedLocations(Date startTime, Date endTime)
    throws DataAccessException;
    
    /**
     * Get counts of location/item that where shorted.
     * 
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @return - list of arrays containing location, items picked, number of visits
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSlotVisitLocations(Date startTime, Date endTime)
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 