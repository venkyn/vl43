/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author treed
 */
public interface ArchiveAssignmentDAORoot extends GenericDAO<ArchiveAssignment> {

    /**
     * Gets a list of archive assignments based on date and status.
     * @param createdDate date criteria
     * @param status status criteria
     * @return a list of archived assignments
     * @throws DataAccessException - database exception
     */
    Integer updateOlderThan(Date createdDate, AssignmentStatus status)
        throws DataAccessException;

    /**
     * Generate list of data for archive portion of assignment report
     * @param queryDecorator - the query decorator
     * @param startDate - the start date of the report
     * @param endDate - the end date of the report
     * @param siteId - the site id for the report
     * @return list of archived assignments for the report
     * @throws DataAccessException
     */
    List<AssignmentReport> listArchiveAssignmentsForAssignmentReport(QueryDecorator queryDecorator,
        Date startDate, Date endDate, Long siteId)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 