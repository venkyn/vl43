/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Assignment Data Access Object (DAO) interface.
 * 
 * @author ddoubleday
 */
public interface AssignmentDAORoot extends GenericDAO<Assignment> {

    /**
     * retrieves a list of assignments associated with a region.
     * 
     * @param regionId - the region to retrieve assignments for
     * @param assignmentTypes - chase or normal
     * @return list of assignments
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listAssignmentsInRegion(Long regionId,
                                             List<AssignmentType> assignmentTypes)
        throws DataAccessException;

    /**
     * retrieves a list of assignments with the specified reservedBy value.
     * 
     * @param reserveValue - value to retrieve by
     * @return list of requested assignments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listReservedAssignments(String reserveValue)
        throws DataAccessException;

    /**
     * retrieves a list of assignments that are active for the operator.
     * 
     * @param operatorId - value to retrieve by
     * @return list of requested assignments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listActiveAssignments(Operator operatorId)
        throws DataAccessException;

    /**
     * retrieves a list of assignments that avaibale and can be assigned to
     * specified operator.
     * 
     * @param queryDecorator - additional query instructions
     * @param operatorID - operator
     * @param reservedBy - Reserved value
     * @param type - type of assignments
     * @param regionID - region to look in
     * @param numberOfAssignments - maximum number of assignments
     * @return list of requested assignments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listAvailableAssignments(QueryDecorator queryDecorator,
                                              Operator operatorID,
                                              String reservedBy,
                                              AssignmentType type,
                                              Long regionID,
                                              Integer numberOfAssignments)
        throws DataAccessException;

    /**
     * retrieves a list of additional assignments that are active for the
     * operator.
     * 
     * @param queryDecorator - additional query instructions
     * @param operatorID - operator ID string
     * @param type - type of assignments
     * @param regionID - region to look in
     * @return list of requested assignments, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listAdditionalAssignments(QueryDecorator queryDecorator,
                                               Operator operatorID,
                                               AssignmentType type,
                                               Long regionID)
        throws DataAccessException;

    /**
     * Get all assignments in specified group.
     * 
     * @param groupNumber - group number to retrieve
     * @return - list of assignments in group
     * @throws DataAccessException - database exception
     */
    List<Assignment> listAssignmentsInGroup(long groupNumber)
        throws DataAccessException;

    /**
     * Get all assignments available for resequence.
     * 
     * @param decorator - additional query instructions.
     * @return - list of assignments
     * @throws DataAccessException - database exception
     */
    List<DataObject> listResequenceAssignments(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get all assignments available for resequence in the specified region.
     * 
     * @param regionId - long representing id of the region.
     * @return - list of assignments
     * @throws DataAccessException - database exception
     */
    List<DataObject> listResequenceAssignmentsByRegion(Long regionId)
        throws DataAccessException;

    /**
     * Get all assignments with specified assignment number.
     * 
     * @param number - group number to retrieve
     * @return - list of assignments in group
     * @throws DataAccessException - database exception
     */
    List<Assignment> listAssignmentsByNumber(Long number)
        throws DataAccessException;

    /**
     * Get max chase assignment number currently in use.
     * 
     * @return - maximum chase assignment number in use
     * @throws DataAccessException - Database Exception
     */
    Long maxChaseAssignment() throws DataAccessException;

    /**
     * Get Maximum group number in use.
     * 
     * @return - max group number in use
     * @throws DataAccessException - database exception
     */
    Long maxGroupNumber() throws DataAccessException;

    /**
     * List all assignments with same number to get maximum split ID.
     * 
     * @param assignmentNumber - assignment number to get
     * @return - list of assignments
     * @throws DataAccessException - Database exception
     */
    List<Assignment> listSplitNumber(Long assignmentNumber)
        throws DataAccessException;

    /**
     * List all assignments. This override is for a left join on the operatorid.
     * 
     * @param decorator - additional query instructions.
     * @return - list of chase assignments in decending number order
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listAssignments(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * List assignments with same work identifier.
     * 
     * @param queryDecorator - additional query instructions
     * @param workId - the work id to search for
     * @param operatorId - the operator requesting the work
     * @param assignmentType - the type of assignment being requested
     * @param regionId - the region being requested in
     * @return - list of assignments
     * @throws DataAccessException - Database Exception
     */
    List<Assignment> listAssignmentsByWorkId(QueryDecorator queryDecorator,
                                             String workId,
                                             Operator operatorId,
                                             AssignmentType assignmentType,
                                             Region regionId)
        throws DataAccessException;

    /**
     * List assignments with same work identifier.
     * 
     * @param queryDecorator - additional query instructions
     * @param workId - the work id to search for
     * @param operatorId - the operator requesting the work
     * @param assignmentType - the type of assignment being requested
     * @param regionId - the region being requested in
     * @return - list of assignments
     * @throws DataAccessException - Database Exception
     */
    List<Assignment> listAssignmentsByPartialWorkId(QueryDecorator queryDecorator,
                                                    String workId,
                                                    Operator operatorId,
                                                    AssignmentType assignmentType,
                                                    Region regionId)
        throws DataAccessException;

    /**
     * get a list of customer numbers in system.
     * 
     * @return list of customers
     */
    List<Object> listCustomerNumbers();
    
    /**
     * Method to get the list of departure date for a Route
     * and Delivery Date combination.
     * 
     * @param route String.
     * @param deliveryDate Date.
     * 
     * @return list of DepartureDate List<Date>.
     */
    public List<Date> listDDTByRouteDeliveryDate(String route, Date deliveryDate);    

    /**
     * get a list of routes in system.
     * @param statuses - list of assignment statuses
     * @return - list of routes
     */
    List<Object> listRoutes(List<AssignmentStatus> statuses);

    /**
     * Get a list of assignments with specified customer number and delivery
     * date in specified range.
     * 
     * @param customerNumber - customer number to look for
     * @param startDate - start date for delivery date range
     * @param endDate - end date for delivery date range
     * @return - list of assignments
     */
    List<Assignment> listCustomersWithDeliveryDate(String customerNumber,
                                                   Date startDate,
                                                   Date endDate);

    /**
     * Get a list of assignments with specified route and delivery date in
     * specified range.
     * 
     * @param route - route to look for
     * @param startDate - start date for delivery date range
     * @param endDate - end date for delivery date range
     * @return - list of assignments
     */
    List<Assignment> listRoutesWithDeliveryDate(String route,
                                                Date startDate,
                                                Date endDate);

    /**
     * Updates Assignments that are ready to be purged or archived.
     * 
     * @param purgeable - whether to mark for archive(1) or purge (2)
     * @param date - Date to find assignment older than
     * @param status - status of assignments to find
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateOlderThan(Integer purgeable,
                            Date date,
                            AssignmentStatus status) throws DataAccessException;

    /**
     * Get a specified number of assignments to archive.
     * 
     * @param queryDecorator - query decorator for number of row to retrieve
     * @return - list of assignments
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listArchive(QueryDecorator queryDecorator)
        throws DataAccessException;

    /**
     * Get assignments specific to the operator and assignment type.
     * 
     * @param operator - operator of whose assignments are to be retrieved
     * @param type assignment type - type of assignments to be retrieved.
     * @return - list of assignments
     * @throws DataAccessException - database exceptions
     */
    List<Assignment> listActiveAssignmentsByOperatorAndType(Operator operator,
                                                            AssignmentType type)
        throws DataAccessException;

    /**
     * @param deliveryLocation the delivery location as string
     * @return List the assignments contains the deliveryLocation
     * @throws DataAccessException dae
     */
    List<Assignment> listAssignmentsByDeliveryLocation(String deliveryLocation)
        throws DataAccessException;

    /**
     * Purge many to many records in sel_container_tag.
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge1() throws DataAccessException;

    /**
     * Purge many to many records in sel_containers.
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge2() throws DataAccessException;

    /**
     * Purge many to many records in sel_assignment_containers.
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge3() throws DataAccessException;

    /**
     * Purge many to many records in sel_pick_tag.
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge4() throws DataAccessException;

    /**
     * Purge many to many records in sel_assignment_tag.
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge5() throws DataAccessException;

    /**
     * Purge assigment records (picks, pick details, labor are database
     * cascades).
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge6() throws DataAccessException;

    /**
     * Purge assigment records (picks, pick details, labor are database
     * cascades).
     * 
     * @return - number of assignments (rows) purged
     * @throws DataAccessException - Database Exceptions
     */
    Integer updateSQLPurge7() throws DataAccessException;

    /**
     * . Gets a list of assignments for the assignment report
     * 
     * @param queryDecorator - the query decorator
     * @param startDate - the start date of the report
     * @param endDate - the end date of the report
     * @return list of assignments
     * @throws DataAccessException dae
     */
    List<AssignmentReport> listAssignmentsForAssignmentReport(QueryDecorator queryDecorator,
                                                              Date startDate,
                                                              Date endDate)
        throws DataAccessException;

    /**
     * Gets a list of suspended assignment picks for the assignment report.
     * 
     * @param queryDecorator - the query decorator
     * @param startDate - the start date of the report
     * @param endDate - the end date of the report
     * @return list of assignments
     * @throws DataAccessException dae
     */
    List<AssignmentReport> listManuallyPickedPicksForAssignmentReport(QueryDecorator queryDecorator,
                                                                      Date startDate,
                                                                      Date endDate)
        throws DataAccessException;    
    
    /**
     * @param route Route to consider
     * @param deliveryDate Delivery date of the route
     * @return average goal rate of the regions the route is in
     */
//    Double avgGoalRateOfRegions(String route, Date deliveryDate);
    
    /**
     * Method that will return the route and departure date on which an 
     * operator is working.
     * @param operator Operator
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByOperator(Operator operator) throws DataAccessException;
    
    /**
     * Method that will return the route and departure date on which an 
     * operator with assignments available or shorted on 
     * that route. 
     * 
     * @param operatorCurrentRegion Long
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByRegion(Long operatorCurrentRegion) throws DataAccessException;
    
    /**
     * Method that will return the route and departure date on which an 
     * operator worked last and completed assignments.
     * 
     * @param operatorCurrentRegion Long
     * @param operator Operator
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByLatestEndTime(Long operatorCurrentRegion,
                                                           Operator operator)
        throws DataAccessException;
    
    /**
     * Method to return list of route and delivery date combinations.  
     * @return list of combinations
     * @throws DataAccessException - the dae
     */
    List<Map<String, Object>> listDistinctRouteAndDeliveryDate() 
        throws DataAccessException;
    
    /**
     * Method to retrieve list of assignments on the basis of route and delivery date.
     * @param route selected route
     * @param deliveryDate selected deliverydate
     * @return number of records updated
     */
     List<Assignment> listAssignmentsByRouteDeliveryDate(String route, Date deliveryDate);
     
    /**
     * Method to sum quantity picked by route for completed assignments.
     * @param route - the route
     * @param shiftStartDate - the shift start date
     * @return The sum of quantity picked by route after shift start time
     */
    Long sumQuantityPickedTodayByRoute(String route, Date shiftStartDate);
    
    /**
     * Method to sum quantity remaining by route.
     * @param route - the route
     * @return The sum of quantity remaining by route
     */
    Long sumQuantityRemainingByRoute(String route);
    
    // Methods used by Route Aggregator and Route Assignment Status Aggregator

    /**
     * 
     * @param startDate start date of range
     * @param endDate end date of range
     * @param statuses list of assignment statuses to consider
     * @return list of assignment count on the basis of route, departure date
     *         and assignment status
     * @throws DataAccessException dae
     */
    List<Object[]> listAssignmentCountByRouteDDTAndStatus(Date startDate,
                                               Date endDate,
                                               List<AssignmentStatus> statuses)
        throws DataAccessException;

    /**
     * Method to get total quantity available per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listTotalQuantityAvailablePerRouteDDTByRegion(Date startDate,
                                                                 Date endDate)
        throws DataAccessException;

    /**
     * Method to get quantity picked per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listQuantityPickedPerRouteDDT(Date startDate, Date endDate)
        throws DataAccessException;

    /**
     * Method to get total quantity available to be picked for a route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listTotalQuantityPerRouteDDT(Date startDate, Date endDate)
        throws DataAccessException;

    /**
     * Method to get all operators in system per region.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return map of regions and associated operator Ids
     * @throws DataAccessException - dae
     */
    List<Object[]> listOperatorsByRouteRegion(Date startDate, Date endDate)
        throws DataAccessException;

    //Also used by Region DA
    /**
     * 
     * @return List of operators working in the region where route is in
     */
    List<Operator> listOperatorsInRegion();   
    
    //Infinite Region DA methods
    
    /**
     * method to return list of total items remaining for all regions from
     * assignment summaryInfo for hours remaining calculation of Infinite region
     * aggregator.
     * @return list of objects containing region id and items remaining
     * @throws DataAccessException dae
     */
    List<Object[]> listTotalItemsRemainingForAllRegions() throws DataAccessException;
    
    /**
     * method to return total items picked in all regions for infinite region aggregator.
     * @param shiftStartDate start time of shift for current site
     * @return list of regions with corresponding quantity picked values.
     * @throws DataAccessException dae
     */
    List<Object[]> listQtyPickedCasesChartData(Date shiftStartDate) throws DataAccessException;
    
    /**
     * method to return total items remaining in all regions for infinite region aggregator.
     * @return list of regions with corresponding quantity remaining values.
     * @throws DataAccessException dae
     */
    List<Object[]> listQtyRemainingCasesChartData() throws DataAccessException;
    
    // Region Assignment Status DA Methods

    /**
     * method to get list of regions with distinct departure dates and
     * assignment status and corresponding assignment count.
     * @param startDate start date of date range
     * @param endDate end date of date range
     * @param statuses list of assignment statuses to consider
     * @return list of map objects containig region id, departureDate,
     *         Assignment status and assignment count
     * @throws DataAccessException dae
     */
    List<Map<String, Object>> listAssignmentsByDateAndStatusInAllRegions(Date startDate,
                                                                         Date endDate,
                                                                         List<AssignmentStatus> statuses)
        throws DataAccessException;
    
    //Region DA
    
    /**
     * Method to get all quantity values in regions by delivery date.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return list of region-departure date map 
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllQuantitiesByRegionDepartureDate(Date startDate,
                                                                     Date endDate)
        throws DataAccessException;
    
    // Route Status DA

    /**
     * Method to sum quantity picked by route for completed assignments.
     * @param shiftStartDate - the shift start date
     * @return The sum of quantity picked by route after shift start time
     * @throws DataAccessException dae
     */
    List<Map<String, Object>> listQuantityPickedTodayForAllRoutes(Date shiftStartDate)
        throws DataAccessException;

    /**
     * Method to sum quantity remaining by route.
     * @return The sum of quantity remaining by route
     * @throws DataAccessException dae
     */
    List<Map<String, Object>> listQuantityRemainingforAllRoutes()
        throws DataAccessException;
    
    /**
     * Method that will return the operators working in each route.
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllOperatorsInRoutes() throws DataAccessException;
    
    /**
     * Method to get all Routes by region in a map. Routes are selected for Available and Shorted assignments
     * @return created map
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllRouteByRegion() throws DataAccessException;
    
    /**
     * Method to get all routes by latest end time.
     * @return list of map returned.
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listRouteByLatestEndTime() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 