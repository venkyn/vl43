/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import java.util.Date;


import java.util.List;

/**
 * AssignmentLabor Data Access Object (DAO) interface.
 * 
 * @author pfunyak
 */
public interface AssignmentLaborDAORoot extends GenericDAO<AssignmentLabor> {

    /**
     * Retrieves the open assignment labor record for the given assignment. or
     * returns null if there is no open assignment labor record.
     * 
     * @param assignmentId - The Id of the assignment.
     * @return The requested assignment laobr record or null
     * @throws DataAccessException on any failure.
     */
    AssignmentLabor findOpenRecordByAssignmentId(long assignmentId)
        throws DataAccessException;

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
     List<AssignmentLabor>  listOpenRecordsByOperatorId(long operatorId) 
                                                        throws DataAccessException;
    
     /**
      * Retrieves the closed assignment labor records for the given operator
      * or returns null if there is no closed assignment labor records.
      *
      * @param operatorId - the Id of the Operator
      * @return The requested Operator Labor record or null
      * @throws DataAccessException on any failure.
      */
      List<AssignmentLabor>  listRecordsByOperatorId(long operatorId) 
                                                         throws DataAccessException;
     
     
    /**
     * Retrieves the list of closed assignment labor record for the given
     * Operator Labor Id returns null is there are no assignment labor record
     * for the given Operator Labor Id.
     * 
     * @param breakLaborId - The Id of the Operator break labor record
     * @return The requested list of assignment labor records closed for this
     *         break or null
     * @throws DataAccessException on any failure.
     */

    List<AssignmentLabor> listClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException;

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorLaborId - the ID of the Operator
     * @return the requested list of assignment labor records or null
     * @throws DataAccessException on any failure.
     */
    List<AssignmentLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException;

    /**
     * Sum the quantity picked field of the assignment labor records by operator
     * labor id.
     * 
     * @param operatorLaborId - OperatorLabor record Id.
     * 
     * @return Integer - the sum of the quantity picked field that meets the the
     *         criteria. Can be zero.
     * 
     * @throws DataAccessException - database exceptions
     */
    Integer sumQuantityPicked(long operatorLaborId) throws DataAccessException;

    /**
     * Get all assignment labor records associated with any of the specified
     * operator labor records.
     * @param decorator - additional query instructions.
     * @return the List of assignment labor records.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listAssignmentLabor(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * 
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorId, 
                                               Date startDate, Date endDate) 
                                               throws DataAccessException;

    /**
     * 
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                               Date startDate, Date endDate) 
                                               throws DataAccessException;
    
    //Methods in Data Aggregators
    //Used in infinite region aggregator for hours remaining calculation
    
    /**
     * method to return list labor actual rate for all regions.
     * @return list of objects containing region id and labor actual rate sum
     * @throws DataAccessException dae
     */
    List<Object[]> listLaborActualRateForAllRegions() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 