/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerReport;

import java.util.Date;
import java.util.List;

/**
 * Container Data Access Object (DAO) interface.
 *
 * @author mkoenig
 */
public interface ContainerDAORoot extends GenericDAO<Container> {

    /**
     * retrieves a container by the container number.
     *
     * @param containerNumber - value to retrieve by
     * @return the container object
     * @throws DataAccessException - database exceptions
     */
    Container findContainerByNumber(String containerNumber)
        throws DataAccessException;

    /**
     * retrieves a container by the last x digits.
     *
     * @param partialContainerNumber - value to retrieve by
     * @return the container object
     * @throws DataAccessException - database exceptions
     */
    Container findContainerByPartialNumber(String partialContainerNumber)
        throws DataAccessException;

    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<Container> listAllContainers()
        throws DataAccessException;

    /**
     * Get all containers.
     * @param decorator - additional query instructions.
     * @return the List of pick details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listContainers(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * @param decorator - additional query instructions.
     * @param assignmentId - the ID of the Assignment associated with the Containers.
     * @return the List of containers for the assignment
     */
    List<DataObject> listContainersByAssignment(QueryDecorator decorator,
                                                Long assignmentId);

    /**
     * @param qd - additional query instructions.
     * @return the list of closed containers.
     * @throws DataAccessException - database failure
     */
    List<Container> listClosedContainers(QueryDecorator qd)
    throws DataAccessException;

    /**
     * Updates container that are ready to be exported.
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateClosedContainers()
    throws DataAccessException;

    /**
     * Gets a list of containers to populate in Container modified report
     *
     * @param queryDecorator - the query decorator
     * @param startDate - start date entered
     * @param endDate - end date entered
     * @return list of containers
     * @throws DataAccessException
     */
    List<ContainerReport> listContainersForContainerReport(QueryDecorator queryDecorator,
                                                      Date startDate,
                                                      Date endDate)
        throws DataAccessException;

    /**
     * Gets a list of containers with manual picks in suspended assignments to
     * populate in Container modified report
     *
     * @param queryDecorator - the query decorator
     * @param startDate - start date entered
     * @param endDate - end date entered
     * @return list of containers
     * @throws DataAccessException
     */
    List<ContainerReport> listContainersWithManuallPicksForContainerReport(QueryDecorator queryDecorator,
                                                                           Date startDate,
                                                                           Date endDate)
        throws DataAccessException;
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 