/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.selection.dao.AssignmentLaborDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for AssignmentLabor-related operations.
 * 
 * @author pfunyak
 */
public interface AssignmentLaborManagerRoot extends 
    GenericManager<AssignmentLabor, AssignmentLaborDAO>, DataProvider {

    
    /**
     * 
     * @param startTime - Assignment Start time.
     * @param operator - Operator that is picking the assignment.
     * @param assignment - The assignment object.
     * @param operLaborRecord - The operator labor to associate the assignment labor.
     * 
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
     void openLaborRecord(Date startTime, 
                          Operator operator,
                          Assignment assignment,
                          OperatorLabor operLaborRecord)
                          throws DataAccessException, BusinessRuleException;
    
    
    /**
     * @param endTime - Assignment Start time.
     * @param assignment - The assignment object.
     * @return TODO
     *
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
     AssignmentLabor closeLaborRecord(Date endTime, 
                           Assignment assignment)
                           throws DataAccessException, BusinessRuleException;
     
     /**
      * @param al - Assignment Labor record to recalculate values and save
      * 
      * @throws DataAccessException - if db failure
      * @throws BusinessRuleException - if save fails.
      */
     void recalculateAggregatesAndSave(AssignmentLabor al) throws DataAccessException, BusinessRuleException;
    
    /**
     * Retrieves the open Assignment Labor record by assignment ID, 
     * or returns null if no labor record exists.
     *
     * @param assignmentId - the assignment ID
     * @return the requested Operator, or null
     * @throws DataAccessException on any failure.
     */
     AssignmentLabor findOpenRecordByAssignmentId(long assignmentId) throws DataAccessException;
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.AssignmentLaborManager#listOpenRecordsByOperatorId(java.lang.String)
     */
     List<AssignmentLabor> listOpenRecordsByOperatorId(long operatorId) throws DataAccessException;
    
      
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.AssignmentLaborManager#findOpenLaborRecord(java.lang.String)
     */
     List<AssignmentLabor> listClosedRecordsByBreakLaborId(long breakLaborId) throws DataAccessException;

    
     /**
      * Retrieves the open assignment labor records for the given operator
      * or returns null if there is no open assignment labor records.
      *
      * @param operatorLaborId - the ID of the Operator
      * @return the requested list of assignment labor records or null
      * @throws DataAccessException on any failure.
      */
      List<AssignmentLabor>  listAllRecordsByOperatorLaborId(long operatorLaborId) 
                                                             throws DataAccessException;
     
     /**
      * {@inheritDoc}
      * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#sumQuantityPicked(java.lang.String)
      */
      Integer sumQuantityPicked(long operatorLaborId) throws DataAccessException;
      
      
      /**
       * 
       * @param operatorId - operator 
       * @param startDate - start of date range
       * @param endDate - end of date range
       * @return list of object 
       * @throws DataAccessException on db failure
       */
      List<Object[]> listLaborSummaryReportRecords(Long operatorId, 
                                                  Date startDate, Date endDate) 
                                                  throws DataAccessException;

      /**
       * 
       * @param operatorId - operator 
       * @param startDate - start of date range
       * @param endDate - end of date range
       * @return list of object 
       * @throws DataAccessException on db failure
       */
      List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                                  Date startDate, Date endDate) 
                                                  throws DataAccessException;
      
      //Methods in Data Aggregators
      //Used in infinite region aggregator for hours remaining calculation
      
      /**
       * method to return list labor actual rate for all regions.
       * @return list of objects containing region id and labor actual rate sum
       * @throws DataAccessException dae
       */
      List<Object[]> listLaborActualRateForAllRegions() throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 