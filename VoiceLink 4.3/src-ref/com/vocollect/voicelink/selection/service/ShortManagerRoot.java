/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.selection.model.Pick;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick-related operations.
 * 
 * @author ddoubleday
 */
public interface ShortManagerRoot extends PickManager {
    
    /**
     * Markout a short item in the database.
     *
     * @param pick to be marked out
     * @throws BusinessRuleException if the instance could not be marked out
     * because of a business rule.
     * @throws DataAccessException on any other failure
     */
    public void executeMarkout(Pick pick) throws BusinessRuleException, DataAccessException;

    /**
     * Accept quantity picked for a short item in the database.
     *
     * @param pick to accept quantity picked
     * @throws BusinessRuleException if the instance could not be accepted
     * because of a business rule.
     * @throws DataAccessException on any other failure
     */
    public void executeAcceptQuantityPicked(Pick pick) throws BusinessRuleException, DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 