/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.selection.dao.AssignmentDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Assignment-related operations.
 * 
 * @author ddoubleday
 */
public interface AssignmentManagerRoot extends
        GenericManager<Assignment, AssignmentDAO>, DataProvider {

    /**
     * get list of reserved assignments based on reserve value.
     * 
     * @param reserveValue
     *            - reserve value to get
     * @return - list of assignment with specified reerve value
     * @throws DataAccessException
     *             - database exception
     */
    List<Assignment> listReservedAssignments(String reserveValue)
            throws DataAccessException;

    /**
     * get list of active assginments for an operator.
     * 
     * @param operatorId
     *            operator to get assignments for.
     * @return a list of assignments.
     * @throws DataAccessException
     *             - database exceptions
     */
    List<Assignment> listActiveAssignments(Operator operatorId)
            throws DataAccessException;

    /**
     * get list of assignments for a region.
     * 
     * @param regionId
     *            region to get assignments for.
     * @param assignmentTypes
     *            - chase or normal
     * @return a list of assignments.
     * @throws DataAccessException
     *             - database exceptions
     */
    List<Assignment> listAssignmentsInRegion(Long regionId,
            List<AssignmentType> assignmentTypes) throws DataAccessException;

    /**
     * get list of available assignments for region, type and operator.
     * 
     * @param queryDecorator
     *            - additional query instructions
     * @param operatorID
     *            - operator ID for reserved field
     * @param reservedBy
     *            - Reserved value
     * @param type
     *            - Type of assignment to get
     * @param regionID
     *            - Region to look in
     * @param numberOfAssignments
     *            - maximum number of assignments in group
     * @return - return list of assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    List<Assignment> listAvailableAssignments(QueryDecorator queryDecorator,
            Operator operatorID, String reservedBy, AssignmentType type,
            Long regionID, Integer numberOfAssignments)
            throws DataAccessException;

    /**
     * get list of available additional assignments for region, type and
     * operator.
     * 
     * @param queryDecorator
     *            - additional query instructions
     * @param operatorID
     *            - operator ID for reserved field
     * @param type
     *            - Type of assignment to get
     * @param regionID
     *            - Region to look in
     * @return - return list of assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    List<Assignment> listAdditionalAssignments(QueryDecorator queryDecorator,
            Operator operatorID, AssignmentType type, Long regionID)
            throws DataAccessException;

    /**
     * get a list of assignment for specified group.
     * 
     * @param groupNumber
     *            - group to retrieve
     * @return - list of assignments in group
     * @throws DataAccessException
     *             - database exception
     */
    List<Assignment> listAssignmentsInGroup(long groupNumber)
            throws DataAccessException;

    /**
     * get list of assignments with specified number. This may be multiple
     * assignments if assignment was split.
     * 
     * @param number
     *            - assignment number to retrieve
     * @return - return list of assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    List<Assignment> listAssignmentsByNumber(Long number)
            throws DataAccessException;

    /**
     * List assignments with same work identifier.
     * 
     * @param queryDecorator
     *            - additional query instructions
     * @param workId
     *            - the work id to search for
     * @param operatorId
     *            - the operator requesting the work
     * @param assignmentType
     *            - the type of assignment being requested
     * @param regionId
     *            - the region being requested in
     * @return - list of assignments
     * @throws DataAccessException
     *             - Database Exception
     */
    List<Assignment> listAssignmentsByWorkId(QueryDecorator queryDecorator,
            String workId, Operator operatorId, AssignmentType assignmentType,
            Region regionId) throws DataAccessException;

    /**
     * List assignments with same work identifier.
     * 
     * @param queryDecorator
     *            - additional query instructions
     * @param workId
     *            - the work id to search for
     * @param operatorId
     *            - the operator requesting the work
     * @param assignmentType
     *            - the type of assignment being requested
     * @param regionId
     *            - the region being requested in
     * @return - list of assignments
     * @throws DataAccessException
     *             - Database Exception
     */
    List<Assignment> listAssignmentsByPartialWorkId(
            QueryDecorator queryDecorator, String workId, Operator operatorId,
            AssignmentType assignmentType, Region regionId)
            throws DataAccessException;

    /**
     * Method for generating Assignment Report.
     * 
     * @param queryDecorator
     *            - the decorator for the where clause
     * @param workId
     *            - WorkID of the assignment.
     * @param operatorId
     *            - Set of Operator ID.
     * @param assignmentNumber
     *            - Assignment Number.
     * @param customerNumber
     *            - Customer Number.
     * @param route
     *            - Route of the Assignment.
     * @param startTime
     *            - Start Time of the Report.
     * @param endTime
     *            - End Time of the Report.
     * @return List<Assignment> - List of Assignments.
     * @throws DataAccessException dae
     */
    List<AssignmentReport> listAssignmentsForAssignmentReport(
            QueryDecorator queryDecorator, String workId,
            Set<String> operatorId, Long assignmentNumber,
            String customerNumber, String route, Date startTime, Date endTime)
            throws DataAccessException;

    /**
     * Method for getting active assignments assigned to an operator of the
     * function type.
     * 
     * @param operator
     *            - operator of whose assignments are to be retrieved
     * @param type assignment type
     *            - type of assignments to be retrieved.
     * @return List<Assignment> - List of Assignments.
     * @throws DataAccessException dae
     */
    List<Assignment> listActiveAssignmentsByOperatorAndType(Operator operator,
            AssignmentType type) throws DataAccessException;

    /**
     * @param deliveryLocation
     *            The delivery location string value
     * @return List of assignments
     * @throws DataAccessException dae
     */
    List<Assignment> listAssignmentsByDeliveryLocation(String deliveryLocation)
            throws DataAccessException;

    /**
     * Create chase assignment using the specified shorted picks.
     * 
     * @param shorts
     *            A collection of shorted picks.
     * @param operator
     *            The operator to assign to the chase assignment
     * @return The new chase assignment.
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    Assignment executeCreateChaseAssignment(ArrayList<Pick> shorts,
            Operator operator) throws DataAccessException,
            BusinessRuleException;

    /**
     * Validate chase assignment using the specified shorted picks.
     * 
     * @param shorts
     *            A collection of shorted picks.
     * @param operator
     *            The operator to assign to the chase assignment
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void validateCreateChaseAssignment(ArrayList<Pick> shorts, Operator operator)
            throws DataAccessException, BusinessRuleException;

    /**
     * Validate and groups assignments together.
     * 
     * @param assignments
     *            group of assignments to create
     * @param fromGUI
     *            detemrines if group is being created from GUI or Task
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void executeGroupAssignments(ArrayList<Assignment> assignments,
            boolean fromGUI) throws DataAccessException, BusinessRuleException;

    /**
     * Validate that list of assignments can be grouped.
     * 
     * @param assignments
     *            - list of assignments
     * @param fromGUI
     *            - whether called from GUI or task
     * @throws BusinessRuleException
     *             - business rule exception
     */
    void executeValidateGroup(ArrayList<Assignment> assignments, boolean fromGUI)
            throws BusinessRuleException;

    /**
     * Ungroups assignments if they belong to a manual issuance region.
     * 
     * @param assignments
     *            - assignments to ungroup
     * @throws DataAccessException
     *             - Database Exception
     * @throws BusinessRuleException
     *             - Business rule exception
     */
    public void executeUngroupFromManualRegion(ArrayList<Assignment> assignments)
            throws DataAccessException, BusinessRuleException;

    /**
     * Validate and ungroups assignments.
     * 
     * @param assignments
     *            group of assignments to ungroup
     * @param fromGUI
     *            detemrines if group is being created from GUI or Task
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void executeUngroupAssignments(ArrayList<Assignment> assignments,
            boolean fromGUI) throws DataAccessException, BusinessRuleException;

    /**
     * Create Split assignment using the specified picks.
     * 
     * @param picks
     *            A collection of picks.
     * @param operatorOriginal
     *            Operator to assign to original part of assignment
     * @param operatorNew
     *            Operator to assign to new part of assignment
     * @return The new split assignment.
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    Assignment executeCreateSplitAssignment(ArrayList<Pick> picks,
            Operator operatorOriginal, Operator operatorNew)
            throws DataAccessException, BusinessRuleException;

    /**
     * Used for reseting state during unit tests.
     */
    public void initializeState();

    /**
     * Clears all reserved assignment for an operator.
     * 
     * @param operator
     *            - operator to clear
     * @throws DataAccessException
     *             - database exceptions
     */
    public void executeClearReservedAssignments(Operator operator)
            throws DataAccessException;

    /**
     * 
     * @param assignment
     *            - save the changes on this assignment.
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void executeAssignmentChangeSave(Assignment assignment)
            throws DataAccessException, BusinessRuleException;

    /**
     * 
     * @param assignment
     *            - The assignment to check.
     * @throws DataAccessException
     *             on database error.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void executeCheckIfEditable(Assignment assignment)
            throws DataAccessException, BusinessRuleException;

    /**
     * 
     * @param picks
     *            - A collection of picks.
     * @throws BusinessRuleException
     *             on violation of business logic.
     */
    void validateSplitAssignment(ArrayList<Pick> picks)
            throws BusinessRuleException;

    /**
     * get list of assignments available to resequence from the given region.
     * 
     * @param regionId
     *            - id of the region to resequence
     * @return - return list of assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    public List<DataObject> listResequenceAssignmentsByRegion(Long regionId)
            throws DataAccessException;

    /**
     * get list of assignments available to resequence from the given region.
     * 
     * @param rdi
     *            - ResultDataInfo to reqsequence
     * @return - return list of assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    List<DataObject> getResequenceData(ResultDataInfo rdi)
            throws DataAccessException;

    /**
     * Updates cartons in line loading if defined on pick records.
     * 
     * @param a
     *            - assignment to check
     * @throws DataAccessException
     *             - database exceptions.
     */
    public void executeUpdateLineLoading(Assignment a)
            throws DataAccessException;

    /**
     * Implementation of list assignments to resequence for the given region.
     * 
     * @param sequenceList
     *            the list of sequence numbers
     * @param regionId
     *            the region being worked in.
     * @throws DataAccessException
     *             on any database failure
     * @throws BusinessRuleException
     *             on any business rule violation.
     */
    public void executeResequence(ArrayList<Long> sequenceList, Long regionId)
            throws DataAccessException, BusinessRuleException;


    /**
     * @param assignment assignment
     *            The assignment for which we are going to create container for
     * @return The list of selection containers created
     * @throws DataAccessException dae
     * @throws BusinessRuleException bre
     */
    public List<Container> executeCreateLoadingContainer(Assignment assignment)
            throws DataAccessException, BusinessRuleException;

    /**
     * Getter for the defaultPriority property.
     * 
     * @return Integer value of the property
     */
    public Integer getDefaultPriority();

    /**
     * Getter for the availablePriorities property.
     * 
     * @return Map&lt;Integer,String&gt; value of the property
     */
    public Map<Integer, String> getAvailablePriorities();

    /**
     * get a list of customer numbers in system.
     * 
     * @return list of customers
     */
    public List<String> listCustomerNumbers();

    /**
     * Method to get the latest departure date for a Route
     * and Delivery Date combination.
     * 
     * @param route String.
     * @param deliveryDate Date.
     * 
     * @return latestDepartureDate Date.
     */
    public Date findEarliestDDTByRouteDeliveryDate(String route, Date deliveryDate);
    
    /**
     * get a list of routes in system.
     * 
     * @return - list of routes
     */
    public List<String> listRoutes();

    /**
     * Update priority for a given set of assignment.
     * 
     * @param applyBy
     *            - customer(0) or route(1)
     * @param applyValue
     *            - customer or route to apply to.
     * @param priority
     *            - priority to apply
     * @param startDate
     *            - delivery date start range
     * @param endDate
     *            - delivery date end range
     * @return - number of assignment affected
     */
    public int executeUpdatePriority(Integer applyBy, String applyValue,
            Integer priority, Date startDate, Date endDate);

    /**
     * This method is used in a assignment import validation. It finds loading
     * routes that have matching route number, loading region, departure date
     * time and customer number. If one is not found, an import record should
     * fail validation.
     * 
     * @param route
     *            - loading route to look for.
     * @param loadingRegion
     *            - loading RegionNumber to look for.
     * @param departureDateTime
     *            - loading departureDateTime to look for.
     * @param customer
     *            - customerNumber to look for
     * @return - loadingRoute found, if any. If loading region and departure
     *         date time fields are not provided a new LoadingRoute object is
     *         returned
     * @throws DataAccessException
     *             -.
     */
    public LoadingRoute findLoadingRoute(String route,
            LoadingRegion loadingRegion, Date departureDateTime,
            Customer customer) throws DataAccessException;

    /**
     * @param assignment
     *            The assignment number required to be checked
     * @return True if integrated, false otherwise
     */
    public boolean isSelectionLoadingIntegrated(Assignment assignment);
    
    /**
     * @param route Route to consider
     * @param deliveryDate Delivery date of the route
     * @return average goal rate of the regions route is in
     */
//    public Double getAvgGoalRateOfRegions(String route, Date deliveryDate);
    
    /**
     * Method that will return the route and departure date on which an 
     * operator is working.
     * @param operator Operator
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByOperator(Operator operator) throws DataAccessException;
    
    /**
     * Method that will return the route and departure date on which an 
     * operator with assignments available or shorted on 
     * that route. 
     * 
     * @param operatorCurrentRegion Long
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByRegion(Long operatorCurrentRegion) throws DataAccessException;
    
    /**
     * Method that will return the route and departure date on which an 
     * operator worked last and completed assignments.
     * 
     * @param operatorCurrentRegion Long
     * @param operator Operator
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Object[]> listRouteAndDepartureDateByLatestEndTime(Long operatorCurrentRegion,
                                                           Operator operator)
        throws DataAccessException;
    
    /**
     * Method to return list of route and delivery date combinations. 
     * @return list of combinations
     * @throws DataAccessException - the dae
     */
    public List<Map<String, Object>> listDistinctRouteAndDeliveryDate() 
        throws DataAccessException;
    
    /**
     * Method to retrieve list of assignments on the basis of route and delivery date.
     * @param route selected route
     * @param deliveryDate selected deliverydate
     * @return number of records updated
     */
    List<Assignment> listAssignmentsByRouteDeliveryDate(String route, Date deliveryDate);
    
    // Methods used by Route Aggregator and Route Assignment Status Aggregator
    
    /**
     * Method to create route and departure date unique combination string.
     * @param route - the route
     * @param departureDate - the departure date
     * @return formatted string
     * @throws DataAccessException dae
     */
    String getRouteDepartureDateField(String route, Date departureDate) throws DataAccessException;

    /**
     * 
     * @param startDate start date of range
     * @param endDate end date of range
     * @param statuses list of assignment statuses to consider
     * @return list of assignment count on the basis of route, departure date
     *         and assignment status
     * @throws DataAccessException dae
     */
    List<Object[]> listAssignmentCountByRouteDDTAndStatus(Date startDate,
                                               Date endDate,
                                               List<AssignmentStatus> statuses)
        throws DataAccessException;

    /**
     * Method to get total quantity available per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listTotalQuantityAvailablePerRouteDDTByRegion(Date startDate,
                                                                 Date endDate)
        throws DataAccessException;

    /**
     * Method to get quantity picked per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listQuantityPickedPerRouteDDT(Date startDate, Date endDate)
        throws DataAccessException;

    /**
     * Method to get total quantity available to be picked for a route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return array of route, DDT, quantity objects
     * @throws DataAccessException - dae
     */
    List<Object[]> listTotalQuantityPerRouteDDT(Date startDate, Date endDate)
        throws DataAccessException;

    /**
     * Method to get all operators in system per region.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return map of regions and associated operator Ids
     * @throws DataAccessException - dae
     */
    List<Object[]> listOperatorsByRouteRegion(Date startDate, Date endDate)
        throws DataAccessException;

    //Also used by Region DA
    /**
     * 
     * @return List of operators working in the region where route is in
     */
    List<Operator> listOperatorsInRegion(); 
    
    //Infinite Region DA methods
    
    /**
     * method to return list of total items remaining for all regions from
     * assignment summaryInfo for hours remaining calculation of Infinite region
     * aggregator.
     * @return list of objects containing region id and items remaining
     * @throws DataAccessException dae
     */
    List<Object[]> listTotalItemsRemainingForAllRegions() throws DataAccessException;
    
    /**
     * method to return total items picked in all regions for infinite region aggregator.
     * @param shiftStartDate start time of shift for current site
     * @return list of regions with corresponding quantity picked values.
     * @throws DataAccessException dae
     */
    List<Object[]> listQtyPickedCasesChartData(Date shiftStartDate) throws DataAccessException;
    
    /**
     * method to return total items remaining in all regions for infinite region aggregator.
     * @return list of regions with corresponding quantity remaining values.
     * @throws DataAccessException dae
     */
    List<Object[]> listQtyRemainingCasesChartData() throws DataAccessException;
    
    // Region Assignment Status DA Methods

    /**
     * method to get list of regions with distinct departure dates and
     * assignment status and corresponding assignment count.
     * @param startDate start date of date range
     * @param endDate end date of date range
     * @param statuses list of assignment statuses to consider
     * @return list of map objects containig region id, departureDate,
     *         Assignment status and assignment count
     * @throws DataAccessException dae
     */
    List<Map<String, Object>> listAssignmentsByDateAndStatusInAllRegions(Date startDate,
                                                                         Date endDate,
                                                                         List<AssignmentStatus> statuses)
        throws DataAccessException;
    
    //Region DA
    
    /**
     * Method to get all quantity values in regions by delivery date.
     * @param startDate - window start time
     * @param endDate - window end time
     * @return list of region-departure date map 
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllQuantitiesByRegionDepartureDate(Date startDate,
                                                                     Date endDate)
        throws DataAccessException;

    /**
     * Method that will return the operators working in each route.
     * @return List<String> representing the route
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllOperatorsInRoutes() throws DataAccessException;
    
    /**
     * Method to get all Routes by region in a map. Routes are selected for Available and Shorted assignments
     * @return created map
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listAllRouteByRegion() throws DataAccessException;

    /**
     * Method to get region wise routes having latest end time or last worked upon.
     * @return list of map created
     * @throws DataAccessException - dae
     */
    List<Map<String, Object>> listRouteByLatestEndTime() throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 