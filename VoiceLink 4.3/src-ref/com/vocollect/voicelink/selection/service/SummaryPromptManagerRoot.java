/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.selection.dao.SummaryPromptDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.SummaryPrompt;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinition;
import com.vocollect.voicelink.selection.model.SummaryPromptItem;

import java.util.List;
import java.util.Locale;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Summary prompt-related operations.
 *
 */
/**
 * 
 *
 * @author mkoenig
 */
public interface SummaryPromptManagerRoot 
extends GenericManager<SummaryPrompt, SummaryPromptDAO>, DataProvider {

    /**
     * @param rdi - Information (such as sorting) regarding how to retreive the data
     * @return A list of all objects
     * @throws DataAccessException on any failure
     */
    public List<SummaryPrompt> getAll(ResultDataInfo rdi) throws DataAccessException;
    
    /**
     * Build a default summary prompt similar to one in task file. 
     * 
     * @return - SummaryPrompt object
     * @throws DataAccessException - Database Exceptions
     */
    public SummaryPrompt getDefaultSummaryPrompt() throws DataAccessException;
    
    /**
     * 
     * @param sp - summary prompt to buld sample prompts for
     * @param locale - locale to build prompt for
     * @return - list of strings for sample prompt 0 = blank or zero, 
     *          1 = single or not blank, 2 = plural or not blank
     */
    public List<String> buildSamplePrompts(SummaryPrompt sp,
        Locale locale);
    
    /**
     * Build a summary prompt for an assignment. 
     * 
     * @param assignment - assignment to build prompt for.
     * @param assignments - group of assignments the assignment belongs to 
     *                      used for prompt items that need group totals
     * @param locale - locale to get prompt for
     * @return - summary prompt to speak.
     */
    public String buildSummaryPrompt(Assignment assignment, 
                                     List<Assignment> assignments,
                                     Locale locale);
    
    /**
     * Returns a list of all support locales that are not currently defined 
     * in the specified summary prompt definition.
     * 
     * @param sp - summary prompt to buld sample prompts for
     * @throws DataAccessException - Database exceptions 
     * @return - list of locales system supports
     */
    public List<Locale> getNotYetDefinedLocales(SummaryPrompt sp) 
    throws DataAccessException;
 
    /**
     * Getter for the summaryPromptItemDAO property.
     * @return GenericDAO&lt;SummaryPromptItem&gt; value of the property
     */
    public GenericDAO<SummaryPromptItem> getSummaryPromptItemDAO();
    
    /**
     * Deletes a locale definition from the summary prompt passed in.
     * 
     * @param sp - summary prompt to delete locale from.
     * @param locale - locale to delete
     * @return - same summary prompt passed in minus the locale definition.
     * @throws DataAccessException - on data access failure.
     * @throws BusinessRuleException - on db save failure
     */
    public SummaryPrompt executeDeleteLocale(SummaryPrompt sp, Locale locale)
                           throws BusinessRuleException, DataAccessException;
   
    /**
     * Returns true id locale exists in any of summary prompts.
     * 
     * @param locale - locale to delete
     * @return - true or false to indicate if locale is present in any of summary prompts.
     * @throws DataAccessException - Data access exceptions
     */
    public boolean localeExistsInPrompts(Locale locale)
    throws DataAccessException;
    
    /**
     * Save the editing of summary prompt defintions. 
     * 
     * @param sp - summary prompt defintions
     * @param definitions - list of new definitions
     * @return - return summary prompt object
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Data access exceptions
     */
    public SummaryPrompt executeSaveEdit(SummaryPrompt sp, 
                                 List<SummaryPromptDefinition> definitions)

    throws BusinessRuleException, DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 