/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.dao.SelectionRegionDAO;
import com.vocollect.voicelink.selection.dao.SelectionSummaryDAO;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.model.SelectionSummary;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;
import com.vocollect.voicelink.selection.service.SelectionRegionProfileManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;

/**
 * Service methods for the <code>SelectionRegion</code> model object.
 *
 * @author bnorthrop
 */
public abstract class SelectionRegionManagerImplRoot extends
    GenericManagerImpl<SelectionRegion, SelectionRegionDAO> implements
    SelectionRegionManager {

    //All Summaries.
    private static final int REGION = 0;

    //Assignment summary query positions
    private static final int TOTAL_ASSIGNMENTS = 1;
    private static final int INPROGRESS = 2;
    private static final int AVAILABLE = 3;
    private static final int COMPLETE = 4;
    private static final int NON_COMPLETE = 5;
    private static final int ASSIGN_SITE = 6;

    //Current work summary query positions
    private static final int OPERATOR_IN_REGION = 1;
    private static final int OPERATOR_ASSIGNED = 2;
    private static final int TOTAL_ITEMS_PICKED = 3;
    private static final int TOTAL_ITEMS_REMAINING = 4;
    private static final int CURRENT_WORK_SITE = 5;

    //Current work summary query positions
    private static final int TOTAL_SHORTS = 1;
    private static final int SHORTED = 2;
    private static final int ASSIGNED = 3;
    private static final int MARKEDOUT = 4;
    private static final int SHORT_SITE = 5;


    //Comparator to sort by group position
    static final Comparator<SelectionRegionProfile> PROFILE_NUMBER =
        new Comparator<SelectionRegionProfile>() {
            public int compare(SelectionRegionProfile srp1,
                               SelectionRegionProfile srp2) {
                 return srp1.getNumber().compareTo(srp2.getNumber());
            }
        };

    private SelectionRegionProfileManager selectionRegionProfileManager;

    private WorkgroupManager workgroupManager;

    private AssignmentManager assignmentManager;

    private SelectionSummaryDAO  selectionSummaryDAO;

    private RegionManager regionManager;

    private OperatorDAO operatorDAO;



    /**
     * Getter for the operatorDAO property.
     * @return OperatorDAO value of the property
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }



    /**
     * Setter for the operatorDAO property.
     * @param operatorDAO the new operatorDAO value
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }


    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }


    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }


    /**
     * Getter for the selectionSummaryDAO property.
     * @return SelectionSummaryDAO value of the property
     */
    public SelectionSummaryDAO getSelectionSummaryDAO() {
        return selectionSummaryDAO;
    }


    /**
     * Setter for the selectionSummaryDAO property.
     * @param selectionSummaryDAO the new selectionSummaryDAO value
     */
    public void setSelectionSummaryDAO(SelectionSummaryDAO selectionSummaryDAO) {
        this.selectionSummaryDAO = selectionSummaryDAO;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return this.assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Getter for the selectionRegionProfileManager property.
     * @return SelectionRegionProfileManager value of the property
     */
    public SelectionRegionProfileManager getSelectionRegionProfileManager() {
        return selectionRegionProfileManager;
    }


    /**
     * Setter for the selectionRegionProfileManager property.
     * @param selectionRegionProfileManager the new selectionRegionProfileManager value
     */
    public void setSelectionRegionProfileManager(SelectionRegionProfileManager selectionRegionProfileManager) {
        this.selectionRegionProfileManager = selectionRegionProfileManager;
    }

    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }


    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public SelectionRegionManagerImplRoot(SelectionRegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Validate the business rules described in the EDD for editing a region.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    public void executeValidateEditRegion(SelectionRegion region)
        throws DataAccessException, BusinessRuleException {

        //Save current site context filter and turn it off for these
        //queries
        boolean siteContextFilter = SiteContextHolder.getSiteContext().isFilterBySite();

        SiteContextHolder.getSiteContext().setFilterBySite(false);

        if (!region.isNew()) {
            validateNoOperatorsSignedIn(region);
            validateAssignmentStatus(region);
            validateDigitsSpoken(region);
            validateNumberOfAssignments(region);
            validateIssuance(region);
            validateWorkIdRequestLength(region);
        }
        //reset site context filter
        SiteContextHolder.getSiteContext().setFilterBySite(siteContextFilter);

        //validateUniqueRegionNameAndNumber(region);
    }


    /**
     * If manual issuance,then work IDs length for normal and chase assignment must be equal.
     *
     * @author nsyed
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */

    protected void validateWorkIdRequestLength(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        final boolean isManualNormal = !region.getProfileNormalAssignment().isAutoIssuance();
        final boolean isManualChase = !region.getProfileChaseAssignment().isAutoIssuance();
        if (isManualNormal && isManualChase) {
            int workIdRequestLengthNormal = region.getUserSettings(AssignmentType.Normal)
                .getWorkIdentifierRequestLength();

            int workIdRequestLengthChase = region.getUserSettings(AssignmentType.Chase)
            .getWorkIdentifierRequestLength();
                if (workIdRequestLengthNormal != workIdRequestLengthChase) {
                    throw new BusinessRuleException(
                        SelectionErrorCode.REGION_WORK_IDS_NOT_EQUAL,
                        new UserMessage(
                            "region.edit.error.workIdsNotEqual"));
                }
        }
    }



    /**
     * From EDD - Edit Region: If a user switches from a region profile that had
     * automatic issuance to one that has manual issuances, there can be no
     * assignments in a group with a status of Available or Unavailable.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateIssuance(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        validateIssuance(region, AssignmentType.Normal);
        validateIssuance(region, AssignmentType.Chase);
    }

    /**
     * Overloaded to check for chase or normal.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @param type - chase or normal
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated *
     */
    private void validateIssuance(SelectionRegion region, AssignmentType type)
        throws DataAccessException, BusinessRuleException {

        final boolean isManual = !region.getProfile(type).isAutoIssuance();

        if (isManual) {
            int numAvailable = convertNumberToInt(
                getPrimaryDAO().countNumberOfAssignmentsInGroup(
                    region, AssignmentStatus.Available, type));

            int numUnavailable = convertNumberToInt(
                getPrimaryDAO().countNumberOfAssignmentsInGroup(
                    region, AssignmentStatus.Unavailable, type));

            if (numAvailable + numUnavailable > 0) {
                throw new BusinessRuleException(
                    SelectionErrorCode.REGION_INVALID_ISSUANCE,
                    new UserMessage("region.edit.error.invalidIssuance"));

            }
        }
    }

    /**
     * From EDD - Edit Region: If a user modifies the number of assignments
     * allowed with a region profile that allows multiple assignments, or
     * switches the region profile from one that allowed multiple assignments to
     * one that doesn't, then there cannot be a group of assignments in the
     * region with more assignments then is now allowed.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateNumberOfAssignments(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        validateNumberOfAssignments(region, AssignmentType.Normal);
        validateNumberOfAssignments(region, AssignmentType.Chase);
    }

    /**
     * Overloaded method for checking number of assignments for a given
     * assignment type.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @param assignmentType - chase or normal
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated *
     */
    private void validateNumberOfAssignments(SelectionRegion region,
                                             AssignmentType assignmentType)
        throws BusinessRuleException, DataAccessException {

        int max = 0;
        if (region.getProfile(assignmentType).isAllowMultipleAssignments()) {
            max = region.getUserSettings(assignmentType)
            .getMaximumAssignmentsAllowed();
        } else {
            max = 1;
        }

        Number currentMax = this.getPrimaryDAO().maxAssignmentsInAnyGroup(
            region, assignmentType);

        if ((currentMax != null) && (currentMax.intValue() > max)) {
            throw new BusinessRuleException(
                SelectionErrorCode.REGION_INVALID_MAX_ASSIGNMENTS,
                new UserMessage("region.edit.error.invalidMaxAssignments"));
        }
    }

    /**
     * From EDD - Edit Region: If a user modifies the number of digits required
     * to speak for manual issuance to a value other than all, and region
     * profile is still a manual issuance region, then all assignments with a
     * status of Available or Unavailable must have a Work Identifier value as
     * long as or longer than the number of digits to speak.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateDigitsSpoken(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        validateDigitsSpoken(region, AssignmentType.Normal);
        validateDigitsSpoken(region, AssignmentType.Chase);
    }

    /**
     * Overloaded to check for chase or normal.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @param type - chase or normal
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateDigitsSpoken(SelectionRegion region,
                                        AssignmentType type)
        throws BusinessRuleException, DataAccessException {

        final boolean isManual = !region.getProfile(type).isAutoIssuance();

        if (isManual) {
            int workIdRequestLength = region.getUserSettings(type)
                .getWorkIdentifierRequestLength();

            // Zero digitsSpoken indicates they must speak all, so no
            // need to check
            if (workIdRequestLength != 0) {
                Number minLengthWorkId = this.getPrimaryDAO().minLengthWorkId(
                    region, type);

                if ((minLengthWorkId != null)
                    && (minLengthWorkId.intValue() < workIdRequestLength)) {
                    throw new BusinessRuleException(
                        SelectionErrorCode.REGION_WORK_IDS_TOO_SHORT,
                        new UserMessage(
                            "region.edit.error.workIdsTooShort"));
                }
            }
        }
    }

    /**
     * From EDD - Edit Region: There can be no assignments in the region with a
     * status of In-Progress, Suspended, or Passed.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateAssignmentStatus(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        final int numInProgress = convertNumberToInt(
            getPrimaryDAO().countNumberOfAssignments(region, AssignmentStatus.InProgress));

        final int numSuspended = convertNumberToInt(
            getPrimaryDAO().countNumberOfAssignments(region, AssignmentStatus.Suspended));

        final int numPassed = convertNumberToInt(
            getPrimaryDAO().countNumberOfAssignments(region, AssignmentStatus.Passed));

        if (numInProgress + numSuspended + numPassed > 0) {
            throw new BusinessRuleException(
                SelectionErrorCode.REGION_INVALID_ASSIGNMENT_STATUS,
                new UserMessage("region.edit.error.invalidAssignmentStatus"));

        }
    }

    /**
     * From EDD - Edit Region: There can be no operators signed into the region
     * when editing.
     *
     * @author bnorthrop
     * @param region - to be persisted
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected void validateNoOperatorsSignedIn(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        Number numSignedIn = this.getOperatorDAO().countNumberOfOperatorsSignedIn(
            region);

        if ((numSignedIn != null) && (numSignedIn.intValue() > 0)) {
            throw new BusinessRuleException(
                SelectionErrorCode.REGION_OPERATORS_SIGNED_IN, new UserMessage(
                    "region.edit.error.operatorsSignedIn"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManager#getRegionProfileNumbers(com.vocollect.voicelink.selection.model.AssignmentType, java.lang.Long)
     */
    public Map<String, Long> getRegionProfileNumbers(AssignmentType assignmentType,
        Long selectionRegionID) throws DataAccessException {
        Map<String, Long> profileNumbers = new LinkedHashMap<String, Long>();
        SelectionRegion region = null;
        boolean caseLabelCD = false;
        boolean targetContainers = false;
        boolean profileCaseLabelCD = false;
        boolean profileTargetContainers = false;

        List<SelectionRegionProfile> profiles =
            getSelectionRegionProfileManager().getAll();
        Collections.sort(profiles, PROFILE_NUMBER);

        if (selectionRegionID != null) {
            region = get(selectionRegionID);
            caseLabelCD = region.getProfile(assignmentType)
                .isRequireCaseLabelCheckDigits();
            if (region.getProfile(assignmentType).getContainerType() != null) {
                targetContainers = region.getProfile(assignmentType)
                    .getContainerType().isRequireTargetContainers();
            }
        }
        for (SelectionRegionProfile srp : profiles) {
            profileTargetContainers = false;
            //Profile must have same type
            if (srp.getAssignmentType().equals(assignmentType)) {
                //if no region then adding so return all profile
                //for type specified
                if (region == null) {
                    profileNumbers.put(srp.getNumber(), srp.getId());
                } else {
                    profileCaseLabelCD = srp.isRequireCaseLabelCheckDigits();
                    if (srp.getContainerType() != null) {
                        profileTargetContainers = srp.getContainerType()
                            .isRequireTargetContainers();
                    }
                    //if bothe case label check digit flag and target container
                    //flag match then add profile to list
                    if ((profileCaseLabelCD == caseLabelCD)
                        && (profileTargetContainers == targetContainers)) {
                        profileNumbers.put(srp.getNumber(), srp.getId());
                    }
                }
            }
        }

        return profileNumbers;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(SelectionRegion region)
        throws BusinessRuleException, DataAccessException {

        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the selection task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        } else {

            //If manual issaunce
            if (!region.getProfileNormalAssignment().isAutoIssuance()) {

                //Save current site context filter and turn it off for these
                //queries
                boolean siteContextFilter = SiteContextHolder.getSiteContext().isFilterBySite();
                SiteContextHolder.getSiteContext().setFilterBySite(false);

                int workIdLength =
                    region.getUserSettingsNormal().getWorkIdentifierRequestLength();
                if (workIdLength == 0) {
                    getPrimaryDAO().updatePartialWorkIdAll(region);
                } else {
                    getPrimaryDAO().updateCustomPartialWorkIdPartial(
                        workIdLength, workIdLength, region.getId());
                }

                //reset site context filter
                SiteContextHolder.getSiteContext()
                    .setFilterBySite(siteContextFilter);
            }
        }

        //Set Number of assignments allowed to 1 if allow multiple is no
        if (!region.getProfileNormalAssignment().isAllowMultipleAssignments()) {
            region.getUserSettingsNormal().setMaximumAssignmentsAllowed(1);
        }
        if (!region.getProfileChaseAssignment().isAllowMultipleAssignments()) {
            region.getUserSettingsChase().setMaximumAssignmentsAllowed(1);
        }

        return super.save(region);
    }

    /**
     * Take the passed in Selection Region and add it to all the
     * workgroups where AutoAddRegions is turned on and the task
     * function is a selection task function.
     * @param region - a selection region
     * @throws DataAccessException - any database exception
     */
    protected void addToWorkgroups(SelectionRegion region)
        throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.Selection) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        SelectionRegion selectionRegion = get(id);
        return delete(selectionRegion);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(SelectionRegion instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("selectionRegion.delete.error.inUse");
            } else {
                throw ex;
        }
    }
        return null;
    }


    /**
     * {@inheritDoc}
     */
    public SelectionRegion findRegionByNumber(int regionNumber)
                            throws DataAccessException, BusinessRuleException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listAssignmentSummary()
     */
    public List<DataObject> listAssignmentSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getSelectionSummaryDAO().listAssignmentSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            SelectionSummary summaryObject = new SelectionSummary();

            summaryObject.setRegion((SelectionRegion) objArray[REGION]);
            summaryObject.setTotalAssignments(convertNumberToInt(objArray[TOTAL_ASSIGNMENTS]));
            summaryObject.setInProgress(convertNumberToInt(objArray[INPROGRESS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setComplete(convertNumberToInt(objArray[COMPLETE]));
            summaryObject.setNonComplete(convertNumberToInt(objArray[NON_COMPLETE]));
            summaryObject.setSite((Site) objArray[ASSIGN_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            newList.add(summaryObject);
        }
        return newList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listCurrentWorkSummary()
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getSelectionSummaryDAO().listCurrentWorkSummary((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            SelectionSummary summaryObject = new SelectionSummary();

            summaryObject.setRegion((SelectionRegion) objArray[REGION]);
            summaryObject.setOperatorsWorkingIn(convertNumberToInt(objArray[OPERATOR_IN_REGION]));
            summaryObject.setOperatorsAssigned(convertNumberToInt(objArray[OPERATOR_ASSIGNED]));
            summaryObject.setTotalItemsPicked(convertNumberToInt(objArray[TOTAL_ITEMS_PICKED]));
            summaryObject.setTotalItemsRemaining(convertNumberToInt(objArray[TOTAL_ITEMS_REMAINING]));
            summaryObject.setSite((Site) objArray[CURRENT_WORK_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            Double estimatedCompleted = 0.0;

            if ((summaryObject.getRegion().getGoalRate() != 0)
                && (summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn() != 0)) {

                Double totalRemain = new Double(summaryObject.getTotalItemsRemaining());
                Double operators = new Double(summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn());
                Double goalRate = new Double(summaryObject.getRegion().getGoalRate());

                estimatedCompleted = totalRemain / goalRate / operators;
            }

            //Round to 2 decimal places and set property.
            summaryObject.setEstimatedCompleted(new BigDecimal(estimatedCompleted)
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            newList.add(summaryObject);
        }
        return newList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listShortsSummary()
     */
    public List<DataObject> listShortsSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getSelectionSummaryDAO().listShortsSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            SelectionSummary summaryObject = new SelectionSummary();

            summaryObject.setRegion((SelectionRegion) objArray[REGION]);
            summaryObject.setTotalShorts(convertNumberToInt(objArray[TOTAL_SHORTS]));
            summaryObject.setShorted(convertNumberToInt(objArray[SHORTED]));
            summaryObject.setAssigned(convertNumberToInt(objArray[ASSIGNED]));
            summaryObject.setMarkedout(convertNumberToInt(objArray[MARKEDOUT]));
            summaryObject.setSite((Site) objArray[SHORT_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());
            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listAllRegionsOrderByNumber()
     */
    @Override
    public List<SelectionRegion> listAllRegionsOrderByNumber()
        throws DataAccessException {
        return this.getPrimaryDAO().listAllRegionsOrderByNumber();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 