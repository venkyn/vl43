/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.selection.dao.AssignmentLaborDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>AssignmentLabor</code> model object.
 *
 * @author pfunyak
 */
public abstract class AssignmentLaborManagerImplRoot extends 
     GenericManagerImpl<AssignmentLabor, AssignmentLaborDAO> 
    implements AssignmentLaborManager {

    private static final Double ONE_HUNDRED = 100.0;
    
    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.0;
    
    private PickDetailManager    pickDetailManager;

    /**
     * Getter for the pickDetailManager property.
     * @return PickDetailManager value of the property
     */
    public PickDetailManager getPickDetailManager() {
        return this.pickDetailManager;
    }

    
    /**
     * Setter for the pickDetailManager property.
     * @param pickDetailManager the new pickDetailManager value
     */
    public void setPickDetailManager(PickDetailManager pickDetailManager) {
        this.pickDetailManager = pickDetailManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public AssignmentLaborManagerImplRoot(AssignmentLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#openLaborRecord(java.util.Date, com.vocollect.voicelink.core.model.Operator, com.vocollect.voicelink.selection.model.Assignment, com.vocollect.voicelink.core.model.OperatorLabor)
     */
    public void openLaborRecord(Date startTime,
                                Operator operator, 
                                Assignment assignment,
                                OperatorLabor operLaborRecord)
                 throws DataAccessException, BusinessRuleException {
        
        
        // close any open labor records for this assignment.
        // this is to handle the rare case of where the operator gets out of 
        // radio range and get assignment is called for a second (or more) time.
        this.closeLaborRecord(startTime, assignment);
        
        AssignmentLabor assignmentLabor = new AssignmentLabor();
        assignmentLabor.setAssignment(assignment);
        assignmentLabor.setOperator(operator);
        assignmentLabor.setStartTime(startTime);
        assignmentLabor.setOperatorLabor(operLaborRecord);
        assignmentLabor.setEndTime(null);
        assignmentLabor.setDuration(new Long(0));
        assignmentLabor.setQuantityPicked(new Integer(0));
        assignmentLabor.setGroupCount(assignment.getGroupInfo().getGroupCount());
        assignmentLabor.setGroupNumber(assignment.getGroupInfo().getGroupNumber());
        assignmentLabor.setAssignmentProrateCount(new Integer(0));
        assignmentLabor.setGroupProrateCount(new Integer(0));
        assignmentLabor.setActualRate(new Double(0.0));
        assignmentLabor.setPercentOfGoal(new Double(0.0));
        this.save(assignmentLabor);
      
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManagerRoot#recalculateAggregatesAndSave(com.vocollect.voicelink.selection.model.AssignmentLabor)
     */
    public void recalculateAggregatesAndSave(AssignmentLabor al) throws DataAccessException, BusinessRuleException {
        PickDetailManager pdManager = this.getPickDetailManager();
        Date startTime = al.getStartTime();
        Date endTime = al.getEndTime();
        al.setDuration(endTime.getTime() - startTime.getTime());
        al.setQuantityPicked(pdManager.sumQuantityPicked(al.getAssignment()
            .getId(), startTime, endTime));
        al.setAssignmentProrateCount(pdManager.sumAssignmentProrateCount(al
            .getAssignment().getId(), startTime, endTime));

        if (al.getAssignment().getGroupInfo().getGroupNumber() != null
            && al.getAssignment().getOperator() != null) {

            al.setGroupProrateCount(pdManager.sumGroupProrateCount(al
                .getAssignment().getGroupInfo().getGroupNumber(), al
                .getAssignment().getOperator().getId(), startTime, endTime));
        }

        if (al.getDuration() > 0) {
            al.setActualRate(new Double(al.getQuantityPicked()
                / (al.getDuration() / MILLISEC_CONVERSION_FACTOR)));
            al.setPercentOfGoal(new Double(al.getActualRate()
                / (al.getAssignment().getRegion().getGoalRate()) * ONE_HUNDRED));
        }
        this.save(al);
    }
 

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#closeLaborRecord(java.util.Date, com.vocollect.voicelink.selection.model.Assignment)
     */
    public AssignmentLabor closeLaborRecord(Date endTime, Assignment assignment)
                throws DataAccessException, BusinessRuleException {
    
        long assignmentId = assignment.getId();
        AssignmentLabor al = this.findOpenRecordByAssignmentId(assignmentId);
 
        // throw exception if already closed ????
        if (al != null) {
            PickDetailManager pdManager = this.getPickDetailManager();
            Date startTime = al.getStartTime();
            al.setEndTime(endTime);
            al.setDuration(new Long(endTime.getTime() - startTime.getTime()));
            al.setQuantityPicked(pdManager.sumQuantityPicked(assignmentId, startTime, endTime));
            al.setAssignmentProrateCount(
                pdManager.sumAssignmentProrateCount(assignmentId, startTime, endTime));
            al.setGroupProrateCount(pdManager.sumGroupProrateCount(assignment
                .getGroupInfo().getGroupNumber(), assignment.getOperator()
                .getId(), startTime, endTime));

            if (al.getDuration() > 0) {
                al.setActualRate(new Double(al.getQuantityPicked() / (al.getDuration() 
                                                                   / MILLISEC_CONVERSION_FACTOR)));
                al.setPercentOfGoal(new Double(al.getActualRate() 
                    / (assignment.getRegion().getGoalRate()) * ONE_HUNDRED));
            }
            this.save(al);
        }
        return al;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#findOpenRecordByAssignmentId(long)
     */
    public AssignmentLabor findOpenRecordByAssignmentId(long assignmentId) throws DataAccessException {
        return getPrimaryDAO().findOpenRecordByAssignmentId(assignmentId);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#listOpenRecordsByOperatorId(long)
     */
    public List<AssignmentLabor> listOpenRecordsByOperatorId(long operatorId) throws DataAccessException {
        return getPrimaryDAO().listOpenRecordsByOperatorId(operatorId);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#listOpenRecordsByOperatorLaborId(long)
     */
    public List<AssignmentLabor> listClosedRecordsByBreakLaborId(long breakLaborId) throws DataAccessException {
        return getPrimaryDAO().listClosedRecordsByBreakLaborId(breakLaborId);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#listAllRecordsByOperatorLaborId(long)
     */
    public List<AssignmentLabor> listAllRecordsByOperatorLaborId(long operatorLaborId) throws DataAccessException {
        return getPrimaryDAO().listAllRecordsByOperatorLaborId(operatorLaborId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {        
        return this.getPrimaryDAO().listAssignmentLabor(new QueryDecorator(rdi));
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborDAO#sumQuantityPicked(long)
     */
    public Integer sumQuantityPicked(long operatorLaborId) throws DataAccessException {
        
        Integer result = getPrimaryDAO().sumQuantityPicked(operatorLaborId);
        
        if (result == null) {
            result =  new Integer(0);
        }
        return result;
    }
    
    /**
     * 
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    public List<Object[]> listLaborSummaryReportRecords(Long operatorId, 
                                                       Date startDate, Date endDate) 
                                                       throws DataAccessException {

        return this.getPrimaryDAO().listLaborSummaryReportRecords(operatorId, startDate, endDate);
    }


    /**
     * 
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    public List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                                       Date startDate, Date endDate) 
                                                       throws DataAccessException {

        return this.getPrimaryDAO().listLaborDetailReportRecords(operatorId, startDate, endDate);
    }
    
    //Methods in Data Aggregators
    //Used in infinite region aggregator for hours remaining calculation
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManagerRoot#listLaborActualRateForAllRegions()
     */
    @Override
    public List<Object[]> listLaborActualRateForAllRegions() throws DataAccessException {
        return this.getPrimaryDAO().listLaborActualRateForAllRegions();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 