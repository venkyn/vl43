package com.vocollect.voicelink.selection.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSourceProvider;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.selection.model.SelectionAssignmentLaborReport;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;

public class SelectionAssignmentLaborReportImplRoot extends
        JRAbstractBeanDataSourceProvider implements ReportDataSourceManager {
 
    
    

    public SelectionAssignmentLaborReportImplRoot(Class<SelectionAssignmentLaborReport> beanClass) {
        super(SelectionAssignmentLaborReport.class);
        // TODO Auto-generated constructor stub
    }

    public SelectionAssignmentLaborReportImplRoot() {
        super(SelectionAssignmentLaborReport.class);
        // TODO Auto-generated constructor stub
    }

    private ArchiveAssignmentLaborManager archiveAssignmentLaborManager;



    /* (non-Javadoc)
     * @see com.vocollect.voicelink.selection.service.impl.SelectionAssignmentLaborReportRoot#getDataSource(java.util.Map)
     */
    @Override
    public JRDataSource getDataSource(Map<String, Object> values)
            throws Exception {
        // Parameter values
        Date startDate = (Date) values.get("START_DATE");
        Date endDate = (Date) values.get("END_DATE");
        String siteId = (String) values.get("SITE");
        @SuppressWarnings("unchecked")
        Set<String> operatorIdentifiers = (Set<String>) values
            .get(ReportUtilities.OPERATOR_IDENTIFIERS_ALL);
        if (StringUtil.isNullOrEmpty((String) values
            .get(ReportUtilities.OPERATOR_ALL))) {
            operatorIdentifiers = null;
        }

        List<Object []> selectionAssignmentLaborReportRecords = getArchiveAssignmentLaborManager()
            .listSelectionAssignmentLaborReportRecords(new QueryDecorator(),
                                  operatorIdentifiers, startDate, endDate, Long.parseLong(siteId));
        return new JRBeanCollectionDataSource(selectionAssignmentLaborReportRecords);
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.selection.service.impl.SelectionAssignmentLaborReportRoot#create(net.sf.jasperreports.engine.JasperReport)
     */
    @Override
    public JRDataSource create(JasperReport arg0) throws JRException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.selection.service.impl.SelectionAssignmentLaborReportRoot#dispose(net.sf.jasperreports.engine.JRDataSource)
     */
    @Override
    public void dispose(JRDataSource arg0) throws JRException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.selection.service.impl.SelectionAssignmentLaborReportRoot#setArchiveAssignmentLaborManager(com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager)
     */
    public void setArchiveAssignmentLaborManager(
            ArchiveAssignmentLaborManager archiveAssignmentLaborManager) {
        this.archiveAssignmentLaborManager = archiveAssignmentLaborManager;
    }

    /* (non-Javadoc)
     * @see com.vocollect.voicelink.selection.service.impl.SelectionAssignmentLaborReportRoot#getArchiveAssignmentLaborManager()
     */
    public ArchiveAssignmentLaborManager getArchiveAssignmentLaborManager() {
        return archiveAssignmentLaborManager;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 