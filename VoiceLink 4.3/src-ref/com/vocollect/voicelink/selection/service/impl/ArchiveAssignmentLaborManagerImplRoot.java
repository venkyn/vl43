/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.selection.dao.ArchiveAssignmentLaborDAO;
import com.vocollect.voicelink.selection.model.ArchiveAssignmentLabor;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Additional service methods for the <code>Assignment</code> model object.
 * 
 * @author ddoubleday
 */
public class ArchiveAssignmentLaborManagerImplRoot extends
    GenericManagerImpl<ArchiveAssignmentLabor, ArchiveAssignmentLaborDAO> implements
    ArchiveAssignmentLaborManager {

    /**
     * Passes the ArchiveAssignmentSAO as the primaryDAO. Constructor.
     * @param primaryDAO ArchiveAssignmentDAO object
     */
    public ArchiveAssignmentLaborManagerImplRoot(ArchiveAssignmentLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * 
     * @param operatorIdentifier  - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    public List<Object[]> listLaborDetailReportRecords(String operatorIdentifier,
                                                       Date startDate, Date endDate) 
                                                       throws DataAccessException {
        return this.getPrimaryDAO().listLaborDetailReportRecords(operatorIdentifier, startDate, endDate);
    }
    
    /**
     * 
     * @param operatorIdentifier  - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    public List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                       Date startDate, Date endDate) 
                                                       throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(operatorIdentifier, startDate, endDate);
    }

    /**
     * 
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects 
     * @throws DataAccessException - on db failure
     */
    public List<Object[]> listLaborDurationByOperRegionDates(String operatorIdentifier, Integer regionNumber, 
                                                                 Date startDate, Date endDate)
                                                                 throws DataAccessException {
       
        return this.getPrimaryDAO().listLaborDurationByOperRegionDates(operatorIdentifier, regionNumber, 
                                                                      startDate, endDate);
    }  


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.dao.ArchiveAssignmentLaborDAO#sumQuantityPickedBetweenDates(long, date, date)
     */
    public Number sumQuantityPickedBetweenDates(Long operatorLaborId, Date startTime, Date endTime)
                                                 throws DataAccessException {
        return this.getPrimaryDAO().sumQuantityPickedBetweenDates(operatorLaborId, startTime, endTime); 
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveAssignmentLaborManagerRoot#listSelectionAssignmentLaborReportRecords(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Set, java.util.Date, java.util.Date, java.lang.Long)
     */
    public List<Object[]> listSelectionAssignmentLaborReportRecords(QueryDecorator queryDecorator,
                                                       Set<String> operatorIdentifiers,
                                                       Date startDate, Date endDate, Long siteId) 

        throws DataAccessException {
            WhereClause whereClause = new WhereClause();

            whereClause.add(operatorIdentifiers, "al.operatorIdentifier");
            if (!whereClause.toString().equalsIgnoreCase("")) {
                queryDecorator.setWhereClause(whereClause.toString());
            }

            return this.getPrimaryDAO().
                listSelectionAssignmentLaborReportRecords(queryDecorator, 
                                                         startDate, endDate, siteId);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 