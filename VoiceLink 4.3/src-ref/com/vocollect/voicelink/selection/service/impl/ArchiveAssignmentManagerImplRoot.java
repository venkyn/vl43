/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.selection.dao.ArchiveAssignmentDAO;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author ddoubleday
 */
public class ArchiveAssignmentManagerImplRoot extends
    GenericManagerImpl<ArchiveAssignment, ArchiveAssignmentDAO> implements
    ArchiveAssignmentManager {

    private AssignmentManager assignmentManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Passes the ArchiveAssignmentSAO as the primaryDAO. Constructor.
     * @param primaryDAO ArchiveAssignmentDAO object
     */
    public ArchiveAssignmentManagerImplRoot(ArchiveAssignmentDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveAssignmentManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.util.Date,
     *      com.vocollect.voicelink.selection.model.AssignmentStatus)
     */
    public Integer updateOlderThan(Date createdDate, AssignmentStatus status)
        throws DataAccessException {
        return getPrimaryDAO().updateOlderThan(createdDate, status);
    }

    /**
     * Implementation for Assignment Report to extract Archive Assignments.
     *
     * @param queryDecorator - the decorator to contain the where clause
     * @param workId - WorkID of the assignment.
     * @param operatorId - Set of Operator ID.
     * @param assignmentNumber - Assignment Number.
     * @param customerNumber - Customer Number.
     * @param route - Route of the Assignment.
     * @param startTime - Start Time of the Report.
     * @param endTime - End Time of the Report.
     * @param siteId - Site in which the Report is being generated.
     * @return List<ArchiveAssignment> - List of Archive Assignments.
     * @throws DataAccessException
     *
     *             {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveAssignmentManagerRoot#listArchiveAssignmentsforAssignmentReport(java.lang.String,
     *      java.lang.String, java.lang.Long, java.lang.String,
     *      java.lang.String, java.util.Date, java.util.Date, java.lang.Long)
     */
    public List<AssignmentReport> listArchiveAssignmentsForAssignmentReport(QueryDecorator queryDecorator,
                                                                             String workId,
                                                                             Set<String> operatorId,
                                                                             Long assignmentNumber,
                                                                             String customerNumber,
                                                                             String route,
                                                                             Date startTime,
                                                                             Date endTime,
                                                                             Long siteId)
        throws DataAccessException {
        WhereClause whereClause = new WhereClause();
        whereClause.add(workId, "aa.workIdentifier.workIdentifierValue");
        whereClause.add(assignmentNumber, "aa.number");
        whereClause.add(customerNumber, "aa.customerInfo.customerNumber");
        whereClause.add(route, "aa.route");
        whereClause.add(operatorId, "aa.operatorIdentifier");
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }
        return getPrimaryDAO().listArchiveAssignmentsForAssignmentReport(
            queryDecorator, startTime, endTime, siteId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveAssignmentManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.selection.model.AssignmentStatus,
     *      java.util.Date)
     */
    public int executePurge(AssignmentStatus status, Date olderThan) {
        int returnRecords = 0;

        // Get List of Archive Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Fetching Archive Assignments to Purge :::");
        }
        try {
            returnRecords = updateOlderThan(olderThan, status);
        } catch (DataAccessException e) {
            log.warn("Error getting Archive "
                + "Assignments from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Archive Assignments for Purge :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 