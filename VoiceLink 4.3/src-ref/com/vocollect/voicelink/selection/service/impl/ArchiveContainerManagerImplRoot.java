/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.selection.dao.ArchiveContainerDAO;
import com.vocollect.voicelink.selection.model.ArchiveContainer;
import com.vocollect.voicelink.selection.model.ContainerReport;
import com.vocollect.voicelink.selection.service.ArchiveContainerManager;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 *
 *
 * @author mraj
 */
public class ArchiveContainerManagerImplRoot extends
    GenericManagerImpl<ArchiveContainer, ArchiveContainerDAO> implements
    ArchiveContainerManager {

    /**
     *
     * Passes the ArchiveContainerDAO as the primaryDAO. Constructor.
     * @param primaryDAO ArchiveContainerDAO object
     */
    public ArchiveContainerManagerImplRoot(ArchiveContainerDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveContainerManagerRoot#listArchiveContainersForContainerReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.lang.String, java.util.Set, java.util.Date, java.util.Date, java.lang.Long)
     */
    public List<ContainerReport> listArchiveContainersForContainerReport(QueryDecorator queryDecorator,
                                                                          String containerNumber,
                                                                          Set<String> operatorIds,
                                                                          Date startDate,
                                                                          Date endDate,
                                                                          Long siteId)
        throws DataAccessException {

        WhereClause whereClause = new WhereClause();
        whereClause.add(containerNumber, "ac.containerNumber");
        whereClause.add(operatorIds, "pd.operatorIdentifier");
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }

        return getPrimaryDAO().listArchiveContainersForContainerReport(
            queryDecorator, startDate, endDate, siteId);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 