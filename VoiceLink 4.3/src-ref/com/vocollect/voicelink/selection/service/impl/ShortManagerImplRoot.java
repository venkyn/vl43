/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.selection.dao.PickDAO;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.ShortManager;

import java.util.List;


/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author ddoubleday
 */
public abstract class ShortManagerImplRoot extends
    PickManagerImpl implements ShortManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ShortManagerImplRoot(PickDAO primaryDAO) {
        super(primaryDAO);
    }

    /** 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return getPrimaryDAO().listShorts(new QueryDecorator(rdi));
    }       
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.service.ShortManager#executeMexecuteAcceptQuantityPicked(Pick)
     */
    public void executeMarkout(Pick pick) throws BusinessRuleException, DataAccessException {
        pick.markoutPick();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.service.ShortManager#executeAcceptQuantityPicked(Pick)
     */
    public void executeAcceptQuantityPicked(Pick pick) throws BusinessRuleException, DataAccessException {
        pick.acceptQuantityPicked();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 