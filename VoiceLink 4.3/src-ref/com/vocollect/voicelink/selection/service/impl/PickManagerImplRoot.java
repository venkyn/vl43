/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.lineloading.model.CartonType;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.dao.PickDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.selection.service.PickManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author ddoubleday
 */
/**
 * @author krishna.udupi
 *
 */
public abstract class PickManagerImplRoot extends
    GenericManagerImpl<Pick, PickDAO> implements PickManager {

    private CartonManager       cartonManager;
    
    
    
    /**
     * Getter for the cartonManager property.
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return cartonManager;
    }

    
    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PickManagerImplRoot(PickDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * Implementation to get list of groups' picks.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManager
     * #listGroupsPicks(java.lang.String)
     */
    public List<Pick> listGroupsPicks(Long groupNumber) 
        throws DataAccessException {
        return getPrimaryDAO().listGroupsPicks(groupNumber);
    }

    /**
     * Manual pick validation business logic.
     * @param p The pick.
     * @throws BusinessRuleException on business rule violation.
     */
    public void validateIfManualPickAllowed(Pick p)
        throws BusinessRuleException {
        Assignment a = p.getAssignment();
        String assignmentStatus = a.getStatus().toString();

        if (!assignmentStatus.equals("Suspended")) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_INVALID_ASSIGNMENT_STATUS,
                new UserMessage(
                    "pick.manualpick.assignmentSuspended.invalid"));
        }
        if (a.getOperator() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_OPERATOR_ASSIGNED,
                new UserMessage("pick.manualpick.operatorAssigned.invalid"));            
        }
        if (!p.getStatus().isInSet(
            PickStatus.BaseItem, PickStatus.NotPicked,
            PickStatus.Skipped, PickStatus.Partial)) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_INVALID_PICK_STATUS,
                new UserMessage(
                    "pick.manualpick.invalidPickStatus.invalid"));
        }        
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#listShortsForLocation(com.vocollect.voicelink.core.model.Location)
     */
    public List<Pick> listShortsForLocation(Location location) 
    throws DataAccessException {
        return getPrimaryDAO().listShortsForLocation(location);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#listShortsForLocationAndItem(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public List<Pick> listShortsForLocationAndItem(Location location, Item item) 
    throws DataAccessException {
        return getPrimaryDAO().listShortsForLocationAndItem(location, item);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#autoMarkOutShorts(com.vocollect.voicelink.core.model.Location, com.vocollect.voicelink.core.model.Item)
     */
    public void executeAutoMarkOutShorts(Location location, Item item)
    throws BusinessRuleException, DataAccessException {
        List<Pick> picks = null;
        
        if (location.getItems().size() == 0) {
            picks = listShortsForLocation(location); 
        } else {
            picks = listShortsForLocationAndItem(location, item);
        }
        
        for (Pick p : picks) {
            p.markoutPick();
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#executeUpdateLineLoading(com.vocollect.voicelink.selection.model.Pick)
     */
    public void executeUpdateLineLoading(Pick p) 
    throws DataAccessException {

        //if carton is not specified return
        if (StringUtil.isNullOrEmpty(p.getCartonNumber())) {
            return;
        }
        
        //Update for case
        getCartonManager().executeUpdatePickStatus(
            p.getCartonNumber(), 
            CartonType.Case, 
            p.getQuantityPicked());
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#executeCancelPick(com.vocollect.voicelink.selection.model.Pick)
     */
    public void executeCancelPick(Pick p) 
    throws BusinessRuleException, DataAccessException {
        p.cancelPick();
        executeUpdateLineLoading(p);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#executeManualPick(com.vocollect.voicelink.selection.model.Pick, java.lang.Integer)
     */
    public void executeManualPick(Pick p, Integer qty) 
    throws BusinessRuleException, DataAccessException {
        p.updateManualPick(qty);
        executeUpdateLineLoading(p);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#listShortedLocations(java.util.Date, java.util.Date)
     */
    public List<Object[]> listShortedLocations(Date startTime, Date endTime)
        throws DataAccessException {
        return getPrimaryDAO().listShortedLocations(startTime, endTime);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickManagerRoot#listSlotVisitLocations(java.util.Date, java.util.Date)
     */
    public List<Object[]> listSlotVisitLocations(Date startTime, Date endTime)
    throws DataAccessException {
       return getPrimaryDAO().listSlotVisitLocations(startTime, endTime);
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 