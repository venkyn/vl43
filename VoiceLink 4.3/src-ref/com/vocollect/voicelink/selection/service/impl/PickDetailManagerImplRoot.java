/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.selection.dao.PickDetailDAO;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import java.util.Date;
import java.util.List;


/**
 * Additional service methods for the <code>PickDetail</code> model object.
 *
 * @author pfunyak
 */
public abstract class PickDetailManagerImplRoot extends
    GenericManagerImpl<PickDetail, PickDetailDAO> implements PickDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PickDetailManagerImplRoot(PickDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickDetailManager#sumQuantityPicked(com.vocollect.voicelink.selection.model.Assignment, java.util.Date, java.util.Date)
     */
    public int sumQuantityPicked(long assignmentId, Date startTime, Date endTime)
                      throws DataAccessException {
        
        return convertNumberToInt(
            getPrimaryDAO().sumQuantityPicked(assignmentId, startTime, endTime));
          
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickDetailManager#sumAssignmentProrateCount(com.vocollect.voicelink.selection.model.Assignment, java.util.Date, java.util.Date)
     */
    public int sumAssignmentProrateCount(long assignmentId, Date startTime, Date endTime)
                                             throws DataAccessException {
        
        return convertNumberToInt(
            getPrimaryDAO().sumAssignmentProrateCount(assignmentId, startTime, endTime));
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentLaborManager#sumGroupProrateCount(long, long)
     */
    public int sumGroupProrateCount(long groupNumber, long operatorId, Date startTime, Date endTime)
                                        throws DataAccessException {
        
        return convertNumberToInt(
            getPrimaryDAO().sumGroupProrateCount(groupNumber, operatorId, startTime, endTime));
    }
    
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {        
            return this.getPrimaryDAO().
                listPickDetails(new QueryDecorator(rdi));
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickDetailManagerRoot#maxEndTime(long, java.util.Date)
     */
      public Date maxEndTime(long operatorId, Date minTime, Date maxTime) throws DataAccessException {
          return this.getPrimaryDAO().maxEndTime(operatorId, minTime, maxTime);
    }
      
      /**
       * 
       * {@inheritDoc}
       * @see com.vocollect.voicelink.selection.service.PickDetailManagerRoot#minPickTime(long, java.util.Date)
       */
        public Date minPickTime(long operatorId, Date maxTime) throws DataAccessException {
            return this.getPrimaryDAO().minPickTime(operatorId, maxTime);
      }      
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 