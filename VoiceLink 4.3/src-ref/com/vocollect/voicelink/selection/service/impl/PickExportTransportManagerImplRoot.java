/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.selection.dao.PickDAO;
import com.vocollect.voicelink.selection.service.PickExportTransportManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author dgold
 *
 */
public abstract class PickExportTransportManagerImplRoot
    extends PickManagerImpl
    implements PickExportTransportManager {

    private static final long MILLISECS_PER_SEC = 1000;

    private static final long FIVE_HUNDRED_MILLISECS = 500;

    //Number of min digits allowed for decimal places
    private static final int MIN_FRAC_DIGITS = 2;

    //Number of max digits allowed for decimal places
    private static final int MAX_FRAC_DIGITS = 2;

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PickExportTransportManagerImplRoot(PickDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * {@inheritDoc}
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @throws DataAccessException
     */
    public List<Map<String, Object>> listCompletedPicks()
    throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        return formatDatesInListOfMap(getPrimaryDAO().listCompletedPicks(qd));
   }

    /**
     * @return list of maps.  Each map contains the fields for a picks file picked record,
     *         the object, and the matchingStatus.
     * @throws DataAccessException .
     */
    public List<Map<String, Object>> listPickedPicks()
    throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        return formatWeightInListOfMap(formatDatesInListOfMap(getPrimaryDAO().listPickedPicks(qd)));
    }

    /**
     * listExportFinish implementation.
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickExportTransportManagerRoot#listExportFinish()
     */
    public List<Map<String, Object>> listExportFinish() throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        return getPrimaryDAO().listExportFinish(qd);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.LaborExportTransportManager#listExportAssignmentLabor()
     */
    public List<Map<String, Object>> listExportAssignmentLabor() throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);
        List<Map<String, Object>> rawData = this.getPrimaryDAO().listExportLaborRecords(qd);

        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map<String, Object> m : rawData) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if (null != myObj) {
                    if (myObj instanceof java.util.Date) {
                        // It is a date!
                        formattedDateTime = sdf.format(myObj);
                        // Replace the key and date with key and formatted
                        // string
                        m.put(keyObj, formattedDateTime);
                    } // end if class is date
                    if (keyObj.equals("TotalPickTime")) {
                        Long totalSeconds = (Long) myObj;
                        Long seconds = new Long(totalSeconds
                            / MILLISECS_PER_SEC);
                        totalSeconds = totalSeconds % MILLISECS_PER_SEC;
                        if (totalSeconds >= FIVE_HUNDRED_MILLISECS) {
                            seconds++;
                        }
                        m.put(keyObj, seconds);

                    }
                } // end for obj
            }
        } //end for map
        return rawData;
    }


    /**
     * Formats the date.
     * @param inputList in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>> formatDatesInListOfMap(List<Map<String, Object>> inputList) {
        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map <String, Object> m : inputList) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if ((myObj != null) && (myObj instanceof java.util.Date)) {
                    // It is a date!
                    formattedDateTime = sdf.format(myObj);
                    // Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } // end if class is date
            } // end for obj

        } //end for map
        return inputList;
    }


    /**
     * This method is to format the variable weight upto two decimal places.
     * @param inputList in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>>
        formatWeightInListOfMap(List<Map<String, Object>> inputList) {

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(MAX_FRAC_DIGITS);
        df.setMinimumFractionDigits(MIN_FRAC_DIGITS);
        df.setDecimalSeparatorAlwaysShown(false);
        df.setGroupingUsed(false);

        //Loop through data, find the variable weight and format it.
        for (Map <String, Object> m : inputList) {
            Iterator<Map.Entry<String, Object>> it = m.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, Object> pairs = it.next();
                if (pairs.getKey().equals("RecordedVariableWeight")) {
                   //Format it
                    if (pairs.getValue() != null) {
                        Double d1 = (Double) pairs.getValue();
                        String s1 = df.format(d1);
                        pairs.setValue(s1);
                    }
                    break;
                }
            }
        } //end for map
        return inputList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickExportTransportManagerRoot#updatePickedPicks()
     */
    public Integer updatePickedPicks() throws DataAccessException {
        boolean siteFilter = SiteContextHolder.getSiteContext().isFilterBySite();
        SiteContextHolder.getSiteContext().setFilterBySite(false);
        Integer i;
        try {
            i = getPrimaryDAO().updatePickedPicks();
        } finally {
            SiteContextHolder.getSiteContext().setFilterBySite(siteFilter);
        }
        return i;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 