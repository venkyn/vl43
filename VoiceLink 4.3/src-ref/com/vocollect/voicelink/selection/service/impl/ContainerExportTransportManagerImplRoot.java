/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.selection.dao.ContainerDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.selection.service.ContainerExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dgold
 *
 */
public abstract class ContainerExportTransportManagerImplRoot extends ContainerManagerImpl
        implements ContainerExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * @param primaryDAO the DAO for this manager.
     */
    public ContainerExportTransportManagerImplRoot(ContainerDAO primaryDAO) {
        super(primaryDAO);
        // Nothing special here
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerExportTransportManagerRoot#updateClosedContainers()
     */
    public Integer updateClosedContainers() throws DataAccessException {
        boolean siteFilter = SiteContextHolder.getSiteContext().isFilterBySite();
        SiteContextHolder.getSiteContext().setFilterBySite(false);

        Integer i;
        try {
            i = getPrimaryDAO().updateClosedContainers();
        } finally {
            SiteContextHolder.getSiteContext().setFilterBySite(siteFilter);
        }
        return i;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerExportTransportManager#listClosedContainers()
     */
    public List<Map<String, Object>> listClosedContainers() throws DataAccessException {
        List<Map<String, Object>> finishedData = new ArrayList<Map<String, Object>>();

        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Container> rawData = this.getPrimaryDAO().listClosedContainers(qd);
        Assignment a = null;
        Pick p = null;

        //Stuff to format timestamps
        String formattedDeliveryDate = "";
        String formattedExportTime = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);

        for (Container container : rawData) {
            List<PickDetail> pickDetails = container.getPickDetails();
            // This will count down to zero so we can find the last one.
            int i = pickDetails.size();
            for (PickDetail pd : container.getPickDetails()) {
                Map<String, Object> mirror = new HashMap<String, Object>();
                p = pd.getPick();
                mirror.put("QuantityPut", pd.getQuantityPicked());
                if (p != null) {
                    mirror.put("Item", p.getItem().getNumber());
                    mirror.put("LocationID", p.getLocation().getScannedVerification());
                    a = p.getAssignment();
                    if (a != null) {
                        //Format the Delivery Date
                        if (a.getDeliveryDate() == null) {
                            // If there is no Delivery Date, we will put "No Timestamp"
                            formattedDeliveryDate = "No Timestamp";
                        } else {
                            formattedDeliveryDate = sdf.format(a.getDeliveryDate());
                        }
                        mirror.put("DeliveryDate", formattedDeliveryDate);
                        mirror.put("AssignmentNumber", a.getNumber());
                        mirror.put("WorkIdentifier", a.getWorkIdentifier().getWorkIdentifierValue());
                    }
                }
                mirror.put("RecordType", "CONTAINER");

                //Format the ExportTime
                formattedExportTime = sdf.format(Calendar.getInstance().getTime());
                mirror.put("ExportTime", formattedExportTime);
                mirror.put("ContainerIdentifier", container.getContainerNumber());
                mirror.put("Status", "CLOSED");
                // Countdown to zero
                i--;
                if (i == 0) {
                    // last PickDetail for this container, add the Container only
                    // only to this record so its status doesn't get updated
                    // redundantly every time a pick detail is processed.
                    mirror.put("source", container);
                }
                finishedData.add(mirror);
            }

        }

        return finishedData;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 