/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.selection.dao.PickImportDataDAO;
import com.vocollect.voicelink.selection.model.PickImportData;
import com.vocollect.voicelink.selection.service.PickImportDataManager;



/**
 * Standard implementation of the Importable Pick data manager implementation.
 * 
 * @author dgold
 *
 */
public abstract class PickImportDataManagerImplRoot 
     extends GenericManagerImpl<PickImportData, PickImportDataDAO>
        implements PickImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PickImportDataManagerImplRoot(PickImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 