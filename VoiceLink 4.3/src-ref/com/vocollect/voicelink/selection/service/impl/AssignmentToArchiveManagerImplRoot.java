/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentToArchiveManager;

import java.util.Date;
import java.util.List;

/**
 * 
 *
 * @author mkoenig
 */
public class AssignmentToArchiveManagerImplRoot implements
    AssignmentToArchiveManager {

    private AssignmentManager assignmentManager;

    private ArchiveAssignmentManager archiveAssignmentManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Getter for the archiveAssignmentManager property.
     * @return ArchiveAssignmentManager value of the property
     */
    public ArchiveAssignmentManager getArchiveAssignmentManager() {
        return archiveAssignmentManager;
    }

    /**
     * Setter for the archiveAssignmentManager property.
     * @param archiveAssignmentManager the new archiveAssignmentManager value
     */
    public void setArchiveAssignmentManager(ArchiveAssignmentManager archiveAssignmentManager) {
        this.archiveAssignmentManager = archiveAssignmentManager;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentToArchiveManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.selection.model.AssignmentStatus, java.util.Date, boolean)
     */
    public int executeArchive(QueryDecorator decorator) {

        int returnRecords = 0;

        //Get List of Assignments to Archive
        if (log.isDebugEnabled()) {
            log.debug("### Finding Transactional Assignments to Archive :::");
        }
        try {
            List<Assignment> assignments = getAssignmentManager()
                .getPrimaryDAO().listArchive(decorator);
            returnRecords = assignments.size();

            for (Assignment a : assignments) {
                getArchiveAssignmentManager().save(new ArchiveAssignment(a));
                a.setPurgeable(2);
                getAssignmentManager().save(a);
            }

        } catch (DataAccessException e) {
            log.warn("!!! Error getting Selection "
                + "Assignments from database to purge: " + e);
            return 0;
        } catch (BusinessRuleException e) {
            log.warn("!!! Error creating Archive Selection Assignment: "
                + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Archived " + returnRecords
                + " Selection Assignments ");
            log.debug("### Selection Assignment Archive Process Complete :::");
        }

        getAssignmentManager().getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentToArchiveManagerRoot#executeMarkForPurge(com.vocollect.voicelink.selection.model.AssignmentStatus, java.util.Date, boolean)
     */
    public Integer executeMarkForPurge(AssignmentStatus status,
                                       Date olderThan,
                                       boolean archive) {
        Integer recordsMarked = 0;

        //Mark all assignments to be purged
        if (log.isDebugEnabled()) {
            log.debug("### Finding Selection " + status.toString()
                + " Assignments to Purge :::");
        }
        try {
            if (archive) {
                recordsMarked = getAssignmentManager().getPrimaryDAO()
                    .updateOlderThan(1, olderThan, status);
            } else {
                recordsMarked = getAssignmentManager().getPrimaryDAO()
                    .updateOlderThan(2, olderThan, status);
            }
        } catch (DataAccessException e) {
            log.warn("!!! Error marking transactional "
                + "Selection " + status.toString() + " Assignments for Purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + recordsMarked
                + " Selection " + status.toString() + " Assignments for Purge :::");
        }
        return recordsMarked;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentToArchiveManagerRoot#executePurgeAssignment()
     */
    public int executePurgeAssignment() {
        int rows = 0;

        try {

            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge1();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge2();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge3();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge4();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge5();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge6();
            rows += getAssignmentManager().getPrimaryDAO().updateSQLPurge7();
        } catch (DataAccessException e) {
            log.warn("!!! Error purging transactional "
                + "assignments from database: " + e);
        }
        if (log.isDebugEnabled()) {
            log
                .debug("### Purged "
                    + rows
                    + " rows including assignments, assignmentLabor, picks, pickDetails, and containers.");
            log.debug("### Selection Assignment Purge Process Complete :::");
        }
        return rows;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 