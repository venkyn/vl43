/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.selection.dao.ArchivePickDAO;
import com.vocollect.voicelink.selection.model.ArchivePick;
import com.vocollect.voicelink.selection.service.ArchivePickManager;

import java.util.Date;
import java.util.List;

/**
 * Additional service methods for the <code>ArchivePick</code> model object.
 * 
 */
public abstract class ArchivePickManagerImplRoot extends
    GenericManagerImpl<ArchivePick, ArchivePickDAO> implements
    ArchivePickManager {

    /**
     * Constructor.
     * @param primaryDAO - primary DAO
     */
    public ArchivePickManagerImplRoot(ArchivePickDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchivePickManagerRoot#listShortedLocations(java.util.Date, java.util.Date)
     */
    public List<Object[]> listShortedLocations(Date startTime, Date endTime, Long siteId)
        throws DataAccessException {
        return getPrimaryDAO().listShortedLocations(startTime, endTime, siteId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchivePickManagerRoot#listSlotVisitLocations(java.util.Date, java.util.Date)
     */
    public List<Object[]> listSlotVisitLocations(Date startTime, Date endTime, Long siteId)
        throws DataAccessException {
        return getPrimaryDAO().listSlotVisitLocations(startTime, endTime, siteId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchivePickManagerRoot#listSlotVisitLocationsBySiteId(java.util.Date, java.util.Date)
     */
    public List<Object[]> listSlotVisitLocationsBySiteId(Long siteId)
        throws DataAccessException {
        return getPrimaryDAO().listSlotVisitLocationsBySiteId(siteId);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 