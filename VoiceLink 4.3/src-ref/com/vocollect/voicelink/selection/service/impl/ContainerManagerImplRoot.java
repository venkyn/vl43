/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.selection.dao.ContainerDAO;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerReport;
import com.vocollect.voicelink.selection.service.ContainerManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 * @author ddoubleday
 */
public abstract class ContainerManagerImplRoot
    extends GenericManagerImpl<Container, ContainerDAO>
    implements ContainerManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ContainerManagerImplRoot(ContainerDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Implementation to get container by number.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManager
     * #findContainerByNumber(java.lang.String)
     */
    public Container findContainerByNumber(String containerNumber)
        throws DataAccessException {
        return getPrimaryDAO().findContainerByNumber(containerNumber);
    }

    /**
     * Implementation to get container by last x digits.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManager
     * #findContainerByPartialNumber(java.lang.String)
     */
    public Container findContainerByPartialNumber(String partialContainerNumber)
        throws DataAccessException {
        return getPrimaryDAO().findContainerByPartialNumber(partialContainerNumber);
    }

    /**
     * Implementation to get a list of all containers.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManager
     * #listAllContainers()
     */
    public List<Container> listAllContainers()
        throws DataAccessException {
        return getPrimaryDAO().listAllContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        if (rdi.getQueryArgs() != null) {
            long assignmentId = Long.valueOf(((String) rdi.getQueryArgs()[0]));
            return this.getPrimaryDAO().listContainersByAssignment(
                new QueryDecorator(rdi), assignmentId);
        } else {
            return this.getPrimaryDAO().listContainers(new QueryDecorator(rdi));
        }
    }


    /**
     * Method to fetch list ContainerReport objects
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ContainerManagerRoot#listContainersForContainerReport(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.lang.String, java.util.Set, java.util.Set, java.util.Date,
     *      java.util.Date)
     */
    public List<ContainerReport> listContainersForContainerReport(QueryDecorator queryDecorator,
                                                                  String containerNumber,
                                                                  Set<String> operatorIdentifiers,
                                                                  Date startDate,
                                                                  Date endDate)
        throws DataAccessException {
        WhereClause whereClause = new WhereClause();
        whereClause.add(containerNumber, "obj.containerNumber");

        List<ContainerReport> containers = new ArrayList<ContainerReport>();

        //get containers with manually picks
        if (operatorIdentifiers == null || operatorIdentifiers.isEmpty()) {
            if (!whereClause.toString().equalsIgnoreCase("")) {
                queryDecorator.setWhereClause(whereClause.toString());
            }
            containers.addAll(getPrimaryDAO()
                .listContainersWithManuallPicksForContainerReport(
                    queryDecorator, startDate, endDate));
        } else {
            whereClause.add(
                operatorIdentifiers, "pd.operator.common.operatorIdentifier");
        }

        //Get all containers with non-manuall picks
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }

        containers.addAll(getPrimaryDAO().listContainersForContainerReport(
            queryDecorator, startDate, endDate));
        return containers;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 