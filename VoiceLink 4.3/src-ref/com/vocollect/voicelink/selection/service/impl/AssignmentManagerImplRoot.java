/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.ResequenceManager;
import com.vocollect.voicelink.lineloading.model.CartonType;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingContainerType;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingStopStatus;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.loading.service.LoadingStopManager;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.dao.AssignmentDAO;
import com.vocollect.voicelink.selection.dao.GroupNumberDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerStatus;
import com.vocollect.voicelink.selection.model.GroupNumber;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.selection.model.SelectionRegionPrintContainerLabels;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Additional service methods for the <code>Assignment</code> model object.
 * 
 * @author ddoubleday
 */
public abstract class AssignmentManagerImplRoot extends
    GenericManagerImpl<Assignment, AssignmentDAO> implements AssignmentManager {

    // The maximum number of times an assignment can be split
    private static final long MAX_SPLIT_NUMBER = 99L;

    private static final long DEFAULT_MIN_CHASE_NUMBER = 800000000L;

    private static final long DEFAULT_MAX_CHASE_NUMBER = 899999999L;

    private Long minimumChaseNumber = DEFAULT_MIN_CHASE_NUMBER;

    private Long maximumChaseNumber = DEFAULT_MAX_CHASE_NUMBER;

    private Long currentChaseNumber = -1L;

    private GroupNumberDAO groupNumberDAO;

    private CartonManager cartonManager;

    private ResequenceManager resequenceManager;

    private LoadingRouteManager loadingRouteManager;

    private LoadingStopManager loadingStopManager;

    private LoadingContainerManager loadingContainerManager;

    private Integer defaultPriority = 2;

    private Map<Integer, String> availablePriorities = new LinkedHashMap<Integer, String>();

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#getAvailablePriorities()
     */
    public Map<Integer, String> getAvailablePriorities() {
        return availablePriorities;
    }

    /**
     * Setter for the availablePriorities property.
     * 
     * @param availablePriorities the new availablePriorities value
     */
    public void setAvailablePriorities(Map<Integer, String> availablePriorities) {
        this.availablePriorities = availablePriorities;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#getDefaultPriority()
     */
    public Integer getDefaultPriority() {
        return defaultPriority;
    }

    /**
     * Setter for the defaultPriority property.
     * 
     * @param defaultPriority the new defaultPriority value
     */
    public void setDefaultPriority(Integer defaultPriority) {
        this.defaultPriority = defaultPriority;
    }

    /**
     * Get resequence manager.
     * 
     * @return ResequenceManager
     */
    public ResequenceManager getResequenceManager() {
        return resequenceManager;
    }

    /**
     * Set resequence manager.
     * 
     * @param resequenceManager - resequence manager
     */
    public void setResequenceManager(ResequenceManager resequenceManager) {
        this.resequenceManager = resequenceManager;
    }

    /**
     * Getter for the cartonManager property.
     * 
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return cartonManager;
    }

    /**
     * Setter for the cartonManager property.
     * 
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

    /**
     * Getter for the groupNumberDAO property.
     * 
     * @return GroupNumberDAO value of the property
     */
    public GroupNumberDAO getGroupNumberDAO() {
        return groupNumberDAO;
    }

    /**
     * Setter for the groupNumberDAO property.
     * 
     * @param groupNumberDAO the new groupNumberDAO value
     */
    public void setGroupNumberDAO(GroupNumberDAO groupNumberDAO) {
        this.groupNumberDAO = groupNumberDAO;
    }

    /**
     * @return the loadingRouteManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * @return the loadingStopManager
     */
    public LoadingStopManager getLoadingStopManager() {
        return loadingStopManager;
    }

    /**
     * @param loadingStopManager the loadingStopManager to set
     */
    public void setLoadingStopManager(LoadingStopManager loadingStopManager) {
        this.loadingStopManager = loadingStopManager;
    }

    /**
     * @return the loadingContainerManager
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * @param loadingContainerManager the loadingContainerManager to set
     */
    public void setLoadingContainerManager(LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#initializeState()
     */
    public void initializeState() {
        currentChaseNumber = -1L;
    }

    /**
     * Constructor.
     * 
     * @param primaryDAO the DAO for this manager
     */
    public AssignmentManagerImplRoot(AssignmentDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAssignments(java.lang.Long)
     */
    public List<Assignment> listAssignmentsInRegion(Long regionId,
                                                    List<AssignmentType> assignmentTypes)
        throws DataAccessException {
        return getPrimaryDAO()
            .listAssignmentsInRegion(regionId, assignmentTypes);
    }

    /**
     * Implementation to get list of reserved assignments.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listReservedAssignments(java.lang.String)
     */
    public List<Assignment> listReservedAssignments(String reserveValue)
        throws DataAccessException {
        return getPrimaryDAO().listReservedAssignments(reserveValue);
    }

    /**
     * Implementation to get list of active assignments for operator.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listActiveAssignments(java.lang.String)
     */
    public List<Assignment> listActiveAssignments(Operator operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listActiveAssignments(operatorId);
    }

    /**
     * Implementation to get list of available assignments to assign to
     * operator.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAvailableAssignments(com.vocollect.voicelink.core.model.Operator,
     *      java.lang.String,
     *      com.vocollect.voicelink.selection.model.AssignmentType,
     *      com.vocollect.voicelink.selection.model.SelectionRegion, int)
     */
    public List<Assignment> listAvailableAssignments(QueryDecorator queryDecorator,
                                                     Operator operatorID,
                                                     String reservedBy,
                                                     AssignmentType type,
                                                     Long regionID,
                                                     Integer numberOfAssignments)
        throws DataAccessException {

        return getPrimaryDAO().listAvailableAssignments(queryDecorator,
            operatorID, reservedBy, type, regionID, numberOfAssignments);
    }

    /**
     * Implementation to get list of additional available assignments to assign
     * to operator.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAvailableAssignments(com.vocollect.voicelink.core.model.Operator,
     *      java.lang.String,
     *      com.vocollect.voicelink.selection.model.AssignmentType,
     *      com.vocollect.voicelink.selection.model.SelectionRegion, int)
     */
    public List<Assignment> listAdditionalAssignments(QueryDecorator queryDecorator,
                                                      Operator operatorID,
                                                      AssignmentType type,
                                                      Long regionID)
        throws DataAccessException {

        return getPrimaryDAO().listAdditionalAssignments(queryDecorator,
            operatorID, type, regionID);
    }

    /**
     * Implmentation of list assignments in group.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAssignmentsInGroup(long)
     */
    public List<Assignment> listAssignmentsInGroup(long groupNumber)
        throws DataAccessException {
        return getPrimaryDAO().listAssignmentsInGroup(groupNumber);
    }

    /**
     * Implmentation of list assignments by work id.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAssignmentsByWorkId()
     */
    public List<Assignment> listAssignmentsByWorkId(QueryDecorator queryDecorator,
                                                    String workId,
                                                    Operator operatorId,
                                                    AssignmentType assignmentType,
                                                    Region regionId)
        throws DataAccessException {

        return getPrimaryDAO().listAssignmentsByWorkId(queryDecorator, workId,
            operatorId, assignmentType, regionId);
    }

    /**
     * Implementation for extracting Normal Assignments for Assignment Report.
     * /** Method for generating Assignment Report.
     * 
     * @param queryDecorator - the query decorator
     * @param workId - WorkID of the assignment.
     * @param operatorId - Set of Operator ID.
     * @param assignmentNumber - Assignment Number.
     * @param customerNumber - Customer Number.
     * @param route - Route of the Assignment.
     * @param startTime - Start Time of the Report.
     * @param endTime - End Time of the Report.
     * @return List<Assignment> - List of Assignments.
     * @throws DataAccessException
     * 
     *             {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listAssignmentsforAssignmentReport(java.lang.String,
     *      java.lang.String, java.lang.Long, java.lang.String,
     *      java.lang.String, java.util.Date, java.util.Date)
     */
    public List<AssignmentReport> listAssignmentsForAssignmentReport(QueryDecorator queryDecorator,
                                                                     String workId,
                                                                     Set<String> operatorId,
                                                                     Long assignmentNumber,
                                                                     String customerNumber,
                                                                     String route,
                                                                     Date startTime,
                                                                     Date endTime)
        throws DataAccessException {
        WhereClause whereClause = new WhereClause();
        whereClause.add(workId, "obj.workIdentifier.workIdentifierValue");
        whereClause.add(assignmentNumber, "obj.number");
        whereClause.add(customerNumber, "obj.customerInfo.customerNumber");
        whereClause.add(route, "obj.route");

        List<AssignmentReport> assignments = new ArrayList<AssignmentReport>();

        // Get Suspended assignment picks
        if (operatorId == null || operatorId.isEmpty()) {
            if (!whereClause.toString().equalsIgnoreCase("")) {
                queryDecorator.setWhereClause(whereClause.toString());
            }
            assignments.addAll(getPrimaryDAO()
                .listManuallyPickedPicksForAssignmentReport(queryDecorator,
                    startTime, endTime));

        } else {
            whereClause.add(operatorId,
                "obj.operator.common.operatorIdentifier");
        }

        // get all other non-suspended assignment picks
        if (!whereClause.toString().equalsIgnoreCase("")) {
            queryDecorator.setWhereClause(whereClause.toString());
        }
        assignments.addAll(getPrimaryDAO().listAssignmentsForAssignmentReport(
            queryDecorator, startTime, endTime));
        return assignments;
    }

    /**
     * Implementation of list assignments by work id.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAssignmentsByNumber(java.lang.String)
     */
    public List<Assignment> listAssignmentsByNumber(Long number)
        throws DataAccessException {
        return getPrimaryDAO().listAssignmentsByNumber(number);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#listAssignmentsByWorkId()
     */
    public List<Assignment> listAssignmentsByPartialWorkId(QueryDecorator queryDecorator,
                                                           String workId,
                                                           Operator operatorId,
                                                           AssignmentType assignmentType,
                                                           Region regionId)
        throws DataAccessException {

        return getPrimaryDAO().listAssignmentsByPartialWorkId(queryDecorator,
            workId, operatorId, assignmentType, regionId);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#
     * listActiveAssignmentsByOperatorAndType
     * (com.vocollect.voicelink.core.model.Operator,
     * com.vocollect.voicelink.selection.model.AssignmentType)
     */
    @Override
    public List<Assignment> listActiveAssignmentsByOperatorAndType(Operator operator,
                                                                   AssignmentType type)
        throws DataAccessException {
        return getPrimaryDAO().listActiveAssignmentsByOperatorAndType(operator,
            type);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#
     * listAssignmentsByDeliveryLocation(java.lang.String)
     */
    @Override
    public List<Assignment> listAssignmentsByDeliveryLocation(String deliveryLocation)
        throws DataAccessException {
        return getPrimaryDAO().listAssignmentsByDeliveryLocation(
            deliveryLocation);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        return this.getPrimaryDAO().listAssignments(new QueryDecorator(rdi));
    }

    /**
     * get list of assignments available to resequence from the given region.
     * 
     * @param regionId - id of the region to resequence
     * @return - return list of assignments
     * @throws DataAccessException - database exceptions
     */
    public List<DataObject> listResequenceAssignmentsByRegion(Long regionId)
        throws DataAccessException {
        return this.getPrimaryDAO().listResequenceAssignmentsByRegion(regionId);
    }

    /**
     * get list of assignments available to resequence from the given region.
     * 
     * @param rdi - ResultDataInfo to reqsequence
     * @return - return list of assignments
     * @throws DataAccessException - database exceptions
     */
    @SuppressWarnings("unchecked")
    public List<DataObject> getResequenceData(ResultDataInfo rdi)
        throws DataAccessException {
        Column col = rdi.getSortColumnObject();
        col.setSortType(ColumnSortType.Normal);
        rdi.setSortColumnObject(col);
        ArrayList<Long> resequenceList = (ArrayList<Long>) (rdi.getQueryArgs()[0]);
        List<DataObject> results = this.getPrimaryDAO()
            .listResequenceAssignments(new QueryDecorator(rdi));
        return getResequenceManager().setupResequenceResults(resequenceList,
            results);
    }

    /**
     * Implmentation of list assignments to resequence for the given region.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#executeResequence(java.util.ArrayList,
     *      java.lang.Long)
     */
    public void executeResequence(ArrayList<Long> resequenceList, Long regionId)
        throws DataAccessException, BusinessRuleException {
        List<DataObject> results = this.getPrimaryDAO()
            .listResequenceAssignmentsByRegion(regionId);
        getResequenceManager().executeResequence(resequenceList, results, this);
    }

    /**
     * Implmentation of validating whether shorts can be made into a chase
     * assignment.
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#validateCreateChaseAssignment(java.util.ArrayList,
     *      java.lang.Long)
     */
    public void validateCreateChaseAssignment(ArrayList<Pick> shorts,
                                              Operator operator)
        throws BusinessRuleException, DataAccessException {
        Assignment chase = new Assignment();
        chase.setType(AssignmentType.Chase);
        chase.changeStatus(AssignmentStatus.Available);
        chase.setNumber(getNextChaseAssignmentNumber());
        Assignment assignment = shorts.get(0).getAssignment();
        // Set Region
        chase.setRegion(assignment.getRegion());

        for (Pick shortedPick : shorts) {
            // Put the pick in this chase assignment.
            shortedPick.assignPickToChase(chase);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeCreateChaseAssignment(java.util.ArrayList)
     */
    public Assignment executeCreateChaseAssignment(ArrayList<Pick> shorts,
                                                   Operator operator)
        throws BusinessRuleException, DataAccessException {

        if (shorts.size() < 1) {
            return null;
        }

        Assignment chase = new Assignment();
        chase.setType(AssignmentType.Chase);
        chase.changeStatus(AssignmentStatus.Available);
        chase.setNumber(getNextChaseAssignmentNumber());
        // Initialize assignment general settings
        Assignment assignment = shorts.get(0).getAssignment();
        // Set Region
        chase.setRegion(assignment.getRegion());

        for (Pick shortedPick : shorts) {
            // Put the pick in this chase assignment.
            shortedPick.assignPickToChase(chase);
        }

        chase.getWorkIdentifier().setWorkIdentifierValue(
            assignment.getWorkIdentifier().getWorkIdentifierValue());
        chase.getWorkIdentifier().setPartialWorkIdentifier(
            assignment.getWorkIdentifier().getPartialWorkIdentifier());
        chase.getCustomerInfo().setCustomerNumber(
            assignment.getCustomerInfo().getCustomerNumber());
        chase.getCustomerInfo().setCustomerName(
            assignment.getCustomerInfo().getCustomerName());
        chase.getCustomerInfo().setCustomerAddress(
            assignment.getCustomerInfo().getCustomerAddress());
        chase.setRoute(assignment.getRoute());
        chase.setDeliveryLocation(assignment.getDeliveryLocation());
        chase.getGroupInfo().setGroupCount(1);
        chase.getGroupInfo().setGroupPosition(1);
        chase.setPriority(getDefaultPriority());

        // calculate assignment information
        calculateAssignmentInfo(chase);

        // Assign the operator
        chase.setOperator(operator);

        // Save the assignment.
        getPrimaryDAO().save(chase);

        // Set the sequence number.
        chase.setSequenceNumber(chase.getId());

        return chase;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#
     * executeCreateLoadingContainer
     * (com.vocollect.voicelink.selection.model.Assignment)
     */
    @Override
    public synchronized List<Container> executeCreateLoadingContainer(Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        List<Container> createdContainers = new ArrayList<Container>();

        if (isSelectionLoadingIntegrated(assignment)) {
            createdContainers = createLoadingContaner(assignment);
        }

        return createdContainers;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#
     * isSelectionLoadingIntegrated
     * (com.vocollect.voicelink.selection.model.Assignment)
     */
    @Override
    public boolean isSelectionLoadingIntegrated(Assignment assignment) {
        SelectionRegionProfile profile = assignment.getRegion().getProfile(
            assignment.getType());

        return assignment.getRoute() != null
            && assignment.getRoute().length() > 0
            && assignment.getCustomerInfo() != null
            && assignment.getLoadingRegion() != null
            && assignment.getDepartureDateTime() != null
            && profile.getPrintContainerLabelsIndicator() != SelectionRegionPrintContainerLabels.DoNotPrint;
    }

    /**
     * @param assignment the assignment for which loading container should be
     *            created
     * @return List of containers created
     * @throws DataAccessException dae
     * @throws BusinessRuleException bre
     */
    protected List<Container> createLoadingContaner(Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        List<Container> createdContainers = new ArrayList<Container>();

        // Initializing variables
        Date departureDateTime = assignment.getDepartureDateTime();
        LoadingRegion loadingRegion = assignment.getLoadingRegion();
        String loadingRouteString = assignment.getRoute();
        Customer customerInfo = assignment.getCustomerInfo();

        // Check if assignment has not picked picks, if so the create loading
        // container is called before stop assignment can be fired
        boolean checkClosedContainer = false;

        List<Pick> picks = assignment.getPicks();

        for (Pick pick : picks) {
            if (pick.getStatus() == PickStatus.NotPicked) {
                checkClosedContainer = true;
                break;
            }
        }

        // Getting route
        LoadingRoute loadingRoute = getLoadingRouteManager().findLoadingRoute(
            loadingRouteString, loadingRegion.getNumber(), departureDateTime,
            customerInfo.getCustomerNumber());

        if (loadingRoute != null) {
            // Getting stop
            LoadingStop loadingStop = getLoadingStopManager()
                .findStopByCustomer(loadingRoute.getNumber(),
                    loadingRegion.getNumber(), departureDateTime,
                    customerInfo.getCustomerNumber());

            // If route and stop are found enter into container creation
            if (loadingStop != null
                && loadingRoute.getStatus() != LoadingRouteStatus.Complete) {
                Set<Container> containers = assignment.getContainers();

                // Loop through the container list
                for (Container container : containers) {

                    // If we have pick in the container go and create the
                    // container
                    if (container.getPickDetails().size() > 0
                        && (!checkClosedContainer || container.getStatus() == ContainerStatus.Closed)) {

                        LoadingContainer newLoadingContainer = new LoadingContainer();

                        String loadingContainerString = container
                            .getContainerNumber();

                        List<LoadingContainerStatus> containerStatus = new ArrayList<LoadingContainerStatus>();

                        // Check if the container exists
                        containerStatus.add(LoadingContainerStatus.NotLoaded);
                        containerStatus.add(LoadingContainerStatus.Loaded);
                        containerStatus
                            .add(LoadingContainerStatus.Consolidated);
                        containerStatus.add(LoadingContainerStatus.Cancelled);
                        containerStatus.add(LoadingContainerStatus.Unloaded);
                        containerStatus.add(LoadingContainerStatus.InProgress);

                        LoadingContainer targetLoadingContainer = getLoadingContainerManager()
                            .findLoadingContainerByNumberAndStatus(
                                loadingContainerString, containerStatus);

                        if (targetLoadingContainer == null) {
                            newLoadingContainer
                                .setContainerNumber(loadingContainerString);
                            newLoadingContainer.setCreatedDate(new Date());
                            newLoadingContainer
                                .setDepartureDateTime(departureDateTime);
                            newLoadingContainer.setRegion(loadingRegion);
                            newLoadingContainer.setRoute(loadingRoute);
                            newLoadingContainer
                                .setStatus(LoadingContainerStatus.NotLoaded);
                            newLoadingContainer.setStop(loadingStop);
                            newLoadingContainer
                                .setType(LoadingContainerType.Selection);
                            newLoadingContainer
                                .setExpectedCube(calculateExpectedCube(container));
                            newLoadingContainer
                                .setExpectedWeight(calculateExpectedWeight(container));

                            getLoadingContainerManager().save(
                                newLoadingContainer);

                            createdContainers.add(container);
                        }

                    }
                }
            }

            // If loading route is in progress and stop is loaded but we drop
            // container it will put the stop to in progress
            if (createdContainers.size() > 0
                && (loadingRoute.getStatus() == LoadingRouteStatus.InProgress || loadingRoute
                    .getStatus() == LoadingRouteStatus.Suspended)
                && loadingStop.getStatus() == LoadingStopStatus.Loaded) {

                loadingStop.setStatus(LoadingStopStatus.InProgress);
            }
        }

        return createdContainers;
    }

    /**
     * @param container The container for which expected weight is calculated
     * @return the expected weight for the container
     */
    protected Double calculateExpectedWeight(Container container) {
        Double expectedWeight = 0.0;

        List<PickDetail> pickDetails = container.getPickDetails();

        for (PickDetail pickDetail : pickDetails) {
            Item item = pickDetail.getPick().getItem();
            Integer pickCount = pickDetail.getQuantityPicked();
            expectedWeight = expectedWeight + (item.getWeight() * pickCount);
        }

        return expectedWeight;
    }

    /**
     * @param container The container for which expected weight is calculated
     * @return the expected cube for the container
     */
    protected Double calculateExpectedCube(Container container) {
        Double expectedCube = 0.0;

        List<PickDetail> pickDetails = container.getPickDetails();

        for (PickDetail pickDetail : pickDetails) {
            Item item = pickDetail.getPick().getItem();
            Integer pickCount = pickDetail.getQuantityPicked();
            expectedCube = expectedCube + (item.getCube() * pickCount);
        }

        return expectedCube;
    }

    /**
     * Generate next available chase assignment number.
     * 
     * @return - next available chase assignment number
     * @throws DataAccessException - Database Exception
     */
    protected synchronized long getNextChaseAssignmentNumber()
        throws DataAccessException {
        // Check to see if number needs intialized based on current data
        if (this.currentChaseNumber == -1L) {

            // VLINK-2018 Duplicate Chase Assignment number can be created after
            // system restart
            // Capture the currect site context
            boolean siteFilter = SiteContextHolder.getSiteContext()
                .isFilterBySite();

            // Turn site filter off
            SiteContextHolder.getSiteContext().setFilterBySite(false);

            this.currentChaseNumber = getPrimaryDAO().maxChaseAssignment();

            // Turn site filter back on
            SiteContextHolder.getSiteContext().setFilterBySite(siteFilter);
            // END VLINK-2018

            if (this.currentChaseNumber == null) {
                this.currentChaseNumber = 0L;
            }
            // TODO: Get min and max value from system properties as well
        }

        // Increment value to next value
        this.currentChaseNumber++;

        // check if we should start over from lower value
        // If minimum value is defined to be less than maximum value
        // then the minimum value will always be issued
        if (this.currentChaseNumber < this.minimumChaseNumber) {
            this.currentChaseNumber = this.minimumChaseNumber;
        } else if (this.currentChaseNumber > this.maximumChaseNumber) {
            this.currentChaseNumber = this.minimumChaseNumber;
        }

        return this.currentChaseNumber;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeGroupAssignments(java.util.ArrayList,
     *      boolean)
     */
    public void executeGroupAssignments(ArrayList<Assignment> assignments,
                                        boolean fromGUI)
        throws DataAccessException, BusinessRuleException {

        // Validate assignments can be grouped
        executeValidateGroup(assignments, fromGUI);

        // Used for ungrouping assignment from previous group
        ArrayList<Assignment> ungroup = new ArrayList<Assignment>();
        // Generate new Group Number
        Long groupNumber = getNextGroupNumber();
        int groupCounter = 1;

        // Assignment status fo all assignments in group will be either
        // available or unavailable. If mixed, then they will be available
        AssignmentStatus finalStatus = assignments.get(0).getStatus();
        // If not already being set to available see if any assignments
        // are available, therefore requiring all assignments being set to
        // available
        if (!finalStatus.equals(AssignmentStatus.Available)) {
            for (Assignment a : assignments) {
                if (a.getStatus().equals(AssignmentStatus.Available)) {
                    finalStatus = a.getStatus();
                }
            }
        }

        // Set group infomation
        for (Assignment a : assignments) {
            // Ungroup if already in group
            if (a.getGroupInfo().getGroupNumber() != null) {
                ungroup.clear();
                ungroup.add(a);
                executeUngroupAssignments(ungroup, fromGUI);
            }

            // set info
            a.getGroupInfo().setGroupCount(assignments.size());
            a.getGroupInfo().setGroupNumber(groupNumber);
            if (fromGUI) {
                a.getGroupInfo().setGroupPosition(groupCounter++);
            } else {
                a.getGroupInfo().setGroupPosition(
                    a.getGroupInfo().getRequestedOrderNo());
            }
            a.setStatus(finalStatus);
        }

    }

    /**
     * Get the next group number.
     * 
     * @return - last group number
     * @throws DataAccessException - Database Exceptions
     */
    protected synchronized long getNextGroupNumber() throws DataAccessException {
        long returnValue = 0;
        // Check to see if number needs intialized based on current data
        GroupNumber gn = new GroupNumber();
        getGroupNumberDAO().save(gn);
        returnValue = gn.getId();
        getGroupNumberDAO().delete(gn);

        return returnValue;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeValidateGroup(java.util.ArrayList,
     *      boolean)
     */
    public void executeValidateGroup(ArrayList<Assignment> assignments,
                                     boolean fromGUI)
        throws BusinessRuleException {

        for (Assignment a : assignments) {
            // REQ4 - Verify all assignments in group have same region
            validateGroupSameRegion(assignments.get(0), a);

            // REQ6 - All assignments must be the same type
            validateGroupSameType(assignments.get(0), a);

            if (fromGUI) {
                // REQ2 - From GUI the assignment must have status of Available,
                // Unavailable or suspended
                validateGroupValidSatus(a);

                // REQ44 - If from GUI cannot have operator assigned
                validateGroupOperator(a);
            }

        }

        if (assignments.size() > 1) {
            // REQ5 - Region cannot be set up for case label check digits
            validateGroupCaseLabelCheckDigit(assignments.get(0));

            // REQ7 Region profile must allow multiple assignment
            validateGroupMultipleAllowed(assignments.get(0));
        }

        // REQ8 Number of assignments being grouped must be less than or equal
        // to
        // what the region allows
        validateGroupMaximum(assignments.size(), assignments.get(0));

        if (fromGUI) {
            // REQ1 - From GUI at least 2 or more assignments must be selected
            validateGroupMoreThanOne(assignments.size());

            // REQ3 - Assignments being grouped from GUI must be in auto
            // issuance region
            validateGroupAutoIssuance(assignments.get(0));
        }

    }

    /**
     * Verify all assignments in group have same region.
     * 
     * @param a1 - first assignment
     * @param a2 - second assignemnt
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupSameRegion(Assignment a1, Assignment a2)
        throws BusinessRuleException {
        if (!a2.getRegion().equals(a1.getRegion())) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_NOT_SAME_REGION, new UserMessage(
                    "assignment.group.error.notAllSameRegion"));
        }

    }

    /**
     * All assignments must be the same type.
     * 
     * @param a1 - first assignment
     * @param a2 - second assignemnt
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupSameType(Assignment a1, Assignment a2)
        throws BusinessRuleException {
        if (a2.getType() != a1.getType()) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_NOT_SAME_TYPE, new UserMessage(
                    "assignment.group.error.notSameType"));
        }
    }

    /**
     * From GUI the assignment must have status of Available, Unavailable or
     * suspended.
     * 
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupValidSatus(Assignment a)
        throws BusinessRuleException {
        if (!a.getStatus().isInSet(AssignmentStatus.Available,
            AssignmentStatus.Unavailable)) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_INVALID_ASSIGNMENT_STATUS,
                new UserMessage("assignment.group.error.invalidgroupStatus"));
        }
    }

    /**
     * If from GUI cannot have operator assigned.
     * 
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupOperator(Assignment a)
        throws BusinessRuleException {
        if (a.getOperator() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_OPERATOR_ASSIGNED, new UserMessage(
                    "assignment.group.error.operatorAssigned"));
        }
    }

    /**
     * Region cannot be set up for case label check digits.
     * 
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupCaseLabelCheckDigit(Assignment a)
        throws BusinessRuleException {
        if (a.getRegion().getProfile(a.getType())
            .isRequireCaseLabelCheckDigits()) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_NO_CASE_LABEL_CD, new UserMessage(
                    "assignment.group.error.notAllowedCaseLabelCheckDigits"));
        }
    }

    /**
     * Region profile must allow multiple assignment.
     * 
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupMultipleAllowed(Assignment a)
        throws BusinessRuleException {
        if (!a.getRegion().getProfile(a.getType()).isAllowMultipleAssignments()) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_REGION_NOT_ALLOW_MULT,
                new UserMessage(
                    "assignment.group.error.regionNotAllowedMultiple"));
        }

    }

    /**
     * Number of assignments being grouped must be less than or equal to what
     * the region allows.
     * 
     * @param count - number of assignments in group
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupMaximum(int count, Assignment a)
        throws BusinessRuleException {
        if (a.getRegion().getUserSettings(a.getType())
            .getMaximumAssignmentsAllowed() < count) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_TOO_MANY_ASSIGNMENTS, new UserMessage(
                    "assignment.group.error.tooManyAssignments", count, a
                        .getRegion().getUserSettings(a.getType())
                        .getMaximumAssignmentsAllowed()));
        }
    }

    /**
     * From GUI at least 2 or more assignments must be selected.
     * 
     * @param count - number of assignments in group
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupMoreThanOne(int count)
        throws BusinessRuleException {
        if (count <= 1) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_2_ASSIGNMENTS_REQUIRED,
                new UserMessage(
                    "assignment.group.error.twoOrMoreAssignmentsRequired"));
        }
    }

    /**
     * Assignments being grouped from GUI must be in auto issuance region.
     * 
     * @param a - assignment to check
     * @throws BusinessRuleException Business rule exception
     */
    protected void validateGroupAutoIssuance(Assignment a)
        throws BusinessRuleException {
        if (!a.getRegion().getProfile(a.getType()).isAutoIssuance()) {
            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_NOT_AUTO_ISSUANCE, new UserMessage(
                    "assignment.group.error.notAutoissuance"));
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeUngroupFromManualRegion(java.util.ArrayList)
     */
    public void executeUngroupFromManualRegion(ArrayList<Assignment> assignments)
        throws DataAccessException, BusinessRuleException {
        if (!assignments.get(0).getRegion()
            .getProfile(assignments.get(0).getType()).isAutoIssuance()) {

            executeUngroupAssignments(assignments, false);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeUngroupAssignments(java.util.ArrayList,
     *      boolean)
     */
    public void executeUngroupAssignments(ArrayList<Assignment> assignments,
                                          boolean fromGUI)
        throws DataAccessException, BusinessRuleException {
        validateUngroup(assignments, fromGUI);

        List<Assignment> group = listAssignmentsInGroup(assignments.get(0)
            .getGroupInfo().getGroupNumber());

        // REQ20 - all assignment must be specified if from task
        if (!fromGUI && !assignments.containsAll(group)) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_TASK_NOT_ALL_ASSIGNMENTS,
                new UserMessage("assignment.ungroup.error.notAllSelected"));
        }

        for (Assignment a : group) {
            // If 1 or less assignments would remain in group
            // of if assignment is in list being ungrouped
            if ((group.size() - assignments.size() <= 1)
                || assignments.contains(a)) {
                a.getGroupInfo().setGroupCount(1);
                a.getGroupInfo().setGroupNumber(null);
                a.getGroupInfo().setGroupPosition(null);
                a.getGroupInfo().setRequestedOrderNo(1);
            } else {
                a.getGroupInfo().setGroupCount(
                    group.size() - assignments.size());
            }
        }
        // Now that we are done -- renumber the group positions
        // for assignments left in the list --
        // Assignments still in the list do not have NULL group position
        // REF: VLINK-1119
        Integer iPosition = 1;
        for (Assignment a : group) {
            if (a.getGroupInfo().getGroupNumber() != null) {
                a.getGroupInfo().setGroupPosition(iPosition++);
            }
        }
    }

    /**
     * Validate if assignments can be ungrouped.
     * 
     * @param assignments - assignments to ungroup
     * @param fromGUI - whether called from GUI or task.
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void validateUngroup(ArrayList<Assignment> assignments,
                                   boolean fromGUI)
        throws BusinessRuleException {
        Long groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();

        for (Assignment a : assignments) {
            if (fromGUI) {
                // REQ16 - From GUI the assignment must have status of
                // Available, Unavailable
                validateUngroupStatus(a);

                // REQ18 - All assignment being ungrouped cannot be assigned to
                // operator
                validateUngroupOperator(a);
            }

            // REQ17 - All assignments being ungrouped must be in a group
            validateUngroupInGroup(a);

            // REQ18 - All assignment being ungrouped must be in same group
            validateUngroupInSameGroup(groupNumber, a);
        }

        // <REQ19> grouped assignments must be in a region defined with manual
        // issuance
        if (!fromGUI) {
            validateUngroupManualIssuance(assignments.get(0));
        }
    }

    /**
     * From GUI the assignment must have status of Available, Unavailable.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - business rule exception
     */
    protected void validateUngroupStatus(Assignment a)
        throws BusinessRuleException {
        if (!a.getStatus().isInSet(AssignmentStatus.Available,
            AssignmentStatus.Unavailable, AssignmentStatus.Suspended)) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_INVALID_STATUS, new UserMessage(
                    "assignment.ungroup.error.invalidStatus"));
        }
    }

    /**
     * All assignments being ungrouped must be in a group.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - business rule exception
     */
    protected void validateUngroupInGroup(Assignment a)
        throws BusinessRuleException {
        if (a.getGroupInfo().getGroupNumber() == null) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_NOT_ALL_IN_GROUP, new UserMessage(
                    "assignment.ungroup.error.notInGroup"));
        }
    }

    /**
     * All assignment being ungrouped must be in same group.
     * 
     * @param groupNumber - group being ungrouped
     * @param a - assignment
     * @throws BusinessRuleException - business rule exception
     */
    protected void validateUngroupInSameGroup(long groupNumber, Assignment a)
        throws BusinessRuleException {
        if (!a.getGroupInfo().getGroupNumber().equals(groupNumber)) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_NOT_ALL_SAME_GROUP, new UserMessage(
                    "assignment.ungroup.error.notSameGroup"));
        }
    }

    /**
     * Check if operator assigned.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - business rule exception
     */
    protected void validateUngroupOperator(Assignment a)
        throws BusinessRuleException {
        if (a.getOperator() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_NO_OPERATOR, new UserMessage(
                    "assignment.ungroup.error.noOperator"));
        }

    }

    /**
     * grouped assignments must be in a region defined with manual issuance.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - business rule exception
     */
    protected void validateUngroupManualIssuance(Assignment a)
        throws BusinessRuleException {
        if (a.getRegion().getProfile(a.getType()).isAutoIssuance()) {
            throw new BusinessRuleException(
                SelectionErrorCode.UNGROUP_TASK_REGION_NOT_MANUAL,
                new UserMessage("assignment.ungroup.error.notManualIssuance"));
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeCreateSplitAssignment(java.util.ArrayList)
     */
    public Assignment executeCreateSplitAssignment(ArrayList<Pick> picks,
                                                   Operator operatorOriginal,
                                                   Operator operatorNew)
        throws DataAccessException, BusinessRuleException {

        // Validate list of picks is valid to split
        validateSplitAssignment(picks);

        Long maxSplitNumber = 0L;

        // <REQ29> An assignment cannot be split more than 99 times
        if (maxSplitNumber >= MAX_SPLIT_NUMBER) {
            throw new BusinessRuleException(SelectionErrorCode.SPLIT_MAX_TIMES,
                new UserMessage("assignment.split.error.maxSplits"));
        }

        // Create and intialize new assignment
        Assignment splitOrig = picks.get(0).getAssignment();
        Assignment splitNew = new Assignment();

        splitNew.setNumber(splitOrig.getNumber());
        splitNew.setType(splitOrig.getType());
        if (splitOrig.getStatus() == AssignmentStatus.Unavailable) {
            splitNew.changeStatus(AssignmentStatus.Unavailable);
        } else {
            splitNew.changeStatus(AssignmentStatus.Available);
        }
        splitNew.setSequenceNumber(splitOrig.getSequenceNumber());
        splitNew.setRegion(splitOrig.getRegion());
        splitNew.setWorkIdentifier(splitOrig.getWorkIdentifier());
        splitNew.setCustomerInfo(splitOrig.getCustomerInfo());
        splitNew.setRoute(splitOrig.getRoute());
        splitNew.setDeliveryLocation(splitOrig.getDeliveryLocation());
        splitNew.setDeliveryDate(splitOrig.getDeliveryDate());
        splitNew.getGroupInfo().setGroupCount(1);
        splitNew.getGroupInfo().setGroupPosition(1);
        splitNew.setPriority(splitOrig.getPriority());

        // Move picks to new assignment
        for (Pick p : picks) {
            // Move pick to new assignment
            splitNew.addPick(p.getCopy());
            splitOrig.getPicks().remove(p);
        }

        // calculate assignment information for new assignment
        calculateAssignmentInfo(splitNew);
        // Recalculate assignment information for original assignment
        calculateAssignmentInfo(splitOrig);

        // Set split Number for assignment
        if (splitOrig.getSplitNumber() != null) {
            maxSplitNumber = getMaxSplitNumber(splitOrig.getNumber());
        }

        if (maxSplitNumber == 0) {
            splitOrig.setSplitNumber(++maxSplitNumber);
        }
        splitNew.setSplitNumber(++maxSplitNumber);

        splitNew.setOperator(operatorNew);
        splitOrig.setOperator(operatorOriginal);

        // Check if original assignment should be completed
        if (splitOrig.getStatus().isInSet(AssignmentStatus.Available,
            AssignmentStatus.Unavailable)
            && (splitOrig.getCancelCount() == splitOrig.getPicks().size())) {
            splitOrig.stopAssignment(AssignmentStatus.Canceled, new Date());
        } else if (splitOrig.getNotPickedCount() + splitOrig.getPartailCount() == 0) {
            splitOrig.stopAssignment(AssignmentStatus.Complete, new Date());
        }
        save(splitNew);

        return null;
    }

    /**
     * Get next split numnber.
     * 
     * @param assignmentNumber - assignment number to get split number for
     * @return - maximum split number
     * @throws DataAccessException - database exception
     */
    protected Long getMaxSplitNumber(Long assignmentNumber)
        throws DataAccessException {
        List<Assignment> as = getPrimaryDAO().listSplitNumber(assignmentNumber);

        return as.get(0).getSplitNumber();
    }

    /**
     * Validate split assignment.
     * 
     * @param picks - picks to split out
     * @throws BusinessRuleException - Business Rule Exception
     */
    public void validateSplitAssignment(ArrayList<Pick> picks)
        throws BusinessRuleException {
        Assignment a = picks.get(0).getAssignment();

        // <REQ25> Assignment must have a status of Available, Unavailable,
        // Suspended, or Passed.
        validateSplitAssignmentStatus(a);

        // <REQ26> If Assignment status is Suspended, then the assigned operator
        // must be blank (Null)
        validateSplitAssignmentSuspended(a);

        // <REQ27> Assignment cannot be part of a group of assignment (group
        // number must be null)
        validateSplitAssignmentInGroup(a);

        // <REQ30> All picks in an assignment cannot be split out (At least 1
        // pick must remain in original assignment)
        validateSplitAllPicks(a, picks);

        // <REQ31> Assignments with case label check digits cannot be split
        validateSplitAssignmentCaseLabelCheckDigits(a);

        // <REQ32> Assignments with container breaks cannot be split
        validateSplitAssignmentTargetContainers(a);

        // <REQ28> Pick Status of selected records must be Not Picked or Base
        // Item
        validateSplitPickStatus(picks);
    }

    /**
     * Assignment must have a status of Available, Unavailable, Suspended, or
     * Passed.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAssignmentStatus(Assignment a)
        throws BusinessRuleException {
        if (!a.getStatus().isInSet(AssignmentStatus.Available,
            AssignmentStatus.Unavailable, AssignmentStatus.Suspended,
            AssignmentStatus.Passed)) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_INVALID_ASSIGNMENT_STATUS,
                new UserMessage(
                    "assignment.split.error.invalidAssignmentStatus"));
        }
    }

    /**
     * If Assignment status is Suspended, then the assigned operator must be
     * blank (Null).
     * 
     * @param a - assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAssignmentSuspended(Assignment a)
        throws BusinessRuleException {
        if (a.getOperator() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_OPERATOR_ASSIGNED, new UserMessage(
                    "assignment.split.error.invalidOperator"));
        }
    }

    /**
     * Assignment cannot be part of a group of assignments.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAssignmentInGroup(Assignment a)
        throws BusinessRuleException {
        if (a.getGroupInfo().isGrouped()) {
            throw new BusinessRuleException(SelectionErrorCode.SPLIT_IN_GROUP,
                new UserMessage("assignment.split.error.notAllowedInGroup"));
        }
    }

    /**
     * All picks in an assignment cannot be split out (At least 1 pick must
     * remain in original assignment).
     * 
     * @param a - assignment
     * @param picks - picks being split from assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAllPicks(Assignment a, ArrayList<Pick> picks)
        throws BusinessRuleException {
        if (picks.containsAll(a.getPicks())) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_NOT_ALL_PICKS_ALLOWED,
                new UserMessage("assignment.split.error.notAllowedAllPicks"));
        }
    }

    /**
     * Assignments with case label check digits cannot be split.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAssignmentCaseLabelCheckDigits(Assignment a)
        throws BusinessRuleException {
        if (a.getRegion().getProfile(a.getType())
            .isRequireCaseLabelCheckDigits()) {
            throw new BusinessRuleException(
                SelectionErrorCode.SPLIT_CASE_LABEL_CD, new UserMessage(
                    "assignment.split.error.caseLabelCheckDigits"));
        }
    }

    /**
     * Assignments with container breaks cannot be split.
     * 
     * @param a - assignment
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitAssignmentTargetContainers(Assignment a)
        throws BusinessRuleException {
        if (a.getRegion().getProfile(a.getType()).getContainerType() != null) {
            if (a.getRegion().getProfile(a.getType()).getContainerType()
                .isRequireTargetContainers()) {
                throw new BusinessRuleException(
                    SelectionErrorCode.SPLIT_TARGET_CONTAINERS,
                    new UserMessage("assignment.split.error.targetContainers"));
            }
        }
    }

    /**
     * Pick Status of selected records must be Not Picked or Base Item.
     * 
     * @param picks - picks being split out
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void validateSplitPickStatus(ArrayList<Pick> picks)
        throws BusinessRuleException {
        for (Pick p : picks) {
            if (!p.getStatus().isInSet(PickStatus.NotPicked,
                PickStatus.BaseItem)) {
                throw new BusinessRuleException(
                    SelectionErrorCode.SPLIT_INVALID_PICK_STATUS,
                    new UserMessage("assignment.split.error.invalidPickStatus"));
            }
        }
    }

    /**
     * Method to ensure all assignment in a group are updated the same when an
     * assignment was edited from the UI.
     * 
     * @param assignment - assignment that was edited
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    public void executeAssignmentChangeSave(Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        // Refresh the region object to ensure there are no flushing
        // errors while saving an assignment.
        getPrimaryDAO().refresh(assignment.getRegion());

        if (assignment.getGroupInfo().getGroupNumber() != null) {
            List<Assignment> group = this.listAssignmentsInGroup(assignment
                .getGroupInfo().getGroupNumber());

            // Modify status and operator to match assignment that was edited
            for (Assignment a : group) {
                if (!a.equals(assignment)) {
                    a.setOperator(assignment.getOperator());
                    a.changeStatus(assignment.getStatus());
                    executeUpdateLineLoading(a);
                }
            }

            // if the operator was changed to null manually issued assignments
            // need to be ungrouped
            if (assignment.getOperator() == null) {
                // ungroup if manual issuance
                executeUngroupFromManualRegion((ArrayList<Assignment>) group);
            }
        }
        executeUpdateLineLoading(assignment);
        save(assignment);
    }

    /**
     * Calculates the summary information of an assignment based on the picks
     * within the assignment.
     * 
     * @param assignment - assignment to calculate info for
     */
    protected void calculateAssignmentInfo(Assignment assignment) {
        double totalWeight = 0;
        double totalCube = 0;
        Integer variableWeightTotal = 0;
        int totalQuantity = 0;
        String multiple = ResourceUtil
            .getLocalizedKeyValue("assignment.column.multiplevalues");
        Assignment tempAssign;

        for (Pick p : assignment.getPicks()) {
            // Calculate summary information
            totalWeight += p.getQuantityToPick() * p.getItem().getWeight();
            totalCube += p.getQuantityToPick() * p.getItem().getCube();
            totalQuantity += p.getQuantityToPick();
            if (p.getItem().getIsVariableWeightItem()) {
                variableWeightTotal++;
            }

            // ensure relationship is completely set up
            if (p.getAssignment() == null) {
                p.setAssignment(assignment);
            }

            // Check for multiple
            if (p.getOriginalPick() == null) {
                tempAssign = p.getAssignment();
            } else {
                tempAssign = p.getOriginalPick().getAssignment();
            }

            // if the object is being created and is a taggable object
            // get the current site from the context and add it to the object
            if (p.isNew()) {
                assignSiteTagsToPick(p);
            }

            // Set partial work ID field based on region settings for
            // assignment.
            String partial = assignment.getWorkIdentifier()
                .getWorkIdentifierValue();
            int partialLength = assignment.getRegion().getUserSettingsNormal()
                .getWorkIdentifierRequestLength();
            if ((0 != partialLength) && (partialLength < partial.length())) {
                partial = partial.substring(partial.length() - partialLength);
            }
            assignment.getWorkIdentifier().setPartialWorkIdentifier(partial);

            // If Assignments not equals
            if (!assignment.equals(tempAssign)) {

                // Intialize delivery date if null
                if (assignment.getDeliveryDate() == null) {
                    assignment.setDeliveryDate(tempAssign.getDeliveryDate());
                }

                // =========================================================
                // Set multiple if needed
                // =========================================================

                // If work ids not equal set to multiple
                if (!assignment
                    .getWorkIdentifier()
                    .getWorkIdentifierValue()
                    .equals(
                        tempAssign.getWorkIdentifier().getWorkIdentifierValue())) {
                    assignment.getWorkIdentifier().setWorkIdentifierValue(
                        multiple);
                    assignment.getWorkIdentifier().setPartialWorkIdentifier(
                        multiple);
                }

                // Check customer name for multiple
                if (!assignment.getCustomerInfo().getCustomerNumber()
                    .equals(tempAssign.getCustomerInfo().getCustomerNumber())) {
                    assignment.getCustomerInfo().setCustomerNumber(multiple);
                    assignment.getCustomerInfo().setCustomerName(multiple);
                    assignment.getCustomerInfo().setCustomerAddress(multiple);
                }

                // Check multiple routes
                if (!assignment.getRoute().equals(tempAssign.getRoute())) {
                    assignment.setRoute(multiple);
                }

                // check for multiple delivery locations
                if (!StringUtil.isNullOrEmpty(assignment.getDeliveryLocation())
                    && !StringUtil.isNullOrEmpty(tempAssign
                        .getDeliveryLocation())) {
                    if (!assignment.getDeliveryLocation().equals(
                        tempAssign.getDeliveryLocation())) {
                        assignment.setDeliveryLocation(multiple);
                    }
                }

                // Set to earliest delivery date
                if (assignment.getDeliveryDate().before(
                    tempAssign.getDeliveryDate())) {
                    assignment.setDeliveryDate(tempAssign.getDeliveryDate());
                }
            }
        }

        // Give this assignment some summary info.
        assignment.getSummaryInfo().setTotalWeight(new Double(totalWeight));
        assignment.getSummaryInfo().setTotalCube(new Double(totalCube));
        assignment.getSummaryInfo().setTotalItemQuantity(totalQuantity);
        assignment.setVariableWeightTotal(variableWeightTotal);

        // Other properties that aren't nullable.
        assignment.setPurgeable(0);
    }

    /**
     * Assign new tag to pick.
     * 
     * @param pick - pick to assign tag to
     */
    public void assignSiteTagsToPick(Pick pick) {
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        siteContext.setSiteToCurrentSite(pick);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManager#executeCheckIfEditable(com.vocollect.voicelink.selection.model.Assignment)
     */
    public void executeCheckIfEditable(Assignment assignment)
        throws DataAccessException, BusinessRuleException {

        validateEditable(assignment);
    }

    /**
     * @param assignment the assignment to test
     * @throws BusinessRuleException if not editable for business reasons.
     */
    public void validateEditable(Assignment assignment)
        throws BusinessRuleException {

        String aStatus = assignment.getStatus().toString();
        if (aStatus.equals("InProgress") || aStatus.equals("Canceled")) {

            throw new BusinessRuleException(
                SelectionErrorCode.GROUP_NOT_SAME_REGION, new UserMessage(
                    "assignment.group.error.notAllSameRegion"));
        }

    }

    /**
     * Method to ensure all assignment in a group are updated the same when an
     * assignment was edited from the UI.
     * 
     * @param assignment - assignment that was edited
     * @return the saved assignment
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    @Override
    public Object save(Assignment assignment) throws DataAccessException,
        BusinessRuleException {
        if (assignment.isNew()) {
            calculateAssignmentInfo(assignment);
        }
        if (assignment.getPriority() == null) {
            assignment.setPriority(getDefaultPriority());
        }

        this.getPrimaryDAO().save(assignment);
        // Fix for VLINK-285 Set sequence number to asignment ID. Possible
        // performance impediment on import.
        if (assignment.getSequenceNumber() == -1L) {
            assignment.setSequenceNumber(assignment.getId());
            this.getPrimaryDAO().save(assignment);
        }
        return assignment;
    }

    /**
     * Method to ensure all assignment in a group are updated the same when
     * assignments are Imported.
     * 
     * @param assignments - assignment that was edited
     * @return saved Object
     * @throws DataAccessException - Database Exceptions
     * @throws BusinessRuleException - Business rule exceptions
     */
    @Override
    public Object save(List<Assignment> assignments)
        throws DataAccessException, BusinessRuleException {
        for (Assignment assignment : assignments) {
            if (assignment.isNew()) {
                calculateAssignmentInfo(assignment);
            }
            if (assignment.getPriority() == null) {
                assignment.setPriority(getDefaultPriority());
            }
        }
        this.getPrimaryDAO().save(assignments);
        for (Assignment assignment : assignments) {
            if (assignment.getSequenceNumber() == -1L) {
                assignment.setSequenceNumber(assignment.getId());
            }
        }
        this.getPrimaryDAO().save(assignments);
        return null;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#executeClearReservedAssignments(com.vocollect.voicelink.core.model.Operator)
     */
    public void executeClearReservedAssignments(Operator operator)
        throws DataAccessException {
        List<Assignment> assignments = listReservedAssignments(operator
            .getOperatorIdentifier());

        if (assignments != null) {
            for (Assignment a : assignments) {
                a.setReservedBy(null);
            }
        }

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#executeUpdateLineLoading(com.vocollect.voicelink.selection.model.Assignment)
     */
    public void executeUpdateLineLoading(Assignment a)
        throws DataAccessException {
        Map<String, Integer> cartons = new HashMap<String, Integer>();
        Integer qty;

        // if Assignment is not completed, canceled or shorted exit
        if (!a.getStatus().isInSet(AssignmentStatus.Complete,
            AssignmentStatus.Canceled, AssignmentStatus.Short)) {
            return;
        }

        // Build list of cartons and how much put in each carton
        for (Pick p : a.getPicks()) {
            if (!StringUtil.isNullOrEmpty(p.getCartonNumber())) {
                qty = cartons.get(p.getCartonNumber());
                if (qty == null) {
                    qty = 0;
                }
                cartons.put(p.getCartonNumber(), p.getQuantityPicked() + qty);
            }
        }

        // Loop though and call update carton for expected type tote
        for (Pick p : a.getPicks()) {
            if (!StringUtil.isNullOrEmpty(p.getCartonNumber())) {

                // Check if we should try and update for tote
                qty = cartons.get(p.getCartonNumber());
                if (qty != null) {
                    getCartonManager().executeUpdatePickStatus(
                        p.getCartonNumber(), CartonType.Tote, qty);
                    cartons.remove(p.getCartonNumber());
                }

                // Update for case
                getCartonManager().executeUpdatePickStatus(p.getCartonNumber(),
                    CartonType.Case, p.getQuantityPicked());
            }
        }

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listCustomerNumbers()
     */
    public List<String> listCustomerNumbers() {
        List<Object> data = getPrimaryDAO().listCustomerNumbers();

        ArrayList<String> newList = new ArrayList<String>(data.size());
        for (Object objString : data) {
            String value = (String) objString;
            newList.add(value);
        }
        return newList;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#findLatestDDTByRouteDeliveryDate(java.lang.String, java.util.Date)
     */
    public Date findEarliestDDTByRouteDeliveryDate(String route, Date deliveryDate) {
        Date latestDepartureDate = DateUtil.convertTimeToSiteTime(new Date());
        
        List<Date> departureDateList = getPrimaryDAO()
            .listDDTByRouteDeliveryDate(route, deliveryDate);
        departureDateList.remove(null);
        if (!departureDateList.isEmpty()) {
            latestDepartureDate = departureDateList.get(0);
        }

        return latestDepartureDate;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listRoutes()
     */
    public List<String> listRoutes() {
        AssignmentStatus[] statuses = {AssignmentStatus.Available, AssignmentStatus.Unavailable};
        List<AssignmentStatus> list = Arrays.asList(statuses);
        List<Object> data = getPrimaryDAO().listRoutes(list);

        ArrayList<String> newList = new ArrayList<String>(data.size());
        for (Object objString : data) {
            String value = (String) objString;
            newList.add(value);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#executeUpdatePriority(java.lang.Integer,
     *      java.lang.String, java.lang.Integer, java.util.Date, java.util.Date)
     */
    public int executeUpdatePriority(Integer applyBy,
                                     String applyValue,
                                     Integer priority,
                                     Date startDate,
                                     Date endDate) {
        List<Assignment> assignments;

        if (applyBy == 0) {
            assignments = getPrimaryDAO().listCustomersWithDeliveryDate(
                applyValue, startDate, endDate);
        } else {
            assignments = getPrimaryDAO().listRoutesWithDeliveryDate(
                applyValue, startDate, endDate);
        }

        for (Assignment a : assignments) {
            a.setPriority(priority);
        }

        return assignments.size();
    }

    /**
     * This method is used in a assignment import validation. It finds loading
     * routes that have matching route number, loading region, departure date
     * time and customer number. If one is not found, an import record should
     * fail validation.
     * 
     * @param route - loading route to look for.
     * @param loadingRegion - loading RegionNumber to look for.
     * @param departureDateTime - loading departureDateTime to look for.
     * @param customer - customerNumber to look for
     * @return - loadingRoute found, if any. If loading region and departure
     *         date time fields are not provided a new LoadingRoute object is
     *         returned
     * @throws DataAccessException -.
     */
    public LoadingRoute findLoadingRoute(String route,
                                         LoadingRegion loadingRegion,
                                         Date departureDateTime,
                                         Customer customer)
        throws DataAccessException {
        if ((loadingRegion == null || loadingRegion.getNumber() == null || loadingRegion
            .getNumber().intValue() == 0)) {
            return new LoadingRoute();
        }

        return loadingRouteManager.findLoadingRoute(route,
            loadingRegion.getNumber(), departureDateTime,
            customer.getCustomerNumber());
    }
    
    @Override
    public List<Object[]> listRouteAndDepartureDateByOperator(Operator operator) throws DataAccessException {
        return this.getPrimaryDAO().listRouteAndDepartureDateByOperator(operator);
    }
    
    @Override
    public List<Object[]> listRouteAndDepartureDateByRegion(Long operatorCurrentRegion) throws DataAccessException {
           return this.getPrimaryDAO().listRouteAndDepartureDateByRegion(operatorCurrentRegion);
    }
    
    
    @Override
    public List<Object[]> listRouteAndDepartureDateByLatestEndTime(Long operatorCurrentRegion,
                                                        Operator operator)
        throws DataAccessException {
        return this.getPrimaryDAO().listRouteAndDepartureDateByLatestEndTime(operatorCurrentRegion, operator);
    }
    
    @Override
    public List<Map<String, Object>> listDistinctRouteAndDeliveryDate() throws DataAccessException {
        return this.getPrimaryDAO().listDistinctRouteAndDeliveryDate();
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listAssignmentsByRouteDeliveryDate(java.lang.String, java.util.Date)
     */
    public List<Assignment> listAssignmentsByRouteDeliveryDate(String route, Date deliveryDate) {
        List<Assignment> assignments = this.getPrimaryDAO().listAssignmentsByRouteDeliveryDate(route, deliveryDate);
        return assignments;
    }
    
    // Methods used by Route Aggregator and Route Assignment Status Aggregator

    /**
     * 
     * {@inheritDoc}
     * @throws DataAccessException 
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#getRouteDepartureDateField(java.lang.String,
     *      java.util.Date)
     */
    @Override
    public String getRouteDepartureDateField(String route, Date departureDate) throws DataAccessException {        
        String datestr = new DisplayUtilities().formatTimeWithTimeZone(DateUtil.convertTimeToServerTime(departureDate),
                null);
        
        return route + " @ " + datestr;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listAssignmentCountByRouteDDTAndStatus(java.util.Date,
     *      java.util.Date, java.util.List)
     */
    @Override
    public List<Object[]> listAssignmentCountByRouteDDTAndStatus(Date startDate,
                                                      Date endDate,
                                                      List<AssignmentStatus> statuses)
        throws DataAccessException {
        return this.getPrimaryDAO().listAssignmentCountByRouteDDTAndStatus(startDate,
            endDate, statuses);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listTotalQuantityAvailablePerRouteDDTByRegion(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<Object[]> listTotalQuantityAvailablePerRouteDDTByRegion(Date startDate,
                                                                        Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO()
            .listTotalQuantityAvailablePerRouteDDTByRegion(startDate, endDate);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listQuantityPickedPerRouteDDT(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<Object[]> listQuantityPickedPerRouteDDT(Date startDate,
                                                        Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listQuantityPickedPerRouteDDT(startDate,
            endDate);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listTotalQuantityPerRouteDDT(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<Object[]> listTotalQuantityPerRouteDDT(Date startDate,
                                                       Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listTotalQuantityPerRouteDDT(startDate,
            endDate);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listOperatorsByRouteRegion(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<Object[]> listOperatorsByRouteRegion(Date startDate,
                                                     Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listOperatorsByRouteRegion(startDate,
            endDate);
    }

    //Also used by Region DA
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listOperatorsInRegion()
     */
    @Override
    public List<Operator> listOperatorsInRegion() {
        return this.getPrimaryDAO().listOperatorsInRegion();
    }
    
    // Infinite region DA Methods

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listTotalItemsRemainingForAllRegions()
     */
    @Override
    public List<Object[]> listTotalItemsRemainingForAllRegions()
        throws DataAccessException {
        return this.getPrimaryDAO().listTotalItemsRemainingForAllRegions();
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listQtyPickedCasesChartData(java.util.Date)
     */
    @Override
    public List<Object[]> listQtyPickedCasesChartData(Date shiftStartDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listQtyPickedCasesChartData(shiftStartDate);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listQtyRemainingCasesChartData()
     */
    @Override
    public List<Object[]> listQtyRemainingCasesChartData()
        throws DataAccessException {
        return this.getPrimaryDAO().listQtyRemainingCasesChartData();
    }
    
    // Region Assignment Status DA Methods

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listAssignmentsByDateAndStatusInAllRegions(java.util.Date,
     *      java.util.Date, java.util.List)
     */
    @Override
    public List<Map<String, Object>> listAssignmentsByDateAndStatusInAllRegions(Date startDate,
                                                                                Date endDate,
                                                                                List<AssignmentStatus> statuses)
        throws DataAccessException {
        return this.getPrimaryDAO().listAssignmentsByDateAndStatusInAllRegions(
            startDate, endDate, statuses);
    }
    
    //Region DA

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.AssignmentManagerRoot#listAllQuantitiesByRegionDepartureDate(java.util.Date,
     *      java.util.Date)
     */
    @Override
    public List<Map<String, Object>> listAllQuantitiesByRegionDepartureDate(Date startDate,
                                                                            Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listAllQuantitiesByRegionDepartureDate(
            startDate, endDate);
    }
    
    
    @Override
    public List<Map<String, Object>> listAllOperatorsInRoutes()
        throws DataAccessException {
        return this.getPrimaryDAO().listAllOperatorsInRoutes();
    }
    
    @Override
    public List<Map<String, Object>> listAllRouteByRegion() throws DataAccessException {
        return this.getPrimaryDAO().listAllRouteByRegion();
    }
    
    @Override
    public List<Map<String, Object>> listRouteByLatestEndTime() throws DataAccessException {
        return this.getPrimaryDAO().listRouteByLatestEndTime();
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 