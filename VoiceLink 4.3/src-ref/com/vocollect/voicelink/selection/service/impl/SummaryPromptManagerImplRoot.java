/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.SystemTranslationManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.LocaleAdapter;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.selection.dao.SummaryPromptDAO;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.SummaryPrompt;
import com.vocollect.voicelink.selection.model.SummaryPromptCharactersToSpeak;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinition;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinitionLocale;
import com.vocollect.voicelink.selection.model.SummaryPromptItem;
import com.vocollect.voicelink.selection.model.SummaryPromptItemFieldType;
import com.vocollect.voicelink.selection.service.SummaryPromptManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.exception.ConstraintViolationException;


/**
 * Additional service methods for the <code>SummaryPrompt</code> model object.
 *
 */
public abstract class SummaryPromptManagerImplRoot 
    extends GenericManagerImpl<SummaryPrompt, SummaryPromptDAO> implements
    SummaryPromptManager {

    private static final long TEXT = -1L;
    private static final long POSITION = -2L;
    private static final long WORKID = -3L;

    private static final long TOTAL_CUBE = -8L;
    private static final long TOTAL_WEIGHT = -9L;
    private static final long GOALTIME = -10L;
    private static final long ASSIGNMENT_NUMBER = -11L;


    private static final int ONE_BOX = 1;
    private static final int TWO_BOX = 2;
    private static final int THREE_BOX = 3;
    
    private static final Double ZERO_DOUBLE_VALUE = 0.0;
    private static final Integer ZERO_INTEGER_VALUE = 0;

    private static final Double SINGLE_DOUBLE_VALUE = 1.0;
    private static final Integer SINGLE_INTEGER_VALUE = 1;
    
    private static final Double PLURAL_DOUBLE_VALUE = 3.0;
    private static final Integer PLURAL_INTEGER_VALUE = 3;
    
    private GenericDAO<SummaryPromptItem> summaryPromptItemDAO;
    
    private SystemTranslationManager systemTranslationManager; 
    
    
    /**
     * @param rdi - Information (such as sorting) regarding how to retreive the data
     * @return A list of all objects
     * @throws DataAccessException on any failure
    */
    public List<SummaryPrompt> getAll(ResultDataInfo rdi) throws DataAccessException {
        return getPrimaryDAO().listSummaryPrompts(new QueryDecorator(rdi));
    }
    
    
    /**
     * Getter for the systemTranslationManager property.
     * @return SystemTranslationManager value of the property
     */
    public SystemTranslationManager getSystemTranslationManager() {
        return systemTranslationManager;
    }
    
    /**
     * Setter for the systemTranslationManager property.
     * @param systemTranslationManager the new systemTranslationManager value
     */
    public void setSystemTranslationManager(SystemTranslationManager systemTranslationManager) {
        this.systemTranslationManager = systemTranslationManager;
    }



    /**
     * Getter for the summaryPromptItemDAO property.
     * @return GenericDAO&lt;SummaryPromptItem&gt; value of the property
     */
    public GenericDAO<SummaryPromptItem> getSummaryPromptItemDAO() {
        return summaryPromptItemDAO;
    }


    
    /**
     * Setter for the summaryPromptItemDAO property.
     * @param summaryPromptItemDAO the new summaryPromptItemDAO value
     */
    public void setSummaryPromptItemDAO(GenericDAO<SummaryPromptItem> summaryPromptItemDAO) {
        this.summaryPromptItemDAO = summaryPromptItemDAO;
    }


    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager.
     */
    public SummaryPromptManagerImplRoot(SummaryPromptDAO primaryDAO) {
        super(primaryDAO);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#getDefaultSummaryPrompt()
     */
    public SummaryPrompt getDefaultSummaryPrompt() throws DataAccessException {
        SummaryPrompt sp = new SummaryPrompt();
        SummaryPromptDefinition spd = null;
        int position = 1;
        
        //Position
        spd = new SummaryPromptDefinition();
        spd.setPromptItem(getSummaryPromptItemDAO().get(POSITION));
        if (spd.getPromptItem() != null) {
            spd.setPosition(position++);
            spd.setPromptValue1("");
            spd.setPromptValue2(ResourceUtil.getLocalizedKeyValue(
                "selection.summarypromptdefault.position"));
            spd.setPromptValue3("");
            spd.setSummaryPrompt(sp);
            sp.getDefinitions().add(spd);
            
        }
        
        //Work ID
        spd = new SummaryPromptDefinition();
        spd.setPromptItem(getSummaryPromptItemDAO().get(WORKID));
        if (spd.getPromptItem() != null) {
            spd.setPosition(position++);
            spd.setPromptValue1(ResourceUtil.getLocalizedKeyValue(
                "selection.summarypromptdefault.workId"));
            spd.setPromptValue2("");
            spd.setPromptValue3("");
            spd.setSummaryPrompt(sp);
            sp.getDefinitions().add(spd);
        }
        
        //Goal Time
        spd = new SummaryPromptDefinition();
        spd.setPromptItem(getSummaryPromptItemDAO().get(GOALTIME));
        if (spd.getPromptItem() != null) {
            spd.setPosition(position++);
            spd.setPromptValue1("");
            spd.setPromptValue2(ResourceUtil.getLocalizedKeyValue(
                "selection.summarypromptdefault.goalTimeSingle"));
            spd.setPromptValue3(ResourceUtil.getLocalizedKeyValue(
                "selection.summarypromptdefault.goalTimeMultiple"));
            spd.setSummaryPrompt(sp);
            sp.getDefinitions().add(spd);
        }
        
        //Text
        spd = new SummaryPromptDefinition();
        spd.setPromptItem(getSummaryPromptItemDAO().get(TEXT));
        if (spd.getPromptItem() != null) {
            spd.setPosition(position++);
            spd.setPromptValue1(ResourceUtil.getLocalizedKeyValue(
                "selection.summarypromptdefault.sayReady"));
            spd.setPromptValue2("");
            spd.setPromptValue3("");
            spd.setSummaryPrompt(sp);
            sp.getDefinitions().add(spd);
        }
        
        return sp;
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#buildSamplePrompts(com.vocollect.voicelink.selection.model.SummaryPrompt, java.util.Locale)
     */
    public List<String> buildSamplePrompts(SummaryPrompt sp,
        Locale locale) {
        List<String> prompts = new ArrayList<String>();
        
        String prompt = "";
        
        //Blank or zero prompt
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            if (spd.getPromptItem().getId().equals(POSITION)) {
                prompt = prompt + buildNumericPart(spd, locale, 1, 1);
            } else if (spd.getPromptItem().getFieldType()
                .equals(SummaryPromptItemFieldType.String)) {
                if (spd.getPromptItem().getEditBoxCount() == 1) {
                    prompt = prompt + buildStringPart(spd, locale, "1234567890");
                } else {
                    prompt = prompt + buildStringPart(spd, locale, "");
                }
            } else  {
                if (spd.getPromptItem().getId().equals(TOTAL_CUBE)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, ZERO_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(TOTAL_WEIGHT)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, ZERO_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(GOALTIME)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, ZERO_DOUBLE_VALUE);
                } else {
                    prompt = prompt + buildNumericPart(spd, locale, 1, ZERO_INTEGER_VALUE);
                }
            }
        }
        prompts.add(prompt);
        
        //Single and not blank
        prompt = "";
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            if (spd.getPromptItem().getId().equals(POSITION)) {
                prompt = prompt + buildNumericPart(spd, locale, 1, 1);
            } else if (spd.getPromptItem().getFieldType()
                .equals(SummaryPromptItemFieldType.String)) {
                prompt = prompt + buildStringPart(spd, locale, "1234567890");
            } else  {
                if (spd.getPromptItem().getId().equals(TOTAL_CUBE)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, SINGLE_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(TOTAL_WEIGHT)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, SINGLE_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(GOALTIME)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, SINGLE_DOUBLE_VALUE);
                } else {
                    prompt = prompt + buildNumericPart(spd, locale, 1, SINGLE_INTEGER_VALUE);
                }
            }
        }
        prompts.add(prompt);

        
        //Multiple and not blank
        prompt = "";
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            if (spd.getPromptItem().getId().equals(POSITION)) {
                prompt = prompt + buildNumericPart(spd, locale, 2, 1);
            } else if (spd.getPromptItem().getFieldType()
                .equals(SummaryPromptItemFieldType.String)) {
                prompt = prompt + buildStringPart(spd, locale, "1234567890");
            } else  {
                if (spd.getPromptItem().getId().equals(TOTAL_CUBE)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, PLURAL_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(TOTAL_WEIGHT)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, PLURAL_DOUBLE_VALUE);
                } else if (spd.getPromptItem().getId().equals(GOALTIME)) {
                    prompt = prompt + buildNumericPart(spd, locale, 1, PLURAL_DOUBLE_VALUE);
                } else {
                    prompt = prompt + buildNumericPart(spd, locale, 1, PLURAL_INTEGER_VALUE);
                }
            }
        }
        prompts.add(prompt);
        
        return prompts;
    }
    
     
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#buildSummaryPrompt(com.vocollect.voicelink.selection.model.Assignment, java.util.List, java.util.Locale)
     */
    public String buildSummaryPrompt(Assignment assignment, 
                                     List<Assignment> assignments,
                                     Locale locale) {
        String prompt = "";
        Object value = null;
        SummaryPrompt sp = assignment.getRegion()
            .getUserSettings(assignment.getType()).getSummaryPrompt();
        
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            
            //If text then do nothing and set value to empty string
            if (spd.getPromptItem().getId().equals(TEXT)) {
                value = "";
                
            //If a group total loop thorugh and add numbers together for each 
            //assignment. Expecting and Integer or Double returned.
            } else if (spd.getSpeakTotalsIndicator() == 1) {
                Object partValue = 0;
                value = 0;

                if (assignment.getGroupInfo().getGroupPosition() == 1) {
                    for (Assignment a : assignments) {
                        try {
                            partValue = PropertyUtils.getNestedProperty(a, 
                                spd.getPromptItem().getAssignmentProperty());
                            
                            if (partValue instanceof Double) {
                                value = ((Double) value) + ((Double) partValue); 
                            } else if (partValue instanceof Integer) {
                                value = ((Integer) value) + ((Integer) partValue);
                            }
                        } catch (Exception e) {
                            //do nothing
                        }
                    }
                }
                
            //Else get value from property
            } else {
                try {
                    value = PropertyUtils.getNestedProperty(assignment, 
                        spd.getPromptItem().getAssignmentProperty());
                } catch (Exception e) {
                    value = null;
                }
            }
            
            //Build prompt value
            //Assignment number is in exception since it is stored as a long,
            //but treated as a string.
            if (spd.getPromptItem().getId() == ASSIGNMENT_NUMBER) {
                if (value == null) {
                    value = "";
                }
                prompt = prompt + buildStringPart(spd, locale, 
                                                ((Long) value).toString());
            } else if (spd.getPromptItem().getFieldType()
                .equals(SummaryPromptItemFieldType.String)) {
                
                if (value == null) {
                    value = "";
                }
                prompt = prompt + buildStringPart(spd, locale, (String) value);
            } else  {
                
                if (value == null) {
                    value = 0;
                }

                if (!(spd.getSpeakTotalsIndicator() == 1 
                    && assignment.getGroupInfo().getGroupPosition() != 1)) {
                    prompt = prompt + buildNumericPart(spd, locale, 
                        assignment.getGroupInfo().getGroupCount(), (Number) value);
                }
            }
                
        }
        
        return prompt;
    }

    /**
     * Find the correct prompt part to speak based on a string value and build 
     * prompt string.
     * 
     * @param prompt - Summary prompt item.
     * @param locale - locale to get prompt for
     * @param value - string value to add to prompt
     * @return - prompt string.
     */
    protected String buildStringPart(SummaryPromptDefinition prompt, 
                                     Locale locale,
                                     String value) {
        String promptString;
        
        //Get the left or right number of charactres
        if (prompt.getSpeakCharactersIndicator()
            .equals(SummaryPromptCharactersToSpeak.Right)) {
            if (value.length() > prompt.getNumberOfCharacters()) {
                value = value.substring(value.length() 
                    - prompt.getNumberOfCharacters());
            }
        } else if (prompt.getSpeakCharactersIndicator()
            .equals(SummaryPromptCharactersToSpeak.Left)) {
            if (value.length() > prompt.getNumberOfCharacters()) {
                value = value.substring(0, prompt.getNumberOfCharacters());
            }
        }
        
        //Change to phonetic value
        if (prompt.isAllowSpeakingPhonetically()) {
            value = createPhoneticString(value);
        }
        
        //trim any spaces
        value = value.trim();

        //Get prompt string to add
        if (prompt.getPromptItem().getEditBoxCount() == ONE_BOX) {
            promptString = prompt.getPromptValue1(locale);
        } else {
            if (StringUtil.isNullOrEmpty(value.trim())) {
                promptString = prompt.getPromptValue1(locale);
            } else {
                promptString = prompt.getPromptValue2(locale);
            }
        }
        
        //Check for null prompt string
        if (StringUtil.isNullOrEmpty(promptString)) {
            promptString = "";
        }
        
        return promptString.replace("{}", value);
    }
    

    /**
     * Build string part for a specified value. 
     * 
     * @param prompt - prompt item to build
     * @param locale - locale to get prompt for
     * @param postions - number of postions in group
     * @param value - value to insert
     * @return - prompt string for value.
     */
    protected String buildNumericPart(SummaryPromptDefinition prompt,
                                      Locale locale,
                                      int postions,
                                      Number value) {
        String promptString = "";
        String promptValue;
        
        //Convert to string value
        if (value instanceof Double) {
            promptValue = new BigDecimal(value.doubleValue())
            .setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
        } else {
            promptValue = new Integer(value.intValue()).toString();
        }
        
        //Change to phonetic value
        if (prompt.isAllowSpeakingPhonetically()) {
            promptValue = createPhoneticString(promptValue);
        }
        
        //trim any spaces
        promptValue = promptValue.trim();

        //Speacial condition for position
        if (prompt.getPromptItem().getId() == POSITION) {
            if (postions > 1) {
                promptString = prompt.getPromptValue2(locale);
            } else {
                promptString = prompt.getPromptValue1(locale);
            }
        //Get prompt string to add
        } else if (prompt.getPromptItem().getEditBoxCount() == ONE_BOX) {
            promptString = prompt.getPromptValue1(locale);
        } else if (prompt.getPromptItem().getEditBoxCount() == TWO_BOX) {
            if (value.equals(1.0)) {
                promptString = prompt.getPromptValue1(locale);
            } else {
                promptString = prompt.getPromptValue2(locale);
            }
        } else if (prompt.getPromptItem().getEditBoxCount() == THREE_BOX) {
            if (value.equals(0.0) || value.equals(0)) {
                promptString = prompt.getPromptValue1(locale);
            } else if (value.equals(1.0) || value.equals(1)) {
                promptString = prompt.getPromptValue2(locale);
            } else {
                promptString = prompt.getPromptValue3(locale);
            }
        }
        
        //Check for null prompt string
        if (StringUtil.isNullOrEmpty(promptString)) {
            promptString = "";
        }

        return promptString.replace("{}", promptValue);
    }

    
    /**
     * inserts spaces between each character of the string. This method
     * is typically used for sending values to the task the the user wishes
     * to have spoken phonetically instead of as value. For example if the value
     * was 30, then without spaces it would be spoken as thirty, with spaces it
     * would be spoken as three zero.
     * 
     * @param data - string to include spaces into.
     * @return - return string with inserted spaces
     */
    protected String createPhoneticString(String data) {

        //if null string passed in the return null back
        if (data == null) {
            return null;
        } else {
            StringBuilder buffer = new StringBuilder();

            //loop through each charater and add space
            for (char c : data.toCharArray()) {
                buffer.append(c);
                buffer.append(' ');
            }

            return buffer.toString().trim();
        }
        
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#getNotyetDefinedLocales(com.vocollect.voicelink.selection.model.SummaryPrompt)
     */
    public List<Locale> getNotYetDefinedLocales(SummaryPrompt sp) 
    throws DataAccessException {
        List<Locale> locales = 
            getSystemTranslationManager().getAllSupportedLocales();
        List<Locale> defined = sp.listLocalesWithDefinitions();
        
        //Removed locales that are currently defined
        for (Locale l : defined) {
            locales.remove(l);
        }

        
        Collections.sort(locales, LocaleAdapter.LOCALE_COMPARATOR);
        return locales;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#deleteLocale(com.vocollect.voicelink.selection.model.SummaryPrompt, java.util.Locale)
     */
    public SummaryPrompt executeDeleteLocale(SummaryPrompt sp, Locale locale)
    throws DataAccessException,   BusinessRuleException {
        
        //loop through all items defintions to see if locale defined.
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            SummaryPromptDefinitionLocale deleteDef = null;

            //See if locale exists in definition
            for (SummaryPromptDefinitionLocale spdl : spd.getLocaleDefintions()) {
                if (spdl.getLocale().equals(locale)) {
                    deleteDef = spdl;
                }
            }

            //if found, then delete
            if (deleteDef != null) {
                spd.getLocaleDefintions().remove(deleteDef);
                this.save(sp);
            }
        }
        
        return sp;
    }
    
    
    /**
     * {@inheritDoc}
     * @throws DataAccessException 
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#localeExistsInPromts(com.vocollect.voicelink.selection.model.SummaryPrompt, java.util.Locale)
     */
    public boolean localeExistsInPrompts(Locale locale) throws DataAccessException {
        
        List<SummaryPrompt> summaryPrompts = getAll();
        
        for (SummaryPrompt sp : summaryPrompts) {
        
        //loop through all items defintions to see if locale defined.
        for (SummaryPromptDefinition spd : sp.getDefinitions()) {
            SummaryPromptDefinitionLocale deleteDef = null;

            //See if locale exists in definition
            for (SummaryPromptDefinitionLocale spdl : spd.getLocaleDefintions()) {
                if (spdl.getLocale().equals(locale)) {
                    deleteDef = spdl;
                }
            }
            if (deleteDef != null) {
              return false;
            }
          }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SummaryPromptManagerRoot#executeSaveEdit(com.vocollect.voicelink.selection.model.SummaryPrompt, java.util.List)
     */
    public SummaryPrompt executeSaveEdit(SummaryPrompt sp, 
                               List<SummaryPromptDefinition> definitions) 
    throws BusinessRuleException, DataAccessException {
        List<Locale> locales = sp.listLocalesWithDefinitions();
        
        //loop through new defintions and create locale defitions from
        //original prompt def or create new ones for new prompt defs.
        for (SummaryPromptDefinition spd : definitions) {
            if (spd.getTempId() == null) {
                //Add all predefined locales to new prompt item
                addDefaultLocales(locales, spd);
            } else {
                //Find original prompt to copy locale information
                SummaryPromptDefinition temp = null;
                for (SummaryPromptDefinition spdOrig : sp.getDefinitions()) {
                    if (spdOrig.getId() != null 
                        && spdOrig.getId().equals(spd.getTempId())) {
                         temp = spdOrig;
                         break;
                    }
                }
                
                //not found in original defintion (should never happen)
                if (temp == null) {
                    addDefaultLocales(locales, spd);
                //If found copy all locale defintions
                } else {
                    for (SummaryPromptDefinitionLocale spdl : temp.getLocaleDefintions()) {
                        SummaryPromptDefinitionLocale spdlCopy = 
                            new SummaryPromptDefinitionLocale();
                        spdlCopy.setLocale(spdl.getLocale());
                        spdlCopy.setPromptValue1(spdl.getPromptValue1());
                        spdlCopy.setPromptValue2(spdl.getPromptValue2());
                        spdlCopy.setPromptValue3(spdl.getPromptValue3());
                        spdlCopy.setSummaryPromptDefinition(spd);
                        spd.getLocaleDefintions().add(spdlCopy);
                        
                    }
                }
                
            }
        }
        
        //Remove all original defitions
        sp.getDefinitions().clear();
        
        //Add all new definitions
        for (SummaryPromptDefinition spd : definitions) {
            spd.setSummaryPrompt(sp);
            sp.getDefinitions().add(spd);
        }
        
        //Save object
        save(sp);
        
        return sp;
    }

    /**
     * create default locales for a summary prompt defintion.
     * 
     * @param locales - list of locales to create
     * @param spd - prompt definition to add them to
     */
    protected void addDefaultLocales(List<Locale> locales, 
                                     SummaryPromptDefinition spd) {
        for (Locale locale : locales) {
            SummaryPromptDefinitionLocale spdl = 
                new SummaryPromptDefinitionLocale();
            spdl.setLocale(locale);
            spdl.setPromptValue1(spd.getPromptValue1());
            spdl.setPromptValue2(spd.getPromptValue2());
            spdl.setPromptValue3(spd.getPromptValue3());
            spdl.setSummaryPromptDefinition(spd);
            spd.getLocaleDefintions().add(spdl);
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(SummaryPrompt instance)
        throws BusinessRuleException, DataAccessException {
        try {
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("summaryPrompt.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
    }    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 