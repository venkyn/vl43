/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.selection.dao.AssignmentImportDataDAO;
import com.vocollect.voicelink.selection.dao.PickImportDataDAO;
import com.vocollect.voicelink.selection.model.AssignmentImportData;
import com.vocollect.voicelink.selection.service.AssignmentImportDataManager;

import java.util.List;

/**
 * @author dgold
 *
 */
public abstract class AssignmentImportDataManagerImplRoot extends
        GenericManagerImpl<AssignmentImportData, AssignmentImportDataDAO> implements AssignmentImportDataManager {

    private static final int RECORDS_TO_QUERY = 10;
    
    private PickImportDataDAO pickImportDataDAO = null;
    
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public AssignmentImportDataManagerImplRoot(AssignmentImportDataDAO primaryDAO) {
        super(primaryDAO);
    }


    
    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    public List<AssignmentImportData> listAssignmentBySiteName(String siteName)
            throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(RECORDS_TO_QUERY);
        return getPrimaryDAO().listAssignmentBySiteName(qd, siteName);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
        getPickImportDataDAO().updateToComplete();
    }



    /**
     * Gets the value of pickImportDataDAO.
     * @return the pickImportDataDAO
     */
    public PickImportDataDAO getPickImportDataDAO() {
        return pickImportDataDAO;
    }



    /**
     * Sets the value of the pickImportDataDAO.
     * @param pickImportDataDAO the pickImportDataDAO to set
     */
    public void setPickImportDataDAO(PickImportDataDAO pickImportDataDAO) {
        this.pickImportDataDAO = pickImportDataDAO;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 