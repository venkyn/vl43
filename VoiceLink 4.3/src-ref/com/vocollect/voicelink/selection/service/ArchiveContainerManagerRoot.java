/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.selection.dao.ArchiveContainerDAO;
import com.vocollect.voicelink.selection.model.ArchiveContainer;
import com.vocollect.voicelink.selection.model.ContainerReport;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for archive container related operations.
 *
 * @author mnichols
 */
public interface ArchiveContainerManagerRoot extends
    GenericManager<ArchiveContainer, ArchiveContainerDAO>, DataProvider {

    /**
     * Gets a list of archive container to populate in Container modified report
     * @param queryDecorator - the query decorator
     * @param containerNumber - container number added by the user
     * @param operatorIds - operator id selected by the user
     * @param startDate - start date entered
     * @param endDate - end date entered
     * @param siteId - site id
     * @return list of containers
     * @throws DataAccessException
     */
    List<ContainerReport> listArchiveContainersForContainerReport(QueryDecorator queryDecorator,
                                                                   String containerNumber,
                                                                   Set<String> operatorIds,
                                                                   Date startDate,
                                                                   Date endDate,
                                                                   Long siteId)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 