/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.selection.dao.SelectionRegionDAO;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegion;

import java.util.List;
import java.util.Map;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick-related operations.
 * 
 * @author ddoubleday
 */
public interface SelectionRegionManagerRoot extends
    GenericManager<SelectionRegion, SelectionRegionDAO>, DataProvider {

    /**
     * Validate the business rules for editing a selection region.
     * @param region - region to edit
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    void executeValidateEditRegion(SelectionRegion region)
        throws DataAccessException, BusinessRuleException;

    /**
     * Get a list of profile number of UI combo list that are valid based on
     * passed in assignment type and current profile.
     * 
     * @param assignmentType - Type og profiles to retrieve
     * @param selectionRegionID - current region (may be null when adding a new
     *            region)
     * @return - list of profile numbers
     * @throws DataAccessException - database exception
     */
    public Map<String, Long> getRegionProfileNumbers(AssignmentType assignmentType,
                                                Long selectionRegionID)
        throws DataAccessException;


    
    /**
     * Find the selection region, given the region number. For Import.
     * @param regionNumber reference to the region
     * @return the selection region referenced, or null if not found
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    public SelectionRegion findRegionByNumber(int regionNumber) 
    throws DataAccessException, BusinessRuleException;
    
    
    /**
     * Get the list of summary objects for assignment summary.
     * 
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listAssignmentSummary(ResultDataInfo rdi)     
    throws DataAccessException;

    /**
     * Get the list of summary objects for current work summary.
     * 
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)     
    throws DataAccessException;

    /**
     * Get the list of summary objects for shorts summary.
     * 
     * @param rdi - Result Data info.
     * @return - list of summary objects.
     * @throws DataAccessException - Database exceptions.
     */
    public List<DataObject> listShortsSummary(ResultDataInfo rdi)     
    throws DataAccessException;

    /**
     * Get all selection regions ordered by region number.
     * @return ordered list of selection region
     * @throws DataAccessException - Database exceptions.
     */
    List<SelectionRegion> listAllRegionsOrderByNumber()
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 