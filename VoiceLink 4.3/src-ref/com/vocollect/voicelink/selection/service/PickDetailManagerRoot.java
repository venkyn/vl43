/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.selection.dao.PickDetailDAO;
import com.vocollect.voicelink.selection.model.PickDetail;

import java.util.Date;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick-related operations.
 * 
 * @author ddoubleday
 */
public interface PickDetailManagerRoot extends 
    GenericManager<PickDetail, PickDetailDAO>, DataProvider {
    
    /**
     * Sum the quantity picked field of the pick detail 
     * records for the specified assignment that fall between
     * the given startTime and endTime.
     * 
     * @param assignmentId - the assignment.
     * @param startTime - the star time.
     * @param endTime = the end time.
     * 
     * @return sum of quantity picked for assignment.
     * @throws DataAccessException - database exceptions
     */
     int sumQuantityPicked(long assignmentId, Date startTime, Date endTime) 
                               throws DataAccessException;
    
    /**
     * sum the quantity picked field for the specified assignment for pick 
     * detail records that fall between the start and end time.
     * for VoiceLink this method does the same thing as sumQuantityPicked
     * but is here to allow for easy customization.
     *   
     * @param assignmentId - assignnment to use.
     * @param startTime - start time
     * @param endTime - end time
     * 
     * @return the sum of the quantity picked field that meets the 
     * the criteria
     * 
     * @throws DataAccessException - database exceptions
     */
     int sumAssignmentProrateCount(long assignmentId, Date startTime, Date endTime) 
                                       throws DataAccessException;

     /**
      * sum the quantity picked field for the specified operator for pick 
      * detail records that fall between the start and end time.
      *   
      * @param groupNumber - group number of the assignment
      * @param operatorId - Operator Id.
      * @param startTime - start time
      * @param endTime - end time
      * 
      * @return the sum of the quantity picked field that meets the 
      * the criteria
      * 
      * @throws DataAccessException - database exceptions
      */
      int sumGroupProrateCount(long groupNumber, long operatorId, Date startTime, Date endTime) 
                                   throws DataAccessException;
   
      
      
      /**
       * Find the time of the last pick detail record for the given operator
       * that is greater than or equal the minTime. 
       *   
       * @param operatorId - the operator to search.
       * @param minTime - the date/time returned must be greater or equal to minTime
       * 
       * @return Date - the maximum end time from the aassignment labor record. 
       * 
       * @throws DataAccessException - database exceptions
       */
       Date maxEndTime(long operatorId, Date minTime, Date maxTime) throws DataAccessException;
       
       
       /**
        * Find the time of the latest/closest pick detail record for the given operator
        * that is greater than or equal the maxTime. 
        *   
        * @param operatorId - the operator to search.
        * @param maxTime - the date/time returned must be greater or equal to minTime
        * 
        * @return Date - the closest pick detail record. 
        * 
        * @throws DataAccessException - database exceptions.
        */
       Date minPickTime(long operatorId, Date maxTime) throws DataAccessException;
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 