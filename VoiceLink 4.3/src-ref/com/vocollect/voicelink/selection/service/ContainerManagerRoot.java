/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.selection.dao.ContainerDAO;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerReport;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick-related operations.
 *
 * @author mkoenig
 */
public interface ContainerManagerRoot extends
    GenericManager<Container, ContainerDAO>, DataProvider {

    /**
     * retrieves a container by the container number.
     *
     * @param partialContainerNumber - value to retrieve by
     * @return the container object
     * @throws DataAccessException - database exceptions
     */
    Container findContainerByNumber(String partialContainerNumber)
        throws DataAccessException;

    /**
     * retrieves a container by the last x digits.
     *
     * @param containerNumber - value to retrieve by
     * @return the container object
     * @throws DataAccessException - database exceptions
     */
    Container findContainerByPartialNumber(String containerNumber)
        throws DataAccessException;

    /**
     * retrieves a list of all containers.
     *
     * @return the container list
     * @throws DataAccessException - database exceptions
     */
    List<Container> listAllContainers()
        throws DataAccessException;

    /**
     * @param queryDecorator - the query decorator
     * @param containerNumber - container number selected by user
     * @param operatorIdentifiers - set of operator identifiers selected by user
     * @param startTime - start Time entered
     * @param endTime - end time entered
     * @return list of containers
     * @throws DataAccessException - database exceptions
     */
    List<ContainerReport> listContainersForContainerReport(QueryDecorator queryDecorator,
                                                           String containerNumber,
                                                           Set<String> operatorIdentifiers,
                                                           Date startTime,
                                                           Date endTime)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 