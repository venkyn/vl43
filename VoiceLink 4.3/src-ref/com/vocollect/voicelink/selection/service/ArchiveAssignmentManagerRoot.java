/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.selection.dao.ArchiveAssignmentDAO;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentStatus;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Assignment-related operations.
 *
 * @author ddoubleday
 */
public interface ArchiveAssignmentManagerRoot extends
    GenericManager<ArchiveAssignment, ArchiveAssignmentDAO>, DataProvider {

    /**
     * deletes archive assignments.
     *
     * @param createdDate - date to check for
     * @param status - status of assignments
     * @return - Number of row deleted
     * @throws DataAccessException - Database Access Exceptions
     */
    Integer updateOlderThan(Date createdDate,
        AssignmentStatus status) throws DataAccessException;



    /**
     * Method for generating Assignment Report.
     *
     * @param queryDecorator - the query decorator for the sql
     * @param workId - WorkID of the assignment.
     * @param operatorId - Set of Operator IDs.
     * @param assignmentNumber - Assignment Number.
     * @param customerNumber - Customer Number.
     * @param route - Route of the Assignment.
     * @param startTime - Start Time of the Report.
     * @param endTime - End Time of the Report.
     * @param siteId - Site in which the Report is being generated.
     * @return List<ArchiveAssignment> - List of Archive Assignments.
     * @throws DataAccessException
     */
    List<AssignmentReport> listArchiveAssignmentsForAssignmentReport(QueryDecorator queryDecorator,
                                                                      String workId,
                                                                      Set<String> operatorId,
                                                                      Long assignmentNumber,
                                                                      String customerNumber,
                                                                      String route,
                                                                      Date startTime,
                                                                      Date endTime,
                                                                      Long siteId)
        throws DataAccessException;


    /**
     * Purges assignments based on date and status.
     *
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of assignments purged
     */
    public int executePurge(AssignmentStatus status,
                            Date olderThan);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 