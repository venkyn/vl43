/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.selection.dao.PickDAO;
import com.vocollect.voicelink.selection.model.Pick;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick-related operations.
 * 
 * @author ddoubleday
 */
public interface PickManagerRoot extends 
    GenericManager<Pick, PickDAO>, DataProvider {
    
    /**
     * retrieves a list of picks from assignments with the specified group number.
     * 
     * @param groupNumber - value to retrieve by
     * @return list of requested picks, list may be empty
     * @throws DataAccessException - database exceptions
     */
    List<Pick> listGroupsPicks(Long groupNumber) 
        throws DataAccessException;

    /**
     * Manual pick validation business logic.
     * @param p The pick.
     * @throws BusinessRuleException on business rule violation.
     */
    public void validateIfManualPickAllowed(Pick p)
        throws BusinessRuleException;
    
    /**
     * Find shorts to automarkout for a specified location.
     * 
     * @param location - location to find for
     * @return - list of picks to auto mark out
     * @throws DataAccessException - Database Exception
     */
    public List<Pick> listShortsForLocation(Location location) 
        throws DataAccessException;

    /**
     * Find shorts to automarkout for a specified location and item.
     * 
     * @param location - location to find for
     * @param item - item to find for
     * @return - list of picks to auto mark out
     * @throws DataAccessException - Database Exception
     */
    public List<Pick> listShortsForLocationAndItem(Location location, Item item) 
        throws DataAccessException;
    
    /**
     * Auto mark out shorts for a specific location or location/item combination.
     * 
     * @param location - location to look for
     * @param item - item to look for
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Database exceptions
     */
    public void executeAutoMarkOutShorts(Location location, Item item)
        throws BusinessRuleException, DataAccessException;
    
    /**
     * Updates line loading information.
     * 
     * @param p - pick to update for
     * @throws DataAccessException - Database Exception
     */
    public void executeUpdateLineLoading(Pick p) 
    throws DataAccessException;
    
    /**
     * cancel a pick.
     * 
     * @param p - pick to cancel
     * @throws BusinessRuleException - business rule exceptions
     * @throws DataAccessException - database exceptions
     */
    public void executeCancelPick(Pick p) 
    throws BusinessRuleException, DataAccessException;
    
    /**
     * manually pick a pick for the quantity specified.
     * 
     * @param p - pick to manaully pick
     * @param qty - quantity to pick
     * @throws BusinessRuleException - business rule exceptions
     * @throws DataAccessException - database exceptions
     */
    public void executeManualPick(Pick p, Integer qty) 
    throws BusinessRuleException, DataAccessException;
    
    /**
     * Get counts of location/item that where shorted.
     * 
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @return - list of arrarys containing location, item, count, last date shorted
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listShortedLocations(Date startTime, Date endTime)
    throws DataAccessException;
    
    /**
     * Get counts of location that were visited.
     * 
     * @param startTime - start of date range to search.
     * @param endTime - end of date range to search.
     * @return - list of arrays containing location, items picked, number of visits
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listSlotVisitLocations(Date startTime, Date endTime)
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 