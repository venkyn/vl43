/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class PassAssignmentCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private LaborManager  laborManager;
    
    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    
    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    } 
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        if (getAssignments().size() > 0) {
            validatePass();
            ungroupAssignments();
        }
        buildResponse();
        return getResponse();
    }
    
    /**
     * Builds the response.
     * 
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }
    
    /**
     * Builds the response record.
     * 
     * @throws DataAccessException - database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        //Check operator's assigned to region
        if (getOperator().getAssignedRegion() != null) {
            if (!(getOperator().getAssignedRegion().equals(getOperator().getCurrentRegion()))) {

                // Build LabelObjectPairs for the parameters
                LOPArrayList lop = new LOPArrayList();
                lop.add("task.regionNumber", 
                    getOperator().getAssignedRegion().getNumber());
                lop.add("task.regionName", getOperator()
                    .getAssignedRegion().getName());
                
                // Create a TaskMessageInfo object and pass
                // it to the setErrorFields method.
                record.setErrorFields(createMessageInfo(
                    TaskErrorCode.ASSIGNED_TO_OTHER_REGION, lop));
            }
        }
        return record;
    }
    
    /**
     * Validates that passing is allowed.
     * 
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     * @throws TaskCommandException - task exception
     */
    protected void validatePass() throws DataAccessException, BusinessRuleException, TaskCommandException {
        //Get a list of assignments based on the group number
        List <Assignment> assignments = getAssignments();
        
        for (Assignment a : assignments) {
            if (a.getType().equals(AssignmentType.Chase)) {
                throw new TaskCommandException(TaskErrorCode.CANNOT_PASS_CHASE);
            }
        }
        for (Assignment a : assignments) {
            // we need to close the assignment labor record before stopping the assignment
            // because stop assignment sets the operator to null and the call to 
            // close assignment labor record uses the operator of the assignment to 
            // find the labor record to close.
            this.getLaborManager().closeAssignmentLaborRecord(getCommandTime(), a);  
            a.stopAssignment(AssignmentStatus.Passed, new Date());

            //Update line loading cartons if needed.
            getAssignmentManager().executeUpdateLineLoading(a);
        }
    }

    /**
     * Ungroup assignments if they belong to a manual issuance region.
     * 
     * @throws DataAccessException - database excetption
     * @throws BusinessRuleException Business rule exception
     */
    protected void ungroupAssignments() 
    throws DataAccessException, BusinessRuleException {
        getAssignmentManager().executeUngroupFromManualRegion(
            (ArrayList<Assignment>) getAssignments());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 