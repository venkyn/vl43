/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class RequestWorkCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private static final int ROW_COUNT = 50;

    private int                 assignmentType;
    
    private String              workIdValue;
    
    private boolean             isPartialWorkId;
    
    private boolean             multAssignmentError = false;
    
    private List<Assignment>    returnAssignment = new ArrayList<Assignment>();
    
    
    /**
     * Getter for the workIdValue property.
     * @return String value of the property
     */
    public String getWorkIdValue() {
        return workIdValue;
    }

    /**
     * Setter for the workIdValue property.
     * @param workIdValue the new workIdValue value
     */
    public void setWorkIdValue(String workIdValue) {
        this.workIdValue = workIdValue;
    }

    /**
     * Getter for the isPartialWorkId property.
     * @return boolean value of the property
     */
    public boolean getIsPartialWorkId() {
        return isPartialWorkId;
    }

    /**
     * Setter for the isPartialWorkId property.
     * @param isPartialWorkId the new isPartialWorkId value
     */
    public void setIsPartialWorkId(boolean isPartialWorkId) {
        this.isPartialWorkId = isPartialWorkId;
    }
    
    /**
     * Getter for the assignmentType property.
     * @return int value of the property
     */
    public int getAssignmentType() {
        return assignmentType;
    }
    
    /**
     * Getter for the assignmentType property.
     * @return AssignmentType value of the property
     */
    public AssignmentType getAssignmentTypeEnum() {
        return AssignmentType.toEnum(assignmentType);
    }
    
    /**
     * Setter for the assignmentType property.
     * @param assignmentType the new assignmentType value
     */
    public void setAssignmentType(int assignmentType) {
        this.assignmentType = assignmentType;
    }
    
    /**
     * Getter for the returnAssignment property.
     * @return List&lt;Assignment$lt; value of the property
     */
    public List<Assignment> getReturnAssignment() {
        return returnAssignment;
    }
    
    /**
     * Setter for the returnAssignment property.
     * @param returnAssignment the new returnAssignment value
     */
    public void setReturnAssignment(List<Assignment> returnAssignment) {
        this.returnAssignment = returnAssignment;
    }
    
    /**
     * Getter for the multAssignmentError property.
     * @return boolean value of the property
     */
    public boolean getMultAssignmentError() {
        return multAssignmentError;
    }
    
    /**
     * Setter for the multAssignmentError property.
     * @param multAssignmentError the new multAssignmentError value
     */
    public void setMultAssignmentError(boolean multAssignmentError) {
        this.multAssignmentError = multAssignmentError;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //If work id being sent is partial
        if (getIsPartialWorkId()) {
            partialWorkId();
        } else {
            fullWorkId();
        }
        buildResponse();
        return getResponse();
    }
    
    /**
     * Builds the response.
     * 
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (Assignment a : getReturnAssignment()) {
            getResponse().addRecord(buildResponseRecord(a));
        }
    }
    
    /**
     * Builds the response record.
     * 
     * @param a - the assignment to use for the response
     * @throws DataAccessException - database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(Assignment a) throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("workIdValue", a.getWorkIdentifier().getWorkIdentifierValue());
        if (getMultAssignmentError()) {

            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.workIdentifier", 
                getWorkIdValue());
            
            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                TaskErrorCode.MULT_MATCHING_ASSIGNMENTS, lop));
        }
        return record;
    }
    
    /**
     * Does the operations for a partial work ID.
     * 
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     * @throws BusinessRuleException - business rule violation
     */
    protected void partialWorkId() 
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(ROW_COUNT);

        //Get a list of assignments with the partial work id
        List<Assignment> matchingAssignments = 
            getAssignmentManager().listAssignmentsByPartialWorkId(queryDecorator,
                                                                  getWorkIdValue(), 
                                                                  getOperator(), 
                                                                  getAssignmentTypeEnum(), 
                                                                  getOperator().getCurrentRegion());
        
        //If more than 1 partial assignment found then set mutliple error
        if (matchingAssignments.size() > 1) {
            setMultAssignmentError(true);
            for (Assignment a : matchingAssignments) {
                getReturnAssignment().add(a);
            }
        } else {
            //Get list of already reserved assignments
            List <Assignment> reservedAssignments = 
                getAssignmentManager().getPrimaryDAO().listReservedAssignments(getOperator().getOperatorIdentifier());
            
            //would be either 1 assignments
            for (Assignment a : matchingAssignments) {
                addAssignment(reservedAssignments, a);
            }
            
            //No assignment found or could not be group with current assignments
            if (matchingAssignments.isEmpty() || getReturnAssignment().isEmpty()) {
                throw new TaskCommandException(TaskErrorCode.WORK_REQUEST_FAILED, getWorkIdValue());
            }
        }
        
    }
    
    /**
     * Does the operations for a full work ID.
     * 
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     * @throws BusinessRuleException - business rule violation
     */
    protected void fullWorkId() 
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        //Get a list of assignments with the partial work id
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(ROW_COUNT);

        List <Assignment> matchingAssignments = 
            getAssignmentManager().listAssignmentsByWorkId(queryDecorator,
                                                           getWorkIdValue(), 
                                                           getOperator(), 
                                                           getAssignmentTypeEnum(), 
                                                           getOperator().getCurrentRegion());
   
        //Get list of already reserved assignments
        List <Assignment> reservedAssignments = 
            getAssignmentManager().getPrimaryDAO().listReservedAssignments(getOperator().getOperatorIdentifier());
        
        //Loop through assignment until 1 is found that cn be grouped with others
        for (Assignment a : matchingAssignments) {
            if (addAssignment(reservedAssignments, a)) {
                break;
            }
        }
        
        //No assignment found or could not be group with current assignments
        if (matchingAssignments.isEmpty() || getReturnAssignment().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.WORK_REQUEST_FAILED, getWorkIdValue());
        }
        
    }
    
    /**
     * @param reserved the list of reserved assignments
     * @param assignment the assignment that is the basis of the group
     * @return success or not.
     * @throws DataAccessException - database failure
     * @throws BusinessRuleException - business rule violation
     */
    protected boolean addAssignment(List<Assignment> reserved, 
                                    Assignment assignment) 
    throws DataAccessException, BusinessRuleException {
        boolean returnValue = false;
        
        reserved.add(assignment);

        if (reserved.size() == 1) {
            getReturnAssignment().add(assignment);
            returnValue = true;
        } else {
            //if assignment can be grouped with currently reserved assignments
            try {
                //Validate assignment can be added to group
                getAssignmentManager().executeValidateGroup(
                    (ArrayList<Assignment>) reserved, false);
                getReturnAssignment().add(assignment);
                returnValue = true;
            } catch (Exception e) {
                //assignment could not be grouped so remove
                reserved.remove(assignment);
            }
        }
        
        //Reserve assignment
        if (returnValue) {
            reserveAssignment(assignment, reserved.size());
        }
        
        return returnValue;
    }
    
    /**
     * reserves assignment assignment.
     * 
     * @param assignment - assignment to reserve
     * @param count - number of assignments reserved
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     */
    protected void reserveAssignment(Assignment assignment, int count) 
    throws DataAccessException, BusinessRuleException {
        assignment.setReservedBy(getOperator().getOperatorIdentifier());
        assignment.getGroupInfo().setRequestedOrderNo(count);
        getAssignmentManager().save(assignment);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 