/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.RegionUserSettings;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;



/**
 * 
 *
 * @author sfahnestock
 */
public abstract class PickingRegionCmdRoot extends BaseSelectionTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;
    
    private String                  selectedWorkType;

    private int                     regionNumber;
    
    private RegionManager           regionManager;
    
    private Region                  currRegion;
    
    private SelectionRegionManager  selectionRegionManager;
    
    private SelectionRegion         selectionRegion;
    
    private LaborManager            laborManager;
    
    
    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    
    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    
    /**
     * Getter for the selectionRegionManager property.
     * @return SelectionRegionManager value of the property
     */
    public SelectionRegionManager getSelectionRegionManager() {
        return selectionRegionManager;
    }
   
    /**
     * Setter for the selectionRegionManager property.
     * @param selectionRegionManager the new selectionRegionManager value
     */
    public void setSelectionRegionManager(SelectionRegionManager selectionRegionManager) {
        this.selectionRegionManager = selectionRegionManager;
    }

    /**
     * Getter for the selected work type property.
     * @return String value of the property
     */
    public String getSelectedWorkType() {
        return selectedWorkType;
    }
    
    /**
     * @return get the selected work type as an Enum value.
     */
    public TaskFunctionType getSelectedWorkTypeEnum() {
        return TaskFunctionType.toEnum(Integer.parseInt(selectedWorkType));
    }
    
    /**
     * Setter for the selected work type property.
     * @param selectedWorkType the new selectedWorkType value
     */
    public void setSelectedWorkType(String selectedWorkType) {
        this.selectedWorkType = selectedWorkType;
    }

    /**
     * Getter for the region number property.
     * @return int value of the property
     */
    public int getRegionNumber() {
        return regionNumber;
    }

    /**
     * Setter for the selected work type property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(int regionNumber) {
        this.regionNumber = regionNumber;
    }
    
    /**
     * Getter for the regionManager property.
     * @return regionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }
  
    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }
    
    /**
     * Getter for the currRegion property.
     * @return currRegion value of the property
     */
    public Region getCurrRegion() {
        return currRegion;
    }
  
    /**
     * Setter for the currRegion property.
     * @param currRegion the new currRegion value
     */
    public void setCurrRegion(Region currRegion) {
        this.currRegion = currRegion;
    }
    
    /**
     * Getter for the selectionRegionByNumber property.
     * @return selectionRegionByNumber value of the property
     */
    public SelectionRegion getSelectionRegion() {
        return selectionRegion;
    }
  
    /**
     * Setter for the selectionRegionByNumber property.
     * @param selectionRegionByNumber the new selectionRegionByNumber value
     */
    public void setSelectionRegion(SelectionRegion selectionRegionByNumber) {
        this.selectionRegion = selectionRegionByNumber;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        verifyPassedInRegion();
        checkWorkAuthorization();
        checkInProgress();
        checkSignOnToAssignedRegion();
        clearReservedAssignments();
        setCurrentRegion();
        openLaborRecord();
        buildResponse();

        return getResponse();
    }

    /**
     * Verifies that the passed in region is valid.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     * @throws BusinessRuleException - Business Rule Exception 
     */
    protected void verifyPassedInRegion() 
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        setSelectionRegion(getSelectionRegionManager().findRegionByNumber(getRegionNumber()));
        if (getSelectionRegion() == null) {
            // Raise region not found error
            throw new TaskCommandException(TaskErrorCode.INVALID_REGION_NUMBER, getRegionNumber());
        }
    }
    
    /**
     * Verifies that the operator has permissions for the passed in region.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void checkWorkAuthorization() throws DataAccessException, TaskCommandException {
        // Get a list of authorized regions for the operator's worktype
        List<Region> authorizedRegions = 
            getRegionManager().listAuthorized(
                getSelectedWorkTypeEnum(), 
                getOperator().getWorkgroup().getId());
        
        // Check to see if the requested region is in authorized regions
        boolean authorized = false;
        for (Region r : authorizedRegions) {
            if (r.equals(getSelectionRegion())) {
                authorized = true;
                break;
            }
        }
        
        // If not authorized for the region raise error
        if (!authorized) {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION, 
                getSelectedWorkType(), getRegionNumber());
        }
    }
    
    /**
     * Checks for in progress work.
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Exception
     */
    protected void checkInProgress() throws DataAccessException, TaskCommandException {
        boolean inProgressCurrentRegion = false; 
        
        // Get a list of active assignemtns for the operator making the request
        List<Assignment>    queriedAssignments
            = getAssignmentManager().listActiveAssignments(getOperator());
        
        if (!(queriedAssignments.isEmpty())) {
            if (getSelectionRegion() != null) {
                //Search for assignment with status of in progress in requested region
                for (Assignment a : queriedAssignments) {
                    if (a.getStatus().isInSet(AssignmentStatus.InProgress, 
                                              AssignmentStatus.Suspended) 
                        && a.getRegion().equals(getSelectionRegion())) {
                        inProgressCurrentRegion = true;
                    }
                }
                if (!(inProgressCurrentRegion)) {
                    for (Assignment a : queriedAssignments) {
                        if (a.getStatus().isInSet(AssignmentStatus.InProgress,
                                                  AssignmentStatus.Suspended)) {
                            throw new TaskCommandException(
                                TaskErrorCode.IN_PROGRESS_IN_OTHER_REGION, 
                                a.getRegion().getNumber());
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Checks to see if the operator is signing on to assigned region.
     * @throws DataAccessException - Database Exception
     */
    protected void checkSignOnToAssignedRegion() throws DataAccessException {
        // If operator is signing into assigned region clear operators assigned region
        if (getOperator().getAssignedRegion() != null) {
            if (getOperator().getAssignedRegion().equals(getSelectionRegion())) {
                getOperator().setAssignedRegion(null);
            }
        }
    }
    
    /**
     * Clears any reserved assignments for the operator.
     * @throws DataAccessException - Database Exception
     */
    protected void clearReservedAssignments() throws DataAccessException {
        List<Assignment> assignments = getAssignmentManager()
            .listReservedAssignments(getOperator().getOperatorIdentifier());
        for (Assignment a : assignments) {
            a.setReservedBy(null);
        }
    }
    
    /**
     * Sets the operator's current region.
     * @throws DataAccessException - Database Exception
     */
    protected void setCurrentRegion() throws DataAccessException {        
        // Sets the operators current region to passed in region number
        getOperator().setCurrentRegion(getSelectionRegion());
        getOperator().getRegions().add(getSelectionRegion());
    }
    
    /**
     * updates the operator's labor records.
     * 
     * @throws DataAccessException - database exception
     * @throws BusinessRuleException - business exception
     */
    protected void openLaborRecord() throws DataAccessException, BusinessRuleException {
        
    
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                                                       this.getOperator(),
                                                       OperatorLaborActionType.Selection);
                                               
    }
    
    
    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        //if current function is normal picking
        if (getSelectedWorkType().equals("3")) {
            getResponse().addRecord(buildResponseRecord(
                getSelectionRegion().getProfileNormalAssignment()));
        } else if (getSelectedWorkType().equals("4")) {
            //else if current funtion is chase picking
            getResponse().addRecord(buildResponseRecord(
                getSelectionRegion().getProfileChaseAssignment()));
        } else if (getSelectedWorkType().equals("6")) {
            //else if current function is both nomal and chase picking
            getResponse().addRecord(buildResponseRecord(
                getSelectionRegion().getProfileChaseAssignment()));
            getResponse().addRecord(buildResponseRecord(
                getSelectionRegion().getProfileNormalAssignment()));
        }
    }
    
    /**
     * Build and return a task response record.
     * 
     * @param srp - Selection region profile
     * @return - Task Response record
     * @throws DataAccessException - Database Exceptions 
     */
    protected TaskResponseRecord buildResponseRecord(SelectionRegionProfile srp) 
    throws DataAccessException {
        TaskResponseRecord record = new TaskResponseRecord();
        RegionUserSettings userSettings = getSelectionRegion()
            .getUserSettings(srp.getAssignmentType());
        
        String useLut = "0";
        
        SystemProperty sp = getSystemPropertyManager()
                                .findByName("Use LUTs for ODR Calls");
        if (sp != null) {
            if (sp.getValue().equals("1") || sp.getValue().equals("2")) {
                useLut = sp.getValue();
            }
        }
        
        //Create a return record for normal picking
        record.put("regionNumber", getSelectionRegion().getNumber());
        record.put("regionName", translateUserData(getSelectionRegion().getName()));
        record.put("autoAssign", srp.isAutoIssuance());
        record.put("numAssignsAllowed", 
            userSettings.getMaximumAssignmentsAllowed());
        record.put("allowSkipAisle", userSettings.isAllowSkipAisle());
        record.put("allowSkipSlot", userSettings.isAllowSkipSlot());
        record.put("allowRepickSkips", 
            userSettings.isAllowRepickSkips());
        record.put("printContLabels", 
            srp.getPrintContainerLabelsIndicator().toValue());
        record.put("printChaseLabels", srp.isChasePrintLabelsOn());
        
        if (srp.isMultiplePickPrompt()) {
            record.put("multPickPrompt", "2");
        } else if (userSettings.isSuppressQuantityOfOne()) {
            record.put("multPickPrompt", "1");
        } else {
            record.put("multPickPrompt", "0");
        }
        
        record.put("allowSignOff", userSettings.isAllowSignOffAtAnyPoint());
        if (srp.getContainerType() == null) {
            record.put("containerType", 0);
            record.put("deliverContainerClosed", false);
            record.put("promptOpContainer", false);
            record.put("allowMultOpenContainers", false);
            record.put("printNumberOfLabelsFlag", false);
        } else {
            record.put("containerType", 1);
            record.put("deliverContainerClosed", 
                srp.getContainerType().isRequireDeliveryAfterClosed());
            record.put("promptOpContainer", 
                srp.getContainerType().isPromptOperatorForContainerID());
            record.put("allowMultOpenContainers", 
                srp.getContainerType().isAllowMultipleOpenContainers());
            record.put("printNumberOfLabelsFlag", 
                srp.getContainerType().getAllowPreCreationOfContainers());
        }
        record.put("allowPass", 
            srp.isAllowPassingAssignment());
        
        //Delivery Type
        if (srp.isRequireDeliveryPrompt()) {
            if (userSettings.isRequireDeliveryConfirmation()) {
                record.put("deliveryType", "1");
            } else {
                record.put("deliveryType", "0");
            }
        } else {
            record.put("deliveryType", "2");
        }
        
        record.put("quantityVerification", 
            srp.isRequireQuantityVerification());
        if (userSettings.getWorkIdentifierRequestLength() == 0) {
            record.put("workIDLength", -1);
        } else {
            record.put("workIDLength", 
                userSettings.getWorkIdentifierRequestLength());
        }
        record.put("goBackForShorts", 
            userSettings.getGoBackForShortsIndicator().toValue());
        record.put("allowReverse", 
            userSettings.isAllowReversePicking());
        record.put("useLut", useLut);
        record.put("currPreAisle", "X");
        record.put("currAisle", "X");
        record.put("currPostAisle", "X");
        record.put("currSlot", "X");
        record.put("spokenContValidLength", 
            userSettings.getNumberOfContainerDigitsSpeak());
        record.put("assignmentType", srp.getAssignmentType().getValue());
        record.put("pickByPick", "0");
        
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 