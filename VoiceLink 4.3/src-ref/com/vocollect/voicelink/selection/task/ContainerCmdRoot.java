/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerStatus;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 
 * 
 * @author sfahnestock
 */
public abstract class ContainerCmdRoot extends BaseSelectionTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    // Comparator to sort by group position
    static final Comparator<Container> ORDER_ID_DESC = new Comparator<Container>() {
        public int compare(Container c2, Container c1) {
            return c1.getId().compareTo(c2.getId());
        }
    };

    // Comparator to sort by group position
    static final Comparator<Container> ORDER_ID_ASC = new Comparator<Container>() {
        public int compare(Container c1, Container c2) {
            return c1.getId().compareTo(c2.getId());
        }
    };

    // Comparator to sort by group position
    static final Comparator<Integer> ORDER_TARGET_CONTAINERS = new Comparator<Integer>() {
        public int compare(Integer i1, Integer i2) {
            return i1.compareTo(i2);
        }
    };

    private static final int CONTAINER_NUMBER_SIZE = 10;

    private static final int CONTAINER_OPERATION_CLOSE = 1;
    private static final int CONTAINER_OPERATION_OPEN = 2;
    private static final int CONTAINER_OPERATION_PRECREATE = 3;

    private String assignmentPk;

    private int operation;

    private int numberOfContainers;

    private int numberContDigits;

    private String targetContainer;

    private String containerNumber;

    private String pickContainerPk;

    private String labels;

    /**
     * Getter for the assignmentPk property.
     * 
     * @return Long value of the property
     */
    public Long getAssignmentPkLong() {
        if (StringUtil.isNullOrEmpty(this.assignmentPk)) {
            return null;
        } else {
            return Long.parseLong(this.assignmentPk);
        }
    }

    /**
     * Getter for the assignmentPk property.
     * 
     * @return String value of the property
     */
    public String getAssignmentPk() {
        return assignmentPk;
    }

    /**
     * Setter for the assignmentPk property.
     * 
     * @param assignmentPk
     *            the new assignmentPk value
     */
    public void setAssignmentPk(String assignmentPk) {
        this.assignmentPk = assignmentPk;
    }

    /**
     * Getter for the pickContainerPk property.
     * 
     * @return String value of the property
     */
    public String getPickContainerPk() {
        return pickContainerPk;
    }

    /**
     * Getter for the pickContainerPk property.
     * 
     * @return Long value of the property
     */
    public Long getPickContainerPkLong() {
        return Long.parseLong(pickContainerPk);
    }

    /**
     * Setter for the pickContainerPk property.
     * 
     * @param pickContainerPk
     *            the new pickContainerPk value
     */
    public void setPickContainerPk(String pickContainerPk) {
        this.pickContainerPk = pickContainerPk;
    }

    /**
     * Getter for the targetContainer property.
     * 
     * @return String value of the property
     */
    public String getTargetContainer() {
        return targetContainer;
    }

    /**
     * Getter for the targetContainer property.
     * 
     * @return Integer value of the property
     */
    public Integer getTargetContainerInt() {
        if (StringUtil.isNullOrEmpty(this.targetContainer)) {
            return null;
        } else {
            return Integer.parseInt(targetContainer);
        }
    }

    /**
     * Setter for the targetContainer property.
     * 
     * @param targetContainer
     *            the new targetContainer value
     */
    public void setTargetContainer(String targetContainer) {
        this.targetContainer = targetContainer;
    }

    /**
     * Getter for the containerNumber property.
     * 
     * @return String value of the property
     */
    public String getContainerNumber() {
        return containerNumber;
    }

    /**
     * Getter for the containerNumber property.
     * 
     * @return Long value of the property
     */
    public Long getContainerNumberLong() {
        return Long.parseLong(containerNumber);
    }

    /**
     * Setter for the containerNumber property.
     * 
     * @param containerNumber
     *            the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for the operation property.
     * 
     * @return int value of the property
     */
    public int getOperation() {
        return operation;
    }

    /**
     * Setter for the operation property.
     * 
     * @param operation
     *            the new operation value
     */
    public void setOperation(int operation) {
        this.operation = operation;
    }

    /**
     * Getter for the labels property.
     * 
     * @return String value of the property
     */
    public String getLabels() {
        return labels;
    }

    /**
     * Getter for the labels property.
     * 
     * @return String value of the property
     */
    public int getLabelsInt() {
        if (StringUtil.isNullOrEmpty(this.labels)) {
            return 1;
        } else {
            return Integer.parseInt(this.labels);
        }
    }

    /**
     * Setter for the labels property.
     * 
     * @param labels
     *            the new labels value
     */
    public void setLabels(String labels) {
        this.labels = labels;
    }

    /**
     * Getter for the numberOfContainers property.
     * 
     * @return int value of the property
     */
    protected int getNumberOfContainers() {
        return numberOfContainers;
    }

    /**
     * Setter for the numberOfContainers property.
     * 
     * @param numberOfContainers
     *            the new numberOfContainers value
     */
    protected void setNumberOfContainers(int numberOfContainers) {
        this.numberOfContainers = numberOfContainers;
    }

    /**
     * Getter for the numberContDigits property.
     * 
     * @return int value of the property
     */
    protected int getNumberContDigits() {
        return numberContDigits;
    }

    /**
     * Setter for the numberContDigits property.
     * 
     * @param numberContDigits
     *            the new numberContDigits value
     */
    protected void setNumberContDigits(int numberContDigits) {
        this.numberContDigits = numberContDigits;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        // Force load all information of assignment and container set
        // TODO: Should consider doing a lazy load false with a fetch join.
        // when we implement some for that.
        for (Assignment a : getAssignments()) {
            for (Container c : a.getContainers()) {
                c.getAssignments();
            }
        }
        containerOperations();
        buildResponse();

        return getResponse();
    }

    /**
     * Executes the container operations.
     * 
     * @throws BusinessRuleException
     *             - Throws business rule exception
     * @throws DataAccessException
     *             - Database Exception
     * @throws TaskCommandException
     *             - Task Exception
     */
    protected void containerOperations() throws BusinessRuleException,
            DataAccessException, TaskCommandException {
        // Get a list of assignments based on the group number
        List<Assignment> assignments = getAssignments();

        // Set number of container digits to speak
        setNumberContDigits(assignments.get(0).getRegion()
                .getUserSettings(assignments.get(0).getType())
                .getNumberOfContainerDigitsSpeak());

        // ===============================================================
        // Close container operation
        if (getOperation() == CONTAINER_OPERATION_CLOSE) {
            closeContainer();

            // ===============================================================
            // Pre-Create container operation
        } else if (getOperation() == CONTAINER_OPERATION_PRECREATE) {
            preCreateContainers(assignments);

            // ===============================================================
            // Open or create new container operation
        } else if (getOperation() == CONTAINER_OPERATION_OPEN) {
            // Validate the container number being passed in, if one exists
            validateContainerNumber(assignments);

            // Check if another container can be opened as per region settings
            if (!isAnotherContainerAllowed(assignments)) {
                return;
            }

            // Open the container
            openContainer();
        }
    }

    /**
     * Method to check if another container can be opened as per region settings
     * of the assignment
     * 
     * @param assignments
     *            List of assignment in the group
     * @return <code>true:</code> if another container can be created<br>
     *         <code>false:</code> if another container cannot be created
     */
    protected boolean isAnotherContainerAllowed(List<Assignment> assignments) {

        boolean allowMultipleOpenContainers = assignments.get(0).getRegion()
                .getProfile(assignments.get(0).getType()).getContainerType()
                .isAllowMultipleOpenContainers();

        // If region allows multiple containers to be open or we are batch
        // picking, no action required
        if (allowMultipleOpenContainers || assignments.size() > 1) {
            return true;
        }

        // Since this is a list (even though its 1) we need to iterate it
        for (Assignment a : assignments) {

            // Again its a list of 1 (or at least should be)
            for (Container c : a.getContainers()) {
                if (c.getStatus() == ContainerStatus.Open) {
                    return false;
                }
            }
            // Since we know its a list of 1 and it doesnt have an open
            // container, open one
            return true;
        }

        // Any other case no
        return false;
    }

    /**
     * Builds the response and adds it to the response record.
     * 
     * @throws BusinessRuleException
     *             - Throws business rule exception
     * @throws DataAccessException
     *             - Database Exception
     */
    protected void buildResponse() throws BusinessRuleException,
            DataAccessException {
        // Get a list of assignments based on the group number
        List<Assignment> assignments = getAssignments();

        // Build a set of containers to return, may have same container
        // for different assignment, by putting in set, we ensure container
        // is only sent once
        List<Container> containers = new ArrayList<Container>();

        // For each assignment in the group
        for (Assignment a : assignments) {
            for (Container c : a.getContainers()) {
                if (!containers.contains(c)) {
                    containers.add(c);
                }
            }
        }

        if (containers.isEmpty()) {
            getResponse().addRecord(new TaskResponseRecord());
        } else {
            Collections.sort(containers, ORDER_ID_DESC);
            Container openTargetContainer = null;

            // If target container requested then add first if opened
            if (getTargetContainerInt() != null) {
                for (Container c : containers) {
                    if (c.getStatus() == ContainerStatus.Open
                            && getTargetContainerInt().equals(
                                    c.getTargetContainerIndicator())) {
                        getResponse().addRecord(buildResponseRecord(c));
                        openTargetContainer = c;
                    }
                }
            }

            // Add other open containers next
            for (Container c : containers) {
                if (c.getStatus() == ContainerStatus.Open
                        && !c.equals(openTargetContainer)) {
                    getResponse().addRecord(buildResponseRecord(c));
                }
            }

            // Add Closed containers next
            for (Container c : containers) {
                if (c.getStatus() == ContainerStatus.Closed) {
                    getResponse().addRecord(buildResponseRecord(c));
                }
            }

            // Add Available containers last
            for (Container c : containers) {
                if (c.getStatus() == ContainerStatus.Available) {
                    getResponse().addRecord(buildResponseRecord(c));
                }
            }
        }
    }

    /**
     * Builds the response record.
     * 
     * @param c
     *            -the container to use in the response
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(Container c) {
        ResponseRecord record = new TaskResponseRecord();
        Assignment a = c.getAssignments().iterator().next();

        record.put("systemContainerPk", c.getId());
        record.put("scannedContainerValidation", c.getContainerNumber());
        record.put(
                "spokenContainerValidation",
                getContainerDigitsToSpeak(c.getContainerNumber(),
                        getNumberContDigits()));
        record.put("assignmentPk", a.getId());
        record.put("idDescription", a.getNumber());
        if (c.getTargetContainerIndicator() == null) {
            record.put("targetContainer", "");
        } else {
            record.put("targetContainer", c.getTargetContainerIndicator()
                    .toString());
        }
        if (c.getStatus().equals(ContainerStatus.Available)) {
            record.put("containerStatus", "A");
        } else if (c.getStatus().equals(ContainerStatus.Closed)) {
            record.put("containerStatus", "C");
        } else {
            record.put("containerStatus", "O");
        }
        record.put("printed", c.getPrinted());

        return record;
    }

    /**
     * Creates a new container and adds it to the assignment.
     * 
     * @param a
     *            - the assignment to add the container to
     * @param cs
     *            - the status to set the container to
     * @param targetContainerIndicator
     *            - target container indicator pick is for
     * 
     * @return The created container
     * @throws DataAccessException
     *             - Data access exception
     * @throws BusinessRuleException
     *             - Business exception
     */
    protected Container createNewContainer(Assignment a, ContainerStatus cs,
            Integer targetContainerIndicator) throws DataAccessException,
            BusinessRuleException {
        // Create new container setting
        Container c = new Container();
        c.setContainerType(a.getRegion().getProfile(a.getType())
                .getContainerType());
        // Set container number to null temporarily
        c.setContainerNumber("1");
        c.setCreatedDate(new Date());
        c.setExportStatus(ExportStatus.NotExported);
        c.setPrinted(0);
        c.setVersion(1);
        c.setTargetContainerIndicator(targetContainerIndicator);
        // Set container status to available
        c.setStatus(cs);
        // Assign container to assignments
        getContainerManager().save(c);
        a.addContainer(c);
        // Set container number correctly
        setContainerNumberOfNewContainer(c.getId(), a);

        return c;
    }

    /**
     * Closes the specified container.
     * 
     * @throws DataAccessException
     *             - Database exception
     */
    protected void closeContainer() throws DataAccessException,
            BusinessRuleException {
        // Get container object based on pick container pk
        Container c = getContainerManager().getPrimaryDAO().get(
                getPickContainerPkLong());
        // Set status closed
        c.setStatus(ContainerStatus.Closed);
    }

    /**
     * Pre-Creates containers.
     * 
     * @param assignments
     *            - the list of assignments
     * @throws BusinessRuleException
     *             - Throws business rule exception
     * @throws DataAccessException
     *             - Database exception
     */
    protected void preCreateContainers(List<Assignment> assignments)
            throws BusinessRuleException, DataAccessException {
        // For each assignment
        for (Assignment a : assignments) {
            // Check if specific assignment or all
            if (getAssignmentPkLong() == null
                    || getAssignmentPkLong().equals(a.getId())) {

                List<Integer> targetContainers = new ArrayList<Integer>();

                // Determine number of containers to pre-create
                if (getOperation() == CONTAINER_OPERATION_PRECREATE) {
                    List<Pick> picks = a.getPicks();
                    if (picks.get(0).getTargetContainerIndicator() != null) {
                        for (Pick p : picks) {
                            if (!targetContainers.contains(p
                                    .getTargetContainerIndicator())) {
                                targetContainers.add(p
                                        .getTargetContainerIndicator());
                            }
                        }
                        setNumberOfContainers(targetContainers.size());
                    } else {
                        setNumberOfContainers(getLabelsInt());
                    }
                }

                // Determine how many containers already exist
                if (a.getContainers().isEmpty()) {
                    if (targetContainers.isEmpty()) {
                        // For each container
                        for (int i = 0; i < getNumberOfContainers(); i++) {
                            // Create a new container
                            createNewContainer(a, ContainerStatus.Available,
                                    getTargetContainerInt());
                        }
                    } else {
                        Collections.sort(targetContainers,
                                ORDER_TARGET_CONTAINERS);
                        for (Integer tc : targetContainers) {
                            // Create a new container
                            createNewContainer(a, ContainerStatus.Available, tc);
                        }
                    }
                }
            }
        }
    }

    /**
     * Validates the container number that is passed in.
     * 
     * @param assignments
     *            - the list of assignments
     * @throws TaskCommandException
     *             - Throws task command exception
     * @throws DataAccessException
     *             - Database exception
     */
    protected void validateContainerNumber(List<Assignment> assignments)
            throws DataAccessException, TaskCommandException {
        // Validate container number passed in
        if (!(getContainerNumber().equals(""))) {
            // Validate enough digits based on region parameter
            if (getContainerNumber().length() < getNumberContDigits()) {
                throw new TaskCommandException(
                        TaskErrorCode.INVALID_CONTAINER_NUMBER, assignments
                                .get(0).getRegion()
                                .getUserSettings(assignments.get(0).getType())
                                .getNumberOfContainerDigitsSpeak());
            }
            // Check if already exists
            Container c1 = getContainerManager().findContainerByNumber(
                    getContainerNumber());
            if (c1 != null) {
                if (c1.getStatus().equals(ContainerStatus.Closed)) {
                    throw new TaskCommandException(
                            TaskErrorCode.CONTAINER_ALREADY_CLOSED,
                            c1.getContainerNumber());
                } else {
                    throw new TaskCommandException(
                            TaskErrorCode.CONTAINER_ALREADY_EXISTS,
                            c1.getContainerNumber());
                }
            }
            // Verify last x digits unique within the region based on region
            // setting
            for (Assignment a : assignments) {
                Iterator<Container> i = a.getContainers().iterator();
                while (i.hasNext()) {
                    Container c2 = i.next();

                    int length = a.getRegion().getUserSettings(a.getType())
                            .getNumberOfContainerDigitsSpeak();
                    String cont2 = getContainerDigitsToSpeak(
                            c2.getContainerNumber(), length);
                    String cont1 = getContainerDigitsToSpeak(
                            getContainerNumber(), length);

                    if (cont1.equals(cont2)) {
                        throw new TaskCommandException(
                                TaskErrorCode.CONTAINER_NUMBER_DUPLICATE);
                    }
                }
            }
        }
    }

    /**
     * Opens a container.
     * 
     * @throws BusinessRuleException
     *             - Throws business rule exception
     * @throws DataAccessException
     *             - Database exception
     */
    protected void openContainer() throws DataAccessException,
            BusinessRuleException {
        // Open a container
        if (!(getPickContainerPk().equals(""))) {
            // Must be opening an existing container
            Container c = getContainerManager().getPrimaryDAO().get(
                    getPickContainerPkLong());
            c.setStatus(ContainerStatus.Open);
        } else {
            // Check if there is a container with available status for
            // assignment
            // Get a list of assignments based on the group number
            List<Assignment> assignments = getAssignments();

            // For each assignment in the group
            for (Assignment a : assignments) {
                if (getAssignmentPkLong() == null
                        || getAssignmentPkLong().equals(a.getId())) {

                    // Build List of available containers
                    List<Container> availableContainers = new ArrayList<Container>();

                    // Loop through existing containers
                    for (Container c : a.getContainers()) {
                        // if not looking for target container
                        if (getTargetContainerInt() == null) {
                            // Add all available containers
                            if (c.getStatus() == ContainerStatus.Available) {
                                availableContainers.add(c);
                            }
                        } else {
                            // Add only specified target container
                            if (getTargetContainerInt().equals(
                                    c.getTargetContainerIndicator())) {
                                availableContainers.add(c);
                            }
                        }

                    }

                    // If no available containers were
                    // found then open a new container, else open first
                    // container
                    // in list
                    if (availableContainers.isEmpty()) {
                        Container c = createNewContainer(a,
                                ContainerStatus.Open, getTargetContainerInt());
                    } else {
                        Collections.sort(availableContainers, ORDER_ID_ASC);
                        availableContainers.get(0).setStatus(
                                ContainerStatus.Open);
                    }
                }
            }
        }
    }


    /**
     * Sets the container number.
     * 
     * @param containerId
     *            - the container id of the container to sett the number
     * @param a
     *            - the assignment the container belongs to
     * @throws DataAccessException
     *             - Database exception
     * @throws BusinessRuleException
     *             - Business rule exception
     */
    protected void setContainerNumberOfNewContainer(Long containerId,
            Assignment a) throws DataAccessException, BusinessRuleException {
        Container c = getContainerManager().get(containerId);
        // If no container number was passed in use the container ID as the
        // container number
        if (getContainerNumber().equals("")) {
            c.setContainerNumber(padContainerNumber(containerId.toString(),
                    CONTAINER_NUMBER_SIZE));
        } else {
            c.setContainerNumber(getContainerNumber());
        }
        // Update the container with the correct container number
        getContainerManager().save(c);
    }

    /**
     * Adds leading zeros to the container number.
     * 
     * @param contNumber
     *            - the container number to pad
     * @param length
     *            - length to pad to
     * @return the padded container number
     */
    protected String padContainerNumber(String contNumber, int length) {
        while (contNumber.length() < length) {
            contNumber = "0" + contNumber;
        }
        return contNumber;
    }

    /**
     * Get the last x number of digits of the container. If container number is
     * shorter than specified length, then pad up to correct length with zeros
     * so it can be spoken by operator.
     * 
     * @param contNumber
     *            - container number to check
     * @param length
     *            - length to get
     * @return - last x digits of container
     */
    protected String getContainerDigitsToSpeak(String contNumber, int length) {

        if (contNumber.length() <= length) {
            return padContainerNumber(contNumber, length);
        } else {
            return contNumber.substring(contNumber.length() - length);

        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 