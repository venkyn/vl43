/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.SelectionRegionBaseItem;
import com.vocollect.voicelink.selection.model.SelectionRegionSummaryPrompt;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;
import com.vocollect.voicelink.selection.service.SummaryPromptManager;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.BaseTaskResponse;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * 
 * 
 * @author mkoenig
 */
public abstract class GetAssignmentCmdRoot extends BaseSelectionTaskCommand {

    //
    private static final long serialVersionUID = -1571537908144487598L;

    private static final int FIRST_ASSIGNMENT = 0;

    private static final int ADDTIONAL_ASSIGNMENTS = 100;

    private static final Semaphore SYNC_SINGLE_THREAD = new Semaphore(1, true);

    // Comparator to sort by group position
    static final Comparator<Assignment> GROUP_POSITION = new Comparator<Assignment>() {

        public int compare(Assignment a1, Assignment a2) {
            return a1.getGroupInfo().getGroupPosition()
                .compareTo(a2.getGroupInfo().getGroupPosition());
        }
    };

    // Comparator to sort by group position
    static final Comparator<Assignment> REQUEST_ORDER = new Comparator<Assignment>() {

        public int compare(Assignment a1, Assignment a2) {
            return a1.getGroupInfo().getRequestedOrderNo()
                - a2.getGroupInfo().getRequestedOrderNo();
        }
    };

    private SelectionRegionManager selectionRegionManager;

    private String numberOfAssignments;

    private int assignmentType;

    private SelectionRegion region;

    private ArrayList<Assignment> assignments = new ArrayList<Assignment>();

    private LaborManager laborManager;

    private SummaryPromptManager summaryPromptManager;

    /**
     * Getter for the summaryPromptManager property.
     * @return SummaryPromptManager value of the property
     */
    public SummaryPromptManager getSummaryPromptManager() {
        return summaryPromptManager;
    }

    /**
     * Setter for the summaryPromptManager property.
     * @param summaryPromptManager the new summaryPromptManager value
     */
    public void setSummaryPromptManager(SummaryPromptManager summaryPromptManager) {
        this.summaryPromptManager = summaryPromptManager;
    }

    /**
     * Getter for the selectionRegionManager property.
     * @return SelectionRegionManager value of the property
     */
    public SelectionRegionManager getSelectionRegionManager() {
        return selectionRegionManager;
    }

    /**
     * Setter for the selectionRegionManager property.
     * @param selectionRegionManager the new selectionRegionManager value
     */
    public void setSelectionRegionManager(SelectionRegionManager selectionRegionManager) {
        this.selectionRegionManager = selectionRegionManager;
    }

    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new assignmentLaborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the assignments property.
     * @return ArrayList&lt;Assignment&gt; value of the property
     */
    @Override
    public ArrayList<Assignment> getAssignments() {
        return assignments;
    }

    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    public void setAssignments(ArrayList<Assignment> assignments) {
        this.assignments = assignments;
    }

    /**
     * Getter for the region property.
     * @return SelectionRegion value of the property
     */
    public SelectionRegion getRegion() {
        return region;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(SelectionRegion region) {
        this.region = region;
    }

    /**
     * Getter for the assignmentType property.
     * @return AssignmentType value of the property
     */
    public AssignmentType getAssignmentTypeEnum() {
        return AssignmentType.Chase.fromValue(getAssignmentType());
    }

    /**
     * Getter for the assignmentType property.
     * @return int value of the property
     */
    public int getAssignmentType() {
        return this.assignmentType;
    }

    /**
     * Setter for the assignmentType property.
     * @param assignmentType the new assignmentType value
     */
    public void setAssignmentType(int assignmentType) {
        this.assignmentType = assignmentType;
    }

    /**
     * Getter of number of assignments as integer.
     * @return - integer representation of number of assignments parameter
     */
    public int getNumberOfAssignmentsInt() {
        if (StringUtil.isNullOrEmpty(this.numberOfAssignments)) {
            return 0;
        } else {
            return Integer.valueOf(this.numberOfAssignments);
        }
    }

    /**
     * Getter for the numberOfAssignments property.
     * @return String value of the property
     */
    public String getNumberOfAssignments() {
        return numberOfAssignments;
    }

    /**
     * Setter for the numberOfAssignments property.
     * @param numberOfAssignments the new numberOfAssignments value
     */
    public void setNumberOfAssignments(String numberOfAssignments) {
        this.numberOfAssignments = numberOfAssignments;
    }

    /**
     * main execute method for prTaskLUTGetAssignment.
     * 
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        // Get Selection Region
        setRegion(getSelectionRegionManager().get(
            getOperator().getCurrentRegion().getId()));

        // Get list of assignment that are reserved by operator
        // and in this region
        buildReservedAssignmentList();

        // If no assignments are reserved,
        // check for active work in current region
        if (getAssignments().isEmpty()) {
            buildActiveAssignmentList();
        }

        // If list is still empty then check if automatic or manual issuance
        if (getAssignments().isEmpty()) {

            // if Automatic issuance then build new list of assignments
            if (getRegion().getProfile(getAssignmentTypeEnum())
                .isAutoIssuance()) {

                // Get next available assignment
                try {
                    SYNC_SINGLE_THREAD.acquire();
                    getNextAvailableAssignment();
                } finally {
                    SYNC_SINGLE_THREAD.release();
                }

                // If no assignments found then raise error
                // else get additional assignments for group if any
                if (getAssignments().isEmpty()) {
                    // if the assignment type is chase AND function is 6, no
                    // exception is required as Normal assignment will be
                    // requested after completing this request
                    if (getOperator().getCurrentWorkType().getFunctionType() == TaskFunctionType.NormalAndChaseAssignments
                        && getAssignmentTypeEnum() == AssignmentType.Chase) {
                        Response errorResponse = getEmptyAssignmentResponse();
                        ResponseRecord record = new TaskResponseRecord();
                        record
                            .setErrorFields(createMessageInfo(TaskErrorCode.NO_ASSIGNMENTS_FOUND));
                        errorResponse.addRecord(record);
                        return errorResponse;
                    } else {
                        throw new TaskCommandException(
                            TaskErrorCode.NO_ASSIGNMENTS_FOUND);
                    }
                } else if (getAssignments().size() == 1
                    && getNumberOfAssignmentsInt() > 1) {
                    try {
                        SYNC_SINGLE_THREAD.acquire();
                        getAdditionalMatchingAssignments();
                    } finally {
                        SYNC_SINGLE_THREAD.release();
                    }
                }

                // if not Automatic issuance then throw error because no
                // assignemnts
                // have been reserved or are active for operator
            } else {
                throw new TaskCommandException(
                    TaskErrorCode.NO_ASSIGNMENTS_RESERVED);
            }
        }

        // do final updates to the assignments and create labor records
        finalUpdates();

        // sort assignments in final list
        sortAssignments();

        // Build response object
        buildResponse();

        return getResponse();
    }

    /**
     * Method to create BaseTaskResponse response object with fields
     * @return Response object
     */
    private Response getEmptyAssignmentResponse() {
        Response errorResponse = new BaseTaskResponse();
        String fields[] = new String[11];
        fields[0] = "groupID";
        fields[1] = "isChase";
        fields[2] = "assignmentPK";
        fields[3] = "idDescription";
        fields[4] = "position";
        fields[5] = "goalTime";
        fields[6] = "route";
        fields[7] = "activeContainer";
        fields[8] = "passAssignment";
        fields[9] = "summaryPromptType";
        fields[10] = "overridePrompt";

        ((BaseTaskResponse) errorResponse).setFields(fields);

        return errorResponse;
    }

    /**
     * checks for and retrieves any assignments reserved for an operator and
     * filters list down to current region.
     * 
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void buildReservedAssignmentList() throws DataAccessException,
        BusinessRuleException {

        // Get List of assignment with the operator ID in the reserved fields
        List<Assignment> queriedAssignments = getAssignmentManager()
            .listReservedAssignments(getOperatorId());

        // Check each resever assignment to see if it is in the correct region
        for (Assignment a : queriedAssignments) {
            // Check for correct region
            if (a.getRegion().getId().equals(getRegion().getId())) {
                getAssignments().add(a);
                // Else unreserve the assignment as it should not have been
                // reserved in the first place
            } else {
                a.setReservedBy(null);
                getAssignmentManager().save(a);
            }
        }

    }

    /**
     * checks for and retrieves any assignments an operator may have in-progress
     * or suspended, and filters down to just assignments in current region.
     * 
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void buildActiveAssignmentList() throws DataAccessException,
        BusinessRuleException {

        Long groupNumber = null;

        // Get a list of active assignemtns for the operator making the request
        List<Assignment> queriedAssignments = getAssignmentManager()
            .listActiveAssignmentsByOperatorAndType(getOperator(),
                getAssignmentTypeEnum());

        // Check each assignment to see if it is in the correct region
        // Check In-progress and suspended first
        for (Assignment a : queriedAssignments) {
            // Only check for suspended or in-progress
            if (a.getStatus().isInSet(AssignmentStatus.Suspended,
                AssignmentStatus.InProgress)) {
                // Check for correct region, and only for a single group of
                // assignment
                // If active work there should always be a group number
                if ((a.getRegion().getId().equals(getRegion().getId()))
                    && (groupNumber == null || groupNumber.equals(a
                        .getGroupInfo().getGroupNumber()))) {
                    getAssignments().add(a);
                    a.setReservedBy(getOperatorId());
                    getAssignmentManager().save(a);
                    groupNumber = a.getGroupInfo().getGroupNumber();

                    // This should not occur, but if it does
                    // only issue out 1 assignment
                    if (groupNumber == null) {
                        break;
                    }
                }
            }
        }

        // Check each assignment to see if it is in the correct region
        // if no In-progress and suspended then check for available
        if (getAssignments().isEmpty()) {
            int requested = 1;

            for (Assignment a : queriedAssignments) {
                // Only check for Available
                if (a.getStatus().isInSet(AssignmentStatus.Available)) {
                    // Check for correct region, and only for a single group of
                    // assignment
                    // If active work there should always be a group number
                    if ((a.getRegion().getId().equals(getRegion().getId()))
                        && a.getGroupInfo().getGroupCount() <= getNumberOfAssignmentsInt()
                        && (groupNumber == null || groupNumber.equals(a
                            .getGroupInfo().getGroupNumber()))) {
                        getAssignments().add(a);
                        a.setReservedBy(getOperatorId());
                        groupNumber = a.getGroupInfo().getGroupNumber();
                        // Assign group request order if not already in group
                        if (groupNumber == null) {
                            a.getGroupInfo().setRequestedOrderNo(requested++);
                        }
                        getAssignmentManager().save(a);

                        if (getAssignments().size() >= getNumberOfAssignmentsInt()) {
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Retrieve the next available assignment for automatic issuance. This is
     * called if not assignments reserved and no assignment in-progress or
     * suspended.
     * 
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void getNextAvailableAssignment() throws DataAccessException,
        BusinessRuleException {
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        // Get List of assignment with the operator ID in the reserved fields
        List<Assignment> queriedAssignments = getAssignmentManager()
            .listAvailableAssignments(queryDecorator,
                getOperator(),
                getOperatorId(), // Reserved By
                getAssignmentTypeEnum(), getRegion().getId(),
                getNumberOfAssignmentsInt());

        // Should be only 1 assignment, but if more, then just get first one
        if (!queriedAssignments.isEmpty()) {
            Assignment a = queriedAssignments.get(FIRST_ASSIGNMENT);
            a.setReservedBy(getOperatorId());
            getAssignmentManager().save(a);
            if (a.getGroupInfo().getGroupNumber() == null) {
                a.getGroupInfo().setRequestedOrderNo(1);
            }
            getAssignments().add(a);

            // If assignment is already part of group then get rest
            // of group
            if (a.getGroupInfo().getGroupNumber() != null) {
                queriedAssignments = getAssignmentManager()
                    .listAssignmentsInGroup(a.getGroupInfo().getGroupNumber());

                if (queriedAssignments != null) {
                    // loop through list to add assignments
                    for (Assignment aGroup : queriedAssignments) {
                        // If assignment is not already in list
                        if (!getAssignments().contains(aGroup)) {
                            aGroup.setReservedBy(getOperatorId());
                            getAssignmentManager().save(aGroup);
                            getAssignments().add(aGroup);
                        }
                    }
                }
            }
        }
    }

    /**
     * Get additional assignment whether for the group or for when a user
     * request more than 1 assignment in a automatic issuance region.
     * 
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void getAdditionalMatchingAssignments()
        throws DataAccessException, BusinessRuleException {
        List<Assignment> queriedAssignments = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(ADDTIONAL_ASSIGNMENTS);

        // If number reserved so far is less than requested then get more
        // assignments that can be group
        queriedAssignments = getAssignmentManager().listAdditionalAssignments(
            queryDecorator, getOperator(), getAssignmentTypeEnum(),
            getRegion().getId());

        // check that we have a list
        if (queriedAssignments != null) {
            int requested = 2;

            // loop through list to add assignments
            for (Assignment a : queriedAssignments) {
                // If assignment is not already in list
                if (!getAssignments().contains(a)) {
                    a.setReservedBy(getOperatorId());
                    getAssignmentManager().save(a);
                    a.getGroupInfo().setRequestedOrderNo(requested++);
                    getAssignments().add(a);
                }

                // If the number of assignments requested have been reserved
                // then return
                if (getAssignments().size() >= getNumberOfAssignmentsInt()) {
                    return;
                }
            }
        }
    }

    /**
     * Sort the list of the assignments if the group position is NULL then sort
     * on Requested order else sort by the group position.
     */
    protected void sortAssignments() {
        if (getAssignments().get(0).getGroupInfo().getGroupPosition() != null) {
            Collections.sort(assignments, GROUP_POSITION);
        } else {
            Collections.sort(assignments, REQUEST_ORDER);
        }
    }

    /**
     * Do the final updates to the assignment before issueing them out to the
     * operator.
     * 
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void finalUpdates() throws DataAccessException,
        BusinessRuleException {

        // Group assignments if not already grouped
        if (getAssignments().get(0).getGroupInfo().getGroupNumber() == null) {
            getAssignmentManager().executeGroupAssignments(getAssignments(),
                false);
        }

        for (Assignment a : getAssignments()) {
            a.startAssignment(getOperator(), getCommandTime());
            a.setReservedBy(null);
            // create assignment labor record for each assignment
            this.getLaborManager().openAssignmentLaborRecord(
                this.getCommandTime(), this.getOperator(), a);
        }

        // calculate base items within assignments
        calculatebaseItems();

    }

    /**
     * build the response object to send back to terminal.
     * @throws DataAccessException - Database Exception
     */
    protected void buildResponse() throws DataAccessException {
        // Build response

        for (Assignment a : getAssignments()) {
            getResponse().addRecord(buildResponseRecord(a));
        }
    }

    /**
     * Builds the response record.
     * 
     * @param a - the assignment to use for the response
     * @return record - the response record
     * @throws DataAccessException - Database Exception
     */
    protected ResponseRecord buildResponseRecord(Assignment a)
        throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("groupID", a.getGroupInfo().getGroupNumber());
        if (a.getType().equals(AssignmentType.Chase)) {
            record.put("isChase", true);
        } else {
            record.put("isChase", false);
        }
        record.put("assignmentPK", a.getId());
        record.put("idDescription", a.getNumber());
        record.put("position", a.getGroupInfo().getGroupPosition());
        record.put("goalTime", new BigDecimal(a.getSummaryInfo().getGoalTime())
            .setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        record.put("route", a.getRoute());
        record.put("activeContainer", "00"); // Always send 00
        record.put("passAssignment", 0);
        record
            .put("summaryPromptType", getRegion().getUserSettings(a.getType())
                .getSummaryPromptIndicator().toValue());

        // if Use configure prompt and prompt is defined for region.
        if (getRegion().getUserSettings(a.getType())
            .getSummaryPromptIndicator()
            .equals(SelectionRegionSummaryPrompt.ConfiguredPrompt)
            && getRegion().getUserSettings(a.getType()).getSummaryPrompt() != null) {
            record.put(
                "overridePrompt",
                getSummaryPromptManager().buildSummaryPrompt(a,
                    getAssignments(), getTerminal().getTaskLocale()));
        } else {
            record.put("overridePrompt", "");
        }

        return record;
    }

    /**
     * Determines if base items should be calculated and loops through
     * assignments in group to calculate the base items within the assignment.
     * 
     * @throws BusinessRuleException - business rule exception
     * @throws DataAccessException - Database exceptions
     */
    protected void calculatebaseItems() throws BusinessRuleException,
        DataAccessException {

        if (getAssignments().size() <= 0) {
            return;
        }

        // Check if region allows based items
        if (getAssignments().get(0).getRegion()
            .getProfile(getAssignments().get(0).getType()).isAllowBaseItems()) {
            for (Assignment a : getAssignments()) {
                if (!a.isCalculatedBaseItems()) {
                    calculateBaseItemsForAssignment(a);
                }
            }
        }
    }

    /**
     * Calculates the base items for a signle assignment.
     * 
     * @param a - assignment to calculte
     * @throws BusinessRuleException - business rule exception
     * @throws DataAccessException - Dtaabase exceptions
     */
    protected void calculateBaseItemsForAssignment(Assignment a)
        throws BusinessRuleException, DataAccessException {

        // Loop through picks
        for (Pick p : a.getPicks()) {
            // If pick is not already a base item calculate
            // if it should be a base item
            if (p.getStatus() == PickStatus.NotPicked) {

                // get total weight and quantity of item
                double weight = 0.0;
                int quantity = 0;
                for (Pick p2 : a.getPicks()) {
                    // Sum quantity
                    if (p2.getStatus() == PickStatus.NotPicked
                        && groupPicksForbaseItemCalculation(p, p2)) {
                        quantity += p2.getQuantityToPick();
                    }
                }
                weight = p.getItem().getWeight();

                // Set picks to base item
                if (setAsBaseItem(p, weight, quantity)) {
                    for (Pick p2 : a.getPicks()) {
                        if (p2.getStatus() == PickStatus.NotPicked
                            && groupPicksForbaseItemCalculation(p, p2)) {
                            p2.setPickAsBaseItem();
                        }
                    }
                }

            }
        }

        // Set picks for rest of aisle
        setBaseItemByAisle(a);

        // Set assignment as being calculated
        a.setCalculatedBaseItems(true);
    }

    /**
     * Determin if the 2 picks are at the same location, for the same item and
     * with the same UOM to group for base item calculation.
     * 
     * @param p1 - first pick to compare
     * @param p2 - second pick to compare
     * @return - return true if the 2 picks are for the same location, item and
     *         UOM
     */
    protected boolean groupPicksForbaseItemCalculation(Pick p1, Pick p2) {
        boolean returnValue = true;

        // not same location
        if (!p2.getLocation().equals(p1.getLocation())) {
            returnValue = false;
        }

        // not same items
        if (!p2.getItem().equals(p1.getItem())) {
            returnValue = false;
        }

        // Compare UOM's
        if (!compareStrings(p1.getUnitOfMeasure(), p2.getUnitOfMeasure())) {
            returnValue = false;
        }

        return returnValue;
    }

    /**
     * determines if item should be a base item.
     * 
     * @param p - pick to check
     * @param weight - weight of all same picks in assignment
     * @param quantity - quantity of all same picks in assignment
     * @return - return true if item should be a base item
     * @throws DataAccessException - Database exceptions
     */
    protected boolean setAsBaseItem(Pick p, double weight, int quantity)
        throws DataAccessException {
        boolean returnValue = true;
        double weightThreshold = getAssignments().get(0).getRegion()
            .getUserSettings(getAssignments().get(0).getType())
            .getBaseWeightThreshold();
        int quantityThreshold = getAssignments().get(0).getRegion()
            .getUserSettings(getAssignments().get(0).getType())
            .getBaseQuantityThreshold();

        // Verify Replenishement is on and location is empty so don't
        // set as base item
        if (getAssignments().get(0).getRegion()
            .getUserSettings(getAssignments().get(0).getType())
            .isVerifyReplenishment()
            && p.getLocation().getStatus() == LocationStatus.Empty) {
            returnValue = false;
        }

        // If quantity and weight are not both over threshold
        // and not override then don't set as base item
        if ((quantity < quantityThreshold || weight < weightThreshold)
            && !p.isBaseItemOverride()) {
            returnValue = false;
        }

        return returnValue;
    }

    /**
     * Sets picks to base item for aisle that have picks already base item when
     * region defined to pick base items by aisle.
     * 
     * @param a - Assignment to check
     * @throws BusinessRuleException - business rule exceptions
     */
    protected void setBaseItemByAisle(Assignment a)
        throws BusinessRuleException {

        // If picking base items by aisle
        if (a.getRegion().getUserSettings(a.getType()).getPickBaseItemsBy()
            .equals(SelectionRegionBaseItem.PickByAisle)) {

            for (Pick p1 : a.getPicks()) {
                // If pick is base item
                if (p1.getStatus().equals(PickStatus.BaseItem)) {
                    // set all other picks in same aisle to be base item
                    for (Pick p2 : a.getPicks()) {
                        if (compareStrings(p2.getLocation().getDescription()
                            .getPreAisle(), p1.getLocation().getDescription()
                            .getPreAisle())
                            && compareStrings(p2.getLocation().getDescription()
                                .getAisle(), p1.getLocation().getDescription()
                                .getAisle())

                        ) {
                            p2.setPickAsBaseItem();
                        }
                    } // end inner loop
                }
            } // end outer loop
        } // end if for picking by aisle
    }

    /**
     * Compare to string to see if equal. Null treated as empty string
     * @param value1 - first string
     * @param value2 - second string
     * @return - true if equal.
     */
    private boolean compareStrings(String value1, String value2) {
        if (value1 == null) {
            value1 = "";
        }

        if (value2 == null) {
            value2 = "";
        }

        return value1.equals(value2);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 