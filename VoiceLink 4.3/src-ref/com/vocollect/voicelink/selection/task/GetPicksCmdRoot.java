/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.LocationDescription;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import static com.vocollect.voicelink.core.model.LocationStatus.Empty;
import static com.vocollect.voicelink.core.model.LocationStatus.Replenished;
import static com.vocollect.voicelink.core.model.LocationStatus.Unknown;
import static com.vocollect.voicelink.selection.model.PickStatus.BaseItem;
import static com.vocollect.voicelink.selection.model.PickStatus.NotPicked;
import static com.vocollect.voicelink.selection.model.PickStatus.Partial;
import static com.vocollect.voicelink.selection.model.PickStatus.ShortsGoBack;
import static com.vocollect.voicelink.selection.model.PickStatus.Skipped;
import static com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts.Always;
import static com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts.IfReplenished;
import static com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts.Never;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * 
 * 
 * @author pfunyak
 */
public abstract class GetPicksCmdRoot extends BaseSelectionTaskCommand {

    private static final long serialVersionUID = 804122582426392620L;
    
    private static final double HUNDRED_PERCENT = 100.0;
    
    private List<Pick>        picksToReturn;
    private boolean           shortsAndSkipsFlag;
    private int               goBackForShortsIndicator;
    private short             pickOrderFlag;
    
    private LocationManager   locationManager;
    private NotificationManager notificationManager;


    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }


    
    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    
    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * Gets the value of the shorts and skips flag.<br> 
     * Indicates how to return picks.
     * @return  
     * <b>true</b> - return shorted and skipped picks only.<br>
     * <b>false</b> - return all picks including shorted and skipped.
     */
    public boolean isShortsAndSkipsFlag() {
        return this.shortsAndSkipsFlag;
    }

    /**
     * Sets the shorts and skips flag. 
     * @param shortsAndSkipsFlag <br>
     * <br>
     * <b>true</b> - return shorted and skipped picks only.<br>
     * <b>false</b> - return all picks including shorted and skipped.
     */
    public void setShortsAndSkipsFlag(boolean shortsAndSkipsFlag) {
        this.shortsAndSkipsFlag = shortsAndSkipsFlag;
    }

    /**
     * Return the integer value of the Go Back For Shorts Indicator.<br>
     * Determines when to return picks that have the status ShortsGoBack. 
     * @return <b>0</b> - Never <br> <b>1</b> - Always <br> <b>2</b>- If replenished
     */
    public int getGoBackForShortsIndicator() {
        return this.goBackForShortsIndicator;
    }
        
    /**
     * Gets the enumerated value of the goBackForShortsIndicator. <br>
     * Determines when to return picks that have the status ShortsGoBack. 
     * @return <b>Never</b> <br> <b>Always</b> <br> <b>IfReplenished</b>
     */
    public SelectionRegionGoBackForShorts getGoBackForShortsIndicatorEnum() {
        return SelectionRegionGoBackForShorts.toEnum(this.goBackForShortsIndicator);
    }

    /**
     * Set the Go Back For Shorts Indicator.<br>
     * Determines when to return picks that have the status ShortsGoBack.
     * @param goBackForShortsIndicator <br><br>
     * <b>0</b> - Never <br> <b>1</b> - Always <br> <b>2</b> - If replenished
     */
    public void setGoBackForShortsIndicator(int goBackForShortsIndicator) {
        this.goBackForShortsIndicator = goBackForShortsIndicator;
    }

    /**
     * Get the pick order flag.<br>
     * Determines which order to return picks to the caller.
     * @return 
     *  <b>0</b> - Normal order<br> <b>1</b> - Reverse order
     */
    public short getPickOrderFlag() {
        return this.pickOrderFlag;
    }

    /**
     * Sets the pick order flag.<br> 
     * Determines which order to return picks to the caller.
     * @param pickOrderFlag <br><br>
     *  <b>0</b> - normal <br>
     *  <b>1</b> - reverse order
     */
    public void setPickOrderFlag(short pickOrderFlag) {
        this.pickOrderFlag = pickOrderFlag;
    }

    /**
     * Get the list of picks.
     * @return List of picks.
     */
    public List<Pick> getPicksToReturn() {
        return this.picksToReturn;
    }

    
    /**
     * Sets the picksToReturn property.
     * @param picksToReturn <br><br> List of picks.
     */
    public void setPicksToReturn(List<Pick> picksToReturn) {
        this.picksToReturn = picksToReturn;
    }
    
    /** ******************************************* */
    /* END Getter and Setter Methods */
    /** ******************************************* */

    // Comparators to sort picks 
    
    /**
     * Compares two strings.  
     * 
     * @param str1 - first string <br>
     * @param str2 - second string <br>
     * @return
     *  <b>negative integer value</b> - when str1 < str2 <br>
     *  <b>zero</b> - when str1 = str2 <br>
     *  <b>positive integer value</b> - when str1 > str2 <br>

     */
    private static final int compareStringObjects(String str1, String str2) { 
        int result = 0;
        if ((str1 == null) && (str2 == null)) {
            result = 0;
        } else if (str1 == null) {
            result =  str2.compareToIgnoreCase("");
        } else if (str2 == null) {
            result = str1.compareToIgnoreCase("");
        } else {
            result = str1.compareToIgnoreCase(str2);
        }
        return result;
    }
    
 

    static final Comparator<Pick> ORDER_PICKS = 
        new Comparator<Pick>() {
        public int compare(Pick p1, Pick p2) {
            
            LocationDescription loc1 = p1.getLocation().getDescription();
            LocationDescription loc2 = p2.getLocation().getDescription();
            
            int result = 0;
            
            // Check the group count to see if we have a single assignment.
            // If we have a single assignment, use the sequence number as
            // one of the sort criteria.
            if (p1.getAssignment().getGroupInfo().getGroupCount() == 1) {
                result = p1.getSequenceNumber().compareTo(p2.getSequenceNumber());
                if (result != 0) {
                    return result;
                }    
            }
            result = compareStringObjects(loc1.getPreAisle(), loc2.getPreAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getAisle(), loc2.getAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getPostAisle(), loc2.getPostAisle());
            if (result != 0) {
                return result;
            }
            result = compareStringObjects(loc1.getSlot(), loc2.getSlot());
            if (result != 0) {
                return result;
            }
            return p1.getAssignment().getGroupInfo().getGroupPosition()
                .compareTo(p2.getAssignment().getGroupInfo().getGroupPosition());
        }
     };
 
     
     /**
     * 
     * Return the filtered list if picks for the given group number.
     * @return - the list of picks
     * @throws Exception - database exception
     */
     @Override
    public Response doExecute() throws Exception {

        getPicks();
        // if there is nothing in the list after this call then throw an exception
        if (this.picksToReturn.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NO_PICKS_IN_GROUP);
        }

        // sort picks based on single or grouped assignment 
        sortPicks();
        
        // order picks (foward or reverse) based on pick order flag
        orderPicks();
        
        // build the response record and return.        
        buildResponse();
        
        return getResponse();
    }

    /**
     * Return the "filtered" list of picks.<br><br>
     * This procedure gets the full pick list for a given group and filters <br>
     * out the picks that should not be returned.
     *  @throws DataAccessException - database exception
     * @throws BusinessRuleException - Business exception
     */
    protected void getPicks() throws DataAccessException, BusinessRuleException {

        // picksForGroup contains ALL of the picks for the group regardless of pick status.
        //List<Pick> picksForGroup = getPickManager().listGroupsPicks(getGroupNumber());
        
        // pickToReturn will contain the filtered list of picks.
        this.picksToReturn = new ArrayList<Pick>();
        
        /*
        * Loop through the list of picks for the group and add picks to the return pick list.
        * NOTE: Picks with a status of ShortsGoBack should only be returned if the GoBackForShortsIndicator is
        * equal to "always" or "ifReplinshed"  If the goBackForShortsIndicator is set to "IfReplenished" then
        * check the location status to see if the slot was actually replenished.
        * Skipped picks should ALWAYS be included in the list.
        * BaseItems and NotPicked should only be included if the the isShortsAndSkipsFlag is false.
        */
        for (Assignment a : getAssignments()) {
            for (Pick p : a.getPicks()) {
                if (p.getStatus().isInSet(BaseItem, NotPicked) 
                    && !this.isShortsAndSkipsFlag()) { 
                    this.picksToReturn.add(p); 
                } else if (p.getStatus().isInSet(Skipped, Partial)) {
                    this.picksToReturn.add(p);
                } else if (p.getStatus() == ShortsGoBack) {
                    if (getGoBackForShortsIndicatorEnum() == Always) {
                        this.picksToReturn.add(p);
                    } else if (getGoBackForShortsIndicatorEnum() == IfReplenished) {
                      if (p.getLocation().getStatus(p.getItem()) == Replenished
                          || p.getLocation().getStatus(p.getItem()) == Unknown) {
                            this.picksToReturn.add(p);
                      }      
                  }            
                }
               // if the pick should trigger a replenishment, and if the pick location is replinished, 
                // report a short item and reset the trigger replenishment flag.
                if (p.getStatus().isInSet(BaseItem, NotPicked, Skipped, ShortsGoBack)) {
                    if ((p.isTriggerReplenish()) 
                        && (p.getLocation().getStatus(p.getItem()) == Replenished)) {
                        try {
                            getLocationManager().triggerReplenishment(
                                p.getLocation(), p.getItem());
                        } catch (BusinessRuleException e) {
                            if (e.getErrorCode() 
                                == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
                                // Do nothing --
                                // we don't want the device to know about this
                                // LocationManager will have posted a Notificaiton
                                // about this.
                            } else {
                                throw e;
                            }
                        }
                    }    
                    p.setTriggerReplenish(false);
                }
            } // end for loop
        }
    }

    
    /**
     * Order the list of picks based on the pickOrderFlag.
     * @throws DataAccessException - database exception
     */
    protected void orderPicks() throws DataAccessException {
        
        if (this.getPickOrderFlag() == 1) {   
            Collections.reverse(this.picksToReturn);
        }    
    }

    /**
     * Sort the list of filtered picks.
     * 
     */
    protected void sortPicks() {
        Collections.sort(this.picksToReturn, ORDER_PICKS);
    }

    /**
     * Build the list of picks to return to doExecute().
     * @throws DataAccessException - Database Exceptions
     * 
     */
    protected void buildResponse() throws DataAccessException {
        for (Pick p : this.picksToReturn) {
            getResponse().addRecord(buildResponseRecord(p));
        }
    }
    
    /**
     * Builds the response record.
     * 
     * @param p - the pick to use for the response
     * @return record - the response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(Pick p) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        //translate pick status
        switch(p.getStatus()) {
            case BaseItem     : record.put("status", "B"); break;
            case NotPicked    : record.put("status", "N"); break; 
            case Skipped      : record.put("status", "S"); break;
            case ShortsGoBack : 
                     { if (this.getGoBackForShortsIndicatorEnum() == Never) {
                         record.put("status", "N"); break;                      
                       } else {
                         record.put("status", "G"); break;
                       }
                     }
            default : record.put("status", "N");    
        }
        record.put("baseItem", p.getIsBaseItem());
        record.put("pickID", p.getId());
        record.put("locationID", p.getLocation().getId());
        record.put("regionNumber", p.getAssignment().getRegion().getNumber());
        record.put("preAisleDirection", 
            translateUserData(p.getLocation().getDescription().getPreAisle()));
        record.put("aisle", createPhoneticString(p.getLocation()
            .getDescription().getAisle()));
        record.put("postAisleDirection", 
            translateUserData(p.getLocation().getDescription().getPostAisle()));
        record.put("slot", createPhoneticString(p.getLocation()
            .getDescription().getSlot()));
        record.put("quantityToPick", p.getQuantityToPick());
        record.put("uom", translateUserData(p.getUnitOfMeasure()));
        record.put("itemNumber", p.getItem().getNumber());
        record.put("variableWeightItem", p.getItem().getIsVariableWeightItem());
        double minWeight = 0.0;
        double maxWeight = 0.0;
        if (p.getItem().getIsVariableWeightItem()) {
            
            minWeight = p.getItem().getWeight()
                      - (p.getItem().getVariableWeightTolerance() / HUNDRED_PERCENT)
                      * p.getItem().getWeight();
            maxWeight = p.getItem().getWeight() 
                      + (p.getItem().getVariableWeightTolerance() / HUNDRED_PERCENT) 
                      * p.getItem().getWeight();
        }
        record.put("minimumWeightAllowed", minWeight);
        record.put("maximumWeightAllowed", maxWeight);
        record.put("quantityPicked", p.getQuantityPicked());
        record.put("checkDigits", p.getLocation().getCheckDigits()); 
        record.put("scanProductID", p.getItem().getScanVerificationCode());
        record.put("spokenProductID", p.getItem().getSpokenVerificationCode());
        record.put("itemDescription", 
                   getTranslatedItemPhoneticDescription(p.getItem()));
        record.put("itemSize", p.getItem().getSize());
        record.put("itemUPC", p.getItem().getUpc());
        record.put("assignmentPK", p.getAssignment().getId());
        record.put("idDescription", p.getAssignment().getNumber());
        record.put("deliveryLocation", p.getAssignment().getDeliveryLocation());
        record.put("dummyField", 0);
        record.put("customerNumber", p.getAssignment().getCustomerInfo().getCustomerNumber());
        record.put("caseLabelCheckDigit", p.getCaseLabelCheckDigit());
        record.put("targetContainer", p.getTargetContainerIndicator());
        if (p.getItem().getLotCount() > 0) {
            record.put("captureLotText", ResourceUtil.getLocalizedMessage("lot.number.prompt",
                null, getTerminal().getTaskLocale()));
            record.put("captureLotFlag", 1);
        } else {
            record.put("captureLotText", "");
            record.put("captureLotFlag", 0);
        }
        record.put("promptMessage", translateUserData(p.getPromptMessage()));
        if ((p.getAssignment().getRegion()
            .getUserSettings(p.getAssignment().getType())
            .isVerifyReplenishment()) 
            && ((p.getLocation().getStatus(p.getItem()) == Empty)
                || (p.getLocation().getStatus(p.getItem()) == LocationStatus.InvalidLot))) {
            record.put("verifyLocationFlag", 1);
        } else {
            record.put("verifyLocationFlag", 0);
        }
        record.put("cycleCount", 0);
        record.put("serialNumber", p.getItem().getIsSerialNumberItem());
        // multiple items per location
        if (p.getLocation().getStatus().equals(LocationStatus.Multiple)) {
            record.put("multipleItemLocation", 1);
        } else {
            record.put("multipleItemLocation", 0);
        }
        
        return record;
    }


}



*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 