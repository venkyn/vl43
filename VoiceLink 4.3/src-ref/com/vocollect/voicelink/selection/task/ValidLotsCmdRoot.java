/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;

import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class ValidLotsCmdRoot extends BaseLotTaskCommandRoot {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private Long      assignmentID;
    
    private Long      groupID;
    
    private Long      locationID;
    
    private String    itemNumber;
    
    private Long      pickID;
    
    
    /**
     * Getter for the assignmentID property.
     * @return Long value of the property
     */
    public Long getAssignmentID() {
        return assignmentID;
    }

    /**
     * Setter for the assignmentID property.
     * @param assignmentID the new assignmentID value
     */
    public void setAssignmentID(Long assignmentID) {
        this.assignmentID = assignmentID;
    }
    
    /**
     * Getter for the groupID property.
     * @return Long value of the property
     */
    public Long getGroupID() {
        return groupID;
    }

    /**
     * Setter for the groupID property.
     * @param groupID the new groupID
     */
    public void setGroupID(Long groupID) {
        this.groupID = groupID;
    }
    
    /**
     * Getter for the locationID property.
     * @return Long value of the property
     */
    public Long getLocationID() {
        return locationID;
    }

    /**
     * Setter for the locationID property.
     * @param locationID the new locationID
     */
    public void setLocationID(Long locationID) {
        this.locationID = locationID;
    }
    
    /**
     * Getter for the itemNumber property.
     * @return Long value of the property
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * Setter for the itemNumber property.
     * @param itemNumber the new itemNumber
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }
    
    /**
     * Getter for the sequence property.
     * @return Long value of the property
     */
    public Long getPickID() {
        return pickID;
    }

    /**
     * Setter for the sequence property.
     * @param pickID the new pickID
     */
    public void setPickID(Long pickID) {
        this.pickID = pickID;
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        getValidLots();
        buildResponse();
        return getResponse();
    }

    /**
     * Builds a list of valid lots.
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task command exception 
     */    
    protected void getValidLots() throws DataAccessException, TaskCommandException {
        setLots(new ArrayList<Lot>(getPickManager().get(getPickID()).getItem().getLots()));
        validateLotExpirations(null, getItemNumber());
        validateLotLocations(getPickID(), null);
    }   
            
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (Lot l : getLots()) {
            getResponse().addRecord(buildResponseRecord(l));
        }
    }
    
    /**
     * Builds the response record.
     * @param lot the lot to transmit
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(Lot lot) {
        ResponseRecord record = new TaskResponseRecord();
        record.put("lotNumber", lot.getNumber());
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 