/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.ApplicationContextHolder;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.selection.events.ShortEvent;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.voicelink.selection.model.PickDetailType.TaskPicked;
import static com.vocollect.voicelink.selection.model.PickStatus.BaseItem;
import static com.vocollect.voicelink.selection.model.PickStatus.Markout;
import static com.vocollect.voicelink.selection.model.PickStatus.NotPicked;
import static com.vocollect.voicelink.selection.model.PickStatus.Partial;
import static com.vocollect.voicelink.selection.model.PickStatus.Shorted;
import static com.vocollect.voicelink.selection.model.PickStatus.ShortsGoBack;
import static com.vocollect.voicelink.selection.model.PickStatus.Skipped;

/**
 * 
 * 
 * @author pfunyak *
 */
public abstract class PickedODRCmdRoot extends BaseSelectionTaskCommand {

    //
    private static final long serialVersionUID = 668730777306196074L;

    private long assignmentID;
    private long locationID;
    private int quantityPicked;
    private boolean endOfPartialPickFlag;

    // This needs to be a String because an empty String is sent by the
    // task when there is no container. If it is not-null or empty, the
    // data must be convertable to a long number.
    private String containerID;

    private long pickID;
    private String lotNumber;
    private String variableWeight;
    private String itemSerialNumber;

    private Pick thePick;

    private LocationManager locationManager;
    private ShortEvent shortEvent;

    /********************************************* */
    /* Getter and Setter Methods */
    /********************************************* */

    /**
     * Getter for the locationManager property.
     * 
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Setter for the locationManager property.
     * 
     * @param locationManager
     *            the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the shortEvent
     */
    public ShortEvent getShortEvent() {
        return shortEvent;
    }

    /**
     * @param shortEvent
     *            the shortEvent to set
     */
    public void setShortEvent(ShortEvent shortEvent) {
        this.shortEvent = shortEvent;
    }

    /**
     * Getter for the assignmentID property.
     * 
     * @return long value of the property
     */
    public long getAssignmentID() {
        return this.assignmentID;
    }

    /**
     * Setter for the assignmentID property.
     * 
     * @param assignmentID
     *            the new assignmentID value
     */
    public void setAssignmentID(long assignmentID) {
        this.assignmentID = assignmentID;
    }

    /**
     * Getter for the locationID property.
     * 
     * @return long value of the property
     */
    public long getLocationID() {
        return this.locationID;
    }

    /**
     * Setter for the locationID property.
     * 
     * @param locationID
     *            the new locationID value
     */
    public void setLocationID(long locationID) {
        this.locationID = locationID;
    }

    /**
     * Getter for the quantityPicked property.
     * 
     * @return int value of the property
     */
    public int getQuantityPicked() {
        return this.quantityPicked;
    }

    /**
     * Setter for the quantityPicked property.
     * 
     * @param quantityPicked
     *            the new quantityPicked value
     */
    public void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * Getter for the endOfPartialPickFlag property.
     * 
     * @return boolean value of the property
     */
    public boolean isEndOfPartialPickFlag() {
        return this.endOfPartialPickFlag;
    }

    /**
     * Setter for the endOfPartialPickFlag property.
     * 
     * @param endOfPartialPickFlag
     *            the new endOfPartialPickFlag value
     */
    public void setEndOfPartialPickFlag(boolean endOfPartialPickFlag) {
        this.endOfPartialPickFlag = endOfPartialPickFlag;
    }

    /**
     * Getter for the containerID property.
     * 
     * @return long value of the property
     */
    public String getContainerID() {
        return this.containerID;
    }

    /**
     * Get the container for this pick. This will be retrieved from the database
     * via the containerID specified in the command.
     * 
     * @return the Container, or null if no containerID was specified.
     * @throws DataAccessException
     *             on failure to retrieve.
     */
    public Container getContainer() throws DataAccessException {
        if (StringUtil.isNullOrEmpty(this.containerID)) {
            return null;
        } else {
            return getContainerManager().get(Long.valueOf(this.containerID));
        }
    }

    /**
     * Setter for the containerID property.
     * 
     * @param containerID
     *            the new containerID value
     */
    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * Getter for the pickID property.
     * 
     * @return long value of the property
     */
    public long getPickID() {
        return this.pickID;
    }

    /**
     * Setter for the pickID property.
     * 
     * @param pickID
     *            the new pickID value
     */
    public void setPickID(long pickID) {
        this.pickID = pickID;
    }

    /**
     * Getter for the lotNumber property.
     * 
     * @return long value of the property
     */
    public String getLotNumber() {
        return this.lotNumber;
    }

    /**
     * Setter for the lotNumber property.
     * 
     * @param lotNumber
     *            the new lotNumber value
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;

    }

    /**
     * Getter for the variableWeight property.
     * 
     * @return double value of the property
     */
    public Double getVariableWeightAsLong() {
        if (StringUtil.isNullOrEmpty(getVariableWeight())) {
            return null;
        } else {
            return Double.valueOf(getVariableWeight());
        }
    }

    /**
     * Getter for the variableWeight property.
     * 
     * @return double value of the property
     */
    public String getVariableWeight() {
        return this.variableWeight;
    }

    /**
     * Setter for the variableWeight property.
     * 
     * @param variableWeight
     *            the new variableWeight value
     */
    public void setVariableWeight(String variableWeight) {
        this.variableWeight = variableWeight;
    }

    /**
     * Getter for the itemSerialNumber property.
     * 
     * @return String value of the property
     */
    public String getItemSerialNumber() {
        if (StringUtil.isNullOrEmpty(this.itemSerialNumber)) {
            return null;
        } else {
            return itemSerialNumber;
        }
    }

    /**
     * Setter for the itemSerialNumber property.
     * 
     * @param itemSerialNumber
     *            the new serialNumber value
     */
    public void setItemSerialNumber(String itemSerialNumber) {
        this.itemSerialNumber = itemSerialNumber;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        validatePick();
        updatePick();

        return getResponse();
    }

    /**
     * @throws DataAccessException
     *             - database exception
     * @throws TaskCommandException
     *             - Task Command exception
     */
    protected void validatePick() throws DataAccessException,
            TaskCommandException {

        // get the pick we intend to update.
        this.thePick = getPickManager().get(getPickID());

        // can not find the pick
        if (this.thePick == null) {
            throw new TaskCommandException(
                    TaskErrorCode.PICKED_ODR_PICK_NOT_FOUND);
        }
        // Force Load of Assignment and region
        this.thePick.getAssignment();
        this.thePick.getAssignment().getRegion();

        // quantity picked must be positive
        if (this.quantityPicked < 0) {
            throw new TaskCommandException(
                    TaskErrorCode.PICKED_ODR_NEGATIVE_QUANTITY_PICKED_VALUE);
        }
        // if the picks status is NOT in the given set
        if (!this.thePick.getStatus().isInSet(BaseItem, NotPicked, Skipped,
                Partial, ShortsGoBack)) {
            throw new TaskCommandException(
                    TaskErrorCode.PICKED_ODR_INVALID_PICK_STATUS);
        }
    }
    
    /**
     * Publishes a short event with the pick detail provided
     * @param pickDetail The pick information to public
     */
    protected void publishShortEvent(PickDetail pickDetail) {
        shortEvent.setPickTime(pickDetail.getPickTime());
        shortEvent.setOriginalAssignmentNo(pickDetail.getPick()
                .getAssignment().getNumber());
        shortEvent.setLocationID(pickDetail.getPick().getLocation()
                .getScannedVerification());
        shortEvent
                .setItemNo(pickDetail.getPick().getItem().getNumber());
        shortEvent.setOperatorID(pickDetail.getOperator()
                .getOperatorIdentifier());
        shortEvent.setPickedQty(pickDetail.getQuantityPicked());

        shortEvent.setSiteId(SiteContextHolder.getSiteContext()
                .getCurrentSite().getId());

        ApplicationContextHolder.getApplicationContext().publishEvent(
                shortEvent);
    }


    /**
     * @throws DataAccessException
     *             - database exception
     * @throws BusinessRuleException
     *             - business rule exception
     */
    protected void updatePick() throws DataAccessException,
            BusinessRuleException {

        // create a new pick detail object
        PickDetail pickDetail = new PickDetail();
        // fill it up
        pickDetail.setType(TaskPicked);
        pickDetail.setOperator(getOperator());
        pickDetail.setPickTime(this.getCommandTime());
        pickDetail.setQuantityPicked(this.quantityPicked);
        if (this.quantityPicked > 0) {
            pickDetail.setVariableWeight(getVariableWeightAsLong());
            pickDetail.setContainer(getContainer());
            pickDetail.setSerialNumber(getItemSerialNumber());
        }

        // If the lot number exists and isn't blank, set the detail record
        if (!StringUtil.isNullOrBlank(getLotNumber())) {
            pickDetail.setLotNumber(getLotNumber());
        }

        // add the details
        thePick.addPickDetail(pickDetail, !endOfPartialPickFlag);

        // see if we need to report a short
        if (thePick.getStatus().isInSet(Shorted, ShortsGoBack, Markout)) {
            try {
                getLocationManager().reportShort(thePick.getLocation(),
                        thePick.getItem());
                
                publishShortEvent(pickDetail);
                
            } catch (BusinessRuleException e) {
                if (e.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED) {
                    // Do nothing --
                    // we don't want the device to know about this
                    // LocationManager will have posted a Notificaiton
                    // about this.
                } else {
                    throw e;
                }
            }
        }

        // Update carton in lineloading if needed
        getPickManager().executeUpdateLineLoading(thePick);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 