/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class RegionPermissionsForWorkTypeCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private String                  selectedWorkType;
    
    private List<Assignment>        assignments = new ArrayList<Assignment>();
    
    private List<Region>            returnRegion = new ArrayList<Region>();
    
    private RegionManager           regionManager;
    
    private TaskErrorCode           inProgressError = null;
    
    private TaskFunctionManager     taskFunctionManager;

    private Boolean                 assignedRegionWorktypeConflict = false;
    
    /**
     * Getter for the taskFunctionManager property.
     * @return TaskFunctionManager value of the property
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    
    /**
     * Setter for the taskFunctionManager property.
     * @param taskFunctionManager the new taskFunctionManager value
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * Getter for the inProgressError property.
     * @return SelectionRegionManager value of the property
     */
    public TaskErrorCode getInProgressError() {
        return inProgressError;
    }

    /**
     * Setter for the inProgressError property.
     * @param inProgressError the new inProgressError value
     */
    public void setInProgressError(TaskErrorCode inProgressError) {
        this.inProgressError = inProgressError;
    }

    /**
     * Getter for the regionManager property.
     * @return regionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    
    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the selected work type property.
     * @return String value of the property
     */
    public String getSelectedWorkType() {
        return selectedWorkType;
    }
    
    /**
     * @return get the selected work type as an Enum value.
     */
    public TaskFunctionType getSelectedWorkTypeEnum() {
        return TaskFunctionType.toEnum(Integer.parseInt(selectedWorkType));
    }
    
    /**
     * Setter for the selected work type property.
     * @param selectedWorkType the new selectedWorkType value
     */
    public void setSelectedWorkType(String selectedWorkType) {
        this.selectedWorkType = selectedWorkType;
    }
    
    /**
     * Getter for the assignments property.
     * @return List&lt;Assignment&gt; value of the property
     */
    @Override
    public List<Assignment> getAssignments() {
        return assignments;
    }
    
    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    @Override
    public void setAssignments(List<Assignment> assignments) {
        this.assignments = assignments;
    }
    
    /**
     * Getter for the return region property.
     * @return List&lt;Region&gt; value of the property
     */
    public List<Region> getReturnRegion() {
        return returnRegion;
    }

    /**
     * Setter for the in progress region property.
     * @param returnRegion the new returnRegion value
     */
    public void setReturnRegion(List<Region> returnRegion) {
        this.returnRegion = returnRegion;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //clear previous reserved assignments
        getAssignmentManager().executeClearReservedAssignments(getOperator());
        
        //Get a list of authorized regions
        getAuthorizedRegions();
        
        if (!checkForInProgressWork()) {
            getAssignedToRegion();
        }
        
        buildResponse();
        // Clear regions signed onto - Done after build response
        //because build response needs to know if they were currently in a region
        resetOperatorsRegions();

        getOperator().setCurrentWorkType(
            getTaskFunctionManager().findByType(getSelectedWorkTypeEnum()));
        return getResponse();
    }
    
    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        
        // Create a return record, populate it, and add to response
        for (Region r : getReturnRegion()) {
            getResponse().addRecord(buildResponseRecord(r));   
        }
    }
    
    /**
     * Builds the response record.
     * 
     * @param r - region to use for response
     * @throws DataAccessException - database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(Region r) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        
        if (getInProgressError() != null) {
            
            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.regionNumber", 
                getReturnRegion().get(0).getNumber());
            
            //Operator has work in another region
            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                getInProgressError(), lop));
        } else if (assignedRegionWorktypeConflict) {
            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.regionNumber", 
                getOperator().getAssignedRegion().getNumber());
            //Operator was assigned to a specific region
            // but their current task function prevents assignment to that region
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                TaskErrorCode.ASSIGNEDTO_REGION_WORKTYPE_MISSMATCH, lop));   
            
        } else if (getOperator().getRegions().isEmpty() 
                    && getOperator().getAssignedRegion() != null) {

            // Build LabelObjectPairs for the parameters
            LOPArrayList lop = new LOPArrayList();
            lop.add("task.regionNumber", 
                getOperator().getAssignedRegion().getNumber());
            lop.add("task.regionName", getOperator()
                .getAssignedRegion().getName());
            //Operator was assigned to a specific region
            // Create a TaskMessageInfo object and pass
            // it to the setErrorFields method.
            record.setErrorFields(createMessageInfo(
                TaskErrorCode.ASSIGNED_TO_OTHER_REGION_AT_SIGNON, lop));   
        }
        return record;
    }
    
    /**
     * Check for in progress assignments in a region.
     * @throws DataAccessException - Database exception
     * @return boolean value - if there is in progress work
     * @throws TaskCommandException - Task Command Exception
     */
    protected boolean checkForInProgressWork() 
    throws DataAccessException, TaskCommandException {
        boolean inProgressExists = false;
        boolean otherWorkType = false;
        Region assignedTo = null;
        
        //set in progress error to null
        setInProgressError(null);

        //Get a list of active assignments for the operator making the request
        List<Assignment>    queriedAssignments
            = getAssignmentManager().listActiveAssignments(getOperator());

        //Check for region to assign operator to for active assignment
        for (Assignment a : queriedAssignments) {
            //Only check for suspended ot in-progress
            if (a.getStatus().isInSet(AssignmentStatus.Suspended, 
                                AssignmentStatus.InProgress)) {
                
                inProgressExists = true;
                //If assignment is for a type signed in for
                if (getSelectedWorkTypeEnum().equals(TaskFunctionType.NormalAndChaseAssignments)
                    || (getSelectedWorkTypeEnum().equals(TaskFunctionType.NormalAssignments)
                        && a.getType().equals(AssignmentType.Normal))
                    || (getSelectedWorkTypeEnum().equals(TaskFunctionType.ChaseAssignments) 
                        && a.getType().equals(AssignmentType.Chase))) {
                        
                    if (getReturnRegion().contains(a.getRegion())) {
                        assignedTo = a.getRegion();
                        setInProgressError(TaskErrorCode.IN_PROGRESS_WORK_IN_REGION);
                        break;
                    }

                //Else if assignment for different work type
                } else if ((getSelectedWorkTypeEnum().equals(TaskFunctionType.NormalAssignments) 
                            && !a.getType().equals(AssignmentType.Normal))
                          || (getSelectedWorkTypeEnum().equals(TaskFunctionType.ChaseAssignments) 
                              && !a.getType().equals(AssignmentType.Chase))) {
                    if (getReturnRegion().contains(a.getRegion())) {
                        otherWorkType = true;
                    }
                }
            } 
        } //End loop
          
        //a suspended or inprogress was found.
        if (inProgressExists) {
            //If we got an assigned to region then 
            //force operator to that region
            if (assignedTo != null) {
                getReturnRegion().clear();
                getReturnRegion().add(assignedTo);
                
            //else if 
            } else if (otherWorkType) {
                if (getSelectedWorkTypeEnum().equals(TaskFunctionType.NormalAssignments)) {
                    throw new TaskCommandException(
                        TaskErrorCode.IN_PROGRESS_WORK_IN_OTHER_FUNCTION,
                        ResourceUtil.getLocalizedEnumName(TaskFunctionType.ChaseAssignments));
                } else {
                    throw new TaskCommandException(
                        TaskErrorCode.IN_PROGRESS_WORK_IN_OTHER_FUNCTION, 
                        ResourceUtil.getLocalizedEnumName(TaskFunctionType.NormalAssignments));
                }
            } else {
                for (Assignment a : queriedAssignments) {
                    //Only check for suspended ot in-progress
                    if (a.getStatus().isInSet(AssignmentStatus.Suspended, 
                                        AssignmentStatus.InProgress)) {
                        getReturnRegion().clear();
                        getReturnRegion().add(a.getRegion());
                        setInProgressError(TaskErrorCode.IN_PROGRESS_WORK_IN_REGION);
                        break;
                    }
                }
            }
        }
        
        return inProgressExists;
    }

    /**
     * Get List of region authorized to work in.
     * 
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task exception
     **/
    protected void getAuthorizedRegions() throws DataAccessException, TaskCommandException {
        
        //Get list of authorized regions
        setReturnRegion(getRegionManager().listAuthorized(
            getSelectedWorkTypeEnum(), 
            getOperator().getWorkgroup().getId())); 
        
        if (getReturnRegion().isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NOT_AUTHORIZED_ANY_REGIONS, getSelectedWorkType());
        }
        
    }

    /**
     * Get region operator was assigned to, will reduce the return set to just that region.
     * 
     * @throws DataAccessException - Database Exceptions
     * @throws TaskCommandException - Task Command Exceptions
     */
    protected void getAssignedToRegion() throws DataAccessException, TaskCommandException {
        
        // Get region operator is assigned to
        if (getOperator().getAssignedRegion() != null) {
            // If assigned to region is in list of authorized regions 
            // reduce list of authorized regions to assigned to region
            //If autorized for assigned to region
            if (getReturnRegion().contains(getOperator().getAssignedRegion())) {
                getReturnRegion().clear();
                getReturnRegion().add(getOperator().getAssignedRegion());
            } else {
                // Turn on flag so a message can be sent to the device
                // informing user that they can't be assigned to the 
                // assignedTo region because of the task function they are 
                // currently performing
                assignedRegionWorktypeConflict = true;
                
            }
        }
    }
    
    /**
     * @throws DataAccessException - any database exception
     */
    protected void resetOperatorsRegions() throws DataAccessException {
        getOperator().getRegions().clear();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 