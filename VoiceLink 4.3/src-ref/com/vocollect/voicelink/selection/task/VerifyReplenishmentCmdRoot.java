/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class VerifyReplenishmentCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private Long                locationPk;
    
    private String              itemNumber;
    
    private LocationManager     locationManager;
    
    private ItemManager         itemManager;
    
    
    /**
     * Getter for the itemNumber property.
     * @return String value of the property
     */
    public String getItemNumber() {
        return itemNumber;
    }

    
    /**
     * Setter for the itemNumber property.
     * @param itemNumber the new itemNumber value
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * Getter for the locationPk property.
     * @return Long value of the property
     */
    public Long getLocationPk() {
        return locationPk;
    }

    /**
     * Setter for the locationPk property.
     * @param locationPk the new locationPk value
     */
    public void setLocationPk(Long locationPk) {
        this.locationPk = locationPk;
    }
    
    
    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }
    
    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildResponse();
        
        return getResponse();
    }
    
    /**
     * Builds the response.
     *
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }
    
    /**
     * Builds the response record.
     * 
     * @throws DataAccessException - database exception
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        //Calculate replenish flag to send to task
        Location l = getLocationManager().get(getLocationPk());
        Item i = getItemManager().findItemByNumber(getItemNumber().toString());
        if (l.getStatus(i).equals(LocationStatus.Empty)
            || l.getStatus(i).equals(LocationStatus.Pending)
            || l.getStatus(i).equals(LocationStatus.InProgress)
            || l.getStatus(i).equals(LocationStatus.InvalidLot)) {
            record.put("replenished", "0");
        } else {
            record.put("replenished", "1");
        }
        return record;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 