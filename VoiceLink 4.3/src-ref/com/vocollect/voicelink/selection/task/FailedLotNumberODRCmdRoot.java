/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.Date;

/**
 * 
 *
 * @author mnichols
 */
public abstract class FailedLotNumberODRCmdRoot extends BaseSelectionTaskCommand {

    private static final long serialVersionUID = 5621009944383791722L;

    private Long      assignmentID;
    
    private Long      groupID;
       
    private String    lotNumber;
    
    private Long      pickID;

    private VoicelinkNotificationUtil   voicelinkNotificationUtil;
    
    /**
     * Getter for the voicelinkNotificationUtil property.
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }


    /**
     * Setter for the voicelinkNotificationUtil property.
     * @param voicelinkNotificationUtil the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }    
    
    /**
     * Getter for the assignmentID property.
     * @return Long value of the property
     */
    public Long getAssignmentID() {
        return assignmentID;
    }
   
    /**
     * Setter for the assignmentID property.
     * @param assignmentID the new assignmentID value
     */
    public void setAssignmentID(Long assignmentID) {
        this.assignmentID = assignmentID;
    }
  
    /**
     * Getter for the groupID property.
     * @return Long value of the property
     */
    public Long getGroupID() {
        return groupID;
    }

    /**
     * Setter for the groupID property.
     * @param groupID the new groupID value
     */
    public void setGroupID(Long groupID) {
        this.groupID = groupID;
    }
   
    /**
     * Getter for the lotNumber property.
     * @return String value of the property
     */
    public String getLotNumber() {
        return lotNumber;
    }
 
    /**
     * Setter for the lotNumber property.
     * @param lotNumber the new lotNumber value
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
 
    /**
     * Getter for the pickID property.
     * @return Long value of the property
     */
    public Long getPickID() {
        return pickID;
    }
 
    /**
     * Setter for the pickID property.
     * @param pickID the new pickID value
     */
    public void setPickID(Long pickID) {
        this.pickID = pickID;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommandRoot#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        setLocationStatus();
        return getResponse();
    }
    
    /**
     * Sets the status of the location and items for a given pick to InvalidLot.
     * @throws DataAccessException error getting pick
     */
    protected void setLocationStatus() throws DataAccessException {
        Location location = null;
        Item item = null;
        Pick pick = getPickManager().getPrimaryDAO().get(getPickID());
        location = pick.getLocation();
        item = pick.getItem();          
        location.setStatus(LocationStatus.InvalidLot, item);
        logNotification(location, item);
    }

    /**
     * Utility to log an error.
     * @param location being replenished
     * @param item being replenished
     */
    private void logNotification(Location location, Item item) {

        LOPArrayList lop = new LOPArrayList();

        try {
            if (this.getOperator() == null) {
                lop.add("task.operator", new String("null"));
            } else {
                lop.add("task.operator", getOperator().getOperatorIdentifier());
            }
            if (getLotNumber() == null) {
                lop.add("task.lotNumber", new String("null"));
            } else {
                lop.add("task.lotNumber", getLotNumber());
            }
            if (location == null) {
                lop.add("task.locationScannedValue", new String("null"));   
            } else {
                lop.add("task.locationScannedValue", 
                    location.getScannedVerification());
            }
            if (item == null) {
                lop.add("task.itemNumber", new String("null"));
            } else {
                lop.add("task.itemNumber", item.getNumber().toString());
            }

            getVoicelinkNotificationUtil().createNotification(
                "notification.column.keyname.Process.Task", 
                "voicelink.notification.task.lot.invalid",
                NotificationPriority.CRITICAL,
                new Date(),
                CoreErrorCode.INVALID_LOT.toString(),
                lop); 
        } catch (Exception ex) {
            // Do nothing -- we don't care about 
            // failures to post the notificaiton
        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 