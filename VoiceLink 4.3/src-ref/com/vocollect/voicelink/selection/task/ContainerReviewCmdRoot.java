/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class ContainerReviewCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private Long        assignmentPk; 
    
    private Long        pickContainerPk;
    
    private Container   reviewContainer;
    
    
    /**
     * Getter for the assignmentPk property.
     * @return Long value of the property
     */
    public Long getAssignmentPk() {
        return assignmentPk;
    }

    /**
     * Setter for the assignmentPk property.
     * @param assignmentPk the new assignmentPk value
     */
    public void setAssignmentPk(Long assignmentPk) {
        this.assignmentPk = assignmentPk;
    }
    
    /**
     * Getter for the pickContainerPk property.
     * @return Long value of the property
     */
    public Long getPickContainerPk() {
        return pickContainerPk;
    }

    /**
     * Setter for the pickContainerPk property.
     * @param pickContainerPk the new pickContainerPk value
     */
    public void setPickContainerPk(Long pickContainerPk) {
        this.pickContainerPk = pickContainerPk;
    }
    
    /**
     * Getter for the reviewContainer property.
     * @return Container value of the property
     */
    public Container getReviewContainer() {
        return reviewContainer;
    }

    /**
     * Setter for the reviewContainer property.
     * @param reviewContainer the new reviewContainer value
     */
    public void setReviewContainer(Container reviewContainer) {
        this.reviewContainer = reviewContainer;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        
        verifyContainer();
        buildResponse();
        
        return getResponse();
    }
    
    /**
     * Verifies that the container passed in is valid and not empty.
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - task exception
     */
    protected void verifyContainer() throws DataAccessException, TaskCommandException {
        // Get container based on container pk
        boolean containerFound = false;
        List<Container> allContainers = getContainerManager().listAllContainers();
        for (Container checkContainer : allContainers) {
            if (checkContainer.getId().equals(getPickContainerPk())) {
                containerFound = true;
                // If container is empty send an error
                if (checkContainer.getPickDetails().isEmpty()) {
                    throw new TaskCommandException(TaskErrorCode.CONTAINER_EMPTY);
                } else {
                    setReviewContainer(checkContainer);
                }
                break;
            }
        }
        // If container was not found send an error
        if (!containerFound) {
            throw new TaskCommandException(TaskErrorCode.CONTAINER_NOT_FOUND, getAssignmentPk());
        }
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        // Get the containers pick details
        Container c = getContainerManager().get(getPickContainerPk());
        List<PickDetail> containerPicks = c.getPickDetails();
        // Get a list of assignments based on the group number
        List <Assignment> assignments = getAssignments();
        
        for (PickDetail p : containerPicks) {
            getResponse().addRecord(buildResponseRecord(c, assignments.get(0), p));
        }
    }
    
    /**
     * Builds the response record.
     * 
     * @param c - the container to use for the response
     * @param a - the assignment to use for the response
     * @param p - the pick detail to use for the response
     * @return record - the response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(Container c, 
                                                 Assignment a, 
                                                 PickDetail p) 
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();
        record.put("spokenContainerValidation", 
            c.getContainerNumber().substring(c.getContainerNumber().length() 
                - a.getRegion().getUserSettings(a.getType())
                .getNumberOfContainerDigitsSpeak()));
        record.put("itemDescription", 
            getTranslatedItemPhoneticDescription(p.getPick().getItem()));
        record.put("quantity", p.getQuantityPicked());
        record.put("location", p.getPick().getLocation().getScannedVerification());
        record.put("pickTime", p.getPick().getPickTime());
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 