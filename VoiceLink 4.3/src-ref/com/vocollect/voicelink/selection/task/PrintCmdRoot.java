/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.AbstractPrintServer;
import com.vocollect.voicelink.printserver.ChaseLabel;
import com.vocollect.voicelink.printserver.ContainerLabel;
import com.vocollect.voicelink.printserver.LabelData;
import com.vocollect.voicelink.printserver.PalletLabel;
import com.vocollect.voicelink.printserver.PrintJob;
import com.vocollect.voicelink.printserver.PrintServerFactory;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * Print Labels Command processing. 
 *
 * @author estoll
 */
public abstract class PrintCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private static final String ALL_CONTAINERS = "all";
    private static final int PRINTED = 1;
    private static final int NOT_PRINTED = 0;
    
    private Long    assignmentPk;
    
    private String    pickContainerPk; 
    
    private Long    printerNumber;
    
    private int     operation; 
    
    private int     reprintLabels;
    
    private boolean printAllContainers;
    
    private PrinterManager    printerManager;
    
    private Printer printer;

    private List<LabelData> labels = new ArrayList<LabelData>();

    private List <Container> containerList = new ArrayList<Container>();

    /**
     * Getter for the printAllContainers property.
     * @return boolean value of the property
     */
    public boolean isPrintAllContainers() {
        return this.printAllContainers;
    }

    
    /**
     * Setter for the printAllContainers property.
     * @param printAllContainers the new printAllContainers value
     */
    public void setPrintAllContainers(boolean printAllContainers) {
        this.printAllContainers = printAllContainers;
    }

    /**
     * Getter for the assignmentPk property.
     * @return Long value of the property
     */
    public Long getAssignmentPk() {
        return assignmentPk;
    }

    /**
     * Setter for the assignmentPk property.
     * @param assignmentPk the new assignmentPk value
     */
    public void setAssignmentPk(Long assignmentPk) {
        this.assignmentPk = assignmentPk;
    }
    
    /**
     * Getter for the printerNumber property.
     * @return Long value of the property
     */
    public Long getPrinterNumber() {
        return printerNumber;
    }

    /**
     * Setter for the printerNumber property.
     * @param printerNumber the new printerNumber value
     */
    public void setPrinterNumber(Long printerNumber) {
        this.printerNumber = printerNumber;
    }
    
    /**
     * Getter of container PK as Long.
     * @return - Long representation of container PK parameter
     */
    public Long getPickContainerPkLong() {
        if (StringUtil.isNullOrEmpty(this.pickContainerPk)) {
            return null;
        } else {
            return Long.valueOf(this.pickContainerPk);
        }
    }

    /**
     * Getter for the pickContainerPk property.
     * @return String value of the property
     */
    public String getPickContainerPk() {
        return pickContainerPk;
    }

    /**
     * Setter for the pickContainerPk property.
     * @param pickContainerPk the new pickContainerPk value
     */
    public void setPickContainerPk(String pickContainerPk) {
        if (pickContainerPk.equals(ALL_CONTAINERS)) {
            this.setPrintAllContainers(true);
            this.pickContainerPk = "";
        } else {
            this.pickContainerPk = pickContainerPk;
            this.setPrintAllContainers(false);
        }
    }
    
    /**
     * Getter for the operation property.
     * @return int value of the property
     */
    public int getOperation() {
        return operation;
    }

    /**
     * Setter for the operation property.
     * @param operation the new operation value
     */
    public void setOperation(int operation) {
        this.operation = operation;
    }
    
    /**
     * Getter for the reprintLabels property.
     * @return int value of the property
     */
    public int getReprintLabels() {
        return reprintLabels;
    }

    /**
     * Setter for the reprintLabels property.
     * @param reprintLabels the new reprintLabels value
     */
    public void setReprintLabels(int reprintLabels) {
        this.reprintLabels = reprintLabels;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception, TaskCommandException {
        //Check for valid printer number
        validatePrinter();
        
        printLabels();
        buildResponse();
        return getResponse();
    }
    
    /**
     * Validate the printer number is in the VoiceLink database
     * and it is enabled.
     * @throws DataAccessException - on database exceptions
     * @throws TaskCommandException - when printer number is not found.
     */
    protected void validatePrinter() 
    throws DataAccessException, TaskCommandException {
        printer = printerManager.findByNumber(printerNumber.intValue());
        if (printer == null) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_NOT_FOUND, 
                printerNumber);
        }
        
        if (!this.printer.getIsEnabled()) {
            throw new TaskCommandException(TaskErrorCode.PRINTER_IS_DISABLED, 
                printerNumber);
            
        }
    }
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        getResponse().addRecord(buildResponseRecord());
    }
    
    /**
     * Builds the response record.
     * 
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        return record;
    }

    /**
     * Getter for the printerManager property.
     * @return PrinterManager value of the property
     */
    public PrinterManager getPrinterManager() {
        return this.printerManager;
    }


    
    /**
     * Setter for the printerManager property.
     * @param printerManager the new printerManager value
     * @throws VocollectException on print server exceptions
     */
    public void setPrinterManager(PrinterManager printerManager) 
    throws VocollectException {
        this.printerManager = printerManager;
    }
    
    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    protected void printLabels() throws DataAccessException, VocollectException {

        // Get the first assignment of the group and check for chase, pallet and 
        // container labels
        Assignment assignment = this.getAssignments().get(0);
        
        // Check for and print chase assignment labels.
        if (assignment.getType() == AssignmentType.Chase) {
            printChaseLabels(assignment);
            sendLabelsToPrinter();
        } else {
            // Check for and print Pallet labels.
            if (assignment.getRegion().getProfileNormalAssignment()
                        .getContainerType() == null) {
                printPalletLabels();
                sendLabelsToPrinter();
            } else {
                // Create a list of all of the containers and print them all.
                for (Assignment a : this.getAssignments()) {
                    createContainerList(a);
                }
                createContainerLabels();
                sendLabelsToPrinter();
                updateContainers();
            }
        }
    } 
    
    /**
     * @param assignment to be printed
     * @throws VocollectException on print server exceptions
     */
    protected void printChaseLabels(Assignment assignment) 
        throws VocollectException {
        List<ChaseLabel> chaseLabels = this.getPrinterManager().
                                        getAssignmentChaseLabels(assignment);

        // for each chase label, add it to the labeldata that
        // gets sent to the printJob
        for (ChaseLabel chase : chaseLabels) {
            this.labels.add(chase.getLabelData());
        }

        return;
        
    }

    /**
     * Prints pallet labels.
     * @throws VocollectException on any print server exceptions
     */
    protected void printPalletLabels() throws VocollectException {
        Short copies = 1;
        PalletLabel pallet = null;
        
        for (Assignment assignment : this.getAssignments()) {
            pallet = new PalletLabel(assignment, copies);
            labels.add(pallet.getLabelData());
        }

    }
    
    /**
     * Build a list of containers that need to be printed.
     * @param assignment - an assignment
     * @throws VocollectException on print server exceptions
     */
    protected void createContainerList(Assignment assignment) 
    throws VocollectException {
        
        // First check to see if we have a container id
        if (this.getPickContainerPkLong() != null) {
            Container container = null;
            // Add this container to the list of containers to
            // to be processed at the end of printing
            container = this.getContainerManager().
                        get(this.getPickContainerPkLong());
            if (container == null) {
                throw new TaskCommandException(
                            TaskErrorCode.PRINT_UNKNOWN_CONTAINER, 
                            this.getPickContainerPk());
            }
            this.containerList.add(container);
            
        } else {
            for (Container ac : assignment.getContainers()) {
                if (this.getReprintLabels() == PRINTED) {
                    if (ac.getPrinted() == PRINTED) {
                        this.containerList.add(ac);
                    }
                } else {
                    if (ac.getPrinted() == NOT_PRINTED) {
                        this.containerList.add(ac);
                    }
                } 
            } 
        } 
    }

    /**
     * @param container to build a label upon
     * @return ContainerLabel
     */
    protected ContainerLabel createContainerLabel(Container container) {
        Short copies = 1;
        return new ContainerLabel(container, copies);
    }

    /**
     * Loop through the list of containers and create a container label.
     */
    protected void createContainerLabels() {
        for (Container c : this.containerList) {
            this.labels.add(createContainerLabel(c).getLabelData());
        }
    }
    
    /**
     * Update the container records to be printed.
     * @throws DataAccessException on data access exceptions
     */
    public void updateContainers() throws DataAccessException {
        for (Container c : this.containerList) {
            c.setPrinted(1);
        }
    }
    
    /**
     * Send the member variable labels to the AbstractPrintServer.
     * @throws TaskCommandException when the print server throws errors.
     */
    protected void sendLabelsToPrinter() throws TaskCommandException {
        // If we have labels - print them
        PrintJob job = null;
        
        try {
            job = new PrintJob(this.printer.getName(), 
                                this.labels);
        } catch (VocollectException e) {
            throw new TaskCommandException(
                TaskErrorCode.PRINTSERVER_EXCEPTION,
                e.getErrorCode());
        }
        
        try {
            AbstractPrintServer printServer = 
                PrintServerFactory.getPrintServer();
            if (this.labels.size() > 0) {
                printServer.submitJob(job);
            }
        } catch (VocollectException e) {
            throw new TaskCommandException(
                  TaskErrorCode.PRINTSERVER_EXCEPTION,
                  e.getErrorCode());
        }
        return;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 