/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import java.util.List;


/**
 * 
 *
 * @author sfahnestock
 */
public abstract class UpdateStatusCmdRoot extends BaseSelectionTaskCommand {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private String          slotAisle;
    
    private String          setStatusTo;
    
    private String          locationID;
    
    private LocationManager locationManager;
    
    private Location        pickLocation;
        
    /**
     * Getter for the pick location property.
     * @return pickLocation; value of the property
     */
    public Location getPickLocation() {
        return pickLocation;
    }
    
    /**
     * Setter for the pick location property.
     * @param pickLocation the new pickLocation value
     */
    public void setPickLocation(Location pickLocation) {
        this.pickLocation = pickLocation;
    }
    
    /**
     * Getter for the location ID property.
     * @return long value of the property
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * Setter for the location ID property.
     * @param locationID the new locationID value
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
       
    /**
     * Getter for the slot aisle indicator property.
     * @return String value of the property
     */
    public String getSlotAisle() {
        return slotAisle;
    }

    /**
     * Setter for the slot aisle indicator property.
     * @param slotAisle the new slotAisle value
     */
    public void setSlotAisle(String slotAisle) {
        this.slotAisle = slotAisle;
    }
       
    /**
     * Getter for the set status to property.
     * @return String value of the property
     */
    public String getSetStatusTo() {
        return setStatusTo;
    }

    /**
     * Setter for the set status to property.
     * @param setStatusTo the new setStatusTo value
     */
    public void setSetStatusTo(String setStatusTo) {
        this.setStatusTo = setStatusTo;
    }
    
    /**
     * Getter for the locationManager property.
     * @return locationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }
  
    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }
   
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //validates the status change
        validate();
        
        //Get Location object for the location ID
        getRecievedLocationObject();
        
        //Updates the statuses of the picks
        update();
        
        return getResponse();
    }

    /**
     * Validates that status can be updated.
     * 
     * @throws Exception in the general case
     */
    protected void validate() throws Exception {
        if (!getSetStatusTo().equals("S") && !getSetStatusTo().equals("N")) {
            throw new TaskCommandException(TaskErrorCode.INVALID_STATUS);
        }
        if (Integer.parseInt(getSlotAisle()) != 0 
            && Integer.parseInt(getSlotAisle()) != 1 
            && Integer.parseInt(getSlotAisle()) != 2) {
            throw new TaskCommandException(TaskErrorCode.INVALID_INDICATOR);
        }
        if (Integer.parseInt(getSlotAisle()) == 2 
            && getSetStatusTo().equals("S")) {
            throw new TaskCommandException(TaskErrorCode.CANNOT_SKIP_ENTIRE_ASSIGNMENT);
        }
    }
    
    /**
     * Updates the status.
     * @throws BusinessRuleException - Throws business rule exception
     * @throws DataAccessException - Database Exception
     */
    protected void update() throws BusinessRuleException, DataAccessException {
        //Get List of assignment with the operator ID in the reserved fields
        List<Pick> picks = getPickManager().listGroupsPicks(getGroupNumber());

        //for each pick in the group update the status accordingly
        for (Pick p : picks) {
            if (Integer.parseInt(getSlotAisle()) == 0 & getSetStatusTo().equals("S")) {
                /* Should base item picks also be marked as skipped? */
                if (p.getStatus() == PickStatus.NotPicked 
                    && p.getLocation().equals(getPickLocation())) {
                        p.reportPickSkipped();
                        p.setBaseItemOverride(false);
                }
            }
            
            if (Integer.parseInt(getSlotAisle()) == 0 & getSetStatusTo().equals("N")) {
                if (p.getStatus().isInSet(PickStatus.BaseItem, PickStatus.Skipped)
                    && p.getLocation().equals(getPickLocation())) {
                        p.setPickAsNotPicked();
                        p.setBaseItemOverride(false);
                }
            }
            
            if (Integer.parseInt(getSlotAisle()) == 1 & getSetStatusTo().equals("S")) {
                /* Should base item picks also be marked as skipped? */
                if (p.getStatus() == PickStatus.NotPicked
                    && stringEquals(p.getLocation().getDescription().getPreAisle(),
                        pickLocation.getDescription().getPreAisle())
                    && stringEquals(p.getLocation().getDescription().getAisle(), 
                        pickLocation.getDescription().getAisle())) {
                    p.reportPickSkipped();
                    p.setBaseItemOverride(false);
                }
            }
            
            if (Integer.parseInt(getSlotAisle()) == 1
                & getSetStatusTo().equals("N")) {
                if (p.getStatus().isInSet(PickStatus.BaseItem, PickStatus.Skipped)
                    && stringEquals(p.getLocation().getDescription().getPreAisle(),
                        pickLocation.getDescription().getPreAisle())
                    && stringEquals(p.getLocation().getDescription().getAisle(), 
                        pickLocation.getDescription().getAisle())) {
                    p.setPickAsNotPicked();
                    p.setBaseItemOverride(false);
                }
            }
            
            if (Integer.parseInt(getSlotAisle()) == 2
                & getSetStatusTo().equals("N")) {
                /* Should base item picks also be marked as skipped? */
                if (p.getStatus().isInSet(PickStatus.BaseItem, PickStatus.Skipped)) {
                    p.setPickAsNotPicked();
                    p.setBaseItemOverride(false);
                }
            }
        }
    }
    
    /**
     * Compare 2 strings and if both null then they are considered equal.
     * @param value1 - first value to compare
     * @param value2 - second value to compare
     * @return equal or not
     */
    protected boolean stringEquals(String value1, String value2) {
        if (value1 != null) {
            return value1.equals(value2);
        } else {
            return (value2 == null);
        }
    }
    
    /**
     * gets a Location object for the location ID.
     * 
     * @throws DataAccessException - Database exceptions
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void getRecievedLocationObject()
        throws DataAccessException, BusinessRuleException {
        //Get the location
        if (!StringUtil.isNullOrEmpty(getLocationID())) {
            setPickLocation(getLocationManager().get(Long.valueOf(getLocationID())));            
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 