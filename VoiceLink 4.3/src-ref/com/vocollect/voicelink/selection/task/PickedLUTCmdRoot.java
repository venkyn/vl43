/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 * 
 *
 * @author pfunyak
 */
public abstract class PickedLUTCmdRoot extends PickedODRCmd {

    //
    private static final long serialVersionUID = -2150443467350380312L;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        // this class extends the Picked ODR class
        // Just call the ODR's do execute"
       super.doExecute();
       // Build the response 
       // Note that there is nothing to return in the response record
       // but we still have to return one for error handling purposes.
       buildResponse();
       
       return getResponse();
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
            getResponse().addRecord(buildResponseRecord());
    }
    
    /**
     * Builds the response record.
     * 
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 