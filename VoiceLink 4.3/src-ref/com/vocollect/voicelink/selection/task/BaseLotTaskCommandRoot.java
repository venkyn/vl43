/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.task.command.BaseSelectionTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Base task command object for lot commands. Contains commonly used
 * methods for lot validation.
 *
 * @author mnichols
 */
public abstract class BaseLotTaskCommandRoot extends BaseSelectionTaskCommand {

    private static final long serialVersionUID = -3402817908935673687L;
    
    private List<Lot> lots = new ArrayList<Lot>();
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    protected Response doExecute() throws Exception {
        return null;
    }

    
    /**
     * Getter for the lots property.
     * @return List &lt;Lot&gt; value of the property
     */
    public List<Lot> getLots() {
        return lots;
    }

    
    /**
     * Setter for the lots property.
     * @param lots the new lots value
     */
    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }  
    
    /**
     * Validates that a lot exists based on the speakableLotNumber and generates a list of valid lots.
     * 
     * @param speakableLotNumber the speakable lot number
     * @throws TaskCommandException - task exception
     */
    protected void validateLotExists(String speakableLotNumber) throws TaskCommandException {
        List<Lot> validLots = new ArrayList<Lot>();
        for (Lot lot : getLots()) {
            if (lot.getSpeakableNumber().equals(speakableLotNumber)) {
                validLots.add(lot);
            }
        }
        if (validLots.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.LOT_DOES_NOT_EXIST, speakableLotNumber);
        }
        setLots(validLots);
    }

    /**
     * Validates that a list of lots is not expired.
     * @param speakableLotNumber the lot number given by the operator
     * @param itemNumber the item number given by the operator 
     * @throws TaskCommandException - task exception
     */    
    protected void validateLotExpirations(String speakableLotNumber, String itemNumber) 
    throws TaskCommandException {
        
        List<Lot> unexpiredLots = new ArrayList<Lot>();
        for (Lot lot : getLots()) {
            if (lot.getExpirationDate() == null 
                || (lot.getExpirationDate().equals(new Date()) 
                    || lot.getExpirationDate().after(new Date()))) {
                unexpiredLots.add(lot);
            }
        }
        if (unexpiredLots.isEmpty()) {
            if (speakableLotNumber != null) {
                throw new TaskCommandException(TaskErrorCode.LOT_NUMBER_EXPIRED, speakableLotNumber);
            } else {
                throw new TaskCommandException(TaskErrorCode.LOT_ITEM_NUMBER_EXPIRED, itemNumber);
            }
        }
        setLots(unexpiredLots);
    }
    
    /**
     * Validates that a list of lot locations match pick locations.
     * 
     * @param pickID the pick ID to check
     * @param speakableLotNumber the lot number given by the operator
     * @throws TaskCommandException - task exception
     * @throws DataAccessException - a DAO exception
     */        
    protected void validateLotLocations(Long pickID, String speakableLotNumber) 
    throws TaskCommandException, DataAccessException {
        //Use a hashmap so entries will be unique by lotNumber
        HashMap<String, Lot> locationLots = new HashMap<String, Lot>();
        Pick pick = getPickManager().getPrimaryDAO().get(pickID);
        for (Lot lot : getLots()) {
            if (lot.getLocation() == null || lot.getLocation().equals(pick.getLocation())) {
                locationLots.put(lot.getNumber(), lot);
            }
        }
        if (locationLots.isEmpty()) {
            if (speakableLotNumber != null) {
                throw new TaskCommandException(TaskErrorCode.LOT_LOCATION_INVALID, 
                    speakableLotNumber, pick.getLocation().getScannedVerification());
            } else {
                throw new TaskCommandException(TaskErrorCode.LOT_ITEM_LOCATION_INVALID, 
                    pick.getItem().getNumber(), pick.getLocation().getScannedVerification());
            }            
        }
        setLots(new ArrayList<Lot>(locationLots.values()));
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 