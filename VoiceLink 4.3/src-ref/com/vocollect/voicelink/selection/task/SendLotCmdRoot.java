/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;


/**
 * 
 *
 * @author mnichols
 */
public abstract class SendLotCmdRoot extends BaseLotTaskCommandRoot {
    
    private static final long serialVersionUID = -5593082031079942973L;
    
    private Long    assignmentPk, pickID;
    
    private String  speakableLotNumber;
    
    private int     lotQuantity;
    
    /**
     * Getter for the lotNumber property.
     * @return String value of the property
     */
    public String getSpeakableLotNumber() {
        return speakableLotNumber;
    }

    /**
     * Setter for the lotNumber property.
     * @param lotNumber the new lotNumber value
     */
    public void setSpeakableLotNumber(String lotNumber) {
        this.speakableLotNumber = lotNumber;
    }
    
    /**
     * Getter for the lotQuantity property.
     * @return int value of the property
     */
    public int getLotQuantity() {
        return lotQuantity;
    }

    /**
     * Setter for the lotQuantity property.
     * @param lotQuantity the new lotQuantity value
     */
    public void setLotQuantity(int lotQuantity) {
        this.lotQuantity = lotQuantity;
    }

    /**
     * Getter for the assignmentPk property.
     * @return Long value of the property
     */
    public Long getAssignmentPk() {
        return assignmentPk;
    }

    /**
     * Setter for the assignmentPk property.
     * @param assignmentPk the new assignmentPk value
     */
    public void setAssignmentPk(Long assignmentPk) {
        this.assignmentPk = assignmentPk;
    }
    
    /**
     * Getter for the sequenceNumber property.
     * @return Long value of the property
     */
    public Long getPickID() {
        return pickID;
    }

    /**
     * Setter for the sequenceNumber property.
     * @param pickID the new sequenceNumber value
     */
    public void setPickID(Long pickID) {
        this.pickID = pickID;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        //Get a list of the valid lots
        getValidLots();
        //Build the response     
        buildResponse();
        return getResponse();
    }
    
    /**
     * Builds a list of valid lots.
     * @throws DataAccessException - database exception
     * @throws TaskCommandException - task command exception 
     */ 
    protected void getValidLots() throws DataAccessException, TaskCommandException {
        setLots(new ArrayList<Lot>(getPickManager().get(getPickID()).getItem().getLots()));
        //Narrow down to match speakable
        validateLotExists(getSpeakableLotNumber());
        validateLotExpirations(getSpeakableLotNumber(), null);
        validateLotLocations(getPickID(), getSpeakableLotNumber());
    }
    
    /**
     * Builds the response.
     * @throws DataAccessException - database exception
     */
    protected void buildResponse() throws DataAccessException {
        for (Lot l : getLots()) {
            getResponse().addRecord(buildResponseRecord(l));
        }
    }
    
    /**
     * Builds the response record.
     * @param lot the lot to transmit
     * @return record - the response record
     */
    protected ResponseRecord buildResponseRecord(Lot lot) {
        ResponseRecord record = new TaskResponseRecord();
        record.put("lotNumber", lot.getNumber());
        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 