/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.selection.event.listeners;

import com.vocollect.epp.errors.ErrorCode;

/**
 * @author khazra
 * 
 */
public class ListenerErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND = 13000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND = 13099;

    /** 
     * No error, just the base initialization.
     */
    public static final ListenerErrorCodeRoot NO_ERROR = new ListenerErrorCodeRoot();

    /**
     * Communication error
     */
    public static final ListenerErrorCodeRoot COMM_ERROR = new ListenerErrorCodeRoot(13000);
    
    /**
     * Initialization error
     */
    public static final ListenerErrorCodeRoot INIT_ERROR = new ListenerErrorCodeRoot(13001);
    
    /**
     * Default Constructor
     */
    public ListenerErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * 
     * @param err
     *            The error code
     */
    public ListenerErrorCodeRoot(long err) {
        super(ListenerErrorCodeRoot.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 