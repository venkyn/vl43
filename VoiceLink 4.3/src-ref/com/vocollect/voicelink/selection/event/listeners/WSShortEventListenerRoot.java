/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.event.listeners;


import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.selection.events.ShortEvent;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.context.ApplicationListener;



/**
 * This listener listens to the event initialized when a picks is shorted. When
 * the pick is shorted this listener communicates this event to WMS in real time.
 * 
 * @author yarora
 * 
 */
@SuppressWarnings("unused")
public class WSShortEventListenerRoot implements
        ApplicationListener<ShortEvent> {

    private String wsdlURL;
    private boolean enabled;
    private static final Logger log = new Logger(WSShortEventListenerRoot.class);
    private Long timeOut;

    /**
     * @return timeOut
     */
    public Long getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut
     *          The timeout time
     */
    public void setTimeOut(Long timeOut) {
        this.timeOut = timeOut;
    }

    private VoicelinkNotificationUtil voicelinkNotificationUtil;

    /**
     * Getter for the voicelinkNotificationUtil property.
     * 
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }

    /**
     * Setter for the voicelinkNotificationUtil property.
     * 
     * @param voicelinkNotificationUtil
     *            the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(
            VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }

    /**
     * @return wsdlURL
     */
    public String getWsdlURL() {
        return wsdlURL;
    }

    /**
     * @param wsdlURL
     *            The url for WSDL
     */
    public void setWsdlURL(String wsdlURL) {
        this.wsdlURL = wsdlURL;
    }

    /**
     * @return enabled
     */
    public boolean getEnabled() {
        return enabled;
    }

    /**
     * @param enabled
     *            Will this listener do anything
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public void onApplicationEvent(ShortEvent evt) {
        if (getEnabled()) {
            log.debug("Listener is enabled");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

            String pickTime = sdf.format(evt.getPickTime());
            String origAssignmentNo = evt.getOriginalAssignmentNo().toString();
            String locationID = evt.getLocationID();
            String itemNo = evt.getItemNo();
            String operID = evt.getOperatorID();
            Integer pickQty = evt.getPickedQty();
            Long siteId = evt.getSiteId();

            String pickData = "Pick Time: %1s, "
                    + "Assignment No: %2s, Location ID: %3s, "
                    + "Item No: %4s, Operator ID: %5s, " + "Pick Qty: %6d";

            log.trace(String.format(pickData, pickTime, origAssignmentNo,
                    locationID, itemNo, operID, pickQty));

            try {
                JaxWsDynamicClientFactory dcf = JaxWsDynamicClientFactory
                        .newInstance();

                log.debug("Connecting to " + getWsdlURL());

                Client client = dcf.createClient(getWsdlURL());

                log.debug("Successfully connected to client");

                if (client != null) {
                    
                    // code to implement the time out
                    HTTPConduit conduit = (HTTPConduit) client.getConduit();
                    HTTPClientPolicy policy = new HTTPClientPolicy();
                    policy.setConnectionTimeout(getTimeOut());
                    policy.setReceiveTimeout(getTimeOut());
                    conduit.setClient(policy);

                    Object[] res;

                    res = client.invoke("reportShort", pickTime,
                            origAssignmentNo, locationID, itemNo, operID,
                            pickQty);

                    // Error reporting
                    if (res.length > 0) {
                        Object obj = res[0];
                        if (obj instanceof Integer) {
                            Integer resCode = (Integer) obj;

                            if (resCode != 0) {
                                String serviceResponseError = "The Report Short web service configured at %1s "
                                        + "reported error code %2d when tried to communicate short for item %3s "
                                        + "at location %4s for assignment no. %5s.";

                                log.info(String.format(serviceResponseError,
                                        getWsdlURL(), resCode, itemNo,
                                        locationID, origAssignmentNo));
                            }
                        }
                    }

                    log.debug("Service returned with response : "
                            + Arrays.toString(res));
                } else {
                    log.error("Not able to create WebService Client",
                            ListenerErrorCode.INIT_ERROR);
                }
                

            } catch (Exception e) {
                String serviceInvokeError = "The Report Short web service configured at %1s is offline. "
                        + "Could not report short for item %2s at location %3s for assignment no. %4s";

                log.error(String.format(serviceInvokeError, getWsdlURL(),
                        itemNo, locationID, origAssignmentNo),
                        ListenerErrorCode.COMM_ERROR, e);

            }

        } else {
            log.info("Listener is disabled");
        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 