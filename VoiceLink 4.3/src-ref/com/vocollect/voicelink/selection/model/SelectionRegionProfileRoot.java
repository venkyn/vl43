/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.BaseModelObject;

/**
 * 
 *
 * @author mkoenig
 */
public class SelectionRegionProfileRoot extends BaseModelObject implements Cloneable {

    //
    private static final long serialVersionUID = -7784993860079100142L;

    //Profile Number
    private String            number;
    
    //Type of assignment profile pertains to.
    private AssignmentType    assignmentType = AssignmentType.Normal;
    
    //************************************************************
    //Assignment Issuance
    //************************************************************
    //Automatic issuance
    private boolean           autoIssuance = false;  
    
    //Allow Multiple Assignments
    private boolean           allowMultipleAssignments = false;  
    
    //Allow Passing Work
    private boolean           allowPassingAssignment = false; 
    
    //Allow Merging Assignments
    private boolean           allowMergingAssignments = false;
    
    //************************************************************
    //Pick Prompt Settings
    //************************************************************
    
    //Determines which pick prompt should be used in region for check digits and quantity
    private boolean          multiplePickPrompt = false;
    
    //Determines if an operator must speak in the quantity pick for confirmation
    private boolean          requireQuantityVerification = false;
    
    //************************************************************
    //Container Settings
    //************************************************************
    //Type of container to use in region, defined from tbMainContainers
    //No Container or Variable quantity containers
    private ContainerType     containerType;
    
    //************************************************************
    //Print Settings
    //************************************************************
    //Should chase labels be printed for chase assignments
    private boolean           chasePrintLabelsOn = false;
    
    //Determines if container labels should be printed and when
    private SelectionRegionPrintContainerLabels   printContainerLabelsIndicator = 
                    SelectionRegionPrintContainerLabels.DoNotPrint;
    
    //************************************************************
    //General Settings
    //************************************************************
    //Should delivery prompt be spoken
    private boolean    requireDeliveryPrompt = false; 
    
    //Determines if region does case label check digits
    private boolean          requireCaseLabelCheckDigits = false;

    //Determines if region allows base items or not
    private boolean         allowBaseItems = false;
    
    

    
    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    
    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the allowBaseItems property.
     * @return boolean value of the property
     */
    public boolean isAllowBaseItems() {
        return allowBaseItems;
    }

    
    /**
     * Setter for the allowBaseItems property.
     * @param allowBaseItems the new allowBaseItems value
     */
    public void setAllowBaseItems(boolean allowBaseItems) {
        this.allowBaseItems = allowBaseItems;
    }

    
    /**
     * Getter for the allowMergingAssignments property.
     * @return boolean value of the property
     */
    public boolean isAllowMergingAssignments() {
        return allowMergingAssignments;
    }

    
    /**
     * Setter for the allowMergingAssignments property.
     * @param allowMergingAssignments the new allowMergingAssignments value
     */
    public void setAllowMergingAssignments(boolean allowMergingAssignments) {
        this.allowMergingAssignments = allowMergingAssignments;
    }

    
    /**
     * Getter for the allowMultipleAssignments property.
     * @return boolean value of the property
     */
    public boolean isAllowMultipleAssignments() {
        return allowMultipleAssignments;
    }

    
    /**
     * Setter for the allowMultipleAssignments property.
     * @param allowMultipleAssignments the new allowMultipleAssignments value
     */
    public void setAllowMultipleAssignments(boolean allowMultipleAssignments) {
        this.allowMultipleAssignments = allowMultipleAssignments;
    }

    
    /**
     * Getter for the allowPassingAssignment property.
     * @return boolean value of the property
     */
    public boolean isAllowPassingAssignment() {
        return allowPassingAssignment;
    }

    
    /**
     * Setter for the allowPassingAssignment property.
     * @param allowPassingAssignment the new allowPassingAssignment value
     */
    public void setAllowPassingAssignment(boolean allowPassingAssignment) {
        this.allowPassingAssignment = allowPassingAssignment;
    }

    
    /**
     * Getter for the assignmentType property.
     * @return AssignmentType value of the property
     */
    public AssignmentType getAssignmentType() {
        return assignmentType;
    }

    
    /**
     * Setter for the assignmentType property.
     * @param assignmentType the new assignmentType value
     */
    public void setAssignmentType(AssignmentType assignmentType) {
        this.assignmentType = assignmentType;
    }

    
    /**
     * Getter for the automaticAssignmentIssuance property.
     * @return boolean value of the property
     */
    public boolean isAutoIssuance() {
        return autoIssuance;
    }

    
    /**
     * Setter for the automaticAssignmentIssuance property.
     * @param autoIssuance the new automaticAssignmentIssuance value
     */
    public void setAutoIssuance(boolean autoIssuance) {
        this.autoIssuance = autoIssuance;
    }

    
    /**
     * Getter for the chasePrintLabelsOn property.
     * @return boolean value of the property
     */
    public boolean isChasePrintLabelsOn() {
        return chasePrintLabelsOn;
    }

    
    /**
     * Setter for the chasePrintLabelsOn property.
     * @param chasePrintLabelsOn the new chasePrintLabelsOn value
     */
    public void setChasePrintLabelsOn(boolean chasePrintLabelsOn) {
        this.chasePrintLabelsOn = chasePrintLabelsOn;
    }

    
    /**
     * Getter for the containerType property.
     * @return ContainerType value of the property
     */
    public ContainerType getContainerType() {
        return containerType;
    }

    
    /**
     * Setter for the containerType property.
     * @param containerType the new containerType value
     */
    public void setContainerType(ContainerType containerType) {
        this.containerType = containerType;
    }

    
    /**
     * Getter for the multiplePickPrompt property.
     * @return boolean value of the property
     */
    public boolean isMultiplePickPrompt() {
        return multiplePickPrompt;
    }

    
    /**
     * Setter for the multiplePickPrompt property.
     * @param multiplePickPrompt the new multiplePickPrompt value
     */
    public void setMultiplePickPrompt(boolean multiplePickPrompt) {
        this.multiplePickPrompt = multiplePickPrompt;
    }

    
    /**
     * Getter for the printContainerLabelsIndicator property.
     * @return SelectionRegionPrintContainerLabels value of the property
     */
    public SelectionRegionPrintContainerLabels getPrintContainerLabelsIndicator() {
        return printContainerLabelsIndicator;
    }

    
    /**
     * Setter for the printContainerLabelsIndicator property.
     * @param printContainerLabelsIndicator the new printContainerLabelsIndicator value
     */
    public void setPrintContainerLabelsIndicator(SelectionRegionPrintContainerLabels printContainerLabelsIndicator) {
        this.printContainerLabelsIndicator = printContainerLabelsIndicator;
    }

    
    /**
     * Getter for the requireCaseLabelCheckDigits property.
     * @return boolean value of the property
     */
    public boolean isRequireCaseLabelCheckDigits() {
        return requireCaseLabelCheckDigits;
    }

    
    /**
     * Setter for the requireCaseLabelCheckDigits property.
     * @param requireCaseLabelCheckDigits the new requireCaseLabelCheckDigits value
     */
    public void setRequireCaseLabelCheckDigits(boolean requireCaseLabelCheckDigits) {
        this.requireCaseLabelCheckDigits = requireCaseLabelCheckDigits;
    }

    
    /**
     * Getter for the requireDeliveryPrompt property.
     * @return boolean value of the property
     */
    public boolean isRequireDeliveryPrompt() {
        return requireDeliveryPrompt;
    }

    
    /**
     * Setter for the requireDeliveryPrompt property.
     * @param requireDeliveryPrompt the new requireDeliveryPrompt value
     */
    public void setRequireDeliveryPrompt(boolean requireDeliveryPrompt) {
        this.requireDeliveryPrompt = requireDeliveryPrompt;
    }

    
    /**
     * Getter for the requireQuantityVerification property.
     * @return boolean value of the property
     */
    public boolean isRequireQuantityVerification() {
        return requireQuantityVerification;
    }

    
    /**
     * Setter for the requireQuantityVerification property.
     * @param requireQuantityVerification the new requireQuantityVerification value
     */
    public void setRequireQuantityVerification(boolean requireQuantityVerification) {
        this.requireQuantityVerification = requireQuantityVerification;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectionRegionProfile)) {
            return false;
        }
        final SelectionRegionProfile other = (SelectionRegionProfile) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        // TODO: No good value for this -- may not be needed, though. -ddoubleday
        return "";
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#clone()
     */
    @Override
    public SelectionRegionProfile clone() throws CloneNotSupportedException {
        return (SelectionRegionProfile) super.clone();    
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 