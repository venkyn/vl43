/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 
 *
 * @author Administrator
 */
public class AssignmentLaborRoot extends CommonModelObject implements Serializable {

    //
    private static final long serialVersionUID = 1969103048748819778L;
    
    //Assignment labor detail belongs to
    private Assignment          assignment;
    
    //Operator that worked the period of time
    private Operator            operator;
    
    //Operators Labor Detail record assignment labor associated with.
    private OperatorLabor       operatorLabor;
    
    // The id of the operator break labor record that is associated with the assignment.
    // This property is populated if the operator takes a break in the middle of the
    // assignment.
    private Long                breakLaborId;
    
    //Time Operator started 
    private Date                startTime;
    
    //Time Operator Ended
    private Date                endTime;
    
    //The difference between startTime and endTime in milliseconds
    private Long                duration;
    
    //Total quantity of items picked during period
    private Integer             quantityPicked;
    
    //Assignment value used to prorate labor when picking a group
    private Integer             assignmentProrateCount;

    //Group value used to prorate labor when picking a group
    private Integer             groupProrateCount;
    
    //Group Number
    private Long                groupNumber;
 
    //Group value used to prorate labor when picking a group
    private Integer             groupCount;
    
    // actual pick rate for this assignment
    private Double              actualRate;
    
    // actual precent of goal
    private Double              percentOfGoal;
 
    //Was this exported yet?
    private ExportStatus    exportStatus = ExportStatus.NotExported;

    /**
     * Getter for the assignment property.
     * @return Assignment value of the property
     */
    public Assignment getAssignment() {
        return this.assignment;
    }

    
    /**
     * Setter for the assignment property.
     * @param assignment the new assignment value
     */
    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    
    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    
    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    
    
    /**
     * Getter for the operatorLabor property.
     * @return OperatorLabor value of the property
     */
    public OperatorLabor getOperatorLabor() {
        return this.operatorLabor;
    }

    
    /**
     * Setter for the operatorLabor property.
     * @param operatorLaborDetail the new operatorLabor value
     */
    public void setOperatorLabor(OperatorLabor operatorLaborDetail) {
        this.operatorLabor = operatorLaborDetail;
    }

    
    /**
     * Getter for the breakLaborId property.
     * @return OperatorBreakLabor value of the property
     */
    public Long getBreakLaborId() {
        return this.breakLaborId;
    }

   
    /**
     * Setter for the breakLaborId property.
     * @param breakLaborId the new breakLaborId value
     */
    public void setBreakLaborId(Long breakLaborId) {
        this.breakLaborId = breakLaborId;
    }
    

    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return this.startTime;
    }

    
    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return this.endTime;
    }


    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
 
    
    /**
     * Getter for the duration property.
     * @return Long value of the property in seconds
     */
    public Long getDuration() {
        return this.duration;
    }

    
    /**
     * Setter for the duration property.
     * @param duration the duration value in milliseconds. 
     */
    public void setDuration(Long duration) {
        this.duration = duration; 
    }
    
    
    /**
     * Getter for the quantityPicked property.
     * @return Integer value of the property
     */
    public Integer getQuantityPicked() {
        return this.quantityPicked;
    }

    
    /**
     * Setter for the quantityPicked property.
     * @param quantityPicked the new quantityPicked value
     */
    public void setQuantityPicked(Integer quantityPicked) {
        this.quantityPicked = quantityPicked;
    }
    
    /**
     * Getter for the assignmentProrateCount property.
     * @return Integer value of the property
     */
    public Integer getAssignmentProrateCount() {
        return this.assignmentProrateCount;
    }

    
    /**
     * Setter for the assignmentProrateCount property.
     * @param assignmentProrateCount the new assignmentProRateCount value
     */
    public void setAssignmentProrateCount(Integer assignmentProrateCount) {
        this.assignmentProrateCount = assignmentProrateCount;
    }

    
    
    /**
     * Getter for the groupProRateCount property.
     * @return Integer value of the property
     */
    public Integer getGroupProrateCount() {
        return this.groupProrateCount;
    }
    
    
    /**
     * Setter for the groupProRateCount property.
     * @param groupProrateCount the new groupProRateCount value
     */
    public void setGroupProrateCount(Integer groupProrateCount) {
        this.groupProrateCount = groupProrateCount;
    }
  
 
    /**
     * Getter for the groupNumber property.
     * @return Long value of the property
     */
    public Long getGroupNumber() {
        return this.groupNumber;
    }

    
    /**
     * Setter for the groupNumber property.
     * @param groupNumber the new groupNumber value
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    
    /**
     * Getter for the groupCount property.
     * @return Integer value of the property
     */
    public Integer getGroupCount() {
        return this.groupCount;
    }

    
    /**
     * Setter for the groupCount property.
     * @param groupCount the new groupCount value
     */
    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }
    
    
    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        //TODO Rupert wants to remove these
        return new BigDecimal(this.actualRate)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    
    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }
    
    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        //TODO Rupert wants to remove these
        return new BigDecimal(this.percentOfGoal)
        .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    
    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

      
    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }


    
    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AssignmentLabor)) {
            return false;
        }
        final AssignmentLabor other = (AssignmentLabor) obj;
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        if (getStartTime() == null) {
            if (other.getStartTime() != null) {
                return false;
            }
        } else if (!getStartTime().equals(other.getStartTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((startTime == null) ? 0 
            : startTime.hashCode());
        return result;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getAssignment().getDescriptiveText();
    }
   
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 