/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.io.Serializable;

/**
 * This class is a component that groups together the fields that
 * make up the user defineable settings for a region.
 */
public class RegionUserSettingsRoot implements Serializable, Cloneable {

    private static final int DEFAULT_BASE_QUANTITY_THRESHOLD = 10;

    private static final int DEFAULT_BASE_WEIGHT_THRESHOLD = 20;

    //
    private static final long serialVersionUID = 7208903822651043377L;

    //************************************************************
    //Assignment Issuance
    //************************************************************
    //Maximum number of work identifiers a operator can request
    private int               maximumAssignmentsAllowed = 1;

    //If region is defined for manual issuance, how many digits an operator
    //must speak to request work, 0=All
    private int               workIdentifierRequestLength = 0;


    //************************************************************
    //Pick Prompt Settings
    //************************************************************
    //Determines if the quantity of one should be spoken at a single pick prompt
    private boolean           suppressQuantityOfOne = false;
    
    //************************************************************
    //Base Item Settings
    //************************************************************
    //Determines if base items should be calculated and how they should be picked
    private SelectionRegionBaseItem     pickBaseItemsBy 
        = SelectionRegionBaseItem.PickByItem;
    
    //How much an item must weight to qualify to be a base item
    private double            baseWeightThreshold = DEFAULT_BASE_WEIGHT_THRESHOLD;
    
    //How many of an item must exist to qualify to be a base item
    private Integer           baseQuantityThreshold = DEFAULT_BASE_QUANTITY_THRESHOLD;
    
    //************************************************************
    //Container Settings
    //************************************************************
    //Number of digits an operator must speak to confirm a container
    private int               numberOfContainerDigitsSpeak = 2;
    
    
    //************************************************************
    //General Task Flow/Functionality Settings
    //************************************************************
    //Determines whether an operator can use skip aisle command in this region
    private boolean           allowSkipSlot = true;

    //Determines whether an operator can use skip slot command in this region
    private boolean           allowSkipAisle = true;

    //Determines whether an operator can sign off while working an assignment 
    //in this region
    private boolean           allowSignOffAtAnyPoint = true;

    //Detemrines if and when an operator will be sent back for shorted items at 
    //the end of an assignment
    private SelectionRegionGoBackForShorts         goBackForShortsIndicator = 
                            SelectionRegionGoBackForShorts.Never;

    //Allows operator to use repick skips command to pick skipped items
    //instead of waiting until end of picks list
    private boolean           allowRepickSkips = false;

    //Determines if task should check flagged locations (empty at time of get picks)
    //to see if they were replenished, and if not automaticlly skip location
    private boolean           verifyReplenishment = false;
    
    private SelectionRegionAutoMarkOutShorts autoMarkOutShorts = 
                            SelectionRegionAutoMarkOutShorts.Never;

    //Determines if an operator will be prompted to get picks in reverse order or not
    private boolean           allowReversePicking = false;

    //Determines if operator is required to confirm delivery
    private boolean           requireDeliveryConfirmation = false;
    

    //************************************************************
    //Configurable Assignment Summary
    //************************************************************
    //Assignment Summary prompt for normal assignments 
    private SelectionRegionSummaryPrompt         summaryPromptIndicator = 
                    SelectionRegionSummaryPrompt.TaskDefaultPrompt;

    private SummaryPrompt     summaryPrompt;
    
    
    
    
    
    /**
     * Getter for the summaryPrompt property.
     * @return SummaryPrompt value of the property
     */
    public SummaryPrompt getSummaryPrompt() {
        return summaryPrompt;
    }





    
    /**
     * Setter for the summaryPrompt property.
     * @param summaryPrompt the new summaryPrompt value
     */
    public void setSummaryPrompt(SummaryPrompt summaryPrompt) {
        this.summaryPrompt = summaryPrompt;
    }





    /**
     * Getter for the summaryPromptIndicator property.
     * @return SelectionRegionSummaryPrompt value of the property
     */
    public SelectionRegionSummaryPrompt getSummaryPromptIndicator() {
        return summaryPromptIndicator;
    }




    
    /**
     * Setter for the summaryPromptIndicator property.
     * @param summaryPromptIndicator the new summaryPromptIndicator value
     */
    public void setSummaryPromptIndicator(SelectionRegionSummaryPrompt summaryPromptIndicator) {
        this.summaryPromptIndicator = summaryPromptIndicator;
    }




    /**
     * Getter for the autoMarkOutShorts property.
     * @return autoMarkOutShorts value of the property
     */
    public SelectionRegionAutoMarkOutShorts getAutoMarkOutShorts() {
        return autoMarkOutShorts;
    }



    
    /**
     * Setter for the autoMarkOutShorts property.
     * @param autoMarkOutShorts the new autoMarkOutShorts value
     */
    public void setAutoMarkOutShorts(SelectionRegionAutoMarkOutShorts autoMarkOutShorts) {
        this.autoMarkOutShorts = autoMarkOutShorts;
    }



    
    /**
     * Getter for the pickBaseItemsBy property.
     * @return pickBaseItemsBy value of the property
     */
    public SelectionRegionBaseItem getPickBaseItemsBy() {
        return pickBaseItemsBy;
    }



    
    /**
     * Setter for the pickBaseItemsBy property.
     * @param pickBaseItemsBy the new pickBaseItemsBy value
     */
    public void setPickBaseItemsBy(SelectionRegionBaseItem pickBaseItemsBy) {
        this.pickBaseItemsBy = pickBaseItemsBy;
    }

    /**
     * Getter for the allowRepickSkips property.
     * @return boolean value of the property
     */
    public boolean isAllowRepickSkips() {
        return this.allowRepickSkips;
    }

    
    /**
     * Setter for the allowRepickSkips property.
     * @param allowRepickSkips the new allowRepickSkips value
     */
    public void setAllowRepickSkips(boolean allowRepickSkips) {
        this.allowRepickSkips = allowRepickSkips;
    }

    
    /**
     * Getter for the allowReversePicking property.
     * @return boolean value of the property
     */
    public boolean isAllowReversePicking() {
        return this.allowReversePicking;
    }

    
    /**
     * Setter for the allowReversePicking property.
     * @param allowReversePicking the new allowReversePicking value
     */
    public void setAllowReversePicking(boolean allowReversePicking) {
        this.allowReversePicking = allowReversePicking;
    }

    
    /**
     * Getter for the allowSignOffAtAnyPoint property.
     * @return boolean value of the property
     */
    public boolean isAllowSignOffAtAnyPoint() {
        return this.allowSignOffAtAnyPoint;
    }

    
    /**
     * Setter for the allowSignOffAtAnyPoint property.
     * @param allowSignOffAtAnyPoint the new allowSignOffAtAnyPoint value
     */
    public void setAllowSignOffAtAnyPoint(boolean allowSignOffAtAnyPoint) {
        this.allowSignOffAtAnyPoint = allowSignOffAtAnyPoint;
    }

    
    /**
     * Getter for the allowSkipAisle property.
     * @return boolean value of the property
     */
    public boolean isAllowSkipAisle() {
        return this.allowSkipAisle;
    }

    
    /**
     * Setter for the allowSkipAisle property.
     * @param allowSkipAisle the new allowSkipAisle value
     */
    public void setAllowSkipAisle(boolean allowSkipAisle) {
        this.allowSkipAisle = allowSkipAisle;
    }

    
    /**
     * Getter for the allowSkipSlot property.
     * @return boolean value of the property
     */
    public boolean isAllowSkipSlot() {
        return this.allowSkipSlot;
    }

    
    /**
     * Setter for the allowSkipSlot property.
     * @param allowSkipSlot the new allowSkipSlot value
     */
    public void setAllowSkipSlot(boolean allowSkipSlot) {
        this.allowSkipSlot = allowSkipSlot;
    }

    
    /**
     * Getter for the baseQuantityThreshold property.
     * @return Integer value of the property
     */
    public Integer getBaseQuantityThreshold() {
        return this.baseQuantityThreshold;
    }

    
    /**
     * Setter for the baseQuantityThreshold property.
     * @param baseQuantityThreshold the new baseQuantityThreshold value
     */
    public void setBaseQuantityThreshold(Integer baseQuantityThreshold) {
        this.baseQuantityThreshold = baseQuantityThreshold;
    }

    
    /**
     * Getter for the baseWeightThreshold property.
     * @return Double value of the property
     */
    public double getBaseWeightThreshold() {
        return this.baseWeightThreshold;
    }

    
    /**
     * Setter for the baseWeightThreshold property.
     * @param baseWeightThreshold the new baseWeightThreshold value
     */
    public void setBaseWeightThreshold(double baseWeightThreshold) {
        this.baseWeightThreshold = baseWeightThreshold;
    }

    
    /**
     * Getter for the goBackForShortsIndicator property.
     * @return SelectionRegionGoBackForShorts value of the property
     */
    public SelectionRegionGoBackForShorts getGoBackForShortsIndicator() {
        return this.goBackForShortsIndicator;
    }

    
    /**
     * Setter for the goBackForShortsIndicator property.
     * @param goBackForShortsIndicator the new goBackForShortsIndicator value
     */
    public void setGoBackForShortsIndicator(SelectionRegionGoBackForShorts goBackForShortsIndicator) {
        this.goBackForShortsIndicator = goBackForShortsIndicator;
    }

    
    /**
     * Getter for the maximumAssignmentsAllowed property.
     * @return int value of the property
     */
    public int getMaximumAssignmentsAllowed() {
        return this.maximumAssignmentsAllowed;
    }

    
    /**
     * Setter for the maximumAssignmentsAllowed property.
     * @param maximumAssignmentsAllowed the new maximumAssignmentsAllowed value
     */
    public void setMaximumAssignmentsAllowed(int maximumAssignmentsAllowed) {
        this.maximumAssignmentsAllowed = maximumAssignmentsAllowed;
    }

    
    /**
     * Getter for the numberOfContainerDigitsSpeak property.
     * @return int value of the property
     */
    public int getNumberOfContainerDigitsSpeak() {
        return this.numberOfContainerDigitsSpeak;
    }

    
    /**
     * Setter for the numberOfContainerDigitsSpeak property.
     * @param numberOfContainerDigitsSpeak the new numberOfContainerDigitsSpeak value
     */
    public void setNumberOfContainerDigitsSpeak(int numberOfContainerDigitsSpeak) {
        this.numberOfContainerDigitsSpeak = numberOfContainerDigitsSpeak;
    }

    
    /**
     * Getter for the requireDeliveryConfirmation property.
     * @return boolean value of the property
     */
    public boolean isRequireDeliveryConfirmation() {
        return this.requireDeliveryConfirmation;
    }

    
    /**
     * Setter for the requireDeliveryConfirmation property.
     * @param requireDeliveryConfirmation the new requireDeliveryConfirmation value
     */
    public void setRequireDeliveryConfirmation(boolean requireDeliveryConfirmation) {
        this.requireDeliveryConfirmation = requireDeliveryConfirmation;
    }

    
    
    /**
     * Getter for the suppressQuantityOfOne property.
     * @return boolean value of the property
     */
    public boolean isSuppressQuantityOfOne() {
        return this.suppressQuantityOfOne;
    }

    
    /**
     * Setter for the suppressQuantityOfOne property.
     * @param suppressQuantityOfOne the new suppressQuantityOfOne value
     */
    public void setSuppressQuantityOfOne(boolean suppressQuantityOfOne) {
        this.suppressQuantityOfOne = suppressQuantityOfOne;
    }

    
    /**
     * Getter for the verifyReplenishment property.
     * @return boolean value of the property
     */
    public boolean isVerifyReplenishment() {
        return this.verifyReplenishment;
    }

    
    /**
     * Setter for the verifyReplenishment property.
     * @param verifyReplenishment the new verifyReplenishment value
     */
    public void setVerifyReplenishment(boolean verifyReplenishment) {
        this.verifyReplenishment = verifyReplenishment;
    }

    
    /**
     * Getter for the workIdentifierRequestLength property.
     * @return int value of the property
     */
    public int getWorkIdentifierRequestLength() {
        return this.workIdentifierRequestLength;
    }

    
    /**
     * Setter for the workIdentifierRequestLength property.
     * @param workIdentifierRequestLength the new workIdentifierRequestLength value
     */
    public void setWorkIdentifierRequestLength(int workIdentifierRequestLength) {
        this.workIdentifierRequestLength = workIdentifierRequestLength;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#clone()
     */
    @Override
    public RegionUserSettings clone() throws CloneNotSupportedException {
        return (RegionUserSettings) super.clone();    
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 