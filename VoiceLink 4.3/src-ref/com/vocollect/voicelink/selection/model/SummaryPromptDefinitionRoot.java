/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.CommonModelObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * 
 *
 * @author mkoenig
 */
public class SummaryPromptDefinitionRoot extends CommonModelObject {

    private static final long serialVersionUID = 488488206811822888L;

    //Region definition belongs with
    private SummaryPrompt     summaryPrompt;
    
    private long              promptItemId;
    
    //order of definition
    private int               position;
    
    //Prompt Item
    private SummaryPromptItem   promptItem;
    
    //Prompt value for condition 1
    private String            promptValue1;

    //Prompt value for condition 2
    private String            promptValue2;
    
    //Prompt value for condition 3
    private String            promptValue3;
    
    //Allow item to be spoken phonetically
    private boolean           allowSpeakingPhonetically = false;
    
    //What part of item to speak
    private SummaryPromptCharactersToSpeak  speakCharactersIndicator = 
                    SummaryPromptCharactersToSpeak.All;
    
    //Number of characters to speak if left or right choosen
    private int               numberOfCharacters = 0;
    
    //For numeric values should value be spoken as a group total 
    //or for each work ID
    private short             speakTotalsIndicator = 0;
    
    private List<SummaryPromptDefinitionLocale> localeDefintions;

    private Long              tempId;
    
    
    
    /**
     * Getter for the tempId property.
     * @return Long value of the property
     */
    public Long getTempId() {
        return tempId;
    }

    
    /**
     * Setter for the tempId property.
     * @param tempId the new tempId value
     */
    public void setTempId(Long tempId) {
        this.tempId = tempId;
    }


    /**
     * Getter for the position property.
     * @return int value of the property
     */
    public int getPosition() {
        return position;
    }


    /**
     * Setter for the position property.
     * @param position the new position value
     */
    public void setPosition(int position) {
        this.position = position;
    }


    /**
     * Getter for the promptItem property.
     * @return SummaryPromptItem value of the property
     */
    public SummaryPromptItem getPromptItem() {
        return promptItem;
    }

    
    /**
     * Setter for the promptItem property.
     * @param promptItem the new promptItem value
     */
    public void setPromptItem(SummaryPromptItem promptItem) {
        this.promptItem = promptItem;
    }

    
    /**
     * Getter for the summaryPrompt property.
     * @return SummaryPrompt value of the property
     */
    public SummaryPrompt getSummaryPrompt() {
        return summaryPrompt;
    }


    
    /**
     * Setter for the summaryPrompt property.
     * @param summaryPrompt the new summaryPrompt value
     */
    public void setSummaryPrompt(SummaryPrompt summaryPrompt) {
        this.summaryPrompt = summaryPrompt;
    }


    /**
     * Getter for the localeDefintions property.
     * @return List&lt;SummaryPromptDefinitionLocale&gt; value of the property
     */
    public List<SummaryPromptDefinitionLocale> getLocaleDefintions() {
        if (localeDefintions == null) {
            localeDefintions = new ArrayList<SummaryPromptDefinitionLocale>();
        }
        return localeDefintions;
    }

    
    /**
     * Setter for the localeDefintions property.
     * @param localeDefintions the new localeDefintions value
     */
    public void setLocaleDefintions(List<SummaryPromptDefinitionLocale> localeDefintions) {
        this.localeDefintions = localeDefintions;
    }


    /**
     * Getter for the allowSpeakingPhonetically property.
     * @return boolean value of the property
     */
    public boolean isAllowSpeakingPhonetically() {
        return this.allowSpeakingPhonetically;
    }

    
    /**
     * Setter for the allowSpeakingPhonetically property.
     * @param allowSpeakingPhonetically the new allowSpeakingPhonetically value
     */
    public void setAllowSpeakingPhonetically(boolean allowSpeakingPhonetically) {
        this.allowSpeakingPhonetically = allowSpeakingPhonetically;
    }

    
    /**
     * Getter for the speakCharactersIndicator property.
     * @return short value of the property
     */
    public SummaryPromptCharactersToSpeak getSpeakCharactersIndicator() {
        return this.speakCharactersIndicator;
    }

    
    /**
     * Setter for the speakCharactersIndicator property.
     * @param charactersToSpeakIndicator the new speakCharactersIndicator value
     */
    public void setSpeakCharactersIndicator(
             SummaryPromptCharactersToSpeak charactersToSpeakIndicator) {
        this.speakCharactersIndicator = charactersToSpeakIndicator;
    }

    
    /**
     * Getter for the numberOfCharacters property.
     * @return int value of the property
     */
    public int getNumberOfCharacters() {
        return this.numberOfCharacters;
    }

    
    /**
     * Setter for the numberOfCharacters property.
     * @param numberOfCharacters the new numberOfCharacters value
     */
    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }

    
    /**
     * Getter for the promptValue1 property.
     * @return String value of the property
     */
    public String getPromptValue1() {
        return this.promptValue1;
    }

    
    /**
     * Setter for the promptValue1 property.
     * @param promptValue1 the new promptValue1 value
     */
    public void setPromptValue1(String promptValue1) {
        this.promptValue1 = promptValue1;
    }

    
    /**
     * Getter for the promptValue2 property.
     * @return String value of the property
     */
    public String getPromptValue2() {
        return this.promptValue2;
    }

    
    /**
     * Setter for the promptValue2 property.
     * @param promptValue2 the new promptValue2 value
     */
    public void setPromptValue2(String promptValue2) {
        this.promptValue2 = promptValue2;
    }

    
    /**
     * Getter for the promptValue3 property.
     * @return String value of the property
     */
    public String getPromptValue3() {
        return this.promptValue3;
    }

    
    /**
     * Setter for the promptValue3 property.
     * @param promptValue3 the new promptValue3 value
     */
    public void setPromptValue3(String promptValue3) {
        this.promptValue3 = promptValue3;
    }

    
    /**
     * Getter for the speakTotalsIndicator property.
     * @return short value of the property
     */
    public short getSpeakTotalsIndicator() {
        return this.speakTotalsIndicator;
    }

    
    /**
     * Setter for the speakTotalsIndicator property.
     * @param speakTotalsIndicator the new speakTotalsIndicator value
     */
    public void setSpeakTotalsIndicator(short speakTotalsIndicator) {
        this.speakTotalsIndicator = speakTotalsIndicator;
    }

    

    /**
     * get prompt for specified locale. Return default if definition for locale
     * is not found.
     * 
     * @param locale - locale to find
     * @return - prompt string.
     */
    public String getPromptValue1(Locale locale) {
        
        for (SummaryPromptDefinitionLocale spdl : getLocaleDefintions()) {
            if (spdl.getLocale().equals(locale)) {
                return spdl.getPromptValue1(); 
            }
        }
        
        return getPromptValue1();
    }

    /**
     * get prompt for specified locale. Return default if definition for locale
     * is not found.
     * 
     * @param locale - locale to find
     * @return - prompt string.
     */
    public String getPromptValue2(Locale locale) {
        
        for (SummaryPromptDefinitionLocale spdl : getLocaleDefintions()) {
            if (spdl.getLocale().equals(locale)) {
                return spdl.getPromptValue2(); 
            }
        }
        
        return getPromptValue2();
    }

    /**
     * get prompt for specified locale. Return default if definition for locale
     * is not found.
     * 
     * @param locale - locale to find
     * @return - prompt string.
     */
    public String getPromptValue3(Locale locale) {
        
        for (SummaryPromptDefinitionLocale spdl : getLocaleDefintions()) {
            if (spdl.getLocale().equals(locale)) {
                return spdl.getPromptValue3(); 
            }
        }
        
        return getPromptValue3();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SummaryPromptDefinition)) {
            return false;
        }
        final SummaryPromptDefinition other = (SummaryPromptDefinition) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getPromptValue1();
    }


    
    /**
     * Getter for the promptItemId property.
     * @return - promptItemId
     */
    public long getPromptItemId() {
        return promptItemId;
    }


    
    /**
     * Setter for the promptItemId property.
     * @param promptItemId prompt item id
     */
    public void setPromptItemId(long promptItemId) {
        this.promptItemId = promptItemId;
    }
    
    /**
     * Check to see if a locale is defined for this defintion.
     * 
     * @param locale - locale to check for
     * @return - true if defined, otherwise false.
     */
    public boolean isLocalDefined(Locale locale) {
        for (SummaryPromptDefinitionLocale spdl : getLocaleDefintions()) {
            if (spdl.getLocale().equals(locale)) {
                return true;
            }
        }
        
        return false;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 