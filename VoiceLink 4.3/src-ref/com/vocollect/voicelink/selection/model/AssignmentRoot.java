/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Resequencable;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.selection.SelectionErrorCode;

import static com.vocollect.voicelink.selection.model.AssignmentStatus.Available;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Canceled;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Complete;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.InProgress;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Passed;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Short;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Suspended;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.Unavailable;
import static com.vocollect.voicelink.selection.model.PickStatus.Assigned;
import static com.vocollect.voicelink.selection.model.PickStatus.BaseItem;
import static com.vocollect.voicelink.selection.model.PickStatus.NotPicked;
import static com.vocollect.voicelink.selection.model.PickStatus.Partial;
import static com.vocollect.voicelink.selection.model.PickStatus.Shorted;
import static com.vocollect.voicelink.selection.model.PickStatus.ShortsGoBack;
import static com.vocollect.voicelink.selection.model.PickStatus.Skipped;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Model object representing a VoiceLink Selection assignment.
 * 
 * @author mkoenig
 */
public class AssignmentRoot extends CommonModelObject implements Serializable, Taggable, Resequencable {

    private static final long serialVersionUID = 6480571401517467618L;

    // Assignment number from WMS, imported
    private Long number;

    // Populated for split assignments, numeric value from 1 to 99 depending
    // on how many times assignment was split. NULL if assignment not split
    private Long splitNumber;

    // Group information about group assignment is assigned to
    private AssignmentGroup groupInfo;

    // Route from WMS, Imported
     private String route;
     
     // Localized version of the route since its changed on import
     private String localizedRoute;

    // Delivery Date from WMS, Imported
    private Date deliveryDate;

    // Delivery Location, This can be Imported or set using delivery location
    // mapping functionality
    private String deliveryLocation;

    // Customer information
    private Customer customerInfo;

    // Region assignment belongs in, (Region Number imported and mapped to
    // regionID)
    private SelectionRegion region;

    // Work Identifier value
    private WorkIdentifier workIdentifier;

    // Summary information about assignment
    private AssignmentSummary summaryInfo;

    // Operator that is currently working on the assignment, operator that
    // finished
    // the assignment, or that was assigned to the assignment
    private Operator operator;

    // Time assignment was instally started
    private Date startTime;

    // Time when assignment was fully completed
    private Date endTime;

    // Base items should only be calculated for an assignment once, if this
    // flag is set then they were already calculated and should not be
    // calculated
    // again
    private boolean calculatedBaseItems = false;

    // Type of assignmet
    private AssignmentType type = AssignmentType.Normal;

    // Status of assignment
    // Default is Available
    private AssignmentStatus status = Available;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;
    
    //Count of containers for Display Purposes
    private int containerCount = 0;
    
    //Count of pick detail information available
    private int pickDetailCount = 0; 

    // Picks belonging to this assignment
    private List<Pick> picks = new ArrayList<Pick>();

    // labor records belonging to this assignment (Added for archive purposes)
    private List<AssignmentLabor> labor = new ArrayList<AssignmentLabor>();

    // Containers that this assignment was put into
    private Set<Container> containers;
    
    //Departure date time
    private Date departureDateTime;
    
    //Loading region number
    private LoadingRegion loadingRegion; 

    private Set<Tag> tags;

    // Viewable assignment number 
    private String viewableAssignmentNumber;

    // Used by resequence, not stored in DB
    private Long newSequenceNumber;

    //Used to determine order to issue assignments, imported
    private Long              sequenceNumber = -1L;

    //Stores original sequence number since the sequence number could be changed
    //by the resequence functionality
    private Long              originalSequenceNumber = -1L;

    private Integer           variableWeightTotal = 0;
    
    private Integer           purgeable = 0;
    
    private String            reservedBy;
    
    private Integer           priority;
    
    
    /**
     * Getter for the priority property.
     * @return Integer value of the property
     */
    public Integer getPriority() {
        return priority;
    }
    
    /**
     * Setter for the priority property.
     * @param priority the new priority value
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * Getter for the reservedBy property.
     * @return String value of the property
     */
    public String getReservedBy() {
        return reservedBy;
    }
    
    /**
     * Setter for the reservedBy property.
     * @param reservedBy the new reservedBy value
     */
    public void setReservedBy(String reservedBy) {
        this.reservedBy = reservedBy;
    }

    /**
     * Getter for the purgeable property.
     * @return Integer value of the property
     */
    public Integer getPurgeable() {
        return purgeable;
    }
    
    /**
     * Setter for the purgeable property.
     * @param purgeable the new purgeable value
     */
    public void setPurgeable(Integer purgeable) {
        this.purgeable = purgeable;
    }

    /**
     * Getter for the labor property.
     * @return List&lt;AssignmentLabor&gt; value of the property
     */
    public List<AssignmentLabor> getLabor() {
        return labor;
    }
    
    /**
     * Setter for the labor property.
     * @param labor the new labor value
     */
    public void setLabor(List<AssignmentLabor> labor) {
        this.labor = labor;
    }


    /**
     * Getter for the originalSequenceNumber property.
     * @return Long value of the property
     */
    public Long getOriginalSequenceNumber() {
        return this.originalSequenceNumber;
    }

    
    /**
     * Setter for the originalSequenceNumber property.
     * @param originalSequenceNumber the new originalSequenceNumber value
     */
    private void setOriginalSequenceNumber(Long originalSequenceNumber) {
        this.originalSequenceNumber = originalSequenceNumber;
    }

    
    /**
     * Getter for the sequenceNumber property.
     * @return Long value of the property
     */
    public Long getSequenceNumber() {
        return this.sequenceNumber;
    }

    
    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(Long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        if (getOriginalSequenceNumber() == -1L) {
            setOriginalSequenceNumber(getSequenceNumber());
        }
    }

    /**
     * Getter for the newSequenceNumber property.
     * @return Long value of the property
     */
    public Long getNewSequenceNumber() {
        return newSequenceNumber;
    }
    
    /**
     * Setter for the newSequenceNumber property.
     * @param newSequenceNumber the new newSequenceNumber value
     */
    public void setNewSequenceNumber(Long newSequenceNumber) {
        this.newSequenceNumber = newSequenceNumber;
    }

    /**
     * Get the viewable assignment number combine with split number if it was
     * split.
     * 
     * @return - viewable assignment number
     */
    public String getViewableAssignmentNumber() {
        if (viewableAssignmentNumber != null) {
            if (splitNumber != null) {
                return new BigDecimal(viewableAssignmentNumber)
                .setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
            } else {
                return new BigDecimal(viewableAssignmentNumber)
                .setScale(0, BigDecimal.ROUND_HALF_UP).toPlainString();
            }
        }
        return viewableAssignmentNumber;
    }
    
    /**
     * Set the viewable assignment number.
     * 
     * @param viewableAssignmentNumber Viewable Assignment Number
     */
    public void setViewableAssignmentNumber(String viewableAssignmentNumber) {
        this.viewableAssignmentNumber = viewableAssignmentNumber;
    }
    
    /**
     * Getter for the containerCount property.
     * @return int value of the property
     */
    public int getContainerCount() {
        return containerCount;
    }

    
    /**
     * Setter for the containerCount property.
     * @param containerCount the new containerCount value
     */
    public void setContainerCount(int containerCount) {
        this.containerCount = containerCount;
    }

    
    /**
     * Getter for the pickDetailCount property.
     * @return int value of the property
     */
    public int getPickDetailCount() {
        return pickDetailCount;
    }

    
    /**
     * Setter for the pickDetailCount property.
     * @param pickDetailCount the new pickDetailCount value
     */
    public void setPickDetailCount(int pickDetailCount) {
        this.pickDetailCount = pickDetailCount;
    }

    /**
     * Getter for the customerInfo property.
     * @return Customer value of the property
     */
    public Customer getCustomerInfo() {
        if (this.customerInfo == null) {
            setCustomerInfo(new Customer());
        }
        return this.customerInfo;
    }

    /**
     * Setter for the customerInfo property.
     * @param customerInfo the new customerInfo value
     */
    public void setCustomerInfo(Customer customerInfo) {
        this.customerInfo = customerInfo;
    }

    /**
     * Getter for the groupInfo property.
     * @return AssignmentGroup value of the property
     */
    public AssignmentGroup getGroupInfo() {
        if (this.groupInfo == null) {
            setGroupInfo(new AssignmentGroup());
        }
        return this.groupInfo;
    }

    /**
     * Setter for the groupInfo property.
     * @param groupInfo the new groupInfo value
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        this.groupInfo = groupInfo;
    }

    /**
     * Getter for the summaryInfo property.
     * @return AssignmentSummary value of the property
     */
    public AssignmentSummary getSummaryInfo() {
        if (this.summaryInfo == null) {
            setSummaryInfo(new AssignmentSummary());
        }
        return this.summaryInfo;
    }

    /**
     * Setter for the summaryInfo property.
     * @param summaryInfo the new summaryInfo value
     */
    public void setSummaryInfo(AssignmentSummary summaryInfo) {
        this.summaryInfo = summaryInfo;
    }

    /**
     * Getter for the workIdentifier property.
     * @return WorkIdentifier value of the property
     */
    public WorkIdentifier getWorkIdentifier() {
        if (this.workIdentifier == null) {
            setWorkIdentifier(new WorkIdentifier());
        }
        return this.workIdentifier;
    }

    /**
     * Setter for the workIdentifier property.
     * @param workIdentifier the new workIdentifier value
     */
    public void setWorkIdentifier(WorkIdentifier workIdentifier) {
        this.workIdentifier = workIdentifier;
    }

    /**
     * Getter for the calculatedBaseItems property.
     * @return boolean value of the property
     */
    public boolean isCalculatedBaseItems() {
        return this.calculatedBaseItems;
    }

    /**
     * Setter for the calculatedBaseItems property.
     * @param calculatedBaseItems the new calculatedBaseItems value
     */
    public void setCalculatedBaseItems(boolean calculatedBaseItems) {
        this.calculatedBaseItems = calculatedBaseItems;
    }

    /**
     * Getter for the containers property.
     * @return Set&lt;Container&gt; value of the property
     */
    public Set<Container> getContainers() {
        if (this.containers == null) {
            this.containers = new HashSet<Container>();
        }
        return this.containers;
    }

    /**
     * Setter for the containers property.
     * @param containers the new containers value
     */
    public void setContainers(Set<Container> containers) {
        this.containers = containers;
    }

    /**
     * Getter for the deliveryDate property.
     * @return Date value of the property
     */
    public Date getDeliveryDate() {
        return this.deliveryDate;
    }

    /**
     * Setter for the deliveryDate property.
     * @param deliveryDate the new deliveryDate value
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Getter for the deliveryLocation property.
     * @return String value of the property
     */
    public String getDeliveryLocation() {
        return this.deliveryLocation;
    }

    /**
     * Setter for the deliveryLocation property.
     * @param deliveryLocation the new deliveryLocation value
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getEndTime() {
        return this.endTime;
    }

    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the number property.
     * @return long value of the property
     */
    public Long getNumber() {
        return this.number;
    }

    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the picks property.
     * @return Set&lt;Pick%gt; value of the property
     */
    public List<Pick> getPicks() {
        return this.picks;
    }

    /**
     * Setter for the picks property.
     * @param picks the new picks value
     */
    public void setPicks(List<Pick> picks) {
        this.picks = picks;
    }

    /**
     * Getter for the region property.
     * @return SelectionRegion value of the property
     */
    public SelectionRegion getRegion() {
        return this.region;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(SelectionRegion region) {
        this.region = region;
    }

    /**
     * Getter for the route property.
     * @return String value of the property
     */
    public String getRoute() {
        return this.route;
    }

    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Setter for the localized route property
     * @param localizedRoute String - localized route value
     */
    public void setLocalizedRoute(String localizedRoute) {
        this.localizedRoute = localizedRoute;
    }

    /**
     * Getter for the localized route
     * @return String - localized route
     */
    public String getLocalizedRoute() {
        return ResourceUtil.getLocalizedKeyValue(this.route);    }

    /**
     * Getter for the splitNumber property.
     * @return Long value of the property
     */
    public Long getSplitNumber() {
        return this.splitNumber;
    }

    /**
     * Setter for the splitNumber property.
     * @param splitNumber the new splitNumber value
     */
    public void setSplitNumber(Long splitNumber) {
        this.splitNumber = splitNumber;
    }

    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for the status property.
     * @return AssignmentStatus value of the property
     */
    public AssignmentStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(AssignmentStatus status) {
        this.status = status;
    }

    /**
     * Getter for the type property.
     * @return AssignmentType value of the property
     */
    public AssignmentType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(AssignmentType type) {
        this.type = type;
    }

    
    /**
     * @return the departureDateTime
     */
    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    
    /**
     * @param departureDateTime the departureDateTime to set
     */
    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    
    /**
     * @return the loadingRegion
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }

    
    /**
     * @param loadingRegion the loadingRegion to set
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Assignment)) {
            return false;
        }
        final Assignment other = (Assignment) obj;
        if (getNumber() != other.getNumber()) {
            return false;
        }
        if (getSplitNumber() == null) {
            if (other.getSplitNumber() != null) {
                return false;
            }
        } else if (!getSplitNumber().equals(other.getSplitNumber())) {
        return false;
    }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        final int power = 32;
        int result = 1;
        result = (prime * result) + (int) (number ^ (number >>> power));
        result = (prime * result) 
            + ((splitNumber == null) ? 0 : splitNumber.hashCode());
        return result;
    }

    
    /**
     * Method to add and associate a new pick object with the assignment.
     * 
     * @param newPick - the pick to add to the assignment
     */
    public void addPick(Pick newPick) {
        getPicks().add(newPick);
        newPick.setAssignment((Assignment) this);
    }
    
    /**
     * Add a container to the assignment and set relationships.
     * 
     * @param container - container to add
     */
    public void addContainer(Container container) {
        getContainers().add(container);
        container.getAssignments().add((Assignment) this);
        setContainerCount(getContainerCount() + 1);
    }
    
    /**
     * Method used to update model, starting assignment for 
     * the specified operator.
     * 
     * @param assignOperator - Operator to assign to assignment
     * @param assignStartTime - Time assignment is being started
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    public void startAssignment(Operator assignOperator, Date assignStartTime) 
    throws BusinessRuleException {

        if (!getStatus().isInSet(Available, Passed, Suspended, InProgress)) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_NOT_AVAILABLE,
                new UserMessage("assignment.updateStatus.error.notAvailable"));
        }

        if (getStatus() == InProgress && getOperator() != assignOperator) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_ALREADY_IN_PROGRESS,
                new UserMessage("assignment.updateStatus.error.inprogressByAnotherOperator"));
        }

        if (getGroupInfo().getGroupNumber() == null) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_NOT_IN_GROUP,
                new UserMessage("assignment.updateStatus.error.inprogressByAnotherOperator"));
        }

        processStatusChange(InProgress);

        setOperator(assignOperator);
        if (getStartTime() == null) {
            setStartTime(assignStartTime);
        }
    }

    /**
     * @param statusTo - Status to set assignment to
     * @param assignEndTime - Time stopping assignment
     * 
     * Method to update model and stop the assignment an operator was working on
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    public void stopAssignment(AssignmentStatus statusTo, Date assignEndTime) throws BusinessRuleException {

        if (!statusTo.isInSet(Complete, Passed, Suspended, Canceled)) {
            throw new BusinessRuleException(SelectionErrorCode.ASSIGNMENT_INVALID_STOP_STATUS,
                new UserMessage("assignment.updateStatus.error.invalidStopStatus"));
        }

        // Must be called before following updates to ensure pick statuses get
        // set correctly
        processStatusChange(statusTo);

        // Change Go Back for Short to Shorted
        if (getStatus().isInSet(Complete, Short, Passed)) {

            for (Pick pick : getPicks()) {
                
                if (pick.getStatus().isInSet(ShortsGoBack, Partial)) {
                    pick.finalizePickStatus();
                }
            }
        }

        // Cancel picks if canceling assignment
        if (getStatus() == Canceled) {
            //Clear operator from assingment first since no operator should be
            //assigned to a cancled assignment
            setOperator(null);
            for (Pick pick : getPicks()) {
                if (pick.getStatus().isInSet(NotPicked, Skipped, BaseItem)) {
                    pick.cancelPick();
                }
            }
        }

        //Adjust the status based on pick information
        // this needs to happen here to fix VLINK-1951
        adjustStatus(statusTo);
        
        // Close Containers if necessary
        if (getStatus().isInSet(Complete, Short, Canceled)) {

            for (Container container : getContainers()) {
                if (container.getStatus() == ContainerStatus.Available
                    || container.getStatus() == ContainerStatus.Open) {
                    container.setStatus(ContainerStatus.Closed);
                }
            }
        }

        // Final Processing
        if (getStatus().isInSet(Complete, Short)) {
            //Only set end time if it wasn't already set.
            if (getEndTime() == null) {
                setEndTime(assignEndTime);
            }
        } else if (getStatus().isInSet(Passed)) {
            setOperator(null);
        }

    }

    /**
     * Method to update model assignment status.
     * @param statusTo - Status to change to
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    public void changeStatus(AssignmentStatus statusTo) throws BusinessRuleException {
        AssignmentStatus changeTo = statusTo;

        //if attempting to set to short then set to complete and let the rest
        // of the logix decide if it needs to be shorted or not.
        if (changeTo == Short) {
            changeTo = Complete; 
        }
        
        // If setting status to complete or canceled directly then calculate end
        // time and call
        // stop assignment, else just call processStatusChange
        if (changeTo.isInSet(Complete, Canceled)) {
            stopAssignment(changeTo, new Date());
        } else {
            processStatusChange(changeTo);
        }
    }

    /**
     * Method to validate and update status change to the model object. 
     * @param statusTo - status to change assignment to
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    protected void processStatusChange(AssignmentStatus statusTo) throws BusinessRuleException {

        switch (getStatus()) {
        case Available:
            checkStatusChangeFromAvailable(statusTo);
            break;
        case Unavailable:
            checkStatusChangeFromUnavailable(statusTo);
            break;
        case InProgress:
            checkStatusChangeFromInProgress(statusTo);
            break;
        case Suspended:
            checkStatusChangeFromSuspended(statusTo);
            break;
        case Passed:
            checkStatusChangeFromPassed(statusTo);
            break;
        case Short:
            checkStatusChangeFromShort(statusTo);
            break;
        default:
            checkStatusChangeFromOther(statusTo);
            break;
        }

        // Short any remaining picks
        shortRemainingPicks(statusTo);

        //Set status to what callered asked
        setStatus(statusTo);

    }
    

    /**
     * Method to check if status to be changed from Available to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromAvailable(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Available, Unavailable, InProgress, Canceled)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }

    /**
     * Method to check if status to be changed from Unavailable to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromUnavailable(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Available, Unavailable, Canceled)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }
    
    /**
     * Method to check if status to be changed from In-Progress to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromInProgress(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, Passed, Suspended, InProgress)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }

        // Make sure all picks are completed
        if (statusTo == Complete) {
            for (Pick pick : getPicks()) {
                if (pick.getStatus().isInSet(NotPicked, BaseItem, 
                                             Skipped, Partial)) {
                    throw new BusinessRuleException(
                        SelectionErrorCode.ASSIGNMENT_PICKS_NOT_COMPLETED,
                        new UserMessage("assignment.updateStatus.error.picksNotCompleted"));
                }
            }
        }
    }
    
    /**
     * Method to check if status to be changed from Available to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromSuspended(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, InProgress, Suspended)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }
    
    /**
     * Method to check if status to be changed from Passed to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromPassed(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, InProgress, Suspended, Passed)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }
    
    /**
     * Method to check if status to be changed from Short to the specified status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromShort(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (!statusTo.isInSet(Complete, Short)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }
    
    /**
     * Method to check if status to be changed from other statuses to 
     * the specified status. This method was added to allow 
     * for customization that require the addition
     * of a new status.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Business exception
     */
    protected void checkStatusChangeFromOther(AssignmentStatus statusTo) 
    throws BusinessRuleException {
        if (statusTo != getStatus()) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.ASSIGNMENT_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange", 
                getStatus().name(), statusTo.name()));
        }
    }
    
    /**
     * Method to short any remaining picks when assignment is being complete
     * from the User Interface.
     * 
     * @param statusTo - status assignment is being changed to
     * @throws BusinessRuleException - Throws business Rule Exception
     */
    protected void shortRemainingPicks(AssignmentStatus statusTo) throws 
    BusinessRuleException {
        if (statusTo == Complete) {
            for (Pick pick : getPicks()) {
                if (pick.getStatus().isInSet(NotPicked, BaseItem, 
                                             Skipped, Partial)) {

                    Date pickTime = new Date();
                    PickDetail detail = new PickDetail();
                    detail.setType(PickDetailType.ManualPicked);
                    detail.setPick(pick);
                    detail.setPickTime(pickTime);
                    detail.setQuantityPicked(0);

                    pick.addPickDetail(detail);
                }
            }
        }
    }

    /**
     * Method to adjust the status from what the caller specified when appropriate
     * Mainly used for when a user passes a group of assignments and 1 or more
     * assignments in that group had all picks completed, therefore the assignment will
     * be set to complete (or short) instead of passed.
     *  
     * @param statusTo - status assignment is being changed to
     */
    protected void adjustStatus(AssignmentStatus statusTo) {
        // complete passed assignments instead of setting them to passed
        if (statusTo.isInSet(Passed, Complete)) {
            int shortCount = getShortedCount() + getPartailCount();
            int notPickedCount = getNotPickedCount();

            if (getType() == AssignmentType.Chase) {
                setStatus(statusTo);
            } else if (statusTo == Passed && notPickedCount == 0 && shortCount == 0) {
                setStatus(Complete);
            } else if (statusTo == Passed && notPickedCount == 0
                && shortCount > 0) {
                setStatus(Short);
            } else if (statusTo == Complete && shortCount > 0) {
                setStatus(Short);
            } else {
                setStatus(statusTo);
            }
         
        }
        
    }
       
    /**
     * get number of picks that are not picked.
     * 
     * @return - number of picks that are not picked.
     */
    public int getNotPickedCount() {
        int count = 0;

        for (Pick pick : getPicks()) {
            if (pick.getStatus().isInSet(NotPicked, BaseItem, Skipped)) {
                count = count + 1;
            }
        }
        
        return count; 
    }

    /**
     * get number of picks that are shorted.
     * 
     * @return - number of picks that are not picked.
     */
    public int getShortedCount() {
        int count = 0;

        for (Pick pick : getPicks()) {
            if (pick.getStatus().isInSet(Shorted, Assigned)) {
                count = count + 1;
            }
        }
        
        return count; 
    }
    
    /**
     * get number of picks that are shorted.
     * 
     * @return - number of picks that are not picked.
     */
    public int getPartailCount() {
        int count = 0;

        for (Pick pick : getPicks()) {
            if (pick.getStatus().isInSet(ShortsGoBack, Partial)) {
                count = count + 1;
            }
        }
        
        return count; 
    }
    
    /**
     * get number of picks that are shorted.
     * 
     * @return - number of picks that are not picked.
     */
    public int getCancelCount() {
        int count = 0;

        for (Pick pick : getPicks()) {
            if (pick.getStatus().isInSet(PickStatus.Canceled)) {
                count = count + 1;
            }
        }
        
        return count; 
    }
    
    
    /**
     * Returns a list of statuses that an user can set the assignment to when
     * editing the assignment from the UI.
     * 
     * @return list of status operator can change this assignment to from GUI
     */
    public Map<Integer, String> getGUITransitionStatuses() {

        Map<Integer, String> statuses = new TreeMap<Integer, String>();
        switch (getStatus()) {
        case Available:
          statuses.put(new Integer(AssignmentStatus.Available.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Available));
          statuses.put(new Integer(AssignmentStatus.Unavailable.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Unavailable));
          statuses.put(new Integer(AssignmentStatus.Canceled.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Canceled));
            break;
        case Unavailable:
          statuses.put(new Integer(AssignmentStatus.Unavailable.getValue()),
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Unavailable));
          statuses.put(new Integer(AssignmentStatus.Available.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Available));
          statuses.put(new Integer(AssignmentStatus.Canceled.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Canceled));
            break;
        case Suspended:
          statuses.put(new Integer(AssignmentStatus.Suspended.getValue()),
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Suspended));
          statuses.put(new Integer(AssignmentStatus.Complete.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Complete));
            break;
        case Passed:
          statuses.put(new Integer(AssignmentStatus.Passed.getValue()), 
              ResourceUtil.getLocalizedEnumName(AssignmentStatus.Passed));
          statuses.put(new Integer(AssignmentStatus.Complete.getValue()),
            ResourceUtil.getLocalizedEnumName(AssignmentStatus.Complete));
            break;
        default:
            statuses.put(new Integer(getStatus().getValue()), 
                ResourceUtil.getLocalizedEnumName(getStatus()));
            break;
        }

        return statuses;
    }




    /**
     * Getter for the variableWeightTotal property.
     * @return Integer value of the property
     */
    public Integer getVariableWeightTotal() {
        return variableWeightTotal;
    }

    /**
     * Setter for the variableWeightTotal property.
     * @param variableWeightTotal the new variableWeightTotal value
     */
    public void setVariableWeightTotal(Integer variableWeightTotal) {
        this.variableWeightTotal = variableWeightTotal;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return Long.toString(this.getNumber());
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#toString()
     */
    @Override
    public String toString() {
        return this.getDescriptiveText();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * return the items remaining of assignment.
     * 
     * @return - total items remaining
     */
    public Integer getItemsRemaining() {
        Integer itemsRemaining = 0;

        for (Pick p : getPicks()) {
            if (p.getStatus().isInSet(PickStatus.NotPicked, 
                                      PickStatus.BaseItem, 
                                      PickStatus.Partial, 
                                      PickStatus.ShortsGoBack, 
                                      PickStatus.Skipped)) {
                itemsRemaining += (p.getQuantityToPick() - p.getQuantityPicked());
            }
        }
        
        return itemsRemaining;
    }
    
    /**
     * return the items picked of assignment.
     * 
     * @return - total items picked
     */
    public Integer getItemsPicked() {
        Integer itemsPicked = 0;

        for (Pick p : getPicks()) {
            itemsPicked += p.getQuantityPicked();
        }
        
        return itemsPicked;
    }
    
    /**
     * return the base items remaining of assignment.
     * 
     * @return - total base items remaining
     */
    public Integer getBaseItemsRemaining() {
        Integer itemsRemaining = 0;

        for (Pick p : getPicks()) {
            if (p.getStatus().equals(PickStatus.BaseItem)) {
                itemsRemaining += (p.getQuantityToPick() - p.getQuantityPicked());
            }
        }
        
        return itemsRemaining;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 