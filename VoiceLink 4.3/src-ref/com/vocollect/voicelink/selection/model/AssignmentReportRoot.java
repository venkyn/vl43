/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Date;

/**
 * Model object representing a VoiceLink Selection assignment report.
 * 
 * @author mnichols
 */
public class AssignmentReportRoot {

    private String assignmentNumber;
    private String workIdentifier;
    private AssignmentStatus assignmentStatus;
    private String customerNumber;
    private String route;
    private String containerNumber;
    private ContainerStatus containerStatus;
    private Long containerType;
    private Long pickID;
    private String preAisle;
    private String aisle;
    private String postAisle;
    private String slot;
    private String itemNumber;
    private String itemDescription;
    private Integer quantityPicked;
    private Integer quantityToPick;
    private Date pickTime;
    private String operatorId;
    private String operatorName;
    private Double variableWeight;
    private String serialNumber;
    private String lotNumber;
    private PickStatus pickStatus;

    
    /**
     * Constructor.
     * @param assignmentNumber assignment number
     * @param workIdentifier work identifier
     * @param assignmentStatus assignment status
     * @param customerNumber customer number
     * @param route route
     * @param containerNumber container number
     * @param containerStatus container status
     * @param containerType container type
     * @param pickID pick id
     * @param preAisle location pre-aisle
     * @param aisle location aisle
     * @param postAisle location post-aisle
     * @param slot location slot
     * @param itemNumber item number
     * @param itemDescription item description
     * @param quantityPicked pick quantity
     * @param quantityToPick picks remaining
     * @param pickTime time of pick
     * @param operatorId operator ID
     * @param operatorName operator name
     * @param variableWeight variable weight
     * @param serialNumber serial number
     * @param lotNumber lot number
     * @param pickStatus status of pick
     */
    public AssignmentReportRoot(String assignmentNumber,
                                String workIdentifier,
                                AssignmentStatus assignmentStatus,
                                String customerNumber,
                                String route,
                                String containerNumber,
                                ContainerStatus containerStatus,
                                Long containerType,
                                Long pickID,
                                String preAisle,
                                String aisle,
                                String postAisle,
                                String slot,
                                String itemNumber,
                                String itemDescription,
                                Integer quantityPicked,
                                Integer quantityToPick,
                                Date pickTime,
                                String operatorId,
                                String operatorName,
                                Double variableWeight,
                                String serialNumber,
                                String lotNumber,
                                PickStatus pickStatus) {
        
        
        super();
        this.assignmentNumber = assignmentNumber;
        this.workIdentifier = workIdentifier;
        this.assignmentStatus = assignmentStatus;
        this.customerNumber = customerNumber;
        this.route = route;
        this.containerNumber = containerNumber;
        this.containerStatus = containerStatus;
        this.containerType = containerType;
        this.pickID = pickID;
        this.preAisle = preAisle;
        this.aisle = aisle;
        this.postAisle = postAisle;
        this.slot = slot;
        this.itemNumber = itemNumber;
        this.itemDescription = itemDescription;
        this.quantityPicked = quantityPicked;
        this.quantityToPick = quantityToPick;
        this.pickTime = pickTime;
        this.operatorId = operatorId;
        this.operatorName = operatorName;
        this.variableWeight = variableWeight;
        this.serialNumber = serialNumber;
        this.lotNumber = lotNumber;
        this.pickStatus = pickStatus;
    }

    /**
     * Constructor.
     * @param assignmentNumber assignment number
     * @param workIdentifier work identifier
     * @param assignmentStatus assignment status
     * @param customerNumber customer number
     * @param route route
     * @param containerNumber container number
     * @param containerStatus container status
     * @param containerType container type
     * @param pickID pick id
     * @param preAisle location pre-aisle
     * @param aisle location aisle
     * @param postAisle location post-aisle
     * @param slot location slot
     * @param itemNumber item number
     * @param itemDescription item description
     * @param quantityPicked pick quantity
     * @param quantityToPick picks remaining
     * @param pickTime time of pick
     * @param operatorId operator ID
     * @param operatorName operator name
     * @param variableWeight variable weight
     * @param serialNumber serial number
     * @param lotNumber lot number
     * @param pickStatus status of pick
     */
    public AssignmentReportRoot(Long assignmentNumber,
                                String workIdentifier,
                                AssignmentStatus assignmentStatus,
                                String customerNumber,
                                String route,
                                String containerNumber,
                                ContainerStatus containerStatus,
                                Long containerType,
                                Long pickID,
                                String preAisle,
                                String aisle,
                                String postAisle,
                                String slot,
                                String itemNumber,
                                String itemDescription,
                                Integer quantityPicked,
                                Integer quantityToPick,
                                Date pickTime,
                                String operatorId,
                                String operatorName,
                                Double variableWeight,
                                String serialNumber,
                                String lotNumber,
                                PickStatus pickStatus) {
        
        
        super();
        this.assignmentNumber = assignmentNumber.toString();
        this.workIdentifier = workIdentifier;
        this.assignmentStatus = assignmentStatus;
        this.customerNumber = customerNumber;
        this.route = route;
        this.containerNumber = containerNumber;
        this.containerStatus = containerStatus;
        this.containerType = containerType;
        this.pickID = pickID;
        this.preAisle = preAisle;
        this.aisle = aisle;
        this.postAisle = postAisle;
        this.slot = slot;
        this.itemNumber = itemNumber;
        this.itemDescription = itemDescription;
        this.quantityPicked = quantityPicked;
        this.quantityToPick = quantityToPick;
        this.pickTime = pickTime;
        this.operatorId = operatorId;
        this.operatorName = operatorName;
        this.variableWeight = variableWeight;
        this.serialNumber = serialNumber;
        this.lotNumber = lotNumber;
        this.pickStatus = pickStatus;
    }    
    
    /**
     * @return assignmentNumber
     */
     public String getAssignmentNumber() {
         return assignmentNumber;
     }

     /**
      * @param assignmentNumber String value for the AssignmentNumber
      */
     public void setAssignmentNumber(String assignmentNumber) {
         this.assignmentNumber = assignmentNumber;
     }

     /**
      * @return workIdentifier
      */
     public String getWorkIdentifier() {
         return workIdentifier;
     }

     /**
      * @param workIdentifier String value of the WorkId
      */
     public void setWorkIdentifier(String workIdentifier) {
         this.workIdentifier = workIdentifier;
     }

     /**
      * @return assignmentStatus Assignments Status
      */
     public AssignmentStatus getAssignmentStatus() {
         return assignmentStatus;
     }

     /**
      * @param assignmentStatus Assignments Status
      */
     public void setAssignmentStatus(AssignmentStatus assignmentStatus) {
         this.assignmentStatus = assignmentStatus;
     }

     /**
      * @return customernumber
      */
     public String getCustomerNumber() {
         return customerNumber;
     }

     /**
      * @param customerNumber String value for the customerNumber
      */
     public void setCustomerNumber(String customerNumber) {
         this.customerNumber = customerNumber;
     }

     /**
      * @return route
      */
     public String getRoute() {
         return route;
     }

     /**
      * @param route String value for the route
      */
     public void setRoute(String route) {
         this.route = route;
     }

     /**
      * @return containerNumber
      */
     public String getContainerNumber() {
         return containerNumber;
     }

     /**
      * @param containerNumber String value for the ContainerNumber
      */
     public void setContainerNumber(String containerNumber) {
         this.containerNumber = containerNumber;
     }

     /**
      * @return containerStatus
      */
     public ContainerStatus getContainerStatus() {
         return containerStatus;
     }

     /**
      * @param containerStatus containerStatus
      */
     public void setContainerStatus(ContainerStatus containerStatus) {
         this.containerStatus = containerStatus;
     }

     /**
      * @return containerType
      */
     public Long getContainerType() {
         return containerType;
     }

     /**
      * @param containerType Long value for thecontainerType
      */
     public void setContainerType(Long containerType) {
         this.containerType = containerType;
     }

     /**
      * @return pickID
      */
     public Long getPickID() {
         return pickID;
     }

     /**
      * @param pickID Long value for the pickID
      */
     public void setPickID(Long pickID) {
         this.pickID = pickID;
     }

     /**
      * @return preAisle
      */
     public String getPreAisle() {
         return preAisle;
     }

     /**
      * @param preAisle String value for the preAisle
      */
     public void setPreAisle(String preAisle) {
         this.preAisle = preAisle;
     }

     /**
      * @return aisle
      */
     public String getAisle() {
         return aisle;
     }

     /**
      * @param aisle String value for the aisle
      */
     public void setAisle(String aisle) {
         this.aisle = aisle;
     }

     /**
      * @return postAisle
      */
     public String getPostAisle() {
         return postAisle;
     }

     /**
      * @param postAisle String value for the postAisle
      */
     public void setPostAisle(String postAisle) {
         this.postAisle = postAisle;
     }

     /**
      * @return slot
      */
     public String getSlot() {
         return slot;
     }

     /**
      * @param slot String value for the slot
      */
     public void setSlot(String slot) {
         this.slot = slot;
     }

     /**
      * @return itemNumber
      */
     public String getItemNumber() {
         return itemNumber;
     }

     /**
      * @param itemNumber String value for the itemNumber
      */
     public void setItemNumber(String itemNumber) {
         this.itemNumber = itemNumber;
     }

     /**
      * @return itemDescription
      */
     public String getItemDescription() {
         return itemDescription;
     }

     /**
      * @param itemDescription String value for the itemDescription
      */
     public void setItemDescription(String itemDescription) {
         this.itemDescription = itemDescription;
     }

     /**
      * @return quantityPicked
      */
     public Integer getQuantityPicked() {
         return quantityPicked;
     }

     /**
      * @param quantityPicked Integer value for the quantityPicked
      */
     public void setQuantityPicked(Integer quantityPicked) {
         this.quantityPicked = quantityPicked;
     }

     /**
      * @return quantityToPick
      */
     public Integer getQuantityToPick() {
         return quantityToPick;
     }

     /**
      * @param quantityToPick Integer value for the quantityToPick
      */
     public void setQuantityToPick(Integer quantityToPick) {
         this.quantityToPick = quantityToPick;
     }

     /**
      * @return pickTime
      */
     public Date getPickTime() {
         return pickTime;
     }

     /**
      * @param pickTime Date value for the pickTime
      */
     public void setPickTime(Date pickTime) {
         this.pickTime = pickTime;
     }

     /**
      * @return operatorId
      */
     public String getOperatorId() {
         return operatorId;
     }

     /**
      * @param operatorId String value for the operatorId
      */
     public void setOperatorId(String operatorId) {
         this.operatorId = operatorId;
     }

     /**
      * @return operatorName
      */
     public String getOperatorName() {
         return operatorName;
     }

     /**
      * @param operatorName String value for the operatorName
      */
     public void setOperatorName(String operatorName) {
         this.operatorName = operatorName;
     }

     /**
      * @return variableWeight
      */
     public Double getVariableWeight() {
         return variableWeight;
     }

     /**
      * @param variableWeight Double value for the variableWeight
      */
     public void setVariableWeight(Double variableWeight) {
         this.variableWeight = variableWeight;
     }

     /**
      * @return serialNumber
      */
     public String getSerialNumber() {
         return serialNumber;
     }

     /**
      * @param serialNumber String value for the serialNumber
      */
     public void setSerialNumber(String serialNumber) {
         this.serialNumber = serialNumber;
     }

     /**
      * @return lotNumber
      */
     public String getLotNumber() {
         return lotNumber;
     }

     /**
      * @param lotNumber String value for the lotNumber
      */
     public void setLotNumber(String lotNumber) {
         this.lotNumber = lotNumber;
     }

     /**
      * @return pickStatus
      */
     public PickStatus getPickStatus() {
         return pickStatus;
     }

     /**
      * @param pickStatus PickStatus value for the pickStatus
      */
     public void setPickStatus(PickStatus pickStatus) {
         this.pickStatus = pickStatus;
     }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 