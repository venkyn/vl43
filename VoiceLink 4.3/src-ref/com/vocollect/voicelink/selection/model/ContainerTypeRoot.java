/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.BaseModelObject;


/**
 * 
 *
 * @author mkoenig
 */
public class ContainerTypeRoot extends BaseModelObject {

    //
    private static final long serialVersionUID = 6467845489835392595L;

    //Viewable Container description
    private String              name;
    
    //Speakable Container description
    private String              phoneticName;
    
    //Detemrines if operator should be prompted to create multiple containers, if true
    //a specified number of containers are created and a label printed for each one
    private Boolean           allowPreCreationOfContainers = false; 

    //Determines if operator is prompted for a container ID when container is created
    //otherwise VL system generates container ID 
    private boolean           promptOperatorForContainerID = false;
    
    //Determines if an operator is told to deliver a container when it is closed
    private boolean           requireDeliveryAfterClosed = false;

    //Determines if the operator can have more than 1 container open at a time
    //for a single assignment
    private boolean           allowMultipleOpenContainers = false;

    //Determines if target containers are required
    private boolean           requireTargetContainers = false;

    
    
    /**
     * Getter for the allowMultipleOpenContainers property.
     * @return boolean value of the property
     */
    public boolean isAllowMultipleOpenContainers() {
        return allowMultipleOpenContainers;
    }


    
    /**
     * Setter for the allowMultipleOpenContainers property.
     * @param allowMultipleOpenContainers the new allowMultipleOpenContainers value
     */
    public void setAllowMultipleOpenContainers(boolean allowMultipleOpenContainers) {
        this.allowMultipleOpenContainers = allowMultipleOpenContainers;
    }


    
    /**
     * Getter for the allowPreCreationOfContainers property.
     * @return Boolean value of the property
     */
    public Boolean getAllowPreCreationOfContainers() {
        return allowPreCreationOfContainers;
    }


    
    /**
     * Setter for the allowPreCreationOfContainers property.
     * @param allowPreCreationOfContainers the new allowPreCreationOfContainers value
     */
    public void setAllowPreCreationOfContainers(Boolean allowPreCreationOfContainers) {
        this.allowPreCreationOfContainers = allowPreCreationOfContainers;
    }


    
    /**
     * Getter for the promptOperatorForContainerID property.
     * @return boolean value of the property
     */
    public boolean isPromptOperatorForContainerID() {
        return promptOperatorForContainerID;
    }


    
    /**
     * Setter for the promptOperatorForContainerID property.
     * @param promptOperatorForContainerID the new promptOperatorForContainerID value
     */
    public void setPromptOperatorForContainerID(boolean promptOperatorForContainerID) {
        this.promptOperatorForContainerID = promptOperatorForContainerID;
    }


    
    /**
     * Getter for the requireDeliveryAfterContainerClosed property.
     * @return boolean value of the property
     */
    public boolean isRequireDeliveryAfterClosed() {
        return requireDeliveryAfterClosed;
    }


    
    /**
     * Setter for the requireDeliveryAfterClosed property.
     * @param requireDeliveryAfterClosed the new requireDeliveryAfterClosed value
     */
    public void setRequireDeliveryAfterClosed(boolean requireDeliveryAfterClosed) {
        this.requireDeliveryAfterClosed = requireDeliveryAfterClosed;
    }


    
    /**
     * Getter for the requireTargetContainers property.
     * @return boolean value of the property
     */
    public boolean isRequireTargetContainers() {
        return requireTargetContainers;
    }


    
    /**
     * Setter for the requireTargetContainers property.
     * @param requireTargetContainers the new requireTargetContainers value
     */
    public void setRequireTargetContainers(boolean requireTargetContainers) {
        this.requireTargetContainers = requireTargetContainers;
    }


    /**
     * Getter for the phoneticDescription property.
     * @return String value of the property
     */
    public String getPhoneticName() {
        return this.phoneticName;
    }

    
    /**
     * Setter for the phoneticName property.
     * @param phoneticName the new phoneticName value
     */
    public void setPhoneticName(String phoneticName) {
        this.phoneticName = phoneticName;
    }

    
    /**
     * Getter for the viewableDescription property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContainerType)) {
            return false;
        }
        final ContainerType other = (ContainerType) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((name == null) ? 0 : name.hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 