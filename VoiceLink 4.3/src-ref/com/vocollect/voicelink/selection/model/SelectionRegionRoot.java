/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.Region;

import java.io.Serializable;
import java.util.Set;

/**
 * Selection Region Model. 
 *
 * @author Administrator
 */
public class SelectionRegionRoot extends Region implements Serializable {

    private static final long serialVersionUID = 7512446134319774782L;
    
    private boolean hiddenCheck = false;

    //************************************************************
    //Region Profile
    //************************************************************
    //Region profile for normal assignments
    private SelectionRegionProfile  profileNormalAssignment;

    //Region profile for chase assignemnts
    private SelectionRegionProfile  profileChaseAssignment;
    
    //************************************************************
    //User Settings
    //************************************************************
    private RegionUserSettings            userSettingsNormal = new RegionUserSettings();
    
    private RegionUserSettings            userSettingsChase = new RegionUserSettings();
    
    //************************************************************
    //Related sets
    //************************************************************
    //Set of assignments within region
    private Set<Assignment>  assignments;
    
    
    
    
    /**
     * Getter for the assignments property.
     * @return Set&gt;Assignment&lt; value of the property
     */
    public Set<Assignment> getAssignments() {
        return this.assignments;
    }

    
    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }

    
    /**
     * Getter for the userSettingsChase property.
     * @return UserSettings value of the property
     */
    public RegionUserSettings getUserSettingsChase() {
        return userSettingsChase;
    }



    
    /**
     * Setter for the userSettingsChase property.
     * @param userSettingsChase the new userSettingsChase value
     */
    public void setUserSettingsChase(RegionUserSettings userSettingsChase) {
        this.userSettingsChase = userSettingsChase;
    }



    
    /**
     * Getter for the userSettingsNormal property.
     * @return UserSettings value of the property
     */
    public RegionUserSettings getUserSettingsNormal() {
        return userSettingsNormal;
    }



    
    /**
     * Setter for the userSettingsNormal property.
     * @param userSettingsNormal the new userSettingsNormal value
     */
    public void setUserSettingsNormal(RegionUserSettings userSettingsNormal) {
        this.userSettingsNormal = userSettingsNormal;
    }



    /**
     * Getter for the profileChaseAssignment property.
     * @return SelectionRegionProfile value of the property
     */
    public SelectionRegionProfile getProfileChaseAssignment() {
        return this.profileChaseAssignment;
    }


    
    /**
     * Setter for the profileChaseAssignment property.
     * @param profileChaseAssignment the new profileChaseAssignment value
     */
    public void setProfileChaseAssignment(SelectionRegionProfile profileChaseAssignment) {
        this.profileChaseAssignment = profileChaseAssignment;
    }


    
    /**
     * Getter for the profileNormalAssignment property.
     * @return SelectionRegionProfile value of the property
     */
    public SelectionRegionProfile getProfileNormalAssignment() {
        return this.profileNormalAssignment;
    }


    
    /**
     * Setter for the profileNormalAssignment property.
     * @param profileNormalAssignment the new profileNormalAssignment value
     */
    public void setProfileNormalAssignment(SelectionRegionProfile profileNormalAssignment) {
        this.profileNormalAssignment = profileNormalAssignment;
    }



    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    /**
     * getter method to get the profile for a particular assignment type.
     * 
     * @param assignmentType - type of assignment to get profile for.
     * @return - return profile for specified assignment type
     */
    public SelectionRegionProfile getProfile(AssignmentType assignmentType) {
        if (assignmentType == AssignmentType.Normal) {
            return getProfileNormalAssignment();
        } else {
            return getProfileChaseAssignment();
        }
    }
    
    /**
     * Gte the user definable settings for a region based on assignment type.
     * 
     * @param assignmentType - Assignment type to get settings for
     * @return - user settings object
     */
    public RegionUserSettings getUserSettings(AssignmentType assignmentType) {
        if (assignmentType == AssignmentType.Normal) {
            return getUserSettingsNormal();
        } else {
            return getUserSettingsChase();
        }
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#clone()
     */
    @Override
    public SelectionRegion clone() throws CloneNotSupportedException {
        SelectionRegion regionClone = new SelectionRegion();
       if (this.getDescription() != null) {
        regionClone.setDescription(new String(this.getDescription()));
       }
        regionClone.setGoalRate(new Integer(this.getGoalRate()));
        regionClone.setName(new String(this.getName()));
        regionClone.setNumber(new Integer(this.getNumber()));
        regionClone.setProfileChaseAssignment(this.getProfileChaseAssignment().clone());
        regionClone.setProfileNormalAssignment(this.getProfileNormalAssignment().clone());
        regionClone.setUserSettingsChase(this.getUserSettingsChase().clone());
        regionClone.setUserSettingsNormal(this.getUserSettingsNormal().clone());
        return (regionClone);
    }


    
    /**
     * Getter for the hiddenCheck property.
     * @return the value of the property.
     */
    public boolean isHiddenCheck() {
        return hiddenCheck;
    }


    
    /**
     * Setter for the hiddenCheck property.
     * @param hiddenCheck the value
     */
    public void setHiddenCheck(boolean hiddenCheck) {
        this.hiddenCheck = hiddenCheck;
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 