/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author Administrator
 */
public class ContainerRoot extends CommonModelObject implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -3657499873087189673L;

    //Type Of Container
    private ContainerType     containerType;

    //container number supplied by task or system generated
    private String            containerNumber;

    //Targe Container indicator for picks going into this container
    private Integer           targetContainerIndicator;

    //Has container been exportStatus
    private ExportStatus      exportStatus = ExportStatus.NotExported;

    //Status
    private ContainerStatus   status = ContainerStatus.Available;

    //has container label previously been printed, used to determine if container
    //should be printed during reprint label command
    private int               printed = 0;

    //Assignments that were put into this container
    private Set<Assignment>   assignments;

    //Pick Detail records put into this container
    private List<PickDetail>   pickDetails;

    private Set<Tag> tags;



    /**
     * Getter for the targetContainerIndicator property.
     * @return Integer value of the property
     */
    public Integer getTargetContainerIndicator() {
        return targetContainerIndicator;
    }



    /**
     * Setter for the targetContainerIndicator property.
     * @param targetContainerIndicator the new targetContainerIndicator value
     */
    public void setTargetContainerIndicator(Integer targetContainerIndicator) {
        this.targetContainerIndicator = targetContainerIndicator;
    }

    /**
     * Getter for the first assignment in the list.
     * @return Integer value of the property
     */
    public Assignment getFirstAssignment() {
        if (this.assignments == null || this.assignments.size() == 0) {
            return null;
        }
        return (Assignment) this.assignments.toArray()[0];
    }

    /**
     * Getter for the assignments property.
     * @return Set&lt;Assignment&gt; value of the property
     */
    public Set<Assignment> getAssignments() {
        if (this.assignments == null) {
            this.assignments = new HashSet<Assignment>();
        }
        return this.assignments;
    }


    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    public void setAssignments(Set<Assignment> assignments) {
        this.assignments = assignments;
    }


    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }


    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }


    /**
     * Getter for the containerType property.
     * @return ContainerType value of the property
     */
    public ContainerType getContainerType() {
        return this.containerType;
    }


    /**
     * Setter for the containerType property.
     * @param containerType the new containerType value
     */
    public void setContainerType(ContainerType containerType) {
        this.containerType = containerType;
    }


    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }


    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }


    /**
     * Getter for the pickDetails property.
     * @return List&lt;PickDetail&gt; value of the property
     */
    public List<PickDetail> getPickDetails() {
        return this.pickDetails;
    }


    /**
     * Setter for the pickDetails property.
     * @param pickDetails the new pickDetails value
     */
    public void setPickDetails(List<PickDetail> pickDetails) {
        this.pickDetails = pickDetails;
    }


    /**
     * Getter for the printed property.
     * @return int value of the property
     */
    public int getPrinted() {
        return this.printed;
    }


    /**
     * Setter for the printed property.
     * @param printed the new printed value
     */
    public void setPrinted(int printed) {
        this.printed = printed;
    }


    /**
     * Getter for the status property.
     * @return ContainerStatus value of the property
     */
    public ContainerStatus getStatus() {
        return this.status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(ContainerStatus status) {
        this.status = status;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Container)) {
            return false;
        }
        final Container other = (Container) obj;
        if (getContainerNumber() == null) {
            if (other.getContainerNumber() != null) {
                return false;
            }
        } else if (!getContainerNumber().equals(other.getContainerNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((containerNumber == null) ? 0 : containerNumber.hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getContainerNumber();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 