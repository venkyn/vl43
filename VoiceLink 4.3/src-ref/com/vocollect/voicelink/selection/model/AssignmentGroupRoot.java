/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.io.Serializable;


/**
 * 
 *
 * @author mkoenig
 */
public class AssignmentGroupRoot implements Serializable {

    private static final long serialVersionUID = 6073977061779187769L;

    //Group assignment is assigned to
    private Long              groupNumber;

    //Order assignment was requested from task
    private int               requestedOrderNo = 1;

    //used to order assignments within a group
    private Integer           groupPosition;

    //used to order assignments within a group
    private Integer           groupCount = 1;

    /**
     * Getter for the groupCount property.
     * @return Integer value of the property
     */
    public Integer getGroupCount() {
        return groupCount;
    }


    
    /**
     * Setter for the groupCount property.
     * @param groupCount the new groupCount value
     */
    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }


    /**
     * Getter for the groupNumber property.
     * @return Long value of the property
     */
    public Long getGroupNumber() {
        return this.groupNumber;
    }

    
    /**
     * Setter for the groupNumber property.
     * @param groupNumber the new groupNumber value
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    
    /**
     * Getter for the groupPosition property.
     * @return Integer value of the property
     */
    public Integer getGroupPosition() {
        return this.groupPosition;
    }

    
    /**
     * Setter for the groupPosition property.
     * @param groupPosition the new groupPosition value
     */
    public void setGroupPosition(Integer groupPosition) {
        this.groupPosition = groupPosition;
    }

    
    /**
     * Getter for the requestedOrderNo property.
     * @return int value of the property
     */
    public int getRequestedOrderNo() {
        return this.requestedOrderNo;
    }

    
    /**
     * Setter for the requestedOrderNo property.
     * @param requestedOrderNo the new requestedOrderNo value
     */
    public void setRequestedOrderNo(int requestedOrderNo) {
        this.requestedOrderNo = requestedOrderNo;
    }

    /**
     * A grouped assignment is determined by the groupNumber & 
     * the count of assignments (groupCount).
     * @return boolean indicating if this assignment is grouped
     */
    public boolean isGrouped() {
        return (this.groupNumber != null && this.groupCount > 1);        
    }
 

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 