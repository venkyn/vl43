/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.selection.model;


import com.vocollect.voicelink.core.model.ExportTransportObject;

import java.util.HashMap;

/**
 * @author dgold
 *
 */
public class LaborExportTransport extends ExportTransportObject {

    /**
     * 
     */
    public LaborExportTransport() {
        super();
    }

    /**
     * @param fieldValues A collection of fieldValues
     * @param object new object
     */
    public LaborExportTransport(HashMap<String, Object> fieldValues,
            Object object) {
        super(fieldValues, object);
        // Nothing else to do.
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToExported(Object obj) {
        // TODO Auto-generated method stub

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateExportToInProgress(Object obj) {
        // TODO Auto-generated method stub

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 