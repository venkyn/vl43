/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.BaseModelObject;

import java.io.Serializable;


/**
 * Model object to generate a unique group number.
 *
 * @author mkoenig
 */
public class GroupNumberRoot extends BaseModelObject implements Serializable {

    //
    private static final long serialVersionUID = -8479111473296839804L;
    
    private int groupValue = 1;
    
    
    /**
     * Getter for the groupValue property.
     * @return int value of the property
     */
    public int getGroupValue() {
        return groupValue;
    }

    
    /**
     * Setter for the groupValue property.
     * @param groupValue the new groupValue value
     */
    public void setGroupValue(int groupValue) {
        this.groupValue = groupValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 