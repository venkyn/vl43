/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.BaseModelObject;



/**
 * 
 *
 * @author Administrator
 */
public class SummaryPromptItemRoot extends BaseModelObject {

    private static final long serialVersionUID = -8408778526001920023L;

    private static final String MSG_KEY = "summaryPrompt.prompt.type.";
    
    //Key Value of item
    private String            name;
    
    //Field type
    private SummaryPromptItemFieldType    fieldType = SummaryPromptItemFieldType.String;
    
    //Show additon options for item such as speak phoneticlly or left and right
    //characters to speak
    private boolean           showOptions = true;
    
    //How many of the three edit conditions to display
    private short             editBoxCount = 1;
    
    //Field/Property for item
    private String            assignmentProperty;
    
    
    
    
    
    
    /**
     * Getter for the assignmentProperty property.
     * @return String value of the property
     */
    public String getAssignmentProperty() {
        return assignmentProperty;
    }


    
    /**
     * Setter for the assignmentProperty property.
     * @param assignmentProperty the new assignmentProperty value
     */
    public void setAssignmentProperty(String assignmentProperty) {
        this.assignmentProperty = assignmentProperty;
    }


    /**
     * Getter for the editBoxCount property.
     * @return short value of the property
     */
    public short getEditBoxCount() {
        return editBoxCount;
    }

    
    /**
     * Setter for the editBoxCount property.
     * @param editBoxCount the new editBoxCount value
     */
    public void setEditBoxCount(short editBoxCount) {
        this.editBoxCount = editBoxCount;
    }

    
    /**
     * Getter for the fieldType property.
     * @return SummaryPromptItemFieldType value of the property
     */
    public SummaryPromptItemFieldType getFieldType() {
        return fieldType;
    }

    
    /**
     * Setter for the fieldType property.
     * @param fieldType the new fieldType value
     */
    public void setFieldType(SummaryPromptItemFieldType fieldType) {
        this.fieldType = fieldType;
    }

    
    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return MSG_KEY + getId();
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * Getter for the showOptions property.
     * @return boolean value of the property
     */
    public boolean isShowOptions() {
        return showOptions;
    }

    
    /**
     * Setter for the showOptions property.
     * @param showOptions the new showOptions value
     */
    public void setShowOptions(boolean showOptions) {
        this.showOptions = showOptions;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SummaryPromptItem other = (SummaryPromptItem) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((name == null) ? 0 : name.hashCode());
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 