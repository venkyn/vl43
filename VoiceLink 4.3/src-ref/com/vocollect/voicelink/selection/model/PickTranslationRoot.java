/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.DataTranslation;

import java.util.List;


/**
 * Translation class for Pick Sorting.
 *
 */
public class PickTranslationRoot {

    private Long        id;

    // Unit of measure to pick. Spoken in task at pick prompt (Optionsl)
    private String unitOfMeasure;

    // Message to speak at pick prompt (Optional)
    private String promptMessage;

    //Properties for localized sorting
    private List<DataTranslation>    unitOfMeasureTrans;

    //Properties for localized sorting
    private List<DataTranslation>    promptMessageTrans;

    
    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    public Long getId() {
        return id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    
    /**
     * Getter for the promptMessage property.
     * @return String value of the property
     */
    public String getPromptMessage() {
        return promptMessage;
    }

    
    /**
     * Setter for the promptMessage property.
     * @param promptMessage the new promptMessage value
     */
    public void setPromptMessage(String promptMessage) {
        this.promptMessage = promptMessage;
    }

    
    /**
     * Getter for the promptMessageTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getPromptMessageTrans() {
        return promptMessageTrans;
    }

    
    /**
     * Setter for the promptMessageTrans property.
     * @param promptMessageTrans the new promptMessageTrans value
     */
    public void setPromptMessageTrans(List<DataTranslation> promptMessageTrans) {
        this.promptMessageTrans = promptMessageTrans;
    }

    
    /**
     * Getter for the unitOfMeasure property.
     * @return String value of the property
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    
    /**
     * Setter for the unitOfMeasure property.
     * @param unitOfMeasure the new unitOfMeasure value
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    
    /**
     * Getter for the unitOfMeasureTrans property.
     * @return List&lt;DataTranslation&gt; value of the property
     */
    public List<DataTranslation> getUnitOfMeasureTrans() {
        return unitOfMeasureTrans;
    }

    
    /**
     * Setter for the unitOfMeasureTrans property.
     * @param unitOfMeasureTrans the new unitOfMeasureTrans value
     */
    public void setUnitOfMeasureTrans(List<DataTranslation> unitOfMeasureTrans) {
        this.unitOfMeasureTrans = unitOfMeasureTrans;
    }

    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 