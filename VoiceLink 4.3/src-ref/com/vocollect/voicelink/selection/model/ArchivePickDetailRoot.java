/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Model object representing a VoiceLink Archive Selection Pick Details.
 * 
 * @author mlashinsky
 */
public class ArchivePickDetailRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = -8896492781744866764L;

    // Action that occured to generate detail record
    private PickDetailType type;

    // Operator that performed type
    private String operatorIdentifier;
    private String operatorName;

    // Time of type
    private Date pickTime;

    // Quantity of type
    private Integer quantityPicked = 0;

    // Variable weight of pickId (quantityPicked must equal 1)
    private Double variableWeight;

    // Container picks went in
    private ArchiveContainer container;

    // ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    // Serial Number
    private String serialNumber;

    // Lot Number
    private String            lotNumber;

    private ArchivePick pick;

    /**
     * 
     * Constructor.
     */
    public ArchivePickDetailRoot() {

    }

    /**
     * 
     * Constructor.
     * @param pd PickDetail used to construct ArchivePickDetail
     * @param archivePick - Archive pick the pick belongs to
     * @param containers - archive containers pick detail may be in
     */
    public ArchivePickDetailRoot(PickDetail pd, ArchivePick archivePick, 
                                 Set<ArchiveContainer> containers) {

        //Assign to pick
        this.setPick(archivePick);
        
        //If container is not null assign to container
        if (pd.getContainer() != null) {
            for (ArchiveContainer c : containers) {
                if (c.getContainerNumber()
                    .equals(pd.getContainer().getContainerNumber())) {
                    this.setContainer(c);
                    c.getPickDetails().add((ArchivePickDetail) this);
                    break;
                }
            }
        }
        if (pd.getOperator() != null) {
            this.setOperatorIdentifier(pd.getOperator().getOperatorIdentifier());
            this.setOperatorName(pd.getOperator().getName());
        } else {
            this.setOperatorIdentifier(" ");
            this.setOperatorName(" ");
        }
        this.setCreatedDate(new Date());
        this.setExportStatus(pd.getExportStatus());
        this.setPickTime(pd.getPickTime());
        this.setQuantityPicked(pd.getQuantityPicked());
        this.setSerialNumber(pd.getSerialNumber());
        this.setVariableWeight(pd.getVariableWeight());
        this.setLotNumber(pd.getLotNumber());
        this.setType(pd.getType());
    }

    
    /**
     * Getter for the pick property.
     * @return ArchivePick value of the property
     */
    public ArchivePick getPick() {
        return pick;
    }

    
    /**
     * Setter for the pick property.
     * @param pick the new pick value
     */
    public void setPick(ArchivePick pick) {
        this.pick = pick;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the type property.
     * @return Integer value of the property
     */
    public PickDetailType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PickDetailType type) {
        this.type = type;
    }

    /**
     * Getter for the pickTime property.
     * @return Date value of the property
     */
    public Date getPickTime() {
        return this.pickTime;
    }

    /**
     * Setter for the pickTime property.
     * @param pickTime the new pickTime value
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }

    /**
     * Getter for the quantityPicked property.
     * @return Integer value of the property
     */
    public Integer getQuantityPicked() {
        return this.quantityPicked;
    }

    /**
     * Setter for the quantityPicked property.
     * @param quantityPicked the new quantityPicked value
     */
    public void setQuantityPicked(Integer quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * Getter for the variableWeight property.
     * @return Double value of the property
     */
    public Double getVariableWeight() {
        return this.variableWeight;
    }

    /**
     * Setter for the variableWeight property.
     * @param variableWeight the new variableWeight value
     */
    public void setVariableWeight(Double variableWeight) {
        this.variableWeight = variableWeight;
    }

    /**
     * Getter for the serialNumber property.
     * @return String value of the property
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Setter for the serialNumber property.
     * @param serialNumber the new serialNumber value
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PickDetail)) {
            return false;
        }
        final PickDetail other = (PickDetail) obj;

        if (getPickTime() == null) {
            if (other.getPickTime() != null) {
                return false;
            }
        } else if (!getPickTime().equals(other.getPickTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((pickTime == null) ? 0 : pickTime.hashCode());
        return result;
    }

    
    /**
     * Getter for the container property.
     * @return ArchiveContainer value of the property
     */
    public ArchiveContainer getContainer() {
        return container;
    }

    
    /**
     * Setter for the container property.
     * @param container the new container value
     */
    public void setContainer(ArchiveContainer container) {
        this.container = container;
    }

    
    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    
    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    
    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    
    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    
    /**
     * Getter for the lotNumber property.
     * @return String value of the property
     */
    public String getLotNumber() {
        return lotNumber;
    }

    
    /**
     * Setter for the lotNumber property.
     * @param lotNumber the new lotNumber value
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 