/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.io.Serializable;


/**
 * 
 *
 * @author mkoenig
 */
public class WorkIdentifierRoot implements Serializable {

    private static final long serialVersionUID = -3898486640161152580L;

    //How work is requested in manual issuance, Imported from WMS. 
    //Usually assignment number, but can be any value. 
    private String            workIdentifierValue;
    
    //Last x characters of WorkIdentifier value. Calculated on import or when 
    //Number of work id digits operator speaks is changed in the Region. 
    //Field is indexed and was added for database lookup performance  
    private String            partialWorkIdentifier;

    
    /**
     * Getter for the partialWorkIdentifier property.
     * @return String value of the property
     */
    public String getPartialWorkIdentifier() {
        return this.partialWorkIdentifier;
    }

    
    /**
     * Setter for the partialWorkIdentifier property.
     * @param partialWorkIdentifier the new partialWorkIdentifier value
     */
    public void setPartialWorkIdentifier(String partialWorkIdentifier) {
        this.partialWorkIdentifier = partialWorkIdentifier;
    }

    
    /**
     * Getter for the workIdentifierValue property.
     * @return String value of the property
     */
    public String getWorkIdentifierValue() {
        return this.workIdentifierValue;
    }

    
    /**
     * Setter for the workIdentifierValue property.
     * @param workIdentifierValue the new workIdentifierValue value
     */
    public void setWorkIdentifierValue(String workIdentifierValue) {
        this.workIdentifierValue = workIdentifierValue;
    }

    
 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 