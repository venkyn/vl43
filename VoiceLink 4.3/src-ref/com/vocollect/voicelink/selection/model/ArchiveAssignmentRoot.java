/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.loading.model.LoadingRegion;

import static com.vocollect.voicelink.selection.model.AssignmentStatus.Available;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Model object representing a VoiceLink Archive Selection Assignment.
 * 
 * @author mlashinsky
 */
public class ArchiveAssignmentRoot extends ArchiveModelObject implements
    Serializable {

    private static final long serialVersionUID = 6480571401517467618L;

    // Assignment number from WMS, imported
    private Long number;

    // Populated for split assignments, numeric value from 1 to 99 depending
    // on how many times assignment was split. NULL if assignment not split
    private Long splitNumber;

    // Group information about group assignment is assigned to
    private AssignmentGroup groupInfo;

    // Route from WMS, Imported
    private String route;

    // Deleivery Date from WMS, Imported
    private Date deliveryDate;

    // Delivery Location, This can be Imported or set using delivery location
    // mapping functionality
    private String deliveryLocation;

    // Operator picked assignment
    private String operatorIdentifier;
    private String operatorName;
    
    // Customer information
    private Customer customerInfo;

    // Region assignment belongs in, (Region Number imported and mapped to
    // regionID)
    private SelectionRegion region;

    // Work Identifier value
    private WorkIdentifier workIdentifier;

    // Summary information about assignment
    private AssignmentSummary summaryInfo;

    // Type of assignmet
    private AssignmentType type = AssignmentType.Normal;

    // Status of assignment
    // Default is Available
    private AssignmentStatus status = Available;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    // Count of containers for Display Purposes
    private int containerCount = 0;

    // Count of pick detail information available
    private int pickDetailCount = 0;

    // Viewable assignment number
    private String viewableAssignmentNumber;

    // Site to which the Assignment belongs
    private Site site;

    // Picks belonging to this assignment
    private List<ArchivePick> picks = new ArrayList<ArchivePick>();

    // labor records belonging to this assignment (Added for archive purposes)
    private List<ArchiveAssignmentLabor> labor = new ArrayList<ArchiveAssignmentLabor>();

    // Archive Containers that this Archive Assignment was put into
    private Set<ArchiveContainer> containers = new HashSet<ArchiveContainer>();

    //Loading Region of the Route
    private LoadingRegion loadingRegion; 
    
    //Loading Route departure date time
    private Date departureDateTime;
    
    /**
     * 
     * Constructor.
     */
    public ArchiveAssignmentRoot() {

    }

    /**
     * 
     * Constructor.
     * @param assignment Assignment to use to build ArchiveAssignment.
     */
    public ArchiveAssignmentRoot(Assignment assignment) {
        if (assignment != null) {
            
            for (AssignmentLabor al : assignment.getLabor()) {
                getLabor().add(new ArchiveAssignmentLabor(al, 
                    (ArchiveAssignment) this));
            }

            for (Container c : assignment.getContainers()) {
                getContainers().add(new ArchiveContainer(c, 
                    (ArchiveAssignment) this));
            }

            for (Pick p : assignment.getPicks()) {
                getPicks().add(new ArchivePick(p, (ArchiveAssignment) this, getContainers()));
            }

            for (Tag t : assignment.getTags()) {
                if (t.getTagType().equals(Tag.SITE)) {
                    this.setSite(getSiteMap().get(t.getTaggableId()));
                    break;
                }
            }
            if (assignment.getOperator() != null) {
                this.setOperatorIdentifier(assignment.getOperator().getOperatorIdentifier());
                this.setOperatorName(assignment.getOperator().getName());
            } else {
                this.setOperatorIdentifier(" ");
                this.setOperatorName(" ");
            } 
            this.setContainerCount(assignment.getContainerCount());
            this.setCreatedDate(new Date());
            this.setCustomerInfo(assignment.getCustomerInfo());
            this.setDeliveryDate(assignment.getDeliveryDate());
            this.setDeliveryLocation(assignment.getDeliveryLocation());
            this.setExportStatus(assignment.getExportStatus());
            this.setGroupInfo(assignment.getGroupInfo());
            this.setNumber(assignment.getNumber());
            this.setPickDetailCount(assignment.getPickDetailCount());
            this.setRegion(assignment.getRegion());
            this.setRoute(assignment.getRoute());
            this.setSplitNumber(assignment.getSplitNumber());
            this.setStatus(assignment.getStatus());
            this.setSummaryInfo(assignment.getSummaryInfo());
            this.setType(assignment.getType());
            this.setViewableAssignmentNumber(assignment
                .getViewableAssignmentNumber());
            this.setWorkIdentifier(assignment.getWorkIdentifier());
            this.setLoadingRegion(assignment.getLoadingRegion());
            this.setDepartureDateTime(assignment.getDepartureDateTime());
        }
    }

    /**
     * Get the viewable assignment number combine with split number if it was
     * split.
     * 
     * @return - viewable assignment number
     */
    public String getViewableAssignmentNumber() {
        if (viewableAssignmentNumber != null) {
            if (splitNumber != null) {
                return new BigDecimal(viewableAssignmentNumber).setScale(
                    2, BigDecimal.ROUND_HALF_UP).toPlainString();
            } else {
                return new BigDecimal(viewableAssignmentNumber).setScale(
                    0, BigDecimal.ROUND_HALF_UP).toPlainString();
            }
        }
        return viewableAssignmentNumber;
    }

    /**
     * Set the viewable assignment number.
     * 
     * @param viewableAssignmentNumber Viewable Assignment Number
     */
    public void setViewableAssignmentNumber(String viewableAssignmentNumber) {
        this.viewableAssignmentNumber = viewableAssignmentNumber;
    }

    /**
     * Getter for the containerCount property.
     * @return int value of the property
     */
    public int getContainerCount() {
        return containerCount;
    }

    /**
     * Setter for the containerCount property.
     * @param containerCount the new containerCount value
     */
    public void setContainerCount(int containerCount) {
        this.containerCount = containerCount;
    }

    /**
     * Getter for the pickDetailCount property.
     * @return int value of the property
     */
    public int getPickDetailCount() {
        return pickDetailCount;
    }

    /**
     * Setter for the pickDetailCount property.
     * @param pickDetailCount the new pickDetailCount value
     */
    public void setPickDetailCount(int pickDetailCount) {
        this.pickDetailCount = pickDetailCount;
    }

    /**
     * Getter for the customerInfo property.
     * @return Customer value of the property
     */
    public Customer getCustomerInfo() {
        if (this.customerInfo == null) {
            setCustomerInfo(new Customer());
        }
        return this.customerInfo;
    }

    /**
     * Setter for the customerInfo property.
     * @param customerInfo the new customerInfo value
     */
    public void setCustomerInfo(Customer customerInfo) {
        this.customerInfo = customerInfo;
    }

    /**
     * Getter for the summaryInfo property.
     * @return AssignmentSummary value of the property
     */
    public AssignmentSummary getSummaryInfo() {
        if (this.summaryInfo == null) {
            setSummaryInfo(new AssignmentSummary());
        }
        return this.summaryInfo;
    }

    /**
     * Setter for the summaryInfo property.
     * @param summaryInfo the new summaryInfo value
     */
    public void setSummaryInfo(AssignmentSummary summaryInfo) {
        this.summaryInfo = summaryInfo;
    }

    /**
     * Getter for the workIdentifier property.
     * @return WorkIdentifier value of the property
     */
    public WorkIdentifier getWorkIdentifier() {
        if (this.workIdentifier == null) {
            setWorkIdentifier(new WorkIdentifier());
        }
        return this.workIdentifier;
    }

    /**
     * Setter for the workIdentifier property.
     * @param workIdentifier the new workIdentifier value
     */
    public void setWorkIdentifier(WorkIdentifier workIdentifier) {
        this.workIdentifier = workIdentifier;
    }

    /**
     * Getter for the deliveryDate property.
     * @return Date value of the property
     */
    public Date getDeliveryDate() {
        return this.deliveryDate;
    }

    /**
     * Setter for the deliveryDate property.
     * @param deliveryDate the new deliveryDate value
     */
    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /**
     * Getter for the deliveryLocation property.
     * @return String value of the property
     */
    public String getDeliveryLocation() {
        return this.deliveryLocation;
    }

    /**
     * Setter for the deliveryLocation property.
     * @param deliveryLocation the new deliveryLocation value
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the number property.
     * @return long value of the property
     */
    public Long getNumber() {
        return this.number;
    }

    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(Long number) {
        this.number = number;
    }

    
    /**
     * Getter for the picks property.
     * @return List&lt;ArchivePick&gt; value of the property
     */
    public List<ArchivePick> getPicks() {
        return picks;
    }

    
    /**
     * Setter for the picks property.
     * @param picks the new picks value
     */
    public void setPicks(List<ArchivePick> picks) {
        this.picks = picks;
    }

    /**
     * Getter for the region property.
     * @return SelectionRegion value of the property
     */
    public SelectionRegion getRegion() {
        return this.region;
    }

    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(SelectionRegion region) {
        this.region = region;
    }

    /**
     * Getter for the route property.
     * @return String value of the property
     */
    public String getRoute() {
        return this.route;
    }

    /**
     * Setter for the route property.
     * @param route the new route value
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Getter for the splitNumber property.
     * @return Long value of the property
     */
    public Long getSplitNumber() {
        return this.splitNumber;
    }

    /**
     * Setter for the splitNumber property.
     * @param splitNumber the new splitNumber value
     */
    public void setSplitNumber(Long splitNumber) {
        this.splitNumber = splitNumber;
    }

    /**
     * Getter for the status property.
     * @return AssignmentStatus value of the property
     */
    public AssignmentStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(AssignmentStatus status) {
        this.status = status;
    }

    /**
     * Getter for the type property.
     * @return AssignmentType value of the property
     */
    public AssignmentType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(AssignmentType type) {
        this.type = type;
    }

    
    /**
     * Getter for the loadingRegion property.
     * @return LoadingRegion value of the property
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }

    
    /**
     * Setter for the loadingRegion property.
     * @param loadingRegion the new loadingRegion value
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }

    
    
    /**
     * Getter for the departureDateTime property.
     * @return Date value of the property
     */
    public Date getDepartureDateTime() {
        return departureDateTime;
    }

    
    /**
     * Setter for the departureDateTime property.
     * @param departureDateTime the new departureDateTime value
     */
    public void setDepartureDateTime(Date departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Assignment)) {
            return false;
        }
        final Assignment other = (Assignment) obj;
        if (getNumber() != other.getNumber()) {
            return false;
        }
        if (getSplitNumber() == null) {
            if (other.getSplitNumber() != null) {
                return false;
            }
        } else if (!getSplitNumber().equals(other.getSplitNumber())) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        final int power = 32;
        int result = 1;
        result = (prime * result) + (int) (number ^ (number >>> power));
        result = (prime * result)
            + ((splitNumber == null) ? 0 : splitNumber.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return Long.toString(this.getNumber());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#toString()
     */
    @Override
    public String toString() {
        return this.getDescriptiveText();
    }

    /**
     * Getter for the groupInfo property.
     * @return AssignmentGroup value of the property
     */
    public AssignmentGroup getGroupInfo() {
        if (this.groupInfo == null) {
            setGroupInfo(new AssignmentGroup());
        }
        return this.groupInfo;
    }

    /**
     * Setter for the groupInfo property.
     * @param groupInfo the new groupInfo value
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        this.groupInfo = groupInfo;
    }


    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return site;
    }

    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Getter for the labor property.
     * @return List&lt;AssignmentLabor&gt; value of the property
     */
    public List<ArchiveAssignmentLabor> getLabor() {
        return labor;
    }

    /**
     * Setter for the labor property.
     * @param labor the new labor value
     */
    public void setLabor(List<ArchiveAssignmentLabor> labor) {
        this.labor = labor;
    }

    /**
     * Getter for the containers property.
     * @return Set&lt;ArchiveContainer&gt; value of the property
     */
    public Set<ArchiveContainer> getContainers() {
        return containers;
    }

    /**
     * Setter for the containers property.
     * @param containers the new containers value
     */
    public void setContainers(Set<ArchiveContainer> containers) {
        this.containers = containers;
    }

    
    /**
     * Getter for the operatorName property.
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }

    
    /**
     * Setter for the operatorName property.
     * @param operatorName the new operatorName value
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    
    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    
    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 