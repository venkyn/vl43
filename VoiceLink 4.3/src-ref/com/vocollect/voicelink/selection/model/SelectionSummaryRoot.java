/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.io.Serializable;
import java.util.Set;


/**
 * Summary Model selection.
 *
 * @author mkoenig
 */
public class SelectionSummaryRoot extends BaseModelObject implements
    Serializable, Taggable {

    //
    private static final long serialVersionUID = 6911731003170392525L;

    private SelectionRegion         region;

    private Long                    id;

    private int                     totalAssignments;

    private int                     inProgress;

    private int                     available;

    private int                     complete;

    private int                     nonComplete;

    private int                     operatorsWorkingIn;

    private int                     operatorsAssigned;

    private int                     totalItemsPicked;

    private int                     totalItemsRemaining;

    private int                     totalShorts;

    private int                     shorted;

    private int                     assigned;

    private int                     markedout;

    private Double                  estimatedCompleted;

    private Site                    site = null; // cache the site to improve performance


    /**
     * Getter for the estimatedCompleted property.
     * @return Double value of the property
     */
    public Double getEstimatedCompleted() {
        return estimatedCompleted;
    }


    /**
     * Setter for the estimatedCompleted property.
     * @param estimatedCompleted the new estimatedCompleted value
     */
    public void setEstimatedCompleted(Double estimatedCompleted) {
        this.estimatedCompleted = estimatedCompleted;
    }

    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return region.getTags();
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        //do nothing. Tags are derived from the region
    }


    /**
     * Getter for the assigned property.
     * @return int value of the property
     */
    public int getAssigned() {
        return assigned;
    }



    /**
     * Setter for the assigned property.
     * @param assigned the new assigned value
     */
    public void setAssigned(int assigned) {
        this.assigned = assigned;
    }


    /**
     * Getter for the markedout property.
     * @return int value of the property
     */
    public int getMarkedout() {
        return markedout;
    }



    /**
     * Setter for the markedout property.
     * @param markedout the new markedout value
     */
    public void setMarkedout(int markedout) {
        this.markedout = markedout;
    }



    /**
     * Getter for the operatorsAssigned property.
     * @return int value of the property
     */
    public int getOperatorsAssigned() {
        return operatorsAssigned;
    }



    /**
     * Setter for the operatorsAssigned property.
     * @param operatorsAssigned the new operatorsAssigned value
     */
    public void setOperatorsAssigned(int operatorsAssigned) {
        this.operatorsAssigned = operatorsAssigned;
    }



    /**
     * Getter for the operatorsWorkingIn property.
     * @return int value of the property
     */
    public int getOperatorsWorkingIn() {
        return operatorsWorkingIn;
    }



    /**
     * Setter for the operatorsWorkingIn property.
     * @param operatorsWorkingIn the new operatorsWorkingIn value
     */
    public void setOperatorsWorkingIn(int operatorsWorkingIn) {
        this.operatorsWorkingIn = operatorsWorkingIn;
    }



    /**
     * Getter for the shorted property.
     * @return int value of the property
     */
    public int getShorted() {
        return shorted;
    }



    /**
     * Setter for the shorted property.
     * @param shorted the new shorted value
     */
    public void setShorted(int shorted) {
        this.shorted = shorted;
    }



    /**
     * Getter for the totalAssignments property.
     * @return int value of the property
     */
    public int getTotalAssignments() {
        return totalAssignments;
    }



    /**
     * Setter for the totalAssignments property.
     * @param totalAssignments the new totalAssignments value
     */
    public void setTotalAssignments(int totalAssignments) {
        this.totalAssignments = totalAssignments;
    }



    /**
     * Getter for the totalItemsPicked property.
     * @return int value of the property
     */
    public int getTotalItemsPicked() {
        return totalItemsPicked;
    }



    /**
     * Setter for the totalItemsPicked property.
     * @param totalItemsPicked the new totalItemsPicked value
     */
    public void setTotalItemsPicked(int totalItemsPicked) {
        this.totalItemsPicked = totalItemsPicked;
    }



    /**
     * Getter for the totalItemsRemaining property.
     * @return int value of the property
     */
    public int getTotalItemsRemaining() {
        return totalItemsRemaining;
    }



    /**
     * Setter for the totalItemsRemaining property.
     * @param totalItemsRemaining the new totalItemsRemaining value
     */
    public void setTotalItemsRemaining(int totalItemsRemaining) {
        this.totalItemsRemaining = totalItemsRemaining;
    }



    /**
     * Getter for the totalShorts property.
     * @return int value of the property
     */
    public int getTotalShorts() {
        return totalShorts;
    }



    /**
     * Setter for the totalShorts property.
     * @param totalShorts the new totalShorts value
     */
    public void setTotalShorts(int totalShorts) {
        this.totalShorts = totalShorts;
    }


    /**
     * Getter for the available property.
     * @return int value of the property
     */
    public int getAvailable() {
        return available;
    }


    /**
     * Setter for the available property.
     * @param available the new available value
     */
    public void setAvailable(int available) {
        this.available = available;
    }


    /**
     * Getter for the complete property.
     * @return int value of the property
     */
    public int getComplete() {
        return complete;
    }


    /**
     * Setter for the complete property.
     * @param complete the new complete value
     */
    public void setComplete(int complete) {
        this.complete = complete;
    }


    /**
     * Getter for the inProgress property.
     * @return int value of the property
     */
    public int getInProgress() {
        return inProgress;
    }


    /**
     * Setter for the inProgress property.
     * @param inProgress the new inProgress value
     */
    public void setInProgress(int inProgress) {
        this.inProgress = inProgress;
    }


    /**
     * Getter for the nonComplete property.
     * @return int value of the property
     */
    public int getNonComplete() {
        return nonComplete;
    }


    /**
     * Setter for the nonComplete property.
     * @param nonComplete the new nonComplete value
     */
    public void setNonComplete(int nonComplete) {
        this.nonComplete = nonComplete;
    }


    /**
     * Getter for the region property.
     * @return SelectionRegion value of the property
     */
    public SelectionRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(SelectionRegion region) {
        this.region = region;
    }


    /**
     * Getter for the site property.
     * @return Site value of the property
     * @throws DataAccessException - Database exceptions
     */
    public Site getSite() throws DataAccessException {
        if (this.site == null) {
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            this.site = siteContext.getSite(region);
        }
        return this.site;
    }

    /**
     * Setter for the site property.
     * @param newSite the new site
     */
    public void setSite(Site newSite) {
        this.site = newSite;
    }


    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    @Override
    public Long getId() {
        return id;
    }


    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj instanceof SelectionSummaryRoot) {
            return false;
        }
        final SelectionSummaryRoot other = (SelectionSummaryRoot) obj;
        if (region == null) {
            if (other.region != null) {
                return false;
            }
        } else if (!region.equals(other.region)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((region == null) ? 0 : region.hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 