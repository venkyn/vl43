/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model object representing a VoiceLink Archive Selection Containers.
 * 
 * @author mlashinsky
 */
public class ArchiveContainerRoot extends ArchiveModelObject implements
    Serializable {

    //
    private static final long serialVersionUID = -3657499873087189673L;

    // Type Of Container
    private ContainerType containerType;

    // container number supplied by task or system generated
    private String containerNumber;

    // Targe Container indicator for picks going into this container
    private Integer targetContainerIndicator;

    // Has container been exportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    // Status
    private ContainerStatus status = ContainerStatus.Available;

    // has container label previously been printed, used to determine if
    // container
    // should be printed during reprint label command
    private int printed = 0;

    // Site to which Containers
    private Site site;
    
    // archive Assignment
    private ArchiveAssignment assignment;

    //Pick Detail records put into this container
    private List<ArchivePickDetail>   pickDetails = 
        new ArrayList<ArchivePickDetail>();
    

    /**
     * 
     * Constructor.
     */
    public ArchiveContainerRoot() {

    }

    /**
     * 
     * Constructor.
     * @param container Container to use to construct ArchiveContainer
     * @param assignment - archive assignment container belongs to
     */
    public ArchiveContainerRoot(Container container, 
                                ArchiveAssignment assignment) {

        for (Tag t : container.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }
        this.setAssignment(assignment);
        this.setContainerNumber(container.getContainerNumber());
        this.setContainerType(container.getContainerType());
        this.setCreatedDate(new Date());
        this.setExportStatus(container.getExportStatus());
        this.setPrinted(container.getPrinted());
        this.setStatus(container.getStatus());
        this.setTargetContainerIndicator(container
            .getTargetContainerIndicator());
    }

    /**
     * Getter for the targetContainerIndicator property.
     * @return Integer value of the property
     */
    public Integer getTargetContainerIndicator() {
        return targetContainerIndicator;
    }

    /**
     * Setter for the targetContainerIndicator property.
     * @param targetContainerIndicator the new targetContainerIndicator value
     */
    public void setTargetContainerIndicator(Integer targetContainerIndicator) {
        this.targetContainerIndicator = targetContainerIndicator;
    }

    /**
     * Getter for the containerNumber property.
     * @return String value of the property
     */
    public String getContainerNumber() {
        return this.containerNumber;
    }

    /**
     * Setter for the containerNumber property.
     * @param containerNumber the new containerNumber value
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    /**
     * Getter for the containerType property.
     * @return ContainerType value of the property
     */
    public ContainerType getContainerType() {
        return this.containerType;
    }

    /**
     * Setter for the containerType property.
     * @param containerType the new containerType value
     */
    public void setContainerType(ContainerType containerType) {
        this.containerType = containerType;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the printed property.
     * @return int value of the property
     */
    public int getPrinted() {
        return this.printed;
    }

    /**
     * Setter for the printed property.
     * @param printed the new printed value
     */
    public void setPrinted(int printed) {
        this.printed = printed;
    }

    /**
     * Getter for the status property.
     * @return ContainerStatus value of the property
     */
    public ContainerStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(ContainerStatus status) {
        this.status = status;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Container)) {
            return false;
        }
        final Container other = (Container) obj;
        if (getContainerNumber() == null) {
            if (other.getContainerNumber() != null) {
                return false;
            }
        } else if (!getContainerNumber().equals(other.getContainerNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((containerNumber == null) ? 0 : containerNumber.hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getContainerNumber();
    }
       
    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return site;
    }

    
    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    
    /**
     * Getter for the assignment property.
     * @return ArchiveAssignment value of the property
     */
    public ArchiveAssignment getAssignment() {
        return assignment;
    }

    
    /**
     * Setter for the assignment property.
     * @param assignment the new assignment value
     */
    public void setAssignment(ArchiveAssignment assignment) {
        this.assignment = assignment;
    }

    
    /**
     * Getter for the pickDetails property.
     * @return List&lt;ArchivePickDetail&gt; value of the property
     */
    public List<ArchivePickDetail> getPickDetails() {
        return pickDetails;
    }

    
    /**
     * Setter for the pickDetails property.
     * @param pickDetails the new pickDetails value
     */
    public void setPickDetails(List<ArchivePickDetail> pickDetails) {
        this.pickDetails = pickDetails;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 