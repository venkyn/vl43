/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Map;

import ognl.DefaultTypeConverter;


/**
 * Converts a String to a SummaryPromptCharactersToSpeak Enum and vice versa.
 *
 * @author mnichols
 */
public class SummaryPromptCharactersToSpeakConverter extends DefaultTypeConverter {
    
    /**
     * Converts the given object to a given type. 
     * @param ctx - OGNL context under which the conversion is being done
     * @param o - the object to be converted
     * @param toType - the class that contains the code to convert to enumeration
     * @return Converted value of type declared in toClass 
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object convertValue(Map ctx, Object o, Class toType) {
        if (toType == SummaryPromptCharactersToSpeak.class) {
            if (o instanceof String[]) {
                return SummaryPromptCharactersToSpeak.toEnum(Integer.parseInt(((String[]) o)[0]));
            } else {
                return SummaryPromptCharactersToSpeak.toEnum(Integer.parseInt(o.toString()));
            }
        } else if (toType == String.class) {
            if (o instanceof String[]) {
                return Enum.valueOf(SummaryPromptCharactersToSpeak.class, ((String[]) o)[0]);
            } else if (o instanceof String) {
                return Enum.valueOf(SummaryPromptCharactersToSpeak.class, ((String) o));
            }
        }
        return super.convertValue(ctx, o, toType);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 