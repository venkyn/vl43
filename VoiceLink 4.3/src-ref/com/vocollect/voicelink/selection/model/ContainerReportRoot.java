/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Date;

/**
 * Model object representing a VoiceLink Selection container report.
 *
 * @author mnichols
 */
public class ContainerReportRoot {
    private String containerNumber;
    private ContainerStatus containerStatus;
    private Integer quantityPicked;
    private String itemDescription;
    private String itemNumber;
    private String operatorName;
    private String operatorId;
    private Date pickTime;
    private String aisle;
    private String postAisle;
    private String preAisle;
    private String slot;
    private Long pickId;
    private Long pickDetailId;
    private PickStatus pickStatus;
    private String serialNumber;



    /**
     * Constructor.
     * @param containerNumber - the container number
     * @param containerStatus - the container status
     * @param quantityPicked - the quantity picked
     * @param itemDescription - the item description
     * @param itemNumber - the item number
     * @param operatorName - the operator number
     * @param operatorId - the operator id
     * @param pickTime - the pick time
     * @param aisle - the aisle
     * @param postAisle - the post aisle
     * @param preAisle - the pre aisle
     * @param slot - the slot
     * @param pickId - the pick id
     * @param pickDetailId - the pick detail id
     * @param pickStatus - the pick status
     * @param serialNumber - the serial number
     * @param lotNumber - the lot number
     * @param variableWeight - the variable weight
     */
    public ContainerReportRoot(String containerNumber,
                               ContainerStatus containerStatus,
                               Integer quantityPicked,
                               String itemDescription,
                               String itemNumber,
                               String operatorName,
                               String operatorId,
                               Date pickTime,
                               String aisle,
                               String postAisle,
                               String preAisle,
                               String slot,
                               Long pickId,
                               Long pickDetailId,
                               PickStatus pickStatus,
                               String serialNumber,
                               String lotNumber,
                               Double variableWeight) {
        super();
        this.containerNumber = containerNumber;
        this.containerStatus = containerStatus;
        this.quantityPicked = quantityPicked;
        this.itemDescription = itemDescription;
        this.itemNumber = itemNumber;
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.pickTime = pickTime;
        this.aisle = aisle;
        this.postAisle = postAisle;
        this.preAisle = preAisle;
        this.slot = slot;
        this.pickId = pickId;
        this.pickDetailId = pickDetailId;
        this.pickStatus = pickStatus;
        this.serialNumber = serialNumber;
        this.lotNumber = lotNumber;
        this.variableWeight = variableWeight;
    }



    /**
     * getter for containerNumber property
     * @return String value of the property
     */
    public String getContainerNumber() {
        return containerNumber;
    }



    /**
     * setter for containerNumber property
     * @param containerNumber for setting the container number
     */
    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }


    /**
     * getter for containerStatus property
     * @return String value of the property
     */
    public ContainerStatus getContainerStatus() {
        return containerStatus;
    }


    /**
     * setter for containerStatus property
     * @param containerStatus for setting the container status
     */
    public void setContainerStatus(ContainerStatus containerStatus) {
        this.containerStatus = containerStatus;
    }


    /**
     * getter for quantityPicked property
     * @return String value of the property
     */
    public Integer getQuantityPicked() {
        return quantityPicked;
    }



    /**
     * setter for quantityPicked property
     * @param quantityPicked for setting the quantity picked
     */
    public void setQuantityPicked(Integer quantityPicked) {
        this.quantityPicked = quantityPicked;
    }


    /**
     * getter for itemDescription property
     * @return String value of the property
     */
    public String getItemDescription() {
        return itemDescription;
    }


    /**
     * setter for itemDescription property
     * @param itemDescription for setting the item description
     */
    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }


    /**
     * getter for itemNumber property
     * @return String value of the property
     */
    public String getItemNumber() {
        return itemNumber;
    }


    /**
     * setter for itemNumber property
     * @param itemNumber for setting the item number
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }


    /**
     * getter for operatorName property
     * @return String value of the property
     */
    public String getOperatorName() {
        return operatorName;
    }


    /**
     * setter for operatorName property
     * @param operatorName for setting the operator name
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }


    /**
     * getter for operatorId property
     * @return String value of the property
     */
    public String getOperatorId() {
        return operatorId;
    }


    /**
     * setter for operatorId property
     * @param operatorId for setting the operator id
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }


    /**
     * getter for pickTime property
     * @return String value of the property
     */
    public Date getPickTime() {
        return pickTime;
    }


    /**
     * setter for pickTime property
     * @param pickTime for setting the time of pick
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }


    /**
     * getter for aisle property
     * @return String value of the property
     */
    public String getAisle() {
        return aisle;
    }


    /**
     * setter for aisle property
     * @param aisle for setting the aisle
     */
    public void setAisle(String aisle) {
        this.aisle = aisle;
    }


    /**
     * getter for postAisle property
     * @return String value of the property
     */
    public String getPostAisle() {
        return postAisle;
    }


    /**
     * setter for postAisle property
     * @param postAisle for setting the post aisle
     */
    public void setPostAisle(String postAisle) {
        this.postAisle = postAisle;
    }


    /**
     * getter for preAisle property
     * @return String value of the property
     */
    public String getPreAisle() {
        return preAisle;
    }


    /**
     * setter for preAisle property
     * @param preAisle for setting the pre aisle
     */
    public void setPreAisle(String preAisle) {
        this.preAisle = preAisle;
    }


    /**
     * getter for slot property
     * @return String value of the property
     */
    public String getSlot() {
        return slot;
    }


    /**
     * setter for slot property
     * @param slot for setting the slot
     */
    public void setSlot(String slot) {
        this.slot = slot;
    }


    /**
     * getter for pickId property
     * @return String value of the property
     */
    public Long getPickId() {
        return pickId;
    }


    /**
     * setter for pickId property
     * @param pickId for setting the pick id
     */
    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }


    /**
     * getter for pickDetailId property
     * @return String value of the property
     */
    public Long getPickDetailId() {
        return pickDetailId;
    }


    /**
     * setter for pickDetailId property
     * @param pickDetailId for setting the pick detail id
     */
    public void setPickDetailId(Long pickDetailId) {
        this.pickDetailId = pickDetailId;
    }


    /**
     * getter for pickStatus property
     * @return String value of the property
     */
    public PickStatus getPickStatus() {
        return pickStatus;
    }


    /**
     * setter for pickStatus property
     * @param pickStatus for setting the status of the pick
     */
    public void setPickStatus(PickStatus pickStatus) {
        this.pickStatus = pickStatus;
    }


    /**
     * getter for serialNumber property
     * @return String value of the property
     */
    public String getSerialNumber() {
        return serialNumber;
    }


    /**
     * setter for serialNumber property
     * @param serialNumber for setting the serial number
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }


    /**
     * getter for lotNumber property
     * @return String value of the property
     */
    public String getLotNumber() {
        return lotNumber;
    }


    /**
     * setter for lotNumber property
     * @param lotNumber for setting the lot number
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }


    /**
     * getter for variableWeight property
     * @return String value of the property
     */
    public Double getVariableWeight() {
        return variableWeight;
    }


    /**
     * setter for variableWeight property
     * @param variableWeight for setting the variable weight
     */
    public void setVariableWeight(Double variableWeight) {
        this.variableWeight = variableWeight;
    }

    private String lotNumber;
    private Double variableWeight;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 