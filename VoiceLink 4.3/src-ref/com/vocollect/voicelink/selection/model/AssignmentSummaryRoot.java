/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.io.Serializable;


/**
 * 
 *
 * @author mkoenig
 */
public class AssignmentSummaryRoot implements Serializable  {
    
    private static final long serialVersionUID = 4623154899985781339L;

    //Total Quantity to pick, calculated at import or when assignment is split
    private int               totalItemQuantity = 0;

    //Total weight of assignment, calculated at import or when assignment is split
    private Double            totalWeight;

    //Total cube of assignment, calculated at import or when assignment is split
    private Double            totalCube;

    //Expected amount of time assignment should take. Imported or manually 
    //entered when assignment is split (or chase assignment created)
    private double            goalTime = 0;

    
    /**
     * Getter for the goalTime property.
     * @return double value of the property
     */
    public double getGoalTime() {
        return this.goalTime;
    }

    
    /**
     * Setter for the goalTime property.
     * @param goalTime the new goalTime value
     */
    public void setGoalTime(double goalTime) {
        this.goalTime = goalTime;
    }

    
    /**
     * Getter for the totalCube property.
     * @return Double value of the property
     */
    public Double getTotalCube() {
        return this.totalCube;
    }

    
    /**
     * Setter for the totalCube property.
     * @param totalCube the new totalCube value
     */
    public void setTotalCube(Double totalCube) {
        this.totalCube = totalCube;
    }

    
    /**
     * Getter for the totalItemQuantity property.
     * @return int value of the property
     */
    public int getTotalItemQuantity() {
        return this.totalItemQuantity;
    }

    
    /**
     * Setter for the totalItemQuantity property.
     * @param totalItemQuantity the new totalItemQuantity value
     */
    public void setTotalItemQuantity(int totalItemQuantity) {
        this.totalItemQuantity = totalItemQuantity;
    }

    
    /**
     * Getter for the totalWeight property.
     * @return Double value of the property
     */
    public Double getTotalWeight() {
        return this.totalWeight;
    }

    
    /**
     * Setter for the totalWeight property.
     * @param totalWeight the new totalWeight value
     */
    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 