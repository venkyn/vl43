package com.vocollect.voicelink.selection.model;

import java.util.Date;

public class SelectionAssignmentLaborReportRoot {


    private String operatorName;
    
    private String operatorId;
    
    private Date endTime;
    
    private Date startTime;
    
    private Integer proRateCount;
    
    private Double percentOfGoal;
    
    private Integer quantityPicked;
    
    private Long groupNumber;
    
    private Integer groupCount;
    
    private Integer groupProRateCount;
    
    private String assignmentNumber;

    /**
     * @param operatorName
     * @param operatorId
     * @param endTime
     * @param startTime
     * @param proRateCount
     * @param percentOfGoal
     * @param quantityPicked
     * @param groupNumber
     * @param groupCount
     * @param groupProRateCount
     * @param assignmentNumber
     */
    public SelectionAssignmentLaborReportRoot(
            String operatorName,
            String operatorId, 
            Date endTime, 
            Date startTime,
            Integer proRateCount, 
            Double percentOfGoal, 
            Integer quantityPicked,
            Long groupNumber, 
            Integer groupCount, 
            Integer groupProRateCount,
            String assignmentNumber) {
        super();
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.endTime = endTime;
        this.startTime = startTime;
        this.proRateCount = proRateCount;
        this.percentOfGoal = percentOfGoal;
        this.quantityPicked = quantityPicked;
        this.groupNumber = groupNumber;
        this.groupCount = groupCount;
        this.groupProRateCount = groupProRateCount;
        this.assignmentNumber = assignmentNumber;
    }

    public SelectionAssignmentLaborReportRoot(
            String operatorName,
            String operatorId, 
            Date endTime, 
            Date startTime,
            Integer proRateCount, 
            Double percentOfGoal, 
            Integer quantityPicked,
            Long groupNumber) {
        super();
        this.operatorName = operatorName;
        this.operatorId = operatorId;
        this.endTime = endTime;
        this.startTime = startTime;
        this.proRateCount = proRateCount;
        this.percentOfGoal = percentOfGoal;
        this.quantityPicked = quantityPicked;
        this.groupNumber = groupNumber;
    }
    
    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the operatorId
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * @param operatorId the operatorId to set
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the proRateCount
     */
    public Integer getProRateCount() {
        return proRateCount;
    }

    /**
     * @param proRateCount the proRateCount to set
     */
    public void setProRateCount(Integer proRateCount) {
        this.proRateCount = proRateCount;
    }

    /**
     * @return the percentOfGoal
     */
    public Double getPercentOfGoal() {
        return percentOfGoal;
    }

    /**
     * @param percentOfGoal the percentOfGoal to set
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

    /**
     * @return the quantityPicked
     */
    public Integer getQuantityPicked() {
        return quantityPicked;
    }

    /**
     * @param quantityPicked the quantityPicked to set
     */
    public void setQuantityPicked(Integer quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * @return the groupNumber
     */
    public Long getGroupNumber() {
        return groupNumber;
    }

    /**
     * @param groupNumber the groupNumber to set
     */
    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    /**
     * @return the groupCount
     */
    public Integer getGroupCount() {
        return groupCount;
    }

    /**
     * @param groupCount the groupCount to set
     */
    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }

    /**
     * @return the groupProRateCount
     */
    public Integer getGroupProRateCount() {
        return groupProRateCount;
    }

    /**
     * @param groupProRateCount the groupProRateCount to set
     */
    public void setGroupProRateCount(Integer groupProRateCount) {
        this.groupProRateCount = groupProRateCount;
    }

    /**
     * @return the assignmentNumber
     */
    public String getAssignmentNumber() {
        return assignmentNumber;
    }

    /**
     * @param assignmentNumber the assignmentNumber to set
     */
    public void setAssignmentNumber(String assignmentNumber) {
        this.assignmentNumber = assignmentNumber;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 