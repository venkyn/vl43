/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 *
 * @author Administrator
 */
public class PickDetailRoot extends CommonModelObject implements Serializable {

    //
    private static final long serialVersionUID = -8896492781744866764L;

    // WMS Pick the details is associated with
    private Pick              pick;

    // Action that occured to generate detail record
    private PickDetailType    type;

    // Operator that performed type
    private Operator          operator;
    
    // Time of type
    private Date              pickTime;

    // Quantity of type
    private Integer           quantityPicked = 0;

    // Variable weight of pick (quantityPicked must equal 1)
    private Double            variableWeight;
    
    // Container picks went in
    private Container         container;
    
    // ExportStatus
    private ExportStatus      exportStatus = ExportStatus.NotExported;
    
    // Serial Number
    private String            serialNumber;
    
    // Lot Number
    private String            lotNumber;
    
    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return this.exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exported the new exportStatus value
     */
    public void setExportStatus(ExportStatus exported) {
        this.exportStatus = exported;
    }

    /**
     * Getter for the type property.
     * @return Integer value of the property
     */
    public PickDetailType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PickDetailType type) {
        this.type = type;
    }

    /**
     * Getter for the container property.
     * @return container value of the property
     */
    public Container getContainer() {
        return this.container;
    }

    /**
     * Setter for the container property.
     * @param container the new container value
     */
    public void setContainer(Container container) {
        this.container = container;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the pick property.
     * @return Pick value of the property
     */
    public Pick getPick() {
        return this.pick;
    }

    /**
     * Setter for the pick property.
     * @param pick the new pick value
     */
    public void setPick(Pick pick) {
        this.pick = pick;
    }

    /**
     * Getter for the pickTime property.
     * @return Date value of the property
     */
    public Date getPickTime() {
        return this.pickTime;
    }

    /**
     * Setter for the pickTime property.
     * @param pickTime the new pickTime value
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }

    /**
     * Getter for the quantityPicked property.
     * @return Integer value of the property
     */
    public Integer getQuantityPicked() {
        return this.quantityPicked;
    }

    /**
     * Setter for the quantityPicked property.
     * @param quantityPicked the new quantityPicked value
     */
    public void setQuantityPicked(Integer quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * Getter for the variableWeight property.
     * @return Double value of the property
     */
    public Double getVariableWeight() {
        return this.variableWeight;
    }

    /**
     * Setter for the variableWeight property.
     * @param variableWeight the new variableWeight value
     */
    public void setVariableWeight(Double variableWeight) {
        this.variableWeight = variableWeight;
    }

    /**
     * Getter for the serialNumber property.
     * @return String value of the property
     */
    public String getSerialNumber() {
        return serialNumber;
    }
    
    /**
     * Setter for the serialNumber property.
     * @param serialNumber the new serialNumber value
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PickDetail)) {
            return false;
        }
        final PickDetail other = (PickDetail) obj;
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        if (getPickTime() == null) {
            if (other.getPickTime() != null) {
                return false;
            }
        } else if (!getPickTime().equals(other.getPickTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result
            + ((pickTime == null) ? 0 : pickTime.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return "" + this.getId();
    }

    /**
     * @return the lotNumber
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * @param lotNumber the lotNumber to set
     */
    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 