/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.voicelink.core.model.CommonModelObject;

import java.util.Locale;


/**
 * 
 *
 * @author mkoenig
 */
public class SummaryPromptDefinitionLocaleRoot extends CommonModelObject {

    private static final long serialVersionUID = 488488206811822888L;

    //Region definition belongs with
    private SummaryPromptDefinition     summaryPromptDefinition;
    
    //Language of definition
    private Locale            locale;

    //Prompt value for condition 1
    private String            promptValue1;

    //Prompt value for condition 2
    private String            promptValue2;
    
    //Prompt value for condition 3
    private String            promptValue3;
    
    
    
    
    /**
     * Getter for the summaryPromptDefinition property.
     * @return SummaryPromptDefinition value of the property
     */
    public SummaryPromptDefinition getSummaryPromptDefinition() {
        return summaryPromptDefinition;
    }

    
    /**
     * Setter for the summaryPromptDefinition property.
     * @param summaryPromptDefinition the new summaryPromptDefinition value
     */
    public void setSummaryPromptDefinition(SummaryPromptDefinition summaryPromptDefinition) {
        this.summaryPromptDefinition = summaryPromptDefinition;
    }

    

    /**
     * Getter for the languageCode property.
     * @return Locale value of the property
     */
    public Locale getLocale() {
        return this.locale;
    }

    
    /**
     * Setter for the languageCode property.
     * @param languageCode the new languageCode value
     */
    public void setLocale(Locale languageCode) {
        this.locale = languageCode;
    }

    
    /**
     * Getter for the promptValue1 property.
     * @return String value of the property
     */
    public String getPromptValue1() {
        return this.promptValue1;
    }

    
    /**
     * Setter for the promptValue1 property.
     * @param promptValue1 the new promptValue1 value
     */
    public void setPromptValue1(String promptValue1) {
        this.promptValue1 = promptValue1;
    }

    
    /**
     * Getter for the promptValue2 property.
     * @return String value of the property
     */
    public String getPromptValue2() {
        return this.promptValue2;
    }

    
    /**
     * Setter for the promptValue2 property.
     * @param promptValue2 the new promptValue2 value
     */
    public void setPromptValue2(String promptValue2) {
        this.promptValue2 = promptValue2;
    }

    
    /**
     * Getter for the promptValue3 property.
     * @return String value of the property
     */
    public String getPromptValue3() {
        return this.promptValue3;
    }

    
    /**
     * Setter for the promptValue3 property.
     * @param promptValue3 the new promptValue3 value
     */
    public void setPromptValue3(String promptValue3) {
        this.promptValue3 = promptValue3;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SummaryPromptDefinitionLocaleRoot)) {
            return false;
        }
        final SummaryPromptDefinitionLocaleRoot other 
                = (SummaryPromptDefinitionLocaleRoot) obj;
        if (getLocale() == null) {
            if (other.getLocale() != null) {
                return false;
            }
        } else if (!getLocale().equals(other.getLocale())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getLocale() == null) ? 0 : getLocale().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getPromptValue1();
    }
    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 