/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.loading.model.LoadingRegion;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author dgold
 *
 */
public class AssignmentImportDataRoot extends BaseModelObject implements
        Importable {

    /**
     * Serializable id.
     */
    private static final long serialVersionUID = -4202342517915067270L;

    private ImportableImpl impl = new ImportableImpl();
    
    private Assignment     myModel = new Assignment();
    
    private List<PickImportData> importablePickData;
    


    /**
     * Set the number in the region. This will be used to look up a region in the database.
     * @param number the number to set the regionnumber to.
     */
    public void setRegionNumber(String number) {
        SelectionRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new SelectionRegion();
            setRegion(tempRegion);
        }
        tempRegion.setNumber(new Integer(number));
    }
    
    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public String getRegionNumber() {
        SelectionRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new SelectionRegion();
            setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return tempRegion.getNumber().toString();
    }

    /**
     * Set number in the loading region. This will be used to look up a region
     * in the database.
     * @param number the number to set in the loading region.
     */
    public void setLoadingRegionNumber(Long number) {
        LoadingRegion tempRegion = myModel.getLoadingRegion();
        if (number == null || this.getDepartureDateTime() == null
            || null == tempRegion) {
            tempRegion = new LoadingRegion();
            setLoadingRegion(tempRegion);
        }

        if (number != null) {
            tempRegion.setNumber(number.intValue());
        }
    }

    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public Long getLoadingRegionNumber() {
        LoadingRegion tempRegion = myModel.getLoadingRegion();
        if (null != tempRegion && null != tempRegion.getNumber()) {
            return new Long(tempRegion.getNumber());
        }
        
        return null;
    }
    
    /**
     * Set the customer number.
     * @param number cust number
     */
    public void setCustomerNumber(String number) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerNumber(number);
    }
    
    /**
     * Set the customer address.
     * @param address customer address
     */
    public void setCustomerAddress(String address) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerAddress(address);
    }
    
    /**
     * Set the customer name.
     * @param name customer name
     */
    public void setCustomerName(String name) {
        Customer customerInfo = getCustomerInfo();
        if (null == customerInfo) {
            customerInfo = new Customer();
            setCustomerInfo(customerInfo);
        }
        customerInfo.setCustomerName(name);
    }
    
    /**
     * Set the work id for the assignment.
     * @param workID the work identifier value
     */
    public void setWorkIdentifier(String workID) {
        WorkIdentifier workIdentifier = getWorkIdentifier();
        if (null == workIdentifier) {
            workIdentifier = new WorkIdentifier();
            setWorkIdentifier(workIdentifier);
        }
        workIdentifier.setWorkIdentifierValue(workID);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        for (PickImportData pick : this.importablePickData) {
            myModel.addPick((Pick) pick.convertToModel());
        }
        return myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public AssignmentStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long customerID) {
        impl.setImportID(customerID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setStatus(AssignmentStatus status) {
        myModel.setStatus(status);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return impl.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#addContainer(com.vocollect.voicelink.selection.model.Container)
     */
    public void addContainer(Container container) {
        myModel.addContainer(container);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#addPick(com.vocollect.voicelink.selection.model.Pick)
     */
    public void addPick(Pick newPick) {
        myModel.addPick(newPick);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#changeStatus(com.vocollect.voicelink.selection.model.AssignmentStatus)
     */
    public void changeStatus(AssignmentStatus statusTo) throws BusinessRuleException {
        myModel.changeStatus(statusTo);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getBaseItemsRemaining()
     */
    public Integer getBaseItemsRemaining() {
        return myModel.getBaseItemsRemaining();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getCancelCount()
     */
    public int getCancelCount() {
        return myModel.getCancelCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getContainerCount()
     */
    public int getContainerCount() {
        return myModel.getContainerCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getContainers()
     */
    public Set<Container> getContainers() {
        return myModel.getContainers();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getCustomerInfo()
     */
    public Customer getCustomerInfo() {
        return myModel.getCustomerInfo();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getDeliveryDate()
     */
    public Date getDeliveryDate() {
        return myModel.getDeliveryDate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getDeliveryLocation()
     */
    public String getDeliveryLocation() {
        return myModel.getDeliveryLocation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getEndTime()
     */
    public Date getEndTime() {
        return myModel.getEndTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getExportStatus()
     */
    public ExportStatus getExportStatus() {
        return myModel.getExportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getGroupInfo()
     */
    public AssignmentGroup getGroupInfo() {
        return myModel.getGroupInfo();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getGUITransitionStatuses()
     */
    public Map<Integer, String> getGUITransitionStatuses() {
        return myModel.getGUITransitionStatuses();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getItemsPicked()
     */
    public Integer getItemsPicked() {
        return myModel.getItemsPicked();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getItemsRemaining()
     */
    public Integer getItemsRemaining() {
        return myModel.getItemsRemaining();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getNewSequenceNumber()
     */
    public Long getNewSequenceNumber() {
        return myModel.getNewSequenceNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getNotPickedCount()
     */
    public int getNotPickedCount() {
        return myModel.getNotPickedCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getNumber()
     */
    public long getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getOperator()
     */
    public Operator getOperator() {
        return myModel.getOperator();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getOriginalSequenceNumber()
     */
    public Long getOriginalSequenceNumber() {
        return myModel.getOriginalSequenceNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getPartailCount()
     */
    public int getPartailCount() {
        return myModel.getPartailCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getPickDetailCount()
     */
    public int getPickDetailCount() {
        return myModel.getPickDetailCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getPicks()
     */
    public List<Pick> getPicks() {
        return myModel.getPicks();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getRegion()
     */
    public SelectionRegion getRegion() {
        return myModel.getRegion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getReservedBy()
     */
    public String getReservedBy() {
        return myModel.getReservedBy();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getRoute()
     */
    public String getRoute() {
        return myModel.getRoute();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getSequenceNumber()
     */
    public Long getSequenceNumber() {
        return myModel.getSequenceNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getShortedCount()
     */
    public int getShortedCount() {
        return myModel.getShortedCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getSplitNumber()
     */
    public Long getSplitNumber() {
        return myModel.getSplitNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getStartTime()
     */
    public Date getStartTime() {
        return myModel.getStartTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getSummaryInfo()
     */
    public AssignmentSummary getSummaryInfo() {
        return myModel.getSummaryInfo();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getType()
     */
    public AssignmentType getType() {
        return myModel.getType();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getVariableWeightTotal()
     */
    public int getVariableWeightTotal() {
        return myModel.getVariableWeightTotal();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getViewableAssignmentNumber()
     */
    public String getViewableAssignmentNumber() {
        return myModel.getViewableAssignmentNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getWorkIdentifier()
     */
    public WorkIdentifier getWorkIdentifier() {
        return myModel.getWorkIdentifier();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#isCalculatedBaseItems()
     */
    public boolean isCalculatedBaseItems() {
        return myModel.isCalculatedBaseItems();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    @Override
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setContainerCount(int)
     */
    public void setContainerCount(int containerCount) {
        myModel.setContainerCount(containerCount);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setContainers(java.util.Set)
     */
    public void setContainers(Set<Container> containers) {
        myModel.setContainers(containers);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setCustomerInfo(com.vocollect.voicelink.core.model.Customer)
     */
    public void setCustomerInfo(Customer customerInfo) {
        myModel.setCustomerInfo(customerInfo);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setDeliveryDate(java.util.Date)
     */
    public void setDeliveryDate(Date deliveryDate) {
        myModel.setDeliveryDate(deliveryDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setDeliveryLocation(java.lang.String)
     */
    public void setDeliveryLocation(String deliveryLocation) {
        myModel.setDeliveryLocation(deliveryLocation);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setGroupInfo(com.vocollect.voicelink.selection.model.AssignmentGroup)
     */
    public void setGroupInfo(AssignmentGroup groupInfo) {
        myModel.setGroupInfo(groupInfo);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setNumber(long)
     */
    public void setNumber(long number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public void setOperator(Operator operator) {
        myModel.setOperator(operator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setPickDetailCount(int)
     */
    public void setPickDetailCount(int pickDetailCount) {
        myModel.setPickDetailCount(pickDetailCount);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setPicks(java.util.List)
     */
    public void setPicks(List<Pick> picks) {
        myModel.setPicks(picks);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setRegion(com.vocollect.voicelink.selection.model.SelectionRegion)
     */
    public void setRegion(SelectionRegion region) {
        myModel.setRegion(region);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setRoute(java.lang.String)
     */
    public void setRoute(String route) {
        myModel.setRoute(route);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setSequenceNumber(java.lang.Long)
     */
    public void setSequenceNumber(Long sequenceNumber) {
        myModel.setSequenceNumber(sequenceNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setSummaryInfo(com.vocollect.voicelink.selection.model.AssignmentSummary)
     */
    public void setSummaryInfo(AssignmentSummary summaryInfo) {
        myModel.setSummaryInfo(summaryInfo);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setType(com.vocollect.voicelink.selection.model.AssignmentType)
     */
    public void setType(AssignmentType type) {
        myModel.setType(type);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setViewableAssignmentNumber(java.lang.String)
     */
    public void setViewableAssignmentNumber(String viewableAssignmentNumber) {
        myModel.setViewableAssignmentNumber(viewableAssignmentNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setWorkIdentifier(com.vocollect.voicelink.selection.model.WorkIdentifier)
     */
    public void setWorkIdentifier(WorkIdentifier workIdentifier) {
        myModel.setWorkIdentifier(workIdentifier);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * Gets the value of importablePickData.
     * @return the importablePickData
     */
    public List<PickImportData> getImportablePickData() {
        return importablePickData;
    }

    /**
     * Sets the value of the importablePickData.
     * @param importablePickData the importablePickData to set
     */
    public void setImportablePickData(List<PickImportData> importablePickData) {
        this.importablePickData = importablePickData;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        // TODO Auto-generated method stub
        return this.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getDepartureDateTime()
     */
    public Date getDepartureDateTime() {
        return myModel.getDepartureDateTime();
    }

    /**
     * @param departureDateTime the departureDateTime to set
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setDepartureDateTime(java.util.Date)
     */
    public void setDepartureDateTime(Date departureDateTime) {
        myModel.setDepartureDateTime(departureDateTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#getLoadingRegion()
     */
    public LoadingRegion getLoadingRegion() {
        return myModel.getLoadingRegion();
    }

    /**
     * @param loadingRegion the loadingRegion to set
     * @see com.vocollect.voicelink.selection.model.AssignmentRoot#setLoadingRegion(com.vocollect.voicelink.loading.model.LoadingRegion)
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        myModel.setLoadingRegion(loadingRegion);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 