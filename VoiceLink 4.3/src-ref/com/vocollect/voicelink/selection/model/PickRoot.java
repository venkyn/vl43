/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataTranslation;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.SelectionErrorCode;

import static com.vocollect.voicelink.selection.model.AssignmentStatus.Complete;
import static com.vocollect.voicelink.selection.model.AssignmentStatus.InProgress;
import static com.vocollect.voicelink.selection.model.PickStatus.Assigned;
import static com.vocollect.voicelink.selection.model.PickStatus.BaseItem;
import static com.vocollect.voicelink.selection.model.PickStatus.Canceled;
import static com.vocollect.voicelink.selection.model.PickStatus.Markout;
import static com.vocollect.voicelink.selection.model.PickStatus.NotPicked;
import static com.vocollect.voicelink.selection.model.PickStatus.Partial;
import static com.vocollect.voicelink.selection.model.PickStatus.Picked;
import static com.vocollect.voicelink.selection.model.PickStatus.Shorted;
import static com.vocollect.voicelink.selection.model.PickStatus.ShortsGoBack;
import static com.vocollect.voicelink.selection.model.PickStatus.Skipped;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 *
 *
 * @author Administrator
 */
public class PickRoot extends CommonModelObject implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 4148390107580118549L;

    // Assignment Pick Belongs to
    private Assignment assignment;

    // Location for pick
    private Location location;

    // Item to Pick
    private Item item;

    // Order picks should be done, base on order of import or supplied from WMS
    private Integer sequenceNumber;

    // Case Label Check Digit (Optional)
    private String caseLabelCheckDigit;

    // Carton In Line Loading (Optional)
    private String cartonNumber;

    // When pick is issued to terminal, should a replenishment be triggered
    private boolean triggerReplenish = false;

    // Unit of measure to pick. Spoken in task at pick prompt (Optionsl)
    private String unitOfMeasure;

    // Quantity to Pick
    private int quantityToPick;

    // Override Base item calculations
    private boolean baseItemOverride = false;

    // Specific container pick goes in (Optional)
    private Integer targetContainerIndicator;

    // Message to speak at pick prompt (Optional)
    private String promptMessage;

    // Total Quantity Picked
    private int quantityPicked = 0;

    // Total Quantity Picked
    private int quantityAdjusted = 0;

    // Is pick a base item
    private boolean isBaseItem = false;

    // Operator that last picked the item (PickDetails have all operator that
    // picked parts of pick)
    private Operator operator;

    // Last Date pick was picked (Pick Details has more details)
    private Date pickTime;

    // Type of Pick
    private PickType type = PickType.Normal;

    // Status of pick
    private PickStatus status = PickStatus.NotPicked;

    //Count of pick detail information available
    private int pickDetailCount = 0;

    // If chase pick, this would be original pick from WMS
    private Pick originalPick;

    //Date/Time a pick was shorted
    private Date shortedDate;

    // Pick Details are actions performed on pick
    private List<PickDetail> pickDetails;

    private Set<Tag> tags;

    //Properties for localized sorting
    private List<DataTranslation>    unitOfMeasureTrans;

    private List<DataTranslation>    promptMessageTrans;


    //The following getters and setters are private due to the fact they
    //should not be referenced from outside of this object. There only use
    //is for sorting localized data using hibernate and the database
    /**
     * Getter for the shortedDate property.
     * @return Date value of the property
     */
    public Date getShortedDate() {
        return shortedDate;
    }

    /**
     * Setter for the shortedDate property.
     * @param shortedDate the new shortedDate value
     */
    public void setShortedDate(Date shortedDate) {
        this.shortedDate = shortedDate;
    }

    /**
     * Getter for the promptMessageTrans property.
     * @return DataTranslation List of DataTranslation
     */
    @SuppressWarnings("unused")
    private List<DataTranslation> getPromptMessageTrans() {
        return promptMessageTrans;
    }



    /**
     * Setter for the promptMessageTrans property.
     * @param promptMessageTrans the new promptMessageTrans value
     */
    @SuppressWarnings("unused")
    private void setPromptMessageTrans(List<DataTranslation> promptMessageTrans) {
        this.promptMessageTrans = promptMessageTrans;
    }



    /**
     * Getter for the unitOfMeasureTrans property.
     * @return DataTranslation List of DataTranslation
     */
    @SuppressWarnings("unused")
    private List<DataTranslation> getUnitOfMeasureTrans() {
        return unitOfMeasureTrans;
    }



    /**
     * Setter for the unitOfMeasureTrans property.
     * @param unitOfMeasureTrans the new unitOfMeasureTrans value
     */
    @SuppressWarnings("unused")
    private void setUnitOfMeasureTrans(List<DataTranslation> unitOfMeasureTrans) {
        this.unitOfMeasureTrans = unitOfMeasureTrans;
    }


    /**
     * Getter for the pickDetailCount property.
     * @return int value of the property
     */
    public int getPickDetailCount() {
        return pickDetailCount;
    }


    /**
     * Setter for the pickDetailCount property.
     * @param pickDetailCount the new pickDetailCount value
     */
    public void setPickDetailCount(int pickDetailCount) {
        this.pickDetailCount = pickDetailCount;
    }


    /**
     * Getter for the quantityAdjusted property.
     * @return int value of the property
     */
    public int getQuantityAdjusted() {
        return quantityAdjusted;
    }

    /**
     * Setter for the quantityAdjusted property.
     * @param quantityAdjusted the new quantityAdjusted value
     */
    public void setQuantityAdjusted(int quantityAdjusted) {
        this.quantityAdjusted = quantityAdjusted;
    }

    /**
     * Getter for the assignment property.
     * @return Assignment value of the property
     */
    public Assignment getAssignment() {
        return this.assignment;
    }

    /**
     * Setter for the assignment property.
     * @param assignment the new assignment value
     */
    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    /**
     * Getter for the baseItemOverride property.
     * @return boolean value of the property
     */
    public boolean isBaseItemOverride() {
        return this.baseItemOverride;
    }

    /**
     * Setter for the baseItemOverride property.
     * @param baseItemOverride the new baseItemOverride value
     */
    public void setBaseItemOverride(boolean baseItemOverride) {
        this.baseItemOverride = baseItemOverride;
    }

    /**
     * Getter for the cartonNumber property.
     * @return String value of the property
     */
    public String getCartonNumber() {
        return this.cartonNumber;
    }

    /**
     * Setter for the cartonNumber property.
     * @param cartonNumber the new cartonNumber value
     */
    public void setCartonNumber(String cartonNumber) {
        this.cartonNumber = cartonNumber;
    }

    /**
     * Getter for the caseLabelCheckDigit property.
     * @return String value of the property
     */
    public String getCaseLabelCheckDigit() {
        return this.caseLabelCheckDigit;
    }

    /**
     * Setter for the caseLabelCheckDigit property.
     * @param caseLabelCheckDigit the new caseLabelCheckDigit value
     */
    public void setCaseLabelCheckDigit(String caseLabelCheckDigit) {
        this.caseLabelCheckDigit = caseLabelCheckDigit;
    }

    /**
     * Getter for the isBaseItem property.
     * @return boolean value of the property
     */
    public boolean getIsBaseItem() {
        return this.isBaseItem;
    }

    /**
     * Setter for the isBaseItem property.
     * @param isBaseItem the new isBaseItem value
     */
    public void setIsBaseItem(boolean isBaseItem) {
        this.isBaseItem = isBaseItem;
    }

    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Getter for the location property.
     * @return Location value of the property
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the originalPick property.
     * @return Pick value of the property
     */
    public Pick getOriginalPick() {
        return this.originalPick;
    }

    /**
     * Setter for the originalPick property.
     * @param originalPick the new originalPick value
     */
    public void setOriginalPick(Pick originalPick) {
        this.originalPick = originalPick;
    }

    /**
     * Getter for the pickDetails property.
     * @return Set&lt;PickDetail&gt; value of the property
     */
    public List<PickDetail> getPickDetails() {
        if (this.pickDetails == null) {
            this.pickDetails = new ArrayList<PickDetail>();
        }
        return this.pickDetails;
    }

    /**
     * Setter for the pickDetails property.
     * @param pickDetails the new pickDetails value
     */
    public void setPickDetails(List<PickDetail> pickDetails) {
        this.pickDetails = pickDetails;
    }

    /**
     * Getter for the pickMessage property.
     * @return String value of the property
     */
    public String getPromptMessage() {
        return this.promptMessage;
    }

    /**
     * Setter for the pickMessage property.
     * @param pickMessage the new pickMessage value
     */
    public void setPromptMessage(String pickMessage) {
        this.promptMessage = pickMessage;
    }

    /**
     * Getter for the pickTime property.
     * @return Date value of the property
     */
    public Date getPickTime() {
        return this.pickTime;
    }

    /**
     * Setter for the pickTime property.
     * @param pickTime the new pickTime value
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }

    /**
     * Getter for the quantityPicked property.
     * @return int value of the property
     */
    public int getQuantityPicked() {
        return this.quantityPicked;
    }

    /**
     * Setter for the quantityPicked property.
     * @param quantityPicked the new quantityPicked value
     */
    public void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * Getter for the quantityToPick property.
     * @return int value of the property
     */
    public int getQuantityToPick() {
        return this.quantityToPick;
    }

    /**
     * Setter for the quantityToPick property.
     * @param quantityToPick the new quantityToPick value
     */
    public void setQuantityToPick(int quantityToPick) {
        this.quantityToPick = quantityToPick;
    }

    /**
     * Getter for the sequenceNumber property.
     * @return Integer value of the property
     */
    public Integer getSequenceNumber() {
        return this.sequenceNumber;
    }

    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * Getter for the status property.
     * @return short value of the property
     */
    public PickStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    protected void setStatus(PickStatus status) {
        //set the shorted date so the location shorted report can
        //track when a locations and items are shorted
        if (status.equals(PickStatus.Shorted)) {
            if (getShortedDate() == null) {
                setShortedDate(new Date());
            }
        }
        this.status = status;
    }

    /**
     * Getter for the targetContainerIndicator property.
     * @return Integer value of the property
     */
    public Integer getTargetContainerIndicator() {
        return this.targetContainerIndicator;
    }

    /**
     * Setter for the targetContainerIndicator property.
     * @param targetContainerIndicator the new targetContainerIndicator value
     */
    public void setTargetContainerIndicator(Integer targetContainerIndicator) {
        this.targetContainerIndicator = targetContainerIndicator;
    }

    /**
     * Getter for the triggerReplenish property.
     * @return boolean value of the property
     */
    public boolean isTriggerReplenish() {
        return this.triggerReplenish;
    }

    /**
     * Setter for the triggerReplenish property.
     * @param triggerReplenish the new triggerReplenish value
     */
    public void setTriggerReplenish(boolean triggerReplenish) {
        this.triggerReplenish = triggerReplenish;
    }

    /**
     * Getter for the type property.
     * @return short value of the property
     */
    public PickType getType() {
        return this.type;
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(PickType type) {
        this.type = type;
    }

    /**
     * Getter for the unitOfMeasure property.
     * @return String value of the property
     */
    public String getUnitOfMeasure() {
        return this.unitOfMeasure;
    }

    /**
     * Setter for the unitOfMeasure property.
     * @param unitOfMeasure the new unitOfMeasure value
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Pick)) {
            return false;
        }
        final Pick other = (Pick) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }





    /**
     * Sets a picks status to base item.
     * processStatusChange will validate if change is valid.
     * This method would typically be called from the Get Assignment LUT.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void setPickAsBaseItem() throws BusinessRuleException {
        // Call the private method to change status
        processStatusChange(BaseItem, false);
        setIsBaseItem(true);
    }

    /**
     * Sets a picks status to not picked.
     * processStatusChange will validate if change is valid.
     * This method would typically be called from the UpdateStatus ODR.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void setPickAsNotPicked() throws BusinessRuleException {
        // Call the private method to change status
        processStatusChange(NotPicked, false);
    }

    /**
     * Sets a picks status to skipped.
     * processStatusChange will validate if change is valid.
     * This method would typically be called from the UpdateStatus ODR.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void reportPickSkipped() throws BusinessRuleException {
        // Call the private method to change status
        processStatusChange(Skipped, false);
    }

    /**
     * Validate that picks is not a chase pick, and if not, then
     * creates a pick detail for adjusting the remaining quantity
     * and finally sets the pick's status to adjusted
     * processStatusChange will validate if change is valid
     * Intention is that this method would be called from the UI
     * Accept Quantity Picked functiuonality.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void acceptQuantityPicked() throws BusinessRuleException {
        if (getType() == PickType.Chase) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_ADJUST_CHASE,
                new UserMessage("pick.updateStatus.error.cannotAdjustChasePick"));
        }
        //This check must occur here since changing from
        //assigned to picked is valid, only shorted picks
        //can have thier quantity adjusted
        if (!getStatus().isInSet(PickStatus.Shorted)) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_ADJUST_UNLESS_SHORT,
                new UserMessage("pick.updateStatus.error.cannotAdjustOnlyShorted"));
        }
        addPickDetail(createPickDetail(PickDetailType.AcceptedQuantity));
    }

    /**
     * After validating that the quantity (passed in) plus the picked quantity
     * is not greater than the quantity to pick, create a pick detail for a
     * manual pick, setting time to now.
     *
     * @param quantity - number picked
     * @throws BusinessRuleException - on adding the pick detail
     */
    public void updateManualPick(int quantity) throws BusinessRuleException {

        if (quantity + this.getQuantityPicked() > this.getQuantityToPick()) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_MANUAL_PICK_QUANTITY,
                new UserMessage(
                    "pick.updateStatus.error.invalidManualPickQuantity"));
        }

        PickDetail detail = new PickDetail();
        detail.setType(PickDetailType.ManualPicked);
        detail.setPickTime(new Date());
        detail.setQuantityPicked(quantity);
        detail.setPick((Pick) this);

        try {
            addPickDetail(detail);
        } catch (BusinessRuleException bre) {

            // override the status change exception to be manual specific
            if (bre.getErrorCode() == SelectionErrorCode.PICK_INVALID_STATUS_CHANGE
                || bre.getErrorCode() == SelectionErrorCode.PICK_ALREADY_COMPLETE) {
                throw new BusinessRuleException(
                    bre.getErrorCode(), new UserMessage(
                        "pick.updateStatus.error.cannotManualPickShort"));
            } else {
                throw bre;
            }

        }
    }

    /**
     * Validate that picks is not a chase pick, and if not, then creates a pick
     * detail for marking out the remaining quantity and finally sets the pick's
     * status to markout processStatusChange will validate if change is valid.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void markoutPick() throws BusinessRuleException  {
        try {
            addPickDetail(createPickDetail(PickDetailType.MarkOut));
        } catch (BusinessRuleException bre) {
            // override the status change exception to be markout specific
            if (bre.getErrorCode() == SelectionErrorCode.PICK_INVALID_STATUS_CHANGE
                || bre.getErrorCode() == SelectionErrorCode.PICK_ALREADY_COMPLETE) {
                throw new BusinessRuleException(
                    bre.getErrorCode(),
                    new UserMessage("pick.updateStatus.error.cannotMarkoutShort"));
            } else {
                throw bre;
            }
        }
    }

    /**
     * Creates a pick detail for the remaining quantity
     * and sets the pick's status to canceled
     * processStatusChange will validate if change is valid.
     *
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void cancelPick() throws BusinessRuleException {
        //Assignment must be available, unavailable, or suspended
        if (!getAssignment().getStatus().isInSet(AssignmentStatus.Available,
                                                AssignmentStatus.Unavailable,
                                                AssignmentStatus.Suspended,
                                                AssignmentStatus.Canceled)) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_BE_CANCELED_INPROGRESS,
                new UserMessage("pick.updateStatus.error.inavlidAssignmentStatusForCancel"));
        }

        //Assignment cannot have an operator assigned
        if (getAssignment().getOperator() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_BE_CANCELED_OPERATOR,
                new UserMessage("pick.updateStatus.error.inavlidAssignmentOperatorForCancel"));
        }

        //Pick Status must be not Picked, Base Item, or Skipped
        if (!getStatus().isInSet(NotPicked, BaseItem, Skipped)) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_BE_CANCELED_INVALID_STATUS,
                new UserMessage("pick.updateStatus.error.inavlidPickStatusForCancel"));
        }


        addPickDetail(createPickDetail(PickDetailType.Canceled));
    }

    /**
     * This method is used to finalize a pick that has a current status
     * of go back for short or partial, otherwise leave the pick as it is.
     * The intention is that this method is called when completing and assignment
     * from the terminal, and to finailize any partially completed pick.
     * Generally setting them to short.
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void finalizePickStatus() throws BusinessRuleException {
        if (getStatus() == ShortsGoBack) {
            processStatusChange(Shorted, false);
            if (getOriginalPick() != null) {
                if (getOriginalPick().getStatus() == Assigned) {
                    getOriginalPick().processStatusChange(Shorted, false);
                }
            }
        } else if (getStatus() == Partial) {
            processStatusChange(Picked, false);
        }
    }

    /**
     * This method creates a new detail record for an action being taken.
     * It will create a detail record for the remaining quantity of the pick
     * and assign the type of detail based on the PickDetailType passed in.
     *
     * @param detailAction - detail action to create the detail for
     * @return - returns the detail object that was created
     */
    protected PickDetail createPickDetail(PickDetailType detailAction) {
        PickDetail detail = new PickDetail();

        detail.setType(detailAction);

        detail.setPickTime(new Date());
        detail.setQuantityPicked(getQuantityToPick() - getQuantityPicked());
        detail.setPick((Pick) this);

        return detail;
    }


    /**
     * This method is used to assign the current pick to a chase assignment
     * It will validate that the pick can be assigned, and the assignment is also
     * valid.
     *
     * It will first create a new pick object based on the infomration in this pick
     * and then add that pick to the chase assignment that was passed in.
     *
     * @param chaseAssignment - Chase assignment to assign new pick to
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void assignPickToChase(Assignment chaseAssignment)
    throws BusinessRuleException {

        //Cannot assign a chase pick to another assignment
        if (getType() == PickType.Chase) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_ADD_CHASE,
                new UserMessage("pick.updateStatus.error.cannotAddChasePick"));
        }

        //Can only assign picks that are shorted
        if (getStatus() != Shorted) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_MUST_BE_SHORT,
                new UserMessage("pick.updateStatus.error.mustBeShort"));
        }

        //Can only assign to chase assignments
        if (chaseAssignment.getType() != AssignmentType.Chase) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_ADD_CHASE_TO_NORMAL,
                new UserMessage("pick.updateStatus.error.cannotAddChaseToNormal"));
        }

        //Assignment status must be Available or Unavailable
        if (!chaseAssignment.getStatus().isInSet(AssignmentStatus.Available,
                                                AssignmentStatus.Unavailable)) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_CANNOT_ADD_ASSIGNMENT,
                new UserMessage("pick.updateStatus.error.invalidAssignmentStatusCannotAddPick"));
        }

        //Pick must be from same region as assignment
        if (!chaseAssignment.getRegion().equals(getAssignment().getRegion())) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_REGION,
                new UserMessage("pick.updateStatus.error.invalidAssignmentRegionCannotAddPick"));
        }

        Pick chasePick = new Pick();

        chasePick.setItem(this.getItem());
        chasePick.setLocation(this.getLocation());
        chasePick.setOriginalPick((Pick) this);
        chasePick.setPromptMessage(this.getPromptMessage());
        chasePick.setQuantityToPick(this.getQuantityToPick()
            - this.getQuantityPicked());
        chasePick.setSequenceNumber(0);
        chasePick.setType(PickType.Chase);
        chasePick.setUnitOfMeasure(this.getUnitOfMeasure());

        //assignment chase pick to chase assignment
        chaseAssignment.addPick(chasePick);

        //Mark this pick as assigned
        processStatusChange(Assigned, false);
    }

    /**
     * This method is called when a chase pick is canceled. It is usually
     * only called from the chase pick itself. The purpose is to set the
     * original pick back to short so it can be reassinged, marked out, etc...
     *
     * @param childPick - the child pick the was canceled
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void childPickCanceled(Pick childPick) throws BusinessRuleException {
        if (childPick.getOriginalPick().getId() != getId()) {
            //TODO - Phase I: Throw error invalid child pick
        }

        if (childPick.getStatus() != Canceled) {
            //TODO - Phase I: Throw error invalid status
        }

        processStatusChange(Shorted, false);
    }

    /**
     * This method would usually be called from the Pick ODR to add a new pick
     * detail record to the pick. It will handle setting the status of the pick based
     * on the type of pick detail record that is received. If the detail record passed
     * in is known not to be the final pick detail, then the partial flag would be set to false
     *
     * @param pickDetail - pick detail to add to pick
     * @param partial - indicator to determine if more infomration is to follow
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void addPickDetail(PickDetail pickDetail, boolean partial) throws BusinessRuleException {

        // Cannot add details if pick is completed (Picked, Canceled, or Marked
        // Out)
        if (getStatus().isInSet(Picked, Canceled, Markout)) {
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_ALREADY_COMPLETE,
                new UserMessage("pick.updateStatus.error.pickComplete"));
        }

        // Update the picked quantity field
        if (pickDetail.getType() == PickDetailType.AcceptedQuantity) {
            setQuantityAdjusted(getQuantityAdjusted()
                + pickDetail.getQuantityPicked());

            //Reduce the original quantity to pick
            setQuantityToPick(getQuantityToPick()
                - pickDetail.getQuantityPicked());
        } else if (pickDetail.getType() == PickDetailType.ManualPicked
            || pickDetail.getType() == PickDetailType.TaskPicked) {
            setQuantityPicked(getQuantityPicked()
                + pickDetail.getQuantityPicked());
        }

        // Update the latest pick time and latest operator
        if (pickDetail.getType() != PickDetailType.AcceptedQuantity
            && pickDetail.getType() != PickDetailType.MarkOut) {
            setPickTime(pickDetail.getPickTime());
        }

        if (pickDetail.getOperator() != null) {
            setOperator(pickDetail.getOperator());
        }

        // Add the detail information
        pickDetail.setPick((Pick) this);
        getPickDetails().add(pickDetail);
        setPickDetailCount(getPickDetailCount() + 1);
        getAssignment().setPickDetailCount(getAssignment().getPickDetailCount() + 1);

        // Call process pick to set status accordingly
        if (pickDetail.getType() == PickDetailType.MarkOut) {
            processStatusChange(Markout, partial);
        } else if (pickDetail.getType() == PickDetailType.Canceled) {
            processStatusChange(Canceled, partial);
        } else {
            processStatusChange(Picked, partial);
        }

        // Check if a original pick exists and update it as well
        if (getOriginalPick() != null) {

            if (getStatus() == Canceled) {
                getOriginalPick().childPickCanceled((Pick) this);
            } else {
                PickDetail originalDetail = new PickDetail();

                originalDetail.setType(pickDetail.getType());
                originalDetail.setContainer(pickDetail.getContainer());
                originalDetail.setOperator(pickDetail.getOperator());
                originalDetail.setPickTime(pickDetail.getPickTime());
                originalDetail.setQuantityPicked(pickDetail.getQuantityPicked());
                originalDetail.setVariableWeight(pickDetail.getVariableWeight());

                if (getStatus() == Markout) {
                    getOriginalPick().setStatus(Shorted);
                }
                getOriginalPick().addPickDetail(originalDetail, partial);
                //If pick is currently go back for short, then
                //make sure original pick is still assigned
                if (getStatus() == ShortsGoBack) {
                    getOriginalPick().processStatusChange(Assigned, partial);
                } else if (getStatus() == Markout && getOriginalPick().getStatus() == Shorted) {
                    getOriginalPick().markoutPick();
                }


            }

            // If chase pick is picked, set original assignment to complete if
            // all picks in original
            // assignment are now complete
            if (getOriginalPick().getStatus() == Picked
                && (getOriginalPick().getAssignment().getStatus()
                    == AssignmentStatus.Short)) {
                getOriginalPick().getAssignment().changeStatus(Complete);
            }
        } else if (
                (pickDetail.getType().equals(PickDetailType.AcceptedQuantity)
             || pickDetail.getType().equals(PickDetailType.MarkOut))
             && getAssignment().getStatus() == AssignmentStatus.Short) {
            getAssignment().changeStatus(Complete);
        }
    }

    /**
     * This is an overladed call for the method above. It assumes that the pick
     * detail being passed in is the final detail
     *
     * @param pickDetail - Detail information to add
     * @throws BusinessRuleException - Throws business rule exception
     */
    public void addPickDetail(PickDetail pickDetail) throws BusinessRuleException {
        addPickDetail(pickDetail, false);
    }

    /**
     * This is the main method for changing the status of a pick. It enforces
     * the valid changes and determines the actual final status to set the pick to
     * based on other pick infomration. It will also report the item/location as shorted
     * as well as handle auto mark out shorts. This method has been broken down
     * to smaller method to allow for easier customizations. The method called by this method
     * should not be called directly
     *
     * @param statusTo - status attemping to change to
     * @param partial  - indicator to determine if more infomration is to follow
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void processStatusChange(PickStatus statusTo, boolean partial) throws BusinessRuleException {

        // Checks for Valid Status changes
        switch (getStatus()) {
        // Not Picked and Skipped have same processing
        case NotPicked:
            checkStatusChangeFromNotPicked(statusTo);
            break;
        case Skipped:
            checkStatusChangeFromSkipped(statusTo);
            break;
        case BaseItem:
            checkStatusChangeFromBaseItem(statusTo);
            break;
        case Shorted:
            checkStatusChangeFromShorted(statusTo);
            break;
        case ShortsGoBack:
            checkStatusChangeFromShorsGoBack(statusTo);
            break;
        case Assigned:
            checkStatusChangeFromAssigned(statusTo);
            break;
        case Partial:
            checkStatusChangeFromPartial(statusTo);
            break;
        // Picked, Canceled, and Marked Out are all final and cannot be changed
        case Picked:
            checkStatusChangeFromPicked(statusTo);
            break;
        case Canceled:
            checkStatusChangeFromCanceled(statusTo);
            break;
        case Markout:
            checkStatusChangeFromMarkedOut(statusTo);
            break;

        // Default is basiclly to throw error of invalid status change, although
        // we will allow setting status to be same as current status
        default:
            checkStatusChangeFromOther(statusTo);
            break;
        }

        // Update Pick Status
        updatePickStatus(statusTo, partial);

        // Auto Mark out pick if shorted
        autoMarkOutShort();
    }

    /**
     * Validates the status changing to, is valid from current not pick status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromNotPicked(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, BaseItem, Skipped, Canceled)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }

    /**
     * Validates the status changing to, is valid from current Skipped status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromSkipped(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(NotPicked, Picked, BaseItem, Skipped, Canceled)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }

    /**
     * Validates the status changing to, is valid from current base item status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromBaseItem(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, NotPicked, Skipped, Canceled, BaseItem)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }
    /**
     * Validates the status changing to, is valid from current shorted status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromShorted(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, Assigned, Markout)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }
    /**
     * Validates the status changing to, is valid from current shorts go back status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromShorsGoBack(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, Shorted)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }
    /**
     * Validates the status changing to, is valid from current Assigned status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromAssigned(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, Shorted)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }
    /**
     * Validates the status changing to, is valid from current partial status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromPartial(PickStatus statusTo)
    throws BusinessRuleException {
        if (!statusTo.isInSet(Picked, Shorted)) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }
    /**
     * Validates the status changing to, is valid from current Picked status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromPicked(PickStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }
    /**
     * Validates the status changing to, is valid from current canceled status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromCanceled(PickStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }
    /**
     * Validates the status changing to, is valid from current mark out status
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromMarkedOut(PickStatus statusTo)
    throws BusinessRuleException {
        checkStatusChangeFromFinalState(statusTo);
    }
    /**
     * called from above methods always error to change status from final state.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromFinalState(PickStatus statusTo)
    throws BusinessRuleException {
        // TODO - Phase II: Need to change status name to resource key when localization implemented
        throw new BusinessRuleException(
            SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
            new UserMessage("pick.updateStatus.error.invalidStatusChange",
                getStatus().name(), statusTo.name()));
    }
    /**
     * Hook put in for check against a custom status that may be added to the system
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status pick is being changed to
     * @throws BusinessRuleException - Throws business rule exception
     */
    protected void checkStatusChangeFromOther(PickStatus statusTo)
    throws BusinessRuleException {
        if (statusTo != getStatus()) {
            // TODO - Phase II: Need to change status name to resource key when localization implemented
            throw new BusinessRuleException(
                SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                new UserMessage("pick.updateStatus.error.invalidStatusChange",
                    getStatus().name(), statusTo.name()));
        }
    }


    /**
     * Determines what status to set the pick to based on the status requested and
     * other information about the pick, assignment and regions.
     * called from process status change and should not be called directly.
     *
     * @param statusTo - status attemping to change to
     * @param partial - indicator to determine if more infomration is to follow
     */
    protected void updatePickStatus(PickStatus statusTo, boolean partial) {
        if (statusTo == Picked) {
            // Check if more information is expected, if so then set status to
            // partial
            if (partial && getAssignment().getStatus() == InProgress) {
                setStatus(Partial);
                // If quantity was not fully picked then determine short status
            } else if (getQuantityPicked() < getQuantityToPick()) {
                // if in a region with go back for shorts set to never then set
                // shorted
                if (getAssignment().getRegion()
                    .getUserSettings(getAssignment().getType())
                    .getGoBackForShortsIndicator()
                    == SelectionRegionGoBackForShorts.Never) {
                    setStatus(Shorted);

                // if region allows go back for shorts then determine status
                } else {

                    // If updating an in-progress assignment, then set to shorts
                    // go back
                    if (getAssignment().getStatus() == InProgress) {
                        setStatus(ShortsGoBack);

                    // Else set to shorted
                    } else {
                        setStatus(Shorted);
                    }
                }

                // Full quantity was picked or adjusted so set to picked
            } else {
                setStatus(Picked);
            }

        } else {
            setStatus(statusTo);
        }
    }

    /**
     * If the pick was shorted, then check if the pick should automaticlly be
     * marked out.
     * called from process status change and should not be called directly.
     * @throws BusinessRuleException - business rule exceptions
     */
    protected void autoMarkOutShort() throws BusinessRuleException {
        if (getStatus() == Shorted) {
            // If mark out when location is not replenished
            if (getAssignment().getRegion()
                    .getUserSettings(getAssignment().getType())
                    .getAutoMarkOutShorts()
                    == SelectionRegionAutoMarkOutShorts.WhenNotReplenished) {
                // TODO - Phase III: if condition needs to be changed when itemStatus method
                // is implemented
                if (getLocation().getStatus(getItem()) == LocationStatus.Empty) {
                    markoutPick();
                }
                // New feature not yet implemented
                // } else if (getAssignment().getRegion().getAutoMarkOutShorts()
                // == SelectionRegionAutoMarkOutShorts.Always) {
                // addFinalPickDetail(PickStatus.Markout);
                // setStatus(PickStatus.Markout);
            }
        }
    }

    /**
     * Creates a duplicate copy of this pick object.
     *
     * @return - A pick object that is a copy of this object
     */
    public Pick getCopy() {
        Pick pick = new Pick();

        pick.setBaseItemOverride(this.baseItemOverride);
        pick.setCartonNumber(this.cartonNumber);
        pick.setCaseLabelCheckDigit(this.caseLabelCheckDigit);
        pick.setIsBaseItem(this.isBaseItem);
        pick.setItem(this.item);
        pick.setLocation(this.location);
        pick.setOperator(this.operator);
        pick.setOriginalPick(this.originalPick);
        pick.setPickDetails(null);
        pick.setPickTime(this.pickTime);
        pick.setPromptMessage(this.promptMessage);
        pick.setQuantityAdjusted(this.quantityAdjusted);
        pick.setQuantityPicked(this.quantityPicked);
        pick.setQuantityToPick(this.quantityToPick);
        pick.setSequenceNumber(this.sequenceNumber);
        pick.setStatus(this.status);
        pick.setTargetContainerIndicator(this.targetContainerIndicator);
        pick.setTriggerReplenish(this.triggerReplenish);
        pick.setType(this.type);
        pick.setUnitOfMeasure(this.unitOfMeasure);

        return pick;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getItem().getDescriptiveText();
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Return the replenishment status for item/location.
     *
     * @return - replenishment status for pick's item/location
     */
    public LocationStatus getReplenishmentStatus() {
        return getLocation().getStatus(getItem());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 