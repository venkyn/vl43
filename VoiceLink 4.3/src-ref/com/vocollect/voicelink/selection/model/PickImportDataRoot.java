/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;
import java.util.List;

/**
 * Pick data to be imported. 
 * @author dgold
 *
 */
public class PickImportDataRoot extends BaseModelObject implements Importable {
 
    private static final long serialVersionUID = -6431233232572304204L;
    private ImportableImpl impl    = new ImportableImpl();
    private Pick     myModel = new Pick();
    private AssignmentImportData parent = null;
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        myModel.setStatus(PickStatus.NotPicked);
        return myModel;
    }
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }
    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return impl.toString();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getAssignment()
     */
    public Assignment getAssignment() {
        return myModel.getAssignment();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getCaseLabelCheckDigit()
     */
    public String getCaseLabelCheckDigit() {
        return myModel.getCaseLabelCheckDigit();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getIsBaseItem()
     */
    public boolean getIsBaseItem() {
        return myModel.getIsBaseItem();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getLocation()
     */
    public Location getLocation() {
        return myModel.getLocation();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getOperator()
     */
    public Operator getOperator() {
        return myModel.getOperator();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getOriginalPick()
     */
    public Pick getOriginalPick() {
        return myModel.getOriginalPick();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getPickDetailCount()
     */
    public int getPickDetailCount() {
        return myModel.getPickDetailCount();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getPickDetails()
     */
    public List<PickDetail> getPickDetails() {
        return myModel.getPickDetails();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getPickTime()
     */
    public Date getPickTime() {
        return myModel.getPickTime();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getPromptMessage()
     */
    public String getPromptMessage() {
        return myModel.getPromptMessage();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getQuantityToPick()
     */
    public int getQuantityToPick() {
        return myModel.getQuantityToPick();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getReplenishmentStatus()
     */
    public LocationStatus getReplenishmentStatus() {
        return myModel.getReplenishmentStatus();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getSequenceNumber()
     */
    public Integer getSequenceNumber() {
        return myModel.getSequenceNumber();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getTargetContainerIndicator()
     */
    public Integer getTargetContainerIndicator() {
        return myModel.getTargetContainerIndicator();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getType()
     */
    public PickType getType() {
        return myModel.getType();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#getUnitOfMeasure()
     */
    public String getUnitOfMeasure() {
        return myModel.getUnitOfMeasure();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#isBaseItemOverride()
     */
    public boolean isBaseItemOverride() {
        return myModel.isBaseItemOverride();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#isTriggerReplenish()
     */
    public boolean isTriggerReplenish() {
        return myModel.isTriggerReplenish();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setAssignment(com.vocollect.voicelink.selection.model.Assignment)
     */
    public void setAssignment(Assignment assignment) {
        myModel.setAssignment(assignment);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setBaseItemOverride(boolean)
     */
    public void setBaseItemOverride(boolean baseItemOverride) {
        myModel.setBaseItemOverride(baseItemOverride);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setCaseLabelCheckDigit(java.lang.String)
     */
    public void setCaseLabelCheckDigit(String caseLabelCheckDigit) {
        myModel.setCaseLabelCheckDigit(caseLabelCheckDigit);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setIsBaseItem(boolean)
     */
    /**
     * @author amukherjee
     * Removed as a part of VLINK-2886
     */
    /*  public void setIsBaseItem(boolean isBaseItem) {
        myModel.setIsBaseItem(isBaseItem);
    }*/
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }
    
    /**
     * Set the item number for this pick.
     * @param itemNumber the item number for this pick.
     */
    public void setItemNumber(String itemNumber) {
        Item item = getItem();
        if (null == item) {
            item = new Item();
            setItem(item);
        }
        item.setNumber(itemNumber);
    }
    
    /**
     * Get the item number for this pick.
     * @return the item number for this pick.
     */
    public String getItemNumber() {
        Item item = getItem();
        if (null == item) {
            item = new Item();
            setItem(item);
        }
        return item.getNumber();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }
    
    /**
     * Set the scanned verification string to use to identify this location.
     * @param locationNumber the scanned verification string to use to identify this location.
     */
    public void setLocationNumber(String locationNumber) {
        Location location = getLocation();
        if (null == location) {
            location = new Location();
            setLocation(location);
        }
        location.setScannedVerification(locationNumber);
    }
    
    /**
     * Get the scanned verification string to use to identify this location.
     * @return the scanned verification string to use to identify this location.
     */
    public String getLocationNumber() {
        Location location = getLocation();
        if (null == location) {
            location = new Location();
            setLocation(location);
        }
        return location.getScannedVerification();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setPickAsBaseItem()
     */
    public void setPickAsBaseItem() throws BusinessRuleException {
        myModel.setPickAsBaseItem();
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setPromptMessage(java.lang.String)
     */
    public void setPromptMessage(String pickMessage) {
        myModel.setPromptMessage(pickMessage);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setQuantityToPick(int)
     */
    public void setQuantityToPick(int quantityToPick) {
        myModel.setQuantityToPick(quantityToPick);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setSequenceNumber(java.lang.Integer)
     */
    public void setSequenceNumber(Integer sequenceNumber) {
        myModel.setSequenceNumber(sequenceNumber);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setTargetContainerIndicator(java.lang.Integer)
     */
    public void setTargetContainerIndicator(Integer targetContainerIndicator) {
        myModel.setTargetContainerIndicator(targetContainerIndicator);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setTriggerReplenish(boolean)
     */
    public void setTriggerReplenish(boolean triggerReplenish) {
        myModel.setTriggerReplenish(triggerReplenish);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setType(com.vocollect.voicelink.selection.model.PickType)
     */
    public void setType(PickType type) {
        myModel.setType(type);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.model.PickRoot#setUnitOfMeasure(java.lang.String)
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        myModel.setUnitOfMeasure(unitOfMeasure);
    }
    /**
     * Gets the value of parent.
     * @return the parent
     */
    public AssignmentImportData getParent() {
        return parent;
    }
    /**
     * Sets the value of the parent.
     * @param parent the parent to set
     */
    public void setParent(AssignmentImportData parent) {
        this.parent = parent;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
       return this.getImportID();
    }

    
    /**
     * @return returns the carton number of the myModel object.
     * @see com.vocollect.voicelink.selection.model.PickRoot#getCartonNumber()
     */
    public String getCartonNumber() {
        return myModel.getCartonNumber();
    }
    
    
    /**
     * @param cartonNumber - carton number to set.
     * @see com.vocollect.voicelink.selection.model.PickRoot#setCartonNumber(java.lang.String)
     */
    public void setCartonNumber(String cartonNumber) {
        myModel.setCartonNumber(cartonNumber);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 