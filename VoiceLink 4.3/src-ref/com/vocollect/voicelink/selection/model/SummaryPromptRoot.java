/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;


/**
 * 
 *
 * @author mkoenig
 */
public class SummaryPromptRoot extends CommonModelObject  
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 2032181409852139928L;

    private String          name;

    private List<SummaryPromptDefinition>  definitions 
            = new ArrayList<SummaryPromptDefinition>();
    
    private Set<Tag> tags;
    

    
    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    
    /**
     * Getter for the definitions property.
     * @return List&lt;SummaryPromptDefinition&gt; value of the property
     */
    public List<SummaryPromptDefinition> getDefinitions() {
        return definitions;
    }


    
    /**
     * Setter for the definitions property.
     * @param definitions the new definitions value
     */
    public void setDefinitions(List<SummaryPromptDefinition> definitions) {
        this.definitions = definitions;
    }


    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Check if defintion contains definition for locale specified.
     * 
     * @param locale - locale to look for.
     * @return - true if definition includes specified locale otherwise false.
     */
    public boolean localeDefined(Locale locale) {
        for (SummaryPromptDefinition spd : getDefinitions()) {
            for (SummaryPromptDefinitionLocale spdl : spd.getLocaleDefintions()) {
                if (spdl.getLocale().equals(locale)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    

    /**
     * get a list of locales that have a prompt definition for this prompt.
     * 
     * @return a list of locales that have a prompt definition
     */
    public List<Locale> listLocalesWithDefinitions() {
        List<Locale> locales = new ArrayList<Locale>();
        try {
        if (getDefinitions() != null && getDefinitions().size() > 0) {
        SummaryPromptDefinition spd = getDefinitions().get(0);
        
        for (SummaryPromptDefinitionLocale spdl : spd.getLocaleDefintions()) {
            if (!locales.contains(spdl.getLocale())) {
                locales.add(spdl.getLocale());
            }
        }
        }
        } catch (Exception e) {
        }
      
        return locales;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SummaryPrompt)) {
            return false;
        }
        final SummaryPrompt other = (SummaryPrompt) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
       return this.getName();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 