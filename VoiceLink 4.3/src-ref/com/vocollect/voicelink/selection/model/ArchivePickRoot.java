/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.LocationDescription;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Model object representing a VoiceLink Archive Selection Pick.
 * 
 * @author mlashinsky
 */
public class ArchivePickRoot extends ArchiveModelObject implements Serializable {

    //
    private static final long serialVersionUID = 4148390107580118549L;

    // Archive Assignment pick belongs to
    private ArchiveAssignment assignment;

    //Location scanned verification
    private String scannedVerification;
    
    // Location for pick
    private LocationDescription description;

    // Item to Pick
    private Item item;

    // Order picks should be done, base on order of import or supplied from WMS
    private Integer sequenceNumber;

    // Unit of measure to pick. Spoken in task at pick prompt (Optionsl)
    private String unitOfMeasure;

    // Quantity to Pick
    private int quantityToPick;

    // Specific container pick goes in (Optional)
    private Integer targetContainerIndicator;

    // Total Quantity Picked
    private int quantityPicked = 0;

    // Total Quantity Picked
    private int quantityAdjusted = 0;

    // Operator that last picked the item (PickDetails have all operator that
    // picked parts of pick)
    private Operator operator;

    // Last Date pick was picked (Pick Details has more details)
    private Date pickTime;

    // Status of pick
    private PickStatus status = PickStatus.NotPicked;

    // Count of pick detail information available
    private int pickDetailCount = 0;

    //Date/Time a pick was shorted
    private Date shortedDate;

    // Pick Details are actions performed on pick
    private List<ArchivePickDetail> pickDetails = new ArrayList<ArchivePickDetail>();

    // Site to which the Pick Belongs
    private Site site;

    /**
     * 
     * Constructor.
     */
    public ArchivePickRoot() {

    }

    /**
     * 
     * Constructor.
     * @param pick Pick used to construct ArchivePick.
     * @param archiveAssignment ArchiveAssignment used to construct ArchivePick
     * @param containers - containers in archive assignment
     */
    public ArchivePickRoot(Pick pick,
                           ArchiveAssignment archiveAssignment,
                           Set<ArchiveContainer> containers) {
        for (PickDetail pd : pick.getPickDetails()) {
            getPickDetails().add(
                new ArchivePickDetail(pd, (ArchivePick) this, containers));
        }

        for (Tag t : pick.getTags()) {
            if (t.getTagType().equals(Tag.SITE)) {
                this.setSite(getSiteMap().get(t.getTaggableId()));
                break;
            }
        }

        this.setAssignment(archiveAssignment);
        this.setCreatedDate(new Date());
        this.setItem(pick.getItem());
        this.setDescription(pick.getLocation().getDescription());
        this.setOperator(pick.getOperator());
        this.setPickDetailCount(pick.getPickDetailCount());
        this.setPickTime(pick.getPickTime());
        this.setQuantityAdjusted(pick.getQuantityAdjusted());
        this.setQuantityPicked(pick.getQuantityPicked());
        this.setQuantityToPick(pick.getQuantityToPick());
        this.setSequenceNumber(pick.getSequenceNumber());
        this.setStatus(pick.getStatus());
        this.setTargetContainerIndicator(pick.getTargetContainerIndicator());
        this.setUnitOfMeasure(pick.getUnitOfMeasure());
        this.setShortedDate(pick.getShortedDate());
        this.setScannedVerification(pick.getLocation().getScannedVerification());
    }
    
    /**
     * Getter for the scannedVerification property.
     * @return String value of the property
     */
    public String getScannedVerification() {
        return scannedVerification;
    }

    
    /**
     * Setter for the scannedVerification property.
     * @param scannedVerification the new scannedVerification value
     */
    public void setScannedVerification(String scannedVerification) {
        this.scannedVerification = scannedVerification;
    }

    /**
     * Getter for the shortedDate property.
     * @return Date value of the property
     */
    public Date getShortedDate() {
        return shortedDate;
    }

    /**
     * Setter for the shortedDate property.
     * @param shortedDate the new shortedDate value
     */
    public void setShortedDate(Date shortedDate) {
        this.shortedDate = shortedDate;
    }

    /**
     * Getter for the pickDetailCount property.
     * @return int value of the property
     */
    public int getPickDetailCount() {
        return pickDetailCount;
    }

    /**
     * Setter for the pickDetailCount property.
     * @param pickDetailCount the new pickDetailCount value
     */
    public void setPickDetailCount(int pickDetailCount) {
        this.pickDetailCount = pickDetailCount;
    }

    /**
     * Getter for the quantityAdjusted property.
     * @return int value of the property
     */
    public int getQuantityAdjusted() {
        return quantityAdjusted;
    }

    /**
     * Setter for the quantityAdjusted property.
     * @param quantityAdjusted the new quantityAdjusted value
     */
    public void setQuantityAdjusted(int quantityAdjusted) {
        this.quantityAdjusted = quantityAdjusted;
    }

    /**
     * Getter for the pickDetails property.
     * @return Set&lt;PickDetail&gt; value of the property
     */
    public List<ArchivePickDetail> getPickDetails() {
        if (this.pickDetails == null) {
            this.pickDetails = new ArrayList<ArchivePickDetail>();
        }
        return this.pickDetails;
    }

    /**
     * Setter for the pickDetails property.
     * @param pickDetails the new pickDetails value
     */
    public void setPickDetails(List<ArchivePickDetail> pickDetails) {
        this.pickDetails = pickDetails;
    }

    /**
     * Getter for the pickTime property.
     * @return Date value of the property
     */
    public Date getPickTime() {
        return this.pickTime;
    }

    /**
     * Setter for the pickTime property.
     * @param pickTime the new pickTime value
     */
    public void setPickTime(Date pickTime) {
        this.pickTime = pickTime;
    }

    /**
     * Getter for the quantityPicked property.
     * @return int value of the property
     */
    public int getQuantityPicked() {
        return this.quantityPicked;
    }

    /**
     * Setter for the quantityPicked property.
     * @param quantityPicked the new quantityPicked value
     */
    public void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    /**
     * Getter for the quantityToPick property.
     * @return int value of the property
     */
    public int getQuantityToPick() {
        return this.quantityToPick;
    }

    /**
     * Setter for the quantityToPick property.
     * @param quantityToPick the new quantityToPick value
     */
    public void setQuantityToPick(int quantityToPick) {
        this.quantityToPick = quantityToPick;
    }

    /**
     * Getter for the sequenceNumber property.
     * @return Integer value of the property
     */
    public Integer getSequenceNumber() {
        return this.sequenceNumber;
    }

    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * Getter for the status property.
     * @return short value of the property
     */
    public PickStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    protected void setStatus(PickStatus status) {
        this.status = status;
    }

    /**
     * Getter for the targetContainerIndicator property.
     * @return Integer value of the property
     */
    public Integer getTargetContainerIndicator() {
        return this.targetContainerIndicator;
    }

    /**
     * Setter for the targetContainerIndicator property.
     * @param targetContainerIndicator the new targetContainerIndicator value
     */
    public void setTargetContainerIndicator(Integer targetContainerIndicator) {
        this.targetContainerIndicator = targetContainerIndicator;
    }

    /**
     * Getter for the unitOfMeasure property.
     * @return String value of the property
     */
    public String getUnitOfMeasure() {
        return this.unitOfMeasure;
    }

    /**
     * Setter for the unitOfMeasure property.
     * @param unitOfMeasure the new unitOfMeasure value
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Pick)) {
            return false;
        }
        final Pick other = (Pick) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }

    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return site;
    }

    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * Getter for the assignment property.
     * @return ArchiveAssignment value of the property
     */
    public ArchiveAssignment getAssignment() {
        return assignment;
    }

    /**
     * Setter for the assignment property.
     * @param assignment the new assignment value
     */
    public void setAssignment(ArchiveAssignment assignment) {
        this.assignment = assignment;
    }

    /**
     * @return LocationDescription
     */
    public LocationDescription getDescription() {
        return description;
    }

    /**
     * @param description - the location description
     */
    public void setDescription(LocationDescription description) {
        this.description = description;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 