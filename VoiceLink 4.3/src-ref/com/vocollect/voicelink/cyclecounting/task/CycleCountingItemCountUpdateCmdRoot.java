/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.util.VoicelinkNotificationUtil;

import java.util.Date;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingItemCountUpdateCmdRoot extends
        BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -3470990419494294672L;

    private String locationID;
    private String itemNumber;
    private Integer quantity;
    private String uom;
    private String skippedSlot;
    private String reasonCode;

    private LaborManager laborManager;

    private VoicelinkNotificationUtil voicelinkNotificationUtil;
    
    private static final int CYCLECOUNTINGONTHEFLYINDICATOR = -1;

    /**
     * Getter for the voicelinkNotificationUtil property.
     * 
     * @return VoicelinkNotificationUtil value of the property
     */
    public VoicelinkNotificationUtil getVoicelinkNotificationUtil() {
        return this.voicelinkNotificationUtil;
    }

    /**
     * Setter for the voicelinkNotificationUtil property.
     * 
     * @param voicelinkNotificationUtil
     *            the new voicelinkNotificationUtil value
     */
    public void setVoicelinkNotificationUtil(
            VoicelinkNotificationUtil voicelinkNotificationUtil) {
        this.voicelinkNotificationUtil = voicelinkNotificationUtil;
    }

    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * @param locationID
     *            the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    /**
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }

    /**
     * @param itemNumber
     *            the itemNumber to set
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity
     *            the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the uom
     */
    public String getUom() {
        return uom;
    }

    /**
     * @param uom
     *            the uom to set
     */
    public void setUom(String uom) {
        this.uom = uom;
    }

    /**
     * @return the skippedSlot
     */
    public String getSkippedSlot() {
        return skippedSlot;
    }

    /**
     * @param skippedSlot
     *            the skippedSlot to set
     */
    public void setSkippedSlot(String skippedSlot) {
        this.skippedSlot = skippedSlot;
    }

    /**
     * @return the reasonCode
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * @param reasonCode
     *            the reasonCode to set
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * @param laborManager
     *            the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    @Override
    protected Response doExecute() throws Exception {

        if (getSkippedSlot().equalsIgnoreCase("y")) {
            skipAssignment();
        } else {
            updateDetail();
        }

        closeCycleCountingLaborRecord();
        return getResponse();

    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    private void skipAssignment() throws DataAccessException,
            BusinessRuleException {

        CycleCountingAssignment assignment = new CycleCountingAssignment();

        assignment = this.getCycleCountingAssignmentManager()
                .findInProgressAssignment(getOperator().getId());

        assignment.setOperator(null);
        assignment.setStatus(CycleCountStatus.Skipped);
        assignment.setStartTime(null);

        this.getCycleCountingAssignmentManager().save(assignment);

    }

    /**
     * @throws DataAccessException
     */
    private void updateDetail() throws DataAccessException {

        CycleCountingDetail detail = this.getCycleCountingAssignmentManager()
                .executeUpdateDetailCount(this.getCommandTime(),
                        this.getQuantity(), this.getOperator().getId(),
                        this.getLocationID(), this.getItemNumber(),
                        this.getUom(), this.getReasonCode());

        // Don't know the expected quantity for an 'on-the-fly' assignment
        if (detail.getCycleCountingAssignment().getSequence() != CYCLECOUNTINGONTHEFLYINDICATOR) {
            // Generate notification if cycle count is not expected.
            if (this.getQuantity() != detail.getExpectedQuantity()) {
                logNotification(this.getLocationID(), this.getItemNumber());
            }
        }

    }

    /**
     * Utility to log an error.
     * 
     * @param locationId
     *            being counted
     * @param itemNumber
     *            being counted
     */
    private void logNotification(String locationId, String itemNumber) {

        LOPArrayList lop = new LOPArrayList();

        try {
            if (this.getOperator() == null) {
                lop.add("task.operator", new String("null"));
            } else {
                lop.add("task.operator", getOperator().getOperatorIdentifier());
            }
            if (this.quantity == null) {
                lop.add("task.actual.quantity", new String("null"));
            } else {
                lop.add("task.actual.quantity", this.quantity);
            }
            if (locationId == null) {
                lop.add("task.locationScannedValue", new String("null"));
            } else {
                lop.add("task.locationScannedValue", locationId);
            }
            if (itemNumber == null) {
                lop.add("task.itemNumber", new String("null"));
            } else {
                lop.add("task.itemNumber", itemNumber);
            }

            getVoicelinkNotificationUtil()
                    .createNotification(
                            "notification.column.keyname.Process.Task",
                            "voicelink.notification.task.cyclecounting.notexpectedquanity",
                            NotificationPriority.CRITICAL, new Date(),
                            CoreErrorCode.INVALID_LOT.toString(), lop);
        } catch (Exception ex) {
            // Do nothing -- we don't care about
            // failures to post the notificaiton
        }

    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     * 
     */
    private void closeCycleCountingLaborRecord() throws DataAccessException,
            BusinessRuleException {
        CycleCountingAssignment assignment = this
                .getCycleCountingAssignmentManager()
                .findAssignmentByLocationId(this.locationID);

        if (assignment != null
                && assignment.getStatus() == CycleCountStatus.Complete) {
            this.laborManager.closeCycleCountingLaborRecord(
                    this.getCommandTime(), assignment, getOperator());
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 