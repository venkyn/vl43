/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;

/**
 * Cycle Counting region command
 * 
 * @author khazra
 * 
 */
public class CycleCountingRegionCmdRoot extends BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -3470990419494294672L;

    private Integer regionNumber;
    private Boolean allRegion;

    private RegionManager regionManager;
    private TaskFunctionManager taskFunctionManager;

    /**
     * @return the regionNumber
     */
    public Integer getRegionNumber() {
        return regionNumber;
    }

    /**
     * @param regionNumber
     *            the regionNumber to set
     */
    public void setRegionNumber(Integer regionNumber) {
        this.regionNumber = regionNumber;
    }

    /**
     * @return the allRegion
     */
    public Boolean getAllRegion() {
        return allRegion;
    }

    /**
     * @param allRegion
     *            the allRegion to set
     */
    public void setAllRegion(Boolean allRegion) {
        this.allRegion = allRegion;
    }

    /**
     * @return the regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * @param regionManager
     *            the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * @return the taskFunctionManager
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    /**
     * @param taskFunctionManager
     *            the taskFunctionManager to set
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        validateAndSaveRequest();

        buildResponse();
        return getResponse();
    }

    /**
     * Populate the response.
     * 
     * @throws DataAccessException
     *             - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        // Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     * 
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * @throws DataAccessException
     * @throws TaskCommandException
     */
    protected void validateAndSaveRequest() throws DataAccessException,
            TaskCommandException {

        // Get a list of all regions operator is authorized for
        List<Region> operatorAuthorizedRegions = getRegionManager()
                .listAuthorized(TaskFunctionType.CycleCounting,
                        getOperator().getWorkgroup().getId());

        // if no authorized regions available for operator
        if (operatorAuthorizedRegions.isEmpty()) {
            throw new TaskCommandException(
                    TaskErrorCode.CC_NO_REGION_AUTHORIZED, getOperator()
                            .getId());
        }

        if (getAllRegion()) {
            // Get a list of all Cycle Counting regions
            List<CycleCountingRegion> allCycleCountingRegions = getCycleCountingRegionManager()
                    .getAll();

            // There is no listAuthorized() function for the
            // CycleCountingRegionManager. Therefore, we
            // must get all cycle counting regions and loop through to check if
            // operator is authorized for each region.
            for (CycleCountingRegion r : allCycleCountingRegions) {
                if (operatorAuthorizedRegions.contains(r)) {
                    getOperator().getRegions().add(r);
                }
            }

            // If no Cycle Counting regions are authorized for operator throw
            // exception
            if (getOperator().getRegions().isEmpty()) {
                throw new TaskCommandException(
                        TaskErrorCode.CC_NO_REGION_AUTHORIZED, getOperator()
                                .getId());
            }

            Region pr = getOperator().getRegions().iterator().next();
            for (Region r : getOperator().getRegions()) {

                // If not all the regions has same count type and count mode
                // that operator is attempting to sign in, throw exception
                if (((CycleCountingRegion) pr).getCountMode() != ((CycleCountingRegion) r)
                        .getCountMode()
                        || ((CycleCountingRegion) pr).getCountType() != ((CycleCountingRegion) r)
                                .getCountType()) {

                    throw new TaskCommandException(
                            TaskErrorCode.CC_ALL_REGION_NOT_ALLOWED);
                }
            }

        } else {
            CycleCountingRegion region = getCycleCountingRegionManager()
                    .findRegionByNumber(getRegionNumber());

            // If region not found throw exception
            if (region == null) {
                throw new TaskCommandException(
                        TaskErrorCode.CC_REGION_NOT_FOUND, getRegionNumber());
            }

            // If operator is not authorized for the region throw exception
            if (!(operatorAuthorizedRegions.contains(region))) {
                throw new TaskCommandException(
                        TaskErrorCode.CC_NO_REGION_AUTHORIZED,
                        getRegionNumber());
            }

            getOperator().getRegions().add(region);

            // Set the current region if operator selects one region
            if (getOperator().getRegions().size() == 1) {
                getOperator().setCurrentRegion(
                        getCycleCountingRegionManager().findRegionByNumber(
                                regionNumber));
            } else {
                getOperator().setCurrentRegion(null);
            }

            Region pr = getOperator().getRegions().iterator().next();

            CycleCountingRegion regionToCheck = getCycleCountingRegionManager()
                    .findRegionByNumber(pr.getNumber());

            for (Region r : getOperator().getRegions()) {

                // If not all the regions has same count type and count mode
                // that operator is attempting to sign in, throw exception
                CycleCountingRegion regionInList = getCycleCountingRegionManager()
                        .findRegionByNumber(r.getNumber());
                if (regionToCheck.getCountMode() != regionInList.getCountMode()
                        || regionToCheck.getCountType() != regionInList
                                .getCountType()) {

                    throw new TaskCommandException(
                            TaskErrorCode.CC_NOT_COMPAT_WITH_PREV_REGION);
                }
            }
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 