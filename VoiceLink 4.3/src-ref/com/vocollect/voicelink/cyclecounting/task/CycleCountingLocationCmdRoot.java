/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingLocationCmdRoot extends BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -8547185194413348290L;

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private LaborManager laborManager;

    private Location location = null;

    private String pvid;

    private CycleCountingAssignment ccAssignment;

    private static final Semaphore SYNC_SINGLE_THREAD = new Semaphore(1, true);

    /**
     * @return the pvid
     */
    public String getPvid() {
        return pvid;
    }

    /**
     * @param pvid
     *            the pvid to set
     */
    public void setPvid(String pvid) {
        this.pvid = pvid;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location
     *            the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    @Override
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    @Override
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * @param laborManager
     *            the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        getNextCycleCountingAssignment();

        if (getLocation() == null) {
            throw new TaskCommandException(TaskErrorCode.NO_ASSIGNMENTS_FOUND);
        }
       
        buildResponse();
        
        return getResponse();
    }

    /**
     * Get next cycle counting assignment
     * 
     * @throws DataAccessException
     * @throws BusinessRuleException
     * @throws InterruptedException 
     */
    private void getNextCycleCountingAssignment() throws DataAccessException,
            BusinessRuleException, InterruptedException {
        
        // Get next available assignment
        try {
            SYNC_SINGLE_THREAD.acquire();
            ccAssignment = getCycleCountingAssignmentManager()
                    .findNextAssignment(this.getOperator().getId());

            if (ccAssignment != null) {
                // update assignment
                ccAssignment.updateStatus(CycleCountStatus.InProgress);
                ccAssignment.setStartTime(this.getCommandTime());
                ccAssignment.setOperator(this.getOperator());
                getCycleCountingAssignmentManager().save(ccAssignment);
            }
        } finally {
            SYNC_SINGLE_THREAD.release();
        }

        if (ccAssignment != null) {
            List<CycleCountingDetail> ccDetails = ccAssignment
                    .getCycleCountingDetails();
            // if more than one item at location do not send the pvid
            // there may be no item information if this location is being
            // counted 'on the fly'
            if (ccDetails.size() == 1 && ccDetails.get(0).getItem() != null) {
                setPvid(ccDetails.get(0).getItem().getSpokenVerificationCode());
            } else {
                setPvid(null);
            }

            setLocation(ccAssignment.getLocation());
        }
    }

    /**
     * Populate the response.
     * 
     * @throws DataAccessException
     *             - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        getResponse().addRecord(buildResponseRecord());

    }

    /**
     * Build Response Record.
     * 
     * @return - response record
     * @throws DataAccessException
     *             - Database exceptions
     */
    protected ResponseRecord buildResponseRecord() throws DataAccessException {

        ResponseRecord record = new TaskResponseRecord();

        Location loc = getLocation();

        record.put("preAisle", loc.getPreAisle());
        record.put("aisle", loc.getAisle());
        record.put("postAisle", loc.getPostAisle());
        record.put("slot", loc.getSlot());
        record.put("locationCheckDigits", loc.getCheckDigits());
        record.put("locationID", loc.getScannedVerification());
        record.put("pvid", getPvid());

        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 