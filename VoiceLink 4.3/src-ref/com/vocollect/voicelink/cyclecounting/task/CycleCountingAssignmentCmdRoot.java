/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationItem;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.UnitOfMeasure;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.UnitOfMeasureManager;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailManager;
import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author khazra
 * 
 */
public class CycleCountingAssignmentCmdRoot extends
        BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -3470990419494294672L;

    private Long spokenScannedIndicator;
    private String location;
    private String locationCheckDigit;

    private CycleCountingAssignment assignment;

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;
    private CycleCountingDetailManager cycleCountingDetailManager;
    private LocationManager locationManager;
    private ItemManager itemManager;
    private LaborManager laborManager;
    private UnitOfMeasureManager unitOfMeasureManager;
    
    
    /**
     * @return the unitOfMeasureManager
     */
    public UnitOfMeasureManager getUnitOfMeasureManager() {
        return unitOfMeasureManager;
    }

    /**
     * @param unitOfMeasureManager the unitOfMeasureManager to set
     */
    public void setUnitOfMeasureManager(UnitOfMeasureManager unitOfMeasureManager) {
        this.unitOfMeasureManager = unitOfMeasureManager;
    }

    /**
     * @return the spokenScannedIndicator
     */
    public Long getSpokenScannedIndicator() {
        return spokenScannedIndicator;
    }

    /**
     * @param spokenScannedIndicator
     *            the spokenScannedIndicator to set
     */
    public void setSpokenScannedIndicator(Long spokenScannedIndicator) {
        this.spokenScannedIndicator = spokenScannedIndicator;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location
     *            the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return the locationCheckDigit
     */
    public String getLocationCheckDigit() {
        return locationCheckDigit;
    }

    /**
     * @return the locationManager
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * @param locationManager
     *            the locationManager to set
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the itemManager
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * @param itemManager
     *            the itemManager to set
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    /**
     * @param locationCheckDigit
     *            the locationCheckDigit to set
     */
    public void setLocationCheckDigit(String locationCheckDigit) {
        this.locationCheckDigit = locationCheckDigit;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    @Override
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    @Override
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the cycleCountingDetailManager
     */
    public CycleCountingDetailManager getCycleCountingDetailManager() {
        return cycleCountingDetailManager;
    }

    /**
     * @param cycleCountingDetailManager
     *            the cycleCountingDetailManager to set
     */
    public void setCycleCountingDetailManager(
            CycleCountingDetailManager cycleCountingDetailManager) {
        this.cycleCountingDetailManager = cycleCountingDetailManager;
    }

    /**
     * Getter for the laborManager property.
     * 
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * 
     * @param laborManager
     *            the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildAssignmentList();
        openCycleCountingLaborRecord();
        buildResponse();
        return getResponse();
    }

    /**
     * Builds the response
     */
    private void buildResponse() {
        
        if (assignment == null) {
            getResponse().addRecord(buildEmptyResponseRecord());
        } else {
            List<CycleCountingDetail> assignmentDetails = assignment
                    .getCycleCountingDetails();

            if (assignmentDetails != null && assignmentDetails.size() > 0) {
                for (CycleCountingDetail assignmentDetail : assignmentDetails) {
                    getResponse().addRecord(
                            buildResponseRecord(assignmentDetail));
                }
            } else {
                getResponse().addRecord(buildResponseRecord(null));
            }
        }
    }

    /**
     * @param assignmentDetail
     *            The assignment detail record
     * 
     * @return record build from Cycle Counting detail
     */
    private ResponseRecord buildResponseRecord(
            CycleCountingDetail assignmentDetail) {
        ResponseRecord record = new TaskResponseRecord();

        String countMode = "";
        if (assignment.getRegion() == null
                || assignment.getRegion().getCountType() == CountType.Blind) {
            countMode = "B";
        } else {
            countMode = "K";
        }

        record.put("locationID", assignment.getLocation()
                .getSpokenVerification());
        record.put("preAisle", assignment.getLocation().getPreAisle());
        record.put("aisle", assignment.getLocation().getAisle());
        record.put("postAisle", assignment.getLocation().getPostAisle());
        record.put("slot", assignment.getLocation().getSlot());
        record.put("locationCheckDigit", assignment.getLocation()
                .getCheckDigits());
        if (assignmentDetail != null) {
            if (assignmentDetail.getItem() != null) {
                record.put("pvid", assignmentDetail.getItem()
                        .getScanVerificationCode());
                record.put("itemNumber", assignmentDetail.getItem().getNumber());
                record.put("itemDescription", assignmentDetail.getItem()
                        .getDescription());
                record.put("itemUPC", assignmentDetail.getItem().getUpc());
            }
            record.put("expectedQuantity",
                    assignmentDetail.getExpectedQuantity());
            record.put("uom", assignmentDetail.getUnitOfMeasure());
        } else {
            record.put("pvid", "");
            record.put("itemNumber", "");
            record.put("itemDescription", "");
            record.put("itemUPC", "");
            record.put("expectedQuantity", "");
            record.put("uom", "");

        }
        record.put("countMode", countMode);
        record.put("tie", "");
        record.put("high", "");
        record.put("image", "");

        return record;

    }

    /**
     * @return Empty response
     */
    private ResponseRecord buildEmptyResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();

        record.put("locationID", "");
        record.put("preAisle", "");
        record.put("aisle", "");
        record.put("postAisle", "");
        record.put("slot", "");
        record.put("locationCheckDigit", "");
        record.put("pvid", "");
        record.put("itemNumber", "");
        record.put("itemDescription", "");
        record.put("itemUPC", "");
        record.put("expectedQuantity", "");
        record.put("uom", "");
        record.put("countMode", "");
        record.put("tie", "");
        record.put("high", "");
        record.put("image", "");

        return record;

    }

    /**
     * Builds a list of assignments available to the operator. If the work is
     * operator directed then this method builds the assignment based on the
     * location supplied, and if the work is system directed this method is
     * going to next in-progress assignment for the operator.
     * 
     * If location number is specified and there are no cycle counting
     * assignment for the location present this method will create an assignment
     * for that location.
     * 
     * @throws DataAccessException
     */
    private void buildAssignmentList() throws DataAccessException,
            BusinessRuleException, TaskCommandException {
        if (location != null && location.length() > 0) {
            getAssignmentForLocation();
        } else {
            getNextAssignmentForOperator();
        }
    }

    /**
     * @throws DataAccessException
     * 
     */
    private void getNextAssignmentForOperator() throws DataAccessException {
        assignment = cycleCountingAssignmentManager
                .findInProgressAssignment(getOperator().getId());

    }

    /**
     * Get assignment for the spoken location
     */
    private void getAssignmentForLocation() throws DataAccessException,
            BusinessRuleException, TaskCommandException {
        Location loc = null;
        if (getSpokenScannedIndicator() != null && getSpokenScannedIndicator() == 1) { // If the location is scanned
            loc = locationManager.findLocationByScanned(location);
        } else { // If the location is spoken
            loc = locationManager.findLocationBySpoken(location,
                locationCheckDigit);
        }
        
        if (loc == null) {
            throw new TaskCommandException(TaskErrorCode.CC_INVALID_LOCATION,
                    getOperator().getId());
        }

        assignment = cycleCountingAssignmentManager
                .findAvailableAssignmentByLocation(location);

        if (assignment == null) {
            createAssignment();
        } else {
            if (getOperator().getRegions().contains(assignment.getRegion())) {
                assignment.updateStatus(CycleCountStatus.InProgress);
                assignment.setStartTime(this.getCommandTime());
                assignment.setOperator(this.getOperator());
            } else {
                assignment = null;
            }
        }
    }

    /**
     * @throws DataAccessException
     * 
     */
    private void createAssignment() throws DataAccessException,
            BusinessRuleException, TaskCommandException {
        
        Location loc = null;
        if (getSpokenScannedIndicator() != null && getSpokenScannedIndicator() == 1) { // If the location is scanned
            loc = locationManager.findLocationByScanned(location);
        } else { // If the location is spoken
            loc = locationManager.findLocationBySpoken(location,
                locationCheckDigit);
        }
        

        if (loc == null) {
            throw new TaskCommandException(TaskErrorCode.CC_INVALID_LOCATION);
        }

        Set<LocationItem> locItems = loc.getItems();

        assignment = new CycleCountingAssignment();
        assignment
                .setCycleCountingDetails(new ArrayList<CycleCountingDetail>());

        Region region = getOperator().getRegions().iterator().next();

        CycleCountingRegion ccRegion = getCycleCountingRegionManager()
                .findRegionByNumber(region.getNumber());

        assignment.setRegion(ccRegion);
        assignment.setOperator(getOperator());
        assignment.setStartTime(this.getCommandTime());
        assignment.setLocation(loc);
        assignment.setStatus(CycleCountStatus.InProgress);

        List<CycleCountingDetail> details = assignment.getCycleCountingDetails();
        List<UnitOfMeasure> uoms = this.getUnitOfMeasureManager().getPrimaryDAO().listUnitsOfMeasure();
        
        //use the Item-Location mapping if available
        if (locItems != null && locItems.size() > 0) {
            for (LocationItem locItem : locItems) {
                if (uoms != null && uoms.size() > 0) {
                    // Add a detail record for each UOM in the system.
                    for (UnitOfMeasure uom : uoms) {
                        CycleCountingDetail detail = new CycleCountingDetail();
                        detail.setCycleCountingAssignment(assignment);
                        detail.setItem(locItem.getItem());
                        detail.setUnitOfMeasure(uom.getName());
                        details.add(detail);
                    }
                } else {
                    CycleCountingDetail detail = new CycleCountingDetail();
                    detail.setCycleCountingAssignment(assignment);
                    detail.setItem(locItem.getItem());
                    details.add(detail);
                }
            }
        } else {
            if (uoms != null && uoms.size() > 0) {
                // Add a detail record for each UOM in the system.
                for (UnitOfMeasure uom : uoms) {
                    CycleCountingDetail detail = new CycleCountingDetail();
                    detail.setCycleCountingAssignment(assignment);
                    detail.setUnitOfMeasure(uom.getName());
                    details.add(detail);
                }
            } else {
                CycleCountingDetail detail = new CycleCountingDetail();
                detail.setCycleCountingAssignment(assignment);
                details.add(detail);
            }
        }        
        assignment.setCycleCountingDetails(details);
        cycleCountingAssignmentManager.save(assignment);
    }

    /**
     * 
     */
    private void openCycleCountingLaborRecord() throws DataAccessException,
            BusinessRuleException {
        // Open a labor record here if a location is being provided by the operator
        if (assignment != null
                && this.getLocation().length() >= 0) {
            this.laborManager.openCycleCountingLaborRecord(
                    this.getCommandTime(), this.getOperator(), assignment);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 