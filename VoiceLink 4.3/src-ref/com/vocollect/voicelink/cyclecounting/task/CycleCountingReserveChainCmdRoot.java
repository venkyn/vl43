/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

/**
 * @author khazra
 *
 */
public class CycleCountingReserveChainCmdRoot extends BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = 7066248498466219059L;
    
    private String locationID;
    private String itemNumber;
    
    
    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }




    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }




    /**
     * @return the itemNumber
     */
    public String getItemNumber() {
        return itemNumber;
    }




    /**
     * @param itemNumber the itemNumber to set
     */
    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }




    @Override
    protected Response doExecute() throws Exception {
        buildResponse();
        return getResponse();
    }

    /**
     * Builds the response
     */
    private void buildResponse() {
        getResponse().addRecord(buildEmptyResponseRecord());
    }
    
    /**
     * @return Empty response
     */
    private ResponseRecord buildEmptyResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();

        record.put("locationID", "");
        record.put("palletID", "");
        record.put("item", "");
        record.put("quantity", "");

        return record;

    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 