/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.task.command.BaseCycleCountingTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingModeCmdRoot extends BaseCycleCountingTaskCommand {

    /**
     * 
     */
    private static final long serialVersionUID = -3470990419494294672L;
    
    private LaborManager laborManager;
    
    /**
     * Getter for the laborManager property.
     * 
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * 
     * @param laborManager
     *            the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        openLaborRecord();
        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * 
     * @throws DataAccessException
     *             - Database exception
     */
    protected void buildResponse() throws DataAccessException {

        Region pr = this.getOperator().getRegions().iterator().next();

        getResponse().addRecord(
                buildResponseRecord(getCycleCountingRegionManager()
                        .findRegionByNumber(pr.getNumber())));
    }

    /**
     * Build Response Record.
     * 
     * @param r
     *            - region to build record for
     * @return - response record
     * @throws DataAccessException
     *             - Database exceptions
     */
    protected ResponseRecord buildResponseRecord(CycleCountingRegion r)
            throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        // convert enum to value required by voice application
        // SystemDirected=H : OperatorDirected=P
        record.put("modeIndicator", (r.getCountMode().toValue() == 1 ? "H"
                : "P"));

        return record;
    }
    
    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    private void openLaborRecord() throws DataAccessException,
            BusinessRuleException {
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                this.getOperator(), OperatorLaborActionType.CycleCounting);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 