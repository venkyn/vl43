/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;

import java.util.List;
import java.util.Map;

/**
 * @author amukherjee
 *
 */
    public interface CycleCountingDetailExportTransportManagerRoot extends CycleCountingDetailManager  {

    
    /**
     * get cycle counting Detail assignment records to export.
     * 
     * @return list of cycle counting detail records
     * @throws DataAccessException - database errors
     */
    public List<Map<String, Object>> listDetails() throws DataAccessException;
    
    /**
     * update pick details to be exported.
     * 
     * @return - number of container updated
     * @throws DataAccessException - database exceptions
     */
    public Integer updateDetailsExportStatus() throws DataAccessException;
        

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 