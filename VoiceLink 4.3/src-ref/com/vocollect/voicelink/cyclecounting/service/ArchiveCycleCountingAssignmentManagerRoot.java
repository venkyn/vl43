/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.cyclecounting.dao.ArchiveCycleCountingAssignmentDAO;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;

import java.util.Date;


/**
 * @author ktanneru
 *
 */
public interface ArchiveCycleCountingAssignmentManagerRoot extends
GenericManager<ArchiveCycleCountingAssignment, ArchiveCycleCountingAssignmentDAO>, DataProvider {

    /**
     * deletes archive cycle counting assignments.
     *
     * @param createdDate - date to check for
     * @param status - status of cycle counting assignments
     * @return - Number of row deleted
     * @throws DataAccessException - Database Access Exceptions
     */
    Integer updateOlderThan(Date createdDate,
        CycleCountStatus status) throws DataAccessException;

    /**
     * Purges cycle counting assignments based on date and status.
     *
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of cycle counting assignments purged
     */
    public int executePurge(CycleCountStatus status,
                            Date olderThan);
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 