/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingAssignmentDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;

import java.util.Date;

/**
 * @author pkolonay
 * 
 */
public interface CycleCountingAssignmentManagerRoot extends
        GenericManager<CycleCountingAssignment, CycleCountingAssignmentDAO>,
        DataProvider {

    /**
     * Find the next cyclecounting assignment to issue out for a location.
     * 
     * @param operatorId
     *            - operator to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findNextAssignment(Long operatorId)
            throws DataAccessException;

    /**
     * @param operatorid
     *            - the operator id
     * @return - the cycle counting assignment
     * @throws DataAccessException
     */
    public CycleCountingAssignment findInProgressAssignment(Long operatorid)
            throws DataAccessException;

    /**
     * Find the next cyclecounting assignment to issue out for a location.
     * 
     * @param operatorId
     *            - operator to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findAvailableAssignmentByLocation(
            String operatorId) throws DataAccessException;

    /**
     * Find the next cyclecounting assignment to issue out for a location.
     * 
     * @param location
     *            - location to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findAssignmentByScannedLocation(
            Location location) throws DataAccessException;

    /**
     * Find the next cyclecounting assignment to issue out for a location.
     * 
     * @param locationId
     *            - location to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findAssignmentByLocationId(String locationId)
            throws DataAccessException;

    /**
     * Find in-progress cyclecounting assignment to block import.
     * 
     * @param scannedVerification
     *            - location id to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findInProgressAssignmentByScannedLocation(
            String scannedVerification) throws DataAccessException;

    
    /**
     * Find assignments that have a status that allows them to be updated.
     * 
     * @param scannedVerification
     *            - location id to get cyclecount for
     * @return - return assignment if found otherwise null
     * @throws DataAccessException
     *             - database exceptions
     */
    public CycleCountingAssignment findAssignmentByUpdateableStatusAndScannedLocation(
            String scannedVerification) throws DataAccessException;
    
    
    
    /**
     * @param obj
     *            - the obj
     * @param enum1
     *            - the enum
     * @throws BusinessRuleException
     */
    void executeUpdateStatus(CycleCountingAssignment obj, CycleCountStatus enum1)
            throws BusinessRuleException;

    /**
     * @param cycleCountingAssignment
     *            - the cycleCountingAssignment
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    void executeUpdateCycleCountingAssignment(
            CycleCountingAssignment cycleCountingAssignment)
            throws BusinessRuleException, DataAccessException,
            BusinessRuleException, DataAccessException;

    /**
     * @param cycleCountingAssignment
     *            - the cycleCountingAssignment
     * @param operatorId
     *            - the operatorId
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    void executeUpdateOperator(CycleCountingAssignment cycleCountingAssignment,
            Long operatorId) throws DataAccessException, BusinessRuleException;

    /**
     * @param countTime
     *            - the count time
     * @param actualQuantity
     *            - the actual quantity
     * @param operatorId
     *            - the operator id
     * @param spokenVerification
     *            - the spoken verification
     * @param itemNumber
     *            - the item number
     * @param uom
     *            - the unit of measure
     * @param reasonCode
     *            - the reason code
     * @return CycleCountingDetail - the detail that was updated.
     * @throws DataAccessException
     */
    CycleCountingDetail executeUpdateDetailCount(Date countTime, int actualQuantity,
            Long operatorId, String spokenVerification, String itemNumber,
            String uom, String reasonCode) throws DataAccessException;

    /**
     * @param operatorId
     *            - the operatorId
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    void executeClearInProgressAssignment(Long operatorId)
            throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 