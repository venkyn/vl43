/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.cyclecounting.dao.ArchiveCycleCountingLaborDAO;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingLabor;

import java.util.Date;
import java.util.List;

/**
 * @author ktanneru
 * 
 */
public interface ArchiveCycleCountingLaborManagerRoot extends
    GenericManager<ArchiveCycleCountingLabor, ArchiveCycleCountingLaborDAO>,
    DataProvider {

    /**
     * Returns a list of archive labor summary report objects based on
     * OperatorId and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;

    /**
     * Returns a list of archive labor detail report objects based on operatorId
     * and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(String operatorIdentifier,
                                                Date startDate,
                                                Date endDate)
        throws DataAccessException;

    /**
     * Returns a list of archive report objects based on operatorId, a region
     * Number and a date range.
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDurationByOperRegionDates(String operatorIdentifier,
                                                      Integer regionNumber,
                                                      Date startDate,
                                                      Date endDate)
        throws DataAccessException;

    /**
     * Returns the sum of archive slots (locations) counted based on operatorId
     * in a date range.
     * @param operatorLaborId operator
     * @param startTime start of date range
     * @param endTime end of date range
     * @return list of objects
     * @throws DataAccessException on DB failure
     */
    Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                           Date startTime,
                                           Date endTime)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 