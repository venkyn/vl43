/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingLaborDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;

import java.util.Date;
import java.util.List;

/**
 * @author khazra
 * 
 */
public interface CycleCountingLaborManagerRoot extends
    GenericManager<CycleCountingLabor, CycleCountingLaborDAO>, DataProvider {

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the closed assignment labor records for the given operator or
     * returns null if there is no closed assignment labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listRecordsByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the open assignment labor record for the given assignment. or
     * returns null if there is no open assignment labor record.
     * 
     * @param assignmentId - The Id of the assignment
     * @return The requested assignment laobr record or null
     * @throws DataAccessException on any failure
     */
    CycleCountingLabor findOpenRecordByAssignmentId(long assignmentId)
        throws DataAccessException;

    /**
     * Retrieves closed labor records based on breakLaborId.
     * 
     * @param breakLaborId the break id
     * @return Cycle Counting labor record
     * @throws DataAccessException
     */
    CycleCountingLabor findClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException;

    /**
     * Sums the number of locations counter by operatorLaborId.
     * 
     * @param operatorLaborId OperatorLaborId
     * @return Integer value of the locations counted
     * @throws DataAccessException database access exception
     */
    Integer sumLocationsCounted(long operatorLaborId)
        throws DataAccessException;

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorLaborId - the ID of the Operator
     * @return the requested list of assignment labor records or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException;

    /**
     * Get all assignment labor records associated with any of the specified
     * operator labor records.
     * 
     * @param decorator - additional query instructions.
     * @return the List of assignment labor records.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listAssignmentLabor(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Returns a list of report objects based on OperatorId and a date range.
     * 
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
    
    /**
     * Returns a list of report objects based on OperatorId and a date range.
     * 
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                                Date startDate, Date endDate) 
                                                throws DataAccessException;

    /**
     * Returns a list of report objects based on operatorId, a region Number and
     * a date range.
     * 
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDurationByOperRegionDates(Long operatorIdentifier,
                                                      Integer regionNumber,
                                                      Date startDate,
                                                      Date endDate)
        throws DataAccessException;

    /**
     * Returns the sum of locations counted based on operatorId in a date range.
     * 
     * @param operatorLaborId operator
     * @param startTime start of date range
     * @param endTime end of date range
     * @return list of objects
     * @throws DataAccessException on DB failure
     */
    Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                           Date startTime,
                                           Date endTime)
        throws DataAccessException;

    /**
     * Opens the Labor Record for an operator performing cycle counting.
     * 
     * @param startTime - Assignment Start time.
     * @param operator - Operator that is picking the assignment.
     * @param assignment - The assignment object.
     * @param operLaborRecord - The operator labor to associate the cc
     *            assignment labor.
     * @throws DataAccessException - if db failure.
     * @throws BusinessRuleException - if save fails.
     */
    void openLaborRecord(Date startTime,
                         Operator operator,
                         CycleCountingAssignment assignment,
                         OperatorLabor operLaborRecord)
        throws DataAccessException, BusinessRuleException;

    /**
     * Close the labor record.
     * @param endTime Assignment End Time
     * @param assignment CycleCountingAssignment
     * @return CycleCountingLabor Record
     * @throws DataAccessException DataAccessException
     * @throws BusinessRuleException BusinessRuleException
     */
    CycleCountingLabor closeLaborRecord(Date endTime,
                                        CycleCountingAssignment assignment)
        throws DataAccessException, BusinessRuleException;

    /**
     * @param al - Assignment Labor record to recalculate values and save
     * 
     * @throws DataAccessException - if db failure
     * @throws BusinessRuleException - if save fails.
     */
    void recalculateAggregatesAndSave(CycleCountingLabor al) throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 