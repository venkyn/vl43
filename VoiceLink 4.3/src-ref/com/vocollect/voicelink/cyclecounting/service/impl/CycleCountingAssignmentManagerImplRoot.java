/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.ReasonCodeManager;
import com.vocollect.voicelink.cyclecounting.CycleCountingErrorCode;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingAssignmentDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

import java.util.Date;
import java.util.List;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingAssignmentManagerImplRoot extends
        GenericManagerImpl<CycleCountingAssignment, CycleCountingAssignmentDAO>
        implements CycleCountingAssignmentManager {

    private ItemManager itemManager;

    private LocationManager locationManager;

    private OperatorManager operatorManager;

    private CycleCountingRegionManager cycleCountingRegionManager;

    private CycleCountingDetailManager cycleCountingDetailManager;

    private ReasonCodeManager reasonCodeManager;

    /**
     * @return the reasonCodeManager
     */
    public ReasonCodeManager getReasonCodeManager() {
        return reasonCodeManager;
    }

    /**
     * @param reasonCodeManager
     *            the reasonCodeManager to set
     */
    public void setReasonCodeManager(ReasonCodeManager reasonCodeManager) {
        this.reasonCodeManager = reasonCodeManager;
    }

    /**
     * @return the cycleCountingDetailManager
     */
    public CycleCountingDetailManager getCycleCountingDetailManager() {
        return cycleCountingDetailManager;
    }

    /**
     * @param cycleCountingDetailManager
     *            the cycleCountingDetailManager to set
     */
    public void setCycleCountingDetailManager(
            CycleCountingDetailManager cycleCountingDetailManager) {
        this.cycleCountingDetailManager = cycleCountingDetailManager;
    }

    /**
     * @return the itemManager
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * @param itemManager
     *            the itemManager to set
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    /**
     * @return the locationManager
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * @param locationManager
     *            the locationManager to set
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager
     *            the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return the cycleCountingRegionManager
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * @param cycleCountingRegionManager
     *            the cycleCountingRegionManager to set
     */
    public void setCycleCountingRegionManager(
            CycleCountingRegionManager cycleCountingRegionManager) {
        this.cycleCountingRegionManager = cycleCountingRegionManager;
    }

    /**
     * @param primaryDAO
     *            - the primary DAO
     */
    public CycleCountingAssignmentManagerImplRoot(
            CycleCountingAssignmentDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #executeUpdateStatus(com.vocollect.voicelink
     * .cyclecounting.model.CycleCountingAssignment,
     * com.vocollect.voicelink.cyclecounting.model.CycleCountStatus)
     */
    @Override
    public void executeUpdateStatus(CycleCountingAssignment cycleCountAssignment,
                                    CycleCountStatus status) throws BusinessRuleException {

        if (((cycleCountAssignment.getStatus() == CycleCountStatus.Available)
                || (cycleCountAssignment.getStatus() == CycleCountStatus.Unavailable) 
                || (cycleCountAssignment.getStatus() == CycleCountStatus.Skipped)) 
                && cycleCountAssignment.getStatus() != status) {
            cycleCountAssignment.setStatus(status);
            if (status == CycleCountStatus.Canceled) {
                for (CycleCountingDetail d : cycleCountAssignment
                        .getCycleCountingDetails()) {
                    d.setStatus(CycleCountDetailStatus.Canceled);
                }
            }
        } else {
            throw new BusinessRuleException(
                    CycleCountingErrorCode.EDIT_ASSIGNMENT_INVALID_STATUS,
                    new UserMessage(
                            "cyclecounting.updateStatus.error.cannot.update.status"));
        }
    }

    /**
     * 
     * {@inheritDoc}
     * @see
     * com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager
     * #
     * executeUpdateCycleCountingAssignment(com.vocollect.voicelink.cyclecounting
     * .model.CycleCountingAssignment)
     */
    @Override
    public void executeUpdateCycleCountingAssignment(
            CycleCountingAssignment cycleCountingAssignment)
            throws DataAccessException, BusinessRuleException {
        
        if (cycleCountingAssignment.getStatus() == CycleCountStatus.Canceled) {
            for (CycleCountingDetail d : cycleCountingAssignment
                    .getCycleCountingDetails()) {
                d.setStatus(CycleCountDetailStatus.Canceled);
            }
        }
        this.save(cycleCountingAssignment);
    }

    /**
     * {@inheritDoc}
     * @see
     * com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.lang.
     * Object)
     */
    @Override
    public Object save(CycleCountingAssignment instance)
            throws BusinessRuleException, DataAccessException {

        this.getPrimaryDAO().save(instance);
        return instance;
    }

    /**
     * {@inheritDoc}
     * @see
     * com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.util.
     * List)
     */
    @Override
    public Object save(List<CycleCountingAssignment> assignments)
            throws BusinessRuleException, DataAccessException {
        
        for (CycleCountingAssignment assignment : assignments) {
            calculateAssignmentInfo(assignment);
        }
        this.getPrimaryDAO().save(assignments);

        for (CycleCountingAssignment assignment : assignments) {
            if (assignment.getSequence() == -1L) {
                assignment.setSequence(assignment.getId());
            }
        }
        this.getPrimaryDAO().save(assignments);

        return null;
    }

    /**
     * Calculates the summary information of an assignment based on the picks
     * within the assignment.
     * 
     * @param assignment
     *            - assignment to calculate info for
     */
    protected void calculateAssignmentInfo(CycleCountingAssignment assignment) {

        for (CycleCountingDetail d : assignment.getCycleCountingDetails()) {

            // ensure relationship is completely set up
            if (d.getCycleCountingAssignment() == null) {
                d.setCycleCountingAssignment(assignment);
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot#findNextAssignment(java.lang.Long)
     */
    @Override
    public CycleCountingAssignment findNextAssignment(Long operatorId)
            throws DataAccessException {

        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        // assignments for this specific operator
        counts = this.getPrimaryDAO().listAssignmentsByOperatorRegion(
                operatorId);

        // if none were found get any assignments in the operator's region
        if ((counts == null) || (counts.size() == 0)) {
            counts = this.getPrimaryDAO().listAssignmentsByRegion(operatorId);
        }

        // ...and finally check if there are any skipped assignments in the
        // region that the operator is in.
        if ((counts == null) || (counts.size() == 0)) {
            counts = this.getPrimaryDAO().listSkippedAssignmentsByRegion(
                    operatorId);
        }

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findInProgressAssignment(java.lang.Long)
     */
    @Override
    public CycleCountingAssignment findInProgressAssignment(Long operatorId)
            throws DataAccessException {
        
        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = this.getPrimaryDAO().listInProgressAssignmentsByOperator(
                operatorId);

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findInProgressAssignmentByScannedLocation(java.lang.String)
     */
    @Override
    public CycleCountingAssignment findInProgressAssignmentByScannedLocation(
            String scannedVerification) throws DataAccessException {
        List<CycleCountingAssignment> counts = null;

        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = this.getPrimaryDAO()
                .listInProgressAssignmentByScannedLocation(scannedVerification);
                 
        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findAssignmentByScannedLocationAndUpdateableStatus(java.lang.Long)
     */
    @Override
    public CycleCountingAssignment findAssignmentByUpdateableStatusAndScannedLocation(
            String scannedVerification) throws DataAccessException {


        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = 
            this.getPrimaryDAO().listAssignmentByScannedLocationAndUpdateableStatus(
                                 scannedVerification);

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findAvailableAssignmentByLocation(java.lang.String)
     */
    @Override
    public CycleCountingAssignment findAvailableAssignmentByLocation(
            String locationId) throws DataAccessException {
        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = this.getPrimaryDAO().listAvailableAssignmentByLocation(
                locationId);

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findAvailableAssignmentByLocation(java.lang.String)
     */
    @Override
    public CycleCountingAssignment findAssignmentByScannedLocation(
            Location location) throws DataAccessException {

        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = this.getPrimaryDAO().listAssignmentByScannedLocation(
                location.getScannedVerification());

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #findAvailableAssignmentByLocation(java.lang.String)
     */
    /**
     * @param locationId
     *            the scanned or spoken location id
     * @return any cycle counting assignment associated to the location or null
     * @throws DataAccessException
     */
    @Override
    public CycleCountingAssignment findAssignmentByLocationId(String locationId)
            throws DataAccessException {

        List<CycleCountingAssignment> counts = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);

        counts = this.getPrimaryDAO().listAssignmentByLocation(locationId);

        if (counts == null) {
            return null;
        } else if (counts.isEmpty()) {
            return null;
        } else {
            return counts.get(0);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #executeUpdateOperator(com.vocollect.voicelink
     * .cyclecounting.model.CycleCountingAssignment, java.lang.Long)
     */
    @Override
    public void executeUpdateOperator(
            CycleCountingAssignment cycleCountAssignment, Long operatorId)
            throws DataAccessException, BusinessRuleException {
        if (cycleCountAssignment.getStatus() == CycleCountStatus.Available
                || cycleCountAssignment.getStatus() == CycleCountStatus.Unavailable 
                || cycleCountAssignment.getStatus() == CycleCountStatus.Skipped) {

            if (operatorId == 0L) {
                cycleCountAssignment.setOperator(null);
            } else {
                cycleCountAssignment.setOperator(this.getOperatorManager().get(
                        operatorId));
            }
        } else {
            throw new BusinessRuleException(
                    CycleCountingErrorCode.EDIT_ASSIGNMENT_INVALID_STATUS,
                    new UserMessage(
                            "cyclecounting.changeOperator.error.cannot.change.operator"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.
     * CycleCountingAssignmentManagerRoot
     * #executeUpdateDetailCount(java.util.Date, int, java.lang.Long,
     * java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public CycleCountingDetail executeUpdateDetailCount(Date countTime,
            int actualQuantity, Long operatorId, String spokenVerification,
            String itemNumber, String uom, String reasonCode)
            throws DataAccessException {

        CycleCountingDetail foundDetail = null;
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);
        CycleCountingAssignment ccA = findInProgressAssignment(operatorId);

        // Assume that this will complete the count assignment
        boolean isComplete = true;

        // update the detail. Item may be null for 'on the fly' counting
        for (CycleCountingDetail detail : ccA.getCycleCountingDetails()) {
            if (detail.getItem() != null) {
                if (detail.getItem().getNumber().equals(itemNumber)) {
                    if ((detail.getUnitOfMeasure() != null && detail
                            .getUnitOfMeasure().equals(uom))
                            || detail.getUnitOfMeasure() == null) {
                        updateDetailFields(detail, countTime, actualQuantity,
                                reasonCode);
                        foundDetail = detail;
                    }
                }
            } else {
                if (detail.getUnitOfMeasure() != null
                        && detail.getUnitOfMeasure().equals(uom)
                        || detail.getUnitOfMeasure() == null) {
                    updateDetailFields(detail, countTime, actualQuantity,
                            reasonCode);
                    foundDetail = detail;
                }
            }

            // more details needed before complete assignment
            if (detail.getStatus() == CycleCountDetailStatus.NotCounted) {
                // Still something at this location to count.
                isComplete = false;
            }
        }

        // update counting assignment status and end time.
        if (isComplete) {
            ccA.setStatus(CycleCountStatus.Complete);
            ccA.setEndTime(countTime);
        }
        this.getPrimaryDAO().save(ccA);

        return foundDetail;
    }

    /**
     * @param detail - the detail to update
     * @param countTime - the countTime to use.
     * @param actualQuantity - the quantity to use
     * @param reasonCode - the reason code to use.
     * @throws NumberFormatException
     * @throws DataAccessException
     */
    private void updateDetailFields(CycleCountingDetail detail,
            Date countTime, int actualQuantity, String reasonCode)
            throws NumberFormatException, DataAccessException {
        detail.setActualQuantity(actualQuantity);
        detail.setCountTime(countTime);
        detail.setStatus(CycleCountDetailStatus.Counted);
        if (reasonCode.length() != 0) {
            detail.setReason(this.getReasonCodeManager().findByReasonNumber(
                    Integer.parseInt(reasonCode)));
        }
    }
    
    /**
     * when operator signs off
     * 
     * @param operatorId
     *            - the operator id
     * @throws DataAccessException
     */
    @Override
    public void executeClearInProgressAssignment(Long operatorId)
            throws DataAccessException {

        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(1);
        CycleCountingAssignment ccA = findInProgressAssignment(operatorId);

        if (ccA != null) {
            for (CycleCountingDetail detail : ccA.getCycleCountingDetails()) {
                if (detail.getCountTime() != null) {
                    detail.setActualQuantity(0);
                    detail.setCountTime(null);
                    detail.setStatus(CycleCountDetailStatus.NotCounted);
                    detail.setExportStatus(ExportStatus.NotExported);
                    detail.setReason(null);
                }
            }
            ccA.setOperator(null);
            ccA.setStartTime(null);
            ccA.setStatus(CycleCountStatus.Available);
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 