/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailManager;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingDetailManagerImplRoot extends
        GenericManagerImpl<CycleCountingDetail, CycleCountingDetailDAO>
        implements CycleCountingDetailManager {

    private ItemManager     itemManager;
    
    /**
     * @param primaryDAO - the primaryDAO
     */
    public CycleCountingDetailManagerImplRoot(CycleCountingDetailDAO primaryDAO) {
        super(primaryDAO);
    }
    

    /**
     * @return the itemManager
     */
    public ItemManager getItemManager() {
        return itemManager;
    }


    /**
     * @param itemManager the itemManager to set
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 