/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.cyclecounting.dao.ArchiveCycleCountingAssignmentDAO;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingAssignmentManager;

import java.util.Date;
import java.util.List;


/**
 * @author ktanneru
 * 
 */
public class ArchiveCycleCountingAssignmentManagerImplRoot
        extends
        GenericManagerImpl<ArchiveCycleCountingAssignment, ArchiveCycleCountingAssignmentDAO>
        implements ArchiveCycleCountingAssignmentManager {

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
        VoiceLinkPurgeArchiveManagerImpl.class);
    
    /**
     * Passes the ArchiveCycleCountingAssignmentDAO as the primaryDAO.
     * Constructor.
     * 
     * @param primaryDAO
     *            ArchiveCycleCountingAssignmentDAO object
     */
    public ArchiveCycleCountingAssignmentManagerImplRoot(
            ArchiveCycleCountingAssignmentDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.ArchiveCycleCountingAssignmentManagerRoot
     * #listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      java.util.Date,
     *      com.vocollect.voicelink.cyclecounting.model.CycleCountStatus)
     */
    @Override
    public Integer updateOlderThan(Date createdDate, CycleCountStatus status)
        throws DataAccessException {
        return getPrimaryDAO().updateOlderThan(createdDate, status);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingAssignmentManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.cyclecounting.model.CycleCountStatus,
     *      java.util.Date)
     */
    @Override
    public int executePurge(CycleCountStatus status, Date olderThan) {
        int returnRecords = 0;

        // Get List of Archive Cycle Counting Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Fetching Archive Cycle Counting Assignments to Purge :::");
        }
        try {
            returnRecords = updateOlderThan(olderThan, status);
        } catch (DataAccessException e) {
            log.warn("Error getting Archive "
                + " Cycle Counting Assignments from database to purge: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords
                + " Archive Cycle Counting Assignments for Purge :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getAllData(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        return this.getPrimaryDAO().listArchiveCycleCountingAssignments(
            new QueryDecorator(rdi));
    }

}*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 