/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.cyclecounting.CycleCountingErrorCode;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingRegionDAO;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingSummaryDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingSummary;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;

/**
 * @author khazra
 * 
 */
public class CycleCountingRegionManagerImplRoot extends
        GenericManagerImpl<CycleCountingRegion, CycleCountingRegionDAO>
        implements CycleCountingRegionManager {

    //All Summaries.
    private static final int REGION = 0;

    //Assignment summary query positions
    private static final int TOTAL_ASSIGNMENTS = 1;
    private static final int INPROGRESS = 2;
    private static final int AVAILABLE = 3;
    private static final int COMPLETE = 4;
    private static final int NON_COMPLETE = 5;
    private static final int ASSIGN_SITE = 6;

    //Current work summary query positions
    private static final int OPERATOR_IN_REGION = 1;
    private static final int OPERATOR_ASSIGNED = 2;
    private static final int TOTAL_LOCATIONS_COUNTED = 3;
    private static final int TOTAL_LOCATIONS_REMAINING = 4;
    private static final int CURRENT_WORK_SITE = 5;

    private WorkgroupManager workgroupManager;

    private RegionManager regionManager;

    private OperatorDAO operatorDAO;

    private CycleCountingSummaryDAO  cycleCountingSummaryDAO;
    /**
     * @return the cycleCountingSummaryDAO
     */
    public CycleCountingSummaryDAO getCycleCountingSummaryDAO() {
        return cycleCountingSummaryDAO;
    }

    /**
     * @param cycleCountingSummaryDAO the cycleCountingSummaryDAO to set
     */
    public void setCycleCountingSummaryDAO(CycleCountingSummaryDAO cycleCountingSummaryDAO) {
        this.cycleCountingSummaryDAO = cycleCountingSummaryDAO;
    }

    /**
     * Getter for the regionManager property.
     * 
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the regionManager property.
     * 
     * @param regionManager
     *            the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the workgroupManager property.
     * 
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * 
     * @param workgroupManager
     *            the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * @return the operatorDAO
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }

    /**
     * @param operatorDAO
     *            the operatorDAO to set
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

    /**
     * Constructor
     * 
     * @param primaryDAO
     *            the CycleCountingRegionDAO
     */
    public CycleCountingRegionManagerImplRoot(CycleCountingRegionDAO primaryDAO) {
        super(primaryDAO);

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(CycleCountingRegion region)
            throws BusinessRuleException, DataAccessException {

        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the cyclecounting task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        }

        return super.save(region);
    }

    /**
     * Take the passed in cyclecounting Region and add it to all the workgroups
     * where AutoAddRegions is turned on and the task function is a
     * cyclecounting task function.
     * 
     * @param region
     *            - a cyclecounting region
     * @throws DataAccessException
     *             - any database exception
     */
    protected void addToWorkgroups(CycleCountingRegion region)
            throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.CycleCounting) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CycleCountingRegion findRegionByNumber(int regionNumber)
            throws DataAccessException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }

    /**
     * @return the average of the goal rates for all cyclecounting regions
     * @throws DataAccessException
     *             - database failure
     */
    @Override
    public Double avgGoalRate() throws DataAccessException {
        return getPrimaryDAO().avgGoalRate();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.cyclecountingRegionManagerRoot#uniquenessByName(java.lang.String)
     */
    @Override
    public Long uniquenessByName(String regionName) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByName(regionName);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.cyclecountingRegionManagerRoot#uniquenessByNumber(int)
     */
    @Override
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException {
        return this.getPrimaryDAO().uniquenessByNumber(regionNumber);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.cyclecountingRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.cyclecounting.model.cyclecountingRegion)
     */
    @Override
    public void executeValidateEditRegion(CycleCountingRegion region)
            throws DataAccessException, BusinessRuleException {
        // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            if (!verifyNoOperatorsSignedIn(region)) {
                throw new BusinessRuleException(
                        CycleCountingErrorCode.REGION_OPERATORS_SIGNED_IN,
                        new UserMessage(
                                "cyclecounting.region.edit.error.operatorsSignedIn"));
            }
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.cyclecountingRegionManagerRoot#executeValidateDeleteRegion(com.vocollect.voicelink.cyclecounting.model.cyclecountingRegion)
     */
    public void executeValidateDeleteRegion(CycleCountingRegion region)
            throws DataAccessException, BusinessRuleException {
        // verify that no operators are signed into the region before deleteing
        if (!verifyNoOperatorsSignedIn(region)) {
            throw new BusinessRuleException(
                    CycleCountingErrorCode.REGION_OPERATORS_SIGNED_IN,
                    new UserMessage(
                            "cyclecounting.region.delete.error.operatorsSignedIn"));
        }
    }

    /**
     * 
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(CycleCountingRegion instance)
            throws BusinessRuleException, DataAccessException {
        try {
            executeValidateDeleteRegion(instance);
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("cyclecounting.region.delete.error.inUse");
            } else {
                throw ex;
            }
        }
        return null;
    }

    /**
     * There can be no operators signed into the region when editing or
     * deleting.
     * 
     * @param region
     *            - to be persisted or deleted
     * @return true if no operators are signed into region otherwise return
     *         false
     * @throws DataAccessException
     *             - indicates a database error
     * @throws BusinessRuleException
     *             - if business rule violated
     */
    protected boolean verifyNoOperatorsSignedIn(CycleCountingRegion region)
            throws BusinessRuleException, DataAccessException {

        if (this.getOperatorDAO().countNumberOfOperatorsSignedIn(region)
                .intValue() > 0) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listAssignmentSummary()
     */
    public List<DataObject> listAssignmentSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getCycleCountingSummaryDAO().listAssignmentSummary((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            CycleCountingSummary summaryObject = new CycleCountingSummary();

            summaryObject.setRegion((CycleCountingRegion) objArray[REGION]);
            summaryObject.setTotalAssignments(convertNumberToInt(objArray[TOTAL_ASSIGNMENTS]));
            summaryObject.setInProgress(convertNumberToInt(objArray[INPROGRESS]));
            summaryObject.setAvailable(convertNumberToInt(objArray[AVAILABLE]));
            summaryObject.setComplete(convertNumberToInt(objArray[COMPLETE]));
            summaryObject.setNonComplete(convertNumberToInt(objArray[NON_COMPLETE]));
            summaryObject.setSite((Site) objArray[ASSIGN_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.SelectionRegionManagerRoot#listCurrentWorkSummary()
     */
    public List<DataObject> listCurrentWorkSummary(ResultDataInfo rdi)
    throws DataAccessException {
        // Retrieve it all.
        Object[] queryArgs = rdi.getQueryArgs();
        List<Object[]> data = getCycleCountingSummaryDAO().listCurrentWorkSummary((Date) queryArgs[0]);
        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            CycleCountingSummary summaryObject = new CycleCountingSummary();

            summaryObject.setRegion((CycleCountingRegion) objArray[REGION]);
            summaryObject.setOperatorsWorkingIn(convertNumberToInt(objArray[OPERATOR_IN_REGION]));
            summaryObject.setOperatorsAssigned(convertNumberToInt(objArray[OPERATOR_ASSIGNED]));
            summaryObject.setTotalLocationsCounted(convertNumberToInt(objArray[TOTAL_LOCATIONS_COUNTED]));
            summaryObject.setTotalLocationsRemaining(convertNumberToInt(objArray[TOTAL_LOCATIONS_REMAINING]));
            summaryObject.setSite((Site) objArray[CURRENT_WORK_SITE]);
            summaryObject.setId(summaryObject.getRegion().getId());

            Double estimatedCompleted = 0.0;

            if ((summaryObject.getRegion().getGoalRate() != 0)
                && (summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn() != 0)) {

                Double totalRemain = new Double(summaryObject.getTotalLocationsRemaining());
                Double operators = new Double(summaryObject.getOperatorsAssigned()
                    + summaryObject.getOperatorsWorkingIn());
                Double goalRate = new Double(summaryObject.getRegion().getGoalRate());

                estimatedCompleted = totalRemain / goalRate / operators;
            }

            //Round to 2 decimal places and set property.
            summaryObject.setEstimatedCompleted(new BigDecimal(estimatedCompleted)
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());

            newList.add(summaryObject);
        }
        return newList;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 