/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailDAO;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;



/**
 * @author khazra
 *
 */
public abstract class CycleCountingDetailExportTransportManagerImplRoot
    extends CycleCountingDetailManagerImplRoot
    implements CycleCountingDetailExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CycleCountingDetailExportTransportManagerImplRoot(CycleCountingDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    

    /**
     * {@inheritDoc}
     * @return list of maps.  Each map contains the fields for a completed pick,
     *         the object, and the matchingStatus of '1,2'.
     * @throws DataAccessException
     */
    @Override
    public List<Map<String, Object>> listDetails()
    throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Map<String, Object>> records = getPrimaryDAO().listDetails(qd);
        records.addAll(getPrimaryDAO().listDetailsNoOperator(qd));
                
        return formatDatesInListOfMap(records);
   }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.PickExportTransportManagerRoot#updatePickedPicks()
     */
    @Override
    public Integer updateDetailsExportStatus() throws DataAccessException {
        boolean siteFilter = SiteContextHolder.getSiteContext().isFilterBySite();
        SiteContextHolder.getSiteContext().setFilterBySite(false);
        Integer i;
        try {
            i = getPrimaryDAO().updateDetailsExportStatus();
        } finally {
            SiteContextHolder.getSiteContext().setFilterBySite(siteFilter);
        }
        return i;
    }
    
    

    /**
     * Formats the date.
     * @param inputList in which objects needs to be formated.
     * @return list
     */
    private List<Map<String, Object>> formatDatesInListOfMap(List<Map<String, Object>> inputList) {
        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;
        
        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map <String, Object> m : inputList) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if ((myObj != null) && (myObj instanceof java.util.Date)) {
                    // It is a date!
                    formattedDateTime = sdf.format(myObj);
                    // Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } // end if class is date
               
            } // end for obj

        } //end for map
        return inputList;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 