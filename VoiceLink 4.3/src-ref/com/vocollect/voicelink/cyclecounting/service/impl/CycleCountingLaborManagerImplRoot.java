/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingLaborDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;

import java.util.Date;
import java.util.List;

/**
 * @author khazra
 * 
 */
public class CycleCountingLaborManagerImplRoot extends
    GenericManagerImpl<CycleCountingLabor, CycleCountingLaborDAO> implements
    CycleCountingLaborManager {

    private static final Double MILLISEC_CONVERSION_FACTOR = 3600000.0;

    private static final Double ONE_HUNDRED = 100.0;

    /**
     * Constructor.
     * @param primaryDAO the Cycle counting labor dao
     */
    public CycleCountingLaborManagerImplRoot(CycleCountingLaborDAO primaryDAO) {
        super(primaryDAO);

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listOpenRecordsByOperatorId(long)
     */
    @Override
    public List<CycleCountingLabor> listOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listOpenRecordsByOperatorId(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listRecordsByOperatorId(long)
     */
    @Override
    public List<CycleCountingLabor> listRecordsByOperatorId(long operatorId)
        throws DataAccessException {
        return getPrimaryDAO().listRecordsByOperatorId(operatorId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#findOpenRecordByAssignmentId(long)
     */
    @Override
    public CycleCountingLabor findOpenRecordByAssignmentId(long assignmentId)
        throws DataAccessException {
        return getPrimaryDAO().findOpenRecordByAssignmentId(assignmentId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#findClosedRecordsByBreakLaborId(long)
     */
    @Override
    public CycleCountingLabor findClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException {
        return getPrimaryDAO().findClosedRecordsByBreakLaborId(breakLaborId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#sumLocationsCounted(long)
     */
    @Override
    public Integer sumLocationsCounted(long operatorLaborId)
        throws DataAccessException {
        Integer result = getPrimaryDAO().sumLocationsCounted(operatorLaborId);

        if (result == null) {
            result = new Integer(0);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listAllRecordsByOperatorLaborId(long)
     */
    @Override
    public List<CycleCountingLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException {
        return getPrimaryDAO().listAllRecordsByOperatorLaborId(operatorLaborId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listAssignmentLabor(com.vocollect.epp.dao.hibernate.finder.QueryDecorator)
     */
    @Override
    public List<DataObject> listAssignmentLabor(QueryDecorator decorator)
        throws DataAccessException {
        return getPrimaryDAO().listAssignmentLabor(decorator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listLaborSummaryReportRecords(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborSummaryReportRecords(Long operatorIdentifier,
                                                        Date startDate,
                                                        Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(
            operatorIdentifier, startDate, endDate);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listLaborDetailReportRecords(java.lang.Long, java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                                       Date startDate, Date endDate) 
                                                       throws DataAccessException {

        return this.getPrimaryDAO().listLaborDetailReportRecords(operatorId, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#listLaborDurationByOperRegionDates(java.lang.Long,
     *      java.lang.Integer, java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborDurationByOperRegionDates(Long operatorIdentifier,
                                                             Integer regionNumber,
                                                             Date startDate,
                                                             Date endDate)
        throws DataAccessException {

        return this.getPrimaryDAO().listLaborDurationByOperRegionDates(
            operatorIdentifier, regionNumber, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#sumLocationsCountedBetweenDates(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    @Override
    public Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                                  Date startTime,
                                                  Date endTime)
        throws DataAccessException {
        return this.getPrimaryDAO().sumLocationsCountedBetweenDates(
            operatorLaborId, startTime, endTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#openLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.core.model.Operator,
     *      com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment,
     *      com.vocollect.voicelink.core.model.OperatorLabor)
     */
    @Override
    public void openLaborRecord(Date startTime,
                                Operator operator,
                                CycleCountingAssignment assignment,
                                OperatorLabor operLaborRecord)
        throws DataAccessException, BusinessRuleException {

        // close any open labor records for this assignment.
        // this is to handle the rare case of where the operator gets out of
        // radio range and get assignment is called for a second (or more) time.
        this.closeLaborRecord(startTime, assignment);

        CycleCountingLabor cycleCountingLabor = new CycleCountingLabor();
        cycleCountingLabor.setAssignment(assignment);
        cycleCountingLabor.setOperator(operator);
        cycleCountingLabor.setOperatorLabor(operLaborRecord);
        cycleCountingLabor.setStartTime(startTime);
        cycleCountingLabor.setEndTime(null);
        cycleCountingLabor.setDuration(new Long(0));
        cycleCountingLabor.setLocationsCounted(new Integer(0));
        cycleCountingLabor.setActualRate(new Double(0.0));
        cycleCountingLabor.setPercentOfGoal(new Double(0.0));

        this.save(cycleCountingLabor);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#recalculateAggregatesAndSave(com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor)
     */
    public void recalculateAggregatesAndSave(CycleCountingLabor al) throws DataAccessException, BusinessRuleException {
        
        // adjust the duration since the endtime has been modified.
        al.setDuration(new Long(al.getEndTime().getTime() -  al.getStartTime().getTime()));

        if (al.getDuration() > 0) {
            al.setActualRate(new Double(al.getLocationsCounted()
                / (al.getDuration() / MILLISEC_CONVERSION_FACTOR)));
            al.setPercentOfGoal(new Double(al.getActualRate()
                / (al.getAssignment().getRegion().getGoalRate()) * ONE_HUNDRED));
        }
        this.save(al);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManagerRoot#closeLaborRecord(java.util.Date,
     *      com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment)
     */
    @Override
    public CycleCountingLabor closeLaborRecord(Date endTime, CycleCountingAssignment assignment)
                                                throws DataAccessException, BusinessRuleException {

        long assignmentId = assignment.getId();
        CycleCountingLabor ccl = this.findOpenRecordByAssignmentId(assignmentId);

        if (ccl != null) {
            Date startTime = ccl.getStartTime();
            ccl.setEndTime(endTime);
            ccl.setDuration(new Long(endTime.getTime() - startTime.getTime()));
            // set locations counted to one. Assume that all UOM's at this location were counted.
            ccl.setLocationsCounted(1);
            // now see if ALL UOM's were counted. 
            // If they were not ALL counted, then they do not get credit for counting the location.
            for (CycleCountingDetail detail : ccl.getAssignment().getCycleCountingDetails()) {
                if (detail.getStatus() == CycleCountDetailStatus.NotCounted) {
                    ccl.setLocationsCounted(0);
                    break;
                }
            }
            if (ccl.getDuration() > 0) {
                ccl.setActualRate(new Double(ccl.getLocationsCounted() / (ccl.getDuration() / 
                        MILLISEC_CONVERSION_FACTOR)));
                ccl.setPercentOfGoal(new Double(ccl.getActualRate() / 
                        (assignment.getRegion().getGoalRate()) * ONE_HUNDRED));
            }
            this.save(ccl);
        }
        return ccl;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 