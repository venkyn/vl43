/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingAssignmentImportDataDAO;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailImportDataDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentImportData;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentImportDataManager;

import java.util.List;
/**
 * @author pkolonay
 * 
 */
public abstract class CycleCountingAssignmentImportDataManagerImplRoot extends
    GenericManagerImpl<CycleCountingAssignmentImportData, CycleCountingAssignmentImportDataDAO>
    implements CycleCountingAssignmentImportDataManager {

    private static final int RECORDS_TO_QUERY = 10;

    private CycleCountingDetailImportDataDAO cycleCountingDetailImportDataDAO = null;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CycleCountingAssignmentImportDataManagerImplRoot(CycleCountingAssignmentImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @param siteName
     * @return
     * @throws DataAccessException
     */
    @Override
    public List<CycleCountingAssignmentImportData> listAssignmentBySiteName(String siteName)
        throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(RECORDS_TO_QUERY);
        return getPrimaryDAO().listAssignmentBySiteName(qd, siteName);
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     */
    @Override
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
        getCycleCountingDetailImportDataDAO().updateToComplete();
    }
    
    /**
     * @return the cycleCountingDetailImportDataDAO
     */
    public CycleCountingDetailImportDataDAO getCycleCountingDetailImportDataDAO() {
        return cycleCountingDetailImportDataDAO;
    }

    /**
     * @param cycleCountingDetailImportDataDAO the cycleCountingDetailImportDataDAO to set
     */
    public void setCycleCountingDetailImportDataDAO(CycleCountingDetailImportDataDAO cycleCountingDetailImportDataDAO) {
        this.cycleCountingDetailImportDataDAO = cycleCountingDetailImportDataDAO;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 