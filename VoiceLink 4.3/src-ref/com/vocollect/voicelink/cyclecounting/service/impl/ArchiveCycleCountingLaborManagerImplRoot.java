/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.cyclecounting.dao.ArchiveCycleCountingLaborDAO;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager;

import java.util.Date;
import java.util.List;

/**
 * Manager Implementation class for Archive Cycle Counting Assignment Labor.
 * 
 * @author ktanneru
 */
public abstract class ArchiveCycleCountingLaborManagerImplRoot extends
    GenericManagerImpl<ArchiveCycleCountingLabor, ArchiveCycleCountingLaborDAO>
    implements ArchiveCycleCountingLaborManager {

    /**
     * Constructor.
     * 
     * @param primaryDAO the DAO for this manager
     */
    public ArchiveCycleCountingLaborManagerImplRoot(ArchiveCycleCountingLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        return this.getPrimaryDAO().listArchiveCycleCountingLabor(
            new QueryDecorator(rdi));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManagerRoot#listLaborSummaryReportRecords(java.lang.String,
     *      java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                        Date startDate,
                                                        Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(
            operatorIdentifier, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManagerRoot#listLaborDetailReportRecords(java.lang.String,
     *      java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborDetailReportRecords(String operatorIdentifier,
                                                       Date startDate,
                                                       Date endDate)
        throws DataAccessException {
        return this.getPrimaryDAO().listLaborDetailReportRecords(
            operatorIdentifier, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManagerRoot#listLaborDurationByOperRegionDates(java.lang.String,
     *      java.lang.Integer, java.util.Date, java.util.Date)
     */
    @Override
    public List<Object[]> listLaborDurationByOperRegionDates(String operatorIdentifier,
                                                             Integer regionNumber,
                                                             Date startDate,
                                                             Date endDate)
        throws DataAccessException {

        return this.getPrimaryDAO().listLaborDurationByOperRegionDates(
            operatorIdentifier, regionNumber, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManagerRoot#sumLocationsCountedBetweenDates(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    @Override
    public Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                                  Date startTime,
                                                  Date endTime)
        throws DataAccessException {
        return this.getPrimaryDAO().sumLocationsCountedBetweenDates(
            operatorLaborId, startTime, endTime);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 