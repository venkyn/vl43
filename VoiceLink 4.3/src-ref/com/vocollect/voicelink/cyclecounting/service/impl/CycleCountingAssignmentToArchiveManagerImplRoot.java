/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentToArchiveManager;

import java.util.Date;
import java.util.List;

/**
 * @author ktanneru
 * 
 */
public class CycleCountingAssignmentToArchiveManagerImplRoot implements
        CycleCountingAssignmentToArchiveManager {

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private ArchiveCycleCountingAssignmentManager archiveCycleCountingAssignmentManager;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(
            VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentToArchiveManagerRoot#executeMarkForPurge(com.vocollect.voicelink.selection.model.AssignmentStatus,
     *      java.util.Date, boolean)
     */
    @Override
    public Integer executeMarkForPurge(CycleCountStatus status, Date olderThan,
            boolean archive) {
        Integer recordsMarked = 0;

        // Mark all assignments to be purged
        if (log.isDebugEnabled()) {
            log.debug("### Finding Cycle Counting " + status.toString()
                    + " Assignments that are elligible for purge/archive :::");
        }
        try {
            if (archive) {
                recordsMarked = getCycleCountingAssignmentManager()
                        .getPrimaryDAO().updateOlderThan(1, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked
                            + " CC Assignment - " + status.toString()
                            + " for Archive :::");
                }
            } else {
                recordsMarked = getCycleCountingAssignmentManager()
                        .getPrimaryDAO().updateOlderThan(2, olderThan, status);
                if (log.isDebugEnabled()) {
                    log.debug("### Found " + recordsMarked
                            + " CC Assignment - " + status.toString()
                            + " for Purge :::");
                }
            }
        } catch (DataAccessException e) {
            log.warn("!!! Error: DataAccessException marking transactional " + "Cycle Counting "
                    + status.toString() + " Assignments for Purge/Archive: " + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Updated " + recordsMarked + " Cycle Counting "
                    + status.toString() + " Assignments for Purge :::");
        }
        return recordsMarked;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.selection.service.AssignmentToArchiveManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator,
     *      com.vocollect.voicelink.selection.model.AssignmentStatus,
     *      java.util.Date, boolean)
     */
    @Override
    public int executeArchive(QueryDecorator decorator) {

        int returnRecords = 0;

        // Get List of Cycle Counting Assignments to Archive
        if (log.isDebugEnabled()) {
            log.debug("### Finding Transactional Cycle Counting Assignments to Archive :::");
        }
        try {
            List<CycleCountingAssignment> cycleCountingAssignments = getCycleCountingAssignmentManager()
                    .getPrimaryDAO().listArchive(decorator);
            returnRecords = cycleCountingAssignments.size();

            for (CycleCountingAssignment a : cycleCountingAssignments) {
                getArchiveCycleCountingAssignmentManager().save(
                        new ArchiveCycleCountingAssignment(a));
                a.setPurgeable(2);
                getCycleCountingAssignmentManager().save(a);
            }

        } catch (DataAccessException e) {
            log.warn("!!! Error DataAccessException getting Cycle Counting "
                    + "Assignment(s) from database to archive: " + e);
            return 0;
        } catch (BusinessRuleException e) {
            log.warn("!!! Error BusinessRuleException creating Archive Cycle Counting Assignment: "
                    + e);
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Archived " + returnRecords
                    + " Cycle Counting Assignment(s) ");
            log.debug("### Cycle Counting Assignment Archive Process Complete :::");
        }

        getCycleCountingAssignmentManager().getPrimaryDAO().clearSession();
        return returnRecords;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentToArchiveManagerRoot
     *      #executePurgeCycleCountingAssignment()
     */
    @Override
    public int executePurgeCycleCountingAssignment(QueryDecorator queryDecorator) {
        int returnRecords = 0;

        List<CycleCountingAssignment> ccAssignments = null;

        try {
            ccAssignments = getCycleCountingAssignmentManager().getPrimaryDAO()
                    .listPurge(queryDecorator);

            returnRecords = ccAssignments.size();
        } catch (DataAccessException e) {
            log.warn("!!!Error: DataAccessException purging transactional "
                    + "Cycle Counting Assignments from database: " + e);
        }

        for (CycleCountingAssignment assignment : ccAssignments) {
            try {
                getCycleCountingAssignmentManager().delete(assignment);

            } catch (BusinessRuleException e) {
                log.warn("!!!Error: BusinessRuleException purging transactional "
                        + "Cycle Counting Assignments from database: " + e);
            } catch (DataAccessException e) {
                log.warn("!!!Error: DataAccessException purging transactional "
                        + "Cycle Counting Assignments from database: " + e);

            }

        }
        if (log.isDebugEnabled()) {
            log.debug("### Purged " + returnRecords
                    + " rows from the database.");
            log.debug("### Cycle Counting Assignments Purge Process Complete :::");
        }
        return returnRecords;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the archiveCycleCountingAssignmentManager
     */
    public ArchiveCycleCountingAssignmentManager getArchiveCycleCountingAssignmentManager() {
        return archiveCycleCountingAssignmentManager;
    }

    /**
     * @param archiveCycleCountingAssignmentManager
     *            the archiveCycleCountingAssignmentManager to set
     */
    public void setArchiveCycleCountingAssignmentManager(
            ArchiveCycleCountingAssignmentManager archiveCycleCountingAssignmentManager) {
        this.archiveCycleCountingAssignmentManager = archiveCycleCountingAssignmentManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 