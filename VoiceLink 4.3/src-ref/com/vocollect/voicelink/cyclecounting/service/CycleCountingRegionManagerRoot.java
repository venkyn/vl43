/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.cyclecounting.dao.CycleCountingRegionDAO;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;

/**
 * 
 * @author khazra
 * 
 */
public interface CycleCountingRegionManagerRoot extends
        GenericManager<CycleCountingRegion, CycleCountingRegionDAO>,
        DataProvider {

    /**
     * Find a region by number
     * 
     * @param regionNumber
     *            as integer
     * @return CycleCountingRegion corresponding the region number
     */
    public CycleCountingRegion findRegionByNumber(int regionNumber)
            throws DataAccessException;

    /**
     * Find the average goal rate for Cycle Counting regions
     *     
     * @return Average as double
     * @throws DataAccessException
     */
    public Double avgGoalRate()
            throws DataAccessException;

    /**
     * Finds if the supplied region name is unique.
     * 
     * @param regionName
     *            to search
     * @return the record id if found, null if not found
     * @throws DataAccessException
     */
    public Long uniquenessByName(String regionName) throws DataAccessException;

    /**
     * Finds if the supplied region number is unique.
     * 
     * @param regionNumber
     *            to search
     * @return the record id if found, null if not found
     * @throws DataAccessException
     */
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException;

    /**
     * Validate the business rules for editing a putaway region region.
     * 
     * @param region
     *            - region to edit
     * @throws DataAccessException
     *             - indicates database error
     * @throws BusinessRuleException
     *             - thrown in business rule violated
     */
    void executeValidateEditRegion(CycleCountingRegion region)
            throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 