/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author khazra
 *
 */
public enum CountMode implements ValueBasedEnum<CountMode> {
    SystemDirected(1), OperatorDirected(2);

    private static ReverseEnumMap<CountMode> toValueMap = new ReverseEnumMap<CountMode>(
            CountMode.class);

    private int value;

    /**
     * @param value the enum value
     */
    private CountMode(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public CountMode fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public CountMode fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * 
     * @param val
     *            the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException
     *             if the val doesn't map to a constant.
     */
    public static CountMode toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 