/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.ExportTransportObject;

/**
 * @author khazra
 * 
 */
public class CycleCountingExportTransport extends ExportTransportObject {
    
    @Override
    public void updateExportToInProgress(Object obj) {
        if (obj instanceof CycleCountingAssignment) {
            ((CycleCountingAssignment) obj).setExportStatus(ExportStatus.InProgress);
        } else if (obj instanceof CycleCountingLabor) {
            ((CycleCountingLabor) obj).setExportStatus(ExportStatus.InProgress);
        } else if (obj instanceof CycleCountingDetail) {
            ((CycleCountingDetail) obj).setExportStatus(ExportStatus.InProgress);
        }

    }

    @Override
    public void updateExportToExported(Object obj) {
        if (obj instanceof CycleCountingAssignment) {
            ((CycleCountingAssignment) obj).setExportStatus(ExportStatus.Exported);
        } else if (obj instanceof CycleCountingLabor) {
            ((CycleCountingLabor) obj).setExportStatus(ExportStatus.Exported);
        } else if (obj instanceof CycleCountingDetail) {
            ((CycleCountingDetail) obj).setExportStatus(ExportStatus.Exported);
        }

    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToInProgress(CycleCountingAssignment object) {
        object.setExportStatus(ExportStatus.InProgress);
    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToExported(CycleCountingAssignment object) {
        object.setExportStatus(ExportStatus.Exported);
    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToInProgress(CycleCountingDetail object) {
        object.setExportStatus(ExportStatus.InProgress);
    }

    /**
     * {@inheritDoc}
     */
    public void updateExportToExported(CycleCountingDetail object) {
        object.setExportStatus(ExportStatus.Exported);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 