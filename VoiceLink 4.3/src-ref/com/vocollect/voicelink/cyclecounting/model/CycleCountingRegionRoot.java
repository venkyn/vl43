/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.Region;

import java.util.Set;

/**
 * Model object root for Cycle Counting region.
 * 
 * @author khazra
 * 
 */
public class CycleCountingRegionRoot extends Region {

    private static final long serialVersionUID = -1027990453735813682L;

    private CountType countType;
    private CountMode workIdentifier;

    //************************************************************
    //Related sets
    //************************************************************
    //Set of assignments within region
    private Set<CycleCountingAssignment>  assignments;
    
    
    
    
    /**
     * Getter for the assignments property.
     * @return Set&gt;Assignment&lt; value of the property
     */
    public Set<CycleCountingAssignment> getCycleCountingAssignments() {
        return this.assignments;
    }

    
    /**
     * Setter for the assignments property.
     * @param assignments the new assignments value
     */
    public void setCycleCountingAssignments(Set<CycleCountingAssignment> assignments) {
        this.assignments = assignments;
    }
    
    /**
     * @return the countType
     */
    public CountType getCountType() {
        return countType;
    }

    /**
     * @param countType
     *            the countType to set
     */
    public void setCountType(CountType countType) {
        this.countType = countType;
    }

    /**
     * @return the countDirector
     */
    public CountMode getCountMode() {
        return workIdentifier;
    }

    /**
     * @param countDirector
     *            the countDirector to set
     */
    public void setCountMode(CountMode countDirector) {
        this.workIdentifier = countDirector;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 