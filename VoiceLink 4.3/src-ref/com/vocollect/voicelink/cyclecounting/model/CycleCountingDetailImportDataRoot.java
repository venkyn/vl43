/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Item;

import java.util.Date;

/**
 * @author pkolonay
 *
 */
public class CycleCountingDetailImportDataRoot extends BaseModelObject
        implements Importable {

    /**
     * 
     */
    private static final long serialVersionUID = -9147704549301181054L;

    private ImportableImpl impl = new ImportableImpl();
    
    private CycleCountingDetail     myModel = new CycleCountingDetail();

    private CycleCountingAssignmentImportData parent = null;
    
    
    /**
     * @return the parent
     */
    public CycleCountingAssignmentImportData getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(CycleCountingAssignmentImportData parent) {
        this.parent = parent;
    }

    /**
     * @return - the created date
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }

    /**
     * @return - the version number
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }

    /**
     * @return - the id
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return myModel.getId();
    }

    /**
     * @param version - the version to set
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }

    /**
     * @param createdDate - the created date to set.
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }

    /**
     * @return - the cycle counting assignment
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#getCycleCountingAssignment()
     */
    public CycleCountingAssignment getCycleCountingAssignment() {
        return myModel.getCycleCountingAssignment();
    }

    /**
     * @param ccAssignment - the cycle counting assignment to set.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#setCycleCountingAssignment(com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment)
     */
    public void setCycleCountingAssignment(CycleCountingAssignment ccAssignment) {
        myModel.setCycleCountingAssignment(ccAssignment);
    }

    /**
     * @return - the status
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#getStatus()
     */
    public CycleCountDetailStatus getStatus() {
        return myModel.getStatus();
    }

    /**
     * @return - the new boolean
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    @Override
    public boolean isNew() {
        return myModel.isNew();
    }

    /**
     * @param status - the status to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#setStatus(com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus)
     */
    public void setStatus(CycleCountDetailStatus status) {
        myModel.setStatus(status);
    }

    /**
     * Set the number in the Item. This will be used to look up an Item in the database.
     * @param itemId the ID to set the Item.number to.
     */
    public void setItemId(String itemId) {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        tempItem.setNumber(itemId);
    }

    
    /**
     * Get the Item Id (Item.number).
     * @return the Item Id (Item.number a String).
     */
    public String getItemId() {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        if (null == tempItem.getNumber()) {
            return null;
        }
        return tempItem.getNumber();
    }

    /**
     * @return - the item 
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }

    /**
     * @param item - the new item to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }

    /**
     * @return - the expected quantity
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#getExpectedQuantity()
     */
    public int getExpectedQuantity() {
        return myModel.getExpectedQuantity();
    }

    /**
     * @return - the descriptive text
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }

    /**
     * @param expectedQuantity - the expected quantity to set.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#setExpectedQuantity(int)
     */
    public void setExpectedQuantity(int expectedQuantity) {
        myModel.setExpectedQuantity(expectedQuantity);
    }

    /**
     * @return - the unit of measure
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#getUnitOfMeasure()
     */
    public String getUnitOfMeasure() {
        return myModel.getUnitOfMeasure();
    }

    /**
     * @param unitOfMeasure - the new unit of measure value to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingDetailRoot#setUnitOfMeasure(java.lang.String)
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        myModel.setUnitOfMeasure(unitOfMeasure);
    }

    /**
     * @return - the model object
     * @throws VocollectException
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    @Override
    public Object convertToModel() throws VocollectException {
        return myModel;
    }

    /**
     * @return - the isCompleted value
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * @return - the isinprogress boolean
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    @Override
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * @param completedParam - the completed value
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    @Override
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * @return - hash code
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * @param inProgressParam - in progress value to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    @Override
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * @return - import id
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    @Override
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * @param importID - import id to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.Long)
     */
    @Override
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * @return - site name
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    @Override
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * @param siteNameParam - site name to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    @Override
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * @return - import status
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    @Override
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * @param status - the status to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    @Override
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }

    /**
     * @param obj - the obj to compare
     * @return - the equality indicator
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }
    


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 