/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author khazra
 * 
 */
public class CycleCountingLaborRoot extends CommonModelObject implements
    Serializable {

    private static final long serialVersionUID = 5312808675619500560L;

    private CycleCountingAssignment assignment;

    private Operator operator;

    private OperatorLabor operatorLabor;

    private Long breakLaborId;

    private Date startTime;

    private Date endTime;

    private Long duration;

    private Integer locationsCounted = 1;

    private Double actualRate;

    private Double percentOfGoal;

    private ExportStatus exportStatus = ExportStatus.NotExported;

    /**
     * @return the assignment
     */
    public CycleCountingAssignment getAssignment() {
        return assignment;
    }

    /**
     * @param assignment the assignment to set
     */
    public void setAssignment(CycleCountingAssignment assignment) {
        this.assignment = assignment;
    }

    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * @return the operatorLabor
     */
    public OperatorLabor getOperatorLabor() {
        return operatorLabor;
    }

    /**
     * @param operatorLabor the operatorLabor to set
     */
    public void setOperatorLabor(OperatorLabor operatorLabor) {
        this.operatorLabor = operatorLabor;
    }

    /**
     * @return the breakLaborId
     */
    public Long getBreakLaborId() {
        return breakLaborId;
    }

    /**
     * @param breakLaborId the breakLaborId to set
     */
    public void setBreakLaborId(Long breakLaborId) {
        this.breakLaborId = breakLaborId;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * Getter for the locationsCounted property.
     * @return Long value of the property
     */
    public Integer getLocationsCounted() {
        return locationsCounted;
    }

    /**
     * Setter for the locationsCounted property.
     * @param locationsCounted the new locationsCounted value
     */
    public void setLocationsCounted(Integer locationsCounted) {
        this.locationsCounted = locationsCounted;
    }

    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        return actualRate;
    }

    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        return percentOfGoal;
    }

    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CycleCountingLaborRoot)) {
            return false;
        }
        final CycleCountingLaborRoot other = (CycleCountingLaborRoot) obj;
        if (getOperator() == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!getOperator().equals(other.getOperator())) {
            return false;
        }
        if (getStartTime() == null) {
            if (other.getStartTime() != null) {
                return false;
            }
        } else if (!getStartTime().equals(other.getStartTime())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
            + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result
            + ((startTime == null) ? 0 : startTime.hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 