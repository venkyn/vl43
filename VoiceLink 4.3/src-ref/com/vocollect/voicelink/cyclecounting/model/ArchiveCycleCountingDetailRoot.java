/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.ReasonCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ktanneru
 * 
 */
public class ArchiveCycleCountingDetailRoot extends ArchiveModelObject
        implements Serializable {

    private static final long serialVersionUID = -2791528739542441958L;

    private ArchiveCycleCountingAssignment archiveCycleCountingAssignment;
    
    private Item item;

    private ReasonCode reason;
    
    private int expectedQuantity;

    private int actualQuantity;

    private String unitOfMeasure;

    private Date countTime;

    private CycleCountDetailStatus status = CycleCountDetailStatus.NotCounted;
  

    /**
     * 
     * Constructor.
     */
    public ArchiveCycleCountingDetailRoot() {

    }

    /**
     * 
     * Constructor.
     * 
     * @param cycleCountingDetail
     *            used to construct ArchivecycleCountingDetail.
     * @param archiveCycleCountingAssignment
     *            used to construct ArchiveCycleCountingAssignment
     */
    public ArchiveCycleCountingDetailRoot(
            CycleCountingDetail cycleCountingDetail,
            ArchiveCycleCountingAssignment archiveCycleCountingAssignment) {
        
/*        if (cycleCountingDetail != null) {
            getArchiveCycleCountingAssignment().add(
                    new ArchiveCycleCountingAssignment)
        }*/
//        this.setArchiveCycleCountingAssignment(cycleCountingDetail.getCycleCountingAssignment());
        this.setArchiveCycleCountingAssignment(archiveCycleCountingAssignment);
        this.setItem(cycleCountingDetail.getItem());
        this.setReason(cycleCountingDetail.getReason());
        this.setExpectedQuantity(cycleCountingDetail.getExpectedQuantity());
        this.setActualQuantity(cycleCountingDetail.getActualQuantity());
        this.setUnitOfMeasure(cycleCountingDetail.getUnitOfMeasure());
        this.setCountTime(cycleCountingDetail.getCountTime());
        this.setCreatedDate(new Date());
    }

    /**
     * @return the status
     */
    public CycleCountDetailStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(CycleCountDetailStatus status) {
        this.status = status;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item
     *            the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the expectedQuantity
     */
    public int getExpectedQuantity() {
        return expectedQuantity;
    }

    /**
     * @param expectedQuantity
     *            the expectedQuantity to set
     */
    public void setExpectedQuantity(int expectedQuantity) {
        this.expectedQuantity = expectedQuantity;
    }

    /**
     * @return the actualQuantity
     */
    public int getActualQuantity() {
        return actualQuantity;
    }

    /**
     * @param actualQuantity
     *            the actualQuantity to set
     */
    public void setActualQuantity(int actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    /**
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * @param unitOfMeasure
     *            the unitOfMeasure to set
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * @return the countTime
     */
    public Date getCountTime() {
        return countTime;
    }

    /**
     * @param countTime
     *            the countTime to set
     */
    public void setCountTime(Date countTime) {
        this.countTime = countTime;
    }

    /**
     * @return the reason
     */
    public ReasonCode getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(ReasonCode reason) {
        this.reason = reason;
    }



    /**
     * @return the archiveCycleCountingAssignment
     */
    public ArchiveCycleCountingAssignment getArchiveCycleCountingAssignment() {
        return archiveCycleCountingAssignment;
    }

    /**
     * @param archiveCycleCountingAssignment the archiveCycleCountingAssignment to set
     */
    public void setArchiveCycleCountingAssignment(
            ArchiveCycleCountingAssignment archiveCycleCountingAssignment) {
        this.archiveCycleCountingAssignment = archiveCycleCountingAssignment;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return ((getId() == null) ? 0 : getId().hashCode());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 