/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.LocationDescription;

import static com.vocollect.voicelink.cyclecounting.model.CycleCountStatus.Available;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model object representing a VoiceLink Archive Cycle Counting Assignment.
 * 
 * @author ktanneru
 * 
 */
public class ArchiveCycleCountingAssignmentRoot extends ArchiveModelObject
        implements Serializable {

    private static final long serialVersionUID = -7278568744350372133L;

    private Long sequence = -1L;

    private CycleCountingRegion region;

    private CycleCountStatus status = Available;

    // Operator that last put the item.
    private String operatorIdentifier;

    private String operatorName;

    private Date startTime;

    private Date endTime;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    private Date createdDate;

    private LocationDescription description;

    private List<ArchiveCycleCountingDetail> archiveCCDetails = new ArrayList<ArchiveCycleCountingDetail>();

    private List<ArchiveCycleCountingLabor> archiveCCLabor = new ArrayList<ArchiveCycleCountingLabor>();

    private Site site;

    /**
     * 
     * Constructor.
     */
    public ArchiveCycleCountingAssignmentRoot() {

    }

    /**
     * 
     * Constructor.
     * 
     * @param cycleCountingAssignment
     *            CycleCountingAssignment used to build
     *            ArchiveCycleCountingAssignment.
     */
    public ArchiveCycleCountingAssignmentRoot(
            CycleCountingAssignment cycleCountingAssignment) {
        if (cycleCountingAssignment != null) {

            for (CycleCountingLabor ccl : cycleCountingAssignment.getLabor()) {
                this.getArchiveCCLabor()
                        .add(new ArchiveCycleCountingLabor(ccl,
                                (ArchiveCycleCountingAssignment) this));
            }

            for (CycleCountingDetail detail : cycleCountingAssignment
                    .getCycleCountingDetails()) {
                getArchiveCCDetails().add(
                        new ArchiveCycleCountingDetail(detail,
                                (ArchiveCycleCountingAssignment) this));
            }
            for (Tag t : cycleCountingAssignment.getTags()) {
                if (t.getTagType().equals(Tag.SITE)) {
                    this.setSite(getSiteMap().get(t.getTaggableId()));
                    break;
                }
            }
            if (cycleCountingAssignment.getOperator() != null) {
                this.setOperatorIdentifier(cycleCountingAssignment
                        .getOperator().getOperatorIdentifier());
                this.setOperatorName(cycleCountingAssignment.getOperator()
                        .getName());
            } else {
                this.setOperatorIdentifier(" ");
                this.setOperatorName(" ");
            }

            this.setCreatedDate(new Date());
            this.setEndTime(cycleCountingAssignment.getEndTime());
            this.setStartTime(cycleCountingAssignment.getStartTime());
            this.setRegion(cycleCountingAssignment.getRegion());
            this.setStatus(cycleCountingAssignment.getStatus());
            this.setSite(this.getSite());
            this.setDescription(cycleCountingAssignment.getLocation()
                    .getDescription());

        }
    }

    /**
     * @return the sequence
     */
    public Long getSequence() {
        return sequence;
    }

    /**
     * @param sequence
     *            the sequence to set
     */
    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    /**
     * @return the region
     */
    public CycleCountingRegion getRegion() {
        return this.region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(CycleCountingRegion region) {
        this.region = region;
    }

    /**
     * @return the createdDate
     */
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate
     *            the createdDate to set
     */
    @Override
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the location description
     */
    public LocationDescription getDescription() {
        return description;
    }

    /**
     * @param description
     *            the location to set
     */
    public void setDescription(LocationDescription description) {
        this.description = description;
    }

    /**
     * @return the status
     */
    public CycleCountStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(CycleCountStatus status) {
        this.status = status;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     *            the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the ArchiveCCDetails
     */
    public List<ArchiveCycleCountingDetail> getArchiveCCDetails() {
        return archiveCCDetails;
    }

    /**
     * @param archCycleCountingDetails
     *            the ArchiveCCDetails to set
     */
    public void setArchiveCCDetails(
            List<ArchiveCycleCountingDetail> archCycleCountingDetails) {
        this.archiveCCDetails = archCycleCountingDetails;
    }

    /**
     * @return the archiveCCLabor
     */
    public List<ArchiveCycleCountingLabor> getArchiveCCLabor() {
        return archiveCCLabor;
    }

    /**
     * @param archiveCCLabor the archiveCCLabor to set
     */
    public void setArchiveCCLabor(List<ArchiveCycleCountingLabor> archiveCCLabor) {
        this.archiveCCLabor = archiveCCLabor;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus
     *            the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @return the site
     */
    public Site getSite() {
        return site;
    }

    /**
     * @param site
     *            the site to set
     */
    public void setSite(Site site) {
        this.site = site;
    }

    /**
     * @return the operatorIdentifier
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    /**
     * @param operatorIdentifier
     *            the operatorIdentifier to set
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName
     *            the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 