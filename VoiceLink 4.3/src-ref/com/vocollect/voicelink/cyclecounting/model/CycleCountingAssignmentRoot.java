/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.SelectionErrorCode;

import static com.vocollect.voicelink.cyclecounting.model.CycleCountStatus.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingAssignmentRoot extends CommonModelObject implements
        Serializable, Taggable {

    /**
     * 
     */
    private static final long serialVersionUID = -5954657011798270435L;

    private Long sequence = -1L;

    private CycleCountingRegion region;

    private Date dateReceived;

    private Location location;

    private CycleCountStatus status = Available;

    private Operator operator;

    private Date startTime;

    private Date endTime;

    private List<CycleCountingDetail> cycleCountingDetails = new ArrayList<CycleCountingDetail>();
    
    // labor records belonging to this assignment (Added for archive purposes)
    private List<CycleCountingLabor>       labor = new ArrayList<CycleCountingLabor>();
    
    private Set<Tag> tags;

    // ExportStatus Flag 0=Not exportStatus, 1=Currently being exportStatus,
    // 2=ExportStatus
    private ExportStatus exportStatus = ExportStatus.NotExported;

    private Integer purgeable = 0;

    /**
     * Method to add and associate a new detail(item) object with the
     * assignment.
     * 
     * @param newDetail
     *            - the detail(item) to add to the assignment
     */
    public void addDetail(CycleCountingDetail newDetail) {
        getCycleCountingDetails().add(newDetail);
        newDetail.setCycleCountingAssignment((CycleCountingAssignment) this);
    }

    /**
     * @return the cycleCountingDetails
     */
    public List<CycleCountingDetail> getCycleCountingDetails() {
        return cycleCountingDetails;
    }

    /**
     * Used by the importer to get "this" instance of the object.
     * Specifically it is used by the nested look up trigger that 
     * is looking for inprogress assignments.  This is needed by the
     * importer so it can figure out which manager to use to execute the
     * named query
     * @return "this" instance
     */
    public CycleCountingAssignmentRoot getThis() {
        return this;
    }
    
    /**
     * Used by the importer
     * Specifically it is used by the nested look up trigger that 
     * is looking for inprogress assignments.  This is needed by the
     * importer because it attempts to set the "composite" part of the
     * object when executing a lookup trigger
     * We are not setting anything here by design
     * @param thisInstance - 
     */
    public void setThis(CycleCountingAssignmentRoot thisInstance) {
        // Do not do anything
    }
    
    
    /**
     * Used By the importer to update the details of a cycle counting
     * assignment.
     * 
     * @param importDetails
     *            the cycleCountingDetails to set
     */
    public void setImportDetails(List<CycleCountingDetail> importDetails) {

        // Don't change the details of an in-progress or completed assignment.
        if ((this.status != CycleCountStatus.InProgress) && 
            (this.status != CycleCountStatus.Complete) &&
            (this.status != CycleCountStatus.Canceled)) {
            
            this.cycleCountingDetails.removeAll(getCycleCountingDetails());
            this.cycleCountingDetails.addAll(importDetails);
        }
    }

    /**
     * @param cycleCountingDetails
     *            the cycleCountingDetails to set
     */
    public void setCycleCountingDetails(
            List<CycleCountingDetail> cycleCountingDetails) {
        this.cycleCountingDetails = cycleCountingDetails;
    }

    /**
     * @return the region
     */
    public CycleCountingRegion getRegion() {
        return region;
    }

    /**
     * @param region
     *            the region to set
     */
    public void setRegion(CycleCountingRegion region) {
        this.region = region;
    }

    /**
     * @return the sequence
     */
    public Long getSequence() {
        return sequence;
    }

    /**
     * @param sequence
     *            the sequence to set
     */
    public void setSequence(Long sequence) {
        this.sequence = sequence;
    }

    /**
     * @return the dateReceived
     */
    public Date getDateReceived() {
        return dateReceived;
    }

    /**
     * @param dateReceived
     *            the dateReceived to set
     */
    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    /**
     * @return the location
     */
    public Location getLocation() {
        return location;
    }

    /**
     * @param location
     *            the location to set
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Used by the importer
     * 
     * @return the ScannedVerification field of the location
     */
    public String getScannedVerification() {
        return location.getScannedVerification();
    }
    
    /**
     * This is here only to make the importer happy
     */
    public void setScannedVerification(String stupidImporter) {
        
    }
        

    /**
     * @return the status
     */
    public CycleCountStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(CycleCountStatus status) {
        this.status = status;
    }

    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * @param operator
     *            the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime
     *            the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime
     *            the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the labor
     */
    public List<CycleCountingLabor> getLabor() {
        return labor;
    }

    /**
     * @param labor the labor to set
     */
    public void setLabor(List<CycleCountingLabor> labor) {
        this.labor = labor;
    }

    /**
     * @return the tags
     */
    @Override
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * @param tags
     *            the tags to set
     */
    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus
     *            the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * Getter for the purgeable property.
     * 
     * @return Integer value of the property
     */
    public Integer getPurgeable() {
        return purgeable;
    }

    /**
     * Setter for the purgeable property.
     * 
     * @param purgeable
     *            the new purgeable value
     */
    public void setPurgeable(Integer purgeable) {
        this.purgeable = purgeable;
    }

    /**
     * @param toStatus
     *            - the status changing to.
     * @throws BusinessRuleException
     */
    public void updateStatus(CycleCountStatus toStatus)
            throws BusinessRuleException {
        if (toStatus == InProgress) {
            if (getStatus() == Available || getStatus() == Skipped) {
                setStatus(toStatus);
            }
        } else {
            throw new BusinessRuleException(
                    SelectionErrorCode.PICK_INVALID_STATUS_CHANGE,
                    new UserMessage(
                            "pick.updateStatus.error.invalidStatusChange",
                            getStatus().name(), toStatus.name()));

        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((location == null) ? 0 : location.hashCode());
        result = (int) (prime * result + sequence);
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof CycleCountingAssignmentRoot)) {
            return false;
        }
        CycleCountingAssignmentRoot other = (CycleCountingAssignmentRoot) obj;
        if (location == null) {
            if (other.location != null) {
                return false;
            }
        } else if (!location.equals(other.location)) {
            return false;
        }
        if (sequence != other.sequence) {
            return false;
        }
        return true;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 