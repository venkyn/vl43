/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author pkolonay
 *
 */
public class CycleCountingAssignmentImportDataRoot extends BaseModelObject
        implements Importable {

    /**
     * 
     */
    private static final long serialVersionUID = -9133524102126628788L;

    private ImportableImpl impl = new ImportableImpl();
    
    private CycleCountingAssignment     myModel = new CycleCountingAssignment();
    
    private Set<CycleCountingDetailImportData> importableDetailData;

    
    /**
     * Set the number in the region. This will be used to look up a region in the database.
     * @param number the number to set the regionnumber to.
     */
    public void setRegionNumber(String number) {
        CycleCountingRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new CycleCountingRegion();
            setRegion(tempRegion);
        }
        tempRegion.setNumber(new Integer(number));
    }
    

    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public String getRegionNumber() {
        CycleCountingRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new CycleCountingRegion();
            setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return tempRegion.getNumber().toString();
    }
    
    /**
     * @return the myModel
     */
    public CycleCountingAssignment getMyModel() {
        return myModel;
    }

    /**
     * @param myModel the myModel to set
     */
    public void setMyModel(CycleCountingAssignment myModel) {
        this.myModel = myModel;
    }

    /**
     * @return the importableDetailData
     */
    public Set<CycleCountingDetailImportData> getImportableDetailData() {
        return importableDetailData;
    }

    /**
     * @param importableDetailData the importableDetailData to set
     */
    public void setImportableDetailData(
            Set<CycleCountingDetailImportData> importableDetailData) {
        this.importableDetailData = importableDetailData;
    }

    
    
    /**
     * @return - the created date
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#getCreatedDate()
     */
    public Date getCreatedDate() {
        return myModel.getCreatedDate();
    }


    /**
     * @return - the version number
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#getVersion()
     */
    public Integer getVersion() {
        return myModel.getVersion();
    }


    /**
     * @return - the id
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return myModel.getId();
    }


    /**
     * @param version - the version value to set
     * @see com.vocollect.epp.model.VersionedModelObjectRoot#setVersion(java.lang.Integer)
     */
    public void setVersion(Integer version) {
        myModel.setVersion(version);
    }


    /**
     * @param createdDate - the created date to set
     * @see com.vocollect.voicelink.core.model.CommonModelObjectRoot#setCreatedDate(java.util.Date)
     */
    public void setCreatedDate(Date createdDate) {
        myModel.setCreatedDate(createdDate);
    }


    /**
     * @return - the string representation of myModel.
     * @see com.vocollect.epp.model.BaseModelObjectRoot#toString()
     */
    @Override
    public String toString() {
        return myModel.toString();
    }


    /**
     * @param newDetail - the new detail to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#addDetail(com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail)
     */
    public void addDetail(CycleCountingDetail newDetail) {
        myModel.addDetail(newDetail);
    }


    /**
     * @return - the new property
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    @Override
    public boolean isNew() {
        return myModel.isNew();
    }


    /**
     * @return - the cycle counting details
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getCycleCountingDetails()
     */
    public List<CycleCountingDetail> getCycleCountingDetails() {
        return myModel.getCycleCountingDetails();
    }


    /**
     * @param importDetails - new import details to update
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setImportDetails(java.util.List)
     */
    public void setImportDetails(List<CycleCountingDetail> importDetails) {
        // Don't change the details of an in-progress or completed assignment.   
        if (myModel.getStatus() != CycleCountStatus.InProgress &&
                myModel.getStatus() != CycleCountStatus.Complete) {
            myModel.getCycleCountingDetails().removeAll(getCycleCountingDetails());
            myModel.getCycleCountingDetails().addAll(importDetails);
        }
    }


    /**
     * @return - the descriptive text
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return myModel.getDescriptiveText();
    }


    /**
     * @param cycleCountingDetails - the new cycle counting details to set.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setCycleCountingDetails(java.util.List)
     */
    public void setCycleCountingDetails(
            List<CycleCountingDetail> cycleCountingDetails) {
        myModel.setCycleCountingDetails(cycleCountingDetails);
    }


    /**
     * @return - the region
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getRegion()
     */
    public CycleCountingRegion getRegion() {
        return myModel.getRegion();
    }


    /**
     * @param region - the region to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setRegion(com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion)
     */
    public void setRegion(CycleCountingRegion region) {
        myModel.setRegion(region);
    }


    /**
     * @return - the sequence number
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getSequence()
     */
    public Long getSequence() {
        return myModel.getSequence();
    }


    /**
     * @param sequence - the sequence to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setSequence(java.lang.Long)
     */
    public void setSequence(Long sequence) {
        myModel.setSequence(sequence);
    }

    
    /**
     * Set the LocationID (scannedVerification) in the Location.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the Location.scannedVerification to.
     */
    public void setLocationId(String scannedVerification) {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }

    
    /**
     * Get the Location's id (scannedVerification).
     * @return the Location ID (Location.scannedVerification) (String).
     */
    public String getLocationId() {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }

    
    /**
     * @return - the location
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getLocation()
     */
    public Location getLocation() {
        return myModel.getLocation();
    }


    /**
     * @param location - the location to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }


    /**
     * @return - the scanned verification
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getScannedVerification()
     */
    public String getScannedVerification() {
        return myModel.getScannedVerification();
    }


    /**
     * @return - the status
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getStatus()
     */
    public CycleCountStatus getStatus() {
        return myModel.getStatus();
    }


    /**
     * @param status - the new status value to set.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setStatus(com.vocollect.voicelink.cyclecounting.model.CycleCountStatus)
     */
    public void setStatus(CycleCountStatus status) {
        myModel.setStatus(status);
    }


    /**
     * @return - the operator that was got.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getOperator()
     */
    public Operator getOperator() {
        return myModel.getOperator();
    }


    /**
     * @param operator - the operator to set.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public void setOperator(Operator operator) {
        myModel.setOperator(operator);
    }


    /**
     * @return - the start time
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getStartTime()
     */
    public Date getStartTime() {
        return myModel.getStartTime();
    }


    /**
     * @param startTime - the start time to set to.
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setStartTime(java.util.Date)
     */
    public void setStartTime(Date startTime) {
        myModel.setStartTime(startTime);
    }


    /**
     * @return - the end time
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getEndTime()
     */
    public Date getEndTime() {
        return myModel.getEndTime();
    }


    /**
     * @param endTime - the end time to set
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setEndTime(java.util.Date)
     */
    public void setEndTime(Date endTime) {
        myModel.setEndTime(endTime);
    }


    /**
     * @return - the tags
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#getTags()
     */
    public Set<Tag> getTags() {
        return myModel.getTags();
    }


    /**
     * @param tags - the tags to update
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        myModel.setTags(tags);
    }


    /**
     * @param toStatus - the new status
     * @throws BusinessRuleException
     * @see com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignmentRoot#updateStatus(com.vocollect.voicelink.cyclecounting.model.CycleCountStatus)
     */
    public void updateStatus(CycleCountStatus toStatus)
            throws BusinessRuleException {
        myModel.updateStatus(toStatus);
    }


    /**
     * @return - my model
     * @throws VocollectException
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    @Override
    public Object convertToModel() throws VocollectException {
        for (CycleCountingDetailImportData detail : this.importableDetailData) {
            myModel.getCycleCountingDetails().add((CycleCountingDetail) detail.convertToModel());
        }
        
        return myModel;
        
    }


    /**
     * @return - is completed
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    @Override
    public boolean isCompleted() {
        return impl.isCompleted();
    }


    /**
     * @return - in progress
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    @Override
    public boolean isInProgress() {
        return impl.isInProgress();
    }


    /**
     * @param completedParam - the completed param.
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    @Override
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }


    /**
     * @return - the hashcode
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return impl.hashCode();
    }


    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.importer.parsers.Importable#setInProgress(boolean)
     */
    @Override
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }


    /**
     * @return - the import id.
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    @Override
    public Long getImportID() {
        return impl.getImportID();
    }


    /**
     * @param importID - the importId to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.Long)
     */
    @Override
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }


    /**
     * @return - the site name
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    @Override
    public String getSiteName() {
        return impl.getSiteName();
    }


    /**
     * @param siteNameParam - the site name to set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    @Override
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }


    /**
     * @return - the import status
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    @Override
    public int getImportStatus() {
        return impl.getImportStatus();
    }


    /**
     * @param status - the import status set
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportStatus(int)
     */
    @Override
    public void setImportStatus(int status) {
        impl.setImportStatus(status);
    }


    /**
     * @param obj - the object
     * @return - the equality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 