/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.ReasonCode;

import java.io.Serializable;
import java.util.Date;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingDetailRoot extends CommonModelObject implements
        Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8560432409637056762L;

    private CycleCountDetailStatus      status = CycleCountDetailStatus.NotCounted;

    private Item                        item;

    private int                         expectedQuantity;

    private int                         actualQuantity;

    private String                      unitOfMeasure;

    private Date                        countTime;

    private ReasonCode                  reason;

    private CycleCountingAssignment     cycleCountingAssignment;
    
    // ExportStatus
    private ExportStatus      exportStatus = ExportStatus.NotExported;


    /**
     * @return the exportStatus
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * @param exportStatus the exportStatus to set
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * @return the cycleCountingAssignment
     */
    public CycleCountingAssignment getCycleCountingAssignment() {
        return cycleCountingAssignment;
    }

    /**
     * @param ccAssignment
     *            the ccAssignment to set
     */
    public void setCycleCountingAssignment(CycleCountingAssignment ccAssignment) {
        this.cycleCountingAssignment = ccAssignment;
    }

    /**
     * @return the status
     */
    public CycleCountDetailStatus getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(CycleCountDetailStatus status) {
        this.status = status;
    }

    /**
     * @return the item
     */
    public Item getItem() {
        return item;
    }

    /**
     * @param item
     *            the item to set
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * @return the expectedQuantity
     */
    public int getExpectedQuantity() {
        return expectedQuantity;
    }

    /**
     * @param expectedQuantity
     *            the expectedQuantity to set
     */
    public void setExpectedQuantity(int expectedQuantity) {
        this.expectedQuantity = expectedQuantity;
    }

    /**
     * @return the actualQuantity
     */
    public int getActualQuantity() {
        return actualQuantity;
    }

    /**
     * @param actualQuantity
     *            the actualQuantity to set
     */
    public void setActualQuantity(int actualQuantity) {
        this.actualQuantity = actualQuantity;
    }

    /**
     * @return the unitOfMeasure
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * @param unitOfMeasure
     *            the unitOfMeasure to set
     */
    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * @return the countTime
     */
    public Date getCountTime() {
        return countTime;
    }

    /**
     * @param countTime
     *            the countTime to set
     */
    public void setCountTime(Date countTime) {
        this.countTime = countTime;
    }

    /**
     * @return the reason
     */
    public ReasonCode getReason() {
        return reason;
    }

    /**
     * @param reason
     *            the reason to set
     */
    public void setReason(ReasonCode reason) {
        this.reason = reason;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((cycleCountingAssignment == null) ? 0 : cycleCountingAssignment.hashCode());
        result = prime * result + expectedQuantity;
        result = prime * result + ((item == null) ? 0 : item.hashCode());
        result = prime * result
                + ((unitOfMeasure == null) ? 0 : unitOfMeasure.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (!(obj instanceof CycleCountingDetailRoot)) {
            return false;
        }
        CycleCountingDetailRoot other = (CycleCountingDetailRoot) obj;
        if (cycleCountingAssignment == null) {
            if (other.cycleCountingAssignment != null) {
                return false;
            }
        } else if (!cycleCountingAssignment.equals(other.cycleCountingAssignment)) {
            return false;
        }
        if (expectedQuantity != other.expectedQuantity) {
            return false;
        }
        if (item == null) {
            if (other.item != null) {
                return false;
            }
        } else if (!item.equals(other.item)) {
            return false;
        }
        if (unitOfMeasure == null) {
            if (other.unitOfMeasure != null) {
                return false;
            }
        } else if (!unitOfMeasure.equals(other.unitOfMeasure)) {
            return false;
        }
        return true;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 