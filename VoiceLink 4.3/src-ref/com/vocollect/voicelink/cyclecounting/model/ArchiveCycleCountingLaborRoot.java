/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.voicelink.core.model.ArchiveModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorLabor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ktanneru
 * 
 */
public class ArchiveCycleCountingLaborRoot extends ArchiveModelObject implements
    Serializable {

    private static final long serialVersionUID = -2519886191714932238L;

    // Archive License labor detail belongs to
    private ArchiveCycleCountingAssignment assignment;

    // Operator that worked the period of time
    private String operatorName;

    private String operatorIdentifier;

    // Operators Labor Detail record labor associated with.
    private OperatorLabor operatorLabor;

    // The id of the operator break labor record that is associated with the
    // assignment.
    // This property is populated if the operator takes a break in the middle of
    // the assignment.
    private Long breakLaborId;

    // Time Operator started
    private Date startTime;

    // Time Operator Ended
    private Date endTime;

    // The difference between startTime and endTime in milliseconds
    private Long duration;

    // The number of locations counting
    private Integer locationsCounted;

    // actual pick rate for this assignmentId
    private Double actualRate;

    // actual percent of goal
    private Double percentOfGoal;

    private ExportStatus exportStatus = ExportStatus.Exported;

    /**
     * Constructor.
     */
    public ArchiveCycleCountingLaborRoot() {

    }

    /**
     * 
     * Constructor.
     * 
     * @param cycleCountingLabor ArchiveCycleCountingLabor to use to build
     *            ArchiveCycleCountingLabor.
     * @param archiveCycleCountingAssignment - Archive CycleCountingAssignment
     *            labor belongs to.
     */
    public ArchiveCycleCountingLaborRoot(CycleCountingLabor cycleCountingLabor,
                                         ArchiveCycleCountingAssignment archiveCycleCountingAssignment) {
        this.setAssignment(archiveCycleCountingAssignment);
        this.setOperatorLabor(cycleCountingLabor.getOperatorLabor());
        this.setBreakLaborId(cycleCountingLabor.getBreakLaborId());
        this.setStartTime(cycleCountingLabor.getStartTime());
        this.setEndTime(cycleCountingLabor.getEndTime());
        this.setDuration(cycleCountingLabor.getDuration());
        this.setLocationsCounted(cycleCountingLabor.getLocationsCounted());
        this.setLocationsCounted(cycleCountingLabor.getLocationsCounted());
        this.setActualRate(cycleCountingLabor.getActualRate());
        this.setPercentOfGoal(cycleCountingLabor.getPercentOfGoal());
        this.setExportStatus(cycleCountingLabor.getExportStatus());

        if (cycleCountingLabor.getOperator() != null) {
            this.setOperatorIdentifier(cycleCountingLabor.getOperator()
                .getOperatorIdentifier());
            this.setOperatorName(cycleCountingLabor.getOperator().getName());
        } else {
            this.setOperatorIdentifier(" ");
            this.setOperatorName(" ");
        }
    }

    /**
     * @return the archiveCycleCountingAssignment
     */
    public ArchiveCycleCountingAssignment getAssignment() {
        return assignment;
    }

    /**
     * @param assignment the archiveCycleCountingAssignment to set
     */
    public void setAssignment(ArchiveCycleCountingAssignment assignment) {
        this.assignment = assignment;
    }

    /**
     * @return the operatorIdentifier
     */
    public String getOperatorIdentifier() {
        return operatorIdentifier;
    }

    /**
     * @param operatorIdentifier the operatorIdentifier to set
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * @return the operatorName
     */
    public String getOperatorName() {
        return operatorName;
    }

    /**
     * @param operatorName the operatorName to set
     */
    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    /**
     * @return the operatorLabor
     */
    public OperatorLabor getOperatorLabor() {
        return operatorLabor;
    }

    /**
     * @param operatorLabor the operatorLabor to set
     */
    public void setOperatorLabor(OperatorLabor operatorLabor) {
        this.operatorLabor = operatorLabor;
    }

    /**
     * @return the breakLaborId
     */
    public Long getBreakLaborId() {
        return breakLaborId;
    }

    /**
     * @param breakLaborId the breakLaborId to set
     */
    public void setBreakLaborId(Long breakLaborId) {
        this.breakLaborId = breakLaborId;
    }

    /**
     * @return the startTime
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * Getter for the locationsCounted property.
     * @return Long value of the property
     */
    public Integer getLocationsCounted() {
        return locationsCounted;
    }

    /**
     * Setter for the locationsCounted property.
     * @param locationsCounted the new locationsCounted value
     */
    public void setLocationsCounted(Integer locationsCounted) {
        this.locationsCounted = locationsCounted;
    }

    /**
     * Getter for the actualRate property.
     * @return Double value of the property
     */
    public Double getActualRate() {
        return actualRate;
    }

    /**
     * Setter for the actualRate property.
     * @param actualRate the new actualRate value
     */
    public void setActualRate(Double actualRate) {
        this.actualRate = actualRate;
    }

    /**
     * Getter for the percentOfGoal property.
     * @return Double value of the property
     */
    public Double getPercentOfGoal() {
        return percentOfGoal;
    }

    /**
     * Setter for the percentOfGoal property.
     * @param percentOfGoal the new percentOfGoal value
     */
    public void setPercentOfGoal(Double percentOfGoal) {
        this.percentOfGoal = percentOfGoal;
    }

    /**
     * Getter for the exportStatus property.
     * @return ExportStatus value of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */
    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 