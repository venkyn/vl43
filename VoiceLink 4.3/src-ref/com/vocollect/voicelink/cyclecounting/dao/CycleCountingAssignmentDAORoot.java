/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * CycleCounting Assignment Data Access Object (DAO) interface.
 * 
 * @author pkolonay
 */
public interface CycleCountingAssignmentDAORoot extends
        GenericDAO<CycleCountingAssignment> {

    /**
     * @param operatorId
     *            - the operator id to search by.
     * @return - the cyclecount
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentsByOperator(Long operatorId)
            throws DataAccessException;

    /**
     * @param operatorId
     *            - the operator id to search by.
     * @return - the cyclecount
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentsByOperatorRegion(
            Long operatorId) throws DataAccessException;

    /**
     * @param operatorId
     *            - the operator id to search by.
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentsByRegion(Long operatorId);

    /**
     * @param operatorId
     *            - the operator id to search by.
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listSkippedAssignmentsByRegion(Long operatorId);

    /**
     * @param locationId
     *            - the location id
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentByLocation(String locationId)
            throws DataAccessException;

    /**
     * @param locationId
     *            - the location id
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentByScannedLocation(
            String locationId) throws DataAccessException;

    /**
     * @param locationId
     *            - the location id
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAvailableAssignmentByLocation(
            String locationId) throws DataAccessException;

    /**
     * @param locationId
     *            - the location id
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listInProgressAssignmentByScannedLocation(
            String locationId) throws DataAccessException;

    /**
     * @param locationId
     *            - the location id
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentByScannedLocationAndUpdateableStatus(
                                     String locationId) throws DataAccessException;
    
    
    
    /**
     * @param operatorId
     *            - the operator id
     * @param spokenVerification
     *            - the spoken verification
     * @return - the cyclecount assignments
     * @throws DataAccessException
     */
    List<CycleCountingAssignment> listAssignmentsByOperatorLocation(
            Long operatorId, String spokenVerification)
            throws DataAccessException;

    /**
     * @param operatorId
     *            - the operator id
     * @return the cyclecount assignments
     */
    List<CycleCountingAssignment> listInProgressAssignmentsByOperator(
            Long operatorId) throws DataAccessException;

    /**
     * @param operatorId
     *            - the operator id
     * @param spokenVerification
     *            - the spoken location
     * @param itemNumber
     *            - the item number
     * @param uom
     *            - the unit of measure
     * @return - the detail
     * @throws DataAccessException
     */
    CycleCountingDetail findDetail(Long operatorId, String spokenVerification,
            String itemNumber, String uom) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listExportableAssignments(
            QueryDecorator qd) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map finished export
     */
    List<Map<String, Object>> listExportFinish(QueryDecorator qd)
            throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of cycle counting assignment record
     */
    List<Map<String, Object>> listExportLaborRecords(QueryDecorator qd)
            throws DataAccessException;

    /**
     * @return number of record updated
     */
    Integer updateCompletedAssignments() throws DataAccessException;

    /**
     * Updates Cycle Counting Assignments that are ready to be purged or
     * archived.
     * 
     * @param purgeable
     *            - whether to mark for archive(1) or purge (2)
     * @param date
     *            - Date to find assignment older than
     * @param status
     *            - status of assignments to find
     * @return - number of records updated
     * @throws DataAccessException
     *             - database exceptions
     */
    Integer updateOlderThan(Integer purgeable, Date date,
            CycleCountStatus status) throws DataAccessException;

    /**
     * Get a specified number of Cycle Counting Assignments to archive.
     * 
     * @param queryDecorator
     *            - query decorator for number of rows to archive
     * @return - list of Cycle Counting assignments
     * @throws DataAccessException
     *             - database exceptions
     */
    List<CycleCountingAssignment> listArchive(QueryDecorator queryDecorator)
            throws DataAccessException;

    /**
     * @param queryDecorator
     *            query decorator for number of rows to delete
     * @return the rows to delete
     */
    List<CycleCountingAssignment> listPurge(QueryDecorator queryDecorator)
            throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listDetailsReasonCode(
            QueryDecorator qd, Long arg) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listDetailsNoReasonCode(
            QueryDecorator qd, Long arg) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listDetailsNoItemsReasonCode(
            QueryDecorator qd) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listDetailsNoItemsNoReasonCode(
            QueryDecorator qd) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listDetailsItemsNoOperatorReasonCode(
            QueryDecorator qd, Long arg) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listDetailsItemsNoOperatorNoReasonCode(
            QueryDecorator qd, Long arg) throws DataAccessException;

    
    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listDetailsItemsNoOperatorNoItemsReasonCode(
            QueryDecorator qd) throws DataAccessException;
    
    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listDetailsItemsNoOperatorNoItemsNoReasonCode(
            QueryDecorator qd) throws DataAccessException;
    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listExportableDetails(
            QueryDecorator qd) throws DataAccessException;
    
    
    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Long> listNoOperatorItems(
            QueryDecorator qd) throws DataAccessException;
    
 
    
    /**
     * Updates pick details that are ready to be exported.
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateDetailsExportStatus() 
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 