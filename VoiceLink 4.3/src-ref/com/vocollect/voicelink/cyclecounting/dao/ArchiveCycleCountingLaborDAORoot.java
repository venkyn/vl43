/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingLabor;

import java.util.Date;
import java.util.List;

/**
 * Data Access Object for ArchiveCycleCountingLabor.
 * @author ktanneru
 * 
 */
public interface ArchiveCycleCountingLaborDAORoot extends
    GenericDAO<ArchiveCycleCountingLabor> {

    /**
     * Finds all Archive Cycle Counting Assignment Labor records.
     * @param decorator - query decorator
     * @return - list of Archive Cycle Counting Assignment Labor
     * @throws DataAccessException - Database Exception
     */
    List<DataObject> listArchiveCycleCountingLabor(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get Archive Labor Summary Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(String operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;

    /**
     * Get Archive Detailed Labor Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDetailReportRecords(String operatorIdentifier,
                                                Date startDate,
                                                Date endDate)
        throws DataAccessException;

    /**
     * Get labor duration by operator, region and date range.
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDurationByOperRegionDates(String operatorIdentifier,
                                                      Integer regionNumber,
                                                      Date startDate,
                                                      Date endDate)
        throws DataAccessException;

    /**
     * Sum the locations counted by an operator for a date span.
     * @param operatorLaborId - OperatorLabor record Id.
     * @param startTime - start of date range.
     * @param endTime - end of date range
     * @return Number - the sum of the loctions counted that meets the criteria.
     *         Can be zero.
     * @throws DataAccessException - database exceptions
     */
    Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                           Date startTime,
                                           Date endTime)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 