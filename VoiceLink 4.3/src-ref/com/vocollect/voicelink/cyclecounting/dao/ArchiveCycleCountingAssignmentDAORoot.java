/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.cyclecounting.model.ArchiveCycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;

import java.util.Date;
import java.util.List;

/**
 * @author ktanneru
 *
 */
public interface ArchiveCycleCountingAssignmentDAORoot extends GenericDAO<ArchiveCycleCountingAssignment> {
    
    /**
     * Gets a list of archive cycle counting assignments based on date and status.
     * @param createdDate date criteria
     * @param status status criteria
     * @return a list of archived cycle counting assignments
     * @throws DataAccessException - database exception
     */
    Integer updateOlderThan(Date createdDate, CycleCountStatus status)
        throws DataAccessException;
    
    /**
     * Get all Assignments.
     * @param decorator - additional query instructions.
     * @return the List of assignment details.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listArchiveCycleCountingAssignments(QueryDecorator decorator)
        throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 