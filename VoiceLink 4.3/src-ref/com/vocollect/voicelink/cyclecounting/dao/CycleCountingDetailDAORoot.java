/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;


import java.util.List;
import java.util.Map;
/**
 * @author pkolonay
 * 
 */
public interface CycleCountingDetailDAORoot extends
        GenericDAO<CycleCountingDetail> {
    
    
    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has an operator
     */
    List<Map<String, Object>> listDetails(
            QueryDecorator qd) throws DataAccessException;

    /**
     * @param qd
     *            The query descriptor
     * @return Map of exportable assignments which has no operator
     */
    List<Map<String, Object>> listDetailsNoOperator(
            QueryDecorator qd) throws DataAccessException;

    

    /**
     * Updates pick details that are ready to be exported.
     * @return - number of records updated
     * @throws DataAccessException - database exceptions
     */
    Integer updateDetailsExportStatus() 
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 