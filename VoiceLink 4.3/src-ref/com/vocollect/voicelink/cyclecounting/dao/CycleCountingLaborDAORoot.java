/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;

import java.util.Date;
import java.util.List;

/**
 * @author khazra
 * 
 */
public interface CycleCountingLaborDAORoot extends
    GenericDAO<CycleCountingLabor> {

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listOpenRecordsByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the closed assignment labor records for the given operator or
     * returns null if there is no closed assignment labor records.
     * 
     * @param operatorId - the Id of the Operator
     * @return The requested Operator Labor record or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listRecordsByOperatorId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves the open assignment labor record for the given assignment. or
     * returns null if there is no open assignment labor record.
     * 
     * @param operatorId - The Id of the assignment.
     * @return The requested assignment laobr record or null
     * @throws DataAccessException on any failure.
     */
    CycleCountingLabor findOpenRecordByAssignmentId(long operatorId)
        throws DataAccessException;

    /**
     * Retrieves closed labor records by break labor id.
     * 
     * @param breakLaborId the break id
     * @return Cycle Counting labor record
     * @throws DataAccessException
     */
    CycleCountingLabor findClosedRecordsByBreakLaborId(long breakLaborId)
        throws DataAccessException;

    /**
     * Sum the locations counted field of the cc assignment labor records by operator
     * labor id.
     * 
     * @param operatorLaborId - OperatorLabor record Id. 
     * @return Integer - Number of locations counted, can be zero. 
     * @throws DataAccessException - database exceptions
     */
    Integer sumLocationsCounted(long operatorLaborId) throws DataAccessException;

    /**
     * Retrieves the open assignment labor records for the given operator or
     * returns null if there is no open assignment labor records.
     * 
     * @param operatorLaborId - the ID of the Operator
     * @return the requested list of assignment labor records or null
     * @throws DataAccessException on any failure.
     */
    List<CycleCountingLabor> listAllRecordsByOperatorLaborId(long operatorLaborId)
        throws DataAccessException;

    /**
     * Get all assignment labor records associated with any of the specified
     * operator labor records.
     * 
     * @param decorator - additional query instructions.
     * @return the List of assignment labor records.
     * @throws DataAccessException - database exceptions.
     */
    List<DataObject> listAssignmentLabor(QueryDecorator decorator)
        throws DataAccessException;

    /**
     * Get Labor Summary Records for an operator and a date range.
     * @param operatorIdentifier - operator
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborSummaryReportRecords(Long operatorIdentifier,
                                                 Date startDate,
                                                 Date endDate)
        throws DataAccessException;
    
    /**
     * Get Labor Detail Records for an operator and a date range.
     * @param operatorId - operator 
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return list of object 
     * @throws DataAccessException on db failure
     */
    List<Object[]> listLaborDetailReportRecords(Long operatorId, 
                                               Date startDate, Date endDate) 
                                               throws DataAccessException;

    /**
     * Get labor duration by operator, region and date range.
     * @param operatorIdentifier - operator
     * @param regionNumber - region number
     * @param startDate - start of date range
     * @param endDate - end of date range
     * @return - list of objects
     * @throws DataAccessException - on db failure
     */
    List<Object[]> listLaborDurationByOperRegionDates(Long operatorIdentifier,
                                                      Integer regionNumber,
                                                      Date startDate,
                                                      Date endDate)
        throws DataAccessException;

    /**
     * Sum the locations counted by an operator for a date span.
     * @param operatorLaborId - OperatorLabor record Id.
     * @param startTime - start of date range.
     * @param endTime - end of date range
     * @return Number - the sum of the loctions counted that meets the criteria.
     *         Can be zero.
     * @throws DataAccessException - database exceptions
     */
    Number sumLocationsCountedBetweenDates(Long operatorLaborId,
                                           Date startTime,
                                           Date endTime)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 