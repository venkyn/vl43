/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for selection business. Although the
 * range for these error codes is 20000-20999, please use only 20000-20499 for
 * the predefined ones; 20499-20999 should be reserved for customization error
 * codes.
 *
 * @author khazra
 */
public class CycleCountingErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 20000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 20999;

    /**
     * No error, just the base initialization.
     */
    public static final CycleCountingErrorCodeRoot NO_ERROR
        = new CycleCountingErrorCodeRoot();

    /**
     * Constructor.
     */
    private CycleCountingErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected CycleCountingErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(CycleCountingErrorCodeRoot.NO_ERROR, err);
    }


    //===================================================================
    //Cycle Counting Errors
    //===================================================================


    /**
     * cannot edit or delete a region when operators are signed in.
     */
    public static final CycleCountingErrorCodeRoot REGION_OPERATORS_SIGNED_IN
    = new CycleCountingErrorCodeRoot(20001);

    /**
     * cannot edit a region when operators are signed in.
     */
    public static final CycleCountingErrorCodeRoot REPLENISHMENT_CANNOT_CHANGE_STATUS
    = new CycleCountingErrorCodeRoot(20002);

    /**
     * Can not edit an assignment when current status is "In progress,  Complete, or Canceled".
     */
    public static final CycleCountingErrorCodeRoot EDIT_ASSIGNMENT_INVALID_STATUS
    = new CycleCountingErrorCodeRoot(20003);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 