/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.lineloading.dao.LineLoadingRegionDAO;
import com.vocollect.voicelink.lineloading.model.LineLoadingRegion;
import com.vocollect.voicelink.lineloading.model.PalletIdentificationMode;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;

/**
 * Action class for LineLoadingRegion.
 * @author snalan
 */
@SuppressWarnings("serial")
public class LineLoadingRegionActionRoot extends RegionAction implements Preparable {

    private static final int PALLET_LABLE_MAX_LIMIT = 50;

    private Logger log = new Logger(RouteStopActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1037;

    // The routestops management service.
    private GenericManager<LineLoadingRegion, LineLoadingRegionDAO> lineLoadingRegionManager;

    // The lineLoadingRegion object, which will either be newly created, or retrieved
    // via the LineLoadingRegionId.
    private LineLoadingRegion lineLoadingRegion;

    // To indicate if the action is a view or save
    private String actionValue;

    /**
     * @param lineLoadingRegion The lineLoadingRegion to set.
     */
    public void setLineLoadingRegion(LineLoadingRegion lineLoadingRegion) {
        this.lineLoadingRegion = lineLoadingRegion;
    }

    /**
     * @return The lineLoadingRegion property.
     */
    public LineLoadingRegion getLineLoadingRegion() {
        return this.lineLoadingRegion;
    }


    /**
     * Getter for the lineLoadingRegionManager property.
     * @return value of the property.
     */
    public GenericManager<LineLoadingRegion, LineLoadingRegionDAO> getLineLoadingRegionManager() {
        return this.lineLoadingRegionManager;
    }

    /**
     * Setter for the lineLoadingRegionManager property.
     * @param manager the new lineLoadingRegionManager value.
     */
    public void setLineLoadingRegionManager(GenericManager<LineLoadingRegion, LineLoadingRegionDAO> manager) {
        this.lineLoadingRegionManager = manager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "lineLoadingRegion";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getLineLoadingRegionManager();
    }

    /**
     * This method sets up the <code>selectionRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);
        log.debug("prepare");
        log.debug("regionId = " + getRegionId());
        if (getRegionId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.lineLoadingRegion = (LineLoadingRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.lineLoadingRegion = this.lineLoadingRegionManager.get(getRegionId());
                saveEntityInSession(this.lineLoadingRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Region version is: " + this.lineLoadingRegion.getVersion());
            }
        } else if (this.lineLoadingRegion == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new region object");
            }
            this.lineLoadingRegion = new LineLoadingRegion();
        }

        // To reset the checkbox values, only before saving the model.
        if (this.actionValue != null) {
            this.lineLoadingRegion.setAllowCloseStopFromTask(false);
            this.lineLoadingRegion.setAutoPrintManifestReport(false);
        }

    }


    /**
     * The validate method from Action Support is overriden to validate the inputs before
     * action methods are called.
     */
    @Override
    public void validate() {

            Integer palletIdStart = lineLoadingRegion.getPalletLabelLayout().getPalletIDStart();
            Integer palletIdLength = lineLoadingRegion.getPalletLabelLayout().getPalletIDLength();

            Integer routeIdStart = lineLoadingRegion.getPalletLabelLayout().getRouteStart();
            Integer routeIdLength = lineLoadingRegion.getPalletLabelLayout().getRouteLength();

            Integer stopIdStart = lineLoadingRegion.getPalletLabelLayout().getStopStart();
            Integer stopIdLength = lineLoadingRegion.getPalletLabelLayout().getStopLength();

            // Validation to check if the Pallet Label exceeds the maximum specified limit

            if (palletIdStart != null && palletIdLength != null) {

                if ((palletIdStart.intValue() + palletIdLength.intValue()) > PALLET_LABLE_MAX_LIMIT) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.palletIDStart",
                        getText("lineloading.region.pallet.label.moreThanFifty"));
                }
            }

            if (routeIdStart != null && routeIdLength != null) {
                if ((routeIdStart.intValue() + routeIdLength.intValue()) > PALLET_LABLE_MAX_LIMIT) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.routeStart",
                        getText("lineloading.region.pallet.label.moreThanFifty"));
                }
            }

            if (stopIdStart != null && stopIdLength != null) {
                if ((stopIdStart.intValue() + stopIdLength.intValue()) > PALLET_LABLE_MAX_LIMIT) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.stopStart",
                        getText("lineloading.region.pallet.label.moreThanFifty"));
                }
            }

            if (routeIdStart != null && palletIdStart != null && palletIdLength != null) {
                if (((routeIdStart.intValue() >= palletIdStart.intValue())
                    && (routeIdStart.intValue() < palletIdStart.intValue() + palletIdLength.intValue()))
                    || ((palletIdStart.intValue() >= routeIdStart.intValue())
                        && (palletIdStart.intValue() < routeIdStart.intValue() + routeIdLength.intValue()))) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.palletIDStart",
                         getText("lineloading.region.pallet.route.overlapp"));
                }
            }

            if (stopIdStart != null && routeIdStart != null && stopIdLength != null && routeIdLength != null) {
                if (((stopIdStart.intValue() >= routeIdStart.intValue())
                    && (stopIdStart.intValue() < routeIdStart.intValue() + routeIdLength.intValue()))
                    || ((routeIdStart.intValue() >= stopIdStart.intValue())
                        && (routeIdStart.intValue() < stopIdStart.intValue() + stopIdLength.intValue()))) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.routeStart",
                        getText("lineloading.region.route.stop.overlapp"));
                }
            }

            if (stopIdStart != null && palletIdStart != null && stopIdLength != null && palletIdLength != null) {
                if (((stopIdStart.intValue() >= palletIdStart.intValue())
                    && (stopIdStart.intValue() < palletIdStart.intValue() + palletIdLength.intValue()))
                    || ((palletIdStart.intValue() >= stopIdStart.intValue())
                        && (palletIdStart.intValue() < stopIdStart.intValue() + stopIdLength.intValue()))) {
                    addFieldError("lineLoadingRegion.palletLabelLayout.stopStart",
                        getText("lineloading.region.pallet.stop.overlapp"));
                }
            }

    }

    /**
     * Saves the data from the Line Loading Page.
     * @return INPUT
     * @throws Exception General Exception
     */
    public String save() throws Exception {

        boolean isNew = this.lineLoadingRegion.isNew();

        try {
            lineLoadingRegion.setType(RegionType.LineLoading);
            lineLoadingRegionManager.save(this.lineLoadingRegion);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                if (fve.getField() == "region.name") {
                    fve.setField("lineLoadingRegion.name");
                } else {
                    fve.setField("lineLoadingRegion.number");
                }
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified lineLoadingRegion "
                + this.lineLoadingRegion.getId());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Region"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If item has been deleted, this will throw EntityNotFoundException
            try {
                LineLoadingRegion modifiedItem = getLineLoadingRegionManager().get(getRegionId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.lineLoadingRegion.setVersion(modifiedItem.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedItem);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.lineLoadingRegion.getNumber().toString())
                        , null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "region." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/region/view.action?regionId=" + lineLoadingRegion.getId()),
                this.lineLoadingRegion.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Function for validating if a region is editable or not.
     *
     * @return String  success string
     * @throws Exception - any exception
     */
    public String checkIfEditable() throws Exception {
        try {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }
                this.lineLoadingRegion = (LineLoadingRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.lineLoadingRegion = this.lineLoadingRegionManager.get(getIds()[0]);
                saveEntityInSession(this.lineLoadingRegion);
            }
            ((LineLoadingRegionManager) lineLoadingRegionManager).
                                executeValidateEditRegion(this.lineLoadingRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        setJsonMessage("edit!input.action?regionId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Deletes the currently viewed region.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

        LineLoadingRegion regionToDelete = null;

        try {
            regionToDelete = lineLoadingRegionManager.get(getRegionId());
            lineLoadingRegionManager.delete(getRegionId());

            addSessionActionMessage(new UserMessage(
                "selectionRegion.delete.message.success", regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }


    /**
     * Getter for the LINELOADINGREGION_VIEW_ID property.
     * @return long value of the property
     */
    public static long getLineLoadingRegionViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

   /**
   * @return actionValue
   */
   public String getActionValue() {
      return actionValue;
   }

 /**
   * @param actionValue actionValue
   */
   public void setActionValue(String actionValue) {
      this.actionValue = actionValue;
   }

   /**
    * {@inheritDoc}
    * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
    */
   @Override
   protected long getStaticViewId() {
       return VIEW_ID;
   }

   /**
    * {@inheritDoc}
    * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
    */
   @Override
   protected Region getRegion() {
       return getLineLoadingRegion();
   }

   /**
    * Getter for the PalletIdentificationMode property.
    * @return String value of the property
    */
   public String getPalletIdentificationMode() {
      return   getText(PalletIdentificationMode.
           ScanPalletForEveryCarton.getResourceKey());
 }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 