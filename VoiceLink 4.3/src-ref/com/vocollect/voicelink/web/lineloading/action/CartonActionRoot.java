/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.lineloading.service.PalletManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Struts action class to handle Carton actions.
 *
 * @author ypore
 */
public class CartonActionRoot extends DataProviderAction implements Preparable {

    //
    private static final long serialVersionUID = -4770860169357920094L;

    private static final Logger log = new Logger(CartonAction.class);

    private CartonManager cartonManager = null;

    private static final long CARTON_VIEW_ID = -1101;

    private List<Column> cartonColumns;

    private Carton carton;

    private Long cartonId;

    // Operator Manager.
    private OperatorManager operatorManager = null;

    // selected pallet from the manual load dropdown box.
    private String palletID = null;

    private Long operatorID;

    private PalletManager palletManager = null;

    // to check if the shorted confirm message should be displayed.
    private boolean shortedConfirmed;

    private String routeStopId;

    private String status;

    /**
     * Action for the pallets view page. Initializes the pallets table columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View cartonView = getUserPreferencesManager().getView(CARTON_VIEW_ID);
        this.cartonColumns = this.getUserPreferencesManager().getColumns(
            cartonView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * /** {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Carton";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getCartonManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CARTON_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.cartonId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {

                this.carton = (Carton) getEntityFromSession(getSavedEntityKey());
            } else {
                this.carton = this.cartonManager.get(this.cartonId);
                saveEntityInSession(this.carton);
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("Pallet version is: " + this.carton.getVersion());
        }

    }

    /**
     * Getter for the cartonManager property.
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return this.cartonManager;
    }

    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

    /**
     * Getter for the carton property.
     * @return Carton value of the property
     */
    public Carton getCarton() {
        return this.carton;
    }

    /**
     * Setter for the carton property.
     * @param carton the new carton value
     */
    public void setCarton(Carton carton) {
        this.carton = carton;
    }

    /**
     * Getter for the cartonColumns property.
     * @return List</Column/> value of the property
     */
    public List<Column> getCartonColumns() {
        return this.cartonColumns;
    }

    /**
     * Setter for the cartonColumns property.
     * @param cartonColumns the new cartonColumns value
     */
    public void setCartonColumns(List<Column> cartonColumns) {
        this.cartonColumns = cartonColumns;
    }

    /**
     * Getter for the CARTON_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCartonViewId() {
        return CARTON_VIEW_ID;
    }

    /**
     * Getter for the cartonId property.
     * @return Long value of the property
     */
    public Long getCartonId() {
        return this.cartonId;
    }

    /**
     * Setter for the cartonId property.
     * @param cartonId the new cartonId value
     */
    public void setCartonId(Long cartonId) {
        this.cartonId = cartonId;
    }

    /**
     * Getter for the palletID property.
     * @return String value of the property
     */
    public String getPalletID() {
        return this.palletID;
    }

    /**
     * Setter for the palletID property.
     * @param palletID the new palletID value
     */
    public void setPalletID(String palletID) {
        this.palletID = palletID;
    }

    /**
     * For getting carton table data on pallets view page.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     *
     */
    public String getPalletCartonTableData() throws Exception {
        if (getPalletID() != null && getPalletID().length() > 0) {
            super.getTableData("obj.pallet.id in ( " + this.palletID
                + " )");
        } else {
            super.getTableData("obj.pallet.id = 0");
        }

        return SUCCESS;
    }

    /**
     * For validating if manual load is allowed on the given carton.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     *
     */
    public String getDataForManualLoad() throws Exception {
        if (getShortedConfirmed()) {
            Carton c = getCartonManager().get(getIds()[0]);
            if (c.getStatus().equals(CartonStatus.Loaded)) {
                setJsonMessage(
                    getText(new UserMessage(
                        "carton.manualLoad.shorted.text", ERROR_PARTIAL_SUCCESS)),
                    ERROR_PARTIAL_SUCCESS);
                return SUCCESS;
            }
        }
        return INPUT;

    }

    /**
     * getter method for shorted confirmed.
     * @return boolean if shorted confirm box should be shown.
     */
    public boolean getShortedConfirmed() {
        return this.shortedConfirmed;
    }

    /**
     * setter method for shorted confirmed.
     * @param shortedConfirmed to be set.
     */
    public void setShortedConfirmed(boolean shortedConfirmed) {
        this.shortedConfirmed = shortedConfirmed;
    }

    /**
     * For validate and execute manual load on selected carton.
     * @return String value of outcome.
     * @throws BusinessRuleException in case of business rule exception.
     *
     */
    public String manualLoad() throws BusinessRuleException {

        if (!"0".equals(getPalletID()) && getOperatorID() != 0) {

            try {
                Operator o = null;
                if (getOperatorID() != null) {
                    o = getOperatorManager().get(getOperatorID());
                }
                Carton c = getCartonManager().get(getIds()[0]);
                Pallet p = getPalletManager()
                    .get(Long.parseLong(getPalletID()));
                getCartonManager().executeManualLoad(c, p, o);
                setJsonMessage(
                    getText(new UserMessage(
                        "lineloading.manualload.success.message", ERROR_SUCCESS)),
                    ERROR_SUCCESS);

            } catch (DataAccessException e) {
                log.error(
                    "Error while manual loading cartons",
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
                setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
                return SUCCESS;
            } catch (BusinessRuleException e) {
                setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
                log.error(
                    "Error in carton manual load",
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
                return SUCCESS;
            }
        } else {
            setJsonMessage(
                getText(new UserMessage(
                    "lineloading.error.noselection.message", ERROR_FAILURE)),
                ERROR_FAILURE);

        }

        return SUCCESS;
    }

    /**
     * Returns the list of operators in the region of the routstop.
     * @return map of operators in a regions.
     * @throws DataAccessException on database error.
     */
    public Map<Long, String> getAllOperatorsInRegion()
        throws DataAccessException {
        Map<Long, String> operators = new LinkedHashMap<Long, String>();
        operators.put(0L, getText(new UserMessage("operator.none")));
        Region reg = getCartonManager().get(getIds()[0]).getRouteStop()
            .getRegion();
        List<Operator> allOperators = getOperatorManager()
            .listAuthorizedForRegion(reg.getId());
        for (Operator operatorname : allOperators) {
            operators.put(operatorname.getId(), operatorname
                .getOperatorIdentifier());
        }
        return operators;
    }

    /**
     * Returns the list of pallets in the routstop.
     * @return map of pallets for a routstop.
     * @throws DataAccessException on database error.
     */
    public Map<Long, String> getOpenPalletsForRoutStop()
        throws DataAccessException {
        Map<Long, String> allPallets = new LinkedHashMap<Long, String>();
        allPallets.put(0L, getText(new UserMessage("operator.none")));
        Set<Pallet> pallets = getCartonManager().get(getIds()[0])
            .getRouteStop().getPallets();
        for (Pallet pallet : pallets) {
            allPallets.put(pallet.getId(), pallet.getNumber());
        }
        return allPallets;
    }

    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Getter for the operatorID property.
     * @return Long value of the property
     */
    public Long getOperatorID() {
        return this.operatorID;
    }

    /**
     * Setter for the operatorID property.
     * @param operatorID the new operatorID value
     */
    public void setOperatorID(Long operatorID) {
        this.operatorID = operatorID;
    }

    /**
     * Getter for the palletManager property.
     * @return PalletManager value of the property
     */
    public PalletManager getPalletManager() {
        return this.palletManager;
    }

    /**
     * Setter for the palletManager property.
     * @param palletManager the new palletManager value
     */
    public void setPalletManager(PalletManager palletManager) {
        this.palletManager = palletManager;
    }

    /**
     * Getter for the routeStopId property.
     * @return String value of the property
     */
    public String getRouteStopId() {
        return this.routeStopId;
    }

    /**
     * Setter for the routeStopId property.
     * @param routeStopId the new routeStopId value
     */
    public void setRouteStopId(String routeStopId) {
        this.routeStopId = routeStopId;
    }

    /**
     * Getter for the status property.
     * @return String value of the property
     */
    public String getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(String status) {
        this.status = status;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 