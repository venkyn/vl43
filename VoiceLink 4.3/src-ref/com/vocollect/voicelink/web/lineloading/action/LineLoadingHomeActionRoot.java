/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.web.selection.action.VoiceLinkHomeAction;

/**
 * Decides on a default page for the current user.
 *
 * @author ypore
 */
public class LineLoadingHomeActionRoot extends VoiceLinkHomeAction {

    private static final long serialVersionUID = -4141437391682812970L;

    private static final long LINELOADING_HOMEPAGE_ID = -6;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.ConfigurableHomepagesActionRoot#getHomepageId()
     */
    @Override
    public long getHomepageId() {
        return LINELOADING_HOMEPAGE_ID;
    }

    /**
     * Returns the List of summaries.
     * @return Long
     * @throws DataAccessException if unable to retrieve summaries
     */
    public String getLineLoadingHomeSummaries() throws DataAccessException {
        if (hasGroupAccess(VOICELINK_LINELOADING_GROUP_NAME)) {
            getAdminHomeSummaries();
            return SUCCESS;
        } else if (hasGroupAccess(VOICELINK_SELECTION_GROUP_NAME)) {
            return VOICELINK_SELECTION_HOME;
        } else if (hasGroupAccess(VOICELINK_PUTAWAY_GROUP_NAME)) {
            return VOICELINK_PUTAWAY_HOME;
        } else if (hasGroupAccess(VOICELINK_REPLENISHMENT_GROUP_NAME)) {
            return VOICELINK_REPLENISHMENT_HOME;
        } else if (hasGroupAccess(VOICELINK_LOADING_GROUP_NAME)) {
            return VOICELINK_LOADING_HOME;
        } else if (hasGroupAccess(VOICELINK_CYCLECOUNTING_GROUP_NAME)) {
            return VOICELINK_CYCLECOUNTING_HOME;
        } else {
            return noAccessMessage();
        }
    }

    /**
     * Adds summaries to db and returns.
     * @return - control flow of action
     * @throws DataAccessException - while trying to retrieve data
     * @throws BusinessRuleException - throws a business rule exception if
     *             unable to save
     */
    public String addSummariesForLineLoading() throws DataAccessException,
        BusinessRuleException {
        super.addSummaries();
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 