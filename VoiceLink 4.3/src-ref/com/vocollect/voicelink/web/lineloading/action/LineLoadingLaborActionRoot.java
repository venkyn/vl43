/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.web.core.action.LaborAction;

/**
 * This class handles all line loading labor actions from the user interface.
 * @author brupert
 */
public class LineLoadingLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = -2698641313632373583L;

    /**
     * Action method for LineLoading home page labor summary (table component).
     * @return summary data to display
     * @throws Exception On error.
     */
    public String getSummaryData() throws Exception {
        return this.getTableData(null, new Object[]{ OperatorLaborFilterType.LineLoading, RegionType.LineLoading });
    }


    /**
     * Action method for Putaway home page labor summary (Printable version action).
     * @return summary data to print
     * @throws Exception On error.
     */
    public String getSummaryPrintData() throws Exception {
        return super.getPrintData(null, new Object[]{ OperatorLaborFilterType.LineLoading, RegionType.LineLoading });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String list() throws Exception {
        super.list();
        if (getFilterByFunction()) {
            overrideFunctionFilter(OperatorLaborFilterType.LineLoading);
        }
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 