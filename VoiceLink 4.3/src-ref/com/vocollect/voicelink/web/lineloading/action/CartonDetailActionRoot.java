/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.lineloading.model.CartonDetail;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * Struts action Class to handle Carton Detail actions.
 *
 * @author ypore
 */
public class CartonDetailActionRoot extends DataProviderAction implements
    Preparable {

    //
    private static final long serialVersionUID = 6020182215978571276L;

    private static final Logger log = new Logger(CartonAction.class);

    private CartonDetailManager cartonDetailManager = null;

    private static final long CARTON_DETAIL_VIEW_ID = -1102;

    private List<Column> cartonDetailsColumns;

    private CartonDetail cartonDetail;

    private Long cartonDetailId;

    private Long cartonId;

    /**
     * Action for the pallets view page. Initializes the pallets table columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View cartonDetailsView = getUserPreferencesManager().getView(
            CARTON_DETAIL_VIEW_ID);
        this.cartonDetailsColumns = this.getUserPreferencesManager()
            .getColumns(cartonDetailsView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * /** {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "CartonDetail";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getCartonDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CARTON_DETAIL_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.cartonDetailId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {

                this.cartonDetail = (CartonDetail) getEntityFromSession(getSavedEntityKey());
            } else {
                this.cartonDetail = this.cartonDetailManager
                    .get(this.cartonDetailId);
                saveEntityInSession(this.cartonDetail);
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("Pallet version is: " + this.cartonDetail.getVersion());
        }

    }

    /**
     * Getter for the cartonDetailManager property.
     * @return CartonDetailManager value of the property
     */
    public CartonDetailManager getCartonDetailManager() {
        return this.cartonDetailManager;
    }

    /**
     * Setter for the cartonDetailManager property.
     * @param cartonDetailManager the new cartonManager value
     */
    public void setCartonDetailManager(CartonDetailManager cartonDetailManager) {
        this.cartonDetailManager = cartonDetailManager;
    }

    /**
     * Getter for the cartonDetail property.
     * @return CartonDetail value of the property
     */
    public CartonDetail getCartonDetail() {
        return this.cartonDetail;
    }

    /**
     * Setter for the cartonDetail property.
     * @param cartonDetail the new cartonDetail value
     */
    public void setCartonDetail(CartonDetail cartonDetail) {
        this.cartonDetail = cartonDetail;
    }

    /**
     * Getter for the cartonDetailsColumns property.
     * @return List</ColumnDetail/> value of the property
     */
    public List<Column> getCartonDetailsColumns() {
        return this.cartonDetailsColumns;
    }

    /**
     * Setter for the cartonDetailsColumns property.
     * @param cartonDetailsColumns the new cartonDetailsColumns value
     */
    public void setCartonDetailsColumns(List<Column> cartonDetailsColumns) {
        this.cartonDetailsColumns = cartonDetailsColumns;
    }

    /**
     * Getter for the CARTON_DETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCartonDetailsViewId() {
        return CARTON_DETAIL_VIEW_ID;
    }

    /**
     * Getter for the cartonDetailId property.
     * @return Long value of the property
     */
    public Long getCartonDetailId() {
        return this.cartonDetailId;
    }

    /**
     * Setter for the cartonDetailId property.
     * @param cartonDetailId the new cartonDetailId value
     */
    public void setCartonDetailId(Long cartonDetailId) {
        this.cartonDetailId = cartonDetailId;
    }

    /**
     * Getter for the cartonId property.
     * @return Long value of the property
     */
    public Long getCartonId() {
        return this.cartonId;
    }

    /**
     * Setter for the cartonId property.
     * @param cartonId the new cartonId value
     */
    public void setCartonId(Long cartonId) {
        this.cartonId = cartonId;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 