/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.lineloading.model.LineLoadSummary;
import com.vocollect.voicelink.lineloading.service.CartonManager;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Struts action class to handle lineloading summary actions.
 *
 * @author ypore
 */
public class LineLoadingSummaryActionRoot extends DataProviderAction implements
    Preparable {

    //
    private static final long serialVersionUID = -8339957095348600026L;

    /*
     * The ID of the view we need to grab from the DB.
     */
    private static final long LINELOADING_WAVE_SUMMARY_VIEW_ID = -1103;

    /*
     * The ID of the view we need to grab from the DB.
     */
    private static final long LINELOADING_REGION_SUMMARY_VIEW_ID = -1104;

    // The lineLoading management service.
    private CartonManager cartonManager;

    /*
     * The Replenishment management service.
     */
    private LineLoadSummary lineLoadSummary = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getCartonManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "lineloadingsummary";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(LINELOADING_WAVE_SUMMARY_VIEW_ID);
        viewIds.add(LINELOADING_REGION_SUMMARY_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
        List<ListObject> list = new ArrayList<ListObject>();
        list.add(new ListObject(
            Tag.ALL, getText("notification.dropdown.site.All")));
        list.add(new ListObject(
            Tag.SYSTEM, getText("notification.dropdown.site.System")));
        for (ListObject lo : super.getSiteList()) {
            list.add(lo);
        }
        return list;
    }

    /**
     * Getter for the lineLoadingAssignmentSummary property.
     * @return LineLoadingWaveSummary value of the property
     */
    public LineLoadSummary getLineLoadSummary() {
        return lineLoadSummary;
    }

    /**
     * Setter for the lineLoadingAssignmentSummary property.
     * @param lineLoadSummary the new lineLoadSummary value
     */
    public void setLineLoadSummary(LineLoadSummary lineLoadSummary) {
        this.lineLoadSummary = lineLoadSummary;
    }

    /**
     * Getter for the lineLoadingManager property.
     * @return LineLoadingManager value of the property
     */
    public CartonManager getCartonManager() {
        return cartonManager;
    }

    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 