/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ReportType;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.ReportTypeManager;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.JSONResponseBuilder;
import com.vocollect.voicelink.lineloading.LineLoadingErrorCode;
import com.vocollect.voicelink.lineloading.dao.RouteStopDAO;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.lineloading.service.RouteStopManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Action class for Route Stops.
 * @author snalan
 */
@SuppressWarnings("serial")
public class RouteStopActionRoot extends DataProviderAction implements Preparable {

    private Logger log = new Logger(RouteStopActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1036;

    // The ID of the view we need to grab from the DB.
    private static final long REPORT_ID = -500;

    // The list of columns for the operators table.
    private List<Column> columns;

    // The routestops management service.
    private GenericManager<RouteStop, RouteStopDAO> routeStopManager;

    // The RouteStop object, which will either be newly created, or retrieved
    // via the RouteStopId.
    private RouteStop routeStop;
    private Long reportDetailId;

    /**
     * The report manager responsible for retrieving the reports for Selection.
     */
    private ReportTypeManager reportTypeManager;


    /**
     * The name of the report type to redirect to.
     */
    private String reportTypeName;


    /**
     * @param routeStop The routeStop to set.
     */
    public void setRouteStop(RouteStop routeStop) {
        this.routeStop = routeStop;
    }

    /**
     * @return The routeStop property.
     */
    public RouteStop getRouteStop() {
        return this.routeStop;
    }


    /**
     * Getter for the routeStopManager property.
     * @return value of the property.
     */
    public GenericManager<RouteStop, RouteStopDAO> getRouteStopManager() {
        return this.routeStopManager;
    }

    /**
     * Setter for the routeStopManager property.
     * @param manager the new routeStopManager value.
     */
    public void setRouteStopManager(GenericManager<RouteStop, RouteStopDAO> manager) {
        this.routeStopManager = manager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "routeStop";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getRouteStopManager();
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getRouteStopColumns() {
        return this.columns;
    }

    /**
     * Action for the Route Stop view page. Initializes the Route Stop table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View routeStopsView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(routeStopsView, getCurrentUser());
        return SUCCESS;
    }

   /**
     * Prints out the Manifest Report if all the selected Route Stops are
     * closed, else throws a BusinessRuleException.
     * @return INPUT
     * @throws Exception General Exception
     */
    public String printManifestReport() throws Exception {

        try {

            routeStop = routeStopManager.get(this.getIds()[0]);

            if (!routeStop.getStatus().equals(RouteStopStatus.Closed)) {
                throw new BusinessRuleException(
                    LineLoadingErrorCode.ROUTE_STOP_NOT_CLOSED,
                    new UserMessage(
                        "lineload.printManifest.error.routeStopNotClose"));
            } else {
                ReportType report = getReportTypeManager().get(REPORT_ID);
                this.setReportTypeName(report.getReportTypeName());
                getSession().setAttribute("SESSION_VALUE", routeStop.getRoute() + "-" + routeStop.getStop());
                getSession().setAttribute("SESSION_VALUE1", routeStop.getId());
                setJsonMessage("errorCode", "0");
            }

        } catch (BusinessRuleException e) {

            addSessionActionMessage(new UserMessage(
                getText(e.getUserMessage()), getText(e.getUserMessage()), null));
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return ERROR;
        }

        return SUCCESS;
    }

    /**
     * Method changes the status of all the selected regions to closed.
     * @return SUCCESS
     * @throws Exception General Exception
     */
    public String closeRouteStop() throws Exception {
            for (int i = 0; i < this.getIds().length; i++) {
               routeStop = routeStopManager.get(this.getIds()[i]);
               ((RouteStopManager) routeStopManager).executeCloseRouteStop(routeStop, new Date());
            }

        setJsonMessage(getText(new UserMessage("lineload.closeRouteStop.success")),
                                             JSONResponseBuilder.ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * The method validates if all the cartons for the selected regions have been loaded,
     * before redirecting to the Close Route Stops functionality.
     * @return INPUT/SUCCESS
     * @throws Exception BusinessRuleException
     */
    public String validateIfAllCartonsLoaded() throws Exception {

       String parametes = "";
       try {

            for (int i = 0; i < this.getIds().length; i++) {
                routeStop = routeStopManager.get(this.getIds()[i]);
                parametes = parametes + "ids=" + getIds()[i] + "&";
                ((RouteStopManager) routeStopManager).checkAllCartonsLoaded(routeStop);
               }

            } catch (BusinessRuleException e) {
                log.warn("All Cartons for the selected regions have not been loaded", e);
                return INPUT;
            }
            return closeRouteStop();

    }

    /**
     * This method sets up the <code>RouteStop</code> object by retrieving it
     * from the database when a routeStopId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }


    /**
     * Getter for the ROUTESTOP_VIEW_ID property.
     * @return long value of the property
     */
    public static long getRouteStopViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the reportDetailId property.
     * @return Long value of the property
     */
    public Long getReportDetailId() {
        return this.reportDetailId;
    }



    /**
     * Setter for the reportDetailId property.
     * @param reportDetailId the new reportDetailId value
     */
    public void setReportDetailId(Long reportDetailId) {
        this.reportDetailId = reportDetailId;
    }



    /**
     * Getter for the reportTypeName property.
     * @return String value of the property
     */
    public String getReportTypeName() {
        return this.reportTypeName;
    }



    /**
     * Setter for the reportTypeName property.
     * @param reportTypeName the new reportTypeName value
     */
    public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }

    /**
     * Getter for the reportManager property.
     * @return ReportManager value of the property
     */
    public ReportTypeManager getReportTypeManager() {
        return this.reportTypeManager;
    }

    /**
     * Setter for the reportTypeManager property.
     * @param reportTypeManager the new reportTypeManager value
     */
    public void setReportTypeManager(ReportTypeManager reportTypeManager) {
        this.reportTypeManager = reportTypeManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 