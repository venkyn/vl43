/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.service.CartonManager;
import com.vocollect.voicelink.lineloading.service.PalletManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>Pallet</code> objects.
 * @author ypore
 */
public class PalletActionRoot extends DataProviderAction implements Preparable {

    /** Version id for serialization. */
    private static final long serialVersionUID = 4273065455753636189L;

    private static final Logger log = new Logger(PalletAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long PALLET_VIEW_ID = -1100;

    private static final long CARTON_VIEW_ID = -1101;

    // The list of columns for the pallets table.
    private List<Column> palletColumns;

    // The list of columns for the cartons table.
    private List<Column> cartonColumns;

    // The Pallet management service.
    private PalletManager palletManager = null;

    // The Carton management service.
    private CartonManager cartonManager = null;

    // The Pallet object, which will either be newly created, or retrieved
    // via the PalletId.
    private Pallet pallet;

    // The Carton object, which will either be newly created, or retrieved
    // via the CartonId.
    private Carton carton;

    // The ID of the Pallet.
    private Long palletId;

    // Pallet Status
    private Integer palletStatus;

    /**
     * Action for the pallets view page. Initializes the pallets table columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View palletView = getUserPreferencesManager().getView(PALLET_VIEW_ID);
        this.palletColumns = this.getUserPreferencesManager().getColumns(
            palletView, getCurrentUser());
        View cartonView = getUserPreferencesManager().getView(CARTON_VIEW_ID);
        this.cartonColumns = this.getUserPreferencesManager().getColumns(
            cartonView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Pallet";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPalletManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(PALLET_VIEW_ID);
        viewIds.add(CARTON_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.palletId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {

                this.pallet = (Pallet) getEntityFromSession(getSavedEntityKey());
            } else {
                this.pallet = this.palletManager.get(this.palletId);
                saveEntityInSession(this.pallet);
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("Pallet version is: " + this.pallet.getVersion());
        }

    }

    /**
     * Getter for the carton property.
     * @return Carton value of the property
     */
    public Carton getCarton() {
        return this.carton;
    }

    /**
     * Setter for the carton property.
     * @param carton the new carton value
     */
    public void setCarton(Carton carton) {
        this.carton = carton;
    }

    /**
     * Getter for the cartonColumns property.
     * @return List</Column/> value of the property
     */
    public List<Column> getCartonColumns() {
        return this.cartonColumns;
    }

    /**
     * Setter for the cartonColumns property.
     * @param cartonColumns the new cartonColumns value
     */
    public void setCartonColumns(List<Column> cartonColumns) {
        this.cartonColumns = cartonColumns;
    }

    /**
     * Getter for the cartonManager property.
     * @return CartonManager value of the property
     */
    public CartonManager getCartonManager() {
        return this.cartonManager;
    }

    /**
     * Setter for the cartonManager property.
     * @param cartonManager the new cartonManager value
     */
    public void setCartonManager(CartonManager cartonManager) {
        this.cartonManager = cartonManager;
    }

    /**
     * Getter for the pallet property.
     * @return Pallet value of the property
     */
    public Pallet getPallet() {
        return this.pallet;
    }

    /**
     * Setter for the pallet property.
     * @param pallet the new pallet value
     */
    public void setPallet(Pallet pallet) {
        this.pallet = pallet;
    }

    /**
     * Getter for the palletColumns property.
     * @return List</Column/> value of the property
     */
    public List<Column> getPalletColumns() {
        return this.palletColumns;
    }

    /**
     * Setter for the palletColumns property.
     * @param palletColumns the new palletColumns value
     */
    public void setPalletColumns(List<Column> palletColumns) {
        this.palletColumns = palletColumns;
    }

    /**
     * Getter for the palletId property.
     * @return Long value of the property
     */
    public Long getPalletId() {
        return this.palletId;
    }

    /**
     * Setter for the palletId property.
     * @param palletId the new palletId value
     */
    public void setPalletId(Long palletId) {
        this.palletId = palletId;
    }

    /**
     * Getter for the palletManager property.
     * @return PalletManager value of the property
     */
    public PalletManager getPalletManager() {
        return this.palletManager;
    }

    /**
     * Setter for the palletManager property.
     * @param palletManager the new palletManager value
     */
    public void setPalletManager(PalletManager palletManager) {
        this.palletManager = palletManager;
    }

    /**
     * Getter for the palletStatus property.
     * @return Integer value of the property
     */
    public Integer getPalletStatus() {
        return this.palletStatus;
    }

    /**
     * Setter for the palletStatus property.
     * @param palletStatus the new palletStatus value
     */
    public void setPalletStatus(Integer palletStatus) {
        this.palletStatus = palletStatus;
    }


    /**
     * Getter for the CARTON_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCartonViewId() {
        return CARTON_VIEW_ID;
    }


    /**
     * Getter for the PALLET_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPalletViewId() {
        return PALLET_VIEW_ID;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 