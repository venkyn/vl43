/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.voicelink.core.model.LocationDescription;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TimeFormatter;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponseType;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.replenishment.model.IssuanceOrder;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.model.SummaryPrompt;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is a utility class that is responsible for rendering the various
 * VoiceLink-specific table component items due to any unique display
 * requirements.
 *
 * @author ddoubleday : refactored VL-specific utilities from
 *         <code>DisplayUtilities</code>
 * @see com.vocollect.epp.web.util.DisplayUtilities
 */
public class VoiceLinkDisplayUtilities extends DisplayUtilities {

   /**
     * Constructor.
     */
     public VoiceLinkDisplayUtilities() {
        super();
    }


     /**
      * Format a set of Assignments.
      * @param obj the set of Assignments.
      * @param dataObj the containing object.
      * @return the JSONArray of formatted Assignments.
      * @throws JSONException on JSON error.
      */
    public JSONArray formatAssignments(Object obj, DataObject dataObj)
        throws JSONException {

        Set<Assignment> assignments = (Set<Assignment>) obj;
        JSONArray ja = new JSONArray();

        for (Assignment element : assignments) {
            JSONObject assignmentJO = new JSONObject();
            assignmentJO.put("id", element.getId());
            assignmentJO.put("number", element.getNumber());
            ja.put(assignmentJO);
        }
        return ja;
    }

    /**
     * Format an issuance order.
     * @param obj the IssuanceOrder.
     * @param dataObj the containing object.
     * @return the JSON formatted IssuanceOrder.
     * @throws JSONException on JSON error.
     */
    public JSONObject displayIssuanceOrder(Object obj, DataObject dataObj)
        throws JSONException {

        JSONObject returnJSON = new JSONObject();
        IssuanceOrder issOrder = (IssuanceOrder) obj;

        returnJSON.put("priority", issOrder.getPriority());
        returnJSON.put("sequenceNumber", issOrder.getSequenceNumber());
        returnJSON.put("aggregate", issOrder.toString());
        return returnJSON;
    }

    /**
     * Format a LocationDespcription.
     * @param obj the LocationDescription.
     * @param dataObj the containing object.
     * @return the JSON-formatted LocationDescription.
     * @throws JSONException on JSON error.
     */
    public JSONObject displayLocationDescription(Object obj, DataObject dataObj)
        throws JSONException {
        JSONObject returnJSON = new JSONObject();
        LocationDescription locDesc = (LocationDescription) obj;

        try {
            returnJSON.put("preAisle", translateUserData(locDesc.getPreAisle()));
        } catch (DataAccessException e) {
            // Attempted translation failed, just put the untranslated value.
            returnJSON.put("preAisle", locDesc.getPreAisle());
        }
        returnJSON.put("aisle", locDesc.getAisle());
        try {
            returnJSON.put("postAisle", translateUserData(locDesc.getPostAisle()));
        } catch (DataAccessException e) {
            // Attempted translation failed, just put the untranslated value.
            returnJSON.put("preAisle", locDesc.getPostAisle());
        }
        returnJSON.put("slot", locDesc.getSlot());
        returnJSON.put("aggregate", locDesc.getDirection());
        return returnJSON;
    }


    /**
     * Format a SummaryPrompt.
     * @param obj the SummaryPrompt.
     * @param dataObj the containing object.
     * @return the JSON-formatted SummaryPrompt.
     * @throws JSONException on JSON error.
     */
    public JSONObject displayPromptLanguage(Object obj, DataObject dataObj)
        throws JSONException    {
        JSONObject returnJSON = new JSONObject();
        if (obj != null) {
            SummaryPrompt prompt = (SummaryPrompt) dataObj;
            List<Locale> locales = prompt.listLocalesWithDefinitions();
            if (locales != null) {
                for (Locale locale : locales) {
                    if (locale.toString().equals("in")) {
                        returnJSON.put(locale.toString() + "_IN", locale.getDisplayLanguage());
                    } else {
                        returnJSON.put(locale.toString(), locale.getDisplayLanguage());
                    }
                }
            }
        }
        return returnJSON;
    }

    /**
     * Format a set of WorkgroupFunctions.
     * @param obj the set of WorkGroupFunctions.
     * @param dataObj the containing object.
     * @return the JSONArray of formatted functions.
     * @throws JSONException on JSON error.
     */
    @SuppressWarnings("unchecked")
    public JSONArray formatFunctions(Object obj, DataObject dataObj)
        throws JSONException {

        Collection<WorkgroupFunction> functions = (Collection<WorkgroupFunction>) obj;
        JSONArray ja = new JSONArray();

        for (WorkgroupFunction function : functions) {
            JSONObject functionJO = new JSONObject();
            functionJO.put("id", function.getId());
            functionJO.put("regionType",
                function.getTaskFunction().getRegionType().toValue());
            functionJO.put("functionType",
                ResourceUtil.getLocalizedEnumName(
                    function.getTaskFunction().getFunctionType()));
            ja.put(functionJO);

        }
        return ja;
    }

    /**
     * Format a SelectionRegionProfile.
     * @param obj the SelectionRegionProfile.
     * @param dataObj the containing object.
     * @return the JSON-formatted profile.
     * @throws JSONException on JSON error.
     */
    public JSONObject formatSelectionRegionProfile(Object obj, DataObject dataObj)
        throws JSONException {

        JSONObject returnJSON = new JSONObject();
        SelectionRegionProfile profile = (SelectionRegionProfile) obj;
        String message = ResourceUtil.getLocalizedMessage(
            "profile.id." + profile.getNumber(), null, profile.getNumber());
        returnJSON.put("name", message);
        returnJSON.put("id", profile.getId());
        return returnJSON;
    }

    /**
     * Format a VSC Check Response.
     * @param obj the Check Response.
     * @param dataObj the containing object.
     * @return the String checkResponse.
     * @throws JSONException on JSON error.
     */
    public String displayCheckResponseFilter(Object obj, DataObject dataObj)
        throws JSONException {
        Integer checkResponse =  (Integer) obj;
        String message = null;
        if (checkResponse == VehicleSafetyCheckResponseType.YES.toValue()) {
            message = ResourceUtil.getLocalizedKeyValue("vsc.response.pass");
        } else if (checkResponse == VehicleSafetyCheckResponseType.NO.toValue()) {
            message = ResourceUtil.getLocalizedKeyValue("vsc.response.fail");
        } else if (checkResponse == VehicleSafetyCheckResponseType.NOBUTCONTINUE.toValue()) {
            message = ResourceUtil.getLocalizedKeyValue("vsc.response.failButContinued");
        } else {
            message = obj.toString();
        }
        return message;
    }    

    /**
     * Format a time duration for Labor reporting.
     * @param obj the time as a Long.
     * @param dataObj the containing object.
     * @return the formatted time string.
     * @see com.vocollect.voicelink.core.model.TimeFormatter
     */
    public String formatTimeDurationFromLong(Object obj, DataObject dataObj) {
        return TimeFormatter.formatTime((Long) obj);
    }

    /**
     * Display the localized name of a report.
     * @param reportKey the resource key of the report to look up in
     *            package.properties.
     * @param dataObject the containing object.
     * @return the report name, localized.
     */
    public String displayReportTypeName(Object reportKey, DataObject dataObject) {
        return ResourceUtil.getLocalizedKeyValue(reportKey.toString());
    }

    /**
     * Display the localized name of a Chart.
     * @param chartNameKey the resource key of the report to look up in
     *            package.properties.
     * @param dataObject the containing object.
     * @return the report name, localized.
     */
    public String displayChartName(Object chartNameKey, DataObject dataObject) {
        return ResourceUtil.getLocalizedKeyValue(chartNameKey.toString());
    }

    /**
     * Display the localized description of a chart.
     * @param chartDescriptionKey the resource key of the report to look up in
     *            package.properties.
     * @param dataObject the containing object.
     * @return the report name, localized.
     */
    public String displayChartDescription(Object chartDescriptionKey, DataObject dataObject) {
        return ResourceUtil.getLocalizedKeyValue(chartDescriptionKey.toString());
    }

    /**
     * Format a set of WorkgroupFunctions.
     * @param obj the set of WorkGroupFunctions.
     * @param dataObj the containing object.
     * @return the JSONArray of formatted functions.
     * @throws JSONException on JSON error.
     */
    @SuppressWarnings("unchecked")
    public JSONArray displayRegions(Object obj, DataObject dataObj)
        throws JSONException {

        Collection<Region> regions = (Collection<Region>) obj;
        JSONArray ja = new JSONArray();

        for (Region region : regions) {
            JSONObject regionJO = new JSONObject();
            regionJO.put("id", region.getId());
            try {
            regionJO.put("name",
                translateUserData(region.getName()));
            } catch (DataAccessException dae) {
                // If we can't look up translation, don't translate
                regionJO.put("name",
                    region.getName());
            }
            ja.put(regionJO);

        }
        return ja;
    }

    /**
     * Format the allow skipping for regions(Put to store).
     * @param obj - not really an object its a field allowSkipAisle.
     * @param dataObj - the containing object Region Object.
     * @return the JSONArray of formatted functions.
     * @throws JSONException on JSON error.
     */
    @SuppressWarnings("unchecked")
    public JSONObject displayAllowSkipping(Object obj, DataObject dataObj)
                throws JSONException {
        JSONObject ja = new JSONObject();

        PtsRegion region = (PtsRegion) dataObj;
        ja.put("id", region.getId());
        if (region.isAllowSkipSlot() && region.isAllowSkipAisle()) {
            ja.put("skip", ResourceUtil.getLocalizedMessage(
                "region.skips.both" , null, "Both"));

       } else if (!region.isAllowSkipSlot() && !region.isAllowSkipAisle()) {
           ja.put("skip", ResourceUtil.getLocalizedMessage(
               "region.skips.neither" , null, "Neither"));

       } else if (region.isAllowSkipSlot() && !region.isAllowSkipAisle()) {
           ja.put("skip", ResourceUtil.getLocalizedMessage(
               "region.skips.slot" , null, "Slot"));

       } else if (!region.isAllowSkipSlot() && region.isAllowSkipAisle()) {
           ja.put("skip", ResourceUtil.getLocalizedMessage(
               "region.skips.aisle" , null, "Aisle"));

       }
         return ja;
    }

    /**
     * Format a set of operators.
     * @param obj the set of operators.
     * @param dataObj the containing object.
     * @return the JSONArray of formatted roles.
     * @throws JSONException on JSON error.
     */
    public JSONArray displayOperators(Object obj, DataObject dataObj)
        throws JSONException {

        Set<Operator> operators = (Set<Operator>) obj;
        JSONArray ja = new JSONArray();

        for (Iterator<Operator> iter = operators.iterator(); iter.hasNext();) {
            JSONObject operatorJO = new JSONObject();
            Operator element = iter.next();
            operatorJO.put("id", element.getId());
            operatorJO.put("name", element.getName());
            operatorJO.put("operatorId", element.getOperatorIdentifier());
            ja.put(operatorJO);
        }
        return ja;
    }

    /**
     * Format a set of operator teams.
     * @param obj the set of operator teams.
     * @param dataObj the containing object.
     * @return the JSONArray of formatted roles.
     * @throws JSONException on JSON error.
     */
    public JSONArray displayOperatorTeams(Object obj, DataObject dataObj)
        throws JSONException {

        Set<OperatorTeam> operatorTeams = (Set<OperatorTeam>) obj;
        JSONArray ja = new JSONArray();

        for (Iterator<OperatorTeam> iter = operatorTeams.iterator(); iter.hasNext();) {
            JSONObject operatorTeamJO = new JSONObject();
            OperatorTeam element = iter.next();
            operatorTeamJO.put("id", element.getId());
            operatorTeamJO.put("name", element.getName());
            ja.put(operatorTeamJO);
        }
        return ja;
    }
    
    /**
     * Display the rounded off decimal value upto 2 places.
     * @param obj the decimal object to be rounded off
     * @param dataObj the containing object.
     * @return the report name, localized.
     */
    public String formatDecimalValue(Object obj, DataObject dataObj) {
        double result = new BigDecimal((Double) obj).setScale(
            2, BigDecimal.ROUND_HALF_UP).doubleValue();
        String str = String.valueOf(result);

        if (str.endsWith(".0") || str.endsWith(".00")) {
            str = str.substring(0, str.indexOf("."));
        }

        return str;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 