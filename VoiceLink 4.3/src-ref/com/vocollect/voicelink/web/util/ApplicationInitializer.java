/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.util;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.web.licensing.enforcement.Licensers;
import com.vocollect.voicelink.core.service.ImportSetupManager;
import com.vocollect.voicelink.licensing.VoiceLinkLicenser;


/**
 * Bean that does post <code>BeanFactory</code> creation initialization
 * actions for the application.
 *
 * @author ddoubleday
 */
public class ApplicationInitializer
    extends com.vocollect.epp.web.util.ApplicationInitializer {

    //This will be spring injected to read system properties
    private SystemPropertyManager systemPropertyManager = null;

    // This will be spring injected to refresh the import export setup files.
    private ImportSetupManager importSetupManager = null;

    // This is the notification manager
    private NotificationManager notificationManager = null;

    /**
     * @return SystemPropertyManager
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * @param systemPropertyManager - the system property manager
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * @return ImportSetupManager
     */
    public ImportSetupManager getImportSetupManager() {
        return importSetupManager;
    }

    /**
     * @param importSetupManager - the import setup manager
     */
    public void setImportSetupManager(ImportSetupManager importSetupManager) {
        this.importSetupManager = importSetupManager;
    }

    private static Logger log = new Logger(ApplicationInitializer.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.util.ApplicationInitializer#initialize()
     */
    @Override
    protected void initialize() {
        //If system property set, rebuild import-setup.xml and export-setup.xml
        refreshImportExportSetups();

        //TODO replace with bean injection code os that site context can also be passed
        addLicensers();
        // This must be called after the licensers are added so that
        // when the base class initializes the License, all licensers will
        // be initialized.
        super.initialize();
    }

    /**
      * Initialize licenser classes.
      */
     private void addLicensers() {
         VoiceLinkLicenser.setNotificationManager(notificationManager);
         Licensers.addLicenser(VoiceLinkLicenser.getInstance());
         log.info("Added VoiceLink Licensers");
     }


     /**
      * this code will make the call to re-create the import and export setup
      * files if the appropriate system property is set.
      */
     private void refreshImportExportSetups() {
         // Get the refreshImportExportSetups property
         try {
             SystemProperty refresh = systemPropertyManager.findByName("Refresh Import Export Setups");

             // If it is set to update...
             if (refresh.getValue().equals("1")) {
                 boolean problem = false;
                 // Warn in log
                 log.warn("Starting refresh of import export setups...");
                 try {
                     this.getImportSetupManager().refreshAllSites();
                 } catch (Exception e) {
                     log
                         .warn("Exception attempting to refresh import export setups.  Will try again.  "
                             + e.toString());
                     problem = true;
                 }
                 if (!problem) {
                     // clear flag and send log message
                     refresh.setValue("0");
                     systemPropertyManager.save(refresh);
                     log.warn("Refresh of import export setups succeeded.");
                 }
             }

         } catch (Exception e) {
             log
             .warn("Exception attempting to refresh import export setups.  Can't get System Property."
                 + e.toString());
         }

     }


    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }


    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 