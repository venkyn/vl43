/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.loading.LoadingErrorCode;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingContainerStatus;
import com.vocollect.voicelink.loading.model.LoadingContainerType;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Action class to handle various HTTP requests for Loading container object
 * @author mraj
 * 
 */
public class LoadingContainerActionRoot extends DataProviderAction implements Preparable {

    private static final Logger log = new Logger(LoadingContainerActionRoot.class);

    private static final long serialVersionUID = 7986067698231319743L;

    private static final long LOADING_CONTAINER_VIEW_ID = -1219;

    private LoadingContainerManager loadingContainerManager;

    private LoadingContainer loadingContainer;
    
    private Map<LoadingContainerType, String> loadingContainerTypeMap;

    private List<Column> columns;

    private Long containerId;

    private Map<Integer, String> statusMap;
    
    private static final String FORMATTED_DEPARTURE_DATETIME = "MM-dd-yyyy hh:mm:ss a";
    
    private Integer containerStatus;
    
    /**
     * Action for the regions view page. Initializes the regions table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View containerView = getUserPreferencesManager().getView(LOADING_CONTAINER_VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(containerView, getCurrentUser());
        return SUCCESS;
    }
    
    /**
     * Method called when a request is made to list.action for a single
     * container. This method saves the requested object, if found, in the
     * session
     */
    @Override
    public void prepare() throws Exception {
        if (this.containerId != null) {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                this.loadingContainer = (LoadingContainer) getEntityFromSession(getSavedEntityKey());
            } else {
                this.loadingContainer = this.loadingContainerManager
                    .get(this.containerId);
                saveEntityInSession(this.loadingContainer);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("Loading container version is: "
                + this.loadingContainer.getVersion());
        }
    }

    /**
     * @return the captureLoadPositionMap
     */
    public Map<Integer, String> getStatusMap() {
        this.statusMap = new LinkedHashMap<Integer, String>(2);
        statusMap = new LinkedHashMap<Integer, String>();
        statusMap.put(
            Integer.valueOf(this.loadingContainer.getStatus().toValue()),
            getText(this.loadingContainer.getStatus().getResourceKey()));
        statusMap.put(Integer.valueOf(LoadingContainerStatus.Cancelled.toValue()),
            getText(LoadingContainerStatus.Cancelled.getResourceKey()));
        return statusMap;
    }
    
    /**
     * Returns a list of Loading Container Type.
     * 
     * @return list of Loading Container Type GUI
     */
    public Map<LoadingContainerType, String> getLoadingContainerTypeMap() {
        this.loadingContainerTypeMap = new LinkedHashMap<LoadingContainerType, String>(
            LoadingContainerType.values().length);
        for (LoadingContainerType loadingContainerType : LoadingContainerType.values()) {
            loadingContainerTypeMap.put(loadingContainerType, 
                ResourceUtil.getLocalizedEnumName(loadingContainerType));
        }
        return this.loadingContainerTypeMap;
    }     

    /**
     * Function for validating if the route is editable or not.
     * 
     * @return String defining next target to forward to.
     */
    public String checkIfEditable() {
        try {
            LoadingContainer container = loadingContainerManager.get(getIds()[0]);
            validateIfEditable(container);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating route for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?containerId=" + getIds()[0],
            ERROR_REDIRECT);
        return SUCCESS;
    }
    
    /**
     * Function to check for editing validation business logic.
     * 
     * @param container
     *            Container to be validated
     * @throws BusinessRuleException
     *             if the business rule is violated.
     */
    protected void validateIfEditable(LoadingContainer container)
            throws BusinessRuleException {
        if (container.getStatus().isInSet(LoadingContainerStatus.Cancelled)) {
            throw new BusinessRuleException(
                    LoadingErrorCode.CONTAINER_STATUS_INVALID, new UserMessage(
                            "loading.container.edit.error.invalidContainerStatus"));
        }
    }
    
    /**
     * Update the loading container object specified by
     * <code>loadingContainer</code> member of this class.
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        if (log.isDebugEnabled()) {
            log.debug("Saving container");
        }
        try {
            //Validate is container can be edited and has not been reserved
            validateIfEditable(this.loadingContainer);

            // Need to save this once to attach it back to the session
            // before changing status
            getLoadingContainerManager().getPrimaryDAO().reattach(this.loadingContainer);
            
            
            if (containerStatus != null) {
                if (loadingContainer.getStatus() == LoadingContainerStatus.Consolidated) {
                    loadingContainer.setMasterContainerNumber(null);
                }
                loadingContainer.setStatus(LoadingContainerStatus.toEnum(containerStatus.intValue()));
            }
            
            loadingContainerManager.save(this.loadingContainer);
            cleanSession();
        } catch (BusinessRuleException bre) {
            addSessionActionErrorMessage(bre.getUserMessage());
            log.error("Error while editing loading container",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, bre);
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            log.warn("Loading container ID " + this.loadingContainer.getId()
                + " not found, possibly a concurrency");
            addSessionActionMessage(new UserMessage(
                "loading.container.edit.error.containerNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified container "
                + this.loadingContainer.getId());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.Loading.Container"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                LoadingContainer modifiedContainer = 
                    getLoadingContainerManager().get(getContainerId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.loadingContainer.setVersion(modifiedContainer.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedContainer);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.Loading.Container"), null));
                return SUCCESS;
            }
            return INPUT;
        }
        addSessionActionMessage(new UserMessage("loading.container.edit.message.success",
            makeGenericContextURL("/container/view.action?containerId=" + this.loadingContainer.getId()), 
            this.loadingContainer.getContainerNumber()));

        return SUCCESS;
    }

    /**
     * @return the containerManager
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * @param containerManager the containerManager to set
     */
    public void setLoadingContainerManager(LoadingContainerManager containerManager) {
        this.loadingContainerManager = containerManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getLoadingContainerManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "loadingContainer";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(LOADING_CONTAINER_VIEW_ID);
        return viewIds;
    }
    

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewId()
     */
    @Override
    public Long getViewId() {
        return LOADING_CONTAINER_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getContainerId() == null) {
            super.getTableData();
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * @return the columns of the loading container table component
     */
    public List<Column> getContainerColumns() {
        return this.columns;
    }

    /**
     * @return the loadingContainer object to be displayed in Loading container TC
     */
    public LoadingContainer getLoadingContainer() {
        return loadingContainer;
    }

    /**
     * @param loadingContainer the loadingContainer to set
     */
    public void setLoadingContainer(LoadingContainer loadingContainer) {
        this.loadingContainer = loadingContainer;
    }

    /**
     * @return the containerId
     */
    public Long getContainerId() {
        return containerId;
    }

    /**
     * @param containerId the containerId to set
     */
    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }

    
    /**
     * Setter for the statusMap property.
     * @param statusMap the new statusMap value
     */
    public void setStatusMap(Map<Integer, String> statusMap) {
        this.statusMap = statusMap;
    }
    
    
    
    /**
     * Getter for the containerStatus property.
     * @return Integer value of the property
     */
    public Integer getContainerStatus() {
        return this.loadingContainer.getStatus().toValue();
    }

    
    /**
     * Setter for the containerStatus property.
     * @param containerStatus the new containerStatus value
     */
    public void setContainerStatus(Integer containerStatus) {
        this.containerStatus = containerStatus;
    }

    /**
     * Method to format the departureDateTime.
     * @return formatted departure date String.
     */
    public String getFormattedDepartureDateTime() {
        return DateUtil.getTimestamp(this.loadingContainer.getStop().getRoute().getDepartureDateTime(),
            FORMATTED_DEPARTURE_DATETIME);
    }
    
    /**
     * Method to format the container load time.
     * @return formatted departure date String.
     */
    public String getFormattedLoadTime() {
        return DateUtil.getTimestamp(this.loadingContainer.getLoadTime(),
            FORMATTED_DEPARTURE_DATETIME);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 