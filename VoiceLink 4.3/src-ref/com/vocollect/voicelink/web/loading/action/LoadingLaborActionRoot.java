/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.web.core.action.LaborAction;

import java.util.List;

/**
 * This class handles all loading labor actions from the user interface.
 * 
 */
public class LoadingLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = -3004931362218852737L;

    private static final Long ROUTE_LABOR_VIEW_ID = -1220L;

    private List<Column> routeLaborColumns;
    
    // Request parameter for assignment labor screen.
    private String operatorLaborId;    
    
    /**
     * @return the routeLaborColumns
     */
    public List<Column> getRouteLaborColumns() {
        return routeLaborColumns;
    }

    
    /**
     * @param routeLaborColumns the routeLaborColumns to set
     */
    public void setRouteLaborColumns(List<Column> routeLaborColumns) {
        this.routeLaborColumns = routeLaborColumns;
    }

    
    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    
    /**
     * @return the routeLaborViewId
     */
    public static Long getRouteLaborViewId() {
        return ROUTE_LABOR_VIEW_ID;
    }

    
    /**
     * @return the operatorLaborId
     */
    public String getOperatorLaborId() {
        return operatorLaborId;
    }

    
    /**
     * @param operatorLaborId the operatorLaborId to set
     */
    public void setOperatorLaborId(String operatorLaborId) {
        this.operatorLaborId = operatorLaborId;
    }


    /**
     * Action method for loading home page labor summary (table component).
     * @return summary data to display
     * @throws Exception On error.
     */
    public String getSummaryData() throws Exception {
        return this.getTableData(null, new Object[] {
            OperatorLaborFilterType.Loading, RegionType.Loading });
    }

    /**
     * Action method for Loading home page labor summary (Printable version
     * action).
     * @return summary data to print
     * @throws Exception On error.
     */
    public String getSummaryPrintData() throws Exception {
        return this.getPrintData(null, new Object[] {
            OperatorLaborFilterType.Loading, RegionType.Loading });
    }

    /**
     * {@inheritDoc}
     */
    public String list() throws Exception {
        super.list();
        if (getFilterByFunction()) {
            overrideFunctionFilter(OperatorLaborFilterType.Loading);
        }
        return SUCCESS;
    }
    
    /**
     * Populates the columns for the route labor table.
     * @return The Struts target name.
     * @throws DataAccessException On database error.
     */
    public String getRouteList() throws DataAccessException {
        View view = getUserPreferencesManager().getView(ROUTE_LABOR_VIEW_ID);
        this.routeLaborColumns = getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getRouteData() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getTableData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }
    
    
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = super.getViewIds();
        viewIds.add(ROUTE_LABOR_VIEW_ID);
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 