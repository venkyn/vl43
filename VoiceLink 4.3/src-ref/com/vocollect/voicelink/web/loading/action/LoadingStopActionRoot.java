/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;




import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.loading.model.LoadingStop;
import com.vocollect.voicelink.loading.model.LoadingStopStatus;
import com.vocollect.voicelink.loading.service.LoadingStopManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author pfunyak
 *
 */
public class LoadingStopActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 2986093467155981484L;


    private static final Logger log = new Logger(LoadingStopActionRoot.class);
    
    private Long stopId;

    private LoadingStopManager loadingStopManager;
        
    private LoadingStop loadingStop;
    
    private Map<LoadingStopStatus, String> loadingStopStatusMap;
    
    private static final long LOADING_STOP_VIEW_ID = -1218;
    
    private List<Column> stopColumns;
    
    // Comma seperated list of route Ids, which are to be queried
    private String routeId;
    
    /**
     * @return message 
     * @throws Exception any exception
     */
    public String getStopTableData() throws Exception {
        if (getRouteId() != null && getRouteId().length() > 0) {
            super.getTableData("obj.route.id in ( " + this.routeId + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
        
    }
    

    /**
     * Method is invoked when a request is made to list.action for the Loading
     * view page. Initializes the route and stop table columns.
     * 
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View stopView = getUserPreferencesManager().getView(LOADING_STOP_VIEW_ID);
        this.stopColumns = this.getUserPreferencesManager().getColumns(
                stopView, getCurrentUser());
        return SUCCESS;
    }
    
    @Override
    public void prepare() throws Exception {
        if (this.stopId != null) {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                this.loadingStop = (LoadingStop) getEntityFromSession(getSavedEntityKey());
            } else {
                this.loadingStop = this.loadingStopManager.get(this.stopId);
                saveEntityInSession(this.loadingStop);
            }
            if (log.isDebugEnabled()) {
                log.debug("Loading stop version is: "
                    + this.loadingStop.getVersion());
            }
        } else {
            log.warn("Loading stop object not found");
        }
    }
    
    /**
     * Saves the data from the Loading Stop Page.
     * @return INPUT
     * @throws Exception General Exception
     */
    public String save() throws Exception {
                
        try {
            loadingStopManager.save(this.loadingStop);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified loadingStop " + this.loadingStop.getId());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("loading.stop.stopnumber"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If item has been deleted, this will throw EntityNotFoundException
            try {
                LoadingStop modifiedItem = getLoadingStopManager().get(getStopId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.loadingStop.setVersion(modifiedItem.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedItem);
                this.loadingStop.setContainers(modifiedItem.getContainers());
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.loadingStop.getNumber().toString())
                        , null));
                return SUCCESS;
            }
            return INPUT;
        }
        // add success message
        String successKey = "loading.stop.edit.message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/stop/view.action?stopId=" + loadingStop.getId()),
                this.loadingStop.getNumber()));

        // Go to the success target.
        return SUCCESS;
    }    

    @Override
    protected DataProvider getManager() {
        return this.getLoadingStopManager();
    }

    @Override
    protected String getKeyPrefix() {
        return "loadingStop";
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(LOADING_STOP_VIEW_ID);
        return viewIds;
    }
    
    /**
     * Returns a list of statuses of loading Stop.
     * 
     * @return list of statuses of loading Stop GUI
     */
    public Map<LoadingStopStatus, String> getLoadingStopStatusMap() {
        this.loadingStopStatusMap = new LinkedHashMap<LoadingStopStatus, String>(LoadingStopStatus.values().length);
        for (LoadingStopStatus loadingStopStatus : LoadingStopStatus.values()) {
            loadingStopStatusMap.put(loadingStopStatus, 
                ResourceUtil.getLocalizedEnumName(loadingStopStatus));
        }
        return this.loadingStopStatusMap;
    }    
    

    /**
     * @return the loadingStopManager
     */
    public LoadingStopManager getLoadingStopManager() {
        return loadingStopManager;
    }

    /**
     * @param loadingStopManager the loadingStopManager to set
     */
    public void setLoadingStopManager(LoadingStopManager loadingStopManager) {
        this.loadingStopManager = loadingStopManager;
    }

    /**
     * @return the loadingStop
     */
    public LoadingStop getLoadingStop() {
        return loadingStop;
    }

    /**
     * @param loadingStop the loadingStop to set
     */
    public void setLoadingStop(LoadingStop loadingStop) {
        this.loadingStop = loadingStop;
    }
    
    /**
     * @return the stopId
     */
    public Long getStopId() {
        return stopId;
    }


    /**
     * @param stopId the stopId to set
     */
    public void setStopId(Long stopId) {
        this.stopId = stopId;
    }

    /**
     * @return the stopColumns
     */
    public List<Column> getStopColumns() {
        return stopColumns;
    }

    /**
     * @param stopColumns the stopColumns to set
     */
    public void setStopColumns(List<Column> stopColumns) {
        this.stopColumns = stopColumns;
    }



    /**
     * @return the routeId
     */
    public String getRouteId() {
        return routeId;
    }



    /**
     * @param routeId the routeId to set
     */
    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 