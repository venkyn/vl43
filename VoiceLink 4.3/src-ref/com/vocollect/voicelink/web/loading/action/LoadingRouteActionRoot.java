/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.JasperReportWrapper;
import com.vocollect.epp.util.ReportWrapper;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.JSONResponseBuilder;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.loading.LoadingErrorCode;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;
import com.vocollect.voicelink.loading.model.LoadingRouteType;
import com.vocollect.voicelink.loading.model.LoadingTruckDiagramRow;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;

import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_LOCALE;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_SOURCE_EXTENSION;
import static com.vocollect.epp.util.ReportWrapperRoot.REPORT_PATH_BASE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import org.json.JSONException;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


/**
 * Action class to handle various HTTP requests for Loading route object
 * 
 * @author mraj
 * 
 */
public class LoadingRouteActionRoot extends DataProviderAction implements
    Preparable {

    private static final long serialVersionUID = -9218918960235960517L;

    private static final Logger log = new Logger(LoadingRouteActionRoot.class);

    private static final long ROUTE_VIEW_ID = -1217;

    private static final long STOP_VIEW_ID = -1218;

    private static final int HOURS_IN_A_DAY = 24;

    private static final int MINUTES_IN_HOUR = 60;

    private static final int SECONDS_IN_MINUTE = 60;

    private static final String DATE_FORMAT_VIEW = "MM-dd-yyyy";

    private static final String DATE_FORMAT_EDIT = DATE_FORMAT_VIEW + "-H-m-s";

    private static final String FORMATTED_DEPARTURE_DATETIME = "MM-dd-yyyy hh:mm:ss a";

    private LoadingRouteManager loadingRouteManager;

    private LoadingContainerManager loadingContainerManager;

    private LocationManager locationManager;

    private Map<Long, String> captureLoadPositionMap;
    
    private Map<LoadingRouteStatus, String> loadingRouteStatusMap;
    
    private Map<LoadingRouteType, String> loadingRouteTypeMap;

    private static final Integer CAPTURE_LOAD_POSTION_OPTIONS = 2;
    
    private final String imagePath = "../../images/vocollect_icon_32x32.gif";

    public static final String IMAGEPATH = "IMAGEPATH";

    private InputStream reportStream;

    private ServletContext servletContext;

    private List<Column> routeColumns;

    private List<Column> stopColumns;

    private Long routeId;

    private String stopId;

    private String dockDoor;

    private int hours;

    private int minutes;

    private int seconds;

    private LoadingRoute loadingRoute;

    private LoadingRoute modifiedLoadingRoute;

    private String departureDateTime;

    private boolean routeIntegrated;

    /**
     * @return the captureLoadPositionMap
     */
    public Map<Long, String> getCaptureLoadPositionMap() {
        this.captureLoadPositionMap = new LinkedHashMap<Long, String>(
            CAPTURE_LOAD_POSTION_OPTIONS);
        this.captureLoadPositionMap.put(0L,
            getText("loading.route.captureload.false"));
        this.captureLoadPositionMap.put(1L,
            getText("loading.route.captureload.true"));
        return this.captureLoadPositionMap;
    }

    /**
     * Returns a list of statuses of loading route.
     * 
     * @return list of statuses of loading route GUI
     */
    public Map<LoadingRouteStatus, String> getLoadingRouteStatusMap() {
        this.loadingRouteStatusMap = new LinkedHashMap<LoadingRouteStatus, String>(LoadingRouteStatus.values().length);
        
        for (LoadingRouteStatus loadingRouteStatus : LoadingRouteStatus.values()) {
             loadingRouteStatusMap.put(loadingRouteStatus, 
                 ResourceUtil.getLocalizedEnumName(loadingRouteStatus));
        }
        
        return this.loadingRouteStatusMap;
    }  
    
    /**
     * Returns a type of loading route.
     * 
     * @return list of type of loading route for GUI
     */
    public Map<LoadingRouteType, String> getLoadingRouteTypeMap() {
        this.loadingRouteTypeMap = new LinkedHashMap<LoadingRouteType, String>();
        loadingRouteTypeMap.put(LoadingRouteType.Dedicated, 
                  ResourceUtil.getLocalizedEnumName(LoadingRouteType.Dedicated));
        loadingRouteTypeMap.put(LoadingRouteType.MultiStop, 
                  ResourceUtil.getLocalizedEnumName(LoadingRouteType.MultiStop));
       
        return this.loadingRouteTypeMap;
    }  
    
    /**
     * Getter for the reportStream property.
     * 
     * @return the value of the property
     */
    public InputStream getReportStream() {
        return this.reportStream;
    }

    /**
     * Setter for the reportStream property.
     * 
     * @param reportStream the new reportStream value
     */
    public void setReportStream(InputStream reportStream) {
        this.reportStream = reportStream;
    }

    /**
     * Getter for the servletContext property.
     * 
     * @return the value of the property
     */
    public ServletContext getServletContext() {
        return this.servletContext;
    }

    /**
     * Setter for the servletContext property.
     * 
     * @param servletContext the new servletContext value
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Method called when a request is made to view or edit action for a single
     * route. This method saves the requested object, if found, in the session
     */
    @Override
    public void prepare() throws Exception {

        if (this.routeId != null) {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                this.loadingRoute = (LoadingRoute) getEntityFromSession(getSavedEntityKey());
            } else {
                this.loadingRoute = this.loadingRouteManager.get(this.routeId);
                saveEntityInSession(this.loadingRoute);
            }

            this.routeIntegrated = this.loadingRoute.getAssignmentCount() > 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("Route version is: " + this.loadingRoute.getVersion());
        }
    }

    /**
     * Create or update the container specified by the <code>container</code>
     * member of this class.
     * 
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        if (log.isDebugEnabled()) {
            log.debug("Saving Route");
        }
        try {
            if (!this.routeIntegrated) {
                String finalDepartureDateTime = this.departureDateTime + "-"
                    + this.hours + "-" + this.minutes + "-" + this.seconds;
                Date departureDateValue = DateUtil.getDate(
                    finalDepartureDateTime, DATE_FORMAT_EDIT);
                LoadingRegion loadingRegion = this.loadingRoute.getRegion();
                if (!loadingRouteManager.validateEditRoute(loadingRoute,
                    departureDateValue, loadingRegion)) {
                    addActionError(new UserMessage(
                        "loading.route.duplicate.route.found"));
                    return INPUT;
                }
                if (!StringUtil.isNullOrEmpty(this.loadingRoute.getTrailer())) {
                    if (this.loadingRoute.getTrailer().length() < this.loadingRoute
                        .getRegion().getTrailerIDDigitsOperSpeaks()) {
                        addActionError(new UserMessage(
                            "loading.route.trailer.length.error",
                            this.loadingRoute.getRegion()
                                .getTrailerIDDigitsOperSpeaks()));
                        return INPUT;
                    }
                }
                this.loadingRoute.setDepartureDateTime(departureDateValue);
            }
            if (!StringUtil.isNullOrEmpty(this.loadingRoute.getDockDoor()
                .getScannedVerification())) {
                Location dockDoorlocation = getLocationManager()
                    .findLocationByScanned(
                        this.loadingRoute.getDockDoor()
                            .getScannedVerification());
                if (dockDoorlocation == null) {
                    addFieldError("loadingRoute.dockDoor.scannedVerification",
                        new UserMessage("loading.route.dockdoor.notfound",
                            this.loadingRoute.getDockDoor()
                                .getScannedVerification()));
                    return INPUT;
                } else {
                    this.loadingRoute.setDockDoor(dockDoorlocation);
                }
            } else {
                addFieldError("loadingRoute.dockDoor.scannedVerification",
                    new UserMessage("loading.route.dockdoor.empty"));
                return INPUT;
            }
            loadingRouteManager.save(this.loadingRoute);
            cleanSession();
        } catch (BusinessRuleException bre) {
            addSessionActionErrorMessage(bre.getUserMessage());
            log.error("Error while editing loading route",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, bre);
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            log.warn("Loading route " + this.loadingRoute.getNumber()
                + " not found, possibly a concurrency");

            addSessionActionMessage(new UserMessage(
                "loading.route.edit.error.RouteNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified route "
                + this.loadingRoute.getNumber());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.Loading.Route"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                LoadingRoute modifiedRoute = getLoadingRouteManager().get(
                    getRouteId());
                setModifiedLoadingRoute(modifiedRoute);
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.loadingRoute.setVersion(modifiedRoute.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedRoute);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.Loading.Route"),
                    null));
                return SUCCESS;
            }
            return INPUT;
        }
        addSessionActionMessage(new UserMessage(
            "loading.route.edit.message.success",
            makeGenericContextURL("/route/view.action?routeId="
                + this.loadingRoute.getId()), this.loadingRoute.getNumber()));

        return SUCCESS;
    }

    /**
     * Generates a map of the 24 hours in a day
     * 
     * @return - Map of each hour
     */
    public Map<Integer, Integer> getScheduleHoursMap() {
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (Integer i = 0; i < HOURS_IN_A_DAY; i++) {
            map.put(i, i);
        }
        return map;
    }

    /**
     * Generates a map of the 60 minutes in an hour
     * 
     * @return - Map of each minute
     */
    public Map<Integer, Integer> getScheduleMinutesMap() {
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (Integer i = 0; i < MINUTES_IN_HOUR; i++) {
            map.put(i, i);
        }
        return map;
    }

    /**
     * Generates a map of the 60 seconds in an minute
     * 
     * @return - Map of each second
     */
    public Map<Integer, Integer> getScheduleSecondsMap() {
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (Integer i = 0; i <= SECONDS_IN_MINUTE; i++) {
            map.put(i, i);
        }
        return map;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getLoadingRouteManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "loadingRoute";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(ROUTE_VIEW_ID);
        viewIds.add(STOP_VIEW_ID);
        return viewIds;
    }

    /**
     * Method is invoked when a request is made to list.action for the Loading
     * view page. Initializes the route and stop table columns.
     * 
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View routeView = getUserPreferencesManager().getView(ROUTE_VIEW_ID);
        this.routeColumns = this.getUserPreferencesManager().getColumns(
            routeView, getCurrentUser());
        View stopView = getUserPreferencesManager().getView(STOP_VIEW_ID);
        this.stopColumns = this.getUserPreferencesManager().getColumns(
            stopView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * @return the routeViewId
     */
    public static long getRouteViewId() {
        return ROUTE_VIEW_ID;
    }

    /**
     * @return the containerViewId
     */
    public static long getStopViewId() {
        return STOP_VIEW_ID;
    }

    /**
     * Function for validating if the route is editable or not.
     * 
     * @return String defining next target to forward to.
     */
    public String checkIfEditable() {
        try {
            LoadingRoute route = loadingRouteManager.get(getIds()[0]);
            validateIfEditable(route);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating route for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?routeId=" + getIds()[0],
            ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Function to check for editing validation business logic.
     * 
     * @param route Route to be validated
     * @throws BusinessRuleException if the business rule is violated.
     */
    protected void validateIfEditable(LoadingRoute route)
        throws BusinessRuleException {
        if (route.getStatus().isInSet(LoadingRouteStatus.InProgress,
            LoadingRouteStatus.Complete, LoadingRouteStatus.Reserved)) {
            throw new BusinessRuleException(
                LoadingErrorCode.ROUTE_STATUS_INVALID, new UserMessage(
                    "loading.route.edit.error.invalidRouteStatus"));
        }
    }

    /**
     * Function for validating if the route is printable or not.
     * 
     * @return String defining next target to forward to.
     */
    public String checkIfPrintable() {
        try {
            LoadingRoute route = loadingRouteManager.get(routeId);
            validateIfPrintable(route);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating route for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("launch.action?routeId=" + routeId, ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Method implementation to provide auto complete feature for dock door
     * values in Loading Route edit page.
     * @return String.
     * @throws JSONException
     * @throws DataAccessException
     */
    public String getDockDoorData() throws JSONException, DataAccessException {
        String dockDoorValue = this.getDockDoor();
        List<String> scannedLocations = getLocationManager()
            .listLocationsAutoComplete(dockDoorValue);
        setMessage(JSONResponseBuilder.buildFilterResponse(scannedLocations));
        return SUCCESS;
    }

    /**
     * Function to check for editing validation business logic.
     * 
     * @param route Route to be validated
     * @throws BusinessRuleException if the business rule is violated.
     */
    protected void validateIfPrintable(LoadingRoute route)
        throws BusinessRuleException {
        if (!route.getStatus().isInSet(LoadingRouteStatus.Complete)) {
            throw new BusinessRuleException(
                LoadingErrorCode.ROUTE_STATUS_INVALID, new UserMessage(
                    "loading.route.truckdiagram.print.error"));
        }
    }

    /**
     * Method to Launch the truck diagram Report.
     * 
     * @return reportFormat String.
     * @throws Exception
     */
    public String launchDiagram() throws Exception {
        boolean isTruckLoadCustom = getLoadingContainerManager()
            .isTruckConfigurationCustom(routeId);

        String absolutePath = servletContext.getRealPath(REPORT_PATH_BASE);
        String reportName = isTruckLoadCustom
            ? "LoadingTruckDiagramList" : "LoadingTruckDiagram";

        if (log.isInfoEnabled()) {
            log.info("Generating report: " + reportName + " from "
                + absolutePath);
        }
        ReportWrapper reportWrapper = new JasperReportWrapper(absolutePath,
            reportName + JASPER_REPORT_SOURCE_EXTENSION);

        // Check and initialize if the report exists
        if (!reportWrapper.reportExists(absolutePath, reportName
            + JASPER_REPORT_SOURCE_EXTENSION)) {
            log.warn("Report definition " + reportName
                + JASPER_REPORT_SOURCE_EXTENSION + " NOT found at: "
                + absolutePath);
            return ERROR_404_RETURN;
        }

        Map<String, Object> values = new HashMap<String, Object>();
        values.put(JASPER_REPORT_LOCALE, getLocale());
        values.put(IMAGEPATH, imagePath);
        String reportFormat = "HTML";
        List<LoadingTruckDiagramRow> containerList = getLoadingContainerManager()
            .listTruckLoadedContainers(routeId, isTruckLoadCustom);
        Object dataSource = new JRBeanCollectionDataSource(containerList);

        InputStream reportInputStream = reportWrapper.generateReport(values,
            reportFormat, getLocale().getLanguage(), dataSource);
        this.reportStream = reportInputStream;

        if (log.isInfoEnabled()) {
            log.info("Report TruckDiagram generated successfully.");
        }

        return reportFormat.toLowerCase();
    }

    /**
     * @return the routeManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * @return the loadingContainerManager
     */
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * @param loadingContainerManager the loadingContainerManager to set
     */
    public void setLoadingContainerManager(LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }

    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the routeColumns
     */
    public List<Column> getRouteColumns() {
        return routeColumns;
    }

    /**
     * @param routeColumns the routeColumns to set
     */
    public void setRouteColumns(List<Column> routeColumns) {
        this.routeColumns = routeColumns;
    }

    /**
     * @return the stopColumns
     */
    public List<Column> getStopColumns() {
        return stopColumns;
    }

    /**
     * @param stopColumns the stopColumns to set
     */
    public void setStopColumns(List<Column> stopColumns) {
        this.stopColumns = stopColumns;
    }

    /**
     * @return the routeId
     */
    public Long getRouteId() {
        return routeId;
    }

    /**
     * @param routeId the routeId to set
     */
    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    /**
     * @return the stopId
     */
    public String getStopId() {
        return stopId;
    }

    /**
     * @param stopId the stopId to set
     */
    public void setStopId(String stopId) {
        this.stopId = stopId;
    }

    /**
     * @return the loadingRoute
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }

    /**
     * @param loadingRoute the loadingRoute to set
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }

    /**
     * @return the modifiedLoadingRoute
     */
    public LoadingRoute getModifiedLoadingRoute() {
        return modifiedLoadingRoute;
    }

    /**
     * @param modifiedLoadingRoute the modifiedLoadingRoute to set
     */
    public void setModifiedLoadingRoute(LoadingRoute modifiedLoadingRoute) {
        this.modifiedLoadingRoute = modifiedLoadingRoute;
    }

    /**
     * @return the departureDateTime
     */
    public String getDepartureDateTime() {
        return DateUtil.getTimestamp(this.loadingRoute.getDepartureDateTime(),
            DATE_FORMAT_VIEW);
    }

    /**
     * @param departureDateTime the departureDateTime to set
     */
    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    /**
     * Getter for the dockDoor property.
     * @return String value of the property
     */
    public String getDockDoor() {
        return dockDoor;
    }

    /**
     * Setter for the dockDoor property.
     * @param dockDoor the new dockDoor value
     */
    public void setDockDoor(String dockDoor) {
        this.dockDoor = dockDoor;
    }

    /**
     * Getter for the routeIntegrated property.
     * @return boolean value of the property
     */
    public boolean getRouteIntegrated() {
        return routeIntegrated;
    }

    /**
     * Setter for the routeIntegrated property.
     * @param routeIntegrated the new routeIntegrated value
     */
    public void setRouteIntegrated(boolean routeIntegrated) {
        this.routeIntegrated = routeIntegrated;
    }

    /**
     * Method to format the departureDateTime.
     * @return formatted departure date String.
     */
    public String getFormattedDepartureDateTime() {
        return DateUtil.getTimestamp(this.loadingRoute.getDepartureDateTime(),
            FORMATTED_DEPARTURE_DATETIME);
    }

    /**
     * Method to format the modified departureDateTime.
     * @return formatted departure date String.
     */
    public String getModifiedFormattedDepartureDateTime() {
        return DateUtil.getTimestamp(getModifiedLoadingRoute()
            .getDepartureDateTime(), FORMATTED_DEPARTURE_DATETIME);
    }

    /**
     * Getter for hours.
     * 
     * @return the hours int.
     */
    @SuppressWarnings("deprecation")
    public int getHours() {
        if (this.loadingRoute.getDepartureDateTime() != null) {
            return this.loadingRoute.getDepartureDateTime().getHours();
        } else {
            return 0;
        }
    }

    /**
     * Setter for hours.
     * 
     * @param hours the hours to set
     */
    public void setHours(int hours) {
        this.hours = hours;
    }

    /**
     * Setter for minutes.
     * 
     * @return the minutes int.
     */
    @SuppressWarnings("deprecation")
    public int getMinutes() {
        if (this.loadingRoute.getDepartureDateTime() != null) {
            return this.loadingRoute.getDepartureDateTime().getMinutes();
        } else {
            return 0;
        }
    }

    /**
     * Setter for minutes.
     * 
     * @param minutes the minutes to set
     */
    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    /**
     * Setter for seconds.
     * 
     * @return the seconds int.
     */
    @SuppressWarnings("deprecation")
    public int getSeconds() {
        if (this.loadingRoute.getDepartureDateTime() != null) {
            return this.loadingRoute.getDepartureDateTime().getSeconds();
        } else {
            return 1;
        }
    }

    /**
     * Setter for seconds.
     * 
     * @param seconds the seconds to set
     */
    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 