/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.loading.dao.LoadingRegionDAO;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRouteIssuance;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;

/**
 * Action class for Loading Regions.
 * 
 * @author kudupi
 */
public class LoadingRegionActionRoot extends RegionAction implements Preparable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 3520253140091514656L;

    private Logger log = new Logger(LoadingRegionActionRoot.class);
    
    private static final long VIEW_ID = -1216;
    
    private GenericManager<LoadingRegion, LoadingRegionDAO> loadingRegionManager;
    
    // The LoadingRegion object, which will either be newly created, or retrieved
    // via the LoadingRegionId.
    private LoadingRegion loadingRegion;
    
    // To indicate if the action is a view or save
    private String actionValue;
    
    /* Uncomment this code for showing a drop down with both Automatic and Manual. 
    private Map<Integer, String> loadingRouteIssuanceMap;
    
    private static final Integer LOADING_ROUTE_ISSUANCE_OPTIONS = 2;
    */

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "loadingRegion";
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getLoadingRegionManager();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
     */
    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
     */
    @Override
    protected Region getRegion() {
        return getLoadingRegion();
    }
    
    /**
     * This method sets up the <code>LoadingRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);
        log.debug("prepare");
        log.debug("regionId = " + getRegionId());
        if (getRegionId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.loadingRegion = (LoadingRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.loadingRegion = this.loadingRegionManager.get(getRegionId());
                saveEntityInSession(this.loadingRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Region version is: " + this.loadingRegion.getVersion());
            }
        } else if (this.loadingRegion == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new region object");
            }
            this.loadingRegion = new LoadingRegion();
        }
    }
    
    /**
     * Getter for LoadingRegionManager.
     * @return loadingRegionManager
     */
    public GenericManager<LoadingRegion, LoadingRegionDAO> getLoadingRegionManager() {
        return loadingRegionManager;
    }

    /**
     * Setter for LoadingRegionManager.
     * @param loadingRegionManager LoadingRegionManager
     */
    public void setLoadingRegionManager(
            GenericManager<LoadingRegion, LoadingRegionDAO> loadingRegionManager) {
        this.loadingRegionManager = loadingRegionManager;
    }
    
    /**
     * Getter for LoadingRegion Object.
     * @return loadingRegion
     */
    public LoadingRegion getLoadingRegion() {
        return loadingRegion;
    }
    
    /**
     * Setter for LoadingRegion Object.
     * @param loadingRegion LoadingRegion
     */
    public void setLoadingRegion(LoadingRegion loadingRegion) {
        this.loadingRegion = loadingRegion;
    }
    
    /**
     * Getter for actionValue. 
     * @return actionValue String.
     */
    public String getActionValue() {
        return actionValue;
    }

    /**
     * Setter for actionValue.
     * @param actionValue String.
     */
    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }
    
    /**
     * Getter for the LOADINGREGION_VIEW_ID property.
     * @return long value of the property
     */
    public static long getLoadingRegionViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    
/*
 * Uncomment this code for showing a drop down with both Automatic and Manual. 
 * The requirement for VoiceLink 4.1 release is just Manual Issuance of Routes.
 * So if we want to enable both Manual and Automatic Issuance then we can uncomment this code
 * and comment the existing getLoadingRouteIssuance method right below.
 * 
   public Map<Integer, String> getLoadingRouteIssuanceMap() {
       if (this.loadingRouteIssuanceMap == null) {
           this.loadingRouteIssuanceMap = new LinkedHashMap<Integer, String>(LOADING_ROUTE_ISSUANCE_OPTIONS);
           for (int i = 0; i < LOADING_ROUTE_ISSUANCE_OPTIONS; i++) {
               this.loadingRouteIssuanceMap.put(i, getText(LoadingRouteIssuance.toEnum(i).getResourceKey()));
           }
       }
       return this.loadingRouteIssuanceMap;
   }
   
   public Integer getLoadingRouteIssuance() {
       return this.loadingRegion.getLoadingRouteIssuance().toValue();
   }

   public void setLoadingRouteIssuance(Integer value) {
       this.loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.toEnum(value));
   }
*/  
    /**
    * Getter for the LoadingRouteIssuance property.
    * @return String value of the property
    */
   public String getLoadingRouteIssuance() {
       return getText(LoadingRouteIssuance.Manual.getResourceKey());
   }
   
   /**
    * Saves the data from the Loading Regions Page.
    * @return INPUT
    * @throws Exception General Exception
    */
   public String save() throws Exception {
       boolean isNew = this.loadingRegion.isNew();
       try {
           loadingRegion.setType(RegionType.Loading);
           loadingRegionManager.save(this.loadingRegion);
           cleanSession();
       } catch (BusinessRuleException bre) {
           if (bre instanceof FieldValidationException) {
               FieldValidationException fve = (FieldValidationException) bre;
               if (fve.getField() == "region.name") {
                   fve.setField("loadingRegion.name");
               } else {
                   fve.setField("loadingRegion.number");
               }
               log.warn(fve.getField() + " " + fve.getValue() + " already exists");
               addFieldError(fve.getField(), fve.getUserMessage());
               // Go back to the form to show the error message.
               return INPUT;
           }
       } catch (OptimisticLockingFailureException e) {
           log.warn("Attempt to change already modified loadingRegion " + this.loadingRegion.getId());
           addActionError(new UserMessage(
               "entity.error.modified", UserMessage
                   .markForLocalization("entity.Region"),
               SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
           // Get the modified data
           // If item has been deleted, this will throw EntityNotFoundException
           try {
               LoadingRegion modifiedItem = getLoadingRegionManager().get(getRegionId());
               // Set the local object's version to match, so it will work
               // if the user resubmits.
               this.loadingRegion.setVersion(modifiedItem.getVersion());
               // Store the modified data for display
               setModifiedEntity(modifiedItem);
           } catch (EntityNotFoundException ex) {
               addSessionActionMessage(new UserMessage(
                   "entity.error.deleted", UserMessage
                       .markForLocalization(this.loadingRegion.getNumber().toString())
                       , null));
               return SUCCESS;
           }
           return INPUT;
       }
       // add success message
       String successKey = "region." + (isNew ? "create" : "edit")
           + ".message.success";
       addSessionActionMessage(
           new UserMessage(successKey,
               makeGenericContextURL("/region/view.action?regionId=" + loadingRegion.getId()),
               this.loadingRegion.getName()));

       // Go to the success target.
       return SUCCESS;
   }
   
   /**
    * Function for validating if a region is editable or not.
    *
    * @return String  success string
    * @throws Exception - any exception
    */
   public String checkIfEditable() throws Exception {
       try {
           if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
               // This means the Region is being edited, so
               // get it from the session.
               if (log.isDebugEnabled()) {
                   log.debug("Getting saved region from session");
               }
               this.loadingRegion = (LoadingRegion) getEntityFromSession(getSavedEntityKey());
           } else {
               if (log.isDebugEnabled()) {
                   log.debug("Getting region from database");
               }
               this.loadingRegion = this.loadingRegionManager.get(getIds()[0]);
               saveEntityInSession(this.loadingRegion);
           }
           ((LoadingRegionManager) loadingRegionManager).
                               executeValidateEditRegion(this.loadingRegion);

       } catch (DataAccessException e) {
           setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
           return SUCCESS;
       } catch (BusinessRuleException e) {
           log.warn("Error while validating region for editing", e);
           setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
           return SUCCESS;
       }

       setJsonMessage("edit!input.action?regionId=" + getIds()[0], ERROR_REDIRECT);
       return SUCCESS;
   }
   
   /**
    * Deletes the currently viewed region.
    * @return the control flow target name.
    * @throws Exception Throws Exception if error occurs deleting current region
    */
   public String deleteCurrentRegion() throws Exception {

       LoadingRegion regionToDelete = null;

       try {
           regionToDelete = loadingRegionManager.get(getRegionId());
           loadingRegionManager.delete(getRegionId());

           addSessionActionMessage(new UserMessage(
               "loadingRegion.delete.message.success", regionToDelete.getName()));
       } catch (BusinessRuleException e) {
           log.warn("Unable to delete region: "
               + getText(e.getUserMessage()));
           addSessionActionErrorMessage(e.getUserMessage());
           return SUCCESS;
       } catch (EntityNotFoundException e) {
           addSessionActionErrorMessage(e.getUserMessage());
           return SUCCESS;
       } catch (DataAccessException e) {
           addSessionActionErrorMessage(e.getUserMessage());
           return SUCCESS;
       }
       return SUCCESS;
   }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 