/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;

import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.web.core.action.LaborAction;

/**
 * This class handles all replenishment labor actions from the user interface.
 * @author brupert
 */
public class ReplenishmentLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = -3937309072072927767L;

    /**
     * Action method for Replenihsment home page labor summary (table component).
     * @return summary data to display
     * @throws Exception On error.
     */
    public String getSummaryData() throws Exception {
        return this.getTableData(null, new Object[]{ OperatorLaborFilterType.Replenishment, RegionType.Replenishment });
    }


    /**
     * Action method for Replenishment home page labor summary (Printable version action).
     * @return summary data to print
     * @throws Exception On error.
     */
    public String getSummaryPrintData() throws Exception {
        return this.getTableData(null, new Object[]{ OperatorLaborFilterType.Replenishment, RegionType.Replenishment });
    }



    /**
     * {@inheritDoc}
     */
    @Override
    public String list() throws Exception {
        super.list();
        if (getFilterByFunction()) {
            overrideFunctionFilter(OperatorLaborFilterType.Replenishment);
        }
        return SUCCESS;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 