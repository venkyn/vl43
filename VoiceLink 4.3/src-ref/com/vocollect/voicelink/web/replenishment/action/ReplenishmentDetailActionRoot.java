/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;


/**
 * This is the Struts action class that handles operations on
 * <code>ReplenishmentDetail</code> objects.
 *
 * @author svoruganti
 */
public class ReplenishmentDetailActionRoot  extends DataProviderAction implements Preparable {


    /*
     * Auto Generated Serial Version ID
     */
    private static final long serialVersionUID = -3057703568501031816L;

    /*
     * View Id of replenishment details
     */
    private static final long REPLENISHMENT_DETAIL_VIEW_ID = -1029;

     /*
      * The list of columns for the replenishment details table.
      */
    private List<Column> replenishmentDetailColumns;

    /*
     * The ReplenishmentDetail management service.
     */
    private ReplenishmentDetailManager replenishmentDetailManager = null;

    /*
     *  The ReplenishmentDetail object, which will either be newly created, or retrieved
     *  using replenishmentId
     */
    private ReplenishmentDetail replenishmentDetail;

    /*
     * The ID of the replenishment.
     */
    private String assignmentId;

    /*
     * The ID of the replenishmentDetail.
     */
    private Long replenishmentDetailId;

    /**
     * Getter for the replenishmentDetailsManager property.
     *
     * @return replenishmentDetailsManager value of the property
     */
    public ReplenishmentDetailManager getReplenishmentDetailManager() {
        return replenishmentDetailManager;
    }

    /**
     * Setter for the replenishmentDetail service.
     *
     * @param replenishmentDetailManager the new replenishmentDetailsManager
     */
    public void setReplenishmentDetailManager(ReplenishmentDetailManager replenishmentDetailManager) {
        this.replenishmentDetailManager = replenishmentDetailManager;
    }

     /**
     * Getter for the replenishmentDetail property.
     *
     * @return replenishmentDetail value of the property
     */
    public ReplenishmentDetail getReplenishmentDetail() {
        return replenishmentDetail;
    }

    /**
     * Setter for the replenishment.
     *
     * @param replenishmentDetail the new replenishmentDetail
     */
    public void setReplenishmentDetail(ReplenishmentDetail replenishmentDetail) {
        this.replenishmentDetail = replenishmentDetail;
    }


    /**
     * Getter for the REPLENISHMENT_DETAIL_VIEW_ID property.
     *
     * @return long value of the property
     */
    public static long getReplenishmentDetailViewId() {
        return REPLENISHMENT_DETAIL_VIEW_ID;
    }

    /**
     * Getter for the assignmentId property.
     *
     * @return String value of the property
     */
    public String getAssignmentId() {
        return assignmentId;
    }

    /**
     * Setter for the repassignmentId.
     *
     * @param assignmentId the new assignmentId
     */
    public void setAssignmentId(String assignmentId) {
        this.assignmentId = assignmentId;
    }

    /**
     * Getter for the replenishmentDetailId property.
     *
     * @return long value of the property
     */
    public Long getReplenishmentDetailId() {
        return replenishmentDetailId;
    }

    /**
     * Setter for the replenishmentDetailId.
     *
     * @param replenishmentDetailId the new replenishmentDetailId
     */
    public void setReplenishmentDetailId(Long replenishmentDetailId) {
        this.replenishmentDetailId = replenishmentDetailId;
    }

    /**
     * Action for the replenishment detail view page. Initializes the replenishments detail table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {

        View replenishmentDetailsView = getUserPreferencesManager()
        .getView(REPLENISHMENT_DETAIL_VIEW_ID);
         this.replenishmentDetailColumns = this.getUserPreferencesManager().getColumns(
             replenishmentDetailsView, getCurrentUser());
         setAssignmentId(this.assignmentId);
        return SUCCESS;
    }


    /**
     * Getter for the replenishmentDetailColumns property.
     *
     * @return List of Column value of the property.
     */
    public List<Column> getReplenishmentDetailColumns() {
        return this.replenishmentDetailColumns;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getReplenishmentDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
       return "replenishmentdetail";
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getAssignmentId() != null && getAssignmentId().length() > 0) {
            super.getTableData(
                " obj.replenishment.id in ( " + this.assignmentId + " )");
        } else {
            super.getTableData(
                " obj.replenishment.id not in ( 0 )");
        }

        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(REPLENISHMENT_DETAIL_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 