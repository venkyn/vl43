/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.classic.Validatable;

/**
 * This is the Struts action class that handles operations on <code>Replenishment Region</code>
 * objects.
 *
 * @author mnichols
 */
public class ReplenishmentRegionActionRoot extends RegionAction
    implements Preparable, Validatable {

    private static final long serialVersionUID = 5824301006857941610L;

    private static final Logger log = new Logger(ReplenishmentRegionAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1031;

    // The region manager service.
    private ReplenishmentRegionManager replenishmentRegionManager;

    // The region object, which will either be newly created, or retrieved
    // via the regionId.
    private ReplenishmentRegion replenishmentRegion;

    // To indicate if the action is a view or save
    private String actionValue;

    // Number of options for drop down menus

    private static final Integer DIGITS_OPERATOR_SPEAKS_OPTIONS = 51;

    private static final Integer CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS = 6;

    private static final Integer DEFAULT_LOC_DIGITS_OPERATOR_SPEAKS = 3;
    private static final Integer DEFAULT_LIC_DIGITS_TASK_SPEAKS = 5;
    private static final Integer DEFAULT_CHECK_DIGITS_OPERATOR_SPEAKS = 2;
    private static final boolean DEFAULT_ALLOW_OVERRIDE_PICKUP_QUANTITY = true;
    private static final String DEFAULT_EXCEPTION_LOCATION = "replenishment.region.default.exceptionLocation";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "replenishmentRegion";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.replenishmentRegionManager;
    }

    /**
     * Getter for the region property.
     *
     * @return Region value of the property
     */
    public ReplenishmentRegion getReplenishmentRegion() {
        return this.replenishmentRegion;
    }

    /**
     * Setter for the region property.
     * @param replenishmentRegion The new region value
     */
    public void setReplenishmentRegion(ReplenishmentRegion replenishmentRegion) {
        this.replenishmentRegion = replenishmentRegion;
    }

    /**
     * Getter for the replenishmentRegionManager property.
     * @return RegionManager value of the property.
     */
    public ReplenishmentRegionManager getReplenishmentRegionManager() {
        return this.replenishmentRegionManager;
    }

    /**
     * Setter for the replenishmentRegionManager property.
     * @param replenishmentRegionManager The new regionManager value.
     */
    public void setReplenishmentRegionManager(
                                          ReplenishmentRegionManager replenishmentRegionManager) {
        this.replenishmentRegionManager = replenishmentRegionManager;
    }

    /**
     * Create or update the replenishment region specified by the <code>replenishmentRegion</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.replenishmentRegion.isNew();
        try {
            this.replenishmentRegion.setType(RegionType.Replenishment);
            replenishmentRegionManager.save(this.replenishmentRegion);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                if (fve.getField() == "region.name") {
                    fve.setField("replenishmentRegion.name");
                } else {
                    fve.setField("replenishmentRegion.number");
                }
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified region "
                + this.replenishmentRegion.getName());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.ReplenishmentRegion"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                ReplenishmentRegion modifiedEntity = getReplenishmentRegionManager().get(getRegionId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.replenishmentRegion.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.replenishmentRegion.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "replenishment.region." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeContextURL("/replenishment/region/view.action?regionId=" + replenishmentRegion.getId()),
                this.replenishmentRegion.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * This method sets up the <code>replenishmentRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (getRegionId() != null) {
            // We have an ID, but not a region object yet.
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + this.getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved replenishment region from session");
                }
                this.replenishmentRegion = (ReplenishmentRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting replenishment region from database");
                }
                this.replenishmentRegion = this.replenishmentRegionManager.get(this.getRegionId());
                saveEntityInSession(this.replenishmentRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Replenishment region version is: " + this.replenishmentRegion.getVersion());
            }
        } else if (this.replenishmentRegion == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new replenishment region object");
            }
            this.replenishmentRegion = getDefaultRegion();
        }
        // To reset the checkbox values, only before saving the model.
        if (this.actionValue != null) {
            this.resetBools();
        }
    }

    /**
     * Deletes the currently viewed region.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

       ReplenishmentRegion regionToDelete = null;

        try {
            regionToDelete = replenishmentRegionManager.get(getRegionId());
            replenishmentRegionManager.delete(getRegionId());

            addSessionActionMessage(new UserMessage(
                "replenishmentRegion.delete.message.success", regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Creates a new <code>ReplenishmentRegion</code> with default values populated.
     * @return A default <code>ReplenishmentRegion</code> object.
     */
    public ReplenishmentRegion getDefaultRegion() {
        ReplenishmentRegion region = new ReplenishmentRegion();
        region.setType(RegionType.Replenishment);
        region.setLocDigitsOperSpeaks(DEFAULT_LOC_DIGITS_OPERATOR_SPEAKS);
        region.setLicDigitsTaskSpeaks(DEFAULT_LIC_DIGITS_TASK_SPEAKS);
        region.setCheckDigitsOperSpeaks(DEFAULT_CHECK_DIGITS_OPERATOR_SPEAKS);
        region.setExceptionLocation(getText(DEFAULT_EXCEPTION_LOCATION));
        region.setAllowOverridePickUpQty(DEFAULT_ALLOW_OVERRIDE_PICKUP_QUANTITY);
        return region;
    }


    /**
     * Function for validating if a region is editable or not.
     *
     * @return SUCCESS
     */
    public String checkIfEditable() {

        try {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.replenishmentRegion = (ReplenishmentRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.replenishmentRegion = this.replenishmentRegionManager.get(getIds()[0]);
                saveEntityInSession(this.replenishmentRegion);
            }
          replenishmentRegionManager.executeValidateEditRegion(this.replenishmentRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        log.info("REDIRECTING");
        setJsonMessage("edit!input.action?regionId=" + getIds()[0], ERROR_REDIRECT);

        return SUCCESS;
    }

    /**
     * Getter for the REGION_VIEW_ID property.
     * @return long value of the property
     */
    public static long getRegionViewId() {
        return VIEW_ID;
    }
    /**
     *
     *
     */
    public void resetBools() {
        this.replenishmentRegion.setAllowCancelLicense(false);
        this.replenishmentRegion.setAllowOverrideLocation(false);
        this.replenishmentRegion.setAllowPartialPut(false);
        this.replenishmentRegion.setAllowOverridePickUpQty(false);
        this.replenishmentRegion.setCapturePickUpQty(false);
        this.replenishmentRegion.setCapturePutQty(false);
        this.replenishmentRegion.setVerifySpokenLicenseLocation(false);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Retrieves options for the location digits operator speaks dropdown.
     * @return Translated array of options.
     */
    public Map<Integer, String> getLocDigitsOperatorSpeaksOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(DIGITS_OPERATOR_SPEAKS_OPTIONS);
        options.put(0, getText("replenishment.region.option.all"));
        for (int i = 1; i < DIGITS_OPERATOR_SPEAKS_OPTIONS; i++) {
            options.put(i, String.valueOf(i));
        }
        return options;
    }

    /**
     * Retrieves options for the check digits operator speaks dropdown.
     * @return Translated array of options.
     */
    public Map<Integer, String> getCheckDigitsOperatorSpeaksOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS);
        options.put(0, getText("replenishment.region.option.all"));
        for (int i = 1; i < CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS; i++) {
            options.put(i, String.valueOf(i));
        }
        return options;
    }


    /**
     * Getter for the actionValue property.
     *
     * @return string
     */
    public String getActionValue() {
        return actionValue;
    }


    /**
     * Setter for the actionValue property.
     *
     * @param actionValue - new actionValue to set
     */
    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
     */
    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
     */
    @Override
    protected Region getRegion() {
        return getReplenishmentRegion();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 