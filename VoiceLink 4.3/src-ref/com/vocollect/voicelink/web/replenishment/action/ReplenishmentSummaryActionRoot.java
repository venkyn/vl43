/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.replenishment.model.ReplenishmentAssignmentSummary;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


/**
 * Action Class for replenishment Summary.
 *
 * @author svoruganti
 */
public class ReplenishmentSummaryActionRoot extends DataProviderAction implements Preparable {

    /*
     * Auto Generated Serial Version ID
     */
    private static final long serialVersionUID = 1912684108383039003L;

    /*
     * The ID of the view we need to grab from the DB.
     */
    private static final long REPLENISHMENT_SUMMARY_VIEW_ID = -1038;

    private static final String SITE_COLUMN = "site";

    /*
     * The list of columns for the replenishment table.
     */
    private List<Column> replenishmentSummaryColumns;

    // The replenishment management service.
    private ReplenishmentManager replenishmentManager;


    /*
     *  The Replenishment management service.
     */
    private ReplenishmentAssignmentSummary replenishmentAssignmentSummary = null;

    /**
     * Getter for the REPLENISHMENT_SUMMARY_VIEW_ID property.
     *
     * @return long value of the property
     */
    public static long getReplenishmentSummaryViewId() {
        return REPLENISHMENT_SUMMARY_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getReplenishmentManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "replenishmentsummary";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(REPLENISHMENT_SUMMARY_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     * Action for the replenishment view page. Initializes the replenishments table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View replenishmentSummaryView = getUserPreferencesManager()
            .getView(REPLENISHMENT_SUMMARY_VIEW_ID);
        this.replenishmentSummaryColumns = this.getUserPreferencesManager().getColumns(
            replenishmentSummaryView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
         List<ListObject> list = new ArrayList<ListObject>();
         list.add(new ListObject(Tag.ALL, getText("notification.dropdown.site.All")));
         list.add(new ListObject(Tag.SYSTEM, getText("notification.dropdown.site.System")));
         for (ListObject lo : super.getSiteList()) {
             list.add(lo);
         }
         return list;
    }



    /**
     * Getter for the replenishmentSummaryColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getReplenishmentSummaryColumns() {
        Iterator<Column> columnIter = this.replenishmentSummaryColumns.iterator();
        boolean found = false;
        while (columnIter.hasNext() && !found) {
            Column column = columnIter.next();
            if (column.getField().equals(SITE_COLUMN)) {
                found = true;

                if (SiteContextHolder.getSiteContext().isInAllSiteMode()) {
                    // We only want to display the site column when in the all
                    // display mode
                    column.setDisplayable(true);
                } else {
                    column.setDisplayable(false);
                }
            }
        }

        return this.replenishmentSummaryColumns;
    }


    /**
     * Setter for the replenishmentSummaryColumns property.
     * @param replenishmentSummaryColumns the new replenishmentSummaryColumns value
     */
    public void setReplenishmentSummaryColumns(List<Column> replenishmentSummaryColumns) {
        this.replenishmentSummaryColumns = replenishmentSummaryColumns;
    }

    /**
     * Getter for the replenishmentAssignmentSummary property.
     * @return ReplenishmentAssignmentSummary value of the property
     */
    public ReplenishmentAssignmentSummary getReplenishmentAssignmentSummary() {
        return replenishmentAssignmentSummary;
    }


    /**
     * Setter for the replenishmentAssignmentSummary property.
     * @param replenishmentAssignmentSummary the new replenishmentAssignmentSummary value
     */
    public void setReplenishmentAssignmentSummary(ReplenishmentAssignmentSummary replenishmentAssignmentSummary) {
        this.replenishmentAssignmentSummary = replenishmentAssignmentSummary;
    }


    /**
     * Getter for the replenishmentManager property.
     * @return ReplenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }


    /**
     * Setter for the replenishmentManager property.
     * @param replenishmentManager the new replenishmentManager value
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 