/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.replenishment.ReplenishmentErrorCode;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.web.core.action.ResequenceAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * This is the Struts action class that handles operations on <code>Replenishment</code> objects.
 *
 * @author svoruganti
 */
public class ReplenishmentActionRoot extends ResequenceAction implements Preparable {

    // Auto Generated Serial Version ID
    private static final long serialVersionUID = -3057703568501031816L;

    // Logger for Replenishment Action Class
    private static final Logger log = new Logger(ReplenishmentAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long REPLENISHMENT_VIEW_ID = -1028;
    private static final long RESEQUENCE_ASSIGNMENT_VIEW_ID = -1034;

    // The list of columns for the replenishment table.
    private List<Column> replenishmentColumns;

    // The Replenishment management service.
    private ReplenishmentManager replenishmentManager = null;

    private LocationManager locationManager = null;

    // The Replenishment object, which will either be newly created,
    // or retrieved via the ReplenishmentId.
    private Replenishment replenishment;

    // The ID of the replenishment.
    private Long replenishmentId;

    // The ID of the region.
    private Long regionId;

    // assignment Status List
    private Map<Integer, String> replenishmentStatus;

    // Operator Manager.
    private OperatorManager operatorManager = null;

    // parameter that Indicates replenishment status
    private Integer status;

    // parameter that indiactes replenishment status
    private Integer replenStatus;


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return this.locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }


    /**
     * This method sets up the <code>Replenishment</code> object by retrieving it
     * from the database when a replenishmentId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {

       if (this.replenishmentId != null) {
           // We have an ID, but not a replenishment object yet.
           if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
           // This means the replenishmentis being edited, so get it from the session.
               this.replenishment = (Replenishment) getEntityFromSession(getSavedEntityKey());
           } else {
                this.replenishment = this.replenishmentManager.get(this.replenishmentId);
                saveEntityInSession(this.replenishment);
            }
            if (log.isDebugEnabled()) {
                log.debug("Replenishment version is: " + this.replenishment.getVersion());
            }
       } else {
           this.replenishment = new Replenishment();
       }
    }

    /**
     * Create or update the user specified by the <code>Replenishment</code>
     * member of this class.
     *
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = replenishment.isNew();

        try {
            if (isNew) {
                replenishmentManager.save(this.replenishment);
            } else {
                // otherwise we are editing the assignment
                replenishmentManager.executeUpdateReplenishment(replenishment);
                // NOTE: The call to executeReportReplenished should be made as part
                //       of the call to executeUpdateReplenishment but is it being done
                //       here to work around a circular dependency between the
                //       Location Manager and the Replenishment Manager.
                //       This Location manager needs to be split into two managers,
                //       the Location Manager and the ReplenishmentLocationManager.
                //       Splitting up this manager would solve the dependency problem.
                //       We could move the executeReportReplenished call to the
                //       executeUpdateReplenishment call.
                if (replenishment.getStatus() == ReplenishStatus.Canceled) {
                    this.getLocationManager().executeReportReplenished(replenishment.getToLocation(),
                        replenishment.getItem());
                }
            }
            cleanSession();
        } catch (EntityNotFoundException e) {
            log.warn("replenishment Number " + this.replenishment.getNumber()
                + " not found, possibly a concurrency");

            addSessionActionMessage(new UserMessage(
                "replenishment.edit.error.replenishmentNotFound", null, null));
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified replenishment " + this.replenishment.getNumber());
            addActionError(new UserMessage("entity.error.modified",
                                           UserMessage.markForLocalization("entity.replenishment"),
                                           SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Replenishment modifiedReplenishment =
                    getReplenishmentManager().get(getReplenishmentId());
                // Set the local object's version to match, so it will work if the user resubmits.
                this.replenishment.setVersion(modifiedReplenishment.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedReplenishment);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted", this.replenishment.getNumber()));
                return SUCCESS;
            }
            return INPUT;
        }
        addSessionActionMessage(new UserMessage("replenishment." + (isNew ? "create" : "edit")
             + ".message.success", String.valueOf(this.replenishment.getNumber())));

        return SUCCESS;
    }


    /**
     * Action for the replenishment view page. Initializes the replenishments table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View replenishmentView = getUserPreferencesManager().getView(REPLENISHMENT_VIEW_ID);
        this.replenishmentColumns =
            this.getUserPreferencesManager().getColumns(replenishmentView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Get the map for function of Replenishment for modify replenishment status.
     * @return the  Replenishment replenishment Status.
     */
    public Map<Integer, String>  getReplenishmentStatus() {
        replenishmentStatus = new HashMap<Integer, String>();
        replenishmentStatus.put(ReplenishStatus.Available.toValue(),
                                ResourceUtil.getLocalizedEnumName(ReplenishStatus.Available));
        replenishmentStatus.put(ReplenishStatus.Unavailable.toValue(),
                                ResourceUtil.getLocalizedEnumName(ReplenishStatus.Unavailable));
        return replenishmentStatus;
    }

    /**
     * Get the map for function of Replenishment for edit form.
     * @return the  Replenishment replenishment Status.
     */
    public Map<Integer, String>  getReplenishmentStatusMap() {
        replenishmentStatus = new HashMap<Integer, String>();
        replenishmentStatus.put(ReplenishStatus.Available.toValue(),
                                ResourceUtil.getLocalizedEnumName(ReplenishStatus.Available));
        replenishmentStatus.put(ReplenishStatus.Unavailable.toValue(),
                                ResourceUtil.getLocalizedEnumName(ReplenishStatus.Unavailable));
        replenishmentStatus.put(ReplenishStatus.Canceled.toValue(),
                                ResourceUtil.getLocalizedEnumName(ReplenishStatus.Canceled));
        return replenishmentStatus;
    }

    /**
     * Setter for Operator ID used by Edit Assignment functionality.
     * @param operatorId The operator ID.
     */
    public void setOperatorId(Long operatorId) {
        try {
            if (operatorId == 0 || operatorId == null) {
                this.replenishment.setOperator(null);
            } else {
                Operator operator = this.operatorManager.get(operatorId);
                this.replenishment.setOperator(operator);
            }
        } catch (DataAccessException e) {
            log.warn("Operator not found in database", e);
        }
    }



    /**
     * Function for obtaining all operator names to display in the drop down of
     * the operator field on the edit page for a specific region.
     *
     * @return the map of operator identifiers, key by operator PKs (Ids)
     */

    public Map<Long, String> getAllOperatorsInRegion() {
        Map<Long, String> operators = new LinkedHashMap<Long, String>();

        try {
            if (this.replenishment.getStatus() != ReplenishStatus.Complete) {
                Region region = this.replenishment.getRegion();
                Operator op = this.replenishment.getOperator();
                if (op != null) {
                    if (op.getId() != null) {
                        operators.put(op.getId(), op.getOperatorIdentifier());
                    }
                }
                operators.put(0L, getText(new UserMessage("operator.none")));
                List<Operator> allOperators = getOperatorManager().listAuthorizedForRegion(region.getId());
                for (Operator operatorname : allOperators) {
                    operators.put(operatorname.getId(), operatorname.getOperatorIdentifier());
                }
            }
        } catch (Exception e) {
            log.warn("Error occurred while obtaining Operators in Region");
        }
        return operators;
    }


    /**
     * Function for validating if an assignment is editable or not.
     *
     * @return String defining next target to forward to.
     * @throws BusinessRuleException - on exception
     */
    public String checkIfEditable() throws BusinessRuleException {

        try {
            Replenishment r = this.replenishmentManager.get(getIds()[0]);
            validateIfEditable(r);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating assignment for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?replenishmentId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }


    /**
     * Function to check for editing validation business logic.
     *
     * @param r Replenishment to be validated
     * @throws BusinessRuleException if the business rule is violated.
     */
    protected void validateIfEditable(Replenishment r) throws BusinessRuleException {

        if (r.getStatus().isInSet(ReplenishStatus.Canceled, ReplenishStatus.InProgress,
                                  ReplenishStatus.Complete)) {

            throw new BusinessRuleException(ReplenishmentErrorCode.EDIT_ASSIGNMENT_INVALID_STATUS,
                    new UserMessage("replenishment.edit.not.editable"));
            }
    }

    /**
     * Getter for the replenStatusForChange property.
     * @return String value of the property
     * @throws DataAccessException - unable to retrieve replenishment
     */
    public String getReplenishmentStatusForChange() throws DataAccessException {
        if (getReplenishmentStatus().isEmpty()) {
            setJsonMessage(getText(new UserMessage("replenishment.label.error.message",
                                                    ERROR_FAILURE)), ERROR_FAILURE);
            return SUCCESS;
        } else {
            return INPUT;
        }
    }


    /**
     * Setter for the replenStatus property.
     * @param replenStatus the new replenStatus value
     */
    public void setReplenStatus(Integer replenStatus) {
        this.replenStatus = replenStatus;
        this.replenishment.setStatus(ReplenishStatus.toEnum(this.replenStatus));
    }


    /**
     * changeReplenishmentStatus - changes the status of replenishments.
     * @return String value - controls the xwork flow
     * @throws DataAccessException - when replenishment cannot be retrieved
     */
    public String changeReplenishmentStatus() throws DataAccessException {
    return super.performAction(new CustomActionImpl() {

        public SystemErrorCode getSystemErrorCode() {
            return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
        }

        public String getActionPrefix() {
            return "status";
        }

        public boolean execute(DataObject obj) throws VocollectException, DataAccessException,
                                                      BusinessRuleException {
            getReplenishmentManager().executeUpdateStatus((Replenishment) obj, ReplenishStatus.toEnum(status));
            return true;
        }
     });
    }


    /**
     * Get the region selection screen or error.
     * @return the control flow target name.
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public String getResequenceRegion() throws DataAccessException, VocollectException {
    if (getResequenceRegionMap().isEmpty()) {
        setJsonMessage(
            getText(new UserMessage("replenishment.resequence.error.noRegions.message",
                                    ERROR_FAILURE)), ERROR_FAILURE);
        return SUCCESS;
        } else {
            return INPUT;
        }
    }

    /**
     * Function for obtaining all regions for resequence.
     *
     * @return the map of region names, key by region ids
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public Map<Long, String> getResequenceRegionMap()
    throws DataAccessException, VocollectException {
        try {
            resequenceRegionMap = new LinkedHashMap<Long, String>();
            List<Region> regions = getRegionManager().listReplenishmentResequenceRegions();
            for (Region r : regions) {
                resequenceRegionMap.put(r.getId(), r.getName());
            }
        } catch (DataAccessException e) {
            addSessionActionMessage(e.getUserMessage());
        }
        return resequenceRegionMap;
    }

    /**
     * Gets the viewId for resequence.
     * @return resequence viewId.
     */
    @Override
    public Long getResequenceViewId() {
        return RESEQUENCE_ASSIGNMENT_VIEW_ID;
    }

    /**
     * Getter for list of assignments.
     * @param theRegionId - the region to resequence
     * @throws DataAccessException  - on failure
     * @return - list of resequenced assignments
     */
    @Override
    public List<DataObject> getAssignments(Long theRegionId) throws DataAccessException {
        return replenishmentManager.listResequenceAssignmentsByRegion(theRegionId);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.ResequenceActionRoot#executeResequence(java.util.ArrayList, java.lang.Long)
     */
    @Override
    public void executeResequence(ArrayList<Long> sequenceList, Long theRegionId)
        throws DataAccessException, BusinessRuleException {
        replenishmentManager.executeResequence(sequenceList, theRegionId);
    }

    /**
     * Retrieves table data for the resequence page.
     * @return String SUCCESS
     * @throws Exception in case of DataAccess issues
     */
    @Override
    public String getResequenceTableData() throws Exception {
        // Always want data to start of in order of current sequence number
        setSortAsc(true);
        setSortColumn("issuanceOrder.sequenceNumber");
        return super.getResequenceTableData();
    }

    /**
     * Getter for the replenStatus property.
     * @return String value of the property
     */
    public Integer getReplenStatus() {
        if (this.replenishment != null) {
            if (this.replenishment.getStatus() == ReplenishStatus.Unavailable) {
                return ReplenishStatus.Unavailable.toValue();
            } else if (this.replenishment.getStatus() == ReplenishStatus.Available) {
                return ReplenishStatus.Available.toValue();
            } else if (this.replenishment.getStatus() == ReplenishStatus.InProgress) {
                return ReplenishStatus.InProgress.toValue();
            } else if (this.replenishment.getStatus() == ReplenishStatus.Complete) {
                return ReplenishStatus.Complete.toValue();
            } else if (this.replenishment.getStatus() == ReplenishStatus.Canceled) {
                return ReplenishStatus.Canceled.toValue();
            }
        }
        return 0;
    }




    /**
     * Set the replenishmentStatus for the Replenishment.
     * @param replenishmentStatus the new replenishmentStatus
     */
    public void setReplenishmentStatus(Map<Integer, String> replenishmentStatus) {
        this.replenishmentStatus = replenishmentStatus;
    }


    /**
     * Getter for the Operator Manager.
     * @return Operator Manager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Sets Work Group Manager.
     * @param manager Instance of Work Group Manager class
     */
    public void setOperatorManager(OperatorManager manager) {
        this.operatorManager = manager;
    }

    /**
     * Getter for the status property.
     * @return Integer value of the property
     */
    public Integer getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * Getter for the regionId property.
     * @return Long value of the property
     */
    @Override
    public Long getRegionId() {
        return regionId;
    }


    /**
     * Setter for the regionId property.
     * @param regionId the new regionId value
     */
    @Override
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getReplenishmentManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "replenishment";
    }

    /**
     * Getter for the replenishmentManager property.
     * @return replenishmentManager value of the property
     */
    public ReplenishmentManager getReplenishmentManager() {
        return replenishmentManager;
    }

    /**
     * Setter for the replenishment service.
     * @param replenishmentManager the new replenishmentManager
     */
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenishmentManager = replenishmentManager;
    }

    /**
     * Getter for the replenishment property.
     * @return replenishment value of the property
     */
    public Replenishment getReplenishment() {
        return replenishment;
    }

    /**
     * Setter for the replenishment.
     * @param replenishment the new replenishment
     */
    public void setReplenishment(Replenishment replenishment) {
        this.replenishment = replenishment;
    }

    /**
     * Getter for the replenishment property.
     * @return replenishment value of the property
     */
    public Long getReplenishmentId() {
        return replenishmentId;
    }

    /**
     * Setter for the replenishmentId.
     * @param replenishmentId the new replenishmentId
     */
    public void setReplenishmentId(Long replenishmentId) {
        this.replenishmentId = replenishmentId;
    }

    /**
     * Getter for the REPLENISHMENT_VIEW_ID property.
     * @return long value of the property
     */
    public static long getReplenishmentViewId() {
        return REPLENISHMENT_VIEW_ID;
    }


    /**
     * Getter for the replenishmentColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getReplenishmentColumns() {
        return this.replenishmentColumns;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(REPLENISHMENT_VIEW_ID);
        viewIds.add(RESEQUENCE_ASSIGNMENT_VIEW_ID);
        return viewIds;
    }
    /**
     * Getter for the Replenishment Status property.
     * @return String - localized value of replenishment status
     */
    public String getReplenishStatus() {
        return getText(this.replenishment.getStatus().getResourceKey());
    }

    /**
     * Getter for the replenishment type property.
     * @return String - localized value of replenishment type.
     */
    public String getReplenishType() {
        return getText(this.replenishment.getType().getResourceKey());
    }

    /**
     * Getter for the replenishment priority property.
     * @return String - localized value of replenishment priority.
     */
    public String getReplenishPriority() {
        return getText(this.replenishment.getPriority().getResourceKey());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 