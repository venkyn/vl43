/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.FilterTimeWindow;
import com.vocollect.epp.util.FilterUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.epp.web.util.JSONResponseBuilder;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;

import com.opensymphony.xwork2.Preparable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class handles all labor actions from the user interface.
 * @author brupert
 */
public class LaborActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 4904894066272153678L;

    private static final Logger log = new Logger(LaborActionRoot.class);

    // Manager class for queries on operator labor including aggregates.
    private OperatorLaborManager operatorLaborManager;
    private LaborManager laborManager;

    // Request parameters that drive the operator history table.
    private String operatorIds;
    private String regionIds;
    private String filterTypes;

    // Request parameter for edit end time.
    private String endTime;
    private String newEndTime;


    /**
     * Getter for the newEndTime property.
     * @return String value of the property
     */
    public String getNewEndTime() {
        return newEndTime;
    }


    /**
     * Setter for the newEndTime property.
     * @param newEndTime the new newEndTime value
     */
    public void setNewEndTime(String newEndTime) {
        this.newEndTime = newEndTime;
    }


    // Columns for the operator labor dual-table page.
    private List<Column> operatorSummaryColumns;
    private List<Column> operatorHistoryColumns;

    // The view ID of the upper table.
    private static final Long OPERATOR_LABOR_SUMMARY_VIEW_ID = -1022L;

    // The view ID of the lower table.
    private static final Long OPERATOR_LABOR_HISTORY_VIEW_ID = -1023L;

    private static final Long OPERATOR_LABOR_SUMMARY_FUNCTION_COLUMN_ID = -12110L;
    private static final Long ENUM_EQUALS_OPERAND_ID = -31L;

    private static final String ARRAY_DELIMITER = ",";

    private Long timeFilterSaved;

    private Long timeWindow;

    // Set to false to comply with VL 3.1 REQ 251
    private Boolean filterByFunction = false;

    // Removed because this was never read locally
    //private static final String SHORT_DATE_FORMAT = "M/d/y h:m:s a";

    /**
     * Gets the earliest end time for a labor record.
     * @return Struts code.
     * @throws Exception On error.
     */
    public String getEndTimeToModify() throws Exception {

        JSONObject json = new JSONObject();
        Date date = null;

        try {
            date = getLaborManager().getEarliestEndTime(this.getIds()[0]);
        } catch (BusinessRuleException bre) {
            log.error(
                "Error while modifying labor record end time. could not calculate valid end time.",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, bre);
            json.put(JSONResponseBuilder.ERROR_CODE, JSONResponseBuilder.ERROR_FAILURE);
            json.put("errorMessage", getText(bre.getUserMessage()));
            setJsonMessage(json.toString());
            return SUCCESS;
        }

        DisplayUtilities du = getDisplayUtilities();
        String endtime;
        try {
            endtime = du.formatTimeWithTimeZone(date, null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            endtime = date.toString();
        }

        try {
            json.put(JSONResponseBuilder.ERROR_CODE, JSONResponseBuilder.ERROR_SUCCESS);
            json.put("endTime", endtime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.setNewEndTime(endtime);
        setJsonMessage(json.toString());
        return SUCCESS;
    }

    /**
     * Modifies the end time of a labor record.
     * @return Struts code.
     * @throws Exception On error.
     */
    public String modifyEndTime()  throws Exception {
        return super.performAction(new CustomActionImpl() {
            public String getActionPrefix() {
                return "endTime";
            }

            public boolean execute(DataObject obj)
                    throws VocollectException, DataAccessException, BusinessRuleException {
                Date date = getLaborManager().getEarliestEndTime(getIds()[0]);
                getLaborManager().executeModifyEndTime((OperatorLabor) obj, date);
                return true;
            }
            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }
        });
    }

    /**
     * Constructs a where clause and determines the value for the time window
     * before attempting to fetch data from the DB.
     * @return The result of <code>DataProviderAction.getTableData(String)</code>.
     * @throws Exception On error.
     */
    public String getLaborHistoryData() throws Exception {

        if (this.hasDataToDisplay()) {
            return getTableData(this.buildWhereClause(), this.determineTimeWindow());
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
    }

    /**
     * Constructs a where clause and determines the value for the time window
     * before attempting to fetch data from the DB.
     * @return The result of <code>DataProviderAction.getPrintData(String)</code>.
     * @throws Exception On error.
     */
    public String getLaborHistoryDataForPrint() throws Exception {

        if (this.hasDataToDisplay()) {
            return getPrintData(this.buildWhereClause(), this.determineTimeWindow());
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
    }

    /**
     *
     * @return - true if we need to display data
     */
    private boolean hasDataToDisplay() {

        boolean result = true;
        if (getOperatorIds() == null || getRegionIds() == null
            || getOperatorIds().length() == 0 || getRegionIds().length() == 0) {
            result = false;
           }
        return result;
    }

    /**
     * Determine the value of the time window for the query.
     * @return and array of objects that contains the time window value.
     */
    private Object[] determineTimeWindow() {
        FilterTimeWindow filterTimeWindow =
            FilterTimeWindow.getById(timeWindow.intValue());
        Date dateOffset = filterTimeWindow.applyOffset(new Date());
        // add new query arg
        Object[] queryArgs = new Object[1];
        queryArgs[0] = dateOffset;
        return queryArgs;
    }


    /**
     *
     * @return where clause to send to business logic servie
     * @throws Exception - on failure
     */
    public String buildWhereClause() throws Exception {
        StringBuffer where = new StringBuffer();
        String[] operIds = getOperatorIds().split(ARRAY_DELIMITER);
        String[] regIds = getRegionIds().split(ARRAY_DELIMITER);
        String[] types = getFilterTypes().split(ARRAY_DELIMITER);
        for (int i = 0; i < operIds.length; i++) {
            // These arrays should always be in sync.
            if (i == 0) {
                where.append("(");
            }
            if (i != 0) {
                where.append(" or ");
            }
            where.append("(obj.operator.id=");
            where.append(operIds[i]);
            where.append(" and obj.region.id=");
            where.append(regIds[i]);
            if (regIds[i].equals("null")) {
                where.append(" and obj.filterType=");
                where.append(types[i]);
            }
            where.append(")");
        }
        if (where.length() > 0) {
            where.append(")");
        }
        if (log.isDebugEnabled()) {
            log.debug("constructed where clause: " + where);
        }
        return where.toString();
    }


    /**
     * Action for the labor view page. Initializes the operator labor columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View view = getUserPreferencesManager().getView(OPERATOR_LABOR_SUMMARY_VIEW_ID);
        this.operatorSummaryColumns = this.getUserPreferencesManager().getColumns(view, getCurrentUser());
        if (getTimeFilterSaved() != null) {
            setTimeWindow(getTimeFilterSaved());
            getUserPreferencesManager().saveTimeWindow(view.getId(), getCurrentUser(), getTimeFilterSaved());
        } else {
            Long theTimeWindow = getUserPreferencesManager().getTimeWindow(view, getCurrentUser());
            setTimeWindow(theTimeWindow);
        }
        view = getUserPreferencesManager().getView(OPERATOR_LABOR_HISTORY_VIEW_ID);
        this.operatorHistoryColumns = this.getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Overrides the user's filter for the Function column.
     * @param olft Type to filter by.
     * @throws DataAccessException On database issue.
     */
    public void overrideFunctionFilter(OperatorLaborFilterType olft) throws DataAccessException {
        // lets go...
        FilterCriterion crit = this.getFilterManager().constructFilterCriterion(
                                   OPERATOR_LABOR_SUMMARY_VIEW_ID,
                                   OPERATOR_LABOR_SUMMARY_FUNCTION_COLUMN_ID,
                                   ENUM_EQUALS_OPERAND_ID,
                                   String.valueOf(olft.toValue()), "", false);
        View view = getUserPreferencesManager().getView(OPERATOR_LABOR_SUMMARY_VIEW_ID);
        if (getFilters() == null) {
            setFilters(new LinkedList<Filter>());
        }
        // remove filters for this column, the append the one we want to override.
        FilterUtil.removeFiltersFromColumn(getFilters(), OPERATOR_LABOR_SUMMARY_FUNCTION_COLUMN_ID);
        FilterUtil.appendFilterToView(getFilters(), view, crit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getKeyPrefix() {
        return "labor";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataProvider getManager() {
        return getOperatorLaborManager();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getViewIds() {
        ArrayList<Long> viewIds = new ArrayList<Long>(2);
        viewIds.add(OPERATOR_LABOR_SUMMARY_VIEW_ID);
        viewIds.add(OPERATOR_LABOR_HISTORY_VIEW_ID);
        return viewIds;
    }


    /**
     * Gets the ID for the summary view.
     * @return The view ID for the operator labor table.
     */
    public Long getOperatorLaborSummaryViewId() {
        return OPERATOR_LABOR_SUMMARY_VIEW_ID;
    }

    /**
     * Gets the ID for the history view.
     * @return The view ID for the operator labor table.
     */
    public Long getOperatorLaborHistoryViewId() {
        return OPERATOR_LABOR_HISTORY_VIEW_ID;
    }

    /**
     * Gets the labor manager property.
     * @return The labor manager.
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Sets the labor manager property.
     * @param operatorLaborManager The labor manager to set.
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * Gets the columns for the operator labor history table.
     * @return Column data.
     */
    public List<Column> getOperatorHistoryColumns() {
        return operatorHistoryColumns;
    }

    /**
     * Gets the columns for the operator labor summary table.
     * @return Column data.
     */
    public List<Column> getOperatorSummaryColumns() {
        return operatorSummaryColumns;
    }

    /**
     * Gets the operator Ids that the history table's query uses.
     * @return A comma separated list of IDs for <code>Operator</code>s.
     */
    public String getOperatorIds() {
        return operatorIds;
    }

    /**
     * Sets the operator Ids for the history table's query.
     * @param operatorIds A list of Ids that map to <code>Operator</code>s.
     */
    public void setOperatorIds(String operatorIds) {
        this.operatorIds = operatorIds;
    }

    /**
     * Gets the region Ids that the history table's query uses.
     * @return A comma separated list of IDs for <code>Region</code>s.
     */
    public String getRegionIds() {
        return regionIds;
    }

    /**
     * Sets the region Ids for the history table's query.
     * @param regionIds A list of Ids that map to <code>Region</code>s.
     */
    public void setRegionIds(String regionIds) {
        this.regionIds = regionIds;
    }

    /**
     * @return the labor manger
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * @param laborManager set with DI
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * @return The end time request parameter.
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime The end time request parameter.
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * Getter for the filterTypes property.
     * @return String value of the property
     */
    public String getFilterTypes() {
        return this.filterTypes;
    }

    /**
     * Setter for the filterTypes property.
     * @param filterTypes the new filterTypes value
     */
    public void setFilterTypes(String filterTypes) {
        this.filterTypes = filterTypes;
    }


    /**
     * @return the saved time filter
     */
    public Long getTimeFilterSaved() {
        return timeFilterSaved;
    }

    /**
     *
     * @param timeFilterSaved save the time filter
     */

    public void setTimeFilterSaved(Long timeFilterSaved) {
        this.timeFilterSaved = timeFilterSaved;
    }

    /**
     *
     * @return the filter
     */
    public Boolean getFilterByFunction() {
        return this.filterByFunction;
    }

    /**
     *
     * @param defaultFilter - the default filter
     */
    public void setFilterByFunction(Boolean defaultFilter) {
        this.filterByFunction = defaultFilter;
    }

    /**
     *
     * @return the time window value
     */
    public Long getTimeWindow() {
        return timeWindow;
    }

    /**
     *
     * @param timeWindow the time window value
     */
    public void setTimeWindow(Long timeWindow) {
        this.timeWindow = timeWindow;
    }


    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 