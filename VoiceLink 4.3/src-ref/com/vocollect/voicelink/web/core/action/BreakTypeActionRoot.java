/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.dao.BreakTypeDAO;
import com.vocollect.voicelink.core.model.BreakType;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>BreakType</code> objects.
 *
 * @author mnichols
 */
public class BreakTypeActionRoot extends DataProviderAction implements Preparable {

        //
    private static final long serialVersionUID = -6498360412669771524L;

    private static final Logger log = new Logger(BreakTypeActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1012;

    // The list of columns for the breaktypes table.
    private List<Column> columns;

    // The BreakType management service.
    private GenericManager<BreakType, BreakTypeDAO> breakTypeManager = null;

    // The BreakType object, which will either be newly created, or retrieved
    // via the BreakTypeId.
    private BreakType breakType;

    // The ID of the break type.
    private Long breakTypeId;

    /**
     * Getter for the breakType property.
     * @return User value of the property
     */
    public BreakType getBreakType() {
        return this.breakType;
    }

    /**
     * Setter for the breakType property.
     * @param breakType the new breakType value
     */
    public void setBreakType(BreakType breakType) {
        this.breakType = breakType;
    }

    /**
     * Getter for the breakTypeId property.
     * @return Long value of the property
     */
    public Long getBreakTypeId() {
        return this.breakTypeId;
    }

    /**
     * Setter for the breakTypeId property.
     * @param breakTypeId the new breakTypeId value
     */
    public void setBreakTypeId(Long breakTypeId) {
        this.breakTypeId = breakTypeId;
    }

    /**
     * Getter for the breakTypeManager property.
     * @return UserManager value of the property
     */
    public GenericManager<BreakType, BreakTypeDAO> getBreakTypeManager() {
        return this.breakTypeManager;
    }

    /**
     * Setter for the breakTypeManager property.
     * @param manager the new breakTypeManager value
     */
    public void setBreakTypeManager(GenericManager<BreakType, BreakTypeDAO> manager) {
        this.breakTypeManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getBreakTypeColumns() {
        return this.columns;
    }

    /**
     * Action for the breakTypes view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View breakTypeView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            breakTypeView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the breakType specified by the <code>breakType</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.breakType.isNew();
        try {

            if (this.breakType.getName() == null
                || this.breakType.getName().trim().equalsIgnoreCase("")) {
                addFieldError(
                    "breakType.name", getText("region.create.label.name")
                        + getText("errors.required"));
            }
            if (this.breakType.getNumber() == null) {
                addFieldError(
                    "breakType.number", getText("region.create.label.number")
                        + getText("errors.required"));
            }
            if (hasErrors()) {
                return (INPUT);
            }

            //Uniqueness check for number
            Long numberUniquenessId = this.breakTypeManager.getPrimaryDAO()
                .uniquenessByNumber(this.breakType.getNumber());
            if (numberUniquenessId != null
                && (isNew || (!isNew && numberUniquenessId.longValue() != this.breakType
                    .getId().longValue()))) {
                log.warn("A break type '" + this.breakType.getNumber()
                    + "' already exists");
                addFieldError("breakType.number", new UserMessage(
                    "breakType.create.error.existingNumber", this.breakType
                        .getNumber()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            //Uniqueness check for name
            Long nameUniquenessId = this.breakTypeManager.getPrimaryDAO()
                .uniquenessByName(this.breakType.getName());
            if (nameUniquenessId != null
                && (isNew || (!isNew && nameUniquenessId.longValue() != this.breakType
                    .getId().longValue()))) {
                log.warn("A break type '" + this.breakType.getName()
                    + "' already exists");
                addFieldError("breakType.name", new UserMessage(
                    "breakType.create.error.existingName", this.breakType
                        .getName()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            breakTypeManager.save(this.breakType);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified break type "
                + this.breakType.getName());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.BreakType"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                BreakType modifiedEntity = getBreakTypeManager().get(
                    getBreakTypeId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.breakType.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.breakType.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "breakType." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(new UserMessage(
            successKey,
            makeGenericContextURL("/breakType/view.action?breakTypeId="
                + breakType.getId()), this.breakType.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * This method sets up the <code>BreakType</code> object by retrieving it
     * from the database when a breakTypeId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.breakTypeId != null) {
            // We have an ID, but not a breakType object yet.
            if (log.isDebugEnabled()) {
                log.debug("breakTypeId is " + this.breakTypeId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the break type is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved break type from session");
                }
                this.breakType = (BreakType) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting break type from database");
                }
                this.breakType = this.breakTypeManager.get(this.breakTypeId);
                saveEntityInSession(this.breakType);
            }
            if (log.isDebugEnabled()) {
                log.debug("Break type version is: "
                    + this.breakType.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new breakType object");
            }
            this.breakType = new BreakType();
        }
    }

    /**
     * Delete the location identified by the <code>locationId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentBreakType() throws Exception {

        BreakType breakTypeToDelete = null;

        try {
            breakTypeToDelete = breakTypeManager.get(this.breakTypeId);
            breakTypeManager.delete(this.breakTypeId);

            addSessionActionMessage(new UserMessage(
                "breakType.delete.message.success", breakTypeToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete break type: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getBreakTypeManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "breakType";
    }

    /**
     * Getter for the BREAKTYPE_VIEW_ID property.
     * @return long value of the property
     */
    public static long getBreakTypeViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * @return the current BreakType name, translated for the current Locale, or
     * the untranslated name if the BreakType name isn't translated for this Locale,
     * or <code>null</code> if the current BreakType isn't defined.
     */
    public String getTranslatedBreakTypeName() {
        if (getBreakType() == null) {
            return null;
        } else {
            return getDataTranslation(getBreakType().getName());
        }
    }

    /**
     * @return the current BreakType name, translated for the current Locale, or
     * <code>null</code> if the BreakType name isn't translated for this Locale,
     * or if the current BreakType isn't defined.
     */
    public String getTranslatedBreakTypeNameOrNull() {
        if (getBreakType() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getBreakType().getName());
        }
    }

    /**
     * @return the message containing the Locale and the translated BreakType name,
     *         or null if there is no translation.
     */
    public String getTranslatedBreakTypeNameMessage() {
        return getDataTranslationMessage(getTranslatedBreakTypeNameOrNull());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 