/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.RemovableDataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Resequencable;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.ResequenceManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *  Action code for Resequence - should be extended for Selection and
 *  Replenishment.
 *
 * @author bnichols
 */
public abstract class ResequenceActionRoot extends DataProviderAction implements
    Preparable {

    private static final int MOVE_UP = 1;
    private static final int MOVE_DOWN = 2;
    private static final int MOVE_TO_TOP = 3;

    //  logger for ResequenceRootAction class
    private static final Logger log = new Logger(ResequenceActionRoot.class);

    // The list of columns for the resequence assignments table.
    private List<Column> resequenceAssignmentColumns;

    // Region manager is service for regions
    private RegionManager regionManager;

    // Resequence manager is service for resequencable objects
    private ResequenceManager resequenceManager;

    // Sorted list of assignment ids to resequence
    private ArrayList<Long> sequenceList = new ArrayList<Long>();

    // Id of the region to resequence
    private Long regionId;

    // map of regions available for resequence
    protected Map<Long, String> resequenceRegionMap;

    private Integer moveDirection;

    private ArrayList<Long> selectedIds = new ArrayList<Long>();

    /**
     * Getter for moveDirection.
     * @return moveDirection
     */
    public Integer getMoveDirection() {
        return moveDirection;
    }


    /**
     * Setter for moveDirection.
     * @param moveDirection the new moveDirection
     */
    public void setMoveDirection(Integer moveDirection) {
        this.moveDirection = moveDirection;
    }


    /**
     * Getter for resequence manager.
     * @return ResequenceManager
     */
    public ResequenceManager getResequenceManager() {
        return resequenceManager;
    }

    /**
     * Setter for resequenceManager.
     * @param resequenceManager the new ResequenceManager
     */
    public void setResequenceManager(ResequenceManager resequenceManager) {
        this.resequenceManager = resequenceManager;
    }

    /**
     * Getter for region manager.
     * @return RegionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for regionManager.
     * @param regionManager the new RegionManager
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for region id.
     * @return Long region Id
     */
    public Long getRegionId() {
        return regionId;
    }

    /**
     * Setter for region id.
     * @param regionId the new region id
     */
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    /**
     * Retrieves table data for the resequence page.
     * @return String SUCCESS
     * @throws Exception in case of DataAccess issues
     */
    @SuppressWarnings("unchecked")
    public String getResequenceTableData() throws Exception {
        if (getSavedEntityKey() != null) {
            sequenceList = (ArrayList<Long>) getEntityFromSession(getSavedEntityKey());
            super.getTableData(
                    "obj.region.id = " + getRegionId(), new Object[] { sequenceList });
        }
        return SUCCESS;
    }

    /**
     * Moves the selected assignment up in the list by swapping it with the previous assignment.
     * @param assignment the assignment to move
     */
    public void moveAssignmentUp(Resequencable assignment) {
        int assignmentIndex = sequenceList.indexOf(assignment.getId());
        if (assignmentIndex != 0) {
            if (!selectedIds.contains(sequenceList.get(assignmentIndex - 1))) {
                Collections.swap(sequenceList, assignmentIndex, assignmentIndex - 1);
            }
        }
    }

    /**
     * Moves the selected assignment to the top of the list by swapping it with the
     * previous assignment over and over.
     * @param assignment the assignment to move
     */
    public void moveAssignmentTop(Resequencable assignment) {
        int assignmentIndex = sequenceList.indexOf(assignment.getId());
        while (assignmentIndex != 0) {
            if (!selectedIds.contains(sequenceList.get(assignmentIndex - 1))) {
                Collections.swap(sequenceList, assignmentIndex, assignmentIndex - 1);
            }
            assignmentIndex -= 1;
        }
    }

    /**
     * Moves the selected assignment down in the list by swapping it with the
     * next assignment.
     * @param assignment the assignment to move
     */
    public void moveAssignmentDown(Resequencable assignment) {
        int assignmentIndex = sequenceList.indexOf(assignment.getId());
        if (assignmentIndex != (sequenceList.size() - 1)) {
            if (!selectedIds.contains(sequenceList.get(assignmentIndex + 1))) {
                Collections.swap(sequenceList, assignmentIndex, assignmentIndex + 1);
            }
        }
    }

    /**
     * Moves the selected assignment to the bottom of the list by swapping it with the
     * next assignment over and over.
     * @param assignment the assignment to move
     */
    public void moveAssignmentBottom(Resequencable assignment) {
        int assignmentIndex = sequenceList.indexOf(assignment.getId());
        while (assignmentIndex != (sequenceList.size() - 1)) {
            if (!selectedIds.contains(sequenceList.get(assignmentIndex + 1))) {
                Collections.swap(sequenceList, assignmentIndex, assignmentIndex + 1);
            }
            assignmentIndex += 1;
        }
    }

    /**
     * Applies the sequence map to the assignments, updating the database.
     * @return SUCCESS after execution
     * @throws BusinessRuleException if the resequence fails due to business logic
     * @throws DataAccessException if there is an issue accessing or updating the data
     */
    @SuppressWarnings("unchecked")
    public String resequenceApply() throws DataAccessException, BusinessRuleException {
        if (getSavedEntityKey() != null) {
            sequenceList = (ArrayList<Long>) getEntityFromSession(getSavedEntityKey());
            executeResequence(sequenceList, getRegionId());
        }
        setJsonMessage(getText(new UserMessage(
            "assignment.resequence.success.message")),
            ERROR_SUCCESS);
        return SUCCESS;
    }


    /**
     * Moves the selected assignments in the sequence map depending on the move direction.
     * @return SUCCESS
     * @throws DataAccessException if there is an issue accessing or updating the data
     */

    public String resequenceMove() throws DataAccessException {
        updateAssignmentOrderList();
        // Build the selectedIds array list for convenience
        for (int i = 0; i < getIds().length; i++) {
            selectedIds.add(getIds()[i]);
        }
        DataObject obj;
        RemovableDataProvider rdm = (RemovableDataProvider) getManager();
        // if this is a move upward, we want to go through the
        // list from the top down, otherwise, vice versa
        switch (moveDirection) {
        case MOVE_UP:
        case MOVE_TO_TOP:
            for (int i = 0; i < sequenceList.size(); i++) {
                for (int j = 0; j < selectedIds.size(); j++) {
                    if (selectedIds.get(j).longValue() == sequenceList.get(i).longValue()) {
                        obj = rdm.getDataObject(selectedIds.get(j));
                        if (moveDirection == MOVE_UP) {
                            moveAssignmentUp((Resequencable) obj);
                        } else {
                            moveAssignmentTop((Resequencable) obj);
                        }
                    }
                }
            }
            break;
        default:
            for (int i = sequenceList.size() - 1; i >= 0; i--) {
                for (int j = 0; j < selectedIds.size(); j++) {
                    if (selectedIds.get(j).longValue() == sequenceList.get(i).longValue()) {
                        obj = rdm.getDataObject(selectedIds.get(j));
                        if (moveDirection == MOVE_DOWN) {
                            moveAssignmentDown((Resequencable) obj);
                        } else {
                            moveAssignmentBottom((Resequencable) obj);
                        }
                    }
                }
            }
            break;
        }
        getSession().setAttribute(getSavedEntityKey(), sequenceList);
        return SUCCESS;
    }

    /**
     * Checks for assignments that have been moved, but the changes have not been applied.
     * @return SUCCESS if there are no outstanding changes, INPUT otherwise
     * @throws DataAccessException on any database failure.
     */
    @SuppressWarnings("unchecked")
    public String checkForOutstandingChanges() throws DataAccessException {
        try {
            if (getSavedEntityKey() != null) {
                List<DataObject> assignments = getAssignments(getRegionId());
                sequenceList = (ArrayList<Long>) getEntityFromSession(getSavedEntityKey());
                ArrayList<Long> sequenceNumberList = new ArrayList<Long>();

                // Update the resequenceList and create the sequenceNumber list
                ArrayList<Long> ids = new ArrayList<Long>();
                Iterator assignmentIter = assignments.iterator();
                while (assignmentIter.hasNext()) {
                    Resequencable assignment = (Resequencable) assignmentIter.next();
                    ids.add(assignment.getId());
                    sequenceNumberList.add(assignment.getSequenceNumber());
                }
                for (int i = 0; i < sequenceList.size(); i++) {
                    if (!ids.contains(sequenceList.get(i))) {
                        sequenceList.remove(i);
                    }
                }

                // Loop through the list of assignments, if the current sequence number
                // doesn't match the new sequence number, then send back SUCCESS to display
                // a confirmation box
                for (int i = 0; i < sequenceList.size(); i++) {
                    Long id = sequenceList.get(i);
                    assignmentIter = assignments.iterator();
                    while (assignmentIter.hasNext()) {
                        Resequencable assignment = (Resequencable) assignmentIter.next();
                        if (assignment.getId().longValue() == id.longValue()) {
                            if (assignment.getSequenceNumber().longValue()
                                != sequenceNumberList.get(i).longValue()) {
                                setJsonMessage("list.action", ERROR);
                                return SUCCESS;
                            }
                        }
                    }
                }
            }
        } catch (DataAccessException dae) {
            log.warn("Error retrieving assignment list to check for outstanding changes."
                + "  Outstanding changes to sequence numbers will be lost.");
        }
        setJsonMessage("list.action", ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Recalculates the assignment order list in case the status of an assignment changed.
     */
    @SuppressWarnings("unchecked")
    private void updateAssignmentOrderList() {
        try {
            if (getSavedEntityKey() != null) {
                sequenceList = (ArrayList<Long>) getEntityFromSession(getSavedEntityKey());
                List<DataObject> assignments = getAssignments(getRegionId());
                // create list of just the ids
                ArrayList<Long> ids = new ArrayList<Long>();
                Iterator assignmentIter = assignments.iterator();
                while (assignmentIter.hasNext()) {
                    Resequencable assignment = (Resequencable) assignmentIter.next();
                    ids.add(assignment.getId());
                }
                for (int i = 0; i < sequenceList.size(); i++) {
                    if (!ids.contains(sequenceList.get(i))) {
                        sequenceList.remove(i);
                    }
                }
            }
        } catch (DataAccessException dae) {
            log.warn("Error retrieving list of assignments.  "
                + "No assignments will be available for resequence");
        }
        getSession().setAttribute(getSavedEntityKey(), sequenceList);
    }

    /**
     * Called when the page is initially displayed, preserves the initial order
     * of the assignments.
     */

    private void storeInitialAssignmentOrder() {
        sequenceList.clear();
        try {
            List<DataObject> assignments = getAssignments(getRegionId());
            // store the current sort order
            for (int i = 0; i < assignments.size(); i++) {
                Resequencable assignment = (Resequencable) assignments.get(i);
                sequenceList.add(assignment.getId());
            }
        } catch (DataAccessException dae) {
            log.warn("Error retrieving initial list of assignments.  "
                + "No assignments will be available for resequence");
        }
        saveEntityInSession(sequenceList);
    }

    /**
     * Getter for the resequenceAssignmentColumns property.
     *
     * @return List of Column value of the property.
     */
    public List<Column> getResequenceAssignmentColumns() {
        return this.resequenceAssignmentColumns;
    }

    /**
     * Action for the resequence view page. Initializes the table columns
     * after updating list of resequence entities.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String resequenceList() throws Exception {
        storeInitialAssignmentOrder();
        View resequenceAssignmentView = getUserPreferencesManager()
            .getView(getResequenceViewId());
        this.resequenceAssignmentColumns = this.getUserPreferencesManager().getColumns(
            resequenceAssignmentView, getCurrentUser());
        if (getUserSelection() != null && getResequenceViewId() != null) {
            getUserSelection().remove(getResequenceViewId().toString());
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "resequence";
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getRegionManager();
    }

    /**
     * @return the GUI View ID
     */
    public abstract Long getResequenceViewId();

    /**
     * @param forRegionId the region ID for the assignments
     * @return the list of Assignments
     * @throws DataAccessException on database failure
     */
    public abstract List<DataObject> getAssignments(Long forRegionId) throws DataAccessException;

    /**
     * @param resequenceList the list of assignment IDs for resequencing
     * @param forRegionId the ID of region associated with the assignments
     * @throws DataAccessException on any database failure
     * @throws BusinessRuleException on failure due to business rule violation.
     */
    public abstract void executeResequence(ArrayList<Long> resequenceList, Long forRegionId)
        throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 