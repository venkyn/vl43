/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.SafetyCheckType;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;
import com.vocollect.voicelink.core.service.VehicleTypeManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author mraj
 * 
 */
@SuppressWarnings("serial")
public class VehicleSafetyCheckActionRoot extends DataProviderAction implements
    Preparable {

    public static final Logger log = new Logger(
        VehicleSafetyCheckActionRoot.class);

    private static final long SAFETY_CHECK_VIEW_ID = -1227;

    private VehicleSafetyCheckManager vehicleSafetyCheckManager;

    private VehicleTypeManager vehicleTypeManager;

    private List<Column> safetyCheckColumns;

    private Long safetyCheckId;
    
    private Long typeId;

    private VehicleSafetyCheck safetyCheck;

    /**
     * @return the safetyCheckId
     */
    public Long getSafetyCheckId() {
        return safetyCheckId;
    }

    /**
     * @param safetyCheckId the safetyCheckId to set
     */
    public void setSafetyCheckId(Long safetyCheckId) {
        this.safetyCheckId = safetyCheckId;
    }

    /**
     * Getter for the vehicleSafetyCheckManager property.
     * @return VehicleSafetyCheckManager value of the property
     */
    public VehicleSafetyCheckManager getVehicleSafetyCheckManager() {
        return vehicleSafetyCheckManager;
    }

    /**
     * Setter for the vehicleSafetyCheckManager property.
     * @param vehicleSafetyCheckManager the new vehicleSafetyCheckManager value
     */
    public void setVehicleSafetyCheckManager(VehicleSafetyCheckManager vehicleSafetyCheckManager) {
        this.vehicleSafetyCheckManager = vehicleSafetyCheckManager;
    }

    /**
     * Getter for the vehicleTypeManager property.
     * @return VehicleTypeManager value of the property
     */
    public VehicleTypeManager getVehicleTypeManager() {
        return vehicleTypeManager;
    }

    /**
     * Setter for the vehicleTypeManager property.
     * @param vehicleTypeManager the new vehicleTypeManager value
     */
    public void setVehicleTypeManager(VehicleTypeManager vehicleTypeManager) {
        this.vehicleTypeManager = vehicleTypeManager;
    }

    /**
     * Getter for the safetyCheckColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getSafetyCheckColumns() {
        return safetyCheckColumns;
    }

    /**
     * Setter for the safetyCheckColumns property.
     * @param safetyCheckColumns the new safetyCheckColumns value
     */
    public void setSafetyCheckColumns(List<Column> safetyCheckColumns) {
        this.safetyCheckColumns = safetyCheckColumns;
    }

    /**
     * Getter for the vehicleSafetyCheck property.
     * @return VehicleSafetyCheck value of the property
     */
    public VehicleSafetyCheck getSafetyCheck() {
        return safetyCheck;
    }

    /**
     * Setter for the vehicleSafetyCheck property.
     * @param safetyCheck the new vehicleSafetyCheck value
     */
    public void setSafetyCheck(VehicleSafetyCheck safetyCheck) {
        this.safetyCheck = safetyCheck;
    }

    
    
    /**
     * Getter for the typeId property.
     * @return Long value of the property
     */
    public Long getTypeId() {
        return typeId;
    }

    
    /**
     * Setter for the typeId property.
     * @param typeId the new typeId value
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        if (getSafetyCheckId() != null) {
            // We have an ID, but not a region object yet.
            if (log.isDebugEnabled()) {
                log.debug("safetyCheckId is " + this.getSafetyCheckId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the safety check is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved safety check from session");
                }
                this.safetyCheck = (VehicleSafetyCheck) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting safety check from database");
                }
                this.safetyCheck = this.getVehicleSafetyCheckManager().get(
                    this.getSafetyCheckId());
                saveEntityInSession(this.safetyCheck);
            }
            if (log.isDebugEnabled()) {
                log.debug("Safety Check version is: "
                    + this.safetyCheck.getVersion());
            }
        } else if (this.safetyCheck == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new Safety Check object");
            }

            this.safetyCheck = new VehicleSafetyCheck();
            
            //If we are invoking create safety check from vehicle type list page 
            if (typeId != null) {
                VehicleType vehicleType = getVehicleTypeManager().get(typeId);
                this.safetyCheck.setVehicleType(vehicleType);
            }
        }
    }

    /**
     * Delete the safety check identified by the <code>safetyCheckId</code>
     * member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified vehicle wasn't found (with different messages to
     *         the end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentSafetyCheck() throws Exception {

        VehicleSafetyCheck safetyCheckToDelete = null;

        try {
            safetyCheckToDelete = vehicleSafetyCheckManager
                .get(this.safetyCheckId);
            vehicleSafetyCheckManager.delete(this.safetyCheckId);

            addSessionActionMessage(new UserMessage(
                "vehicle.delete.message.success",
                safetyCheckToDelete.getDescription()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete vehicle: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Function for validating if a safety check is editable or not.
     *
     * @return String defining next target to forward to.
     */
    public String checkIfEditable() {

        try {
            VehicleSafetyCheck check = vehicleSafetyCheckManager.get(getIds()[0]);
            validateIfEditable(check);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating vehicle safety check for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?safetyCheckId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }
    
    /**
     * Function to check for editing validation business logic.
     *
     * @param check Vehicle safety check to be validated
     * @throws BusinessRuleException if the business rule is violated.
     */
    protected void validateIfEditable(VehicleSafetyCheck check)
        throws BusinessRuleException {
        List<Vehicle> vehicles = check.getVehicleType().getVehicles();
        for (Vehicle vehicle : vehicles) {
            if (!vehicle.getOperators().isEmpty()) {
                throw new BusinessRuleException(
                    CoreErrorCode.SAFETYCHECK_EXISTS_FOR_VEHICLETYPE,
                    new UserMessage(
                        "vsc.safetyCheck.edit.error.vehicleType.inUse"));
            }
        }
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.vehicleSafetyCheckManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "safetyCheck";
    }

    /**
     * @return SUCCESS
     * @throws Exception
     */
    public String list() throws Exception {
        View checkResponseView = getUserPreferencesManager().getView(
            SAFETY_CHECK_VIEW_ID);
        this.safetyCheckColumns = this.getUserPreferencesManager().getColumns(
            checkResponseView, getCurrentUser());
        return SUCCESS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(SAFETY_CHECK_VIEW_ID);
        return viewIds;
    }

    /**
     * @return the safety check View ID
     */
    public static long getSafetyChecksViewId() {
        return SAFETY_CHECK_VIEW_ID;
    }

    /**
     * Create or update the safety checks specified by the <code>user</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = this.safetyCheck.isNew();

        try {
            this.getVehicleSafetyCheckManager().save(safetyCheck);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified safety check "
                + this.safetyCheck.getDescription());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.safetyCheck"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                VehicleSafetyCheck modifiedVehicle = getVehicleSafetyCheckManager()
                    .get(getSafetyCheckId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.safetyCheck.setVersion(modifiedVehicle.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedVehicle);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.safetyCheck"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("vsc.safetyCheck."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/vscsafetycheck/view.action?safetyCheckId="
                + this.safetyCheck.getId()),
            this.safetyCheck.getSpokenDescription()));

        return SUCCESS;
    }

    /**
     * Returns a Map of vehicle type description and ids
     * @return description and id map
     * @throws DataAccessException
     */
    public Map<String, Long> getVehicleTypeMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(
            StringUtil.getPrimaryCollator());

        List<VehicleType> vehicleTypes = getVehicleTypeManager()
            .listAllVehicleTypesByNumber();
        for (VehicleType vehicleType : vehicleTypes) {
            map.put(getDataTranslation(vehicleType.getDescription()),
                vehicleType.getId());
        }

        return map;
    }

    /**
     * @return the vehicle type id
     */
    public Long getVehicleType() {
        return this.safetyCheck.getVehicleType().getId();
    }

    /**
     * @param vehicleTypeId the vehicle type id
     */
    public void setVehicleType(Long vehicleTypeId) {
        try {
            VehicleType vehicleType = getVehicleTypeManager().get(vehicleTypeId);
            this.safetyCheck.setVehicleType(vehicleType);
        } catch (DataAccessException e) {
            addFieldError("safetyCheck.vehicleType", e.getUserMessage());
        }
    }

    /**
     * @return String value of the property
     */
    public Map<Integer, String> getResponseTypeMap() {
        Map<Integer, String> responseTypeMap = new HashMap<Integer, String>();

        responseTypeMap.put(SafetyCheckType.STOPONNO.toValue(),
            getText(SafetyCheckType.STOPONNO.getResourceKey()));

        responseTypeMap.put(SafetyCheckType.CONTINUEONNO.toValue(),
            getText(SafetyCheckType.CONTINUEONNO.getResourceKey()));

        responseTypeMap.put(SafetyCheckType.NUMERIC.toValue(),
            getText(SafetyCheckType.NUMERIC.getResourceKey()));
        
        return responseTypeMap;
    }

    /**
     * @return String value of the property
     */
    public Integer getResponseType() {
        return this.safetyCheck.getResponseType().toValue();
    }

    /**
     * @param responseType the response type value
     */
    public void setResponseType(Integer responseType) {

        if (responseType == SafetyCheckType.STOPONNO.toValue()) {
            this.safetyCheck.setResponseType(SafetyCheckType.STOPONNO);
        } else if (responseType == SafetyCheckType.CONTINUEONNO.toValue()) {
            this.safetyCheck.setResponseType(SafetyCheckType.CONTINUEONNO);
        } else if (responseType == SafetyCheckType.NUMERIC.toValue()) {
            this.safetyCheck.setResponseType(SafetyCheckType.NUMERIC);
        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 