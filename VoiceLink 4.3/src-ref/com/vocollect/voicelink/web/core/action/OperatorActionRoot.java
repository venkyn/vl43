/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.epp.web.util.JSONResponseBuilder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.OperatorTeamManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentSummary;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the Struts action class that handles operations on
 * <code>Operator</code> objects.
 *
 * @author ddoubleday
 */
/**
 * 
 *
 * @author mnichols
 */
public class OperatorActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

    private static final Logger log = new Logger(OperatorActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1005;

    private static final long ASSIGN_VIEW_ID = -1040;

    private static final int NUMBER_DECIMALS = 4;


    // The list of columns for the operators table.
    private List<Column> columns;

    // The Operator management service.
    private OperatorManager operatorManager = null;

    // The SignOffManager management service.
    private SignOffManager signOffManager = null;

    // The RegionManager management service.
    private RegionManager regionManager = null;

    // The AssignmentManager management service.
    private AssignmentManager assignmentManager = null;

    // The WorkGroupManager management service.
    private WorkgroupManager workgroupManager = null;
    
    // The OperatorTeamManager management service.
    private OperatorTeamManager operatorTeamManager = null;

    // The Operator object, which will either be newly created, or retrieved
    // via the OperatorId.
    private Operator operator;

    // The ID of the Operator.
    private Long operatorId;

    // The workgroup to assign the operators to
    private Workgroup newWorkgroup;

    // The region chosen for assigning operator
    private Long selectedRegion;

    private Double estimatedTime;

    private String assignedOperatorText;

    private String availableOperatorText;

    private String estimatedTimeText;

    private String selectedRegionName;

    // used by sign off ajax call
    private String signOffDate;

    /**
     * Getter for the operator property.
     * @return operator value of the property
     */
    public Operator getOperator() {
        return this.operator;
    }

    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * Getter for the operatorId property.
     * @return Long value of the property
     */
    public Long getOperatorId() {
        return this.operatorId;
    }

    /**
     * Setter for the operatorId property.
     * @param operatorId the new operatorId value
     */
    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * Getter for the operatorManager property.
     * @return operatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param manager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager manager) {
        this.operatorManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getOperatorColumns() {
        return this.columns;
    }

    /**
     * Action for the operators view page. Initializes the operators table
     * columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View operatorView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            operatorView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        // If this is the assign page, setup the query data
        if (getViewId().longValue() == ASSIGN_VIEW_ID) {
            super.getTableData("", new Object[] { selectedRegion });
        } else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * Create or update the operator specified by the <code>operator</code>
     * member of this class.
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        boolean isNew = operator.isNew();
        preProcessSave();
        if (log.isDebugEnabled()) {
            log.debug("Saving operator");
        }
        try {
            operatorManager.save(this.operator);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (EntityNotFoundException e) {
            log.warn("Operator ID " + this.operator.getOperatorIdentifier()
                + " not found, possibly a concurrency");

            addSessionActionMessage(new UserMessage(
                "operator.edit.error.operatorNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified operator "
                + this.operator.getOperatorIdentifier());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Operator"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Operator modifiedOperator = getOperatorManager().get(
                    getOperatorId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.operator.setVersion(modifiedOperator.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedOperator);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization("entity.Operator"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage(
            "operator." + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/operator/view.action?operatorId="
                + this.operator.getId()), this.operator.getOperatorIdentifier()));

        return SUCCESS;
    }


    /**
     * Initializes the summary data for the region and
     * forward to the assign operators view page.
     * @return success
     * @throws Exception general exception
     */
    public String showAssignOperators() throws Exception {

       View operatorView = getUserPreferencesManager().getView(ASSIGN_VIEW_ID);
       this.columns = this.getUserPreferencesManager().getColumns(
            operatorView, getCurrentUser());

       initSummaryDataForAssignOperator();

       return SUCCESS;
    }

    /**
     * Set the summary data for the assign operators view page.
     * @throws DataAccessException and exception which occurs while accessing the data.
     */
    private void initSummaryDataForAssignOperator()
    throws DataAccessException {
           List<Operator> currentlyInRegion = operatorManager
                           .listCurrentlyInRegion(this.selectedRegion);
           List<Operator> availableToWorkInRegion = operatorManager
                           .listAvailableToWorkInRegion(getRegionId());

           Integer assignedToRegion = getAssignedFromAvailable(
                                           availableToWorkInRegion);
           String estimatedTimeString = getEstimatedCompletionTime(
                                           currentlyInRegion.size(),
                                           assignedToRegion);

           assignedOperatorText = getText(new UserMessage(
                               "assignoperators.summary.currentlyWorking",
                               currentlyInRegion.size()));
           availableOperatorText = getText(new UserMessage(
                               "assignoperators.summary.assigned",
                               assignedToRegion.toString()));
           if (estimatedTime == 0D) {
               estimatedTimeText = getText(new UserMessage(
                   "assignoperators.summary.estimatedTimeCanNotCalculate"));
           } else {
               estimatedTimeText = getText(new UserMessage(
                   "assignoperators.summary.estimatedTime",
                   estimatedTimeString));
           }

    }

    /**
     * The operators are assigned to the specified region.
     * @return success
     * @throws Exception general exception
     */
    public String assignOperators() throws Exception {

        View operatorView = getUserPreferencesManager().getView(ASSIGN_VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
             operatorView, getCurrentUser());

        if (null != this.getIds()[0]) {

            for (int i = 0; i < this.getIds().length; i++) {
                operator = operatorManager.get(this.getIds()[i]);

                if (null != operator.getAssignedRegion()) {
                    String assgnedRegionId = operator.getAssignedRegion()
                                                .getId().toString();

                    if (!this.selectedRegion.toString()
                        .equals(assgnedRegionId)) {
                        operator.setAssignedRegion(regionManager
                            .get(this.selectedRegion));
                        operatorManager.save(operator);
                    }
                 } else {
                     operator.setAssignedRegion(regionManager
                         .get(this.selectedRegion));
                     operatorManager.save(operator);
                 }
            }

        }

        initSummaryDataForAssignOperator();
        return SUCCESS;
    }

    /**
     * Unassigns the operator from a specified region.
     * @return success
     * @throws Exception general exception
     */
    public String unassignOperators() throws Exception {

        View operatorView = getUserPreferencesManager().getView(ASSIGN_VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
             operatorView, getCurrentUser());

        if (null != this.getIds()[0]) {

            for (int i = 0; i < this.getIds().length; i++) {
                operator = operatorManager.get(this.getIds()[i]);

                if (null != operator.getAssignedRegion()) {
                        operator.setAssignedRegion(null);
                        operatorManager.save(operator);
                 }
            }

        }

        initSummaryDataForAssignOperator();
        return SUCCESS;
    }

    /**
     * Calculates the estimated time taken to complete the work.
     * @param currentlyInRegion operators currently working in the region.
     * @param assignedToRegion available to work in region
     * @return the estimated time
     * @throws DataAccessException any exception thrown during data access
     */
    private String getEstimatedCompletionTime(Integer currentlyInRegion,
                      Integer assignedToRegion) throws DataAccessException {

           Region region = regionManager.get(this.selectedRegion);
           this.selectedRegionName = region.getName();
           Integer goalRate = region.getGoalRate();
           Integer totalQuantity = getTotalItemQuantityForRegion();

           // reset estimatedTime
           estimatedTime = 0D;

           // Protect divide by zero
           if (!((goalRate == 0)
               || ((currentlyInRegion + assignedToRegion) == 0)
               || (totalQuantity == 0))) {
               estimatedTime = ((double) totalQuantity / (double) goalRate)
                                   / (currentlyInRegion + assignedToRegion);
           }

           // Do not allow negative estimated completion time.
           if (estimatedTime < 0D) {
               estimatedTime = 0D;
           }

               NumberFormat nf = NumberFormat.getInstance(getRequest().getLocale());
               try {
               // Trim the estimate to three values of decimal place

                    String estTime =  estimatedTime.toString().substring(0,
                              estimatedTime.toString().indexOf('.') + NUMBER_DECIMALS);

                   double d = Double.parseDouble(estTime);
                   return  nf.format(d);

               } catch (StringIndexOutOfBoundsException e) {
                    return nf.format(Double.parseDouble(estimatedTime.toString()));
               }

     }

    /**
     * Gets the total item quantity for a particular region.
     * @return total item quantity
     * @throws DataAccessException any exception thrown during data access
     */
    private int getTotalItemQuantityForRegion()
    throws DataAccessException {
        List<AssignmentType> assignmentTypes = new ArrayList<AssignmentType>();
        assignmentTypes.add(AssignmentType.Normal);
       List<Assignment> assignments = assignmentManager.listAssignmentsInRegion(
                           this.selectedRegion, assignmentTypes);
          Iterator<Assignment> itr = assignments.iterator();
          int totalQuantity = 0;
           while (itr.hasNext()) {
               AssignmentSummary assignmentSummary = itr.next().getSummaryInfo();
               totalQuantity = totalQuantity + assignmentSummary
                                               .getTotalItemQuantity();
           }
          return totalQuantity;
    }

    /**
     * Sets the summary data while assigning a group of operators to a region.
     * @return success
     * @throws Exception general exception
     */
     public String showAssignSummary() throws Exception {

        String assignSummary = null;

        if (null != this.getIds()[0]) {
            Map<String, String[]> parameters = ActionContext.getContext().getParameters();
            int tobeAssigned = this.getIds().length;

            for (int i = 0; i < this.getIds().length; i++) {
                operator = operatorManager.get(this.getIds()[i]);

                String[] regionId = parameters.get("regionId");

                String assgnedRegionId = "NULL";
                if (null != operator.getAssignedRegion()) {
                   assgnedRegionId = operator.getAssignedRegion()
                                       .getId().toString();
                }

                String currentRegionId = "NULL";
                if (null != operator.getCurrentRegion()) {
                   currentRegionId = operator.getCurrentRegion()
                                       .getId().toString();
                }

                if (regionId[0].equals(assgnedRegionId)
                    || regionId[0].equals(currentRegionId)) {
                   tobeAssigned--;
                }
            }

            List<Operator> currentlyInRegion = operatorManager
                                .listCurrentlyInRegion(this.selectedRegion);
            List<Operator> availableToWorkInRegion = operatorManager
                                .listAvailableToWorkInRegion(getRegionId());

            String estimatedTimeString = getEstimatedCompletionTime(
                currentlyInRegion.size(),
                getAssignedFromAvailable(availableToWorkInRegion)
                + tobeAssigned);

            if (estimatedTime == 0D) {
                assignSummary = getText(new UserMessage(
                    "assignoperators.summary.assignMessage.CannotCalculate",
                    this.getIds().length, tobeAssigned));
            } else {
                assignSummary = getText(new UserMessage(
                                    "assignoperators.summary.assignMessage",
                                    this.getIds().length, tobeAssigned,
                                    estimatedTimeString));
            }
        }
        setJsonMessage(assignSummary , GENERAL_MESSAGE);
        return SUCCESS;
    }

    /**
     * Sets the summary data while unassigning a group of operators from a region.
     * @return success
     * @throws Exception general exception
     */
     public String showUnassignSummary() throws Exception {

         String unassignSummary = null;

         if (null != this.getIds()[0]) {
             Map<String, String[]> parameters = ActionContext.getContext().getParameters();
             int tobeUnassigned = 0;

             for (int i = 0; i < this.getIds().length; i++) {
                 operator = operatorManager.get(this.getIds()[i]);

                 String[] regionId = parameters.get("regionId");

                 String assgnedRegionId = "NULL";
                 if (null != operator.getAssignedRegion()) {
                    assgnedRegionId = operator.getAssignedRegion()
                                        .getId().toString();
                 }

                 String currentRegionId = "NULL";
                 if (null != operator.getCurrentRegion()) {
                    currentRegionId = operator.getCurrentRegion()
                                        .getId().toString();
                 }

                 if (regionId[0].equals(assgnedRegionId)
                     && !regionId[0].equals(currentRegionId)) {
                    tobeUnassigned++;
                 }
             }

             List<Operator> currentlyInRegion = operatorManager
                                 .listCurrentlyInRegion(this.selectedRegion);
             List<Operator> availableToWorkInRegion = operatorManager
                                 .listAvailableToWorkInRegion(getRegionId());

             String estimatedTimeString = getEstimatedCompletionTime(
                 currentlyInRegion.size(),
                 getAssignedFromAvailable(availableToWorkInRegion)
                 - tobeUnassigned);

             if (estimatedTime == 0D) {
                 unassignSummary = getText(new UserMessage(
                     "assignoperators.summary.unassignMessage.CannotCalculate",
                     this.getIds().length, tobeUnassigned));
             } else {
                 unassignSummary = getText(new UserMessage(
                                 "assignoperators.summary.unassignMessage",
                                 this.getIds().length, tobeUnassigned,
                                 estimatedTimeString));
             }
         }

             setJsonMessage(unassignSummary , GENERAL_MESSAGE);
             return SUCCESS;
     }


   /**
     * Gets the count of operators assigned to the region from the total count
     * of operators available to work in the region.
     * @param availableToWorkInRegion total operators available to work in region
     * @return operators assigned to the region
     */
   private Integer getAssignedFromAvailable(List<Operator> availableToWorkInRegion) {
        Integer count = 0;
        // From the available to work in region only the operators
        // assigned to this region are filtered out

        for (Operator o : availableToWorkInRegion) {

            if (o.getAssignedRegion() != null) {
             String assgnedRegionId = o.getAssignedRegion().getId().toString();

             if (this.selectedRegion.toString().equals(assgnedRegionId)) {
                count++;
             }
          }
        }
        return count;
      }

   /**
    * Overriden to take care of scenarios arising due to the
    * addition of assign operator functionality.
    * @return whether or not this is a text action.
    */
   @Override
   public boolean isTextAction() {
       if (getActionName().toLowerCase().endsWith("assignOperators".toLowerCase())) {
           return true;
       } else {
           return super.isTextAction();
       }
   }

   /**
    * @return the name of this action, without the action piece.
    */
    private String getActionName() {
      // TODO: I think this can be done directly with the Struts
      // ActionContext. -- ddoubleday.
      String fullURI = getRequest().getRequestURI();
      int startIndex = fullURI.lastIndexOf('/') + 1;
      if (startIndex == -1) {
          return null;
      }

      int endIndex;
      if (fullURI.contains("!input")) {
          endIndex = fullURI.indexOf("!input", startIndex);
      } else {
          endIndex = fullURI.indexOf(".action", startIndex);
      }
      if (endIndex == -1) {
          return null;
      }

      return fullURI.substring(startIndex, endIndex);
    }

    /**
     * Deletes the currently viewed operator.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current user
     */
    public String deleteCurrentOperator() throws Exception {

        Operator operatorToDelete = null;

        try {
            operatorToDelete = operatorManager.get(this.operatorId);
            operatorManager.delete(this.operatorId);

            addSessionActionMessage(new UserMessage(
                "operator.delete.message.success", operatorToDelete
                    .getOperatorIdentifier()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete operator: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Signs off operator(s).
     * @return the control flow target name
     * @throws DataAccessException if any error occurs signing of an
     *             operator
     */
    public String signOff() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "signOff";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                getSignOffManager().executeSignOffOperatorFromUI(
                    (Operator) obj, new Date());
                return true;
            }
        });
    }

    /**
     * Action to return the sign off time based on the last labor record in the system.
     * @return JSON code
     */
    public String getLastLaborRecord() {
        try {
            Operator operatorToSignoff = operatorManager.get(this.operatorId);
            Date date = getSignOffManager().getEarliestSignOffTime(operatorToSignoff);

            DisplayUtilities du = getDisplayUtilities();
            String signOffTime;
            try {
                signOffTime = du.formatTimeWithTimeZone(date, null);
            } catch (DataAccessException e) {
                log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
                signOffTime = date.toString();
            }

            JSONObject json = new JSONObject();

            try {
                json.put(JSONResponseBuilder.ERROR_CODE, JSONResponseBuilder.ERROR_SUCCESS);
                json.put("dateValue", signOffTime);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setJsonMessage(json.toString());

        } catch (VocollectException e) {
            setJsonMessage(JSONResponseBuilder.makeErrorResponse(getText(e.getUserMessage())));
        }

        return SUCCESS;
    }




    /**
     * getter for sign off date.
     * @return the sign off date
     */
    public String getSignOffDate() {
        return signOffDate;
    }


    /**
     * setter for sign off date.
     * @param signOffDate the sign off date
     */
    public void setSignOffDate(String signOffDate) {
        this.signOffDate = signOffDate;
    }

    /**
     * AJAX call to update the last labor record for an operator.
     * @return JSON code
     * @throws DataAccessException on database failure
     */
    public String updateLastLaborRecord() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {
            public String getActionPrefix() {
                return "signOff";
            }

            public boolean execute(DataObject obj)
                    throws VocollectException, DataAccessException, BusinessRuleException {
                if (!getSignOffDate().equals("current")) {
                    Date date = getSignOffManager().getEarliestSignOffTime((Operator)obj);
                    getSignOffManager().executeSignOffOperatorFromUI((Operator) obj, date);
                }else{
                    Date date = new Date();
                    getSignOffManager().executeSignOffOperatorFromUI((Operator) obj, date);
                }
                
                return true;
            }

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }
        });

    }

    /**
     * AJAX call to assign the operator to a workgroup.
     * @return JSON code
     * @throws DataAccessException on database failure
     */
    public String assignToWorkgroup() throws DataAccessException {
        final Workgroup newWorkgroupObject = this.newWorkgroup;
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "assignWorkgroups";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                operatorManager.executeChangeWorkgroup(((Operator) obj), newWorkgroupObject);
                return true;
            }
        });
    }


    /**
     * This method sets up the <code>operator</code> object by retrieving it
     * from the database when a operatorId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.operatorId != null) {
            if (log.isDebugEnabled()) {
                log.debug("Operator id is " + this.operatorId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {

                if (log.isDebugEnabled()) {
                    log.debug("Getting saved operator from session");
                }
                this.operator = (Operator) getEntityFromSession(getSavedEntityKey());
                this.operator.setOperatorTeams(null);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting operator from database");
                }
                this.operator = this.operatorManager.get(this.operatorId);

                saveEntityInSession(this.operator);
            }
            if (log.isDebugEnabled()) {
                log.debug("Operator version is: " + this.operator.getVersion());
            }
        }
    }

    /**
     * Get the <code>Workgroups</code>s for the current Site for display.
     * @return the Map of defined <code>Workgroups</code>s, where the map value is
     *         the ID of the role, and the map key is the (possibly localized)
     *         workgroup name. The name is used as the key so the data
     *         will be sorted by name.
     * @throws DataAccessException on failure to retrieve.
     */
    public Map<String, Long> getWorkgroupMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(StringUtil
            .getPrimaryCollator());
        List<Workgroup> workgroups = getWorkgroupManager().getWorkgroupPerSite();
        for (Workgroup wg : workgroups) {
            map.put(getDataTranslation(wg.getGroupName()), wg.getId());
        }
        return map;
    }


    /**
     *
     * @return "INPUT" to force xwork to load the appropriate FTL file
     */
    public String showChangeWorkgroupDialog() {
        return INPUT;
    }

    /**
     * Setter method for Work Group.
     * @param workGroupId The Work Group Id to find the work group
     */
    public void setWorkgroup(Long workGroupId) {
        try {
            newWorkgroup = this.workgroupManager.get(workGroupId);
        } catch (DataAccessException e) {
            log.warn("No workgroup found in database");
        }
    }

    /**
     * Do pre-processing before the call to save occurs.
     */
    private void preProcessSave() {
        this.operator.setWorkgroup(newWorkgroup);
    }

    /**
     *
     * @return the workgroup ID of this operator's current workgroup.
     */
    public Long getWorkgroup() {
        if (operator == null || operator.getWorkgroup() == null) {
            return null;
        } else {
            return operator.getWorkgroup().getId();
        }
    }

    /**
     * Only calls this class's <code>setWorkgroup</code> method.
     * @param workgroupId The Id to set the workgroup to.
     * @see com.vocollect.voicelink.web.core.action.OperatorAction#setWorkgroup(Long)
     */
    public void setDefaultWorkgroup(Long workgroupId) {
        setWorkgroup(workgroupId);
    }

    /**
     *
     * @return the id of the default workgroup.
     */
    public Long getDefaultWorkgroup() {
        Workgroup defGroup;
        try {
            defGroup = this.getWorkgroupManager().findDefaultWorkgroup();
        } catch (DataAccessException e) {
            log.warn("could not retrieve default workgroup", e);
            return null;
        }
        if (defGroup == null) {
            return null;
        } else {
            return defGroup.getId();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getOperatorManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "operator";
    }

    /**
     *
     * @return Returns Sign Off Manager
     */
    public SignOffManager getSignOffManager() {
        return signOffManager;
    }

    /**
     * Sets Sign Off Manager.
     * @param signOffManager Object of an instance of SignOffManager
     */
    public void setSignOffManager(SignOffManager signOffManager) {
        this.signOffManager = signOffManager;
    }

    /**
     *
     * @return Returns Work Group Manager
     */
    public WorkgroupManager getWorkgroupManager() {
        return workgroupManager;
    }

    /**
     * Sets Work Group Manager.
     * @param manager Instance of Work Group Manager class
     */
    public void setWorkgroupManager(WorkgroupManager manager) {
        this.workgroupManager = manager;
    }

    /**
     *
     * @return Returns the modified Work Group Name
     */
    public String getModifiedWorkgroup() {
        Workgroup wg = ((Operator) getModifiedEntity()).getWorkgroup();
        if (wg != null) {
            return ((Operator) getModifiedEntity()).getWorkgroup().getGroupName();
        } else {
            return null;
        }
    }

    /**
     * Getter for the OPERATOR_VIEW_ID property.
     * @return long value of the property
     */
    public static long getOperatorViewId() {
        return VIEW_ID;
    }

    /**
     * Getter for the ASSIGN_VIEW_ID property.
     * @return long value of the property
     */
    public static long getAssignOperatorViewId() {
        return ASSIGN_VIEW_ID;
    }

    /**
     * Getter for the signOnTime, which will return the properly zone formatted time.
     * @return String value of the property
     */
    public String getSignOnTime() {
        String signOnTime = null;

        DisplayUtilities du = getDisplayUtilities();

        try {
            signOnTime = du.formatTimeWithTimeZone(getOperator().getSignOnTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            signOnTime = getOperator().getSignOnTime().toString();
        }

        return signOnTime;
    }

    /**
     * Getter for the signOffTime, which will return the properly zone formatted time.
     * @return String value of the property
     */
    public String getSignOffTime() {
        String signOffTime = null;

        DisplayUtilities du = getDisplayUtilities();

        try {
            signOffTime = du.formatTimeWithTimeZone(getOperator().getSignOffTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            signOffTime = getOperator().getSignOffTime().toString();
        }

        return signOffTime;
    }



    /**
     * @return selected region
     */
    public Long getRegionId() {
        return selectedRegion;
    }

    /**
     * @param regionId regionId
     */
    public void setRegionId(Long regionId) {
        this.selectedRegion = regionId;
    }

    /**
     * @return assignedOperatorText
     */
    public String getAssignedOperatorText() {
        return assignedOperatorText;
    }

    /**
     * @return availableOperatorText
     */
    public String getAvailableOperatorText() {
        return availableOperatorText;
    }

    /**
     * @return estimatedTimeText
     */
    public String getEstimatedTimeText() {
        return estimatedTimeText;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        viewIds.add(ASSIGN_VIEW_ID);
        return viewIds;
    }

    /**
     * @return regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * @param regionManager regionManager
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * @return assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager assignmentManager
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * @return selectedRegionName
     */
    public String getSelectedRegionName() {
        return selectedRegionName;
    }


    /**
     * Getter for the operatorStatus property.
     * This has to be done similar to assignments and reasoncodes status if the
     * the option is enabled in future in edit pages.
     * @return String value of the property
     */
    public String getOperatorStatus() {
           return getText(operator.getStatus().getResourceKey());
     }
    
    
    /**
     * @return - Map of operator team names and ids
     * @throws DataAccessException
     */
    public Map<String, Long> getOperatorTeamMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(StringUtil
            .getPrimaryCollator());
        List<OperatorTeam> operatorTeams = getOperatorTeamManager().listAllOrderByName();
        for (OperatorTeam opTeam : operatorTeams) {
            map.put(opTeam.getName(), opTeam.getId());
        }
        return map;     
    }
    
    /**
     * @return long[] - list of operator team ids
     */
    public long[] getOperatorTeams() {
        long[] operatorTeamIds = new long[operator.getOperatorTeams().size()];
        if (this.operator != null) {
            Iterator<OperatorTeam> opTeamIt = operator.getOperatorTeams().iterator();
            int i = 0;
            while (opTeamIt.hasNext()) {
                operatorTeamIds[i] = opTeamIt.next().getId();
                i++;
            }
        }
        return operatorTeamIds;
    }
    
    
    /**
     * @param operatorTeamIds - list of operator team ids
     * @throws DataAccessException 
     */
    public void setOperatorTeams(long[] operatorTeamIds) throws DataAccessException {
        if (this.operator != null) {
            this.operator.setOperatorTeams(null);
            for (int i = 0; i < operatorTeamIds.length; i++) {
                this.operator.addOperatorTeam(getOperatorTeamManager().get(operatorTeamIds[i]));
            }
        }
    }

    /**
     * Setter for the operatorTeamManager property.
     * @param operatorTeamManager the new operatorTeamManager value
     */
    public void setOperatorTeamManager(OperatorTeamManager operatorTeamManager) {
        this.operatorTeamManager = operatorTeamManager;
        
    }

    /**
     * Getter for the operatorTeamManager property.
     * @return the value of the property
     */
    public OperatorTeamManager getOperatorTeamManager() {
        return this.operatorTeamManager;
        
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 