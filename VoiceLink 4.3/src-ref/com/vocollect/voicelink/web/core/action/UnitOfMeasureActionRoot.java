/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.UnitOfMeasure;
import com.vocollect.voicelink.core.service.UnitOfMeasureManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * @author pkolonay
 *
 */
public class UnitOfMeasureActionRoot extends DataProviderAction implements
        Preparable {

    /**
     * 
     */
    private static final long serialVersionUID = -3148084741653206086L;

    //  logger for ReasonCodeRootAction class
    private static final Logger log = new Logger(UnitOfMeasureActionRoot.class);

    //  The ID of the ReasonCode
    private Long unitOfMeasureId;

    // The UnitOfMeasure object which will either be newly created, or retrieved
    // via the unitOfMeasureId.
    private UnitOfMeasure unitOfMeasure;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1050;

    // The list of columns for the Reason Code table.
    private List<Column> uomColumns;

    // ReasonCodeManager is service for unitOfMeasure
    private UnitOfMeasureManager unitOfMeasureManager;
    
    
    
    /**
     * @return the unitOfMeasureId
     */
    public Long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    /**
     * @param unitOfMeasureId the unitOfMeasureId to set
     */
    public void setUnitOfMeasureId(Long unitOfMeasureId) {
        this.unitOfMeasureId = unitOfMeasureId;
    }

    /**
     * @return the unitOfMeasure
     */
    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * @param unitOfMeasure the unitOfMeasure to set
     */
    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    /**
     * @return the uomColumns
     */
    public List<Column> getUomColumns() {
        return uomColumns;
    }

    /**
     * @param uomColumns the uomColumns to set
     */
    public void setUomColumns(List<Column> uomColumns) {
        this.uomColumns = uomColumns;
    }

    /**
     * @return the unitOfMeasureManager
     */
    public UnitOfMeasureManager getUnitOfMeasureManager() {
        return unitOfMeasureManager;
    }

    /**
     * @param unitOfMeasureManager the unitOfMeasureManager to set
     */
    public void setUnitOfMeasureManager(UnitOfMeasureManager unitOfMeasureManager) {
        this.unitOfMeasureManager = unitOfMeasureManager;
    }

    /**
     * @return the viewId
     */
    public static long getUnitOfMeasureViewId() {
        return VIEW_ID;
    }

    /**
     * Action for the Reason Codes view page. Initializes the reason code table columns
     * after communicating with the Reason Codes for an updated list of Reason Codes.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        log.info(" Getting a list of columns for Unit Of Measure");
        View view = getUserPreferencesManager().getView(VIEW_ID);
        this.uomColumns = this.getUserPreferencesManager().getColumns(
            view, getCurrentUser());
        return SUCCESS;
    }

    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        if (this.unitOfMeasureId != null) {
            // We have an ID, but not a unitOfMeasure object yet.
            if (log.isDebugEnabled()) {
                log.debug("unitOfMeasureId is " + this.unitOfMeasureId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the reason code is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved Unit of Measure from session");
                }
                this.unitOfMeasure = (UnitOfMeasure) getEntityFromSession(getSavedEntityKey());
              } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting reason code from database");
                }
                this.unitOfMeasure = this.unitOfMeasureManager.get(this.unitOfMeasureId);
                saveEntityInSession(this.unitOfMeasure);
            }
            if (log.isDebugEnabled()) {
                log.debug("Unit Of Measure version is: " + this.unitOfMeasure.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new unitOfMeasure object");
            }
            this.unitOfMeasure = new UnitOfMeasure();
        }
    }

    /**
     * Create or update the unitOfMeasure specified by the <code>unitOfMeasure</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.unitOfMeasure.isNew();
        try {

            if (this.unitOfMeasure.getName() == null
                || this.unitOfMeasure.getName().trim().equalsIgnoreCase("")) {
                addFieldError("unitOfMeasure.name", getText("unitofmeasure.create.label.name")
                    + getText("errors.required"));
            }

            if (hasErrors()) {
                return (INPUT);
            }

            //Uniqueness check for number
            Long numberUniquenessId = this.unitOfMeasureManager.getPrimaryDAO()
                .uniquenessByName(this.unitOfMeasure.getName());
            if (numberUniquenessId != null && (isNew || (!isNew
                && numberUniquenessId.longValue() != this.unitOfMeasure.getId().longValue()))) {
                log.warn("Unit Of Measure '" + this.unitOfMeasure.getName()
                    + "' already exists");
                addFieldError("unitOfMeasure.name", new UserMessage(
                    "unitofmeasure.create.error.existing", this.unitOfMeasure.getName()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            unitOfMeasureManager.save(this.unitOfMeasure);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified reason number "
                + this.unitOfMeasure.getId());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.unitOfMeasure"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));

            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                UnitOfMeasure modifiedEntity = getUnitOfMeasureManager().get(getUnitOfMeasureId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.unitOfMeasure.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionErrorMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.unitOfMeasure.getName())));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "unitofmeasure." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/unitOfMeasure/view.action?unitOfMeasureId=" + unitOfMeasure.getId()),
                this.unitOfMeasure.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Delete the reason code identified by the <code>unitOfMeasureId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentUnitOfMeasure() throws Exception {

        UnitOfMeasure unitOfMeasureToDelete = null;

        try {
            unitOfMeasureToDelete = unitOfMeasureManager.get(this.unitOfMeasureId);
            unitOfMeasureManager.delete(this.unitOfMeasureId);

            addSessionActionMessage(new UserMessage(
                "unitofmeasure.delete.message.success", unitOfMeasureToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete reason code: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getUnitOfMeasureManager();
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "unitofmeasure";
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 