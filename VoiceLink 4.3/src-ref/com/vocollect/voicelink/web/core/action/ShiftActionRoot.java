/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Shift;
import com.vocollect.voicelink.core.model.ShiftTime;
import com.vocollect.voicelink.core.service.ShiftManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Level;

/**
 * @author smittal
 */
public class ShiftActionRoot extends DataProviderAction implements Preparable {

    // Serial Version UID for the class.
    private static final long serialVersionUID = 1337139578267810361L;

    private static final long SHIFT_VIEW_ID = -1250;

    private static final Logger log = new Logger(ShiftActionRoot.class);

    private ShiftManager shiftManager;

    private Shift shift;

    private List<Column> shiftColumns;

    private Long shiftId;

    private String startTime = "";

    private String endTime = "";

    /**
     * Getter for the shiftManager property.
     * @return ShiftManager value of the property
     */
    public ShiftManager getShiftManager() {
        return shiftManager;
    }

    /**
     * Setter for the shiftManager property.
     * @param shiftManager the new shiftManager value
     */
    public void setShiftManager(ShiftManager shiftManager) {
        this.shiftManager = shiftManager;
    }

    /**
     * Getter for the shift property.
     * @return Shift value of the property
     */
    public Shift getShift() {
        return shift;
    }

    /**
     * Setter for the shift property.
     * @param shift the new shift value
     */
    public void setShift(Shift shift) {
        this.shift = shift;
    }

    /**
     * Getter for the shiftColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getShiftColumns() {
        return shiftColumns;
    }

    /**
     * Setter for the shiftColumns property.
     * @param shiftColumns the new shiftColumns value
     */
    public void setShiftColumns(List<Column> shiftColumns) {
        this.shiftColumns = shiftColumns;
    }

    /**
     * Getter for the shiftId property.
     * @return Long value of the property
     */
    public Long getShiftId() {
        return shiftId;
    }

    /**
     * Setter for the shiftId property.
     * @param shiftId the new shiftId value
     */
    public void setShiftId(Long shiftId) {
        this.shiftId = shiftId;
    }

    /**
     * Getter for the startTime property.
     * @return String value of the property
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * Getter for the endTime property.
     * @return String value of the property
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     * Setter for the endTime property.
     * @param endTime the new endTime value
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * 
     * @param shiftTime shift time to be converted to a persistable format
     * @return Shift time in persistable format
     */
    private ShiftTime getShiftTime(String shiftTime) {
        if (StringUtil.isNullOrEmpty(shiftTime)) {
            return null;
        }

        String[] hm = shiftTime.split(":");
        ShiftTime time = new ShiftTime();
        time.setHours(Integer.valueOf(hm[0]));
        time.setMinutes(Integer.valueOf(hm[1]));
        return time;
    }

    /**
     * 
     * @param shiftTime shift time to be formatted as displayable
     * @return displayable shift time
     */
    private String getShiftTimeStr(ShiftTime shiftTime) {
        if (shiftTime != null) {
            return shiftTime.getDescriptiveText();
        }

        return null;
    }

    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getShiftManager();
    }

    @Override
    protected String getKeyPrefix() {
        return "shift";
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(SHIFT_VIEW_ID);
        return viewIds;
    }

    public static long getShiftViewId() {
        return SHIFT_VIEW_ID;
    }

    /**
     * 
     * @return success if list of columns obtained successfully
     * @throws Exception e
     */
    public String list() throws Exception {
        View shiftView = getUserPreferencesManager().getView(SHIFT_VIEW_ID);
        this.shiftColumns = this.getUserPreferencesManager().getColumns(
            shiftView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);

        log.debug("Inside prepare method");
        log.debug("shiftId = " + getShiftId());
        if (getShiftId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("shiftId is " + getShiftId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the shift is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved shift from session");
                }

                this.shift = (Shift) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting shift from database");
                }
                this.shift = this.shiftManager.get(getShiftId());
                saveEntityInSession(this.shift);
            }

            this.startTime = getShiftTimeStr(this.shift.getStartTime());
            this.endTime = getShiftTimeStr(this.shift.getEndTime());

            if (log.isDebugEnabled()) {
                log.debug("Shift version is: " + this.shift.getVersion());
            }
        } else if (this.shift == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new Shift object");
            }
            this.shift = new Shift();
        }
    }

    /**
     * @return SUCCESS or INPUT
     * @throws Exception When save fails
     */
    public String save() throws Exception {
        boolean isNew = this.shift.isNew();

        try {
            // Before saving the Shift, checking if already a Shift
            // exists with the same name.
            this.getShiftManager().verifyUniquenessByName(this.shift);

            this.shift.setStartTime(getShiftTime(this.startTime));
            this.shift.setEndTime(getShiftTime(this.endTime));

            this.getShiftManager().save(this.shift);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified shift "
                + this.shift.getName());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.shift"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Shift modifiedShift = getShiftManager().get(getShiftId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.shift.setVersion(modifiedShift.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedShift);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.shift"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("shift."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/shift/view.action?shiftId="
                + this.shift.getId()), this.shift.getName()));

        return SUCCESS;
    }
    
    /**
     * Deletes the currently viewed Shift.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current shift
     */
    public String deleteCurrentShift() throws Exception {

        Shift shiftToDelete = null;

        try {
            shiftToDelete = shiftManager.get(getShiftId());
            shiftManager.delete(getShiftId());

            addSessionActionMessage(new UserMessage(
                "shift.delete.message.success", shiftToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete Shift: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 