/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.dao.OperatorTeamDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.service.OperatorManager;

import com.opensymphony.xwork2.Preparable;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 *
 * @author mnichols
 */
public class OperatorTeamActionRoot extends DataProviderAction implements Preparable {

    //
    private static final long serialVersionUID = -6312449641192748058L;
    //

    private static final Logger log = new Logger(OperatorTeamActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1207;

    // The list of columns for the operatorTeams table.
    private List<Column> columns;

    // The OperatorTeam management service.
    private GenericManager<OperatorTeam, OperatorTeamDAO> operatorTeamManager = null;

    private OperatorManager operatorManager = null;

    // The OperatorTeam object, which will either be newly created, or retrieved
    // via the OperatorTeamId.
    private OperatorTeam operatorTeam;

    // The ID of the operator team.
    private Long operatorTeamId;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "operatorTeam";
    }

    /**
     * Action for the operatorTeams view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View operatorTeamView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            operatorTeamView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the operatorTeam specified by the <code>operatorTeam</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.operatorTeam.isNew();
        try {

            if (this.operatorTeam.getName() == null
                || this.operatorTeam.getName().trim().equalsIgnoreCase("")) {
                addFieldError(
                    "operatorTeam.name", getText("region.create.label.name")
                        + getText("errors.required"));
            }
            if (hasErrors()) {
                return (INPUT);
            }
            //Uniqueness check for name
            Long nameUniquenessId = this.operatorTeamManager.getPrimaryDAO()
                .uniquenessByName(this.operatorTeam.getName());
            if (nameUniquenessId != null
                && (isNew || (!isNew && nameUniquenessId.longValue() != this.operatorTeam
                    .getId().longValue()))) {
                log.warn("An operator team '" + this.operatorTeam.getName()
                    + "' already exists");
                addFieldError("operatorTeam.name", new UserMessage(
                    "operatorTeam.create.error.existingName", this.operatorTeam
                        .getName()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            operatorTeamManager.save(this.operatorTeam);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified operator team "
                + this.operatorTeam.getName());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.OperatorTeam"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                OperatorTeam modifiedEntity = getOperatorTeamManager().get(
                    getOperatorTeamId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.operatorTeam.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.operatorTeam.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "operatorTeam." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(new UserMessage(
            successKey,
            makeGenericContextURL("/operatorTeam/view.action?operatorTeamId="
                + operatorTeam.getId()), this.operatorTeam.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * This method sets up the <code>OperatorTeam</code> object by retrieving it
     * from the database when a operatorTeamId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.operatorTeamId != null) {
            // We have an ID, but not a operatorTeam object yet.
            if (log.isDebugEnabled()) {
                log.debug("operatorTeamId is " + this.operatorTeamId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the operator team is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved operator team from session");
                }
                this.operatorTeam = (OperatorTeam) getEntityFromSession(getSavedEntityKey());
                this.operatorTeam.setOperators(null);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting operator team from database");
                }
                this.operatorTeam = this.operatorTeamManager.get(this.operatorTeamId);
                saveEntityInSession(this.operatorTeam);
            }
            if (log.isDebugEnabled()) {
                log.debug("Operator team version is: "
                    + this.operatorTeam.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new operatorTeam object");
            }
            this.operatorTeam = new OperatorTeam();
        }
    }

    /**
     * Delete the operator team identified by the <code>OperatorTeamId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentOperatorTeam() throws Exception {

        OperatorTeam operatorTeamToDelete = null;

        try {
            operatorTeamToDelete = operatorTeamManager.get(this.operatorTeamId);
            operatorTeamManager.delete(this.operatorTeamId);

            addSessionActionMessage(new UserMessage(
                "operatorTeam.delete.message.success", operatorTeamToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete operator team: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Get the <code>Operators</code>s for the current Site for display.
     * @return the Map of defined <code>Operators</code>s, where the map value is
     *         the ID of the role, and the map key is the (possibly localized)
     *         operator name. The name is used as the key so the data
     *         will be sorted by name.
     * @throws DataAccessException on failure to retrieve.
     */
    public Map<String, Long> getOperatorMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(StringUtil
            .getPrimaryCollator());
        List<Operator> operators = getOperatorManager().listAllOrderByName();
        for (Operator op : operators) {
            if (op.getName() != null && op.getName().trim().length() > 0) {
                map.put(op.getOperatorIdentifier() + " - " + op.getName(), op.getId());
            } else {
                map.put(op.getOperatorIdentifier(), op.getId());
            }
        }
        return map;
    }

    /**
     * @param operatorIds - the ids of the operators
     * @throws DataAccessException
     */
    public void setOperators(long[] operatorIds) throws DataAccessException {
        if (this.operatorTeam != null) {
            this.operatorTeam.setOperators(null);
            for (int i = 0; i < operatorIds.length; i++) {
                this.operatorTeam.addOperator(getOperatorManager().get(operatorIds[i]));
            }
        }
    }

    /**
     * @return long[] - list of operator ids
     */
    public long[] getOperators() {
        long[] operatorIds = new long[operatorTeam.getOperators().size()];
        if (this.operatorTeam != null) {
            Iterator<Operator> opIt = operatorTeam.getOperators().iterator();
            int i = 0;
            while (opIt.hasNext()) {
                operatorIds[i] = opIt.next().getId();
                i++;
            }
        }
        return operatorIds;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) getOperatorTeamManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Setter for the operatorTeamId property.
     * @param operatorTeamId the new operatorTeamId value
     */
    public void setOperatorTeamId(Long operatorTeamId) {
        this.operatorTeamId = operatorTeamId;

    }

    /**
     * Getter for the operatorTeamId property.
     * @return the value of the property
     */
    public Long getOperatorTeamId() {
        return this.operatorTeamId;

    }

    /**
     * Setter for the operatorTeam property.
     * @param operatorTeam the new operatorTeam value
     */
    public void setOperatorTeam(OperatorTeam operatorTeam) {
        this.operatorTeam = operatorTeam;

    }

    /**
     * Getter for the operatorTeam property.
     * @return the value of the property
     */
    public OperatorTeam getOperatorTeam() {
        return this.operatorTeam;

    }

    /**
     * Setter for the operatorTeamManager property.
     * @param operatorTeamManager the new operatorTeamManager value
     */
    public void setOperatorTeamManager(GenericManager<OperatorTeam, OperatorTeamDAO> operatorTeamManager) {
        this.operatorTeamManager = operatorTeamManager;

    }

    /**
     * Getter for the operatorTeamManager property.
     * @return the value of the property
     */
    public GenericManager<OperatorTeam, OperatorTeamDAO> getOperatorTeamManager() {
        return this.operatorTeamManager;

    }

    /**
     * Setter for the columns property.
     * @param columns the new columns value
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;

    }

    /**
     * Getter for the columns property.
     * @return the value of the property
     */
    public List<Column> getOperatorTeamColumns() {
        return this.columns;

    }

    /**
     * Getter for the OPERATORTEAM_VIEW_ID property.
     * @return long value of the property
     */
    public static long getOperatorTeamViewId() {
        return VIEW_ID;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;

    }

    /**
     * Getter for the operatorManager property.
     * @return the value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 