/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingManager;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;
import static com.vocollect.voicelink.core.model.DeliveryLocationMappingType.CustomerNumber;
import static com.vocollect.voicelink.core.model.DeliveryLocationMappingType.Route;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;

/**
 * This is the Struts action class that handles operations on
 * <code>DeliveryLocationMapping</code> objects. Delivery location mappings are
 * rule which are used at assignment completion to determine where the operator
 * should delivery the items.
 * 
 * @author Ben Northrop
 */
public abstract class DeliveryLocationMappingActionRoot extends
        DataProviderAction implements Preparable {

    /**
     * A map of mapping types (key) and view ids (long value) used to determine
     * what table component to display.
     */
    private Map<DeliveryLocationMappingType, Long> viewMap = new HashMap<DeliveryLocationMappingType, Long>();

    /**
     * The ID of the view we need to grab from the DB for Route mappings.
     */
    private static final long ROUTE_MAPPING_VIEW_ID = -1025;

    /**
     * The ID of the view we need to grab from the DB for customer number
     * mappings.
     */
    private static final long CUSTOMER_NUMBER_MAPPING_VIEW_ID = -1027;

    /**
     * The list of columns for the picks table.
     */
    private List<Column> deliveryLocationMappingColumns;

    /**
     * The deliveryLocation to set a collection of mappings to.
     */
    private String deliveryLocation;

    /**
     * The ids of delivery location mappings that will be modified.
     */
    private Collection<Long> deliveryLocationMappingIds;

    /**
     * SUID generated from Eclipse.
     */
    private static final long serialVersionUID = 6233234648237046898L;

    /**
     * A list of DeliveryLocationMapping objects for the create action.
     */
    private Collection<DeliveryLocationMapping> deliveryLocationMappings = new ArrayList<DeliveryLocationMapping>();

    /**
     * A manager that will determine the delivery location mapping settings for
     * the current site the user is logged in to (e.g. route, customer number,
     * etc.)
     */
    private DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager;

    /**
     * The manager for managing all the mappings.
     */
    private DeliveryLocationMappingManager deliveryLocationMappingManager;

    /**
     * The DeliveryLocationMappingType for the change mapping field action.
     */
    private Integer deliveryLocationMappingTypeId;

    /**
     * The number of fields in the create form - initially there is only one
     * mapping on the create screen.
     */
    private int numberOfMappings = 1;

    /**
     * The current mode the site is in (e.g. customer number, route, etc.)
     */
    private DeliveryLocationMappingType currentMappingType;

    /**
     * Default constructor. Create the map of different view types, which
     * corresponds to the different mapping types (e.g. customer number, route).
     */
    public DeliveryLocationMappingActionRoot() {
        viewMap.put(CustomerNumber, CUSTOMER_NUMBER_MAPPING_VIEW_ID);
        viewMap.put(Route, ROUTE_MAPPING_VIEW_ID);
        deliveryLocationMappings.add(new DeliveryLocationMapping());
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "deliveryLocationMapping";
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getDeliveryLocationMappingManager();
    }

    /**
     * Find the mapping type for the current site. {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        currentMappingType = this.getDeliveryLocationMappingSettingManager()
                .getMappingType();
        this.setDeliveryLocationMappingTypeId(currentMappingType.getValue());
    }

    /**
     * Return the list of delivery location mapping columns.
     * 
     * @return String value of outcome.
     * @throws Exception
     *             in case of DataAccess issues
     */
    public String list() throws Exception {
        View deliveryLocationMappingsView = this.getUserPreferencesManager()
                .getView(getDeliveryLocationMappingViewId());
        this.deliveryLocationMappingColumns = this.getUserPreferencesManager()
                .getColumns(deliveryLocationMappingsView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create one or many mappings. Looping and individually saving because want
     * to be able to validate that the mapping doesn't already exist, and if it
     * does, want to be able to flag the correct field.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             of any unanticipated failure.
     */
    public String save() throws Exception {

        int count = 0;
        for (DeliveryLocationMapping dlm : deliveryLocationMappings) {
            try {
                DeliveryLocationMappingType mappingType = getDeliveryLocationMappingSettingManager()
                        .getMappingType();
                dlm.setMappingType(mappingType);
                getDeliveryLocationMappingManager().save(dlm);
            } catch (BusinessRuleException bre) {
                addFieldError(
                        "deliveryLocationMappings[" + count + "]",
                        new UserMessage(
                                "deliveryLocationMapping.create.message.error.mappingExists"));
                return INPUT;
            }
            count++;
        }

        if (deliveryLocationMappings.size() == 1) {
            addSessionActionMessage(new UserMessage(
                    getText("deliveryLocationMapping.create.message.success"),
                    ERROR_SUCCESS));
        } else if (deliveryLocationMappings.size() > 1) {
            addSessionActionMessage(new UserMessage(
                    getText("deliveryLocationMapping.create.message.multiple.success"),
                    deliveryLocationMappings.size(), ERROR_SUCCESS));
        }
        return SUCCESS;
    }

    /**
     * Change the mapping field.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             on any unanticipated failure. *
     */
    public String changeMappingField() throws Exception {
        DeliveryLocationMappingType mappingType = DeliveryLocationMappingType
                .toEnum(getDeliveryLocationMappingTypeId());
        getDeliveryLocationMappingSettingManager().executeChangeMappingType(
                mappingType);
        addSessionActionMessage(new UserMessage(
                getText("deliveryLocationMapping.changeMappingField.mappingField")
                        + "  " + getText(mappingType.getResourceKey()),
                ERROR_SUCCESS));
        return SUCCESS;
    }

    /**
     * Modify the delivery location for a set of mappings.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             on any unanticipated failure.
     */
    public String modifyDeliveryLocation() throws Exception {
        LOG.info("Modifying deliveryLocation: " + deliveryLocation);
        JSONArray jsonSuccessIds = new JSONArray();

        if (!StringUtil.isNullOrEmpty(getDeliveryLocation().trim())) {

            if (getDeliveryLocationMappingManager()
                    .isDeliveryLocationReferenced(getDeliveryLocationMappingIds())) {
                setJsonMessage(
                        getText(new UserMessage(
                                getText("deliveryLocationMapping.modify.message.error.referenced"),
                                getDeliveryLocation(), true)), ERROR_FAILURE,
                        jsonSuccessIds);
                return SUCCESS;
            }

            getDeliveryLocationMappingManager().executeModifyDeliveryLocation(
                    getDeliveryLocationMappingIds(), getDeliveryLocation());
            if (getDeliveryLocationMappingIds().size() == 1) {
                setJsonMessage(
                        getText(new UserMessage(
                                getText("deliveryLocationMapping.modify.message.success"),
                                getDeliveryLocation(), true)), ERROR_SUCCESS,
                        jsonSuccessIds);
                return SUCCESS;
            } else {
                setJsonMessage(
                        getText(new UserMessage(
                                getText("deliveryLocationMapping.modify.message.multiple.success"),
                                deliveryLocationMappingIds.size(),
                                getDeliveryLocation(), true)), ERROR_SUCCESS,
                        jsonSuccessIds);
                return SUCCESS;
            }
        }
        setJsonMessage(getText(new UserMessage(
                getText("deliveryLocationMapping.modify.deliveryLocation"),
                true)), ERROR_FAILURE, jsonSuccessIds);
        return SUCCESS;
    }

    /**
     * Getter for the deliveryLocationViewId property.
     * 
     * @return long value of the property
     */
    public long getDeliveryLocationMappingViewId() {
        try {
            DeliveryLocationMappingType mappingType = getDeliveryLocationMappingSettingManager()
                    .getMappingType();

            return viewMap.get(mappingType);
        } catch (DataAccessException dae) {
            throw new IllegalStateException(
                    "Got db exception from setting manager!" + dae);
        }
    }

    /**
     * Getter for the deliveryLocationMappingColumns property.
     * 
     * @return List value of the property
     */
    public List<Column> getDeliveryLocationMappingColumns() {
        return this.deliveryLocationMappingColumns;
    }

    /**
     * Setter for the deliveryLocationMappingColumns property.
     * 
     * @param deliveryLocationMappingColumns
     *            the new deliveryLocationMappingColumns value
     */
    public void setDeliveryLocationMappingColumns(
            List<Column> deliveryLocationMappingColumns) {
        this.deliveryLocationMappingColumns = deliveryLocationMappingColumns;
    }

    /**
     * Getter for the deliveryLocationMappings property.
     * 
     * @return List value of the property
     */
    public Collection<DeliveryLocationMapping> getDeliveryLocationMappings() {
        return this.deliveryLocationMappings;
    }

    /**
     * Setter for the deliveryLocationMappings property.
     * 
     * @param deliveryLocationMappings
     *            the new deliveryLocationMappings value
     */
    public void setDeliveryLocationMappings(
            Collection<DeliveryLocationMapping> deliveryLocationMappings) {
        this.deliveryLocationMappings = deliveryLocationMappings;
    }

    /**
     * Getter for the deliveryLocation property.
     * 
     * @return String value of the property
     */
    public String getDeliveryLocation() {
        return this.deliveryLocation;
    }

    /**
     * Setter for the deliveryLocation property.
     * 
     * @param deliveryLocation
     *            the new deliveryLocation value
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * Getter for the deliveryLocationMappingIds property.
     * 
     * @return Collection value of the property
     */
    public Collection<Long> getDeliveryLocationMappingIds() {
        return this.deliveryLocationMappingIds;
    }

    /**
     * Setter for the deliveryLocationMappingIds property.
     * 
     * @param deliveryLocationIds
     *            the new deliveryLocationMappingIds value
     */
    public void setDeliveryLocationMappingIds(
            Collection<Long> deliveryLocationIds) {
        this.deliveryLocationMappingIds = deliveryLocationIds;
    }

    /**
     * Getter for the deliveryLocationMappingSettingManager property.
     * 
     * @return DeliveryLocationMappingSettingManager value of the property
     */
    public DeliveryLocationMappingSettingManager getDeliveryLocationMappingSettingManager() {
        return this.deliveryLocationMappingSettingManager;
    }

    /**
     * Setter for the deliveryLocationMappingSettingManager property.
     * 
     * @param deliveryLocationMappingSettingManager
     *            the new deliveryLocationMappingSettingManager value
     */
    public void setDeliveryLocationMappingSettingManager(
            DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager) {

        this.deliveryLocationMappingSettingManager = deliveryLocationMappingSettingManager;
    }

    /**
     * Getter for the types of mappings.
     * 
     * @return Map of the mapping types, to be used in a combo box.
     */
    public Map<Integer, String> getDeliveryLocationMappingTypesMap() {
        Map<Integer, String> map = new LinkedHashMap<Integer, String>();
        for (DeliveryLocationMappingType type : DeliveryLocationMappingType
                .values()) {
            LOG.info(type.name());
            LOG.info("type.resourceKey: " + type.getResourceKey());
            LOG.info(type.toString());
            map.put(type.getValue(), getText(type.getResourceKey()));
        }
        return map;
    }

    /**
     * Add a new mapping type to the map.
     * 
     * @param mappingType
     *            - a type of mapping for delivery location.
     * @param viewId
     *            - the user's preferences view
     */
    public void addViewMapping(DeliveryLocationMappingType mappingType,
            long viewId) {
        viewMap.put(mappingType, viewId);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(ROUTE_MAPPING_VIEW_ID);
        viewIds.add(CUSTOMER_NUMBER_MAPPING_VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the numberOfMappings property.
     * 
     * @return int value of the property
     */
    public int getNumberOfMappings() {
        return this.numberOfMappings;
    }

    /**
     * Setter for the numberOfMappings property.
     * 
     * @param createNumberOfMappings
     *            the new numberOfMappings value
     */
    public void setNumberOfMappings(int createNumberOfMappings) {
        this.numberOfMappings = createNumberOfMappings;
    }

    /**
     * Validating here rather than in XML file because OpenSymphony seems to
     * have taken away the "collection" validator from the validation framework.
     * Rather than write a new validator (with Dennis Doubleday's advice) I
     * decided to validate here since this is such a unique case.
     * 
     * {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.ActionSupport#validate()
     */
    @Override
    public void validate() {

        validateFields();

        validateDuplicates();

        validateDatabaseDuplicates();
    }

    /**
     * Check to see that neither the delivery location nor the mapping value is
     * null.
     */
    private void validateFields() {
        int count = 0;
        for (DeliveryLocationMapping dlm : deliveryLocationMappings) {

            if (dlm == null) {
                LOG.info("Delivery Location Mapping is null." + count);
                break;
            }

            // Validate delivery locaiton is not null condition.
            if (StringUtil.isNullOrEmpty(dlm.getDeliveryLocation().trim())) {
                addFieldError(
                        "deliveryLocationMappings[" + count + "]",
                        new UserMessage(
                                "deliveryLocationMapping.create.message.error.deliveryLocationIsNull"));
            }
            // Validate mapping value not null
            if (StringUtil.isNullOrEmpty(dlm.getMappingValue().trim())) {
                addFieldError(
                        "deliveryLocationMappings[" + count + "]",
                        new UserMessage(
                                "deliveryLocationMapping.create.message.error.mappingValueIsNull."
                                        + this.currentMappingType
                                                .getResourceKey()));
            }

            count++;
        }
    }

    /**
     * Ensure that no mapping rules are duplicates (either within the database
     * or within the set of mapping rules that are to be created).
     */
    private void validateDuplicates() {
        int count = 0;
        for (DeliveryLocationMapping dlm1 : deliveryLocationMappings) {
            for (DeliveryLocationMapping dlm2 : deliveryLocationMappings) {
                if (StringUtil.isNullOrEmpty(dlm2.getDeliveryLocation().trim())
                        && StringUtil.isNullOrEmpty(dlm2.getMappingValue()
                                .trim())) {
                } else if (dlm1 != dlm2
                        && dlm1.getMappingValue()
                                .equals(dlm2.getMappingValue())) {
                    addFieldError(
                            "deliveryLocationMappings[" + count + "]",
                            new UserMessage(
                                    "deliveryLocationMapping.create.message.error.duplicateMapping"));
                }
            }

            count++;
        }
    }

    /**
     * Loop through all mappings and ensure that none already exist in the
     * database.
     */
    private void validateDatabaseDuplicates() {
        int count = 0;
        for (DeliveryLocationMapping mapping : deliveryLocationMappings) {
            try {
                String dLocation = getDeliveryLocationMappingManager()
                        .findDeliveryLocation(mapping.getMappingValue());
                if (dLocation != null) {
                    addFieldError(
                            "deliveryLocationMappings[" + count + "]",
                            new UserMessage(
                                    "deliveryLocationMapping.create.message.error.mappingExists"));
                }
            } catch (DataAccessException dae) {
                LOG.error(dae);
                throw new IllegalStateException(
                        "Unexpected DataAccessException when validating mapping duplicates",
                        dae);
            }
            count++;
        }
    }

    /**
     * Getter for the deliveryLocationMappingManager property.
     * 
     * @return DeliveryLocationMappingManager value of the property
     */
    public DeliveryLocationMappingManager getDeliveryLocationMappingManager() {
        return this.deliveryLocationMappingManager;
    }

    /**
     * Setter for the deliveryLocationMappingManager property.
     * 
     * @param deliveryLocationMappingManager
     *            the new deliveryLocationMappingManager value
     */
    public void setDeliveryLocationMappingManager(
            DeliveryLocationMappingManager deliveryLocationMappingManager) {
        this.deliveryLocationMappingManager = deliveryLocationMappingManager;
    }

    /**
     * Getter for the deliveryLocationMappingTypeId property.
     * 
     * @return DeliveryLocationMappingType value of the property
     */
    public Integer getDeliveryLocationMappingTypeId() {
        return this.deliveryLocationMappingTypeId;
    }

    /**
     * Setter for the deliveryLocationMappingTypeId property.
     * 
     * @param deliveryLocationMappingTypeId
     *            the new deliveryLocationMappingTypeId value
     */
    public void setDeliveryLocationMappingTypeId(
            Integer deliveryLocationMappingTypeId) {
        this.deliveryLocationMappingTypeId = deliveryLocationMappingTypeId;
    }

    /**
     * Get the resource key for the current mapping type.
     * 
     * @return the resource key for the current mapping type.
     */
    public String getCurrentMappingTypeResourceKey() {
        return currentMappingType.getResourceKey();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getPrintData() This
     *      method is included to handle the filtering feature for Delivery
     *      location This method ensures that the correct mapping value is
     *      printed and not the entire list
     */
    @Override
    public String getPrintData() throws Exception {
        if ((this.getDeliveryLocationMappingSettingManager().getMappingType() != null)) {

            super.getPrintData("obj.mappingType =  "
                    + this.getDeliveryLocationMappingSettingManager()
                            .getMappingType().getValue());
        } else {
            super.getPrintData();
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData() This
     *      method is included to handle the filtering feature for Delivery
     *      location
     */
    @Override
    public String getTableData() throws Exception {
        if ((this.getDeliveryLocationMappingSettingManager().getMappingType() != null)) {

            super.getTableData("obj.mappingType =  "
                    + this.getDeliveryLocationMappingSettingManager()
                            .getMappingType().getValue());
        } else {
            super.getTableData();
        }
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 