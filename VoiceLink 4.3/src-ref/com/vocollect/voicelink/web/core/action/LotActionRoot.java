/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.dao.LotDAO;
import com.vocollect.voicelink.core.model.Lot;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>Lot</code> objects.
 *
 * @author mnichols
 */
public class LotActionRoot extends DataProviderAction implements Preparable {

        //
    private static final long serialVersionUID = -6498360412669771524L;

    private static final Logger log = new Logger(LotActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1041;

    // The list of columns for the lots table.
    private List<Column> columns;

    // The Lot management service.
    private GenericManager<Lot, LotDAO> lotManager = null;

    // The Lot object, which will either be newly created, or retrieved
    // via the LotId.
    private Lot lot;

    // The ID of the break type.
    private Long lotId;

    /**
     * Getter for the lot property.
     * @return User value of the property
     */
    public Lot getLot() {
        return this.lot;
    }

    /**
     * Setter for the lot property.
     * @param lot the new lot value
     */
    public void setLot(Lot lot) {
        this.lot = lot;
    }

    /**
     * Getter for the lotId property.
     * @return Long value of the property
     */
    public Long getLotId() {
        return this.lotId;
    }

    /**
     * Setter for the lotId property.
     * @param lotId the new lotId value
     */
    public void setLotId(Long lotId) {
        this.lotId = lotId;
    }

    /**
     * Getter for the lotManager property.
     * @return UserManager value of the property
     */
    public GenericManager<Lot, LotDAO> getLotManager() {
        return this.lotManager;
    }

    /**
     * Setter for the lotManager property.
     * @param manager the new lotManager value
     */
    public void setLotManager(GenericManager<Lot, LotDAO> manager) {
        this.lotManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getLotColumns() {
        return this.columns;
    }

    /**
     * Action for the lots view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View lotView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            lotView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the lot specified by the <code>lot</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Lot</code> object by retrieving it
     * from the database when a lotId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.lotId != null) {
            // We have an ID, but not a lot object yet.
            if (log.isDebugEnabled()) {
                log.debug("lotId is " + this.lotId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the break type is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved break type from session");
                }
                this.lot = (Lot) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting break type from database");
                }
                this.lot = this.lotManager.get(this.lotId);
                saveEntityInSession(this.lot);
            }
            if (log.isDebugEnabled()) {
                log.debug("Break type version is: "
                    + this.lot.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new lot object");
            }
            this.lot = new Lot();
        }
    }

    /**
     * Delete the lot identified by the <code>lotId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentLot() throws Exception {

        Lot lotToDelete = null;

        try {
            lotToDelete = lotManager.get(this.lotId);
            lotManager.delete(this.lotId);

            addSessionActionMessage(new UserMessage(
                "lot.delete.message.success", lotToDelete.getNumber()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete lot: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getLotManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "lot";
    }

    /**
     * Getter for the LOT_VIEW_ID property.
     * @return long value of the property
     */
    public static long getLotViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 