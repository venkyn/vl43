/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.ReportDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.ReportException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Report;
import com.vocollect.epp.model.ReportFormat;
import com.vocollect.epp.model.ReportFrequency;
import com.vocollect.epp.model.ReportInterval;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.model.ReportType;
import com.vocollect.epp.model.ReportTypeParameter;
import com.vocollect.epp.model.ReportTypeParameterRoot;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.service.ReportParameterManager;
import com.vocollect.epp.service.ReportTypeManager;
import com.vocollect.epp.service.ReportTypeParameterManager;
import com.vocollect.epp.service.SystemTranslationManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.JasperReportWrapper;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.ReportWrapper;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.OperatorTeamManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;
import com.vocollect.voicelink.lineloading.service.LineLoadingRegionManager;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;

import static com.vocollect.epp.model.ReportParameterFieldType.DropDownWithAll;
import static com.vocollect.epp.model.ReportParameterFieldType.DropDownWithoutAll;
import static com.vocollect.epp.model.ReportParameterFieldType.ListField;
import static com.vocollect.epp.util.ReportUtilities.DATE_FORMAT;
import static com.vocollect.epp.util.ReportUtilities.END_DATE;
import static com.vocollect.epp.util.ReportUtilities.ENUM;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_ALL;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_IDENTIFIERS;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_IDENTIFIERS_ALL;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_TEAM;
import static com.vocollect.epp.util.ReportUtilities.OPERATOR_TEAM_ID;
import static com.vocollect.epp.util.ReportUtilities.PARAM_CURRENT_SITE_CONTEXT;
import static com.vocollect.epp.util.ReportUtilities.PARAM_CURRENT_USER;
import static com.vocollect.epp.util.ReportUtilities.PARAM_LIST_ALL;
import static com.vocollect.epp.util.ReportUtilities.REPORT_NAME;
import static com.vocollect.epp.util.ReportUtilities.SESSION_VALUE;
import static com.vocollect.epp.util.ReportUtilities.SITE;
import static com.vocollect.epp.util.ReportUtilities.START_DATE;
import static com.vocollect.epp.util.ReportUtilities.TIME_ZONE;
import static com.vocollect.epp.util.ReportUtilities.getReportValuesMap;
import static com.vocollect.epp.util.ReportUtilities.setReportOperatorIdentifiers;
import static com.vocollect.epp.util.ReportUtilities.setReportOperatorTeams;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_HIBERNATE_SESSION_PARAM;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_LOCALE;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_SOURCE_EXTENSION;
import static com.vocollect.epp.util.ReportWrapperRoot.JASPER_REPORT_TIMEZONE;
import static com.vocollect.epp.util.ReportWrapperRoot.REPORT_PATH_BASE;

import com.opensymphony.xwork2.Preparable;

import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.servlet.ServletContext;

import org.apache.struts2.util.ServletContextAware;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * This is the Struts action class that handles operations on
 * <code>Report</code> objects.
 * 
 * @author mnichols
 */
public class ReportActionRoot extends DataProviderAction implements Preparable,
        ApplicationContextAware, ServletContextAware {

    private static final String APP_TYPE = "voicelink%";

    public static final String LIST_ALL_DESCRIPTION = "report.parameter.all";

    public static final String REPORT_DEFAULT_RENDER_FORMAT = "report.DefaultRenderFormat";

    public static final String PARAM_STRING = "REPORTTYPEPARAM";

    public static final String REPORT_NAMESPACE_ROOT = "reports";

    private static final int HOURS_IN_A_DAY = 24;

    private static final int O_CLOCK = 0;

    private static final int QUARTER_AFTER = 15;

    private static final int HALF_PAST = 30;

    private static final int QUARTER_TO = 45;

    private static final long DEFAULT_REPORT_ID = -200;

    private static final long serialVersionUID = -6498360412669771524L;

    private static final Logger log = new Logger(ReportActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1039;

    // The list of columns for the reports table.
    private List<Column> columns;

    // The Report management service.
    private GenericManager<Report, ReportDAO> reportManager = null;

    private ReportParameterManager reportParameterManager = null;

    //
    private ReportTypeManager reportTypeManager = null;

    private OperatorManager operatorManager = null;

    private OperatorTeamManager operatorTeamManager = null;

    private ReportTypeParameterManager reportTypeParameterManager = null;

    private ApplicationContext applicationContext;

    private SelectionRegionManager selRegionManager = null;

    private PtsRegionManager ptsRegionManager = null;

    private ReplenishmentRegionManager repRegionManager = null;

    private PutawayRegionManager putRegionManager = null;

    private LineLoadingRegionManager llRegionManager = null;

    private CycleCountingRegionManager cycleCountingRegionManager = null;

    private LoadingRegionManager loadingRegionManager = null;

    private SystemTranslationManager systemTranslationManager;

    // The Report object, which will either be newly created, or retrieved
    // via the ReportId.
    private Report report;

    // The ID of the report.
    private Long reportId;

    private Long reportTypeId = DEFAULT_REPORT_ID;

    private ReportType reportType;

    private static Map<String, String> enumValues;

    private String startDate = null;

    private String endDate = null;

    // Report Launching Stuff
    private ServletContext servletContext;

    private String path = null;

    private String reportNamespace;

    private String userName;

    private String siteName;

    private static UserManager userManager;

    private InputStream reportStream;

    private SessionFactory sessionFactory;

    private String sessionBeanId;

    private static long siteId;

    private Boolean readOnly = false;

    private String fieldErr = null;

    private String actionId = null;

    private Map<String, String> languageMap;

    /**
     * Getter for the format property.
     * 
     * @return the value of the property
     */
    public Integer getFormat() {
        return this.report.getFormat().getValue();
    }

    /**
     * Setter for the format property.
     * 
     * @param format
     *            the new format value
     */
    public void setFormat(Integer format) {
        this.report.setFormat(ReportFormat.toEnum(format));
    }

    /**
     * Getter for the interval property.
     * 
     * @return the value of the property
     */
    public Integer getInterval() {
        return this.report.getInterval().getValue();
    }

    /**
     * Setter for the interval property.
     * 
     * @param interval
     *            the new interval value
     */
    public void setInterval(Integer interval) {
        this.report.setInterval(ReportInterval.toEnum(interval));
    }

    /**
     * Getter for the frequency property.
     * 
     * @return the value of the property
     */
    public Integer getFrequency() {
        return this.report.getFrequency().getValue();
    }

    /**
     * Setter for the interval property.
     * 
     * @param frequency
     *            the new frequency value
     */
    public void setFrequency(Integer frequency) {
        this.report.setFrequency(ReportFrequency.toEnum(frequency));
    }

    /**
     * Getter for the report property.
     * 
     * @return User value of the property
     */
    public Report getReport() {
        return this.report;
    }

    /**
     * Setter for the report property.
     * 
     * @param report
     *            the new report value
     */
    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * Getter for the reportId property.
     * 
     * @return Long value of the property
     */
    public Long getReportId() {
        return this.reportId;
    }

    /**
     * Setter for the reportId property.
     * 
     * @param reportId
     *            the new reportId value
     */
    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    /**
     * Getter for the reportManager property.
     * 
     * @return UserManager value of the property
     */
    public GenericManager<Report, ReportDAO> getReportManager() {
        return this.reportManager;
    }

    /**
     * Setter for the reportManager property.
     * 
     * @param manager
     *            the new reportManager value
     */
    public void setReportManager(GenericManager<Report, ReportDAO> manager) {
        this.reportManager = manager;
    }

    /**
     * Getter for the columns property.
     * 
     * @return List of Column value of the property.
     */
    public List<Column> getReportColumns() {
        return this.columns;
    }

    /**
     * Action for the reports view page. Initializes the table columns.
     * 
     * @return String value of outcome.
     * @throws Exception
     *             in case of DataAccess issues
     */
    public String list() throws Exception {
        View reportView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(reportView,
                getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the report specified by the <code>report</code> member
     * of this class.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             on any unanticipated failure.
     */
    @SuppressWarnings("unused")
    public String save() throws Exception {
        Enumeration<String> enumer = this.getRequest().getParameterNames();
        Set<ReportParameter> repParams = new HashSet<ReportParameter>();
        while (enumer.hasMoreElements()) {
            String name = enumer.nextElement();
            if (name.toUpperCase().indexOf(PARAM_STRING) >= 0) {
                int paramIdIndex = PARAM_STRING.length();
                name = name.replace('_', '-');
                long paramId = Long.valueOf(name.substring(paramIdIndex,
                        name.length()));

                String[] values = this.getRequest().getParameterValues(name);
                if (values != null) {
                    ReportTypeParameter repTypeParam = this
                            .getReportTypeParameterManager().get(paramId);
                    if (values.length > 1) {
                        for (int i = 0; i < values.length; i++) {
                            repParams.add(new ReportParameter(repTypeParam,
                                    values[i]));
                        }
                    } else {
                        repParams.add(new ReportParameter(repTypeParam,
                                values[0]));
                    }
                }
            }
        }
        this.report.setReportParameters(repParams);
        if (getReportTypeId() != null) {
            this.report.setType(this.getReportTypeManager().get(
                    getReportTypeId()));
        }

        boolean isNew = this.report.isNew();

        try {

            // Check for mandatory name field
            if (this.report.getName() == null
                    || this.report.getName().trim().equalsIgnoreCase("")) {
                addFieldError("report.name", getText("report.label.name") + " "
                        + getText("errors.required"));
            }

            // Check for parameter validation
            if (report.getReportParameters() != null) {
                Iterator<ReportParameter> iter = report.getReportParameters()
                        .iterator();
                while (iter.hasNext()) {
                    ReportParameter param = iter.next();
                    if (param.getReportTypeParameter().getIsRequired()) {
                        if (param.getValue() == null
                                || param.getValue().trim().equalsIgnoreCase("")) {
                            addFieldError("reportTypeParam"
                                    + param.getReportTypeParameter().getId(),
                                    getText(param.getReportTypeParameter()
                                            .getDescription())
                                            + " "
                                            + getText("errors.required"));
                        }
                    }
                    if (param.getReportTypeParameter().getValidationFunction() != null) {
                        if (param.getReportTypeParameter()
                                .getValidationFunction()
                                .equalsIgnoreCase("isNumeric")) {
                            if (!(param.getValue() == null || param.getValue()
                                    .trim().equalsIgnoreCase(""))) {
                                try {
                                    Integer testInt = Integer.parseInt(param
                                            .getValue().trim());
                                } catch (Exception e) {
                                    addFieldError("reportTypeParam"
                                            + param.getReportTypeParameter()
                                                    .getId(), getText(param
                                            .getReportTypeParameter()
                                            .getDescription())
                                            + " " + getText("errors.integer"));
                                }
                            }
                        }
                    }
                }
            }

            if (hasErrors()) {
                return (INPUT);
            }
            // Uniqueness check for name
            Long nameUniquenessId = this.reportManager.getPrimaryDAO()
                    .uniquenessByName(this.report.getName());
            if (nameUniquenessId != null
                    && (isNew || (!isNew && nameUniquenessId.longValue() != this.report
                            .getId().longValue()))) {
                log.warn("A report '" + this.report.getName()
                        + "' already exists");
                addFieldError("report.name",
                        new UserMessage("report.create.error.existingName",
                                this.report.getName()));
                // Go back to the form to show the error message.
                return INPUT;
            }

            // Reset the lastRun variable for the scheduler
            this.report.setLastRun(null);
            reportManager.save(this.report);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            return getOptimisticLockingFailureResponse();
        }

        // add success message
        String successKey = "report." + (isNew ? "create" : "edit")
                + ".message.success";
        addSessionActionMessage(new UserMessage(successKey,
                makeGenericContextURL("/report/view.action?reportId="
                        + report.getId()), this.report.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Create or update the report specified by the <code>report</code> member
     * of this class.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             if any error is encountered while saving the report.
     */
    public String schedule() throws Exception {
        try {
            // Check for mandatory email fields if scheduled
            if (this.report.getFrequency() != ReportFrequency.Unscheduled) {
                if (StringUtil.isNullOrBlank(this.report.getEmails())) {
                    addFieldError("report.emails",
                            getText("report.label.email") + " "
                                    + getText("errors.required"));
                }
                if (StringUtil.isNullOrBlank(this.report.getSubject())) {
                    addFieldError("report.subject",
                            getText("report.label.subject") + " "
                                    + getText("errors.required"));
                }
            }

            if (hasErrors()) {
                return (INPUT);
            }

            // Create new report parameter objects so that hibernate inserts
            // new. VLINK-3473
            Set<ReportParameter> reportParamsOld = this.report
                    .getReportParameters();
            Set<ReportParameter> reportParams = new HashSet<ReportParameter>(
                    reportParamsOld.size());
            for (ReportParameter reportParameter : reportParamsOld) {
                reportParams.add(new ReportParameter(reportParameter
                        .getReportTypeParameter(), reportParameter.getValue()));
            }
            this.report.setReportParameters(reportParams);

            // Reset the lastRun variable for the scheduler
            this.report.setLastRun(null);
            reportManager.save(this.report);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            return getOptimisticLockingFailureResponse();
        }

        // add success message
        addSessionActionMessage(new UserMessage("report.edit.message.success",
                makeGenericContextURL("/report/view.action?reportId="
                        + report.getId()), this.report.getName()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * 
     * @return Map - the language drop down values
     * @throws DataAccessException
     *             when cannot get languages
     */
    public Map<String, String> getLanguageMap() throws DataAccessException {

        if (this.languageMap == null) {
            this.languageMap = new LinkedHashMap<String, String>();
            for (Locale locale : getSystemTranslationManager()
                    .getAllSupportedLocales()) {
                languageMap.put(
                        locale.toString(),
                        getText("selection.reports.view.column."
                                + locale.toString()));
            }
        }
        return this.languageMap;
    }

    /**
     * Method to handle application response in case of OptimisticLockingFailure
     * when modifying a report.
     * 
     * @return the control flow target name.
     * @throws DataAccessException
     *             thrown when a problem is encountered in getting a report
     *             using id
     */
    private String getOptimisticLockingFailureResponse()
            throws DataAccessException {
        log.warn("Attempt to change already modified report "
                + this.report.getName());
        addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.Report"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
        // Get the modified data
        // If the entity has been deleted, this will throw
        // EntityNotFoundException
        try {
            Report modifiedEntity = getReportManager().get(getReportId());
            // Set the local object's version to match, so it will work
            // if the user resubmits.
            this.report.setVersion(modifiedEntity.getVersion());
            // Store the modified data for display
            setModifiedEntity(modifiedEntity);
        } catch (EntityNotFoundException ex) {
            addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization(this.report.getName()),
                    null));
            return SUCCESS;
        }
        return INPUT;
    }

    /**
     * This method sets up the <code>Report</code> object by retrieving it from
     * the database when a reportId is set by the form submission. {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {

        // Add Error messages to Parameter fields
        if (fieldErr != null && fieldErr.equalsIgnoreCase("") == false) {
            fieldErr = fieldErr.replace('[', ' ');
            fieldErr = fieldErr.replace(']', ' ');
            fieldErr = fieldErr.replace('{', ' ');
            fieldErr = fieldErr.replace('}', ' ');
            String[] errors = fieldErr.split(",");
            for (int i = 0; i < errors.length; i++) {
                String[] values = errors[i].split("=");
                addFieldError(values[0].trim(), values[1].trim());
            }
        }
        // Set the values of the dynamic report type parameters
        // this.report.setReportParameters(parseParameters());
        if (this.reportId != null) {
            // We have an ID, but not a report object yet.
            if (log.isDebugEnabled()) {
                log.debug("reportId is " + this.reportId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the report is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved report from session");
                }
                this.report = (Report) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting report from database");
                }
                this.report = this.reportManager.get(this.reportId);
                this.report.setOldReportParameters(this.report
                        .getReportParameters());
                saveEntityInSession(this.report);
            }
            if (log.isDebugEnabled()) {
                log.debug("Report version is: " + this.report.getVersion());
            }
            if (!this.getRequest().getRequestURI().contains("getParameters")) {
                this.reportTypeId = report.getType().getId();
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new report object");
            }
            this.report = new Report();
        }

        this.setReportType(this.getReportTypeManager().get(getReportTypeId()));
        Iterator<ReportTypeParameter> params = this.getReportType()
                .getReportTypeParameters().iterator();
        while (params.hasNext()) {
            ReportTypeParameter param = params.next();
            if (param.getFieldType().isInSet(DropDownWithAll,
                    DropDownWithoutAll, ListField)) {
                // Build a drop-down list of values
                Map<String, String> values = callServiceMethod(param);
                param.setDropDownData(values);
            }
        }
    }

    /**
     * Delete the location identified by the <code>locationId</code> member.
     * 
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to
     *         the end user)
     * @throws Exception
     *             on unanticipated error condition
     */
    public String deleteCurrentReport() throws Exception {

        Report reportToDelete = null;

        try {
            reportToDelete = reportManager.get(this.reportId);
            reportManager.delete(this.reportId);

            addSessionActionMessage(new UserMessage(
                    "report.delete.message.success", reportToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete report: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * @return long[] - list of operator team ids
     */
    public String[] getOperatorTeams() {
        Set<ReportParameter> reportParameters = this.report
                .getReportParameters();
        Set<String> operatorTeamIds = new HashSet<String>();
        for (ReportParameter reportParameter : reportParameters) {
            if (reportParameter.getReportTypeParameter().getParameterName()
                    .equals("OPERATOR_TEAM_ID")) {
                operatorTeamIds.add(reportParameter.getValue());
            }
        }
        return operatorTeamIds.toArray(new String[0]);
    }

    /**
     * @return long[] - list of operator ids
     */
    public String[] getOperators() {
        Set<ReportParameter> reportParameters = this.report
                .getReportParameters();
        Set<String> operatorIds = new HashSet<String>();
        for (ReportParameter reportParameter : reportParameters) {
            if (reportParameter.getReportTypeParameter().getParameterName()
                    .equals("OPERATOR_ID")) {
                operatorIds.add(reportParameter.getValue());
            }
        }
        return operatorIds.toArray(new String[0]);

    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getReportManager();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "report";
    }

    /**
     * Getter for the REPORT_VIEW_ID property.
     * 
     * @return long value of the property
     */
    public static long getReportViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Get the <code>Interval</code>s for the current Site for display.
     * 
     * @return the Map of defined <code>Interval</code>s, where the map value is
     *         the ID of the interval, and the map key is the interval name.
     * @throws DataAccessException
     *             on failure to retrieve.
     */
    public Map<Integer, String> getIntervalMap() throws DataAccessException {
        Map<Integer, String> map = new TreeMap<Integer, String>();
        for (ReportInterval interval : ReportInterval.values()) {
            map.put(interval.getValue(),
                    getText("report.interval." + interval.name()));
        }
        return map;
    }

    /**
     * Get the <code>Interval</code>s for the current Site for display.
     * 
     * @return the Map of defined <code>Interval</code>s, where the map value is
     *         the ID of the interval, and the map key is the interval name.
     * @throws DataAccessException
     *             on failure to retrieve.
     */
    public Map<String, Integer> getFormatMap() throws DataAccessException {
        Map<String, Integer> map = new TreeMap<String, Integer>();
        for (ReportFormat format : ReportFormat.values()) {
            map.put(getText("report.format." + format.name()),
                    format.getValue());
        }
        return map;
    }

    /**
     * Get the <code>Interval</code>s for the current Site for display.
     * 
     * @return the Map of defined <code>Interval</code>s, where the map value is
     *         the ID of the interval, and the map key is the interval name.
     * @throws DataAccessException
     *             on failure to retrieve.
     */
    public Map<String, Long> getTypeMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>();
        List<ReportType> types = reportTypeManager.getAllByAppType(APP_TYPE);
        for (ReportType type : types) {
            map.put(getText(type.getReportTypeNameKey()), type.getId());
        }
        return map;
    }

    /**
     * Get the <code>Interval</code>s for the current Site for display.
     * 
     * @return the Map of defined <code>Interval</code>s, where the map value is
     *         the ID of the interval, and the map key is the interval name.
     * @throws DataAccessException
     *             on failure to retrieve.
     */
    public Map<Integer, String> getFrequencyMap() throws DataAccessException {
        Map<Integer, String> map = new TreeMap<Integer, String>();
        for (ReportFrequency frequency : ReportFrequency.values()) {
            map.put(frequency.getValue(), getText("report.frequency."
                    + frequency.name()));
        }
        return map;
    }

    /**
     * Get the time minutes for the current Site for display.
     * 
     * @return the Map of defined time minute intervals, where the map value is
     *         the number of minutes, and the map key is the number of minutes.
     */
    public Map<String, Integer> getScheduleMinutesMap() {
        Map<String, Integer> map = new TreeMap<String, Integer>();
        map.put("00", O_CLOCK);
        map.put("15", QUARTER_AFTER);
        map.put("30", HALF_PAST);
        map.put("45", QUARTER_TO);
        return map;
    }

    /**
     * Generates a map of the 24 hours in a day
     * 
     * @return - Map of each hour
     */
    public Map<Integer, Integer> getScheduleHoursMap() {
        Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
        for (Integer i = 0; i < HOURS_IN_A_DAY; i++) {
            map.put(i, i);
        }
        return map;
    }

    /**
     * Setter for the reportTypeManager property.
     * 
     * @param reportTypeManager
     *            the new reportTypeManager value
     */
    public void setReportTypeManager(ReportTypeManager reportTypeManager) {
        this.reportTypeManager = reportTypeManager;

    }

    /**
     * Getter for the reportTypeManager property.
     * 
     * @return the value of the property
     */
    public ReportTypeManager getReportTypeManager() {
        return this.reportTypeManager;

    }

    /**
     * Getter for the hours of the time property.
     * 
     * @return the value of the property
     */
    public Integer getTimeHours() {
        Calendar calendar = Calendar.getInstance();
        if (this.report.getTime() == null) {
            return 0;
        } else {
            calendar.setTime(this.report.getTime());
            return (calendar.get(Calendar.HOUR_OF_DAY));
        }
    }

    /**
     * Setter for the hours in the time property.
     * 
     * @param hour
     *            - the hour of the day
     */
    public void setTimeHours(Integer hour) {
        Calendar calendar = Calendar.getInstance();
        if (this.report.getTime() == null) {
            this.report.setTime(new Date());
        }
        calendar.setTime(this.report.getTime());
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        this.report.setTime(calendar.getTime());
    }

    /**
     * Getter for the minutes of the time property.
     * 
     * @return the value of the property
     */
    public Integer getTimeMinutes() {
        Calendar calendar = Calendar.getInstance();
        if (this.report.getTime() == null) {
            return 0;
        } else {
            calendar.setTime(this.report.getTime());
            return (calendar.get(Calendar.MINUTE));
        }
    }

    /**
     * Setter for the minutes of the time property.
     * 
     * @param minutes
     *            - the minutes of the hour
     */
    public void setTimeMinutes(Integer minutes) {
        Calendar calendar = Calendar.getInstance();
        if (this.report.getTime() == null) {
            this.report.setTime(new Date());
        }
        calendar.setTime(this.report.getTime());
        calendar.set(Calendar.MINUTE, minutes);
        this.report.setTime(calendar.getTime());
    }

    /**
     * Getter for the typeId property.
     * 
     * @return the value of the property
     */
    public Long getType() {
        if (this.report.getType() != null) {
            return this.report.getType().getId();
        } else {
            return null;
        }
    }

    /**
     * Setter for the typeId property.
     * 
     * @param typeId
     *            the new typeId value
     */
    public void setType(Long typeId) throws Exception {
        this.report.setType(this.getReportTypeManager().get(typeId));
    }

    /**
     * Sets the report type Id
     * 
     * @return - Struts SUCCESS value
     * @throws Exception
     */
    public String retrieveParameters() throws Exception {
        this.setReportType(reportTypeManager.get(this.getReportTypeId()));
        return SUCCESS;
    }

    /**
     * Getter for the reportTypeId property.
     * 
     * @return the value of the property
     */
    public Long getReportTypeId() {
        return this.reportTypeId;
    }

    /**
     * Setter for the reportTypeId property.
     * 
     * @param reportTypeId
     *            the new reportTypeId value
     */
    public void setReportTypeId(Long reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    /**
     * Setter for the reportType property.
     * 
     * @param reportType
     *            the new reportType value
     */
    public void setReportType(ReportType reportType) {
        this.reportType = reportType;

    }

    /**
     * Getter for the reportType property.
     * 
     * @return the value of the property
     */
    public ReportType getReportType() {
        return this.reportType;

    }

    /**
     * This will examine the service call fields in the report parameter and use
     * them to build a list of drop-down value pairs. It will attempt this by
     * using Java reflection.
     * 
     * @param param
     *            The report parameter with service call information.
     * @return The new list of values. Null if input is insufficient.
     * @throws VocollectException
     *             An exception will be thrown during any problem with the Java
     *             reflection and method invocation.
     */
    private Map<String, String> callServiceMethod(ReportTypeParameterRoot param)
            throws VocollectException {
        Map<String, String> values = new TreeMap<String, String>();
        // First, make sure we have all the required fields
        if (param.getServiceName() == null
                || param.getServiceName().length() < 1) {
            return values;
        }

        // Add a special first element representing ALL
        if (param.getFieldType().equals(DropDownWithAll)) {
            values.put(getText(LIST_ALL_DESCRIPTION), PARAM_LIST_ALL);
        }

        // Attempt to get a bean from the specified service class & method.
        boolean valid = applicationContext.containsBean(param.getServiceName());
        if (!valid) {
            throw new VocollectException(ReportError.SERVICE_BEAN_NOT_FOUND);
        } else if (param.getServiceMethod() == null
                || param.getServiceMethod().length() < 1
                || param.getDisplayMember() == null
                || param.getDisplayMember().length() < 1
                || param.getDataMember() == null
                || param.getDataMember().length() < 1) {
            return values;
        }
        // Try to actually get the bean
        Object service = applicationContext.getBean(param.getServiceName());
        if (service == null) {
            throw new VocollectException(ReportError.SERVICE_BEAN_NULL);
        }
        // Convert bean-style property into a "get" call
        String methodName = getBeanMethod(param.getServiceMethod());

        Method method = null;
        Collection<?> collectionResults = null;
        try {
            method = service.getClass().getMethod(methodName, new Class[] {});
            Object genericResults = method.invoke(service, (Object[]) null);
            collectionResults = (Collection<?>) genericResults;

            // Loop through the collection result
            Iterator<?> iter = collectionResults.iterator();
            while (iter.hasNext()) {
                Object item = iter.next();
                methodName = getBeanMethod(param.getDisplayMember());

                // Get display value
                method = item.getClass().getMethod(methodName, (Class[]) null);
                Object displayValObject = method.invoke(item, (Object[]) null);
                String displayVal = null;
                if (displayValObject != null) {
                    displayVal = displayValObject.toString();
                }

                // Get data value
                methodName = getBeanMethod(param.getDataMember());
                method = item.getClass().getMethod(methodName, (Class[]) null);
                Object dataValObject = method.invoke(item, (Object[]) null);
                String dataVal = null;
                if (dataValObject != null) {
                    dataVal = dataValObject.toString();
                }

                // Create the pair and add to the list
                if (log.isTraceEnabled()) {
                    log.trace("Display val : " + displayVal);
                }

                if (validSite(item)) {
                    values.put(displayVal, dataVal);
                }
            }
        } catch (NoSuchMethodException ex) {
            throw new VocollectException(ReportError.SERVICE_METHOD_NOT_FOUND,
                    ex);
        } catch (InvocationTargetException ex) {
            throw new VocollectException(ReportError.SERVICE_INVOCATION_ERROR,
                    ex);
        } catch (IllegalAccessException ex) {
            throw new VocollectException(ReportError.SERVICE_INVOCATION_ERROR,
                    ex);
        } catch (IllegalArgumentException ex) {
            throw new VocollectException(ReportError.SERVICE_INVOCATION_ERROR,
                    ex);
        } catch (ClassCastException ex) {
            throw new VocollectException(ReportError.SERVICE_NO_COLLECTION, ex);
        }
        return values;
    }

    /**
     * Take a bean-style property name ("name") and convert into a method call
     * ("getName").
     * 
     * @param beanProperty
     *            The property name to convert
     * @return The method name
     */
    private static String getBeanMethod(String beanProperty) {
        // Try to invoke the method, prepending "get" and capitalizing
        // the first letters.
        String methodName = "get"
                + Character.toUpperCase(beanProperty.charAt(0))
                + beanProperty.substring(1);
        return methodName;
    }

    /**
     * Check if object is taggable and for current site.
     * 
     * @param item
     *            - item to check site for
     * @return - True if not tagable, in current site, or current site is All
     *         Sites, otherwise return False
     * @throws DataAccessException
     *             - database exceptions
     */
    private boolean validSite(Object item) throws DataAccessException {

        // Check if item is taggable, and not all sites
        if (item instanceof Taggable
                && !SiteContextHolder.getSiteContext().isInAllSiteMode()) {
            Site site = SiteContextHolder.getSiteContext().getCurrentSite();
            return SiteContextHolder.getSiteContext().isInSite((Taggable) item,
                    site);
        } else {
            // not taggable or all sites so return true
            return true;
        }
    }

    /**
     * Getter for the applicationContext property.
     * 
     * @return the value of the property
     */
    public ApplicationContext getApplicationContext() {
        return this.applicationContext;
    }

    /**
     * Setter for the applicationContext property.
     * 
     * @param applicationContext
     *            the new applicationContext value
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Getter for the reportTypeParameterManager property.
     * 
     * @return the value of the property
     */
    public ReportTypeParameterManager getReportTypeParameterManager() {
        return this.reportTypeParameterManager;
    }

    /**
     * Setter for the reportTypeParameterManager property.
     * 
     * @param reportTypeParameterManager
     *            the new reportTypeParameterManager value
     */
    public void setReportTypeParameterManager(
            ReportTypeParameterManager reportTypeParameterManager) {
        this.reportTypeParameterManager = reportTypeParameterManager;
    }

    /**
     * Getter for the reportParameterManager property.
     * 
     * @return the value of the property
     */
    public ReportParameterManager getReportParameterManager() {
        return this.reportParameterManager;
    }

    /**
     * Setter for the reportParameterManager property.
     * 
     * @param reportParameterManager
     *            the new reportParameterManager value
     */
    public void setReportParameterManager(
            ReportParameterManager reportParameterManager) {
        this.reportParameterManager = reportParameterManager;
    }

    /**
     * Given the request url, get an absolute path to the report.
     * 
     * @param url
     *            containing the report path/name
     * @return Absolute path to file
     */
    public String parseReportPath(String url) {

        int i = url.indexOf(REPORT_NAMESPACE_ROOT)
                + REPORT_NAMESPACE_ROOT.length() + 1;
        int j = url.lastIndexOf("/");

        String reportPath = "";
        try {
            reportPath = url.substring(i, j);
        } catch (StringIndexOutOfBoundsException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Report path not found in " + url);
            }
        }

        return reportPath;
    }

    /**
     * Given the request url, get the report name.
     * 
     * @param url
     *            containing the report path/name
     * @return Absolute path to file
     */
    public String parseReportName(String url) {

        String reportPath = parseReportPath(url);

        int i = reportPath.lastIndexOf("/");

        String newReportName = reportPath.substring(i + 1);

        if (log.isDebugEnabled()) {
            log.debug("Report name: " + newReportName);
        }

        return newReportName;
    }

    /**
     * Helper function to get the start or end date from either the specified
     * date or the interval.
     * 
     * @param isStartDate
     *            - boolean to determine start or end
     * @return String - the interval date
     */
    private String parseIntervalDate(boolean isStartDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        if (isStartDate) {
            if (this.getStartDate() != null) {
                return this.getStartDate()  + "-00";
            } else {
                return sdf.format(this.report.getInterval().getStartDate())  + "-00";
            }
        } else {
            if (this.getEndDate() != null) {
                return this.getEndDate()  + "-00";
            } else {
                return sdf.format(this.report.getInterval().getEndDate())  + "-00";
            }
        }
    }

    /**
     * This method is invoked, whenever a request is received for report
     * generation along with the requisite parameters. Once invoked, report
     * definition validity is verified using the report wrapper. 404 error is
     * thrown, if the definition does not exist. ReportWrapper is further
     * invoked to generate the report as Byte stream using the user submitted
     * parameter values.
     * 
     * @return Lower case file extension as per the report format value selected
     *         by the user. Valid return values are <code>pdf,
     *         rtf, html</code> and <code>xls</code>
     * @throws VocollectException
     *             Thrown when bean could not be located using the bean id
     *             specified.
     * @throws ReportException
     *             Thrown when there is an error encountered while report
     *             generation. Specific report implementation class must be
     *             debugged for the same
     */
    public String launch() throws VocollectException, ReportException {
        String absolutePath = servletContext.getRealPath(REPORT_PATH_BASE);

        String reportName = this.report.getType().getReportTypeName();
        if (log.isInfoEnabled()) {
            log.info("Generating report: " + reportName + " from "
                    + absolutePath);
        }
        ReportWrapper reportWrapper = new JasperReportWrapper(absolutePath,
                reportName + JASPER_REPORT_SOURCE_EXTENSION);

        // Check and initialize if the report exists
        if (!reportWrapper.reportExists(absolutePath, reportName
                + JASPER_REPORT_SOURCE_EXTENSION)) {
            log.warn("Report definition " + reportName
                    + JASPER_REPORT_SOURCE_EXTENSION + " NOT found at: "
                    + absolutePath);
            return ERROR_404_RETURN;
        }

        Map<String, Object> reportValues = getReportValues();
        Set<ReportParametersDTO> reportParams = convertToDTO(this.report
                .getReportParameters());

        // Build Parameters value map
        Map<String, Object> values = getReportValuesMap(
                reportWrapper.getParameters(), reportParams, reportValues);
        values.put(JASPER_REPORT_LOCALE, getLocale());
        values.put(JASPER_REPORT_TIMEZONE, getCurrentSiteTimeZone());
        values.put(TIME_ZONE, Calendar.getInstance().getTimeZone());

        setReportOperatorIdentifiers(reportParams, values);
        setReportOperatorTeams(reportParams, values);
        setAllTeamOperators(values);
        try {
            if (this.getStartDate() != null) {
                getTimeZoneAdjustedDate(values);
                values.put(ReportUtilities.IS_INTERVAL, Boolean.FALSE);
            } else {
                values.put(ReportUtilities.IS_INTERVAL, Boolean.TRUE);
            }
        } catch (ParseException e) {
            log.warn("Error parsing Start/end dates");
        }
        Object dataSource = getDataSource(values); // Set data source

        // get render format
        String renderFormat = this.report.getFormat().toString();

        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);

        format.setLenient(false);
        try {
            Boolean isInterval = (Boolean) values.get("IS_INTERVAL");
            Date displayStartDate = new Date();
            Date displayEndDate = new Date();
            if (isInterval) {
                displayStartDate = getDisplayDateforInterval(true);
                displayEndDate = getDisplayDateforInterval(false);
            } else {
                displayStartDate = format.parse(parseIntervalDate(true));
                displayEndDate = format.parse(parseIntervalDate(false));
            }

            values.put(START_DATE, displayStartDate);
            values.put(END_DATE, displayEndDate);
        } catch (Exception e) {
            log.warn("Error parsing Start/end dates");
        }

        if (log.isDebugEnabled()) {
            log.debug("Invoking report generation for report:" + reportName
                    + ", format:" + renderFormat + ", language:"
                    + getLocale().getLanguage());
        }
        InputStream reportInputStream = reportWrapper.generateReport(values,
                renderFormat, getLocale().getLanguage(), dataSource);
        this.reportStream = reportInputStream;

        if (log.isInfoEnabled()) {
            log.info("Report:" + this.report.getName() + " generated.");
        }
        // Return lower-case version of file extension as result
        return renderFormat.toLowerCase();
    }

    /**
     * @param isStartDate 
     *               - boolean value if the date is start date
     *
     * @return Date  value
     */
    private Date getDisplayDateforInterval(boolean isStartDate) {
        Date displayDate = new Date();
        GregorianCalendar cal = new GregorianCalendar();
        if (isStartDate) {
            cal.setTime(this.report.getInterval().getStartDate());
        } else {
            cal.setTime(this.report.getInterval().getEndDate());
        }
        displayDate = DateUtil.convertDateTimeZone(cal, TimeZone.getDefault(),
                SiteContextHolder.getSiteContext().getCurrentSite()
                        .getTimeZone());
        return displayDate;
    }

    /**
     * Method to convert start/end time to site timezone if time span selected
     * in report launcher
     * 
     * @param values
     *            - report parameters value map
     * @throws ParseException
     */
    private void getTimeZoneAdjustedDate(Map<String, Object> values)
            throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        format.setLenient(false);
        Calendar cal = Calendar.getInstance();

        TimeZone siteTimeZone = SiteContextHolder.getSiteContext()
                .getCurrentSite().getTimeZone();
        TimeZone systemTimeZone = Calendar.getInstance().getTimeZone();

        // Start date
        Date dateVal = format.parse(parseIntervalDate(true));
        cal.setTime(dateVal);
        dateVal = DateUtil.convertDateTimeZone(cal, siteTimeZone,
                systemTimeZone);
        values.put(START_DATE, dateVal);

        // End date
        dateVal = format.parse(parseIntervalDate(false));
        cal.setTime(dateVal);
        dateVal = DateUtil.convertDateTimeZone(cal, siteTimeZone,
                systemTimeZone);
        values.put(END_DATE, dateVal);
    }

    /**
     * Method to get current site's time zone for formatting reports data times.
     * 
     * @return TimeZone current site time zone
     */
    private TimeZone getCurrentSiteTimeZone() {
        return SiteContextHolder.getSiteContext().getCurrentSite()
                .getTimeZone();
    }

    /**
     * Method to add identifiers of all operators to the values map for querying
     * archive database
     * 
     * @param values
     *            Parameter name-value map
     * @throws DataAccessException
     *             Thrown when operator team is not retrieved using
     *             operatorTeamManager
     */
    private void setAllTeamOperators(Map<String, Object> values)
            throws DataAccessException {

        Set<String> operatorIdAll = new HashSet<String>();

        Set<String> operatorId = (Set<String>) values.get(OPERATOR_IDENTIFIERS);
        String op = (String) values.get(OPERATOR);
        if (!StringUtil.isNullOrEmpty(op)) {
            operatorIdAll.addAll(operatorId);
        }

        Long opTeam = (Long) values.get(OPERATOR_TEAM);
        if (opTeam != null) {
            Set<Long> operatorTeamIds = (Set<Long>) values
                    .get(OPERATOR_TEAM_ID);
            for (Long operatorTeamId : operatorTeamIds) {
                OperatorTeam operatorTeam = this.operatorTeamManager
                        .get(operatorTeamId);
                Set<Operator> operators = operatorTeam.getOperators();
                for (Operator operator : operators) {
                    operatorIdAll.add(operator.getOperatorIdentifier());
                }
            }
        }

        String operatorIds = null;
        if (operatorIdAll.isEmpty()) {
            operatorIdAll.add(" ");
        } else {
            operatorIds = operatorIdAll.iterator().next();
        }
        values.put(OPERATOR_IDENTIFIERS_ALL, operatorIdAll);
        values.put(OPERATOR_ALL, operatorIds);
    }

    /**
     * Method to get the datasource object
     * 
     * @param values
     *            Map of report values <code>key</code> Parameter name <br>
     *            <code>value</code> Parameter value
     * @return Datasource object. Could be either of type
     *         ReportDataSourceManager or null
     * @throws VocollectException
     *             Thrown when error encountered while getting datasource of
     *             type ReportDataSourceManager or bean associated object is not
     *             of either type SessionFactory and ReportDataSourceManager
     */
    private Object getDataSource(Map<String, Object> values)
            throws VocollectException {

        if (log.isDebugEnabled()) {
            log.debug("Get data source bean with bean id=" + getSessionBeanId()
                    + ", report=" + this.report.getName());
        }
        Object dataSource = getReportDataSourceBean();
        if (dataSource instanceof SessionFactory) {
            // Set hibernate session
            Session session = ((SessionFactory) dataSource).openSession();
            values.put(JASPER_HIBERNATE_SESSION_PARAM, session);
            dataSource = null;
            if (log.isInfoEnabled()) {
                log.info("Hibernate session with id : " + getSessionBeanId()
                        + " to be used, report=" + this.report.getName());
            }
        } else if ((dataSource instanceof ReportDataSourceManager)) {
            try {
                dataSource = ((ReportDataSourceManager) dataSource)
                        .getDataSource(values);
                if (log.isInfoEnabled()) {
                    log.info("datasource = ReportDataSourceManager"
                            + " , report=" + this.report.getName());
                }
            } catch (Exception ex) {
                throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
            }
        } else {
            log.error("Unknown session id:" + getSessionBeanId()
                    + " for report " + this.report.getName(),
                    ReportError.DATASOURCE_BEAN_UNKOWN_ERROR);
            throw new VocollectException(
                    ReportError.DATASOURCE_BEAN_UNKOWN_ERROR);
        }

        return dataSource;
    }

    /**
     * Method to get list of saved records, which are to be shown as selected on
     * the reports page
     * 
     * @param parameterName
     *            name of the field of which selected values are to be retrieved
     *            and returned
     * @return Array of values retrieved from the system.
     */
    public String[] getSelectedValues(String parameterName) {
        if (!StringUtil.isNullOrEmpty(parameterName)) {
            if (parameterName.equals("operators")) {
                return getOperators();
            } else if (parameterName.equals("operatorTeams")) {
                return getOperatorTeams();
            }
        }
        String[] emptyArray = { "" };
        return emptyArray;
    }

    /**
     * Utility method to collect all user submitted parameters values in a map
     * of values to be used for invocation of report generation
     * 
     * @return Map of user submitted values. <code>key</code> Parameter name <br>
     *         <code>value</code> Parameter value
     */
    private Map<String, Object> getReportValues() {
        Map<String, Object> values = new HashMap<String, Object>();
        values.put(PARAM_CURRENT_SITE_CONTEXT, getSiteName());
        values.put(PARAM_CURRENT_USER, getUserName());
        Object sessionValue = getSession().getAttribute("SESSION_VALUE1");
        if (sessionValue != null) {
            values.put(SESSION_VALUE,
                    getSession().getAttribute("SESSION_VALUE1").toString());
        }
        values.put(SITE, SiteContextHolder.getSiteContext().getCurrentSite()
                .getId().toString());
        values.put(START_DATE, parseIntervalDate(true));
        values.put(END_DATE, parseIntervalDate(false));
        values.put(REPORT_NAME, this.report.getName());
        values.put(ENUM, enumValues);
        return values;
    }

    /**
     * Method to convert ReportParameter object to ReportParametersDTO object,
     * so that it can be used in util classes
     * 
     * @param reportParameters
     *            Set of ReportParameter objects associated with the report
     *            object
     * @return Set of ReportParametersDTO objects adapted from ReportParameter
     *         objects
     */
    private Set<ReportParametersDTO> convertToDTO(
            Set<ReportParameter> reportParameters) {
        Set<ReportParametersDTO> paramList = new HashSet<ReportParametersDTO>(
                reportParameters.size());
        ReportParametersDTO param;
        for (ReportParameter reportParam : reportParameters) {

            param = new ReportParametersDTO();
            param.setDescription(reportParam.getDescriptiveText());
            param.setName(reportParam.getReportTypeParameter()
                    .getParameterName());
            param.setValue(reportParam.getValue());
            param.setValueClass(reportParam.getClass());
            paramList.add(param);
        }

        return paramList;
    }

    /**
     * Find the bean data source specified for report.
     * 
     * @return - Bean object obtained from applicationContext
     * @throws VocollectException
     *             - If required bean is not found in the applicationContext
     */
    private Object getReportDataSourceBean() throws VocollectException {
        // Get the hibernate session from our context
        if (!applicationContext.containsBean(this.report.getType()
                .getSessionBeanId())) {
            throw new VocollectException(
                    ReportError.DATASOURCE_BEAN_NOTFOUND_ERROR);
        }
        return applicationContext.getBean(this.report.getType()
                .getSessionBeanId());
    }

    /**
     * Getter for the servletContext property.
     * 
     * @return the value of the property
     */
    public ServletContext getServletContext() {
        return this.servletContext;
    }

    /**
     * Setter for the servletContext property.
     * 
     * @param servletContext
     *            the new servletContext value
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Getter for the path property.
     * 
     * @return the value of the property
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Setter for the path property.
     * 
     * @param path
     *            the new path value
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Getter for the reportNamespace property.
     * 
     * @return the value of the property
     */
    public String getReportNamespace() {
        return this.reportNamespace;
    }

    /**
     * Setter for the reportNamespace property.
     * 
     * @param reportNamespace
     *            the new reportNamespace value
     */
    public void setReportNamespace(String reportNamespace) {
        this.reportNamespace = reportNamespace;
    }

    /**
     * Getter for the userName property.
     * 
     * @return the value of the property
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Setter for the userName property.
     * 
     * @param userName
     *            the new userName value
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Getter for the siteName property.
     * 
     * @return the value of the property
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * Setter for the siteName property.
     * 
     * @param siteName
     *            the new siteName value
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * Getter for the userManager property.
     * 
     * @return the value of the property
     */
    public static UserManager getUserManager() {
        return userManager;
    }

    /**
     * Setter for the userManager property.
     * 
     * @param userManager
     *            the new userManager value
     */
    public static void setUserManager(UserManager userManager) {
        ReportActionRoot.userManager = userManager;
    }

    /**
     * Getter for the reportStream property.
     * 
     * @return the value of the property
     */
    public InputStream getReportStream() {
        return this.reportStream;
    }

    /**
     * Setter for the reportStream property.
     * 
     * @param reportStream
     *            the new reportStream value
     */
    public void setReportStream(InputStream reportStream) {
        this.reportStream = reportStream;
    }

    /**
     * Getter for the sessionFactory property.
     * 
     * @return the value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * 
     * @param sessionFactory
     *            the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Getter for the sessionBeanId property.
     * 
     * @return the value of the property
     */
    public String getSessionBeanId() {
        return this.sessionBeanId;
    }

    /**
     * Setter for the sessionBeanId property.
     * 
     * @param sessionBeanId
     *            the new sessionBeanId value
     */
    public void setSessionBeanId(String sessionBeanId) {
        this.sessionBeanId = sessionBeanId;
    }

    /**
     * Getter for the siteId property.
     * 
     * @return the value of the property
     */
    public static long getSiteId() {
        return siteId;
    }

    /**
     * Setter for the siteId property.
     * 
     * @param siteId
     *            the new siteId value
     */
    public static void setSiteId(long siteId) {
        ReportActionRoot.siteId = siteId;
    }

    /**
     * Setter for the startDate property.
     * 
     * @param startDate
     *            the new startDate value
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Setter for the endDate property.
     * 
     * @param endDate
     *            the new endDate value
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter for the startDate property.
     * 
     * @return the value of the property
     */
    public String getStartDate() {
        return this.startDate;
    }

    /**
     * Getter for the endDate property.
     * 
     * @return the value of the property
     */
    public String getEndDate() {
        return this.endDate;
    }

    /**
     * Getter for the operatorManager property.
     * 
     * @return the value of the property
     */
    public OperatorManager getOperatorManager() {
        return this.operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * 
     * @param operatorManager
     *            the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Getter for the operatorTeamManager property.
     * 
     * @return OperatorTeamManager value of the property
     */
    public OperatorTeamManager getOperatorTeamManager() {
        return operatorTeamManager;
    }

    /**
     * Setter for the operatorTeamManager property.
     * 
     * @param operatorTeamManager
     *            the new operatorTeamManager value
     */
    public void setOperatorTeamManager(OperatorTeamManager operatorTeamManager) {
        this.operatorTeamManager = operatorTeamManager;
    }

    /**
     * Getter for the readOnly property.
     * 
     * @return the value of the property
     */
    public Boolean getReadOnly() {
        return this.readOnly;
    }

    /**
     * Setter for the readOnly property.
     * 
     * @param readOnly
     *            the new readOnly value
     */
    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    /**
     * Getter for the fieldErr property.
     * 
     * @return the value of the property
     */
    public String getFieldErr() {
        return this.fieldErr;
    }

    /**
     * Setter for the fieldErr property.
     * 
     * @param fieldErr
     *            the new fieldErr value
     */
    public void setFieldErr(String fieldErr) {
        this.fieldErr = fieldErr;
    }

    /**
     * Overriden to take care of scenarios arising due to the addition of assign
     * operator functionality.
     * 
     * @return whether or not this is a text action.
     */
    @Override
    public boolean isTextAction() {
        if (getActionName().toLowerCase().endsWith("schedule".toLowerCase())) {
            return true;
        } else {
            return super.isTextAction();
        }
    }

    /**
     * @return the name of this action, without the action piece.
     */
    private String getActionName() {
        // TODO: I think this can be done directly with the Struts
        // ActionContext. -- ddoubleday.
        String fullURI = getRequest().getRequestURI();
        int startIndex = fullURI.lastIndexOf('/') + 1;
        if (startIndex == -1) {
            return null;
        }

        int endIndex;
        if (fullURI.contains("!input")) {
            endIndex = fullURI.indexOf("!input", startIndex);
        } else {
            endIndex = fullURI.indexOf(".action", startIndex);
        }
        if (endIndex == -1) {
            return null;
        }

        return fullURI.substring(startIndex, endIndex);
    }

    /**
     * Method to return region lists based on the action selected for the
     * Operator Top / Bottom Performance report
     * 
     * @return JSON object with a list of region ids and names.
     * @throws DataAccessException
     */
    public String getPerfReportRegions() throws DataAccessException {
        JSONObject jo = new JSONObject(true);
        final int regionParamID = -1206;
        // Integer id = Integer.parseInt(this.actionId);
        ArrayList<Region> regions = getPerfReportRegionArr();
        try {
            // Add the default selected value to the JSON object
            try {
                setReport(reportManager.get(getReportId()));
                // If the report has parameters (not new)
                if (report.getReportParameters() != null) {
                    Iterator<ReportParameter> iter = report
                            .getReportParameters().iterator();
                    while (iter.hasNext()) {
                        ReportParameter param = iter.next();
                        if (param.getReportTypeParameter().getId() == regionParamID) {
                            jo.put("DEFAULT", param.getValue());
                        }
                    }
                }
            } catch (EntityNotFoundException e) {
                // Report not found because it is being created.
            }
            for (Region region : regions) {
                if (this.getActionId().equalsIgnoreCase("_ALL_")) {
                    String regionType = ResourceUtil
                            .getLocalizedKeyValue(region.getType()
                                    .getResourceKey());
                    jo.put(region.getId().toString(), regionType + " - "
                            + region.getName());
                } else {
                    jo.put(region.getId().toString(), region.getName());
                }
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        this.setJsonMessage(jo.toString());
        return SUCCESS;
    }

    /**
     * @return an array of regions for the performance report
     * @throws DataAccessException
     */
    public ArrayList<Region> getPerfReportRegionArr()
            throws DataAccessException {
        ArrayList<Region> regions = null;
        try {
            Integer id = Integer.parseInt(this.actionId);
            switch (OperatorLaborActionType.toEnum(id)) {
            case Selection:
                regions = new ArrayList<Region>(selRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            case Replenishment:
                regions = new ArrayList<Region>(repRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            case PutAway:
                regions = new ArrayList<Region>(putRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            case LineLoading:
                regions = new ArrayList<Region>(llRegionManager.getPrimaryDAO()
                        .listAllRegionsOrderByNumber());
                break;
            case PutToStore:
                regions = new ArrayList<Region>(ptsRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            case CycleCounting:
                regions = new ArrayList<Region>(cycleCountingRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            case Loading:
                regions = new ArrayList<Region>(loadingRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                break;
            default:
                regions = new ArrayList<Region>(selRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber());
                regions.addAll(new ArrayList<Region>(repRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                regions.addAll(new ArrayList<Region>(putRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                regions.addAll(new ArrayList<Region>(llRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                regions.addAll(new ArrayList<Region>(ptsRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                regions.addAll(new ArrayList<Region>(cycleCountingRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                regions.addAll(new ArrayList<Region>(loadingRegionManager
                        .getPrimaryDAO().listAllRegionsOrderByNumber()));
                break;
            }
        } catch (NumberFormatException x) {
            // If the action id is not recognizable, fail politely. (Return all
            // regions)
            regions = new ArrayList<Region>(selRegionManager.getPrimaryDAO()
                    .listAllRegionsOrderByNumber());
            regions.addAll(new ArrayList<Region>(repRegionManager
                    .getPrimaryDAO().listAllRegionsOrderByNumber()));
            regions.addAll(new ArrayList<Region>(putRegionManager
                    .getPrimaryDAO().listAllRegionsOrderByNumber()));
            regions.addAll(new ArrayList<Region>(llRegionManager
                    .getPrimaryDAO().listAllRegionsOrderByNumber()));
            regions.addAll(new ArrayList<Region>(ptsRegionManager
                    .getPrimaryDAO().listAllRegionsOrderByNumber()));
            regions.addAll(new ArrayList<Region>(cycleCountingRegionManager
                    .getPrimaryDAO().listAllRegionsOrderByNumber()));
        }
        return regions;
    }

    /**
     * Getter for the actionId property.
     * 
     * @return the value of the property
     */
    public String getActionId() {
        return this.actionId;
    }

    /**
     * Setter for the actionId property.
     * 
     * @param actionId
     *            the new actionId value
     */
    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    /**
     * Getter for the selRegionManager property.
     * 
     * @return the value of the property
     */
    public SelectionRegionManager getSelRegionManager() {
        return this.selRegionManager;
    }

    /**
     * Setter for the selRegionManager property.
     * 
     * @param selRegionManager
     *            the new selRegionManager value
     */
    public void setSelRegionManager(SelectionRegionManager selRegionManager) {
        this.selRegionManager = selRegionManager;
    }

    /**
     * Getter for the ptsRegionManager property.
     * 
     * @return the value of the property
     */
    public PtsRegionManager getPtsRegionManager() {
        return this.ptsRegionManager;
    }

    /**
     * Setter for the ptsRegionManager property.
     * 
     * @param ptsRegionManager
     *            the new ptsRegionManager value
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     * Getter for the repRegionManager property.
     * 
     * @return the value of the property
     */
    public ReplenishmentRegionManager getRepRegionManager() {
        return this.repRegionManager;
    }

    /**
     * Setter for the repRegionManager property.
     * 
     * @param repRegionManager
     *            the new repRegionManager value
     */
    public void setRepRegionManager(ReplenishmentRegionManager repRegionManager) {
        this.repRegionManager = repRegionManager;
    }

    /**
     * Getter for the putRegionManager property.
     * 
     * @return the value of the property
     */
    public PutawayRegionManager getPutRegionManager() {
        return this.putRegionManager;
    }

    /**
     * Setter for the putRegionManager property.
     * 
     * @param putRegionManager
     *            the new putRegionManager value
     */
    public void setPutRegionManager(PutawayRegionManager putRegionManager) {
        this.putRegionManager = putRegionManager;
    }

    /**
     * Getter for the llRegionManager property.
     * 
     * @return the value of the property
     */
    public LineLoadingRegionManager getLlRegionManager() {
        return this.llRegionManager;
    }

    /**
     * Setter for the llRegionManager property.
     * 
     * @param llRegionManager
     *            the new llRegionManager value
     */
    public void setLlRegionManager(LineLoadingRegionManager llRegionManager) {
        this.llRegionManager = llRegionManager;
    }

    /**
     * Getter for the ccRegionManager property.
     * 
     * @return CycleCountingRegionManager value of the property
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * Setter for the ccRegionManager property.
     * 
     * @param ccRegionManager
     *            the new ccRegionManager value
     */
    public void setCycleCountingRegionManager(
            CycleCountingRegionManager ccRegionManager) {
        this.cycleCountingRegionManager = ccRegionManager;
    }

    /**
     * Getter for the loadingRegionManager property.
     * 
     * @return LoadingRegionManager value of the property
     */
    public LoadingRegionManager getLoadingRegionManager() {
        return loadingRegionManager;
    }

    /**
     * Setter for the loadingRegionManager property.
     * 
     * @param loadingRegionManager
     *            the new ccRegionManager value
     */
    public void setLoadingRegionManager(
            LoadingRegionManager loadingRegionManager) {
        this.loadingRegionManager = loadingRegionManager;
    }

    /**
     * @param systemTranslationManager
     *            systemTranslationManager
     */
    public void setSystemTranslationManager(
            SystemTranslationManager systemTranslationManager) {
        this.systemTranslationManager = systemTranslationManager;
    }

    /**
     * @return system translation manager
     */
    public SystemTranslationManager getSystemTranslationManager() {
        return systemTranslationManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 