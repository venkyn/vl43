/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;



import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.service.LocationManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;


/**
 * This is the Struts action class that handles operations on <code>Location</code>
 * objects.
 *
 * @author Brian Rupert
 * @author snalan
 */
public class LocationActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 6478026686916495773L;

    private static final Logger log = new Logger(LocationActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1007;

    // The list of columns for the operators table.
    private List<Column> columns;

    // The Operator management service.
    private LocationManager locationManager = null;

    //private LocationManager locManager;

    // The Operator object, which will either be newly created, or retrieved
    // via the OperatorId.
    private Location location;

    // The ID of the Operator.
    private Long locationId;

    /**
     * Getter for the location property.
     * @return User value of the property
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Getter for the locationId property.
     * @return Long value of the property
     */
    public Long getLocationId() {
        return this.locationId;
    }

    /**
     * Setter for the locationId property.
     * @param locationId the new locationId value
     */
    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    /**
     * Getter for the locationManager property.
     * @return UserManager value of the property
     */
    public LocationManager getLocationManager() {
        return this.locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param manager the new locationManager value
     */
    public void setLocationManager(LocationManager manager) {
        this.locationManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getLocationColumns() {
        return this.columns;
    }

    /**
     * Action for the locations view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View locationView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(locationView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the location specified by the <code>location</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
       boolean isNew = this.location.isNew();

        try {
            Long scannedVerificationUniquenessId =
                locationManager.getPrimaryDAO().uniquenessByScannedVerification(
                    this.location.getScannedVerification());
            if (scannedVerificationUniquenessId != null
                && (isNew || (!isNew && scannedVerificationUniquenessId.longValue() != location.getId().longValue()))) {
                log.warn("Location '" + this.location.getScannedVerification()
                    + "' already exists");
                addFieldError("location.scannedVerification", new UserMessage(
                    "location.create.error.existing", this.location.getScannedVerification()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            locationManager.save(this.location);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified location "
                + this.location.getScannedVerification());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Location"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Location modifiedItem = getLocationManager().get(getLocationId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.location.setVersion(modifiedItem.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedItem);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(
                    new UserMessage("entity.error.deleted",
                        UserMessage.markForLocalization(this.location.getScannedVerification().toString()),
                        null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "location." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/location/view.action?locationId=" + this.location.getId()),
                this.location.getScannedVerification()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Delete the location identified by the <code>locationId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentLocation() throws Exception {

        Location locationToDelete = null;

        try {
            locationToDelete = locationManager.get(this.locationId);
            locationManager.delete(this.locationId);

            addSessionActionMessage(new UserMessage(
                "location.delete.message.success", locationToDelete.getScannedVerification()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete location: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }


    /**
     * This method sets up the <code>Location</code> object by retrieving it
     * from the database when a locationId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
       if (this.locationId != null) {
            // We have an ID, but not an Item object yet.
            if (log.isDebugEnabled()) {
                log.debug("locationId is " + this.locationId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the User is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved location from session");
                }
                this.location = (Location) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting location from database");
                }
                this.location = this.locationManager.get(this.locationId);
                saveEntityInSession(this.location);
            }
            if (log.isDebugEnabled()) {
                log.debug("Location version is: " + this.location.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new location object");
            }
            this.location = new Location();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getLocationManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "location";
    }

    /**
     * Getter for the LOCATION_VIEW_ID property.
     * @return long value of the property
     */
    public static long getLocationViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }
    /**
     * Sets selected items location status to Replenished.
     * @return Success - on success
     * @throws Exception on unanticipated error condition
     */
    public String setToReplenished() throws Exception {

        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "setToReplenished";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                Location li = (Location) obj;
                getLocationManager().executeReportReplenished(li, null);
                return true;
            }
        });
    }

    /**
     * Clears invalid lot from selected items location.
     * @return Success - on success
     * @throws Exception on unanticipated error condition
     */
    public String clearInvalidLot() throws Exception {

        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "clearInvalidLot";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                Location li = (Location) obj;
                getLocationManager().executeClearInvalidLot(li, null);
                //getLocationManager().executeReportReplenished(li, null);
                return true;
            }
        });
    }

    /**
     * @return the current Location preAisle direction, translated for the
     *         current Locale, or the untranslated direction if the Location
     *         preAisle direction isn't translated for this Locale, or
     *         <code>null</code> if the current Location isn't defined.
     */
    public String getTranslatedPreAisle() {
        if (getLocation() == null) {
            return null;
        } else {
            return getDataTranslation(getLocation().getPreAisle());
        }
    }

    /**
     * @return the current Location preAisle direction, translated for the
     *         current Locale, or <code>null</code> if the Location preAisle
     *         direction isn't translated for this Locale, or if the current
     *         Location isn't defined.
     */
    public String getTranslatedPreAisleOrNull() {
        if (getLocation() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getLocation().getPreAisle());
        }
    }

    /**
     * @return the message containing the Locale and the translated Location
     *         preAisle direction, or null if there is no translation.
     */
    public String getTranslatedPreAisleMessage() {
        return getDataTranslationMessage(getTranslatedPreAisleOrNull());
    }

    /**
     * @return the current Location postAisle direction, translated for the
     *         current Locale, or the untranslated direction if the Location
     *         postAisle direction isn't translated for this Locale, or
     *         <code>null</code> if the current Location isn't defined.
     */
    public String getTranslatedPostAisle() {
        if (getLocation() == null) {
            return null;
        } else {
            return getDataTranslation(getLocation().getPostAisle());
        }
    }

    /**
     * @return the current Location postAisle direction, translated for the
     *         current Locale, or <code>null</code> if the Location postAisle
     *         direction isn't translated for this Locale, or if the current
     *         Location isn't defined.
     */
    public String getTranslatedPostAisleOrNull() {
        if (getLocation() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getLocation().getPostAisle());
        }
    }

    /**
     * @return the message containing the Locale and the translated Location
     *         postAisle direction, or null if there is no translation.
     */
    public String getTranslatedPostAisleMessage() {
        return getDataTranslationMessage(getTranslatedPostAisleOrNull());
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 