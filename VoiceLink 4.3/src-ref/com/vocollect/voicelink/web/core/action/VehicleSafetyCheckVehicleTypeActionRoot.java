/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleCategory;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleTypeManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 
 * @author khazra
 */
@SuppressWarnings("serial")
public class VehicleSafetyCheckVehicleTypeActionRoot extends DataProviderAction
    implements Preparable {

    private static final Logger log = new Logger(
        VehicleSafetyCheckVehicleTypeActionRoot.class);

    private static final long VEHICLETYPE_VIEW_ID = -1225;

    private static final long VEHICLE_VIEW_ID = -1226;

    private VehicleTypeManager vehicleTypeManager;

    private VehicleManager vehicleManager;

    private List<Column> vehicleTypeColumns;

    private List<Column> vehicleColumns;

    private VehicleType vehicleType;
    
    private Long typeId;

    /**
     * @return the vehicleTypeManager
     */
    public VehicleTypeManager getVehicleTypeManager() {
        return vehicleTypeManager;
    }

    /**
     * @param vehicleTypeManager the vehicleTypeManager to set
     */
    public void setVehicleTypeManager(VehicleTypeManager vehicleTypeManager) {
        this.vehicleTypeManager = vehicleTypeManager;
    }

    /**
     * @return the vehicleManager
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    /**
     * @param vehicleManager the vehicleManager to set
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    /**
     * Getter for the vehicleType property.
     * @return VehicleType value of the property
     */
    public VehicleType getVehicleType() {
        return vehicleType;
    }

    /**
     * Setter for the vehicleType property.
     * @param vehicleType the new vehicleType value
     */
    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }
    
    
    /**
     * Getter for the typeId property.
     * @return Long value of the property
     */
    public Long getTypeId() {
        return typeId;
    }

    
    /**
     * Setter for the typeId property.
     * @param typeId the new typeId value
     */
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        if (this.typeId == null) {
            this.vehicleType = new VehicleType();
            this.vehicleType.setIsCaptureVehicleID(true);
        } else {
            // We have an ID, but not a vehicleType object yet.
            if (log.isDebugEnabled()) {
                log.debug("vehicleTypeID is " + this.typeId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
            // This means the vehicle type is being edited, so
            // get it from the session.
            if (log.isDebugEnabled()) {
                log.debug("Getting saved vehicle type from session");
            }
            this.vehicleType = (VehicleType) getEntityFromSession(getSavedEntityKey());
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Getting vehicle type from database");
            }
            this.vehicleType = this.vehicleTypeManager.get(typeId);
            saveEntityInSession(this.vehicleType);
        }
        if (log.isDebugEnabled()) {
            log.debug("Vehicle type version is: "
                + this.vehicleType.getVersion());
        }
        }
    }

    
    /**
     * Delete the vehicle type identified by the <code>typeId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified vehicle type wasn't found (with different messages
     *         to the end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentVehicleType() throws Exception {

        VehicleType vehicleTypeToDelete = null;

        try {
            vehicleTypeToDelete = vehicleTypeManager.get(this.typeId);
            vehicleTypeManager.delete(this.typeId);

            addSessionActionMessage(new UserMessage(
                "vehicleType.delete.message.success", vehicleTypeToDelete.getDescription()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete vehicle type: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }
    
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getVehicleTypeManager();
    }

    @Override
    protected String getKeyPrefix() {
        return "vehicleType";
    }

    /**
     * 
     * @return long value of the property
     */
    public static long getVehicleTypeViewId() {
        return VEHICLETYPE_VIEW_ID;
    }

    /**
     * 
     * @return long value of the property
     */
    public static long getVehicleViewId() {
        return VEHICLE_VIEW_ID;
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VEHICLETYPE_VIEW_ID);
        viewIds.add(VEHICLE_VIEW_ID);
        return viewIds;
    }

    /**
     * @return SUCCESS
     * @throws Exception
     */
    public String list() throws Exception {
        View vehicleTypeView = getUserPreferencesManager().getView(
            VEHICLETYPE_VIEW_ID);
        this.vehicleTypeColumns = this.getUserPreferencesManager().getColumns(
            vehicleTypeView, getCurrentUser());
        View stopView = getUserPreferencesManager().getView(VEHICLE_VIEW_ID);
        this.vehicleColumns = this.getUserPreferencesManager().getColumns(
            stopView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * @return the vehicleTypeColumns
     */
    public List<Column> getVehicleTypeColumns() {
        return vehicleTypeColumns;
    }

    /**
     * @param vehicleTypeColumns the vehicleTypeColumns to set
     */
    public void setVehicleTypeColumns(List<Column> vehicleTypeColumns) {
        this.vehicleTypeColumns = vehicleTypeColumns;
    }

    /**
     * @return the vehicleColumns
     */
    public List<Column> getVehicleColumns() {
        return vehicleColumns;
    }

    /**
     * @param vehicleColumns the vehicleColumns to set
     */
    public void setVehicleColumns(List<Column> vehicleColumns) {
        this.vehicleColumns = vehicleColumns;
    }

    /**
     * Create or update the vehicle type specified by the <code>user</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = this.vehicleType.isNew();
        final int vehicleSpokenLength = 5;
        if (log.isDebugEnabled()) {
            log.debug("Saving vehicle type");
        }

        try {
            vehicleTypeManager.save(vehicleType);
            
            if (isNew) {
                String vehicleNumber = "D" + vehicleType.getNumber();
                final int length = vehicleNumber.length() < vehicleSpokenLength
                    ? vehicleNumber.length() : vehicleSpokenLength;
                Vehicle defaultVehicle = new Vehicle();
                defaultVehicle.setVehicleNumber(vehicleNumber);
                defaultVehicle.setVehicleCategory(VehicleCategory.Default);
                defaultVehicle.setVehicleType(vehicleType);
                defaultVehicle.setSpokenVehicleNumber(vehicleNumber.substring(
                    0, length));
                vehicleManager.save(defaultVehicle);
            }
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified vehicle type "
                + this.vehicleType.getDescription());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.vehicleType"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                VehicleType modifiedEntity = getVehicleTypeManager().get(
                    getTypeId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.vehicleType.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.vehicleType.getDescription()), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("vsc.vehicleType."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/vscvehicletype/view.action?typeId="
                + this.vehicleType.getId()), this.vehicleType.getDescription()));

        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 