/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationItem;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationItemManager;
import com.vocollect.voicelink.core.service.LocationManager;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Action class for Item Location Mapping.
 *
 * @author vjayaram
 */
public abstract class LocationItemActionRoot extends DataProviderAction
    implements Preparable {

    private static final long serialVersionUID = -1008481683414914131L;

    public static final long MAPPING_VIEW_ID = -1030;

    private LocationItemManager locationItemManager;

    private LocationManager locationManager;

    private ItemManager itemManager;

    private LocationItem locationItem;

    private Location location;

    private Item item;

    private LocationStatus status = LocationStatus.Replenished;

    private String importAction = null;

    private Collection<LocationItem> locationItems = new ArrayList<LocationItem>();

    private List<Column> locationItemColumns;

    /**
     * The number of fields in the create form - initially there is only one
     * mapping on the create screen.
     */
    private int numberOfMappings = 1;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getLocationItemManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.selection.core.LocationItemActionRoot#getDeliveryLocationMappingViewId()
     */

    public long getLocationItemViewId() {
        return MAPPING_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(MAPPING_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "locationItem";
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View locationItemsView = this.getUserPreferencesManager().getView(
            getLocationItemViewId());
        this.locationItemColumns = this.getUserPreferencesManager().getColumns(
            locationItemsView, getCurrentUser());

        return SUCCESS;
    }

    /**
     * Getter for the locationItemManager property.
     * @return LocationItemManager value of the property
     */
    public LocationItemManager getLocationItemManager() {
        return this.locationItemManager;
    }

    /**
     * Setter for the locationItemManager property.
     * @param locationItemManager the new locationItemManager value
     */
    public void setLocationItemManager(LocationItemManager locationItemManager) {
        this.locationItemManager = locationItemManager;
    }

    /**
     * Getter for the locationItem property.
     * @return LocationItem value of the property
     */
    public LocationItem getLocationItem() {
        return this.locationItem;
    }

    /**
     * Setter for the locationItem property.
     * @param locationItem the new locationItem value
     */
    public void setLocationItem(LocationItem locationItem) {
        this.locationItem = locationItem;
    }

    /**
     * Creates the locationItem mapping after validating if the item - location
     * mappings are valid and the mappings are not duplicate.
     * @return Success.
     * @throws Exception general exception
     */
    public String create() throws Exception {

        LocationItemManager mappingManager = this.getLocationItemManager();
        List<UserMessage> detailMessages = new ArrayList<UserMessage>();
        String itemNum = null;
        String scanVer = null;

        int recordsSaved = 0;
        int totalRecords = locationItems.size();

        for (LocationItem li : locationItems) {
            try {
                itemNum = li.getItem().getNumber();
                scanVer = li.getLocation().getScannedVerification();
                Location loc = getLocationManager().findLocationByScanned(
                    scanVer);
                Item ite = getItemManager().findItemByNumber(itemNum);
                li.setLocation(loc);
                li.setItem(ite);
                mappingManager.executeCreateMapping(li);
                recordsSaved++;

            } catch (NullPointerException e) {
                addSessionActionMessage(new UserMessage(
                    "locationItem.import.error.itemUndefined"));
            } catch (BusinessRuleException bre) {
                if (bre.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ALREADY_EXISTS) {
                    detailMessages
                        .add(new UserMessage(
                            "locationItem.save.partialSuccess.duplicateItemLocation.message",
                            itemNum, scanVer));
                } else if (bre.getErrorCode() == CoreErrorCode.ITEM_LOCATION_ITEM_NOT_DEFINED) {
                    detailMessages
                        .add(new UserMessage(
                            "locationItem.save.partialSuccess.invalidItemLocation.message",
                            itemNum, scanVer));
                } else {
                    LOG.error("BusinessRuleException! ", bre);
                    throw bre;
                }
            }
        }

        if (detailMessages.size() > 0) {
            addPartialSuccessSessionActionMessage(new UserMessage(
                "locationItem.save.partialSuccess.general.message",
                recordsSaved, totalRecords), detailMessages);
        } else {
            addSessionActionMessage(new UserMessage(
                "locationItem.create.message.success"));
        }

        return SUCCESS;
    }

    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return this.locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * Sets Item to Relenished.
     * @return Success
     * @throws DataAccessException on query failure.
     */
    public String setToReplenished() throws DataAccessException {

        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "setToReplenished";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                LocationItem locItem = (LocationItem) obj;
                Location li = locItem.getLocation();
                Item ite = locItem.getItem();
                getLocationManager().executeReportReplenished(li, ite);
                return true;
            }
        });
    }

    /**
     * Clears invalid lot from selected items location.
     * @return Success - on success
     * @throws Exception on unanticipated error condition
     */
    public String clearInvalidLot() throws Exception {

        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "clearInvalidLot";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                LocationItem locItem = (LocationItem) obj;
                Location li = locItem.getLocation();
                Item ite = locItem.getItem();
                getLocationManager().executeClearInvalidLot(li, ite);
                return true;
            }
        });
    }

    /**
     * Getter for the locationItems property.
     * @return Collection value of the property
     */
    public Collection<LocationItem> getLocationItems() {
        return this.locationItems;
    }

    /**
     * Setter for the locationItems property.
     * @param locationItems the new locationItems value
     */
    public void setLocationItems(Collection<LocationItem> locationItems) {
        this.locationItems = locationItems;
    }

    /**
     * Getter for the importAction property.
     * @return String value of the property
     */
    public String getImportAction() {
        return this.importAction;
    }

    /**
     * Setter for the importAction property.
     * @param importAction the new importAction value
     */
    public void setImportAction(String importAction) {
        this.importAction = importAction;
    }

    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Getter for the location property.
     * @return Location value of the property
     */
    public Location getLocation() {
        return this.location;
    }

    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * Getter for the status property.
     * @return LocationStatus value of the property
     */
    public LocationStatus getStatus() {
        return this.status;
    }

    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(LocationStatus status) {
        this.status = status;
    }

    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    public ItemManager getItemManager() {
        return this.itemManager;
    }

    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    /**
     * Getter for the numberOfMappings property.
     * @return int value of the property
     */
    public int getNumberOfMappings() {
        return this.numberOfMappings;
    }

    /**
     * Setter for the numberOfMappings property.
     * @param createNumberOfMappings the new numberOfMappings value
     */
    public void setNumberOfMappings(int createNumberOfMappings) {
        this.numberOfMappings = createNumberOfMappings;
    }

    /**
     * Validating here rather than in XML file because OpenSymphony seems to
     * have taken away the "collection" validator from the validation framework.
     * Rather than write a new validator (with Dennis Doubleday's advice) I
     * decided to validate here since this is such a unique case.
     *
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#validate()
     */
    @Override
    public void validate() {

        validateFields();

        validateDuplicates();
    }

    /**
     * Check to see that neither the delivery location nor the mapping value is
     * null.
     */
    private void validateFields() {
        int count = 0;
        for (LocationItem dlm : locationItems) {

            if (dlm == null) {
                LOG.info("Item Location Mapping is null." + count);
                break;
            }

            // Validate delivery locaiton is not null condition.
            if (StringUtil.isNullOrEmpty(dlm.getItem().getNumber())) {
                addFieldError(
                    "locationItems[" + count + "]",
                    new UserMessage(
                        "itemLocationMapping.create.message.error.itemNumberIsNull"));
            }

            // Validate mapping value not null
            if (StringUtil.isNullOrEmpty(dlm.getLocation().getScannedVerification())) {
                addFieldError(
                    "locationItems[" + count + "]", new UserMessage(
                        "itemLocationMapping.create.message.error.locationIdIsNull"));
            }

            count++;
        }
    }

    /**
     * Ensure that no mapping rules are duplicates (either within the database
     * or within the set of mapping rules that are to be created.
     */
    private void validateDuplicates() {
        int count = 0;
        for (LocationItem dlm1 : locationItems) {
            for (LocationItem dlm2 : locationItems) {
                if (dlm1 != dlm2
                    && dlm1.getLocation().getScannedVerification().equals(dlm2.getLocation().getScannedVerification())
                    && dlm1.getItem().getNumber().equals(dlm2.getItem().getNumber())) {
                    addFieldError(
                        "locationItems[" + count + "]",
                        new UserMessage(
                            "itemLocationMapping.create.message.error.duplicateMapping"));
                }
            }
            count++;
        }
    }

    /**
     * Getter for the locationItemColumns property.
     * @return the value of the property
     */
    public List<Column> getLocationItemColumns() {
        return this.locationItemColumns;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 