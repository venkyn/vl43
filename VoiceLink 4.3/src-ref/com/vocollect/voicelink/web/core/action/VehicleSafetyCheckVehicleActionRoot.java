/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleCategory;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleTypeManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 
 * @author khazra
 */
@SuppressWarnings("serial")
public class VehicleSafetyCheckVehicleActionRoot extends DataProviderAction
    implements Preparable {

    private static final Logger log = new Logger(
        VehicleSafetyCheckVehicleActionRoot.class);

    private static final long VEHICLE_VIEW_ID = -1226;

    private VehicleTypeManager vehicleTypeManager;

    private VehicleManager vehicleManager;

    // The list of columns for the vehicle table.
    private List<Column> vehicleColumns;

    private String typeId = null;

    private Long vehicleId = null;

    private Vehicle vehicle = null;

    // This property is for action enablers of delete and edit
    private Integer vehicleCategory = null;

    /**
     * Getter for the vehicleTypeManager property.
     * @return VehicleTypeManager value of the property
     */
    public VehicleTypeManager getVehicleTypeManager() {
        return vehicleTypeManager;
    }

    /**
     * Setter for the vehicleTypeManager property.
     * @param vehicleTypeManager the new vehicleTypeManager value
     */
    public void setVehicleTypeManager(VehicleTypeManager vehicleTypeManager) {
        this.vehicleTypeManager = vehicleTypeManager;
    }

    /**
     * Getter for the vehicleManager property.
     * @return VehicleManager value of the property
     */
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    /**
     * Setter for the vehicleManager property.
     * @param vehicleManager the new vehicleManager value
     */
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    /**
     * Getter for the typeId property.
     * @return String value of the property
     */
    public String getTypeId() {
        return typeId;
    }

    /**
     * Setter for the typeId property.
     * @param typeId the new typeId value
     */
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    /**
     * Getter for the vehicle property.
     * @return Vehicle value of the property
     */
    public Vehicle getVehicle() {
        return vehicle;
    }

    /**
     * Setter for the vehicle property.
     * @param vehicle the new vehicle value
     */
    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    /**
     * Getter for the vehicleId property.
     * @return String value of the property
     */
    public Long getVehicleId() {
        return vehicleId;
    }

    /**
     * Setter for the vehicleId property.
     * @param vehicleId the new vehicleId value
     */
    public void setVehicleId(Long vehicleId) {
        this.vehicleId = vehicleId;
    }


    
    /**
     * Getter for the vehicleCategory property.
     * @return Integer value of the property
     */
    public Integer getVehicleCategory() {
        return vehicleCategory;
    }

    
    /**
     * Setter for the vehicleCategory property.
     * @param vehicleCategory the new vehicleCategory value
     */
    public void setVehicleCategory(Integer vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        if (this.vehicleId != null) {
            // We have an ID, but not an Vehicle object yet.
            if (log.isDebugEnabled()) {
                log.debug("vehcileId is " + this.vehicleId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Vehicle is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved item from session");
                }
                this.vehicle = (Vehicle) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting vehicle from database");
                }
                this.vehicle = this.vehicleManager.get(this.vehicleId);
                saveEntityInSession(this.vehicle);
            }
            
            this.typeId = this.vehicle.getVehicleType().getId().toString();
            this.vehicleCategory = this.vehicle.getVehicleCategory().toValue();
            
            if (log.isDebugEnabled()) {
                log.debug("Vehicle version is: " + this.vehicle.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new vehicle object");
            }

            this.vehicle = new Vehicle();

            VehicleType vehicleType = this.vehicleTypeManager.get(Long.parseLong(this.typeId));
            this.vehicle.setVehicleType(vehicleType);
            this.vehicle.setVehicleCategory(VehicleCategory.Normal);
        }
    }

    /**
     * Delete the vehicle identified by the <code>vehicleId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified vehicle wasn't found (with different messages to
     *         the end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentVehicle() throws Exception {

        Vehicle vehicleToDelete = null;

        try {
            vehicleToDelete = vehicleManager.get(this.vehicleId);
            vehicleManager.delete(this.vehicleId);

            addSessionActionMessage(new UserMessage(
                "vehicle.delete.message.success",
                vehicleToDelete.getVehicleNumber()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete vehicle: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    @Override
    protected DataProvider getManager() {
        return this.getVehicleManager();
    }

    @Override
    protected String getKeyPrefix() {
        return "vehicle";
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VEHICLE_VIEW_ID);
        return viewIds;
    }

    /**
     * @return SUCCESS
     * @throws Exception
     */
    public String list() throws Exception {
        View vehicleView = getUserPreferencesManager().getView(VEHICLE_VIEW_ID);
        this.vehicleColumns = this.getUserPreferencesManager().getColumns(
            vehicleView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * View id for Vehicle table
     * @return long view id
     */
    public static long getVehicleViewId() {
        return VEHICLE_VIEW_ID;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getVehicleColumns() {
        return this.vehicleColumns;
    }

    /**
     * Returns data for vehicles
     * @return Vehicle data
     * @throws Exception if any
     */
    public String getVehicleData() throws Exception {
        if (getTypeId() != null) {
            super.getTableData("obj.vehicleType.id in ( " + this.typeId + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Create or update the vehicle specified by the <code>user</code> member of
     * this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = this.vehicle.isNew();

        if (log.isDebugEnabled()) {
            log.debug("Saving vehilce");
        }

        try {
            vehicleManager.save(this.vehicle);
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (EntityNotFoundException e) {
            log.warn("Vehicle Number " + this.vehicle.getVehicleNumber()
                + " not found, possibly a concurrency");

            addSessionActionMessage(new UserMessage(
                "vsc.vehicle.edit.error.vehicleNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified vehicle "
                + this.vehicle.getVehicleNumber());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.vehicle"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Vehicle modifiedVehicle = getVehicleManager().get(
                    getVehicleId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.vehicle.setVersion(modifiedVehicle.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedVehicle);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.vehicle"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("vsc.vehicle."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/vscvehicles/view.action?vehicleId="
                + this.vehicle.getId()), this.vehicle.getVehicleNumber()));

        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 