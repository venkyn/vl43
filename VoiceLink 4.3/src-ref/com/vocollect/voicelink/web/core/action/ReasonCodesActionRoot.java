/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.ReasonCode;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.ReasonCodeManager;

import com.opensymphony.xwork2.Preparable;


import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *  Action code for Reason Codes.
 *
 * @author svoruganti
 */
public class ReasonCodesActionRoot extends DataProviderAction implements
    Preparable {

    //
    private static final long serialVersionUID = -4931983545329653761L;

    //  logger for ReasonCodeRootAction class
    private static final Logger log = new Logger(ReasonCodesActionRoot.class);

    //  The ID of the ReasonCode
    private Long reasonCodeId;

    // The ReasonCodeprinter object, which will either be newly created, or retrieved
    // via the reasonCodeId.
    private ReasonCode reasonCode;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1026;

    // The list of columns for the Reason Code table.
    private List<Column> reasonCodeColumns;

    // ReasonCodeManager is service for reasonCode
    private ReasonCodeManager reasonCodeManager;

    // function for reason codes
    private Map< Integer, String > functionMap;

    //To retrieve taskFunctionType. from GUI
    private Integer taskApplicationStatus;


    //  SELECTION for TaskFunctionType.
    private static final int SELECTION = 6;

    //LINE_LOADING for TaskFunctionType.
    private static final int LINE_LOADING = 7;

    //PUT_AWAY for TaskFunctionType.
    private static final int PUT_AWAY = 1;

    //REPLENISHMENT for TaskFunctionType.
    private static final int REPLENISHMENT = 2;

    //CycleCounting for TaskFunctionType.
    private static final int CYCLECOUNTING = 9;

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getReasonCodesColumns() {
        return this.reasonCodeColumns;
    }

    /**
     * Getter for the Reason codes view ID property.
     * @return long value of the property
     */
    public static long getReasonCodesViewId() {
        return VIEW_ID;
    }


    /**
     * Action for the Reason Codes view page. Initializes the reason code table columns
     * after communicating with the Reason Codes for an updated list of Reason Codes.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        log.info(" Getting a list of columns for Reason Codes");
        View view = getUserPreferencesManager().getView(VIEW_ID);
        this.reasonCodeColumns = this.getUserPreferencesManager().getColumns(
            view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getReasonCodeManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "reasoncode";
    }

    /**
     * Get the id for the reasonCode.
     * @return the  ReasonCodeId.
     */
    public Long getReasonCodeId() {
        return reasonCodeId;
    }

    /**
     * Set the id for the reasonCode.
     * @param reasonCodeId
     *      the new reasonCodeId
     */
    public void setReasonCodeId(Long reasonCodeId) {
        this.reasonCodeId = reasonCodeId;
    }

    /**
     * Get the  reasonCode.
     * @return the  reasonCode.
     */
    public ReasonCode getReasonCode() {
        return reasonCode;
    }

    /**
     * Set the  the reasonCode.
     * @param reasonCode
     *      the new reasonCode
     */
    public void setReasonCode(ReasonCode reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Get the manager for the reasonCode.
     * @return the  ReasonCode manager.
     */
    public ReasonCodeManager getReasonCodeManager() {
        return reasonCodeManager;
    }

    /**
     * Set the manager for the reasonCode.
     * @param reasonCodeManager
     *      the new reasonCodeManager
     */
    public void setReasonCodeManager(ReasonCodeManager reasonCodeManager) {
        this.reasonCodeManager = reasonCodeManager;

    }

    /**
     * Get the taskFunctionType. .
     * @return the taskFunctionType..
     */
    public Integer getTaskApplicationStatus() {

          if (this.reasonCode != null) {
            if (this.reasonCode.getTaskFunctionType() == TaskFunctionType.LineLoading) {
                return LINE_LOADING;
            } else if (this.reasonCode.getTaskFunctionType() == TaskFunctionType.Putaway) {
                return PUT_AWAY;
            } else if (this.reasonCode.getTaskFunctionType() == TaskFunctionType.Replenishment) {
                return REPLENISHMENT;
            } else if (this.reasonCode.getTaskFunctionType() == TaskFunctionType.NormalAndChaseAssignments) {
                    return SELECTION;
            } else if (this.reasonCode.getTaskFunctionType() == TaskFunctionType.CycleCounting) {
                return CYCLECOUNTING;
            }
        }
        return 0;
    }

    /**
     * Get the map for function of reason code.
     * @return the  ReasonCode function map.
     */
    public Map<Integer, String>  getFunctionMap() {
        functionMap = new TreeMap<Integer, String>();
        functionMap.put(new Integer(LINE_LOADING),
            getText(TaskFunctionType.LineLoading.getResourceKey()));
        functionMap.put(new Integer(PUT_AWAY),
            getText(TaskFunctionType.Putaway.getResourceKey()));
        functionMap.put(new Integer(REPLENISHMENT),
            getText(TaskFunctionType.Replenishment.getResourceKey()));
        functionMap.put(new Integer(SELECTION),
                getText("com.vocollect.voicelink.core.model.TaskFunctionType.alternative.6"));
        functionMap.put(new Integer(CYCLECOUNTING),
                getText(TaskFunctionType.CycleCounting.getResourceKey()));
        return functionMap;
    }


    /**
     * Set the function map for the reasonCode.
     * @param functionMap
     *      the new functionMap
     */
    public void setFunctionMap(Map<Integer, String> functionMap) {
        this.functionMap = functionMap;
    }

     /**
     * Calls changeStatus in model class to set taskApplication .
     *
     * @param taskApplicationStatus taskApplication the String representing the function to change to.
     */
    public void setTaskApplicationStatus(Integer taskApplicationStatus) {
        this.taskApplicationStatus = taskApplicationStatus;
        this.reasonCode.setTaskFunctionType(TaskFunctionType.toEnum(this.taskApplicationStatus));

    }

    /**
     * This method sets up the <code>ReasonCode</code> object by retrieving it
     * from the database when a reasonCodeId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
       if (this.reasonCodeId != null) {
            // We have an ID, but not a reasonCode object yet.
            if (log.isDebugEnabled()) {
                log.debug("reasonCodeId is " + this.reasonCodeId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the reason code is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved reason Code from session");
                }
                this.reasonCode = (ReasonCode) getEntityFromSession(getSavedEntityKey());
              } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting reason code from database");
                }
                this.reasonCode = this.reasonCodeManager.get(this.reasonCodeId);
                saveEntityInSession(this.reasonCode);
            }
            if (log.isDebugEnabled()) {
                log.debug("Reason code version is: " + this.reasonCode.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new reasonCode object");
            }
            this.reasonCode = new ReasonCode();
        }
    }

    /**
     * Create or update the reasonCode specified by the <code>reasonCode</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.reasonCode.isNew();
        try {

            if (this.reasonCode.getReasonDescription() == null
                || this.reasonCode.getReasonDescription().trim().equalsIgnoreCase("")) {
                addFieldError("reasonCode.reasonDescription", getText("reasoncode.create.label.description")
                    + getText("errors.required"));
            }

            if (this.reasonCode.getReasonNumber() == null
                 || this.reasonCode.getReasonNumber().toString().equalsIgnoreCase("")) {
                addFieldError("reasonCode.reasonNumber", getText("reasoncode.create.label.number")
                    + getText("errors.required"));
            }

          /*  if (Long.toString(this.reasonCode.getReasonNumber()).equalsIgnoreCase("")) {
                addFieldError("reasonCode.reasonNumber", getText("reasoncode.create.label.number")
                    + getText("errors.required"));
            }*/
            if (hasErrors()) {
                return (INPUT);
            }

            //Uniqueness check for number
            Long numberUniquenessId = this.reasonCodeManager.getPrimaryDAO()
                .uniquenessByNumber(this.reasonCode.getReasonNumber());
            if (numberUniquenessId != null && (isNew || (!isNew
                && numberUniquenessId.longValue() != this.reasonCode.getId().longValue()))) {
                log.warn("Reason Code '" + this.reasonCode.getReasonNumber()
                    + "' already exists");
                addFieldError("reasonCode.reasonNumber", new UserMessage(
                    "reasonCode.create.error.existingNumber", this.reasonCode.getReasonNumber()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            reasonCodeManager.save(this.reasonCode);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified reason number "
                + this.reasonCode.getReasonNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.reasonCode"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));

            // Get the modified data
            // If the entity has been deleted, this will throw EntityNotFoundException
            try {
                ReasonCode modifiedEntity = getReasonCodeManager().get(getReasonCodeId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.reasonCode.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.reasonCode.getReasonDescription()), null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "reasonCode." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/reasonCodes/view.action?reasonCodeId=" + reasonCode.getReasonNumber()),
                this.reasonCode.getReasonNumber()));

        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Delete the reason code identified by the <code>reasonCodeId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentReasonCode() throws Exception {

        ReasonCode reasonCodeToDelete = null;

        try {
            reasonCodeToDelete = reasonCodeManager.get(this.reasonCodeId);
            reasonCodeManager.delete(this.reasonCodeId);

            addSessionActionMessage(new UserMessage(
                "reasoncode.delete.message.success", reasonCodeToDelete.getReasonNumber()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete reason code: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * @return the current ReasonCode description, translated for the current
     *         Locale, or the untranslated description if the ReasonCode
     *         description isn't translated for this Locale, or
     *         <code>null</code> if the current ReasonCode isn't defined.
     */
    public String getTranslatedReasonDescription() {
        if (getReasonCode() == null) {
            return null;
        } else {
            return getDataTranslation(getReasonCode().getReasonDescription());
        }
    }

    /**
     * @return the current ReasonCode description, translated for the current
     *         Locale, or <code>null</code> if the ReasonCode description
     *         isn't translated for this Locale, or if the current ReasonCode
     *         isn't defined.
     */
    public String getTranslatedReasonDescriptionOrNull() {
        if (getReasonCode() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getReasonCode().getReasonDescription());
        }
    }

    /**
     * @return the message containing the Locale and the translated ReasonCode
     *         description, or null if there is no translation.
     */
    public String getTranslatedReasonDescriptionMessage() {
        return getDataTranslationMessage(getTranslatedReasonDescriptionOrNull());
    }


    /**
     * @return the localized nmae of the task function for the reason code.
     */
    public String getTaskFunction() {
        return  getText(this.reasonCode.getTaskFunctionType().getResourceKey());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 