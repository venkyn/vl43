/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.dao.ItemDAO;
import com.vocollect.voicelink.core.model.Item;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on <code>Item</code>
 * objects.
 * @author Brian Rupert
 * @author snalan
 */
public class ItemActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -9083192577584580012L;

    private static final Logger log = new Logger(ItemActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1006;

    // The list of columns for the operators table.
    private List<Column> columns;

    // The Item management service.
    private GenericManager<Item, ItemDAO> itemManager;

    // The Item object, which will either be newly created, or retrieved
    // via the ItemId.
    private Item item;

    // The ID of the Item.
    private Long itemId;

    /**
     * Getter for the item property.
     * @return User value of the property
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
     * Getter for the itemId property.
     * @return Long value of the property
     */
    public Long getItemId() {
        return this.itemId;
    }

    /**
     * Setter for the itemId property.
     * @param itemId the new itemId value
     */
    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    /**
     * Getter for the itemManager property.
     * @return UserManager value of the property
     */
    public GenericManager<Item, ItemDAO> getItemManager() {
        return this.itemManager;
    }

    /**
     * Setter for the itemManager property.
     * @param manager the new itemManager value
     */
    public void setItemManager(GenericManager<Item, ItemDAO> manager) {
        this.itemManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getItemColumns() {
        return this.columns;
    }

    /**
     * Action for the items view page. Initializes the item table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View itemView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(itemView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the item specified by the <code>item</code> member of
     * this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.item.isNew();

        try {
            Long numberUniquenessId =
                this.itemManager.getPrimaryDAO().uniquenessByNumber(this.item.getNumber());
            if (numberUniquenessId != null
                && (isNew || (!isNew && numberUniquenessId.longValue() != this.item
                    .getId().longValue()))) {
                log.warn("A Item '" + this.item.getNumber()
                    + "' already exists");
                addFieldError("item.number", new UserMessage(
                    "item.create.error.existing", this.item.getNumber()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            itemManager.save(this.item);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified item "
                + this.item.getId());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Item"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If item has been deleted, this will throw EntityNotFoundException
            try {
                Item modifiedItem = getItemManager().get(getItemId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.item.setVersion(modifiedItem.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedItem);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.item.getNumber().toString())
                        , null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "item." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeGenericContextURL("/item/view.action?itemId=" + item.getId()),
                this.item.getNumber()));

        // Go to the success target.
        return SUCCESS;

    }

    /**
     * Delete the item identified by the <code>itemId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified item wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentItem() throws Exception {

        Item itemToDelete = null;

        try {
            itemToDelete = itemManager.get(this.itemId);
            itemManager.delete(this.itemId);

            addSessionActionMessage(new UserMessage(
                "item.delete.message.success", itemToDelete.getNumber()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete item: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Item</code> object by retrieving it
     * from the database when a itemId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.itemId != null) {
            // We have an ID, but not an Item object yet.
            if (log.isDebugEnabled()) {
                log.debug("itemId is " + this.itemId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the User is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved item from session");
                }
                this.item = (Item) getEntityFromSession(getSavedEntityKey());
                // Setting the value of the isVariableWeightItem to false
                // to ensure that the checkbox data is correctly updated
                this.item.setIsVariableWeightItem(false);
                this.item.setIsSerialNumberItem(false);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting user from database");
                }
                this.item = this.itemManager.get(this.itemId);

                saveEntityInSession(this.item);
            }
            if (log.isDebugEnabled()) {
                log.debug("User version is: " + this.item.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new item object");
            }
            this.item = new Item();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getItemManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "item";
    }

    /**
     * Getter for the ITEM_VIEW_ID property.
     * @return long value of the property
     */
    public static long getItemViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * @return the current Item description, translated for the current
     *         Locale, or the untranslated description if the Item
     *         description isn't translated for this Locale, or
     *         <code>null</code> if the current Item isn't defined.
     */
    public String getTranslatedItemDescription() {
        if (getItem() == null) {
            return null;
        } else {
            return getDataTranslation(getItem().getDescription());
        }
    }

    /**
     * @return the current Item description, translated for the current
     *         Locale, or <code>null</code> if the Item description
     *         isn't translated for this Locale, or if the current Item
     *         isn't defined.
     */
    public String getTranslatedItemDescriptionOrNull() {
        if (getItem() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getItem().getDescription());
        }
    }

    /**
     * @return the message containing the Locale and the translated Item
     *         description, or null if there is no translation.
     */
    public String getTranslatedItemDescriptionMessage() {
        return getDataTranslationMessage(getTranslatedItemDescriptionOrNull());
    }


    /**
     * @return the current Item phonetic description, translated for the current
     *         Locale, or the untranslated phonetic description if the Item
     *         phonetic phonetic description isn't translated for this Locale, or
     *         <code>null</code> if the current Item isn't defined.
     */
    public String getTranslatedPhoneticDescription() {
        if (getItem() == null) {
            return null;
        } else {
            return getDataTranslation(getItem().getPhoneticDescription());
        }
    }

    /**
     * @return the current Item phonetic description, translated for the current
     *         Locale, or <code>null</code> if the Item phonetic description
     *         isn't translated for this Locale, or if the current Item
     *         isn't defined.
     */
    public String getTranslatedPhoneticDescriptionOrNull() {
        if (getItem() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getItem().getPhoneticDescription());
        }
    }

    /**
     * @return the message containing the Locale and the translated Item
     *         phonetic description, or null if there is no translation.
     */
    public String getTranslatedPhoneticDescriptionMessage() {
        return getDataTranslationMessage(getTranslatedPhoneticDescriptionOrNull());
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 