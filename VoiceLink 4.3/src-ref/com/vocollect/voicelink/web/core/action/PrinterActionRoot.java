/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.PrintServerErrorCode;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.ContainerManager;
import com.vocollect.voicelink.selection.service.PickManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.*;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.NonUniqueResultException;

/**
 * Action code for print label.
 * @author svoruganti
 */
public class PrinterActionRoot extends DataProviderAction implements Preparable {

    //The serial version id.
    private static final long serialVersionUID = 6478026686916495773L;

    //  logger for fPrinterAction class
    private static final Logger log = new Logger(PrinterActionRoot.class);

    // Printer maanger is service for Printer
    private PrinterManager printerManager;

    //  The ID of the printerId
    private Long printerId;

    //the name of the printer
    private String printerName;

    // The printer object, which will either be newly created, or retrieved
    // via the printerId.
    private Printer printer;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1024;

    // The list of columns for the print label table.
    private List<Column> printerColumns;

    // map of printer statuses.
    private Map<Boolean, String> printerStatusMap;

    // map of printer statuses.
    private Map<Long, String> printerNamesMap;

    // Printer maanger is service for assignemnt
    private AssignmentManager assignmentManager;

    // Printer maanger is service for assignemnt
    private PickManager pickManager;

    // Printer maanger is service for assignemnt
    private ContainerManager containerManager;

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPrinterColumns() {
        return this.printerColumns;
    }

    /**
     * Action for the printer view page. Initializes the printer table columns
     * after communicating with the PrintServer for an updated list of printers.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View view = getUserPreferencesManager().getView(VIEW_ID);
        this.printerColumns = this.getUserPreferencesManager().getColumns(
                                      view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "PrintLabel";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPrinterManager();
    }

    /**
     * Get the manager for the Printer.
     * @return the printer manager.
     */
    public PrinterManager getPrinterManager() {
        return printerManager;
    }

    /**
     * Set the manager for the printer.
     * @param printerManager
     *      the new printerManager
     */
    public void setPrinterManager(PrinterManager printerManager) {
        this.printerManager = printerManager;
    }

    /**
     * Get the printerId for the Printer.
     * @return the printerId.
     */
    public Long getPrinterId() {
        return printerId;
    }

    /**
     * Set the printerId for the printer.
     * @param printerId
     *    the new printerId
     */
    public void setPrinterId(Long printerId) {
        this.printerId = printerId;
    }

    /**
     * Get the printerName .
     * @return the printer.
     */
    public String getPrinterName() {
        return this.printerName;
    }

    /**
     * Set the printerName.
     * @param printerName
     *        the new printerName
     */
    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }
     /**
     * Get the printer model .
     * @return the printer.
     */
    public Printer getPrinter() {
        return printer;
    }

    /**
     * Set the model for the printer.
     * @param printer
     *        the new printer
     */
    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

      /**
     * Getter for the Print_Label_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPrintLabelViewId() {
        return VIEW_ID;
    }

    /**
     * This method sets up the <code>prinetr</code> object by retrieving it from
     * the database when a printerId is set by the form submission.
     * {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.printerId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {

                this.printer = (Printer) getEntityFromSession(
                                                getSavedEntityKey());
            } else {
                this.printer = this.printerManager.get(this.printerId);
                saveEntityInSession(this.printer);
            }

        }
    }

    /**
     * The validate method from Action Support is overriden to validate the inputs.
     */
    @Override
    public void validate() {

        if (printer.getNumber() == null) {
            addFieldError("printer.number",
                    getText("selection.printer.number.required"));
        } else {
            if (printer.getNumber() < 0) {
                addFieldError("printer.number",
                        getText("selection.printer.number.notNegative"));
            }
        }
    }

    /**
     * Get the possible <code>Printer</code> statuses for display.
     * @return the Map of defined <code>Printer</code> status, where the map key
     *         is true/false, and the map value is the resource key for the
     *         status name.
     */
    public Map<Boolean, String> getStatusMap() {
        if (this.printerStatusMap == null) {
            this.printerStatusMap = new LinkedHashMap <Boolean, String>();
            this.printerStatusMap.put(true, getText("printer.status.true"));
            this.printerStatusMap.put(false, getText("printer.status.false"));
        }
        return this.printerStatusMap;
    }

    /**
     * This function just checks to see if there are any other printers with
     * the same printer number.
     * @param isNew - Whether this is a new printer or an existing one.
     * @return boolean for whether the printer's number is unique.
     * @throws DataAccessException - thrown if we have any issues checking the uniqueness in the db.
     */
    private boolean isPrinterNumberUnique(boolean isNew) throws DataAccessException {
        Long printerIdentifierUniquenessId = this.printerManager.
            getPrimaryDAO().uniquenessByNumber(
                this.printer.getNumber().intValue());

        return (printerIdentifierUniquenessId != null)
            && (isNew || (!isNew && (printerIdentifierUniquenessId.longValue()
                !=  this.printer.getId().longValue())));
    }

    /**
     * This function just checks to see if there are any other printers with
     * the same printer name.
     * @param isNew - Whether this is a new printer or an existing one.
     * @return boolean for whether the printer's name is unique.
     * @throws DataAccessException - thrown if we have any issues checking the uniqueness in the db.
     */
    private boolean isPrinterNameUnique(boolean isNew) throws DataAccessException {

        Long printerIdentifierUniquenessId = printerManager.
            getPrimaryDAO().uniquenessByName(
                this.printer.getName());
        return (printerIdentifierUniquenessId != null)
            && (isNew || (!isNew && (printerIdentifierUniquenessId.longValue()
                !=  this.printer.getId().longValue())));

    }

    /**
     * This function checks to see if there are any other printers in
     * any of the sites with the same name.
     * @param isNew - Whether this is a new printer or an existing one.
     * @return boolean for whether the printer's name is unique across all sites.
     * @throws DataAccessException - thrown if we have any issues check the uniqueness in the db.
     */
    private boolean isPrinterNameUniqueForAllSites(boolean isNew) throws DataAccessException {
        boolean isUnique = false;
        boolean filterBySite = false;
        SiteContext sc = SiteContextHolder.getSiteContext();
        filterBySite = sc.isFilterBySite();
        sc.setFilterBySite(false);

        try {
            isUnique = isPrinterNameUnique(isNew);
        } catch (DataAccessException e) {
            isUnique = true;
        } catch (NonUniqueResultException  e) {
            //defined in other sites. when defined in more than 1 site
            isUnique = true;
        } finally {
            SiteContextHolder.getSiteContext().setFilterBySite(filterBySite);
        }

        return isUnique;
    }

    /**
     * Create or update the Printer    specified by the <code>printer</code>
     * member of this class.
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        if (printer == null) {
            return INPUT;
        }
        boolean isNew = printer.isNew();
        // If it exists in other sites, we need to display a special message.
        boolean existsInOtherSites = false;
        try {
            // First check to make sure the printer number is unique
            if (isPrinterNumberUnique(isNew)) {
                log.warn("printer number" + this.printer.getNumber() + " already exists");
                addFieldError("printer.number", new UserMessage(
                    "printer.edit.error.existing", this.printer.getNumber()));
            }

            // Second, check to make sure the printer name is unique for this site
            if (isPrinterNameUnique(isNew)) {
                log.warn("printer name" + this.printer.getNumber() + " already exists in this site");
                addFieldError("printer.name",
                    new UserMessage("printer.create.duplicate",
                        this.printer.getName()));
            } else if (isPrinterNameUniqueForAllSites(isNew)) {
                // We know it's unique within the current site. Let's check within all sites.
                // We'll add a warning if it's in another site.
                existsInOtherSites = true;
            }
            if (hasErrors()) {
                // Go back to the form and show the errors
                return INPUT;
            }
            printerManager.save(this.printer);
            cleanSession();
        } catch (EntityNotFoundException e) {
            log.warn("printer Number " + this.printer.getNumber()
                + " not found, possibly a concurrency");

            addSessionActionMessage(new UserMessage(
                "printer.edit.error.printerNotFound", null, null));
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified printer "
                + this.printer.getNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.printer"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
               Printer modifiedPrinter = getPrinterManager().
                                           get(getPrinterId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
               this.printer.setVersion(modifiedPrinter.getVersion());
               setModifiedEntity(modifiedPrinter);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.printer.getName()
                            ), null));
                return SUCCESS;
            }
            return INPUT;
        }

        if (isNew && existsInOtherSites) {
            addSessionActionMessage(new UserMessage(
                "printer.create.message.success.existsinothersites",
                makeContextURL("/selection/print/view.action?printerId=" + printer.getId()),
                this.printer.getName()));
        } else {
            addSessionActionMessage(new UserMessage(
                "printer." + (isNew ? "create" : "edit") + ".message.success",
                makeContextURL("/selection/print/view.action?printerId=" + printer.getId()),
                this.printer.getName()));
        }
        return SUCCESS;
    }

    /**
     * Deletes the currently viewed printer.
     * This function is useful if we have printer view in future
     * @return the control flow target name.
     */
    public String deleteCurrentPrinter() {

        Printer printerToDelete = null;
        try {
            printerToDelete = printerManager.get(this.printerId);
            printerManager.delete(this.printerId);

            addSessionActionMessage(new UserMessage(
                "printer.delete.message.success", printerToDelete
                    .getNumber()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete printer: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     *
     * @return the control flow target name.
     * @throws DataAccessException if unable to retrieve or check for printer names.
     */
    public String checkForNameUniqueness() throws DataAccessException {
        boolean uniqueSite = false;
        boolean uniqueAll = false;
        boolean exceptionHit = false;
        SiteContext sc = SiteContextHolder.getSiteContext();

        boolean oldFilterValue = sc.isFilterBySite();
        sc.setFilterBySite(false);
        try {
            if (printerManager.getPrimaryDAO().uniquenessByName(this.printerName) != null) {
                uniqueAll = false;
            } else {
                uniqueAll = true;
            }
        } catch (DataAccessException e) {
            exceptionHit = true;
        } catch (NonUniqueResultException  e) {
            //defined in other sites. when defined in more than 1 site
            uniqueAll = false;
        }
        sc.setFilterBySite(oldFilterValue);

        try {
            if (printerManager.getPrimaryDAO().uniquenessByName(this.printerName) == null) {
                uniqueSite = true;
            } else {
                uniqueSite = false;
            }
        } catch (DataAccessException e) {
            exceptionHit = true;
        }
        if (exceptionHit) {
            setJsonMessage("exception");
        } else if (uniqueSite) {
            //not found in this site, so we're OK to save it, but is it in a different site?
            if (uniqueAll) {
                //unique across all sites, save
                setJsonMessage("unique");
            } else {
                //exists in another site, show the warning
                setJsonMessage("anothersite");
            }
        } else {
            //no exceptions and it's already in the current site,
            setJsonMessage("duplicate");
        }
        return SUCCESS;
    }

    /**
     * @return the control flow target name.
     * @throws VocollectException if error occurs printing assignment labels.
     */
    public String printContainerLabel() throws VocollectException {

        ArrayList<Container> containersToPrint = new ArrayList<Container>(getIds().length);
        try {
            for (Long id : getIds()) {
                containersToPrint.add(containerManager.get(id));
            }
        } catch (ClassCastException e) {
            // This should never happen.
            log.warn("Casting error while retrieving assignments", e);
            setJsonMessage(getText(new UserMessage(
                    "assignment.print.message.failure.retrieval")),
                    ERROR_FAILURE);
            return SUCCESS;
        }
        try {
            this.printer = this.printerManager.get(this.printerId);
            this.printerManager.generateContainerLabels(containersToPrint,
                this.printer);
        } catch (DataAccessException e) {
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }

        if (containersToPrint.size() == 1) {
            setJsonMessage(getText(
                new UserMessage(
                   "print.containerLabel.success.single", false,
                   containersToPrint.get(0).getContainerNumber(),
                   this.printer.getName())),
                ERROR_SUCCESS);
        } else if (containersToPrint.size() > 1) {
            setJsonMessage(getText(
                new UserMessage(
                    "print.containerLabel.success.multiple", false,
                    containersToPrint.size(),
                    this.printer.getName())),
                ERROR_SUCCESS);
        }

        return SUCCESS;
    }

    /**
     * @return the control flow target name.
     * @throws VocollectException if error occurs printing assignment labels.
     */
    public String printAssignmentLabel() throws VocollectException {

        ArrayList<Assignment> assignmentsToPrint = new ArrayList<Assignment>(getIds().length);
        try {
            for (Long id : getIds()) {
                assignmentsToPrint.add(assignmentManager.get(id));
            }
        } catch (ClassCastException e) {
            // This should never happen.
            log.warn("Casting error while retrieving assignments", e);
            setJsonMessage(getText(new UserMessage(
                    "assignment.print.message.failure.retrieval")),
                    ERROR_FAILURE);
            return SUCCESS;
        }
        try {
            this.printer = this.printerManager.get(this.printerId);
            this.printerManager.generateAssignmentLabels(assignmentsToPrint,
                    this.printer);
        } catch (DataAccessException e) {
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }

        if (assignmentsToPrint.size() == 1) {
            setJsonMessage(getText(
                new UserMessage(
                   "print.assignmentLabel.success.single", false,
                   String.valueOf(assignmentsToPrint.get(0).getNumber()),
                   this.printer.getName())),
                ERROR_SUCCESS);
        } else if (assignmentsToPrint.size() > 1) {
            setJsonMessage(getText(
                new UserMessage(
                    "print.assignmentLabel.success.multiple", false,
                    assignmentsToPrint.size(),
                    this.printer.getName())),
                ERROR_SUCCESS);
        }

        return SUCCESS;
    }

    /**
     * @return the control flow target name.
     * @throws VocollectException if error occurs printing assignment labels.
     */
    public String printPickLabel() throws VocollectException {

        ArrayList<Pick> picksToPrint = new ArrayList<Pick>();
        try {
            for (Long id : getIds()) {
                picksToPrint.add(this.getPickManager().get(id));
            }
        } catch (ClassCastException e) {
            // This should never happen.
            log.warn("Casting error while retrieving assignments", e);
            setJsonMessage(getText(new UserMessage(
                    "assignment.print.message.failure.retrieval")),
                    ERROR_FAILURE);
            return SUCCESS;
        }
        try {
            this.printer = this.printerManager.get(this.printerId);
            this.printerManager.generatePickLabels(picksToPrint, this.printer);

        } catch (DataAccessException e) {
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }

        if (picksToPrint.size() == 1) {
            setJsonMessage(getText(
                new UserMessage(
                   "print.pickLabel.success.single", false,
                   this.printer.getName())),
                ERROR_SUCCESS);
        } else if (picksToPrint.size() > 1) {
            setJsonMessage(getText(
                new UserMessage(
                    "print.pickLabel.success.multiple", false,
                    picksToPrint.size(),
                    this.printer.getName())),
                ERROR_SUCCESS);
        }

        return SUCCESS;
    }

    /**
     * Get the printer label .
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws VocollectException -
     */
    public String getPrintLabel() throws DataAccessException,
    VocollectException {
    if (getPrinterNamesMap().isEmpty()) {
        setJsonMessage(
            getText(new UserMessage(
                "print.label.error.noprintersavailable.message",
                ERROR_FAILURE)), ERROR_FAILURE);
        return SUCCESS;
        } else {
            return INPUT;
        }
    }

    /**
     * Get the printerNamesMap .
     * @return the printerNamesMap.
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    public Map<Long, String> getPrinterNamesMap()
    throws DataAccessException, VocollectException {
        if (this.printerNamesMap == null) {
            this.printerNamesMap = new LinkedHashMap<Long, String>();
            try {
                List<Printer> printerNames = this.printerManager.
                                         listAllEnabledPrinters();
                for (Printer p : printerNames) {
                    printerNamesMap.put(p.getId(), p.getName());
                }
            } catch (DataAccessException e) {
                addSessionActionMessage(e.getUserMessage());
            }
        }
        return printerNamesMap;
    }

    /**
     * Get the assignmentManager property.
     * @return The assignmentManager property.
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Set the assignmentManager property.
     * @param assignmentManager The new assignment manager.
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Get the pickManager property.
     * @return The pickManager property.
     */
    public PickManager getPickManager() {
        return pickManager;
    }

    /**
     * Set the pickManager property.
     * @param pickManager The new pick manager.
     */
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }
     /**
     * Get the containerManager property.
     * @return The containerManager property.
     */
    public ContainerManager getContainerManager() {
        return containerManager;
    }

    /**
    * Set the containerManager property.
    * @param containerManager The new container manager.
    */
    public void setContainerManager(ContainerManager containerManager) {
        this.containerManager = containerManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Get the list of printers for the create dropdown.
     * @return something.
     */
    public List<Printer> getPrinterList() {
        try {
            return this.printerManager.getServerPrinters();
        } catch (VocollectException e) {
            log.error("Unable to retrieve printer list",
                PrintServerErrorCode.PRINTER_LIST_UNAVAILABLE,
                e);

        }

        return null;

    }
 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 