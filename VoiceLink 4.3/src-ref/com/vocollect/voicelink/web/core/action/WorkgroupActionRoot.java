/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.PropertyComparator;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.dao.WorkgroupDAO;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Action class for workgroups.
 * @author Brian Rupert
 */
@SuppressWarnings("serial")
public class WorkgroupActionRoot extends DataProviderAction implements Preparable {

    private Logger log = new Logger(WorkgroupActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1011;

    // The list of columns for the operators table.
    private List<Column> columns;

    // The workgroup management service.
    private GenericManager<Workgroup, WorkgroupDAO> workgroupManager;

    // The task function management service.
    private TaskFunctionManager taskFunctionManager;

    // The region management service.
    private RegionManager regionManager;

    // The Workgroup object, which will either be newly created, or retrieved
    // via the WorkgroupId.
    private Workgroup workgroup;

    // The ID of the workgroup.
    private Long workgroupId;

    private HashMap<Long, List<Region>> functionIdsToRegions;

    private HashSet<UserMessage> deleteResultMessages;

    private static final String FUNCTION_REGION_DELIMITER = "_";

    private boolean functionsSet = false;

    /**
     * @param workgroup The workgroup to set.
     */
    public void setWorkgroup(Workgroup workgroup) {
        this.workgroup = workgroup;
    }

    /**
     * @return The workgroup property.
     */
    public Workgroup getWorkgroup() {
        return this.workgroup;
    }

    /**
     * Get the workgroup ID.
     * @return The ID of the workgroup.
     */
    public Long getWorkgroupId() {
        return workgroupId;
    }

    /**
     * Set the workgroup ID.
     * @param workgroupId The ID of the workgroup.
     */
    public void setWorkgroupId(Long workgroupId) {
        this.workgroupId = workgroupId;
    }

    /**
     * Getter for the workgroupManager property.
     * @return value of the property.
     */
    public GenericManager<Workgroup, WorkgroupDAO> getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param manager the new workgroupManager value.
     */
    public void setWorkgroupManager(GenericManager<Workgroup, WorkgroupDAO> manager) {
        this.workgroupManager = manager;
    }

    /**
     * set the region manager.
     * @return The region management service.
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * get the region manager.
     * @param regionManager The region management service.
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * set the task function manager.
     * @return The task function management service.
     */
    public TaskFunctionManager getTaskFunctionManager() {
        return taskFunctionManager;
    }

    /**
     * get the task function manager.
     * @param taskFunctionManager The task function management service.
     */
    public void setTaskFunctionManager(TaskFunctionManager taskFunctionManager) {
        this.taskFunctionManager = taskFunctionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "workgroup";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getWorkgroupManager();
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getWorkgroupColumns() {
        return this.columns;
    }

    /**
     * Action for the workgroups view page. Initializes the workgroups table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View workgroupView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(workgroupView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Delete the workgroup identified by the <code>workgroupId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or
     * if the specified workgroup wasn't found (with different messages to the
     * end user)
     * @throws Exception on unanticipated exception condition
     */
    public String deleteCurrentWorkgroup() throws Exception {
        Workgroup workgroupToDelete = null;
        try {
            workgroupToDelete = workgroupManager.get(this.workgroupId);
            UserMessage msg = (UserMessage) workgroupManager.delete(workgroupToDelete);

            addSessionActionMessage(new UserMessage(
                "workgroup.delete.message.success", workgroupToDelete.getGroupName()));
            if (msg != null) {
                addSessionActionMessage(msg);
            }
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete workgroup: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#postProcessDelete(java.lang.Object)
     */
    @Override
    public void postProcessDelete(Object deleteResult) {
        if (deleteResult != null) {
            if (this.deleteResultMessages == null) {
                this.deleteResultMessages = new HashSet<UserMessage>();
            }
            this.deleteResultMessages.add((UserMessage) deleteResult);
        }
    }

    /**
     * Create or update the workgroup specified by the <code>workgroup</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        // before we save, update the functions/regions if necessary.
        if (!functionsSet) {
            assignFunctionsAndRegions(functionIdsToRegions);
        }
        boolean isNew = workgroup.isNew();
        try {
            //Uniqueness check for name
            Long nameUniquenessId = this.workgroupManager.getPrimaryDAO()
                .uniquenessByName(this.workgroup.getGroupName());
            if (nameUniquenessId != null
                && (isNew || (!isNew && nameUniquenessId.longValue() != this.workgroup
                    .getId().longValue()))) {
                addFieldError("workgroup.groupName", new UserMessage(
                    "workgroup.create.error.existing", workgroup.getGroupName()));
                // Go back to the form to show the error message.
                return INPUT;
            }
            workgroupManager.save(workgroup);
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified workgroup "
                + workgroup.getGroupName());
            addActionError(
                new UserMessage("entity.error.modified",
                                UserMessage.markForLocalization("entity.Workgroup"),
                                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT)
                    );
            // Get the modified data
            // If Workgroup has been deleted, this will throw EntityNotFoundException
            try {
                Workgroup modifiedWorkgroup = workgroupManager.get(workgroupId);
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                workgroup.setVersion(modifiedWorkgroup.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedWorkgroup);
            } catch (EntityNotFoundException ex) {
                log.warn("Workgroup " + this.workgroup.getGroupName()
                    + " not found, possibly a concurrency issue.");
                addSessionActionMessage(
                    new UserMessage("entity.error.deleted",
                                    UserMessage.markForLocalization(workgroup.getGroupName()), null)
                        );
               return SUCCESS;
            }
            return INPUT;
        } catch (BusinessRuleException e) {
            log.warn("Unable to save or update workgroup: "
                + getText(e.getUserMessage()));
            addActionError(e.getUserMessage());
            return INPUT;
        }
        // Add success message
        String successKey = "workgroup." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(new UserMessage(
            successKey,
            makeGenericContextURL(
                "/workgroup/view.action?workgroupId=" + workgroup.getId()),
                workgroup.getGroupName()));
        // Go to the success target.
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Role</code> object by retrieving it
     * from the database when a roleId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.workgroupId != null) {
            if (log.isDebugEnabled()) {
                log.debug("workgroupId is " + this.workgroupId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Workgroup is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved workgroup from session");
                }
                this.workgroup =
                    (Workgroup) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting workgroup from database");
                }
                this.workgroup = this.workgroupManager.get(this.workgroupId);
                saveEntityInSession(this.workgroup);
            }
            if (log.isDebugEnabled()) {
                log.debug("Workgroup version is: " + this.workgroup.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new workgroup object");
            }
            this.workgroup = new Workgroup();
        }
    }

    /**
     * Retrieves all the workgroup functions to display on the create page.
     * @return All workgroup functions.
     * @throws DataAccessException on database issue.
     */
    public List<WorkgroupFunction> getAllWorkgroupFunctions() throws DataAccessException {
        List<WorkgroupFunction> wFunctions = new ArrayList<WorkgroupFunction>();
        List<TaskFunction> tFunctions = taskFunctionManager.listAllOrderByNumberDesc();
        for (TaskFunction function : tFunctions) {
            // Create workgroup function from task function.
            WorkgroupFunction wf = new WorkgroupFunction();
            wf.setTaskFunction(function);
            // Call region manager to get all for functions that have regions
            // based upon the regiontype
            // then attach regions to the workgroup function.
            wf.setRegions(regionManager.listByTypeOrderByName(function.getRegionType()));
            wFunctions.add(wf);
        }
        return wFunctions;
    }

    /**
     * Ids in the form of <code>TaskFuntionID_RegionID</code>.
     * @param rawIds The unparsed identifiers.
     * @throws DataAccessException on database issue.
     */
    public void setFunctionsAndRegions(String[] rawIds) throws DataAccessException {
        if (workgroup == null || rawIds == null || rawIds.length == 0) {
            return;
        }
        functionsSet = true;
        functionIdsToRegions = new HashMap<Long, List<Region>>();
        for (String rawId : rawIds) {
            // Parse the IDs to get the function and region ids.
            String[] ids = rawId.split(FUNCTION_REGION_DELIMITER);
            if (ids.length == 1) {
                // Regions are not applicable for this function.
                functionIdsToRegions.put(Long.valueOf(ids[0]), null);
            } else if (ids.length == 2) {
                Long functionId = Long.valueOf(ids[0]);
                Long regionId = Long.valueOf(ids[1]);
                // May as well get the region now.
                Region r = regionManager.get(regionId);
                if (functionIdsToRegions.containsKey(functionId)) {
                    functionIdsToRegions.get(functionId).add(r);
                } else {
                    ArrayList<Region> regions = new ArrayList<Region>();
                    regions.add(r);
                    functionIdsToRegions.put(functionId, regions);
                }
            }
        }
        assignFunctionsAndRegions(functionIdsToRegions);
    }

    /**
     * Assign the user's selection of functions/regions to this workgroup.
     * @param functionMap A map of <code>TaskFunction</code> ids to a <code>
     * List</code> of <code>Region</code>s.
     * @throws DataAccessException on database error.
     */
    private void assignFunctionsAndRegions(Map<Long, List<Region>> functionMap)
    throws DataAccessException {
        if (functionMap == null) {
            // Remove all the functions. We check null on the functionMap
            // because Struts only persists checked checkboxes, and if the
            // user unchecks all the boxes, the setter isn't invoked.
            List<WorkgroupFunction> wfList = workgroup.getWorkgroupFunctions();
            if (wfList != null && wfList.size() > 0) {
                wfList.removeAll(wfList);
            }
        } else {
            // Add, modify, or remove the functions as appropriate.
            Set<Long> toAdd = new HashSet<Long>(functionMap.keySet());
            List<WorkgroupFunction> wfList = workgroup.getWorkgroupFunctions();
            for (Iterator<WorkgroupFunction> i = wfList.iterator(); i.hasNext();) {
                WorkgroupFunction wf = i.next();
                Long id = wf.getTaskFunction().getId();
                if (functionMap.keySet().contains(id)) {
                    // modify regions of a function
                    wf.setRegions(functionMap.get(id));
                } else {
                    // remove the entire function
                    i.remove();
                }
                toAdd.remove(id);
            }
            for (Long newId : toAdd) {
                // add an entire function and the selected regions
                workgroup.addTaskFunction(taskFunctionManager.get(newId),
                    functionMap.get(newId));
            }
        }
    }

    /**
     * Gets the identifiers for the regions in a string form as
     * "TaskFunctionID_RegionID".
     * @return A collection of strings composed of <code>TaskFunction</code> and
     * <code>Region</code> ids.
     */
    public List<String> getExistingWorkgroupFunctions() {
        ArrayList<String> functionAndRegionIds = new ArrayList<String>();
        for (WorkgroupFunction wf : workgroup.getWorkgroupFunctions()) {
            if (wf.getRegions() == null || wf.getRegions().size() == 0) {
                functionAndRegionIds.add(wf.getTaskFunction().getId()
                    + FUNCTION_REGION_DELIMITER);
            } else {
                for (Region r : wf.getRegions()) {
                    functionAndRegionIds.add(wf.getTaskFunction().getId()
                        + FUNCTION_REGION_DELIMITER
                        + r.getId());
                }
            }
        }
        return functionAndRegionIds;
    }

    /**
     * Sets the optional parameters of a workgroup. These include wether the
     * workgroup is the default or not, and wether or not to auto-add new
     * regions to the workgroup.
     * @param options Array of values from the checkbox inputs.
     */
    public void setOptions(String[] options) {
        this.workgroup.setAutoAddRegions(true);
        this.workgroup.setIsDefault(false);
        for (String option : options) {
            if ("1".equals(option)) {
                this.workgroup.setIsDefault(true);
            } else if ("2".equals(option)) {
                this.workgroup.setAutoAddRegions(false);
            }
        }
    }

    /**
     * Retrieves the state of auto-addition in the context of this edit form.
     * This form refers to the auto-addition variable inversely, so we must negate.
     * @return The opposite of the autoAddRegions property of the workgroup.
     */
    public Boolean getAutoAddRegions() {
        return !this.workgroup.getAutoAddRegions();
    }

    /**
     * Getter for the WORKGROUP_VIEW_ID property.
     * @return long value of the property
     */
    public static long getWorkgroupViewId() {
        return VIEW_ID;
    }

    /**
     * @return Sorted list of workgroup functions
     */
    public List<WorkgroupFunction> getSortedWorkgroupFunctions() {
        List<WorkgroupFunction> wfs = workgroup.getWorkgroupFunctions();
        Collections.sort(wfs,
            new PropertyComparator<WorkgroupFunction>(
                "taskFunction.functionNumber", false));
        return wfs;

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * @return the current Workgroup name, translated for the current Locale, or
     * the untranslated name if the workgroup name isn't translated for this Locale,
     * or <code>null</code> if the current workgroup isn't defined.
     */
    public String getTranslatedWorkgroupName() {
        if (getWorkgroup() == null) {
            return null;
        } else {
            return getDataTranslation(getWorkgroup().getGroupName());
        }
    }

    /**
     * @return the current workgroup name, translated for the current Locale, or
     * <code>null</code> if the workgroup name isn't translated for this Locale,
     * or if the current workgroup isn't defined.
     */
    public String getTranslatedWorkgroupNameOrNull() {
        if (getWorkgroup() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getWorkgroup().getGroupName());
        }
    }

    /**
     * @return the message containing the Locale and the translated workgroup name,
     *         or null if there is no translation.
     */
    public String getTranslatedWorkgroupNameMessage() {
        return getDataTranslationMessage(getTranslatedWorkgroupNameOrNull());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 