/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;


import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Region;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 *
 *
 * @author khazra
 */
public abstract class RegionActionRoot extends DataProviderAction implements Preparable {

    //
    private static final long serialVersionUID = 5824301006857941610L;

    public static final Logger log = new Logger(RegionAction.class);

    // The list of columns for the operators table.
    private List<Column> columns;

    // The ID of the Region.
    private Long regionId;

    /**
     * Getter for the columns property.
     * @return value of the property
     */
    public List<Column> getRegionColumns() {
        return columns;
    }


    /**
     * Setter for the columns property.
     * @param columns the new columns value
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }


    /**
     * Getter for the regionId property.
     * @return Long value of the property
     */
    public Long getRegionId() {
        return regionId;
    }


    /**
     * Setter for the regionId property.
     * @param regionId the new regionId value
     */
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }


    /**
     * Action for the regions view page. Initializes the regions table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View regionView = getUserPreferencesManager().getView(getStaticViewId());
        this.columns = this.getUserPreferencesManager().getColumns(
            regionView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(getStaticViewId());
        return viewIds;
    }


    /**
     * @return the static value representing the VIEW_ID, defined
     * by subclasses.
     */
    protected abstract long getStaticViewId();

    /**
     * @return the current region name, translated for the current Locale, or
     * the untranslated name if the region name isn't translated for this Locale,
     * or <code>null</code> if the current region isn't defined.
     */
    public String getTranslatedRegionName() {
        if (getRegion() == null) {
            return null;
        } else {
            return getDataTranslation(getRegion().getName());
        }
    }

    /**
     * @return the current region description, translated for the current
     *         Locale, or the untranslated description if the region description
     *         isn't translated for this Locale, or <code>null</code> if the
     *         current region isn't defined.
     */
    public String getTranslatedRegionDescription() {
        if (getRegion() == null) {
            return null;
        } else {
            return getDataTranslation(getRegion().getDescription());
        }
    }

    /**
     * @return the current region name, translated for the current Locale, or
     * <code>null</code> if the region name isn't translated for this Locale,
     * or if the current region isn't defined.
     */
    public String getTranslatedRegionNameOrNull() {
        if (getRegion() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getRegion().getName());
        }
    }

    /**
     * @return the current region description, translated for the current
     *         Locale, or <code>null</code> if the region description isn't
     *         translated for this Locale, or if the current region isn't
     *         defined.
     */
    public String getTranslatedRegionDescriptionOrNull() {
        if (getRegion() == null) {
            return null;
        } else {
            return getDataTranslationOrNull(getRegion().getDescription());
        }
    }

    /**
     * @return the message containing the Locale and the translated region name,
     *         or null if there is no translation.
     */
    public String getTranslatedRegionNameMessage() {
        return getDataTranslationMessage(getTranslatedRegionNameOrNull());
    }

    /**
     * @return the message containing the Locale and the translated region
     *         description, or null if there is no translation.
     */
    public String getTranslatedRegionDescriptionMessage() {
        return getDataTranslationMessage(getTranslatedRegionDescriptionOrNull());
    }

    /**
     * @return the current Region object, if any.
     */
    protected abstract Region getRegion();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 