/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.OperandType;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This class handles the Actions for Vehicle Safety Check Responses Table
 * Component.
 * 
 * @author khazra
 */
public class VehicleSafetyCheckResponseActionRoot extends DataProviderAction
    implements Preparable {

    // Serial Version UID for the class.
    private static final long serialVersionUID = 1337139578267810361L;

    private static final long CHECK_RESPONSE_VIEW_ID = -1224;

    public static final int INITIAL = -3;

    private List<Column> checkResponseColumns;

    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager;

    private String newNote = null;

    /**
     * Getter for the checkResponseColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getCheckResponseColumns() {
        return checkResponseColumns;
    }

    /**
     * Setter for the checkResponseColumns property.
     * @param checkResponseColumns the new checkResponseColumns value
     */
    public void setCheckResponseColumns(List<Column> checkResponseColumns) {
        this.checkResponseColumns = checkResponseColumns;
    }

    /**
     * Getter for the vehicleSafetyCheckResponseManager property.
     * @return VehicleSafetyCheckResponseManager value of the property
     */
    public VehicleSafetyCheckResponseManager getVehicleSafetyCheckResponseManager() {
        return vehicleSafetyCheckResponseManager;
    }

    /**
     * Setter for the vehicleSafetyCheckResponseManager property.
     * @param vehicleSafetyCheckResponseManager the new
     *            vehicleSafetyCheckResponseManager value
     */
    public void setVehicleSafetyCheckResponseManager(VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }

    /**
     * Getter for the newNote property.
     * @return String value of the property
     */
    public String getNewNote() {
        return newNote;
    }

    /**
     * Setter for the newNote property.
     * @param newNote the new newNote value
     */
    public void setNewNote(String newNote) {
        this.newNote = newNote;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getVehicleSafetyCheckResponseManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "vehicleSafetyCheck";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CHECK_RESPONSE_VIEW_ID);
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        super.getTableData("obj.checkResponse not in ( " + INITIAL + ") ");
        return SUCCESS;
    }

    /**
     * Method is invoked when a request is made to list.action for the Safety
     * Checks Responses view page. Initializes the Safety Check Responses table
     * columns. And adds a default filter on check date for showing safety check
     * responses from last 7 days
     * 
     * @return String value of outcome. SUCCESS
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {

        // Add default filter for showing responses from last 7 days to reduce
        // the amount of upfront data getting into TC
        final int dateColumnId = -22808;

        // Search the date column in the already existing filters
        List<Filter> filters = this.getFilters();

        boolean dataFilterFound = false;
        for (Filter filter : filters) {
            if (filter != null) {
                List<FilterCriterion> filterCriterias = filter
                    .getFilterCriteria();
                for (FilterCriterion filterCriteria : filterCriterias) {
                    if (filterCriteria.getColumn().getId() == dateColumnId) {
                        dataFilterFound = true;
                        break;
                    }
                }
            }
        }

        // Set 7 day date filter only if there is not other date filters
        // specified by user
        if (!dataFilterFound) {
            String filterCriterion = "{'viewId':'" + CHECK_RESPONSE_VIEW_ID
                + "','columnId':'" + dateColumnId + "','operandId':'"
                + OperandType.WITHIN.getId()
                + "','value1':'6','value2':'','locked':false}";

            setFilters(getFilterManager().
                constructFilterFromSerializedJSON(filterCriterion));
            
            // We should also save it
            getFilterManager().addFilters(getFilters(),
                                          getCurrentUser());
        }

        
        View checkResponseView = getUserPreferencesManager().getView(
            CHECK_RESPONSE_VIEW_ID);
        this.checkResponseColumns = this.getUserPreferencesManager()
            .getColumns(checkResponseView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * @return the checkResponse View ID
     */
    public static long getCheckResponseViewId() {
        return CHECK_RESPONSE_VIEW_ID;
    }

    /**
     * Method to edit the notes
     * @return JSON code
     * @throws DataAccessException
     */
    public String editNotes() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "editNotes";
            }

            public boolean execute(DataObject obj) throws VocollectException,
                DataAccessException, BusinessRuleException {
                for (Long id : getIds()) {
                    VehicleSafetyCheckResponse response = vehicleSafetyCheckResponseManager
                        .get(id);
                    response.setNote(newNote);

                    vehicleSafetyCheckResponseManager.save(response);
                }
                return true;
            }
        });
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 