/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.web.cyclecounting.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.hibernate.classic.Validatable;

/**
 * @author khazra
 * 
 */
public class CycleCountingRegionActionRoot extends RegionAction implements
        Preparable, Validatable {

    private static final long serialVersionUID = 703104429462256025L;

    private CycleCountingRegionManager cycleCountingRegionManager;

    private CycleCountingRegion cycleCountingRegion;

    private static final long VIEW_ID = -1208;

    /**
     * @return the cycleCountingRegionManager
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * @param cycleCountingRegionManager
     *            the cycleCountingRegionManager to set
     */
    public void setCycleCountingRegionManager(
            CycleCountingRegionManager cycleCountingRegionManager) {
        this.cycleCountingRegionManager = cycleCountingRegionManager;
    }

    /**
     * @return the cycleCountingRegion
     */
    public CycleCountingRegion getCycleCountingRegion() {
        return cycleCountingRegion;
    }

    /**
     * @return A default instance of cycle counting region.
     */
    private CycleCountingRegion getDefaultCycleCountingRegion() {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.SystemDirected);
        return ccRegion;
    }

    /**
     * @param cycleCountingRegion
     *            the cycleCountingRegion to set
     */
    public void setCycleCountingRegion(CycleCountingRegion cycleCountingRegion) {
        this.cycleCountingRegion = cycleCountingRegion;
    }

    /**
     * @param countType
     *            the integer value of count type
     */
    public void setCountType(Integer countType) {
        switch (countType) {
        case 1:
            cycleCountingRegion.setCountType(CountType.Blind);
            break;
        case 2:
            cycleCountingRegion.setCountType(CountType.Known);
            break;
        default:
            break;
        }

    }

    /**
     * @return The integer value of count type, if region is null, returns 1
     */
    public Integer getCountType() {
        Integer countType = 1;
        if (cycleCountingRegion != null) {
            countType = cycleCountingRegion.getCountType().toValue();
        }

        return countType;
    }

    /**
     * 
     * @param countMode
     *            the integer value of count type.
     */
    public void setCountMode(Integer countMode) {
        switch (countMode) {
        case 1:
            cycleCountingRegion.setCountMode(CountMode.SystemDirected);
            break;
        case 2:
            cycleCountingRegion.setCountMode(CountMode.OperatorDirected);
            break;
        default:
            break;
        }
    }

    /**
     * @return The integer value of count type, if region is null, returns 1
     */
    public Integer getCountMode() {
        Integer countMode = 1;
        if (cycleCountingRegion != null) {
            countMode = cycleCountingRegion.getCountMode().toValue();
        }

        return countMode;
    }

    @Override
    public void prepare() throws Exception {
        if (getRegionId() != null) {
            // We have an ID, but not a region object yet.
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + this.getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved replenishment region from session");
                }
                this.cycleCountingRegion = (CycleCountingRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting replenishment region from database");
                }
                this.cycleCountingRegion = this.cycleCountingRegionManager
                        .get(this.getRegionId());
                saveEntityInSession(this.cycleCountingRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Replenishment region version is: "
                        + this.cycleCountingRegion.getVersion());
            }
        } else if (this.cycleCountingRegion == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new cyclecounting region object");
            }
            this.cycleCountingRegion = getDefaultCycleCountingRegion();
        }
    }

    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }

    @Override
    protected Region getRegion() {
        return this.cycleCountingRegion;
    }

    @Override
    protected DataProvider getManager() {
        return this.cycleCountingRegionManager;
    }

    @Override
    protected String getKeyPrefix() {
        return "cycleCountingRegion";
    }

    /**
     * Getter for the REGION_VIEW_ID property.
     * 
     * @return long value of the property
     */
    public long getRegionViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Returns the list of count mode.
     * 
     * @return Map of count mode value and their string presentation.
     */
    public Map<Integer, String> getCountModeOptions() {
        Map<Integer, String> countModeMap = new HashMap<Integer, String>();
        CountMode[] countModes = CountMode.values();

        for (CountMode countMode : countModes) {
            countModeMap.put(countMode.toValue(), getText(countMode.getResourceKey()));
        }

        return countModeMap;
    }

    /**
     * Returns the list of count types.
     * 
     * @return Map of count type value and their string presentation.
     */
    public Map<Integer, String> getCountTypeOptions() {
        Map<Integer, String> countTypeMap = new HashMap<Integer, String>();
        CountType[] countTypes = CountType.values();

        for (CountType countType : countTypes) {
            countTypeMap.put(countType.toValue(),
                    getText(countType.getResourceKey()));
        }

        return countTypeMap;
    }

    /**
     * 
     * @return Returns success, failure or input
     */
    public String save() throws Exception {
        boolean isNew = this.cycleCountingRegion.isNew();
        try {
            this.cycleCountingRegion.setType(RegionType.CycleCounting);
            this.cycleCountingRegionManager.save(this.cycleCountingRegion);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                if (fve.getField() == "region.name") {
                    fve.setField("cycleCountingRegion.name");
                } else {
                    fve.setField("cycleCountingRegion.number");
                }
                log.warn(fve.getField() + " " + fve.getValue()
                        + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (DataAccessException e) {
            log.warn("Attempt to change already modified region "
                    + this.cycleCountingRegion.getName());
            addActionError(new UserMessage("entity.error.modified",
                    UserMessage
                            .markForLocalization("entity.ReplenishmentRegion"),
                    SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If the entity has been deleted, this will throw
            // EntityNotFoundException
            try {
                CycleCountingRegion modifiedEntity = getCycleCountingRegionManager()
                        .get(getRegionId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.cycleCountingRegion
                        .setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                        UserMessage
                                .markForLocalization(this.cycleCountingRegion
                                        .getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }
        
        // add success message
        String successKey = "cyclecounting.region." + (isNew ? "create" : "edit")
            + ".message.success";
        addSessionActionMessage(
            new UserMessage(successKey,
                makeContextURL("/cyclecounting/region/view.action?regionId=" + cycleCountingRegion.getId()),
                this.cycleCountingRegion.getName()));

        // Go to the success target.
        
        return SUCCESS;

    }

    /**
     * Function for validating if a region is editable or not.
     * 
     * @return SUCCESS
     */
    public String checkIfEditable() {

        try {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.cycleCountingRegion = (CycleCountingRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.cycleCountingRegion = this.cycleCountingRegionManager
                        .get(getIds()[0]);
                saveEntityInSession(this.cycleCountingRegion);
            }
            cycleCountingRegionManager
                    .executeValidateEditRegion(this.cycleCountingRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        log.info("REDIRECTING");
        setJsonMessage("edit!input.action?regionId=" + getIds()[0],
                ERROR_REDIRECT);

        return SUCCESS;
    }

    /**
     * Deletes the currently viewed region.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

        CycleCountingRegion regionToDelete = null;

        try {
            regionToDelete = cycleCountingRegionManager.get(getRegionId());
            cycleCountingRegionManager.delete(getRegionId());

            addSessionActionMessage(new UserMessage(
                    "cyclecounting.region.delete.message.success",
                    regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 