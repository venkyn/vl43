/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.cyclecounting.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.cyclecounting.CycleCountingErrorCode;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingActionRoot extends DataProviderAction implements
        Preparable {

    /**
     * 
     */
    private static final long serialVersionUID = -6399233520866430884L;

    private static final Logger log = new Logger(CycleCountingActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long CYCLECOUNTING_VIEW_ID = -1209;

    private static final long CYCLECOUNTING_DETAIL_VIEW_ID = -1210;

    // The list of columns for the Cycle table.
    private List<Column>                    cyclecountingColumns;

    private List<Column>                    detailColumns;

    // The CycleCountingAssignment management service.
    private CycleCountingAssignmentManager  cycleCountingAssignmentManager = null;

    private CycleCountingDetailManager      cycleCountingDetailManager = null;

    private LocationManager                 locationManager = null;

    // Operator Manager.
    private OperatorManager                 operatorManager = null;

    
    private Integer                         cycleCountStatus;

    private Integer                         status;

    // assignment Status List
    private Map<Integer, String>            cycleCountAssignmentStatus;
    

    private CycleCountingAssignment         cycleCountingAssignment;

    private Long                            cycleCountId;

    private Collection<String>              cycleCountIds;
    
    
    private Long                            cycleCountOperatorId;
    
    private Long                            operatorId;


    
    
    /**
     * @return the operatorId
     */
    public Long getOperatorId() {
        return operatorId;
    }

    /**
     * @param operatorId the operatorId to set
     */
    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * @param cycleCountOperatorId
     *            the cycleCountOperatorId to set
     * @throws Exception
     */
    public void setCycleCountOperatorId(Long cycleCountOperatorId)
            throws Exception {
        this.cycleCountOperatorId = cycleCountOperatorId;
        if (cycleCountOperatorId != 0L) {
        this.cycleCountingAssignment.setOperator(this.getOperatorManager().get(cycleCountOperatorId));
        } else {
            this.cycleCountingAssignment.setOperator(null);
        }
    }

    /**
     * @return the cycleCountingAssignment
     */
    public CycleCountingAssignment getCycleCountingAssignment() {
        return this.cycleCountingAssignment;
    }

    /**
     * @param cycleCountingAssignment
     *            the cycleCountingAssignment to set
     */
    public void setCycleCountingAssignment(
            CycleCountingAssignment cycleCountingAssignment) {
        this.cycleCountingAssignment = cycleCountingAssignment;
    }

    /**
     * @return the cycleCountId
     */
    public Long getCycleCountId() {
        return cycleCountId;
    }

    /**
     * @param cycleCountId
     *            the cycleCountId to set
     */
    public void setCycleCountId(Long cycleCountId) {
        this.cycleCountId = cycleCountId;
    }

    /**
     * @return the cycleCountIds
     */
    public Collection<String> getCycleCountIds() {
        return this.cycleCountIds;
    }

    /**
     * @param cycleCountIds
     *            the cycleCountIds to set
     */
    public void setCycleCountIds(Collection<String> cycleCountIds) {
        this.cycleCountIds = cycleCountIds;
    }

    /**
     * @return the detailColumns
     */
    public List<Column> getDetailColumns() {
        return detailColumns;
    }

    /**
     * @param detailColumns
     *            the detailColumns to set
     */
    public void setDetailColumns(List<Column> detailColumns) {
        this.detailColumns = detailColumns;
    }

    /**
     * @return the cycleCountingDetailManager
     */
    public CycleCountingDetailManager getCycleCountingDetailManager() {
        return cycleCountingDetailManager;
    }

    /**
     * @param cycleCountingDetailManager
     *            the cycleCountingDetailManager to set
     */
    public void setCycleCountingDetailManager(
            CycleCountingDetailManager cycleCountingDetailManager) {
        this.cycleCountingDetailManager = cycleCountingDetailManager;
    }

    /**
     * @return the cyclecountingDetailViewId
     */
    public static long getDetailViewId() {
        return CYCLECOUNTING_DETAIL_VIEW_ID;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager
     *            the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return the locationManager
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * @param locationManager
     *            the locationManager to set
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the cyclecountingViewId
     */
    public static long getCyclecountingViewId() {
        return CYCLECOUNTING_VIEW_ID;
    }

    /**
     * @return the cyclecountingColumns
     */
    public List<Column> getCyclecountingColumns() {
        return cyclecountingColumns;
    }

    /**
     * @param cyclecountingColumns
     *            the cyclecountingColumns to set
     */
    public void setCyclecountingColumns(List<Column> cyclecountingColumns) {
        this.cyclecountingColumns = cyclecountingColumns;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return this.cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * Create or update the user specified by the <code>cyclecounting</code>
     * member of this class.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = cycleCountingAssignment.isNew();

        // Need to save this once to attach it back to the session
        // before changing status
        getCycleCountingAssignmentManager().getPrimaryDAO().reattach(this.cycleCountingAssignment);
        
        try {
            if (isNew) {
                this.cycleCountingAssignmentManager
                        .save(this.cycleCountingAssignment);
            } else {
                // otherwise we are editing the assignment
                this.cycleCountingAssignmentManager
                        .executeUpdateCycleCountingAssignment(this.cycleCountingAssignment);
            }
            cleanSession();
        } catch (EntityNotFoundException e) {
            log.warn("Assignment Number "
                    + this.cycleCountingAssignment.getId()
                    + " not found, possibly a concurrency issue");

            addSessionActionMessage(new UserMessage(
                    "cyclecounting.edit.error.cyclecountingNotFound", null,
                    null));
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified cycle counting assignment "
                    + this.cycleCountingAssignment.getId());
            addActionError(new UserMessage("entity.error.modified",
                    UserMessage.markForLocalization("entity.cyclecounting"),
                    SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                CycleCountingAssignment modifiedCycleCountingAssignment = getCycleCountingAssignmentManager()
                        .get(getCycleCountId());
                // Set the local object's version to match, so it will work if
                // the user resubmits.
                this.cycleCountingAssignment
                        .setVersion(modifiedCycleCountingAssignment
                                .getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedCycleCountingAssignment);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                        this.cycleCountingAssignment.getId()));
                return SUCCESS;
            }
            return INPUT;
        }
        addSessionActionMessage(new UserMessage("cyclecounting."
                + (isNew ? "create" : "edit") + ".message.success",
                String.valueOf(this.cycleCountingAssignment.getId())));

        return SUCCESS;
    }

    /**
     * Action for the cyclecounting view page. Initializes the cyclecounting
     * table columns.
     * 
     * @return String value of outcome.
     * @throws Exception
     *             in case of DataAccess issues
     */
    public String list() throws Exception {
        View cyclecountingView = getUserPreferencesManager().getView(
                CYCLECOUNTING_VIEW_ID);
        this.cyclecountingColumns = this.getUserPreferencesManager()
                .getColumns(cyclecountingView, getCurrentUser());
        View detailView = getUserPreferencesManager().getView(
                CYCLECOUNTING_DETAIL_VIEW_ID);
        this.detailColumns = this.getUserPreferencesManager().getColumns(
                detailView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Get the map for function of cycle counting for edit form.
     * 
     * @return the CycleCounting assignment CycleCount Status.
     */
    public Map<Integer, String> getCycleCountStatusMap() {
        cycleCountAssignmentStatus = new HashMap<Integer, String>();
        cycleCountAssignmentStatus.put(CycleCountStatus.Available.toValue(),
                ResourceUtil.getLocalizedEnumName(CycleCountStatus.Available));
        cycleCountAssignmentStatus
                .put(CycleCountStatus.Unavailable.toValue(), ResourceUtil
                        .getLocalizedEnumName(CycleCountStatus.Unavailable));
        cycleCountAssignmentStatus.put(CycleCountStatus.Canceled.toValue(),
                ResourceUtil.getLocalizedEnumName(CycleCountStatus.Canceled));
        return cycleCountAssignmentStatus;
    }
    
    /**
     * @return cycleCountAssignmentOperatorId - map of operator IDs.
     * @throws Exception
     */
    public Map<Long, String> getCycleCountOperatorMap() throws Exception {
        Map<Long, String> cycleCountAssignmentOperatorId = new HashMap<Long, String>();
        
        try {
            cycleCountAssignmentOperatorId.put(0L, getText(new UserMessage("operator.none")));
            /*
             * If the user had selected single assignment, 'edited' the assignment
             * and then wants to change/reassign a operator
             * i.e the assignemnt is known when operator is being reassigned
             */
            if (this.cycleCountingAssignment != null) {
                Region a = this.cycleCountingAssignment.getRegion();
                Operator op = this.cycleCountingAssignment.getOperator();
                CycleCountingAssignment asmt = this.cycleCountingAssignment;

                if (asmt.getOperator() != null) {
                    if (op.getId() != null) {
                        cycleCountAssignmentOperatorId.put(op.getId(), op.getOperatorIdentifier());
                    }
                }
                List<Operator> allOperators = getOperatorManager()
                        .listAuthorizedForRegion(a.getId());
                for (Operator operatorname : allOperators) {
                    cycleCountAssignmentOperatorId.put(operatorname.getId(), operatorname
                            .getOperatorIdentifier());
                }
            } else {
                /*
                 * If the user had selected mutiple assignments
                 * and then wants to change/reassign operators of all these 
                 * assignments, i.e the assignemnt number is not known
                 */
                for (Operator o : this.getOperatorManager().listAllOrderByName()) {
                    cycleCountAssignmentOperatorId.put(o.getId(),
                            o.getOperatorIdentifier());
                }
            }
        } catch (Exception e) {
            log.warn("Error occurred while obtaining Operators in Region");
        }
        return cycleCountAssignmentOperatorId;

    }

    /**
     * Get the map for function of CycleCounting for modify assignment status.
     * 
     * @return the Cycle Counting assignment Status.
     */
    public Map<Integer, String> getCycleCountingAssignmentStatus() {
        cycleCountAssignmentStatus = new HashMap<Integer, String>();
        cycleCountAssignmentStatus.put(CycleCountStatus.Available.toValue(),
                ResourceUtil.getLocalizedEnumName(CycleCountStatus.Available));
        cycleCountAssignmentStatus
        .put(CycleCountStatus.Unavailable.toValue(), ResourceUtil
                .getLocalizedEnumName(CycleCountStatus.Unavailable));
        cycleCountAssignmentStatus
        .put(CycleCountStatus.Canceled.toValue(), ResourceUtil
                .getLocalizedEnumName(CycleCountStatus.Canceled));
        return cycleCountAssignmentStatus;
    }

    
    /**
     * Getter for the cycleCountStatusForChange property.
     * 
     * @return String value of the property
     * @throws DataAccessException
     *             - unable to retrieve cyclecount assignment
     */
    public String getCycleCountingAssignmentStatusForChange()
            throws DataAccessException {
        if (getCycleCountingAssignmentStatus().isEmpty()) {
            setJsonMessage(getText(new UserMessage(
                    "cyclecounting.label.error.message", ERROR_FAILURE)),
                    ERROR_FAILURE);
            return SUCCESS;
        } else {
            return INPUT;
        }
    }

    /**
     * Getter for the cycleCountStatusForChange property.
     * 
     * @return String value of the property
     * @throws Exception 
     */
    public String getCycleCountingAssignmentOperatorForChange()
            throws Exception {
        getCycleCountOperatorMap();
        return INPUT;
    }

   /**
     * Getter for the cycleCountStatus property.
     * 
     * @return String value of the property
     */
    public Integer getCycleCountStatus() {
        if (this.cycleCountingAssignment != null) {
            if (this.cycleCountingAssignment.getStatus() == CycleCountStatus.Unavailable) {
                return CycleCountStatus.Unavailable.toValue();
            } else if (this.cycleCountingAssignment.getStatus() == CycleCountStatus.Available) {
                return CycleCountStatus.Available.toValue();
            } else if (this.cycleCountingAssignment.getStatus() == CycleCountStatus.InProgress) {
                return CycleCountStatus.InProgress.toValue();
            } else if (this.cycleCountingAssignment.getStatus() == CycleCountStatus.Complete) {
                return CycleCountStatus.Complete.toValue();
            } else if (this.cycleCountingAssignment.getStatus() == CycleCountStatus.Canceled) {
                return CycleCountStatus.Canceled.toValue();
            }
        }
        return 0;
    }
    
    
    /**
     * @return - the operatorId
     */
    public Long getCycleCountOperatorId() {
        if (this.cycleCountingAssignment != null) {
            return this.getCycleCountingAssignment().getOperator().getId();
        }
        return 0L;
    }

    /**
     * Function for validating if an assignment is editable or not.
     * 
     * @return String defining next target to forward to.
     * @throws BusinessRuleException
     *             - on exception
     */
    public String checkIfEditable() throws BusinessRuleException {

        try {
            CycleCountingAssignment c = this.cycleCountingAssignmentManager
                    .get(getIds()[0]);
            validateIfEditable(c);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating assignment for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?cycleCountId=" + getIds()[0],
                ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Function to check for editing validation business logic.
     * 
     * @param c
     *            Cycle Counting Assignment to be validated
     * @throws BusinessRuleException
     *             if the business rule is violated.
     */
    protected void validateIfEditable(CycleCountingAssignment c)
            throws BusinessRuleException {

        if (c.getStatus().isInSet(CycleCountStatus.Canceled,
                CycleCountStatus.InProgress, CycleCountStatus.Complete)) {

            throw new BusinessRuleException(
                    CycleCountingErrorCode.EDIT_ASSIGNMENT_INVALID_STATUS,
                    new UserMessage("cyclecounting.edit.not.editable"));
        }
    }

    /**
     * Setter for the replenStatus property.
     * 
     * @param cycleCountStatus
     *            the new cycleCountStatus value
     */
    public void setCycleCountStatus(Integer cycleCountStatus) {
        this.cycleCountStatus = cycleCountStatus;
        this.cycleCountingAssignment.setStatus(CycleCountStatus
                .toEnum(this.cycleCountStatus));
    }

    /**
     * changeCycleCountAssignmentStatus - changes the status of cycle counting assignments.
     * 
     * @return String value - controls the xwork flow
     * @throws DataAccessException
     *             - when cycle counting assignment cannot be retrieved
     */
    public String changeCycleCountAssignmentStatus() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "status";
            }

            public boolean execute(DataObject obj) throws VocollectException,
                    DataAccessException, BusinessRuleException {
                getCycleCountingAssignmentManager().executeUpdateStatus(
                        (CycleCountingAssignment) obj,
                        CycleCountStatus.toEnum(status));
                return true;
            }
        });
    }

    /**
     * changeCycleCountAssignmentOperator - changes the operator of cycle counting assignments.
     * 
     * @return String value - controls the xwork flow
     * @throws DataAccessException
     *             - when cycle counting assignment cannot be retrieved
     */
    public String changeCycleCountAssignmentOperator() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "operator";
            }

            public boolean execute(DataObject obj) throws VocollectException,
                    DataAccessException, BusinessRuleException {
                getCycleCountingAssignmentManager().executeUpdateOperator(
                        (CycleCountingAssignment) obj,
                        operatorId);
                return true;
            }
        });
    }

    @Override
    public void prepare() throws Exception {
        if (this.cycleCountId != null) {
            // We have an ID, but not a CycleCountingAssignment object yet.
            if (log.isDebugEnabled()) {
                log.debug("cycleCountId is " + this.cycleCountId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the User is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved cyclecount assignment from session");
                }
                this.cycleCountingAssignment = (CycleCountingAssignment) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting cyclecount assignment from database");
                }
                this.cycleCountingAssignment = this.cycleCountingAssignmentManager
                        .get(this.cycleCountId);
                saveEntityInSession(this.cycleCountingAssignment);  
            }
        }
    }

    @Override
    protected DataProvider getManager() {
        return this.getCycleCountingAssignmentManager();
    }

    @Override
    protected String getKeyPrefix() {
        return "cyclecounting";
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CYCLECOUNTING_VIEW_ID);
        viewIds.add(CYCLECOUNTING_DETAIL_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 