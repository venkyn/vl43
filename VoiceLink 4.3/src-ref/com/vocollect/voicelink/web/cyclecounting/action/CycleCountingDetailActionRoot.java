/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.cyclecounting.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * 
 * 
 * @author pkolonay
 */
public class CycleCountingDetailActionRoot extends DataProviderAction implements
        Preparable {

    /**
     * 
     */
    private static final long serialVersionUID = -7684133301550086522L;

    
    // private static final Logger log = new
    // Logger(CycleCountingDetailAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1210;

    // The list of columns for the picks table.
    private List<Column> columns;

    // The Assignment-Pick management service.
    private CycleCountingDetailManager cycleCountingDetailManager = null;

    // A comma separated String of IDs to query on
    private String cycleCountId = null;

    // The Pick object, which will either be newly created, or retrieved
    // via the PickId.
    private CycleCountingDetail detail;

    // The ID of the Pick.
    private Long detailId;

    
    /**
     * @return the viewId
     */
    public static long getDetailViewId() {
        return VIEW_ID;
    }

    /**
     * @return the cycleCountingDetailManager
     */
    public CycleCountingDetailManager getCycleCountingDetailManager() {
        return cycleCountingDetailManager;
    }

    /**
     * @param cycleCountingDetailManager
     *            the cycleCountingDetailManager to set
     */
    public void setCycleCountingDetailManager(
            CycleCountingDetailManager cycleCountingDetailManager) {
        this.cycleCountingDetailManager = cycleCountingDetailManager;
    }

    /**
     * @return the detail
     */
    public CycleCountingDetail getDetail() {
        return detail;
    }

    /**
     * @param detail
     *            the detail to set
     */
    public void setDetail(CycleCountingDetail detail) {
        this.detail = detail;
    }

    /**
     * @return the detailId
     */
    public Long getDetailId() {
        return detailId;
    }

    /**
     * @param detailId
     *            the detailId to set
     */
    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    /**
     * Getter for the columns property.
     * 
     * @return List of Column value of the property.
     */
    public List<Column> getDetailColumns() {
        return this.columns;
    }

    
    
    /**
     * Action for the picks view page. Initializes the picks table columns.
     * 
     * @return String value of outcome.
     * @throws Exception
     *             in case of DataAccess issues
     */
    public String list() throws Exception {
        View detailView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(detailView,
                getCurrentUser());
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it from
     * the database when a userId is set by the form submission. {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "CycleCountDetail";
    }

    /**
     * Getter for the cycleCountId property.
     * 
     * @return the assigmentID
     */
    public String getCycleCountId() {
        return cycleCountId;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getCycleCountId() != null && getCycleCountId().length() > 0) {
            super.getTableData("obj.cycleCountingAssignment.id in ( " + this.cycleCountId
                    + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getPrintData()
     */
    @Override
    public String getPrintData() throws Exception {
        if (getCycleCountId() != null && getCycleCountId().length() > 0) {
            super.getPrintData("obj.assignment.id in ( " + this.cycleCountId
                    + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Setter for the cycleCountId property.
     * 
     * @param cycleCountId
     *            - the id to be set
     */
    public void setCycleCountId(String cycleCountId) {
        this.cycleCountId = cycleCountId;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    @Override
    protected DataProvider getManager() {
        return this.getCycleCountingDetailManager();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 