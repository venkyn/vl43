/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.cyclecounting.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.web.core.action.LaborAction;

import java.util.List;

/**
 * This class handles all cycle counting labor actions from the user interface.
 * 
 * @author khazra
 */
public class CycleCountingLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = -2237387693142554480L;
    
    private static final Long ASSIGNMENT_LABOR_VIEW_ID = -1211L;
    
    private List<Column> assignmentLaborColumns;
    
    // Request parameter for assignment labor screen.
    private String operatorLaborId;

    /**
     * Gets the columns for the assignment labor screen.
     * @return List of assignment labor columns.
     */
    public List<Column> getAssignmentLaborColumns() {
        return assignmentLaborColumns;
    }

    /**
     * Sets the columns for the assignment labor screen.
     * @param assignmentLaborColumns The columns for the assignment labor
     * screen to display.
     */
    public void setAssignmentLaborColumns(List<Column> assignmentLaborColumns) {
        this.assignmentLaborColumns = assignmentLaborColumns;
    }
    
    /**
     * Gets the request parameter value for operatorLaborId.
     * @return Value of the operatorLaborId request param.
     */
    public String getOperatorLaborId() {
        return operatorLaborId;
    }

    /**
     * Sets the request parameter value for operatorLaborId.
     * @param operatorLaborId The value to set.
     */
    public void setOperatorLaborId(String operatorLaborId) {
        this.operatorLaborId = operatorLaborId;
    }
    
    /**
     * Getter for view Id.
     * @return The ID of the cycle counting assignment labor view.
     */
    public Long getAssignmentLaborViewId() {
        return ASSIGNMENT_LABOR_VIEW_ID;
    }
    
    /**
     * Action method for Putaway home page labor summary (table component).
     * 
     * @return summary data to display
     * @throws Exception
     *             On error.
     */
    public String getSummaryData() throws Exception {
        return this.getTableData(null,
                new Object[] { OperatorLaborFilterType.CycleCounting,
                        RegionType.CycleCounting });
    }

    /**
     * Action method for Putaway home page labor summary (Printable version
     * action).
     * 
     * @return summary data to print
     * @throws Exception
     *             On error.
     */
    public String getSummaryPrintData() throws Exception {
        return this.getPrintData(null,
                new Object[] { OperatorLaborFilterType.CycleCounting,
                        RegionType.CycleCounting });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String list() throws Exception {
        super.list();
        if (getFilterByFunction()) {
            overrideFunctionFilter(OperatorLaborFilterType.CycleCounting);
        }
        return SUCCESS;
    }
    
    /**
     * Populates the columns for the assignment labor table.
     * @return The Struts target name.
     * @throws DataAccessException On database error.
     */
    public String getAssignmentList() throws DataAccessException {
        View view = getUserPreferencesManager().getView(ASSIGNMENT_LABOR_VIEW_ID);
        this.assignmentLaborColumns = getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getAssignmentData() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getTableData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }
    
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = super.getViewIds();
        viewIds.add(ASSIGNMENT_LABOR_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 