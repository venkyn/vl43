/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service;

import com.vocollect.voicelink.web.service.rro.DeliveryLocationMappingRRO;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * A web service for Viewing/Adding/Updating/Deleting
 * <code>DeliveryLocationMapping</code>.
 * 
 * @author mlashinsky
 */
@WebService
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_XML })
@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON, MediaType.TEXT_HTML, MediaType.TEXT_XML })
public abstract interface DeliveryLocationWebServiceRoot {

    
    /**
     * @param mappings the mappings.
     * @return long
     */
    @POST
    @WebMethod
    @Path("/create")
    public String createDeliveryLocationMapping(
            @WebParam(name = "deliveryMappings")
            List<DeliveryLocationMappingRRO> mappings);


     /**
      * Edits a delivery location mapping.
      * 
      * @param mappings The mappings to be modified.
      * @return - Fault code that indicates that a
      */
    @POST
    @WebMethod
    @Path("/modify")
    public String modifyDeliveryLocationMapping(
            @WebParam(name = "deliveryMappings")
            List<DeliveryLocationMappingRRO> mappings);

    
    /**
     * Deletes the mappings specified in the list
     * 
     * @param mappings
     *            The mappings list to be deleted. Only mapping values needs to
     *            be populated.
     * @return 0 if deleted successfully, or a fault code.
     */
    @POST
    @WebMethod
    @Path("/delete")
    public String deleteDeliveryLocationMapping(
            @WebParam(name = "deliveryMappings") List<DeliveryLocationMappingRRO> mappings);

    /**
     * Returns the number of Delivery Location Mappings.
     * 
     * @param siteName
     *            - the name of the site in which to count
     *            <code>DeliveryLocationMapping</code>
     * @return the number of DeliveryLocationMappings for a site
     */
    @GET
    @WebMethod
    @Path("/count")
    public long countDeliveryLocationMappings(
            @QueryParam(value = "SiteName") @WebParam(name = "SiteName") String siteName);

    /**
     * Lists all the Delivery Location Mappings for a site.
     * 
     * @param siteName
     *            - the name of the site in which to count
     *            <code>DeliveryLocationMapping</code>
     * @return the number of DeliveryLocationMappings for a site
     */
    @GET
    @WebMethod
    @Path("/list")
    public List<DeliveryLocationMappingRRO> listDeliveryLocationMappings(
            @QueryParam(value = "SiteName") @WebParam(name = "SiteName") String siteName);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 