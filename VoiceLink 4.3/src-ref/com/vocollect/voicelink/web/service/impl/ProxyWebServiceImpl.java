/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service.impl;

import com.vocollect.voicelink.task.service.impl.StreamlinedLocalTaskCommandServiceImpl;
import com.vocollect.voicelink.web.service.ProxyWebService;

import javax.jws.WebService;

/**
 * The implementation of the ProxyWebService.
 *
 * @author daich, sthomas
 */
@WebService(serviceName = "ProxyWebService",
    endpointInterface = "com.vocollect.voicelink.web.service.ProxyWebService")
public class ProxyWebServiceImpl implements ProxyWebService {

    // [start] access to business layer logic
    // business logic service variable
    private StreamlinedLocalTaskCommandServiceImpl taskSrv = null;

    /**
     * Retrieves the business layer logic object for use.
     *
     * @return The StreamLinedLocalTaskCommandService
     */
    public StreamlinedLocalTaskCommandServiceImpl getTaskSrv() {
        return this.taskSrv;
    }

    /**
     * Sets the business layer logic object for later use.
     *
     * @param taskSrv - The StreamlinedLocalTaskCommandServiceImpl to use.
     */
    public void setTaskSrv(StreamlinedLocalTaskCommandServiceImpl taskSrv) {
        this.taskSrv = taskSrv;
    }
    // [end]

    // [start] options
    private boolean doReplaceTerminatingChars = true;

    /**
     * Sets the option to restore terminating characters encoded on the server to prevent their loss
     * during webservice communication.
     *
     * @param value - If true, the restoration will be performed; if false, it will not.
     */
    public void setDoReplaceTerminatingChars(boolean value) {
        this.doReplaceTerminatingChars = value;
    }

    /**
     * Gets the option to restore terminating characters encoded on the server.
     *
     * @return true if restoration should occur; false if not.
     */
    private boolean getDoReplaceTerminatingChars() {
        return this.doReplaceTerminatingChars;
    }

    // [end]

    // [start] supporting functions
    /**
     * Replace whitespace characters used to mark end-of-record and end-of-transmission
     * which would otherwise be lost during XFire/webservice communication.
     *
     * The default implementation replaces all instances of "\r" with "@r@",
     * and "\n" with "@n@".
     *
     * @param exeResult - The String result of executing the webservice method.
     * @return - The value with replacements made.
     */
    protected String replaceTerminatingChars(String exeResult) {
        return exeResult.replaceAll("\r", "@r@").replaceAll("\n", "@n@");
    }

    // [end]

    // [start] executeCommand
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.service.ProxyWebService#executeCommand(java.lang.String, java.lang.String[])
     */
    public String executeCommand(String commandName, String[] values) throws Exception {

        // get the result of the command
        String exeResult = this.getTaskSrv().executeCommand(commandName, values);

        if (null != exeResult && this.getDoReplaceTerminatingChars()) {
            // XFire trims whitespace, so convert for later restoration
            exeResult = this.replaceTerminatingChars(exeResult);
        }

        return exeResult;
    }
    // [end]

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 