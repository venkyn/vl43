/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.service.LotManager;
import com.vocollect.voicelink.web.service.LotWebService;

import java.util.Date;

/**
 * Implmentation of Lot Web Services.
 *
 * @author mkoenig
 */
public abstract class LotWebServiceImplRoot  extends SiteAwareWebServiceImplRoot
implements LotWebService {

    private static final Logger log = new Logger(LotWebServiceImplRoot.class);

    private LotManager lotManager;

    /**
     * Getter for the lotManager property.
     * @return LotManager value of the property
     */
    public LotManager getLotManager() {
        return lotManager;
    }


    /**
     * Setter for the lotManager property.
     * @param lotManager the new lotManager value
     */
    public void setLotManager(LotManager lotManager) {
        this.lotManager = lotManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.service.LotWebServiceRoot#createUpdateLot(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date)
     */
    public long createUpdateLot(String siteName,
                                String lotNumber,
                                String itemNumber,
                                String locationId,
                                String speakableLotNumber,
                                Date expirationDate) {
        long returnValue = 0L;

        //Set site context based on site name.
        try {
            //Set site context based on site name.
            setCurrentSite(siteName);

            getLotManager().executeCreateUpdateLot(lotNumber,
                                            itemNumber,
                                            locationId,
                                            speakableLotNumber,
                                            expirationDate);
        } catch (DataAccessException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (BusinessRuleException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (Throwable t) {
            t.printStackTrace();
            returnValue = -1; //unknown error
            log.error(t.getMessage(), null, t);
        }

        return returnValue;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.service.LotWebServiceRoot#deleteLot(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public long deleteLot(String siteName,
                          String lotNumber,
                          String itemNumber,
                          String locationId) {
        long returnValue = 0L;

        try {
            //Set site context based on site name.
            setCurrentSite(siteName);

            getLotManager().executeDeleteLot(lotNumber, itemNumber, locationId);

        } catch (DataAccessException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (BusinessRuleException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (Throwable t) {
            returnValue = -1; //unknown error
            log.error(t.getMessage(), null, t);
        }

        return returnValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.service.LotWebServiceRoot#countLots(java.lang.String)
     */
    public long countLots(String siteName) {
        long returnValue = 0L;
        //Set site context based on site name.
        try {
            setCurrentSite(siteName);
            Long count = getLotManager().countAll();
            returnValue = count.longValue();
        } catch (Throwable t) {
            returnValue = -1; //unknown error
            log.error(t.getMessage(), null, t);
        }
        return returnValue;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 