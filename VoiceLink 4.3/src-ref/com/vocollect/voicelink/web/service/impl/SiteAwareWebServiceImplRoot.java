/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.CoreErrorCode;

/**
 *
 *
 * @author mkoenig
 */
public abstract class SiteAwareWebServiceImplRoot {

    private SiteContext           siteContext;


    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }


    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * Find a site by name.
     *
     * @param siteName - site name
     * @return site or null if not found
     * @throws DataAccessException - any database exception.
     */
    protected Site findSite(String siteName) throws DataAccessException {
        Site currentSite = null;

        for (Site s : getSiteContext().getAllSites()) {

            if (s.getName().equals(siteName)) {
                currentSite = s;
            }
        }

        return currentSite;
    }
    /**
     * Set the current site based on a site name.
     *
     * @param siteName - site name to set
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Buinsess Rule Exception
     */
    protected void setCurrentSite(String siteName)
    throws DataAccessException, BusinessRuleException {
        Site currentSite = null;

        currentSite = getSiteContext().getSiteByName(siteName);
        if (null == currentSite) {
            getSiteContext().refreshSites();
        }
        if (null == currentSite) {
            throw new BusinessRuleException(CoreErrorCode.SITE_NOT_FOUND);
        }

        getSiteContext().setCurrentSite(currentSite);

        SiteContextHolder.setSiteContext(getSiteContext());

        getSiteContext().setFilterBySite(true);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 