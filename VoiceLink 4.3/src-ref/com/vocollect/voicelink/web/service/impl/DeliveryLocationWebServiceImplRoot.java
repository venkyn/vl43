/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingManager;
import com.vocollect.voicelink.web.service.DeliveryLocationWebService;
import com.vocollect.voicelink.web.service.rro.DeliveryLocationMappingRRO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementation of DeliveryLocation web service.
 * 
 * @author mlashinsky
 */
public abstract class DeliveryLocationWebServiceImplRoot extends
        SiteAwareWebServiceImplRoot implements DeliveryLocationWebService {

    private static final Logger log = new Logger(
            DeliveryLocationWebServiceImplRoot.class);

    private DeliveryLocationMappingManager deliveryLocationMappingManager;

    /**
     * @param mappings
     *            the mappings
     * @return returns error code
     */
    public String createDeliveryLocationMapping(
            List<DeliveryLocationMappingRRO> mappings) {
        // Start the transaction
        log.debug("Starting DeliveryLocationMapping Create(s)");
        // Set the return value
        String returnValue = "success";

        DeliveryLocationMappingRRO currentMapping = null;

        // Try to create the DeliveryLocationMapping
        try {
            for (DeliveryLocationMappingRRO mapping : mappings) {
                currentMapping = mapping;

                // Mapping type not defined
                if (DeliveryLocationMappingType.CustomerNumber != mapping
                        .getType()
                        && DeliveryLocationMappingType.Route != mapping
                                .getType()) {

                    throw new BusinessRuleException(
                            CoreErrorCode.MAPPING_TYPE_NOT_DEFINED);

                }

                // Delivery location and mapping value cannot be empty
                if (mapping.getDeliveryLocation() == null
                        || mapping.getDeliveryLocation().length() == 0
                        || mapping.getMappingValue() == null
                        || mapping.getMappingValue().length() == 0) {

                    throw new BusinessRuleException(
                            CoreErrorCode.DELIVERY_MAPPING_VALUES_NOT_FOUND);
                }

                // Delivery location and mapping value max length check
                final int maxMappingValue = 50;
                final int maxDeliveryLocationLen = 9;
                if (mapping.getDeliveryLocation().length() > maxDeliveryLocationLen
                        || mapping.getMappingValue().length() > maxMappingValue) {

                    throw new BusinessRuleException(
                            CoreErrorCode.PARAMETER_LENGTH_NOT_ACCEPTABLE);

                }

                // Set the current site context
                setCurrentSite(mapping.getSite());

                // Create the DeliveryLocationMapping
                DeliveryLocationMapping deliveryMapping = new DeliveryLocationMapping();

                // Iterate through the HashMap Saving each value
                deliveryMapping.setMappingType(mapping.getType());
                deliveryMapping.setCreatedDate(new Date());
                deliveryMapping.setDeliveryLocation(mapping
                        .getDeliveryLocation());
                deliveryMapping.setMappingValue(mapping.getMappingValue());

                // Save the DeliveryLocationMapping
                getDeliveryLocationMappingManager().save(deliveryMapping);

            }

        } catch (DataAccessException e) {

            // Any data access exception
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
            e.printStackTrace();

        } catch (BusinessRuleException e) {

            // Any business rule exception
            // TODO make sure we validate the information in the business logic
            // layer
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
            e.printStackTrace();

        } catch (Throwable t) {

            // something unknown happened return -1 value
            t.printStackTrace();
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:-1";

            log.error(t.getMessage(), null, t);

        }

        // Log the end of the transaction
        log.debug("Finished DeliveryLocationMapping Create(s)");
        return returnValue;
    }

    /**
     * Edits a delivery location mapping.
     * 
     * @param mappings
     *            The mappings to be modified.
     * @return - Fault code that indicates that a
     */
    public String modifyDeliveryLocationMapping(
            List<DeliveryLocationMappingRRO> mappings) {
        log.debug("Starting DeliveryLocationMapping Update(s)");
        String returnValue = "success";

        DeliveryLocationMappingRRO currentMapping = null;

        // Try to create the DeliveryLocationMapping
        try {
            for (DeliveryLocationMappingRRO mapping : mappings) {
                currentMapping = mapping;

                // Mapping type not defined
                if (DeliveryLocationMappingType.CustomerNumber != mapping
                        .getType()
                        && DeliveryLocationMappingType.Route != mapping
                                .getType()) {

                    throw new BusinessRuleException(
                            CoreErrorCode.MAPPING_TYPE_NOT_DEFINED);

                }

                // Delivery location and mapping value cannot be empty
                if (mapping.getDeliveryLocation() == null
                        || mapping.getDeliveryLocation().length() == 0
                        || mapping.getMappingValue() == null
                        || mapping.getMappingValue().length() == 0) {

                    throw new BusinessRuleException(
                            CoreErrorCode.DELIVERY_MAPPING_VALUES_NOT_FOUND);
                }

                // Delivery location and mapping value max length check
                final int maxMappingValue = 50;
                final int maxDeliveryLocationLen = 9;
                if (mapping.getDeliveryLocation().length() > maxDeliveryLocationLen
                        || mapping.getMappingValue().length() > maxMappingValue) {

                    throw new BusinessRuleException(
                            CoreErrorCode.PARAMETER_LENGTH_NOT_ACCEPTABLE);

                }

                // Set the current site context
                setCurrentSite(mapping.getSite());

                // Save the DeliveryLocationMapping
                getDeliveryLocationMappingManager()
                        .executeModifyDeliveryLocation(
                                mapping.getMappingValue(),
                                mapping.getDeliveryLocation(),
                                mapping.getType());

            }

        } catch (DataAccessException e) {
            // Any data access exception
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
            e.printStackTrace();

        } catch (BusinessRuleException e) {
            // Any business rule exception
            // TODO make sure we validate the information in the business logic
            // layer
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
            e.printStackTrace();

        } catch (Throwable t) {

            // something unknown happened return -1 value
            t.printStackTrace();
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:-1";

            log.error(t.getMessage(), null, t);

        }
        log.debug("Finished DeliveryLocationMapping Update(s)");
        return returnValue;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.web.service.DeliveryLocationWebServiceRoot#deleteDeliveryLocationMapping(java.lang.String,
     *      java.util.List)
     */
    public String deleteDeliveryLocationMapping(
            List<DeliveryLocationMappingRRO> mappings) {
        log.debug("Starting DeliveryLocationMapping Delete(s)");
        String returnValue = "success";

        DeliveryLocationMappingRRO currentMapping = null;

        try {
            for (DeliveryLocationMappingRRO mapping : mappings) {
                currentMapping = mapping;

                // Mapping type not defined
                if (DeliveryLocationMappingType.CustomerNumber != mapping
                        .getType()
                        && DeliveryLocationMappingType.Route != mapping
                                .getType()) {

                    throw new BusinessRuleException(
                            CoreErrorCode.MAPPING_TYPE_NOT_DEFINED);

                }

                // Delivery location and mapping value cannot be empty
                if (mapping.getMappingValue() == null
                        || mapping.getMappingValue().length() == 0) {

                    throw new BusinessRuleException(
                            CoreErrorCode.DELIVERY_MAPPING_VALUES_NOT_FOUND);
                }

                // Delivery location and mapping value max length check
                final int maxMappingValue = 9;
                final int maxDeliveryLocationLen = 50;
                if (mapping.getDeliveryLocation().length() > maxDeliveryLocationLen
                        || mapping.getMappingValue().length() > maxMappingValue) {

                    throw new BusinessRuleException(
                            CoreErrorCode.PARAMETER_LENGTH_NOT_ACCEPTABLE);

                }

                // Set site context based on site name.
                setCurrentSite(mapping.getSite());

                getDeliveryLocationMappingManager()
                        .executeDeleteDeliveryLocation(
                                mapping.getMappingValue(), mapping.getType());
            }
        } catch (DataAccessException e) {
            // Any data access exception
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (BusinessRuleException e) {
            // Any business rule exception
            // TODO make sure we validate the information in the business logic
            // layer
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:"
                    + e.getErrorCode().getErrorCode();

            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (Throwable t) {
            // something unknown happened return -1 value
            returnValue = "DeliveryLocation:"
                    + currentMapping.getDeliveryLocation() + ",MappingValue:"
                    + currentMapping.getMappingValue() + ",ErrorCode:-1";

            log.error(t.getMessage(), null, t);
        }

        log.debug("Finished DeliveryLocationMapping Delete(s)");
        return returnValue;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.web.service.DeliveryLocationWebServiceRoot#countDeliveryLocationMappings(java.lang.String)
     */
    public long countDeliveryLocationMappings(String siteName) {
        log.debug("Starting DeliveryLocationMapping count");
        long returnValue = 0L;

        try {
            // Set site context based on site name.
            setCurrentSite(siteName);
            returnValue = getDeliveryLocationMappingManager().getAllMappings()
                    .size();
        } catch (DataAccessException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (BusinessRuleException e) {
            returnValue = e.getErrorCode().getErrorCode();
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (Throwable t) {
            // something unknown happened return -1 value
            returnValue = -1;
            log.error(t.getMessage(), null, t);
        }

        log.debug("Finished DeliveryLocationMapping count");
        return returnValue;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.web.service.DeliveryLocationWebServiceRoot#listDeliveryLocationMappings(java.lang.String)
     */
    public List<DeliveryLocationMappingRRO> listDeliveryLocationMappings(
            String siteName) {
        log.debug("Starting DeliveryLocationMapping list");

        List<DeliveryLocationMappingRRO> returnMappings = new ArrayList<DeliveryLocationMappingRRO>();

        try {
            // Set site context based on site name.
            setCurrentSite(siteName);

            List<DeliveryLocationMapping> mappings = getDeliveryLocationMappingManager()
                    .getAllMappings();

            for (DeliveryLocationMapping mapping : mappings) {
                DeliveryLocationMappingRRO returnMapping = new DeliveryLocationMappingRRO();
                returnMapping
                        .setDeliveryLocation(mapping.getDeliveryLocation());
                returnMapping.setMappingValue(mapping.getMappingValue());
                returnMapping.setSite(siteName);
                returnMapping.setType(mapping.getMappingType());

                returnMappings.add(returnMapping);
            }

        } catch (DataAccessException e) {
            returnMappings = null;
            log.error(e.getMessage(), e.getErrorCode(), e);
        } catch (BusinessRuleException e) {
            returnMappings = null;
            log.error(e.getMessage(), e.getErrorCode(), e);
        }

        log.debug("Finished DeliveryLocationMapping list");
        return returnMappings;
    }

    /**
     * Getter for the deliveryLocationMappingManager property.
     * 
     * @return DeliveryLocationMappingManager value of the property
     */
    public DeliveryLocationMappingManager getDeliveryLocationMappingManager() {
        return deliveryLocationMappingManager;
    }

    /**
     * Setter for the deliveryLocationMappingManager property.
     * 
     * @param deliveryLocationMappingManager
     *            the new deliveryLocationMappingManager value
     */
    public void setDeliveryLocationMappingManager(
            DeliveryLocationMappingManager deliveryLocationMappingManager) {
        this.deliveryLocationMappingManager = deliveryLocationMappingManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 