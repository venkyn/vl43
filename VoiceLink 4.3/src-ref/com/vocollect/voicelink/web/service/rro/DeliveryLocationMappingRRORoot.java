/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.web.service.rro;

import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Delivery Location Request Response Object (RRO)
 * @author khazra
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "deliveryMappingRoot")
public class DeliveryLocationMappingRRORoot {
    @XmlElement(required = true)
    String site;
    @XmlElement(required = false)
    DeliveryLocationMappingType type;
    @XmlElement(required = true)
    String deliveryLocation;
    @XmlElement(required = true)
    String mappingValue;

    /**
     * @return the site
     */
    public String getSite() {
        return site;
    }

    /**
     * @param site
     *            the site to set
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @return the type
     */
    public DeliveryLocationMappingType getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public void setType(DeliveryLocationMappingType type) {
        this.type = type;
    }

    /**
     * @return the deliveryLocation
     */
    public String getDeliveryLocation() {
        return deliveryLocation;
    }

    /**
     * @param deliveryLocation
     *            the deliveryLocation to set
     */
    public void setDeliveryLocation(String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /**
     * @return the mappingValue
     */
    public String getMappingValue() {
        return mappingValue;
    }

    /**
     * @param mappingValue
     *            the mappingValue to set
     */
    public void setMappingValue(String mappingValue) {
        this.mappingValue = mappingValue;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 