/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service;

import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * A web service for Adding/Updating/Deleting Lots.
 *
 * @author mkoenig
 */
@WebService
@Path("/LotRESTService")
@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
public abstract interface LotWebServiceRoot {

    /**
     * Method to Add or Update a lot number. If the lot already exists, then
     * the speakable lot number and expiration date will be updated.
     * If the lot number does not exist then it will be created.
     * A lot is determined by the Site, Lot number, Item Number, and Location Number.
     *
     * @param siteName - site the lot belongs to
     * @param lotNumber - lot number to add
     * @param itemNumber - item number lot belongs to.
     * @param locationId - (Optional) Location where lot is stored
     * @param speakableLotNumber - A value representing the lot number and operator can speak
     * @param expirationDate - (Optional) expiration date of lot number
     * @return - Internal lot ID for lot that was created or updated.
     */
    @PUT
    @WebMethod
    @Path("/create")
    public long createUpdateLot(
        @QueryParam(value = "siteName")            @WebParam(name = "siteName")           String siteName,
        @QueryParam(value = "lotNumber")           @WebParam(name = "lotNumber")          String lotNumber,
        @QueryParam(value = "itemNumber")          @WebParam(name = "itemNumber")         String itemNumber,
        @QueryParam(value = "locationId")          @WebParam(name = "locationId")         String locationId,
        @QueryParam(value = "speakableLotNumber")  @WebParam(name = "speakableLotNumber") String speakableLotNumber,
        @QueryParam(value = "expirationDate")      @WebParam(name = "expirationDate")     Date expirationDate);

    /**
     * Delete an existing Lot with the specified item and location.
     * @param siteName - site to delete from.
     * @param lotNumber - lot number to delete
     * @param itemNumber - item lot belongs to
     * @param locationId - (Optional) location lot is stored at.
     * @return - Internal lot ID for lot that was deleted.
     */
    @DELETE
    @WebMethod
    @Path("/delete")
    public long deleteLot(@QueryParam(value = "siteName")   @WebParam(name = "siteName")   String siteName,
                          @QueryParam(value = "lotNumber")  @WebParam(name = "lotNumber")  String lotNumber,
                          @QueryParam(value = "itemNumber") @WebParam(name = "itemNumber") String itemNumber,
                          @QueryParam(value = "itemNumber") @WebParam(name = "locationId") String locationId);

    /**
     * Returns a total count of lot records for a site. used for unit testing
     *
     * @param siteName - site to count for.
     * @return - total number of lot records in a site
     */
    @GET
    @WebMethod
    @Path("/count")
    public long countLots(@QueryParam(value = "siteName") @WebParam(name = "siteName") String siteName);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 