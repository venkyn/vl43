/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.putaway.service.LicenseManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the root action class for putaway LicenseAction.
 * It is used  to populate the putaway license's table component.
 * @author pmukhopadhyay
 * @author brupert
 */
public class LicenseActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2551367923634249735L;

    private LicenseManager licenseManager;

    // The list of columns for the license table.
    private List<Column> licenseColumns;
    //  The ID of the view we need to grab from the DB.
    private static final Long VIEW_ID = -1202L;

    private static final Long SUMMARY_VIEW_ID = -1204L;

    private static final String SITE_FIELD = "site.name";

    private List<Column> summaryColumns;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "license";
    }

    /**
     *
     * @return String value of outcome.
     * @throws DataAccessException - any database exception.
     */
    public String list() throws DataAccessException {
        View licenseView = getUserPreferencesManager().getView(VIEW_ID);
        this.licenseColumns = this.getUserPreferencesManager().getColumns(licenseView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Does a lookup on the user preferences in order to construct the table
     * column setup.
     * @return control flow target <code>SUCCESS</code>
     * @throws DataAccessException on a database problem.
     */
    public String summaryList() throws DataAccessException {
        View licenseView = getUserPreferencesManager().getView(SUMMARY_VIEW_ID);
        this.summaryColumns = this.getUserPreferencesManager().getColumns(licenseView, getCurrentUser());
        return SUCCESS;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getLicenseManager();
    }

    /**
     * Getter for the licenseManager property.
     * @return LicenseManager value of the property.
     */
    public LicenseManager getLicenseManager() {
        return licenseManager;
    }

    /**
     * Setter for the licenseManager property.
     * @param licenseManager the new licenseManager value.
     */
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }

    /**
     * Getter for the licenseColumns.
     * @return list of licenseColumns
     */
    public List<Column> getLicenseColumns() {
        return licenseColumns;
    }

    /**
     * Setter for the  licenseColumns.
     * @param licenseColumns list of new licenseColumns
     */
    public void setLicenseColumns(List<Column> licenseColumns) {
        this.licenseColumns = licenseColumns;
    }

    /**
     * getter for license summary table columns.
     * @return the list of columns that are displayable.
     */
    public List<Column> getSummaryColumns() {
        for (Column column : summaryColumns) {
            if (column.getField().equals(SITE_FIELD)) {
                if (SiteContextHolder.getSiteContext().isInAllSiteMode()) {
                    // We only want to display the site column when in the all
                    // display mode
                    column.setDisplayable(true);
                } else {
                    column.setDisplayable(false);
                }
                break;
            }
        }
        return this.summaryColumns;
    }

    /**
     * Gets the license view id.
     * @return Long view_id.
     */
    public static long getLicenseViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 