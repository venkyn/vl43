/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.putaway.model.PutawayCaptureStartLocation;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.classic.Validatable;


/**
 * This is the Struts action class that handles operations on
 * <code>PutawayRegion</code> objects.
 * @author brupert
 */
public class PutawayRegionActionRoot extends RegionAction
    implements Preparable, Validatable {

    private static final long serialVersionUID = -11673006855868008L;

    private static final Logger log = new Logger(PutawayRegionActionRoot.class);

    // Defaults for the create page.
    private static final Integer DEFAULT_LOC_DIGITS_OPERATOR_SPEAKS = 5;
    private static final Integer DEFAULT_LIC_DIGITS_OPERATOR_SPEAKS = 3;
    private static final Integer DEFAULT_LIC_DIGITS_TASK_SPEAKS = 5;
    private static final Integer DEFAULT_CHECK_DIGITS_OPERATOR_SPEAKS = 2;
    private static final Integer DEFAULT_LICENSES_ALLOWED = 1;
    private static final String DEFAULT_EXCEPTION_LOCATION = "putaway.region.default.exceptionLocation";
    private static final boolean DEFAULT_ALLOW_SKIP_LICENSE = true;
    private static final boolean DEFAULT_VERIFY_LICENSE = true;
    private static final PutawayCaptureStartLocation
        DEFAULT_CAPTURE_START_LOCATION = PutawayCaptureStartLocation.toEnum(1);

    // Number of options for drop down menus
    private static final Integer CAPTURE_START_LOCATION_OPTIONS = 4;
    private static final Integer DIGITS_OPERATOR_SPEAKS_OPTIONS = 51;
    private static final Integer CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS = 6;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1201;

    // The region manager service.
    private PutawayRegionManager putawayRegionManager;

    // To indicate if the action is a view or save
    private String actionValue;

    private PutawayRegion putawayRegion;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "putaway.region";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.putawayRegionManager;
    }

    /**
     * Create or update the operator specified by the <code>operator</code>
     * member of this class.
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        boolean isNew = putawayRegion.isNew();
        //preProcessSave();
        if (log.isDebugEnabled()) {
            log.debug("Saving region");
        }
        try {
            this.putawayRegion.setType(RegionType.PutAway);
            putawayRegionManager.save(this.putawayRegion);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                if (fve.getField() == "region.name") {
                    fve.setField("putawayRegion.name");
                } else {
                    fve.setField("putawayRegion.number");
                }
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (EntityNotFoundException e) {
            log.warn("Region Number " + this.putawayRegion.getNumber()
                + " not found, possibly a concurrency problem");

            addSessionActionMessage(new UserMessage(
                "putaway.region.edit.error.regionNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified region "
                + this.putawayRegion.getNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Region"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                PutawayRegion modifiedRegion = putawayRegionManager.get(
                    this.getRegionId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.putawayRegion.setVersion(modifiedRegion.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedRegion);
            } catch (EntityNotFoundException ex) {
                addSessionActionErrorMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization("entity.Region"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage(
            "putaway.region." + (isNew ? "create" : "edit") + ".message.success",
            makeContextURL("/putaway/region/view.action?regionId="
                + this.putawayRegion.getId()), this.putawayRegion.getName()));

        return SUCCESS;
    }

    /**
     * Deletes the currently viewed region.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

       PutawayRegion regionToDelete = null;

        try {
            regionToDelete = putawayRegionManager.get(this.getRegionId());
            putawayRegionManager.delete(this.getRegionId());

            addSessionActionMessage(new UserMessage(
                "putaway.region.delete.message.success", regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>PutawayRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission.
     * <p>
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (getRegionId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.putawayRegion = (PutawayRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.putawayRegion = this.putawayRegionManager.get(getRegionId());
                saveEntityInSession(this.putawayRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Region version is: " + this.putawayRegion.getVersion());
            }
        } else if (this.putawayRegion == null) {
            // set default values for create screen.
            this.putawayRegion = getDefaultRegion();
        }
        // To reset the checkbox values, only before saving the model.
        if (this.actionValue != null) {
            this.resetBools();
        }
    }

    /**
     * Retrieves the options for the capture start location property of a
     * putaway region.
     * @return Translated array of options.
     */
    public Map<Integer, String> getCaptureStartLocationOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(CAPTURE_START_LOCATION_OPTIONS);
        for (int i = 0; i < CAPTURE_START_LOCATION_OPTIONS; i++) {
            options.put(i, getText(PutawayCaptureStartLocation.toEnum(i).getResourceKey()));
        }
        return options;
    }

    /**
     * Retrieves options for the license digits operator speaks dropdown.
     * @return Translated array of options.
     */
    public Map<Integer, String> getLicDigitsOperatorSpeaksOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(DIGITS_OPERATOR_SPEAKS_OPTIONS);
        options.put(0, getText("putaway.region.option.variable"));
        for (int i = 1; i < DIGITS_OPERATOR_SPEAKS_OPTIONS; i++) {
            options.put(i, String.valueOf(i));
        }
        return options;
    }

    /**
     * Retrieves options for the location digits operator speaks dropdown.
     * @return Translated array of options.
     */
    public Map<Integer, String> getLocDigitsOperatorSpeaksOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(DIGITS_OPERATOR_SPEAKS_OPTIONS);
        options.put(0, getText("putaway.region.option.all"));
        for (int i = 1; i < DIGITS_OPERATOR_SPEAKS_OPTIONS; i++) {
            options.put(i, String.valueOf(i));
        }
        return options;
    }

    /**
     * Retrieves options for the check digits operator speaks dropdown.
     * @return Translated array of options.
     */
    public Map<Integer, String> getCheckDigitsOperatorSpeaksOptions() {
        Map<Integer, String> options = new LinkedHashMap<Integer, String>(CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS);
        options.put(0, getText("putaway.region.option.all"));
        for (int i = 1; i < CHECK_DIGITS_OPERATOR_SPEAKS_OPTIONS; i++) {
            options.put(i, String.valueOf(i));
        }
        return options;
    }

    /**
     * Gets the integer value of the capture start location enumerated property.
     * @return The int value of the enum.
     */
    public Integer getCaptureStartLocation() {
        return this.putawayRegion.getCaptureStartLocation().toValue();
    }

    /**
     * Sets the capture start location enumerated property via the integer value.
     * @param value The int value of the enum.
     */
    public void setCaptureStartLocation(Integer value) {
        this.putawayRegion.setCaptureStartLocation(PutawayCaptureStartLocation.toEnum(value));
    }

    /**
     * Function for validating if a region is editable or not.
     *
     * @return String  success string
     * @throws Exception - any exception
     */
    public String checkIfEditable() throws Exception {
        try {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }
                this.putawayRegion = (PutawayRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.putawayRegion = this.putawayRegionManager.get(getIds()[0]);
                saveEntityInSession(this.putawayRegion);
            }
            putawayRegionManager.executeValidateEditRegion(this.putawayRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        setJsonMessage("edit!input.action?regionId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Creates a new <code>PutawayRegion</code> with default values populated.
     * @return A default <code>PutawayRegion</code> object.
     */
    public PutawayRegion getDefaultRegion() {
        PutawayRegion region = new PutawayRegion();
        region.setType(RegionType.PutAway);
        region.setLicDigitsOperatorSpeaks(DEFAULT_LIC_DIGITS_OPERATOR_SPEAKS);
        region.setLocDigitsOperatorSpeaks(DEFAULT_LOC_DIGITS_OPERATOR_SPEAKS);
        region.setLicDigitsTaskSpeaks(DEFAULT_LIC_DIGITS_TASK_SPEAKS);
        region.setCheckDigitsOperatorSpeaks(DEFAULT_CHECK_DIGITS_OPERATOR_SPEAKS);
        region.setCaptureStartLocation(DEFAULT_CAPTURE_START_LOCATION);
        region.setAllowSkipLicense(DEFAULT_ALLOW_SKIP_LICENSE);
        region.setVerifyLicense(DEFAULT_VERIFY_LICENSE);
        region.setNumberOfLicensesAllowed(DEFAULT_LICENSES_ALLOWED);
        region.setExceptionLocation(getText(DEFAULT_EXCEPTION_LOCATION));
        return region;
    }

    /**
     * Reset all boolean values.
     */
    private void resetBools() {
        this.putawayRegion.setAllowSkipLicense(false);
        this.putawayRegion.setAllowCancelLicense(false);
        this.putawayRegion.setAllowOverrideLocation(false);
        this.putawayRegion.setVerifySpokenLicOrLoc(false);
        this.putawayRegion.setAllowOverrideQuantity(false);
        this.putawayRegion.setAllowPartialPut(false);
        this.putawayRegion.setCapturePickupQuantity(false);
        this.putawayRegion.setCapturePutQuantity(false);
        this.putawayRegion.setVerifyLicense(false);
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getRegionViewId() {
        return VIEW_ID;
    }

    /**
     * Gets the region manager.
     * @return The region manager.
     */
    public PutawayRegionManager getPutawayRegionManager() {
        return this.putawayRegionManager;
    }

    /**
     * Sets the region manager.
     * @param putawayRegionManager The new region manager.
     */
    public void setPutawayRegionManager(PutawayRegionManager putawayRegionManager) {
        this.putawayRegionManager = putawayRegionManager;
    }

    /**
     * Get the region.
     * @return The putaway region.
     */
    public PutawayRegion getPutawayRegion() {
        return putawayRegion;
    }

    /**
     * Set the region.
     * @param putawayRegion The new putaway region.
     */
    public void setPutawayRegion(PutawayRegion putawayRegion) {
        this.putawayRegion = putawayRegion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getViewIds() {
        ArrayList<Long> viewIds = new ArrayList<Long>(1);
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * @return the actionValue property.
     */
    public String getActionValue() {
        return actionValue;
    }


    /**
     * Setter for the actionValue property.
     * @param actionValue the new value.
     */
    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
     */
    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
     */
    @Override
    protected Region getRegion() {
        return getPutawayRegion();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 