/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.putaway.service.LicenseDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the root action class to populate the license detail view
 * of putaway.
 * @author pmukhopadhyay
 */
public class LicenseDetailActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 1512279948596197710L;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1203;

    // The Operator management service.
    private LicenseDetailManager licenseDetailManager;

    // The list of columns for the licenseDetail table.
    private List<Column> licenseDetailColumns;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "licenseDetail";
    }

    /**
     * Action method for the table view page.
     * @return String value of outcome.
     * @throws DataAccessException - any database exception
     */
    public String list() throws DataAccessException {
        View licenseDetailView = getUserPreferencesManager().getView(VIEW_ID);
        this.licenseDetailColumns = this.getUserPreferencesManager().getColumns(licenseDetailView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.licenseDetailManager;
    }

    /**
     * Getter for the licenseDetailManager property.
     * @return LicenseDetailManager value of the property.
     */
    public LicenseDetailManager getLicenseDetailManager() {
        return licenseDetailManager;
    }

    /**
     * Setter for the licenseDetailManager property.
     * @param licenseDetailManager the new licenseDetailManager value.
     */
    public void setLicenseDetailManager(LicenseDetailManager licenseDetailManager) {
        this.licenseDetailManager = licenseDetailManager;
    }

    /**
     * @return list of viewIds.
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the licenseDetailColumns property.
     * @return list of licenseDetailColumns.
     */
    public List<Column> getLicenseDetailColumns() {
        return licenseDetailColumns;
    }

    /**
     * Setter for the licenseDetailColumns properties.
     * @param licenseDetailColumns the new list of properties
     */
    public void setLicenseDetailColumns(List<Column> licenseDetailColumns) {
        this.licenseDetailColumns = licenseDetailColumns;
    }

    /**
     * Gets the licenseDetailViewId.
     * @return Long viewId
     */
    public static long getLicenseDetailViewId() {
        return VIEW_ID;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 