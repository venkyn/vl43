/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.web.selection.action.VoiceLinkHomeAction;
/**
 * Putaway homepage action class.
 * @author brupert
 */
public class PutawayHomeActionRoot extends VoiceLinkHomeAction {

    private static final long serialVersionUID = 9123319629173176324L;

    private static final long PUTAWAY_HOMEPAGE_ID = -5;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.ConfigurableHomepagesActionRoot#getHomepageId()
     */
    @Override
    public long getHomepageId() {
        return PUTAWAY_HOMEPAGE_ID;
    }

    /**
     * Returns the list of summaries.
     * @return Control flow target name.
     * @throws DataAccessException if unable to retrieve summaries.
     */
    public String getHomeSummaries() throws DataAccessException {

        if (hasGroupAccess(VOICELINK_PUTAWAY_GROUP_NAME)) {
            getAdminHomeSummaries();
            return SUCCESS;
        } else if (hasGroupAccess(VOICELINK_SELECTION_GROUP_NAME)) {
            return VOICELINK_SELECTION_HOME;
        } else if (hasGroupAccess(VOICELINK_REPLENISHMENT_GROUP_NAME)) {
            return VOICELINK_REPLENISHMENT_HOME;
        } else if (hasGroupAccess(VOICELINK_LINELOADING_GROUP_NAME)) {
            return VOICELINK_LINELOADING_HOME;
        } else if (hasGroupAccess(VOICELINK_PUTTOSTORE_GROUP_NAME)) {
            return VOICELINK_PUTTOSTORE_HOME;
        } else if (hasGroupAccess(VOICELINK_LOADING_GROUP_NAME)) {
            return VOICELINK_LOADING_HOME;
        } else if (hasGroupAccess(VOICELINK_CYCLECOUNTING_GROUP_NAME)) {
            return VOICELINK_CYCLECOUNTING_HOME;
        } else {
            return noAccessMessage();
        }
    }

    /**
     * Persist summaries to the database.
     * @return Control flow target name.
     * @throws DataAccessException On database error.
     * @throws BusinessRuleException On failure durring save.
     */
    @Override
    public String addSummaries() throws DataAccessException, BusinessRuleException {
        super.addSummaries();
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 