/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.puttostore.dao.PtsPutDAO;
import com.vocollect.voicelink.puttostore.model.PtsPut;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>Put</code> objects.
 *
 * @author mnichols
 */
public class PtsPutActionRoot extends DataProviderAction implements Preparable {

    //
    private static final long serialVersionUID = -6498360412669771524L;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1044;

    private static final long CUSTOMER_ORDER_VIEW_ID = -1045;

    // The list of columns for the puts table.
    private List<Column> columns;

    // The Put management service.
    private GenericManager<PtsPut, PtsPutDAO> ptsPutManager = null;

    // The Put object, which will either be newly created, or retrieved
    // via the PutId.
    private PtsPut ptsPut;

    // The ID of the route customer.
    private Long putId;

    // The ID of the customer location.
    private String customerLocationID = null;

    // A comma separated String of IDs to query on
    private String licenseID = null;

    /**
     * Getter for the put property.
     * @return User value of the property
     */
    public PtsPut getPtsPut() {
        return this.ptsPut;
    }

    /**
     * Setter for the put property.
     * @param ptsPut the new ptsPut value
     */
    public void setPtsPut(PtsPut ptsPut) {
        this.ptsPut = ptsPut;
    }

    /**
     * Getter for the putId property.
     * @return Long value of the property
     */
    public Long getPutId() {
        return this.putId;
    }

    /**
     * Setter for the putId property.
     * @param putId the new putId value
     */
    public void setPutId(Long putId) {
        this.putId = putId;
    }

    /**
     * Getter for the putManager property.
     * @return UserManager value of the property
     */
    public GenericManager<PtsPut, PtsPutDAO> getPtsPutManager() {
        return this.ptsPutManager;
    }

    /**
     * Setter for the putManager property.
     * @param manager the new putManager value
     */
    public void setPtsPutManager(GenericManager<PtsPut, PtsPutDAO> manager) {
        this.ptsPutManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPutColumns() {
        return this.columns;
    }

    /**
     * Action for the puts view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View putView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            putView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * For getting carton table data on pallets view page.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     *
     */
    public String getLicensePutTableData() throws Exception {
        if (getLicenseID() != null && getLicenseID().length() > 0) {
            super.getTableData(
                "obj.license.id in ( " + this.licenseID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Create or update the put specified by the <code>put</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Put</code> object by retrieving it
     * from the database when a putId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {

    }

    /**
     * Delete the location identified by the <code>locationId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentPut() throws Exception {
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getPtsPutManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "put";
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getPutViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for the assignmentID property.
     * @return String value of the property
     */
    public String getLicenseID() {
        return licenseID;
    }


    /**
     * Setter for the assignmentID property.
     * @param assignmentID the new assignmentID value
     */
    public void setLicenseID(String assignmentID) {
        this.licenseID = assignmentID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getPrintData()
     */
    @Override
    public String getPrintData() throws Exception {
        if (getLicenseID() != null && getLicenseID().length() > 0) {
            super.getPrintData(
                "obj.license.id in ( " + this.licenseID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Getter for the CUSTOMER_ORDER_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCustomerOrderViewId() {
        return CUSTOMER_ORDER_VIEW_ID;
    }

    /**
     * For getting pick details data on customer location view page.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     *
     */
    public String getCustomerOrderData() throws Exception {
        if (getCustomerLocationID() != null && getCustomerLocationID().length() > 0) {
            super.getTableData(
                "obj.customer.id in ( " + this.customerLocationID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * For getting pick details print data on customer location view page.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     *
     */
    public String getCustomerOrderDataForPrint() throws Exception {
        if (getCustomerLocationID() != null && getCustomerLocationID().length() > 0) {
            super.getPrintData(
                "obj.customer.id in ( " + this.customerLocationID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Getter for the customerLocationID property.
     * @return String value of the property
     */
    public String getCustomerLocationID() {
        return customerLocationID;
    }

    /**
     * Setter for the customerLocationID property.
     * @param customerLocationID the new customerLocationID value
     */
    public void setCustomerLocationID(String customerLocationID) {
        this.customerLocationID = customerLocationID;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 