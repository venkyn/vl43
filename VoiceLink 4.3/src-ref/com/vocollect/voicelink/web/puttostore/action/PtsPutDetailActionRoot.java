/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

//import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.puttostore.service.PtsPutDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on <code>Container</code>
 * objects.
 *
 * @author svoruganti
 */
public class PtsPutDetailActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

    //the license id from license page  to look up for put details corresponding to this id
    private Long licenseId = null;

    //the put id from license page  to look up for put details corresponding to this id
    private Long putId = null;

    // The ID of the view we need to grab from the DB.
    private static final long PUT_DETAIL_VIEW_ID = -1114;

    // The list of columns for the put table.
    private List<Column> putDetailColumns;

    // The put Detail management service.
    private PtsPutDetailManager ptsPutDetailManager = null;

    /**
     *
     * @return Long putId
     */
    public Long getPutId() {
        return putId;
    }
    /**
     *
     * @param putId new put id
     */
    public void setPutId(Long putId) {
        this.putId = putId;
    }
    /**
     *
     * @return Long license Id
     */
    public Long getLicenseId() {
        return licenseId;
    }

    /**
     *
     * @param licenseId new license Id
     */
    public void setLicenseId(Long licenseId) {
        this.licenseId = licenseId;
    }

    /**
     * Getter for the putDetailManager property.
     * @return PutDetailManager value of the property
     */
    public PtsPutDetailManager getPtsPutDetailManager() {
        return this.ptsPutDetailManager;
    }

    /**
     * Setter for the putDetailManager property.
     * @param manager the new putDetailManager value
     */
    public void setPtsPutDetailManager(PtsPutDetailManager manager) {
        this.ptsPutDetailManager = manager;
    }

    /**
     * Getter for the putDetailColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPutDetailColumns() {
        return this.putDetailColumns;
    }

    /**
     * Action for the containers view page. Initializes the containers table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View putDetailView = getUserPreferencesManager().getView(PUT_DETAIL_VIEW_ID);
        this.putDetailColumns = this.getUserPreferencesManager().getColumns(putDetailView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Put Detail</code> object by retrieving it
     * from the database when a containerId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
     // this is required method but not being used

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPtsPutDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getLicenseId() != -1) {
            super.getTableData("obj.put.license.id = " + getLicenseId()
                + " and " + getCurrentSiteTagId()
                + " in elements(obj.put.tags)");
        } else if (getPutId() !=  -1) {
            super.getTableData("obj.put.id = " + getPutId() + " and "
                + getCurrentSiteTagId() + " in elements(obj.put.tags)");
        } else {
            super.getTableData(getCurrentSiteTagId()
                + " in elements(obj.put.tags)");
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "putDetail";
    }

    /**
     * Getter for the PUT_DETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPutDetailViewId() {
        return PUT_DETAIL_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(PUT_DETAIL_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 