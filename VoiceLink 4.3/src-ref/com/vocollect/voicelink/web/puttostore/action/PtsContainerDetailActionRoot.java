/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.puttostore.service.PtsContainerDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;


/**
 * This is the Action class for the PTS Containers screen.
 *
 * @author jtauberg
 */
public class PtsContainerDetailActionRoot extends DataProviderAction
    implements Preparable {

    private static final long serialVersionUID = -4724874111834817349L;

    // The ID of the view we need to grab from the DB (PutDetail View).
    private static final long VIEW_ID = -1049;

    // The list of columns for the put detail table.
    private List<Column> columns;

    // The container put detail management service.
    private PtsContainerDetailManager ptsContainerDetailManager = null;

    // A comma separated String of IDs to query on
    private String containerID = null;


    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getContainerDetailColumns() {
        return this.columns;
    }

    /**
     * Action for the container details view page. Initializes the container details table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View containerDetailsView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(containerDetailsView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPtsContainerDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "ContainerDetail";
    }


    /**
     * Getter for the containerID property.
     * @return the containerID
     */
    public String getContainerID() {
        return containerID;
    }

    /**
     * Setter for the containerID property.
     * @param containerID  the new containerID
     */
    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getContainerID() != null && getContainerID().length() > 0) {
            super.getTableData("obj.container.id in ( " + getContainerID() + " )");
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getPrintData() throws Exception {
        if (getContainerID() != null && getContainerID().length() > 0) {
            super.getPrintData("obj.container.id in ( " + getContainerID() + " )");
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
        return SUCCESS;
    }


    /**
     * This method sets up the <code>User</code> object by retrieving it
     * from the database when a userId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * Getter for the CONTAINERDETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long containerDetailViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for the ptsContainerDetailManager property.
     * @return PtsContainerDetailManager value of the property
     */
    public PtsContainerDetailManager getPtsContainerDetailManager() {
        return ptsContainerDetailManager;
    }


    /**
     * Setter for the ptsContainerDetailManager property.
     * @param ptsContainerDetailManager the new ptsContainerDetailManager value
     */
    public void setPtsContainerDetailManager(PtsContainerDetailManager ptsContainerDetailManager) {
        this.ptsContainerDetailManager = ptsContainerDetailManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 