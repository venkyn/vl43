/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>PtsContainer</code> objects.
 *
 * @author jtauberg
 */
public class PtsContainerActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 8109962482320456598L;

    private static final Logger log = new Logger(PtsContainerActionRoot.class);

    // The ID of the view we need to grab from the DB (PtsContainers View).
    private static final long VIEW_ID = -1047;

    // The ID of the view for the PutDetails we need to grab from the DB.
    private static final long CONTAINER_DETAIL_VIEW_ID = -1049;

    // The list of columns for the ptscontainers table.
    private List<Column> columns;

    // The list of columns for the putDetails table.
    private List<Column> containerDetailColumns;

    // The PtsContainer management service.
    private PtsContainerManager ptsContainerManager = null;

    // The PtsContainer object, which will either be newly created, or retrieved
    // via the PtsContainerId.
    private PtsContainer ptsContainer;

    // The ID of the break type.
    private Long ptsContainerId;

    /**
     * Getter for the ptsContainer property.
     * @return User value of the property
     */
    public PtsContainer getPtsContainer() {
        return this.ptsContainer;
    }

    /**
     * Setter for the ptsContainer property.
     * @param ptsContainer the new ptsContainer value
     */
    public void setPtsContainer(PtsContainer ptsContainer) {
        this.ptsContainer = ptsContainer;
    }

    /**
     * Getter for the ptsContainerId property.
     * @return Long value of the property
     */
    public Long getPtsContainerId() {
        return this.ptsContainerId;
    }

    /**
     * Setter for the ptsContainerId property.
     * @param ptsContainerId the new ptsContainerId value
     */
    public void setPtsContainerId(Long ptsContainerId) {
        this.ptsContainerId = ptsContainerId;
    }


    /**
     * Getter for the ptsContainerManager property.
     * @return PtsContainerManager value of the property
     */
    public PtsContainerManager getPtsContainerManager() {
        return ptsContainerManager;
    }


    /**
     * Setter for the ptsContainerManager property.
     * @param ptsContainerManager the new ptsContainerManager value
     */
    public void setPtsContainerManager(PtsContainerManager ptsContainerManager) {
        this.ptsContainerManager = ptsContainerManager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPtsContainerColumns() {
        return this.columns;
    }


    /**
     * Getter for the putDetailColumns property.
     * @return List Column value of the property
     */
    public List<Column> getContainerDetailColumns() {
        return containerDetailColumns;
    }


    /**
     * Setter for the putDetailColumns property.
     * @param containerDetailColumns the new containerDetailColumns value
     */
    public void setContainerDetailColumns(List<Column> containerDetailColumns) {
        this.containerDetailColumns = containerDetailColumns;
    }

    /**
     * Action for the ptsContainers view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View ptsContainerView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            ptsContainerView, getCurrentUser());

        //Get the PutDetails stuff too...
        View containerDetailView = getUserPreferencesManager().getView(CONTAINER_DETAIL_VIEW_ID);
        this.containerDetailColumns = this.getUserPreferencesManager().getColumns(
            containerDetailView, getCurrentUser());

        //TODO: Get rid of this comment if this works
        //Note that in the Selection containers screen there's a thing that removes
        // the filter here.

        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return  this.getPtsContainerManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "ptsContainer";
    }

    /**
     * Getter for the PTSCONTAINER_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPtsContainerViewId() {
        return VIEW_ID;
    }


    /**
     * Getter for the CONTAINER_DETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getContainerDetailViewId() {
        return CONTAINER_DETAIL_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        viewIds.add(CONTAINER_DETAIL_VIEW_ID);
        return viewIds;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }


    /**
     * Close the containers identified by the <code>List[] ids</code>.
     * @return SUCCESS control flow target if the close was successful.
     * @throws DataAccessException when can't save container status.
     */
    public String closeContainer()
        throws DataAccessException {
            return super.performAction(new CustomActionImpl() {

                public SystemErrorCode getSystemErrorCode() {
                    return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
                }

                public String getActionPrefix() {
                    return "close";
                }

                public boolean execute(DataObject obj)
                    throws VocollectException, DataAccessException,
                    BusinessRuleException {
                    boolean result = false;
                    PtsContainer c = (PtsContainer) obj;
                    // If the container is already closed, we can't close it again.
                    if (PtsContainerStatus.Closed.equals(c.getStatus())) {
                        // Report that we cannot close the container.
                        if (log.isDebugEnabled()) {
                            log.debug("Attempted to close Put To Store container "
                                + c.getContainerNumber() + " that is already closed.");
                        }
                        throw new BusinessRuleException(null,
                            new UserMessage("ptsContainer.close.error.alreadyClosed"));
                    } else {
                        // The container is not already closed so attempt to close it.
                        getPtsContainerManager().closeContainer(c);
                        result = true;
                    }
                    return result;
                }
            });
        }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 