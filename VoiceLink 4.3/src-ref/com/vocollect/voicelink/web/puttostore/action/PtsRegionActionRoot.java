/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.classic.Validatable;


/**
 * This is the Struts action class that handles operations on
 * <code>PtsRegion</code> objects.
 *
 * @author mnichols
 * @author svoruganti
 */
public class PtsRegionActionRoot extends RegionAction
    implements Preparable, Validatable {

    private static final long serialVersionUID = -11673006855868008L;

    private static final Logger log = new Logger(PtsRegionActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1046;

    // The region manager service.
    private PtsRegionManager ptsRegionManager;

    // To indicate if the action is a view or save
    private String actionValue;

    private PtsRegion ptsRegion;

    //default speak all
    private static final String SPEAK_ALL = "region.spokenDigits.all";

    //allow skip
    private Map<Integer, String> allowSkipsMap;

    //validate Container
    private Map<Integer, String> validateContainerMap;

    //skip values
    private static final int SKIP_VALUE_0 = 0;
    private static final int SKIP_VALUE_1 = 1;
    private static final int SKIP_VALUE_2 = 2;
    private static final int SKIP_VALUE_3 = 3;

    //lsit of values for spoken license map
    private Map<Integer, String> spokenLicenseMap;

    //list of values for default length
    private Map<Integer, String> locationCheckDigitLengthMap;

    //list of values for max license length
    private Map<Integer, String> maxLicensesLengthMap;

    //list of values for spoken location length
    private Map<Integer, String> spokenLocationLengthMap;

    //list of values for validate container length
    private Map<Integer, String> validateContainerLengthMap;

    private static final int MAX_SPOKEN_DIGITS = 18;

    private static final int MAX_DIGITS = 10;
    private static final int MAX_DIGITS_LENGTH = 5;


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "puttostore.region";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.ptsRegionManager;
    }

    /**
     * Create or update the operator specified by the <code>operator</code>
     * member of this class.
     * @return the control flow target name.
     * @throws DataAccessException on database error.
     * @throws BusinessRuleException on business rule violation.
     */
    public String save() throws BusinessRuleException, DataAccessException {
        boolean isNew = ptsRegion.isNew();
        //preProcessSave();
        if (log.isDebugEnabled()) {
            log.debug("Saving region");
        }

        try {
            this.ptsRegion.setType(RegionType.PutToStore);
            try {
                ptsRegionManager.executeValidateEditRegion(this.ptsRegion);
            } catch (DataAccessException e) {
               log.warn(e.getUserMessage());
               addSessionActionErrorMessage(e.getUserMessage());
                return (INPUT);
            } catch (BusinessRuleException e) {
                log.warn(e.getUserMessage());
                addSessionActionErrorMessage(e.getUserMessage());
                return (INPUT);
            }
            ptsRegionManager.save(this.ptsRegion);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                if (fve.getField() == "region.name") {
                    fve.setField("ptsRegion.name");
                } else {
                    fve.setField("ptsRegion.number");
                }
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (EntityNotFoundException e) {
            log.warn("Region Number " + this.ptsRegion.getNumber()
                + " not found, possibly a concurrency problem");

            addSessionActionMessage(new UserMessage(
                "puttostore.region.edit.error.regionNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified region "
                + this.ptsRegion.getNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Region"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                PtsRegion modifiedRegion = ptsRegionManager.get(
                    this.getRegionId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.ptsRegion.setVersion(modifiedRegion.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedRegion);
            } catch (EntityNotFoundException ex) {
                addSessionActionErrorMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization("entity.Region"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage(
            "puttostore.region." + (isNew ? "create" : "edit") + ".message.success",
            makeContextURL("/puttostore/region/view.action?regionId="
                + this.ptsRegion.getId()), this.ptsRegion.getName()));

        return SUCCESS;
    }

    /**
     * Deletes the currently viewed region.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

       PtsRegion regionToDelete = null;

        try {
            regionToDelete = ptsRegionManager.get(this.getRegionId());
            ptsRegionManager.delete(this.getRegionId());

            addSessionActionMessage(new UserMessage(
                "puttostore.region.delete.message.success", regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }


    /**
     * This method sets up the <code>PtsRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission.
     * <p>
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (getRegionId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.ptsRegion = (PtsRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.ptsRegion = this.ptsRegionManager.get(getRegionId());
                saveEntityInSession(this.ptsRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Region version is: " + this.ptsRegion.getVersion());
            }
        } else if (this.ptsRegion == null) {
            // set default values for create screen.
            this.ptsRegion =  new PtsRegion();
            ptsRegion.setType(RegionType.PutToStore);
        }
        // To reset the checkbox values, only before saving the model.
        if (this.actionValue != null) {
            this.resetBools();
        }
    }

    /**
     * Reset all boolean values.
     */
    private void resetBools() {
       this.ptsRegion.setAllowRepickSkips(false);
       this.ptsRegion.setAllowSignOff(false);
       this.ptsRegion.setAllowPassAssignment(false);
       this.ptsRegion.setAllowMultipleOpenContainers(false);
       this.ptsRegion.setSystemGeneratesContainerID(false);
       this.ptsRegion.setConfirmSpokenLocation(false);
       this.ptsRegion.setConfirmSpokenLicense(false);
      }

    /**
     * Function for validating if a region is editable or not.
     *
     * @return String  success string
     * @throws Exception - any exception
     */
    public String checkIfEditable() throws Exception {
        try {
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }
                this.ptsRegion = (PtsRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.ptsRegion = this.ptsRegionManager.get(getIds()[0]);
                saveEntityInSession(this.ptsRegion);
            }
            ptsRegionManager.executeValidateEditRegion(this.ptsRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        setJsonMessage("edit!input.action?regionId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getRegionViewId() {
        return VIEW_ID;
    }

    /**
     * Gets the region manager.
     * @return The region manager.
     */
    public PtsRegionManager getPtsRegionManager() {
        return this.ptsRegionManager;
    }

    /**
     * Sets the region manager.
     * @param ptsRegionManager The new region manager.
     */
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     * Get the region.
     * @return The ptsRegion region.
     */
    public PtsRegion getPtsRegion() {
        return ptsRegion;
    }

    /**
     * Set the region.
     * @param ptsRegion - The new pts region.
     */
    public void setPtsRegion(PtsRegion ptsRegion) {
        this.ptsRegion = ptsRegion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Long> getViewIds() {
        ArrayList<Long> viewIds = new ArrayList<Long>(1);
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * @return the actionValue property.
     */
    public String getActionValue() {
        return actionValue;
    }


    /**
     * Setter for the actionValue property.
     * @param actionValue the new value.
     */
    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
     */
    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
     */
    @Override
    protected Region getRegion() {
        return getPtsRegion();
    }

    /**
     *
     * @return Collection - the collection of spokenDigits
     */
    public Map<Integer, String> getSpokenLicenseLengthMap() {
        if (this.spokenLicenseMap == null) {
            this.spokenLicenseMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 0; i <= MAX_SPOKEN_DIGITS; i++) {
                if (i == 0) {
                    this.spokenLicenseMap.put(i, getText(SPEAK_ALL));
                } else {
                    this.spokenLicenseMap.put(i, i.toString());
                }
            }
        }
        return this.spokenLicenseMap;
    }

    /**
     *
     * @return Collection - the collection of spokenDigits
     */
    public Map<Integer, String> getLocationCheckDigitLengthMap() {
        if (this.locationCheckDigitLengthMap == null) {
            this.locationCheckDigitLengthMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 0; i <= MAX_DIGITS_LENGTH; i++) {
                if (i == 0) {
                    this.locationCheckDigitLengthMap.put(i, getText(SPEAK_ALL));
                } else {
                    this.locationCheckDigitLengthMap.put(i, i.toString());
                }
            }
        }
        return this.locationCheckDigitLengthMap;
  }

    /**
     * Getter for the normalAllowSkips property.
     * @return Integer - return the value of normal allow skips.
     */
    public Integer getAllowSkips() {
        if (this.ptsRegion != null) {
            if (this.ptsRegion.isAllowSkipAisle()
                && !this.ptsRegion.isAllowSkipSlot()) {
                return SKIP_VALUE_0;
            } else if (!this.ptsRegion.isAllowSkipAisle()
               && this.ptsRegion.isAllowSkipSlot()) {
                return SKIP_VALUE_1;
            } else if (this.ptsRegion.isAllowSkipAisle()
                && this.ptsRegion.isAllowSkipSlot()) {
                return SKIP_VALUE_2;
            } else {
                return SKIP_VALUE_3;
            }
        }
        return SKIP_VALUE_2;
    }


    /**
     * Setter for the normalAllowSkips property.
     * @param normalAllowSkips - the new normal allow skips
     */
    public void setAllowSkips(Integer normalAllowSkips) {
        switch (normalAllowSkips) {
        case SKIP_VALUE_0:
            this.ptsRegion.setAllowSkipAisle(true);
            this.ptsRegion.setAllowSkipSlot(false);
        break;
        case SKIP_VALUE_1:
            this.ptsRegion.setAllowSkipAisle(false);
            this.ptsRegion.setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_2:
            this.ptsRegion.setAllowSkipAisle(true);
            this.ptsRegion.setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_3:
            this.ptsRegion.setAllowSkipAisle(false);
            this.ptsRegion.setAllowSkipSlot(false);
        break;
        default:
        break;
        }
    }

    /**
     *
     * @return Collection - collection of allow skips
     */
    public Map<Integer, String> getAllowSkipsMap() {
        if (this.allowSkipsMap == null) {
            this.allowSkipsMap = new LinkedHashMap<Integer, String>();
            this.allowSkipsMap.put(SKIP_VALUE_0, getText("region.skips.aisle"));
            this.allowSkipsMap.put(SKIP_VALUE_1, getText("region.skips.slot"));
            this.allowSkipsMap.put(SKIP_VALUE_2, getText("region.skips.both"));
            this.allowSkipsMap.put(SKIP_VALUE_3, getText("region.skips.neither"));
        }
        return this.allowSkipsMap;
    }

    /**
     *
     * @return Collection - collection of validateContainer
     */
    public Map<Integer, String> getValidateContainerMap() {
        if (this.validateContainerMap == null) {
            this.validateContainerMap = new LinkedHashMap<Integer, String>();
            this.validateContainerMap.put(0, getText("puttostore.region.validateContainer.false"));
            this.validateContainerMap.put(1, getText("puttostore.region.validateContainer.true"));
        }
        return this.validateContainerMap;
    }

    /**
     *
     * @return Collection - collection of validateContainer
     */
    public Map<Integer, String> getValidateContainerLengthMap() {
        if (this.validateContainerLengthMap == null) {
            this.validateContainerLengthMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 1; i <= MAX_DIGITS; i++) {

                    this.validateContainerLengthMap.put(i, i.toString());
         }
        }
        return this.validateContainerLengthMap;
    }

    /**
     * Getter for validate container id.
     *
     * @return boolean
     */
    public Integer getValidateContainerId() {
        return (ptsRegion.isValidateContainerId()) ? 1 : 0;
    }

    /**
     * Setter for validate container id.
     * @param validateContainerId -  validate conatiner id
     */
    public void setValidateContainerId(Integer validateContainerId) {

        if (this.ptsRegion != null) {
            if (validateContainerId == 1) {
                this.ptsRegion.setValidateContainerId(true);
            } else {
                this.ptsRegion.setValidateContainerId(false);
            }
        }

    }


    /**
     * Getter for the spokenLocationLengthMap property.
     * @return Map value of the property
     */
    public Map<Integer, String> getSpokenLocationLengthMap() {

        if (this.spokenLocationLengthMap == null) {
            this.spokenLocationLengthMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 0; i <= MAX_DIGITS; i++) {
                if (i == 0) {
                    this.spokenLocationLengthMap.put(i, getText(SPEAK_ALL));
                } else {
                    this.spokenLocationLengthMap.put(i, i.toString());
                }
            }
        }
        return this.spokenLocationLengthMap;
    }


    /**
     * Getter for the maxLicensesLengthMap property.
     * @return Map value of the property
     */
    public Map<Integer, String> getMaxLicensesLengthMap() {

        if (this.maxLicensesLengthMap == null) {
            this.maxLicensesLengthMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 1; i <= MAX_DIGITS; i++) {

                    this.maxLicensesLengthMap.put(i, i.toString());
         }
        }
        return this.maxLicensesLengthMap;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 