/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.web.core.action.LaborAction;

import java.util.LinkedList;
import java.util.List;

/**
 * This class handles all puttostore labor actions from the user interface.
 * @author treed
 */
public class PtsLicenseLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = 6189412170866740361L;

    private static final Long LICENSE_LABOR_VIEW_ID = -1048L;

    private List<Column> licenseLaborColumns;

    private PtsLicenseLaborManager ptsLicenseLaborManager;

    // Request parameter for assignment labor screen.
    private String operatorLaborId;


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getTableData() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getTableData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getTableDataForPrint() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getPrintData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * Populates the columns for the assignment labor table.
     * @return The Struts target name.
     * @throws DataAccessException On database error.
     */
    public String list() throws DataAccessException {
        View view = getUserPreferencesManager().getView(LICENSE_LABOR_VIEW_ID);
        this.licenseLaborColumns = getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(LICENSE_LABOR_VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for view Id.
     * @return The ID of the assignment labor view.
     */
    public Long getLicenseLaborViewId() {
        return LICENSE_LABOR_VIEW_ID;
    }

    /**
     * Getter for the operatorLaborId property.
     * @return String value of the property
     */
    public String getOperatorLaborId() {
        return this.operatorLaborId;
    }

    /**
     * Setter for the operatorLaborId property.
     * @param operatorLaborId the new operatorLaborId value
     */
    public void setOperatorLaborId(String operatorLaborId) {
        this.operatorLaborId = operatorLaborId;
    }

    /**
     * Getter for the licenseLaborColumns property.
     * @return List&lt;Column&gt; value of the property
     */
    public List<Column> getLicenseLaborColumns() {
        return this.licenseLaborColumns;
    }

    /**
     * Setter for the licenseLaborColumns property.
     * @param licenseLaborColumns the new licenseLaborColumns value
     */
    public void setLicenseLaborColumns(List<Column> licenseLaborColumns) {
        this.licenseLaborColumns = licenseLaborColumns;
    }

    /**
     * Getter for the ptsLicenseLaborManager property.
     * @return PtsLicenseLaborManager value of the property
     */
    public PtsLicenseLaborManager getPtsLicenseLaborManager() {
        return this.ptsLicenseLaborManager;
    }

    /**
     * Setter for the ptsLicenseLaborManager property.
     * @param ptsLicenseLaborManager the new ptsLicenseLaborManager value
     */
    public void setPtsLicenseLaborManager(PtsLicenseLaborManager ptsLicenseLaborManager) {
        this.ptsLicenseLaborManager = ptsLicenseLaborManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataProvider getManager() {
        return getPtsLicenseLaborManager();
    }

    /**
     * Overriden to take care of License labor section
     * @return whether or not this is a text action.
     */
    @Override
    public boolean isTextAction() {
        return true;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 