/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsPutManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>PtsLicense</code> objects.
 *
 * @author mnichols
 */
public class PtsLicenseActionRoot extends DataProviderAction implements Preparable {

        //
    private static final long serialVersionUID = -6498360412669771524L;

    private static final Logger log = new Logger(PtsLicenseActionRoot.class);

    // The ID of the put licenses table view.
    private static final long LICENSE_VIEW_ID = -1043;

    // The ID of the put table view.
    private static final long PUT_VIEW_ID = -1044;

    // The list of columns for the licenses table.
    private List<Column> licenseColumns;

    // The list of columns for the puts table.
    private List<Column> putColumns;

    // The PtsLicense management service.
    private PtsLicenseManager licenseManager = null;

    private PtsPutManager putManager = null;

    // The PtsLicense object, which will either be newly created, or retrieved
    // via the LicenseId.
    private PtsLicense license;

    // The Put object
    private PtsPut put;

    // The ID of the put license.
    private Long licenseId;

    private Integer licenseStatus;

    /**
     * Getter for the license property.
     * @return User value of the property
     */
    public PtsLicense getLicense() {
        return this.license;
    }

    /**
     * Setter for the license property.
     * @param license the new license value
     */
    public void setLicense(PtsLicense license) {
        this.license = license;
    }

    /**
     * Getter for the licenseId property.
     * @return Long value of the property
     */
    public Long getLicenseId() {
        return this.licenseId;
    }

    /**
     * Setter for the licenseId property.
     * @param licenseId the new licenseId value
     */
    public void setLicenseId(Long licenseId) {
        this.licenseId = licenseId;
    }

    /**
     * Getter for the licenseManager property.
     * @return UserManager value of the property
     */
    public PtsLicenseManager getPtsLicenseManager() {
        return this.licenseManager;
    }

    /**
     * Setter for the licenseManager property.
     * @param manager the new licenseManager value
     */
    public void setPtsLicenseManager(PtsLicenseManager manager) {
        this.licenseManager = manager;
    }

    /**
     * Action for the licenses view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View licenseView = getUserPreferencesManager().getView(LICENSE_VIEW_ID);
        this.licenseColumns = this.getUserPreferencesManager().getColumns(
            licenseView, getCurrentUser());
        View putView = getUserPreferencesManager().getView(PUT_VIEW_ID);
        this.putColumns = this.getUserPreferencesManager().getColumns(
            putView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the license specified by the <code>license</code>
     * member of this class.  Create is currently not used but has been left
     * in the code to accomodate future development.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = this.license.isNew();
        try {
            // Need to save this once to attach it back to the session
            // before changing status
            getPtsLicenseManager().getPrimaryDAO().reattach(this.license);
            if (licenseStatus != null) {
                license.changeStatus(PtsLicenseStatus.toEnum(licenseStatus.intValue()));
            }
            getPtsLicenseManager().executeLicenseChangeSave(this.license);
            cleanSession();
        } catch (BusinessRuleException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            log.error(
                "Error while editing licenses",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            log.warn("License Number " + this.license.getNumber()
                + " not found, possibly a concurrency");

            addSessionActionErrorMessage(new UserMessage(
                "puttostore.license.edit.error.licenseNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified license "
                + this.license.getNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.license"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                PtsLicense modifiedLicense = getPtsLicenseManager().get(
                    getLicenseId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.license.setVersion(modifiedLicense.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedLicense);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", this.license
                            .getNumber()));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage(
            "puttostore.license." + (isNew ? "create" : "edit") + ".message.success",
             String.valueOf(this.license.getNumber())));

        return SUCCESS;
    }

    /**
     * This method sets up the <code>PtsLicense</code> object by retrieving it
     * from the database when a licenseId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.licenseId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                this.license = (PtsLicense) getEntityFromSession(getSavedEntityKey());
            } else {
                this.license = this.licenseManager.get(this.licenseId);
                saveEntityInSession(this.license);
            }

            if (log.isDebugEnabled()) {
                log.debug("PtsLicense version is: " + this.license.getVersion());
            }

        }
    }

    /**
     * Delete the location identified by the <code>locationId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentPtsLicense() throws Exception {
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPtsLicenseManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "license";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(LICENSE_VIEW_ID);
        viewIds.add(PUT_VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for the licenseColumns property.
     * @return List&lt;Column&gt; value of the property
     */
    public List<Column> getLicenseColumns() {
        return licenseColumns;
    }


    /**
     * Setter for the licenseColumns property.
     * @param licenseColumns the new licenseColumns value
     */
    public void setLicenseColumns(List<Column> licenseColumns) {
        this.licenseColumns = licenseColumns;
    }


    /**
     * Getter for the putColumns property.
     * @return List&lt;Column&gt; value of the property
     */
    public List<Column> getPutColumns() {
        return putColumns;
    }


    /**
     * Setter for the putColumns property.
     * @param putColumns the new putColumns value
     */
    public void setPutColumns(List<Column> putColumns) {
        this.putColumns = putColumns;
    }


    /**
     * Getter for the LICENSE_VIEW_ID property.
     * @return long value of the property
     */
    public static long getLicenseViewId() {
        return LICENSE_VIEW_ID;
    }


    /**
     * Getter for the pUT_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPutViewId() {
        return PUT_VIEW_ID;
    }

    /**
     * Getter for the put property.
     * @return Put value of the property
     */
    public PtsPut getPut() {
        return put;
    }


    /**
     * Setter for the put property.
     * @param put the new put value
     */
    public void setPut(PtsPut put) {
        this.put = put;
    }


    /**
     * Getter for the putManager property.
     * @return PutManager value of the property
     */
    public PtsPutManager getPutManager() {
        return putManager;
    }


    /**
     * Setter for the putManager property.
     * @param putManager the new putManager value
     */
    public void setPutManager(PtsPutManager putManager) {
        this.putManager = putManager;
    }

    /**
     * Function for validating if an license is editable or not.
     *
     * @return String defining next target to forward to.
     */
    public String checkIfEditable() {
        try {
            //Get the first and only license from the id array
            PtsLicense theLicense = getPtsLicenseManager().get(getIds()[0]);

            //If the license is not editable, log a warning, and return the error json message
            if (!theLicense.isEditable()) {
                log.warn("Error while validating license " + theLicense.getId() + " for editing");
                setJsonMessage(getText("puttostore.license.edit.error.invalidLicenseStatus"), ERROR_FAILURE);
                return SUCCESS;
            }
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        //The license is editable, so redirect to the edit screen
        setJsonMessage("edit!input.action?licenseId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Getter for License Status.
     * @return The name property of the license's status.
     */
    public Integer getLicenseStatus() {
        return this.license.getStatus().getValue();
    }

    /**
     * Calls changeStatus in model class to set License Status.
     * @param status the String representing the status to change to.
     */
    public void setLicenseStatus(Integer status) {
        this.licenseStatus = status;
    }

    /**
     * Getter for the exportStatus property.
     * @return String value of the property
     */
    public String getExportStatus() {
        return getText(license.getExportStatus().getResourceKey());
    }

    /**
     * Getter for the license startTime, which will return the properly formatted time.
     * @return String value of the property
     */
    public String getLicenseStartTime() {
        String startTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            startTime = du.formatTimeWithTimeZone(getLicense().getStartTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            startTime = getLicense().getStartTime().toString();
        }

        return startTime;
    }

    /**
     * Getter for the license endTime, which will return the properly formatted time.
     * @return String value of the property
     */
    public String getLicenseEndTime() {
        String endTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            endTime = du.formatTimeWithTimeZone(getLicense().getEndTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            endTime = getLicense().getEndTime().toString();
        }

        return endTime;
    }

    /**
     * Getter for the createdDate, which will return the properly formatted time.
     * @return String value of the property
     */
    public String getDateImported() {
        String dateImported = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            dateImported = du.formatTimeWithTimeZone(getLicense().getCreatedDate(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            dateImported = getLicense().getCreatedDate().toString();
        }

        return dateImported;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 