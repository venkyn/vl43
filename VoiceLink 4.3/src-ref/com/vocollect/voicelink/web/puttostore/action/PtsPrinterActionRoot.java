/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.web.core.action.PrinterActionRoot;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;



/**
 * Action code for print label.
 * @author jtauberg
 */
public class PtsPrinterActionRoot extends PrinterActionRoot {

    //
    private static final long serialVersionUID = -8766943563088254031L;


    //  logger for PtsPrinterAction class
    private static final Logger log = new Logger(PtsPrinterActionRoot.class);


     // ContainerManager is service for ptsContainers
    private PtsContainerManager ptsContainerManager;



    /**
     * Getter for the ptsContainerManager property.
     * @return PtsContainerManager value of the property
     */
    public PtsContainerManager getPtsContainerManager() {
        return ptsContainerManager;
    }


    /**
     * Setter for the ptsContainerManager property.
     * @param ptsContainerManager the new ptsContainerManager value
     */
    public void setPtsContainerManager(PtsContainerManager ptsContainerManager) {
        this.ptsContainerManager = ptsContainerManager;
    }



    /**
     * @return the control flow target name.
     * @throws VocollectException if error occurs printing assignment labels.
     */
    public String printPtsContainerLabel() throws VocollectException {

        ArrayList<PtsContainer> containersToPrint = new ArrayList<PtsContainer>(getIds().length);

        for (Long id : getIds()) {
            containersToPrint.add(this.getPtsContainerManager().get(id));
        }

        try {
            this.setPrinter(this.getPrinterManager().get(this.getPrinterId()));
            this.getPrinterManager().generatePtsContainerLabels(containersToPrint, this.getPrinter());
        } catch (DataAccessException e) {
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while printing label",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }


        if (containersToPrint.size() == 1) {
            setJsonMessage(getText(
                new UserMessage(
                   "print.containerLabel.success.single",  false,
                   containersToPrint.get(0).getContainerNumber(),
                   this.getPrinter().getName())),
                ERROR_SUCCESS);
        } else if (containersToPrint.size() > 1) {
            setJsonMessage(getText(
                new UserMessage(
                    "print.containerLabel.success.multiple", false,
                    containersToPrint.size(),
                    this.getPrinter().getName())),
                ERROR_SUCCESS);
        }

        return SUCCESS;
    }


 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 