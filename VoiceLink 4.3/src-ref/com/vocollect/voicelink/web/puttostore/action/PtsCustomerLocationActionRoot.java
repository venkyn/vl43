/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>PtsCustomerLocation</code> objects.
 *
 * @author mnichols
 */
public class PtsCustomerLocationActionRoot extends DataProviderAction implements Preparable {

        //
    private static final long serialVersionUID = -6498360412669771524L;

    private static final Logger log = new Logger(PtsCustomerLocationActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long ROUTE_CUSTOMER_VIEW_ID = -1042;

    private static final long CUSTOMER_ORDER_VIEW_ID = -1045;

    // The list of columns for the route customer table.
    private List<Column> customerLocationColumns;

    // The list of columns for the customer order table.
    private List<Column> customerOrderColumns;

    // The CustomerLocation management service.
    private PtsCustomerLocationManager customerLocationManager = null;

    // The PtsCustomerLocation object, which will either be newly created, or retrieved
    // via the CustomerLocationId.
    private PtsCustomerLocation customerLocation;

    // The ID of the route customer.
    private Long customerLocationId;

    /**
     * Getter for the customerLocation property.
     * @return User value of the property
     */
    public PtsCustomerLocation getCustomerLocation() {
        return this.customerLocation;
    }

    /**
     * Setter for the customerLocation property.
     * @param customerLocation the new customerLocation value
     */
    public void setCustomerLocation(PtsCustomerLocation customerLocation) {
        this.customerLocation = customerLocation;
    }

    /**
     * Getter for the customerLocationId property.
     * @return Long value of the property
     */
    public Long getCustomerLocationId() {
        return this.customerLocationId;
    }

    /**
     * Setter for the customerLocationId property.
     * @param customerLocationId the new customerLocationId value
     */
    public void setCustomerLocationId(Long customerLocationId) {
        this.customerLocationId = customerLocationId;
    }

    /**
     * Getter for the customerLocationManager property.
     * @return UserManager value of the property
     */
    public PtsCustomerLocationManager getPtsCustomerLocationManager() {
        return this.customerLocationManager;
    }

    /**
     * Setter for the customerLocationManager property.
     * @param manager the new customerLocationManager value
     */
    public void setPtsCustomerLocationManager(PtsCustomerLocationManager manager) {
        this.customerLocationManager = manager;
    }

    /**
     * Getter for the route customer columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getCustomerLocationColumns() {
        return this.customerLocationColumns;
    }

    /**
     * Getter for the customer order columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getCustomerOrderColumns() {
        return this.customerOrderColumns;
    }

    /**
     * Action for the customerLocations view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View customerLocationView = getUserPreferencesManager().getView(ROUTE_CUSTOMER_VIEW_ID);
        this.customerLocationColumns = this.getUserPreferencesManager().getColumns(
            customerLocationView, getCurrentUser());
        View customerOrderView = getUserPreferencesManager().getView(CUSTOMER_ORDER_VIEW_ID);
        this.customerOrderColumns = this.getUserPreferencesManager().getColumns(
            customerOrderView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the customerLocation specified by the <code>customerLocation</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        return SUCCESS;
    }

    /**
     * This method sets up the <code>PtsCustomerLocation</code> object by retrieving it
     * from the database when a customerLocationId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getPtsCustomerLocationManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "puttostore.customerLocation";
    }

    /**
     * Getter for the ROUTE_CUSTOMER_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCustomerLocationViewId() {
        return ROUTE_CUSTOMER_VIEW_ID;
    }

    /**
     * Getter for the CUSTOMER_ORDER_VIEW_ID property.
     * @return long value of the property
     */
    public static long getCustomerOrderViewId() {
        return CUSTOMER_ORDER_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(ROUTE_CUSTOMER_VIEW_ID);
        viewIds.add(CUSTOMER_ORDER_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 