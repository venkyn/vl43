/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

//import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on <code>Container</code>
 * objects.
 *
 * @author ddoubleday
 */
public class PickDetailActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

//    private static final Logger log = new Logger(PickDetailAction.class);

    private Long assignmentId = null;
    private Long pickId = null;

    // The ID of the view we need to grab from the DB.
    private static final long PICK_DETAIL_VIEW_ID = -1015;

    // The list of columns for the picks table.
    private List<Column> pickDetailColumns;

    // The Pick Detail management service.
    private PickDetailManager pickDetailManager = null;
    /**
     *
     * @return Long pickId
     */
    public Long getPickId() {
        return pickId;
    }
    /**
     *
     * @param pickId new pick id
     */
    public void setPickId(Long pickId) {
        this.pickId = pickId;
    }
    /**
     *
     * @return Long assignment Id
     */
    public Long getAssignmentId() {
        return assignmentId;
    }

    /**
     *
     * @param assignmentId new assignment Id
     */
    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    /**
     * Getter for the pickDetailManager property.
     * @return PickDetailManager value of the property
     */
    public PickDetailManager getPickDetailManager() {
        return this.pickDetailManager;
    }

    /**
     * Setter for the pickDetailManager property.
     * @param manager the new pickDetailManager value
     */
    public void setPickDetailManager(PickDetailManager manager) {
        this.pickDetailManager = manager;
    }

    /**
     * Getter for the pickDetailColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPickDetailColumns() {
        return this.pickDetailColumns;
    }

    /**
     * Action for the containers view page. Initializes the containers table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View pickDetailView = getUserPreferencesManager().getView(PICK_DETAIL_VIEW_ID);
        this.pickDetailColumns = this.getUserPreferencesManager().getColumns(pickDetailView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it
     * from the database when a containerId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPickDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getAssignmentId() != -1) {
            super.getTableData("obj.pick.assignment.id = " + getAssignmentId()
                + " and " + getCurrentSiteTagId()
                + " in elements(obj.pick.tags)");
        } else if (getPickId() != -1) {
            super.getTableData("obj.pick.id = " + getPickId() + " and "
                + getCurrentSiteTagId() + " in elements(obj.pick.tags)");
        } else {
            super.getTableData(getCurrentSiteTagId()
                + " in elements(obj.pick.tags)");
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "PickDetail";
    }

    /**
     * Getter for the PICK_DETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPickDetail_View_Id() {
        return PICK_DETAIL_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(PICK_DETAIL_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 