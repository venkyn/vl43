/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.PickManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.*;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * This is the Struts action class that handles operations on <code>Pick</code>
 * objects.
 *
 * @author ddoubleday
 */
public class PickActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

    private static final Logger log = new Logger(PickAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1010;

    // The list of columns for the picks table.
    private List<Column> columns;

    // The Pick management service.
    private PickManager pickManager = null;

    // The Pick object, which will either be newly created, or retrieved
    // via the PickId.
    private Pick pick;

    // The ID of the Pick.
    private Long pickId;

    // Preferences manager.
    private UserPreferencesManager userPreferencesManager;

    // Assignment Manager
    private AssignmentManager assignmentManager = null;

    // Operator Manager.
    private OperatorManager operatorManager = null;

    // New Operator parameter for split function(from URL)
    private String newOperator;

    // Original Operator parameter for split function(from URL)
    private String oldOperator;

    //Quantity Entered by user for manual picking
    private String quantityEntered;

    private Long regionId;

    /**
     * Getter for the pick property.
     * @return value of the property
     */
    public Pick getPick() {
        return this.pick;
    }

    /**
     * Setter for the pick property.
     * @param pick the new user value
     */
    public void setPick(Pick pick) {
        this.pick = pick;
    }

    /**
     * Getter for the userId property.
     * @return Long value of the property
     */
    public Long getPickId() {
        return this.pickId;
    }

    /**
     * Setter for the userId property.
     * @param userId the new userId value
     */
    public void setPickId(Long userId) {
        this.pickId = userId;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public PickManager getPickManager() {
        return this.pickManager;
    }

    /**
     * Setter for the userManager property.
     * @param manager the new userManager value
     */
    public void setPickManager(PickManager manager) {
        this.pickManager = manager;
    }

    /**
     * Getter for the userPreferencesManager property.
     * @return UserPreferencesManager value of the property
     */
    @Override
    public UserPreferencesManager getUserPreferencesManager() {
        return this.userPreferencesManager;
    }

    /**
     * Setter for the userPreferencesManager property.
     * @param userPreferencesManager the new userPreferencesManager
     */
    @Override
    public void setUserPreferencesManager(UserPreferencesManager userPreferencesManager) {
        this.userPreferencesManager = userPreferencesManager;
    }

    /**
     * Getter for the assignmentManager property.
     *
     * @return assignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return this.assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     *
     * @param manager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager manager) {
        this.assignmentManager = manager;
    }

    /**
     *
     * @return Returns Operator Manager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Sets Work Group Manager.
     *
     * @param manager Instance of Work Group Manager class
     */
    public void setOperatorManager(OperatorManager manager) {
        this.operatorManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPickColumns() {
        return this.columns;
    }

    /**
     * Action for the picks view page. Initializes the picks table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View pickView = userPreferencesManager.getView(VIEW_ID);
        this.columns = this.userPreferencesManager.getColumns(
            pickView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the user specified by the <code>user</code> member of
     * this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        // TODO
        if (log.isDebugEnabled()) {
            log.debug("Saving Pick");
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it from
     * the database when a userId is set by the form submission. {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getPickManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Pick";
    }

    /**
     * Getter for URL param newOperator to be used for splitting picks.
     * @return String
     */
    public String getNewOperator() {
        return newOperator;
    }

    /**
     * Setter for URL param newOperator to be used for splitting picks.
     * @param newOperator value to set
     */
    public void setNewOperator(String newOperator) {
        this.newOperator = newOperator;
    }

    /**
     * Getter for URL param oldOperator to be used for splitting picks.
     * @return String
     */
    public String getOldOperator() {
        return oldOperator;
    }

    /**
     * Setter for URL param oldOperator to be used for splitting picks.
     * @param oldOperator The old operator.
     */
    public void setOldOperator(String oldOperator) {
        this.oldOperator = oldOperator;
    }

    /**
     * Getter for URL param quantityEntered to be used for manual picking.
     * @return String
     */
    public String getQuantityEntered() {
        return quantityEntered;
    }

    /**
     * Setter for URL param quantityEntered to be used for manual picking.
     * @param quantityEntered value entered
     * @return
     */
    public void setQuantityEntered(String quantityEntered) {
        this.quantityEntered = quantityEntered;
    }

    /**
     * Method for validating if assignment can be split.
     * @return the control flow target of this action method.
     */
    public String validateSplit() {
        ArrayList<Pick> picks = new ArrayList<Pick>(getIds().length);
        try {
            for (Long id : getIds()) {
                picks.add(pickManager.get(id));
            }
        } catch (DataAccessException e) {
            log.warn("Error while retrieving pick");
            setJsonMessage(getText(new UserMessage(
                "pick.validatesplit.message.failure.retrieval")), ERROR_FAILURE);
            return SUCCESS;
        }
        try {
            getAssignmentManager().validateSplitAssignment(picks);
        } catch (BusinessRuleException e) {
            log.error(
                "Error while validating splits",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        return INPUT;
    }

    /**
     * Method for validating if manual picking is allowed.
     * @return String
     * @throws DataAccessException db failure
     * @throws BusinessRuleException business rule failure
     */
    public String validateManualPick() throws DataAccessException,
        BusinessRuleException {
        this.pick = getPickManager().get(this.pickId);
        try {
            getPickManager().validateIfManualPickAllowed(pick);
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while validating manual pick",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }
        return INPUT;
    }

    /**
     * Returns the list of operators in the passed region.
     * @return map of operators in a regoins.
     * @throws DataAccessException on database error.
     */
    public Map<Long, String> getAllOperatorsInRegion()
    throws DataAccessException {
        // TODO get by region, right now returning all just for testing.
        Map<Long, String> operators = new LinkedHashMap<Long, String>();
        Pick p = pickManager.get(getIds()[0]);
        operators.put(0L, getText(new UserMessage("operator.none")));
        Region reg = p.getAssignment().getRegion();
        List<Operator> allOperators = getOperatorManager()
            .listAuthorizedForRegion(reg.getId());
        for (Operator operatorname : allOperators) {
            operators.put(operatorname.getId(), operatorname
                .getOperatorIdentifier());
        }
        return operators;
    }

    /**
     * Getter for the regionId property.
     * @return The region ID.
     */
    public Long getRegionId() {
        return this.regionId;
    }

    /**
     * Setter for the regionId property.
     * @param regionId the new regionId value
     */
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    /**
     * Manually pick the picks identified by the <code>List[] ids</code>.
     * @return SUCCESS control flow target if the manual pick was successful or if
     *         the specified object wasn't found (with different messages to the
     *         end object)
     * @throws DataAccessException on unanticipated
     */
    public String manualPick() throws DataAccessException {
        final int quantity = Integer.parseInt(getQuantityEntered());
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "manualPick";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                Pick p = (Pick) obj;
                p.updateManualPick(quantity);
                getPickManager().save(p);
                return true;
            }
        });
    }

    /**
     * Method for splitting picks selected by user.
     * @throws DataAccessException on db failure
     * @throws BusinessRuleException on business rule failure
     * @return String
     */
    public String splitPicks() throws DataAccessException,
        BusinessRuleException {

        long origOperator = Long.parseLong(getOldOperator());
        long nOperator = Long.parseLong(getNewOperator());

        ArrayList<Pick> picksToSplit = new ArrayList<Pick>(getIds().length);

        Operator operatorOriginal = null;
        Operator operatorNew = null;
        if (origOperator != 0) {
            operatorOriginal = this.operatorManager.get(origOperator);
        }
        if (nOperator != 0) {
            operatorNew = this.operatorManager.get(nOperator);
        }
        try {
            for (Long id : getIds()) {
                picksToSplit.add(pickManager.get(id));
            }
        } catch (DataAccessException e) {
            log.warn("Error while retreiving Pick", e);
            setJsonMessage(
                getText(new UserMessage(
                    "pick.cancelPick.message.failure.retrieval")),
                    ERROR_FAILURE);
            return SUCCESS;
        }

        try {
            getAssignmentManager().executeCreateSplitAssignment(
                picksToSplit, operatorNew, operatorOriginal);

        } catch (DataAccessException e) {
            log.error(
                "Error while splitting picks",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while creating grouping assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }
        if (picksToSplit.size() == 1) {
            setJsonMessage(getText(
                new UserMessage("pick.splitPick.message.success.single")),
                ERROR_SUCCESS);
        } else {
            setJsonMessage(getText(
                new UserMessage(
                    "pick.splitPick.message.success.multiple",
                    picksToSplit.size())),
                ERROR_SUCCESS);
        }

        return SUCCESS;

    }

    /**
     * Cancel the picks identified by the <code>List[] ids</code>.
     * @return SUCCESS control flow target if the cancel was successful or if
     *         the specified object wasn't found (with different messages to the
     *         end object)
     * @throws DataAccessException on unanticipated
     */
    public String cancelPick() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "cancel";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                Pick p = (Pick) obj;
                p.cancelPick();
                getPickManager().save(p);
                return true;
            }
        });
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for the pickStatus property.
     * @return String value of the property
     */
    public String getPickStatus() {
        return getText(pick.getStatus().getResourceKey());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 