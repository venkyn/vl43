/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.SelectionRegionAutoMarkOutShorts;
import com.vocollect.voicelink.selection.model.SelectionRegionBaseItem;
import com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.model.SelectionRegionSummaryPrompt;
import com.vocollect.voicelink.selection.model.SummaryPrompt;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;
import com.vocollect.voicelink.selection.service.SelectionRegionProfileManager;
import com.vocollect.voicelink.selection.service.SummaryPromptManager;
import com.vocollect.voicelink.web.core.action.RegionAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.Preparable;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.hibernate.classic.Validatable;

/**
 * This is the Struts action class that handles operations on <code>Selection Region</code>
 * objects.
 *
 * @author svoruganti
 */
public class SelectionRegionActionRoot
    extends RegionAction implements Preparable, Validatable {

    private static final int PAGE_ONE = 1;
    private static final int PAGE_TWO = 2;
    private static final int PAGE_THREE = 3;

    private static final int SKIP_VALUE_0 = 0;
    private static final int SKIP_VALUE_1 = 1;
    private static final int SKIP_VALUE_2 = 2;
    private static final int SKIP_VALUE_3 = 3;


    private static final int MAX_SPOKEN_DIGITS = 18;
    private static final String SPEAK_ALL = "region.spokenDigits.all";
    private static final int MAX_RANGE = 99999;

    private static final long serialVersionUID = 5824301006857941610L;

    private static final Logger log = new Logger(SelectionRegionAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1013;

    // The list of profiles for the create and edit pages.
    private Map<String, Long> profileList;

    private Map<Long, String> profileMap;

    // The region manager service.
    private SelectionRegionManager selectionRegionManager = null;

    // The region manager service.
    private SelectionRegionProfile selectionRegionProfile;

    // The selection region profile manager service.
    private SelectionRegionProfileManager selectionRegionProfileManager;

    private SummaryPromptManager summaryPromptManager;

    // The region object, which will either be newly created, or retrieved
    // via the regionId.
    private SelectionRegion selectionRegion;

    // The Page number for the create screen
    private int pageNumber = 0;

    private String changeAction;

    // The Page number for the create screen
    private String formName;

    // The ID of the selectionNormalProfileId
    private Long selectionNormalProfileId;

    // The ID of the selectionChaseProfileId.
    private Long selectionChaseProfileId;

    private Long normalSummaryPromptId;

    private Long chaseSummaryPromptId;

    // Preferences manager.
    //private UserPreferencesManager userPreferencesManager;

    private Map<Integer, String> pickTypeMap;

    private Map<Integer, String> allowSkipsMap;

    private Map<Integer, String> allowGoBackForShortsMap;

    private Map<Long, String> assignmentSummaryPromptMap;

    private Map<Integer, String> autoMarkOutShortsMap;

    private Map<Integer, String> spokenDigitsMap;

    /*
     * List of prompt items
     */
    private List<SummaryPrompt> summaryPrompts;


    /**
     *
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "selectionRegion";
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.selectionRegionManager;
    }

    /**
     * Getter for the region property.
     *
     * @return Region value of the property
     */
    public SelectionRegion getSelectionRegion() {
        return this.selectionRegion;
    }

    /**
     * Setter for the region property.
     *
     * @param selectionRegion
     *            the new region value
     */
    public void setSelectionRegion(SelectionRegion selectionRegion) {
        this.selectionRegion = selectionRegion;
    }

    /**
     * Getter for the selectionRegionManager property.
     *
     * @return RegionManager value of the property
     */
    public SelectionRegionManager getSelectionRegionManager() {
        return this.selectionRegionManager;
    }

    /**
     * Setter for the selectionRegionManager property.
     *
     * @param selectionRegionManager
     *            the new regionManager value
     */
    public void setSelectionRegionManager(
                                          SelectionRegionManager selectionRegionManager) {
        this.selectionRegionManager = selectionRegionManager;
    }

    /**
     * Getter for the selectionRegionProfile property.
     *
     * @return SelectionRegionProfile value of the property
     */
    public SelectionRegionProfile getSelectionRegionProfile() {
        return selectionRegionProfile;
    }

    /**
     * Setter for the selectionRegionProfile property.
     *
     * @param selectionRegionProfile
     *              the new selectionRegionProfile value
     *
     */
    public void setSelectionRegionProfile(SelectionRegionProfile selectionRegionProfile) {
        this.selectionRegionProfile = selectionRegionProfile;
    }

    /**
     * Getter for the selectionRegionProfileManager property.
     *
     * @return selectionRegionProfileManager value of the property
     */
    public SelectionRegionProfileManager getSelectionRegionProfileManager() {
        return selectionRegionProfileManager;
    }

    /**
     * Setter for the selectionRegionProfileManager property.
     *
     * @param selectionRegionProfileManager
     *          the new selectionRegionProfileManager value
     *
     */
    public void setSelectionRegionProfileManager(SelectionRegionProfileManager selectionRegionProfileManager) {
        this.selectionRegionProfileManager = selectionRegionProfileManager;
    }

    /**
     * Getter for the selectionChaseProfileId property.
     *
     * @return selectionChaseProfileId value of the property
     */
    public Long getSelectionChaseProfileId() {
        return selectionChaseProfileId;
    }

    /**
     * Setter for the selectionChaseProfileId property.
     *
     * @param selectionChaseProfileId
     *                    the new selectionChaseProfileId value
     *
     */
    public void setSelectionChaseProfileId(Long selectionChaseProfileId) {
        this.selectionChaseProfileId = selectionChaseProfileId;
    }

    /**
     * Getter for the selectionNormalProfileId property.
     *
     * @return selectionNormalProfileId value of the property
     */
    public Long getSelectionNormalProfileId() {
        return selectionNormalProfileId;
    }

    /**
     * Setter for the selectionNormalProfileId property.
     *
     * @param selectionNormalProfileId
     *       the new selectionNormalProfileId value
     *
     */
    public void setSelectionNormalProfileId(Long selectionNormalProfileId) {
        this.selectionNormalProfileId = selectionNormalProfileId;
    }

    /**
     * This method sets up the <code>selectionRegion</code> object by
     * retrieving it from the database when a regionId is set by the form
     * submission. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (summaryPrompts == null) {
            ResultDataInfo rdi = makeResultDataInfo();
            summaryPrompts = this.summaryPromptManager.getAll(rdi);
        }
        log.setLevel(Level.DEBUG);
        log.debug("prepare");
        if (this.pageNumber == PAGE_ONE) {
            this.setProfileList(AssignmentType.Normal, getRegionId());
        }
        if (this.pageNumber == PAGE_TWO) {
            this.setProfileList(AssignmentType.Chase, getRegionId());
        }
        log.debug("regionId = " + getRegionId());
        if (getRegionId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("regionId is " + getRegionId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.selectionRegion = (SelectionRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.selectionRegion = this.selectionRegionManager.get(getRegionId());
                this.selectionNormalProfileId = this.selectionRegion.getProfileNormalAssignment().getId();
                this.selectionChaseProfileId = this.selectionRegion.getProfileChaseAssignment().getId();
                saveEntityInSession(this.selectionRegion);
            }
            if (log.isDebugEnabled()) {
                log.debug("Region version is: " + this.selectionRegion.getVersion());
            }
        } else {
            if (StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                if (log.isDebugEnabled()) {
                    log.debug("Created new region object");
                }
                this.selectionRegion = new SelectionRegion();
                saveEntityInSession(this.selectionRegion);
            } else {
                this.selectionRegion = (SelectionRegion) getEntityFromSession(getSavedEntityKey());
            }
        }
        if (this.changeAction != null) {
            if (this.pageNumber == PAGE_ONE) {
                log.info("resetting normal bools");
                this.resetNormalBools();
            }
            if (this.pageNumber == PAGE_TWO) {
                log.info("resetting chase bools");
                this.resetChaseBools();
            }
        }
    }

    /**
     * Action called to duplicate an existing region and display the 3rd create page
     * for the user.
     * @return String INPUT value
     * @throws Exception general unexpected exception
     */
    public String duplicate() throws Exception {
        //Clone the region so it does not have an id
        this.selectionRegion = this.selectionRegion.clone();
        
        // the summary prompt ids need to be explicitly set since the values are
        // not posted from page 1 and 2 as page 3 is directly shown  
        if (this.getSelectionRegion().getUserSettingsNormal()
            .getSummaryPrompt() != null) {
            setNormalSummaryPromptId(this.getSelectionRegion()
                .getUserSettingsNormal().getSummaryPrompt().getId());
        }
        
        if (this.getSelectionRegion().getUserSettingsChase().getSummaryPrompt() != null) {
            setChaseSummaryPromptId(this.getSelectionRegion()
                .getUserSettingsChase().getSummaryPrompt().getId());
        }
        
        //Clear the session added to by the prepare method
        cleanSession();
        //Save the id-less region into the session
        saveEntityInSession(this.selectionRegion);
        //Set the freemarker templates to display page 3
        this.pageNumber = PAGE_THREE;
        return (INPUT);
    }

    /**
     * @return String -
     */
    public String input() {
        this.pageNumber = PAGE_ONE;
        try {
            this.setProfileList(AssignmentType.Normal, getRegionId());
        } catch (Exception e) {
            //Input doesn't throw exceptions so had to catch it
            e.printStackTrace();
        }
        return (INPUT);
    }

    /**
     * Function for validating if a region is editable or not.
     *
     * @return String  success string
     * @throws Exception - any exception
     */
    public String checkIfEditable() throws Exception {

        Long[] ids = getIds();
        try {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the Region is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved region from session");
                }

                this.selectionRegion = (SelectionRegion) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting region from database");
                }
                this.selectionRegion = this.selectionRegionManager.get(ids[0]);
                this.selectionNormalProfileId = this.selectionRegion.getProfileNormalAssignment().getId();
                this.selectionChaseProfileId = this.selectionRegion.getProfileChaseAssignment().getId();
                saveEntityInSession(this.selectionRegion);
            }
            selectionRegionManager.executeValidateEditRegion(this.selectionRegion);

        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating region for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }

        boolean redirect = true;
        String urlToRedirect = "edit!input.action?regionId=" + ids[0];
        if (redirect) {
            log.info("REDIRECTING");
            setJsonMessage(urlToRedirect, ERROR_REDIRECT);
        }
        return SUCCESS;
    }

    /**
     *
     * @return String -
     * @throws Exception - any exception
     */
    public String save() throws Exception {
        log.debug("calling save");
        if (this.selectionRegion.getUserSettingsNormal().getBaseQuantityThreshold() == null) {
            addActionError(getText("region.create.label.name")
                + getText("errors.required"));
        }

        if (hasErrors()) {
            return (INPUT);
        }
        if (changeAction != null) {

            if (changeAction.trim().equalsIgnoreCase("prev")) {

                //Going to page 1 so save page 2 info into the session
                if (pageNumber == PAGE_TWO) {
                    selectionRegion.setProfileChaseAssignment(this.selectionRegionProfileManager.
                        get(this.selectionChaseProfileId));
                    this.setProfileList(AssignmentType.Normal, getRegionId());
                }

                //Going to page 2 so save page 3 info into the session
                if (pageNumber == PAGE_THREE) {
                    this.setProfileList(AssignmentType.Chase, getRegionId());
                }

                pageNumber--;
            } else if (changeAction.trim().equalsIgnoreCase("next")) {

                //Going to page 2 so save page 1 info into the session
                if (pageNumber == PAGE_ONE) {
                    //Validate page 1 fields

                    /*
                    if (this.selectionRegion.getUserSettingsNormal().getWorkIdentifierRequestLength()==null)
                        addFieldError("selectionRegion.name", getText("region.create.label.name")
                            + getText("errors.required"));
                    if (this.selectionRegion.)*/
                    selectionRegion.setProfileNormalAssignment(this.selectionRegionProfileManager.
                        get(this.selectionNormalProfileId));
                    this.setProfileList(AssignmentType.Chase, getRegionId());
                }

                //Going to page 3 so save page 2 info into the session
                if (pageNumber == PAGE_TWO) {
                    selectionRegion.setProfileChaseAssignment(this.selectionRegionProfileManager.
                        get(this.selectionChaseProfileId));
                }
                pageNumber++;
            } else {
                // Submitting the form to the database
                selectionRegion.setType(RegionType.Selection);


                if (this.selectionRegion.getName() == null
                    || this.selectionRegion.getName().trim().equalsIgnoreCase("")) {
                    addFieldError("selectionRegion.name", getText("region.create.label.name")
                        + getText("errors.required"));
                }
                if (this.selectionRegion.getNumber() == null) {
                    addFieldError("selectionRegion.number", getText("region.create.label.number")
                        + getText("errors.required"));
                }
                if (this.selectionRegion.getGoalRate() == null) {
                    addFieldError("selectionRegion.goalRate", getText("region.create.label.goalRate")
                        + getText("errors.required"));
                }
                if (this.selectionRegion.getGoalRate() != null) {
                    if (this.selectionRegion.getGoalRate() <= 0 || this.selectionRegion.getGoalRate() > MAX_RANGE) {
                       addFieldError("selectionRegion.goalRate", getText("selectionRegion.goalRate.invalid.range"));
                }
                }
                if (hasErrors()) {
                    return (INPUT);
                }

                try {
                    selectionRegionManager.executeValidateEditRegion(this.selectionRegion);
                } catch (DataAccessException e) {
                   log.warn(e.getUserMessage());
                    addSessionActionMessage(e.getUserMessage());
                    return (INPUT);
                } catch (BusinessRuleException e) {
                    log.warn(e.getUserMessage());
                    addSessionActionMessage(e.getUserMessage());
                    return (INPUT);
                }
                if (normalSummaryPromptId == null) {
                    this.selectionRegion.getUserSettingsNormal().setSummaryPrompt(null);
                }
                if (chaseSummaryPromptId == null) {
                    this.selectionRegion.getUserSettingsChase().setSummaryPrompt(null);
                }
                if (normalSummaryPromptId != null) {
                    this.selectionRegion.getUserSettingsNormal()
                        .setSummaryPrompt(summaryPromptManager.get(normalSummaryPromptId));
                }
                if (chaseSummaryPromptId != null) {
                    this.selectionRegion.getUserSettingsChase()
                        .setSummaryPrompt(summaryPromptManager.get(chaseSummaryPromptId));
                }
                boolean isNew = this.selectionRegion.isNew();
                try {
                    this.selectionRegionManager.save(this.selectionRegion);
                    cleanSession();
                } catch (BusinessRuleException bre) {
                    if (bre instanceof FieldValidationException) {
                        FieldValidationException fve = (FieldValidationException) bre;
                        if (fve.getField() == "region.name") {
                            fve.setField("selectionRegion.name");
                        } else {
                            fve.setField("selectionRegion.number");
                        }
                        log.warn(fve.getField() + " " + fve.getValue()
                            + " already exists");
                        addFieldError(fve.getField(), fve.getUserMessage());
                        // Go back to the form to show the error message.
                        return INPUT;
                    }
                } catch (OptimisticLockingFailureException e) {
                    log.warn("Attempt to change already modified region "
                        + selectionRegion.getName());
                    addActionError(
                        new UserMessage("entity.error.modified",
                                        UserMessage.markForLocalization("entity.Region"),
                                        SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT)
                            );
                    // Get the modified data
                    // If Region has been deleted, this will throw EntityNotFoundException
                    try {
                        SelectionRegion modifiedRegion = selectionRegionManager.get(getRegionId());
                        // Set the local object's version to match, so it will work
                        // if the user resubmits.
                        selectionRegion.setVersion(modifiedRegion.getVersion());
                        // Store the modified data for display
                        setModifiedEntity(modifiedRegion);
                    } catch (EntityNotFoundException ex) {
                        addSessionActionMessage(
                            new UserMessage("entity.error.deleted",
                                            UserMessage.markForLocalization("entity.Region"), null)
                                );
                       return SUCCESS;
                    }
                    return INPUT;
                } catch (DatabaseConstraintException e) {
                    log.warn("A region named '" + this.selectionRegion.getName()
                        + "' already exists");
                    addFieldError("selectionRegion.name", new UserMessage(
                        "region.create.error.existing", this.selectionRegion.getName()));
                    // Go back to the form to show the error message.
                    return (INPUT);
                }

                String successKey = "region." + (isNew ? "create" : "edit")
                    + ".message.success";
                addSessionActionMessage(new UserMessage(
                    successKey,
                    makeContextURL("/selection/region/viewRegion!input.action?regionId="
                        + this.selectionRegion.getId()), this.selectionRegion.getName()));
                return (SUCCESS);
            }
            //this.selectionRegion = tmpSelectionRegion;
        }
        return INPUT;
    }

    /**
     *
     * @return String -
     * @throws Exception - any exception
     */
    public String retrieveProfile() throws Exception {
        Long profileId = null;
        if (this.selectionNormalProfileId != null) {
            profileId = this.selectionNormalProfileId;
        } else if (this.selectionChaseProfileId != null) {
            profileId = this.selectionChaseProfileId;
        }
        this.selectionRegionProfile = this.selectionRegionProfileManager.get(profileId);
        return SUCCESS;
    }

    /**
     * ViewNormalProfileId.
     *
     * This method sets up the <code>selectionRegionProfile</code> object by
     * retrieving it from the database using a selectionNormalProfileId
     *@return String
     *
     *@throws Exception if it cannot find matching selectionRegionProfile for the profileID
     *
     */
    public String viewNormalProfile() throws Exception {

        if (this.selectionNormalProfileId != null) {
            if (log.isDebugEnabled()) {
                log.debug("selectionNormalProfileId is "
                    + this.selectionNormalProfileId);

            }
                this.selectionRegionProfile = this.selectionRegionProfileManager.get(this.selectionNormalProfileId);

        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new selected region profile object");
            }
            this.selectionRegionProfile = new SelectionRegionProfile();
        }
        this.selectionChaseProfileId = null;
        return SUCCESS;

    }

    /**
     * viewChaseProfile.
     *
     * This method sets up the <code>selectionRegionProfile</code> object by
     * retrieving it from the database using a selectionChaseProfileId
     *@return String
     *
     *@throws Exception if it cannot find matching selectionRegionProfile for the profileID
     *
     */
    public String viewChaseProfile() throws Exception {

        if (this.selectionChaseProfileId != null) {
            if (log.isDebugEnabled()) {
                log.debug("selectionChaseProfileId is "
                    + this.selectionChaseProfileId);
            }
           this.selectionRegionProfile = this.selectionRegionProfileManager.get(this.selectionChaseProfileId);

        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new selected region profile object");
            }
            this.selectionRegionProfile = new SelectionRegionProfile();
        }
        this.selectionNormalProfileId = null;
        return SUCCESS;

    }

    /**
     * Getter for the profileMap property.
     * @return Map map of the profiles
     */
    public Map<Long, String> getProfileMap() {
        this.profileMap = new LinkedHashMap<Long, String>();
        Iterator<String> mapKeys = profileList.keySet().iterator();
        while (mapKeys.hasNext()) {
            String key = mapKeys.next();
            profileMap.put(profileList.get(key), getText("profile.id." + key));
        }
        return profileMap;
    }

    /**
     * Getter for the profileList property.
     * @return Map map of the profile ids
     */
    public Map<String, Long> getProfileList() {
        return profileList;
    }

    /**
     * Populates the profileList property.
     * @param id Long id
     * @param type Assignment Type
     * @throws Exception when manager can't retrieve profile values
     */
    public void setProfileList(AssignmentType type, Long id) throws Exception {
        setProfileList(this.selectionRegionManager.getRegionProfileNumbers(type, id));
    }


    /**
     * Setter for the profileList property.
     * @param profileList List of profiles
     */
    public void setProfileList(Map<String, Long> profileList) {
        this.profileList = profileList;
    }


    /**
     * Getter for the pageNumber property.
     * @return int pageNumber
     */
    public int getPageNumber() {
        return pageNumber;
    }


    /**
     * Setter for the pageNumber property.
     * @param pageNumber int pageNumber
     */
    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }


    /**
     * Getter for the changeAction property.
     * @return String changeAction
     */
    public String getChangeAction() {
        return changeAction;
    }


    /**
     * Setter for the changeAction property.
     * @param changeAction the  new changeAction
     */
    public void setChangeAction(String changeAction) {
        this.changeAction = changeAction;
    }


    /**
     * Getter for the REGION_VIEW_ID property.
     * @return long value of the property
     */
    public static long getRegionViewId() {
        return VIEW_ID;
    }

    /**
     *
     * @return Collection - the collection of spokenDigits
     */
    public Map<Integer, String> getSpokenDigitsMap() {
        if (this.spokenDigitsMap == null) {
            this.spokenDigitsMap = new LinkedHashMap<Integer, String>();

            for (Integer i = 0; i <= MAX_SPOKEN_DIGITS; i++) {
                if (i == 0) {
                    this.spokenDigitsMap.put(i, getText(SPEAK_ALL));
                } else {
                    this.spokenDigitsMap.put(i, i.toString());
                }
            }
        }
        return this.spokenDigitsMap;
    }

    /**
     *
     * @return Collection - the collection of pick type
     */
    public Map<Integer, String> getPickTypeMap() {
        if (this.pickTypeMap == null) {
            this.pickTypeMap = new LinkedHashMap<Integer, String>();
            this.pickTypeMap.put(1, getText("region.picking.type.item"));
            this.pickTypeMap.put(2, getText("region.picking.type.slot"));
        }
        return this.pickTypeMap;
    }

    /**
     *
     * @return Collection - collection of allow skips
     */
    public Map<Integer, String> getAllowSkipsMap() {
        if (this.allowSkipsMap == null) {
            this.allowSkipsMap = new LinkedHashMap<Integer, String>();
            this.allowSkipsMap.put(SKIP_VALUE_0, getText("region.skips.aisle"));
            this.allowSkipsMap.put(SKIP_VALUE_1, getText("region.skips.slot"));
            this.allowSkipsMap.put(SKIP_VALUE_2, getText("region.skips.both"));
            this.allowSkipsMap.put(SKIP_VALUE_3, getText("region.skips.neither"));
        }
        return this.allowSkipsMap;
    }

    /**
     *
     * @return Collection - collection of allowGoBackForShorts
     */
    public Map<Integer, String> getAllowGoBackForShortsMap() {
        if (this.allowGoBackForShortsMap == null) {
            this.allowGoBackForShortsMap = new LinkedHashMap<Integer, String>();
            this.allowGoBackForShortsMap.put(0, getText("region.goBack.shorts.never"));
            this.allowGoBackForShortsMap.put(1, getText("region.goBack.shorts.always"));
            this.allowGoBackForShortsMap.put(2, getText("region.goBack.shorts.replenished"));
        }
        return this.allowGoBackForShortsMap;
    }

    /**
     *
     * @return Collection - collection of assignment summary prompt
     */
    public Map<Long, String> getAssignmentSummaryPromptMap() {
        if (this.assignmentSummaryPromptMap == null) {
            this.assignmentSummaryPromptMap = new LinkedHashMap<Long, String>();
            this.assignmentSummaryPromptMap.put(new Long(-1), getText("region.summaryPrompt.default"));
            //this.assignmentSummaryPromptMap.put(1, getText("region.summaryPrompt.custom"));
            this.assignmentSummaryPromptMap.put(new Long(-2), getText("region.summaryPrompt.skip"));
            for (int i = 0; i < summaryPrompts.size(); i++) {
                this.assignmentSummaryPromptMap.put(summaryPrompts.get(i).getId(), summaryPrompts.get(i).getName());
            }
        }
        return this.assignmentSummaryPromptMap;
    }

    /**
     *
     * @return Collection - collection of auto mark out shorts
     */
    public Map<Integer, String> getAutoMarkOutShortsMap() {
        if (this.autoMarkOutShortsMap == null) {
            this.autoMarkOutShortsMap = new LinkedHashMap<Integer, String>();
            this.autoMarkOutShortsMap.put(1, getText("region.markOutShorts.never"));
            this.autoMarkOutShortsMap.put(2, getText("region.markOutShorts.replenished"));
        }
        return this.autoMarkOutShortsMap;
    }

    /**
     * Getter for the normalPickingType property.
     * @return Integer - return the type of normal pick
     */
    public Integer getNormalPickingType() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsNormal().getPickBaseItemsBy()
                == SelectionRegionBaseItem.PickByItem) {
                return 1;
            } else {
                return 2;
            }
        }
        return 1;
    }


    /**
     * Setter for the normalPickingType property.
     * @param normalPickingType the new normal picking type value
     */
    public void setNormalPickingType(Integer normalPickingType) {
        switch (normalPickingType) {
        case 1:
            this.selectionRegion.getUserSettingsNormal().setPickBaseItemsBy(
                SelectionRegionBaseItem.PickByItem);
        break;
        case 2:
            this.selectionRegion.getUserSettingsNormal().setPickBaseItemsBy(
                SelectionRegionBaseItem.PickByAisle);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the chasePickingType property.
     * @return Integer - return the type of chase pick.
     */
    public Integer getChasePickingType() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsChase().getPickBaseItemsBy()
                == SelectionRegionBaseItem.PickByItem) {
                return 1;
            } else {
                return 2;
            }
        }
        return 0;
    }


    /**
     * Setter for the chasePickingType property.
     * @param chasePickingType - the new chase picking type
     */
    public void setChasePickingType(Integer chasePickingType) {
        switch (chasePickingType) {
        case 1:
            this.selectionRegion.getUserSettingsChase().setPickBaseItemsBy(
                SelectionRegionBaseItem.PickByItem);
        break;
        case 2:
            this.selectionRegion.getUserSettingsChase().setPickBaseItemsBy(
                SelectionRegionBaseItem.PickByAisle);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the normalAllowSkips property.
     * @return Integer - return the value of normal allow skips.
     */
    public Integer getNormalAllowSkips() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsNormal().isAllowSkipAisle()
                && !this.selectionRegion.getUserSettingsNormal().isAllowSkipSlot()) {
                return SKIP_VALUE_0;
            } else if (!this.selectionRegion.getUserSettingsNormal().isAllowSkipAisle()
               && this.selectionRegion.getUserSettingsNormal().isAllowSkipSlot()) {
                return SKIP_VALUE_1;
            } else if (this.selectionRegion.getUserSettingsNormal().isAllowSkipAisle()
                && this.selectionRegion.getUserSettingsNormal().isAllowSkipSlot()) {
                return SKIP_VALUE_2;
            } else {
                return SKIP_VALUE_3;
            }
        }
        return 0;
    }


    /**
     * Setter for the normalAllowSkips property.
     * @param normalAllowSkips - the new normal allow skips
     */
    public void setNormalAllowSkips(Integer normalAllowSkips) {
        switch (normalAllowSkips) {
        case SKIP_VALUE_0:
            this.selectionRegion.getUserSettingsNormal().setAllowSkipAisle(true);
            this.selectionRegion.getUserSettingsNormal().setAllowSkipSlot(false);
        break;
        case SKIP_VALUE_1:
            this.selectionRegion.getUserSettingsNormal().setAllowSkipAisle(false);
            this.selectionRegion.getUserSettingsNormal().setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_2:
            this.selectionRegion.getUserSettingsNormal().setAllowSkipAisle(true);
            this.selectionRegion.getUserSettingsNormal().setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_3:
            this.selectionRegion.getUserSettingsNormal().setAllowSkipAisle(false);
            this.selectionRegion.getUserSettingsNormal().setAllowSkipSlot(false);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the chaseAllowSkips property.
     * @return Integer - return chase allow skips value
     */
    public Integer getChaseAllowSkips() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsChase().isAllowSkipAisle()
                && !this.selectionRegion.getUserSettingsChase().isAllowSkipSlot()) {
                return SKIP_VALUE_0;
            } else if (!this.selectionRegion.getUserSettingsChase().isAllowSkipAisle()
                && this.selectionRegion.getUserSettingsChase().isAllowSkipSlot()) {
                return SKIP_VALUE_1;
            } else if (this.selectionRegion.getUserSettingsChase().isAllowSkipAisle()
                && this.selectionRegion.getUserSettingsChase().isAllowSkipSlot()) {
                return SKIP_VALUE_2;
            } else {
                return SKIP_VALUE_3;
            }
        }
        return 0;
    }


    /**
     * Setter for the chaseAllowSkips property.
     * @param chaseAllowSkips - the new chase allow skips
     */
    public void setChaseAllowSkips(Integer chaseAllowSkips) {
        switch (chaseAllowSkips) {
        case SKIP_VALUE_0:
            this.selectionRegion.getUserSettingsChase().setAllowSkipAisle(true);
            this.selectionRegion.getUserSettingsChase().setAllowSkipSlot(false);
        break;
        case SKIP_VALUE_1:
            this.selectionRegion.getUserSettingsChase().setAllowSkipAisle(false);
            this.selectionRegion.getUserSettingsChase().setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_2:
            this.selectionRegion.getUserSettingsChase().setAllowSkipAisle(true);
            this.selectionRegion.getUserSettingsChase().setAllowSkipSlot(true);
        break;
        case SKIP_VALUE_3:
            this.selectionRegion.getUserSettingsChase().setAllowSkipAisle(false);
            this.selectionRegion.getUserSettingsChase().setAllowSkipSlot(false);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the normalAllowGoBackForShorts property.
     * @return Integer - normarAllowGoBackForShorts
     */
    public Integer getNormalAllowGoBackForShorts() {
        if (this.selectionRegion != null) {
            if (selectionRegion.getUserSettingsNormal().getGoBackForShortsIndicator()
                == SelectionRegionGoBackForShorts.Never) {
                return 0;
            } else if (selectionRegion.getUserSettingsNormal().getGoBackForShortsIndicator()
                == SelectionRegionGoBackForShorts.Always) {
                return 1;
            } else {
                return 2;
            }
        }
        return 0;
    }


    /**
     * Setter for the normalAllowGoBackForShorts property.
     * @param normalAllowGoBackForShorts - the new normalAllowGoBackForShorts
     */
    public void setNormalAllowGoBackForShorts(Integer normalAllowGoBackForShorts) {
        switch (normalAllowGoBackForShorts) {
        case 0:
            this.selectionRegion.getUserSettingsNormal().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.Never);
        break;
        case 1:
            this.selectionRegion.getUserSettingsNormal().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.Always);
        break;
        case 2:
            this.selectionRegion.getUserSettingsNormal().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.IfReplenished);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the chaseAllowGoBackForShorts property.
     * @return Integer - return chaseAllowGoBackForShorts
     */
    public Integer getChaseAllowGoBackForShorts() {
        if (this.selectionRegion != null) {
            if (selectionRegion.getUserSettingsChase().getGoBackForShortsIndicator()
                == SelectionRegionGoBackForShorts.Never) {
                return 0;
            } else if (selectionRegion.getUserSettingsChase().getGoBackForShortsIndicator()
                == SelectionRegionGoBackForShorts.Always) {
                return 1;
            } else {
                return 2;
            }
        }
        return 0;
    }


    /**
     * Setter for the chaseAllowGoBackForShorts property.
     * @param chaseAllowGoBackForShorts - the new chaseAllowGoBackForShorts
     */
    public void setChaseAllowGoBackForShorts(Integer chaseAllowGoBackForShorts) {
        switch (chaseAllowGoBackForShorts) {
        case 0:
            this.selectionRegion.getUserSettingsChase().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.Never);
        break;
        case 1:
            this.selectionRegion.getUserSettingsChase().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.Always);
        break;
        case 2:
            this.selectionRegion.getUserSettingsChase().
            setGoBackForShortsIndicator(SelectionRegionGoBackForShorts.IfReplenished);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the normalSummaryPrompt property.
     * @return Long - return normalSummaryPrompt value
     */
    public Long getNormalSummaryPrompt() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsNormal().getSummaryPromptIndicator()
                == SelectionRegionSummaryPrompt.TaskDefaultPrompt) {
                return new Long(-1);
            } else if (this.selectionRegion.getUserSettingsNormal().getSummaryPromptIndicator()
                == SelectionRegionSummaryPrompt.SkipPrompt) {
                return new Long(-2);
            } else {
                return (this.selectionRegion.getUserSettingsNormal().getSummaryPrompt().getId());
            }
        }
        return new Long(0);
    }


    /**
     * Setter for the normalSummaryPrompt property.
     * @param normalSummaryPrompt - the new normalSummaryPromt
     */
    public void setNormalSummaryPrompt(Long normalSummaryPrompt) {
        if (normalSummaryPrompt.longValue() == -1) {
            this.selectionRegion.getUserSettingsNormal().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.TaskDefaultPrompt);
        } else if (normalSummaryPrompt.longValue() == -2) {
            this.selectionRegion.getUserSettingsNormal().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.SkipPrompt);
        } else {
            this.selectionRegion.getUserSettingsNormal().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.ConfiguredPrompt);
            this.normalSummaryPromptId = normalSummaryPrompt;
        }
    }


    /**
     * Getter for the chaseSummaryPrompt property.
     * @return Long - return chaseSummaryPrompt value
     */
    public Long getChaseSummaryPrompt() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsChase().getSummaryPromptIndicator()
                == SelectionRegionSummaryPrompt.TaskDefaultPrompt) {
                return new Long(-1);
            } else if (this.selectionRegion.getUserSettingsChase().getSummaryPromptIndicator()
                == SelectionRegionSummaryPrompt.SkipPrompt) {
                return new Long(-2);
            } else {
                return (this.selectionRegion.getUserSettingsChase().getSummaryPrompt().getId());
            }

        }
        return new Long(0);
    }


    /**
     * Setter for the chaseSummaryPrompt property.
     * @param chaseSummaryPrompt - the new chaseSummaryPrompt value
     */
    public void setChaseSummaryPrompt(Long chaseSummaryPrompt) {
        if (chaseSummaryPrompt.longValue() == -1) {
            this.selectionRegion.getUserSettingsChase().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.TaskDefaultPrompt);
        } else if (chaseSummaryPrompt.longValue() == -2) {
            this.selectionRegion.getUserSettingsChase().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.SkipPrompt);
        } else {
            this.selectionRegion.getUserSettingsChase().
            setSummaryPromptIndicator(SelectionRegionSummaryPrompt.ConfiguredPrompt);
            this.chaseSummaryPromptId = chaseSummaryPrompt;
        }
    }


    /**
     * Getter for the normalAutoMarkOutShorts property.
     * @return Integer - return normalAutoMarkOutShorts value
     */
    public Integer getNormalAutoMarkOutShorts() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsNormal().getAutoMarkOutShorts()
                == SelectionRegionAutoMarkOutShorts.Never) {
                return 1;
            } else {
                return 2;
            }
        }
        return 1;
    }


    /**
     * Setter for the normalAutoMarkOutShorts property.
     * @param normalAutoMarkOutShorts - the new normalAutoMarkOutShorts
     */
    public void setNormalAutoMarkOutShorts(Integer normalAutoMarkOutShorts) {
        switch (normalAutoMarkOutShorts) {
        case 1:
            this.selectionRegion.getUserSettingsNormal().
            setAutoMarkOutShorts(SelectionRegionAutoMarkOutShorts.Never);
        break;
        case 2:
            this.selectionRegion.getUserSettingsNormal().
            setAutoMarkOutShorts(SelectionRegionAutoMarkOutShorts.WhenNotReplenished);
        break;
        default:
        break;
        }
    }


    /**
     * Getter for the chaseAutoMarkOutShorts property.
     * @return Integer the index of the property
     */
    public Integer getChaseAutoMarkOutShorts() {
        if (this.selectionRegion != null) {
            if (this.selectionRegion.getUserSettingsChase().getAutoMarkOutShorts()
                == SelectionRegionAutoMarkOutShorts.Never) {
                return 1;
            } else {
                return 2;
            }
        }
        return 1;
    }


    /**
     * Setter for the chaseAutoMarkOutShorts property.
     * @param chaseAutoMarkOutShorts the select index
     */
    public void setChaseAutoMarkOutShorts(Integer chaseAutoMarkOutShorts) {
        switch (chaseAutoMarkOutShorts) {
        case 1:
            this.selectionRegion.getUserSettingsChase().
                setAutoMarkOutShorts(SelectionRegionAutoMarkOutShorts.Never);
        break;
        case 2:
            this.selectionRegion.getUserSettingsChase().
                setAutoMarkOutShorts(SelectionRegionAutoMarkOutShorts.WhenNotReplenished);
        break;
        default:
        break;
        }
    }

    /**
     * Sets all chase profile booleans to false.
     */
    private void resetChaseBools() {
        this.selectionRegion.getUserSettingsChase().setAllowRepickSkips(false);
        this.selectionRegion.getUserSettingsChase().setAllowReversePicking(false);
        this.selectionRegion.getUserSettingsChase().setAllowSignOffAtAnyPoint(false);
        this.selectionRegion.getUserSettingsChase().setRequireDeliveryConfirmation(false);
        this.selectionRegion.getUserSettingsChase().setSuppressQuantityOfOne(false);
        this.selectionRegion.getUserSettingsChase().setVerifyReplenishment(false);
    }

    /**
     * Sets all normal profile booleans to false.
     */
    private void resetNormalBools() {
        this.selectionRegion.getUserSettingsNormal().setAllowRepickSkips(false);
        this.selectionRegion.getUserSettingsNormal().setAllowReversePicking(false);
        this.selectionRegion.getUserSettingsNormal().setAllowSignOffAtAnyPoint(false);
        this.selectionRegion.getUserSettingsNormal().setRequireDeliveryConfirmation(false);
        this.selectionRegion.getUserSettingsNormal().setSuppressQuantityOfOne(false);
        this.selectionRegion.getUserSettingsNormal().setVerifyReplenishment(false);
    }

    /**
     *  Validate the selection region.
     */
    public void validate() {
        log.info("Validator called");
        Double threshHold =
            this.selectionRegion.getUserSettingsNormal().getBaseWeightThreshold();
        if (threshHold != null) {
            this.selectionRegion.getUserSettingsNormal().setBaseWeightThreshold(threshHold);
        }
        log.info(this.selectionRegion.getUserSettingsNormal().getBaseQuantityThreshold());
        log.info(this.selectionRegion.getUserSettingsNormal().getBaseWeightThreshold());
        log.info(this.selectionRegion.getUserSettingsNormal().getAutoMarkOutShorts());
        log.info(this.selectionRegion.getUserSettingsNormal().getSummaryPromptIndicator());
    }

    /**
     * Deletes the currently viewed region.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current region
     */
    public String deleteCurrentRegion() throws Exception {

        SelectionRegion regionToDelete = null;

        try {
            regionToDelete = selectionRegionManager.get(getRegionId());
            selectionRegionManager.delete(getRegionId());

            addSessionActionMessage(new UserMessage(
                "selectionRegion.delete.message.success", regionToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete region: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Assigns summary prompts to selected regions.
     *
     * @return the control flow target name.
     * @throws DataAccessException when selection region cannot be retrieved
     * @throws BusinessRuleException when business exception occurs
     */
    public String assignSummaryPrompt() throws DataAccessException, BusinessRuleException {
        Long[] ids = getIds();

        if (this.chaseSummaryPromptId != null) {
           for (Long id : ids) {
              this.selectionRegion = this.getSelectionRegionManager().get(id);
              if (chaseSummaryPromptId.equals(new Long(-1))) {
                   this.selectionRegion.getUserSettingsChase().setSummaryPrompt(null);
                   this.selectionRegion.getUserSettingsChase().
                   setSummaryPromptIndicator(SelectionRegionSummaryPrompt.TaskDefaultPrompt);
              } else if (chaseSummaryPromptId.equals(new Long(-2))) {
                   this.selectionRegion.getUserSettingsChase().setSummaryPrompt(null);
                   this.selectionRegion.getUserSettingsChase().
                   setSummaryPromptIndicator(SelectionRegionSummaryPrompt.SkipPrompt);
              } else {
                   this.selectionRegion.getUserSettingsChase().
                   setSummaryPromptIndicator(SelectionRegionSummaryPrompt.ConfiguredPrompt);
                   SummaryPrompt summaryPrompt = this.summaryPromptManager.get(chaseSummaryPromptId);
                   this.selectionRegion.getUserSettingsChase().setSummaryPrompt(summaryPrompt);
              }
                this.selectionRegionManager.save(selectionRegion);
           }
        }

        if (this.normalSummaryPromptId != null) {
            for (Long id : ids) {
                this.selectionRegion = this.getSelectionRegionManager().get(id);
                if (normalSummaryPromptId.equals(new Long(-1))) {
                     this.selectionRegion.getUserSettingsNormal().setSummaryPrompt(null);
                     this.selectionRegion.getUserSettingsNormal().
                     setSummaryPromptIndicator(SelectionRegionSummaryPrompt.TaskDefaultPrompt);
                } else if (normalSummaryPromptId.equals(new Long(-2))) {
                     this.selectionRegion.getUserSettingsNormal().setSummaryPrompt(null);
                     this.selectionRegion.getUserSettingsNormal().
                     setSummaryPromptIndicator(SelectionRegionSummaryPrompt.SkipPrompt);
                } else {
                     this.selectionRegion.getUserSettingsNormal().
                     setSummaryPromptIndicator(SelectionRegionSummaryPrompt.ConfiguredPrompt);
                     SummaryPrompt summaryPrompt = this.summaryPromptManager.get(normalSummaryPromptId);
                     this.selectionRegion.getUserSettingsNormal().setSummaryPrompt(summaryPrompt);
                }
                  this.selectionRegionManager.save(selectionRegion);
             }
       }

        setJsonMessage(getText(
            new UserMessage(
                "selectionRegion.assign.summaryPrompt.success.multiple",
                ids.length,
                chaseSummaryPromptId)),
            ERROR_SUCCESS);

        return SUCCESS;
    }


    /**
     * Getter for the summaryPromptManager property.
     * @return SummaryPromptManager the manager for the summary prompt
     */
    public SummaryPromptManager getSummaryPromptManager() {
        return summaryPromptManager;
    }


    /**
     * Setter for the summaryPromptManager property.
     * @param summaryPromptManager - the manager for summary prompts
     */
    public void setSummaryPromptManager(SummaryPromptManager summaryPromptManager) {
        this.summaryPromptManager = summaryPromptManager;
    }


    /**
     * Getter for the summaryPrompts property.
     * @return List a list of available summary prompts
     */
    public List<SummaryPrompt> getSummaryPrompts() {
        return summaryPrompts;
    }


    /**
     * Setter for the summaryPrompts property.
     * @param summaryPrompts - a list of SummaryPrompts
     */
    public void setSummaryPrompts(List<SummaryPrompt> summaryPrompts) {
        this.summaryPrompts = summaryPrompts;
    }


    /**
     * Getter for the normalSummaryPromptId property.
     * @return Long id of the normal summary prompt
     */
    public Long getNormalSummaryPromptId() {
        return normalSummaryPromptId;
    }


    /**
     * Setter for the normalSummaryPromptId property.
     * @param normalSummaryPromptId - the id for the normal summary prompt
     */
    public void setNormalSummaryPromptId(Long normalSummaryPromptId) {
        this.normalSummaryPromptId = normalSummaryPromptId;
    }


    /**
     * Getter for the chaseSummaryPromptId property.
     * @return Long id of the chase summary prompt
     */
    public Long getChaseSummaryPromptId() {
        return chaseSummaryPromptId;
    }


    /**
     * Setter for the chaseSummaryPromptId property.
     * @param chaseSummaryPromptId - the id for the chase summary prompt
     */
    public void setChaseSummaryPromptId(Long chaseSummaryPromptId) {
        this.chaseSummaryPromptId = chaseSummaryPromptId;
    }


    /**
     * Getter for the formName property.
     * @return String value of the property
     */
    public String getFormName() {
        return formName;
    }


    /**
     * Setter for the formName property.
     * @param formName the new formName value
     */
    public void setFormName(String formName) {
        this.formName = formName;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getStaticViewId()
     */
    @Override
    protected long getStaticViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.RegionActionRoot#getRegion()
     */
    @Override
    protected Region getRegion() {
        return getSelectionRegion();
    }

    /**
     * Overriden to take care disabling site on view page.
     * @return whether or not this is a text action.
     */
    public boolean isTextAction() {
        //Note - this is used to disable the site on view page.
        if (ActionContext.getContext().getName().equals("viewRegion")) {
            return true;
        } else {
            return super.isTextAction();
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 