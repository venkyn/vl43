/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.selection.service.ContainerManager;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on <code>Container</code>
 * objects.
 *
 * @author ddoubleday
 */
public class ContainerActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

    private Long assignmentId = null;

    // The ID of the view we need to grab from the DB.
    private static final long CONTAINER_VIEW_ID = -1014;
    private static final long PICK_DETAIL_VIEW_ID = -1015;

    // The list of columns for the containers table.
    private List<Column> containerColumns;

    // The list of columns for the picks table.
    private List<Column> pickDetailColumns;

    // The Container management service.
    private ContainerManager containerManager = null;

    // The Pick Detail management service.
    private PickDetailManager pickDetailManager = null;

    /**
     * @return the assignment ID
     */
    public Long getAssignmentId() {
        return assignmentId;
    }


    /**
     * @param assignmentId the new ID.
     */
    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    /**
     * Getter for the containerManager property.
     * @return UserManager value of the property
     */
    public ContainerManager getContainerManager() {
        return this.containerManager;
    }

    /**
     * Getter for the pickDetailManager property.
     * @return PickDetailManager value of the property
     */
    public PickDetailManager getPickManager() {
        return this.pickDetailManager;
    }

    /**
     * Setter for the containerManager property.
     * @param manager the new containerManager value
     */
    public void setContainerManager(ContainerManager manager) {
        this.containerManager = manager;
    }

    /**
     * Setter for the pickDetailManager property.
     * @param manager the new pickDetailManager value
     */
    public void setPickDetailManager(PickDetailManager manager) {
        this.pickDetailManager = manager;
    }

    /**
     * Getter for the containerColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getContainerColumns() {
        return this.containerColumns;
    }

    /**
     * Getter for the pickDetailColumns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPickDetailColumns() {
        return this.pickDetailColumns;
    }

    /**
     * Action for the containers view page. Initializes the containers table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View containerView = getUserPreferencesManager().getView(CONTAINER_VIEW_ID);
        this.containerColumns = this.getUserPreferencesManager().getColumns(containerView, getCurrentUser());
        View pickDetailView = getUserPreferencesManager().getView(PICK_DETAIL_VIEW_ID);
        this.pickDetailColumns = this.getUserPreferencesManager().getColumns(pickDetailView, getCurrentUser());
        Filter filter = getFilterManager().findByUserIdAndViewId(getCurrentUser().getId(), pickDetailView.getId());
        if (filter != null) {
            getFilterManager().delete(filter);
        }
        List<Filter> filters = getFilters();
        List<Filter> newFilters = new ArrayList<Filter>();
        if (filters != null) {
            for (Filter f : filters) {
                if (f != null) {
                    if (!f.getView().getId().equals(pickDetailView.getId())) {
                        newFilters.add(f);
                    }
                }
            }
            setFilters(newFilters);
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it
     * from the database when a containerId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getContainerManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getAssignmentId() != null) {
            super.getTableData(null, new String[]{"" + getAssignmentId()});
        } else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Container";
    }

    /**
     * Getter for the CONTAINER_VIEW_ID property.
     * @return long value of the property
     */
    public static long getContainerViewId() {
        return CONTAINER_VIEW_ID;
    }

    /**
     * Getter for the PICKDETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPickDetailViewId() {
        return PICK_DETAIL_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CONTAINER_VIEW_ID);
        viewIds.add(PICK_DETAIL_VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 