/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.selection.service.ContainerPickDetailManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;


/**
 *
 *
 * @author bnichols
 */
public class ContainerPickDetailActionRoot extends DataProviderAction implements Preparable {


    //
    private static final long serialVersionUID = 6501267566202627089L;

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1014;

    // The list of columns for the picks table.
    private List<Column> columns;

    // The container pick detail management service.
    private ContainerPickDetailManager containerPickDetailManager = null;

    // A comma separated String of IDs to query on
    private String containerID = null;

    /**
     * Getter for the containerPickDetailManager property.
     * @return ContainerPickDetailManager value of the property
     */
    public ContainerPickDetailManager getContainerPickDetailManager() {
        return containerPickDetailManager;
    }


    /**
     *
     * @param containerPickDetailManager
     */
    /**
     * Setter for the containerPickDetailManager property.
     * @param containerPickDetailManager the new containerPickDetailManager value
     */
    public void setContainerPickDetailManager(ContainerPickDetailManager containerPickDetailManager) {
        this.containerPickDetailManager = containerPickDetailManager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getContainerColumns() {
        return this.columns;
    }

    /**
     * Action for the pick details view page. Initializes the pick details table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View pickDetailsView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(pickDetailsView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getContainerPickDetailManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "ContainerPickDetail";
    }


    /**
     * Getter for the containerID property.
     * @return the containerID
     */
    public String getContainerID() {
        return containerID;
    }

    /**
     * Setter for the containerID property.
     * @param containerID  the new containerID
     */
    public void setContainerID(String containerID) {
        this.containerID = containerID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getContainerID() != null && getContainerID().length() > 0) {
            super.getTableData("obj.container.id in ( " + getContainerID() + " )");
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getPrintData() throws Exception {
        if (getContainerID() != null && getContainerID().length() > 0) {
            super.getPrintData("obj.container.id in ( " + getContainerID() + " )");
        } else {
            setMessage(BLANK_RESPONSE);
            return SUCCESS;
        }
        return SUCCESS;
    }


    /**
     * This method sets up the <code>User</code> object by retrieving it
     * from the database when a userId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * Getter for the PICKDETAIL_VIEW_ID property.
     * @return long value of the property
     */
    public static long getPickDetail_View_Id() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 