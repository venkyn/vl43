/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.errors.MessageMap;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderActionRoot;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;


/**
 * Action class for Assigning assignment Priorities.
 *
 */
public abstract class AssignmentPrioritzeActionRoot extends DataProviderActionRoot
implements Preparable {

    /**
     * Class used for mapping properties.
     *
     */
    public class PriorityMapping {
        public String applyTo;
        public Integer priority;
    }

    //
    private static final long serialVersionUID = 2053620991353048111L;

    private static Logger log = Logger.getLogger(AssignmentPrioritzeActionRoot.class);

    private AssignmentManager assignmentManager;

    //Value for how mapping is to be applied customer(0)/Route(1)
    private Integer howToApply = 0;

    //value for assignment priorities
    private int numberOfMappings = 0;

    //list of mappings
    private List<PriorityMapping> mappings = new ArrayList<PriorityMapping>();

    private Map<String, String> applyList0;

    private Map<String, String> applyList1;

    private String startDate = createDateString();

    private String endDate = createDateString();


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseActionRoot#cancel()
     */
    @Override
    public String cancel() {
        setSessionActionMessages(new MessageMap());
        return super.cancel();
    }

    /**
     * Getter for the mappings property.
     * @return List&lt;PriorityMapping&gt; value of the property
     */
    public List<PriorityMapping> getMappings() {
        return mappings;
    }

    /**
     * Setter for the mappings property.
     * @param mappings the new mappings value
     */
    public void setMappings(List<PriorityMapping> mappings) {
        this.mappings = mappings;
    }

    /**
     * Getter for the endDate property.
     * @return String value of the property
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * Setter for the endDate property.
     * @param endDate the new endDate value
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter for the startDate property.
     * @return String value of the property
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Setter for the startDate property.
     * @param startDate the new startDate value
     */
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the availablePriorities property.
     * @return Map&lt;Integer,String&gt; value of the property
     */
    public Map<Integer, String> getAvailablePriorities() {
        return this.assignmentManager.getAvailablePriorities();
    }

    /**
     * Getter for the defaultPriority property.
     * @return Integer value of the property
     */
    public Integer getDefaultPriority() {
        return this.assignmentManager.getDefaultPriority();
    }



    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Getter for the applyList0 property.
     * @return Map&lt;String,String&gt; value of the property
     */
    public Map<String, String> getApplyList0() {
        return applyList0;
    }

    /**
     * Setter for the applyList0 property.
     * @param applyList0 the new applyList0 value
     */
    public void setApplyList0(Map<String, String> applyList0) {
        this.applyList0 = applyList0;
    }

    /**
     * Getter for the applyList1 property.
     * @return Map&lt;String,String&gt; value of the property
     */
    public Map<String, String> getApplyList1() {
        return applyList1;
    }

    /**
     * Setter for the applyList1 property.
     * @param applyList1 the new applyList1 value
     */
    public void setApplyList1(Map<String, String> applyList1) {
        this.applyList1 = applyList1;
    }

    /**
     * Getter for the howToApply property.
     * @return Integer value of the property
     */
    public Integer getHowToApply() {
        return howToApply;
    }
    /**
     * Setter for the howToApply property.
     * @param howToApply the new howToApply value
     */
    public void setHowToApply(Integer howToApply) {
        this.howToApply = howToApply;
    }

    /**
     * Getter for the numberOfMappings property.
     * @return int value of the property
     */
    public int getNumberOfMappings() {
        return numberOfMappings;
    }

    /**
     * Setter for the numberOfMappings property.
     * @param numberOfMappings the new numberOfMappings value
     */
    public void setNumberOfMappings(int numberOfMappings) {
        this.numberOfMappings = numberOfMappings;
    }





    /**
     * return a map of possible apply by settings for UI drop down control.
     *
     * @return - map of values
     */
    public Map<Integer, String> getApplyBy() {
        Map<Integer, String> myMap = new LinkedHashMap<Integer, String>();
        myMap.put(0, getText("assignment.label.edit.customernumber"));
        myMap.put(1, getText("assignment.label.edit.route"));
        return myMap;
    }

    /**
     * return a map of possible choices for first apply by value (customer).
     *
     * @return - map of values for first apply by option (customers)
     */

     public Map<String, String> getApplyValue0() {
        if (applyList0 == null) {
            applyList0 = new LinkedHashMap<String, String>();
            List<String> customers = getAssignmentManager().listCustomerNumbers();
            applyList0.put("", getText("assignment.prioritize.label.selectOne"));
            for (String customer : customers) {
                 if (customer.trim().equalsIgnoreCase("multiple")) {
                    String customerMultiple = getText("assignment.prioritize.customerNumber.multiple");
                    applyList0.put(customerMultiple, customerMultiple);
                } else {
                applyList0.put(customer, customer);
                }
            }
        }
        return applyList0;
    }

    /**
     * return a map of possible choices for first apply by value (route).
     *
     * @return - map of values for first apply by option (route)
     */
    public Map<String, String> getApplyValue1() {
        if (applyList1 == null) {
            applyList1 = new LinkedHashMap<String, String>();
            List<String> routes = getAssignmentManager().listRoutes();
            applyList1.put("", getText("assignment.prioritize.label.selectOne"));
            for (String route : routes) {
                if (route.trim().equalsIgnoreCase("multiple")) {
                    String routeMultiple = getText("assignment.prioritize.route.multiple");
                    applyList1.put(routeMultiple, routeMultiple);
                } else {
                applyList1.put(route, route);
                }
            }
        }
        return applyList1;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        //nothing to do now.
    }

    /**
     * apply priorities entered.
     *
     * @return - success or failure
     * @throws Exception - any exceptions
     */
    public String save() throws Exception {
        Date sDate = getDate(startDate);
        Date eDate = getDate(endDate);
        int errorCount = 0;
        int recordsAffected = 0;
        String returnValue = SUCCESS;
        int processed = getNumberOfMappings();
        List<UserMessage> details = new ArrayList<UserMessage>();

        setSessionActionMessages(new MessageMap());

        //loop through mappings and removed when processed successfully.
        for (int i = 0; i < getNumberOfMappings(); i++) {
            try {

                //process mapping
                PriorityMapping mapping = getMappings().get(errorCount);
                recordsAffected = getAssignmentManager().executeUpdatePriority(
                    getHowToApply(), mapping.applyTo,
                    mapping.priority, sDate, eDate);

                //check number of assignments updated, if 0 then treat as error
                if (recordsAffected > 0) {
                    getMappings().remove(errorCount);
                    details.add(new UserMessage(
                        getText("assignment.prioritize.message.success"), recordsAffected, mapping.applyTo));
                } else {
                    addFieldError(
                        "priorityError[" + errorCount + "]",
                        new UserMessage(
                            "assignment.prioritize.message.error.norecords"));
                    errorCount++;
                }

            } catch (Throwable t) {
                //if any error occurs catch it and flag as error
                addFieldError(
                    "priorityError[" + errorCount + "]",
                    new UserMessage(
                        "assignment.prioritize.message.error.update"));
                errorCount++;
                log.error("Error updating assignment priorities", t);
            }

        }

        //if any errors occured than return to input view with displayed message
        if (errorCount > 0) {
            processed = getNumberOfMappings() - errorCount;
            setNumberOfMappings(getMappings().size());
            returnValue = INPUT;
            if (processed == 1) {
                addPartialSuccessSessionActionMessage(new UserMessage(
                    getText("assignment.prioritize.message.single.success"),
                    ERROR_SUCCESS), details);
            //display multiple success message
            } else if (processed > 1) {
                addPartialSuccessSessionActionMessage(new UserMessage(
                    getText("assignment.prioritize.message.multiple.success"),
                    processed, ERROR_SUCCESS), details);
            }
        } else {
            //display single success message
            if (processed == 1) {
                addSessionActionMessage(new UserMessage(
                    getText("assignment.prioritize.message.single.success"),
                    ERROR_SUCCESS));
            //display multiple success message
            } else if (processed > 1) {
                addSessionActionMessage(new UserMessage(
                    getText("assignment.prioritize.message.multiple.success"),
                    processed, ERROR_SUCCESS));
            }
        }

        return returnValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        return viewIds;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getAssignmentManager();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Assignment";
    }

    /**
     * Validating here rather than in XML file because OpenSymphony seems to
     * have taken away the "collection" validator from the validation framework.
     * Rather than write a new validator (with Dennis Doubleday's advice) I
     * decided to validate here since this is such a unique case.
     *
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#validate()
     */
    @Override
    public void validate() {
        validateBlankEntry();
        validateDuplicates();
        validateDates();
    }

    /**
     * Check priority mappings for blank entries.
     */
    protected void validateBlankEntry() {
        int count = 0;

        for (PriorityMapping mapping : getMappings()) {
            if (StringUtil.isNullOrEmpty(mapping.applyTo)) {
                if (getHowToApply() == 0) {
                    addFieldError(
                        "priorityError[" + count + "]",
                        new UserMessage("assignment.prioritize.message.error.customer.applyToNULL"));
                } else if (getHowToApply() == 1) {
                    addFieldError(
                        "priorityError[" + count + "]",
                        new UserMessage("assignment.prioritize.message.error.route.applyToNULL"));
                }
            }
            count++;
        }
    }

    /**
     * Check for duplicate entires.
     */
    protected void validateDuplicates() {
        Map<String, Integer> applyTos = new HashMap<String, Integer>();
        Integer applyCount;
        int count = 0;

        //first pass, get count of each spplyTo value that is not blank
        for (PriorityMapping mapping : getMappings()) {
            if (!StringUtil.isNullOrEmpty(mapping.applyTo)) {
                applyCount = applyTos.get(mapping.applyTo);
                if (applyCount == null) {
                    applyCount = 1;
                } else {
                    applyCount++;
                }
                applyTos.put(mapping.applyTo, applyCount);
            }
        }

        //Pass through again to check if count is more than 1. if so mark as dup.
        for (PriorityMapping mapping : getMappings()) {
            applyCount = applyTos.get(mapping.applyTo);
            if (applyCount != null && applyCount > 1) {
                if (getHowToApply() == 0) {
                    addFieldError(
                        "priorityError[" + count + "]",
                        new UserMessage("assignment.prioritize.message.error.customer.duplicate"));
                } else if (getHowToApply() == 1) {
                    addFieldError(
                        "priorityError[" + count + "]",
                        new UserMessage("assignment.prioritize.message.error.route.duplicate"));
                }
            }
            count++;
        }
    }

    /**
     * Validate date range entered.
     */
    protected void validateDates() {
        Date start = getDate(startDate);
        Date end = getDate(endDate);

        if (start.after(end)) {
            addFieldError(
                "dateError",
                new UserMessage("assignment.prioritize.message.error.startDate"));
        }
    }

    /**
     * convert UI date string (YYYY-MM-DD) to a date object.
     *
     * @param date - string date
     * @return - date object
     */
    private Date getDate(String date) {
        Calendar now = Calendar.getInstance();
        String[] tokens = date.split("-");
        now.set(Integer.parseInt(tokens[0]),
                Integer.parseInt(tokens[1]) - 1,
                Integer.parseInt(tokens[2]));

        return now.getTime();
    }

    /**
     * Create standard UI date string (YYYY-MM-DD).
     *
     * @return - current date as string.
     */
    protected String createDateString() {
        Calendar now = Calendar.getInstance();
        Integer year = now.get(Calendar.YEAR);
        Integer month = now.get(Calendar.MONTH) + 1;
        Integer day = now.get(Calendar.DAY_OF_MONTH);
        return year.toString() + "-" + month.toString() + "-" + day.toString();

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 