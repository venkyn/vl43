/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.opensymphony.xwork2.ActionContext;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.web.core.action.LaborAction;

import java.util.List;

/**
 * This class handles all selection labor actions from the user interface.
 * @author brupert
 */
public class SelectionLaborActionRoot extends LaborAction {

    private static final long serialVersionUID = -2009624499142929258L;

    private static final Long ASSIGNMENT_LABOR_VIEW_ID = -1021L;

    private List<Column> assignmentLaborColumns;

    private AssignmentLaborManager assignmentLaborManager;

    // Request parameter for assignment labor screen.
    private String operatorLaborId;

    /**
     * Overriden to take care of Assignment labor section
     * @return whether or not this is a text action.
     */
    @Override
    public boolean isTextAction() {
        if (ActionContext.getContext().getName().toLowerCase().endsWith("assignmentlist".toLowerCase())) {
            return true;
        } else {
            return super.isTextAction();
        }
    }
    
    /**
     * Action method for summary data for selection only.
     * @return Action control flow target name.
     * @throws Exception On error.
     */
    public String getSummaryData() throws Exception {
        return this.getTableData(null, new Object[]{ OperatorLaborFilterType.Selection, RegionType.Selection  });
    }

    /**
     * Action method for summary print data for selection only.
     * @return Action control flow target name.
     * @throws Exception On error.
     */
    public String getSummaryPrintData() throws Exception {
        return this.getPrintData(null, new Object[]{ OperatorLaborFilterType.Selection, RegionType.Selection  });
    }


    /**
     * Populates the columns for the assignment labor table.
     * @return The Struts target name.
     * @throws DataAccessException On database error.
     */
    public String getAssignmentList() throws DataAccessException {
        View view = getUserPreferencesManager().getView(ASSIGNMENT_LABOR_VIEW_ID);
        this.assignmentLaborColumns = getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getAssignmentData() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getTableData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getAssignmentDataForPrint() throws Exception {
        if (getOperatorLaborId() != null) {
            super.getPrintData("obj.operatorLabor.id in ( " + getOperatorLaborId() + " )");
        }  else {
            super.getTableData();
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public String list() throws Exception {
        super.list();
        if (getFilterByFunction()) {
            overrideFunctionFilter(OperatorLaborFilterType.Selection);
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     */
    public List<Long> getViewIds() {
        List<Long> viewIds = super.getViewIds();
        viewIds.add(ASSIGNMENT_LABOR_VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for view Id.
     * @return The ID of the assignment labor view.
     */
    public Long getAssignmentLaborViewId() {
        return ASSIGNMENT_LABOR_VIEW_ID;
    }

    /**
     * Gets the columns for the assignment labor screen.
     * @return List of assignment labor columns.
     */
    public List<Column> getAssignmentLaborColumns() {
        return assignmentLaborColumns;
    }

    /**
     * Sets the columns for the assignment labor screen.
     * @param assignmentLaborColumns The columns for the assignment labor
     * screen to display.
     */
    public void setAssignmentLaborColumns(List<Column> assignmentLaborColumns) {
        this.assignmentLaborColumns = assignmentLaborColumns;
    }

    /**
     * Gets the assignmentLaborManager property.
     * @return The assignmentLaborManager property.
     */
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }

    /**
     * Sets the assignmentLaborManager property.
     * @param assignmentLaborManager The manager to assign.
     */
    public void setAssignmentLaborManager(
            AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }

    /**
     * Gets the request parameter value for operatorLaborId.
     * @return Value of the operatorLaborId request param.
     */
    public String getOperatorLaborId() {
        return operatorLaborId;
    }

    /**
     * Sets the request parameter value for operatorLaborId.
     * @param operatorLaborId The value to set.
     */
    public void setOperatorLaborId(String operatorLaborId) {
        this.operatorLaborId = operatorLaborId;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 