/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.PickManager;
import com.vocollect.voicelink.web.core.action.ResequenceAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;
import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the Struts action class that handles operations on
 * <code>Assignment</code> objects.
 *
 * @author asuri
 */
public class AssignmentActionRoot extends ResequenceAction implements Preparable {

    private static final long serialVersionUID = -2195964850116905955L;

    private static final Logger log = new Logger(AssignmentAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long ASSIGNMENT_VIEW_ID = -1009;

    private static final long PICK_VIEW_ID = -1010;

    private static final long RESEQUENCE_ASSIGNMENT_VIEW_ID = -1032;
    
    public static final String DATE_FORMAT = "yyyy-M-d-H-m-s";
    
    // The list of columns for the assignments table.
    private List<Column> assignmentColumns;

    // The list of columns for the picks table.
    private List<Column> pickColumns;

    // The Assignment management service.
    private AssignmentManager assignmentManager = null;

    // The Pick management service.
    private PickManager pickManager = null;

    // The Assignment object, which will either be newly created, or retrieved
    // via the AssignmentId.
    private Assignment assignment;

    // The Pick object, which will either be newly created, or retrieved
    // via the AssignmentId.
    private Pick pick;

    // The ID of the Assignment.
    private Long assignmentId;

    // Operator Manager.
    private OperatorManager operatorManager = null;

    // Delivery Location Entered by user for assignment
    private String assignmentDeliveryLocation;

    // The ids of delivery location mappings that will be modified.
    private Collection<String> assignmentIds;

    //assignment status
    private Integer assignmentStatus;
    
    //route and delivery date map
    private Map<String, String> routeDeliveryDateMap;
    
    //route and deliverydate separated by @ symbol
    private String routeAtDeliveryDate;
    
    private String routeDepartureDate;

    // Loading route manager used for updating route with new departure date
    private LoadingRouteManager loadingRouteManager = null;

    // Loading route used for updating route with new departure date
    private LoadingRoute loadingRoute;

    /**
     * Getter for the routeDepartureDate property.
     * @return String value of the property
     */
    public String getRouteDepartureDate() {
        return routeDepartureDate;
    }

    
    /**
     * Setter for the routeDepartureDate property.
     * @param routeDepartureDate the new routeDepartureDate value
     */
    public void setRouteDepartureDate(String routeDepartureDate) {
        this.routeDepartureDate = routeDepartureDate;
    }

    /**
     * Getter for the assignment property.
     *
     * @return Assignment value of the property
     */
    public Assignment getAssignment() {
        return this.assignment;
    }

    /**
     * Getter for the pick property.
     *
     * @return Pick value of the property
     */
    public Pick getPick() {
        return this.pick;
    }

    /**
     * Setter for the assignment property.
     *
     * @param assignment the new assignment value
     */
    public void setAssignment(Assignment assignment) {
        this.assignment = assignment;
    }

    /**
     * Setter for the pick property.
     *
     * @param pick the new pick value
     */
    public void setPick(Pick pick) {
        this.pick = pick;
    }

    /**
     * Getter for the assignmentId property.
     *
     * @return Long value of the property
     */
    public Long getAssignmentId() {
        return this.assignmentId;
    }

    /**
     * Setter for the assignmentId property.
     *
     * @param assignmentId the new assignmentId value
     */
    public void setAssignmentId(Long assignmentId) {
        this.assignmentId = assignmentId;
    }

    /**
     * Getter for the assignmentManager property.
     *
     * @return UserManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return this.assignmentManager;
    }

    /**
     * Getter for the userManager property.
     *
     * @return UserManager value of the property
     */
    public PickManager getPickManager() {
        return this.pickManager;
    }

    /**
     * Setter for the assignmentManager property.
     *
     * @param manager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager manager) {
        this.assignmentManager = manager;
    }

    /**
     * Setter for the pickManager property.
     *
     * @param manager the new pickManager value
     */
    public void setPickManager(PickManager manager) {
        this.pickManager = manager;
    }

    /**
     * Getter for the assignmentColumns property.
     *
     * @return List of Column value of the property.
     */
    public List<Column> getAssignmentColumns() {
        return this.assignmentColumns;
    }

    /**
     * Getter for the pickColumns property.
     *
     * @return List of Column value of the property.
     */
    public List<Column> getPickColumns() {
        return this.pickColumns;
    }

    /**
     *
     * @return Returns Operator Manager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Sets Work Group Manager.
     *
     * @param manager Instance of Work Group Manager class
     */
    public void setOperatorManager(OperatorManager manager) {
        this.operatorManager = manager;
    }

    /**
     * Getter for the availablePriorities property.
     * @return Map&lt;Integer,String&gt; value of the property
     */
    public Map<Integer, String> getAvailablePriorities() {
        return this.assignmentManager.getAvailablePriorities();
    }

    /**
     * Action for the assignments view page. Initializes the assignments table
     * columns.
     *
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View assignmentView = getUserPreferencesManager()
            .getView(ASSIGNMENT_VIEW_ID);
        this.assignmentColumns = this.getUserPreferencesManager().getColumns(
            assignmentView, getCurrentUser());
        View pickView = getUserPreferencesManager().getView(PICK_VIEW_ID);
        this.pickColumns = this.getUserPreferencesManager().getColumns(
            pickView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the user specified by the <code>user</code> member of
     * this class.
     *
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = this.assignment.isNew();
        try {
            // Need to save this once to attach it back to the session
            // before changing status
            getAssignmentManager().getPrimaryDAO().reattach(this.assignment);

            //Validate is assignment can be edited and has not been reserved
            validateIfEditable(this.assignment);

            if (assignmentStatus != null) {
                assignment.changeStatus(AssignmentStatus.toEnum(assignmentStatus.intValue()));
            }
            getAssignmentManager().executeAssignmentChangeSave(this.assignment);
            cleanSession();
        } catch (BusinessRuleException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            log.error(
                "Error while editing assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            log.warn("Assignment Number " + this.assignment.getNumber()
                + " not found, possibly a concurrency");

            addSessionActionErrorMessage(new UserMessage(
                "assignment.edit.error.assignmentNotFound", null, null));
            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified assignment "
                + this.assignment.getNumber());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.Assignment"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Assignment modifiedAssignment = getAssignmentManager().get(
                    getAssignmentId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.assignment.setVersion(modifiedAssignment.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedAssignment);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", this.assignment
                            .getNumber()));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage(
            "assignment." + (isNew ? "create" : "edit") + ".message.success",
             String.valueOf(this.assignment.getViewableAssignmentNumber())));

        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getAssignmentManager();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "Assignment";
    }

    /**
     * Getter for Assignment Status.
     * @return The name property of the assignment's status.
     */
    public Integer getAssignmentStatus() {
        return this.assignment.getStatus().getValue();
    }

    /**
     * Calls changeStatus in model class to set Assignment Status.
     * TODO: Bug here, business rule exception caught and logged instead
     * of handled. --ddoubleday
     * @param status the String representing the status to change to.
     */
    public void setAssignmentStatus(Integer status) {
              assignmentStatus = status;

    }


    /**
     * Setter for Operator ID used by Edit Assignment functionality.
     * @param operatorId The operator ID.
     */
    public void setOperatorID(Long operatorId) {
        try {
            if (operatorId == 0 || operatorId == null) {
                this.assignment.setOperator(null);
            } else {
                Operator operator = this.operatorManager.get(operatorId);
                this.assignment.setOperator(operator);
            }

        } catch (DataAccessException e) {
            log.warn("Operator not found in database", e);
        }
    }

    /**
     * This method sets up the <code>user</code> object by retrieving it from
     * the database when a assignmentId is set by the form submission.
     * {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.assignmentId != null) {

            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                this.assignment = (Assignment) getEntityFromSession(getSavedEntityKey());
            } else {
                this.assignment = this.assignmentManager.get(this.assignmentId);
                saveEntityInSession(this.assignment);
            }

        }
        if (log.isDebugEnabled()) {
            log.debug("Assignment version is: " + this.assignment.getVersion());
        }

    }

    /**
     * Method for grouping Assignments from URL.
     *
     * @return the control flow target of this action method.
     * @throws DataAccessException on database error.
     */
    public String groupAssignments() throws DataAccessException {
        ArrayList<Assignment> assignmentsToGroup = new ArrayList<Assignment>(getIds().length);
        for (Long id : getIds()) {
            assignmentsToGroup.add(assignmentManager.get(id));
        }
        try {
            getAssignmentManager().executeGroupAssignments(
                assignmentsToGroup, true);
        } catch (DataAccessException e) {
            log.error(
                "Error while grouping assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while creating grouping assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }
        setJsonMessage(getText(new UserMessage(
            "assignment.group.message.success",
            assignmentsToGroup.size())),
            ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Method for ungrouping Assignments from URL.
     *
     * @return String defining next target to forward to.
     * @throws DataAccessException on any database failure.
     */
    public String ungroupAssignments() throws DataAccessException {
        ArrayList<Assignment> assignmentsToUngroup = new ArrayList<Assignment>(getIds().length);
        // Round up all the selected assignemnts.
        for (Long id : getIds()) {
            assignmentsToUngroup.add(assignmentManager.get(id));
        }
        try {
            getAssignmentManager().executeUngroupAssignments(
                assignmentsToUngroup, true);
        } catch (DataAccessException e) {
            log.error(
                "Error while ungrouping assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while ungrouping assignments",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }
        setJsonMessage(getText(new UserMessage(
            "assignment.ungroup.message.success",
            assignmentsToUngroup.size())), ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Function for validating if an assignment is editable or not.
     *
     * @return String defining next target to forward to.
     */
    public String checkIfEditable() {

        // Round up all the selected assignemnts.
        try {
            Assignment a = assignmentManager.get(getIds()[0]);
            validateIfEditable(a);
        } catch (DataAccessException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            log.warn("Error while validating assignment for editing", e);
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            return SUCCESS;
        }
        setJsonMessage("edit!input.action?assignmentId=" + getIds()[0], ERROR_REDIRECT);
        return SUCCESS;
    }

    /**
     * Function to check for editing validation business logic.
     *
     * @param a Assignment to be validated
     * @throws BusinessRuleException if the business rule is violated.
     */
    protected void validateIfEditable(Assignment a)
        throws BusinessRuleException {
        if (a.getStatus().isInSet(AssignmentStatus.Canceled,
                AssignmentStatus.InProgress, AssignmentStatus.Complete)
                || a.getReservedBy() != null) {
            throw new BusinessRuleException(
                SelectionErrorCode.EDIT_ASSIGNMENT_INVALID_STATUS,
                new UserMessage(
                    "assignment.edit.error.invalidAssignmentStatus"));
        }
    }

    /**
     * Function for obtaining all operator names to display in the drop down of
     * the operator field on the edit page. TODO: Change it to get operators only
     * for specific region
     *
     * @return the map of operator identifiers, key by operator PKs (Ids)
     */

    public Map<Long, String> getAllOperatorsInRegion() {
        Map<Long, String> operators = new LinkedHashMap<Long, String>();
        try {
            if (this.assignment.getStatus() != AssignmentStatus.Passed) {
                Region a = this.assignment.getRegion();
                Operator op = this.assignment.getOperator();
                Assignment asmt = this.assignment;

                if (asmt.getOperator() != null) {
                    if (op.getId() != null) {
                        operators.put(op.getId(), op.getOperatorIdentifier());
                    }
                }
                operators.put(0L, getText(new UserMessage("operator.none")));
                List<Operator> allOperators = getOperatorManager()
                    .listAuthorizedForRegion(a.getId());
                for (Operator operatorname : allOperators) {
                    operators.put(operatorname.getId(), operatorname
                        .getOperatorIdentifier());
                }
            }
        } catch (Exception e) {
            log.warn("Error occurred while obtaining Operators in Region");
        }
        return operators;
    }

    /**
     * Getter for the ASSIGNMENT_VIEW_ID property.
     *
     * @return long value of the property
     */
    public static long getAssignmentViewId() {
        return ASSIGNMENT_VIEW_ID;
    }

    /**
     * Getter for the PICK_VIEW_ID property.
     *
     * @return long value of the property
     */
    public static long getPickViewId() {
        return PICK_VIEW_ID;
    }

    /**
     * Getter for the assignmentStartTime, which will return the properly formatted time.
     * @return String value of the property
     */
    public String getAssignmentStartTime() {
        String startTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            startTime = du.formatTimeWithTimeZone(getAssignment().getStartTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            startTime = getAssignment().getStartTime().toString();
        }

        return startTime;
    }

    /**
     * Getter for the assignmentEndTime, which will return the properly formatted time.
     * @return String value of the property
     */
    public String getAssignmentEndTime() {
        String endTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            endTime = du.formatTimeWithTimeZone(getAssignment().getEndTime(), null);
        } catch (DataAccessException e) {
            log.warn("error occurred on site retrieval. falling back to JVM default timezone.", e);
            endTime = getAssignment().getEndTime().toString();
        }

        return endTime;
    }    
    
    /**
     * Assignment the assignments identified by the <code>List[] ids</code>.
     * @return SUCCESS updated the delivery location for the selected assignment(s)
     *
     * @throws Exception on any unanticipated failure.
     * @author snayeem
     */
    public String modifyAssignmentDeliveryLocation() throws Exception {

        JSONArray jsonSuccessIds = new JSONArray();
        for (Object assignmentIdObj : assignmentIds) {
            Assignment assignmentDL = getAssignmentManager().get(
                Long.parseLong(assignmentIdObj.toString()));
            assignmentDL.setDeliveryLocation(getAssignmentDeliveryLocation());
            getAssignmentManager().save(assignmentDL);
        }

        if (assignmentIds.size() == 1) {
            setJsonMessage(
                getText(new UserMessage(
                    getText("assignment.modify.message.success"),
                    getAssignmentDeliveryLocation())), ERROR_SUCCESS,
                jsonSuccessIds);
            return SUCCESS;
        } else {
            setJsonMessage(
                getText(new UserMessage(
                    getText("assignment.modify.message.multiple.success"),
                    assignmentIds.size(), getAssignmentDeliveryLocation(), true)),
                ERROR_SUCCESS, jsonSuccessIds);

            return SUCCESS;
        }

    }

    /**
     * Getter for the assignmentIds property.
     * @return Collection value of the property
     */
    public Collection<String> getAssignmentIds() {
        return this.assignmentIds;
    }

    /**
     * Setter for the assignmentIds property.
     * @param assignmentIds the new assignmentIds value
     */
    public void setAssignmentIds(Collection<String> assignmentIds) {
        this.assignmentIds = assignmentIds;
    }


    /**
     * Getter for URL param assignmentDeliveryLocation to be used for
     * assignment.
     *
     * @return String
     * @author snayeem
     */
    public String getAssignmentDeliveryLocation() {
        return assignmentDeliveryLocation;
    }

    /**
     * Setter for URL param assignmentDeliveryLocation to be used for
     * assignment.
     *
     * @param assignmentDeliveryLocation
     *            value entered
     * @author snayeem
     */
    public void setAssignmentDeliveryLocation(String assignmentDeliveryLocation) {
        this.assignmentDeliveryLocation = assignmentDeliveryLocation;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(ASSIGNMENT_VIEW_ID);
        viewIds.add(PICK_VIEW_ID);
        viewIds.add(RESEQUENCE_ASSIGNMENT_VIEW_ID);
        return viewIds;
    }

    /**
     * Get the region selection screen or error.
     * @return the control flow target name.
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public String getResequenceRegion() throws DataAccessException,
    VocollectException {
    if (getResequenceRegionMap().isEmpty()) {
        setJsonMessage(
            getText(new UserMessage(
                "assignment.resequence.error.noRegions.message",
                ERROR_FAILURE)), ERROR_FAILURE);
        return SUCCESS;
        } else {
            return INPUT;
        }
    }

    /**
     * Function for obtaining all regions for resequence.
     *
     * @return the map of region names, key by region ids
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public Map<Long, String> getResequenceRegionMap()
    throws DataAccessException, VocollectException {
        try {
            resequenceRegionMap = new LinkedHashMap<Long, String>();
            List<Region> regions = getRegionManager().
                listSelectionResequenceRegions();
            for (Region r : regions) {
                resequenceRegionMap.put(r.getId(), r.getName());
            }
        } catch (DataAccessException e) {
            addSessionActionMessage(e.getUserMessage());
        }
        return resequenceRegionMap;
    }
    
    /**
     * Getter for the routeAtDeliveryDate property.
     * @return String value of the property
     */
    public String getRouteAtDeliveryDate() {
        return routeAtDeliveryDate;
    }

    /**
     * Setter for the routeAtDeliveryDate property.
     * @param routeAtDeliveryDate the new routeAtDeliveryDate value
     */
    public void setRouteAtDeliveryDate(String routeAtDeliveryDate) {
        this.routeAtDeliveryDate = routeAtDeliveryDate;
    }

    /**
     * Get the region selection screen or error.
     * @return the control flow target name.
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public String getRouteWithDeliveryDate() throws DataAccessException,
        VocollectException {
        if (getRouteDeliveryDateMap().isEmpty()) {
            setJsonMessage(getText(new UserMessage(
                "route.modfiy.departure.time.error.noRoutes.message",
                ERROR_FAILURE)), ERROR_FAILURE);
        } else {
            setJsonMessage("Success");
        }
        return SUCCESS;
    }
    
    /**
     * Function for obtaining all route with delivery date for departure date modification.
     *
     * @return the map of region names, key by region ids
     * @throws DataAccessException if there is an issue accessing the regions
     * @throws VocollectException if there is an issue with the UserMessage
     */
    public Map<String, String> getRouteDeliveryDateMap()
    throws DataAccessException, VocollectException {
        try {
            routeDeliveryDateMap = new LinkedHashMap<String, String>();
            List<Map<String, Object>> objects = getAssignmentManager().
                listDistinctRouteAndDeliveryDate();
            for (Map<String, Object> object : objects) {
                String route = (String) object.get("Route");
                Date deliveryDate = (Date) object.get("DeliveryDate");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String routeDeliveryDateToDisplay = route + " : " + sdf.format(deliveryDate);
                String routeDeliveryDateKey = route + "@" + deliveryDate.toString();
                routeDeliveryDateMap.put(routeDeliveryDateKey, routeDeliveryDateToDisplay);
            }
        } catch (DataAccessException e) {
            addSessionActionMessage(e.getUserMessage());
        }
        return routeDeliveryDateMap;
    }
    
    /**
     * 
     * @return route departure date
     * @throws JSONException je
     * @throws ParseException   pe
     */
    public String getDepartureTimeByRouteAndDeliveryDate()
        throws JSONException, ParseException {
        final int first = 0;
        final int second = 1;
        if (getRouteAtDeliveryDate() != null) {
            String[] routeDeliveryDate = getRouteAtDeliveryDate().split("@");
            String selectedRoute = routeDeliveryDate[first];
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date deliveryDate = sdf.parse(routeDeliveryDate[second]);
            
            Date departureDate = getAssignmentManager()
                .findEarliestDDTByRouteDeliveryDate(selectedRoute, deliveryDate); 
            sdf.applyPattern("yyyy-MM-dd HH:mm:ss");
            String departureDateString = sdf.format(departureDate);
            
            JSONObject departureDateJson = new JSONObject();
            departureDateJson
                .put("routeDepartureDate", departureDateString);
            setJsonMessage(departureDateJson.toString());
            
            return SUCCESS;
        }
        return SUCCESS;
    }
    
    /**
     * This method is responsible to update the departure
     * date and time based on Route and Delivery Date.
     *   
     * @return actionResponse String.
     */
    public String updateDepartureDate() {
        final int first = 0;
        final int second = 1;
        String[] routeDeliveryDate = getRouteAtDeliveryDate().split("@");
        String selectedRoute = routeDeliveryDate[first];
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date deliveryDate;
        try {
            deliveryDate = sdf.parse(routeDeliveryDate[second]);
        
            List<Assignment> assignments = getAssignmentManager()
                    .listAssignmentsByRouteDeliveryDate(selectedRoute,
                            deliveryDate);
            Assignment a = assignments.get(0);
            // Find route object for this assignment
            List<LoadingRoute> loadingRoutes = getLoadingRouteManager()
                .listRouteByNumberAndDDT(selectedRoute, a.getDepartureDateTime());
            DateFormat dateformatter = new SimpleDateFormat(DATE_FORMAT);
            Date departureDateTime = dateformatter.parse(getRouteDepartureDate());
            for (Assignment assign : assignments) {
                assign.setDepartureDateTime(departureDateTime);
            }
            getAssignmentManager().save(assignments);
            // If there was a matching loading route record then
            // update the departureDate field and save. 
            for (LoadingRoute lRoute : loadingRoutes) {
                lRoute.setDepartureDateTime(departureDateTime);
            }
            getLoadingRouteManager().save(loadingRoutes);
        } catch (Exception e) {
            e.printStackTrace();
            setJsonMessage("message", getText(new UserMessage("assignment.route.departuredate.update.error")),
                "errorCode", ERROR_FAILURE);
            return ERROR;
        }
        setJsonMessage(
            getText("assignment.route.departuredate.update.success"), SUCCESS);
        return SUCCESS;
    }
    
    /**
     * Overridden setter for the setJsonMessage function.
     * @param statusKey Key value for the json object representing the status
     * @param statusValue Value attributed to key for the json object
     *            representing the status
     * @param returnKey Key value for the json object representing the return
     *            var
     * @param returnValue Value attributed to key for the json object
     *            representing the return var
     */
    public void setJsonMessage(String statusKey,
                               String statusValue,
                               String returnKey,
                               String returnValue) {
        JSONObject json = new JSONObject();
        try {
            json.put(statusKey, statusValue);
            json.put(returnKey, returnValue);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        setJsonMessage(json.toString());
    }    

    /**.
     * Get the time minutes for minutes dropdown
     * 
     * @return the Map of time minutes values
     */
    public Map<Integer, String> getMinutesMap() {
        Map<Integer, String> map = new TreeMap<Integer, String>();
        for (Integer i = 0; i < DateUtil.MINUTES_IN_AN_HOUR; i++) {
            map.put(i, String.format("%02d", i));
        }
        return map;
    }

    /**.
     * Generates a map of the 24 hours in a day
     * 
     * @return - Map of each hour
     */
    public Map<Integer, String> getHoursMap() {
        Map<Integer, String> map = new TreeMap<Integer, String>();
        for (Integer i = 0; i < DateUtil.HOURS_IN_A_DAY; i++) {
            map.put(i, String.format("%02d", i));
        }
        return map;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.ResequenceActionRoot#getResequenceViewId()
     */
    public Long getResequenceViewId() {
        return RESEQUENCE_ASSIGNMENT_VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.ResequenceActionRoot#getAssignments(java.lang.Long)
     */
    public List<DataObject> getAssignments(Long regionId) throws DataAccessException {
        return assignmentManager.listResequenceAssignmentsByRegion(regionId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.web.core.action.ResequenceActionRoot#executeResequence(java.util.ArrayList, java.lang.Long)
     */
    public void executeResequence(ArrayList<Long> sequenceList, Long regionId)
        throws DataAccessException, BusinessRuleException {
        assignmentManager.executeResequence(sequenceList, regionId);
    }

    /**
     * Retrieves table data for the resequence page.
     * @return String SUCCESS
     * @throws Exception in case of DataAccess issues
     */
    public String getResequenceTableData() throws Exception {
        // Always want data to start of in order of current sequence number
        setSortAsc(true);
        setSortColumn("sequenceNumber");
        return super.getResequenceTableData();
    }


    /**
     * Getter for the exportStatus property.
     * @return String value of the property
     */
    public String getExportStatus() {
        return getText(assignment.getExportStatus().getResourceKey());
    }
    
    /**
     * Getter for the assignment Type property.
     * @return String value of the property
     */
    public String getAssignmentType() {
        return getText(assignment.getType().getResourceKey());
    }


    /**
     * @return the loadingRouteManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }


    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }


    /**
     * @return the loadingRoute
     */
    public LoadingRoute getLoadingRoute() {
        return loadingRoute;
    }


    /**
     * @param loadingRoute the loadingRoute to set
     */
    public void setLoadingRoute(LoadingRoute loadingRoute) {
        this.loadingRoute = loadingRoute;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 