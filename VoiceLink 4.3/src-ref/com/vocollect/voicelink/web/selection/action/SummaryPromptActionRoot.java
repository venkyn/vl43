/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.SystemTranslationManager;
import com.vocollect.epp.util.LocaleAdapter;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.selection.model.SummaryPrompt;
import com.vocollect.voicelink.selection.model.SummaryPromptCharactersToSpeak;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinition;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinitionLocale;
import com.vocollect.voicelink.selection.model.SummaryPromptItem;
import com.vocollect.voicelink.selection.service.SummaryPromptDefinitionManager;
import com.vocollect.voicelink.selection.service.SummaryPromptManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Action Class for Summary Prompt.
 *
 * @author svoruganti
 */
public class SummaryPromptActionRoot extends DataProviderAction implements Preparable {

    /* generated serial version id */
    private static final long serialVersionUID = 9001609724930212351L;

    private static final int NUM_CHAR_SPEAK_MAX = 18;

    /* logger for summaryPromptAction class  */
    private static final Logger log = new Logger(SummaryPromptActionRoot.class);

    /* The ID of the summaryPromptId */
    private Long summaryPromptId;

    /* The model for summaryPrompt */
    private SummaryPrompt summaryPrompt;

    /* manager for summaryPrompt */
    private SummaryPromptManager summaryPromptManager;

    /* The ID of the view we need to grab from the DB. */
    private static final long VIEW_ID = -1033;

    /* List of prompt items */
    private List<SummaryPromptItem> promptItems;

    /* The list of columns for the summary prompt table. */
    private List<Column> summaryPromptColumns;

    /* The list of columns for totalValues. */
    private List<String> totalValues = null;

    /* The list of charSpeakValues. */
    private List<String> charSpeakValues = null;

    /* The list of numCharSpeakValues. */
    private List<String> numCharSpeakValues = null;

    /*language value from UI */
    private String language;

     /* list of locales for summary prompt */
    private Map<String, String> localesForSummaryPrompt;

    /* Default languages */
    private Map<String, String> languageMap;

    /* All prompt items */
    private Map<Long, String> promptTypeMap;

    private String[] promptItemArray;

    private SummaryPromptDefinitionManager summaryPromptDefinitionManager;

    private SystemTranslationManager systemTranslationManager;


    private ArrayList<SummaryPromptDefinition>  definitions = new ArrayList<SummaryPromptDefinition>();

    /* Default languages */
    private Map<String, String> languageMapForAdd;

    private String languageSummaryPrompt;

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List< Column > getSummaryPromptColumns() {
        return summaryPromptColumns;
    }

    /**
     * Getter for the Summary prompt view id property.
     * @return long value of the property
     */
    public static long getSummaryPromptViewId() {
        return VIEW_ID;
    }

    /**
     * Action for the Summary prompt view page. Initializes the Summary Prompt table columns
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View view = getUserPreferencesManager().getView(VIEW_ID);
        this.summaryPromptColumns = this.getUserPreferencesManager().getColumns(view, getCurrentUser());
        return SUCCESS;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getSummaryPromptManager();
     }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
       return "summaryPrompt";
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {

        //Get a list of all of the prompt items
       promptItems = this.summaryPromptManager.getSummaryPromptItemDAO().getAll();
       if (this.summaryPromptId != null) {
            // We have an ID, but not a Summary prompt object yet.
            if (log.isDebugEnabled()) {
                log.debug("summaryPromptId is " + this.summaryPromptId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the summary prompt is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved summary prompt from session");
                }
                this.summaryPrompt = (SummaryPrompt) getEntityFromSession(getSavedEntityKey());
              } else {
                  if (log.isDebugEnabled()) {
                      log.debug("Getting summary Prompt from database");
                  }
                  this.summaryPrompt = this.summaryPromptManager.get(this.summaryPromptId);
                  saveEntityInSession(this.summaryPrompt);
            }
            if (log.isDebugEnabled()) {
                log.debug("Summary prompt version is: " + this.summaryPrompt.getVersion());
            }
           } else {
               if (log.isDebugEnabled()) {
                   log.debug("Created new summary prompt object");
               }
               // Set the prompts to the defaults
               this.summaryPrompt = summaryPromptManager.getDefaultSummaryPrompt();
        }
   }


    /**
     * Action to check if languages are present to add.
     *
     * @return String value of outcome.
     * @throws DataAccessException
     *
     */
    @SuppressWarnings("finally")
    public String checkIfLanguagesPresent() {

        try {
            setSummaryPromptId(getIds()[0]);
            this.languageMapForAdd = getLanguageMapForAdd();
            if (this.languageMapForAdd.isEmpty()) {
                setJsonMessage(getText(new UserMessage("summaryPrompt.languages.notpresent.message",
                                       ERROR_FAILURE)), ERROR_FAILURE);
            } else {
                setJsonMessage("createLanguage!input.action?summaryPromptId=" + this.summaryPromptId, ERROR_REDIRECT);
            }
        } catch (DataAccessException e) {
            setJsonMessage(getText(new UserMessage("summaryPrompt.languages.retrieve.error.message",
                                   ERROR_FAILURE)), ERROR_FAILURE);
        } finally {
            return SUCCESS;
        }
   }

    /**
     * Delete the summary prompt identified by the <code>summaryPromptId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if the specified
     * location wasn't found (with different messages to the end user)
     * @throws Exception on unanticipated error condition
     */
    @SuppressWarnings("finally")
    public String deleteCurrentSummaryPrompt() throws Exception {

        try {
            summaryPromptManager.delete(this.summaryPromptId);
            addSessionActionMessage(new UserMessage("summaryPrompt.delete.message.success",
                                                     summaryPromptManager.get(this.summaryPromptId).getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete summary Prompt: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
        } finally {
            return SUCCESS;
        }
    }

    /**
     * Create or update the SummaryPrompt specified by the <code>summaryPrompt</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String editSummaryPrompt() throws Exception {

        try {
            if (this.summaryPrompt != null) {
                if (this.definitions != null) {
                    Iterator<SummaryPromptDefinition> defIterator = this.getDefinitions().iterator();
                    int position = 1;
                    while (defIterator.hasNext()) {
                        SummaryPromptDefinition def = defIterator.next();
                        def.setPromptItem(this.summaryPromptManager.getSummaryPromptItemDAO()
                                          .get(def.getPromptItemId()));
                        def.setPosition(position);
                        def.setSummaryPrompt(this.summaryPrompt);
                        position++;
                    }
                    this.summaryPromptManager.executeSaveEdit(this.summaryPrompt, definitions);
                }
            }
            cleanSession();
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified summary prompt " + this.summaryPrompt.getName());
            addActionError(new UserMessage("entity.error.modified",
                                           UserMessage.markForLocalization("entity.summaryPrompt"),
                                           SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data. If the entity has been deleted, this will throw EntityNotFoundException
            try {
                SummaryPrompt modifiedEntity = getSummaryPromptManager().get(getSummaryPromptId());
                // Set the local object's version to match, so it will work if the user resubmits.
                this.summaryPrompt.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted", UserMessage.markForLocalization(
                                                        this.summaryPrompt.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }
        addSessionActionMessage(new UserMessage("summaryPrompt.edit.message.success",
                                makeContextURL("/selection/summaryPrompt/view.action?summaryPromptId="
                                + summaryPrompt.getId()), this.summaryPrompt.getName()));
        return SUCCESS;
    }

    /**
     * Load the prompt values stored in the DB.
     *
     * @throws DataAccessException on db access failure
     */
    private void setPromptValues() throws DataAccessException {
        String prompt1 = "";
        String prompt2 = "";
        String prompt3 = "";
        this.summaryPrompt = this.summaryPromptManager.get(this.summaryPromptId);
        Iterator<SummaryPromptDefinition> definitionIterator = this.summaryPrompt.getDefinitions().iterator();

        while (definitionIterator.hasNext()) {
            SummaryPromptDefinition def = definitionIterator.next();
            List<SummaryPromptDefinitionLocale> defLocales = def.getLocaleDefintions();
            for (SummaryPromptDefinitionLocale definitionLocales : defLocales) {
                if (definitionLocales.getLocale().getDisplayName().
                    equals(LocaleAdapter.makeLocaleFromString(this.language).getDisplayName())) {

                    prompt1 = definitionLocales.getPromptValue1();
                    prompt2 = definitionLocales.getPromptValue2();
                    prompt3 = definitionLocales.getPromptValue3();
               }
           }
           def.setPromptValue1(prompt1);
           def.setPromptValue2(prompt2);
           def.setPromptValue3(prompt3);
       }
    }



    /**
     * Create or view the SummaryPrompt Language specified by the <code>summaryPrompt</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String viewLanguage() throws Exception {

        if (this.definitions == null || this.definitions.isEmpty()) {
            if (this.summaryPromptId != null) {
                this.setPromptValues();
            }
        }
        return SUCCESS;
    }


    /**
     * Create or edit Language the SummaryPrompt specified by the <code>summaryPrompt</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String editLanguage() throws Exception {

        if (this.definitions == null || this.definitions.isEmpty()) {
            if (this.summaryPromptId != null) {
                this.setPromptValues();
            }
            return INPUT;
        }

        try {
            if (this.language != null) {
                //this.summaryPromptManager.
                Iterator<SummaryPromptDefinition> definitionIterator = this.summaryPrompt.getDefinitions().iterator();
                List<SummaryPromptDefinition> changedDefinitions = new ArrayList<SummaryPromptDefinition>(definitions);
                int pos = 0;
                while (definitionIterator.hasNext()) {
                    SummaryPromptDefinition def = definitionIterator.next();
                    List<SummaryPromptDefinitionLocale> defLocales = def.getLocaleDefintions();
                    for (SummaryPromptDefinitionLocale definitionLocales : defLocales) {
                        if (definitionLocales.getLocale().getDisplayName().
                            equals(LocaleAdapter.makeLocaleFromString(this.language).getDisplayName())) {

                            SummaryPromptDefinition changedDef = changedDefinitions.get(pos);
                            definitionLocales.setPromptValue1(changedDef.getPromptValue1());
                            definitionLocales.setPromptValue2(changedDef.getPromptValue2());
                            definitionLocales.setPromptValue3(changedDef.getPromptValue3());
                        }
                    }
                    pos++;
                }
            }
            this.summaryPromptManager.save(this.summaryPrompt);
            cleanSession();

            // add success message
            String successKey = "summaryPrompt.language.edit" + ".message.success";
            addSessionActionMessage(new UserMessage(successKey,
                                    makeContextURL("/selection/summaryPrompt/view.action?summaryPromptId="
               + getText("summaryPrompt.view.column." + LocaleAdapter.makeLocaleFromString(this.language).toString())),
               getText("summaryPrompt.view.column." + LocaleAdapter.makeLocaleFromString(this.language).toString())));

            return SUCCESS;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified summary prompt " + this.summaryPrompt.getName());
            addActionError(new UserMessage("entity.error.modified",
                                           UserMessage.markForLocalization("entity.summaryPrompt"),
                                           SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));

            // Get the modified data. If the entity has been deleted, this will throw EntityNotFoundException
           try {
                SummaryPrompt modifiedEntity = getSummaryPromptManager().get(getSummaryPromptId());
                // Set the local object's version to match, so it will work if the user resubmits.
                this.summaryPrompt.setVersion(modifiedEntity.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedEntity);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                                        UserMessage.markForLocalization(this.summaryPrompt.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        }
    }




    /**
     * Create or update the SummaryPrompt specified by the <code>summaryPrompt</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.summaryPrompt.isNew();
        if (this.language != null) {
            Iterator<SummaryPromptDefinition> defIterator =  this.definitions.iterator();
            List<SummaryPromptDefinitionLocale> definitionLocalesList = new
                                    ArrayList<SummaryPromptDefinitionLocale>();
            while (defIterator.hasNext()) {
                SummaryPromptDefinition def = defIterator.next();
                SummaryPromptDefinitionLocale defLocale = new SummaryPromptDefinitionLocale();
                defLocale.setLocale(LocaleAdapter.makeLocaleFromString(this.language));
                defLocale.setPromptValue1(def.getPromptValue1());
                defLocale.setPromptValue2(def.getPromptValue2());
                defLocale.setPromptValue3(def.getPromptValue3());
                definitionLocalesList.add(defLocale);
            }
            Iterator<SummaryPromptDefinition> definitionIterator = this.summaryPrompt.getDefinitions().iterator();
            int pos = 0;
            while (definitionIterator.hasNext()) {
                SummaryPromptDefinition def = definitionIterator.next();
                List<SummaryPromptDefinitionLocale> defLocales = def.getLocaleDefintions();
                if (defLocales == null) {
                    defLocales = new ArrayList<SummaryPromptDefinitionLocale>();
                }
                SummaryPromptDefinitionLocale defLocaleToAdd = definitionLocalesList.get(pos);
                defLocaleToAdd.setSummaryPromptDefinition(def);
                defLocales.add(defLocaleToAdd);
                pos++;
            }
            this.summaryPromptManager.save(this.summaryPrompt);
            cleanSession();
            addSessionActionMessage(new UserMessage(
                "summaryPrompt.language.add.message.success",
                makeContextURL("/selection/summaryPrompt/view.action?summaryPromptId="
                    + summaryPrompt.getId()),
                getText("summaryPrompt.view.column."
                    + LocaleAdapter.makeLocaleFromString(this.language)
                        .toString())));
            return SUCCESS;

        } else {
            try {
                this.summaryPrompt.setDefinitions(new ArrayList<SummaryPromptDefinition>());
                //Put the Summary Items into the definitions by ID
                Iterator<SummaryPromptDefinition> defIterator = this.getDefinitions().iterator();
                int position = 1;
                while (defIterator.hasNext()) {
                    SummaryPromptDefinition def = defIterator.next();
                    def.setPromptItem(this.summaryPromptManager.getSummaryPromptItemDAO().get(def.getPromptItemId()));
                    def.setPosition(position);
                    def.setSummaryPrompt(this.summaryPrompt);
                    position++;
                }
                this.summaryPrompt.setDefinitions(definitions);
                if (this.summaryPrompt.getName() == null
                    || this.summaryPrompt.getName().trim().equalsIgnoreCase("")) {
                    addFieldError("summaryPrompt.name", getText("summaryPrompt.create.label.name")
                        + getText("errors.required"));
                }
                if (hasErrors()) {
                    return (INPUT);
                }

                //Uniqueness check for summary prompt name
                Long numberUniquenessByName =
                    this.summaryPromptManager.getPrimaryDAO().uniquenessByName(this.summaryPrompt.getName());
                if (numberUniquenessByName != null
                    && (isNew || (!isNew && numberUniquenessByName.longValue()
                        != this.summaryPrompt.getId().longValue()))) {
                    log.warn("Summary Prompt'" + this.summaryPrompt.getName() + "' already exists");
                    addFieldError("summaryPrompt.name",
                                    new UserMessage("summaryPrompt.create.error.existingName",
                                                    this.summaryPrompt.getName()));
                      // Go back to the form to show the error message.
                      return INPUT;
                  }

                  summaryPromptManager.save(this.summaryPrompt);
                  cleanSession();
              } catch (OptimisticLockingFailureException e) {
                  log.warn("Attempt to change already modified summary prompt "
                           + this.summaryPrompt.getName());
                  addActionError(new UserMessage("entity.error.modified",
                                                 UserMessage.markForLocalization("entity.summaryPrompt"),
                                                 SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
                  // Get the modified data
                  // If the entity has been deleted, this will throw EntityNotFoundException
                  try {
                      SummaryPrompt modifiedEntity = getSummaryPromptManager().get(getSummaryPromptId());
                      // Set the local object's version to match, so it will work if the user resubmits.
                      this.summaryPrompt.setVersion(modifiedEntity.getVersion());
                      // Store the modified data for display
                      setModifiedEntity(modifiedEntity);
                  } catch (EntityNotFoundException ex) {
                      addSessionActionMessage(new UserMessage("entity.error.deleted",
                                              UserMessage.markForLocalization(this.summaryPrompt.getName()), null));
                      return SUCCESS;
                  }
                  return INPUT;
              }
              // add success message
              String successKey = "summaryPrompt." + (isNew ? "create" : "edit") + ".message.success";
              addSessionActionMessage(new UserMessage(successKey,
                                      makeContextURL("/selection/summaryPrompt/view.action?summaryPromptId="
                                                     + summaryPrompt.getId()), this.summaryPrompt.getName()));
        }
        // Go to the success target.
        return SUCCESS;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }


    /**
     * Getter for the languageMapForAdd property.
     * @return Map value of the property
     * @throws DataAccessException when cannot retrieve notyetdefinedlocales
     */
    public Map<String , String> getLanguageMapForAdd() throws DataAccessException {

       this.languageMapForAdd = new LinkedHashMap<String , String>();

        if (summaryPromptId != null) {
           this.summaryPrompt = summaryPromptManager.get(summaryPromptId);
           for (Locale locale : this.getSummaryPromptManager().getNotYetDefinedLocales(this.summaryPrompt)) {
               languageMapForAdd.put(locale.toString(), getText("summaryPrompt.view.column." + locale.toString()));
           }
       }
       return this.languageMapForAdd;
    }


    /**
     *
     * @return Map - the language drop down values
     * @throws DataAccessException when cannot get languages
     */
    public Map<String, String> getLanguageMap() throws DataAccessException {

     if (this.languageMap == null) {
            this.languageMap = new LinkedHashMap<String, String>();
            for (Locale locale : getSystemTranslationManager().getAllSupportedLocales()) {
                   languageMap.put(locale.toString(), getText("summaryPrompt.view.column." + locale.toString()));
            }
      }
      return this.languageMap;
    }

    /**
     *
     * @return Map - the prompt type drop down values
     */
    public Map<Long, String> getPromptTypeMap() {

        if (this.promptTypeMap == null) {
            this.promptTypeMap = new LinkedHashMap<Long, String>();
            if (this.promptItems != null) {
                for (SummaryPromptItem nextItem : this.promptItems) {
                   this.promptTypeMap.put(nextItem.getId(), getText("summaryPrompt.prompt.type."
                                          + nextItem.getId()));
                }
            }
        }
        return this.promptTypeMap;
    }



    /**
     * Getter for the totalValues property.
     *
     * @return totalValues
     */
    public List<String> getTotalValues() {
        if (totalValues == null) {
            this.totalValues = new ArrayList<String>();
            this.totalValues.add(0, getText("summaryPrompt.prompt.totalVal.0"));
            this.totalValues.add(1, getText("summaryPrompt.prompt.totalVal.1"));
        }
        return totalValues;
    }

    /**
     * Setter for the totalValues property.
     * @param totalValues the new totalValues
     */
    public void setTotalValues(List<String> totalValues) {
        this.totalValues = totalValues;
    }

    /**
     * Getter for the Languages for property.
     * @return xwork flow of the method
     * @throws DataAccessException when summaryPrompt Definition cannot be retrieved
     */
    public String getLanguagesForSummaryPrompt() throws DataAccessException {

        localesForSummaryPrompt = new HashMap<String, String>();
        Long[] ids = getIds();

        if (ids.length > 0) {
            this.summaryPrompt = this.getSummaryPromptManager().get(ids[0]);
            for (Locale locale : this.summaryPrompt.listLocalesWithDefinitions()) {
                localesForSummaryPrompt.put(locale.toString(),
                    getText("summaryPrompt.view.column." + locale.toString()));
            }
        }

        if (localesForSummaryPrompt.isEmpty()) {
            setJsonMessage(getText(new UserMessage("summaryPrompt.languages.notpresent.message",
                                                   ERROR_FAILURE)), ERROR_FAILURE);
            return SUCCESS;
        }

        return INPUT;
    }


    /**
     * Getter for the charSpeakValues property.
     *
     * @return list of charSpeakValues
     *
     */
    public List<String> getCharSpeakValues() {
        if (charSpeakValues == null) {
            this.charSpeakValues = new ArrayList<String>();
            this.charSpeakValues.add(0, getText("summaryPrompt.prompt.charVal."
                + SummaryPromptCharactersToSpeak.All));
            this.charSpeakValues.add(1, getText("summaryPrompt.prompt.charVal."
                + SummaryPromptCharactersToSpeak.Right));
            this.charSpeakValues.add(2, getText("summaryPrompt.prompt.charVal."
                + SummaryPromptCharactersToSpeak.Left));
        }
        return charSpeakValues;
    }

    /**
     * Getter for the numCharSpeakValues property.
     *
     * @return list of numCharSpeakValues
     *
     */
    public List<String> getNumCharSpeakValues() {
        if (numCharSpeakValues == null) {
            this.numCharSpeakValues = new ArrayList<String>();
            for (int i = 0; i < NUM_CHAR_SPEAK_MAX; i++) {
                this.numCharSpeakValues.add(i, Integer.toString(i + 1));
            }
        }
        return numCharSpeakValues;
    }


    /**
     * Setter for the charSpeakValues property.
     *  @param  charSpeakValues the new charSpeakValues
     */
    public void setCharSpeakValues(List<String> charSpeakValues) {
        this.charSpeakValues = charSpeakValues;
    }


    /**
     * Delete the language identified by the <code>language</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    @SuppressWarnings("finally")
    public String deleteLanguage() throws Exception {

       try {
           Long[] ids = getIds();
           if (ids != null && ids.length > 0) {
                this.summaryPromptManager.executeDeleteLocale(
                    summaryPromptManager.get(ids[0]), LocaleAdapter
                        .makeLocaleFromString(this.language));
                addSessionActionMessage(new UserMessage(
                    "language.delete.message.success",
                    getText("summaryPrompt.view.column."
                        + LocaleAdapter.makeLocaleFromString(this.language).toString())));
            } else {
                addSessionActionMessage(new UserMessage(
                    "language.delete.message.failure",
                    getText("summaryPrompt.view.column."
                        + LocaleAdapter.makeLocaleFromString(this.language).toString())));
            }
        } catch (EntityNotFoundException e) {
            addSessionActionMessage(e.getUserMessage());
        } catch (DataAccessException e) {
            addSessionActionMessage(e.getUserMessage());
        } finally {
            return SUCCESS;
        }
    }



    /**
     * Deletes current language identified by the <code>language</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified location wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    @SuppressWarnings("finally")
    public String deleteCurrentLanguage() throws Exception {

        try {
            if (summaryPromptId != null) {
                this.summaryPromptManager.executeDeleteLocale(
                    summaryPromptManager.get(summaryPromptId), LocaleAdapter
                        .makeLocaleFromString(this.language));
                addSessionActionMessage(new UserMessage(
                    "language.delete.message.success",
                    getText("summaryPrompt.view.column."
                        + LocaleAdapter.makeLocaleFromString(this.language).toString())));
            } else {
                addSessionActionMessage(new UserMessage(
                    "language.delete.message.failure",
                    getText("summaryPrompt.view.column."
                        + LocaleAdapter.makeLocaleFromString(this.language).toString())));

            }
        } catch (BusinessRuleException e) {
             log.warn("Unable to delete language in summary prompt: " + getText(e.getUserMessage()));
             addSessionActionMessage(e.getUserMessage());
         } catch (EntityNotFoundException e) {
             addSessionActionMessage(e.getUserMessage());
         } catch (DataAccessException e) {
             addSessionActionMessage(e.getUserMessage());
         } finally {
             return SUCCESS;
         }
     }


    /**
     * Getter for the languageSummaryPrompt property.
     * @return languageSummaryPrompt value of the property
     */
        public String getLanguageSummaryPrompt() {
        if (this.language != null) {
            this.languageSummaryPrompt = LocaleAdapter.makeLocaleFromString(this.language).getDisplayName();
        }
        return languageSummaryPrompt;
    }



    /**
     * Setter for the languageMapForAdd property.
     * @param languageMapForAdd the new languageMapForAdd value
     */
    public void setLanguageMapForAdd(Map<String, String> languageMapForAdd) {
        this.languageMapForAdd = languageMapForAdd;
    }


    /**
     * Getter for the localesForSummaryPrompt property.
     * @return Map value of the property
     */
    public Map<String, String> getLocalesForSummaryPrompt() {
        return localesForSummaryPrompt;
    }


    /**
     * Setter for the localesForSummaryPrompt property.
     * @param localesForSummaryPrompt the new localesForSummaryPrompt value
     */
    public void setLocalesForSummaryPrompt(Map<String, String> localesForSummaryPrompt) {
        this.localesForSummaryPrompt = localesForSummaryPrompt;
    }


    /**
     * Getter for the language property.
     * @return String value of the property
     */
    public String getLanguage() {
        return language;
    }


    /**
     * Setter for the language property.
     * @param language the new language value
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * Getter for the definitions property.
     *
     * @return list of summarypromptdefinitions
     */
    public ArrayList<SummaryPromptDefinition> getDefinitions() {
        return definitions;
    }


    /**
     * Setter for the definitions property.
     * @param definitions new definition
     */
    public void setDefinitions(ArrayList<SummaryPromptDefinition> definitions) {
        this.definitions = definitions;
    }



    /**
     * Getter for the summaryPromptId property.
     * @return Long value of the property
     */
    public Long getSummaryPromptId() {
        return summaryPromptId;
    }


    /**
     * Setter for the summaryPromptId property.
     * @param summaryPromptId the new summaryPromptId value
     */
    public void setSummaryPromptId(Long summaryPromptId) {
        this.summaryPromptId = summaryPromptId;
    }


    /**
     * Getter for the summaryPromptManager property.
     * @return SummaryPromptManager value of the property
     */
    public SummaryPromptManager getSummaryPromptManager() {
        return summaryPromptManager;
    }


    /**
     * Setter for the summaryPromptManager property.
     * @param summaryPromptManager the new summaryPromptManager value
     */
    public void setSummaryPromptManager(SummaryPromptManager summaryPromptManager) {
        this.summaryPromptManager = summaryPromptManager;
    }


    /**
     * Getter for the summaryPrompt property.
     * @return SummaryPrompt value of the property
     */
    public SummaryPrompt getSummaryPrompt() {
        return summaryPrompt;
    }


    /**
     * Setter for the summaryPrompt property.
     * @param summaryPrompt the new summaryPrompt value
     */
    public void setSummaryPrompt(SummaryPrompt summaryPrompt) {
        this.summaryPrompt = summaryPrompt;
    }


    /**
     * Getter for the promptItems property.
     * @return promptItems value of the property
     */
    public List<SummaryPromptItem> getPromptItems() {
        return promptItems;
    }


    /**
     * Setter for the promptItems property.
     * @param promptItems the new promptItems value
     */
    public void setPromptItems(List<SummaryPromptItem> promptItems) {
        this.promptItems = promptItems;
    }


    /**
     * Getter for the promptItemArray property.
     * @return promptItemArray
     */
    public String[] getPromptItemArray() {
        return promptItemArray;
    }


    /**
     * Setter for the promptItemArray property.
     *
     * @param promptItemArray the new promptItems
     */
    public void setPromptItemArray(String[] promptItemArray) {
        this.promptItemArray = promptItemArray;
    }

    /**
     * Getter for the summaryPromptDefinitionManager property.
     * @return SummaryPromptDefinitionManager value of the property
     */
    public SummaryPromptDefinitionManager getSummaryPromptDefinitionManager() {
        return summaryPromptDefinitionManager;
    }


    /**
     * Setter for the summaryPromptDefinitionManager property.
     * @param summaryPromptDefinitionManager the new summaryPromptDefinitionManager value
     */
    public void setSummaryPromptDefinitionManager(SummaryPromptDefinitionManager summaryPromptDefinitionManager) {
        this.summaryPromptDefinitionManager = summaryPromptDefinitionManager;
    }


    /**
     * Getter for the systemTranslationManager property.
     * @return SystemTranslationManager value of the property
     */
    public SystemTranslationManager getSystemTranslationManager() {
        return this.systemTranslationManager;
    }


    /**
     * Setter for the systemTranslationManager property.
     * @param systemTranslationManager the new systemTranslationManager value
     */
    public void setSystemTranslationManager(SystemTranslationManager systemTranslationManager) {
        this.systemTranslationManager = systemTranslationManager;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 