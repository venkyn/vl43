/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.web.action.ConfigurableHomepagesAction;

/**
 * Base acttion for VoiceLink Home Page actions.
 * @author ddoubleday
 */
public abstract class VoiceLinkHomeActionRoot extends  ConfigurableHomepagesAction {

    /**
     * The name of the group that includes common VoiceLink features.
     */
    public static final String VOICELINK_GROUP_NAME = "featureGroup.voicelink.name";

    /**
     * The name of the group that includes features for selection.
     */
    public static final String VOICELINK_SELECTION_GROUP_NAME = "featureGroup.voicelink.selection.name";

    /**
     * The forwarding target for the Selection Home page.
     */
    public static final String VOICELINK_SELECTION_HOME = "selectionHome";

    /**
     * The name of the group that includes features for Putaway.
     */
    public static final String VOICELINK_PUTAWAY_GROUP_NAME = "featureGroup.voicelink.putaway.name";

    /**
     * The forwarding target for the Putaway Home page.
     */
    public static final String VOICELINK_PUTAWAY_HOME = "putawayHome";

    /**
     * The name of the group that includes features for Replenishment.
     */
    public static final String VOICELINK_REPLENISHMENT_GROUP_NAME = "featureGroup.voicelink.replenishment.name";

    /**
     * The forwarding target for the Replenishment Home page.
     */
    public static final String VOICELINK_REPLENISHMENT_HOME = "replenishmentHome";

    /**
     * The name of the group that includes features for Line Loading.
     */
    public static final String VOICELINK_LINELOADING_GROUP_NAME = "featureGroup.voicelink.lineloading.name";

    /**
     * The forwarding target for the Line Loading Home page.
     */
    public static final String VOICELINK_LINELOADING_HOME = "lineloadingHome";

    /**
     * The name of the group that includes features for Put To Store.
     */
    public static final String VOICELINK_PUTTOSTORE_GROUP_NAME = "featureGroup.voicelink.puttostore.name";

    /**
     * The forwarding target for the Put To Store page.
     */
    public static final String VOICELINK_PUTTOSTORE_HOME = "puttostoreHome";
    
    
    /**
     * The name of the group that includes features for Loading.
     */
    public static final String VOICELINK_LOADING_GROUP_NAME = "featureGroup.voicelink.loading.name";

    /**
     * The forwarding target for the Loading Home page.
     */
    public static final String VOICELINK_LOADING_HOME = "loadingHome";
    
    /**
     * The name of the group that includes features for CycleCounting.
     */
    public static final String VOICELINK_CYCLECOUNTING_GROUP_NAME = "featureGroup.voicelink.cyclecounting.name";

    /**
     * The forwarding target for the Loading Home page.
     */
    public static final String VOICELINK_CYCLECOUNTING_HOME = "cyclecountingHome";

    /**
     * Set a no access message and return INPUT.
     * @return INPUT forward token.
     */
    protected String noAccessMessage() {
        // Send the user to their homepage with a little message...
        addSessionActionErrorMessage(new UserMessage("voicelink.no.view.access"));
        return INPUT;

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 