/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.RemovableDataProvider;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.ShortManager;

import static com.vocollect.epp.web.util.JSONResponseBuilder.*;

import com.opensymphony.xwork2.Preparable;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;

/**
 * This is the Struts action class that handles operations on
 * <code>Short</code> objects.
 *
 * @author ddoubleday
 */
public class ShortActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 6478026686916495773L;

    private static final Logger log = new Logger(ShortAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long SHORT_ID = -1008;

    // The list of columns for the operators table.
    private List<Column> columns;

    // The short management service.
    private ShortManager shortManager;

    // The assignment management service.
    private AssignmentManager assignmentManager;

    private Pick shortPick;

    // The ID of the Pick.
    private Long shortPickId;

    private Long operatorID;

    private OperatorManager operatorManager;

    private LocationManager locationManager;


    private ItemManager itemManager;

    /**
     * @return the operator manager property.
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager The new operator manager.
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Getter for the shortPick property.
     * @return User value of the property
     */
    public Pick getShortPick() {
        return this.shortPick;
    }

    /**
     * Setter for the shortPick property.
     * @param shortPick the new shortPick value
     */
    public void setShortPick(Pick shortPick) {
        this.shortPick = shortPick;
    }

    /**
     * Getter for the shortPickId property.
     * @return Long value of the property
     */
    public Long getShortPickId() {
        return this.shortPickId;
    }

    /**
     * Setter for the shortAssignmentId property.
     * @param shortAssignmentId the new shortAssignmentId value
     */
    public void setShortAssignmentId(Long shortAssignmentId) {
        this.shortPickId = shortAssignmentId;
    }

    /**
     * Getter for the shortManager property.
     * @return UserManager value of the property
     */
    public ShortManager getShortManager() {
        return this.shortManager;
    }

    /**
     * Setter for the shortManager property.
     * @param manager the new shortManager value
     */
    public void setShortManager(ShortManager manager) {
        this.shortManager = manager;
    }

    /**
     * Getter for the assignment management service.
     * @return the assignment manager.
     */
    public AssignmentManager getAssignmentManager() {
        return this.assignmentManager;
    }

    /**
     * Setter for the assignment management service.
     * @param am the new assignment manager.
     */
    public void setAssignmentManager(AssignmentManager am) {
        this.assignmentManager = am;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getShortColumns() {
        return this.columns;
    }

    /**
     * Action for the shorts view page. Initializes the table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View shortView = getUserPreferencesManager().getView(SHORT_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            shortView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the short specified by the <code>short pick</code>
     * member of this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        // TODO
        if (log.isDebugEnabled()) {
            log.debug("Saving short pick");
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it from
     * the database when a shortPickId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getShortManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "short";
    }

    /**
     * Markout the objects identified by the <code>List[] ids</code>.
     * @return SUCCESS control flow target if the markout was successful or if
     *         the specified object wasn't found (with different messages to the
     *         end object)
     * @throws DataAccessException on unanticipated
     */
    public String markout() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "markout";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                getShortManager().executeMarkout((Pick) obj);
                return true;
            }
        });
    }

    /**
     * Accept quantity picked for the objects identified by the
     * <code>List[] ids</code>.
     * @return SUCCESS control flow target if the markout was successful or if
     *         the specified object wasn't found (with different messages to the
     *         end object)
     * @throws DataAccessException on unanticipated
     */
    public String acceptQuantityPicked() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "acceptQuantityPicked";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                getShortManager().executeAcceptQuantityPicked((Pick) obj);
                return true;
            }
        });
    }

    /**
     * Creates a chase assignment from the selected shorted picks.
     * @return The JSON response.
     * @throws DataAccessException on database error.
     */
    public String createChaseAssignment() throws DataAccessException {

        JSONArray jsonSuccessIds = new JSONArray();
        ArrayList<Pick> shorts = new ArrayList<Pick>();
        RemovableDataProvider rdm = (RemovableDataProvider) getManager();
        Long[] ids = getIds();

        // Round up all the shorted picks we need.
        try {
            for (int i = 0; i < ids.length; i++) {
                Pick pick = (Pick) rdm.getDataObject(ids[i]);

                shorts.add(pick);
            }
        } catch (ClassCastException e) {
            // This should never happen.
            log.warn("Casting error while retrieving picks", e);
            setJsonMessage(
                getText(new UserMessage(
                    "short.createChaseAssignment.message.failure.retrieval")),
                ERROR_FAILURE);
            return SUCCESS;
        }

        Assignment newChaseAssignment = generateChaseAssignment();

        if (newChaseAssignment != null) {
            for (Pick shortedPick : shorts) {
                jsonSuccessIds.put(shortedPick.getId());
            }

            NumberFormat nf = NumberFormat.getInstance();
            nf.setGroupingUsed(false);
            setJsonMessage(
                getText(new UserMessage(
                    "short.createChaseAssignment.message.success", nf
                        .format(newChaseAssignment.getNumber()), shorts.size())),
                ERROR_SUCCESS, jsonSuccessIds);
        } else {
            setJsonMessage(
                getText(new UserMessage(
                    "short.createChaseAssignment.message.failure.create")),
                ERROR_FAILURE);
        }

        return SUCCESS;
    }

    /**
     * Generates a new list of picks from their ids.
     * @return list of picks
     * @throws DataAccessException on unexpected
     */
    private ArrayList<Pick> generatePickList() throws DataAccessException {
        ArrayList<Pick> shorts = new ArrayList<Pick>();
        RemovableDataProvider rdm = (RemovableDataProvider) getManager();
        Long[] ids = getIds();

        // Round up all the shorted picks we need.
        try {
            for (int i = 0; i < ids.length; i++) {
                Pick pick = (Pick) rdm.getDataObject(ids[i]);
                if (pick != null) {
                    shorts.add(pick);
                }
            }
        } catch (ClassCastException e) {
            // This should never happen.
            log.warn("Casting error while retrieving picks", e);
            setJsonMessage(
                getText(new UserMessage(
                    "short.createChaseAssignment.message.failure.retrieval")),
                ERROR_FAILURE);
            return null;
        }
        return (shorts);
    }

    /**
     * Generates a new chase assignment from a list of shorts.
     * @return newly created chase Assignment
     * @throws DataAccessException on chase assignment creation
     */
    private Assignment generateChaseAssignment() throws DataAccessException {
        ArrayList<Pick> shorts = generatePickList();
        Assignment newChaseAssignment = null;
        try {
            Operator chaseOperator = null;
            if (this.operatorID != null) {
                chaseOperator = getOperatorManager().get(this.operatorID);
            }
            newChaseAssignment = assignmentManager
                .executeCreateChaseAssignment(shorts, chaseOperator);
        } catch (DataAccessException e) {
            log.error(
                "Error while creating chase assignment",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return null;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while creating chase assignment",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return null;
        }
        return newChaseAssignment;
    }

    /**
     * Validates chase assignment creation and redirects to the operator dialogue box.
     * @return Control string for freemarker
     */
    public String getOperatorDialogue() {
        try {
            ArrayList<Pick> shorts = generatePickList();
            Operator chaseOperator = null;
            if (this.operatorID != null) {
                chaseOperator = getOperatorManager().get(this.operatorID);
            }
            assignmentManager.validateCreateChaseAssignment(shorts, chaseOperator);
        } catch (DataAccessException e) {
            log.error(
                "Error while creating chase assignment",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            setJsonMessage(
                getText(new UserMessage(
                    SystemErrorCode.ENTITY_NOT_UPDATEABLE, UserMessage
                        .markForLocalization("entity."
                            + StringUtils.capitalize(getKeyPrefix())))),
                ERROR_FAILURE);
            return SUCCESS;
        } catch (BusinessRuleException e) {
            setJsonMessage(getText(e.getUserMessage()), ERROR_FAILURE);
            log.error(
                "Error while creating chase assignment",
                SystemErrorCode.ENTITY_NOT_UPDATEABLE, e);
            return SUCCESS;
        }
        return INPUT;
    }

    /**
     * Returns the operator map depending on the selected Shorts in table.
     * @return The operator map
     */
    public Map<Long, String> getOperatorList() {
        Map<Long, String> operatorMap = new LinkedHashMap<Long, String>();
        RemovableDataProvider rdm = (RemovableDataProvider) getManager();

        Long[] ids = getIds();

        try {
            List<Region> regions = new ArrayList<Region>();

            for (Long id : ids) {
                Pick pick = (Pick) rdm.getDataObject(id);
                regions.add(pick.getAssignment().getRegion());
            }

            List<Operator> operators = getOperatorManager().getAll();
            operatorMap.put(0L, getText(new UserMessage("operator.none")));

            for (Operator operator : operators) {
                boolean qualifiedOperator = true;

                for (Region region : regions) {
                    boolean qualifiedForThisRegion = false;
                    if (operator.getWorkgroup() != null) {
                        for (WorkgroupFunction wf : operator.getWorkgroup().getWorkgroupFunctions())
                        {
                            if (wf.getRegions().contains(region)) {
                                qualifiedForThisRegion = true;
                            }
                        }
                    }
                    if (!qualifiedForThisRegion) {
                        qualifiedOperator = false;
                    }
                }

                if (qualifiedOperator) {
                    operatorMap.put(operator.getId(), operator
                        .getOperatorIdentifier());
                }
            }
        } catch (DataAccessException ex) {
            log.warn(ex.getUserMessage());
        }

        return operatorMap;

    }

    /**
     * @return The operator ID property used in chase assignment creation.
     */
    public Long getOperatorID() {
        return operatorID;
    }

    /**
     * @param operatorID The operator to use in chase assignment creation.
     */
    public void setOperatorID(Long operatorID) {
        if (operatorID == 0) {
            this.operatorID = null;
        } else {
            this.operatorID = operatorID;
        }
    }

    /**
     * Getter for the SHORT_VIEW_ID property.
     * @return long value of the property
     */
    public static long getShortViewId() {
        return SHORT_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(SHORT_ID);
        return viewIds;
    }

    /**
     * Sets selected items location status to Replenished.
     * @return Success - on success
     * @throws DataAccessException on database failure.
     */
    public String setToReplenished() throws DataAccessException {
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "setToReplenished";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                Pick pick = (Pick) obj;
                Location li = pick.getLocation();
                Item ite = pick.getItem();
                getLocationManager().executeReportReplenished(li, ite);
                return true;
            }
        });
    }

    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    public ItemManager getItemManager() {
        return this.itemManager;
    }


    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return this.locationManager;
    }


    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 