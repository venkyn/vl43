/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;

import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentPickManager;

import com.opensymphony.xwork2.Preparable;

import java.util.LinkedList;
import java.util.List;


/**
 *
 *
 * @author mnichols
 */
public class AssignmentPickActionRoot extends DataProviderAction implements Preparable {


    //
    private static final long serialVersionUID = 6501267566202627089L;

    private static final Logger log = new Logger(PickAction.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -1010;

    // The list of columns for the picks table.
    private List<Column> columns;

    // The Assignment-Pick management service.
    private AssignmentPickManager assignmentPickManager = null;

    // A comma separated String of IDs to query on
    private String assignmentID = null;

    // The Pick object, which will either be newly created, or retrieved
    // via the PickId.
    private Pick pick;

    // The ID of the Pick.
    private Long pickId;

    /**
     * Getter for the user property.
     * @return User value of the property
     */
    public Pick getPick() {
        return this.pick;
    }

    /**
     * Setter for the user property.
     * @param user the new user value
     */
    public void setPick(Pick user) {
        this.pick = user;
    }

    /**
     * Getter for the userId property.
     * @return Long value of the property
     */
    public Long getPickId() {
        return this.pickId;
    }

    /**
     * Setter for the userId property.
     * @param userId the new userId value
     */
    public void setPickId(Long userId) {
        this.pickId = userId;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public AssignmentPickManager getAssignmentPickManager() {
        return this.assignmentPickManager;
    }

    /**
     * Setter for the userManager property.
     * @param manager the new userManager value
     */
    public void setAssignmentPickManager(AssignmentPickManager manager) {
        this.assignmentPickManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getPickColumns() {
        return this.columns;
    }

    /**
     * Action for the picks view page. Initializes the picks table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View pickView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(pickView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Create or update the user specified by the <code>user</code> member of
     * this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        // TODO
        if (log.isDebugEnabled()) {
            log.debug("Saving Pick");
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it
     * from the database when a userId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        // TODO
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getAssignmentPickManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "AssignmentPick";
    }


    /**
     * Getter for the assignmentID property.
     * @return the assigmentID
     */
    public String getAssignmentID() {
        return assignmentID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        if (getAssignmentID() != null && getAssignmentID().length() > 0) {
            super.getTableData(
                "obj.assignment.id in ( " + this.assignmentID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getPrintData()
     */
    @Override
    public String getPrintData() throws Exception {
        if (getAssignmentID() != null && getAssignmentID().length() > 0) {
            super.getPrintData(
                "obj.assignment.id in ( " + this.assignmentID + " )");
        } else {
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;
    }

    /**
     * Setter for the assignmentID property.
     * @param assignmentID - the id to be set
     */
    public void setAssignmentID(String assignmentID) {
        this.assignmentID = assignmentID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 