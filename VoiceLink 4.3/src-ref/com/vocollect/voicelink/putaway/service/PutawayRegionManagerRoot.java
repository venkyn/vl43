/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.putaway.dao.PutawayRegionDAO;
import com.vocollect.voicelink.putaway.model.PutawayRegion;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Putaway-related operations.
 *
 * @author mkoenig
 */
public interface PutawayRegionManagerRoot extends
    GenericManager<PutawayRegion, PutawayRegionDAO>, DataProvider {

    /**
     * retrieves the region specified.
     *
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    PutawayRegion findRegionByNumber(int regionNumber)
        throws DataAccessException;


    /**
     *
     * @return the average of the goal rates for all replenishment regions
     * @throws DataAccessException - database failure
     */
    public Double avgGoalRate() throws DataAccessException;


    /**
     * Returns the Long of the id, or null if not found.
     * @param regionNumber - the region number to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByNumber(int regionNumber)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionName - the name of the region to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByName(String regionName)
        throws DataAccessException;


    /**
     * Validate the business rules for editing a putaway region region.
     * @param region - region to edit
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    void executeValidateEditRegion(PutawayRegion region)
        throws DataAccessException, BusinessRuleException;


    /**
     * Validate the business rules for deleteting a putaway region.
     * @param region - region to edit
     * @throws DataAccessException - indicates database error
     * @throws BusinessRuleException - thrown in business rule violated
     */
    void executeValidateDeleteRegion(PutawayRegion region)
        throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 