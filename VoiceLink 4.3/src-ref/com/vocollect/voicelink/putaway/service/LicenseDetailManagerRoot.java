/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.putaway.dao.LicenseDetailDAO;
import com.vocollect.voicelink.putaway.model.LicenseDetail;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Putaway-related operations.
 *
 * @author mkoenig
 */
public interface LicenseDetailManagerRoot extends
    GenericManager<LicenseDetail, LicenseDetailDAO>, DataProvider {


    /**
     * Get all license detail records for an operator ordered by putTime.
     * @param operator to work on
     * @param startDate - date must be greater than start date
     * @return licenseDetail records
     * @throws DataAccessException for any database exception
     */
    List<LicenseDetail> listMostRecentRecordsByOperator(Operator operator,
                                                        Date startDate)
                                                  throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 