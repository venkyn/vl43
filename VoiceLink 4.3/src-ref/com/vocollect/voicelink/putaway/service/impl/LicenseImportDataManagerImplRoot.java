/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.putaway.dao.LicenseImportDataDAO;
import com.vocollect.voicelink.putaway.model.LicenseImportData;
import com.vocollect.voicelink.putaway.service.LicenseImportDataManager;

import java.util.List;



/**
 * Additional service methods for the <code>LicenseImportData</code> model object.
 * @author jtauberg
 */
public abstract class LicenseImportDataManagerImplRoot extends
        GenericManagerImpl<LicenseImportData, LicenseImportDataDAO> implements
        LicenseImportDataManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LicenseImportDataManagerImplRoot(LicenseImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     */
    public List<LicenseImportData> listLicenseBySiteName(String siteName)
        throws DataAccessException {
            return getPrimaryDAO().listLicenseBySiteName(siteName);
    }

    /**
     *  {@inheritDoc}
     */
    public void updateToComplete() throws DataAccessException {
        getPrimaryDAO().updateToComplete();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 