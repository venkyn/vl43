/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.putaway.dao.LicenseDAO;
import com.vocollect.voicelink.putaway.service.PutawayExportTransportManager;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

/**
 *
 * @author vsubramani
 *
 */
public abstract class PutawayExportTransportManagerImplRoot extends LicenseManagerImpl
        implements PutawayExportTransportManager {

    //Number of records to process at a time
    private static final int BATCH_SIZE = 50;

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PutawayExportTransportManagerImplRoot(LicenseDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     *
     */
    public List<Map<String, Object>> listExportPutaway() throws DataAccessException {
        QueryDecorator qd = new QueryDecorator();
        qd.setRowCount(BATCH_SIZE);

        List<Map<String, Object>> rawData = this.getPrimaryDAO()
                .listExportPutawayRecords(qd);

        //Stuff to format timestamps
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        sdf.setLenient(false);
        String formattedDateTime = "";
        Object myObj = null;

        //Loop through returned data looking for dates... if date is found,
        // format it.
        for (Map <String, Object> m : rawData) {
            //Get all the keys for this map.
            for (String keyObj : m.keySet()) {
                //Get the object associated to this key and see if it's a date
                myObj = m.get(keyObj);
                if (null == myObj) {
                    continue;
                }
                if (myObj instanceof java.util.Date) {
                    //It is a date!
                    if (myObj == null) {
                        // If null replace with "No Timestamp"
                        formattedDateTime = "No Timestamp";
                    } else {
                        //it isn't null so format as Vocollect yyyyMMddHHmmss
                        formattedDateTime = sdf.format(myObj);
                    } // end if null
                    //Replace the key and date with key and formatted string
                    m.put(keyObj, formattedDateTime);
                } //end if class is date
            } // end for obj
        } //end for map
        return rawData;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 