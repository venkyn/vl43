/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.impl.VoiceLinkPurgeArchiveManagerImpl;
import com.vocollect.voicelink.putaway.dao.LicenseDAO;
import com.vocollect.voicelink.putaway.dao.PutawayLicenseSummaryDAO;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.putaway.model.PutawayLicenseSummary;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.LicenseManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Additional service methods for the <code>License</code> model object.
 *
 * @author mkoenig
 */
public class LicenseManagerImplRoot extends
    GenericManagerImpl<License, LicenseDAO> implements LicenseManager {


    // this is here so we do not have to have a separate manager for the summary method.
    private PutawayLicenseSummaryDAO putawayLicenseSummaryDAO;

    private static final int REGION = 0;
    private static final int TOTAL_LICENSES = 1;
    private static final int IN_PROGRESS_LICENSES = 2;
    private static final int AVAILABLE_LICENSES = 3;
    private static final int COMPLETED_LICENSES = 4;
    private static final int NON_COMPLETED_LICENSES = 5;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(VoiceLinkPurgeArchiveManagerImpl.class);

    /**
     * Getter for the putawayLicenseSummaryDAO property.
     * @return PutawayLicenseSummaryDAO value of the property
     */
    public PutawayLicenseSummaryDAO getPutawayLicenseSummaryDAO() {
        return this.putawayLicenseSummaryDAO;
    }


    /**
     * Setter for the putawayLicenseSummaryDAO property.
     * @param putawayLicenseSummaryDAO the new putawayLicenseSummaryDAO value
     */
    public void setPutawayLicenseSummaryDAO(PutawayLicenseSummaryDAO putawayLicenseSummaryDAO) {
        this.putawayLicenseSummaryDAO = putawayLicenseSummaryDAO;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LicenseManagerImplRoot(LicenseDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManager#findByNumber(java.lang.String)
     */
    public License findByNumber(String licenseNumber) throws DataAccessException {
        return getPrimaryDAO().findByNumber(licenseNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManager#findLikeNumber(java.lang.String)
     */
    public List<License> listLikeNumber(String licenseNumber) throws DataAccessException {
        return getPrimaryDAO().listLikeNumber(licenseNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManager#listInprogressLicenses(com.vocollect.voicelink.core.model.Operator)
     */
    public List<License> listInprogressLicenses(Long operatorId) throws DataAccessException {
        return getPrimaryDAO().listInprogressLicenses(operatorId);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManagerRoot#listLicenseCountsByRegionAndStatus()
     */
    public List<DataObject> listLicenseCountsByRegionAndStatus(ResultDataInfo rdi) throws DataAccessException {

        Object[] queryArgs = rdi.getQueryArgs();
        // Retrieve it all.
        List<Object[]> data = getPutawayLicenseSummaryDAO().listLicenseCountsByRegionAndStatus((Date) queryArgs[0]);

        ArrayList<DataObject> newList = new ArrayList<DataObject>(data.size());
        for (Object[] objArray : data) {
            PutawayLicenseSummary summaryObject = new PutawayLicenseSummary();
            // get the region object and figure out which site we are asking for.
            summaryObject.setRegion((PutawayRegion) objArray[REGION]);
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            Site theSite =  siteContext.getSite(summaryObject.getRegion());
            summaryObject.setSite(theSite);
            summaryObject.setTotalLicenses(
                convertNumberToInt(objArray[TOTAL_LICENSES]));
            summaryObject.setInProgressLicenses(
                convertNumberToInt(objArray[IN_PROGRESS_LICENSES]));
            summaryObject.setAvailableLicenses(
                convertNumberToInt(objArray[AVAILABLE_LICENSES]));
            summaryObject.setCompletedLicenses(
                convertNumberToInt(objArray[COMPLETED_LICENSES]));
            summaryObject.setNonCompletedLicenses(
                convertNumberToInt(objArray[NON_COMPLETED_LICENSES]));
            newList.add(summaryObject);
        }
        return newList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date, com.vocollect.voicelink.putaway.model.LicenseStatus)
     *
     */
    public List<License> listOlderThan(QueryDecorator decorator,
        Date date, LicenseStatus status) throws DataAccessException {
        return getPrimaryDAO().listOlderThan(decorator, date, status);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.replenishment.service.ReplenishmentManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public List<License> listMostRecentRecordsByOperator(Operator operator, Date startTime, Date endTime)
                                                         throws DataAccessException {
        return this.getPrimaryDAO().listMostRecentRecordsByOperator(operator, startTime, endTime);
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManagerRoot#listRecordsByOperatorGreaterThanDate(com.vocollect.voicelink.core.model.Operator, java.util.Date)
     */
    public List<License> listRecordsByOperatorGreaterThanDate(Operator operator, Date startTime)
                                                              throws DataAccessException {
        return this.getPrimaryDAO().listRecordsByOperatorGreaterThanDate(operator, startTime);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, com.vocollect.voicelink.putaway.model.LicenseStatus, java.util.Date)
     */
    public int executePurge(QueryDecorator decorator, LicenseStatus status, Date olderThan) {
        int returnRecords = 0;
        List<License> licenses = null;

        //Get List of PutAway Assignments to Purge
        if (log.isDebugEnabled()) {
            log.debug("### Finding Put Away License - " + status.toString() + " to Purge :::");
        }
        try {
            licenses = listOlderThan(decorator, olderThan, status);
            returnRecords = licenses.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting PutAway Licenses "
                + "from database to purge: " + e
                );
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords + " PutAway License - " + status.toString() + "  for purge :::");
            log.debug("### Starting PutAway Licenses Purge Process :::");
        }
        for (License l : licenses) {
            try {
                delete(l);
            } catch (Throwable t) {
                log.warn("!!! Error purging transactional Put Away License - "
                    + l.getNumber() + " from database: " + t
                    , t);
            }
        }
        if (log.isDebugEnabled()) {
            log.debug("### PutAway Licenses Purge Process Complete :::");
        }
        getPrimaryDAO().clearSession();
        return returnRecords;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 