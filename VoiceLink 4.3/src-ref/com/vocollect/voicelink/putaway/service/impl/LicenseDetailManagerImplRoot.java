/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.putaway.dao.LicenseDetailDAO;
import com.vocollect.voicelink.putaway.model.LicenseDetail;
import com.vocollect.voicelink.putaway.service.LicenseDetailManager;

import java.util.Date;
import java.util.List;



/**
 * Additional service methods for the <code>LicenseDetail</code> model object.
 *
 * @author mkoenig
 */
public class LicenseDetailManagerImplRoot extends
    GenericManagerImpl<LicenseDetail, LicenseDetailDAO>
    implements LicenseDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LicenseDetailManagerImplRoot(LicenseDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.LicenseDetailManagerRoot#listMostRecentRecordsByOperator(com.vocollect.voicelink.core.model.Operator)
     */
    public List<LicenseDetail> listMostRecentRecordsByOperator(Operator operator,
                                                               Date startDate)
                                                               throws DataAccessException {
        return this.getPrimaryDAO().listMostRecentRecordsByOperator(operator, startDate);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 