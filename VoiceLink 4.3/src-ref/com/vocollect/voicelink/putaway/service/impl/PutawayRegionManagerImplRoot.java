/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.putaway.PutawayErrorCode;
import com.vocollect.voicelink.putaway.dao.PutawayRegionDAO;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;

import java.util.List;

import org.hibernate.exception.ConstraintViolationException;



/**
 * Additional service methods for the <code>PutawayRegion</code> model object.
 *
 * @author mkoenig
 */
public class PutawayRegionManagerImplRoot extends
    GenericManagerImpl<PutawayRegion, PutawayRegionDAO> implements PutawayRegionManager {

    private WorkgroupManager workgroupManager;
    private RegionManager regionManager;

    private OperatorDAO operatorDAO;


    /**
     * Getter for the operatorDAO property.
     * @return OperatorDAO value of the property
     */
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }


    /**
     * Setter for the operatorDAO property.
     * @param operatorDAO the new operatorDAO value
     */
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PutawayRegionManagerImplRoot(PutawayRegionDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for the workgroupManager property.
     * @return WorkgroupManager value of the property
     */
    public WorkgroupManager getWorkgroupManager() {
        return this.workgroupManager;
    }

    /**
     * Setter for the workgroupManager property.
     * @param workgroupManager the new workgroupManager value
     */
    public void setWorkgroupManager(WorkgroupManager workgroupManager) {
        this.workgroupManager = workgroupManager;
    }

    /**
     * Implementation to get specified region.
     *
     * {@inheritDoc}
     * @see com.vocollect.voicelink.selection.service.RegionManager#findRegionByNumber(int)
     */
    public PutawayRegion findRegionByNumber(int regionNumber)
        throws DataAccessException {
        return getPrimaryDAO().findRegionByNumber(regionNumber);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(PutawayRegion region)
        throws BusinessRuleException, DataAccessException {

        getRegionManager().verifyUniqueness(region);
        // Business rule: If the region being saved is new,
        // cycle through the Workgroups where autoaddregions is true
        // and save the regions to all the putaway task functions
        if (region.isNew()) {
            addToWorkgroups(region);
        }

        return super.save(region);
    }

    /**
     * Take the passed in Putaway Region and add it to all the
     * workgroups where AutoAddRegions is turned on and the task
     * function is a putaway task function.
     * @param region - a putaway region
     * @throws DataAccessException - any database exception
     */
    protected void addToWorkgroups(PutawayRegion region)
        throws DataAccessException {

        List<Workgroup> wgs = workgroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : wgs) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.PutAway) {
                    wgf.getRegions().add(region);
                }
            }
        }
    }


    /**
     *
     * @return the average of the goal rates for all putaway regions
     * @throws DataAccessException - database failure
     */
    public Double avgGoalRate() throws DataAccessException {
            return getPrimaryDAO().avgGoalRate();

    }

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionNumber - the region number to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByNumber(int regionNumber) throws DataAccessException {
        return getPrimaryDAO().uniquenessByNumber(regionNumber);
    }

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionName - the name of the region to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByName(String regionName) throws DataAccessException {
        return getPrimaryDAO().uniquenessByName(regionName);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.PutawayRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.selection.model.SelectionRegion)
     */
    public void executeValidateEditRegion(PutawayRegion region) throws DataAccessException, BusinessRuleException {

        // if we are editing a region verify that no operators are signed in.
        if (!region.isNew()) {
            if (!verifyNoOperatorsSignedIn(region)) {
                throw new BusinessRuleException(PutawayErrorCode.REGION_OPERATORS_SIGNED_IN,
                    new UserMessage("putawayregion.edit.error.operatorsSignedIn"));
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.service.PutawayRegionManagerRoot#executeValidateEditRegion(com.vocollect.voicelink.selection.model.SelectionRegion)
     */
    public void executeValidateDeleteRegion(PutawayRegion region) throws DataAccessException, BusinessRuleException {

        // verify that no operators are signed into the region before deleteing
        if (!verifyNoOperatorsSignedIn(region)) {
            throw new BusinessRuleException(PutawayErrorCode.REGION_OPERATORS_SIGNED_IN,
                new UserMessage("putawayregion.delete.error.operatorsSignedIn"));
           }
    }

    /**
     *
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(PutawayRegion instance)
        throws BusinessRuleException, DataAccessException {
        try {
            executeValidateDeleteRegion(instance);
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("putawayRegion.delete.error.inUse");
            } else {
                throw ex;
        }
    }
        return null;
    }


    /**
     * There can be no operators signed into the region when editing or deleting.
     *
     * @param region - to be persisted or deleted
     * @return true if no operators are signed into region otherwise return false
     * @throws DataAccessException - indicates a database error
     * @throws BusinessRuleException - if business rule violated
     */
    protected boolean verifyNoOperatorsSignedIn(PutawayRegion region)
        throws BusinessRuleException, DataAccessException {

        if (this.getOperatorDAO().
                countNumberOfOperatorsSignedIn(region).intValue() > 0) {
           return false;
        }
        return true;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 