/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.putaway.dao.LicenseDAO;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseStatus;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Putaway-related operations.
 *
 * @author mkoenig
 */
public interface LicenseManagerRoot extends
    GenericManager<License, LicenseDAO>, DataProvider {

    /**
     * Retrieves an license by number string, or returns null is there is no such
     * license.
     *
     * @param licenseNumber - the number of the License
     * @return the requested License, or null
     * @throws DataAccessException on any failure.
     */
    License findByNumber(String licenseNumber) throws DataAccessException;

    /**
     * Rerieves a list of license that are like the specified value.
     *
     * @param licenseNumber - the number of the License
     * @return the requested License, or null
     * @throws DataAccessException on any failure.
     */
    List<License> listLikeNumber(String licenseNumber) throws DataAccessException;

    /**
     * Rerieves a list of license that are inprogess for operator.
     *
     * @param operatorId - operatorId to find
     * @return the requested Licenses, or null
     * @throws DataAccessException on any failure.
     */
    List<License> listInprogressLicenses(Long operatorId) throws DataAccessException;



    /**
     * Get the summary counts of the Putaway licenses.
     * @param rdi the ResultDataInfo object.
     * @return - return list of summary information
     * @throws DataAccessException - database exceptions
     */
    List<DataObject> listLicenseCountsByRegionAndStatus(ResultDataInfo rdi) throws DataAccessException;

    /**
     * Gets all Licenses older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the LicenseStatus used in the query
     * @return a list of Licenses
     * @throws DataAccessException Database failure
     */
    public List<License> listOlderThan(QueryDecorator decorator,
        Date date, LicenseStatus status) throws DataAccessException;

    /**
     * Get all InProgress and Completed License records
     * for this operator.
     * @param operator to search
     * @param startTime of labor record
     * @param endTime of labor record
     * @return list of License InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<License> listMostRecentRecordsByOperator(Operator operator,
                                                  Date startTime,
                                                  Date endTime)
                                                  throws DataAccessException;

    /**
     * Get all InProgress and Completed License records
     * for this operator.
     * @param operator to search
     * @param startTime of labor record
     * @return list of License InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<License> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                       Date startTime)
                                                       throws DataAccessException;

    /**
     * Purges Licenses based on date and status.
     *
     * @param decorator - query decorator
     * @param status - status to purge
     * @param olderThan - date to purge by
     * @return - number of licenses purged
     */
    public int executePurge(QueryDecorator decorator, LicenseStatus status,
                                       Date olderThan);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 