/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway;

import com.vocollect.epp.errors.ErrorCode;


/**
 * Class to hold the instances of error codes for selection business. Although the
 * range for these error codes is 4000-4999, please use only 4000-4499 for
 * the predefined ones; 4500-4999 should be reserved for customization error
 * codes.
 *
 * @author mkoenig
 */
public class PutawayErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the task error code range.
     */
    public static final int LOWER_BOUND  = 4000;

    /**
     * Upper boundary of the task error code range.
     */
    public static final int UPPER_BOUND  = 4999;

    /**
     * No error, just the base initialization.
     */
    public static final PutawayErrorCodeRoot NO_ERROR
        = new PutawayErrorCodeRoot();



    /**
     * Constructor.
     */
    private PutawayErrorCodeRoot() {
        super("VoiceLink", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected PutawayErrorCodeRoot(long err) {
        // The error is defined in the TaskErrorCode context.
        super(PutawayErrorCodeRoot.NO_ERROR, err);
    }




    /**
     * Assignment not available to be issued.
     */
    public static final PutawayErrorCode REGION_OPERATORS_SIGNED_IN
    = new PutawayErrorCode(4000);


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 