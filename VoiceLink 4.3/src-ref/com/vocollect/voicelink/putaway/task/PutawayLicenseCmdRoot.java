/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.ReasonCodeManager;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseDetail;
import com.vocollect.voicelink.putaway.model.LicenseDetailStatus;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 *
 *
 * @author mkoenig
 */
public class PutawayLicenseCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private static final int PUT_INDICATOR_PARTIAL = 0;
    private static final int PUT_INDICATOR_CANCELED = 2;
    private static final int PUT_INDICATOR_RELEASED = 3;

    private String            licenseNumber;

    private String            quantityPut;

    private String            locationId;

    private Integer           putIndicator;

    private String            reasonCode;

    private String            startTime;

    private LocationManager   locationManager;

    private LaborManager      laborManager;

    private ReasonCodeManager reasonCodeManager;


    /**
     * Getter for the reason code  property.
     * @return Integer value of the property
     */
    public Integer getReasonCodeAsInteger() {
        if (StringUtil.isNullOrEmpty(this.reasonCode)) {
            return null;
        } else {
            return Integer.parseInt(this.reasonCode);
        }
    }

    /**
     * Getter for the reasonCodeManager property.
     * @return ReasonCodeManager value of the property
     */
    public ReasonCodeManager getReasonCodeManager() {
        return this.reasonCodeManager;
    }

    /**
     * Setter for the reasonCodeManager property.
     * @param reasonCodeManager the new reasonCodeManager value
     */
    public void setReasonCodeManager(ReasonCodeManager reasonCodeManager) {
        this.reasonCodeManager = reasonCodeManager;
    }

    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * Getter for the locationId property.
     * @return Long value of the property
     */
    public Long getLocationIdLong() {
        if (!StringUtil.isNullOrEmpty(getLocationId())) {
            return Long.valueOf(getLocationId());
        } else {
            return null;
        }
    }

    /**
     * Getter for the locationId property.
     * @return String value of the property
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * Setter for the locationId property.
     * @param locationId the new locationId value
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    /**
     * Getter for the putIndicator property.
     * @return Integer value of the property
     */
    public Integer getPutIndicator() {
        return putIndicator;
    }

    /**
     * Setter for the putIndicator property.
     * @param putIndicator the new putIndicator value
     */
    public void setPutIndicator(Integer putIndicator) {
        this.putIndicator = putIndicator;
    }

    /**
     * Getter for the quantityPut property.
     * @return Integer value of the property
     */
    public Integer getQuantityPutInt() {
        if (!StringUtil.isNullOrEmpty(getQuantityPut())) {
            return Integer.valueOf(getQuantityPut());
        } else {
            return null;
        }
    }

    /**
     * Getter for the quantityPut property.
     * @return String value of the property
     */
    public String getQuantityPut() {
        return quantityPut;
    }

    /**
     * Setter for the quantityPut property.
     * @param quantityPut the new quantityPut value
     */
    public void setQuantityPut(String quantityPut) {
        this.quantityPut = quantityPut;
    }

    /**
     * Getter for the reasonCode property.
     * @return String value of the property
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Setter for the reasonCode property.
     * @param reasonCode the new reasonCode value
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * Getter for the startTime property.
     * @return String value of the property
     */
    public Date getStartTimeAsDate() {
        
        Site currentSite = SiteContextHolder.getSiteContext().getCurrentSite();
        TimeZone timeZone = currentSite.getTimeZone();
        
        SimpleDateFormat formatter =
            new SimpleDateFormat(BaseTaskCommand.TASK_COMMAND_DATE_FORMAT);
        
        if (timeZone != null) {
            formatter.setTimeZone(timeZone);
        }
        try {
            return formatter.parse(getStartTime());
        } catch (ParseException e) {
            return null;
        }    
    }

    /**
     * Getter for the startTime property.
     * @return String value of the property
     */
    public String getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        addNewDetail();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * @throws DataAccessException - Database Exception
     * @throws TaskCommandException - Task Command Exception
     * @throws BusinessRuleException - Business Rule Exceptions
     */
    protected void addNewDetail()
    throws DataAccessException, TaskCommandException, BusinessRuleException {
        License license = getLicenseManager().findByNumber(getLicenseNumber());

        if (license.getStatus() == LicenseStatus.Complete) {
            throw new TaskCommandException(
                TaskErrorCode.LICENSE_ALREADY_COMPLETE, getLicenseNumber());
        }

        //If Released
        if (getPutIndicator() == PUT_INDICATOR_RELEASED) {
            updateLicense(license, null);
        } else {
            //Create a new details record
            LicenseDetail detail = new LicenseDetail();
            //Canceled
            if (getPutIndicator() == PUT_INDICATOR_CANCELED) {
                detail.setStatus(LicenseDetailStatus.Canceled);
            } else if (getPutIndicator() == PUT_INDICATOR_PARTIAL) {
                detail.setStatus(LicenseDetailStatus.Put);
            } else {
                detail.setStatus(LicenseDetailStatus.Put);
            }

            detail.setStartLocation(getOperator().getLastLocation());

            if (getLocationIdLong() != null) {
                detail.setPutLocation(getLocationManager().get(getLocationIdLong()));
            }

            if (detail.getStatus() == LicenseDetailStatus.Canceled) {
                detail.setQuantity(0);
            } else {
                detail.setQuantity(getQuantityPutInt());
            }
            detail.setOperator(getOperator());
            detail.setStartTime(getStartTimeAsDate());
            detail.setPutTime(this.getCommandTime());

            if (StringUtil.isNullOrEmpty(this.getReasonCode())) {
                detail.setReason(null);
            } else {
                detail.setReason(this.getReasonCodeManager().findByReasonNumber(
                    this.getReasonCodeAsInteger()));
            }
            //Update License
            updateLicense(license, detail);
            getLicenseDetailManager().save(detail);

            //Update Operator
            if (getLocationIdLong() != null) {
                getOperator().setLastLocation(detail.getPutLocation());
            }
        }
    }

    
    
        
    /**
     * Update license information.
     *
     * @param license - license to update
     * @param detail - detail to update with
     * @throws DataAccessException - database error
     * @throws BusinessRuleException - error on save
     */
    protected void updateLicense(License license, LicenseDetail detail)
    throws DataAccessException, BusinessRuleException {
        if (detail == null) {
            license.setStatus(LicenseStatus.Available);
            license.setOperator(null);
        } else {
            //Not a partial
            if (getPutIndicator() == PUT_INDICATOR_PARTIAL) {
                license.setStatus(LicenseStatus.InProgress);
            } else {
                license.setPutTime(getCommandTime());
                license.setStatus(LicenseStatus.Complete);
            }
            license.setOperator(getOperator());
            if (license.getStartTime() == null) {
                license.setStartTime(getStartTimeAsDate());
            }
            // If the license detail wasn't canceled, added the quantity to the license.

            if ((getQuantityPutInt() != null) && detail.getStatus() != LicenseDetailStatus.Canceled) {
                license.setQuantityPutaway(license.getQuantityPutaway() + getQuantityPutInt());
            }

            if (license.getStatus() == LicenseStatus.Complete) {
                this.getLaborManager().updatePutAwayLaborStatistics(getOperator(), license);
            }
            license.addLicenseDetail(detail);

            // if the license wasn't imported then it was created on the fly.
            // update the fields that need populated.
            if (!license.isImported()) {
                license.setLocation(detail.getPutLocation());
                license.setPutTime(this.getCommandTime());
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 