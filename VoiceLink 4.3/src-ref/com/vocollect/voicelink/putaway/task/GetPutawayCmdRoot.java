/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 *
 * @author mkoenig
 */
public class GetPutawayCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private List<License>   licenses;


    /**
     * Getter for the licenses property.
     * @return List&lt;License&gt; value of the property
     */
    public List<License> getLicenses() {
        return licenses;
    }

    /**
     * Setter for the licenses property.
     * @param licenses the new licenses value
     */
    public void setLicenses(List<License> licenses) {
        this.licenses = licenses;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildLicenseList();
        buildResponse();
        return getResponse();
    }


    /**
     * get list of license to return.
     *
     * @throws DataAccessException - database exceptions
     * @throws TaskCommandException - task command exceptions
     */
    protected void buildLicenseList()
    throws DataAccessException, TaskCommandException {
        setLicenses(getLicenseManager().listInprogressLicenses(getOperator().getId()));

        if (getLicenses().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.NO_LICENSE_FOUND_FOR_OPERATOR);
        }
    }
    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        for (License l : getLicenses()) {
            getResponse().addRecord(buildResponseRecord(l));
        }
    }

    /**
     * Build Response Record.
     *
     * @param l - license to create record for
     * @return - return response record
     * @throws DataAccessException - Database Exceptions
     */
    protected ResponseRecord buildResponseRecord(License l)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("licenseNumber", l.getNumber());
        record.put("regionNumber", l.getRegion().getNumber());

        // A putaway license does not have to have a location
        // therefore, test for null -- if null, don't populate
        // the location values
        if (l.getLocation() != null) {
            record.put("locationId", l.getLocation().getId());
            record.put("scannedLocation",
                l.getLocation().getScannedVerification());
            record.put("preAisleDirection",
                translateUserData(l.getLocation().getDescription().getPreAisle()));
            record.put("aisle",
                l.getLocation().getDescription().getAisle());
            record.put("postAisleDirection",
                translateUserData(l.getLocation().getDescription().getPostAisle()));
            record.put("slot",
                l.getLocation().getDescription().getSlot());
            record.put("checkDigits",
                l.getLocation().getCheckDigits());
        }

        if (l.getItem() != null) {
            record.put("itemNumber", l.getItem().getNumber());
            record.put("itemDescription",
                getTranslatedItemPhoneticDescription(l.getItem()));
        }
        record.put("quantity", l.getQuantityReceived());
        record.put("goalTime", l.getGoalTime());

        return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 