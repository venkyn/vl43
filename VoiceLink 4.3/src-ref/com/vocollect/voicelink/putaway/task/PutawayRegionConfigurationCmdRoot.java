/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 *
 *
 * @author mkoenig
 */
public class PutawayRegionConfigurationCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private PutawayRegionManager  putawayRegionManager;

    private List<PutawayRegion>   returnRegions = new ArrayList<PutawayRegion>();

    private LaborManager          laborManager;


    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return this.laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the returnRegions property.
     * @return List&lt;PutawayRegion&gt; value of the property
     */
    public List<PutawayRegion> getReturnRegions() {
        return returnRegions;
    }

    /**
     * Setter for the returnRegions property.
     * @param returnRegions the new returnRegions value
     */
    public void setReturnRegions(List<PutawayRegion> returnRegions) {
        this.returnRegions = returnRegions;
    }

    /**
     * Getter for the putawayRegionManager property.
     * @return PutawayRegionManager value of the property
     */
    public PutawayRegionManager getPutawayRegionManager() {
        return putawayRegionManager;
    }

    /**
     * Setter for the putawayRegionManager property.
     * @param putawayRegionManager the new putawayRegionManager value
     */
    public void setPutawayRegionManager(PutawayRegionManager putawayRegionManager) {
        this.putawayRegionManager = putawayRegionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildReturnRegionList();
        if (getReturnRegions().isEmpty()) {
            throw new TaskCommandException(
                TaskErrorCode.REGIONS_NO_LONGER_VALID);
        }
        setOperatorLastLocation();
        buildResponse();
        this.getLaborManager().openOperatorLaborRecord(this.getCommandTime(),
                               this.getOperator(), OperatorLaborActionType.PutAway);

        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return

        for (PutawayRegion r : getReturnRegions()) {
            getResponse().addRecord(buildResponseRecord(r));
        }
    }

    /**
     * Build response record.
     *
     * @param r - region to build record for
     * @return - response record.
     * @throws DataAccessException - Database exceptions
     */
    protected ResponseRecord buildResponseRecord(PutawayRegion r)
    throws DataAccessException {
        ResponseRecord record = new TaskResponseRecord();

        record.put("regionNumber", r.getNumber());
        record.put("regionName", translateUserData(r.getName()));
        record.put("captureStartLocation", r.getCaptureStartLocation().toValue());
        record.put("allowSkipLicense", r.getAllowSkipLicense());
        record.put("allowCancelLicense", r.getAllowCancelLicense());
        record.put("allowOverrideLocation", r.getAllowOverrideLocation());
        record.put("allowOverrideQuantity", r.getAllowOverrideQuantity());
        record.put("allowPartialPut", r.getAllowPartialPut());
        record.put("capturePickUpQuantity", r.getCapturePickupQuantity());
        record.put("capturePutQuantity", r.getCapturePutQuantity());
        record.put("licenseDigitsTaskSpeaks", r.getLicDigitsTaskSpeaks());
        record.put("licenseDigitsOperatorSpeaks", r.getLicDigitsOperatorSpeaks());
        record.put("locationDigitsOperatorSpeaks", r.getLocDigitsOperatorSpeaks());
        record.put("numberCheckDigitsOperatorSpeaks", r.getCheckDigitsOperatorSpeaks());
        record.put("numberLicenseAllowed", r.getNumberOfLicensesAllowed());
        record.put("exceptionLocation",
                   translateUserData(r.getExceptionLocation()));
        record.put("verifySpokenLicenseLocation", r.getVerifySpokenLicOrLoc());

        return record;
    }

    /**
     * Build a list of regions to return.
     *
     * @throws DataAccessException - Database exception
     */
    protected void buildReturnRegionList() throws DataAccessException {
        Set<Region> regions = getOperator().getRegions();

        for (Region r : regions) {
            if (regions.size() == 1 || ((PutawayRegion) r).getVerifyLicense()) {
                getReturnRegions().add((PutawayRegion) r);
            }
        }

        if (getReturnRegions().isEmpty() && !regions.isEmpty()) {
            getReturnRegions().add((PutawayRegion) regions.iterator().next());
        }

    }

    /**
     * Sets the operators last location to null.
     *
     * @throws DataAccessException - database exception
     */
    protected void setOperatorLastLocation() throws DataAccessException {
        getOperator().setLastLocation(null);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 