/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.List;


/**
 *
 *
 * @author mkoenig
 */
public class RequestPutawayRegionCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private PutawayRegionManager  putawayRegionManager;

    private boolean               allRegions;

    private String                regionNumber;

    private RegionManager         regionManager;

    /**
     * Getter for the allRegions property.
     * @return boolean value of the property
     */
    public boolean getAllRegions() {
        return allRegions;
    }


    /**
     * Setter for the allRegions property.
     * @param allRegions the new allRegions value
     */
    public void setAllRegions(boolean allRegions) {
        this.allRegions = allRegions;
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public Integer getRegionNumberInt() {
        if (!StringUtil.isNullOrEmpty(getRegionNumber())) {
            return Integer.valueOf(getRegionNumber());
        } else {
            return null;
        }
    }


    /**
     * Getter for the regionNumber property.
     * @return String value of the property
     */
    public String getRegionNumber() {
        return regionNumber;
    }


    /**
     * Setter for the regionNumber property.
     * @param regionNumber the new regionNumber value
     */
    public void setRegionNumber(String regionNumber) {
        this.regionNumber = regionNumber;
    }



    /**
     * Getter for the putawayRegionManager property.
     * @return PutawayRegionManager value of the property
     */
    public PutawayRegionManager getPutawayRegionManager() {
        return putawayRegionManager;
    }



    /**
     * Setter for the putawayRegionManager property.
     * @param putawayRegionManager the new putawayRegionManager value
     */
    public void setPutawayRegionManager(PutawayRegionManager putawayRegionManager) {
        this.putawayRegionManager = putawayRegionManager;
    }

    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }



    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }




    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        validateAndSaveRequest();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * Validate and signs operator into regions.
     *
     * @throws DataAccessException - Database excpetions
     * @throws TaskCommandException - Task Command Exceptions
     */
    protected void validateAndSaveRequest()
    throws DataAccessException, TaskCommandException {
        PutawayRegion       region;

        //Get a list of all regions operator is authorized for
        List<Region> operatorAuthorizedRegions =
            getRegionManager().listAuthorized(TaskFunctionType.Putaway,
                                              getOperator().getWorkgroup().getId());

        //if no authorized regions available for operator
        if (operatorAuthorizedRegions.isEmpty()) {
            throw new TaskCommandException(TaskErrorCode.NO_AUTHORIZED_REGIONS, getOperator().getId());
        }

        //Attempt to sign on to all regions
        int verifyLicenseRegionCount = 0;
        if (getAllRegions()) {
            //Get a list of all putaway regions
            List<PutawayRegion> allPutawayRegions = getPutawayRegionManager().getAll();
            // There is no listAuthorized() function for the PutawayRegionManager. Therefore, we
            // must get all putaway regions and loop through to check if operator is
            // authorized for each region.
            for (PutawayRegion r : allPutawayRegions) {
                if (operatorAuthorizedRegions.contains(r)) {

                    getOperator().getRegions().add(r);
                }
            }
            // If no PUTAWAY regions are authorized for operator throw exception
            if (getOperator().getRegions().isEmpty()) {
                throw new TaskCommandException(TaskErrorCode.NO_AUTHORIZED_PUTAWAY_REGIONS, getOperator().getId());
            }

            Region pr = getOperator().getRegions().iterator().next();
            for (Region r : getOperator().getRegions()) {
                if (!((PutawayRegion) r).getVerifyLicense()) {
                    verifyLicenseRegionCount++;
                }
                // If the region is set up to not verify license then we can only sign into
                // a single region.
                if (verifyLicenseRegionCount > 1) {
                    throw new TaskCommandException(
                        TaskErrorCode.ALL_REGIONS_NOT_ALLOWED);
                }
                // If the operator is attempting to sign into a region that is verifying licenses
                // and a region that is not verifying licenses then throw an error.
                if (((PutawayRegion) pr).getVerifyLicense() != ((PutawayRegion) r).getVerifyLicense()) {
                    throw new TaskCommandException(
                        TaskErrorCode.ALL_REGIONS_NOT_ALLOWED);
                }
            }
        //Attempt single region sign on
        } else if (getRegionNumberInt() != null) {
            region = getPutawayRegionManager()
                        .findRegionByNumber(getRegionNumberInt());
            if (region == null) {
                throw new TaskCommandException(
                    TaskErrorCode.REGION_NOT_FOUND, getRegionNumberInt());
            }
            if (!(operatorAuthorizedRegions.contains(region))) {
                throw new TaskCommandException(
                    TaskErrorCode.NOT_AUTH_FOR_REGION, getRegionNumberInt());
            }
            getOperator().getRegions().add(region);
            // Check for valid region pairings.
            Region pr = getOperator().getRegions().iterator().next();
            for (Region r : getOperator().getRegions()) {
                if (!((PutawayRegion) r).getVerifyLicense()) {
                    verifyLicenseRegionCount++;
                }
                // If the region is set up to not verify license then we can only sign into
                // a single region.
                if (verifyLicenseRegionCount > 1) {
                    throw new TaskCommandException(
                        TaskErrorCode.NOT_COMP_WITH_PREV_REGION, getRegionNumberInt());
                }
                // If the operator is attempting to sign into a region that is verifying licenses
                // and a region that is not verifying licenses then throw an error.
                if (((PutawayRegion) pr).getVerifyLicense() != ((PutawayRegion) r).getVerifyLicense()) {
                    throw new TaskCommandException(
                        TaskErrorCode.NOT_COMP_WITH_PREV_REGION, getRegionNumberInt());
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 