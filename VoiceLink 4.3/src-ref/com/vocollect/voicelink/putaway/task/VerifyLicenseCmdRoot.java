/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandException;
import com.vocollect.voicelink.task.command.TaskErrorCode;
import com.vocollect.voicelink.task.command.TaskResponseRecord;

import java.util.ArrayList;
import java.util.List;


/**
 *
 *
 * @author mkoenig
 */
public class VerifyLicenseCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private PutawayRegionManager  putawayRegionManager;

    private String                licenseNumber;

    private boolean               scanned;

    private License               license;

    private List<PutawayRegion>   signedInRegions;


    /**
     * Getter for the signedInRegions property.
//     * @return List&lt;PutawayRegiongt; value of the property
     */
    public List<PutawayRegion> getSignedInRegions() {
        if (this.signedInRegions == null) {
            this.signedInRegions = new ArrayList <PutawayRegion>();
            return signedInRegions;
        } else {
            return signedInRegions;
        }

    }

    /**
     * Setter for the signedInRegions property.
     * @param signedInRegions the new signedInRegions value
     */
    public void setSignedInRegions(List<PutawayRegion> signedInRegions) {
        this.signedInRegions = signedInRegions;
    }

    /**
     * Getter for the license property.
     * @return License value of the property
     */
    public License getLicense() {
        return license;
    }

    /**
     * Setter for the license property.
     * @param license the new license value
     */
    public void setLicense(License license) {
        this.license = license;
    }

    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    /**
     * Getter for the scanned property.
     * @return boolean value of the property
     */
    public boolean getScanned() {
        return scanned;
    }

    /**
     * Setter for the scanned property.
     * @param scanned the new scanned value
     */
    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }

    /**
     * Getter for the putawayRegionManager property.
     * @return PutawayRegionManager value of the property
     */
    public PutawayRegionManager getPutawayRegionManager() {
        return putawayRegionManager;
    }

    /**
     * Setter for the putawayRegionManager property.
     * @param putawayRegionManager the new putawayRegionManager value
     */
    public void setPutawayRegionManager(PutawayRegionManager putawayRegionManager) {
        this.putawayRegionManager = putawayRegionManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {

        // This exception should never happen.  The only way that this will happen
        // is if ValidPutAwayRegion, RequestPutawayRegion, and PutawayRegionConfiguraton
        // are not called.
        if (this.getOperator().getRegions().size() == 0) {
            throw new TaskCommandException(TaskErrorCode.NOT_SIGNED_INTO_ANY_REGIONS,
                              this.getOperator().getOperatorIdentifier());
        }
        // add the regions that the operator is signed into.
        for (Region r : this.getOperator().getRegions()) {
            this.getSignedInRegions().add((PutawayRegion) r);
        }


        findRequestedLicense();

        //if no license found see if one can be created
        if (getLicense() == null) {
            createLicense();
        }

        validateLicense();
        updateLicense();
        buildResponse();
        return getResponse();
    }


    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * create and return a response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        return new TaskResponseRecord();
    }

    /**
     * Find requested license.
     *
     * @throws DataAccessException - Database exception
     * @throws TaskCommandException - task command exception
     */
    protected void findRequestedLicense()
    throws DataAccessException, TaskCommandException {

        //try to get exact match for license
        setLicense(getLicenseManager().findByNumber(getLicenseNumber()));
        //if exact match not found, then try to find like if spoken value
        if (getLicense() == null && !getScanned()) {
            List<License> licenses = getLicenseManager().
                                        listLikeNumber("%" + getLicenseNumber());

            if (licenses != null) {
                if (licenses.size() == 1) {
                    setLicense(licenses.get(0));
                } else if (licenses.size() > 1) {
                    throw new TaskCommandException(
                        TaskErrorCode.MULTIPLE_LICENSE_FOUND, getLicenseNumber());
                }
            }
        }
    }

    /**
     * if license not found and region is not verified then create a new license.
     *
     * @throws DataAccessException - Database Exception
     * @throws BusinessRuleException - Business Rule Exception
     */
    protected void createLicense()
    throws DataAccessException, BusinessRuleException {

        // The operator can only be signed into one region and the region must be
        // set to not verify licenses with host in order to create the license.
        if (getSignedInRegions().size() == 1
            && !getSignedInRegions().get(0).getVerifyLicense()) {
            License newLicense = new License();
            newLicense.setNumber(getLicenseNumber());
            newLicense.setStatus(LicenseStatus.Available);
            newLicense.setImported(false);
            newLicense.setQuantityPutaway(0);
            newLicense.setQuantityReceived(0);
            newLicense.setRegion((PutawayRegion) getOperator().getRegions().iterator().next());
            newLicense.setGoalTime("0");
            setLicense(newLicense);
            getLicenseManager().save(getLicense());
        }
    }

    /**
     * Validate license.
     *
     * @throws TaskCommandException - Task Command exception
     * @throws DataAccessException - Database exception
     */
    protected void validateLicense()
    throws TaskCommandException, DataAccessException {

        //Check if a license was found
        validateLicenseFound();

        //check if already sleected by operator
        validateLicenseAlreadySelected();

        //License already completed
        validateLicenseComplete();

        //License not in operator region
        validateLicenseRegion();
    }

    /**
     * Check if a license was found.
     *
     * @throws TaskCommandException - Task Command Exception
     */
    protected void validateLicenseFound() throws TaskCommandException {
        if (getLicense() == null) {
            throw new TaskCommandException(
                TaskErrorCode.LICENSE_NOT_FOUND, this.getLicenseNumber());
        }
    }

    /**
     * check if already sleected by operator.
     *
     * @throws TaskCommandException - Task Command Exception
     * @throws DataAccessException - Database exception
     */
    protected void validateLicenseAlreadySelected()
    throws TaskCommandException, DataAccessException {
        if (getLicense().getStatus() == LicenseStatus.InProgress
            && getOperator().equals(getLicense().getOperator())) {
            throw new TaskCommandException(
                TaskErrorCode.LICENSE_ALREADY_SELECTED, this.getLicenseNumber());
        }
    }

    /**
     * License already completed.
     *
     * @throws TaskCommandException - Task Command Exception
     */
    protected void validateLicenseComplete() throws TaskCommandException {
        if (getLicense().getStatus() == LicenseStatus.Complete) {
            throw new TaskCommandException(
                TaskErrorCode.LICENSE_NOT_AVAILABLE, this.getLicenseNumber());
        }
    }

    /**
     * License not in operator region.
     *
     * @throws TaskCommandException - Task Command Exception
     */
    protected void validateLicenseRegion() throws TaskCommandException {
        boolean regionFound = false;
        for (PutawayRegion r : getSignedInRegions()) {
            if (r.equals(getLicense().getRegion())) {
                regionFound = true;
            }
        }

        if (!regionFound) {
            throw new TaskCommandException(
                TaskErrorCode.LICENSE_NOT_IN_REGION);
        }
    }

    /**
     * Reserves license for operator.
     *
     * @throws BusinessRuleException - Business rule exception
     * @throws DataAccessException - Database exception
     */
    protected void updateLicense()
    throws BusinessRuleException, DataAccessException {
        getLicense().setOperator(getOperator());
        getLicense().setStatus(LicenseStatus.InProgress);
        getLicenseManager().save(getLicense());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 