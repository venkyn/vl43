/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.task.command.BasePutawayTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskResponseRecord;


/**
 *
 *
 * @author mkoenig
 */
public class GetAlternatePutLocationCmdRoot extends BasePutawayTaskCommand {

    private static final long serialVersionUID = -5593082031079942973L;

    private String          function;

    private String          licenseNumber;

    private String          locationId;

    private String          quantityRemaining;



    /**
     * Getter for the function property.
     * @return String value of the property
     */
    public String getFunction() {
        return function;
    }



    /**
     * Setter for the function property.
     * @param function the new function value
     */
    public void setFunction(String function) {
        this.function = function;
    }



    /**
     * Getter for the licenseNumber property.
     * @return String value of the property
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }



    /**
     * Setter for the licenseNumber property.
     * @param licenseNumber the new licenseNumber value
     */
    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }



    /**
     * Getter for the locationId property.
     * @return String value of the property
     */
    public String getLocationId() {
        return locationId;
    }



    /**
     * Setter for the locationId property.
     * @param locationId the new locationId value
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }



    /**
     * Getter for the quantityRemaining property.
     * @return String value of the property
     */
    public String getQuantityRemaining() {
        return quantityRemaining;
    }



    /**
     * Setter for the quantityRemaining property.
     * @param quantityRemaining the new quantityRemaining value
     */
    public void setQuantityRemaining(String quantityRemaining) {
        this.quantityRemaining = quantityRemaining;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.task.command.BaseTaskCommand#doExecute()
     */
    @Override
    public Response doExecute() throws Exception {
        buildResponse();

        return getResponse();
    }

    /**
     * Populate the response.
     * @throws DataAccessException - Database exception
     */
    protected void buildResponse() throws DataAccessException {
        //Create a return record, no fields to return
        getResponse().addRecord(buildResponseRecord());
    }

    /**
     * Build response record.
     *
     * @return - response record
     */
    protected ResponseRecord buildResponseRecord() {
        ResponseRecord record = new TaskResponseRecord();

         return record;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 