/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseStatus;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Putaway License Access Object (DAO) interface.
 *
 * @author mkoenig
 */
public interface LicenseDAORoot extends GenericDAO<License> {

    /**
     * Retrieves an license by number string, or returns null is there is no such
     * license.
     *
     * @param licenseNumber - the number of the License
     * @return the requested Licenses, or null
     * @throws DataAccessException on any failure.
     */
    License findByNumber(String licenseNumber) throws DataAccessException;

    /**
     * Rerieves a list of license that are like the specified value.
     *
     * @param licenseNumber - the number of the License
     * @return the requested Licenses, or null
     * @throws DataAccessException on any failure.
     */
    List<License> listLikeNumber(String licenseNumber) throws DataAccessException;

    /**
     * Rerieves a list of license that are inprogess for operator.
     *
     * @param operatorId - operator to find
     * @return the requested Licenses, or null
     * @throws DataAccessException on any failure.
     */
    List<License> listInprogressLicenses(Long operatorId) throws DataAccessException;

    /**
     * Gets all License older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @param status the LicenseStatus used in the query
     * @return a list of Licenses
     * @throws DataAccessException Database failure
     */
    List<License> listOlderThan(QueryDecorator decorator,
        Date date, LicenseStatus status) throws DataAccessException;

    /**
     * @param decorator extra instructions for the query
     * @return the list of putaway to export
     * @throws DataAccessException - Database failure
     */
    List<Map<String, Object>> listExportPutawayRecords(QueryDecorator decorator)
    throws DataAccessException;



    /**
     * Get all InProgress and Completed License records
     * for this operator.
     * @param operator to search
     * @param startDate to search
     * @param endDate to search
     * @return list of License InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<License> listMostRecentRecordsByOperator(Operator operator,
                                                  Date startDate,
                                                  Date endDate)
                                                  throws DataAccessException;


    /**
     * Get all InProgress and Completed License records
     * for this operator.
     * @param operator to search
     * @param startDate to search
     * @return list of License InProgress and Completed records
     * @throws DataAccessException for any database exception
     */
    List<License> listRecordsByOperatorGreaterThanDate(Operator operator,
                                                       Date startDate)
                                                       throws DataAccessException;








}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 