/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.putaway.model.PutawayRegion;

/**
 * Putaway Region Data Access Object (DAO) interface.
 *
 * @author Dennis Doubleday
 */
public interface PutawayRegionDAORoot extends GenericDAO<PutawayRegion> {


    /**
     * retrieves the region specified.
     *
     * @param regionNumber - value to retrieve by
     * @return the region corresponding to the region number
     * @throws DataAccessException - database exceptions
     */
    PutawayRegion findRegionByNumber(int regionNumber)
        throws DataAccessException;

    /**
     *
     * @return the average of the goal rates for all putaway regions
     * @throws DataAccessException - database failure
     */
    public Double avgGoalRate()  throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionNumber - the region number to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByNumber(int regionNumber)
        throws DataAccessException;

    /**
     * Returns the Long of the id, or null if not found.
     * @param regionName - the name of the region to look for
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Long uniquenessByName(String regionName)
        throws DataAccessException;


    /**
     * Returns the Long of the id, or null if not found.
     * @param region - the region to count operators in.
     * @throws DataAccessException - indicates database error
     * @return region id or null
     */
    public Number countNumberOfOperatorsSignedIn(PutawayRegion region)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 