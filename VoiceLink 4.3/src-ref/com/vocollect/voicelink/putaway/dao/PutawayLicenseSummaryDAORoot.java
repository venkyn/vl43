/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.putaway.model.PutawayLicenseSummary;

import java.util.Date;
import java.util.List;


/**
 *
 *
 * @author pfunyak
 */
public interface PutawayLicenseSummaryDAORoot extends GenericDAO<PutawayLicenseSummary> {
    /**
     * Find License plate summary counts for putaway.
     * @param timeWindow the time window in which to query.
     * @return - list of DataObject containing the summary information.
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listLicenseCountsByRegionAndStatus(Date timeWindow)
    throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 