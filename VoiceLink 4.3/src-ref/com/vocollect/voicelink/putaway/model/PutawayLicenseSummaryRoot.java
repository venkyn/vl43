/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.util.Set;


/**
 *
 *
 * @author pfunyak
 */
public class PutawayLicenseSummaryRoot implements DataObject, Taggable {

    private Site           site;

    private PutawayRegion  region;

    private Integer        totalLicenses;

    private Integer        inProgressLicenses;

    private Integer        availableLicenses;

    private Integer        completedLicenses;

    private Integer        nonCompletedLicenses;



    /**
     * Getter for the site property.
     * @return Site value of the property
     */
    public Site getSite() {
        return this.site;
    }


    /**
     * Setter for the site property.
     * @param site the new site value
     */
    public void setSite(Site site) {
        this.site = site;
    }


    /**
     * Getter for the region property.
     * @return Region value of the property
     */
    public PutawayRegion getRegion() {
        return this.region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PutawayRegion region) {
        this.region = region;
    }



    /**
     * Getter for the totalLicenses property.
     * @return Integer value of the property
     */
    public Integer getTotalLicenses() {
        return this.totalLicenses;
    }


    /**
     * Setter for the totalLicenses property.
     * @param totalLicenses the new totalLicenses value
     */
    public void setTotalLicenses(Integer totalLicenses) {
        this.totalLicenses = totalLicenses;
    }



    /**
     * Getter for the availableLicenses property.
     * @return Integer value of the property
     */
    public Integer getAvailableLicenses() {
        return this.availableLicenses;
    }


    /**
     * Getter for the inProgressLicenses property.
     * @return Integer value of the property
     */
    public Integer getInProgressLicenses() {
        return this.inProgressLicenses;
    }


    /**
     * Setter for the inProgressLicenses property.
     * @param inProgressLicenses the new inProgressLicenses value
     */
    public void setInProgressLicenses(Integer inProgressLicenses) {
        this.inProgressLicenses = inProgressLicenses;
    }


    /**
     * Setter for the availableLicenses property.
     * @param availableLicenses the new availableLicenses value
     */
    public void setAvailableLicenses(Integer availableLicenses) {
        this.availableLicenses = availableLicenses;
    }


    /**
     * Getter for the completedLicenses property.
     * @return Integer value of the property
     */
    public Integer getCompletedLicenses() {
        return this.completedLicenses;
    }


    /**
     * Setter for the completedLicenses property.
     * @param completedLicenses the new completedLicenses value
     */
    public void setCompletedLicenses(Integer completedLicenses) {
        this.completedLicenses = completedLicenses;
    }


    /**
     * Getter for the nonCompletedLicenses property.
     * @return Integer value of the property
     */
    public Integer getNonCompletedLicenses() {
        return this.nonCompletedLicenses;
    }


    /**
     * Setter for the nonCompletedLicenses property.
     * @param nonCompletedLicenses the new nonCompletedLicenses value
     */
    public void setNonCompletedLicenses(Integer nonCompletedLicenses) {
        this.nonCompletedLicenses = nonCompletedLicenses;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObjectRoot#getDescriptiveText()
     */
    public String getDescriptiveText() {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Returns the ID of the region property.
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObjectRoot#getId()
     */
    public Long getId() {
        return this.getRegion().getId();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return ((getId() == null) ? 0 : getId().hashCode());
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PutawayLicenseSummary)) {
            return false;
        }
        final PutawayLicenseSummary other = (PutawayLicenseSummary) obj;
        if (getId() == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!getId().equals(other.getId())) {
            return false;
        }
        return true;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#getTags()
     */
    public Set<Tag> getTags() {
        // TODO Auto-generated method stub
        return null;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.Taggable#setTags(java.util.Set)
     */
    public void setTags(Set<Tag> tags) {
        // TODO Auto-generated method stub

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 