/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.putaway.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.ReasonCode;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;


/**
 * License Details.
 *
 * @author mkoenig
 */
public class LicenseDetailRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 3231495093487133756L;

    private License             license;

    private LicenseDetailStatus status;

    private Location            startLocation;

    private Location            putLocation;

    private Integer             quantity;

    private Operator            operator;

    private Date                startTime;

    private Date                putTime;

    private ReasonCode          reason;

    private Set<Tag> tags;



    /**
     * Getter for the license property.
     * @return License value of the property
     */
    public License getLicense() {
        return license;
    }


    /**
     * Setter for the license property.
     * @param license the new license value
     */
    public void setLicense(License license) {
        this.license = license;
    }


    /**
     * Getter for the status property.
     * @return LicenseDetailStatus value of the property
     */
    public LicenseDetailStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(LicenseDetailStatus status) {
        this.status = status;
    }


    /**
     * Getter for the startLocation property.
     * @return Location value of the property
     */
    public Location getStartLocation() {
        return startLocation;
    }


    /**
     * Setter for the startLocation property.
     * @param startLocation the new startLocation value
     */
    public void setStartLocation(Location startLocation) {
        this.startLocation = startLocation;
    }



    /**
     * Getter for the putLocation property.
     * @return Location value of the property
     */
    public Location getPutLocation() {
        return this.putLocation;
    }




    /**
     * Setter for the putLocation property.
     * @param putLocation the new putLocation value
     */
    public void setPutLocation(Location putLocation) {
        this.putLocation = putLocation;
    }



    /**
     * Getter for the quantity property.
     * @return Integer value of the property
     */
    public Integer getQuantity() {
        return quantity;
    }


    /**
     * Setter for the quantity property.
     * @param quantity the new quantity value
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the endTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }


    /**
     * Setter for the endTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }



    /**
     * Getter for the reason property.
     * @return ReasonCode value of the property
     */
    public ReasonCode getReason() {
        return reason;
    }


    /**
     * Setter for the reason property.
     * @param reason the new reason value
     */
    public void setReason(ReasonCode reason) {
        this.reason = reason;
    }


    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }


    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LicenseDetail)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LicenseDetail other = (LicenseDetail) obj;
        if (operator == null) {
            if (other.getOperator() != null) {
                return false;
            }
        } else if (!operator.equals(other.getOperator())) {
            return false;
        }
        if (putTime == null) {
            if (other.getPutTime() != null) {
                return false;
            }
        } else if (!putTime.equals(other.getPutTime())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((operator == null) ? 0 : operator.hashCode());
        result = prime * result + ((putTime == null) ? 0 : putTime.hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 