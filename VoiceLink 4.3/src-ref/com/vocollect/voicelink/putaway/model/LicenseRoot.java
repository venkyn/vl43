/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.model;

import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.CommonModelObject;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.Operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Main Putaway License class.
 *
 * @author mkoenig
 */
public class LicenseRoot extends CommonModelObject
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 4953348240151978588L;

    private PutawayRegion       region;

    private Date                dateReceived;

    private String              number;

    private LicenseStatus       status;

    private int                 quantityReceived;

    private int                 quantityPutaway;

    private Location            location;

    private Operator            operator;

    private Date                startTime;

    private Date                putTime;

    private Item                item;

    private boolean             expirationFlag;

    private boolean             imported;

    private String              goalTime;

    private List<LicenseDetail> licenseDetails;

    private Set<Tag> tags;

    private ExportStatus exportStatus = ExportStatus.NotExported;

    /**
     * Getter for the exportStatus.
     * @return exportStatus of the property
     */
    public ExportStatus getExportStatus() {
        return exportStatus;
    }

    /**
     * Setter for the exportStatus property.
     * @param exportStatus the new exportStatus value
     */

    public void setExportStatus(ExportStatus exportStatus) {
        this.exportStatus = exportStatus;
    }
    /**
     * Getter for the tags property.
     * @return Set&lt;Tag&gt; value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }




    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }


    /**
     * Getter for the licenseDetails property.
     * @return List&lt;LicenseDetail&gt; value of the property
     */
    public List<LicenseDetail> getLicenseDetails() {
        if (this.licenseDetails == null) {
            this.licenseDetails = new ArrayList<LicenseDetail>();
        }

        return licenseDetails;
    }



    /**
     * Setter for the licenseDetails property.
     * @param licenseDetails the new licenseDetails value
     */
    public void setLicenseDetails(List<LicenseDetail> licenseDetails) {
        this.licenseDetails = licenseDetails;
    }

    /**
     * Gets the number of license details.
     * @return The number of license details for this license.
     */
    public Integer getLicenseDetailsCount() {
        return getLicenseDetails().size();
    }


    /**
     * Getter for the dateReceived property.
     * @return Date value of the property
     */
    public Date getDateReceived() {
        return dateReceived;
    }


    /**
     * Setter for the dateReceived property.
     * @param dateReceived the new dateReceived value
     */
    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }


    /**
     * Getter for the expirationFlag property.
     * @return boolean value of the property
     */
    public boolean isExpirationFlag() {
        return expirationFlag;
    }


    /**
     * Setter for the expirationFlag property.
     * @param expirationFlag the new expirationFlag value
     */
    public void setExpirationFlag(boolean expirationFlag) {
        this.expirationFlag = expirationFlag;
    }


    /**
     * Getter for the goalTime property.
     * @return String value of the property
     */
    public String getGoalTime() {
        return goalTime;
    }


    /**
     * Setter for the goalTime property.
     * @param goalTime the new goalTime value
     */
    public void setGoalTime(String goalTime) {
        this.goalTime = goalTime;
    }


    /**
     * Getter for the imported property.
     * @return boolean value of the property
     */
    public boolean isImported() {
        return imported;
    }


    /**
     * Setter for the imported property.
     * @param imported the new imported value
     */
    public void setImported(boolean imported) {
        this.imported = imported;
    }


    /**
     * Getter for the item property.
     * @return Item value of the property
     */
    public Item getItem() {
        return item;
    }


    /**
     * Setter for the item property.
     * @param item the new item value
     */
    public void setItem(Item item) {
        this.item = item;
    }


    /**
     * Getter for the location property.
     * @return Location value of the property
     */
    public Location getLocation() {
        return location;
    }


    /**
     * Setter for the location property.
     * @param location the new location value
     */
    public void setLocation(Location location) {
        this.location = location;
    }


    /**
     * Getter for the number property.
     * @return String value of the property
     */
    public String getNumber() {
        return number;
    }


    /**
     * Setter for the number property.
     * @param number the new number value
     */
    public void setNumber(String number) {
        this.number = number;
    }


    /**
     * Getter for the operator property.
     * @return Operator value of the property
     */
    public Operator getOperator() {
        return operator;
    }


    /**
     * Setter for the operator property.
     * @param operator the new operator value
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }


    /**
     * Getter for the putTime property.
     * @return Date value of the property
     */
    public Date getPutTime() {
        return putTime;
    }


    /**
     * Setter for the putTime property.
     * @param putTime the new putTime value
     */
    public void setPutTime(Date putTime) {
        this.putTime = putTime;
    }


    /**
     * Getter for the quantityPutaway property.
     * @return int value of the property
     */
    public int getQuantityPutaway() {
        return quantityPutaway;
    }


    /**
     * Setter for the quantityPutaway property.
     * @param quantityPutaway the new quantityPutaway value
     */
    public void setQuantityPutaway(int quantityPutaway) {
        this.quantityPutaway = quantityPutaway;
    }


    /**
     * Getter for the quantityReceived property.
     * @return int value of the property
     */
    public int getQuantityReceived() {
        return quantityReceived;
    }


    /**
     * Setter for the quantityReceived property.
     * @param quantityReceived the new quantityReceived value
     */
    public void setQuantityReceived(int quantityReceived) {
        this.quantityReceived = quantityReceived;
    }


    /**
     * Getter for the region property.
     * @return PutawayRegion value of the property
     */
    public PutawayRegion getRegion() {
        return region;
    }


    /**
     * Setter for the region property.
     * @param region the new region value
     */
    public void setRegion(PutawayRegion region) {
        this.region = region;
    }


    /**
     * Getter for the startTime property.
     * @return Date value of the property
     */
    public Date getStartTime() {
        return startTime;
    }


    /**
     * Setter for the startTime property.
     * @param startTime the new startTime value
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }


    /**
     * Getter for the status property.
     * @return LicenseStatus value of the property
     */
    public LicenseStatus getStatus() {
        return status;
    }


    /**
     * Setter for the status property.
     * @param status the new status value
     */
    public void setStatus(LicenseStatus status) {
        this.status = status;
    }

    /**
     * Add a license detail record to licesne.
     *
     * @param licenseDetail license detail to add
     */
    public void addLicenseDetail(LicenseDetail licenseDetail) {
        licenseDetail.setLicense((License) this);
        getLicenseDetails().add(licenseDetail);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof License)) {
            return false;
        }
        final License other = (License) obj;
        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.number == null
        ? 0 : this.number.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getNumber();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 