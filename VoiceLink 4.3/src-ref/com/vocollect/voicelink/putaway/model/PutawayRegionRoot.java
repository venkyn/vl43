/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.putaway.model;

import com.vocollect.voicelink.core.model.Region;

import java.util.Set;

/**
 *
 *
 * @author mkoenig
 */
public class PutawayRegionRoot extends Region {

    //
    private static final long serialVersionUID = -7284981351124644810L;


    private PutawayCaptureStartLocation     captureStartLocation;

    private boolean                         allowSkipLicense;

    private boolean                         allowCancelLicense;

    private boolean                         allowOverrideLocation;

    private boolean                         allowOverrideQuantity;

    private boolean                         allowPartialPut;

    private boolean                         capturePickupQuantity;

    private boolean                         capturePutQuantity;

    private Integer                         licDigitsTaskSpeaks;

    private int                             licDigitsOperatorSpeaks;

    private int                             locDigitsOperatorSpeaks;

    private int                             checkDigitsOperatorSpeaks;

    private Integer                         numberOfLicensesAllowed;

    private boolean                         verifySpokenLicOrLoc;

    private String                          exceptionLocation;

    private Set<License>                    licenses;

    private boolean                         verifyLicense;

    /**
     * Getter for the licenses property.
     * @return Set&lt;License&gt; value of the property
     */
    public Set<License> getLicenses() {
        return licenses;
    }


    /**
     * Setter for the licenses property.
     * @param licenses the new licenses value
     */
    public void setLicenses(Set<License> licenses) {
        this.licenses = licenses;
    }


    /**
     * Getter for the allowCancelLicense property.
     * @return boolean value of the property
     */
    public boolean getAllowCancelLicense() {
        return allowCancelLicense;
    }


    /**
     * Setter for the allowCancelLicense property.
     * @param allowCancelLicense the new allowCancelLicense value
     */
    public void setAllowCancelLicense(boolean allowCancelLicense) {
        this.allowCancelLicense = allowCancelLicense;
    }


    /**
     * Getter for the allowOverrideLocation property.
     * @return boolean value of the property
     */
    public boolean getAllowOverrideLocation() {
        return allowOverrideLocation;
    }


    /**
     * Setter for the allowOverrideLocation property.
     * @param allowOverrideLocation the new allowOverrideLocation value
     */
    public void setAllowOverrideLocation(boolean allowOverrideLocation) {
        this.allowOverrideLocation = allowOverrideLocation;
    }


    /**
     * Getter for the allowOverrideQuantity property.
     * @return boolean value of the property
     */
    public boolean getAllowOverrideQuantity() {
        return allowOverrideQuantity;
    }


    /**
     * Setter for the allowOverrideQuantity property.
     * @param allowOverrideQuantity the new allowOverrideQuantity value
     */
    public void setAllowOverrideQuantity(boolean allowOverrideQuantity) {
        this.allowOverrideQuantity = allowOverrideQuantity;
    }


    /**
     * Getter for the allowPartialPut property.
     * @return boolean value of the property
     */
    public boolean getAllowPartialPut() {
        return allowPartialPut;
    }


    /**
     * Setter for the allowPartialPut property.
     * @param allowPartialPut the new allowPartialPut value
     */
    public void setAllowPartialPut(boolean allowPartialPut) {
        this.allowPartialPut = allowPartialPut;
    }


    /**
     * Getter for the allowSkipLicense property.
     * @return boolean value of the property
     */
    public boolean getAllowSkipLicense() {
        return allowSkipLicense;
    }


    /**
     * Setter for the allowSkipLicense property.
     * @param allowSkipLicense the new allowSkipLicense value
     */
    public void setAllowSkipLicense(boolean allowSkipLicense) {
        this.allowSkipLicense = allowSkipLicense;
    }


    /**
     * Getter for the capturePickupQuantity property.
     * @return boolean value of the property
     */
    public boolean getCapturePickupQuantity() {
        return capturePickupQuantity;
    }


    /**
     * Setter for the capturePickupQuantity property.
     * @param capturePickupQuantity the new capturePickupQuantity value
     */
    public void setCapturePickupQuantity(boolean capturePickupQuantity) {
        this.capturePickupQuantity = capturePickupQuantity;
    }


    /**
     * Getter for the capturePutQuantity property.
     * @return boolean value of the property
     */
    public boolean getCapturePutQuantity() {
        return capturePutQuantity;
    }


    /**
     * Setter for the capturePutQuantity property.
     * @param capturePutQuantity the new capturePutQuantity value
     */
    public void setCapturePutQuantity(boolean capturePutQuantity) {
        this.capturePutQuantity = capturePutQuantity;
    }


    /**
     * Getter for the captureStartLocation property.
     * @return PutawayCaptureStartLocation value of the property
     */
    public PutawayCaptureStartLocation getCaptureStartLocation() {
        return captureStartLocation;
    }


    /**
     * Setter for the captureStartLocation property.
     * @param captureStartLocation the new captureStartLocation value
     */
    public void setCaptureStartLocation(PutawayCaptureStartLocation captureStartLocation) {
        this.captureStartLocation = captureStartLocation;
    }


    /**
     * Getter for the checkDigitsOperatorSpeaks property.
     * @return int value of the property
     */
    public int getCheckDigitsOperatorSpeaks() {
        return checkDigitsOperatorSpeaks;
    }


    /**
     * Setter for the checkDigitsOperatorSpeaks property.
     * @param checkDigitsOperatorSpeaks the new checkDigitsOperatorSpeaks value
     */
    public void setCheckDigitsOperatorSpeaks(int checkDigitsOperatorSpeaks) {
        this.checkDigitsOperatorSpeaks = checkDigitsOperatorSpeaks;
    }


    /**
     * Getter for the exceptionLocation property.
     * @return String value of the property
     */
    public String getExceptionLocation() {
        return exceptionLocation;
    }


    /**
     * Setter for the exceptionLocation property.
     * @param exceptionLocation the new exceptionLocation value
     */
    public void setExceptionLocation(String exceptionLocation) {
        this.exceptionLocation = exceptionLocation;
    }


    /**
     * Getter for the licDigitsOperatorSpeaks property.
     * @return int value of the property
     */
    public int getLicDigitsOperatorSpeaks() {
        return licDigitsOperatorSpeaks;
    }


    /**
     * Setter for the licDigitsOperatorSpeaks property.
     * @param licDigitsOperatorSpeaks the new licDigitsOperatorSpeaks value
     */
    public void setLicDigitsOperatorSpeaks(int licDigitsOperatorSpeaks) {
        this.licDigitsOperatorSpeaks = licDigitsOperatorSpeaks;
    }


    /**
     * Getter for the licDigitsTaskSpeaks property.
     * @return int value of the property
     */
    public Integer getLicDigitsTaskSpeaks() {
        return licDigitsTaskSpeaks;
    }


    /**
     * Setter for the licDigitsTaskSpeaks property.
     * @param licDigitsTaskSpeaks the new licDigitsTaskSpeaks value
     */
    public void setLicDigitsTaskSpeaks(Integer licDigitsTaskSpeaks) {
        this.licDigitsTaskSpeaks = licDigitsTaskSpeaks;
    }


    /**
     * Getter for the locDigitsOperatorSpeaks property.
     * @return int value of the property
     */
    public int getLocDigitsOperatorSpeaks() {
        return locDigitsOperatorSpeaks;
    }


    /**
     * Setter for the locDigitsOperatorSpeaks property.
     * @param locDigitsOperatorSpeaks the new locDigitsOperatorSpeaks value
     */
    public void setLocDigitsOperatorSpeaks(int locDigitsOperatorSpeaks) {
        this.locDigitsOperatorSpeaks = locDigitsOperatorSpeaks;
    }


    /**
     * Getter for the numberOfLicensesAllowed property.
     * @return int value of the property
     */
    public Integer getNumberOfLicensesAllowed() {
        return numberOfLicensesAllowed;
    }


    /**
     * Setter for the numberOfLicensesAllowed property.
     * @param numberOfLicensesAllowed the new numberOfLicensesAllowed value
     */
    public void setNumberOfLicensesAllowed(Integer numberOfLicensesAllowed) {
        this.numberOfLicensesAllowed = numberOfLicensesAllowed;
    }


    /**
     * Getter for the verifyLicense property.
     * @return boolean value of the property
     */
    public boolean getVerifyLicense() {
        return verifyLicense;
    }


    /**
     * Setter for the verifyLicense property.
     * @param verifyLicense the new verifyLicense value
     */
    public void setVerifyLicense(boolean verifyLicense) {
        this.verifyLicense = verifyLicense;
    }


    /**
     * Getter for the verifySpokenLicOrLoc property.
     * @return boolean value of the property
     */
    public boolean getVerifySpokenLicOrLoc() {
        return verifySpokenLicOrLoc;
    }


    /**
     * Setter for the verifySpokenLicOrLoc property.
     * @param verifySpokenLicOrLoc the new verifySpokenLicOrLoc value
     */
    public void setVerifySpokenLicOrLoc(boolean verifySpokenLicOrLoc) {
        this.verifySpokenLicOrLoc = verifySpokenLicOrLoc;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 