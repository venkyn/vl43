/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.model;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.voicelink.core.importer.parsers.Importable;
import com.vocollect.voicelink.core.importer.parsers.ImportableImpl;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;

import java.util.Date;




/**
 * @author jtauberg
 *
 * Image of an Item that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the
 * tool to do it. Additionally, any methods we need to override exist in the body.
 */
public class LicenseImportDataRoot  extends BaseModelObject implements Importable {

    private static final long serialVersionUID = 2168334776567495066L;

    private ImportableImpl impl = new ImportableImpl();

    private License myModel = new License();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#convertToModel()
     */
    public Object convertToModel() throws VocollectException {
        return this.myModel;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj) {
        return impl.equals(obj);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportID()
     */
    public Long getImportID() {
        return impl.getImportID();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getSiteName()
     */
    public String getSiteName() {
        return impl.getSiteName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#getImportStatus()
     */
    public int getImportStatus() {
        return impl.getImportStatus();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return impl.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isCompleted()
     */
    public boolean isCompleted() {
        return impl.isCompleted();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#isInProgress()
     */
    public boolean isInProgress() {
        return impl.isInProgress();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setCompleted(boolean)
     */
    public void setCompleted(boolean completedParam) {
        impl.setCompleted(completedParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setImportID(java.lang.String)
     */
    public void setImportID(Long importID) {
        impl.setImportID(importID);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setInProgress(boolean)
     */
    public void setInProgress(boolean inProgressParam) {
        impl.setInProgress(inProgressParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setSiteName(java.lang.String)
     */
    public void setSiteName(String siteNameParam) {
        impl.setSiteName(siteNameParam);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.importer.parsers.ImportableImpl#setStatus(int)
     */
    public void setImportStatus(int importStatus) {
        impl.setImportStatus(importStatus);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return impl.toString();
    }

    //Helper methods for setting and getting complex members

    /**
     * Set the number in the region. This will be used to look up a region in the database.
     * @param number the number to set the regionnumber to.
     */
    public void setRegionNumber(String number) {
        PutawayRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new PutawayRegion();
            this.setRegion(tempRegion);
        }
        tempRegion.setNumber(new Integer(number));
    }


    /**
     * Get the region number.
     * @return the region number as a string.
     */
    public String getRegionNumber() {
        PutawayRegion tempRegion = myModel.getRegion();
        if (null == tempRegion) {
            tempRegion = new PutawayRegion();
            this.setRegion(tempRegion);
        }
        if (null == tempRegion.getNumber()) {
            return null;
        }
        return tempRegion.getNumber().toString();
    }


    /**
     * Set the number in the Item. This will be used to look up an Item in the database.
     * @param itemId the ID to set the Item.number to.
     */
    public void setItemId(String itemId) {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            tempItem = new Item();
            this.setItem(tempItem);
        }
        tempItem.setNumber(itemId);
    }


    /**
     * Get the Item Id (Item.number).
     * @return the Item Id (Item.number a String).
     */
    public String getItemId() {
        Item tempItem = myModel.getItem();
        if (null == tempItem) {
            return null;
        }
        if (null == tempItem.getNumber()) {
            return null;
        }
        return tempItem.getNumber();
    }


    /**
     * Set the Location id (scannedVerification) in the Location.
     * This will be used to look up a Location in the database.
     * @param scannedVerification the ID to set the Location.scannedVerification to.
     */
    public void setLocationId(String scannedVerification) {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            tempLocation = new Location();
            this.setLocation(tempLocation);
        }
        tempLocation.setScannedVerification(scannedVerification);
    }


    /**
     * Get the Location's id (scannedVerification).
     * @return the Location ID (scannedVerification) (String).
     */
    public String getLocationId() {
        Location tempLocation = myModel.getLocation();
        if (null == tempLocation) {
            return null;
        }
        if (null == tempLocation.getScannedVerification()) {
            return null;
        }
        return tempLocation.getScannedVerification();
    }


    //Delegates from License model below


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getDateReceived()
     */
    public Date getDateReceived() {
        return myModel.getDateReceived();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getGoalTime()
     */
    public String getGoalTime() {
        return myModel.getGoalTime();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getItem()
     */
    public Item getItem() {
        return myModel.getItem();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getLocation()
     */
    public Location getLocation() {
        return myModel.getLocation();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getNumber()
     */
    public String getNumber() {
        return myModel.getNumber();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getQuantityReceived()
     */
    public int getQuantityReceived() {
        return myModel.getQuantityReceived();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#getRegion()
     */
    public PutawayRegion getRegion() {
        return myModel.getRegion();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#isExpirationFlag()
     */
    public boolean isExpirationFlag() {
        return myModel.isExpirationFlag();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setDateReceived(java.util.Date)
     */
    public void setDateReceived(Date dateReceived) {
        myModel.setDateReceived(dateReceived);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setExpirationFlag(boolean)
     */
    public void setExpirationFlag(boolean expirationFlag) {
        myModel.setExpirationFlag(expirationFlag);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setGoalTime(java.lang.String)
     */
    public void setGoalTime(String goalTime) {
        myModel.setGoalTime(goalTime);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setItem(com.vocollect.voicelink.core.model.Item)
     */
    public void setItem(Item item) {
        myModel.setItem(item);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setLocation(com.vocollect.voicelink.core.model.Location)
     */
    public void setLocation(Location location) {
        myModel.setLocation(location);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setNumber(java.lang.String)
     */
    public void setNumber(String number) {
        myModel.setNumber(number);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setQuantityReceived(int)
     */
    public void setQuantityReceived(int quantityReceived) {
        myModel.setQuantityReceived(quantityReceived);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.putaway.model.LicenseRoot#setRegion(com.vocollect.voicelink.putaway.model.PutawayRegion)
     */
    public void setRegion(PutawayRegion region) {
        myModel.setRegion(region);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getId()
     */
    @Override
    public Long getId() {
        return this.getImportID();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 