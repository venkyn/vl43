/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.voicelink.core.model.DataAggregatorFieldType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 
 * @author khazra, kudupi
 */
public class OperatorDataAggregatorRoot extends GenericDataAggregatorImpl {

    private static final Logger log = new Logger(
        OperatorDataAggregatorRoot.class);

    public static final String IDENTITY_COLUMN = "operator";

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "routeatdeparturedate", "actualrate", "signon",
        "breakduration", "idleaftersignin", "idlebeforesignoff",
        "idlebeforebreak", "idleafterbreak", "region", "operatorteams", "route" };

    private OperatorManager operatorManager;

    private AssignmentManager assignmentManager;

    private LaborManager laborManager;

    private AssignmentLaborManager assignmentLaborManager;
    
    public static final String DELIMITER = "##";

    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setLaborManager(LaborManager operatorLaborManager) {
        this.laborManager = operatorLaborManager;
    }

    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return the assignmentLaborManager
     */
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }

    /**
     * @param assignmentLaborManager the assignmentLaborManager to set
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### OPERATOR AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        
        try {
            Date startTime = getStartTime(null);
            Date endTime = getEndTime(null);

            if ((startTime == null) || (endTime == null)) {
                throw new JSONException(
                    "Error in populating Time Window. parameters JSONObject doesn't contain time window values.");
            }
            
            DateTime q1Start = new DateTime();
            List<Operator> operators = this.getOperatorManager()
                .listAllOrderByName();
            DateTime q1End = new DateTime();
            log.trace("$$$Operator List query Time$$$" + "@@@"
                + (q1End.getMillis() - q1Start.getMillis()) + "@@@");

            Map<Long, List<OperatorLabor>> operatorLaborRecordMap = getLaborRecordsMap(
                startTime, endTime);
            Map<Operator, String> operatorRouteMap = getOperatorsInRoutesMap();
            Map<Region, List<String>> routeRegionMap = getRouteRegionMap();
            Map<Operator, Map<Region, String>> opRoutesWorkedMap = getRoutesWorked();
            Map<String, Double> actualRateMap = getActualRateMap();
            Map<Operator, List<OperatorLabor>> closedBreakLaborRecordsMap = closedBreakLaborRecordsMap(
                startTime, endTime);
            Map<Operator, OperatorLabor> openLaborRecordsMap = openLaborRecordsMap();

            for (Operator operator : operators) {
                List<OperatorLabor> laborRecords = operatorLaborRecordMap.get(operator.getId());
                Map<Region, String> routesWorkedMap = opRoutesWorkedMap.get(operator);
                String routeInformation = getRoute(operator, operatorRouteMap, routeRegionMap, routesWorkedMap);
                
                JSONObject operatorLaborJSON = new JSONObject();
                operatorLaborJSON.put(IDENTITY_COLUMN,
                    operator.getOperatorIdentifier());

                operatorLaborJSON.put(FIELDS[1], getFormatetdRouteDepartureDate(routeInformation, false));

                operatorLaborJSON.put(FIELDS[2], getActualRate(operator, actualRateMap));

                operatorLaborJSON.put(FIELDS[3], getSignOnTime(operator));

                operatorLaborJSON.put(FIELDS[4], getBreakDuration(laborRecords));

                operatorLaborJSON.put(FIELDS[5], idleAfterSignOn(laborRecords));

                operatorLaborJSON.put(FIELDS[6], idleBeforeSignOff(laborRecords));

                operatorLaborJSON.put(FIELDS[7], idleBeforeBreak(laborRecords));

                operatorLaborJSON.put(
                    FIELDS[8],
                    idleAfterBreak(operator, closedBreakLaborRecordsMap,
                        openLaborRecordsMap));

                operatorLaborJSON
                    .put(FIELDS[9], getCurrentRegionName(operator));

                operatorLaborJSON.put(FIELDS[10], getOperatorTeams(operator));

                operatorLaborJSON.put(FIELDS[11], getFormatetdRouteDepartureDate(routeInformation, true));

                if (operatorLaborJSON != null) {
                    outputArray.put(operatorLaborJSON);
                }

            }

        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        }
        DateTime end = new DateTime();
        log.trace("$$$Operator Aggregator Execution Time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");

        return outputArray;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;
        
        try {

            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN, DAFieldType.STRING.name(),
                "aggregator.operator.field." + IDENTITY_COLUMN, " ", i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.operator.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.operator.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.TIME.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Time.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));
            
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.operator.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.operator.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.operator.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.operator.field." + FIELDS[i], " ", ++i));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * @param laborRecords for which labor record are getting evaluated
     * @return time in minutes after operator signed on and not working
     */
    private Integer idleAfterSignOn(List<OperatorLabor> laborRecords) {
        Integer idleAfterSignOn = 0;

        DateTime startTime = null;
        DateTime endTime = null;

        if (laborRecords == null || laborRecords.isEmpty()) {
            return idleAfterSignOn;
        }

        for (int i = laborRecords.size() - 1; i >= 0; i--) {
            OperatorLabor laborRecord = laborRecords.get(i);

            if (laborRecord.getActionType() == OperatorLaborActionType.SignOn) {
                startTime = new DateTime(laborRecord.getStartTime());
                endTime = new DateTime(laborRecord.getEndTime());

                break;
            }
        }

        if (startTime != null) {
            idleAfterSignOn = Math.abs(Minutes.minutesBetween(endTime,
                startTime).getMinutes());
        } else {
            idleAfterSignOn = 0;

        }
        return idleAfterSignOn;
    }

    /**
     * @param laborRecords the operator to evaluate
     * @return the duration of no work
     */
    private Integer idleBeforeBreak(List<OperatorLabor> laborRecords) {
        DateTime start = new DateTime();
        Integer breakDuration = 0;

        try {
            // If no labor record found, return 0
            if (laborRecords == null || laborRecords.isEmpty()) {
                return breakDuration;
            }

            // Reverse scan to find last break record
            for (int i = laborRecords.size() - 1; i >= 0; i--) {
                OperatorLabor operatorLabor = laborRecords.get(i);
                if (operatorLabor.getActionType() == OperatorLaborActionType.Break) {
                    DateTime targetTime = null;
                    // Get the function labor record associated to the break
                    OperatorBreakLabor breakLabor = (OperatorBreakLabor) operatorLabor;
                    OperatorLabor previousOperatorLabor = breakLabor
                        .getPreviousOperatorLabor();

                    if (previousOperatorLabor.getActionType() == OperatorLaborActionType.SignOn) {
                        targetTime = new DateTime(
                            previousOperatorLabor.getStartTime());
                    } else {
                        // Find out how what's the last activity time on that
                        // function labor record
                        Date targetDateVal = this.laborManager.lastWorkOnLaborRecord(operatorLabor.getOperator(),
                            previousOperatorLabor);
                        
                        if (targetDateVal == null || targetDateVal.after(breakLabor.getStartTime())) {
                            return breakDuration;
                        }                        
                        
                        targetTime = new DateTime(targetDateVal);
                    }

                    // Find the difference
                    breakDuration = Math.abs(Minutes.minutesBetween(
                        new DateTime(breakLabor.getStartTime()), targetTime)
                        .getMinutes());

                    break;
                }
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        DateTime end = new DateTime();
        log.trace("$$$ Idle before break calc time $$$@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");

        return breakDuration;
    }

    /**
     * Method to return idle after break in minutes.
     * @param operator <code>Operator</code>
     * @param closedBreakLaborRecordsMap 
     * @param openLaborRecordsMap 
     * @return Integer Value of idleness in minutes after break ended.
     */
    private Integer idleAfterBreak(Operator operator,
                                   Map<Operator, List<OperatorLabor>> closedBreakLaborRecordsMap,
                                   Map<Operator, OperatorLabor> openLaborRecordsMap) {
        Integer idleAfterBreakDuration = 0;
        DateTime targetTime = null;

        try {
            List<OperatorLabor> closedBreakLaborRecords = closedBreakLaborRecordsMap
                .get(operator);

            // If no closed break labor record found, return 0
            if (closedBreakLaborRecords == null
                || closedBreakLaborRecords.isEmpty()) {
                return idleAfterBreakDuration;
            }

            OperatorLabor latestBreakRecord = closedBreakLaborRecords.get(closedBreakLaborRecords.size() - 1);

            DateTime start = new DateTime();
            // Give the Date and Time of when the operator next picked.
            Date nextWorkedTime = this.laborManager.nextWorkStarted(operator, latestBreakRecord);
            DateTime end = new DateTime();
            log.trace("$$$ nextWorkStarted query time $$$@@@"
                + (end.getMillis() - start.getMillis()) + "@@@");

            if (operator.getSignOnTime().before(latestBreakRecord.getStartTime())) {
                // Calculate the difference only if some picking work is done
                // after coming back from the break.
                if (nextWorkedTime != null) {
                    targetTime = new DateTime(nextWorkedTime);
                } else { // If No picking done then fetching open labor record
                         // and if its sign off finding the difference with respect to
                         // signOff start Time
                    OperatorLabor openLaborRecord = openLaborRecordsMap
                        .get(operator);
                    if (openLaborRecord != null
                        && ((openLaborRecord.getActionType() == OperatorLaborActionType.SignOff) || (openLaborRecord
                            .getActionType() == OperatorLaborActionType.Break))) {
                        targetTime = new DateTime(
                            openLaborRecord.getStartTime());
                    } else {
                        // If the open labor record is not sign off then
                        // finding difference with respect to current Time
                        targetTime = new DateTime();
                    }
                }
            } else {
                if ((nextWorkedTime != null) && (nextWorkedTime.before(operator.getSignOnTime()))) {
                    targetTime = new DateTime(nextWorkedTime);
                } else {
                    targetTime = new DateTime(this.laborManager
                        .getOperatorLaborManager()
                        .getFirstSignOffTimeAfterBreak(operator.getId(),
                            latestBreakRecord.getEndTime()));
                }
            }
            
            idleAfterBreakDuration = Math.abs(Minutes.minutesBetween(
                new DateTime(latestBreakRecord.getEndTime()), targetTime)
                .getMinutes());
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return idleAfterBreakDuration;
    }

    /**
     * @param laborRecords the operator to evaluate
     * @return the duration of no work
     */
    private Integer idleBeforeSignOff(List<OperatorLabor> laborRecords) {
        Integer idleDuration = 0;

        try {
            // If no labor record found, return 0
            if (laborRecords == null || laborRecords.isEmpty()) {
                return idleDuration;
            }

            // Reverse scan to find last sign off record
            for (int i = laborRecords.size() - 1; i >= 0; i--) {
                OperatorLabor operatorLabor = laborRecords.get(i);
                if (operatorLabor.getActionType() == OperatorLaborActionType.SignOff
                    && i > 0) {

                    DateTime targetTime = null;

                    OperatorLabor previousOperatorLabor = laborRecords
                        .get(i - 1);

                    if (previousOperatorLabor.getActionType() == OperatorLaborActionType.SignOn) {
                        targetTime = new DateTime(
                            previousOperatorLabor.getStartTime());

                        idleDuration = Math.abs(Minutes.minutesBetween(
                            new DateTime(operatorLabor.getStartTime()),
                            targetTime).getMinutes());
                    } else if (previousOperatorLabor.getActionType() == OperatorLaborActionType.Break) {

                        OperatorBreakLabor breakLabor = (OperatorBreakLabor) previousOperatorLabor;
                        targetTime = new DateTime(breakLabor.getEndTime());
                        // Find the difference
                        idleDuration = Math.abs(Minutes.minutesBetween(
                            new DateTime(operatorLabor.getStartTime()),
                            targetTime).getMinutes());
                    } else {

                        // Find out how what's the last activity time on that
                        // function labor record
                        Date targetDateVal = this.laborManager.lastWorkOnLaborRecord(operatorLabor.getOperator(),
                            previousOperatorLabor);
                        
                        if (targetDateVal == null || targetDateVal.after(operatorLabor.getStartTime())) {
                            return idleDuration;
                        }                        
                        
                        targetTime = new DateTime(targetDateVal);
                        
                        // Find the difference
                        idleDuration = Math.abs(Minutes.minutesBetween(
                            new DateTime(operatorLabor.getStartTime()),
                            targetTime).getMinutes());
                    }
                    break;
                }
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        return idleDuration;
    }

    /**
     * 
     * @param laborRecords for which idle time is getting calculated
     * @return duration of break operator took
     */
    private Integer getBreakDuration(List<OperatorLabor> laborRecords) {
        Integer breakDuration = 0;

        if (laborRecords == null || laborRecords.isEmpty()) {
            return breakDuration;
        }

        for (int i = laborRecords.size() - 1; i >= 0; i--) {
            OperatorLabor operatorLabor = laborRecords.get(i);
            if (operatorLabor.getActionType() == OperatorLaborActionType.Break) {
                DateTime endTime = null;
                if (operatorLabor.getEndTime() == null) {
                    endTime = new DateTime();
                } else {
                    endTime = new DateTime(operatorLabor.getEndTime());
                }

                breakDuration = Math.abs(Minutes.minutesBetween(
                    new DateTime(operatorLabor.getStartTime()), endTime)
                    .getMinutes());

                break;
            }
        }

        return breakDuration;
    }

    /**
     * 
     * @param operator operator object for which list of associated operator
     *            Teams is needed.
     * @return Set of operatorTeams
     * @throws JSONException throws JSONException
     */
    private String getOperatorTeams(Operator operator) throws JSONException {
        DateTime start = new DateTime();
        String operatorTeams = "";

        Set<OperatorTeam> operatorTeamsSet = operator.getOperatorTeams();

        if (operatorTeamsSet != null) {
            for (OperatorTeam ot : operatorTeamsSet) {
                operatorTeams += ot.getName() + ",";
            }
            if (operatorTeams.endsWith(",")) {
                operatorTeams = operatorTeams.substring(0,
                    operatorTeams.length() - 1);
            }
        }

        operatorTeams = StringUtil.isNullOrEmpty(operatorTeams)
            ? GenericDataAggregator.DEFAULT_STRING_VAL : operatorTeams;
        DateTime end = new DateTime();
        log.trace("$$$ Operator teams calc time $$$@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return operatorTeams;
    }

    /**
     * Method to get operators current region name.
     * @param operator - the operator
     * @return current region name
     */
    private String getCurrentRegionName(Operator operator) {
        return operator.getCurrentRegion() != null
            ? operator.getCurrentRegion().getName()
            : GenericDataAggregator.DEFAULT_STRING_VAL;
    }

    /**
     * Method to get the latest sign-on time of the operator.
     * @param operator - the operator
     * @return sign in time of the operator
     */
    @SuppressWarnings("deprecation")
    private Integer getSignOnTime(Operator operator) {
        DateTime start = new DateTime();
        if (operator.getSignOffTime() == null) {
            Date signOn = operator.getSignOnTime();
            if (signOn != null) {
                signOn = DateUtil.convertTimeToSiteTime(signOn);
                return Integer.parseInt(String.format("%02d%02d",
                    signOn.getHours(), signOn.getMinutes()));
            }
        }
        DateTime end = new DateTime();
        log.trace("$$$  Sign On time calc time $$$@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return GenericDataAggregator.DEFAULT_TIME_VAL;
    }

    /**
     * Method to return the actual goal rate of the operator.
     * @param operator Operator.
     * @param actualRateMap 
     * @return actual goal rate value.
     */
    private Float getActualRate(Operator operator, Map<String, Double> actualRateMap) {
        Double actualRate = 0.00;
        Region currentReg = operator.getCurrentRegion();
        if (currentReg  != null
            && currentReg .getType() == RegionType.Selection) {
            String key = operator.getId() + DELIMITER + currentReg.getId();
            actualRate = actualRateMap.get(key);
        }

        return (actualRate == null || actualRate == 0.00) ? 0.00F : actualRate
            .floatValue();
    }

    /**
     * Method to get operator Route on which an operator is current working.
     * @param operator Operator.
     * @param operatorRouteMap 
     * @param routeRegionMap 
     * @param routesWorkedMap 
     * @return route of the operator.
     * @throws DataAccessException - dae
     */
    private String getRoute(Operator operator,
                            Map<Operator, String> operatorRouteMap,
                            Map<Region, List<String>> routeRegionMap,
                            Map<Region, String> routesWorkedMap)
        throws DataAccessException {
        List<String> operatorOnRoute = null;

        // Operator currently working on a Route meaning there is an assignment
        // which is in In progress state
        String route = operatorRouteMap.get(operator);
        
        // If there is no assignments in inprogress states, finding out if there
        // are any assignments still available/shorted in that region and
        // finding
        // the assignment which was last worked upon and returning the route
        // information of the same.
        if (StringUtil.isNullOrEmpty(route) && operator.getCurrentRegion() != null
            && operator.getCurrentRegion().getType() == RegionType.Selection) {

            operatorOnRoute = routeRegionMap.get(operator.getCurrentRegion());
            String routeLastWorked = null;
            
            if (routesWorkedMap != null) {
                routeLastWorked = routesWorkedMap.get(operator.getCurrentRegion());
            }
            
            // If operator is on a region and dint start working.
            // In that case routesWorked will be empty
            if (!StringUtil.isNullOrEmpty(routeLastWorked)) {
                if (operatorOnRoute != null && operatorOnRoute.contains(routeLastWorked)) {
                    route = routeLastWorked;
                }
            } else {
                route = GenericDataAggregator.DEFAULT_STRING_VAL;
            }
        }
        return route;
    }

    /**
     * Method to get formated route departure date.
     * @param route - object retrieved from database
     * @param isOnlyRoute - true if only route is to be required
     * @return formatted route and departure date
     * @throws DataAccessException dae
     */
    private String getFormatetdRouteDepartureDate(String route,
                                                 boolean isOnlyRoute) throws DataAccessException {
        
        if (StringUtil.isNullOrEmpty(route)
            || route.equals(GenericDataAggregator.DEFAULT_STRING_VAL)) {
            return GenericDataAggregator.DEFAULT_STRING_VAL;
        }
        
        final int routeIdx = 0;
        final int ddtIndx = 1;

        if (route.contains(DELIMITER)) {
            if (isOnlyRoute) {
                return ResourceUtil.getLocalizedKeyValue((String) route.split(DELIMITER)[routeIdx]);
            } else {
                Date ddt = new Date(Long.valueOf(route.split(DELIMITER)[ddtIndx]));
                return ResourceUtil.getLocalizedKeyValue(this.getAssignmentManager().getRouteDepartureDateField(
                    (String) route.split(DELIMITER)[routeIdx], ddt));
            }
        } else {
            return route;
        }
    }
    
    /**
     * Method to create map of open labor records.
     * @return created map
     * @throws DataAccessException - dae
     */
    private Map<Operator, OperatorLabor> openLaborRecordsMap()
        throws DataAccessException {
        List<OperatorLabor> openLaborRecord = this.laborManager
            .getOperatorLaborManager().listAllOpenRecords();
        Map<Operator, OperatorLabor> openLaborRecordsMap = new HashMap<Operator, OperatorLabor>(
            openLaborRecord.size());

        for (OperatorLabor record : openLaborRecord) {
            openLaborRecordsMap.put(record.getOperator(), record);
        }
        return openLaborRecordsMap;
    }

    /**
     * Method to create map of closed break labor records.
     * @param startTime time window start time
     * @param endTime time window end time
     * @return created map
     * @throws DataAccessException - dae
     */
    private Map<Operator, List<OperatorLabor>> closedBreakLaborRecordsMap(Date startTime,
                                                                          Date endTime)
        throws DataAccessException {
        DateTime start = new DateTime();
        // Get all the closed break labor record for the operator
        List<OperatorLabor> closedBreakLaborRecords = this.laborManager
            .getOperatorLaborManager().listAllClosedBreakRecords(startTime,
                endTime);
        DateTime qend = new DateTime();
        log.trace("$$$closedBreakLaborRecordsMap query time$$$" + "@@@"
            + (qend.getMillis() - start.getMillis()) + "@@@");

        Map<Operator, List<OperatorLabor>> closedMap = new HashMap<Operator, List<OperatorLabor>>(
            closedBreakLaborRecords.size());
        for (OperatorLabor operatorLabor : closedBreakLaborRecords) {
            List<OperatorLabor> list = closedMap.get(operatorLabor
                .getOperator());
            if (list == null) {
                list = new ArrayList<OperatorLabor>();
                closedMap.put(operatorLabor.getOperator(), list);
            }
            list.add(operatorLabor);
        }
        DateTime end = new DateTime();
        log.trace("$$$closedBreakLaborRecordsMap total time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return closedMap;
    }

    /**
     * Method to create operator actual rate map.
     * @return created map.
     */
    private Map<String, Double> getActualRateMap() {
        DateTime start = new DateTime();
        List<Map<String, Object>> actualRates = getLaborManager()
            .getOperatorLaborManager().listOperatorActualRateByRegion();
        DateTime qend = new DateTime();
        log.trace("$$$getActualRateMap query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        Map<String, Double>  opActualRateMap = new HashMap<String, Double>();
        
        for (Map<String, Object> map : actualRates) {
            Region region = (Region) map.get("Region");
            Double actualRate = (Double) map.get("ActualRate");
            Operator op = (Operator) map.get("Operator");
            
            String key = op.getId() + DELIMITER + region.getId();
            opActualRateMap.put(key, actualRate);
        }
        
        DateTime end = new DateTime();
        log.trace("$$$getActualRateMap total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return opActualRateMap;
    }

    /**
     * Method to create map of routes worked upon by operator.
     * @return created map
     * @throws DataAccessException dae
     */
    private Map<Operator, Map<Region, String>> getRoutesWorked() throws DataAccessException {
        DateTime start = new DateTime();
        List<Map<String, Object>> routesList = getAssignmentManager()
            .listRouteByLatestEndTime();
        DateTime qend = new DateTime();
        log.trace("$$$getRoutesWorked query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        Map<Operator, Map<Region, String>> routesWorkedMap = new HashMap<Operator, Map<Region, String>>();
        
        for (Map<String, Object> map : routesList) {
            Region region = (Region) map.get("Region");
            String route = (String) map.get("Route");
            Date ddt = (Date) map.get("DDT");
            Operator op = (Operator) map.get("Operator");
            
            //Create operator map
            Map<Region, String> opMap = routesWorkedMap.get(op);
            if (opMap == null) {
                opMap = new HashMap<Region, String>();
            }
            
            if (ddt != null) {
                opMap.put(region, route + DELIMITER + ddt.getTime());
            } else {
                opMap.put(region, route);
            }
            routesWorkedMap.put(op, opMap);
        }
        DateTime end = new DateTime();
        log.trace("$$$getRoutesWorked total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return routesWorkedMap;
    }

    /**
     * Method to to get all routes in a region.
     * @return create map
     * @throws DataAccessException - dae
     */
    private Map<Region, List<String>> getRouteRegionMap() throws DataAccessException {
        DateTime start = new DateTime();
        Map<Region, List<String>> routeByRegionMap = new HashMap<Region, List<String>>();
         
        List<Map<String, Object>> regionRouteMap = getAssignmentManager().listAllRouteByRegion();
        DateTime qend = new DateTime();
        log.trace("$$$getRouteRegionMap query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        for (Map<String, Object> map : regionRouteMap) {
            Region region = (Region) map.get("Region");
            String route = (String) map.get("Route");
            Date ddt = (Date) map.get("DDT");
            
            List<String> routes = routeByRegionMap.get(region);
            if (routes == null) {
                routes = new ArrayList<String>();
                routeByRegionMap.put(region, routes);
            }
            
            if (ddt != null) {
                routes.add(route + DELIMITER + ddt.getTime());
            } else {
                routes.add(route);
            }
        }    
        DateTime end = new DateTime();
        log.trace("$$$getRouteRegionMap total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return routeByRegionMap;
    }

    /**
     * Method to create map of operators in a route.
     * @return created map
     * @throws DataAccessException - dae
     */
    private Map<Operator, String> getOperatorsInRoutesMap() throws DataAccessException {
        DateTime start = new DateTime();
        List<Map<String, Object>> records = getAssignmentManager()
        .listAllOperatorsInRoutes();
        DateTime qend = new DateTime();
        log.trace("$$$getOperatorsInRoutesMap query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        Map<Operator, String> operatorRouteMap = new HashMap<Operator, String>();
        for (Map<String, Object> map : records) {
            String route = (String) map.get("Route");
            Date ddt = (Date) map.get("DepartureDateTime");
            Operator op = (Operator) map.get("Operator");
            
            if (ddt != null) {
                operatorRouteMap.put(op, route + DELIMITER + ddt.getTime());
            } else {
                operatorRouteMap.put(op, route); 
            }
        }
        DateTime end = new DateTime();
        log.trace("$$$getOperatorsInRoutesMap total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return operatorRouteMap;
        
    }

    /**
     * Method to create map of labor records per operator.
     * @param startTime time window start time
     * @param endTime time window end time
     * @return created map
     * @throws DataAccessException - dae
     */
    private Map<Long, List<OperatorLabor>> getLaborRecordsMap(Date startTime,
                                                              Date endTime)
        throws DataAccessException {
        Map<Long, List<OperatorLabor>> recordsMap = new HashMap<Long, List<OperatorLabor>>();
        DateTime start = new DateTime();
        List<OperatorLabor> recordList = this.laborManager
            .getOperatorLaborManager().listLaborRecordInTimeWindow(startTime,
                endTime);
        DateTime qend = new DateTime();
        log.trace("$$$getLaborRecordsMap query time$$$" + "@@@"
            + (qend.getMillis() - start.getMillis()) + "@@@");
        for (OperatorLabor laborRecord : recordList) {
            Long operatorId = laborRecord.getOperator().getId();

            List<OperatorLabor> existing = recordsMap.get(operatorId);
            if (existing == null) {
                existing = new ArrayList<OperatorLabor>();
                recordsMap.put(operatorId, existing);
            }

            existing.add(laborRecord);
        }
        DateTime end = new DateTime();
        log.trace("$$$getLaborRecordsMap total time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return recordsMap;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 