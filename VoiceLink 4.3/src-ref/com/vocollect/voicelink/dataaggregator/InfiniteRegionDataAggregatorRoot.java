/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * @author mraj
 *
 */
public class InfiniteRegionDataAggregatorRoot extends GenericDataAggregatorImpl {

    private static final Logger log = new Logger(
        InfiniteRegionDataAggregatorRoot.class);
    
    public static final String IDENTITY_COLUMN = "region";

    public static final String[] FIELDS = { IDENTITY_COLUMN, "regionnumber", "hoursremaining", "itemsremaining", 
        "itemspicked" };
    
    // constants
    public static final int INDEX_REGION_ID = 0;

    public static final int INDEX_DATA_VALUE = 1;
    
    private AssignmentManager assignmentManager;
    
    private RegionManager regionManager;
    
    private AssignmentLaborManager assignmentLaborManager;
    
    /**
     * @return the assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }
    
    /**
     * @param assignmentManager the assignmentManager to set
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }
    
    /**
     * @return the regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }
    
    /**
     * @param regionManager the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }
    
    /**
     * @return the assignmentLaborManager
     */
    public AssignmentLaborManager getAssignmentLaborManager() {
        return assignmentLaborManager;
    }
    
    /**
     * @param assignmentLaborManager the assignmentLaborManager to set
     */
    public void setAssignmentLaborManager(AssignmentLaborManager assignmentLaborManager) {
        this.assignmentLaborManager = assignmentLaborManager;
    }
    
    /* (non-Javadoc)
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### INFINITE REGION AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        try {
            List<Region> regions = getRegionManager().listByTypeOrderByName(
                RegionType.Selection);

            Map<Long, Long> qtyRemainingForhrsRemainingMap = createQuantityRemainingMap();
            Map<Long, Double> actualRateMap = createActualRateMap();
            Map<Long, Long> totalItemsRemainingMap = createTotalItemsRemainingMap();
            Map<Long, Long> totalItemsPickedMap = createTotalItemsPickedMap();

            for (Region region : regions) {
                Long qtyRemaining = qtyRemainingForhrsRemainingMap.get(region
                    .getId());
                Double actualRateValue = actualRateMap.get(region.getId());
                Double hoursRemaining = getHoursRemaining(qtyRemaining,
                    actualRateValue);
                Long totalItemsRemaining = totalItemsRemainingMap.get(region
                    .getId());
                totalItemsRemaining = totalItemsRemaining == null
                    ? 0 : totalItemsRemaining;
                Long totalItemsPicked = totalItemsPickedMap.get(region.getId());
                totalItemsPicked = totalItemsPicked == null
                    ? 0 : totalItemsPicked;

                JSONObject assignmentJSON = new JSONObject();
                assignmentJSON.put(IDENTITY_COLUMN, region.getName());
                assignmentJSON.put(FIELDS[1], region.getNumber() + "");
                assignmentJSON.put(FIELDS[2], hoursRemaining);
                assignmentJSON.put(FIELDS[3], totalItemsRemaining);
                assignmentJSON.put(FIELDS[4], totalItemsPicked);

                if (assignmentJSON != null) {
                    outputArray.put(assignmentJSON);
                }
            }
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }

        DateTime end = new DateTime();
        log.trace("$$$Infinite Aggregator Execution Time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return outputArray;
    }



    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;
        
        try {
            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN,
                DAFieldType.STRING.name(), "aggregator.infinite.region.field."
                    + IDENTITY_COLUMN, " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.infinite.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.infinite.region.field." + FIELDS[i], "hrs.", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.infinite.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.infinite.region.field." + FIELDS[i], " ", ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * method to create quantity remaining map for regions. these values are used for hours remaining calculation per region
     * @return map of region id and quantity remaining
     * @throws DataAccessException dae
     */
    private Map<Long, Long> createQuantityRemainingMap() throws DataAccessException {
        Map<Long, Long> itemsRemainingByRegionMap = new HashMap<Long, Long>();
        DateTime q2Start = new DateTime();
        List<Object[]> itemRemainingList = getAssignmentManager()
            .listTotalItemsRemainingForAllRegions();
        DateTime q2End = new DateTime();

        log.trace("$$$Total item remaining query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        for (Object[] itemRemainingObj : itemRemainingList) {
            itemsRemainingByRegionMap.put((Long) itemRemainingObj[INDEX_REGION_ID],
                (Long) itemRemainingObj[INDEX_DATA_VALUE]);
        }
        return itemsRemainingByRegionMap;
    }

    /**
     * method to create map of regions and corresponding acyual labor rate sum values.
     * @return map of region id and sum of actual labor rate
     * @throws DataAccessException dae
     */
    private Map<Long, Double> createActualRateMap() throws DataAccessException {
        Map<Long, Double> actualRateByRegionMap = new HashMap<Long, Double>();
        DateTime q2Start = new DateTime();
        List<Object[]> actualRateList = getAssignmentLaborManager()
            .listLaborActualRateForAllRegions();
        DateTime q2End = new DateTime();

        log.trace("$$$Actual rate for regions query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        for (Object[] actualRateObj : actualRateList) {
            actualRateByRegionMap.put((Long) actualRateObj[INDEX_REGION_ID],
                (Double) actualRateObj[INDEX_DATA_VALUE]);
        }
        return actualRateByRegionMap;
    }
    
    /**
     * method to create map of total items remaining for all regions.
     * @return map of regions and total items remaining
     * @throws DataAccessException dae
     */
    private Map<Long, Long> createTotalItemsRemainingMap() throws DataAccessException {
        DateTime q2Start = new DateTime();
        List<Object[]> qtyRemainingData = this.getAssignmentManager()
            .listQtyRemainingCasesChartData();
        DateTime q2End = new DateTime();

        log.trace("$$$Quantity remaining query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        
        Map<Long, Long> totalItemsRemainingMap = new HashMap<Long, Long>();
        for (Object[] itemRemainingObj : qtyRemainingData) {
            totalItemsRemainingMap.put((Long) itemRemainingObj[INDEX_REGION_ID],
                (Long) itemRemainingObj[INDEX_DATA_VALUE]);
        }
        
        return totalItemsRemainingMap;
    }
    
    /**
     * method to create map of total items picked for all regions.
     * @return map of regions and total items picked
     * @throws DataAccessException dae
     * @throws JSONException je
     */
    private Map<Long, Long> createTotalItemsPickedMap() throws DataAccessException, JSONException {
        DateTime q2Start = new DateTime();
        Site site = (Site) getParameters().get(GenericDataAggregator.SITE_PARAMETER);
        
        List<Object[]> qtyPickedData = this.getAssignmentManager()
            .listQtyPickedCasesChartData(site.getShiftStartDate());
        DateTime q2End = new DateTime();

        log.trace("$$$Quantity picked query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        
        Map<Long, Long> totalItemsPickedMap = new HashMap<Long, Long>();
        for (Object[] itemPickedObj : qtyPickedData) {
            totalItemsPickedMap.put((Long) itemPickedObj[INDEX_REGION_ID],
                (Long) itemPickedObj[INDEX_DATA_VALUE]);
        }
        
        return totalItemsPickedMap;
    }

    /**
     * Method to calculate the total number of hours required to complete the picking of remaining items.
     * @param itemsRemaining remaing item quantity
     * @param actualRate actual Rate of region
     * @return total hours remaining to finish the picking at the current actual rate
     * @exception DataAccessException data access exception
     */
    private Double getHoursRemaining(Long itemsRemaining, Double actualRate)
        throws DataAccessException {
        itemsRemaining = itemsRemaining == null ? 0 : itemsRemaining;
        
        if (itemsRemaining == 0) {
            return 0.0;
        }
        
        // calculate the total hours remaining
        Double hoursRemaining = 0.0;
        
        if (actualRate != null && actualRate.doubleValue() != 0.0) {
            hoursRemaining = itemsRemaining.doubleValue() / actualRate.doubleValue();
        }            

        return new BigDecimal(hoursRemaining)
                .setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 