/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.DataAggregatorFieldType;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author mraj
 * 
 */
public class AssignmentStatusDataAggregatorRoot extends
    GenericDataAggregatorImpl {

    private static final Logger log = new Logger(
        AssignmentStatusDataAggregatorRoot.class);

    public static final String IDENTITY_COLUMN = "routeatdeparturedate";

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "status", "numberofassignments" };

    // constants
    public static final int INDEX_ROUTE = 0;

    public static final int INDEX_DDT = 1;

    public static final int INDEX_STATUS_COUNT = 2;

    public static final int INDEX_STATUS = 3;

    public static final int INDEX_STATUS_FOR_STRING = 2;

    public static final String DELIMETER = "##";

    private AssignmentManager assignmentManager;

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute
     * ()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### ROUTE ASSIGNMENT STATUS AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        try {

            // default time window start time
            Date now = DateUtil.convertTimeToSiteTime(new Date());

            // default time window end time
            Date nowPlusTimeWindow = (new DateTime(now)).plusHours(24).toDate();

            now = getStartTime(now);

            nowPlusTimeWindow = getEndTime(nowPlusTimeWindow);

            AssignmentStatus[] statuses = {
                AssignmentStatus.Available, AssignmentStatus.Complete,
                AssignmentStatus.InProgress, AssignmentStatus.Short,
                AssignmentStatus.Suspended, AssignmentStatus.Passed };

            // unique Route + DDT falling in time window
            List<Object[]> routeDDT = new ArrayList<Object[]>();

            // Map containing values for route, departure date and status as
            // keys
            Map<String, Long> routeDDTStatusCountValues = new HashMap<String, Long>();

            // get assignment counts per status for every route
            seperateRouteByStatus(now, nowPlusTimeWindow, routeDDTStatusCountValues,
                routeDDT, Arrays.asList(statuses));

            for (Object[] obj : routeDDT) {
                String route = (String) obj[INDEX_ROUTE];
                Date departureDate = (Date) obj[INDEX_DDT];
                for (AssignmentStatus status : statuses) {
                    Object[] keyObj = { route, departureDate, status };
                    JSONObject assignmentJSON = new JSONObject();
                    assignmentJSON.put(
                        IDENTITY_COLUMN,
                        this.getAssignmentManager().getRouteDepartureDateField(
                            route, departureDate));
                    assignmentJSON.put(FIELDS[1], ResourceUtil
                        .getLocalizedKeyValue(status.getResourceKey()));
                    Long statusCountVal = (Long) routeDDTStatusCountValues
                        .get(getRouteDDTStatus(keyObj));
                    statusCountVal = statusCountVal == null
                        ? 0 : statusCountVal;
                    assignmentJSON.put(FIELDS[2], statusCountVal);
                    if (assignmentJSON != null) {
                        outputArray.put(assignmentJSON);
                    }
                }
            }
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }
        DateTime end = new DateTime();
        log.trace("$$$Route Assignment Status Data Aggregator Execution Time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");

        return outputArray;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;

        try {
            fieldsArray
                .put(getfieldJSON(IDENTITY_COLUMN, DAFieldType.STRING.name(),
                    "aggregator.assignment.status.field." + IDENTITY_COLUMN,
                    " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.assignment.status.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.assignment.status.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * Utility method to get route string for key in map.
     * @param obj - object containing key fields
     * @return - concatenated value as route##date##assignmentStatus
     */
    private String getRouteDDTStatus(Object[] obj) {
        return getRoute(obj) + DELIMETER
            + (AssignmentStatus) obj[INDEX_STATUS_FOR_STRING];
    }

    /**
     * Utility method to get route string for key in map.
     * @param obj - object containing key fields
     * @return - concatenated value as route##date
     */
    private String getRoute(Object[] obj) {
        return (String) obj[INDEX_ROUTE] + DELIMETER + ((Date) obj[INDEX_DDT]).getTime();
    }

    /**
     * Method to set count of Assignment with all statuses in the global
     * Route+DDT map.
     * @param startDate - the start date (now)
     * @param endDate - the end date (now +24 hrs)
     * @param routeDDTStatusCountValues - the global route + DDT map
     * @param routeDDT route departure date list
     * @param statuses assignment statuses required
     * @throws DataAccessException - dae
     */
    private void seperateRouteByStatus(Date startDate,
                                       Date endDate,
                                       Map<String, Long> routeDDTStatusCountValues,
                                       List<Object[]> routeDDT,
                                       List<AssignmentStatus> statuses)
        throws DataAccessException {

        DateTime abc1 = new DateTime();
        List<Object[]> assignmentCounts = getAssignmentManager()
            .listAssignmentCountByRouteDDTAndStatus(startDate, endDate, statuses);
        DateTime abc2 = new DateTime();
        log.trace("$$$Route Assignment Status Count query TIME$$$" + "@@@"
            + (abc2.getMillis() - abc1.getMillis()) + "@@@");
        Set<String> addedRoutes = new HashSet<String>();

        for (Object[] assignmentCount : assignmentCounts) {

            // Prepare routes used
            String route = (String) assignmentCount[INDEX_ROUTE];
            Date ddt = (Date) assignmentCount[INDEX_DDT];
            AssignmentStatus status = (AssignmentStatus) assignmentCount[INDEX_STATUS];
            Object[] routeObj = { route, ddt };
            Object[] routeStatusObj = { route, ddt, status };
            String routeObjVal = getRoute(routeObj);
            String routeStatusObjVal = getRouteDDTStatus(routeStatusObj);

            if (status != AssignmentStatus.Complete) {
                if (!addedRoutes.contains(routeObjVal)) {
                    routeDDT.add(routeObj);
                    addedRoutes.add(routeObjVal);
                }
            }

            routeDDTStatusCountValues.put(routeStatusObjVal,
                (Long) assignmentCount[INDEX_STATUS_COUNT]);
        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 