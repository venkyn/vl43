/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.DataAggregatorFieldType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public class RegionAssignmentStatusDataAggregatorRoot extends
    GenericDataAggregatorImpl {

    private static final Logger log = new Logger(
        RegionAssignmentStatusDataAggregatorRoot.class);

    public static final String IDENTITY_COLUMN = "regionininterval";

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "region", "departuredateinterval", "status",
        "numberofassignments" };

    public static final String DELIMITER = "##";

    private AssignmentManager assignmentManager;

    private RegionManager regionManager;

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * @return the regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * @param regionManager the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute
     * ()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### REGION ASSIGNMENT STATUS AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        try {

            // default time window start time
            Date now = DateUtil.convertTimeToSiteTime(new Date());

            // default time window end time
            Date nowPlusTimeWindow = null;

            now = getStartTime(now);

            nowPlusTimeWindow = getEndTime(nowPlusTimeWindow);
            
            List<Region> regions = getRegionManager().listByTypeOrderByName(
                RegionType.Selection);

            Integer[] intervals = getRegionManager()
                .getUserConfiguredIntervals(RegionDataAggregator.INTERVALS);
            
            List<Integer> intervalListValues = Arrays.asList(intervals);
            List<Integer> intervalList = new ArrayList<Integer>(intervalListValues);
            
            Collections.sort(intervalList);
            
            if (nowPlusTimeWindow != null) {
                List<Integer> intervalsToRemoveList = new ArrayList<Integer>(intervalList.size());
                
                for (int i = 0; i < intervalList.size(); i++) {
                    Integer intervalListVal = intervalList.get(i);
                    Date dateVal = new DateTime(now).plusHours(intervalListVal)
                        .toDate();
                    if (dateVal.after(nowPlusTimeWindow)) {
                        intervalsToRemoveList.add(intervalListVal);
                    }
                }
                
                intervalList.removeAll(intervalsToRemoveList);
            }

            AssignmentStatus[] statuses = {
                AssignmentStatus.Available, AssignmentStatus.Complete,
                AssignmentStatus.InProgress, AssignmentStatus.Short,
                AssignmentStatus.Suspended };
            
            Map<String, Long> assignmentCountMap = createRegionAssignmentStatusMap(now, intervalList, statuses);

            for (Region region : regions) {
                for (Integer interval : intervalList) {
                    String defaultIntervalDescription =  "Next " + String.format("%02d", interval) + " hrs.";
                    Object[] args = new Object[1];
                    args[0] = String.format("%02d", interval);
                    String intervalDescription = ResourceUtil.getLocalizedMessage(
                        "chart.interval.description", args, defaultIntervalDescription);
                    
                    for (AssignmentStatus status : statuses) {
                        String key = getRegionAssignmentStatusMapKey(region.getId(), interval, status);
                        Long assignmentCountValue = assignmentCountMap.get(key);
                        assignmentCountValue = assignmentCountValue == null ? 0 : assignmentCountValue;
                        JSONObject assignmentJSON = new JSONObject();
                        assignmentJSON.put(IDENTITY_COLUMN, region.getName()
                            + " in " + intervalDescription);
                        assignmentJSON.put(FIELDS[1], region.getName());
                        assignmentJSON.put(FIELDS[2], intervalDescription);
                        assignmentJSON.put(FIELDS[3], ResourceUtil
                            .getLocalizedKeyValue(status.getResourceKey()));
                        assignmentJSON.put(
                            FIELDS[4], assignmentCountValue);
                        if (assignmentJSON != null) {
                            outputArray.put(assignmentJSON);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }
        DateTime end = new DateTime();
        log.trace("$$$Region Assignment Status Data Aggregator Execution Time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return outputArray;
    }
    
    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;

        try {
            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN,
                DAFieldType.STRING.name(),
                "aggregator.region.assignment.status.field." + IDENTITY_COLUMN,
                " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.region.assignment.status.field." + FIELDS[i], " ",
                ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.region.assignment.status.field." + FIELDS[i], " ",
                ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.region.assignment.status.field." + FIELDS[i], " ",
                ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.assignment.status.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    /**
     * method to create formatted key to access route assignment status map.
     * @param regionId id of selected region
     * @param interval current interval
     * @param status assignment status
     * @return formatted string combining all three values
     */
    private String getRegionAssignmentStatusMapKey(Long regionId,
                                                   Integer interval,
                                                   AssignmentStatus status) {
        return regionId + DELIMITER + interval + DELIMITER + status;
    }
    
    /**
     * Utility method to identify interval based on ddt .
     * @param now - aggregator execution start time
     * @param intervals - list of system configured intervals to match
     * @param ddt - ddt to match interval range
     * @return identified interval
     */
    private Integer getInterval(Date now, List<Integer> intervals, Date ddt) {
        Integer interval = intervals.get(0);
        for (Integer intervalValue : intervals) {
            Date nowPlusInterval = (new DateTime(now)).plusHours(
                intervalValue).toDate();
            if (ddt.before(nowPlusInterval) || ddt.equals(nowPlusInterval)) {
                interval = intervalValue;
                break;
            }
        }
        
        return interval;
    }

    /**
     * method to create a map of assignment count on the basis of region id,
     * departure date and assignment status.
     * @param now aggregator execution start instance time
     * @param intervalList interval list
     * @param statuses status list
     * @return map of assignment count
     * @throws DataAccessException dae
     */
    private Map<String, Long> createRegionAssignmentStatusMap(Date now,
                                                              List<Integer> intervalList,
                                                              AssignmentStatus[] statuses)
        throws DataAccessException {
        Integer largestInterval = intervalList.get(intervalList.size() - 1);
        Date nowPlusInterval = (new DateTime(now)).plusHours(largestInterval)
            .toDate();

        DateTime abc1 = new DateTime();
        List<Map<String, Object>> regionAssignmentStatusMapList = getAssignmentManager()
            .listAssignmentsByDateAndStatusInAllRegions(now, nowPlusInterval,
                Arrays.asList(statuses));
        DateTime abc2 = new DateTime();
        log.trace("$$$REGION Assignment Count query TIME$$$" + "@@@"
            + (abc2.getMillis() - abc1.getMillis()) + "@@@");

        Map<String, Long> regionIntervalAssignmentStatusMap = new HashMap<String, Long>();

        for (Map<String, Object> regionAssignmentStatusMapObj : regionAssignmentStatusMapList) {
            Long regionId = (Long) regionAssignmentStatusMapObj.get("RegionId");
            Date deptDate = (Date) regionAssignmentStatusMapObj
                .get("DepartureDate");
            AssignmentStatus status = (AssignmentStatus) regionAssignmentStatusMapObj
                .get("AssignmentStatus");
            Long assignmentCount = (Long) regionAssignmentStatusMapObj
                .get("AssignmentCount");

            Integer intervalCategoryValue = getInterval(now, intervalList,
                deptDate);

            List<Integer> intervalsToUse = intervalList.subList(
                intervalList.indexOf(intervalCategoryValue),
                intervalList.size());

            for (Integer intervalListVal : intervalsToUse) {

                String key = getRegionAssignmentStatusMapKey(regionId,
                    intervalListVal, status);

                Long value = regionIntervalAssignmentStatusMap.get(key);
                if (value == null) {
                    value = 0L;
                }
                
                value = value + assignmentCount;
                regionIntervalAssignmentStatusMap.put(key, value);
            }
        }

        return regionIntervalAssignmentStatusMap;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 