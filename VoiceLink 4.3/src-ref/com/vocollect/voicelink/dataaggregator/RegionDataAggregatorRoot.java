/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This data aggregator exposed multiple columns related to region.
 * 
 * @author mraj
 */
public class RegionDataAggregatorRoot extends GenericDataAggregatorImpl {

    private static final Logger log = new Logger(RegionDataAggregatorRoot.class);

    public static final Integer[] INTERVALS = { 2, 4, 6, 8, 12, 16, 20, 24 };

    public static final String IDENTITY_COLUMN = "region";

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "departuredateinterval", "operatorsworking",
        "operatorrequired", "percentassignmentscomplete", "quantitytopick",
        "totalquantity", "interval" };

    private static final int QUANTITY_TO_PICK = 0;

    private static final int TOTAL_QUANTITY = 1;
    
    private static final int QUANTITY_PICKED = 2;

    private AssignmentManager assignmentManager;

    private RegionManager regionManager;

    private OperatorLaborManager operatorLaborManager;
    
    private OperatorManager operatorManager;
    
    private LaborManager laborManager;
    
    public static final String DELIMITER = "##";

    
    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    
    /**
     * @param laborManager the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }
    /**
     * @return the assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager the assignmentManager to set
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * @return the regionManager
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * @param regionManager the regionManager to set
     */
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * 
     * @return OperatorLaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * 
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }  
    
    
    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    
    /**
     * @param operatorManager the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }  
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute
     * ()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### REGION AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        try {

            // default time window start time
            Date now = DateUtil.convertTimeToSiteTime(new Date());

            // default time window end time
            Date nowPlusTimeWindow = null;

            now = getStartTime(now);

            nowPlusTimeWindow = getEndTime(nowPlusTimeWindow);
            
            List<Region> regions = getRegionManager().listByTypeOrderByName(
                RegionType.Selection);
            
            Integer[] intervals = getRegionManager()
                .getUserConfiguredIntervals(INTERVALS);
            
            List<Integer> intervalListValues = Arrays.asList(intervals);
            List<Integer> intervalList = new ArrayList<Integer>(intervalListValues);
            
            Collections.sort(intervalList);
            
            if (nowPlusTimeWindow != null) {
                List<Integer> intervalsToRemoveList = new ArrayList<Integer>(intervalList.size());
                
                for (int i = 0; i < intervalList.size(); i++) {
                    Integer intervalListVal = intervalList.get(i);
                    Date dateVal = new DateTime(now).plusHours(intervalListVal)
                        .toDate();
                    if (dateVal.after(nowPlusTimeWindow)) {
                        intervalsToRemoveList.add(intervalListVal);
                    }
                }
                
                intervalList.removeAll(intervalsToRemoveList);
            }
            
            Map<Region, Map<Date, Long>> regionDDTMap = new HashMap<Region, Map<Date, Long>>(
                regions.size());

            Map<Region, Map<Integer, Long[]>> regionValueMap = getRegionDDTValuesMap(
                now, intervals, regionDDTMap);
            
            Map<String, Double> actualRateMap = getActualRateMap();
            
            //Create operators per region map
            Map<Long, List<Long>> regionOperatorMap = getOperatorsInRegion();
            
            for (Region region : regions) {
                Integer operatorsWorking = 0;
                if (regionOperatorMap.get(region.getId()) != null) {
                    operatorsWorking = regionOperatorMap.get(region.getId()).size();
                }
                
                Map<Integer, Long[]> intervalMap = regionValueMap.get(region);
                if (intervalMap == null) {
                    intervalMap = initializeValue(intervals);
                }
                
                Map<Date, Long> ddtMap = regionDDTMap.get(region);
                for (Integer interval : intervalList) {
                    Long[] values = intervalMap.get(interval);
                    String defaultIntervalDescription = "Next " + String.format("%02d", interval) + " hrs.";
                    Object[] args = new Object[1];
                    args[0] = String.format("%02d", interval);
                    String intervalDescription =
                        ResourceUtil.getLocalizedMessage("chart.interval.description", 
                            args, defaultIntervalDescription);

                    Long operatorRequired = getOperatorRequired(region, interval,
                        regionOperatorMap, ddtMap, actualRateMap);
                    
                    Long totalQuantity = values[TOTAL_QUANTITY];
                    Long quantityToPick = values[QUANTITY_TO_PICK];
                    Long quantityPicked = values[QUANTITY_PICKED];
                    
                    Double percentWorkDone = getPercentWorkDone(totalQuantity.doubleValue(),
                        quantityPicked.doubleValue());

                    JSONObject regionJson = new JSONObject();
                    regionJson.put(IDENTITY_COLUMN, region.getName());
                    regionJson.put(FIELDS[1], intervalDescription);
                    regionJson.put(FIELDS[2], operatorsWorking);
                    regionJson.put(FIELDS[3], operatorRequired
                        - operatorsWorking);
                    regionJson.put(FIELDS[4], percentWorkDone);
                    regionJson.put(FIELDS[5], quantityToPick);
                    regionJson.put(FIELDS[6], totalQuantity);
                    regionJson.put(FIELDS[7], interval);

                    if (regionJson != null) {
                        outputArray.put(regionJson);
                    }
                }
            }

        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }
        DateTime end = new DateTime();
        log.trace("$$$Region Data Aggregator Execution Time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return outputArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;

        try {
            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN,
                DAFieldType.STRING.name(), "aggregator.region.field."
                    + IDENTITY_COLUMN, " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.region.field." + FIELDS[i], " ", ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }
    
    /**
     * Utility method to initialize interval quantity values array. <BR>
     * <b>IMPORTANT</b> to modify this method if adding more field in array
     * @param intervals - list of configured intervals to initialize
     * @return key - interval. Value=Quantity values
     */
    private Map<Integer, Long[]> initializeValue(Integer[] intervals) {
        
        Map<Integer, Long[]> intervalMap = new HashMap<Integer, Long[]>(intervals.length); 
        for (Integer intvl : intervals) {
            Long[] intervalValues = { 0L, 0L, 0L};
            intervalMap.put(intvl, intervalValues);
        }
        
        return intervalMap;
    }
    
    /**
     * Utility method to identify interval based on ddt .
     * @param now - aggregator execution start time
     * @param intervals - list of system configured intervals to match
     * @param ddt - ddt to match interval range
     * @return identified interval
     */
    private Integer getInterval(Date now, List<Integer> intervals, Date ddt) {
        Integer interval = intervals.get(0);
        for (Integer intervalValue : intervals) {
            Date nowPlusInterval = (new DateTime(now)).plusHours(
                intervalValue).toDate();
            if (ddt.before(nowPlusInterval) || ddt.equals(nowPlusInterval)) {
                interval = intervalValue;
                break;
            }
        }
        
        return interval;
    }
    
    /**
     * Method to get operators working in each region.
     * @return Map of region vs list of operator ids
     * @throws DataAccessException - dae
     */
    private Map<Long, List<Long>> getOperatorsInRegion()
        throws DataAccessException {
        Map<Long, List<Long>> regionOperatorMap = new HashMap<Long, List<Long>>();

DateTime q2Start = new DateTime();
        List<Map<String, Object>> mapList = getOperatorManager()
            .listCurrentlyInAllRegion();
DateTime q2End = new DateTime();
        log.trace("$$$Operators in Region query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        for (Map<String, Object> map : mapList) {
            Long regionId = (Long) map.get("RegionId");
            Long operatorId = (Long) map.get("OperatorId");
            List<Long> existing = (List<Long>) regionOperatorMap.get(regionId);

            if (existing == null) {
                existing = new ArrayList<Long>();
                regionOperatorMap.put(regionId, existing);
            }

            existing.add(operatorId);
        }
        return regionOperatorMap;
    }

    /**
     * Main method to create collection for storing data retrieved from database.
     * @param now - DA execution start time
     * @param intervals - list of system configured intervals
     * @param regionDDTMap 
     * @return Map of region vs. all values per interval.
     * @throws DataAccessException - dae
     */
    private Map<Region, Map<Integer, Long[]>> getRegionDDTValuesMap(Date now,
                                                                    Integer[] intervals,
                                                                    Map<Region, Map<Date, Long>> regionDDTMap)
        throws DataAccessException {
        
        Map<Region, Map<Integer, Long[]>> regionValMap = new HashMap<Region, Map<Integer, Long[]>>();
        
        //Calculate max interval
        List<Integer> intervalList = Arrays.asList(intervals);
        Collections.sort(intervalList);
        Integer maxInterval = intervalList.get(intervalList.size() - 1);
        Date endDate = (new DateTime(now)).plusHours(maxInterval).toDate();
        
        //fetch all quantities
        DateTime q2Start = new DateTime();
        List<Map<String, Object>> map = this.getAssignmentManager()
            .listAllQuantitiesByRegionDepartureDate(now, endDate);
        DateTime q2End = new DateTime();
        log.trace("$$$All Region quantities query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        
        for (Map<String, Object> map2 : map) {
            Region region = (Region) map2.get("Region");
            Date ddt = (Date) map2.get("DepartureDateTime");
            Long quantityPicked = (Long) map2.get("QuantityPicked");
            Long totalQuantity = (Long) map2.get("TotalQuantity");
            Integer interval = getInterval(now, intervalList, ddt);    
            
            //Initalize for null
            totalQuantity = totalQuantity != null ? totalQuantity : 0L;
            quantityPicked = quantityPicked != null ? quantityPicked : 0L;

            //Populate regionDDTMap
            Map<Date, Long> ddtMap = regionDDTMap.get(region);
            if (ddtMap == null) {
                ddtMap = new HashMap<Date, Long>();
                regionDDTMap.put(region, ddtMap);
            }
            ddtMap.put(ddt, totalQuantity - quantityPicked);
            
            //Populate regionValMap
            Map<Integer, Long[]> intervalMap = regionValMap.get(region);
            if (intervalMap == null) {
                intervalMap = initializeValue(intervals);
                regionValMap.put(region, intervalMap);
            }
            
            List<Integer> nextIntervals = intervalList.subList(
                intervalList.indexOf(interval), intervalList.size());
            for (Integer interval1 : nextIntervals) {
                Long[] intervalValues = intervalMap.get(interval1);
                
                intervalValues[TOTAL_QUANTITY] += totalQuantity;
                intervalValues[QUANTITY_PICKED] += quantityPicked;
                intervalValues[QUANTITY_TO_PICK] += (totalQuantity - quantityPicked);
            }
        }
        
        return regionValMap;
    }
    
    /**
     * Method to create operator actual rate map.
     * @return created map.
     */
    private Map<String, Double> getActualRateMap() {
        DateTime start = new DateTime();
        List<Map<String, Object>> actualRates = getLaborManager()
            .getOperatorLaborManager().listOperatorActualRateByRegion();
        DateTime qend = new DateTime();
        log.trace("$$$getActualRateMap query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        Map<String, Double>  opActualRateMap = new HashMap<String, Double>();
        
        for (Map<String, Object> map : actualRates) {
            Region region = (Region) map.get("Region");
            Double actualRate = (Double) map.get("ActualRate");
            Operator op = (Operator) map.get("Operator");
            
            String key = op.getId() + DELIMITER + region.getId();
            opActualRateMap.put(key, actualRate);
        }
        
        DateTime end = new DateTime();
        log.trace("$$$getActualRateMap total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return opActualRateMap;
    }
    
    /**
     * Method to get goal rate to be used for calculation.
     * @param region - the region
     * @param targetOperators - list of operators working in region
     * @param actualRateMap - actual rate map of operators per region
     * @return goal rate
     * @throws DataAccessException - dae
     */
    private Double getGoalRate(Region region,
                               List<Long> targetOperators,
                               Map<String, Double> actualRateMap)
        throws DataAccessException {
        Double goalRate = 0.0;

        if (targetOperators == null || targetOperators.isEmpty()) {
            goalRate = region.getGoalRate() * 1.0;
        } else {
            Double totalActualRateOfRegion = 0.0d;
            for (Long opId : targetOperators) {
                String key = opId + DELIMITER + region.getId();
                Double rate = actualRateMap.get(key) == null
                    ? 0.0d : actualRateMap.get(key);
                totalActualRateOfRegion += rate;
            }

            if (totalActualRateOfRegion.doubleValue() == 0.0d) {
                goalRate = region.getGoalRate() * 1.0;
            } else {
                goalRate = totalActualRateOfRegion / targetOperators.size();
            }
        }

        return goalRate;
    }

    /**
     * Method to return the Percent Work Done.
     * @param totalQuantity Double.
     * @param quantityPicked Double.
     * @return percentWorkDone Double.
     * @throws DataAccessException dae.
     */
    private Double getPercentWorkDone(Double totalQuantity,
                                      Double quantityPicked)
        throws DataAccessException {
        DateTime start = new DateTime();
        Double percentWorkDone = (totalQuantity == 0
            ? 0 : (quantityPicked / totalQuantity) * 100.0);
        BigDecimal bd = new BigDecimal(percentWorkDone);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

        DateTime end = new DateTime();
        log.trace("$$$percent work done calc time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return bd.doubleValue();
    }

    /**
     * Method to get total operators needed for an interval.
     * @param region - the region
     * @param interval - interval start date
     * @param regionOperatorMap - list of operators
     * @param ddtMap - map of quantity per ddt
     * @param actualRateMap 
     * @return total operators needed for interval duration
     * @throws DataAccessException - dae
     */
    private Long getOperatorRequired(Region region,
                                     Integer interval,
                                     Map<Long, List<Long>> regionOperatorMap,
                                     Map<Date, Long> ddtMap, Map<String, Double> actualRateMap)
        throws DataAccessException {
        List<Long> operators = regionOperatorMap.get(region.getId());
        Double rate = getGoalRate(region, operators, actualRateMap) / 60.00;
        Date date = new Date();
        Date endDate = (new DateTime(date)).plusHours(interval).toDate();
        Set<Date> dateSet = ddtMap == null ? new HashSet<Date>(0) : ddtMap
            .keySet();
        List<Date> dates = new ArrayList<Date>(dateSet);
        Collections.sort(dates);

        Double perMinWorkLoad = 0.0d;
        for (Date key : dates) {
            Date ddt = DateUtil.convertTimeToServerTime(key);
            if (!ddt.before(endDate)) {
                break;
            }

            Long qty = (Long) ddtMap.get(key);
            perMinWorkLoad += qty.doubleValue()
                / (new Double(ddt.getTime() - date.getTime()) / (60 * 1000));
        }

        Double totalOpsNeeded = perMinWorkLoad / rate;

        return ((Double) Math.ceil(totalOpsNeeded)).longValue();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 