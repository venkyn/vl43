/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.voicelink.core.model.DataAggregatorFieldType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public class RouteDataAggregatorRoot extends GenericDataAggregatorImpl {

    private static final Logger log = new Logger(RouteDataAggregatorRoot.class);

    public static final String IDENTITY_COLUMN = "routeatdeparturedate";

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "departuredate", "projecteddeparturedate",
        "projecteddeparturedelay", "operatorrequired",
        "percentageroutecomplete", "availableassignments",
        "shortedassignments", "completedassignments", "route" };

    private AssignmentManager assignmentManager;

    private OperatorLaborManager operatorLaborManager;
    
    private LaborManager laborManager;

    // constants
    private static final int QUANTITY_PICKED = 0;

    private static final int QUANTITY_TO_PICK = 1;

    private static final int COUNT_AVAILABLE = 2;

    private static final int COUNT_COMPLETE = 3;

    private static final int COUNT_SHORT = 4;

    private static final int DATA_VALUE = 2;

    private static final int INDEX_ROUTE = 0;

    private static final int INDEX_DDT = 1;

    private static final int INDEX_REGIONID = 2;

    private static final int INDEX_QTY_TOPICK = 3;

    public static final String DELIMITER = "##";

    /**
     * @return the assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager the assignmentManager to set
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * 
     * @return OperatorLaborManager value of the property
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * 
     * @param operatorLaborManager the new operatorLaborManager value
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    
    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    
    /**
     * @param laborManager the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute
     * ()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### ROUTE AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();
        JSONArray outputArray = new JSONArray();

        try {

            // default time window start time
            Date now = DateUtil.convertTimeToSiteTime(new Date());

            // default time window end time
            Date nowPlusTimeWindow = (new DateTime(now)).plusHours(24).toDate();

            now = getStartTime(now);

            nowPlusTimeWindow = getEndTime(nowPlusTimeWindow);

            // Global map for all route specific properties
            Map<String, Long[]> routeValues = new HashMap<String, Long[]>();

            // unique Route + DDT falling in time window
            List<Object[]> routeDDT = new ArrayList<Object[]>();

            // get assignment counts per status for every route
            seperateRouteByStatus(now, nowPlusTimeWindow, routeValues, routeDDT);

            // Region goal rate map
            Map<Long, Integer> regionGoalRateMap = new HashMap<Long, Integer>();

            // get quantity picked for every route
            Map<String, Long> routeRegionQuantityAvailableValues = geQuantityAvailableByRoutePerRegion(
                now, nowPlusTimeWindow, regionGoalRateMap);

            // get quantity picked for every route
            geQuantityPicked(now, nowPlusTimeWindow, routeValues);

            // Get total Quantity of every route
            geTotalQuantity(now, nowPlusTimeWindow, routeValues);

            // get operators working in every region of all routes
            Map<String, Set<Long>> operatorsInRouteRegion = getOperatorsInRouteRegion(
                now, nowPlusTimeWindow);

            // operators working in all regions declared. fetched once when
            // needed.
            Map<Long, List<Long>> operatorsInRegion = getOperatorsInRegion();

            //Map of operators' actual rate in every region
            Map<String, Double> actualRateMap = getActualRateMap();
            
            // calculate DA fields for every route falling in time window
            for (Object[] obj : routeDDT) {
                String route = (String) obj[INDEX_ROUTE];
                Date departureDate = (Date) obj[INDEX_DDT];
                String routeDDt = getRoute(route, departureDate);

                JSONObject routeJSON = new JSONObject();
                routeJSON.put(IDENTITY_COLUMN, this.getAssignmentManager()
                    .getRouteDepartureDateField(route, departureDate));
                routeJSON.put(FIELDS[1],
                    DateUtil.convertTimeToServerTime(departureDate));
                Map<String, Object> valMap = getProjectedDepartureDate(route,
                    departureDate, routeRegionQuantityAvailableValues,
                    operatorsInRouteRegion, operatorsInRegion,
                    regionGoalRateMap, actualRateMap);
                Date projectedDepartureDate = (Date) valMap.get("PDD");
                Double operatorRequired = (Double) valMap
                    .get("OperatorRequired");
                
                routeJSON.put(FIELDS[2], projectedDepartureDate);
                routeJSON.put(
                    FIELDS[3],
                    getProjectedDepartureDelay(projectedDepartureDate,
                        departureDate));
                routeJSON.put(FIELDS[4], operatorRequired.intValue());
                routeJSON.put(FIELDS[5],
                    getProgressPercentage(route, departureDate, routeValues));
                if (routeValues.get(routeDDt)[COUNT_AVAILABLE] != null) {
                    routeJSON.put(FIELDS[6],
                        routeValues.get(routeDDt)[COUNT_AVAILABLE]);
                } else {
                    routeJSON.put(FIELDS[6], 0);
                }

                if (routeValues.get(routeDDt)[COUNT_SHORT] != null) {
                    routeJSON.put(FIELDS[7],
                        routeValues.get(routeDDt)[COUNT_SHORT]);
                } else {
                    routeJSON.put(FIELDS[7], 0);
                }

                if (routeValues.get(routeDDt)[COUNT_COMPLETE] != null) {
                    routeJSON.put(FIELDS[8],
                        routeValues.get(routeDDt)[COUNT_COMPLETE]);
                } else {
                    routeJSON.put(FIELDS[8], 0);
                }
                routeJSON.put(FIELDS[9], route);

                if (routeJSON != null) {
                    outputArray.put(routeJSON);
                }
            }
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }
        DateTime end = new DateTime();
        log.trace("$$$Route Data Aggregator Execution Time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return outputArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;

        try {
            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN,
                DAFieldType.STRING.name(), "aggregator.route.field."
                    + IDENTITY_COLUMN, " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.DATE.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Date.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.DATE.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Date.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Minutes.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.route.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.route.field." + FIELDS[i],
                DataAggregatorFieldType.Count.getResourceKey(), ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.route.field." + FIELDS[i], " ", ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    /**
     * Utility method to get route string for key in map.
     * @param obj - object containing key fields
     * @return - concatenated value as route||date
     */
    private String getRoute(Object[] obj) {
        return getRoute(obj[0], obj[1]);
    }

    /**
     * Utility method to get route string for key in map.
     * @param route - the route
     * @param departureDate - the departure date of route
     * @return - concatenated value as route||date
     */
    private String getRoute(Object route, Object departureDate) {
        return (String) route + DELIMITER + ((Date) departureDate).getTime();
    }

    /**
     * Method to set count of Assignment with all statuses in the global
     * Route+DDT map.
     * @param startDate - the start date (now)
     * @param endDate - the end date (now +24 hrs)
     * @param routeValues - the global route + DDT map
     * @param routeDDT route departure date list
     * @throws DataAccessException - dae
     */
    private void seperateRouteByStatus(Date startDate,
                                       Date endDate,
                                       Map<String, Long[]> routeValues,
                                       List<Object[]> routeDDT)
        throws DataAccessException {
        // Get assignment count map per status
        List<AssignmentStatus> statuses = Arrays.asList(
            AssignmentStatus.Available, AssignmentStatus.Complete,
            AssignmentStatus.Short, AssignmentStatus.InProgress,
            AssignmentStatus.Suspended, AssignmentStatus.Passed);

        DateTime abc1 = new DateTime();
        List<Object[]> assignmentCounts = getAssignmentManager()
            .listAssignmentCountByRouteDDTAndStatus(startDate, endDate, statuses);
        DateTime abc2 = new DateTime();
        log.trace("$$$Route Assignment Status Count query TIME$$$" + "@@@"
            + (abc2.getMillis() - abc1.getMillis()) + "@@@");
        List<String> addedRoutes = new ArrayList<String>();

        for (Object[] assignmentCount : assignmentCounts) {
            Long[] values = routeValues.get(getRoute(assignmentCount));
            if (values == null) {
                values = new Long[5];
            }

            // Prepare routes used
            String route = (String) assignmentCount[INDEX_ROUTE];
            Date ddt = (Date) assignmentCount[INDEX_DDT];
            Long count = (Long) assignmentCount[DATA_VALUE] == null
                ? 0L : (Long) assignmentCount[DATA_VALUE];
            Object[] obj = { route, ddt };
            String routeDDTVal = getRoute(obj);

            AssignmentStatus key = (AssignmentStatus) assignmentCount[3];
            switch (key) {
            case Available:
                values[COUNT_AVAILABLE] = count;
                if (!addedRoutes.contains(routeDDTVal)) {
                    routeDDT.add(obj);
                    addedRoutes.add(routeDDTVal);
                }
                break;
            case Complete:
                // Do not add route here
                values[COUNT_COMPLETE] = count;
                break;
            case Short:
                values[COUNT_SHORT] = count;
                if (!addedRoutes.contains(routeDDTVal)) {
                    routeDDT.add(obj);
                    addedRoutes.add(routeDDTVal);
                }
                break;
            default:
                if (!addedRoutes.contains(routeDDTVal)) {
                    routeDDT.add(obj);
                    addedRoutes.add(routeDDTVal);
                }
                break;
            }
            routeValues.put(getRoute(assignmentCount), values);
        }

    }

    /**
     * Method to get quantity available per route per region.
     * @param startDate - window start time
     * @param endDate - window end time
     * @param regionGoalRateMap - map region and its goal rate
     * @return - map . key= route+DDT+regionId. Value = quantity to pick
     * @throws DataAccessException - dae
     */
    private Map<String, Long> geQuantityAvailableByRoutePerRegion(Date startDate,
                                                  Date endDate,
                                                  Map<Long, Integer> regionGoalRateMap)
        throws DataAccessException {
        Map<String, Long> routeRegionQuantityAvailableValues = new HashMap<String, Long>();
        DateTime abc3 = new DateTime();
        List<Object[]> qtyAvlblRouteRegion = getAssignmentManager()
            .listTotalQuantityAvailablePerRouteDDTByRegion(startDate, endDate);
        DateTime abc4 = new DateTime();
        log.trace("$$$REGION QTY query TIME$$$" + "@@@"
            + (abc4.getMillis() - abc3.getMillis()) + "@@@");
        for (Object[] qtyAvlbl : qtyAvlblRouteRegion) {
            String key = getRoute(qtyAvlbl) + DELIMITER
                + (Long) qtyAvlbl[INDEX_REGIONID];
            Long quantityToPick = qtyAvlbl[INDEX_QTY_TOPICK] == null
                ? 0L : (Long) qtyAvlbl[INDEX_QTY_TOPICK];
            routeRegionQuantityAvailableValues.put(key, quantityToPick);

            Integer goalRate = qtyAvlbl[4] == null ? 0 : (Integer) qtyAvlbl[4];
            regionGoalRateMap.put((Long) qtyAvlbl[INDEX_REGIONID], goalRate);
        }
        DateTime abc5 = new DateTime();

        log.trace("$$$REGION QTY LOOP TIME$$$" + "@@@"
            + (abc5.getMillis() - abc4.getMillis()) + "@@@");

        return routeRegionQuantityAvailableValues;

    }

    /**
     * Method to get quantity picked per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @param routeValues - the global route + DDT map
     * @throws DataAccessException - dae
     */
    private void geQuantityPicked(Date startDate,
                                  Date endDate,
                                  Map<String, Long[]> routeValues)
        throws DataAccessException {
        DateTime q2Start = new DateTime();
        List<Object[]> qtyPickedRouteDDT = getAssignmentManager()
            .listQuantityPickedPerRouteDDT(startDate, endDate);
        DateTime q2End = new DateTime();

        log.trace("$$$Quantity picked per route query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");

        DateTime abc1 = new DateTime();
        for (Object[] qtyPicked : qtyPickedRouteDDT) {
            Long[] values = routeValues.get(getRoute(qtyPicked));
            if (values == null) {
                values = new Long[5];
            }

            Long quantityPicked = qtyPicked[DATA_VALUE] == null
                ? 0L : (Long) qtyPicked[DATA_VALUE];
            values[QUANTITY_PICKED] = quantityPicked;
            routeValues.put(getRoute(qtyPicked), values);
        }
        DateTime abc2 = new DateTime();

        log.trace("$$$LOOP TIME$$$" + "@@@"
            + (abc2.getMillis() - abc1.getMillis()) + "@@@");

    }

    /**
     * Method to get total quantity per route.
     * @param startDate - window start time
     * @param endDate - window end time
     * @param routeValues - the global route + DDT map
     * @throws DataAccessException - dae
     */
    private void geTotalQuantity(Date startDate,
                                 Date endDate,
                                 Map<String, Long[]> routeValues)
        throws DataAccessException {
        DateTime q3Start = new DateTime();
        List<Object[]> qtyToPickRouteDDT = getAssignmentManager()
            .listTotalQuantityPerRouteDDT(startDate, endDate);
        DateTime q3End = new DateTime();

        for (Object[] qtyToPick : qtyToPickRouteDDT) {
            Long[] values = routeValues.get(getRoute(qtyToPick));
            if (values == null) {
                values = new Long[5];
            }

            Long quantityToPick = qtyToPick[DATA_VALUE] == null
                ? 0L : (Long) qtyToPick[DATA_VALUE];
            values[QUANTITY_TO_PICK] = quantityToPick;
            routeValues.put(getRoute(qtyToPick), values);
        }

        log.trace("$$$Quantity to pick per route query time$$$" + "@@@"
            + (q3End.getMillis() - q3Start.getMillis()) + "@@@");

    }

    /**
     * Method to get operators in Route + Region combination.
     * @param startDate - the start date
     * @param endDate - the end date
     * @return - map. Key=Route+DDT. Value = Set of operator Ids
     * @throws DataAccessException - dae
     */
    private Map<String, Set<Long>> getOperatorsInRouteRegion(Date startDate,
                                                             Date endDate)
        throws DataAccessException {
        DateTime start = new DateTime();
        Map<String, Set<Long>> operatorsInRoute = new HashMap<String, Set<Long>>();

        DateTime q1Start = new DateTime();
        List<Object[]> values = getAssignmentManager()
            .listOperatorsByRouteRegion(startDate, endDate);
        DateTime q1End = new DateTime();
        log.trace("$$$OperatorsByRouteRegion query time$$$@@@"
            + (q1End.getMillis() - q1Start.getMillis()) + "@@@");

        for (Object[] object : values) {
            String routeRegionMapKey = getRoute(object) + DELIMITER + object[2];
            Set<Long> ops = operatorsInRoute.get(routeRegionMapKey);
            if (ops == null || ops.isEmpty()) {
                ops = new HashSet<Long>();
                operatorsInRoute.put(routeRegionMapKey, ops);
            }

            ops.add((Long) object[3]);
        }

        DateTime end = new DateTime();
        log.trace("$$$getOperatorsInRouteRegion calc time$$$@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return operatorsInRoute;
    }

    /**
     * Method to get Map of Operators per region .
     * @return map of Region and Operator IDs per region
     */
    private Map<Long, List<Long>> getOperatorsInRegion() {
        DateTime start = new DateTime();

        List<Operator> operators = getAssignmentManager()
            .listOperatorsInRegion();
        DateTime qEnd = new DateTime();
        log.trace("$$$getOperatorsInRegion query time$$$" + "@@@"
            + (qEnd.getMillis() - start.getMillis()) + "@@@");
        Map<Long, List<Long>> operatorsInRegion = new HashMap<Long, List<Long>>();
        for (Operator operator : operators) {
            if (operator.getCurrentRegion() == null) {
                continue;
            }

            List<Long> ops = operatorsInRegion.get(operator.getCurrentRegion());
            if (ops == null || ops.isEmpty()) {
                ops = new ArrayList<Long>();
                operatorsInRegion.put(operator.getCurrentRegion().getId(), ops);
            }

            ops.add(operator.getId());
        }

        DateTime end = new DateTime();
        log.trace("$$$getOperatorsInRegion Execution Time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        return operatorsInRegion;

    }
    
    /**
     * Method to create operator actual rate map.
     * @return created map.
     */
    private Map<String, Double> getActualRateMap() {
        DateTime start = new DateTime();
        List<Map<String, Object>> actualRates = getLaborManager()
            .getOperatorLaborManager().listOperatorActualRateByRegion();
        DateTime qend = new DateTime();
        log.trace("$$$getActualRateMap query time$$$"
            + "@@@" + (qend.getMillis() - start.getMillis()) + "@@@");
        Map<String, Double>  opActualRateMap = new HashMap<String, Double>();
        
        for (Map<String, Object> map : actualRates) {
            Region region = (Region) map.get("Region");
            Double actualRate = (Double) map.get("ActualRate");
            Operator op = (Operator) map.get("Operator");
            
            String key = op.getId() + DELIMITER + region.getId();
            opActualRateMap.put(key, actualRate);
        }
        
        DateTime end = new DateTime();
        log.trace("$$$getActualRateMap total time$$$"
            + "@@@" + (end.getMillis() - start.getMillis()) + "@@@");
        return opActualRateMap;
    }

    /**
     * method to calculate the progress percentage of a route.
     * 
     * @param route - the route
     * @param departureDate departure date
     * @param routeValues map of route values
     * @return percentage of progress
     * @throws DataAccessException - dae
     */
    private Float getProgressPercentage(String route,
                                        Date departureDate,
                                        Map<String, Long[]> routeValues)
        throws DataAccessException {

        Long quantityToPick = routeValues.get(getRoute(route, departureDate))[QUANTITY_TO_PICK];
        Long quantityPicked = routeValues.get(getRoute(route, departureDate))[QUANTITY_PICKED];

        Float progress = (quantityPicked.floatValue() / quantityToPick
            .floatValue()) * 100;
        BigDecimal bd = new BigDecimal(progress);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }

    /**
     * Calculates the projected departure date for a route.
     * 
     * @param route The route to consider
     * @param departureDate The departure date of the route
     * @param routeRegionQtyAvlblValuesMap - map of route and Quantity available
     * @param operatorsInRouteRegion - map of route + region and set of
     *            operators
     * @param operatorsInRegion - map of region and set of operators in it.
     * @param regionGoalRateMap - map of region and its goal rate
     * @param actualRateMap 
     * @return The projected departure date
     * @exception BusinessRuleException if error retrieving data
     * @throws NumberFormatException nfe
     */
    private Map<String, Object> getProjectedDepartureDate(String route,
                                           Date departureDate,
                                           Map<String, Long> routeRegionQtyAvlblValuesMap,
                                           Map<String, Set<Long>> operatorsInRouteRegion,
                                           Map<Long, List<Long>> operatorsInRegion,
                                           Map<Long, Integer> regionGoalRateMap, Map<String, Double> actualRateMap)
        throws BusinessRuleException, NumberFormatException {
        DateTime start = new DateTime();
        Date projectedDepartureDate = new Date();

        String routeMapKey = getRoute(route, departureDate) + DELIMITER;
        List<Long> regionsInRoute = new ArrayList<Long>();
        Set<String> routeRegionKeys = routeRegionQtyAvlblValuesMap.keySet();

        for (String key : routeRegionKeys) {
            int index = key.indexOf(routeMapKey);
            if (index > -1) {
                key = key.replace(routeMapKey, "");
                // Region region = regionManager.get(Long.valueOf(key));
                regionsInRoute.add(Long.valueOf(key));
            }
        }

        // For each region
        Double maxProjectedTimeToFinish = 0.0;
        Double operatorRequired = 0.0;
        for (Long regionId : regionsInRoute) {
            String key = routeMapKey + regionId;

            if (!routeRegionQtyAvlblValuesMap.containsKey(key)) {
                continue;
            }

            // Get quantity available in the region
            Long quantityAvailableInRegion = routeRegionQtyAvlblValuesMap
                .get(key);

            // Find operators working in the region
            List<Long> targetOperators = null;
            if (operatorsInRouteRegion.get(key) != null) {
                targetOperators = new ArrayList<Long>(
                    operatorsInRouteRegion.get(key));
            } else {
                targetOperators = new ArrayList<Long>();
            }

            boolean routeInProgressForRegion = false;
            if (targetOperators.isEmpty()) {
                // If not operator found, find operators working in same
                // region
                targetOperators = operatorsInRegion.get(regionId);
            } else {
                routeInProgressForRegion = true;
            }

            Double totalActualRateOfRegion = 0.0;
            Double operatorRequiredForRegion = 0.0;

            if ((targetOperators == null) || (targetOperators.isEmpty())) {
                // If still we don't find any operator, then there is no
                // operator working in the region, project how many
                // operators required to finish the route in time
                operatorRequiredForRegion = projectOperatorRequired(
                    regionGoalRateMap.get(regionId), quantityAvailableInRegion,
                    departureDate);

                totalActualRateOfRegion = regionGoalRateMap.get(regionId)
                    * operatorRequiredForRegion * 1.0;

            } else {
                // If we find operator in region, find out the total goal
                // rate of the operators
                for (Long opId : targetOperators) {
                    String mapKey = opId + DELIMITER + regionId;
                    Double rate = actualRateMap.get(mapKey) == null
                        ? 0.0d : actualRateMap.get(mapKey);
                    totalActualRateOfRegion += rate;
                }

                // If goal rate in null, means operators do not have any
                // labor data yet
                if (totalActualRateOfRegion == 0.0) {
                    operatorRequiredForRegion = projectOperatorRequired(
                        regionGoalRateMap.get(regionId),
                        quantityAvailableInRegion, departureDate);

                    totalActualRateOfRegion = regionGoalRateMap.get(regionId)
                        * operatorRequiredForRegion * 1.0;

                } else {
                    Double goalRate = totalActualRateOfRegion
                        / targetOperators.size();

                    operatorRequiredForRegion = projectOperatorRequired(
                        goalRate.intValue(), quantityAvailableInRegion,
                        departureDate);

                }

                // If route is in progress for region then only assume
                // operators are working
                if (routeInProgressForRegion) {
                    operatorRequiredForRegion = operatorRequiredForRegion
                        - targetOperators.size();
                }
            }

            operatorRequired = operatorRequired + operatorRequiredForRegion;

            // Projected time to finish is in minutes
            Double projectedTimeToFinish = quantityAvailableInRegion
                / totalActualRateOfRegion;

            if (maxProjectedTimeToFinish < projectedTimeToFinish) {
                maxProjectedTimeToFinish = projectedTimeToFinish;
            }

        }

        Long projectedTimeToFinishInSecond = Math
            .round(maxProjectedTimeToFinish * 3600);

        projectedDepartureDate = ((new DateTime())
            .plusSeconds(projectedTimeToFinishInSecond.intValue())).toDate();

        DateTime end = new DateTime();
        log.trace("$$$Projected departure date and Operator required calc time$$$@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");
        
        // Need to return a map as the calculation for both is same. Executing the 
        // calculation twice could be expensive.
        Map<String, Object> returnMap = new HashMap<String, Object>(2);
        returnMap.put("PDD", projectedDepartureDate);
        returnMap.put("OperatorRequired", operatorRequired);
        return returnMap;
    }

    /**
     * @param goalRate The actual goal rate or region goal rate
     * @param quantityAvailable Quantity to pick
     * @param departureTime When departure of route to scheduled
     * @return Calculated number of operator to finish in time
     */
    private Double projectOperatorRequired(Integer goalRate,
                                           Long quantityAvailable,
                                           Date departureTime) {
        Double projectedOperatorRequired;

        Date now = DateUtil.convertTimeToSiteTime(new Date());

        Integer minutesToDeparture = Minutes.minutesBetween(new DateTime(now),
            new DateTime(departureTime)).getMinutes();

        Double goalRatePerMinute = goalRate / 60.00;

        if (minutesToDeparture > 0) {
            projectedOperatorRequired = quantityAvailable
                / (goalRatePerMinute * minutesToDeparture);
        } else {
            projectedOperatorRequired = quantityAvailable / goalRatePerMinute;
        }
        

        return Math.ceil(projectedOperatorRequired);
    }

    /**
     * 
     * @param projectedDepartureDate pdd
     * @param departureDate add
     * @return difference in minutes
     */
    private Integer getProjectedDepartureDelay(Date projectedDepartureDate,
                                               Date departureDate) {
        Date departureDateServerTZ = DateUtil
            .convertTimeToServerTime(departureDate);

        Minutes differenceInMinutes = Minutes.minutesBetween(new DateTime(
            departureDateServerTZ), new DateTime(projectedDepartureDate));
        Integer projectedDepartureDelay = differenceInMinutes.getMinutes();
        return projectedDepartureDelay;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 