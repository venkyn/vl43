/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.voicelink.selection.dao.AssignmentDAO;
import com.vocollect.voicelink.selection.model.AssignmentStatus;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * @author smittal
 *
 */
public class RouteOtherStatusDataAggregatorRoot extends
GenericDataAggregatorImpl {

    private static final Logger log = new Logger(
        RouteOtherStatusDataAggregatorRoot.class);

    public static final String IDENTITY_COLUMN = "route";

    public static final String[] FIELDS = { IDENTITY_COLUMN, "status", "percentageroutecomplete" };

    private AssignmentDAO assignmentDAO;

    private static final int IDX_QTY_PICKED = 0;

    private static final int IDX_QTY_REMAINING = 1;
    
    private static final String[] STATUSES = {"Completed", "NotStarted"};

    /**
     * @return the assignmentDAO
     */
    public AssignmentDAO getAssignmentDAO() {
        return assignmentDAO;
    }


    /**
     * @param assignmentDAO the assignmentDAO to set
     */
    public void setAssignmentDAO(AssignmentDAO assignmentDAO) {
        this.assignmentDAO = assignmentDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute
     * ()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        log.trace("############### ROUTE STATUS AGGREGATOR EXECUTION #################");
        DateTime start = new DateTime();

        JSONArray outputArray = new JSONArray();

        try {
            // Get the site for the shift start time (start of day)
            Site site = (Site) getParameters().get(GenericDataAggregator.SITE_PARAMETER);

            List<AssignmentStatus> list = new LinkedList<AssignmentStatus>(
                Arrays.asList(AssignmentStatus.values()));
            list.remove(AssignmentStatus.Canceled);

            List<Object> routes = this.getAssignmentDAO().listRoutes(list);

            Map<String, Long[]> qtyValuesMap = createQantityValuesMap(site
                .getShiftStartDate());

            for (Object object : routes) {

                String route = (String) object;

                Long quantityPicked = 0L;
                Long quantityToPick = 0L;

                Long[] quantityValues = qtyValuesMap.get(route);

                if (quantityValues != null) {
                    quantityPicked = quantityValues[IDX_QTY_PICKED];
                    quantityToPick = quantityValues[IDX_QTY_REMAINING];
                }

                Double progress = 0.0;
                String routeStatus = "InProgress";
                if (quantityPicked == null || quantityPicked == 0) {
                    routeStatus = STATUSES[1];
                } else if (quantityToPick == null || quantityToPick == 0) {
                    routeStatus = STATUSES[0];
                    progress = 100.0;
                }

                List<String> statusesToConsider = Arrays.asList(STATUSES);

                if (statusesToConsider.contains(routeStatus)) {
                    JSONObject routeJson = new JSONObject();
                    routeJson.put(IDENTITY_COLUMN, route);
                    routeJson.put(FIELDS[1], routeStatus);
                    routeJson.put(FIELDS[2], progress);
                    outputArray.put(routeJson);
                }

            }
        } catch (JSONException e) {
            throw new BusinessRuleException(e);
        } catch (DataAccessException e) {
            throw new BusinessRuleException(e);
        }

        DateTime end = new DateTime();
        log.trace("$$$Route Status Aggregator Execution Time$$$" + "@@@"
            + (end.getMillis() - start.getMillis()) + "@@@");

        return outputArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;
        
        try {
            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN, DAFieldType.STRING.name(),
                "aggregator.route.status.field." + IDENTITY_COLUMN, " ", i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.route.status.field." + FIELDS[i], " ", ++i));
            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.route.status.field." + FIELDS[i], " ", ++i));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    /**
     * method to create a map of routes and corresponding total quatity picked and remaining values.
     * @param shiftStartDate shift start date time
     * @return map
     * @throws DataAccessException dae
     */
    private Map<String, Long[]> createQantityValuesMap(Date shiftStartDate) throws DataAccessException {
        DateTime q2Start = new DateTime();
        List<Map<String, Object>> quantityRemainigMapList = getAssignmentDAO()
            .listQuantityRemainingforAllRoutes();
        DateTime q2End = new DateTime();

        log.trace("$$$Quantity remaining query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");

        Map<String, Long[]> quantityValuesMap = createQuantityPickedMap(shiftStartDate);

        for (Map<String, Object> qtyRemainingMapObj : quantityRemainigMapList) {
            String route = (String) qtyRemainingMapObj.get("Route");
            Long qtyRemaining = (Long) qtyRemainingMapObj
                .get("TotalQuantityRemaining");

            Long[] quantityValuesList = quantityValuesMap.get(route);

            if (quantityValuesList == null) {
                quantityValuesList = new Long[2];

                quantityValuesList[IDX_QTY_PICKED] = 0L;
            }

            quantityValuesList[IDX_QTY_REMAINING] = qtyRemaining;

            quantityValuesMap.put(route, quantityValuesList);
        }

        return quantityValuesMap;
    }
    
    /**
     * Method to create map of qty picked values per route.
     * @param shiftStartDate shift start date time
     * @return map
     * @throws DataAccessException dae
     */
    private Map<String, Long[]> createQuantityPickedMap(Date shiftStartDate) throws DataAccessException {
        DateTime q2Start = new DateTime();
        List<Map<String, Object>> quantityPickedMapList = getAssignmentDAO()
            .listQuantityPickedTodayForAllRoutes(shiftStartDate);
        DateTime q2End = new DateTime();

        log.trace("$$$Quantity picked query time$$$" + "@@@"
            + (q2End.getMillis() - q2Start.getMillis()) + "@@@");
        
        Map<String, Long[]> quantitypickedMap = new HashMap<String, Long[]>();

        for (Map<String, Object> qtyPickedMapObj : quantityPickedMapList) {
            String route = (String) qtyPickedMapObj.get("Route");
            Long qtyPicked = (Long) qtyPickedMapObj.get("TotalQuantityPicked");

            Long[] qtyValuesList = new Long[2];

            qtyValuesList[IDX_QTY_PICKED] = qtyPicked;
            qtyValuesList[IDX_QTY_REMAINING] = 0L;

            quantitypickedMap.put(route, qtyValuesList);
        }
        
        return quantitypickedMap;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 