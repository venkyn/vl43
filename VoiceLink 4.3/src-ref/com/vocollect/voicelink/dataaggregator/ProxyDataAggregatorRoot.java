/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.DataAggregatorFieldType;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This data aggregator exposes different fields of Proxy. The fields exposed by
 * this aggregator are as follows.
 * 
 * Proxy Server Name Proxy Server Hang Duration ODR Rejection Count IO Error
 * Count Average Trip Time Data Format Error Count LUT/ODR Execution Error Count
 * Connection Bounced Count
 * 
 * @author kudupi
 */
public class ProxyDataAggregatorRoot extends GenericDataAggregatorImpl {

    public static final String IDENTITY_COLUMN = "proxyhostname";

    private static final Logger log = new Logger(ProxyDataAggregatorRoot.class);

    public static final String[] FIELDS = {
        IDENTITY_COLUMN, "proxyserverstatus", "odrrejectioncount",
        "ioerrorcount", "averagetriptime", "dataformaterrorcount",
        "lutodrexecutionerrorcount", "connectionbouncedcount" };

    /**
     * The JMX connection url.
     */
    private final String connectorAddress = "service:jmx:rmi://localhost/jndi/rmi://localhost:5000/jmxrmi";

    /**
     * Key for the statistics object in proxy server.
     */
    private final String statObjKey = "VL3 Proxy Server:type=monitoring,name=Statistics";

    private final String proxyStatus = "OK";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#execute()
     */
    @Override
    public JSONArray execute() throws BusinessRuleException {
        JSONArray outputArray = new JSONArray();
        JSONObject proxyJSON = new JSONObject();
        // Set up JMX connection
        JMXServiceURL url = null;
        try {

            url = new JMXServiceURL(connectorAddress);
            JMXConnector conn = JMXConnectorFactory.connect(url);
            MBeanServerConnection server = conn.getMBeanServerConnection();
            
            ObjectName statObj = new ObjectName(statObjKey);
            proxyJSON.put(IDENTITY_COLUMN, InetAddress.getLocalHost().getHostName());
            proxyJSON.put(FIELDS[1], proxyStatus);
            proxyJSON.put(FIELDS[2], server.getAttribute(statObj, "ODRREJ"));
            proxyJSON.put(FIELDS[3], server.getAttribute(statObj, "ERRIO"));

            Double avgTripDouble = (Double) server.getAttribute(statObj,
                "AVGTTRIP");
            BigDecimal bd = new BigDecimal(avgTripDouble);
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            proxyJSON.put(FIELDS[4], bd.doubleValue());
            proxyJSON.put(FIELDS[5], server.getAttribute(statObj, "ERRFMT1"));
            proxyJSON.put(FIELDS[6], server.getAttribute(statObj, "ERREXE"));
            proxyJSON.put(FIELDS[7], server.getAttribute(statObj, "BOUNCED"));

            conn.close();
        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            proxyJSON = getExceptionJSONObject(ioe);
        } catch (MalformedObjectNameException mone) {
            proxyJSON = getExceptionJSONObject(mone);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (AttributeNotFoundException anfe) {
            proxyJSON = getExceptionJSONObject(anfe);
        } catch (InstanceNotFoundException infe) {
            proxyJSON = getExceptionJSONObject(infe);
        } catch (MBeanException mbe) {
            proxyJSON = getExceptionJSONObject(mbe);
        } catch (ReflectionException re) {
            proxyJSON = getExceptionJSONObject(re);
        }

        if (proxyJSON != null) {
            outputArray.put(proxyJSON);
        }

        log.info(outputArray.toString());
        return outputArray;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImplRoot#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray fieldsArray = new JSONArray();

        int i = 1;

        try {

            fieldsArray.put(getfieldJSON(IDENTITY_COLUMN,
                DAFieldType.STRING.name(), "aggregator.proxy.field."
                    + IDENTITY_COLUMN, " ", i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.STRING.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.FLOAT.name(),
                "aggregator.proxy.field." + FIELDS[i], DataAggregatorFieldType.milliseconds.getResourceKey(), ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

            fieldsArray.put(getfieldJSON(FIELDS[i], DAFieldType.INTEGER.name(),
                "aggregator.proxy.field." + FIELDS[i], " ", ++i));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fieldsArray;
    }

    @Override
    public String getIdentityColumnName() {
        return IDENTITY_COLUMN;
    }

    /**
     * @param e Exception.
     * @return exceptionObj JSONObject.
     */
    private JSONObject getExceptionJSONObject(Exception e) {
        JSONObject exceptionObj = new JSONObject();

        try {
            exceptionObj.put(IDENTITY_COLUMN, InetAddress.getLocalHost().getHostName());
            exceptionObj.put(
                FIELDS[1],
                ResourceUtil
                    .getLocalizedKeyValue("aggregator.proxy.server.error")
                    + e.toString().split(":")[0]);
            exceptionObj.put(FIELDS[2], 0);
            exceptionObj.put(FIELDS[3], 0);
            exceptionObj.put(FIELDS[4], 0.0);
            exceptionObj.put(FIELDS[5], 0);
            exceptionObj.put(FIELDS[6], 0);
            exceptionObj.put(FIELDS[7], 0);
        } catch (JSONException e1) {
            e1.printStackTrace();
        } catch (UnknownHostException uhe) {
            uhe.printStackTrace();
        }
        return exceptionObj;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 