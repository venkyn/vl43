/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging.exceptions;

/**
 * Thrown when a duplicate or overlapping range is detected.
 *
 * @author dgold
 *
 */
public class DuplicateErrorTypeException extends RuntimeException {

    /**
     * Generated Serialization.externalization stream ID - don't change.
     */
    private static final long serialVersionUID = 52955701763020425L;


    /**
     * Constructor.
     * @param reason - a message describing the exception
     */
    public DuplicateErrorTypeException(String reason) {
        super(reason);
    }

    /**
     * Constructor.
     * @param reason - a message describing the exception
     * @param e - the original exception thrown
     */
    public DuplicateErrorTypeException(String reason, Throwable e) {
        super(reason, e);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 