/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging.exceptions;

/**
 * Thrown when a duplicate error code is detected.
 *
 * @author dgold
 *
 */
public class DuplicateErrorCodeException extends RuntimeException {

    /**
     * Generated Serialization.externalization stream ID - don't change.
     */
    private static final long serialVersionUID = 7231764731016370834L;

    /**
     * Constructor.
     * @param reason - a message describing the error reason
     */
    public DuplicateErrorCodeException(String reason) {
        super(reason);
    }

    /**
     * Constructor.
     * @param reason - a message describing the error reason
     * @param e - the original exception caught and thrown
     */
    public DuplicateErrorCodeException(String reason, Throwable e) {
        super(reason, e);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 