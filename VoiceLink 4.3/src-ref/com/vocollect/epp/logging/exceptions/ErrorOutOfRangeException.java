/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging.exceptions;

/**
 * Thrown when an error is assigned a number which is out of the range of error
 * codes represented by the instance of ErrorBase.
 *
 * @author dgold
 *
 */
public class ErrorOutOfRangeException extends RuntimeException {

    /**
     * Generated Serialization.externalization stream ID - don't change.
     */

    private static final long serialVersionUID = -8600799508702130467L;

    /**
     * Constructor.
     * @param reason - a message describing the exception
     */
    public ErrorOutOfRangeException(String reason) {
        super(reason);
    }

    /**
     * Constructor.
     * @param reason - a message describing the exception
     * @param e - the original exception thrown
     */
    public ErrorOutOfRangeException(String reason, Throwable e) {
        super(reason, e);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 