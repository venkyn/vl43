/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging.exceptions;

/**
 * Thrown when the error code already exists.
 *
 * @author dgold
 *
 */
public class ErrorCodeIsAlreadySetException extends RuntimeException {

    /**
     * Generated Serialization.externalization stream ID - don't change.
     */
    private static final long serialVersionUID = -3267681415655724350L;


    /**
     * Constructor.
     */
    public ErrorCodeIsAlreadySetException() {
        super();
    }

    /**
     * Constructor.
     * @param message - a message describing the exception
     * @param t - the original exception thrown
     */
    public ErrorCodeIsAlreadySetException(String message, Throwable t) {
        super(message, t);
    }

    /**
     * Constructor.
     * @param message - a message describing the exception
     */
    public ErrorCodeIsAlreadySetException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * @param t - the original exception thrown
     */
    public ErrorCodeIsAlreadySetException(Throwable t) {
        super(t);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 