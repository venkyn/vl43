/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import org.apache.log4j.NDC;

/**
 * Convenient wrapper for the log4j NDC (Nested Diagnostic Context) This
 * provides easy setters for the pieces of information we consider necessary
 * information for reporting errors for each thread. Pretty much just POD, with
 * a validation performed before pushing on the stack.
 *
 * <pre></pre>
 *
 * The class will check to see that all setters have been called before pushing
 * onto the stack, and throw an exception if not.
 *
 * <pre></pre>
 *
 * The NDC stack is always present, per thread.
 *
 * <pre></pre>
 *
 * When destroyed, the context will pop itself off of the stack
 *
 * <pre></pre>
 *
 * @author dgold
 *
 */
public class ThreadContext {


    /**
     * Constructor.
     */
    public ThreadContext() {
        super();
    }

    // Thread id <br>
    private long   threadID;

    // Purpose of thread (worker, servicing request)
    private String purpose;

    // IP address of the client, if applicable
    private String clientIP;

    // IP address of the clilent's proxy, if applicable
    private String proxyIP;

    // Device ID, if applicable
    private String deviceID;

    // Request being served (text) if applicable.
    private String requestedService;

    /**
     * @return Returns the clientIP.
     */
    public String getClientIP() {
        return this.clientIP;
    }

    /**
     * @param ip The clientIP to set.
     */
    public void setClientIP(String ip) {
        this.clientIP = ip;
    }

    /**
     * @return Returns the proxyIP.
     */
    public String getProxyIP() {
        return this.proxyIP;
    }

    /**
     * @param ip The proxyIP to set.
     */
    public void setProxyIP(String ip) {
        this.proxyIP = ip;
    }

    /**
     * @return Returns the purpose.
     */
    public String getPurpose() {
        return this.purpose;
    }

    /**
     * @param purpVal The purpose to set.
     */
    public void setPurpose(String purpVal) {
        this.purpose = purpVal;
    }

    /**
     * @return Returns the requestedService.
     */
    public String getRequestedService() {
        return this.requestedService;
    }

    /**
     * @param reqService The requestedService to set.
     */
    public void setRequestedService(String reqService) {
        this.requestedService = reqService;
    }

    /**
     * @return Returns the terminalID.
     */
    public String getDeviceID() {
        return this.deviceID;
    }

    /**
     * @param terminalID The terminalID to set.
     */
    public void setDeviceID(String terminalID) {
        this.deviceID = terminalID;
    }

    /**
     * @return Returns the threadID.
     */
    public long getThreadID() {
        return this.threadID;
    }

    /**
     * @param id The threadID to set.
     */
    public void setThreadID(long id) {
        this.threadID = id;
    }

    /**
     * Initialize the context after population of the vars.
     */
    public void setContext() {
        String aggregateContext = this.purpose + " thread, ID " + this.threadID
            + " servicing request: " + this.requestedService + " from client: "
            + this.clientIP + " through proxy " + this.proxyIP + " (device id "
            + this.deviceID + ")";
        NDC.push(aggregateContext);
    }

    /**
     * Pop the context information off the stack when destroying this.
     * @see java.lang.Object#finalize()
     * @throws Throwable - unhandled exception
     */
    protected void finalize() throws Throwable {
        try {
            NDC.pop(); // remove context from stack
            NDC.remove();
        } finally {
            super.finalize();
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 