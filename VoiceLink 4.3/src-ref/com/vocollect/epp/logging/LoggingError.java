/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for the logging app.
 * @author dgold
 *
 */
public final class LoggingError extends ErrorCode {

    /**
     * Lower boundary of the EPP logging error range.
     */
    public static final int EPP_LOWER_BOUND  = 2000;

    /**
     * Upper boundary of the EPP logging error range.
     */
    public static final int EPP_UPPER_BOUND  = 2999;

    /** No error, just the base initialization. */
    public static final LoggingError NO_ERROR = new LoggingError();

    /** No file requested to be opened. */
    public static final LoggingError NO_FILE_REQUESTED = new LoggingError(2002);

    /** Output stream could not be flushed. */
    public static final LoggingError FLUSH_CLIENT_OUTPUT_STREAM_FAILED
        = new LoggingError(2004);

    /**
     * Attempted to add an entry to the open zip file failed. See associated
     * text.
     */
    public static final LoggingError ADD_ZIP_ENTRY_FAILED
        = new LoggingError(2005);

    /** The user did not select any files. */
    public static final LoggingError NO_FILES_SELECTED = new LoggingError(2007);

    /**
     * Failed to copy data from input stream to output stream. See associated
     * text
     */
    public static final LoggingError UNABLE_TO_COPY_DATA_TO_ZIP
        = new LoggingError(2008);

    /**
     * Attempt to close the source (BufferedInputStream) for the zip file
     * failed. See associated text.
     */
    public static final LoggingError UNABLE_TO_CLOSE_SOURCE_STREAM
        = new LoggingError(2009);

    /**
     * Attempt to close the output stream for the zip file failed. See
     * associated text.
     */
    public static final LoggingError UNABLE_TO_CLOSE_ZIP_STREAM
        = new LoggingError(2010);

    /**
     * Encoding for the URL for the filename on the client is not supported.
     */
    public static final LoggingError UNSUPPORTED_URL_ENCODING
        = new LoggingError(2011);

    /** Could not find source file for reading with a buffered reader. */
    public static final LoggingError FILE_NOT_FOUND = new LoggingError(2012);

    /** Caught exception trying to close buffered reader - see
     * accopmanying text. */
    public static final LoggingError UNABLE_TO_CLOSE_SELECTED_FILE
        = new LoggingError(2013);

    /** Client output stream is unavailable. */
    public static final LoggingError UNABLE_TO_OBTAIN_CLIENT_OUTPUT_STREAM
        = new LoggingError(2014);

    /** Cannot get System directory to view logs. Property not set, or
     * wrong property being queried. */
    public static final LoggingError UNABLE_TO_OPEN_SYSTEM_LOG
        = new LoggingError(2015);

    /** File not found in the FileView list. */
    public static final LoggingError FILE_NOT_IN_VIEW = new LoggingError(2016);

    /** File IO error. */
    public static final LoggingError FILE_IO_ERROR = new LoggingError(2017);

    /** The user did not select any files. */
    public static final LoggingError SELECTED_FILES_LIST_IS_NULL = new LoggingError(2018);

    /**
     * Constructor.
     */
    private LoggingError() {
        super("EPP", EPP_LOWER_BOUND, EPP_UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private LoggingError(long err) {
        super(LoggingError.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 