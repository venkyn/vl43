/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import com.vocollect.epp.errors.ErrorCode;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.log4j.Appender;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;
import org.apache.log4j.spi.LoggerRepository;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Logging class which is primarily a pass-through to the log4j logger. Addition
 * of a parameter to the Logger's error, warn, fatal debug, info and trace
 * methods which enforces the use of an error code (for error, fatal) and allows
 * the use of an error code when logging a message.
 *
 * <pre></pre>
 *
 * A parameter for the ErrorCode was added to the Info, Debug, and Trace levels,
 * but the additional parameter is not mandatory.
 *
 * <pre></pre>
 *
 * This class will auto-check ranges & error codes within those ranges, more as
 * a convenience than anything else. It also has the standard log4j interface
 *
 * <pre></pre>
 *
 * This was originally envisioned as a wrapper, which was changed to an
 * inheritance during code review. Subsequent testing and research indicates
 * that the wrapper (delegate) method was appropriate, which caused this final
 * rev of the Logger. Refer to the log4j manual, chapter 8 for additional
 * reasons.
 *
 *
 * @author dgold
 *
 */

public class Logger {

    /**
     * This String stores the Logger class name.
     */
    private static final String     FQCN = Logger.class.getName() + ".";

    /**
     * Used as an instance of log3j Logger.
     */
    private org.apache.log4j.Logger logger;

    /**
     * Creates another com.vocollect.errors.Logger different name.
     * @postcondition name of logger == name
     * @param name - the name of the logger
     */
    public Logger(final String name) {
        this.logger = org.apache.log4j.Logger.getLogger(name);
    }

    /**
     * Constructor which accepts the logger class to get the correct logger from
     * the repository.
     * @param clazz - the class of the logger
     */
    public Logger(final Class<?> clazz) {
        this(clazz.getName());

    }

    /**
     * Log a message object with the ERROR Level.
     * @param message - the message to be logged
     */
    @Deprecated
    public void error(Object message) {
        error(message.toString(), null, null);
    }

    /**
     * Log a message object with the ERROR Level.
     * @param message - the message to be logged
     */
    @Deprecated
    public void error(String message) {
        error(message, null, null);
    }

    /**
     * Log a message object with the ERROR Level.
     * @see org.apache.log4j.Category#error(java.lang.Object,
     *      java.lang.Throwable)
     * @param message - message object to log
     * @param t - the exception to log
     */
    @Deprecated
    public void error(final Object message, final Throwable t) {
        error(message.toString(), null, t);
    }

    /**
     * Log a message object with the ERROR Level.
     * @param message - message object to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void error(Object message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        error(message.toString(), errorCode, null);
    }

    /**
     * Log a message object with the ERROR Level.
     * @param message - message string to log
     * @param errorCode - representation of the ErrorCode to report.
     */

    public void error(String message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        error(message, errorCode, null);
    }

    /**
     * Log a message object with the ERROR level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - message object to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void error(Object message, ErrorCode errorCode, Throwable t) {
        error(message.toString(), errorCode, t);
    }

    /**
     * Log a message object with the ERROR level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void error(String message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        this.logger.log(FQCN, Level.ERROR, "[ERR:" + errorCodeString(errorCode)
            + "] " + message, t);

    }

    /**
     * Log a message object with the ERROR Level.
     * @see org.apache.log4j.Category#fatal(java.lang.Object,
     *      java.lang.Throwable)
     * @param message - message object to log
     * @param t - the exception to log
     */
    @Deprecated
    public void fatal(Object message, Throwable t) {
        fatal(message.toString(), null, t);
    }

    /**
     * Log a message object with the ERROR Level.
     * @see org.apache.log4j.Category#fatal(java.lang.Object)
     * @param message - message object to log
     */
    @Deprecated
    public void fatal(Object message) {
        fatal(message.toString(), null, null);
    }

    /**
     * Log a message object with the FATAL Level.
     * @param message - the message object to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void fatal(Object message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        fatal(message.toString(), errorCode, null);
    }

    /**
     * Log a message object with the FATAL Level.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void fatal(String message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        fatal(message, errorCode, null);
    }

    /**
     * Log a message object with the FATAL level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message object to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void fatal(Object message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        fatal(message.toString(), errorCode, t);
    }

    /**
     * Log a message object with the FATAL level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void fatal(String message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        this.logger.log(FQCN, Level.FATAL, "[ERR:" + errorCodeString(errorCode)
            + "] " + message , t);

    }


    /**
     * Log a message object with the DEBUG level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message object to log
     * @param t - the exception to log
     */
    public void debug(Object message, Throwable t) {
        this.logger.log(FQCN, Level.DEBUG, message, t);
    }

    /**
     * Log a message object with the DEBUG level.
     * @param message - the message object to log
     */
    public void debug(Object message) {
        this.logger.log(FQCN, Level.DEBUG, message, null);
    }

    /**
     * Log a message object with the DEBUG level.
     * @param message - the message object to log
     * @param errorCode representation of the ErrorCode to report.
     */
    public void debug(Object message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        debug(message.toString(), errorCode, null);
    }

    /**
     * Log a message object with the DEBUG level.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void debug(String message, ErrorCode errorCode) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        debug(message, errorCode, null);
    }

    /**
     * Log a message object with the DEBUG level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void debug(Object message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        debug(message.toString(), errorCode, t);
    }

    /**
     * Log a message string with the FATAL level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void debug(String message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        this.logger.log(FQCN, Level.DEBUG, "[" + errorCodeString(errorCode)
            + "] " + message, t);
    }

    /**
     * Log a message object with the INFO level including the stack trace of the
     * Throwable t passed as parameter.
     * @param message - the message string to log
     * @param t - the exception to log
     */
    public void info(Object message, Throwable t) {
        this.logger.log(FQCN, Level.INFO, message, t);
    }

    /**
     * Log a message object with the INFO level.
     * @param message - the message object to be logged
     */
    public void info(Object message) {
        this.logger.log(FQCN, Level.INFO, message, null);
    }

    /**
     * Log a message object with the INFO level.
     * @param message - the message object to be logged
     * @param errorCode - representation of the ErrorCode to report
     */
    public void info(Object message, ErrorCode errorCode) {
        info(message, errorCode, null);
    }

    /**
     * Log a message string with the INFO level.
     * @param message - the message string to be logged
     * @param errorCode - representation of the ErrorCode to report
     */
    public void info(String message, ErrorCode errorCode) {
        info(message, errorCode, null);
    }

    /**
     * Log a message object with the INFO level.
     * @param message - the message object to be logged
     * @param errorCode - representaiton of the ErrorCode to report
     * @param t - exception to be logged
     */
    public void info(Object message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        info(message.toString(), errorCode, t);
    }

    /**
     * Log a message object with the INFO level including the stack trace of the
     * Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void info(String message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        this.logger.log(FQCN, Level.INFO, "[" + errorCodeString(errorCode)
            + "] " + message, t);
    }

    /**
     * Log a message object with the TRACE level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param t - the exception to log
     */
    public void trace(Object message, Throwable t) {
        this.logger.log(FQCN, Level.TRACE, message, t);
    }

    /**
     * Log a message object with the TRACE level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     */
    public void trace(Object message) {
        this.logger.log(FQCN, Level.TRACE, message, null);
    }

    /**
     * Log a message object with the TRACE.
     * @param message - message object to be logged
     * @param errorCode representation of the ErrorCode to report
     */
    public void trace(Object message, ErrorCode errorCode) {
        trace(message, errorCode, null);
    }

    /**
     * Log a message string with the TRACE level.
     * @param message - message string to be logged
     * @param errorCode - representation of the ErrorCode to report
     */
    public void trace(String message, ErrorCode errorCode) {
        trace(message, errorCode, null);
    }

    /**
     * Log a message object with the TRACE level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void trace(Object message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        trace(message.toString(), errorCode, t);
    }

    /**
     * Log a message string with the TRACE level including the stack trace of
     * the Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void trace(String message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        this.logger.log(FQCN, Level.TRACE, "[" + errorCodeString(errorCode)
            + "] " + message, t);
    }

    /**
     * Log a message object with the WARN level including the stack trace of the
     * Throwable t passed as parameter.
     * @param message - the message string to log
     * @param t - the exception to log
     */
    public void warn(Object message, Throwable t) {
        this.logger.log(FQCN, Level.WARN, message, t);
    }

    /**
     * Log a message object with the WARN level.
     * @param message - the message string to log
     */
    public void warn(Object message) {
        this.logger.log(FQCN, Level.WARN, message, null);
    }

    /**
     * Log a message object with the WARN level.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void warn(Object message, ErrorCode errorCode) {
       warn(message.toString(), errorCode, null);
    }

    /**
     * Log a message string with the WARN level.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     */
    public void warn(String message, ErrorCode errorCode) {
        warn(message, errorCode, null);
    }

    /**
     * Log a message object with the WARN level including the stack trace of the
     * Throwable t passed as parameter.
     * @param message - the message object to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void warn(Object message, ErrorCode errorCode, Throwable t) {
        // Turn the object into a string, prepend errorCode to it.
        // call delegate with the resulting string
        warn(message.toString(), errorCode, t);
    }

    /**
     * Log a message string with the WARN level including the stack trace of the
     * Throwable t passed as parameter.
     * @param message - the message string to log
     * @param errorCode - representation of the ErrorCode to report.
     * @param t - the exception to log
     */
    public void warn(String message, ErrorCode errorCode, Throwable t) {
        this.logger.log(FQCN, Level.WARN, "[" + errorCodeString(errorCode) + "] " + message, t);
    }

    /* Delegate methods below... */

    /**
     * Delegate.
     * @param appender - the Appender to be added
     * @see org.apache.log4j.Category#addAppender(org.apache.log4j.Appender)
     */
    public void addAppender(Appender appender) {
        this.logger.addAppender(appender);
    }

    /**
     * Delegate.
     * @param assertion - the value to be asserted
     * @param message - the message to be asserted
     * @see org.apache.log4j.Category#assertLog(boolean, java.lang.String)
     */
    public void assertLog(boolean assertion, String message) {
        this.logger.assertLog(assertion, message);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#callAppenders(org.apache.log4j.spi.LoggingEvent)
     * @param event - the LoggingEvent that is sent to the appenders
     */
    public void callAppenders(LoggingEvent event) {
        this.logger.callAppenders(event);
    }

    /**
     * Delegate.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param obj - the object to compare for equality
     * @return - true if the two objects are equal, otherwise false
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return false;
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getAdditivity()
     * @return - the additivity value
     */
    public boolean getAdditivity() {
        return this.logger.getAdditivity();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getAllAppenders()
     * @return - an Enumeration of all of the appenders
     */
    public Enumeration<Appender> getAllAppenders() {
        return this.logger.getAllAppenders();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getAppender(java.lang.String)
     * @param appender - the name of the appender
     * @return - the Appender specified by name
     */
    public Appender getAppender(String appender) {
        return this.logger.getAppender(appender);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getEffectiveLevel()
     * @return - the effective Level of the logger
     */
    public Level getEffectiveLevel() {
        return this.logger.getEffectiveLevel();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getLoggerRepository()
     * @return - the LoggerRepository of the logger
     */
    public LoggerRepository getLoggerRepository() {
        return this.logger.getLoggerRepository();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#getResourceBundle()
     * @return - the ResourceBundle of the logger
     */
    public ResourceBundle getResourceBundle() {
        return this.logger.getResourceBundle();
    }

    /**
     * Delegate.
     * @see java.lang.Object#hashCode()
     * @return - the hashCode of the logger
     */
    @Override
    public int hashCode() {
        return this.logger.hashCode();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#isAttached(org.apache.log4j.Appender)
     * @param appender - the appender to check
     * @return - true if the appender is attached, false otherwise
     */
    public boolean isAttached(Appender appender) {
        return this.logger.isAttached(appender);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#isDebugEnabled()
     * @return - true if debug is enabled, false otherwise
     */
    public boolean isDebugEnabled() {
        return this.logger.isDebugEnabled();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#isEnabledFor(org.apache.log4j.Priority)
     * @param priority - the priority level to be checked
     * @return - true if logger is enabled for the specified priority
     */
    public boolean isEnabledFor(Priority priority) {
        return this.logger.isEnabledFor(priority);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#isInfoEnabled()
     * @return - true if info is enabled, false otherwise
     */
    public boolean isInfoEnabled() {
        return this.logger.isInfoEnabled();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Logger#isTraceEnabled()
     * @return - true if the logger is tracable, false otherwise
     */
    public boolean isTraceEnabled() {
        return this.logger.isTraceEnabled();
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#l7dlog(org.apache.log4j.Priority,
     *      java.lang.String, java.lang.Object[], java.lang.Throwable)
     * @param priority - the priority to log at
     * @param msg - the message to log
     * @param msgObj - the message object to be logged
     * @param t - the exception to log
     */
    public void l7dlog(Priority priority,
                       String msg,
                       Object[] msgObj,
                       Throwable t) {
        this.logger.l7dlog(priority, msg, msgObj, t);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#l7dlog(org.apache.log4j.Priority,
     *      java.lang.String, java.lang.Throwable)
     * @param priority - the priority to log at
     * @param message - the message to log
     * @param t - the exception to log
     */
    public void l7dlog(Priority priority, String message, Throwable t) {
        this.logger.l7dlog(priority, message, t);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#log(org.apache.log4j.Priority,
     *      java.lang.Object, java.lang.Throwable)
     * @param priority - the priority to log at
     * @param msgObj - the message object to log
     * @param t - the exception to log
     */
    public void log(Priority priority, Object msgObj, Throwable t) {
        this.logger.log(priority, msgObj, t);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#log(org.apache.log4j.Priority,
     *      java.lang.Object)
     * @param priority - the priority to log at
     * @param msgObj - the message object to log
     */
    public void log(Priority priority, Object msgObj) {
        this.logger.log(priority, msgObj);
    }

    /**
     * Delegate.
     * @see org.apache.log4j.Category#log(java.lang.String,
     *      org.apache.log4j.Priority, java.lang.Object, java.lang.Throwable)
     * @param message - the message to log
     * @param priority - the priority to log at
     * @param obj - the message object to log
     * @param t - the exception to log
     */
    public void log(String message, Priority priority, Object obj, Throwable t) {
        this.logger.log(message, priority, obj, t);
    }

    /**
     * Removes all appenders from the logger.
     * @see org.apache.log4j.Category#removeAllAppenders()
     */
    public void removeAllAppenders() {
        this.logger.removeAllAppenders();
    }

    /**
     * Removes the appender for the logger.
     * @param appender - the appender to be removed
     * @see org.apache.log4j.Category#removeAppender(org.apache.log4j.Appender)
     */
    public void removeAppender(Appender appender) {
        this.logger.removeAppender(appender);
    }

    /**
     * Removes the appender from the logger.
     * @param appender - the name of the appender to be removed
     * @see org.apache.log4j.Category#removeAppender(java.lang.String)
     */
    public void removeAppender(String appender) {
        this.logger.removeAppender(appender);
    }

    /**
     * Sets the additivity of the logger.
     * @param additivity - the additivity to be set
     * @see org.apache.log4j.Category#setAdditivity(boolean)
     */
    public void setAdditivity(boolean additivity) {
        this.logger.setAdditivity(additivity);
    }

    /**
     * Sets the logging level.
     * @param level - the severity level of the logging
     * @see org.apache.log4j.Category#setLevel(org.apache.log4j.Level)
     */
    public void setLevel(Level level) {
        this.logger.setLevel(level);
    }

    /**
     * Sets the ResourceBundle.
     * @param resource - the ResourceBundle to set
     * @see org.apache.log4j.Category#setResourceBundle(
     *      java.util.ResourceBundle)
     */
    public void setResourceBundle(ResourceBundle resource) {
        this.logger.setResourceBundle(resource);
    }

    /**
     * Returns the logger as a string.
     * @return - string representation of the object
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.logger.toString();
    }

    /**
     * Convert and error code to a string after checking if it is null or not.
     *
     * @param errorCode - error code to convert
     * @return - return string of errorCode or unspecified if null
     */
    private String errorCodeString(ErrorCode errorCode) {
        String errorCodeString = "unspecified";
        if (errorCode != null) {
            errorCodeString = errorCode.toString();
        }
        return errorCodeString;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 