/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.exceptions.VocollectException;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperPrint;
import static net.sf.jasperreports.engine.query.JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION;

/**
 * Interface for report wrapper implementations.
 *
 * @author mraj
 */
public interface ReportWrapperRoot {

    // Rendering property value constants
    public static final String SITE_LOCALIZATION_KEY = "report.site";

    public static final String RENDER_FORMAT_DESCRIPTION = "report.renderformat.description";

    public static final String REPORT_DEFAULT_RENDER_FORMAT = "report.DefaultRenderFormat";

    public static final String REPORT_PATH_BASE = "/WEB-INF/classes/reports/voicelink";

    /**
     *
     * @author mraj
     */
    public enum RenderFormat {
        HTML, PDF, RTF, XLS
    }

    /*
     * Jasper report specific constants. Need to be here as implementation
     * dependent classes also need these constants, which cannot be accessed
     * directly from them. For other report implementations add similar or more
     * constants
     */
    public static final String JASPER_HIBERNATE_SESSION_PARAM = PARAMETER_HIBERNATE_SESSION;

    public static final String JASPER_REPORT_LOCALE = JRParameter.REPORT_LOCALE;
    
    public static final String JASPER_REPORT_TIMEZONE = JRParameter.REPORT_TIME_ZONE;

    public static final String JASPER_REPORT_SOURCE_EXTENSION = ".jrxml";

    /**
     * This method verifies the report existence by attempting to load it using
     * the report path initialized in constructor. If successful, it will
     * initialize the class reportDesign member
     * @return boolean <code>true</code> if report was found<br>
     *         <code>false</code> if report with the specified name wasn't found
     *         at the location
     * @throws VocollectException if the report could not be loaded
     */
    public boolean reportExists() throws VocollectException;

    /**
     * This method verifies the report existence by attempting to load it. If
     * successful, it will initialize the class reportDesign member
     *
     * @param pathName File system path of the report files + report name,
     *            including the extension
     * @return boolean <code>true</code> if report was found<br>
     *         <code>false</code> if report with the specified name wasn't found
     *         at the location
     * @throws VocollectException if the report could not be loaded
     */
    public boolean reportExists(String pathName) throws VocollectException;

    /**
     * Method to check the existence of a report file at <code>path</code>
     * having <code>name</code>
     * @param path File system path of the report files
     * @param name report name, including the extension
     * @return boolean <code>true</code> if report was found<br>
     *         <code>false</code> if report with the specified name wasn't found
     *         at the location
     * @throws VocollectException if the report could not be loaded
     */
    public boolean reportExists(String path, String name)
        throws VocollectException;

    /**
     * Method to get all the parameters defined in the report definition adapted
     * to <code>ReportParametersDTO</code>
     * @return List of ReportParametersDTO objects, obtained from report
     *         definition
     * @throws VocollectException if the report could not be loaded or does not
     *             exist
     */
    public List<ReportParametersDTO> getParameters() throws VocollectException;

    /**
     * method exposed to request report generation. Overloaded method for client
     * which have all the details. None of the paramters will be defaulted. This
     * method compiles the jasper report definition. Populates compiled report
     * with the values map passed and custom datasource implementation class, if
     * provided. Otherwise uses hibernate session to get data. Generated report
     * is serialized into byte stream and returned.
     *
     * @param values Parameter name-value map.key=Param name<br>
     *            value=parameter object
     * @param renderFormat Format in which the report is to be presented to the
     *            user
     * @param language Locale language in which report is to be generated
     * @param dataSource Either a hibernate session or custom implementation of
     *            JRDatasource. If dataSource = null, assumed to be hibernate
     *            session
     * @return Byte stream of the generated report
     * @throws VocollectException <b>ReportError.JASPER_COMPILE_ERROR</b> if
     *             report could not be compiled<br>
     *             <b>ReportError.JASPER_FILL_ERROR</b> if report was compiled
     *             but could not be generated using given datasource and values
     * @throws UnsupportedEncodingException
     */
    public InputStream generateReport(Map<String, Object> values,
                                      String renderFormat,
                                      String language,
                                      Object dataSource)
        throws VocollectException;
    
    /**
     * method exposed to request report generation. Overloaded method for client
     * which have all the details. None of the paramters will be defaulted. This
     * method compiles the jasper report definition. Populates compiled report
     * with the values map passed and custom datasource implementation class, if
     * provided. Otherwise uses hibernate session to get data. Generated report
     * is serialized into jasperPrint and returned.
     *
     * @param values Parameter name-value map.key=Param name<br>
     *            value=parameter object
     * @param renderFormat Format in which the report is to be presented to the
     *            user
     * @param language Locale language in which report is to be generated
     * @param dataSource Either a hibernate session or custom implementation of
     *            JRDatasource. If dataSource = null, assumed to be hibernate
     *            session
     * @return jasperPrint of the generated report
     * @throws VocollectException <b>ReportError.JASPER_COMPILE_ERROR</b> if
     *             report could not be compiled<br>
     *             <b>ReportError.JASPER_FILL_ERROR</b> if report was compiled
     *             but could not be generated using given datasource and values
     * @throws UnsupportedEncodingException
     */
    public JasperPrint generateReportForTask(Map<String, Object> values,
                                      String renderFormat,
                                      String language,
                                      Object dataSource)
        throws VocollectException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 