/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.eap.CertificateSocketFactory;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

/**
 * 
 *
 * @author 
 */
public class TestLDAPConnectionThread implements Runnable {

    private Hashtable<String, String> environment = null;
    private DirContext ctx = null;
    private NamingException namingException = null;
    private Throwable throwable = null;
    
    /**
     * Constructor.
     * @param env .
     */
    public TestLDAPConnectionThread(Hashtable<String, String> env) {
        environment = env;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Runnable#run()
     */
    public void run() {
        try {
            ctx = new InitialDirContext(environment);
            CertificateSocketFactory.release();
        } catch (NamingException e) {
            CertificateSocketFactory.release();
            namingException = e;           
        } catch (Throwable e) {
            CertificateSocketFactory.release();
            throwable = e;
        }
    }

    /**
     * @return ctx
     */
    public DirContext getCtx() {
        return ctx;
    }

    /**
     * @return namingException
     */
    public NamingException getNamingException() {
        return namingException;
    }

    /**
     * @return throwable
     */
    public Throwable getThrowable() {
        return throwable;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 