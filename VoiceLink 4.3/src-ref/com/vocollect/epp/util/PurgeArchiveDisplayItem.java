/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.util.List;


/**
 * 
 *
 * @author treed
 */
public class PurgeArchiveDisplayItem {

    private String name;
    
    private String purgeValue = null;
    
    private String archiveValue = null;
    
    private List<String> purgeErrorMessages = null;
    
    private List<String> archiveErrorMessages = null;

    
    /**
     * Getter for the archiveValue property.
     * @return String value of the property
     */
    public String getArchiveValue() {
        return this.archiveValue;
    }

    
    /**
     * Setter for the archiveValue property.
     * @param archiveValue the new archiveValue value
     */
    public void setArchiveValue(String archiveValue) {
        this.archiveValue = archiveValue.trim();
    }

    
    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * Getter for the purgeValue property.
     * @return String value of the property
     */
    public String getPurgeValue() {
        return this.purgeValue;
    }

    
    /**
     * Setter for the purgeValue property.
     * @param purgeValue the new purgeValue value
     */
    public void setPurgeValue(String purgeValue) {
        if (purgeValue != null) {
            this.purgeValue = purgeValue.trim();
        } else {
            this.purgeValue = purgeValue;
        }
    }


    
    /**
     * Getter for the errorMessages property.
     * @return List value of the property
     */
    public List<String> getPurgeErrorMessages() {
        return this.purgeErrorMessages;
    }


    
    /**
     * Setter for the errorMessages property.
     * @param errorMessages the new errorMessages value
     */
    public void setPurgeErrorMessages(List<String> errorMessages) {
        this.purgeErrorMessages = errorMessages;
    }


    
    /**
     * Getter for the archiveErrorMessages property.
     * @return List value of the property
     */
    public List<String> getArchiveErrorMessages() {
        return this.archiveErrorMessages;
    }


    
    /**
     * Setter for the archiveErrorMessages property.
     * @param archiveErrorMessages the new archiveErrorMessages value
     */
    public void setArchiveErrorMessages(List<String> archiveErrorMessages) {
        this.archiveErrorMessages = archiveErrorMessages;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 