/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.io.Serializable;


/**
 * A simple JavaBean to represent label-object pairs.
 * @author Dennis Dooubleday
 */
public class LabelObjectPair implements Serializable, Comparable<LabelObjectPair> {
    
    private static final long serialVersionUID = -309400468327140368L;


    /**
     * The property which supplies the option label visible to the end user.
     */
    private String label;


    /**
     * The property which supplies the value returned to the server.
     */
    private Object value;

    /**
     * Construct an instance with the supplied property values.
     *
     * @param label The label to be displayed to the user.
     * @param value The value to be returned to the server.
     */
    public LabelObjectPair(String label, Object value) {
        this.label = label;
        this.value = value;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        LabelObjectPair other = (LabelObjectPair) o;
        String thisLabel = this.label;
        String otherLabel = other.getLabel();
        Object thisValue = this.value;
        Object otherValue = other.getValue();
        if (otherLabel == null) {
            if (thisLabel != null) {
                return false;
            }
        } else if (thisLabel == null) {
            return false;
        } else if (!thisLabel.equals(otherLabel)) {
            return false;
        }
        if (!thisValue.equals(otherValue)) {
            return false;
        }
        return true;
    }

    /**
     * @return the label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * @return the value.
     */
    public Object getValue() {
        return this.value;
    }
    

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        String rep = this.toString();
        return rep.hashCode();
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param value the value to set
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * @return a string representation of this object.
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("LabelObjectPair[");
        sb.append(this.label);
        sb.append(", ");
        sb.append(this.value.toString());
        sb.append(']');
        return (sb.toString());
    }

    /**
     * Compares LabelValuePairs based on their label attribute.
     * @param pairToCompare the pair we're comparing our label to.
     * @return an integer determining how the current LabelValuePair compares to 
     * the passed in LabelValuePair.  
     */
    public int compareTo(LabelObjectPair pairToCompare) {
        if (getLabel() != null) {
            return getLabel().compareToIgnoreCase(pairToCompare.getLabel());
        } else if (pairToCompare.getLabel() != null) {
            return -1;
        } else {
            return 0;
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 