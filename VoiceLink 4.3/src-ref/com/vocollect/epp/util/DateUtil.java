/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Utility methods for conversion of Dates and Times. The intent of this class
 * is to provide for consistentcy in the way we display dates in the
 * application.
 *
 * @author ddoubleday
 */
public final class DateUtil {

    public static final int HOURS_IN_A_DAY = 24;
    
    public static final int MINUTES_IN_AN_HOUR = 60;
    
    public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
    /**
     * Don't allow creating instances of a static util class.
     */
    private DateUtil() { }

    /**
     * Return the Date representation in the default LONG date and SHORT time
     * formats for the specified Locale.
     * @param theDate the date to convert
     * @param locale the Locale to use for conversion rules.
     * @return the Date/Time string representation.
     */
    public static String getLongDateShortTime(Date theDate, Locale locale) {
        DateFormat dateFormatter = DateFormat.getDateTimeInstance(
            DateFormat.LONG, DateFormat.SHORT, locale);
        return dateFormatter.format(theDate);
    }

    /**
     * Return the Date representation in the default date and time
     * formats for the specified Locale.
     * @param theDate the date to convert
     * @param dateFormat the format to use for the Date. Must be one of the
     * constants in the {@link java.text.DateFormat DateFormat} class
     * @param timeFormat the format to use for the Time. Must be one of the
     * constants in the {@link java.text.DateFormat DateFormat} class
     * @param locale the Locale to use for conversion rules.
     * @return the Date/Time string representation.
     */
    public static String getDateTime(Date   theDate,
                                     int    dateFormat,
                                     int    timeFormat,
                                     Locale locale) {
        DateFormat dateFormatter = DateFormat.getDateTimeInstance(
            dateFormat, timeFormat, locale);
        return dateFormatter.format(theDate);
    }

    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForTableComponent(Date theDate) {
        return formatTimestampForTableComponent(theDate, Locale.getDefault());
    }

    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @param theLocale the locale to use in formatting.
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForTableComponent(Date theDate, Locale theLocale) {

        if (theDate != null) {
            DateFormat dateFormatter = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.LONG,
                theLocale
            );
            return dateFormatter.format(theDate);
        } else {
            return "";
        }
    }

    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForTableComponent(Date theDate, TimeZone timeZone) {
        return formatTimestampForTableComponent(theDate, timeZone, Locale.getDefault());
    }

    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @param theLocale to use for date formatting
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForTableComponent(Date theDate,
            TimeZone timeZone, Locale theLocale) {
        if (theDate != null) {
            DateFormat dateFormatter = DateFormat.getDateTimeInstance(
                DateFormat.SHORT,
                DateFormat.LONG,
                theLocale
            );
            dateFormatter.setTimeZone(timeZone);
            return dateFormatter.format(theDate);
        } else {
            return "";
        }
    }


    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @return the Date/Time string representation.
     */
    public static String formatDateForTableComponent(Date theDate, TimeZone timeZone) {
        return formatDateForTableComponent(theDate, timeZone, Locale.getDefault());
    }

    /**
     * Return a representation of the given Date for display in
     * the table component.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @param theLocale to use for date formatting
     * @return the Date/Time string representation.
     */
    public static String formatDateForTableComponent(Date theDate,
            TimeZone timeZone, Locale theLocale) {
        if (theDate != null) {
            DateFormat dateFormatter = DateFormat.getDateInstance(
                DateFormat.SHORT,
                theLocale
            );
            dateFormatter.setTimeZone(timeZone);
            return dateFormatter.format(theDate);
        } else {
            return "";
        }
    }


    /**
     * Return the Date representation in the specified format.
     * @param theDate the date to convert
     * @param timestampFormat format
     * @return the Date and time in the give representation.
     */
    public static String getTimestamp(Date   theDate,
                                      String timestampFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(timestampFormat);
        sdf.setTimeZone(TimeZone.getDefault());
        String display = sdf.format(theDate);
        return display;
    }

    /**
     * Parces the string to date.
     * @param theDate the date to convert
     * @param timestampFormat format
     * @return the Date and time in the give representation.
     */
    public static Date getDate(String  theDate,
                                      String timestampFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(timestampFormat);
        sdf.setTimeZone(TimeZone.getDefault());

        Date date = null;
        try {
            date = sdf.parse(theDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
    
    /**
     * Parses the string to date.
     * @param theDate the date to convert
     * @param timestampFormat format
     * @param lenient - set to true/false
     * @return the Date and time in the give representation.
     */
    public static Date getDateWithLenient(String  theDate,
                                      String timestampFormat, boolean lenient) {
        SimpleDateFormat sdf = new SimpleDateFormat(timestampFormat);
        sdf.setLenient(lenient);
        Date date = null;
        try {
            date = sdf.parse(theDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }
    
    /**
     * Return a representation of the given Date for display in
     * the report.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @param theLocale to use for date formatting
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForReport(Date theDate,
          TimeZone timeZone, Locale theLocale) {
        DateFormat dateFmt = DateFormat.getDateInstance(
            DateFormat.SHORT, theLocale);
        dateFmt.setTimeZone(timeZone);
        
        DateFormat timeFmt = DateFormat.getTimeInstance(
                DateFormat.SHORT, theLocale);
        timeFmt.setTimeZone(timeZone);
        
        return dateFmt.format(theDate) + " " + timeFmt.format(theDate);
        
    }
    
    /**
     * Return a representation of the given Date for display in
     * the report.
     * @param theDate the date to convert
     * @param timeZone the timezone to use in date conversion
     * @param theLocale to use for date formatting
     * @return the Date/Time string representation.
     */
    public static String formatTimestampForReport(String theDate,
          TimeZone timeZone, Locale theLocale) {
        try {
            return formatTimestampForReport(DateFormat.getDateInstance().parse(theDate), timeZone, theLocale);
        } catch (ParseException e) {
            return theDate;
        }
        
    }
    
    /**
     * Method to convert date value across timezone.
     * @param cal Calendar instance having date to be converted
     * @param srcTz existing time to convert date date from
     * @param destTz new timezone into which date value is to be obtained
     * @return converted date
     */
    public static Date convertDateTimeZone(Calendar cal,
                                           TimeZone srcTz,
                                           TimeZone destTz) {

        long startMillis = cal.getTime().getTime();
        int offset = -1
            * (srcTz.getOffset(startMillis) - (destTz.getOffset(startMillis)));

        cal.add(Calendar.MILLISECOND, offset);

        return cal.getTime();
    }
    
    /**
     * Method to convert given date to site timezone.
     * @param date the date
     * @return date converted to server time
     */
    public static Date convertTimeToSiteTime(Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);

        Date dateServerTZ = DateUtil.convertDateTimeZone(cal,
            cal.getTimeZone(), SiteContextHolder.getSiteContext()
                .getCurrentSite().getTimeZone());
        return dateServerTZ;
    }
    
    /**
     * Method to convert given date to server timezone.
     * @param date the date
     * @return date converted to server time
     */
    public static Date convertTimeToServerTime(Date date) {
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);

        Date dateServerTZ = DateUtil
            .convertDateTimeZone(cal, SiteContextHolder.getSiteContext()
                .getCurrentSite().getTimeZone(), cal.getTimeZone());
        return dateServerTZ;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 