/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.Tag;

import java.lang.reflect.Field;
import java.util.Locale;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * This class contains static query manipulating utility methods for
 * site-specific where clauses.
 * @author Brent Nichols
 * @author Dennis Doubleday - revised and sorting stuff incorporated
 */

public final class QueryUtil {

    private static final Logger log = new Logger(QueryUtil.class);

    public static final Integer ENUM_NULL_MATCH_CODE = 1000;

    private static final String ADD_SELECT_TEXT = "select obj ";

    private static final String SELECT_TEXT = "select ";

    private static final String SELECT_DISTINCT_TEXT = "select distinct ";

    private static final String FROM_TEXT = "from";

    private static final String MODEL_TEXT = " obj";

    private static final String WHERE_TEXT = " where ";

    private static final String ORDER_BY_TEXT = " order by ";

    private static final String TRANSLATION_FIELD = "trans.translation";

    private static final String GROUP_BY_TEXT = " group by ";

    private static final String SITE_WHERE_CLAUSE = " obj.tags.id = :tagId ";

    private static final String SITE_SHARED_WHERE_CLAUSE = " obj.common.tags.id = :tagId ";

    private static final String ALL_SITES_WHERE_CLAUSE = " obj.tags.id in (:tagId) ";

    private static final String ALL_SHARED_WHERE_CLAUSE = " obj.common.tags.id in (:tagId) ";


    /**
     * Constructor is private, no instances needed.
     */
    private QueryUtil() {
        // No work to do...
    }

    /**
     * Add the site tag check to the where clause based on .
     * @param siteContext the site context
     * @param session the Hibernate session
     * @param queryString the HQL query string to modify
     * @param isSharable Says that the model is sharable
     * @return the modified query
     */
    public static Query addQuerySiteRestriction(SiteContext siteContext,
                                                Session session,
                                                String queryString,
                                                Boolean isSharable) {
        Query newQuery;


        if (siteContext.isInAllSiteMode()) {
            // we need all the data we can get
            if (!siteContext.isHasAllSiteAccess()) {
                newQuery = session.createQuery(addQueryRestriction(
                    ((isSharable) ? ALL_SHARED_WHERE_CLAUSE : ALL_SITES_WHERE_CLAUSE),
                    queryString.replace(SELECT_TEXT, SELECT_DISTINCT_TEXT)));
                newQuery.setParameterList("tagId", siteContext.getTagIdsForQuery());
            } else {
                // All site access means don't limit results.
                newQuery = session.createQuery(queryString);
            }
        } else {
            Tag tag = siteContext.getCurrentTagForQuery();
            newQuery = session.createQuery(addQueryRestriction(((isSharable)
                ? SITE_SHARED_WHERE_CLAUSE : SITE_WHERE_CLAUSE), queryString));
            newQuery.setParameter("tagId", tag.getId());
        }

        return newQuery;
    }

    /**
     * Add the filter criterion to the query.
     * @param filter the filter object containing the filter criterion
     * @param session the Hibernate session.
     * @param queryString the HQL query string to modify
     * @return the modified query
     */
    public static String addFilterRestriction(Filter filter,
                                              Session session,
                                              String queryString) {
        if (filter != null) {
            String filterHQL = filter.toHQL();
            if ((filterHQL != null) && (filterHQL.length() > 0)) {
                Query newQuery = session.createQuery(addQueryRestriction(filter
                    .toHQL(), queryString));
                filter.fillInNamedParameters(newQuery);
                return newQuery.getQueryString();
            }
            return queryString;
        }
        return queryString;
    }

    /**
     * If the query requires sorting-related transformations involving joins with
     * other tables, then modify the query to add the join information. Examples
     * of such transformations are joins with translation tables, or specification
     * of an outer join when the sort attribute refers to a column that can
     * be <code>null</code>.
     * @param viewModelClass the model class associated with the view.
     * @param sortColumn the column being sorted on
     * @param isSortAscending the column sort direction
     * @param queryString the HQL query string to modify
     * @return the (possibly) modified Query string.
     */
    public static String addSortingTransform(Class<?> viewModelClass,
                                             Column   sortColumn,
                                             boolean  isSortAscending,
                                             String   queryString) {

        if (sortColumn == null) {
            // Nothing to do.
            return queryString;
        }

        Locale currentLocale = LocaleContextHolder.getLocale();
        // Make sure a Locale is specified or these queries fail.
        if (currentLocale == null) {
            // Use the US English fallback if the browser didn't send one
            // we recognize.
            currentLocale = Locale.US;
            log.warn("Locale was not set--using US English fallback");
        }

        String hql = cleanQuery(queryString);

        switch (sortColumn.getSortType()) {
        case SystemEnumLocalized:
            try {
                hql = addLocalizedEnumSort(
                    viewModelClass, sortColumn, currentLocale, isSortAscending, hql);
            } catch (NoSuchFieldException e) {
                // Can't find the field, so can't do enum sorting
                log.warn("Could not find field " + sortColumn.getField()
                    + " in class " + viewModelClass.getName());
            }
            break;
        case SystemDataLocalized:
            hql = addThetaJoinTable(", SystemTranslation trans", hql);
            hql = addQueryRestriction(
                "(obj." + sortColumn.getField()
                    + " = trans.key and trans.locale = '" + currentLocale
                    + "')", hql);
            break;
        case UserDataLocalized:
            try {
                hql = addUserDataLocalizedSort(
                    viewModelClass, sortColumn, currentLocale, isSortAscending, hql);
            } catch (NoSuchFieldException e) {
                // Can't find the field, so can't do enum sorting
                log.warn("Could not find field " + sortColumn.getField()
                    + " in class " + viewModelClass.getName());
            }
            break;
        case NormalLeftJoin:
        case NormalLeftJoinLevel2:
            try {
                hql = addLeftJoinSort(
                    viewModelClass, sortColumn, isSortAscending, hql);
            } catch (NoSuchFieldException e) {
                // Can't find the field, so can't do left join sorting
                log.warn("Could not find field " + sortColumn.getField()
                    + " in class " + viewModelClass.getName());
            }
            break;
        case Normal:
        case CodeOnly:
        case CodeOnlyLocalized:
        case NoSort:
            // Nothing to do in these cases.
            break;
        default:
            // Announce problem with new unrecognized sort type.
            throw new IllegalArgumentException("Unhandled column sort type "
                + sortColumn.getSortType() + " for column "
                + sortColumn.getField());
        }

        return hql;
    }

    /**
     * @param decorator specifies the sort column and sort order
     * @return an "order by" clause based upon sort column and order in the rdi,
     *         or the empty String if no sort is set.
     */
    public static String getOrderClause(QueryDecorator decorator) {
        return getOrderClause(decorator.getSortColumnObject(), decorator
            .getSortColumn(), decorator.isSortAscending());
    }

    /**
     * @param rdi specifies the sort column and sort order
     * @return an "order by" clause based upon sort column and order in the rdi,
     *         or the empty String if no sort is set.
     */
    public static String getOrderClause(ResultDataInfo rdi) {
        return getOrderClause(
            rdi.getSortColumnObject(), rdi.getSortColumn(), rdi
                .isSortAscending());
    }

    /**
     * This method will take an existing query and add the additional where
     * clause to it.
     * @param addWhereClause the where clause to add in
     * @param queryString The passed in query object
     * @return a new query with the additional where clause
     */
    public static String addQueryRestriction(String addWhereClause,
                                             String queryString) {

        if (addWhereClause == null) {
            // Nothing to do.
            return queryString;
        }

        // Turn tabs into spaces for easier parsing.
        String hql = cleanQuery(queryString);

        int whereIndex = hql.indexOf(WHERE_TEXT);
        if (whereIndex > -1) {
            hql = hql.substring(0, whereIndex + (WHERE_TEXT.length() - 1))
                + " " + addWhereClause + " and ("
                + hql.substring(whereIndex + (WHERE_TEXT.length() - 1));
            int groupByIndex = hql.indexOf(GROUP_BY_TEXT);
            int orderByIndex = hql.indexOf(ORDER_BY_TEXT);
            if (groupByIndex > -1) {
                hql = hql.substring(0, groupByIndex) + ")"
                    + (hql.substring(groupByIndex));
            } else if (orderByIndex > -1) {
                hql = hql.substring(0, orderByIndex) + ")"
                    + (hql.substring(orderByIndex));
            } else {
                hql += ")";
            }
        } else {
            // where clause doesn't exist so simply append the where clause
            int groupByIndex = hql.indexOf(GROUP_BY_TEXT);
            int orderByIndex = hql.indexOf(ORDER_BY_TEXT);
            if (groupByIndex > -1) {
                hql = hql.substring(0, groupByIndex) + WHERE_TEXT
                    + addWhereClause + (hql.substring(groupByIndex));
            } else if (orderByIndex > -1) {
                hql = hql.substring(0, orderByIndex) + WHERE_TEXT
                    + addWhereClause + (hql.substring(orderByIndex));
            } else {
                hql += WHERE_TEXT + addWhereClause;
            }
        }

        return hql;
    }

    /**
     * @param queryString the original query string
     * @return query string trimmed and cleaned of tabs and newlines.
     */
    private static String cleanQuery(String queryString) {
        // Turn tabs and newlines into spaces for easier parsing.
        String hql = queryString.replace('\t', ' ');
        hql = hql.replace('\n', ' ');
        hql = hql.trim();
        return hql;
    }

    /**
     * Get the order clause for the query. The resulting clause depends on the
     * sortType of the column, if a sortColumn object is provided. Otherwise, it
     * depends on the sortColumnName.
     * @param sortColumn the Column object representing the sort column
     * @param sortColumnName if the sortColumn is null, the sortColumnName will
     *            be used to construct an ordinary sort. If both are null, no
     *            sort is applied.
     * @param isSortAscending true for ascending sort, false for descending.
     * @return an "order by" clause based upon sort column and order in the rdi,
     *         or the empty String if no sort is set, or if the type of the sort
     *         is non-database.
     */
    private static String getOrderClause(Column sortColumn,
                                         String sortColumnName,
                                         boolean isSortAscending) {
        if ((sortColumn != null) || (sortColumnName != null)) {
            String orderClause = ORDER_BY_TEXT;

            if (sortColumn != null) {
                switch (sortColumn.getSortType()) {
                case SystemEnumLocalized:
                case SystemDataLocalized:
                    orderClause += TRANSLATION_FIELD;
                    break;
                case UserDataLocalized:
                    // Already added by sort manipulation method
                    orderClause = null;
                    break;
                case Normal:
                case NormalLeftJoin:
                case NormalLeftJoinLevel2:
                    orderClause += MODEL_TEXT + "." + sortColumn.getField();
                    break;
                case CodeOnly:
                case CodeOnlyLocalized:
                case NoSort:
                    // Database can't sort these.
                    orderClause = null;
                    break;
                default:
                    // Announce problem with new unrecognized sort type.
                    throw new IllegalArgumentException(
                        "Unhandled column sort type "
                            + sortColumn.getSortType() + " for column "
                            + sortColumn.getField());
                }
            } else {
                orderClause += MODEL_TEXT + "." + sortColumnName;
            }

            if (orderClause != null) {
                if (isSortAscending) {
                    orderClause += " asc";
                } else {
                    orderClause += " desc";
                }
                return orderClause;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Add a join table to an existing query, using the Theta-style join
     * <code>from Table1, Table2</code>. This is useful when there is no
     * foreign key relationship between the two tables. The relationship must be
     * defined in the where clause, which must be added in a separate step with
     * <code>addQueryRestriction</code>.
     * <p>
     * This is used for localized sorting, but it is generic.
     * @param joinTable the table to join against.
     * @param hql the original HQL
     * @return the HQL with a join table added to the mix. The original HQL will
     *         be returned if the query doesn't follow our naming conventions.
     */
    private static String addThetaJoinTable(String joinTable, String hql) {

        // Find the first occurrence of the MODEL_TEXT in the from clause.
        int fromIndex = hql.indexOf(" " + FROM_TEXT);
        int modelIndex = hql.indexOf(MODEL_TEXT, fromIndex);

        if (modelIndex > -1) {
            // Found the conventional name of the model that's being queried.
            // Now add the table that will be joined against.
            String newHql = hql
                .substring(0, modelIndex + (MODEL_TEXT.length()))
                + joinTable
                + hql.substring(modelIndex + (MODEL_TEXT.length()));
            return addSelectPrefix(newHql);
        } else {
            // Can't do anything to a query that doesn't use the convention.
            return hql;
        }
    }

    /**
     * Add join tables to an existing query as needed to support sorting of
     * localized system enumerated type data. Nested properties are only
     * supported to one level, e.g. the sortColumn can be "status" or
     * "operator.status", where "status" is an enum type, but it doesn't
     * work beyond that. It is quite difficult to construct queries that
     * work to the next level while still accounting for the possibility of
     * intervening null fields in the property path.
     * @param viewModelClass the model class associated with the view.
     * @param sortColumn the column being sorted on
     * @param locale the current locale.
     * @param isSortAscending the column sort direction
     * @param hql the original HQL
     * @return the HQL with a join table added to the mix. The original HQL will
     *         be returned if the query doesn't follow our naming conventions.
     * @throws NoSuchFieldException if the sort field can't be found in the
     *             viewModelClass.
     */
    private static String addLocalizedEnumSort(Class<?> viewModelClass,
                                               Column   sortColumn,
                                               Locale   locale,
                                               boolean  isSortAscending,
                                               String   hql)
        throws NoSuchFieldException {

        // Find the first occurrence of the MODEL_TEXT in the from clause.
        int fromIndex = hql.indexOf(" " + FROM_TEXT);
        int modelIndex = hql.indexOf(MODEL_TEXT, fromIndex);

        if (modelIndex > -1) {

            SortFieldInfo fieldInfo =
                getSortFieldInfo(sortColumn.getField(), viewModelClass, true);

            if (fieldInfo.nestedFieldClass != null) {

                // For queries with a nested enum property, we need to do an
                // outer join the table of the base property, in case it has
                // nulls, then do a case statement that matches against a fake
                // translation code in the event that the base property is null.
                // The end query looks something like:
                // select obj from MODEL obj
                //     left outer join obj.BASEPROPERTY, SystemTranslation trans
                // where
                // ((case when obj.BASEPROPERTY is null then NULLMATCHKEY
                //        else obj.sortColumnName end) = trans.code
                // and
                //   trans.key = ENUM_TYPE_NAME
                // and
                //   trans.locale = 'en_US'))
                // order by trans.translation asc

                String newHql = addThetaJoinTable(" left outer join obj."
                    + fieldInfo.fieldName + ", SystemTranslation trans", hql);
                return addQueryRestriction("((case when obj." + sortColumn.getField()
                    + " is null then " + ENUM_NULL_MATCH_CODE
                    + " else obj." + sortColumn.getField() + " end)"
                    + " = trans.code and trans.key = '"
                    + fieldInfo.nestedFieldClass.getName()
                    + "' and trans.locale = '" + locale + "')", newHql);
            } else {
                // For queries where the enum property isn't nested, just
                // do an inner join on the SystemTranslation table. The
                // query looks something like:
                // select obj from MODEL obj, SystemTranslation trans
                // where
                //    obj.sortColumnName = trans.code
                // and
                //   trans.key = ENUM_TYPE_NAME
                // and
                //   trans.locale = 'en_US'
                // order by trans.translation asc

                String newHql = addThetaJoinTable(", SystemTranslation trans", hql);
                return addQueryRestriction("((case when obj." + sortColumn.getField()
                    + " is null then " + ENUM_NULL_MATCH_CODE
                    + " else obj." + sortColumn.getField() + " end)"
                    + " = trans.code and trans.key = '"
                    + fieldInfo.fieldClass.getName()
                    + "' and trans.locale = '" + locale + "')", newHql);
            }
        } else {
            // Can't do anything to a query that doesn't use the convention.
            log.warn("Cannot modify query using non-standard convention");

            return hql;
        }
    }

    /**
     * Add join tables to an existing query as needed to support sorting of
     * localized user data. The relationship between the tables must be defined
     * in the where clause, which must be added in a separate step with
     * <code>addQueryRestriction</code>.
     * @param viewModelClass the model class associated with the view.
     * @param sortColumn the column being sorted on
     * @param locale the current locale.
     * @param isSortAscending the column sort direction
     * @param hql the original HQL
     * @return the HQL with a join table added to the mix. The original HQL will
     *         be returned if the query doesn't follow our naming conventions.
     * @throws NoSuchFieldException if the sort field can't be found in the
     *             viewModelClass.
     */
    private static String addUserDataLocalizedSort(Class<?> viewModelClass,
                                                   Column   sortColumn,
                                                   Locale   locale,
                                                   boolean  isSortAscending,
                                                   String   hql)
        throws NoSuchFieldException {

        // Find the first occurrence of the MODEL_TEXT in the from clause.
        int fromIndex = hql.indexOf(" " + FROM_TEXT);
        int modelIndex = hql.indexOf(MODEL_TEXT, fromIndex);

        // The Query looks something like this in the end for non-nested one
        // select obj
        // from <Class> obj, <Class>Translation trans
        // left outer join trans.<field>Trans dt
        // with dt.locale='fr_FR'
        // where obj = trans
        // order by (case when dt.translation is null then obj.<field> else
        // dt.translation end)

        // For a nested property, the query looks something like this:
        // select obj
        // from Class obj join obj.<lowerRelClass> objrel, <RelClass>Translation trans
        // left outer join trans.<relfield>Trans dt
        // with dt.locale = 'en_US'
        // where objrel = trans
        // order by (case when dt.translation is null then objrel.<relfield>
        // else dt.translation end)

        if (modelIndex > -1) {

            SortFieldInfo fieldInfo =
                getSortFieldInfo(sortColumn.getField(), viewModelClass, true);

            // Found the conventional name of the model that's being queried.
            // Now add the table that will be joined against.
            String newHql = hql
                .substring(0, modelIndex + (MODEL_TEXT.length()));
            if (fieldInfo.nestedFieldName == null) {
                newHql += ", " + viewModelClass.getSimpleName();
            } else {
                newHql += " join obj." + fieldInfo.fieldName + " objprop, "
                    + fieldInfo.fieldClass.getSimpleName();
            }
            newHql += "Translation trans left outer join trans.";
            if (fieldInfo.nestedFieldName == null) {
                newHql += fieldInfo.fieldName;
            } else {
                newHql += fieldInfo.nestedFieldName;
            }
            newHql += "Trans dt with dt.locale = '" + locale + "'";

            // Append the rest of the original query.
            newHql += hql.substring(modelIndex + (MODEL_TEXT.length()));

            // Now add the where clause
            if (fieldInfo.nestedFieldName == null) {
                newHql = addQueryRestriction("obj = trans", newHql);
            } else {
                newHql = addQueryRestriction("objprop = trans", newHql);
            }

            // Add the order clause. This is done here in a special case
            // because we won't have the information to construct the
            // order clause properly later.
            newHql += " order by (case when dt.translation is null then obj";
            if (fieldInfo.nestedFieldName == null) {
                newHql += "." + fieldInfo.fieldName;
            } else {
                newHql += "prop." + fieldInfo.nestedFieldName;
            }
            newHql += " else dt.translation end)";
            if (isSortAscending) {
                newHql += " asc";
            } else {
                newHql += " desc";
            }

            return addSelectPrefix(newHql);

        } else {
            // Can't do anything to a query that doesn't use the convention.
            log.warn("Cannot modify query using non-standard convention");

            return hql;
        }
    }

    /**
     * Add join tables to an existing query as needed to support sorting on
     * a related field where the field value can be null. This will override
     * Hibernate's normal inner join behavior on an implicit join expressed
     * via HQL property path.
     * <p>
     * At least for now, this is only supported at the first level of the
     * property path. The rest of the property path is assumed to refer to
     * non-nullable fields.
     * @param viewModelClass the model class associated with the view.
     * @param sortColumn the column being sorted on
     * @param isSortAscending the column sort direction
     * @param hql the original HQL
     * @return the HQL with a join table added to the mix. The original HQL will
     *         be returned if the query doesn't follow our naming conventions.
     * @throws NoSuchFieldException if the sort field can't be found in the
     *             viewModelClass.
     */
    private static String addLeftJoinSort(Class<?> viewModelClass,
                                          Column   sortColumn,
                                          boolean  isSortAscending,
                                          String   hql)
        throws NoSuchFieldException {

        // Find the first occurrence of the MODEL_TEXT in the from clause.
        int fromIndex = hql.indexOf(" " + FROM_TEXT);
        int modelIndex = hql.indexOf(MODEL_TEXT, fromIndex);

        // If the original query looks like
        //      from <Class> obj
        // then we will end up with either:
        //
        // For NormalLeftJoin type
        //
        // select obj
        // from <Class> obj left join obj.<fieldName>
        //
        // For NormalLeftJoinLevel2 type
        //
        // select obj
        // from <Class> obj left join obj.<fieldName>.<nestedFieldName>

        if (modelIndex > -1) {

            SortFieldInfo fieldInfo =
                getSortFieldInfo(sortColumn.getField(), viewModelClass, false);

            // Found the conventional name of the model that's being queried.
            // Now add the table that will be joined against.
            String newHql = hql
                .substring(0, modelIndex + (MODEL_TEXT.length()));

            switch (sortColumn.getSortType()) {
            case NormalLeftJoin:
                // Make the outer join at the first level.
                newHql += " left join obj." + fieldInfo.fieldName;
                break;
            case NormalLeftJoinLevel2:
                // Make the outer join at the second level.
                newHql += " left join obj." + fieldInfo.fieldName
                    + "." + fieldInfo.nestedFieldName;
                break;
            default:
                // Announce problem with new unrecognized sort type.
                throw new IllegalArgumentException(
                    "Unhandled column sort type "
                        + sortColumn.getSortType() + " for column "
                        + sortColumn.getField());
            }

            // Add remainder of original query.
            newHql += hql.substring(modelIndex + (MODEL_TEXT.length()));

            return addSelectPrefix(newHql);

        } else {
            // Can't do anything to a query that doesn't use the convention.
            log.warn("Cannot modify query using non-standard convention");

            return hql;
        }
    }

    /**
     * Break up the specified sort field name into component name and class
     * information.
     * @param sortFieldName the sort field
     * @param viewModelClass the model class that the sort field is relative to.
     * @param checkDepth if true, throw an
     * <code>UnsupportedOperationException</code> if the nest depth is greater
     * than 2.
     * @return the component info.
     * @throws NoSuchFieldException if field information could not be found.
     */
    private static SortFieldInfo getSortFieldInfo(String   sortFieldName,
                                                  Class<?> viewModelClass,
                                                  boolean  checkDepth)
        throws NoSuchFieldException {

        SortFieldInfo info = new SortFieldInfo();

        String[] colNameParts = sortFieldName.trim().split("\\.");

        // Get the field name and class for a simple field.
        info.fieldName = colNameParts[0];
        info.fieldClass = getClassForSortField(viewModelClass, info.fieldName);

        // If it is a nested property, get the the field name and class
        // for the nested field.
        if (colNameParts.length > 1) {
            info.nestedFieldName = colNameParts[1];
            info.nestedFieldClass =
                getClassForSortField(info.fieldClass, info.nestedFieldName);
        }

        if (checkDepth && (colNameParts.length > 2)) {
            throw new UnsupportedOperationException(
                "Sorting operation not supported beyond 2 nesting levels");
        }

        return info;
    }

    /**
     * Return the Class of the field that is identified as the sort column.
     * @param viewModelClass the model class associated with the View.
     * @param sortField the field being sorted on
     * @return the Class associated with the column.
     * @throws NoSuchFieldException if no field (and hence no class) can be
     *             found.
     */
    private static Class<?> getClassForSortField(Class<?> viewModelClass,
                                                 String sortField)
        throws NoSuchFieldException {

        Class<?> klass = viewModelClass;
        Field field = null;

        while (field == null) {
            try {
                field = klass.getDeclaredField(sortField);
            } catch (NoSuchFieldException e) {
                klass = klass.getSuperclass();
                if (klass == null) {
                    // Not found, at top of hierarchy.
                    throw e;
                }
            }
        }

        return field.getType();
    }

    /**
     * If the original query started without a FROM clause, add a SELECT clause,
     * since dynamically added joins will now make it required.
     * @param queryString the original query
     * @return the original string with SELECT clause (possibly) added
     */
    private static String addSelectPrefix(String queryString) {
        // Since a new table is in the from clause, a select on the original
        // obj has to be added if it wasn't there already.
        if (queryString.startsWith(FROM_TEXT)) {
            return ADD_SELECT_TEXT + queryString;
        } else {
            return queryString;
        }
    }

    /**
     * Struct representing the components of the sort field (up to one nesting
     * level).
     */
    private static class SortFieldInfo {
        // CHECKSTYLE:OFF This is a private struct, no need for getters/setters
        String    fieldName;
        String    nestedFieldName;
        Class<?>  fieldClass;
        Class<?>  nestedFieldClass;
        // CHECKSTYLE:ON
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 