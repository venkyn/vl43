/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.SystemPropertyManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * This value object class defines properties that describe data imported from
 * or exported to a file.
 *
 * @author J Kercher
 */
public class FileReferenceUtil implements Serializable {

    // serialization identifier
    private static final long serialVersionUID = 2145830765129798533L;

    public static final String FILE_BASE_DIR = "FILE_BASE_DIR";

    private static String fileBaseDir = null; // FindBugs: protecting mutable
                                              // static string

    /*
     * The following properties describe a generic file.
     */

    // File name
    private String name;

    // File size
    private long size;

    // File md5 checksum
    private String md5;

    private String zipFileName;

    /**
     * @return the zip file name
     */
    public String getZipFileName() {
        return zipFileName;
    }

    /**
     * @param zipFileName the new zip file name
     */
    public void setZipFileName(String zipFileName) {
        this.zipFileName = zipFileName;
    }

    /**
     * Return the name of this task component.
     * @return String representing the task component name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name for this task component.
     * @param name String specifies the task component name.
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Return the size of this file's data.
     * @return int representing the size of this file's data.
     */
    public long getSize() {
        return size;
    }

    /**
     * Set the size of this file's data. Note that if data exists, then setting
     * this property will throw a runtime exception.
     * @param size int representing the size of this file's data.
     */
    public void setSize(long size) {
        this.size = size;
    }

    /**
     * Return the md5 checksum of this file's data.
     * @return String md5 checksum of this file's data.
     */
    public String getMD5() {
        return this.md5;
    }

    /**
     * Set the md5 of this file's data. Note that if data exists, then setting
     * this property will throw a runtime exception.
     * @param newMd5 String the md5 of this file's data.
     */
    public void setMD5(String newMd5) {
        this.md5 = newMd5;
    }

    /**
     * This method is needed to extend ValueObject, however, no validation is
     * required. Subclasses may override this method to validate name and
     * contents as needed.
     * @return Map contains error message keys if validation fails, otherwise
     *         empty
     */
    protected Map<String, String[]> validateProperties() {
        final HashMap<String, String[]> errors = new HashMap<String, String[]>();

        // No need for a separate validator class at this time.
        if (name != null) {
            if ((name == null) || (name.length() == 0)) {
                errors.put(
                    "name",
                    new String[] { "fileProperties.error.name.required" });
            }
        }

        return errors;
    }

    /**
     * @return the contents of the zip file as a byte array.
     */
    private byte[] getContents() {
        try {
            final FileInputStream fis = new FileInputStream(getFileBaseDir()
                + File.separator + zipFileName);
            final ZipInputStream zis = new ZipInputStream(fis);
            FileUtil fileVO = null;
            try {
                fileVO = Zipper.unzip(zis, name);
                fis.close();
                zis.close();
                return fileVO.contents;
            } catch (final IOException ioe) {
            } catch (final VocollectException ve) {
            } finally {
                try {
                    fis.close();
                    zis.close();
                } catch (final IOException ioe) {
                }
            }
            return null;
        } catch (final FileNotFoundException fnfe) {
        }
        return null;
    }

    /**
     * @return an input stream on the contents of the zip file.
     */
    public InputStream getInputStream() {
        try {
            final FileInputStream fis = new FileInputStream(getFileBaseDir()
                + File.separator + zipFileName);
            final ZipInputStream zis = new ZipInputStream(fis);
            try {
                return Zipper.positionZipStream(zis, getName());
            } catch (final IOException ioe) {
            } catch (final VocollectException ve) {
            }
            return null;
        } catch (final FileNotFoundException fnfe) {
        }
        return null;
    }

    /**
     * @return the base direcotry for files.
     */
    public String getFileBaseDir() {

        if (fileBaseDir == null) {
            try {
                final SystemPropertyManager spm = (SystemPropertyManager)
                    ApplicationContextHolder
                    .getApplicationContext().getBean("systemPropertyManager");
                final SystemProperty fileBaseDirectory = spm
                    .findByName(FILE_BASE_DIR);
                fileBaseDir = fileBaseDirectory.getValue();
            } catch (final DataAccessException e) {
                // if we can't get the value we'll just return null
            }
        }

        return fileBaseDir;
    }

    /**
     * Method to clear the fileBaseDir (where files go) primarily for testing.
     * As it sits now, you cannot change the file base dir once it has been set.
     * This will cause some unit tests to fail, depending on the order in which they are run.
     * Marked as deprecated, since this will be used in testing only for now. We can undeprecate it if
     * we find a real world use for it.
     */
    @Deprecated
    public void clearFileBaseDir() {
    	fileBaseDir = null;
    }

    /**
     * Return the contents as a String.
     * @return String representing this task component's contents
     */
    public String getContentsAsString() {
        try {
            // I18NM: Setting UTF-8 for the byte array
            return new String(getContents(), I18NData.getInstance()
                .getDefEncoding());
        } catch (final UnsupportedEncodingException e) {
            // If errors out, use an encoding associated to the system locale
            return new String(getContents());
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (null == o) { // SDT: added null check
            return false;
        } else if (o.getClass() == this.getClass()) { // SDT: symmetrical type
                                                        // test
            final FileReferenceUtil compareTo = (FileReferenceUtil) o;
            return (new EqualsBuilder().append(name, compareTo.name))
                .isEquals();
        } else {
            return false;
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        // FindBugs: This code recommended by FindBugs due to override of equals() method
        assert false : "hashCode not designed";
        return 0; // any arbitrary constant will do
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 