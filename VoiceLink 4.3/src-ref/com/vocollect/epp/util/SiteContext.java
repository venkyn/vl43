/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.service.UserManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * The context information for multi-site processing.
 *
 * Notes:
 *
 * AllSiteAccess is a flag which governs what data you can see. If getAllSiteAccess()==true, then
 * your queries are not filtered by site. Otherwise, your queries are restricted by what is contained in the
 * Set<Tag> tags member (notably the site dropdown). The set of tags define the sites you have access to. Typically,
 * the Administrator will have allSiteAccess == true.
 *
 * The (currentSite, currentTag) pair is used to restrict the data being displayed to the user to a single site at a time.
 *
 * The isInAllSiteMode() and isInSystemMode() booleans indicate what the 'current site' is for the purposes of saving
 * a new entity. The SYSTEM tag is used for Notifications, scheduling and Licensing.
 * The ALL tag is used in homepages and Notifications.
 *
 * currentTagId and currentSite association: These two pretty much change together. The only time they do not is when
 * the tag is set to SYSTEM or ALL.
 *
 * The notion of Current Site... is not established unless it gets set somewhere. This is not done by default when
 * the SiteContext is loaded.  Typically, the user's currentSite is used to establish this.
 * It's handled in the SiteInterceptor... usually. In the unit tests, for example, it's not.
 *
 * Additionally, this comes into play only if the object we're after is not shareable. If not, we have to use the
 * 'current site' in the query. If the object is shareable, then we use the list of tags the user has access to.
 *
 * @author dkertis
 * @author ddoubleday - refactored to use SiteContextHolder as ThreadLocal.
 * @author dgold - refactored to remove the notion of "tags" from the interface.
 */
public class SiteContext {

    private static Logger log = new Logger(SiteContext.class);

    // When true, causes the DAO to filter objects by site. OW, returns objs without regard to site.
    private boolean siteFilter;

    // ID of the tag associated with the current site. This is tied to the currentSite. Mostly.
    private Long currentTagId = null;

    // ID of the tag associated with the current site or,
    // when tag == ALL or SYSTEM, the tag for the site just before we switched to a siteless tag.
    private Long restoreTagId = null;

    // Current site
    private Site currentSite = null;

    // Prior site, for the restoreSite function
    private Site restoreableSite = null;


    private Set<Tag> tags = new HashSet<Tag>();


    // property to specify if the user has access to all sites. siteFilter overrides this in the generic DAO queries.
    // This is used straight up in the QueryUtil. IF allSiteAccess == true && siteFilter == true, then the query is
    // limited to those sites "contained in" the SiteContext's tags set. Tag.taggableId == site.id
    private boolean allSiteAccess;


    /**
     * Due to a NonUniqueException being thrown by hibernate when a user is
     * edited, Sites are being stored here. This way, they are non n the session
     * when the user is saved preventing two sites of the same object being in
     * the session causing the NonUniqueException.
     * This is not the collection that is manipulated by any of the code in here...
     * except for getAllSites, getSite and refreshSites.
     */
    private static List<Site> allSites;


    private SiteManager siteManager = null;

    private TagManager tagManager = null;

    private UserManager userManager = null;


    /**
     * @return the list of all existing sites.
     * @throws DataAccessException on any failure to retrieve data.
     */
    public List<Site> getAllSites() throws DataAccessException {
        if (allSites == null) {
            refreshSites();
        }
        // Data loaded through DbUnitAdapter was not updating
        // the site list.  Site refreshing was added to DBUnitAdapter.
        return allSites;
    }

    /**
     * Get a Site by its ID.
     * @param id the ID of the Site
     * @return the Site
     * @throws DataAccessException on failure to retrieve.
     */
    public synchronized Site getSite(Long id) throws DataAccessException {
        if (allSites == null) {
            refreshSites();
        }
        for (Site s : allSites) {
            if (id.equals(s.getId())) {
                return s;
            }
        }

        // If we haven't found the site we should refresh the sites because
        // the id may have come from a DB query.
        refreshSites();
        for (Site s : allSites) {
            if (id.equals(s.getId())) {
                return s;
            }
        }

        return null;
    }

    /**
     * Refresh the cached list of Sites.
     * @throws DataAccessException on failure to retrieve data.
     */
    public synchronized void refreshSites() throws DataAccessException {
        allSites = this.siteManager.getAll();
    }

    /**
     * Update the User's current Site.
     * @param u the User
     * @param userCurrentSite the ID of the Site to assign as the User's current one.
     * @throws DataAccessException on failure to retrieve or update the User.
     * @throws BusinessRuleException on failure to update the User.
     */
    public void updateUser(User u, Long userCurrentSite)
        throws DataAccessException, BusinessRuleException {
        User user = this.userManager.get(u.getId());
        user.setCurrentSite(userCurrentSite);
        this.userManager.save(user);
    }

    /**
     * Setter for the userManager property.
     * @param userManagerParam the manager.
     */
    public void setUserManager(UserManager userManagerParam) {
        this.userManager = userManagerParam;
    }

    /**
     * Setter for the siteManager property.
     * @param siteManagerParam the new siteManager value
     */
    public void setSiteManager(SiteManager siteManagerParam) {
        this.siteManager = siteManagerParam;
    }

    /**
     * Setter for the tagManager property.
     * @param tagManagerParam the new tagManager value
     */
    public void setTagManager(TagManager tagManagerParam) {
        this.tagManager = tagManagerParam;
    }

    /**
     * @return the current site.
     */
    public Site getCurrentSite() {
        return this.currentSite;
    }

    /**
     * Save current site info so we can restore the prior site to current, and set the new site.
     * @param currentSiteParam the new current site
     */
    public void setCurrentSite(Site currentSiteParam) {
        setUpSiteRestore();
        Tag currentTag = null;
        this.currentSite = currentSiteParam;
        currentTag = getTagBySiteId(currentSiteParam.getId());
        this.currentTagId = currentTag.getId();
        this.tags.add(currentTag);
    }

    /**
     * Save current site info so we can restore the prior site to current, and set the new site.
     * @param currentSiteIdParam the id of the new current site
     * @throws DataAccessException
     */
    public void setCurrentSite(Long currentSiteIdParam) throws DataAccessException {
        setUpSiteRestore();
        Tag currentTag = null;
        if (null == allSites) {
            refreshSites();
        }
        for (Site site : allSites) {
            if (0 == site.getId().compareTo(currentSiteIdParam)) {
                this.currentSite = site;
            }
        }

        currentTag = getTagBySiteId(currentSiteIdParam);
        this.currentTagId = currentTag.getId();
        this.tags.add(currentTag);

    }

    /**
     * Save the data we need in order to restore the 'current' site.
     */
    private void setUpSiteRestore() {
        this.restoreableSite  = this.currentSite;
        this.restoreTagId = this.currentTagId;
    }


    /**
     * Restore the last 'current' site.
     */
    public void restorePriorSite() {
        this.currentSite = this.restoreableSite;
        this.currentTagId = this.restoreTagId;
    }
    /**
     * Deprecated to discourage use of tags in the interface.
     * Not made private due to usage in the site interceptor.
     * Find the Tag that represents the Site with the ID.
     * @param id the site ID.
     * @return the Tag associated with the id
     */
    @Deprecated public  Tag getTagBySiteId(Long id) {
        return this.tagManager.findByTaggedObjectId(Tag.SITE, id);
    }

    /**
     *  DHG made private
     * Retrieve a Site by the associated tag.
     * @param tag the tag
     * @return the Site associated with the tag.
     * @throws DataAccessException
     * @throws DataAccessException on failure to retrieve.
     */
    private Site getSiteByTag(Tag tag) throws DataAccessException {
        return getSite(tag.getTaggableId());
    }

    /**
     * The currentTagId is used to set the tag for a new object.
     * It has nothing to do with what the user can see.
     * This is used for the Notifications, when being sent up by the SYSTEM (as in jobs),
     * or for other siteless entities (ALL).
     * If you want the queries to be unrestricted, then use the boolean allSiteAccess, which
     * governs how the queries are produced. Really, the only time you will ever see the SYSTEM and ALL
     * tagged entities is if you _have_ allSiteAccess == true.
     * @return true if the user is viewing a specific site.
     */
    public boolean isInCurrentSiteMode() {
        Long id = getCurrentTagId();
        return ((id != null) && (id != Tag.ALL) && (id != Tag.SYSTEM)) ? true : false;
    }

    /**
     * @return whether or not the user is viewing all sites.
     */
    public boolean isInAllSiteMode() {
        Long id = getCurrentTagId();
        return ((id != null) && (id == Tag.ALL)) ? true : false;
    }

    /**
     * Returns whether or not the user is viewing system wide data.
     * @return the answer.
     */
    public boolean isInSystemMode() {
        Long id = getCurrentTagId();
        return ((id != null) && (id == Tag.SYSTEM)) ? true : false;
    }

    /**
     * Return the currentSite ID set in the thread local. If the value is not an
     * actual site then null is returned.
     * DHG Made private (if possible)
     * @return Long - the current site id
     */
    private Long getCurrentTagId() {
        return this.currentTagId; // the site id the user is currently viewing
    }


//-----------------New interface members---------------------------------------

    /**
     * get the sites associated with the Taggable.
     * @param taggedObj the object we want to get the sites from
     * @return list of sites associated with the Taggable
     * @throws DataAccessException from findSIteByTag
     * @author dgold
     */
    public List<Site> getSites(Taggable taggedObj) throws DataAccessException {
        Set<Tag> objectsTags = taggedObj.getTags();
        if (null == objectsTags) {
            return null;
        }
        return findSiteByTag(objectsTags);
    }



    /**
     * Get the sites corresponding to the set of tags passed in (helper method).
     * @param tagsParam the list of tags we want to "convert" to sites.
     * @return the list of sites associated with the list of tags.
     * @throws DataAccessException from getSiteByTag
     * @author dgold
     * @throws DataAccessException
     */
    private List<Site> findSiteByTag(Set<Tag> tagsParam) throws DataAccessException {
        List<Site> sites = new LinkedList<Site>();
        for (Tag t : tagsParam) {
            sites.add(getSiteByTag(t));
        }
        return sites;
    }

    /**
     * Helper method to get the tag associated with the site.
     *
     * @param sParam the site paramater
     * @return the tag associated with the site.
     * @author dgold
     */
    private Tag getTagForSite(Site sParam) {
        return getTagBySiteId(sParam.getId());
    }

    /**
     * get the site ids associated with the Taggable.
     * @param taggedObj the object we want to get the list of site ids from.
     * @return the list of site ids associated with the tagged object.
     * @throws DataAccessException from getSites
     * @author dgold
     */
    public List<Long> getSiteIds(Taggable taggedObj) throws DataAccessException {
        List<Long> ids = new LinkedList<Long>();
        List<Site> sites = getSites(taggedObj);
        if (null == sites) {
            return null;
        }
        for (Site s : sites) {
            ids.add(s.getId());
        }
        return ids;
    }

    /**
     * Determine if the taggable is in the site passed in.
     * @param taggedObj The object we're interested in.
     * @param site The site we're interested in.
     * @return true if the object is in the site, false OW.
     * @throws DataAccessException whenever it feels like it.
     */
    public boolean isInSite(Taggable taggedObj, Site site) throws DataAccessException {
        return getSiteIds(taggedObj).contains(site.getId());
    }

    /**
     * set the sites associated with the Taggable.
     * @param taggedObj the object we want to associate with new sites.
     * @param sites the list of sites the object should be associated with.
     * @author dgold
     */
    public void setSites(Taggable taggedObj, List<Site> sites) {
        List<Tag> siteTags = new ArrayList<Tag>();
        for (Site s : sites) {
            siteTags.add(getTagForSite(s));
        }
        this.tagManager.saveTags(taggedObj, siteTags);
    }

    /**
     * set the current context's Site list to that in the Taggable's list.
     * @param taggedObj the object containing the sites.
     * @author dgold
     */
    public void setContextSites(Taggable taggedObj) {
        this.tags.clear();
        if (null == taggedObj.getTags()) {
            return;
        }

        this.tags.addAll(taggedObj.getTags());
    }

    /**
     * @param siteId .
     * @throws DataAccessException
     */
    public void setContextSite(Long siteId) throws DataAccessException {
        this.tags.clear();
        addSiteToContext(getSite(siteId));
    }

    /**
     * Set the current context's Site list to be the list of sites passed in.
     * @param sites the list of sites.
     * @author cshoff
     */
    public void setContextSites(List<Site> sites) {
        this.tags.clear();
        if (null == sites) {
            return;
        }

        for (Site site : sites) {
            this.tags.add(getTagForSite(site));
        }
    }

    /**
     * Set the single site associated with the Taggable (clear the list, add this Site) (particularly terminals).
     * @param taggedObj the object we want to work with.
     * @param site the site we want the Taggable to be associated with.
     * @author dgold
     */
    public void setSite(Taggable taggedObj, Site site) {
        setSite(taggedObj, site.getId());
    }

    /**
     * Set the single site associated with the Taggable (clear the list, add this Site) (particularly terminals).
     * @param taggedObj the object we want to work with.
     * @param siteId the id of the site we want the Taggable to be associated with.
     * @author dgold
     */
    public void setSite(Taggable taggedObj, Long siteId) {
        this.tagManager.saveTag(taggedObj, getTagBySiteId(siteId));
    }

    /**
     * Set the taggable's set of sites to the context's current site.
     * @param taggedObj .
     */
    public void setSiteToCurrentSite(Taggable taggedObj) {
        if (isInSystemMode()) {
            try {
                taggedObj.getTags().add(getTagById(Tag.SYSTEM));
            } catch (DataAccessException ex) {
                // DHG: This cannot happen if the database has been set up properly eg: with the install data.
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
            return;
        }
        if (isInAllSiteMode()) {
            try {
                taggedObj.getTags().add(getTagById(Tag.ALL));
            } catch (DataAccessException ex) {
                // DHG: This cannot happen if the database has been set up properly eg: with the install data.
                ex.printStackTrace();
                throw new RuntimeException(ex);
            }
            return;
        }
        setSite(taggedObj, this.currentSite);
    }
    /**
     * add the list of sites to the Taggable's list of sites.
     * @param taggedObj the tagged object we're working with
     * @param sites the list of sites the Taggable is supposed to be associated with
     * @author dgold
     */
    public void addSites(Taggable taggedObj, List<Site> sites) {
        for (Site s : sites) {
            addSite(taggedObj, s);
        }
    }

    /**
     * add the list of sites to the Taggable's list of sites.
     * @param taggedObj the tagged object we're working with
     * @param siteIds the list of site ids the Taggable is supposed to be associated with
     * @author dgold
     */
    public void addSitesById(Taggable taggedObj, List<Long> siteIds) {
        for (Long siteId : siteIds) {
            addSite(taggedObj, siteId);
        }
    }

    /**
     * add the site to the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param site the site the tagged object ios supposed to be associated with.
     * @author dgold
     */
    public void addSite(Taggable taggedObj, Site site) {
       addSite(taggedObj, site.getId());
    }

    /**
     * add the site to the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param siteId the id of the site the tagged object is supposed to be associated with.
     * @author dgold
     */
    public void addSite(Taggable taggedObj, Long siteId) {
        this.tagManager.addTag(taggedObj, getTagBySiteId(siteId));
    }

    /**
     * Add the context's current site to the taggable's list of sites.
     * @param taggedObj .
     */
    public void addCurrentSite(Taggable taggedObj) {
        addSite(taggedObj, this.currentSite);
    }

    /**
     * @param siteParam .
     */
    public void addSiteToContext(Site siteParam) {
        this.tags.add(getTagBySiteId(siteParam.getId()));
    }


    /**
     * remove the list of sites from the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param sites the list of sites to unassociate the object from.
     * @author dgold
     */
    public void removeSites(Taggable taggedObj, List<Site> sites) {
        for (Site site : sites) {
            removeSite(taggedObj, site);
        }
    }

    /**
     * remove the taggable object from the current site.
     * @param taggedObject the object we're interested in.
     */
    public void removeFromCurrentSite(Taggable taggedObject) {
        removeSite(taggedObject, this.currentSite);
    }

    /**
     * remove the list of sites from the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param siteIds the list of sites to unassociate the object from.
     * @author dgold
     */
    public void removeSitesById(Taggable taggedObj, List<Long> siteIds) {
        for (Long siteId : siteIds) {
            removeSite(taggedObj, siteId);
        }
    }

    /**
     * remove the site form the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param site the site to unassociate the object from.
     * @author dgold
     */
    public void removeSite(Taggable taggedObj, Site site) {
        removeSite(taggedObj, site.getId());
    }

    /**
     * remove the site form the Taggable's list.
     * @param taggedObj the tagged object we're working with
     * @param siteId the site to unassociate the object from.
     * @author dgold
     */
    public void removeSite(Taggable taggedObj, Long siteId) {
        this.tagManager.deleteTag(taggedObj, getTagBySiteId(siteId));
    }
    /**
     * Get the list of sites from the current context
     * @author dgold
     * @return .
     * @throws DataAccessException
     * @throws DataAccessException
     */
    public List<Site> getSitesInContext() throws DataAccessException {
        return findSiteByTag(this.tags);
    }




    /**
     * Remove the site from the site context. This removes the tags identifying the site from the set of tags the
     * site context will use to limit queries.
     * @param site the site to remove
     * @throws DataAccessException probably not
     */
    public void removeSiteFromContext(Site site) throws DataAccessException {
        removeSiteFromContext(site.getId());
    }

    /**
     * Remove the site from the site context. This removes the tags identifying the site from the set of tags the
     * site context will use to limit queries.
     * @param idParam id of the site we want to remove.
     * @throws DataAccessException probably not.
     */
    public void removeSiteFromContext(Long idParam) throws DataAccessException {
        getTags().remove(getTagBySiteId(idParam));
    }

    /**
     * Get the list of site ids from the context.
     * @return List of site ids the context is currently keeping.
     * @throws DataAccessException from getSiteByTag
     * @author dgold
     */
    public List<Long> getSiteIdsInContext() throws DataAccessException {
        List<Long> siteIds = new LinkedList<Long>();
        for (Tag tag : this.tags) {
            siteIds.add(getSiteByTag(tag).getId());
        }
        return siteIds;
    }

    /**
     * Get the site in the context identified by the ID.
     * Use this if you want to get a site that the context has registered. Use getSite if you just want a Site,
     * regardless of whether the SiteContext thinks you have access to it.
     * @param siteId id of the site we want
     * @return the site by id, or null if the SiteContext won't allow access to the site.
     * @throws DataAccessException
     */
    public Site getSiteInContext(Long siteId) throws DataAccessException {
        List<Long> sites = getSiteIdsInContext();
        for (Long aSiteId : sites) {
            if (aSiteId.equals(siteId)) {
                return getSite(aSiteId);
            }
        }
        return null;
    }

    /**
     * change the 'current site' for the context.
     * @param site the site we want to change to.
     * @throws DataAccessException
     * @author dgold
     */
    public void changeCurrentSite(Site site) {
        setCurrentSite(site);
    }

    /**
     * change the 'current site' for the context.
     * @param siteId the id for the site we want to change to.
     * @author dgold
     * @throws DataAccessException
     */
    public void changeCurrentSite(Long siteId) throws DataAccessException {
        setCurrentSite(siteId);
    }

    /**
     * change the 'current site' for the context.
     * @param siteName the name for the site we want to change to.
     * @throws DataAccessException from getTagBySiteId
     * @author dgold
     */
    public void changeCurrentSite(String siteName) throws DataAccessException {
        changeCurrentSite(this.siteManager.findByName(siteName));
    }

    /**
     * get the site from the Taggable, or get the first site from the Taggable.
     * @param taggedObj the object we want to get the (first) site from.
     * @throws DataAccessException from getSiteByTag
     * @return the (first) site from the object
     * @author dgold
     */
    public Site getSite(Taggable taggedObj) throws DataAccessException {
        if (null == taggedObj.getTags()) {
            return null;
        }
        Tag tag = (Tag) taggedObj.getTags().toArray()[0];
        return getSiteByTag(tag);
    }

    /**
     * This governs the site in which a new object will be saved. Or which will show in the UI based on site selection.
     * set the 'current site' to SYSTEM.
     * @author dgold
     */
    public void setToSystemMode() {
        setUpSiteRestore();
        this.currentTagId = Tag.SYSTEM;
    }

    /**
     * This governs the site in which a new object will be saved. Or which will show in the UI based on site selection.
     * set 'current site' to ALL.
     * @author dgold
     */
    public void setToAllSiteMode() {
        setUpSiteRestore();
        this.currentTagId = Tag.ALL;
    }

    /**
     * Set the 'current site' to whatever is in the tag list.
     * @author dgold
     */
    public void setToCurrentSiteMode() {
        restorePriorSite();
    }

    /**
     * get the name from the current site. Replaces some Action methods that use getCurrentTagId.
     * @return the name of the current site.
     * @author dgold
     */
    public String getCurrentSiteName() {
        if (null == this.currentSite) {
            return null;
        }
        return this.currentSite.getName();
    }

    /**
     * true if the Taggable is 'in' the current site.
     * @param taggedObj the object we want to investigate.
     * @return true if the current site is associated with the object.
     * @author dgold
     */
    public boolean isInCurrentSite(Taggable taggedObj) {
        if (null == taggedObj.getTags()) {
            return false;
        }
        return taggedObj.getTags().contains(getTagForSite(this.currentSite));
    }

    /**
     * get the count of sites in the site list.
     * @return the number of sites the context is currently holding.
     * @author dgold
     */
    public int getSiteCountInContext() {
        return this.tags.size();
    }

    /**
     * Get the count of the number of sites associated with the taggedObj.
     * @param taggedObj the subject of the query.
     * @return the number of sites associated with the tagged obj.
     */
    public int getSiteCount(Taggable taggedObj) {
        if (null == taggedObj.getTags()) {
            return 0;
        }
        return taggedObj.getTags().size();
    }
    /**
     * clear the list of sites.
     * @author dgold
     */
    public void clearAllSites() {
        this.tags.clear();
    }

    /**
     * clears the site cache list.
     * @ author mnichols
     */
    public void clearSiteCache() {
        SiteContext.allSites = null;
    }

    /**
     * Function to return the current list of tags for any query.
     * This is deprecated in order to discourage the use of tags.
     * @return .
     */
    @Deprecated
    public Tag getCurrentTagForQuery() {
        for (Tag tag : this.tags) {
            if (tag.getId().equals(this.currentTagId)) {
                return tag;
            }
        }
        try {
            return getTagById(this.currentTagId);
        } catch (DataAccessException ex) {
            // This should never occur
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Check to see if the user has access to the tagged object.
     * @param user the user we want to check
     * @param taggedObj the object the user wants to see.
     * @return true if the user has access to the object.
     */
    public boolean userHasAccess(User user, Taggable taggedObj) {
        Set<Tag> intersection = new HashSet<Tag>(user.getTags());
        intersection.retainAll(taggedObj.getTags());
        return !intersection.isEmpty();
    }
    /**
     * Check to see if the current user has access to the tagged object.
     * @param taggedObj the object the user wants to see.
     * @return true if the user has access to the object.
     */
    public boolean userHasAccess(Taggable taggedObj) {
        Set<Tag> intersection = new HashSet<Tag>(getTags());
        intersection.retainAll(taggedObj.getTags());
        return !intersection.isEmpty();
    }

    /**
     * @param user .
     * @param site .
     * @return .
     */
    public boolean userHasAccess(User user, Site site) {
        for (Tag tag : user.getTags()) {
            if (tag.getTaggableId().equals(site.getId())) {
                return true;
            }
        }
        return false;
    }
//-----------------Refactored or renamed functions-----------------------------

    /**
     * Original method was getDefaultSite, which returned a Tag.
     * This replacement returns a site...
     * @param currentUser .
     * @return .
     * @throws DataAccessException
     */
    public Site getDefaultSiteForUser(User currentUser) throws DataAccessException {
        return getSiteByTag(this.siteManager.findDefaultSiteForUser(currentUser));
    }


    /**
     * @return true if the filter-by-site flag is on. This is used to limit the results of queries to those sites
     * currently represented by the tags in the list of Tags
     */
    public boolean isFilterBySite() {
        return this.siteFilter;
    }

    /**
     * Set to true if you want to limit the results of queries to only those sites currently represented in the
     * list of tags.
     * @param siteFilterParam true to limit to sites in the SiteContext.
     */
    public void setFilterBySite(boolean siteFilterParam) {
        this.siteFilter = siteFilterParam;
    }

    /**
     * AllSiteAccess is a flag which governs what data you can see. If getAllSiteAccess()==true, then
     * your queries are not filtered by site. Otherwise, your queries are restricted by what is contained in the
     * Set<Tag> tags member (notably the site dropdown). The set of tags define the sites you have access to. Typically,
     * the Administrator will have allSiteAccess == true.
     *
     * @return true if the all site access is set to true
     */
    public boolean isHasAllSiteAccess() {
        return this.allSiteAccess;
    }

    /**
     * Set the AllSiteAccess flag (generally speaking, admin mode).
     * @param asa new value for AllSiteAccess
     */
    public void setHasAllSiteAccess(boolean asa) {
        this.allSiteAccess = asa;
    }

    /**
     * @param siteNameParam .
     * @return .
     * @throws DataAccessException
     */
    public Site getSiteByName(String siteNameParam) throws DataAccessException {
        List<Site> sites = getAllSites();
        for (Site site : sites) {
            if (site.getName().equals(siteNameParam)) {
                return site;
            }
        }
        return null;
    }

    /**
     * this function is here because of an inability to inject tagDAO into the
     * GenericDAOImpl class. Spring give an error message relating to an
     * aopProxy. To work around, a SiteContext is already being injected so I am
     * forwarding the call here. Maybe DD will have a solution.
     * @param id the tag ID.
     * @return the associated tag
     * @throws DataAccessException on any failure
     */
    private Tag getTagById(Long id) throws DataAccessException {
        return this.tagManager.get(id);
    }

    /**
     * DHG  Made private, rename to getTags. Replace with call to addSite in SiteAction.
     * Getter for the sites property.
     * @return value of the property
     */
    private Set<Tag> getTags() {
        return this.tags;
    }




//-----------------Functions to be refactored or renamed-----------------------
    // Deprecate these and make new functions that are renamed. The old interface should call the new functions...

//-----------------Functions to be deprecated----------------------------------

    /**
     * @return the Tag IDs associated with the sites the user has access to.
     */
    @Deprecated public Long[] getTagIdsForQuery() {
        // get a list of sites the user has access to
        Set<Tag> tagsLocal = getTags();
        if (tagsLocal.isEmpty()) {
            // No sites set, must be in non-GUI space.
            if (log.isDebugEnabled()) {
                log.debug("No user tag sites set");
            }
            return null;
        }
        // create a array for the tag
        Long[] siteTagIds = new Long[tagsLocal.size()];

        // build an array of tags
        int i = 0;
        for (Tag t : tagsLocal) {
            Long siteTagId = t.getId();
            siteTagIds[i++] = siteTagId;
        }
        // return the tags
        return siteTagIds;
    }

    /**
     * Note - this is used by a single function in SiteInterceptor. I'm willing to see the deprecation warning.
     * There is a lot of code in there and I am really not sure if the long we pass into this function is always a
     * TagId... yet.
     * @param currentTagIdParam the site ID.
     * @return whether or not the site is a valid one.
     */
    @Deprecated public boolean validSiteByTagId(Long currentTagIdParam) {
        List<Tag> allSiteTags = this.tagManager.findSiteTags();
        for (Tag s : allSiteTags) {
            if (s.getId().equals(currentTagIdParam)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Retrieve a Site by the associated tag ID.
     * @param tagId the ID of the tag
     * @return the Site associated with the taated publicgId.
     * @throws DataAccessException on failure to retrieve.
     */
    @Deprecated public Site getSiteByTagId(Long tagId) throws DataAccessException {
        Tag t = this.tagManager.get(tagId);
        return getSiteByTag(t);
    }

//-------------------------Delete below this line--------------------------------------------------------//

    /**
     * DHG Made this return a Site. done
     * @param currentUser the user
     * @return the Tag for the default site for the user.
     * @throws DataAccessException on failure to retrieve.
     * @see #getDefaultSiteForUser
     * Cannot remove yet, used in the site interceptor
     */
//    @Deprecated
//    public Tag getDefaultSite(User currentUser) throws DataAccessException {
//        return siteManager.findDefaultSiteForUser(currentUser);
//    }

    /**
     * DHG renamed to isFilterBySite done
     * Getter for the siteFilter property.
     * @return boolean value of the property
     * @see #isFilterBySite
     */
//    @Deprecated
//    public boolean getSiteFilter() {
//        return this.siteFilter;
//    }

    /**
     * DHG renamed to setFilterBySite done
     * Setter for the siteFilter property.
     * @param siteFilterParam the new siteFilter value
     * @see #setFilterBySite(boolean)
     */
//    @Deprecated
//    public void setFilterBySite(boolean siteFilterParam) {
//        this.siteFilter = siteFilterParam;
//    }

    /**
     * DHG Renamed to isHasAllSiteAccess done
     * Getter for the allSiteAccess property.
     * @return boolean value of the property
     * @see #isHasAllSiteAccess()
     */
//    @Deprecated
//    public boolean isHasAllSiteAccess() {
//        return this.allSiteAccess;
//    }
    /**
     *  DHG renamed to setHasAllSiteAccess done
     * Setter for the allSiteAccess property.
     * @param asa the new allSiteAccess value
     * @see #setHasAllSiteAccess(boolean)
     */
//    @Deprecated
//    public void setHasAllSiteAccess(boolean asa) {
//        this.allSiteAccess = asa;
//    }
    /**
     * Setter for the currentTagId property.
     * @param id the new currentTagId value
     */
//    @Deprecated public void setCurrentTagId(Long id) {
//        currentTagId = id;
//        try {
//            currentSite = getSiteByTagId(id);
//        } catch (DataAccessException ex) {
//            // This really should never happen. It's an exception thrown by refeshSites().
//            log.warn("Failed to find a site associated with tag (id: " + currentTagId);
//        }
//        for (Tag t : tags) {
//            if (0 == t.getId().compareTo(id)) {
//                try {
//                    currentSite = getSiteByTag(t);
//                } catch (DataAccessException ex) {
//                    // This really should never happen. It's an exception thrown by refeshSites().
//                    log.warn("Failed to find a site associated with tag (id: " + t.getId()
//                                    + " and Site id" + t.getTaggableId());
//                }
//                break;
//            }
//        }
//    }

// ----- Above this line, we left in the original function definitions with comments for reference during refactoring.
//---------This section (above) should be removed after refactoring is complete in VL
//----------(VC will be done first, but we'll leave these aids to refactoring in until after we are done with VL).

    /**
     *
     * Setter for the sites property.
     * @param tagsParam the tags representing the new sites
     */
//    @Deprecated public void setSites(Set<Tag> tagsParam) {
//        this.tags = tagsParam;
//    }

    /**
     * This function will return the current site context the user is in. It
     * returns the TAG of the site context, not the site ID.
     * @return the tag ID of the site context
     */
//    @Deprecated public Long getSiteTagId() {
//        return getCurrentTagId(); // the site id the user is currently viewing
//    }

    /**
     * Gets a tag based on the site name.
     * @param name the name of the site.
     * @return the Tag associated with the site, or null if there is no
     * such site or no such tag.
     * @throws DataAccessException on failure to retrieve Site or Tag.
     */
//    @Deprecated public Tag getTagBySiteName(String name) throws DataAccessException {
//        Site s = siteManager.findByName(name);
//        if (s != null) {
//            return this.getTagBySiteId(s.getId());
//        }
//        return null;
//    }


    /**
     * Returns all site tag ids for the current SiteContextHolder.
     * @return List of site tag ids
     * @throws DataAccessException on data access error
     */
    @Deprecated public List<Long> getAllTagsForUsers() throws DataAccessException {
        List<Long> tagIds = null;
        if (isHasAllSiteAccess()) {
            tagIds = new ArrayList<Long>();
            List<Site> summarySites = getAllSites();
            for (Site site : summarySites) {
                Tag tag = getTagBySiteId(site.getId());
                tagIds.add(tag.getId());
            }
        } else {
            tagIds = java.util.Arrays.asList(getTagIdsForQuery());
        }
        return tagIds;
    }

// ------------------------------------For testing during conversion only
    // we comment this block out, see what breaks, fix one manager or model or whatever, then uncomment this
    // and run the unit tests.

//    @Deprecated
//    public Tag getDefaultSite(User currentUser) throws DataAccessException {
//        return siteManager.findDefaultSiteForUser(currentUser);
//    }
//
//    @Deprecated
//    public boolean getSiteFilter() {
//        return this.siteFilter;
//    }
//
//    @Deprecated public void setCurrentTagId(Long id) {
//        currentTagId = id;
//        try {
//            currentSite = getSiteByTagId(id);
//        } catch (DataAccessException ex) {
//            // This really should never happen. It's an exception thrown by refeshSites().
//            log.warn("Failed to find a site associated with tag (id: " + currentTagId);
//        }
//        for (Tag t : tags) {
//            if (0 == t.getId().compareTo(id)) {
//                try {
//                    currentSite = getSiteByTag(t);
//                } catch (DataAccessException ex) {
//                    // This really should never happen. It's an exception thrown by refeshSites().
//                    log.warn("Failed to find a site associated with tag (id: " + t.getId()
//                                    + " and Site id" + t.getTaggableId());
//                }
//                break;
//            }
//        }
//    }
//
//    @Deprecated public void setSites(Set<Tag> tagsParam) {
//        this.tags = tagsParam;
//    }
//
//    @Deprecated public Long getSiteTagId() {
//        return getCurrentTagId(); // the site id the user is currently viewing
//    }
//
//    @Deprecated public Tag getTagBySiteName(String name) throws DataAccessException {
//        Site s = siteManager.findByName(name);
//        if (s != null) {
//            return this.getTagBySiteId(s.getId());
//        }
//        return null;
//    }
//
//
//    @Deprecated public List<Long> getAllTagsForUsers() throws DataAccessException {
//        List<Long> tagIds = null;
//        if (isHasAllSiteAccess()) {
//            tagIds = new ArrayList<Long>();
//            List<Site> summarySites = getAllSites();
//            for (Site site : summarySites) {
//                Tag tag = getTagBySiteId(site.getId());
//                tagIds.add(tag.getId());
//            }
//        } else {
//            tagIds = java.util.Arrays.asList(getSiteTagIds());
//        }
//        return tagIds;
//    }
//
//    public Long[] getSiteTagIds() {
//        // get a list of sites the user has access to
//        if (tags.isEmpty()) {
//            // No sites set, must be in non-GUI space.
//            if (log.isDebugEnabled()) {
//                log.debug("No user tag sites set");
//            }
//            return null;
//        }
//        // create a array for the tag
//        Long[] siteTagIds = new Long[tags.size()];
//
//        // build an array of tags
//        int i = 0;
//        for (Tag t : tags) {
//            Long siteTagId = t.getId();
//            siteTagIds[i++] = siteTagId;
//        }
//        // return the tags
//        return siteTagIds;
//    }
//

 }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 