/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.model.DataObject;

/**
 * This class defines model for notification summary.
 *
 * @author daich
 */
public class NotificationSummary implements DataObject {

    private Long siteId;

    private Long tagId;

    private Long notificationCount = new Long(0);

    private String siteName;

    private String flag;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getId()
     */
    public Long getId() {
        return this.siteId != null ? this.siteId : -1L;
    }

    /**
     * @return the siteName.
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the siteName.
     * @param siteName to set
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * Returns the siteName.
     * @return String
     */
    public String getName() {
        return siteName;
    }

    /**
     * Returns the notificationCount.
     * @return String
     */
    public Long getNotificationCount() {
        return notificationCount;
    }

    /**
     * Sets the notificationCount.
     * @param notificationCount to set
     */
    public void setNotificationCount(Long notificationCount) {
        this.notificationCount = notificationCount;
    }

    /**
     * Sets the siteId.
     * @param siteId to set
     */
    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    /**
     * Returns the siteId.
     * @return String
     */
    public Long getSiteId() {
        return siteId;
    }

    /**
     * Returns the flag to indicate if there are nounack notifications,
     * user has access to one site.
     * @return String
     */
    public String getFlag() {
        return this.flag;
    }

    /**
     * Sets the flag to indicate if there are nounack notifications,
     * user has access to one site.
     * @param flag to set
     */
    public void setFlag(String flag) {
        this.flag = flag;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return null;
    }

    /**
     * TODO: This is not implemented properly. -- ddoubleday
     * @return the message string
     */
    public String getMessage() {
        return getFlag();
//        if (getFlag().equals("NoUnacknowledged")) {
//            return new UserMessage("notification.summary.noUnacknowledged");
//        } else if (getFlag().equals("AccessToOneSite")) {
//            return new UserMessage("notification.summary.text", getNotificationCount());
//        } else {
//            return new UserMessage("notification.summary.site.text", getSiteName(), getNotificationCount());
//        }
    }

    /**
     * Getter for the tagId property.
     * @return Long value of the property
     */
    public Long getTagId() {
        return this.tagId;
    }


    /**
     * Setter for the tagId property.
     * @param tagId the new tagId value
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 