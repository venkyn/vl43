/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.ui.ToolTipObject;

import java.util.List;

/**
 *
 *
 * @author dkertis
 */
public class ResultData {

    private List<DataObject> data = null;

    private List<DataObject> selectedData = null;

    private List<ToolTipObject> sliderList = null;

    private long totalRows = 0;

    private long numPreviousRows = 0;

    /**
     * Getter for the data property.
     * @return value of the property
     */
    public List<DataObject> getData() {
        return this.data;
    }

    /**
     * Setter for the data property.
     * @param data the new data value
     */
    public void setData(List<DataObject> data) {
        this.data = data;
    }

    /**
     * Getter for the numPreviousRows property.
     * @return long value of the property
     */
    public long getNumPreviousRows() {
        return this.numPreviousRows;
    }

    /**
     * Setter for the numPreviousRows property.
     * @param numPreviousRows the new numPreviousRows value
     */
    public void setNumPreviousRows(long numPreviousRows) {
        this.numPreviousRows = numPreviousRows;
    }

    /**
     * Getter for the sliderList property.
     * @return value of the property
     */
    public List<ToolTipObject> getSliderList() {
        return this.sliderList;
    }

    /**
     * List of ToolTip objects that will show up in the slider.
     * @param sliderList the new sliderList value
     */
    public void setSliderList(List<ToolTipObject> sliderList) {
        this.sliderList = sliderList;
    }

    /**
     * The total number of rows in the database.
     * @return long value of the property
     */
    public long getTotalRows() {
        return this.totalRows;
    }

    /**
     * Setter for the totalRows property.
     * @param totalRows the new totalRows value
     */
    public void setTotalRows(long totalRows) {
        this.totalRows = totalRows;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return "totalRows=" + totalRows + " numPreviousRows=" + numPreviousRows;
    }

    /**
     * @return List of selected data.
     */
    public List<DataObject> getSelectedData() {
        return selectedData;
    }

    /**
     * @param selectedData the new selected data.
     */
    public void setSelectedData(List<DataObject> selectedData) {
        this.selectedData = selectedData;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 