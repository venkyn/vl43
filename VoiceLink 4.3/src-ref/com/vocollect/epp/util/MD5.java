/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class provides MD5-encoding utility methods.
 * @author Dennis Doubleday
 * @author Andrew Snyder
 */
public abstract class MD5 {

    private static final String VALID_CHARS = "01234567890ABCEF";

    /**
     * Constructor is protected, no instances for static utility class.
     */
    protected MD5() {
    }
    
    /**
     * Create an MD5 digest of the content and return it in raw byte array form.
     * @param content the content to digest
     * @return the MD5 digest byte array
     */
    public static byte[] makeDigest(byte[] content) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return md.digest(content);

        } catch (NoSuchAlgorithmException e) {
            // Should never happen
            throw new RuntimeException("NoSuchAlgorithmException for MD5");
        }
    }

    /**
     * Create an MD5 digest of the content and return it in hex-encoded form.
     * @param content the content to digest
     * @return the hex-encoded MD5 digest (32 chars)
     */
    public static String makeHexDigest(byte[] content) {
        return toHexString(makeDigest(content));
    }

    /**
     * MD5-encode the content and return it in hex-encoded form.
     * @param content the content to encode
     * @return the hex-encoded MD5 digest (32 chars)
     */
    public static String encode(String content) {
        return makeHexDigest(content.getBytes());
    }

    /**
     * Helper method to convert a byte[] into a hex String where each byte
     * is represented by two characters 0-F.  This is useful as the standard
     * representation of MD5 checksum data.
     * @param data the byte[] to convert
     * @return a String hexidecimal representation of the byte array data
     */
    public static String toHexString(byte[] data) {
        StringBuilder hexString = new StringBuilder();
        String hex = null;
        final int hexByte = 0xFF;
        for (int i = 0; i < data.length; i++) {
            // can't directly typecast a byte to an integer...have to preform
            // this bit of bit manipulation
            hex = Integer.toHexString(hexByte & data[i]);
            // make sure we spit out 2 characters for every byte of data
            if (hex.length() < 2) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    /**
     * Tests to see if a given string is a valid output from makeHexDigest --
     * that is, 32 bytes long and made up only of valid hex digits.
     * @param md5 The string to test
     * @return True if the string is valid, false otherwise
     */
    public static boolean isMD5Valid(String md5) {
        final int byteSize = 32;
        if (md5.length() != byteSize) {
            return false;
        }
        for (int i = 0; i < md5.length(); i++) {
            if (VALID_CHARS.indexOf(md5.charAt(i)) == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Main to allow running this from the command line to encode a word
     * and output the result.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        if (args != null && args.length > 0) {
            System.out.println("Word: " + args[0] + ", digested: "
                + new String(makeDigest(args[0].getBytes())));
            System.out.println("Word: " + args[0] + ", digested and encoded: "
                + makeHexDigest(args[0].getBytes()));
            System.out.println("Empty string, digested and encoded: "
                + makeHexDigest("".getBytes()));
        } else {
            System.out.println("No word to encode");
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 