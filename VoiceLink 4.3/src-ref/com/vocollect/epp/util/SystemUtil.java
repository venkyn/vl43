/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.util.ResourceBundle;

/**
 * Miscellaneous utilities that didn't seem to go elsewhere.
 *
 * @author dkertis
 */
public final class SystemUtil {

    /**
     * Constructor is private, no instances needed.
     */
    private SystemUtil() {

    }

    /**
     * @return the path to the system log directory
     */
    public static String getSystemLogDirectory() {

        ResourceBundle rb = null;

        rb = ResourceBundle.getBundle("log");
        String logdir = rb.getString("system.log.directory");
        // now construct the filename of the file we want to see
        if (!(logdir.endsWith("/") || logdir.endsWith("\\"))) {

            logdir += "/";
        }
        return logdir;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 