/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.exceptions.ReportException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

/**
 * This class is jasper reports implementation of ReportWrapper. Application
 * specific APIs are exposed which further call jasper library methods to access
 * report definition .jrxml file. This class also handles report generation by
 * consuming parameter values, locale, font information etc.
 * @author mraj
 * 
 */
public class JasperReportWrapperRoot implements ReportWrapper {

    private JasperDesign reportDesign;

    private JasperReport report;

    private JRFileVirtualizer virtualizer;

    private String reportPathName;

    private static final Integer VIRTUALIZER_SIZE = 500;

    private static final String VIRTUALIZER_DIRECTORY = "temp";

    private static Logger log = new Logger(JasperReportWrapper.class);

    /**
     * Constructor.
     * @param reportsPath String value of the path of the directory where
     *            reports are located
     * @param reportName String value of the filename of the report file
     *            (including .jrxml)
     */
    public JasperReportWrapperRoot(String reportsPath, String reportName) {
        this.reportPathName = reportsPath + '/' + reportName;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapper#getParameters()
     */
    public List<ReportParametersDTO> getParameters() throws VocollectException {

        // Throw exception if is no report
        if (this.reportDesign == null) {
            if (!reportExists()) {
                throw new VocollectException(ReportError.REPORT_NOT_FOUND);
            }
        }

        // Get the parameters from the report
        JRParameter[] reportDefinitionParams = this.reportDesign
            .getParameters();
        if (log.isDebugEnabled()) {
            log.debug("Number of parameters found: "
                + reportDefinitionParams.length);
        }

        // Create a list of the reports from the report
        List<ReportParametersDTO> listReportParameters = new ArrayList<ReportParametersDTO>(
            reportDefinitionParams.length);
        ReportParametersDTO param;
        for (int i = 0; i < reportDefinitionParams.length; i++) {
            param = new ReportParametersDTO();
            param.setDescription(reportDefinitionParams[i].getDescription());
            param.setName(reportDefinitionParams[i].getName());
            param.setFieldType(reportDefinitionParams[i].getValueClassName());
            param.setSystemDefined(reportDefinitionParams[i].isSystemDefined());
            param.setValueClass(reportDefinitionParams[i].getValueClass());
            param.setRequired(false);
            listReportParameters.add(param);

        }
        return listReportParameters;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapper#generateReport(java.util.Map,
     *      java.lang.String, java.lang.String, java.lang.Object)
     */
    public JasperPrint generateReportForTask(final Map<String, Object> values,
                                             final String renderFormat,
                                             final String language,
                                             final Object dataSource)
        throws VocollectException {
        
        if (this.reportDesign == null) {
            if (!reportExists()) {
                throw new VocollectException(ReportError.REPORT_NOT_FOUND);
            }
        }
        JasperPrint jasperPrint = generateJasperPrint(
            values, renderFormat, language, dataSource);
        
        return jasperPrint;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapper#generateReport(java.util.Map,
     *      java.lang.String, java.lang.String, java.lang.Object)
     */
    public InputStream generateReport(final Map<String, Object> values,
                                      final String renderFormat,
                                      final String language,
                                      final Object dataSource)
        throws VocollectException {
        if (this.reportDesign == null) {
            if (!reportExists()) {
                throw new VocollectException(ReportError.REPORT_NOT_FOUND);
            }
        }
        JasperPrint jasperPrint = generateJasperPrint(
            values, renderFormat, language, dataSource);

        return renderReport(jasperPrint, renderFormat, language);
    }

    /**
     * Method to generate jasper print format for report.
     * @param values Map<String, Object>
     * @param renderFormat String
     * @param language String
     * @param dataSource Object
     * @return jasperPrint JasperPrint.
     * @throws VocollectException
     */
    private JasperPrint generateJasperPrint(final Map<String, Object> values,
                                            final String renderFormat,
                                            final String language,
                                            final Object dataSource)
        throws VocollectException {

        long start = System.currentTimeMillis();
        if (log.isInfoEnabled()) {
            log.info("Generating " + this.reportDesign.getName()
                + " report in " + renderFormat + " format for language "
                + language.toUpperCase());
        }

        try {
            this.report = JasperCompileManager.compileReport(reportDesign);
        } catch (JRException ex) {
            log.error(
                reportDesign.getName() + "." + renderFormat
                    + " could not be compiled",
                ReportError.JASPER_COMPILE_ERROR, ex);
            throw new VocollectException(ReportError.JASPER_COMPILE_ERROR, ex);
        }

        // Create a new JasperPrint Object to fill.
        JasperPrint jasperPrint;

        virtualizer = new JRFileVirtualizer(
            VIRTUALIZER_SIZE, VIRTUALIZER_DIRECTORY);

        // Add the new virtualizer to the report parameters
        values.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        try {
            if (dataSource == null) {
                jasperPrint = JasperFillManager.fillReport(this.report, values);
                if (log.isInfoEnabled()) {
                    log.info("No data source specified for "
                        + this.report.getName());
                }
            } else {
                jasperPrint = JasperFillManager.fillReport(
                    this.report, values, (JRDataSource) dataSource);
                if (log.isInfoEnabled()) {
                    log.info("Using " + dataSource.toString()
                        + " data source for " + this.report.getName());
                }
            }
        } catch (JRException ex) {
            log.error(
                reportDesign.getName() + " could not be populated with values",
                ReportError.JASPER_FILL_ERROR, ex);
            throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
        }

        if (log.isInfoEnabled()) {
            log.info(this.report.getName() + " report filled in: "
                + (System.currentTimeMillis() - start) + " ms");
        }

        return jasperPrint;
    }

    /**
     * Method to create the byte stream of the generated report. Applies the
     * encoding, format etc. to the stream to be generated
     * 
     * @param reportResult - Jasper generated reported.
     * @param format - format to render report in
     * @param language - Locale language to be used in reports
     * @return Byte stream of the generated report
     * @throws VocollectException - Vocollect exceptions
     * @throws ReportException - Report exceptions
     */
    private InputStream renderReport(JasperPrint reportResult,
                                     String format,
                                     String language)
        throws VocollectException, ReportException {
        ByteArrayOutputStream outStream = null;
        StringBuffer stringBuffer = null;

        // Create the byte output stream for report generation
        byte[] reportBytes = null;
        outStream = new ByteArrayOutputStream();
        JRAbstractExporter exporter = null;

        // Get the report format and set report format specific
        // exporters/options
        try {
            long start = System.currentTimeMillis();

            if (format.equals(RenderFormat.HTML.name())) {
                // setting this to JRXhtmlExporter for better formatting
                // exporter = new JRHtmlExporter();
                exporter = new JRXhtmlExporter();
                if (log.isDebugEnabled()) {
                    log.debug(this.report.getName()
                        + " HTML report exported in: "
                        + (System.currentTimeMillis() - start) + " ms");
                }

            } else if (format.equals(RenderFormat.PDF.name())) {
                exporter = new JRPdfExporter();
                if (log.isDebugEnabled()) {
                    log.debug(this.report.getName()
                        + " PDF report exported in: "
                        + (System.currentTimeMillis() - start) + " ms");
                }

            } else if (format.equals(RenderFormat.RTF.name())) {
                exporter = new JRRtfExporter();
                if (log.isDebugEnabled()) {
                    log.debug(this.report.getName()
                        + " RTF report exported in: "
                        + (System.currentTimeMillis() - start) + " ms");
                }

            } else if (format.equals(RenderFormat.XLS.name())) {
                exporter = new JExcelApiExporter();
                if (log.isDebugEnabled()) {
                    log.debug(this.report.getName()
                        + " XLS report exported in: "
                        + (System.currentTimeMillis() - start) + " ms");
                }

            } else {
                log.error(
                    "Unsupported format: " + format + " found.",
                    ReportError.BAD_RENDER_FORMAT);
                throw new VocollectException(ReportError.BAD_RENDER_FORMAT);

            }

            // HTML requires a string buffer
            if (format.equals(RenderFormat.HTML.name())) {
                stringBuffer = new StringBuffer();
                exporter.setParameter(
                    JRExporterParameter.OUTPUT_STRING_BUFFER, stringBuffer);

            } else {
                exporter.setParameter(
                    JRExporterParameter.OUTPUT_STREAM, outStream);
            }

            exporter.setParameter(
                JRExporterParameter.JASPER_PRINT, reportResult);

            exporter.exportReport();

        } catch (JRException ex) {
            throw new VocollectException(ReportError.JASPER_EXPORT_ERROR, ex);
        }

        if (format.equals(RenderFormat.HTML.name())) {
            try {
                reportBytes = stringBuffer.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new VocollectException(ex);
            }
        } else {
            reportBytes = outStream.toByteArray();
        }

        if (log.isInfoEnabled()) {
            log.info(this.report.getName() + " report generated in " + format
                + " format in " + reportBytes.length + " bytes");
        }
        // Create input stream for Struts result to read
        virtualizer.cleanup();

        return new ByteArrayInputStream(reportBytes);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapperRoot#reportExists(java.lang.String,
     *      java.lang.String)
     */
    public boolean reportExists(String reportsPath, String reportName)
        throws VocollectException {

        if (StringUtil.isNullOrEmpty(reportsPath)
            || StringUtil.isNullOrEmpty(reportName)) {
            throw new VocollectException(ReportError.BAD_REPORT_NAME);
        }

        return reportExists(reportsPath + '/' + reportName);
    }

    // TODO this makes no sense reportPathName will always be null if this is
    // invoked as is.

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapper#reportExists()
     */
    public boolean reportExists() throws VocollectException {

        if (StringUtil.isNullOrEmpty(this.reportPathName)) {
            throw new VocollectException(ReportError.NO_REPORT_NAME);
        }

        return reportExists(this.reportPathName);
    }

    // TODO if the above code goes away then this needs moved to the
    // reportExists(String, String)and all reports launch calls should be
    // validated with path and report name.

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.util.ReportWrapper#reportExists(java.lang.String)
     */
    public boolean reportExists(String reportsPath) throws VocollectException {

        if (StringUtil.isNullOrEmpty(reportsPath)) {
            throw new VocollectException(ReportError.BAD_REPORT_NAME);
        }

        JasperDesign jasperReportDesign = null;
        try {
            jasperReportDesign = JRXmlLoader.load(reportsPath);
        } catch (JRException ex) {
            if (ex.getCause() instanceof FileNotFoundException) {
                log.warn("Report not found at: " + reportsPath);
                return false;
            } else {
                throw new VocollectException(ReportError.JASPER_LOAD_ERROR, ex);
            }
        }

        // Hold a reference to the opened report.
        this.reportPathName = reportsPath;
        this.reportDesign = jasperReportDesign;
        return Boolean.TRUE;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 