/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * This value object class defines properties that describe data imported from
 * or exported to a file.
 *
 * @author J Kercher
 */
public class FileUtil implements Serializable {

    /*
     * The following properties describe a generic file.
     */
    private static final long serialVersionUID = -6001498271965124586L;

    // File name
    private String name;

    // File contents
    protected byte[] contents;

    /**
     * When faking out the headers, we'll want to save the original byte this
     * byte is what was in the original contents.
     */
    private byte fakedByte;

    // File size
    private long size;

    // File md5 checksum
    private String md5;

    /**
     * Return the name of this task component.
     * @return String representing the task component name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name for this task component.
     * @param name String specifies the task component name.
     */
    public final void setName(String name) {
        this.name = name;
    }

    /**
     * Return this task component's contents.
     * @param fakedHeaders Whether or not to fake out the headers from ABCDARES
     *            to ABCDARE2
     * @return byte[] representing this task component's contents
     */
    public byte[] getContents(boolean fakedHeaders) {

        if (contents != null && fakedHeaders) {
            // Here we'll do an in place switch of the offending byte. This is
            // to minimize the memory overhead in VoiceConsole. Since we already
            // use a large amount, we might as well look for ways to improve our
            // footprint. This should be an easy way to save space.
            if (fakedByte == 0) {
                fakedByte = contents[7];
            }

            contents[7] = fakedHeaders ? (byte) 'S' : fakedByte;
        }

        return contents;
    }

    /**
     * Return this task component's contents.
     * @return byte[] representing this task component's contents
     */
    public byte[] getContents() {
        return getContents(false);
    }

    /**
     * Set this task component's contents.
     * @param contents byte[] representing this task component's contents
     */
    public void setContents(byte[] contents) {
        this.contents = contents;

        // Automatically update the size property.
        if (contents != null) {
            setSize(contents.length);
            setMD5(com.vocollect.epp.util.MD5.makeHexDigest(contents));
        } else {
            setSize(0);
        }
    }

    /**
     * Return this task component's contents as a String.
     * @return String representing this task component's contents
     */
    public String getContentsAsString() {
        try {
            // I18NM: Setting UTF-8 for the byte array
            return new String(getContents(), I18NData.getInstance()
                .getDefEncoding());
        } catch (UnsupportedEncodingException e) {
            // If errors out, use an encoding associated to the system locale
            return new String(getContents());
        }
    }

    /**
     * Return the size of this file's data.
     * @return int representing the size of this file's data.
     */
    public long getSize() {
        return size;
    }

    /**
     * Set the size of this file's data. Note that if data exists, then setting
     * this property will throw a runtime exception.
     * @param size int representing the size of this file's data.
     */
    public void setSize(long size) {
        this.size = size;
    }

    /**
     * Return the md5 checksum of this file's data.
     * @return String md5 checksum of this file's data.
     */
    public String getMD5() {
        return this.md5;
    }

    /**
     * Set the md5 of this file's data. Note that if data exists, then setting
     * this property will throw a runtime exception.
     * @param newMd5 String the md5 of this file's data.
     */
    public void setMD5(String newMd5) {
        this.md5 = newMd5;
    }

    /**
     * This method is needed to extend ValueObject, however, no validation is
     * required. Subclasses may override this method to validate name and
     * contents as needed.
     * @return Map contains error message keys if validation fails, otherwise
     *         empty
     */
    protected Map<String, String[]> validateProperties() {
        HashMap<String, String[]> errors = new HashMap<String, String[]>();

        // No need for a separate validator class at this time.
        if (name != null) {
            if ((name == null) || (name.length() == 0)) {
                errors.put(
                    "name",
                    new String[] { "fileProperties.error.name.required" });
            }
        }

        return errors;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 