/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.model.AlertCriteriaRelation;
import com.vocollect.epp.alert.model.AlertOperandType;
import com.vocollect.epp.alert.model.AlertReNotificationFrequency;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public final class AlertUtil {

    public static final String PROPERTIES = "properties";

    public static final String CRITERIAS = "criterias";

    public static final String ID = "id";

    public static final String TEXT = "text";

    public static final String SEQUENCE = "sequence";

    public static final String OPERANDTYPE = "operandType";

    public static final String THRESHOLD = "threshold";

    public static final String RELATION = "relation";

    public static final int TYPE_STRING = 0;

    public static final int TYPE_NUMBER = 1;

    @SuppressWarnings("serial")
    private static Map<DAFieldType, AlertOperandType[]> fieldOperandMap = 
    new HashMap<DAFieldType, AlertOperandType[]>() {

        {
            // Integer to operand map
            put(DAFieldType.INTEGER, new AlertOperandType[] {
                AlertOperandType.EQUALSTO, AlertOperandType.GREATERTHAN,
                AlertOperandType.GREATERTHANOREQUALSTO,
                AlertOperandType.LESSTHAN, AlertOperandType.LESSTHANOREQUALTO,
                AlertOperandType.NOTEQUALSTO });
            
            // Float to operand map
            put(DAFieldType.FLOAT, new AlertOperandType[] {
                AlertOperandType.EQUALSTO, AlertOperandType.GREATERTHAN,
                AlertOperandType.GREATERTHANOREQUALSTO,
                AlertOperandType.LESSTHAN, AlertOperandType.LESSTHANOREQUALTO,
                AlertOperandType.NOTEQUALSTO });

            // String to operand map
            put(DAFieldType.STRING, new AlertOperandType[] {
                AlertOperandType.STRINGEQUALS, AlertOperandType.STRINGNOTEQUALS,
                AlertOperandType.STARTSWITH, AlertOperandType.ENDSWITH,
                AlertOperandType.CONTAINS });

            // Time to operand map
            put(DAFieldType.TIME, new AlertOperandType[] {
                AlertOperandType.AFTER, AlertOperandType.BEFORE });
            
            // Date to operand map
            put(DAFieldType.DATE, new AlertOperandType[] {
                AlertOperandType.AFTER, AlertOperandType.BEFORE });
        }
    };

    /**
     * Private Constructor.
     */
    private AlertUtil() {

    }

    /**
     * utility method to get alert operands by type.
     * @param type - the return type
     * @return list of alert operands
     */
    public static List<AlertOperandType> getOperandByFieldType(DAFieldType type) {
        AlertOperandType[] operands = AlertUtil.fieldOperandMap.get(type);
        return Arrays.asList(operands);
    }

    /**
     * @param da the data aggregator
     * @return JSON of data aggregator properties and operands
     * @throws JSONException If any JSON building exception happens
     */
    public static JSONArray daPropertyToJSON(DAInformation da)
        throws JSONException {
        JSONArray propertiesArray = new JSONArray();
        JSONObject field;

        // Create array of DA properties
        for (DAColumn column : da.getColumns()) {
            if (column.getColumnState() == DAColumnState.MARKEDFORDELETE) {
                continue;
            }
            field = new JSONObject();

            JSONArray operandArray = new JSONArray();

            List<AlertOperandType> operandTypes = AlertUtil
                .getOperandByFieldType(column.getFieldType());

            // Add operand id and text
            for (AlertOperandType operandType : operandTypes) {
                JSONObject operandObj = new JSONObject();
                operandObj.put(AlertUtil.ID, operandType.toValue());
                operandObj.put(AlertUtil.TEXT, ResourceUtil
                    .getLocalizedKeyValue(operandType.getResourceKey()));
                operandArray.put(operandObj);
            }
            field.put(GenericDataAggregator.FIELD_ID, column.getFieldId());
            field.put(GenericDataAggregator.DISPLAY_NAME,
                ResourceUtil.getLocalizedKeyValue(column.getDisplayName()));
            field.put(GenericDataAggregator.UOM,
                ResourceUtil.getLocalizedKeyValue(column.getUom()));
            field.put(AlertUtil.OPERANDTYPE, operandArray);
            propertiesArray.put(field);
        }

        return propertiesArray;
    }

    /**
     * @param alert the alert object
     * @return JSON version of the criteria
     * @throws JSONException if any JSON persing happens
     */
    public static JSONArray alertCriteriaToJSON(Alert alert)
        throws JSONException {
        List<AlertCriteria> criterias = alert.getAlertCriterias();

        JSONArray criteriaJSONs = new JSONArray();

        // Sorting the criteria on sequence
        sortCriteria(criterias);

        for (AlertCriteria criteria : criterias) {
            String fieldId = criteria.getDaColumn().getFieldId();
            int sequence = criteria.getSequence();
            int operandType = criteria.getOperandType().toValue();
            String threshold = criteria.getThreshold();
            AlertCriteriaRelation relation = criteria.getRelation();

            JSONObject criteriaJSON = new JSONObject();
            criteriaJSON.put(GenericDataAggregator.FIELD_ID, fieldId);
            criteriaJSON.put(AlertUtil.SEQUENCE, sequence);
            criteriaJSON.put(AlertUtil.OPERANDTYPE, operandType);
            criteriaJSON.put(AlertUtil.THRESHOLD, threshold);
            criteriaJSON.put(AlertUtil.RELATION, relation.toValue());

            criteriaJSONs.put(criteriaJSON);
        }

        return criteriaJSONs;
    }

    /**
     * Sorts criteria by sequence.
     * @param criterias to sort
     */
    public static void sortCriteria(List<AlertCriteria> criterias) {
        Collections.sort(criterias, new Comparator<AlertCriteria>() {

            @Override
            public int compare(AlertCriteria ac1, AlertCriteria ac2) {
                return ac1.getSequence().compareTo(ac2.getSequence());
            }
        });
    }

    /**
     * Method to get calculate the re-notification frequency in minutes.
     * @param freq - the re-notifiaction frequency
     * @return frequency in minutes
     */
    public static int getFrequencyInMin(AlertReNotificationFrequency freq) {
        int total = 0;
        if (freq.getHours() != null) {
            total = freq.getHours().intValue() * 60;
        }

        if (freq.getMinutes() != null) {
            total += freq.getMinutes().intValue();
        }
        return total;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 