/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;

import java.util.Calendar;
import java.util.Date;


/**
 * These enums are going to represent the various time windows that can be applied
 * for date filtering situations.
 *
 * @author jstonebrook
 */
public enum FilterTimeWindow implements ValueBasedEnum<FilterTimeWindow> {
    WITHIN_LAST_8_HOURS(1, -8),
    WITHIN_LAST_10_HOURS(2, -10),
    WITHIN_LAST_24_HOURS(3, -24),
    WITHIN_LAST_2_DAYS(4, -48),
    WITHIN_LAST_3_DAYS(5, -72),
    WITHIN_LAST_7_DAYS(6, -168),
    TODAY(7, 0);
    
    private int id;
    private int hourDifference;
    
    private static ReverseEnumMap<FilterTimeWindow> toValueMap =
        new ReverseEnumMap<FilterTimeWindow>(FilterTimeWindow.class);
    
    private static final int WITHIN_LAST_8_HOURS_ID = 1;
    private static final int WITHIN_LAST_10_HOURS_ID = 2;
    private static final int WITHIN_LAST_24_HOURS_ID_ID = 3;
    private static final int WITHIN_LAST_2_DAYS_ID = 4;
    private static final int WITHIN_LAST_3_DAYS_ID = 5;
    private static final int WITHIN_LAST_7_DAYS_ID = 6;
    private static final int TODAY_ID = 7;

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     * @return The message key for this enumeration.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return id;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public FilterTimeWindow fromValue(int idValue) {
        return toValueMap.get(idValue);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     */
    public FilterTimeWindow fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * Constructor.
     * @param id of the enumeration
     * @param hourDifference hour difference from current date/time
     */
    FilterTimeWindow(int id, int hourDifference) {
        setId(id);
        setHourDifference(hourDifference);
    }


    /** 
     * This method, given an id, will return the appropriate filter time window.
     * @param id the id to find the appropriate Filter Time Window for
     * @return FilterTimeWindow representing the filter time window that matches the id
     */
    public static FilterTimeWindow getById(int id) {
        boolean found = false;
        int index = 0;
        FilterTimeWindow filterTimeWindow = null;
        while (index < FilterTimeWindow.values().length && !found) {
            filterTimeWindow = FilterTimeWindow.values()[index];
            if (filterTimeWindow.getId() == id) {
                found = true;
            }
            index++;
        }
        if (found) {
            return filterTimeWindow;
        } else {
            return null;
        }
    }

    /**
     * This parameter will substract a number of hours based on the enumeration
     * instance from the baseDate.
     * @param baseDate the date from which hours will be subtracted for the 
     * calculation
     * @return Date a date that is X hours away from the base date - based on the 
     * enumeration type
     */
    public Date applyOffset(Date baseDate) {
        Calendar baseDateCalendar = Calendar.getInstance();
        baseDateCalendar.setTime(baseDate);
        
        switch (getId()) {
        case WITHIN_LAST_8_HOURS_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_8_HOURS
                .getHourDifference());
            break;
        case WITHIN_LAST_10_HOURS_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_10_HOURS
                .getHourDifference());
            break;
        case WITHIN_LAST_24_HOURS_ID_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_24_HOURS
                .getHourDifference());
            break;
        case WITHIN_LAST_2_DAYS_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_2_DAYS
                .getHourDifference());
            break;
        case WITHIN_LAST_3_DAYS_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_3_DAYS
                .getHourDifference());
            break;
        case WITHIN_LAST_7_DAYS_ID:
            baseDateCalendar.add(Calendar.HOUR_OF_DAY, WITHIN_LAST_7_DAYS
                .getHourDifference());
            break;
        case TODAY_ID: {
            baseDateCalendar.set(Calendar.HOUR_OF_DAY, 0);
            baseDateCalendar.set(Calendar.MINUTE, 0);
            baseDateCalendar.set(Calendar.SECOND, 0);
            break;
        }
        default:
            // Don't do anything
        }
        
        return baseDateCalendar.getTime();       
    }
    
    /**
     * Getter for the hourDifference property.
     * @return int value of the property
     */
    public int getHourDifference() {
        return this.hourDifference;
    }

    
    /**
     * Setter for the hourDifference property.
     * @param hourDifference the new hourDifference value
     */
    public void setHourDifference(int hourDifference) {
        this.hourDifference = hourDifference;
    }

    
    /**
     * Getter for the id property.
     * @return int value of the property
     */
    public int getId() {
        return this.id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(int id) {
        this.id = id;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 