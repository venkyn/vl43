/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.web.util.DisplayUtilities;

import java.util.HashSet;
import java.util.List;

/**
 * This class encapsulates metadata necessary to describe the format of tabular
 * data for display within the table component. Format descriptors include sort
 * field, sort order, etc.
 *
 * @author dkertis
 */
public class ResultDataInfo {

    // identifies column used for tooltip info
    private String  tooltipColumn;

    // Identifies the name of the current sort field
    private String  sortColumn;

    // Identifies the current sort order. True implies ascending, false
    // implies descending.
    private boolean sortAscending;

    // Stores the id of the business object we are interested in
    private Long    rowId;

    // Stores the count of rows in the result data (with %).
    private int     rowCount;

    // index we want to start building the data set on
    private int     startIndex;

    // number of rows represented by a pixel on the verticle slider
    private int     numToolTips;

    private int     rowsPerPage;


    private long    offset;

    private boolean refreshRequest;

    private boolean firstTimeRun;

    private Filter filter;

    // The sort column object. Needed for additional information about
    // type of sorting to do.
    private Column sortColumnObject;

    private String whereClause;

    private Object[] queryArgs;

    private HashSet<Long> selectedIds;

    private DisplayUtilities displayUtilties;


    /**
     * Constructor.
     * @param tooltipColumn the column to get tooltips for
     * @param sortColumn identifies the primary sort field
     * @param sortAscending identifies the sort order
     * @param rowId the start row ID
     * @param offset the offset from the top row
     * @param rowCount the number of rows to ultimately retrieve
     * @param startIndex the starting index of retrieval of sorted data
     * @param sliderHeight the height of the slider
     * @param rowsPerPage the number of rows in the view window
     * @param refreshRequest whether or not this is a refresh request
     * @param filter the filter to be applied
     * @param du .
     * @param firstTimeRun reset firstRow to be last piece of selection data
     */
    public ResultDataInfo(String tooltipColumn,
                          String sortColumn,
                          boolean sortAscending,
                          Long rowId,
                          long offset,
                          int rowCount,
                          int startIndex,
                          int sliderHeight,
                          int rowsPerPage,
                          boolean refreshRequest,
                          Filter filter,
                          DisplayUtilities du,
                          boolean firstTimeRun) {
        this.sortColumn = sortColumn;
        this.sortAscending = sortAscending;
        this.rowId = rowId;
        this.offset = offset;
        this.rowCount = rowCount;
        this.startIndex = startIndex;
        this.numToolTips = sliderHeight;
        this.rowsPerPage = rowsPerPage;
        this.refreshRequest = refreshRequest;
        this.tooltipColumn = tooltipColumn;
        this.filter = filter;
        this.firstTimeRun = firstTimeRun;
        this.displayUtilties = du;
    }

    /**
     * Default constructor used for printing.
     */
    public ResultDataInfo() {
    }

    /**
     * @return the query args property.
     */
    public Object[] getQueryArgs() {
        return queryArgs;
    }

    /**
     * @param queryArgs the query args to add
     */
    public void setQueryArgs(Object[] queryArgs) {
        this.queryArgs = queryArgs;
    }


    /**
     * @return the additional where clause to add
     */
    public String getWhereClause() {
        return whereClause;
    }


    /**
     * @param whereClause the additional where clause to add
     */
    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    /**
     * @return the zero based index of the total data in the database.
     */
    public long getOffset() {
        return offset;
    }

    /**
     * @param offset the offset property
     */
    public void setOffset(long offset) {
        this.offset = offset;
    }

    /**
     * @return the sort order; true implies ascending, false implies descending
     */
    public boolean isSortAscending() {
        return this.sortAscending;
    }

    /**
     * @param sortAscending specifies the sort order
     */
    public void setSortAscending(boolean sortAscending) {
        this.sortAscending = sortAscending;
    }

    /**
     * @return the sort column
     */
    public String getSortColumn() {
        return this.sortColumn;
    }

    /**
     * @param sortColumn identifies the sort column
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * @return the id of the object represented by the first row in the result
     *         data (not including the previous buffer data for windowing.)
     */
    public Long getFirstRowId() {
        return this.rowId;
    }

    /**
     * @param firstRowId identifies the object represented by the first row of
     *            the result data.
     */
    public void setFirstRowId(Long firstRowId) {
        this.rowId = firstRowId;
    }

    /**
     * @return the count of rows in the result data.
     */
    public int getRowCount() {
        return this.rowCount;
    }

    /**
     * @param rowCount the count of rows in the result data.
     */
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    /**
     * @return the start index
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * @param startIndex the start index
     */
    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * @return the number of tooltips to get
     */
    public int getNumToolTips() {
        return numToolTips;
    }

    /**
     * @param numToolTips the number of tooltips to get
     */
    public void setNumToolTips(int numToolTips) {
        this.numToolTips = numToolTips;
    }

    /**
     * @return the rows per view window
     */
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    /**
     * @param rowsPerPage the rows per view window
     */
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    /**
     * @return whether or not this is a refresh request
     */
    public boolean getRefreshRequest() {
        return refreshRequest;
    }

    /**
     * @param refreshRequest whether or not this is a refresh request
     */
    public void setRefreshRequest(boolean refreshRequest) {
        this.refreshRequest = refreshRequest;
    }

    /**
     * @return the column name for tooltips.
     */
    public String getTooltipColumn() {
        return tooltipColumn;
    }

    /**
     * @param tooltipColumn the column name for tooltips.
     */
    public void setTooltipColumn(String tooltipColumn) {
        this.tooltipColumn = tooltipColumn;
    }



    /**
     * Getter for the filter property.
     * @return Filter value of the property
     */
    public Filter getFilter() {
        return this.filter;
    }



    /**
     * Setter for the filter property.
     * @param filter the new filter value
     */
    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * Getter for the sortColumnObject property.
     * @return the value of the property
     */
    public Column getSortColumnObject() {
        return this.sortColumnObject;
    }

    /**
     * Setter for the sortColumnObject property.
     * @param sortColumnObject the new sortColumnObject value
     */
    public void setSortColumnObject(Column sortColumnObject) {
        this.sortColumnObject = sortColumnObject;
    }

    /**
     * Set the sort column object from the list of columns. setSortColumn()
     * must be called first to set the sort column name.
     * @param columns The list of the user's columns.
     */
    public void setSortColumnObjectFromColumns(List<Column> columns) {
        if (columns != null) {
            for (Column col : columns) {
                if (col.getField().equals(getSortColumn())) {
                    setSortColumnObject(col);
                    break;
                }
            }
        }
    }

    /**
     * @return Long[] of selected ids.
     */
    public HashSet<Long> getSelectedIds() {
        return selectedIds;
    }

    /**
     * @param selectedIds new selection.
     */
    public void setSelectedIds(HashSet<Long> selectedIds) {
        this.selectedIds = selectedIds;
    }

    /**
     * @return first time run boolean.
     */
    public boolean isFirstTimeRun() {
        return firstTimeRun;
    }

    /**
     * @param firstTimeRun sets the boolean if its the first time.
     */
    public void setFirstTimeRun(boolean firstTimeRun) {
        this.firstTimeRun = firstTimeRun;
    }


    /**
     * Getter for the displayUtilties property.
     * @return DisplayUtilities value of the property
     */
    public DisplayUtilities getDisplayUtilties() {
        return this.displayUtilties;
    }


    /**
     * Setter for the displayUtilties property.
     * @param displayUtilties the new displayUtilties value
     */
    public void setDisplayUtilties(DisplayUtilities displayUtilties) {
        this.displayUtilties = displayUtilties;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 