/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.service.ZipFileException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * This class is responsible for zipping the source data.
 * Usage call the appropriate static class method
 * @author Kalpna T
 */
public final class ZipUtil {

    /**
     * Do not instantiate the class.
     */
    private ZipUtil() { }

    /*
     * Size of Buffer to read at a time.
     */
    private static final int BUFFER = 1024;

    /*
     * The compression level for zip file creation (0-9) 0 = No compression 1 =
     * Standard compression (Very fast) ... 9 = Best compression (Very slow)
     */
    private static final int    COMPRESSION_LEVEL = 1;

    /**
     * This method generate the Zip output stream.
     * @param out the OutputStream
     * @return the ZipOutputStream
     */
    private static ZipOutputStream getZipOutputStream(OutputStream out) {
        ZipOutputStream zipOut = new ZipOutputStream(out);
        zipOut.setLevel(COMPRESSION_LEVEL);
        zipOut.setComment("Created by epp ZipUtil");

        return zipOut;
    }

    /**
     * This method zipup the input data and populate the OutputStream with it.
     * @param inputData The data to be zipped
     * @param out OutputStream which contains the zip data
     * @throws ZipFileException wrapper for various IO exception
     */
    public static void zipData(Map<String, InputStream> inputData, OutputStream out)
           throws ZipFileException {
        ZipOutputStream zipOut = getZipOutputStream(out);
        byte[] buf = new byte[BUFFER];

        Iterator<Map.Entry<String, InputStream>> it = inputData.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, InputStream> pairs = it.next();
            //Transfer bytes to the output stream
            int len;
            try {
                zipOut.putNextEntry(new ZipEntry(pairs.getKey().toString()));
                while ((len = (pairs.getValue()).read(buf, 0, BUFFER)) != -1) {
                    zipOut.write(buf, 0, len);
                }
            } catch (IOException e) {
                throw new ZipFileException(
                    SystemErrorCode.UNABLE_TO_COPY_DATA_TO_ZIP_OUTPUT_STREAM, e);
            }
        }

        //Close and flush the ZipOutputStream
        closeZipStream(zipOut);

    }

    /**
     * This method zipup the input data and retuns the zipped ByteArrayOutputStream.
     * @param inputData The data to be zipped
     * @return OutputStream zipped OutputStream
     * @throws ZipFileException wrapper for various IO exception
     */
    public static OutputStream zipData(Map<String, InputStream> inputData)
           throws ZipFileException {

        OutputStream baos = new ByteArrayOutputStream();

        zipData(inputData, baos);

        return baos;
    }

    /**
     * This method zipup the InputStream.
     * @param fileName name of the input
     * @param sourceStream the inputStream to be zipped
     * @param out the OutputStream which contains the zipped data
     * @throws ZipFileException wrapper for various IO exception
     */
    public static void zipStream(String fileName, InputStream sourceStream, OutputStream out)
           throws ZipFileException {

        Map<String, InputStream> fileMap = new HashMap<String, InputStream>();
        fileMap.put(fileName, sourceStream);
        zipData(fileMap, out);
    }

    /**
     * This method zipup the single input file.
     * @param source The file to be zipped
     * @param out the OutputStream which contains the zipped data
     * @throws ZipFileException wrapper for various IO exception
     */
    public static void zipFile(File source, OutputStream out)
           throws ZipFileException {

        Map<String, InputStream> fileMap = new HashMap<String, InputStream>();
        InputStream is = null;
        try {
          is = new BufferedInputStream(new FileInputStream(source));
        } catch (IOException e) {
            throw new ZipFileException(
                SystemErrorCode.UNABLE_TO_READ_SOURCE_STREAM, e);
        }
        fileMap.put(source.getName(), is);
        zipData(fileMap, out);
    }

    /**
     * This method cleans up and close the OutputStream.
     * @param stream OutputStream
     * @throws ZipFileException wrapper for various IO exception
     */
    private static void closeZipStream(OutputStream stream) throws ZipFileException {
        // Clean up - close the output stream
        try {
            stream.flush();
        } catch (IOException e) {
            throw new ZipFileException(
                SystemErrorCode.FLUSH_OUTPUT_STREAM_FAILED, e);
        }
        try {
            stream.close();
        } catch (IOException e) {
            throw new ZipFileException(
                SystemErrorCode.FLUSH_OUTPUT_STREAM_FAILED, e);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 