/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import java.io.IOException;
import java.text.Collator;
import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;


/**
 * This class contains static String-handling utility methods.
 * @author Dennis Doubleday
 */
public final class StringUtil {

    /**
     * Constructor is private, no instances needed.
     */
    private StringUtil() {

    }

    /**
     * Encode a string using Base64 encoding. Used when storing passwords
     * as cookies.
     *
     * This is weak encoding in that anyone can use the decodeString
     * routine to reverse the encoding.
     *
     * @param str the String to encode
     * @return String in encoded form.
     */
    public static String encodeBase64(String str) {
        sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
        return encoder.encodeBuffer(str.getBytes()).trim();
    }

    /**
     * Decode a string using Base64 encoding.
     *
     * @param str the String decode
     * @return String in decoded form.
     */
    public static String decodeBase64(String str) {
        sun.misc.BASE64Decoder dec = new sun.misc.BASE64Decoder();
        try {
            return new String(dec.decodeBuffer(str));
        } catch (IOException io) {
            throw new RuntimeException(io.getMessage(), io.getCause());
        }
    }

    /**
     * @param s String to test
     * @return true if s is null or empty, false otherwise.
     */
    public static boolean isNullOrEmpty(String s) {
        return (s == null) || (s.length() == 0);
    }
    

    /**
     * @param s String to test
     * @return true if s is not null and not empty, false otherwise.
     */
    public static boolean isNotEmpty(String s) {
        return ((s != null) && (s.length() > 0));
    }

    /**
     * @param s String to test
     * @return true if s is null or empty or whitespace only, false otherwise.
     */
    public static boolean isNullOrBlank(String s) {
        return (s == null) || (s.trim().length() == 0);
    }

    /** 
     * Rid the string of html tagging. 
     * Leaves original string intact, returns the altered copy.
     * Changes < into &lt and > into &gt
     * @param data the String to check
     * @return the (possibly) modified data as a new string.
     */
    public static String dustForHTML(String data) {
        String temp = new String(data);
        temp = temp.replace("<", "&lt;");
        temp = temp.replace(">", "&gt;");
        return temp;
    }

    /**
     * Check to see if the string passed in is all numeric (excluding
     * leading/trailing spaces.
     * NOTE: THIS FUNCTION RETURNS FALSE FOR NEGATIVE NUMBERS.
     * THERE IS CODE WHICH IS UTILIZING THIS AS A FEATURE HOWEVER.
     * @param data the String to test
     * @return true if all of the chars are numeric
     */
    public static boolean isNumeric(String data) {
        // Ignore leading and trailing spaces
        String temp = data.trim();
        String subject;

        boolean allNumeric = true;
        
        
        for (int i = 0; i < (temp.length()) && (allNumeric); i++) {
              // get the substring of length one
              subject = temp.substring(i, i + 1);
              // return true if it is a digit.
              allNumeric = subject.matches("\\d");
        }
        return allNumeric;
    }

    /**
     * Returns a Collator that is tuned for the current Locale.
     * @return a Collator that recognizes only PRIMARY differences when
     * ordering Strings. For example, case is ignored by this Collator.
     * If the current locale is not in the ThreadLocal, just use the
     * system Locale.
     * @see java.text.Collator
     */
    public static Collator getPrimaryCollator() {
        Locale currentLocale = LocaleContextHolder.getLocale();
        Collator collator = null;
        
        if (currentLocale != null) {
            collator = Collator.getInstance(currentLocale);
        } else {
            // Locale unknown, assume system Locale.
            collator = Collator.getInstance();
        }
        
        collator.setStrength(Collator.PRIMARY);
        return collator;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 