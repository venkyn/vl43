/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.FileView;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.web.util.DisplayUtilities;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * These enums are going to represent the various operands that can be applied
 * for filtering situations.
 * 
 * @author jstonebrook
 */
public enum OperandType {

    STARTS_WITH(-1), // Strings
    ENDS_WITH(-2), // Strings
    CONTAINS(-3), // Strings
    EQUAL_STRING(-4), // Strings
    EQUAL(-11), // Numbers
    GREATER_THAN(-12), // Numbers
    GREATHER_THAN_OR_EQUAL(-13), // Numbers
    LESS_THAN(-14), // Numbers
    LESS_THAN_OR_EQUAL(-15), // Numbers
    BETWEEN(-16), // Numbers
    WITHIN(-23), // Dates
    NOT_WITHIN(-22), // Dates
    EMPTY_DATE(-21), //Dates    
    EQUAL_TO(-31), // Enums
    NOT_EQUAL_TO(-32), // Enums
    EQUAL_TO_BOOLEAN(-41), // Boolean
    NOT_EQUAL_TO_BOOLEAN(-42); // Boolean

    private int id;

    private static final Logger log = new Logger(OperandType.class);

    private static final int STARTS_WITH_ID = -1;

    private static final int ENDS_WITH_ID = -2;

    private static final int CONTAINS_ID = -3;

    private static final int EQUAL_STRING_ID = -4;

    private static final int EQUAL_ID = -11;

    private static final int GREATER_THAN_ID = -12;

    private static final int GREATER_THAN_OR_EQUAL_ID = -13;

    private static final int LESS_THAN_ID = -14;

    private static final int LESS_THAN_OR_EQUAL_ID = -15;

    private static final int BETWEEN_ID = -16;

    private static final int WITHIN_ID = -23;

    private static final int NOT_WITHIN_ID = -22;
    
    private static final int EMPTY_DATE_ID = -21;

    private static final int EQUAL_TO_ID = -31;

    private static final int NOT_EQUAL_TO_ID = -32;

    private static final int EQUAL_TO_BOOLEAN_ID = -41;

    private static final int NOT_EQUAL_TO_BOOLEAN_ID = -42;

    /**
     * Constructor.
     * @param id of the operand
     */
    OperandType(int id) {
        this.id = id;
    }

    /**
     * Getter for the id property.
     * @return int value of the property
     */
    public int getId() {
        return this.id;
    }

    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * This method, given an id, will return the appropriate operand type.
     * @param id the id to find the appropriate Operand for
     * @return OperandType representing the operand that matches the id
     */
    public static OperandType getById(long id) {
        boolean found = false;
        int index = 0;
        OperandType operandType = null;
        while (index < OperandType.values().length && !found) {
            operandType = OperandType.values()[index];
            if (operandType.getId() == (int) id) {
                found = true;
            }
            index++;
        }
        if (found) {
            return operandType;
        } else {
            return null;
        }
    }

    /**
     * This operand will compute whether or not the passed in data model will
     * pass the filter. The filter is based on FilterCriterion.
     * @param dataObject the object to test filtering on
     * @param filterCriterion the filter criteria to use
     * @param du the display utilities
     * @return boolean - true means the object passed and can remain, false
     *         means it should be removed from consideration
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     * @throws JSONException 
     * 
     */
    public boolean filter(DataObject dataObject,
                          FilterCriterion filterCriterion,
                          DisplayUtilities du)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        boolean result = false;

        if (dataObject != null) {
            Object object = null;
            try {
                Object fieldVal = PropertyUtils.getNestedProperty(
                    dataObject, filterCriterion.getColumn().getField());

                object = this.getDisplayedText(du, filterCriterion.getColumn()
                    .getDisplayFunction(), fieldVal, dataObject);

                if (object == null) {
                    object = fieldVal;
                }
            } catch (NestedNullException e) {
                return false;
            } catch (InvocationTargetException e) {
                if (e.getCause() instanceof NullPointerException) {
                    return false;
                }
                throw e;
            }
            if (object != null) {
                // I know someone will yell about the checkstyle errors but you
                // can't define
                // constaints in the enum class - therefore these will have to
                // just be.
                switch (getId()) {
                case STARTS_WITH_ID: { // Starts With Case
                    result = startsWith(dataObject, object, filterCriterion);
                    break;
                }
                case ENDS_WITH_ID: { // Ends with Case
                    result = endsWith(dataObject, object, filterCriterion);
                    break;
                }
                case CONTAINS_ID: { // Contains Case
                    result = contains(dataObject, object, filterCriterion);
                    break;
                }
                case EQUAL_STRING_ID: { // Equal String Case
                    result = equalStringCase(
                        dataObject, object, filterCriterion);
                    break;
                }
                case EQUAL_ID:
                case GREATER_THAN_ID:
                case GREATER_THAN_OR_EQUAL_ID:
                case LESS_THAN_ID:
                case LESS_THAN_OR_EQUAL_ID:
                case BETWEEN_ID: { // Number Case
                    result = compareNumber(dataObject, object, filterCriterion);
                    break;
                }
                case WITHIN_ID: { // Within Case
                    result = withIn(dataObject, object, filterCriterion);
                    break;
                }
                case NOT_WITHIN_ID: { // Not Within Case
                    result = notWithIn(dataObject, object, filterCriterion);
                    break;
                }
                case EMPTY_DATE_ID: { // Empty Date Case
                    result = empty(dataObject, object, filterCriterion);
                    break;
                }
                case EQUAL_TO_ID: { // Equal To Case
                    result = equalTo(dataObject, object, filterCriterion);
                    break;
                }
                case NOT_EQUAL_TO_ID: { // Not Equal To Case
                    result = notEqualTo(dataObject, object, filterCriterion);
                    break;
                }
                case EQUAL_TO_BOOLEAN_ID: { // Equal To Boolean Case
                    result = equalToBoolean(dataObject, object, filterCriterion);
                    break;
                }
                case NOT_EQUAL_TO_BOOLEAN_ID: { // Not Equal To Boolean Case
                    result = notEqualToBoolean(
                        dataObject, object, filterCriterion);
                    break;
                }
                default: {
                    result = false;
                    break;
                }
                }
            }
        }

        return result;
    }

    /**
     * This method will implement the startsWith filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     * @throws JSONException 
     * 
     */
    protected boolean startsWith(DataObject dataObject,
                                 Object object,
                                 FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        String testString = "";
        testString = checkJSON(object);
        
        return testString.toUpperCase().startsWith(
            filterCriterion.getValue1().toUpperCase());
    }

    /**
     * This method will implement the endsWith filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     * @throws JSONException 
     * 
     *  
     */
    protected boolean endsWith(DataObject dataObject,
                               Object object,
                               FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        String testString = "";
        testString = checkJSON(object);
       
        return testString.toUpperCase().endsWith(
            filterCriterion.getValue1().toUpperCase());
    }
    /**
     * The function checks to see if the object is of type JSON or not and then 
     * returns the string value
     * @param object - Object Type
     * @param <T> -
     * @return String - The string value of the object parameter 
     * @throws JSONException
     */

    private <T> T checkJSON(Object object) throws JSONException {
        String errorMsg = "Method: checkJSON threw exception: ";
        if (object instanceof JSONObject) {
            // Parsing the current JSON response with unique key "name"
            try {
                return (T)((JSONObject) object).get("name");
            } catch (JSONException e) {
                throw new JSONException(errorMsg + e.getMessage());
            }
        }
        return (T)object;
    }


    /**
     * This method will implement the contains filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception 
     * 
     *      */
    protected boolean contains(DataObject dataObject,
                               Object object,
                               FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        String testString = "";
        testString = checkJSON(object);
        
        log.debug("Data object : " + dataObject.getClass().getCanonicalName());
        if (dataObject instanceof FileView) {
            log.debug("TYPE : " + ((FileView) dataObject).getFileType());
        }
        log.debug("ts - " + testString.toUpperCase() + " : "
            + filterCriterion.getValue1().toUpperCase());
        return testString.toUpperCase().contains(
            filterCriterion.getValue1().toUpperCase());
    }

    /**
     * This method will implement the equal for string filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     * @throws JSONException 
     *  
     */
    protected boolean equalStringCase(DataObject dataObject,
                                      Object object,
                                      FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        String testString = "";
        testString = checkJSON(object);
        return testString.toUpperCase().equals(
            filterCriterion.getValue1().toUpperCase());
    }

    /**
     * This method will know how to detect the type of number being compared and
     * perform the appropriate cast. Then the operation will be performed
     * @param dataObject the field to test against
     * @param fieldParam the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return whether or not the field should pass the filter - true means yes
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     * @throws JSONException 
     */
    protected boolean compareNumber(DataObject dataObject,
                                    Object fieldParam,
                                    FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException, JSONException {
        Number field = checkJSON(fieldParam);
        if (field instanceof Integer) {
            Integer fieldValue = checkJSON(field);
            Integer filterValue1 = Integer.valueOf(filterCriterion.getValue1());

            // Again - checkstyle - don't complain about the magic numbers
            // because I can't fix these
            switch (getId()) {
            case EQUAL_ID:
                return fieldValue.intValue() == filterValue1.intValue();
            case GREATER_THAN_ID:
                return fieldValue.intValue() > filterValue1.intValue();
            case GREATER_THAN_OR_EQUAL_ID:
                return fieldValue.intValue() >= filterValue1.intValue();
            case LESS_THAN_ID:
                return fieldValue.intValue() < filterValue1.intValue();
            case LESS_THAN_OR_EQUAL_ID:
                return fieldValue.intValue() <= filterValue1.intValue();
            case BETWEEN_ID: {
                Integer filterValue2 = Integer.valueOf(filterCriterion
                    .getValue2());
                return filterValue1.intValue() <= fieldValue.intValue()
                    && fieldValue.intValue() <= filterValue2.intValue();
            }
            default: {
                return false;
            }
            }
        }

        if (field instanceof Long) {
            Long fieldValue = (Long) field;
            Long filterValue1 = Long.valueOf(filterCriterion.getValue1());

            switch (getId()) {
            case EQUAL_ID:
                return fieldValue.longValue() == filterValue1.longValue();
            case GREATER_THAN_ID:
                return fieldValue.longValue() > filterValue1.longValue();
            case GREATER_THAN_OR_EQUAL_ID:
                return fieldValue.longValue() >= filterValue1.longValue();
            case LESS_THAN_ID:
                return fieldValue.longValue() < filterValue1.longValue();
            case LESS_THAN_OR_EQUAL_ID:
                return fieldValue.longValue() <= filterValue1.longValue();
            case BETWEEN_ID: {
                Long filterValue2 = Long.valueOf(filterCriterion.getValue2());
                return filterValue1.longValue() <= fieldValue.longValue()
                    && fieldValue.longValue() <= filterValue2.longValue();
            }
            default: {
                return false;
            }
            }
        }

        if (field instanceof Float) {
            Float fieldValue = (Float) field;
            Float filterValue1 = Float.valueOf(filterCriterion.getValue1());

            switch (getId()) {
            case EQUAL_ID:
                return fieldValue.floatValue() == filterValue1.floatValue();
            case GREATER_THAN_ID:
                return fieldValue.floatValue() > filterValue1.floatValue();
            case GREATER_THAN_OR_EQUAL_ID:
                return fieldValue.floatValue() >= filterValue1.floatValue();
            case LESS_THAN_ID:
                return fieldValue.floatValue() < filterValue1.floatValue();
            case LESS_THAN_OR_EQUAL_ID:
                return fieldValue.floatValue() <= filterValue1.floatValue();
            case BETWEEN_ID: {
                Float filterValue2 = Float.valueOf(filterCriterion.getValue2());
                return filterValue1.floatValue() <= fieldValue.floatValue()
                    && fieldValue.floatValue() <= filterValue2.floatValue();
            }
            default: {
                return false;
            }
            }
        }

        if (field instanceof Double) {
            Double fieldValue = (Double) field;
            Double filterValue1 = Double.valueOf(filterCriterion.getValue1());

            switch (getId()) {
            case EQUAL_ID:
                return fieldValue.doubleValue() == filterValue1.doubleValue();
            case GREATER_THAN_ID:
                return fieldValue.doubleValue() > filterValue1.doubleValue();
            case GREATER_THAN_OR_EQUAL_ID:
                return fieldValue.doubleValue() >= filterValue1.doubleValue();
            case LESS_THAN_ID:
                return fieldValue.doubleValue() < filterValue1.doubleValue();
            case LESS_THAN_OR_EQUAL_ID:
                return fieldValue.doubleValue() <= filterValue1.doubleValue();
            case BETWEEN_ID: {
                Double filterValue2 = Double.valueOf(filterCriterion
                    .getValue2());
                return filterValue1.doubleValue() <= fieldValue.doubleValue()
                    && fieldValue.doubleValue() <= filterValue2.doubleValue();
            }
            default: {
                return false;
            }
            }
        }

        if (field instanceof Byte) {
            Byte fieldValue = (Byte) field;
            Byte filterValue1 = Byte.valueOf(filterCriterion.getValue1());

            switch (getId()) {
            case EQUAL_ID:
                return fieldValue.byteValue() == filterValue1.byteValue();
            case GREATER_THAN_ID:
                return fieldValue.byteValue() > filterValue1.byteValue();
            case GREATER_THAN_OR_EQUAL_ID:
                return fieldValue.byteValue() >= filterValue1.byteValue();
            case LESS_THAN_ID:
                return fieldValue.byteValue() < filterValue1.byteValue();
            case LESS_THAN_OR_EQUAL_ID:
                return fieldValue.byteValue() <= filterValue1.byteValue();
            case BETWEEN_ID: {
                Byte filterValue2 = Byte.valueOf(filterCriterion.getValue2());
                return filterValue1.byteValue() <= fieldValue.byteValue()
                    && fieldValue.byteValue() <= filterValue2.byteValue();
            }
            default: {
                return false;
            }
            }
        }

        // If we get here - we hit a type we didn't expect...
        return false;
    }

    /**
     * This method will implement the with in filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean withIn(DataObject dataObject,
                             Object object,
                             FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        FilterTimeWindow filterTimeWindow = FilterTimeWindow.getById(Integer
            .parseInt(filterCriterion.getValue1()));
        Date filterDate = filterTimeWindow.applyOffset(new Date());

        Locale locale = LocaleContextHolder.getLocale();

        DateFormat dateFormatter = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, DateFormat.LONG, (locale == null ? Locale
                .getDefault() : locale));
        Date testDate = null;
        if (object instanceof Date) {
            testDate = (Date) object;
        } else {
            try {
                testDate = dateFormatter.parse(object.toString());
            } catch (ParseException pe) {
                testDate = new Date(System.currentTimeMillis());
            }
        }
        return testDate.getTime() >= filterDate.getTime();
    }

    /**
     * This method will implement the not with in filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean notWithIn(DataObject dataObject,
                                Object object,
                                FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        FilterTimeWindow filterTimeWindow = FilterTimeWindow.getById(Integer
            .parseInt(filterCriterion.getValue1()));
        Date filterDate = filterTimeWindow.applyOffset(new Date());

        Locale locale = LocaleContextHolder.getLocale();

        DateFormat dateFormatter = DateFormat.getDateTimeInstance(
            DateFormat.SHORT, DateFormat.LONG, (locale == null ? Locale
                .getDefault() : locale));
        Date testDate = null;
        if (object instanceof Date) {
            testDate = (Date) object;
        } else {
            try {
                testDate = dateFormatter.parse(object.toString());
            } catch (ParseException e) {
                testDate = new Date(System.currentTimeMillis());
            }
        }
        return testDate.getTime() < filterDate.getTime();
    }
    
    /**
     * This method will implement the empty filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean empty(DataObject dataObject,
                             Object object,
                             FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
            
        if (object instanceof String) {
            if ((object) == "") {
                return true;
            }
        }
        return false;
    }

    /**
     * This method will implement the equal to filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean equalTo(DataObject dataObject,
                              Object object,
                              FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        ValueBasedEnum<?> valueBasedEnum = (ValueBasedEnum<?>) object;
        Integer filterValue1 = Integer.parseInt(filterCriterion.getValue1());
        return valueBasedEnum.toValue() == filterValue1.intValue();
    }

    /**
     * This method will implement the not equal to filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean notEqualTo(DataObject dataObject,
                                 Object object,
                                 FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        ValueBasedEnum<?> valueBasedEnum = (ValueBasedEnum<?>) object;
        Integer filterValue1 = Integer.parseInt(filterCriterion.getValue1());
        return valueBasedEnum.toValue() != filterValue1.intValue();
    }

    /**
     * This method will implement the equal for boolean filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean equalToBoolean(DataObject dataObject,
                                     Object object,
                                     FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        Boolean testBoolean = (Boolean) object;
        return testBoolean.equals(Boolean.valueOf(filterCriterion.getValue1()));
    }

    /**
     * This method will implement the not equal for boolean filter comparison.
     * @param dataObject the object to test filtering on
     * @param object the variable to test filtering on
     * @param filterCriterion the filter criteria to use
     * @return boolean - is this object filtered or not
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    protected boolean notEqualToBoolean(DataObject dataObject,
                                        Object object,
                                        FilterCriterion filterCriterion)
        throws InvocationTargetException, IllegalAccessException,
        NoSuchMethodException {
        Boolean testBoolean = (Boolean) object;
        return !testBoolean
            .equals(Boolean.valueOf(filterCriterion.getValue1()));
    }

    /**
     * The display functions are used if applicable.
     * @param du the display utilities
     * @param methodName the method name for displaying this field
     * @param fieldVal the field value
     * @param dataObject the data object this applies to
     * @return the displayed value, or null if there is no display method
     */
    private Object getDisplayedText(DisplayUtilities du,
                                    String methodName,
                                    Object fieldVal,
                                    DataObject dataObject) {
        Object obj = null;
        try {
            if ((du != null) && (methodName != null)) {
                Method m = du.getClass().getMethod(
                    methodName, new Class[] { Object.class, DataObject.class });
                obj = m.invoke(du, fieldVal, dataObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 