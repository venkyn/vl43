/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.logging.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * @author I18NM: Masaki Itagaki
 *
 * This singleton class provides a default encoding name used in converting byte arrays
 * into String. The encoding must match with actual file data stored in the database.
 * Such files include a task file and firmware file.
 */
public final class I18NData {

    private static final Logger log = new Logger(I18NData.class);

    // This system's default language code for VOS readin:
    private String voslanguage;

    // This system's default countrycode for VOS reading:
    private String voscountry;

    // This system's default encoding:
    private String encoding;

    // Language locale for VOS date:
    private String voslocale;

    // Unit of Measure - minute:
    private String uomMinute;

    // Unit of Measure - Hour:
    private String uomHour;

    // Unit of Measure - Day(s):
    private String uomDay;

    // This instance
    private static I18NData instance;

    /**
     * The constructor reads I18N property file to set this object's members.
     */
    private I18NData() {
        try {
            //Read the resource bundle file.
            ResourceBundle rb = ResourceBundle.getBundle("com.vocollect.voiceconsole.common.util.i18nConstant");
            encoding = rb.getString("default.encoding");
            voscountry = rb.getString("default.voscountry");
            voslanguage = rb.getString("default.voslanguage");
        } catch (MissingResourceException e) {
            //Default values:
            encoding = "UTF-8";
            voscountry = "us";
            voslanguage = "en";
            if (log.isDebugEnabled()) {
                log.debug("I18N.properties was not found.");
            }
        }

        try {
            //Read the Unit of Measure resource bundle file.
            ResourceBundle uomRb = ResourceBundle.getBundle("com.vocollect.voiceconsole.common.util.l10nResources");
            uomMinute = uomRb.getString("uom.min");
            uomHour = uomRb.getString("uom.hour");
            uomDay = uomRb.getString("uom.day");
        } catch (MissingResourceException e) {
            //Default values:
            uomMinute = "m";
            uomHour = "h";
            uomDay = "d";
            if (log.isDebugEnabled()) {
                log.debug("l10nResources.properties was not found.");
            }
        }

    }
    /**
     * Return a singleton object of I18NData.
     * @return I18NData
     */
    public static I18NData getInstance() {
        if (instance == null) {
            instance = new I18NData();
        }
        return instance;
    }

    /**
     * Return an assigned default encoding specified in I18N.properties.
     * @return String: Encoding name
     */
    public String getDefEncoding() {
        return instance.encoding;
    }

    /**
     * Return an assigned default encoding specified in I18N.properties.
     * @return String: Encoding name
     */
    public String getVosCountryCode() {
        return instance.voscountry;
    }

    /**
     * Return an assigned default encoding specified in I18N.properties.
     * @return String: Encoding name
     */
    public String getVosLanguageCode() {
        return instance.voslanguage;
    }

    /**
     * Return a language name to be used in reading date in VOS file.
     * @return String: Language name
     */
    public String getVOSFileDateLocale() {
        return instance.voslocale;
    }

    /**
     * Return the minute locale resource value.
     * @return String: Language value
     */
    public String getUOMMin() {
        return instance.uomMinute;
    }

    /**
     * Return the hour locale resource value.
     * @return String: Language value
     */
    public String getUOMHour() {
        return instance.uomHour;
    }

    /**
     * Return the day locale resource value.
     * @return String: Language value
     */
    public String getUOMDay() {
        return instance.uomDay;
    }

}


*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 