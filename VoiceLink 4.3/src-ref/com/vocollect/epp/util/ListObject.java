/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;


/**
 *
 *
 * @author ??
 */
public class ListObject {
    private Long id;
    private String value;

    /**
     * Constructor.
     * @param id the id of the object
     * @param value the value of the object
     */
    public ListObject(long id, String value) {
        this.id = id;
        this.value = value;
    }

    /**
     * @return the id property.
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the value property.
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value property.
     */
    public void setValue(String value) {
        this.value = value;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 