/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.service.SystemPropertyManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * This class provides .zip file creation and parsing capabilities.
 * @author J Kercher
 */
public class Zipper {

    /** Buffer size for reading bytes from an IOStream. */
    static final int BUFFER_SIZE = 2048;

    static final int LARGE_BUFFER_SIZE = 1048576;

    static final String FILE_BASE_DIR = "FILE_BASE_DIR";

    private static SystemPropertyManager propertyManager;

    /**
     * @param pm .
     */
    public void setSystemPropertyManager(SystemPropertyManager pm) {
        propertyManager = pm;
    }

    /**
     * @return propertyManager
     */
    public static SystemPropertyManager getPropertyManager() {
        return propertyManager;
    }

    /**
     * Extract and return all files found in the given zip data.
     * @param fileVO FileVO represents a .zip file contents
     * @return FileVO[] array of value objects representing the file data found
     *         in the given zipped data
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil[] unzip(FileReferenceUtil fileVO)
        throws VocollectException {

        // Validate parameters.
        if (fileVO == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."));
        }

        // Create a ByteArrayInputStream from the zip file's contents and
        // use it to instantiate a ZipInputStream.
        try {
            FileInputStream inStreamByte = new FileInputStream(fileVO
                .getZipFileName());
            ZipInputStream inStreamZip = new ZipInputStream(inStreamByte);

            try {
                return unzip(inStreamZip);
            } catch (IOException io) {
                // Error occured during i/o operations or bad zip data.
                throw new VocollectException(
                    ZipUtilErrorCode.IO_EXCEPTION_UNZIP_FILE, new UserMessage(
                        "I/O error encountered while unzipping '"
                            + fileVO.getName() + "'."), io);
            } finally {
                // Clean up.
                try {
                    inStreamZip.close();
                    inStreamByte.close();
                } catch (Exception ignored) {
                    // This can safely be ignored.
                }
            }
        } catch (FileNotFoundException fnfe) {
            throw new VocollectException(fnfe);
        }
    }

    /**
     * Extract and return all files found in the given zip data.
     * @param fileVO FileVO represents a .zip file contents
     * @return FileVO[] array of value objects representing the file data found
     *         in the given zipped data
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil[] unzip(FileUtil fileVO) throws VocollectException {

        // Validate parameters.
        if (fileVO == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."),
                new InvalidParameterException());
        }

        // Create a ByteArrayInputStream from the zip file's contents and
        // use it to instantiate a ZipInputStream.
        ByteArrayInputStream inStreamByte = new ByteArrayInputStream(fileVO
            .getContents());
        ZipInputStream inStreamZip = new ZipInputStream(inStreamByte);

        try {
            return unzip(inStreamZip);
        } catch (IOException io) {
            // Error occured during i/o operations or bad zip data.
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "I/O error encountered while unzipping '"
                        + fileVO.getName() + "'."), io);
        } finally {
            // Clean up.
            try {
                inStreamZip.close();
                inStreamByte.close();
            } catch (Exception ignored) {
                // This can safely be ignored.
            }
        }
    }

    /**
     * Extract and return all files found in the given zip data.
     * @param stream An input stream that is assumed to be tied to .zip file
     *            This method does not close the stream because it did not
     *            create it.
     * @return FileVO[] array of value objects representing the file data found
     *         in the given zipped data
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil[] unzip(InputStream stream)
        throws VocollectException {

        try {
            // Wrap the stream with a zip stream and go to common code.
            return unzip(new ZipInputStream(stream));
        } catch (IOException io) {
            // Error occured during i/o operations or bad zip data.
            throw new VocollectException(io);
        }
    }

    /**
     * Extract and return all files found in the given zip data.
     * @param zipStream A zip file input stream. This method does not close the
     *            stream since it did not create it.
     * @return FileVO[] array of value objects representing the file data found
     *         in the given zipped data
     * @throws IOException if a problem is encountered while unzipping the given
     *             zipped data
     * @throws VocollectException for other errors.
     */
    public static FileUtil[] unzip(ZipInputStream zipStream)
        throws VocollectException, IOException {

        validateParameters(zipStream);

        // Create an ArrayList to store extracted FileVOs.
        ArrayList<FileUtil> arrayList = new ArrayList<FileUtil>();

        // Iterate over all zip entries.
        ZipEntry entry = zipStream.getNextEntry();

        if (entry == null) {
            // This is not a zip file, or it is empty...
            // make the ManageTasksBean aware of the error.
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Zip file empty or not a zip"));
        }

        while (entry != null) {
            // Ignore directories.
            if (!entry.isDirectory()) {
                if (entry.getName() == null) {
                    // Should never happen...
                    throw new VocollectException(
                        ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP,
                        new UserMessage("Zip entry has no name in "
                            + "unzip method."), new InvalidParameterException());
                }
                // Create a VO for this entry and set its name and type.
                FileUtil unzippedVO = new FileUtil();
                unzippedVO.setName(entry.getName());
                // Set the file's contents.
                unzippedVO.setContents(readZipEntry(zipStream));
                arrayList.add(unzippedVO);
                entry = zipStream.getNextEntry();
            } else {
                entry = zipStream.getNextEntry();
            }
        }

        // Return the ArrayList as a typed array.
        return arrayList.toArray(new FileUtil[0]);
    }

    /**
     * Extract and return the named file entries found in the given zip data.
     * @param fileVO .
     * @param entryNames .
     * @return Map of entry names to file reference utils
     * @throws VocollectException
     */
    public static Map<String, FileReferenceUtil> unzip(FileReferenceUtil fileVO,
                                                       List<String> entryNames)
        throws VocollectException {

        //TODO: Most of this code is similar to the unzip method below.  We
        //      Can probably break some, if not most, of it out into another method.
        
        Map<String, FileReferenceUtil> fileRefs = new HashMap<String, FileReferenceUtil>();
        
        // Validate parameters.
        if (fileVO == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."),
                new InvalidParameterException());
        }

        String firmwareBaseDir = getPropertyManager().findByName(FILE_BASE_DIR)
            .getValue()
            + File.separator;

        // Create a ByteArrayInputStream from the zip file's contents and
        // use it to instantiate a ZipInputStream.
        try {
            FileInputStream inStreamByte = new FileInputStream(firmwareBaseDir
                + fileVO.getZipFileName());
            ZipInputStream zipStream = new ZipInputStream(inStreamByte);

            try {
             // Iterate over all zip entries.
                for (ZipEntry entry = zipStream.getNextEntry(); entry != null; entry = zipStream
                    .getNextEntry()) {
                    // Ignore directories.
                    if (!entry.isDirectory()) {
                        if (entryNames.contains(entry.getName())) {
                            // Create a VO for this entry and set its name and
                            // type.
                            FileReferenceUtil unzippedVO = new FileReferenceUtil();
                            unzippedVO.setName(entry.getName());
                            // Set the file's contents.
                            unzippedVO.setZipFileName(fileVO.getZipFileName());
                            // unzippedVO.setFileReference(copyZipEntry(zipStream,
                            // entryName, firmwareBaseDir));
                            // unzippedVO.setContents(readZipEntry(zipStream));
                            fileRefs.put(entry.getName(), unzippedVO);
                        }
                    }
                }
            } catch (IOException io) {
                // Error occured during i/o operations or bad zip data.
                throw new VocollectException(
                    ZipUtilErrorCode.IO_EXCEPTION_UNZIP_FILE, new UserMessage(
                        "I/O error encountered while unzipping '"
                            + fileVO.getName() + "'."), io);
            } finally {
                // Clean up.
                try {
                    zipStream.close();
                    inStreamByte.close();
                } catch (Exception ignored) {
                    // This can safely be ignored.
                }
            }
        } catch (FileNotFoundException fnfe) {
            throw new VocollectException(fnfe);
        }
        
        return fileRefs;
    }
    
    /**
     * Extract and return the named file entry found in the given zip data.
     * @param fileVO FileVO represents a .zip file contents
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileReferenceUtil unzip(FileReferenceUtil fileVO,
                                          String entryName)
        throws VocollectException {

        // Validate parameters.
        if (fileVO == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."),
                new InvalidParameterException());
        }

        String firmwareBaseDir = getPropertyManager().findByName(FILE_BASE_DIR)
            .getValue()
            + File.separator;

        // Create a ByteArrayInputStream from the zip file's contents and
        // use it to instantiate a ZipInputStream.
        try {
            FileInputStream inStreamByte = new FileInputStream(firmwareBaseDir
                + fileVO.getZipFileName());
            ZipInputStream zipStream = new ZipInputStream(inStreamByte);

            try {
                // Iterate over all zip entries.
                for (ZipEntry entry = zipStream.getNextEntry(); entry != null; entry = zipStream
                    .getNextEntry()) {
                    // Ignore directories.
                    if (!entry.isDirectory()) {
                        if (entry.getName().equals(entryName)) {
                            // Create a VO for this entry and set its name and
                            // type.
                            FileReferenceUtil unzippedVO = new FileReferenceUtil();
                            unzippedVO.setName(entry.getName());
                            // Set the file's contents.
                            unzippedVO.setZipFileName(fileVO.getZipFileName());
                            // unzippedVO.setFileReference(copyZipEntry(zipStream,
                            // entryName, firmwareBaseDir));
                            // unzippedVO.setContents(readZipEntry(zipStream));
                            return unzippedVO;
                        }
                    }
                }

                return null;
            } catch (IOException io) {
                // Error occured during i/o operations or bad zip data.
                throw new VocollectException(
                    ZipUtilErrorCode.IO_EXCEPTION_UNZIP_FILE, new UserMessage(
                        "I/O error encountered while unzipping '"
                            + fileVO.getName() + "'."), io);
            } finally {
                // Clean up.
                try {
                    zipStream.close();
                    inStreamByte.close();
                } catch (Exception ignored) {
                    // This can safely be ignored.
                }
            }
        } catch (FileNotFoundException fnfe) {
            throw new VocollectException(fnfe);
        }
    }

    /**
     * Extract and return the named file entry found in the given zip data.
     * @param fileVO FileVO represents a .zip file contents
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil unzip(FileUtil fileVO, String entryName)
        throws VocollectException {

        // Validate parameters.
        if (fileVO == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."),
                new InvalidParameterException());
        }

        // Create a ByteArrayInputStream from the zip file's contents and
        // use it to instantiate a ZipInputStream.
        ByteArrayInputStream inStreamByte = new ByteArrayInputStream(fileVO
            .getContents());
        ZipInputStream inStreamZip = new ZipInputStream(inStreamByte);

        try {
            return unzip(inStreamZip, entryName);
        } catch (IOException io) {
            // Error occured during i/o operations or bad zip data.
            throw new VocollectException(
                ZipUtilErrorCode.IO_EXCEPTION_UNZIP_FILE, new UserMessage(
                    "I/O error encountered while unzipping '"
                        + fileVO.getName() + "'."), io);
        } finally {
            // Clean up.
            try {
                inStreamZip.close();
                inStreamByte.close();
            } catch (Exception ignored) {
                // This can safely be ignored.
            }
        }
    }
    
    /**
     * Extract and return the named file entry found in the given zip data, ignoring
     * the file's path in the zip's file system.
     * @param stream An input stream that is assumed to be tied to .zip file
     *            This method does not close the stream because it did not
     *            create it.
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil unzip(InputStream stream, String entryName) 
        throws VocollectException {
        return unzip(stream, entryName, false);
    }
    
    /**
     * Extract the given map of file keys found in the given zip data and create
     * FileUtil objects for the values, ignoring the file's path in the zip's 
     * file system.
     * @param stream An input stream that is assumed to be tied to .zip file
     *            This method does not close the stream because it did not
     *            create it.
     * @param files Map of files with a file name as the key and a FileUtil object
     *            as the value.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static void unzip(InputStream stream, Map files) 
        throws VocollectException {
        unzip(stream, files, false);
    }

    /**
     * Extract and return the named file entry found in the given zip data.
     * @param stream An input stream that is assumed to be tied to .zip file
     *            This method does not close the stream because it did not
     *            create it.
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @param pathSpecific false if entry paths should be ignored.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static FileUtil unzip(InputStream stream, String entryName, boolean pathSpecific)
        throws VocollectException {

        // Wrap the stream with a zip stream and go to common code.
        try {
            return unzip(new ZipInputStream(stream), entryName, pathSpecific);
        } catch (IOException io) {
            // Error occured during i/o operations or bad zip data.
            throw new VocollectException(io);
        }
    }
    
    /**
     * Extract and assign FileUtil objects to the Map of filenames found in the
     * given InputStream.
     * @param stream An input stream that is assumed to be tied to .zip file
     *            This method does not close the stream because it did not
     *            create it.
     * @param files Map of files with the filename as the key, an a FileUtil object
     *            as the value.
     * @param pathSpecific false if entry paths should be ignored.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws VocollectException if a problem is encountered while unzipping
     *             the given zipped data
     */
    public static void unzip(InputStream stream, Map<String, FileUtil> files, boolean pathSpecific)
        throws VocollectException {
    
        // Wrap the stream with a zip stream and go to common code.
        try {
            unzip(new ZipInputStream(stream), files, pathSpecific);
        } catch (IOException io) {
            // Error occured during i/o operations or bad zip data.
            throw new VocollectException(io);
        }
    }

    /**
     * @param zipStream .
     * @param entryName .
     * @return .
     * @throws VocollectException
     * @throws IOException
     */
    public static InputStream positionZipStream(ZipInputStream zipStream,
                                                String entryName)
        throws VocollectException, IOException {

        validateParameters(zipStream);
        if (entryName.indexOf("/") != -1) {
            entryName = entryName.substring(entryName.lastIndexOf("/") + 1);
        }

        // Iterate over all zip entries.
        for (ZipEntry entry = zipStream.getNextEntry(); entry != null; entry = zipStream
            .getNextEntry()) {
            // Ignore directories.
            if (!entry.isDirectory()) {
                // Only want filename, not directory structure
                String name = entry.getName();
                if (name.indexOf("/") != -1) {
                    name = name.substring(entry.getName().lastIndexOf("/") + 1);
                }
                if (name.equals(entryName)) {
                    return zipStream;
                }
            }
        }

        return null;
    }
    
    /**
     * Extract and return the named file entry found in the given zip data.
     * @param zipStream A zip file input stream. This method does not close the
     *            stream since it did not create it.
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws IOException if a problem is encountered while unzipping the given
     *             zipped data
     * @throws VocollectException for other errors.
     */
    public static FileUtil unzip(ZipInputStream zipStream, String entryName)
        throws VocollectException, IOException {
        return unzip(zipStream, entryName, false);
    }

    /**
     * Extract and return the named file entry found in the given zip data.
     * @param zipStream A zip file input stream. This method does not close the
     *            stream since it did not create it.
     * @param entryName the name of the zip entry we want unzipped and returned.
     * @param pathSpecific false if entry path should be ignored.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws IOException if a problem is encountered while unzipping the given
     *             zipped data
     * @throws VocollectException for other errors.
     */
    public static FileUtil unzip(ZipInputStream zipStream, String entryName, boolean pathSpecific)
        throws VocollectException, IOException {

        ZipEntry entry = getEntry(zipStream, entryName, pathSpecific);
        FileUtil unzippedVO = null;

        if (entry != null) {
            unzippedVO = new FileUtil();
            unzippedVO.setName(entry.getName());
            // Set the file's contents.
            unzippedVO.setContents(readZipEntry(zipStream));
        }

        return unzippedVO;
    }
    
    /**
     * Extract and assign FileUtil objects to the Map of filenames found in the
     * given ZipInputStream.
     * @param zipStream A zip file input stream. This method does not close the
     *            stream since it did not create it.
     * @param files map 
     * @param pathSpecific false if entry path should be ignored.
     * @return FileVO representing the requested file, or null if no such entry
     *         was found.
     * @throws IOException if a problem is encountered while unzipping the given
     *             zipped data
     * @throws VocollectException for other errors.
     */
    public static void unzip(ZipInputStream zipStream, Map<String, FileUtil> files, boolean pathSpecific)
        throws VocollectException, IOException {
        
        ZipEntry whatIFound = null;
        validateParameters(zipStream);
        
        // Iterate over all zip entries.
        ZipEntry entry = zipStream.getNextEntry();
        while (entry != null && whatIFound == null) {
            
            whatIFound = checkAllFileEntries(
                zipStream, files, pathSpecific, entry);
    
            entry = zipStream.getNextEntry();
            
        }
    }

    /**
     * Will return the ZipEntry associated with the entryName or null if not
     * found. Has a side effect of posiitoning the ZipInputStream to the correct
     * entry (so you can read from the InputStream and get the file).
     * 
     * @param zipStream .
     * @param entryName .
     * @param pathSpecific false if entry path should be ignored
     * @return .
     * @throws VocollectException
     * @throws IOException
     */
    private static ZipEntry getEntry(ZipInputStream zipStream, String entryName, boolean pathSpecific)
        throws VocollectException, IOException {

        ZipEntry whatIFound = null;
        validateParameters(zipStream);

        // Iterate over all zip entries.
        ZipEntry entry = zipStream.getNextEntry();
        while (entry != null && whatIFound == null) {
            whatIFound = getEntryIfNamesMatch(entryName, pathSpecific, entry);

            if (whatIFound == null) {
                entry = zipStream.getNextEntry();
            }
        }

        return whatIFound;
    }
    
    /**
     * @param zipStream
     * @param files
     * @param pathSpecific
     * @param whatIFound
     * @param entry
     * @return
     * @throws IOException
     */
    private static ZipEntry checkAllFileEntries(ZipInputStream zipStream,
                                                Map<String, FileUtil> files,
                                                boolean pathSpecific,
                                                ZipEntry entry)
        throws IOException {
        
        ZipEntry whatIFound = null;
        String entryName;
        FileUtil unzippedVO;
        
        // Iterate over each entry in the files map
        Iterator it = files.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry)it.next();
            entryName = (String) pairs.getKey();
            
            whatIFound = getEntryIfNamesMatch(entryName, pathSpecific, entry);
            if (whatIFound != null) {
                break;
            }
        }
        
        unzippedVO = null;
        
        if (whatIFound != null) {
            unzippedVO = new FileUtil();
            unzippedVO.setName(whatIFound.getName());
            // Set the file's contents.
            unzippedVO.setContents(readZipEntry(zipStream));
            files.put(whatIFound.getName(), unzippedVO);
            whatIFound = null;
        }
        return whatIFound;
    }    

    /**
     * @param entryName
     * @param pathSpecific
     * @param entry
     * @return
     */
    private static ZipEntry getEntryIfNamesMatch(String entryName,
                                         boolean pathSpecific,
                                         ZipEntry entry) {
        ZipEntry whatIFound;
        String actualFileName = getActualFileName(pathSpecific, entryName);
        whatIFound = compareFileNames(
            actualFileName, entry, pathSpecific);
        return whatIFound;
    }
    
    /**
     * @param zipStream
     * @throws VocollectException
     */
    private static void validateParameters(ZipInputStream zipStream)
        throws VocollectException {
        if (zipStream == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to unzip method."),
                new InvalidParameterException());
        }
    }

    /**
     * @param pathSpecific
     * @param whatIFound
     * @param entry
     * @param entryName
     * @return
     */
    private static ZipEntry compareFileNames(String entryName,
                                         ZipEntry entry,
                                         boolean pathSpecific) {
        ZipEntry whatIFound = null;
        
        // Ignore directories.
        if (!entry.isDirectory()) {
            String name = entry.getName();
   
            if (!pathSpecific) {
                if (name.indexOf("/") != -1) {
                    name = name.substring(entry.getName().lastIndexOf("/") + 1);
                }
            }
   
            if (name.equals(entryName)) {
                whatIFound = entry;
            }
        }
        return whatIFound;
    }

    /**
     * @param pathSpecific
     * @param entryName
     * @return
     */
    private static String getActualFileName(boolean pathSpecific,
                                            String entryName) {
        if (!pathSpecific) {
            if (entryName.indexOf("/") != -1) {
                entryName = entryName.substring(entryName.lastIndexOf("/") + 1);
            }
        }
        return entryName;
    }    
 
    /**
     * Return a zipped representation of the given files.
     * @param fileVOs FileVO[] represents the file data to be zipped
     * @param name String specifies the name of the new zip file
     * @return FileVO (of type ZIP) represents the zipped files
     * @throws VocollectException if an error is encountered while zipping the
     *             given files.
     */
    public static FileUtil zip(FileUtil[] fileVOs, String name)
        throws VocollectException {
    	return zip(fileVOs, name, null);
     }

    /**
     * Return a zipped representation of the given files, clearing each entry's created-time.
     * This allows creation of a zip file from database entries which will show a consistent MD5 
     * across generations (given that the content is the same). Otherwise, the entry's creation time is the 
     * "current" time & date. VVC-2422.
     * @param fileVOs FileVO[] represents the file data to be zipped
     * @param name String specifies the name of the new zip file
     * @return FileVO (of type ZIP) represents the zipped files
     * @throws VocollectException if an error is encountered while zipping the
     *             given files.
     */
    public static FileUtil zip(FileUtil[] fileVOs, String name, Long timeStamp)
        throws VocollectException {

        // Validate parameter.
        if (fileVOs == null) {
            throw new VocollectException(
                ZipUtilErrorCode.NULL_ARGUMENT_FOR_UNZIP, new UserMessage(
                    "Null argument passed " + "to zip method."),
                new InvalidParameterException());
        }

        // Initialize the returned VO.
        FileUtil zippedVO = new FileUtil();
        zippedVO.setName(name);


        // Prepare the output streams for the zipping.
        ByteArrayOutputStream outStreamByte = new ByteArrayOutputStream();
        ZipOutputStream outStreamZip = new ZipOutputStream(outStreamByte);
        outStreamZip.setMethod(ZipOutputStream.DEFLATED);


        try {
            // Loop through the files to be zipped.
            for (int i = 0; i < fileVOs.length; i++) {
                // Ignore any null files in the array.
                if (fileVOs[i] != null) {
                    // Create a zip entry for this file.
                    ZipEntry entry = new ZipEntry(fileVOs[i].getName());
                    if (null != timeStamp) {
                    	entry.setTime(timeStamp);
                    }
                    // Set the uncompressed data size.
                    byte[] data = fileVOs[i].getContents();
                    entry.setSize(data.length);
                    // Place the entry in the zip stream (and close the last
                    // entry if it exists).
                    outStreamZip.putNextEntry(entry);
                    // Write the entry's contents.
                    outStreamZip.write(data, 0, data.length);

                }
            }
            // Close last entry.
            outStreamZip.closeEntry();
        } catch (IOException io) {
            // Error occurred during i/o operations.
            throw new VocollectException(
                ZipUtilErrorCode.IO_EXCEPTION_UNZIP_FILE, new UserMessage(
                    "I/O error encountered while creating '" + name
                        + "' in zip method."), io);
        } finally {
            // Cleanup.
            try {
                outStreamZip.close();
                outStreamByte.close();
            } catch (Exception ignored) {
                // This can safetly be ignored.
            }
        }
        // Save the zipped data.
        zippedVO.setContents(outStreamByte.toByteArray());

        return zippedVO;
    }

    /**
     * Extract the current ZipEntry's data from the given ZipInputStream. The
     * input stream must be open and pointing to a valid ZipEntry.
     * @param inStream ZipInputStream provides the data source for extraction
     * @return byte[] containing the extracted data
     * @throws IOException if an error occurs while extracting the data. This
     *             may also be of type ZipException.
     */
    private static byte[] readZipEntry(ZipInputStream inStream)
        throws IOException {

        int bytesRead = 0;
        byte[] data = new byte[BUFFER_SIZE];

        // Unzip the contents and write it to an output buffer.
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        while ((bytesRead = inStream.read(data, 0, BUFFER_SIZE)) != -1) {
            outStream.write(data, 0, bytesRead);
        }

        // Save the data before closing the output buffer.
        outStream.close();
        data = outStream.toByteArray();

        return data;
    }

    /**
     * Extract the current ZipEntry's data from the given ZipInputStream. The
     * input stream must be open and pointing to a valid ZipEntry.
     * @param inStream ZipInputStream provides the data source for extraction
     * @return byte[] containing the extracted data
     * @throws IOException if an error occurs while extracting the data. This
     *             may also be of type ZipException.
     */
    public static byte[] readEntry(InputStream inStream)
        throws IOException {

        int bytesRead = 0;
        byte[] data = new byte[BUFFER_SIZE];

        // Unzip the contents and write it to an output buffer.
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        while ((bytesRead = inStream.read(data, 0, BUFFER_SIZE)) != -1) {
            outStream.write(data, 0, bytesRead);
        }

        // Save the data before closing the output buffer.
        outStream.close();
        data = outStream.toByteArray();

        return data;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 