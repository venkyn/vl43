/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Error codes for file ZIP operations.
 *
 * @author 
 */
public final class ZipUtilErrorCode extends ErrorCode {

    /**
     * Upper boundary of the EPP scheduling error range.
     */
    public static final long UPPER_BOUND = 6199;

    /**
     * Lower boundary of the EPP scheduling error range.
     */
    public static final long LOWER_BOUND = 6000;

    /**
     * Constructor.
     */
    private ZipUtilErrorCode() {
        super("EPP", LOWER_BOUND, UPPER_BOUND);
    }

    /** 
     * Perform base initialization.
     */
    public static final ZipUtilErrorCode NO_ERROR = new ZipUtilErrorCode();

    /**
     * When unzipping a FileVO.
     */
    public static final ZipUtilErrorCode NULL_ARGUMENT_FOR_UNZIP = 
        new ZipUtilErrorCode(6000);

    public static final ZipUtilErrorCode IO_EXCEPTION_UNZIP_FILE = 
        new ZipUtilErrorCode(6001);

    /**
     * Create a custom error code using the given value.
     * @param err - error to be logged
     */
    private ZipUtilErrorCode(long err) {
        super(ZipUtilErrorCode.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 