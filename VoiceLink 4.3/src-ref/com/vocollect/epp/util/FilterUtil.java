/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.View;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Utility methods for filtering.
 * @author brupert
 */
public class FilterUtil {
    
    /**
     * Protected contstructor for util class.
     */
    protected FilterUtil() {
    }

    /**
     * Append a filter criterion to the specified list of filters. If no filters
     * match the given view, a new <code>Filter</code> is added to the list with
     * the passed <code>FilterCriterion</code>.
     * @param filterList The list of filters to append to.
     * @param view The view to append the filter to.
     * @param fc The filter criterion that is being appended.
     */
    public static void appendFilterToView(List<Filter> filterList, View view, FilterCriterion fc) {
        for (Filter f : filterList) {
            if (f != null && f.getView().getId().equals(view.getId())) {
                f.getFilterCriteria().add(fc);
                return;
            }
        }
        Filter filter = new Filter();
        filter.setView(view);
        if (filter.getFilterCriteria() == null) {
            filter.setFilterCriteria(new ArrayList<FilterCriterion>());
        }
        filter.getFilterCriteria().add(fc);
        filterList.add(filter);
    }
    
    /**
     * Removes all filters from the specified column.
     * @param filterList The user's list of filters.
     * @param columnId The ID of the column to remove filters from.
     */
    public static void removeFiltersFromColumn(List<Filter> filterList, Long columnId) {
        for (Filter filter : filterList) {
            if (filter != null) {
                // user an iterator here to access the remove function.
                Iterator<FilterCriterion> i = filter.getFilterCriteria().iterator();
                while (i.hasNext()) {
                    FilterCriterion fc = i.next();
                    if (fc.getColumn().getId().equals(columnId)) {
                        i.remove();
                    }
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 