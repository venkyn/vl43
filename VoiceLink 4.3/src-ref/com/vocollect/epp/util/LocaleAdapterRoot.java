/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.SystemTranslationManager;

import java.text.CollationKey;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.i18n.LocaleContextHolder;


/**
 * This class is an adapter that allows for adjustment of a specified
 * Locale to a form that is supported by the current implementation.
 * Since the set of supported languages is a likely customization point,
 * this is a root class to allow for easy extension.
 *
 * @author ddoubleday
 */
public class LocaleAdapterRoot implements BeanFactoryPostProcessor {

    private static final Logger log = new Logger(LocaleAdapter.class);

    // Map of the supported Locales, keyed by the Locale string,
    // mapped to the Locale object.
    private static Map<String, Locale> supportedLocales = 
        new HashMap<String, Locale>();
    
    // Create a singleton instance for (possibly customized) locale adjustment.
    // The reason this is not just done by a static method is to allow for
    // customization.
    private static LocaleAdapter localeAdapterInstance;
    
    /**
     * The default Locale when all else fails.
     */
    public static final Locale DEFAULT_LOCALE = Locale.US;
    
    /**
     * Comparator that compares Locales by displayName in the current
     * Locale context.
     */
    public static final Comparator<Locale> LOCALE_COMPARATOR =
        new Comparator<Locale>() {
            public int compare(Locale l1, Locale l2) {
                
                CollationKey key1 = StringUtil.getPrimaryCollator().getCollationKey(
                    l1.getDisplayName(LocaleContextHolder.getLocale()));
                CollationKey key2 = StringUtil.getPrimaryCollator().getCollationKey(
                    l2.getDisplayName(LocaleContextHolder.getLocale()));
                    
                 return key1.compareTo(key2);
            }
        };

    private SystemTranslationManager systemTranslationManager;
    
    /**
     * Set the list of supported locales in the local cache. This is
     * set once in the <code>ApplicationStateListener</code>.
     * @param locales the list of supported Locales
     */
    public static void setSupportedLocales(List<Locale> locales) {
        supportedLocales.clear();
        for (Locale locale : locales) {
            supportedLocales.put(locale.toString(), locale);
        }
    }
    
    /**
     * @param localeString the locale representation to test, in lc_CC format,
     * as returned by <code>Locale.toString()</code>
     * @return true if supported, false otherwise.
     */
    public static boolean isSupportedLocale(String localeString) {
        return supportedLocales.containsKey(localeString);
    }
    
    /**
     * Return the a locale object based on a ISO string reprenstation of locale.
     * 
     * @param localeString ISO formated string
     * @return A locale object representing locale string.
     */
    public static Locale makeLocaleFromString(String localeString) {
        String[] tokens = localeString.split("_");
        Locale locale = null;
        
        if (StringUtil.isNullOrEmpty(localeString)) {
            throw new IllegalArgumentException();
        }
        
        if (tokens.length >= 2) {
            locale = new Locale(tokens[0], tokens[1]);
        } else if (tokens.length == 1) {
            locale = new Locale(tokens[0]);
        }
      
        return locale;
    }

    /**
     * Check to ensure that the specified Locale is supported. If not,
     * return the closest fallback Locale. 
     * @param locale the locale to check and potentially adjust
     * @return the original Locale if it is supported, or a supported
     * fallback Locale.
     */
    public static Locale adjustLocale(Locale locale) {
        if (locale == null) {
            return adjustLocale((String) null);
        } else {
            return adjustLocale(locale.toString());
        }
    }

    /**
     * Check to ensure that the specified Locale is supported. If not,
     * return the closest fallback Locale. This handles the special case
     * in which all Spanish Locales other than "es", "es_ES", or "es_MX"
     * are converted to "es_MX".
     * @param localeStringRep the String representation of the locale to 
     * check and potentially adjust
     * @return the specified Locale if it is supported, or a supported
     * fallback Locale.
     */
    public static Locale adjustLocale(String localeStringRep) {
        return localeAdapterInstance.
            convertLocaleToSupported(localeStringRep);
    }
    
    /**
     * Check to ensure that the specified Locale is supported. If not,
     * return the closest fallback Locale. This handles the special case
     * in which all Spanish Locales other than "es", "es_ES", or "es_MX"
     * are converted to "es_MX".
     * @param localeStringRep the String representation of the locale to 
     * check and potentially adjust
     * @return the specified Locale if it is supported, or a supported
     * fallback Locale.
     */
    public Locale convertLocaleToSupported(String localeStringRep) {
        
        // Handle null value by returning default Locale.
        if (localeStringRep == null) {
            return DEFAULT_LOCALE;
        }
        
        String localeString = localeStringRep;
        
        if (LocaleAdapter.isSupportedLocale(localeString)) {
            return LocaleAdapter.makeLocaleFromString(localeString);
        } else {
            String[] tokens = localeString.split("_");
            if (tokens.length > 1) {
                // Has country code. Check for special case of Spanish with
                // country code other than Spain or Mexico. (Mexico should
                // be supported already and thus never get here, but just
                // in case a customization removes it, the extra test prevents
                // endless recursion...
                if (tokens[0].equals("es") 
                    && !(tokens[1].equals("ES") || tokens[1].equals("MX"))) {
                    // This is Spanish plus some other country than Spain,
                    // use Spanish (Mexico), call recursively to handle
                    // case where Mexico might not be supported.
                    if (log.isInfoEnabled()) {
                        log.info("Locale " + localeStringRep 
                            + " not supported, converted to fallback locale es_MX");
                    }
                    return adjustLocale("es_MX");
                }
            }
                
            // Now try falling back to language code only.
            localeString = tokens[0];
            
            // Fix for VVC-2364, backward compat mode for Norwegian.
            if (localeString.equals("nn") || localeString.equals("nb")) {
                localeString = "no";
            }
            
            Locale newLocale = null;
            if (LocaleAdapter.isSupportedLocale(localeString)) {
                newLocale = supportedLocales.get(localeString);
                if (log.isInfoEnabled()) {
                    log.info("Locale " + localeStringRep 
                        + " not supported, converted to fallback locale " 
                        + newLocale);
                }
            } else {
                // Return default US Locale--note assumption here that
                // this is always supported.
                newLocale = DEFAULT_LOCALE;
                if (log.isInfoEnabled()) {
                    log.info("Locale " + localeStringRep 
                        + " not supported, converted to default locale " 
                        + DEFAULT_LOCALE);
                }
            }
            return newLocale;
        }
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
        // Set up the static map of supported locales.
        SystemTranslationManager manager = (SystemTranslationManager) 
            factory.getBean("systemTranslationManager");
        try {
            LocaleAdapter.setSupportedLocales(manager.getAllSupportedLocales());
            // Remember this instance so static methods can use it.
            localeAdapterInstance = (LocaleAdapter) this;
        } catch (Exception e) {
            log.error("Problem while initializing supported Locales!", 
                SystemErrorCode.DATA_PROVIDER_FAILURE, e);
        }
    }

    /**
     * Getter for the systemTranslationManager property.
     * @return SystemTranslationManager value of the property
     */
    public SystemTranslationManager getSystemTranslationManager() {
        return this.systemTranslationManager;
    }

    
    /**
     * Setter for the systemTranslationManager property.
     * @param systemTranslationManager the new systemTranslationManager value
     */
    public void setSystemTranslationManager(SystemTranslationManager systemTranslationManager) {
        this.systemTranslationManager = systemTranslationManager;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 