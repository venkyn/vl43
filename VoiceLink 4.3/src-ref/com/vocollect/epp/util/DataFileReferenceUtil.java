/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.service.EncryptionManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

/**
 * 
 *
 * @author ???
 */
public class DataFileReferenceUtil extends FileReferenceUtil {

    private static final long serialVersionUID = -4936692444780990263L;

    private byte[] contents = null;

    /**
     * Constructor.
     */
    public DataFileReferenceUtil() {
        super();
    }

    /**
     * Constructor.
     * @param fileName the file name
     */
    public DataFileReferenceUtil(String fileName) {
        super();
        setName(fileName);
    }

    /**
     * @param newContents the contents to store.
     */
    public void setFileContents(byte[] newContents) {
        this.contents = newContents;
    }

    /**
     * This method is meant to be used for very small files ONLY.  Do not use 
     * this method on files larger than a few kilobytes.  Instead, use 
     * getInputStream.
     * @return the contents of this file
     */
    public byte[] getFileContents() {
        if (contents != null) {
            return contents;
        } else {
            try {
                FileInputStream fis = new FileInputStream(getFileBaseDir()
                    + File.separator + getName());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                int whatWasRead = 0;
                while ((whatWasRead = fis.read()) != -1) {
                    baos.write(whatWasRead);
                }

                return baos.toByteArray();
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
            return null;
        }
    }

    /**
     * @return the Base64 encoded string of the contents byte array
     */
    public String getFormattedString() {
        byte[] fileContents = getFileContents();
        if (fileContents.length > 0) {
            return new String(Base64.encodeBase64(fileContents));
        } else {
            return "";
        }
    }

    /**
     * @param es the encryption manager to use when encrypting.
     */
    public void encrypt(EncryptionManager es) {
        setFileContents(es.encrypt(getFileContents()));
    }

    /**
     * @param es the encryption manager to use when encrypting.
     * @param pin the pin to use for encryption.
     */
    public void encrypt(EncryptionManager es, String pin) {
        if (pin != null) {
            setFileContents(es.encrypt(es.encrypt(getFileContents(), pin)));
        } else {
            encrypt(es);
        }
    }

    /**
     * Create a handle to the specified file.
     * @param certFile the certificates file.
     * @return an instance of this class, a handle to the file passed in, or
     * null if something isn't right.
     */
    public static DataFileReferenceUtil fromFormFile(File certFile) {
        try {
            if (certFile != null && certFile.getName() != null
                && certFile.getName().length() > 0 && certFile.length() > 0) {
                DataFileReferenceUtil certRefFile = new DataFileReferenceUtil(
                    certFile.getName());
                FileInputStream streamer = new FileInputStream(certFile);
                byte[] byteArray = new byte[streamer.available()];
                streamer.read(byteArray);

                certRefFile.setFileContents(byteArray);
                return certRefFile;
            }
            // TODO not informative if none of the conditions above are met.
        } catch (IOException e) {
            // TODO: Bug!! Empty catch.
        }
        return null;
    }

    /**
     * Store the certificate file. This could potentially be moved up to
     * FileReferenceVO
     * @throws VocollectException on any error
     */
    public void store() throws VocollectException {

        File outFile = new File(getFileBaseDir() + File.separator + getName());
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(outFile);
            fout.write(getFileContents());
            setFileContents(null);
        } catch (FileNotFoundException fnfe) {
            throw new VocollectException(fnfe);
        } catch (IOException ioe) {
            throw new VocollectException(ioe);
        } finally {
            if (fout != null) {
                try {
                    fout.close();
                } catch (IOException ioe) {
                    // Can ignore this
                }
            }
        }
    }
    
    /**
     */
    public void delete() {
        if (contents != null) {
            contents = null;
        } else {
            File thisFile = new File(getFileBaseDir() + File.separator + getName());
            thisFile.delete();
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 