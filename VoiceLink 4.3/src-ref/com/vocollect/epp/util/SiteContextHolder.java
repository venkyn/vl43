/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

/**
 * Simple holder class that associates a SiteContext instance
 * with the current thread. The SiteContext will be inherited
 * by any child threads spawned by the current thread.
 *
 * @author ddoubleday
 */
public abstract class SiteContextHolder {

    private static ThreadLocal<SiteContext> siteContextHolder =
        new InheritableThreadLocal<SiteContext>();

    /**
     * Constructor is private, no instantiation allowed.
     */
    private SiteContextHolder() {

    }

    /**
     * Associate the given SiteContext with the current thread.
     * @param siteContext the current SiteContext, or <code>null</code> to
     *            reset the thread-bound context
     */
    public static void setSiteContext(SiteContext siteContext) {
        siteContextHolder.set(siteContext);
    }

    /**
     * Return the SiteContext associated with the current thread, if any.
     * @return the current SiteContext, or <code>null</code> if none
     */
    public static SiteContext getSiteContext() {
        return siteContextHolder.get();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 