/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.filefilter.WildcardFilter;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;



/**
 * This is an extension of the Spring 
 * <code>ReloadableResourceBundleMessageSource</code> which allows for the
 * specification of directories that contain resource bundles rather than
 * specifying resource bundle base names. 
 * <p>
 * For each specified directory, all
 * files that end in ".properties" and contain no "_" character will be treated 
 * as resource bundle base names.
 * <p>
 * This makes it possible to avoid specifying the exact basenames, which allows
 * the resource bundle files to be provided by either EPP or EPP-based 
 * applications, with no hard-coded knowledge of the source of the bundles.
 * <p>
 * This was based off <code>ReloadableResourceBundleMessageSource</code> to
 * gain support for the ability to <code>setFallbackToSystemLocale(false)</code>.
 * This avoids unexpected fallback to the System Locale when calculating fallback
 * resources. For example, if the System Locale is "de" and the browser locale is
 * "es_MX", we want to fall back to English fallbacks rather than German.
 * <p>
 * As a development side benefit, resources loaded through <code>ResourceUtil</code>
 * can be updated without restarting the server, if you set the 
 * <code>cacheSeconds</code> property to something positive in 
 * the creation of this object in <code>ResourceUtil</code>. (However, it has
 * no effect on resources loaded through the Struts getText() methods.) 
 * 
 * @author ddoubleday
 */
public class DirectoryResourceBundleMessageSource extends
    ReloadableResourceBundleMessageSource {
    
    private static final Logger log = 
        new Logger(DirectoryResourceBundleMessageSource.class);

    private ResourceLoader resourceLoader = new DefaultResourceLoader();

    /**
     * Set the list of directory names to search for resource property base
     * names, and then set the base names accordingly.
     * @param dirNames classpath relative path names of directories to use for
     * resources.
     */
    public void setResourceDirectoryNames(String[] dirNames) {
        if (dirNames != null) {
            ArrayList<String> baseNames = new ArrayList<String>();
            // Look only at files with .properties extension.
            FileFilter fileFilter = new WildcardFilter("*.properties");
            for (String dirName : dirNames) {
                File dir = null;
                Resource res = null;
                try {
                    // Get the directory file handle.                                        
                    res = resourceLoader.getResource(dirName);
                    if (!res.exists()) {
                        //The application is not able to load the resource.
                        //This is possible if the current environment is Weblogic. For Weblogic prefix the path
                        //of the resources folder with WEB-INF/classes. Make sure that if there is a change in the 
                        //folders passed from ResourceUtil, the logic here is also updated.
                        if (log.isDebugEnabled()) {
                            log.debug("Not able to locate resource '" + dirName 
                                      + "', trying /WEB-INF/classes/" + dirName);
                        }
                        res = resourceLoader.getResource("/WEB-INF/classes/" + dirName);
                    }
                    dir = res.getFile();
                    if (log.isDebugEnabled()) {
                        log.debug("Resource property directory: " 
                                  + dir.getAbsolutePath());
                    }
                    // Get the property files from the directory
                    File[] propFiles = dir.listFiles(fileFilter);
                    for (File propFile : propFiles) {
                        if (propFile.getName().indexOf('_') == -1) {
                            // This is a fallback property file. Add the file
                            // name path to the list of base names, minus the
                            // .properties extension which Spring doesn't want.
                            baseNames.add(dirName + File.separatorChar 
                                + propFile.getName().substring(
                                    0, propFile.getName().indexOf(".properties")));
                            if (log.isTraceEnabled()) {
                                log.trace("Added base name " 
                                    + baseNames.get(baseNames.size() - 1));
                            }
                       }
                    }
                } catch (IOException e) {
                    // Couldn't find the resources directory.
                    log.error("Unable to initialize resources", 
                        SystemErrorCode.CONFIGURATION_ERROR, e);
                }
            }
            
            setBasenames(baseNames.toArray(new String[0]));
        }
        
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 