/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

/**
 * Format the time as HH:MM:SS for display in labor screens.
 * NOTE : Hours can be greater than 24.
 * @author pfunyak
 */
public final class TimeFormatter {

    private static final long TEN = 10;
    private static final long FIVE_HUNDRED = 500;
    // 1000
    private static final long MILLISECS_PER_SEC = 1000;
    // 60000
    private static final long MILLISECS_PER_MINUTE = (60 * MILLISECS_PER_SEC);
    // 36000000
    private static final long MILLISECS_PER_HOUR = 60 * MILLISECS_PER_MINUTE;

    /**
     * Hide the default constructor for public access.
     * Constructor.
     */
    private TimeFormatter() {

    }

    /**
     *
     * @param milliseconds - value to format
     * @return formatted string
     *
     */
     public static String formatTime(Long milliseconds) {

         // TODO verify format for internationization issues.
         long remainder;
         if (milliseconds == null) {
             remainder = 0;
         } else {
           remainder = milliseconds.longValue();
         }

         StringBuffer output = new StringBuffer("");

         Long hours = new Long(remainder / MILLISECS_PER_HOUR);
         remainder = remainder % MILLISECS_PER_HOUR;
         Long minutes = new Long(remainder / MILLISECS_PER_MINUTE);
         remainder = remainder % MILLISECS_PER_MINUTE;
         Long seconds = new Long(remainder / MILLISECS_PER_SEC);
         remainder = remainder % MILLISECS_PER_SEC;

         if (remainder >= FIVE_HUNDRED) {
             seconds++;
         }
         if (hours < TEN) {
             output.append("0");
         }
         output.append(hours.toString());
         output.append(":");
         if (minutes < TEN) {
             output.append("0");
         }
         output.append(minutes.toString());
         output.append(":");
         if (seconds < TEN) {
             output.append("0");
         }
         output.append(seconds.toString());
         return output.toString();
    }
     
     /**
      * Method to format the time value back to Integer. This method formats
      * hh:mm into hhmm
      * @param time String.
      * @param defaultTimeStringValue default time string representation
      * @param defaultTimeIntegerValue default integer time value
      * @return timeInteger Integer.
      */
    public static Integer formatTimeStringToInteger(String time,
                                                    String defaultTimeStringValue,
                                                    Integer defaultTimeIntegerValue) {
        if (time.equals(defaultTimeStringValue)) {
            return defaultTimeIntegerValue;
        }

        String[] categoryValueArray = time.split(":");
        Integer timeInteger = Integer.parseInt(categoryValueArray[0]
            + categoryValueArray[1]);
        return timeInteger;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 