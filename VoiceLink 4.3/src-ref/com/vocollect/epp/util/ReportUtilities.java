/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.web.util.DisplayUtilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.jasperreports.engine.JRParameter;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * A utilities class for Jasper reports.
 *
 * @author bnorthrop
 */
public final class ReportUtilities implements ApplicationContextAware,
    BeanFactoryPostProcessor {

    // DATA MEMBERS
    /**
     * The date format used by parameters in Jasper reports.
     */
    public static final String DATE_FORMAT = "yyyy-M-d-H-m-s";

    public static final String PARAM_CURRENT_SITE_CONTEXT = "VOC_CURRENT_SITE_CONTEXT";

    public static final String PARAM_CURRENT_USER = "VOC_CURRENT_USER";

    public static final String REPORT_NAME = "REPORT_NAME";

    public static final String START_DATE = "START_DATE";

    public static final String END_DATE = "END_DATE";

    public static final String SESSION_VALUE = "SESSION_VALUE";

    public static final String SITE = "SITE";

    public static final String ENUM = "ENUM";
    
    public static final String TIME_ZONE = "TIME_ZONE";
    
    public static final String IS_INTERVAL = "IS_INTERVAL";

    public static final String PARAM_LIST_ALL = "_ALL_";

    public static final String OPERATOR_IDENTIFIERS = "OPERATOR_IDENTIFIERS";

    public static final String OPERATOR = "OPERATOR";

    public static final String OPERATOR_TEAM_ID = "OPERATOR_TEAM_ID";

    public static final String OPERATOR_TEAM = "OPERATOR_TEAM";

    public static final String OPERATOR_IDENTIFIERS_ALL = "OPERATOR_IDENTIFIERS_ALL";

    public static final String OPERATOR_ALL = "OPERATOR_ALL";



    /**
     * Custom logger.
     */
    private static final Logger log = new Logger(ReportUtilities.class);

    /**
     * This variable holds the spring application context.
     */
    private static ApplicationContext ctx;

    private static DisplayUtilities du;

    /**
     * Helper function; attempt to extract parameter information from the
     * parameter map, using type casting.
     * @param parameterName The name of the param to lookup
     * @param parameterMap The map to look into
     * @return The parameter string value, or null if not found
     */
    public static String getParameterStringValue(String parameterName,
                                                 Map<String, ?> parameterMap) {
        if (parameterMap == null) {
            return null;
        }
        Object value = parameterMap.get(parameterName);
        if (value == null) {
            return null;
        } else {
            return value.toString();
        }
    }

    /**
     * Helper function to convert a String value into the appropriate object
     * value. This is needed when passing in parameter to Jasper Reports.
     * @param parameter The Jasper parameter object, which contains the
     *            parameter data type.
     * @param value The String value to be converted
     * @return The object value to be set in Jasper
     */
    public static Object getParameterValue(JRParameter parameter, String value) {
        Object objectValue = null;
        if (value == null) {
            return null;
        }
        try {
            // Check the data type and cast as needed
            if (parameter.getValueClass().equals(String.class)) {
                objectValue = value;
            } else if (parameter.getValueClass().equals(Long.class)) {
                objectValue = Long.valueOf(value);
            } else if (parameter.getValueClass().equals(Integer.class)) {
                objectValue = Integer.valueOf(value);
            } else if (parameter.getValueClass().equals(Double.class)) {
                objectValue = Double.valueOf(value);
            } else if (parameter.getValueClass().equals(Boolean.class)) {
                objectValue = Boolean.valueOf(value);
            } else if (parameter.getValueClass().equals(Date.class)) {
                // Date formatting will only interpret YYYY-MM-DD
                SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                format.setLenient(false);
                if (!value.equals("")) {
                    Date dateVal = format.parse(value);
                    objectValue = dateVal;
                }
            }
        } catch (NumberFormatException ex) {
            // If an invalid number is passed, ignore it; the report's
            // default value should be used.
            log.warn(ex.getMessage() + ": for values " + parameter.getName()
                + ", value: " + value, ex);
        } catch (ParseException ex) {
            // Invalid date; pass it in as null
            log.warn(ex.getMessage() + ": for values " + parameter.getName()
                + ", value: " + value, ex);
        }

        return objectValue;
    }

    /**
     * Converts value based enums to the localized text. Like STATUS fields
     * @param theObject The object to convert
     * @return The localized text for that enum value
     */
    public static String translateToDisplayValue(Object theObject) {
        if (theObject != null) {
            if (theObject instanceof ValueBasedEnum) {
                return ResourceUtil
                    .getLocalizedEnumName((ValueBasedEnum<?>) theObject);
            } else {
                try {
                    return du.translateUserData(theObject.toString());
                } catch (DataAccessException e) {
                    return theObject.toString();
                }
            }
        } else {
            return "";
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext appCtx)
        throws BeansException {
        ReportUtilities.ctx = appCtx;
    }

    /**
     *
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory arg0)
        throws BeansException {
        du = (DisplayUtilities) ctx.getBean(ctx
            .getBeanNamesForType(DisplayUtilities.class)[0]);
    }

    /**
     * Method to create Parameter name-value map utilizing parameters defined in
     * report definition, database values and user submitted values map
     * @param reportDefinitionParams Map of ReportParametersDTO objects.<br>
     *            <b>key:</b> parameter name<br>
     *            <b>value:</b>ReportParametersDTO object
     * @param reportDBParams Set of ReportParametersDTO objects, representing report
     *            specific parameters saved in database
     * @param valueMap Map of user/client submitted values
     * @return Map of ReportParametersDTO objects.<br>
     *         <b>key:</b> parameter name<br>
     *         <b>value:</b>ReportParametersDTO object
     */
    public static Map<String, Object> getReportValuesMap(List<ReportParametersDTO> reportDefinitionParams,
                                                         Set<ReportParametersDTO> reportDBParams,
                                                         Map<String, Object> valueMap) {
        Map<String, ReportParametersDTO> reportParamsMap = new HashMap<String, ReportParametersDTO>(
            reportDefinitionParams.size());
        for (ReportParametersDTO reportParamDto : reportDefinitionParams) {
            reportParamsMap.put(reportParamDto.getName(), reportParamDto);
        }
        if (log.isInfoEnabled()) {
            log.info("Report: " + valueMap.get(REPORT_NAME)
                + ". Parameter map generated. Loading values against map keys");
        }

        // Parameter value map
        Map<String, Object> values = loadParameterValues(reportParamsMap,
            reportDBParams, valueMap);

        return values;
    }

    /**
     * This method creates a map of objects using the required parameters and
     * the user submitted values.
     *
     * @param reportDefinitionParams Map of ReportParametersDTO objects.<br>
     *            <b>key:</b> parameter name<br>
     *            <b>value:</b>ReportParametersDTO object
     * @param reportDBParams Set of ReportParametersDTO objects, representing report
     *            specific parameters values saved in database
     * @param valueMap Map of user/client submitted values
     * @return New map of parameter values for the report<br>
     *         <b>key-</b>Parameter name <br>
     *         <b>value-</b>Object having the primitive value, created using
     *         <code>*.valueOf</code> according to the <code>valueclass</code>
     *         which is the data type of the respective parameter
     */
    @SuppressWarnings("unchecked")
    private static Map<String, Object> loadParameterValues(Map<String, ReportParametersDTO> reportDefinitionParams,
                                                           Set<ReportParametersDTO> reportDBParams,
                                                           Map<String, Object> valueMap) {
        Set<String> reportNames = reportDefinitionParams.keySet();
        Iterator<String> nameIter = reportNames.iterator();
        ReportParametersDTO parameter = null;

        // report values map with size = definition required params + locale +
        // datasource. hence, +2
        Map<String, Object> values = new HashMap<String, Object>(
            reportNames.size() + 2);
        Map<String, ReportParametersDTO> paramMap = getParameterHashMap(reportDBParams);
        while (nameIter.hasNext()) {
            String paramName = nameIter.next();
            parameter = reportDefinitionParams.get(paramName);
            ReportParametersDTO param = paramMap.get(paramName);
            String value = null;
            if (param != null) {
                value = param.getValue();
            }
            if (paramName.equals(PARAM_CURRENT_SITE_CONTEXT)) {
                // Pass in the current site context ID
                // (this won't be found in post data)
                value = (String) valueMap.get(PARAM_CURRENT_SITE_CONTEXT);
            } else if (paramName.equals(PARAM_CURRENT_USER)) {
                // Pass in current user name
                value = (String) valueMap.get(PARAM_CURRENT_USER);
            } else if (paramName.equals(SESSION_VALUE)) {
                value = (String) valueMap.get(SESSION_VALUE);
            } else if (paramName.equals(SITE)) {
                value = (String) valueMap.get(SITE);
            } else if (paramName.equals(START_DATE)) {
                value = (String) valueMap.get(START_DATE);
            } else if (paramName.equals(END_DATE)) {
                value = (String) valueMap.get(END_DATE);
            } else if (value == null) {
                // Boolean values with no post data will be interpreted
                // as "false"
                if (parameter.getValueClass().equals(Boolean.class)) {
                    values.put(paramName, Boolean.FALSE);
                }
                // No more processing to do for missing params.
                continue;
            } else if (value == ""
                && parameter.getValueClass().equals(String.class)) {
                // Treat empty strings as null; don't pass them in. This
                // will force the report to use the default value.
                continue;
            } else if (paramName.startsWith(ENUM)
                && (!value.equals(PARAM_LIST_ALL))) {
                Map<String, String> enumValues = null;
                if (valueMap.get(ENUM) != null) {
                    enumValues = (Map<String, String>) valueMap.get(ENUM);
                }
                value = enumValues.get(value);
            }
            if (log.isDebugEnabled()) {
                log.debug("Report:" + valueMap.get(REPORT_NAME) + ". Param- "
                    + paramName + "=" + value);
            }
            if (value.equals(PARAM_LIST_ALL)) {
                // No value found for this param
                // OR they selected the ALL option, in which case we don't
                // pass the parameter.
                continue;
            }

            // Convert the String value into an object value
            values.put(
                paramName,
                getParameterValue(parameter, value, valueMap.get(REPORT_NAME)
                    .toString()));
        }

        return values;
    }

    /**
     * Helper function to place report parameters into a hash map for
     * searchability.
     * @param params - a set of report parameters
     * @return - a hash map of the parameter names and he report parameters
     */
    private static Map<String, ReportParametersDTO> getParameterHashMap(Set<ReportParametersDTO> params) {
        Map<String, ReportParametersDTO> map = new HashMap<String, ReportParametersDTO>(
            params.size());
        for (ReportParametersDTO reportParamDto : params) {
            map.put(reportParamDto.getName(), reportParamDto);
        }

        return map;
    }

    /**
     * Helper function to convert a String value into the appropriate object
     * value. This is needed when passing in parameter to Reports class such as
     * for Jasper.
     *
     * @param parameter The parameter object, which contains the parameter data
     *            type.
     * @param value The String value to be converted
     * @param reportName Report name, required for logging
     * @return The data type wrapper class object
     */
    private static Object getParameterValue(ReportParametersDTO parameter,
                                            String value,
                                            String reportName) {
        Object objectValue = null;
        try {
            // Check the data type and cast as needed
            if (parameter.getValueClass().equals(String.class)) {
                objectValue = value;
            }
            if ((value != null) && (!value.equals(""))) {
                if (parameter.getValueClass().equals(Long.class)) {
                    objectValue = Long.valueOf(value);
                } else if (parameter.getValueClass().equals(Integer.class)) {
                    objectValue = Integer.valueOf(value);
                } else if (parameter.getValueClass().equals(Double.class)) {
                    objectValue = Double.valueOf(value);
                } else if (parameter.getValueClass().equals(Boolean.class)) {
                    objectValue = Boolean.valueOf(value);
                } else if (parameter.getValueClass().equals(Date.class)) {
                    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                    format.setLenient(false);
                    Date dateVal = format.parse(value);
                    objectValue = dateVal;
                }
            }
        } catch (NumberFormatException ex) {
            // Ignore invalid number; the report's default value should be used.
            log.warn("Report:" + reportName + ". Param-" + parameter.getName()
                + "=" + value + ". Exception=" + ex.getMessage(), ex);

        } catch (ParseException ex) {
            // Invalid date; pass it in as null
            log.warn("Report:" + reportName + ". Param-" + parameter.getName()
                + "=" + value + ". Exception=" + ex.getMessage());
        }

        return objectValue;
    }

    /**
     * Method to set values in map
     * @param reportParams Set of report parameters
     * @param values map of values to be consumed in reports
     */
    public static void setReportOperatorIdentifiers(Set<ReportParametersDTO> reportParams,
                                                    Map<String, Object> values) {
        Set<String> operatorId = new HashSet<String>();
        for (ReportParametersDTO reportParameter : reportParams) {
            if (reportParameter.getName().equals("OPERATOR_ID")) {
                operatorId.add(reportParameter.getValue());
            }
        }

        String operator = null;
        if (operatorId.isEmpty()) {
            operatorId.add(" "); // Required to make query IN clause working
        } else {
            operator = operatorId.iterator().next();
        }
        values.put(OPERATOR_IDENTIFIERS, operatorId);
        values.put(OPERATOR, operator);
    }

    /**
     * Method to set values in map
     * @param reportParams Set of report parameters
     * @param values map of values to be consumed in reports
     */
    public static void setReportOperatorTeams(Set<ReportParametersDTO> reportParams,
                                              Map<String, Object> values) {
        Set<Long> operatorTeamId = new HashSet<Long>();
        for (ReportParametersDTO reportParameter : reportParams) {
            if (reportParameter.getName().equals("OPERATOR_TEAM_ID")) {
                operatorTeamId.add(Long.valueOf(reportParameter.getValue()));
            }
        }

        Long operatorTeam = null;
        if (operatorTeamId.isEmpty()) {
            operatorTeamId.add(-9999L);
        } else {
            operatorTeam = operatorTeamId.iterator().next();
        }

        values.put(OPERATOR_TEAM_ID, operatorTeamId);
        values.put(OPERATOR_TEAM, operatorTeam);
    }

    // CONSTRUCTOR
    /**
     * Private constructor - no one should instantiate since it is a utilities
     * class.
     */


    /**
     * Constructor.
     */
    private ReportUtilities() {
        // do nothing
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 