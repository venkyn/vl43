/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;

import java.util.Locale;

import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;


/**
 * Common methods for resource handling.
 *
 * @author ddoubleday
 */
public final class ResourceUtil {

    private static final Logger log = new Logger(ResourceUtil.class);

    // The application resources bundle.
    private static DirectoryResourceBundleMessageSource appResources;
    
    static {
        appResources = new DirectoryResourceBundleMessageSource();
        // All resources we know about are in the "resources" directory
        // in the classpath.
        //Before adding any new additions to this list, please see the comments in 
        //DirectoryResourceBundleMessageSource.setResourceDirectoryNames
        appResources.setResourceDirectoryNames(
            new String[] {"resources",
                          "com/vocollect/epp/web/action"});
        // Don't fall back to system locale to avoid weird things like falling
        // back to German when the browser Locale was set to Spanish. Fallbacks
        // will always be English.
        appResources.setFallbackToSystemLocale(false);
    }
    
    /**
     * Constructor, no objects permitted.
     */
    private ResourceUtil() {  
    }
    
    /**
     * Construct a default resource key for a ValueBasedEnum instance.
     * This method is centralized here so the convention doesn't have to
     * be repeated in the <code>getResourceKey</code> method of every enum.
     * @param e the enum value to make a resource key for.
     * @return the resource key
     */
    public static String makeEnumResourceKey(ValueBasedEnum<?> e) {
        return e.getClass().getName() + "." + e.toValue();
    }
    
    /**
     * @param e the enum value to translate
     * @return the String representation of the value in the current Locale
     * context.
     */
    public static String getLocalizedEnumName(ValueBasedEnum<?> e) {
        return getLocalizedKeyValue(e.getResourceKey());
    }
    
    /**
     * Resolve a key from the resource bundle.
     * @param keyName the key to lookup
     * @return String representation of the key in current Locale, or the
     * original key if no match was found.
     */
    public static String getLocalizedKeyValue(String keyName) {
        return getLocalizedMessage(keyName, null, keyName);
    }

    /**
     * Resolve a key from the resource bundle.
     * @param keyName the key to lookup
     * @param args the arguments to insert into the message.
     * @param defaultValue the return value if no key match is found.
     * @return String representation of the key with interpolated args in 
     * current Locale, or the defaultValue if no match was found.
     */
    public static String getLocalizedMessage(String   keyName, 
                                             Object[] args, 
                                             String   defaultValue) {
        return getLocalizedMessage(
            keyName, args, defaultValue, LocaleContextHolder.getLocale());
    }
    
    /**
     * Resolve a key from the resource bundle against the specified Locale.
     * @param keyName the key to lookup
     * @param args the arguments to insert into the message.
     * @param defaultValue the return value if no key match is found.
     * @param locale the Locale to resolve against
     * @return String representation of the key with interpolated args in 
     * the specified Locale, or the defaultValue if no match was found.
     */
    public static String getLocalizedMessage(String   keyName, 
                                             Object[] args, 
                                             String   defaultValue,
                                             Locale   locale) {
        try {
            String val = getLocalizedMessage(keyName, args, locale);
            if (args == null) {
                // This is done to work around an interoperability issue
                // with Java's ResourceBundle and MessageSource classes. 
                // The issue is described in 
                // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4293229
                // Basically, if there is to be a single-quote character in
                // a resource value, it must be escaped by being doubled in
                // order for MessageSource to work correctly, but if it is 
                // doubled, then ResourceBundle.getString() will return both
                // characters. Since the latter is used by Spring when there
                // are no arguments to replace, we have to strip out doubled
                // single quotes ourselves. -- ddoubleday
                val = val.replace("''", "'");
            }
            return val;
        } catch (NoSuchMessageException e) {
            if (log.isDebugEnabled()) {
                log.debug("Could not resolve key: " + keyName);
            }
            return defaultValue;
        }
    }
    
    /**
     * Resolve a key from the resource bundle against the specified Locale,
     * or throw an exception if it couldn't be resolved.
     * @param keyName the key to lookup
     * @param args the arguments to insert into the message.
     * @param locale the Locale to resolve against
     * @return String representation of the key with interpolated args in 
     * the specified Locale
     * @throws NoSuchMessageException if no match was found.
     */
    public static String getLocalizedMessage(String   keyName, 
                                             Object[] args, 
                                             Locale   locale)
        throws NoSuchMessageException {
        
        return appResources.getMessage(keyName, 
                                       translateArgs(args, locale), 
                                       locale);
    }
    
    /**
     * This method is used both here and in BaseAction for an EPP-specific
     * extension to resource handling. If an argument is
     * a String and it begins with a special prefix defined by
     * <code>UserMessage.KEY_EVAL_PREFIX</code>, then the argument is treated
     * as a resource key and it is translated before being inserted into the
     * localized message.
     * @param arg the argument
     * @param locale the locale for translation 
     * @return the (possibly) translated argument.
     */
    public static Object translateArg(Object arg, Locale locale) {
        if (arg instanceof String) {
            // The value is a String
            String strValue = ((String) arg);
            if (strValue.startsWith(UserMessage.KEY_EVAL_PREFIX)) {
                // And it is a String that is a resource key that
                // needs to be evaluated in advance.
                String realKey = strValue.substring(
                    UserMessage.KEY_EVAL_PREFIX.length());
                return getLocalizedMessage(realKey, null, realKey, locale);
            }
        }
        
        return arg;
    }

    /**
     * This method is an EPP-specific
     * extension to resource handling. If an argument in the args array is
     * a String and it begins with a special prefix defined by
     * <code>UserMessage.KEY_EVAL_PREFIX</code>, then the argument is treated
     * as a resource key and it is translated before being inserted into the
     * localized message.
     * @param args the array of arguments
     * @param locale Locale for translation
     * @return the same array, but with any resource keys translated.
     */
    private static Object[] translateArgs(Object[] args, Locale locale) {
        if (args == null) {
            return null;
        } else {
            for (int i = 0; i < args.length; i++) {
                args[i] = translateArg(args[i], locale);
            }
            // Return the (possibly) modified args array.
            return args;
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 