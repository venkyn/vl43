/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import java.util.Comparator;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * A comparator used to compare any two objects that have the same property. The
 * property specified may be a nested property.
 * @author brupert
 * @param <T> Type of objects containing the property in question.
 */
public class PropertyComparator<T> implements Comparator<T> {

    private static final Logger log = new Logger(PropertyComparator.class);

    private String propertyName;

    private boolean sortAsc;

    private static final int EQUAL = 0;

    private static final int LESS_THAN = -1;

    private static final int GREATER_THAN = 1;

    /**
     * Create a new property comparator.
     * @param propertyName The name of the property to compare.
     * @param sortAsc Set to true to sort in ascending order, else descending.
     */
    public PropertyComparator(String propertyName, boolean sortAsc) {
        this.propertyName = propertyName;
        this.sortAsc = sortAsc;
    }

    /**
     * Compares a property of the two objects passed. The property must
     * implement the <code>Comparable</code> interface.
     * @param o1 The first object to compare
     * @param o2 The second objece to compare
     * @return If o2 is greater than, equal to, or less than o1
     * @see java.util.Comparator#compare(Object, Object)
     */
    @SuppressWarnings("unchecked")
    public int compare(T o1, T o2) {
        try {
            Object p1 = null;
            Object p2 = null;
            boolean p1NotFound = false;

            try {
                p1 = PropertyUtils.getNestedProperty(o1, propertyName);
            } catch (Exception e) {
                // We catch Exception to shorthand the handling of the
                // following:
                // NestedNullException, IllegalAccessException,
                // InvocationTargetException, NoSuchMethodException.
                p1NotFound = true;
            } finally {
                try {
                    p2 = PropertyUtils.getNestedProperty(o2, propertyName);
                } catch (Exception e) {
                    if (p1NotFound) {
                        // Nothing to compare.
                        // Therefore, object 1 == object 2
                        if (log.isTraceEnabled()) {
                            log.trace("nothing to compare");
                        }
                        return sortResult(EQUAL);
                    } else {
                        // Object 1 has something to compare but object 2 does
                        // not.
                        // Therefore, object 1 > object 2
                        if (log.isTraceEnabled()) {
                            log.trace(p1 + " is greater by default");
                        }
                        return sortResult(GREATER_THAN);
                    }
                }
                if (p1NotFound) {
                    // Object 1 doesn't have something to compare but object 2
                    // does.
                    // Therefore, object 1 < object 2
                    if (log.isTraceEnabled()) {
                        log.trace(p2 + " is greater by default");
                    }
                    return sortResult(LESS_THAN);
                }
            }
            // This if statement is here for performace. If it wasn't here, then
            // the entire if-else block would run in the best-case scenario
            // where
            // neither are null.
            if (p1 == null || p2 == null) {
                if (p1 == null && p2 == null) {
                    // Nothing to compare.
                    // Therefore, object 1 == object 2
                    if (log.isTraceEnabled()) {
                        log.trace("nothing to compare (null)");
                    }
                    return sortResult(EQUAL);
                } else if (p1 == null && p2 != null) {
                    // Object 1 doesn't have something to compare but object 2
                    // does.
                    // Therefore, object 1 < object 2
                    if (log.isTraceEnabled()) {
                        log.trace(p2 + " is greater by default (null)");
                    }
                    return sortResult(LESS_THAN);
                } else if (p1 != null && p2 == null) {
                    // Object 1 has something to compare but object 2 does not.
                    // Therefore, object 1 > object 2
                    if (log.isTraceEnabled()) {
                        log.trace(p1 + " is greater by default (null)");
                    }
                    return sortResult(GREATER_THAN);
                }
            }

            try {
                if (p1 instanceof ValueBasedEnum
                    && p2 instanceof ValueBasedEnum) {
                    return sortResult(ResourceUtil.getLocalizedEnumName(
                        (ValueBasedEnum) p1).compareTo(
                        ResourceUtil.getLocalizedEnumName((ValueBasedEnum) p2)));
                } else {
                    return sortResult(((Comparable) p1).compareTo(p2));
                }
            } catch (ClassCastException e) {
                log.error(
                    "Invalid use of PropertyComparator on non-Comparables, property name = '"
                        + propertyName + "'",
                    SystemErrorCode.CONFIGURATION_ERROR, e);
            }
        } catch (Exception e) {
            log.error("Invalid use of PropertyComparator, property name = '"
                + propertyName + "'", SystemErrorCode.CONFIGURATION_ERROR, e);
        }
        return 0;
    }

    /**
     * helper method for sorting ascending/decending.
     * @param initalSortValue The initial value of the sort.
     * @return The inverted value of the initial sort value if sort decending.
     */
    private int sortResult(int initalSortValue) {
        return (this.sortAsc) ? initalSortValue : -initalSortValue;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 