/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import org.springframework.context.ApplicationContext;


/**
 * Static holder of the Spring application context for easy access
 * from anywhere in the application. Dependency injection is preferred
 * in all cases, but is not available everywhere, in which case this
 * static reference can be used to do dependency lookup.
 *
 * @author ddoubleday
 */
public final class ApplicationContextHolder {

    // Static holder of the Spring application context for easy access
    // from anywhere in the application. 
    private static ApplicationContext ctx;
    
    /**
     * Private Constructor.
     */
    private ApplicationContextHolder() {
        // Allow no construction of this object.
    }
    
    /**
     * @param applicationContext the context to store.
     */
    public static void setApplicationContext(ApplicationContext applicationContext) {
        ctx = applicationContext;
    }
    
    /**
     * @return the current Spring application context
     */
    public static ApplicationContext getApplicationContext() {
        return ctx;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 