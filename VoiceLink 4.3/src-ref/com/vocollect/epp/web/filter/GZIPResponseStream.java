/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;


/**
 * Wraps Response Stream for GZipFilter.
 *
 * @author  Matt Raible
 * @author ddoubleday
 */
public class GZIPResponseStream extends ServletOutputStream {
    // abstraction of the output stream used for compression
    private OutputStream bufferedOutput = null;

    // state keeping variable for if close() has been called
    private boolean closed = false;

    // reference to original response
    private HttpServletResponse response = null;

    // reference to the output stream to the client's browser
    private ServletOutputStream output = null;

    // default size of the in-memory buffer
    private final int bufferSize = 50000;

    /**
     * Constructor.
     * @param response the response
     * @throws IOException on failure to get the output stream.
     */
    public GZIPResponseStream(HttpServletResponse response)
    throws IOException {
        super();
        closed = false;
        this.response = response;
        this.output = response.getOutputStream();
        bufferedOutput = new ByteArrayOutputStream();
    }

    /**
     * {@inheritDoc}
     * @see java.io.OutputStream#close()
     */
    @Override
    public void close() throws IOException {
        // verify the stream is yet to be closed
        if (closed) {
            throw new IOException("This output stream has already been closed");
        }

        // if we buffered everything in memory, gzip it
        if (bufferedOutput instanceof ByteArrayOutputStream) {
            // get the content
            ByteArrayOutputStream baos = (ByteArrayOutputStream) bufferedOutput;

            // prepare a gzip stream
            ByteArrayOutputStream compressedContent =
                new ByteArrayOutputStream();
            GZIPOutputStream gzipstream =
                new GZIPOutputStream(compressedContent);
            byte[] bytes = baos.toByteArray();
            gzipstream.write(bytes);
            gzipstream.finish();

            // get the compressed content
            byte[] compressedBytes = compressedContent.toByteArray();

            // set appropriate HTTP headers
            response.setContentLength(compressedBytes.length);
            response.addHeader("Content-Encoding", "gzip");
            output.write(compressedBytes);
            output.flush();
            output.close();
            closed = true;
        // if things were not buffered in memory, finish the GZIP stream and response
        } else if (bufferedOutput instanceof GZIPOutputStream) {
            // cast to appropriate type
            GZIPOutputStream gzipstream = (GZIPOutputStream) bufferedOutput;

            // finish the compression
            gzipstream.finish();

            // finish the response
            output.flush();
            output.close();
            closed = true;
        }
    }

    /**
     * {@inheritDoc}
     * @see java.io.OutputStream#flush()
     */
    @Override
    public void flush() throws IOException {
        if (closed) {
            throw new IOException("Cannot flush a closed output stream");
        }

        bufferedOutput.flush();
    }

    /**
     * {@inheritDoc}
     * @see java.io.OutputStream#write(int)
     */
    @Override
    public void write(int b) throws IOException {
        if (closed) {
            throw new IOException("Cannot write to a closed output stream");
        }

        // make sure we aren't over the buffer's limit
        checkBufferSize(1);

        // write the byte to the temporary output
        bufferedOutput.write((byte) b);
    }

    /**
     * Flush to output stream if length is big enough to require it.
     * @param length the length we want to buffer
     * @throws IOException on failure to write to the output stream
     */
    private void checkBufferSize(int length) throws IOException {
        // check if we are buffering too large of a file
        if (bufferedOutput instanceof ByteArrayOutputStream) {
            ByteArrayOutputStream baos = (ByteArrayOutputStream) bufferedOutput;

            if ((baos.size() + length) > bufferSize) {
                // files too large to keep in memory are sent to the client without Content-Length specified
                response.addHeader("Content-Encoding", "gzip");

                // get existing bytes
                byte[] bytes = baos.toByteArray();

                // make new gzip stream using the response output stream
                GZIPOutputStream gzipstream = new GZIPOutputStream(output);
                gzipstream.write(bytes);

                // we are no longer buffering, send content via gzipstream
                bufferedOutput = gzipstream;
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see java.io.OutputStream#write(byte[])
     */
    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    /**
     * {@inheritDoc}
     * @see java.io.OutputStream#write(byte[], int, int)
     */
    @Override
    public void write(byte[] b, int off, int len) throws IOException {

        if (closed) {
            throw new IOException("Cannot write to a closed output stream");
        }

        // make sure we aren't over the buffer's limit
        checkBufferSize(len);

        // write the content to the buffer
        bufferedOutput.write(b, off, len);
    }

    /**
     * @return whether or not the stream is closed.
     */
    public boolean closed() {
        return (this.closed);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 