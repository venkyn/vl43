/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.PurgeArchiveDisplayItem;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;

import org.acegisecurity.BadCredentialsException;
import org.acegisecurity.ldap.DefaultInitialDirContextFactory;
import org.acegisecurity.ldap.LdapDataAccessException;
import org.acegisecurity.ldap.search.FilterBasedLdapUserSearch;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class provides Struts action methods that support the System
 * Configuration feature.
 * 
 * @author V. Subramani
 */
public class SystemConfigurationActionRoot extends SiteEnabledAction implements
    Preparable {

    private static final long serialVersionUID = -1862272770300412448L;

    public static final String SMTP_HOST = "SMTP_Host";

    public static final String SMTP_USERNAME = "SMTP_Username";

    public static final String SMTP_PASSWORD = "SMTP_Password";

    public static final String SMTP_AUTHENTICATION = "SMTP_Authentication";

    public static final String DIRSERV_AUTHENTICATION = "DIRSERV_Authentication";

    public static final String DIRSERV_HOST = "DIRSERV_Host";

    public static final String DIRSERV_PORT = "DIRSERV_Port";

    public static final String DIRSERV_SEARCH_USERNAME = "DIRSERV_Search_Username";

    public static final String DIRSERV_SEARCH_PASSWORD = "DIRSERV_Search_Password";

    public static final String DIRSERV_SEARCH_BASE = "DIRSERV_Search_Base";

    public static final String DIRSERV_SEARCHABLE_ATTRIBUTE = "DIRSERV_Searchable_Attribute";

    private static final int PURGE_ARCHIVE_MAX = 9999;

    private static final int PURGE_ARCHIVE_MIN = 0;

    private static final int PURGE_WITH_ARCHIVE_ENABLED_MAX = 7;

    public static final String BATTERY_LAST_USED_LIMIT_PROPERTY = "BatteryLastUsedLimit_Configuration";
    
    public static final Integer BATTERY_LAST_USED_LIMIT_DEFAULT = 30;
    
    public static final int BATTERY_LAST_USED_LIMIT_MIN = 1;
    
    public static final int BATTERY_LAST_USED_LIMIT_MAX = 999;
    
    public static final String REGION_INTERVAL_CONFIGURATION = "RegionInterval_Configuration";

    private SystemPropertyManager systemPropertyManager;

    private UserManager userManager;

    // Attribute for the SMTP Host
    private String smtpHost = null;

    // Attribute for the SMTP Requiring Authentication
    private boolean smtpAuthenticationRequired = false;

    // Attribute for the SMTP Username
    private String smtpUserName = null;

    // Attribute for the SMTP Password
    private String smtpPassword = null;

    // Attribute for the SMTP Password verified
    private String smtpPasswordVerified = null;

    // Hidden attribute to store authentication
    private String hiddenSmtpAuthenticationRequired = null;

    // Attribute for whether or not directory server authentication is required
    private boolean directoryServerAuthenticationRequired = false;
    
    // Attribute for whether or not to use SSL with the directory server
    private boolean directoryServerUseSSL = false;

    // Attribute for the directory server host
    private String directoryServerHost = null;

    // Attribute for the directory server port
    private String directoryServerPort = null;

    // Attribute for the search user name
    private String directoryServerSearchUserName = null;

    // Attribute for the search user password
    private String directoryServerSearchPassword = null;

    // Attribute for the search user password verification
    private String directoryServerSearchPasswordVerified = null;

    // Attribute for the directory server search base
    private String directoryServerSearchBase = null;

    // Attribute for the directory server searchable attribute
    private String directoryServerSearchableAttribute = null;

    // Attribute for the test user if entered
    private String directoryServerTestUser = null;

    // Hidden attribute to store directory server authentication
    private String hiddenDirectoryServerAuthenticationRequired = null;

    // Parameter passed to the custom result type.
    private String message;

    // Map to hold purge/archive settings
    private Map<String, PurgeArchiveDisplayItem> purgeArchiveItemsMap = null;

    // List to use in ftl to loop through settings
    private List<PurgeArchiveDisplayItem> purgeArchiveItems = null;

    // Hidden attribute to keep track of purge form objects
    private String[] purgeKeys;

    // Hidden attribute to keep track of archive form objects
    private String[] archiveKeys;

    // // Purge enable/disable checkbox values
    // private String[] purgeEnabledCheckboxes;
    //    
    // // Archive enable/disable checkbox values
    // private String[] archiveEnabledCheckboxes;

    // Purge values
    private String[] purgeValueTextboxes;

    // Archive values
    private String[] archiveValueTextboxes;

    // Map of system properties so we can do efficient lookups. 
    private HashMap<String, SystemProperty> properties = null;

    private Integer batteryLastUsedLimit;
    
    private String regionIntervals;
    
    private Boolean showOtherConfiguration = null;
    
    private Boolean showBatteryConfig = null;
    
    private Boolean showVLOtherConfig = null;

    /**
     * Getter for the SystemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return this.systemPropertyManager;
    }

    /**
     * Setter for the SystemPropertyManager property.
     * @param systemPropertyManager the new SystemPropertyManager
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        


        // Instantiate collections to hold purge/archive settings
        if (purgeArchiveItemsMap == null) {
            purgeArchiveItemsMap = new LinkedHashMap<String, PurgeArchiveDisplayItem>();
        }
        purgeArchiveItems = new ArrayList<PurgeArchiveDisplayItem>();

        List<SystemProperty> sList = systemPropertyManager.getAll();

        for (SystemProperty property : sList) {
            if (property.getName().equals(SMTP_HOST)) {
                smtpHost = property.getValue();
            } else if (property.getName().equals(SMTP_USERNAME)) {
                smtpUserName = property.getValue();
            } else if (property.getName().equals(SMTP_PASSWORD)) {
                smtpPassword = property.getValue();
                smtpPasswordVerified = property.getValue();
            } else if (property.getName().equals(SMTP_AUTHENTICATION)) {
                smtpAuthenticationRequired = Boolean.valueOf(property
                    .getValue());
            } else if (property.getName().equals(DIRSERV_AUTHENTICATION)) {
                directoryServerAuthenticationRequired = Boolean
                    .valueOf(property.getValue());
            } else if (property.getName().equals(DIRSERV_HOST)) {
                directoryServerHost = property.getValue();
            } else if (property.getName().equals(DIRSERV_PORT)) {
                directoryServerPort = property.getValue();
            } else if (property.getName().equals(DIRSERV_SEARCH_USERNAME)) {
                directoryServerSearchUserName = property.getValue();
            } else if (property.getName().equals(DIRSERV_SEARCH_PASSWORD)) {
                directoryServerSearchPassword = property.getValue();
                directoryServerSearchPasswordVerified = property.getValue();
            } else if (property.getName().equals(DIRSERV_SEARCH_BASE)) {
                directoryServerSearchBase = property.getValue();
            } else if (property.getName().equals(DIRSERV_SEARCHABLE_ATTRIBUTE)) {
                directoryServerSearchableAttribute = property.getValue();
            } else if (property.getName().endsWith("_Purge")) {
                // Lop off _Purge to get portion of key common with _Archive
                int keyEnd = property.getName().indexOf("_Purge");
                String commonKey = property.getName().substring(0, keyEnd);

                // Get or create object to hold settings
                PurgeArchiveDisplayItem pai = null;
                if (purgeArchiveItemsMap.containsKey(commonKey)) {
                    pai = purgeArchiveItemsMap.get(commonKey);
                } else {
                    pai = new PurgeArchiveDisplayItem();
                    pai.setName(commonKey);
                }

                // Set purge settings
                if (property.getValue() != null) {
                    pai.setPurgeValue(property.getValue());
                } else {
                    pai.setPurgeValue("-1000");
                }
                purgeArchiveItemsMap.put(commonKey, pai);

            } else if (property.getName().endsWith("_Archive")) {
                // Lop off _Purge to get portion of key common with _Archive
                int keyEnd = property.getName().indexOf("_Archive");
                String commonKey = property.getName().substring(0, keyEnd);

                // Get or create object to hold settings
                PurgeArchiveDisplayItem pai = null;
                if (purgeArchiveItemsMap.containsKey(commonKey)) {
                    pai = purgeArchiveItemsMap.get(commonKey);
                } else {
                    pai = new PurgeArchiveDisplayItem();
                    pai.setName(commonKey);
                }

                // Set archive settings
                if (property.getValue() != null) {
                    pai.setArchiveValue(property.getValue());
                } else {
                    pai.setArchiveValue("-1000");
                }
                purgeArchiveItemsMap.put(commonKey, pai);
            }
        }

        // Put purge/archive settings into a list for iteration in ftl.
        purgeArchiveItems.addAll(purgeArchiveItemsMap.values());
        
        try {
            SystemProperty propertyBatteryLastUsedLimit = 
                getSystemPropertyManager().findByName(BATTERY_LAST_USED_LIMIT_PROPERTY);
            if (propertyBatteryLastUsedLimit != null && propertyBatteryLastUsedLimit.getValue() != null) {
                Integer integerBatteryLastUsedLimit = Integer.valueOf(propertyBatteryLastUsedLimit.getValue());
                setBatteryLastUsedLimit(integerBatteryLastUsedLimit);
            }
        } catch (NumberFormatException nfe) {
            setBatteryLastUsedLimit(BATTERY_LAST_USED_LIMIT_DEFAULT);
        }
        
        SystemProperty propertyRegionIntervalConfig = 
            getSystemPropertyManager().findByName(REGION_INTERVAL_CONFIGURATION);
        if (propertyRegionIntervalConfig != null && propertyRegionIntervalConfig.getValue() != null) {
            setRegionIntervals(propertyRegionIntervalConfig.getValue());
        }
        
    }

    /**
     * Save changes to the system configuration.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        try {
            
            List<SystemProperty> sList = systemPropertyManager.getAll();
            properties = new HashMap<String, SystemProperty>();
            for (SystemProperty property : sList) {
                properties.put(property.getName(), property);
            }

            // If we are smtp authenticating - then we have to supply a username
            // and password. If we don't have those - fail save and return
            // errors
            validateSmtpAuthentication();

            // If we are user authenticating against a directory server- then we
            // have
            // to supply lots of parameters. If we don't have those - fail save
            // and
            // return errors
            validateDirectoryServerAuthentication();

            validatePurgeArchiveSettings();

            validateOtherConfiguration();
            
            if (hasErrors()) {
                return INPUT;
            }

            for (SystemProperty property : sList) {
                if (property.getName().equals(SMTP_HOST)) {
                    property.setValue(smtpHost);
                } else if (property.getName().equals(SMTP_USERNAME)) {
                    property.setValue(smtpUserName);
                } else if (property.getName().equals(SMTP_PASSWORD)) {
                    property.setValue(smtpPassword);
                } else if (property.getName().equals(SMTP_AUTHENTICATION)) {
                    property.setValue(hiddenSmtpAuthenticationRequired);
                } else if (property.getName().equals(DIRSERV_HOST)) {
                    property.setValue(directoryServerHost);
                } else if (property.getName().equals(DIRSERV_PORT)) {
                    property.setValue(directoryServerPort);
                } else if (property.getName().equals(DIRSERV_AUTHENTICATION)) {
                    property
                        .setValue(hiddenDirectoryServerAuthenticationRequired);
                } else if (property.getName().equals(DIRSERV_SEARCH_USERNAME)) {
                    property.setValue(directoryServerSearchUserName);
                } else if (property.getName().equals(DIRSERV_SEARCH_PASSWORD)) {
                    property.setValue(directoryServerSearchPassword);
                } else if (property.getName().equals(DIRSERV_SEARCH_BASE)) {
                    property.setValue(directoryServerSearchBase);
                } else if (property.getName().equals(
                    DIRSERV_SEARCHABLE_ATTRIBUTE)) {
                    property.setValue(directoryServerSearchableAttribute);
                } else if (property.getName().endsWith("_Purge")) {
                    int keyEnd = property.getName().indexOf("_Purge");
                    String commonKey = property.getName().substring(0, keyEnd);
                    // Get and save the new purge setting
                    if (purgeArchiveItemsMap.containsKey(commonKey)) {
                        PurgeArchiveDisplayItem padi = purgeArchiveItemsMap
                            .get(commonKey);
                        if (padi.getPurgeValue() != "-1000") {
                            property.setValue(padi.getPurgeValue());
                        } else {
                            property.setValue(null);
                        }
                    } else {
                        property.setValue(null);
                    }
                } else if (property.getName().endsWith("_Archive")) {
                    int keyEnd = property.getName().indexOf("_Archive");
                    String commonKey = property.getName().substring(0, keyEnd);
                    // Get and save the new archive setting
                    if (purgeArchiveItemsMap.containsKey(commonKey)) {
                        PurgeArchiveDisplayItem padi = purgeArchiveItemsMap
                            .get(commonKey);
                        if (padi.getArchiveValue() != "-1000") {
                            property.setValue(padi.getArchiveValue());
                        } else {
                            property.setValue(null);
                        }
                    } else {
                        property.setValue(null);
                    }
                }

                systemPropertyManager.save(property);
            }

            if (getBatteryLastUsedLimit() != null) {
             
                SystemProperty batteryListUsedLimit = 
                    getSystemPropertyManager().findByName(BATTERY_LAST_USED_LIMIT_PROPERTY);
                batteryListUsedLimit.setValue(getBatteryLastUsedLimit().toString());
                getSystemPropertyManager().save(batteryListUsedLimit);
            }
            
            //Blank is allowed    
            SystemProperty regionIntervalConfig = 
                getSystemPropertyManager().findByName(REGION_INTERVAL_CONFIGURATION);
            regionIntervalConfig.setValue(getRegionIntervals());
            getSystemPropertyManager().save(regionIntervalConfig);
            
            return SUCCESS;
        } catch (DatabaseConstraintException e) {
            return CANCEL;
        }
    }

    /**
     * extracted the purge archive setting stuff into a separate method. 
     */
    private void validatePurgeArchiveSettings() {
        // Disable all PurgeArchiveDisplayItems in map.
        Set<String> keySet = purgeArchiveItemsMap.keySet();
        for (String key : keySet) {
            PurgeArchiveDisplayItem padi = purgeArchiveItemsMap.get(key);
            padi.setPurgeValue("-1000");
            if (padi.getArchiveValue() != null) {
                padi.setArchiveValue("-1000");
            }
            purgeArchiveItemsMap.put(key, padi);
        }

        // Update purge settings in map
        for (int i = 0; purgeKeys != null && i < purgeKeys.length; i++) {
            PurgeArchiveDisplayItem padi = new PurgeArchiveDisplayItem();
            padi.setName(purgeKeys[i]);
            // If purge is enabled, take what's in the box. Otherwise set it
            // to -1.
            padi.setPurgeValue(purgeValueTextboxes[i]);
            purgeArchiveItemsMap.put(purgeKeys[i], padi);
        }

        // Update archive settings in map
        for (int i = 0; archiveKeys != null && i < archiveKeys.length; i++) {
            PurgeArchiveDisplayItem padi = purgeArchiveItemsMap
                .get(archiveKeys[i]);
            padi.setArchiveValue(archiveValueTextboxes[i]);
            // If purge is enabled, take what's in the box.
            padi.setArchiveValue(archiveValueTextboxes[i]);
            purgeArchiveItemsMap.put(archiveKeys[i], padi);
        }

        // Validate the fields
        purgeArchiveItems.clear();
        String[] messageArgs = new String[2];
        purgeArchiveItems.addAll(purgeArchiveItemsMap.values());
        List<String> errors = null;
        for (PurgeArchiveDisplayItem padi : purgeArchiveItems) {
            errors = new ArrayList<String>();
            // Validate the archive value
            if (padi.getArchiveValue() != null) {
                int archiveValue;
                try {
                    archiveValue = Integer.parseInt(padi.getArchiveValue());
                } catch (NumberFormatException nfe) {
                    archiveValue = -1;
                }
                //TODO pass archive min to "0" via cast
                if ((archiveValue < PURGE_ARCHIVE_MIN || archiveValue > PURGE_ARCHIVE_MAX)
                    && archiveValue != -1000) {
                    messageArgs[0] = "0";
                    messageArgs[1] = "9999";
                    errors.add(getText("errors.range.args", messageArgs));
                    padi.setArchiveErrorMessages(errors);
                    addFieldError("archiveTextboxes", "errors.range");
                }
            }
            errors = new ArrayList<String>();
            // Validate the purge value
            if (padi.getPurgeValue() != null) {
                int purgeValue;
                int archiveValue;
                try {
                    purgeValue = Integer.parseInt(padi.getPurgeValue());
                } catch (NumberFormatException nfe) {
                    purgeValue = -1;
                }
                try {
                    archiveValue = Integer.parseInt(padi.getArchiveValue());
                } catch (NumberFormatException nfe) {
                    archiveValue = -1;
                }
                // If archive doesn't exist or is not in the range of 0-9999
                if ((padi.getArchiveValue() == null || archiveValue <= PURGE_ARCHIVE_MIN)
                    && (purgeValue < PURGE_ARCHIVE_MIN || purgeValue > PURGE_ARCHIVE_MAX)
                    && purgeValue != -1000) {
                    messageArgs[0] = "0";
                    messageArgs[1] = "9999";
                    errors.add(getText("errors.range.args", messageArgs));
                    padi.setPurgeErrorMessages(errors);
                    addFieldError("purgeTextboxes", "errors.range");
                    // If archve does exist, our purge range is 0-7
                    //TODO replace 0 and 7 with variables controlled from one location.
                } else if ((padi.getArchiveValue() != null && archiveValue > PURGE_ARCHIVE_MIN)
                    && (purgeValue < PURGE_ARCHIVE_MIN || purgeValue > PURGE_WITH_ARCHIVE_ENABLED_MAX)) {
                    messageArgs[0] = "0";
                    messageArgs[1] = "7";
                    errors.add(getText(
                        "errors.range.args", "0", messageArgs));
                    padi.setPurgeErrorMessages(errors);
                    addFieldError("purgeTextboxes", "errors.range");
                }
            }
            purgeArchiveItemsMap.put(padi.getName(), padi);
        }
    }

    /**
     * Validation for the directory server authentication stuff. 
     * What we really want to do here is require a password only if the 
     * data has been changed. 
     */
    private void validateDirectoryServerAuthentication() {
        
        if (Boolean.valueOf(hiddenDirectoryServerAuthenticationRequired)) {
            
            // fields Not required, but we need to get the values to see if they changed at all.
            String searchUserName = getDirectoryServerSearchUserName();
            String searchUserPassword = getDirectoryServerSearchPassword();
            // Conditionally required field
            String verifiedPassword = getDirectoryServerSearchPasswordVerified();
            

            String host = getRequiredField(getDirectoryServerHost(), 
                            "directoryServerHost", 
                            "systemConfiguration.dirauth.host", 
                            "systemConfiguration.errors.required");
            String port = getRequiredField(getDirectoryServerPort(), 
                            "directoryServerPort",
                            "systemConfiguration.dirauth.port",
                            "systemConfiguration.errors.required");
            String searchBase = getRequiredField(getDirectoryServerSearchBase(),
                            "directoryServerSearchBase",
                            "systemConfiguration.dirauth.searchBase",
                            "systemConfiguration.errors.required");
            String searchableAttribute = getRequiredField(getDirectoryServerSearchableAttribute(),
                            "directoryServerSearchableAttribute",
                            "systemConfiguration.dirauth.searchableAttribute",
                            "systemConfiguration.errors.required"
            );
            // If they change any of the dir auth data or entered a password, check the password.
            if (systemPropertyValueChanged(host, DIRSERV_HOST)
                            || systemPropertyValueChanged(port, DIRSERV_PORT)
                            || systemPropertyValueChanged(searchBase, DIRSERV_SEARCH_BASE)
                            || systemPropertyValueChanged(searchableAttribute, DIRSERV_SEARCHABLE_ATTRIBUTE)
                            || systemPropertyValueChanged(searchUserName, DIRSERV_SEARCH_USERNAME)
                            || !StringUtil.isNullOrBlank(searchUserPassword)) {
                if (StringUtil.isNullOrBlank(searchUserName)) {
                    // If we don't have a search user name defined - then we
                    // can't have password defined either
                    if ((searchUserPassword != null && searchUserPassword
                                    .length() > 0)
                                    || (verifiedPassword != null && verifiedPassword
                                                    .length() > 0)) {
                        addFieldError(
                                        "directoryServerSearchUserName",
                                        getText("systemConfiguration.dirauth.impropersearchuser"));
                    }
                } else {
                    // If we have a search user name defined - then we must have
                    // a password defined as well
                    searchUserPassword = getRequiredField(getDirectoryServerSearchPassword(),
                                                          "directoryServerSearchUserName",
                                                          null,
                                                          "systemConfiguration.dirauth.impropersearchuser"
                    );
                    // If they just changed a value, we don't want to trigger 2 identical errors...
                    if (!StringUtil.isNullOrBlank(searchUserPassword)) {
                        verifiedPassword = getRequiredField(getDirectoryServerSearchPasswordVerified(),
                                                            "directoryServerSearchUserName",
                                                            null,
                                                            "systemConfiguration.dirauth.impropersearchuser"
                        );
                    }
                }

                if ((searchUserPassword != null) && (verifiedPassword != null)
                                && !searchUserPassword.equals(verifiedPassword)) {
                    addFieldError(
                                    "directoryServerSearchPasswordVerified",
                                    getText("user.create.error.passwordMatch"));
                }
            } else {
                // need to restore passwords... the page saves 'em as empty if we didn't edit anything in this section
                // we need to make sure the property is not erased from the DB.
                SystemProperty property = properties.get(DIRSERV_SEARCH_PASSWORD);
                if (property != null) {
                    setDirectoryServerSearchPassword(property.getValue());
                    setDirectoryServerSearchPasswordVerified(property.getValue());
                }

            }
        } 
    }

    /**
     * Validation for the SMTP stuff. What we really want to do here is require a password only if the 
     * data has been changed. 
     */
    private void validateSmtpAuthentication() {
        if (Boolean.valueOf(hiddenSmtpAuthenticationRequired)) {
            
            String pass = getSmtpPassword();
            String passVerified = getSmtpPasswordVerified();
            String host = getRequiredField(getSmtpHost(), 
                            "smtpHost", 
                            "systemConfiguration.smtp.host", 
                            "systemConfiguration.errors.required");
            String user = getRequiredField(getSmtpUserName(), 
                            "smtpUserName", 
                            "systemConfiguration.smtp.user.name",
                            "systemConfiguration.errors.required");
            
            // If they changed any values or entered a password, we need to have the password & 
            // verified password fields complete.
            if (systemPropertyValueChanged(host, SMTP_HOST) 
                            || systemPropertyValueChanged(user, SMTP_USERNAME)
                            || !StringUtil.isNullOrBlank(pass)) {

                pass = getRequiredField(getSmtpPassword(),
                                "smtpPassword",
                                "systemConfiguration.smtp.password",
                                "systemConfiguration.errors.required"
                );
                // If they changed the host or name, we don't want to trigger 2 identical errors.
                    passVerified = getRequiredField(getSmtpPasswordVerified(), 
                                    "smtpPasswordVerified",
                                    "systemConfiguration.smtp.password.verified",
                                    "systemConfiguration.errors.required"
                    );
                if ((pass != null) && (passVerified != null) && !pass.equals(passVerified)) {
                    addFieldError(
                                    "smtpPasswordVerified",
                                    getText("user.create.error.passwordMatch"));
                }
            } else {
                // need to restore passwords... the page saves 'em as empty if we didn't edit anything in this section
                // we need to make sure the property is not erased from the DB.
                SystemProperty property = properties.get(SMTP_PASSWORD);
                if (property != null) {
                    setSmtpPassword(property.getValue());
                    setSmtpPasswordVerified(property.getValue());
                }

            }

        }
    }
    
    /** 
     * Validates other configuration.
     */
    private void validateOtherConfiguration() {
        // Checks that the battery last used limit is not null and in the valid range.
        if (getShowOtherConfiguration() != null  && getShowOtherConfiguration()) {
            
            if (((getBatteryLastUsedLimit() == null) 
                    || (getBatteryLastUsedLimit() < BATTERY_LAST_USED_LIMIT_MIN) 
                    || (getBatteryLastUsedLimit() > BATTERY_LAST_USED_LIMIT_MAX))
                    && getShowBatteryConfig()) {
                // Creates a String array holding the min and max values for use in the error message.
                String[] keyArgs = {String.valueOf(BATTERY_LAST_USED_LIMIT_MIN), 
                        String.valueOf(BATTERY_LAST_USED_LIMIT_MAX)};
                
                addFieldError(
                        "batteryLastUsedLimit",
                        getText("errors.range.args", keyArgs));
            }
            
            if (getShowVLOtherConfig()
                && !StringUtil.isNullOrEmpty(getRegionIntervals())
                && !getRegionIntervals().matches("(\\d+)(,\\s*\\d+)*")) {
                String[] example = {"1,2,3"};
                addFieldError("regionIntervals",
                    getText("error.input.pattern.invalid", example));
            }
        }  
    }
    
    /** 
     * Check to see if the value passed in matches the value in the System Properties table.
     * @param newValue the new value for the property
     * @param propertyName name of the property to look up
     * @return true if the value changed.
     */
    private boolean systemPropertyValueChanged(String newValue, String propertyName) {
        SystemProperty property = properties.get(propertyName);
        
        boolean valueChanged = false;
        
        if (null == property && !StringUtil.isNullOrBlank(newValue)) {
            //The original property does not exist but the new property does.
            valueChanged = true;
        } else if (StringUtil.isNullOrBlank(property.getValue()) && !StringUtil.isNullOrBlank(newValue)) {
            //The original property is blank but the new property's value is not blank.
            valueChanged = true;
        } else if (!StringUtil.isNullOrBlank(property.getValue()) && !property.getValue().equals(newValue)) {
            // The original property has a value, but the new property does not match the original value.
            valueChanged = true;
        }
        
        return valueChanged;
    }

    /**
     * Return the required field value passed in. Adds a field error if the field is empty or null.
     * @param requiredField data for the required field
     * @param fieldNameProperty name of the field, for reporting missing data
     * @param parameterText usually the field name, added to the error message
     * @param errorText the error message
     * @return requiredField
     */
    private String getRequiredField(String requiredField, String fieldNameProperty, 
                                    String parameterText, String errorText) {
        if (StringUtil.isNullOrEmpty(requiredField)) {
            ArrayList<String> parameters = new ArrayList<String>();
            if (null != parameterText) {
                parameters.add(getText(parameterText));
            }
            addFieldError(fieldNameProperty, getText(
                errorText, parameters));

        }
        return requiredField;
    }

    /**
     * To get whether the entered user id exists in the directory server.
     * 
     * @return String
     * @throws DataAccessException on database failure.
     * @throws JSONException on JSON failures
     */
    public String numberOfUsersAuthenticatingAgainstDirectoryServer()
        throws DataAccessException, JSONException {
        SiteContextHolder.getSiteContext().setFilterBySite(false);

        int tmpNum = userManager.countDirectoryServerAuthenticatingUsers();
        ArrayList<String> parameters = new ArrayList<String>();
        parameters.add(Integer.toString(tmpNum));

        String displayString = getText(
            "systemConfiguration.dirauth.usersimpactedbychange", parameters);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value", displayString);

        setMessage(jsonObject.toString());
        return SUCCESS;
    }

    /**
     * To determine if a connection to the directory server can be established.
     * @return String representing the successful connection or any error
     *         messages we can make intelligently
     * @throws DataAccessException on database failure
     * @throws JSONException on JSON failures
     */
    public String testConnectionToDirectoryServer() throws DataAccessException,
        JSONException {
        DefaultInitialDirContextFactory context = new DefaultInitialDirContextFactory(
            "ldap://" + getDirectoryServerHost() + ":"
                + getDirectoryServerPort());

        context.setInitialContextFactory("com.sun.jndi.ldap.LdapCtxFactory");

        if (getDirectoryServerSearchUserName() != null
            && getDirectoryServerSearchUserName().length() > 0) {
            // We can support anonymous bind - so only set the manager DN
            // and password if they are defined
            context.setManagerDn(getDirectoryServerSearchUserName());
            context.setManagerPassword(getDirectoryServerSearchPassword());
        }
        
        Map<String, String> vars = new HashMap<String, String>();
        if (isDirectoryServerUseSSL()) {
            vars.put(Context.SECURITY_PROTOCOL, "ssl");
            vars.put("java.naming.ldap.factory.socket", "com.vocollect.epp.eap.CertificateSocketFactory");
        }

        // Application runs indefinetly, if its unable to connect to the
        // ldap server (no ping / powered down)
        // or if the ldap connection parameters were wrong.
        // 5 secs timeout will avoid the same.
        vars.put("java.naming.referral", "follow");
        vars.put("com.sun.jndi.ldap.connect.timeout", "5000");
        context.setExtraEnvVars(vars);

        FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch(
            getDirectoryServerSearchBase(), "("
                + getDirectoryServerSearchableAttribute() + "={0})", context);
        userSearch.setSearchSubtree(true);

        JSONObject jsonObject = new JSONObject();

        try {
            if (getDirectoryServerTestUser() != null
                && getDirectoryServerTestUser().length() > 0) {
                userSearch.searchForUser(getDirectoryServerTestUser());
            } else {
                userSearch.searchForUser(null);
            }
            jsonObject.put(
                "value",
                getText("systemConfiguration.dirauth.successfulconnection"));
            jsonObject.put("connected", "true");
            setMessage(jsonObject.toString());
        } catch (UsernameNotFoundException unfe) {
            if (getDirectoryServerTestUser() != null
                && getDirectoryServerTestUser().length() > 0) {
                jsonObject
                    .put(
                        "value",
                        getText("systemConfiguration.dirauth.unsuccessfulconnection.usernotfound"));
                jsonObject.put("connected", "false");
                setMessage(jsonObject.toString());
            } else {
                // Even though we get a UserNameNotFoundException - that's ok -
                // that means
                // the search was executed
                jsonObject
                    .put(
                        "value",
                        getText("systemConfiguration.dirauth.successfulconnection"));
                jsonObject.put("connected", "true");
                setMessage(jsonObject.toString());
            }
        } catch (BadCredentialsException bce) {
            jsonObject
                .put(
                    "value",
                    getText("systemConfiguration.dirauth.unsuccessfulconnection.badcredentials"));
            jsonObject.put("connected", "false");
            setMessage(jsonObject.toString());
        } catch (LdapDataAccessException lae) {
            jsonObject
                .put(
                    "value",
                    getText("systemConfiguration.dirauth.unsuccessfulconnection.connectionproblem"));
            jsonObject.put("connected", "false");
            setMessage(jsonObject.toString());
        } catch (Exception e) {
            jsonObject.put(
                "value",
                getText("systemConfiguration.dirauth.unsuccessfulconnection"));
            jsonObject.put("connected", "false");
            setMessage(jsonObject.toString());
        }

        return SUCCESS;
    }

    /**
     * Refuse to allow viewing of administrator role page. {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    /**
     * @return whether or not authentication to SMTP host is required.
     */
    public boolean isSmtpAuthenticationRequired() {
        return smtpAuthenticationRequired;
    }

    /**
     * @param authenticationRequired whether or not authentication to SMTP host
     *            is required.
     */
    public void setSmtpAuthenticationRequired(boolean authenticationRequired) {
        this.smtpAuthenticationRequired = authenticationRequired;
    }

    /**
     * @return the SMTP Host string.
     */
    public String getSmtpHost() {
        return smtpHost;
    }

    /**
     * @param smtpHost the SMTP host value.
     */
    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    /**
     * @return the SMTP password.
     */
    public String getSmtpPassword() {
        return smtpPassword;
    }

    /**
     * @param smtpPassword the new SMTP password.
     */
    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    /**
     * @return the SMTP user name
     */
    public String getSmtpUserName() {
        return smtpUserName;
    }

    /**
     * @param smtpUserName the new SMTP user name.
     */
    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    /**
     * @return whether or not hidden authentication to the SMTP host is
     *         required.
     */
    public String getHiddenSmtpAuthenticationRequired() {
        return hiddenSmtpAuthenticationRequired;
    }

    /**
     * @param hiddenAuthenticationRequired whether or not hidden authentication
     *            to the SMTP host is required.
     */
    public void setHiddenSmtpAuthenticationRequired(String hiddenAuthenticationRequired) {
        this.hiddenSmtpAuthenticationRequired = hiddenAuthenticationRequired;
    }

    /**
     * Getter for the directoryServerAuthenticationRequired property.
     * @return boolean value of the property
     */
    public boolean isDirectoryServerAuthenticationRequired() {
        return this.directoryServerAuthenticationRequired;
    }

    /**
     * Setter for the directoryServerAuthenticationRequired property.
     * @param directoryServerAuthenticationRequired the new
     *            directoryServerAuthenticationRequired value
     */
    public void setDirectoryServerAuthenticationRequired(boolean directoryServerAuthenticationRequired) {
        this.directoryServerAuthenticationRequired = directoryServerAuthenticationRequired;
    }

    /**
     * Getter for the directoryServerHost property.
     * @return String value of the property
     */
    public String getDirectoryServerHost() {
        return this.directoryServerHost;
    }

    /**
     * Setter for the directoryServerHost property.
     * @param directoryServerHost the new directoryServerHost value
     */
    public void setDirectoryServerHost(String directoryServerHost) {
        this.directoryServerHost = directoryServerHost;
    }

    /**
     * Getter for the directoryServerPort property.
     * @return String value of the property
     */
    public String getDirectoryServerPort() {
        return this.directoryServerPort;
    }

    /**
     * Setter for the directoryServerPort property.
     * @param directoryServerPort the new directoryServerPort value
     */
    public void setDirectoryServerPort(String directoryServerPort) {
        this.directoryServerPort = directoryServerPort;
    }

    /**
     * Getter for the directoryServerSearchableAttribute property.
     * @return String value of the property
     */
    public String getDirectoryServerSearchableAttribute() {
        return this.directoryServerSearchableAttribute;
    }

    /**
     * Setter for the directoryServerSearchableAttribute property.
     * @param directoryServerSearchableAttribute the new
     *            directoryServerSearchableAttribute value
     */
    public void setDirectoryServerSearchableAttribute(String directoryServerSearchableAttribute) {
        this.directoryServerSearchableAttribute = directoryServerSearchableAttribute;
    }

    /**
     * Getter for the directoryServerSearchBase property.
     * @return String value of the property
     */
    public String getDirectoryServerSearchBase() {
        return this.directoryServerSearchBase;
    }

    /**
     * Setter for the directoryServerSearchBase property.
     * @param directoryServerSearchBase the new directoryServerSearchBase value
     */
    public void setDirectoryServerSearchBase(String directoryServerSearchBase) {
        this.directoryServerSearchBase = directoryServerSearchBase;
    }

    /**
     * Getter for the directoryServerSearchPassword property.
     * @return String value of the property
     */
    public String getDirectoryServerSearchPassword() {
        return this.directoryServerSearchPassword;
    }

    /**
     * Setter for the directoryServerSearchPassword property.
     * @param directoryServerSearchPassword the new
     *            directoryServerSearchPassword value
     */
    public void setDirectoryServerSearchPassword(String directoryServerSearchPassword) {
        this.directoryServerSearchPassword = directoryServerSearchPassword;
    }

    /**
     * Getter for the directoryServerSearchPasswordVerified property.
     * @return String value of the property
     */
    public String getDirectoryServerSearchPasswordVerified() {
        return this.directoryServerSearchPasswordVerified;
    }

    /**
     * Setter for the directoryServerSearchPasswordVerified property.
     * @param directoryServerSearchPasswordVerified the new
     *            directoryServerSearchPasswordVerified value
     */
    public void setDirectoryServerSearchPasswordVerified(String directoryServerSearchPasswordVerified) {
        this.directoryServerSearchPasswordVerified = directoryServerSearchPasswordVerified;
    }

    /**
     * Getter for the directoryServerSearchUserName property.
     * @return String value of the property
     */
    public String getDirectoryServerSearchUserName() {
        return this.directoryServerSearchUserName;
    }

    /**
     * Setter for the directoryServerSearchUserName property.
     * @param directoryServerSearchUserName the new
     *            directoryServerSearchUserName value
     */
    public void setDirectoryServerSearchUserName(String directoryServerSearchUserName) {
        this.directoryServerSearchUserName = directoryServerSearchUserName;
    }

    /**
     * Getter for the hiddenDirectoryServerAuthenticationRequired property.
     * @return String value of the property
     */
    public String getHiddenDirectoryServerAuthenticationRequired() {
        return this.hiddenDirectoryServerAuthenticationRequired;
    }

    /**
     * Setter for the hiddenDirectoryServerAuthenticationRequired property.
     * @param hiddenDirectoryServerAuthenticationRequired the new
     *            hiddenDirectoryServerAuthenticationRequired value
     */
    public void setHiddenDirectoryServerAuthenticationRequired(String hiddenDirectoryServerAuthenticationRequired) {
        this.hiddenDirectoryServerAuthenticationRequired = hiddenDirectoryServerAuthenticationRequired;
    }

    /**
     * Getter for the smtpPasswordVerified property.
     * @return String value of the property
     */
    public String getSmtpPasswordVerified() {
        return this.smtpPasswordVerified;
    }

    /**
     * Setter for the smtpPasswordVerified property.
     * @param smtpPasswordVerified the new smtpPasswordVerified value
     */
    public void setSmtpPasswordVerified(String smtpPasswordVerified) {
        this.smtpPasswordVerified = smtpPasswordVerified;
    }

    /**
     * Getter for the message property.
     * @return String value of the property
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Setter for the message property.
     * @param message the new message value
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return this.userManager;
    }

    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * @return the test user name
     */
    public String getDirectoryServerTestUser() {
        return directoryServerTestUser;
    }

    /**
     * @param directoryServerTestUser the the test user name
     */
    public void setDirectoryServerTestUser(String directoryServerTestUser) {
        this.directoryServerTestUser = directoryServerTestUser;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * Getter for the archiveKeys property.
     * @return String[] value of the property
     */
    public String[] getArchiveKeys() {
        return this.archiveKeys;
    }

    /**
     * Setter for the archiveKeys property.
     * @param archiveKeys the new archiveKeys value
     */
    public void setArchiveKeys(String[] archiveKeys) {
        this.archiveKeys = archiveKeys;
    }

    /**
     * Getter for the archiveValueTextboxes property.
     * @return String[] value of the property
     */
    public String[] getArchiveValueTextboxes() {
        return this.archiveValueTextboxes;
    }

    /**
     * Setter for the archiveValueTextboxes property.
     * @param archiveValueTextboxes the new archiveValueTextboxes value
     */
    public void setArchiveValueTextboxes(String[] archiveValueTextboxes) {
        this.archiveValueTextboxes = archiveValueTextboxes;
    }

    /**
     * Getter for the purgeArchiveItems property.
     * @return List of PurgeArchiveDisplayItem objects.
     */
    public List<PurgeArchiveDisplayItem> getPurgeArchiveItems() {
        return this.purgeArchiveItems;
    }

    /**
     * Setter for the purgeArchiveItems property.
     * @param purgeArchiveItems the new purgeArchiveItems value
     */
    public void setPurgeArchiveItems(List<PurgeArchiveDisplayItem> purgeArchiveItems) {
        this.purgeArchiveItems = purgeArchiveItems;
    }

    /**
     * Getter for the purgeKeys property.
     * @return String[] value of the property
     */
    public String[] getPurgeKeys() {
        return this.purgeKeys;
    }

    /**
     * Setter for the purgeKeys property.
     * @param purgeKeys the new purgeKeys value
     */
    public void setPurgeKeys(String[] purgeKeys) {
        this.purgeKeys = purgeKeys;
    }

    /**
     * Getter for the purgeValueTextboxes property.
     * @return String[] value of the property
     */
    public String[] getPurgeValueTextboxes() {
        return this.purgeValueTextboxes;
    }

    /**
     * Setter for the purgeValueTextboxes property.
     * @param purgeValueTextboxes the new purgeValueTextboxes value
     */
    public void setPurgeValueTextboxes(String[] purgeValueTextboxes) {
        this.purgeValueTextboxes = purgeValueTextboxes;
    }
    
    /**
     * @return the site name of the current user's current site
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }

    /**
     * @return directoryServerUseSSL
     */
    public boolean isDirectoryServerUseSSL() {
        return directoryServerUseSSL;
    }

    /**
     * @param directoryServerUseSSL .
     */
    public void setDirectoryServerUseSSL(boolean directoryServerUseSSL) {
        this.directoryServerUseSSL = directoryServerUseSSL;
    }

    /**
     * @return the batteryLastUsedLimit
     */
    public Integer getBatteryLastUsedLimit() {
        return batteryLastUsedLimit;
    }

    /**
     * @param batteryLastUsedLimit the batteryLastUsedLimit to set
     */
    public void setBatteryLastUsedLimit(Integer batteryLastUsedLimit) {
        this.batteryLastUsedLimit = batteryLastUsedLimit;
    }

    /**
     * @return the showOtherConfiguration
     */
    public Boolean getShowOtherConfiguration() {
        if (this.showOtherConfiguration == null) {
            //TODO: If VoiceLink ever wants to use the other configuration section,
            // they'll need to modify this logic so it shows up in their product.
            showOtherConfiguration = getShowBatteryConfig() || getShowVLOtherConfig();
        }
        return showOtherConfiguration;
    }

    /**
     * @param showOtherConfiguration the showOtherConfiguration to set
     */
    public void setShowOtherConfiguration(Boolean showOtherConfiguration) {
        this.showOtherConfiguration = showOtherConfiguration;
    }

    /**
     * @return the showBatteryConfig
     */
    public Boolean getShowBatteryConfig() {
        if (this.showBatteryConfig == null) {
            try {
                SystemProperty batteryConfig = 
                    getSystemPropertyManager().findByName(BATTERY_LAST_USED_LIMIT_PROPERTY);
                if (batteryConfig != null) {
                    setShowBatteryConfig(Boolean.TRUE);
                } else {
                    setShowBatteryConfig(Boolean.FALSE);
                }
            } catch (DataAccessException dae) {
                setShowBatteryConfig(Boolean.FALSE);
            }
        }
        return showBatteryConfig;
    }

    /**
     * @param showBatteryConfig the showBatteryConfig to set
     */
    public void setShowBatteryConfig(Boolean showBatteryConfig) {
        this.showBatteryConfig = showBatteryConfig;
    }

    
    /**
     * @return the showVLOtherConfig
     */
    public Boolean getShowVLOtherConfig() {
        if (this.showVLOtherConfig == null) {
            try {
                SystemProperty regionIntervalConfig = getSystemPropertyManager()
                    .findByName(REGION_INTERVAL_CONFIGURATION);
                setShowVLOtherConfig(regionIntervalConfig != null);
            } catch (DataAccessException dae) {
                return Boolean.FALSE;
            }
        }
        return showVLOtherConfig;
    }

    
    /**
     * @param showVLOtherConfig the showVLOtherConfig to set
     */
    public void setShowVLOtherConfig(Boolean showVLOtherConfig) {
        this.showVLOtherConfig = showVLOtherConfig;
    }

    /**
     * @return the regionIntervalConfig
     */
    public String getRegionIntervals() {
        return regionIntervals;
    }

    
    /**
     * @param regionIntervals the regionIntervalConfig to set
     */
    public void setRegionIntervals(String regionIntervals) {
        this.regionIntervals = regionIntervals;
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 