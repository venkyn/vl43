/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.ReportError;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.ReportException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.ReportType;
import com.vocollect.epp.model.ReportTypeParameter;
import com.vocollect.epp.model.ReportParameterFieldType;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.service.ReportDataSourceManager;
import com.vocollect.epp.service.ReportTypeManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.LabelValuePair;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import static com.vocollect.epp.model.ReportParameterFieldType.DropDownWithAll;
import static com.vocollect.epp.model.ReportParameterFieldType.DropDownWithoutAll;
import static com.vocollect.epp.model.ReportParameterFieldType.TextField;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.util.ServletContextAware;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleObjectStateException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.FontKey;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.PdfFont;
import net.sf.jasperreports.engine.query.JRHibernateQueryExecuterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.j2ee.servlets.ImageServlet;



/**
 * Handle the parameter querying and rendering of JasperReports. Action links
 * may be created from anywhere in the application, and will be handled by this
 * class.
 * 
 * We must implement a couple *Aware interfaces to support the multi-threading
 * execAndWait interceptor, which displays the Wait dialog; otherwise, the
 * getRequest() method will return null.
 * 
 * Note that ServletRequestAware only seems to work sporradically for accessing
 * request parameters. ParameterAware provides consistent results.
 * 
 * IMPORTANT NOTE: Some logic was added late in the game to use this class to
 * print reports directly from a task. This was then aborted in favor of using a
 * ReportPrinterManager, but the print direct logic was not taken out for fear
 * of risking the release. The print direct logic can be safelySITE taken out
 * (and should be taken out) in a later release.
 * 
 * @author cblake
 */
public class ReportTypeActionRoot extends SiteEnabledAction
    implements ServletContextAware, ParameterAware, ApplicationContextAware {

    //
    private static final long serialVersionUID = -7968610364930575081L;

    /**
     * Indication of what format the report will be rendered in.
     * 
     * @author cblake
     */
    public enum RenderFormat {
        HTML, PDF, RTF, XLS
    }


    // String representing selection of "All" option in drop-down list
    private static final String PARAM_LIST_ALL = "_ALL_";

    private static final int BUFFER_SIZE = 10240;

    public static final String DATE_FORMAT = "yyyy-M-d";

    public static final String LIST_ALL_DESCRIPTION = "report.parameter.all";

    /**
     * System property name for the default render format.
     */
    public static final String REPORT_DEFAULT_RENDER_FORMAT = "report.DefaultRenderFormat";

    public static final String REPORT_PATH_BASE = "/WEB-INF/classes/reports/";

    public static final String REPORT_NAMESPACE_ROOT = "reports";

    public static final String REPORT_SOURCE_EXTENSION = ".jrxml";

    public static final String REPORT_RESOURCE_BUNDLE = "resources/ApplicationResources-Reports";
    public static final String PARAM_CURRENT_SITE_CONTEXT = "VOC_CURRENT_SITE_CONTEXT";

    public static final String PARAM_SITE = "SITE";

    public static final String SITE_LOCALIZATION_KEY = "report.site";

    public static final String PARAM_CURRENT_USER = "VOC_CURRENT_USER";

    public static final Set<String> TEMPLATE_PARAMETERS;

    public static final String PARAM_RENDER_FORMAT = "RENDER_FORMAT";

    public static final String PDF_STANDARD_FONT = "Helvetica";

    // Standard font encoding for PDFs is western european / Windows ANSI
    public static final String PDF_STANDARD_ENCODING = "CP1252";

    /** Localization key for the render format description. */
    public static final String RENDER_FORMAT_DESCRIPTION = "report.renderformat.description";

    public static final List<LabelValuePair> RENDER_FORMATS;
    static {
        TEMPLATE_PARAMETERS = new HashSet<String>();
        TEMPLATE_PARAMETERS.add(PARAM_CURRENT_SITE_CONTEXT);
        TEMPLATE_PARAMETERS.add(PARAM_CURRENT_USER);
        RENDER_FORMATS = new ArrayList<LabelValuePair>();
        RENDER_FORMATS.add(new LabelValuePair(
            RenderFormat.HTML.name(), RenderFormat.HTML.name()));
        RENDER_FORMATS.add(new LabelValuePair(
            RenderFormat.PDF.name(), RenderFormat.PDF.name()));
        RENDER_FORMATS.add(new LabelValuePair("RTF", RenderFormat.RTF.name()));
        RENDER_FORMATS.add(new LabelValuePair("XLS", RenderFormat.XLS.name()));
    }

    private static Logger log = new Logger(ReportTypeActionRoot.class);

    // Map of parameter name to JasperReports parameter
    private Map<String, JRParameter> reportTypeParameters;

    private List<ReportTypeParameter> processedParameters;

    private RenderFormat renderFormat;

    // Note: these manager variables must be static, as Spring
    // cannot inject properly when the code is run in another thread.
    // Also note, we must use our parent's getSiteManager method;
    // using spring injection in here will cause a conflict and
    // may result in a strange Freemarker error.
    private static ReportTypeManager reportManager;

    private static SystemPropertyManager systemPropertyManager;

    private static UserManager userManager;

    private static ReportUtilities reportUtil;
    
    // Querystring: name of report to load
    private String report;

    private String reportTypeName;

    private String reportTypeNameKey;

    private String sessionBeanId;

    private String reportNamespace;

    private String webResultPath;

    private String userName;

    private String siteName;

    private static long siteId;

    private JasperDesign reportDesign;

    private String errorMessage;

    private InputStream reportStream;

    private ServletContext servletContext;

    private Map parameterMap;

    private static Map<String, String> enumValues;

    /**
     * Used for pulling in Spring Configuration to make service calls.
     */
    private ApplicationContext applicationContext;

    private boolean isPrintDirect = false;

    private String printDirectParameter = null;

    private String path = null;

    private SessionFactory sessionFactory;

    private String reportErrorMessage;
    

    
    
    /**
     * Getter for the reportErrorMessage property.
     * @return String value of the property
     */
    public String getReportErrorMessage() {
        return reportErrorMessage;
    }


    
    /**
     * Setter for the reportErrorMessage property.
     * @param reportErrorMessage the new reportErrorMessage value
     */
    public void setReportErrorMessage(String reportErrorMessage) {
        this.reportErrorMessage = reportErrorMessage;
    }


    /**
     * Initialize application context, so we can load an arbitrary bean in order
     * to invoke service calls, which help us build drop-down lists.
     * Constructor.
     */
    public ReportTypeActionRoot() {

    }

    /**
     * Getter for the applicationContext property.
     * @return ApplicationContext value of the property
     */
    public ApplicationContext getApplicationContext() {
        return this.applicationContext;

    }

    /**
     * Setter for the applicationContext property. Set by
     * ApplicationContextAware by Struts.
     * @param applicationContext the new applicationContext value
     */
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /**
     * Getter for the processedParameters property.
     * @return List ReportParameter value of the property
     */
    public List<ReportTypeParameter> getProcessedParameters() {
        return this.processedParameters;
    }

    /**
     * Getter for the renderFormat property.
     * @return RenderFormat value of the property
     */
    public RenderFormat getRenderFormat() {
        return this.renderFormat;
    }

    /**
     * Getter for the reportManager property.
     * @return ReportManager value of the property
     */
    public ReportTypeManager getReportManager() {
        return reportManager;
    }

    /**
     * Setter for the reportManager property. Injected by Spring. This will only
     * run once because the member variable is static; this way if the action
     * class goes into another thread, the static var will remain.
     * @param rm the new reportManager value
     */
    public void setReportManager(ReportTypeManager rm) {
        reportManager = rm;
    }

    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for the systemPropertyManager property. Injected by Spring. This
     * is a static member variable; see comments on setReportManager
     * @param spm the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager spm) {
        systemPropertyManager = spm;
    }

    /**
     * Getter for the reportName property. This is based on the querystring
     * passed in at runtime.
     * @return String value of the property
     */
    public String getReportName() {
        return this.reportTypeName;
    }

    /**
     * Getter for the report querystring variable.
     * @return The full querystring variable (epp.ReportName)
     */
    public String getReport() {
        return this.report;
    }

    /**
     * Setter for the report querystring variable. Magically called by Struts
     * framework.
     * @param report Querystring
     */
    public void setReport(String report) {
        this.report = report;
    }

    /**
     * Getter for the report type name message key.
     * @return The report type name message key.
     */
    public String getReportTypeNameKey() {
        return this.reportTypeNameKey;
    }

    /**
     * Setter for the for the report type name message key.
     * @param reportNameKey report type name message key
     */
    public void setReportTypeNameKey(String reportTypeNameKey) {
        this.reportTypeNameKey = reportTypeNameKey;
    }

    /**
     * Get the session bean id that will be used to provide data to this report.
     * @return session bean id
     */
    public String getSessionBeanId() {
        return this.sessionBeanId;
    }

    /**
     * @param sessionBeanId session bean id that will be used to provide data to
     *            this report.
     */
    public void setSessionBeanId(String sessionBeanId) {
        this.sessionBeanId = sessionBeanId;
    }

    /**
     * Getter for the webResultPath property. Represents the URL that will be
     * passed into the browser to load the rendered report.
     * @return String value of the property
     */
    public String getWebResultPath() {
        return this.webResultPath;
    }

    /**
     * Getter for the report namespace.
     * @return String value of the property
     */
    public String getReportNamespace() {
        return this.reportNamespace;
    }

    /**
     * Getter for the siteName property. Will be mapped to a hidden HTML
     * variable. This information is easily retrieved on the parameter page,
     * because we have not yet spawned another thread. Will ultimately be used
     * as a report parameter.
     * @return String value of the property
     */
    public String getSiteName() {
        return this.siteName;
    }

    /**
     * Setter for the siteName property.
     * @param siteName the new siteName value
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    /**
     * Getter for the userName property. Will be mapped to a hidden HTML
     * variable. Ultimately a generic report parameter.
     * @return String value of the property
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Setter for the userName property.
     * @param userName the new userName value
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Getter for the errorMessage property. This is a string representing any
     * error from the user input to the parameter screen. It should be rendered
     * on the parameter screen.
     * @return String value of the property
     */
    public String getErrorMessage() {
        return this.errorMessage;
    }

    /**
     * Access to the rendered report data stream.
     * @return InputStream value of the property
     */
    public InputStream getReportStream() {
        return this.reportStream;
    }

    /**
     * Implements the ServletContextAware interface, so we can receive the
     * servlet context in another thread. This is used to calculate the real
     * path for loading the .jrxml file. {@inheritDoc}
     * @see org.apache.struts2.util.ServletContextAware#setServletContext(javax.servlet.ServletContext)
     */
    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    /**
     * Get parameters from Struts via ParameterAware workings. {@inheritDoc}
     * @see org.apache.struts2.interceptor.ParameterAware#setParameters(java.util.Map)
     */
    public void setParameters(Map parameters) {
        this.parameterMap = parameters;
    }

    
    /**
     * Getter for the reportUtil property.
     * @return ReportUtilities value of the property
     */
    public static ReportUtilities getReportUtil() {
        return reportUtil;
    }

    
    /**
     * Setter for the reportUtil property.
     * @param reportUtil the new reportUtil value
     */
    public static void setReportUtil(ReportUtilities reportUtil) {
        ReportTypeActionRoot.reportUtil = reportUtil;
    }

    /**
     * This function extracts the report path from the URL and verifies its
     * existence by attempting to open it. If successful, it will also save
     * parameters related to the report file itself.
     * @return boolean true implies report was found, otherwise false.
     * @throws VocollectException if the report could not be opened
     */
    private boolean findReport() throws VocollectException {

        // Convert URL into an absolute path to the requested report.
        String reportPath = null;
        String absolutePath = null;
        /*
         * path = getPath(); absolutePath = path; // TODO }
         */
        if (!(isPrintDirect)) {
            reportPath = parseReportPath(getRequest().getRequestURL().toString());
            absolutePath = servletContext.getRealPath(REPORT_PATH_BASE + reportPath
                + REPORT_SOURCE_EXTENSION);

        }

        JasperDesign source = null;
        try {
            if (isPrintDirect) {
                source = JRXmlLoader.load(getClass().getClassLoader()
                    .getResourceAsStream(getPath()));
            } else {
                source = JRXmlLoader.load(absolutePath);
            }

        } catch (JRException ex) {
            if (ex.getCause() instanceof FileNotFoundException) {
                // report was not found...
                log.warn("Report not found at: " + absolutePath);
                return false;
            } else {
                // bad file...
                throw new VocollectException(ReportError.JASPER_LOAD_ERROR, ex);
            }
        }

        if (!isPrintDirect) {
            // Get the reportName from the request URL.
            this.reportTypeName = parseReportName(getRequest().getRequestURL()
                .toString());
        }
        // Set the namespace for the report
        this.reportNamespace = reportPath;
        // Hold a reference to the opened report.
        this.reportDesign = source;

        return true;
    }

    /**
     * Given the request url, get an absolute path to the report.
     * @param url containing the report path/name
     * @return Absolute path to file
     */
    public String parseReportPath(String url) {

        int i = url.indexOf(REPORT_NAMESPACE_ROOT)
            + REPORT_NAMESPACE_ROOT.length() + 1;
        int j = url.lastIndexOf("/");

        String reportPath = "";
        try {
            reportPath = url.substring(i, j);
        } catch (StringIndexOutOfBoundsException ex) {
            if (log.isDebugEnabled()) {
                log.debug("Report path not found in " + url);
            }
        }

        return reportPath;
    }

    /**
     * Given the request url, get the report name.
     * @param url containing the report path/name
     * @return Absolute path to file
     */
    public String parseReportName(String url) {

        String reportPath = parseReportPath(url);

        int i = reportPath.lastIndexOf("/");

        String newReportName = reportPath.substring(i + 1);

        if (log.isDebugEnabled()) {
            log.debug("Report name: " + newReportName);
        }

        return newReportName;
    }

    /**
     * Processes parameters; this is the first action when a report is called.
     * We will first make sure the report exists, and we will compile the .jrxml
     * if needed. If the report cannot be found or has an error, we will go to
     * an error page.
     * 
     * Note: The Description and DefaultValue properties must be set in every
     * parameter we put into the procesedParameters collection, as the FTL file
     * is not checking for nulls with them.
     * @return Success
     * @throws VocollectException on any error.
     */
    public String processParameters() throws VocollectException {

        // Find the report...
        if (!findReport()) {
            return ERROR_404_RETURN;
        }
        // Inspect the report's parameters
        JRParameter[] parameterArray = this.reportDesign.getParameters();
        buildParameterMap(parameterArray);

        // Create an empty set of parameters
        processedParameters = new ArrayList<ReportTypeParameter>();
        // Copy the new map into a second variable
        Map<String, JRParameter> unprocessedParams = new HashMap<String, JRParameter>(
            reportTypeParameters);

        // Get the user & site info from current context; will be stored
        // in HTML variables.
        String currentSiteName = SiteContextHolder.getSiteContext().getCurrentSiteName();
        ReportType reportObj = reportManager.getPrimaryDAO().findByName(reportTypeName);
        long reportSiteId = -1;
        // TODO: we need to insert the tagId into the Archive Database to
        // eliminate this fix
        Site currentSite = SiteContextHolder.getSiteContext().getCurrentSite();
        if (reportObj.getSessionBeanId().contains("archive")) {
            reportSiteId = currentSite.getId();
        } else {
            reportSiteId = SiteContextHolder.getSiteContext().getTagBySiteId(currentSite.getId()).getId();
        }

        if (log.isDebugEnabled()) {
            log.debug("Report site tag name: " + currentSiteName);
            log.debug("Report user: " + getCurrentUser().getName());
            log.debug("Language: " + getLocale().getLanguage());
        }
        setSiteName(currentSiteName);
        setUserName(getCurrentUser().getName());
        setSiteId(reportSiteId);

        // Report reportObj =
        // reportManager.getPrimaryDAO().findByName(reportName);
        Set<ReportTypeParameter> dbParams = null;
        // get params from DB
        dbParams = reportObj.getReportTypeParameters();

        setReportTypeNameKey(reportObj.getReportTypeNameKey());
        setSessionBeanId(reportObj.getSessionBeanId());

        ReportTypeParameter newParam = null;
        JRParameter reportParam = null;
        String paramName = null;

        // Loop through the database records, if available. We must
        // do this first, because the database records are sorted by
        // prompt_order column.
        Iterator<ReportTypeParameter> dbIter = dbParams.iterator();
        while (dbIter.hasNext()) {
            // Check for parameter in report
            newParam = dbIter.next();
            paramName = newParam.getParameterName();
            reportParam = reportTypeParameters.get(paramName);
            // Note: we will ignore parameters found in the database
            // but not the report.
            if (reportParam != null) {
                // Update null parameters from the report definition
                fillParameter(reportParam, newParam);

                if (newParam.getFieldType()
                    .isInSet(DropDownWithAll, DropDownWithoutAll)) {
                    // Build a drop-down list of values
                    List<LabelValuePair> values = callServiceMethod(newParam);
                    //newParam.setDropDownData(values);
                }

                processedParameters.add(newParam);
                unprocessedParams.remove(paramName);
            }
        }

        // Loop through the remaining report parameters from the .jrxml file.
        // For each one, build the ReportParameter objects.
        Set<String> parameterNames = unprocessedParams.keySet();
        Iterator<String> reportIter = parameterNames.iterator();
        while (reportIter.hasNext()) {
            paramName = reportIter.next();
            reportParam = unprocessedParams.get(paramName);
            if (reportParam.isSystemDefined()
                || reportParam.getName().equals(PARAM_CURRENT_SITE_CONTEXT)
                || reportParam.getName().equals(PARAM_CURRENT_USER)) {
                // This is a built in parameter or one of our
                // template parameters, so don't display it.
                continue;
            } else if (reportParam.getName().equals(PARAM_SITE)) {
                /*
                 * Added 5/17/2007 by Cooper Blake. The Site parameter is
                 * commonly used in order to display on the parameters page.
                 * This should probably be merged with
                 * PARAM_CURRENT_SITE_CONTEXT but without hiding it. The FTL
                 * already has code to set any parameter named "site" to
                 * read-only. Here, we are using a default localization value to
                 * avoid having to create a database parameter record for every
                 * report.
                 */
                reportParam.setDescription(SITE_LOCALIZATION_KEY);
            }

            newParam = new ReportTypeParameter();
            fillParameter(reportParam, newParam);

            // Add the parameter to our list
            processedParameters.add(newParam);
        }

        // Add render format parameter (will always be prompted for)
        newParam = new ReportTypeParameter();
        newParam.setParameterName(PARAM_RENDER_FORMAT);
        newParam.setDescription(getText(RENDER_FORMAT_DESCRIPTION));
        newParam.setParameterType(String.class);
        newParam.setFieldType(DropDownWithoutAll);
        newParam.setIsRequired(false);
        //newParam.setDropDownData(RENDER_FORMATS);

        String userFormat = "";
        /*
         * for (UserProperty prop : getCurrentUser().getUserProperties()) { if
         * (prop.getName().equals(REPORT_DEFAULT_RENDER_FORMAT)) { userFormat =
         * prop.getValue(); continue; } }
         */

        try {
            List<SystemProperty> systemPropertyList = systemPropertyManager
                .getAll();

            // Build list of rules
            for (SystemProperty sp : systemPropertyList) {
                if (sp.getName().equals(REPORT_DEFAULT_RENDER_FORMAT)) {
                    userFormat = sp.getValue();
                    continue;
                }
            }
            if (StringUtil.isNullOrEmpty(userFormat)) {
                userFormat = RenderFormat.RTF.name();
            }
        } catch (DataAccessException e) {
            log.warn("Error getting Default Report Format from database: " + e
                + "\n");
        }

        userFormat = RenderFormat.RTF.name();
        newParam.setDefaultValue(userFormat);
        processedParameters.add(newParam);

        return SUCCESS;
    }

    /**
     * This will examine the service call fields in the report parameter and use
     * them to build a list of drop-down value pairs. It will attempt this by
     * using Java reflection.
     * @param param The report parameter with service call information.
     * @return The new list of values. Null if input is insufficient.
     * @throws VocollectException An exception will be thrown during any problem
     *             with the Java reflection and method invocation.
     */
    private List<LabelValuePair> callServiceMethod(ReportTypeParameter param)
        throws VocollectException {
        List<LabelValuePair> values = new ArrayList<LabelValuePair>();
        // First, make sure we have all the required fields
        if (param.getServiceName() == null
            || param.getServiceName().length() < 1) {
            return values;
        }

        // Add a special first element representing ALL
        if (param.getFieldType().equals(DropDownWithAll)) {
            values.add(new LabelValuePair(
                getText(LIST_ALL_DESCRIPTION), PARAM_LIST_ALL));
        }

        // Attempt to get a bean from the specified service class & method.
        boolean valid = applicationContext.containsBean(param.getServiceName());
        if (!valid) {
            // throw new VocollectException(ReportError.SERVICE_BEAN_NOT_FOUND);
            return getDropDownValuesFromEnum(param.getServiceName());
        } else if (param.getServiceMethod() == null
            || param.getServiceMethod().length() < 1
            || param.getDisplayMember() == null
            || param.getDisplayMember().length() < 1
            || param.getDataMember() == null
            || param.getDataMember().length() < 1) {

            return values;
        }
        // Try to actually get the bean
        Object service = applicationContext.getBean(param.getServiceName());
        if (service == null) {
            throw new VocollectException(ReportError.SERVICE_BEAN_NULL);
        }
        // Convert bean-style property into a "get" call
        String methodName = getBeanMethod(param.getServiceMethod());

        Method method = null;
        Collection collectionResults = null;
        try {
            method = service.getClass().getMethod(methodName, new Class[] {});
            Object genericResults = method.invoke(service, (Object[]) null);
            collectionResults = (Collection) genericResults;

            // Loop through the collection result
            Iterator iter = collectionResults.iterator();
            LabelValuePair pair = null;
            while (iter.hasNext()) {
                Object item = iter.next();
                methodName = getBeanMethod(param.getDisplayMember());
                // Get display value
                method = item.getClass().getMethod(methodName, (Class[]) null);
                Object displayValObject = method.invoke(item, (Object[]) null);
                String displayVal = null;

                if (displayValObject != null) {
                    displayVal = displayValObject.toString();
                }

                // Get data value
                methodName = getBeanMethod(param.getDataMember());
                method = item.getClass().getMethod(methodName, (Class[]) null);
                Object dataValObject = method.invoke(item, (Object[]) null);
                String dataVal = null; 
                if (dataValObject != null) {
                    dataVal = dataValObject.toString();
                }

                // Create the pair and add to the list
                if (log.isDebugEnabled()) {
                log.debug("Display val : " + displayVal);
                }
                pair = new LabelValuePair(displayVal, dataVal);
                
                if (validSite(item)) {
                    values.add(pair);
                }
            }
        } catch (NoSuchMethodException ex) {
            throw new VocollectException(
                ReportError.SERVICE_METHOD_NOT_FOUND, ex);
        } catch (InvocationTargetException ex) {
            throw new VocollectException(
                ReportError.SERVICE_INVOCATION_ERROR, ex);
        } catch (IllegalAccessException ex) {
            throw new VocollectException(
                ReportError.SERVICE_INVOCATION_ERROR, ex);
        } catch (IllegalArgumentException ex) {
            throw new VocollectException(
                ReportError.SERVICE_INVOCATION_ERROR, ex);
        } catch (ClassCastException ex) {
            throw new VocollectException(ReportError.SERVICE_NO_COLLECTION, ex);
        }
        return values;
    }

    /**
     * Returns the set of values from Enum for the report drop down list.
     * @param serviceName - The enum class name
     * @return The new list of values. Null if input is insufficient.
     * @throws VocollectException An exception will be thrown during any problem
     *             with the Java reflection and method invocation.
     */

    private List<LabelValuePair> getDropDownValuesFromEnum(String serviceName)
        throws VocollectException {
        // TODO Auto-generated method stub
        List<LabelValuePair> values = new ArrayList<LabelValuePair>();
        
        //TODO This needs to be addressed in a better way to prevent breakage if the class name were to change.
        values.add(new LabelValuePair(
            getText(LIST_ALL_DESCRIPTION), PARAM_LIST_ALL));
        Class service = null;
        LabelValuePair pair = null;

        try {
            service = Class.forName(serviceName);
            
            Object[] obj = service.getEnumConstants();
            String name = service.getName();
             int cnt = 1;
            enumValues = new HashMap<String, String>();

            for (Object field : obj) {
                
                String value = ResourceUtil.getLocalizedKeyValue(field.toString());

                if (value.equals("SignOn") || value.equals("SignOff")
                    || value.equals("Break")) {
                    
                    cnt++;
                    continue;
                }
                pair = new LabelValuePair(getText(name + "." + cnt),   getText(name + "." + cnt));
                values.add(pair);
                enumValues.put(getText(name + "." + cnt),  cnt + "");
                cnt++;
            }

        } catch (IllegalArgumentException ex) {
            throw new VocollectException(
                ReportError.SERVICE_INVOCATION_ERROR, ex);
        } catch (ClassNotFoundException ex) {
            throw new VocollectException(ReportError.SERVICE_NO_COLLECTION, ex);
        }
        return values;

    }

    /**
     * Take a bean-style property name ("name") and convert into a method call
     * ("getName").
     * @param beanProperty The property name to convert
     * @return The method name
     */
    private static String getBeanMethod(String beanProperty) {
        // Try to invoke the method, prepending "get" and capitalizing
        // the first letters.
        String methodName = "get"
            + Character.toUpperCase(beanProperty.charAt(0))
            + beanProperty.substring(1);
        return methodName;
    }

    /**
     * Populate a new report parameter with data from the JasperReports
     * parameter object. Assumes that neither object is null. Because a
     * partially populated database object may be passed in here, we will not
     * overwrite fields that are already set. Thus, most values will be
     * retrieved from paramToUpdate first, then updated if needed.
     * @param reportData Parameter data from the JasperReports design
     * @param paramToUpdate Report parameter we will populate.
     */
    private void fillParameter(final JRParameter reportData,
                               ReportTypeParameter paramToUpdate) {
        if (reportData == null || paramToUpdate == null) {
            return;
        }
        // Start building the new parameter object
        String description = paramToUpdate.getDescription();
        if (description == null) {
            // Description not yet set; get from report
            description = reportData.getDescription();
        }
        if (description == null) {
            // No description specified; use parameter name
            description = reportData.getName();
        } else if (description.length() > 0) {
            // Description is from either the database or report.
            // Treat it as a localization key for the report resource.
            description = ResourceUtil.getLocalizedKeyValue(description);
        }

        Class valueClass = reportData.getValueClass();
        if (valueClass == null) {
            // Default to String if type isn't specified
            valueClass = String.class;
        }

        ReportParameterFieldType fieldType = paramToUpdate.getFieldType();
        if (fieldType == null) {
            // If not specified in database, set to false
            fieldType = TextField;
        }

        Boolean isRequired = paramToUpdate.getIsRequired();
        if (isRequired == null) {
            // If not specified in database, set to false
            isRequired = false;
        }

        String defaultValue = paramToUpdate.getDefaultValue();
        if (defaultValue == null) {
            // No default value already specified; just use empty string.
            // The default specified in the report is an expression, and
            // will only be executed in the report itself.
            defaultValue = "";
        }
        if (reportData.getName().equals("SESSION_VALUE")) {
            if (isPrintDirect) {
                defaultValue = getPrintDirectParameter();
            } else {
                defaultValue = getSession().getAttribute("SESSION_VALUE")
                    .toString();
            }
        } else if (reportData.getName().equals("SITE")) {
            defaultValue = getSiteName();
        }

        paramToUpdate.setParameterName(reportData.getName());
        paramToUpdate.setParameterType(valueClass);
        paramToUpdate.setDescription(description);
        paramToUpdate.setFieldType(fieldType); // Will be false without DB
        paramToUpdate.setIsRequired(isRequired);
        paramToUpdate.setDefaultValue(defaultValue);
    }

    /**
     * Helper function to put the JR parameters into the member variable Map.
     * This allows us constant time lookups while we iterate through the
     * database parameters.
     * @param parameterArray The array to convert
     */
    private void buildParameterMap(JRParameter[] parameterArray) {
        // Create an empty hashmap
        reportTypeParameters = new HashMap<String, JRParameter>();
        if (parameterArray == null) {
            return;
        }

        // For each array parameter, insert into the map
        for (int i = 0; i < parameterArray.length; i++) {
            JRParameter param = parameterArray[i];
            reportTypeParameters.put(param.getName(), param);
        }
    }

    /**
     * This action is called from the processParameters template, when the
     * parameters have all been populated with legal values.
     * @return Success
     * @throws VocollectException on any error
     */
    public String prepareReport() throws VocollectException {

        // Find the report...
        if (!findReport()) {
            return ERROR_404_RETURN;
        }

        // Build Parameters value map
        Map<String, Object> values = buildParameters();

        // Compile the report
        JasperReport compiled = compileReport();

        // Pass in the parameters, and fill the report
        JasperPrint reportResult = buildReportData(compiled, values);

        //Get report format.
        String format = RenderFormat.HTML.name();
        if (!isPrintDirect) {
            format = setReportFormat(format);
        }
        
        //render the report in the desired format
        renderReport(reportResult, format);

        // Return lower-case version of file extension as result
        return format.toLowerCase();
    }

    /**
     * Build the parameter list for report.
     * @return - return list of paramters
     */
    private Map<String, Object> buildParameters() {
        // Inspect the report's parameters
        JRParameter[] parameterArray = this.reportDesign.getParameters();
        buildParameterMap(parameterArray);

        // Check for POST values for each report parameter.
        // Parameter value map
        Map<String, Object> values = loadParameterValues();

        //Set locale
        values.put(JRParameter.REPORT_LOCALE, getLocale());

        return values;
    }
    
    /**
     * Compile a report.
     * @return - return compiled report
     * @throws VocollectException - vocollect exceptions
     */
    protected JasperReport compileReport() throws VocollectException {
        try {
            return JasperCompileManager.compileReport(reportDesign);
        } catch (JRException ex) {
            throw new VocollectException(ReportError.JASPER_COMPILE_ERROR, ex);
        }
    }
    
    /**
     * Build a report.
     * @param compiled - compiled report
     * @param values - parameter values for report
     * @return - return built report
     * @throws VocollectException - vocollect exceptions
     */
    private JasperPrint buildReportData(JasperReport compiled, 
                                    Map<String, Object> values) 
    throws VocollectException {
        JasperPrint reportResult = null;
        Object bean = getReportDataSourceBean();
        
        if (bean instanceof ReportDataSourceManager) {
            reportResult = buildDataSourceReportData(
                (ReportDataSourceManager) bean, compiled, values);
        } else if (bean instanceof SessionFactory) {
            reportResult =  buildHibernateReportData(
                (SessionFactory) bean, compiled, values);
        } else {
            throw new VocollectException(
                ReportError.DATASOURCE_BEAN_UNKOWN_ERROR);
        }

        if (!isPrintDirect) {
            getSession().setAttribute(
                ImageServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE,
                reportResult);
        }
        
        return reportResult;
    }
    
    /**
     * Find the bean data source specified for report.
     * @return - Bean object
     * @throws VocollectException - Vocollect exception.
     */
    private Object getReportDataSourceBean() 
    throws VocollectException {
        // Get the hibernate session from our context
        if (!applicationContext.containsBean(getSessionBeanId())) {
            throw new VocollectException(
                ReportError.DATASOURCE_BEAN_NOTFOUND_ERROR);
        }
        return applicationContext.getBean(getSessionBeanId());
    }
    
    
    /**
     * Build a report using a hibernate query.
     * @param sessionBean - Hibernate Session Factory
     * @param compiled - compiled report
     * @param values - parameter values for report
     * @return - result of report
     * @throws VocollectException - vocollect exception
     */
    private JasperPrint buildHibernateReportData(SessionFactory sessionBean,
                                                 JasperReport compiled,
                                             Map<String, Object> values) 
    throws VocollectException {
        Session session = null;
        
        //Check to see where to get session from
        if (isPrintDirect) {
            session = getSessionFactory().openSession();
        } else {
            session = sessionBean.openSession();
        }
        
        //Set hibernate session
        values.put(
            JRHibernateQueryExecuterFactory.PARAMETER_HIBERNATE_SESSION,
            session);

        //Build report data
        JasperPrint reportResult = null;
        try {
            reportResult = JasperFillManager.fillReport(compiled, values);
        } catch (JRException ex) {
            throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
        } finally {
            session.close();
            if (log.isDebugEnabled()) {
            log.debug("Session is : " + session.isConnected() + " : "
                + session.isOpen());
            }
        }
        
        return reportResult;
    }
    
    /**
     * Build a report using a java generated data source.
     * @param reportDataSource - ReportDataSourceManager object
     * @param compiled - compiled report object
     * @param values - map of parameter values
     * @return - return result of report
     * @throws VocollectException - vocollect exceptions
     */
    private JasperPrint buildDataSourceReportData(
                             ReportDataSourceManager reportDataSource,
                             JasperReport compiled,
                             Map<String, Object> values) 
    throws VocollectException {
        
        try {
            return JasperFillManager.fillReport(compiled, values, 
                reportDataSource.getDataSource(values));
        } catch (Exception ex) {
            throw new VocollectException(ReportError.JASPER_FILL_ERROR, ex);
        }
    }
    
    /**
     * Render the created report in the desired format.
     * @param reportResult - Report results
     * @param format - format to render report in
     * @throws VocollectException - Vocollect exceptions
     * @throws ReportException - Report exceptions
     */
    private void renderReport(JasperPrint reportResult, String format)
        throws VocollectException, ReportException {
        boolean isHtml = false;
        ByteArrayOutputStream outStream = null;
        StringBuffer stringBuffer = null;

        if (format.equals(RenderFormat.HTML.name())) {
            // In HTML, output a character array, because the characters
            // may be multi-byte.
            isHtml = true;
            stringBuffer = new StringBuffer();
        }

        // Create the byte output stream for report generation
        byte[] reportBytes = null;
        outStream = new ByteArrayOutputStream(BUFFER_SIZE);
        JRAbstractExporter exporter = null;

        try {
            if (format.equals(RenderFormat.HTML.name())) {
                exporter = new JRHtmlExporter();
                // Tell Jasper to not use empty images to space the report;
                // we would need a place to store these image files.
                exporter.setParameter(
                    JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,
                    Boolean.FALSE);
                exporter.setParameter(
                    JRHtmlExporterParameter.IMAGES_URI, "image?image=");
                // Export HTML via String Buffer
                exporter.setParameter(
                    JRExporterParameter.OUTPUT_STRING_BUFFER, stringBuffer);
            } else if (format.equals(RenderFormat.PDF.name())) {
                exporter = new JRPdfExporter();
                // Attempt to render fonts as images where needed
                exporter.setParameter(
                    JRPdfExporterParameter.FORCE_SVG_SHAPES, true);
                // Set special font mapping where needed
                Map fontMap = getFontMap(getLocale());
                exporter.setParameter(JRExporterParameter.FONT_MAP, fontMap);
            } else if (format.equals(RenderFormat.RTF.name())) {
                exporter = new JRRtfExporter();
            } else if (format.equals(RenderFormat.XLS.name())) {
                exporter = new JExcelApiExporter();
                exporter.setParameter(
                    JRHtmlExporterParameter.IMAGES_URI, "image?image=");
            } else {
                // Invalid format value
                throw new VocollectException(ReportError.BAD_RENDER_FORMAT);
            }
            if (isPrintDirect) {
                JasperPrintManager.printReport(reportResult, false);
            }

            // Now we have our exporter instance. Set options, then
            // export to byte array
            exporter.setParameter(
                JRExporterParameter.JASPER_PRINT, reportResult);
            if (!isHtml) {
                exporter.setParameter(
                    JRExporterParameter.OUTPUT_STREAM, outStream);
            }
            // Set default encoding to Unicode
            exporter.setParameter(
                JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
            exporter.exportReport();

        } catch (JRException ex) {
            throw new VocollectException(ReportError.JASPER_EXPORT_ERROR, ex);
        } catch (JRRuntimeException exRuntime) {
            if (exRuntime.getLocalizedMessage().contains(
                "Could not load the following font :")) {
                throw new ReportException(
                    ReportError.JASPER_EXPORT_ERROR,
                    getText("error.font.not.found"));
            } else {
                throw new VocollectException(
                    ReportError.JASPER_EXPORT_ERROR, exRuntime);
            }
        }

        if (isHtml) {
            // Convert string buffer to input array
            try {
                reportBytes = stringBuffer.toString().getBytes("UTF-8");
            } catch (UnsupportedEncodingException ex) {
                throw new VocollectException(ex);
            }
        } else {
            // Extract byte array from stream
            reportBytes = outStream.toByteArray();
        }

        // Create input stream for Struts result to read
        this.reportStream = new ByteArrayInputStream(reportBytes);
    }


    /**
     * Set reports format if printing direct.
     * @param format - currentyl known format
     * @return - format for report
     * @throws VocollectException - Vocollect exception
     * @throws BusinessRuleException - Business rule exceptions
     * @throws DataAccessException - Data Access exceptions
     */
    private String setReportFormat(String format) throws VocollectException,
        BusinessRuleException, DataAccessException {

        // Look for a render format parameter
        // String format = servletRequest.getParameter(PARAM_RENDER_FORMAT);
        format = getParamValue(PARAM_RENDER_FORMAT, parameterMap);
        if (format == null) {
            throw new VocollectException(ReportError.BAD_RENDER_FORMAT);
        }

        boolean found = false;
        for (UserProperty property : getCurrentUser().getUserProperties()) {
            if (property.getName().equals(REPORT_DEFAULT_RENDER_FORMAT)) {
                property.setValue(format);
                found = true;
                break;
            }
        }
        
        if (!found) {
            UserProperty prop = new UserProperty();
            prop.setName(REPORT_DEFAULT_RENDER_FORMAT);
            prop.setValue(format);
            getCurrentUser().getUserProperties().add(prop);
        }
        
        try {
            if (log.isDebugEnabled()) {
            log.debug("Saving format as : " + format);
            }
            getUserManager().save(getCurrentUser());
            if (log.isDebugEnabled()) {
            log.debug("Done saving...");
            }
        } catch (OptimisticLockingFailureException olfe) {
            log.warn("Ignoring the optimistic failure.");
            getUserManager().get(getCurrentUser().getId());
        } catch (StaleObjectStateException sose) {
            log.warn("Ignoring the stale object failure.");
            getUserManager().get(getCurrentUser().getId());
        }
        
        return format;
    }

    /**
     * <p>
     * Create a Font Map for the JasperReports exporter; this allows us to
     * override the default fonts used to render. This only seems to be needed
     * for PDF files, as JasperReports define special "pdfFontName" and
     * "pdfEncoding" attributes for every field. These only seem to be used when
     * exporting to PDF, and they default to standard Windows encoding. The
     * pdfEncoding flag seems to be the most critical.
     * </p>
     * 
     * <p>
     * In this code, we examine the country code from the locale, and for the
     * multi-byte encoding countries, we set a special encoding. Otherwise is
     * uses the "Windows ANSI" standard.
     * </p>
     * 
     * <p>
     * As of today, 5/18/07, this font_map code is still not working, having
     * tested with Greek and Chinese TW languages. It may be simply an issue of
     * including appropriate fonts in the Java classpath, or it may be more
     * involved.
     * </p>
     * 
     * @param locale The locale being rendered.
     * @return The new map.
     */
    private static Map getFontMap(Locale locale) {
        // Figure out the appropriate font based on the locale language
        Map<FontKey, PdfFont> fontMap = new HashMap<FontKey, PdfFont>();

        FontKey normalFontKey = new FontKey(PDF_STANDARD_FONT, false, false);
        FontKey boldFontKey = new FontKey(
            PDF_STANDARD_FONT + "-Bold", true, false);
        // Set default fonts
        String normalFontName = PDF_STANDARD_FONT;
        String boldFontName = PDF_STANDARD_FONT + "-Bold";
        String pdfFontEncoding = PDF_STANDARD_ENCODING;
        boolean embedFont = false;
        PdfFont normalFont = null;
        PdfFont boldFont = null;
        if (locale.getLanguage().equals("zh")) {
            // Chinese dialect; select chinese font
            final String chineseTraditional = "UniCNS-UCS2-H";
            // final String chineseSimplified = "UniGB-UCS2-H";
            pdfFontEncoding = chineseTraditional;
            // Embed font
            embedFont = false;
        } else if (locale.getLanguage().equals("el")) {
            // Greek dialect
            pdfFontEncoding = "CP1253";
            embedFont = false;
        } else if (locale.getLanguage().equals("ja")) {
            // Japanese
            pdfFontEncoding = "UniJIS-UCS2-H";
            embedFont = false;
        }

        // Create actual font objects
        normalFont = new PdfFont(normalFontName, pdfFontEncoding, embedFont);
        boldFont = new PdfFont(
            boldFontName, pdfFontEncoding, embedFont, true, false);
        // Now assign font keys to fonts
        fontMap.put(normalFontKey, normalFont);
        fontMap.put(boldFontKey, boldFont);

        return fontMap;
    }

    /**
     * Get all the values to pass into the report. This will examine the report
     * parameters from the POST data from the form submit.
     * @return New map of parameter values for the report
     */
    private Map<String, Object> loadParameterValues() {
        Set<String> reportNames = reportTypeParameters.keySet();
        Iterator<String> nameIter = reportNames.iterator();
        JRParameter parameter = null;
        Map<String, Object> values = new HashMap<String, Object>();
        while (nameIter.hasNext()) {
            String paramName = nameIter.next();
            parameter = reportTypeParameters.get(paramName);
            // String value = servletRequest.getParameter(paramName);
            String value = getParamValue(paramName, parameterMap);
            if (paramName.equals(PARAM_CURRENT_SITE_CONTEXT)) {
                // Pass in the current site context ID
                // (this won't be found in post data)
                value = getSiteName();
            } else if (paramName.equals(PARAM_CURRENT_USER)) {
                // Pass in current user name
                value = getUserName();
            } else if (paramName.equals("SESSION_VALUE")) {
                if (isPrintDirect) {
                    value = getPrintDirectParameter();
                } else {
                    value = getSession().getAttribute("SESSION_VALUE1")
                        .toString();
                }

            } else if (paramName.equals("SITE")) {
                value = getSiteId() + "";
            } else if (value == null) {
                // Boolean values with no post data will be interpreted
                // as "false"
                if (parameter.getValueClass().equals(Boolean.class)) {
                    values.put(paramName, Boolean.FALSE);
                }

                // No more processing to do for missing params.
                continue;
            } else if (value == ""
                && parameter.getValueClass().equals(String.class)) {
                // Treat empty strings as null; don't pass them in. This
                // will force the report to use the default value.
                continue;
            } else if (paramName.startsWith("ENUM")
                && (!value.equals(PARAM_LIST_ALL))) {
                value = enumValues.get(value);
            }
            if (value.equals(PARAM_LIST_ALL)) {
                // No value found for this param
                // OR they selected the ALL option, in which case we don't
                // pass the parameter.
                continue;
            }

            // Convert the String value into an object value
            values.put(paramName, getParameterValue(parameter, value));
        }

        return values;
    }

    /**
     * Helper function; attempt to extract parameter information from the
     * parameter map, using type casting.
     * @param parameterName The name of the param to lookup
     * @param parameterMap The map to look into
     * @return The parameter string value, or null if not found
     */
    private static String getParamValue(String parameterName, Map parameterMap) {
        if (parameterMap == null) {
            return null;
        }
        Object value = parameterMap.get(parameterName);
        if (value == null) {
            return null;
        }
        // Attempt to cast to String array
        String[] array = (String[]) value;
        if (array.length < 1) {
            return null;
        }
        return array[0];
    }

    /**
     * Helper function to convert a String value into the appropriate object
     * value. This is needed when passing in parameter to Jasper Reports.
     * @param parameter The Jasper parameter object, which contains the
     *            parameter data type.
     * @param value The String value to be converted
     * @return The object value to be set in Jasper
     */
    private Object getParameterValue(JRParameter parameter, String value) {
        Object objectValue = null;
        try {
            // Check the data type and cast as needed
            if (parameter.getValueClass().equals(String.class)) {
                objectValue = value;
            }
            if ((value != null) && (!value.equals(""))) {
                if (parameter.getValueClass().equals(Long.class)) {
                    objectValue = Long.valueOf(value);
                } else if (parameter.getValueClass().equals(Integer.class)) {
                    objectValue = Integer.valueOf(value);
                } else if (parameter.getValueClass().equals(Double.class)) {
                    objectValue = Double.valueOf(value);
                } else if (parameter.getValueClass().equals(Boolean.class)) {
                    objectValue = Boolean.valueOf(value);
                } else if (parameter.getValueClass().equals(Date.class)) {
                    // Date formatting will only interpret YYYY-MM-DD
                    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                    format.setLenient(false);
                    Date dateVal = format.parse(value);
                    objectValue = dateVal;
                }
            }
        } catch (NumberFormatException ex) {
            // If an invalid number is passed, ignore it; the report's
            // default value should be used.
            log.warn(ex.getMessage(), ex);
        } catch (ParseException ex) {
            // Invalid date; pass it in as null
            log.warn(ex.getMessage(), ex);
        }

        return objectValue;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Setter for the userManager property.
     * @param um the new userManager value
     */
    public void setUserManager(UserManager um) {
        userManager = um;
    }

    /**
     * Check if object is taggable and for current site. 
     * @param item - item to check site for
     * @return - True if not tagable, in current site, or current site 
     *           is All Sites, otherwise return False
     * @throws DataAccessException - database exceptions
     */
    private boolean validSite(Object item) throws DataAccessException {
        
        //Check if item is taggable, and not all sites
        if (item instanceof Taggable && !getSiteName().equals("All Sites")) {
            SiteContext sc = SiteContextHolder.getSiteContext();
            Site site = sc.getSiteByName(getSiteName());
            return sc.isInSite((Taggable) item, site);
//            List<Site> sites = sc.getSites((Taggable)item);
//            return sites.contains(site);
            
//            Set<Tag> tags = ((Taggable) item).getTags();
//            //check if tags are defined (should never be null for taggable item)
//            if (tags != null) {
//                //loop through tags to see if item is for current site
//                for (Tag tag : tags) {
//                    String sn = SiteContextHolder.getSiteContext()
//                        .getSiteByTagId(tag.getId()).getName();
//                    if (sn.equals(getSiteName())) {
//                        return true;
//                    }
//                }
//            }
        } else {
            //not taggable or all sites so return true
            return true;
        }

//        return false;
    }

    /**
     * Getter for the siteId property.
     * @return long value of the property
     */
    @SuppressWarnings("static-access")
    public long getSiteId() {
        return this.siteId;
    }

    /**
     * Setter for the siteId property.
     * @param siteId the new siteId value
     */
    @SuppressWarnings("static-access")
    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    /**
     * Setter for the siteId property.
     * @param directParam parameter id passed for direct print
     * @param printerPath to the printer
     * @param sf Hibernate SessionFactory
     * @throws VocollectException exception for direct print.
     */
    public void printDirect(String directParam,
                            String printerPath,
                            SessionFactory sf)
        throws VocollectException {
        setPrintDirect(true);
        setPrintDirectParameter(directParam);
        setPath(printerPath);
        setSessionFactory(sf);
        prepareReport();
    }

    /**
     * Getter for the isPrintDirect property.
     * @return boolean value of the property
     */
    public boolean isPrintDirect() {
        return this.isPrintDirect;
    }

    /**
     * Setter for the isPrintDirect property.
     * @param printDirect the new isPrintDirect value
     */
    public void setPrintDirect(boolean printDirect) {
        this.isPrintDirect = printDirect;
    }

    /**
     * Getter for the printDirectParameter property.
     * @return String value of the property
     */
    public String getPrintDirectParameter() {
        return this.printDirectParameter;
    }

    /**
     * Setter for the printDirectParameter property.
     * @param printDirectParameter the new printDirectParameter value
     */
    public void setPrintDirectParameter(String printDirectParameter) {
        this.printDirectParameter = printDirectParameter;
    }

    /**
     * Getter for the path property.
     * @return String value of the property
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Setter for the path property.
     * @param path the new path value
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 