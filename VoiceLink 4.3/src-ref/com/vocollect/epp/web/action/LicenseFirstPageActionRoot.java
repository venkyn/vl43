/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemLicense;

import com.opensymphony.xwork2.Preparable;

import java.nio.charset.Charset;

/**
 *
 */
public class LicenseFirstPageActionRoot extends SiteEnabledAction implements Preparable  {

    /**
     * Serialization identifier.
     */
    private static final long serialVersionUID = 4546485532914419858L;

    /* VoiceLicense instance needed to display the license data */
    private VoiceLicense voiceLicense;

    /* Logger instance */
    protected static final Logger log = new Logger(LicenseDisplayAction.class
            .getPackage().toString());


    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() {
        try {
            SystemLicense licenseFromDB = getSystemLicenseManager().getCurrentLicense();

            if (licenseFromDB == null) {
                return ERROR;
            } else {
                voiceLicense = new VoiceLicense(licenseFromDB.getContents()
                    .getBytes(Charset.forName("utf-8")));
                return SUCCESS;
            }
        } catch (Exception e) {
            log.debug(e.getMessage());
            return ERROR;
        }
    }

    /**
     * @return .
     */
    public String enterFile() {
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }

    /**
     * @return the license
     */
    public VoiceLicense getVoiceLicense() {
        return voiceLicense;
    }

    /**
     * @param voiceLicense the license
     */
    public void setVoiceLicense(VoiceLicense voiceLicense) {
        this.voiceLicense = voiceLicense;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * @return the site name of the current user's current site
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 