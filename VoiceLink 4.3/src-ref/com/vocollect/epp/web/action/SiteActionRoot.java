/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.LabelValuePair;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.StringUtil;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.Vector;

/**
 * This is the Struts action class that handles operations on <code>Site</code>
 * objects.
 * 
 * @author KalpnaT
 */
public class SiteActionRoot extends DataProviderAction implements Preparable {

    private static final int ONE_SEC = 60;

    private static final int ONE_MIN = 60 * 1000;

    private static final int ONE_HOUR = 60 * ONE_MIN;

    private static final long serialVersionUID = 5247932223098960094L;

    private static final long VIEW_ID = -5;

    private static final Logger log = new Logger(SiteAction.class);

    // added to update the cached sites when a site is added
    private SiteContext siteContext;

    // This is the user manager - used to save user to site relations
    private UserManager userManager;

    /**
     * @param siteContextParam
     *            the new site context
     */
    public void setSiteContext(SiteContext siteContextParam) {
        this.siteContext = siteContextParam;
    }

    // The site object, which will either be newly created, or retrieved
    // via the siteId.
    private Site site;

    // The ID of the site.
    private Long siteId;

    // The list of columns for the site table
    private List<Column> columns;

    private List<LabelValuePair> timeZoneList;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    /**
     * Getter for the site property.
     * 
     * @return Site value of the property
     */
    public Site getSite() {
        return this.site;
    }

    /**
     * Setter for the site property.
     * 
     * @param siteParam
     *            the new site value
     */
    public void setSite(Site siteParam) {
        this.site = siteParam;
    }

    /**
     * Getter for the siteId property.
     * 
     * @return Long value of the property
     */
    public Long getSiteId() {
        return this.siteId;
    }

    /**
     * Setter for the siteId property.
     * 
     * @param siteIdParam
     *            the new siteId value
     */
    public void setSiteId(Long siteIdParam) {
        this.siteId = siteIdParam;
    }

    /**
     * Getter for the columns property.
     * 
     * @return List of Column value of the property.
     */
    public List<Column> getSiteColumns() {
        return this.columns;
    }

    /**
     * @return the site name of the current user's current site when in single
     *         site mode.
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }

    /**
     * Action for the site view page. Initializes the site table columns
     * 
     * @return String value of outcome.
     * @throws Exception
     *             in case of DataAccess issues
     */
    public String list() throws Exception {
        View siteView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(siteView,
                getCurrentUser());
        this.restoreDefaultColumnsEnabled = getUserPreferencesManager()
                .isRestoreDefaultColumnsEnabled(getCurrentUser(), siteView);
        return SUCCESS;
    }

    /**
     * Create or update the site specified by the <code>site</code> member of
     * this class.
     * 
     * @return the control flow target name.
     * @throws Exception
     *             on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.site.isNew();
        try {
            if (this.site.getTimeZoneId() != null) {
                TimeZone tz = TimeZone.getTimeZone(this.site.getTimeZoneId());
                this.site.setTimeZone(tz);
            }

            getSiteManager().save(this.site);
            Site tmpSite = getSiteManager().findByName(this.site.getName());

            siteContext.addSiteToContext(tmpSite);
            siteContext.refreshSites();

            User user = getCurrentUser();
            if (user != null) {
                if (!user.getAllSitesAccess()) {
                    // We have a user who does not have all site access
                    // Therefore - we want to grant this user access to any
                    // sites
                    // that they are able to create
                    if (!user.getSites().contains(this.site)) {
                        user.addSite(tmpSite);
                        Tag tmpTag = siteContext
                                .getTagBySiteId(tmpSite.getId());
                        user.getTags().add(tmpTag);
                        getUserManager().save(user);
                    }
                }
            }

            cleanSession();
        } catch (BusinessRuleException e) {
            log.warn("Unable to save site: " + getText(e.getUserMessage()));
            addSessionActionMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DatabaseConstraintException e) {
            log.warn("A Site named '" + this.site.getName()
                    + "' already exists");
            addFieldError("site.name", new UserMessage(
                    "site.create.error.existing", this.site.getName()));
            // Go back to the form to show the error message.
            return INPUT;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified site "
                    + this.site.getName());
            addActionError(new UserMessage("entity.error.modified",
                    UserMessage.markForLocalization("entity.Site"),
                    SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If site has been deleted, this will throw EntityNotFoundException
            try {
                Site modifiedSite = getSiteManager().get(getSiteId());
                // Set the local object's version to match, so it will work
                // if the site resubmits.
                this.site.setVersion(modifiedSite.getVersion());
                // Store the modified data for display
                modifiedSite.setTimeZoneId(modifiedSite.getTimeZone().getID());
                setModifiedEntity(modifiedSite);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                        UserMessage.markForLocalization(getSite().getName()),
                        null));
                return SUCCESS;
            }
            return INPUT;
        }

        // add success message
        String successKey = "site." + (isNew ? "create" : "edit")
                + ".message.success";
        addSessionActionMessage(new UserMessage(successKey,
                makeContextURL("/admin/site/view.action?siteId="
                        + this.site.getId()), getSite().getName()));
        // Go to the success target.
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Site</code> object by retrieving it from
     * the database when a siteId is set by the form submission. {@inheritDoc}
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.siteId != null) {
            // We have an ID, but not a Site object yet.
            if (log.isDebugEnabled()) {
                log.debug("siteId is " + this.siteId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())
                    && getEntityFromSession(getSavedEntityKey()) != null) {
                // This means the User is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved site from session");
                }
                this.site = (Site) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting site from database");
                }
                this.site = getSiteManager().get(this.siteId);

                saveEntityInSession(this.site);
            }
            if (log.isDebugEnabled()) {
                log.debug("Site version is: " + this.site.getVersion());
            }

        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new site object");
            }
            if (this.site != null) {
                this.site = null;
            }
            this.site = new Site();
        }

        // Make sure the TimeZoneList gets cached.
        getTimeZoneList();

        // Set the time zone attribute
        if (this.site.getTimeZone() != null) {
            this.site.setTimeZoneId(this.site.getTimeZone().getID());
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getSiteManager();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     * @return String value of the keyprefix
     */
    @Override
    protected String getKeyPrefix() {
        return "site";
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * This method will return a list of time zones, sorted alphabetically, and
     * returned to the client.
     * 
     * @return the list of time zones.
     */
    public List<LabelValuePair> getTimeZoneList() {
        if (timeZoneList == null) {
            timeZoneList = new ArrayList<LabelValuePair>();
            String[] timeZoneIds = TimeZone.getAvailableIDs();

            // access all of the localizations for the timeZoneIds - place them in a
            // collection
            Collection<LabelValuePair> translatedTimeZoneIds = new Vector<LabelValuePair>();
            for (int i = 0; i < timeZoneIds.length; i++) {
                String timeZoneId = timeZoneIds[i];

                // Lookup the actual localized text based on the time zone id
                TimeZone tz = TimeZone.getTimeZone(timeZoneId);
                String localizedDisplayName = getText("timezone.id." + tz.getID());
                if (localizedDisplayName == null) {
                    localizedDisplayName = tz.getID();
                }
                // Store the actual time zone id and the localized Display Name
                // translatedTimeZoneIds.add(new LabelValuePair(tz.getID(),
                // localizedDisplayName));
                int rawOffset = tz.getRawOffset();
                int offset = rawOffset / ONE_HOUR;
                int offsetMinutes = Math.abs(rawOffset / ONE_MIN) % ONE_SEC;
                
                localizedDisplayName = "(UTC"
                + String.format("%+02d:%02d", offset, offsetMinutes)
                + ")  " + localizedDisplayName;
                
                translatedTimeZoneIds.add(new LabelValuePair(
                    localizedDisplayName, tz.getID()));
            }


            // spit them out into a list
            for (LabelValuePair pair : translatedTimeZoneIds) {
                timeZoneList.add(pair);
            }
        }
        return timeZoneList;
    }

    /**
     * @return localizedDisplayName
     */
    public String getSiteTimeZone() {
        String localizedDisplayName = getText(site.getTimeZoneId());
        if (localizedDisplayName == null) {
            localizedDisplayName = site.getTimeZoneId();
        }
        return localizedDisplayName;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#delete()
     */
    @Override
    public String delete() throws DataAccessException {
        String ret = super.delete();
        siteContext.refreshSites();
        return ret;
    }

    /**
     * Delete the site identified by the <code>siteId</code> parameter.
     * 
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified site wasn't found (with different messages to the
     *         end user)
     * @throws Exception
     *             on unanticipated error condition
     */
    public String deleteCurrentSite() throws Exception {

        Site siteToDelete = null;

        try {
            siteToDelete = getSiteManager().get(this.siteId);
            getSiteManager().delete(this.siteId);
            siteContext.refreshSites();
            addSessionActionMessage(new UserMessage(
                    "site.delete.message.success", siteToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete site: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Getter for the VIEW_ID property.
     * 
     * @return long value of the property
     */
    public static long getView_Id() {
        return VIEW_ID;
    }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * 
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the userManager property.
     * 
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return this.userManager;
    }

    /**
     * Setter for the userManager property.
     * 
     * @param userManagerParam
     *            the new userManager value
     */
    public void setUserManager(UserManager userManagerParam) {
        this.userManager = userManagerParam;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 