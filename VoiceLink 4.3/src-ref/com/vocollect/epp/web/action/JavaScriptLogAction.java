/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.ActionSupport;

import org.json.JSONObject;


/**
 * This action is used solely for logging messages from an asynchronous
 * call from JavaScript e.g the table component.
 *
 *
 * @author nkocur
 */
public class JavaScriptLogAction extends ActionSupport {

    private static final long serialVersionUID = 8441705994201620329L;

    private static final Logger log = new Logger(BaseAction.class);

    public static final int TRACE = 1;
    public static final int DEBUG = 2;
    public static final int INFO = 3;
    public static final int WARN = 4;
    public static final int ERROR = 5;
    public static final int FATAL = 6;

    private int logLevel = -1;
    // The message to be logged
    private String message;
    // The JSON Messages to be returned after an asynchronous action call.  This will be empty is this case.
    private String jsonMessage;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        switch (getLogLevel()) {
        case TRACE:
            if (log.isTraceEnabled()) {
                log.trace(getMessage());
            }
            break;
        case DEBUG:
            if (log.isDebugEnabled()) {
                log.debug(getMessage());
            }
            break;
        case INFO:
            if (log.isInfoEnabled()) {
                log.info(getMessage());
            }
            break;
        case WARN:
            log.warn(getMessage());
            break;
        case ERROR:
            log.error(getMessage(), SystemErrorCode.JAVASCRIPT_ERROR);
            break;
        case FATAL:
            log.fatal(getMessage(), SystemErrorCode.JAVASCRIPT_FATAL);
            break;
        default:
            log.error("Invalid log level, logging as ERROR", SystemErrorCode.JAVASCRIPT_ERROR);
            log.error(getMessage(), SystemErrorCode.JAVASCRIPT_ERROR);
            break;
        }
        jsonMessage = new JSONObject().toString();
        return SUCCESS;
    }

    /**
     * Getter for the logLevel property.
     * @return int value of the property
     */
    public int getLogLevel() {
        return logLevel;
    }

    /**
     * Setter for the logLevel property.
     * @param logLevel the new logLevel value
     */
    public void setLogLevel(int logLevel) {
        this.logLevel = logLevel;
    }


    /**
     * Getter for the message property.
     * @return String value of the property
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter for the message property.
     * @param message the new message value
     */
    public void setMessage(String message) {
        this.message = message;
    }


    /**
     * Getter for the jsonMessage property.
     * @return String value of the property
     */
    public String getJsonMessage() {
        return jsonMessage;
    }


    /**
     * Setter for the jsonMessage property.
     * @param jsonMessage the new jsonMessage value
     */
    public void setJsonMessage(String jsonMessage) {
        this.jsonMessage = jsonMessage;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 