/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemLicense;
import com.vocollect.epp.service.LicenseManager;
import com.vocollect.epp.web.licensing.enforcement.Licensers;

import com.opensymphony.xwork2.Preparable;

import java.nio.charset.Charset;

import org.springframework.web.util.WebUtils;

/**
 * Action class to 
 * 1) Get the license object  from the session.
 * 2) Store it in database(VOC_EPP_LICENSE).
 * 3) Display the license data to the user.
 *  
 * @author pmukhopadhyay
 */
public class LicenseDisplayActionRoot extends SiteEnabledAction implements Preparable {
    /**
     *    Serialization identifier.
     */
    private static final long serialVersionUID = 5377606498299914214L;
    
    /* LicenseManager service needed to do database transactions */
    private LicenseManager licenseManager;
    
    /*
     * Maximum size of Limit
     */
    static final int MAX_LIMIT = 100000;
    
    /* VoiceLicense instance needed to display the license data */
    private VoiceLicense voiceLicense;
    
    /* Logger instance */
    protected static final Logger log = new Logger(LicenseDisplayAction.class
            .getPackage().toString());
    
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() {
       
        try {
            SystemLicense licenseFromDB = licenseManager.getCurrentLicense();
            SystemLicense licenseFromUser = (SystemLicense) WebUtils.getSessionAttribute(
                this.getRequest(), "licenseContent");

            if ((licenseFromDB != null) && (licenseFromUser != null)) {
                putLicenseInDatabase();
                WebUtils.setSessionAttribute(
                    this.getRequest(), "licenseContent", null);
                getLicenseFromDatabase();
                return SUCCESS;
            } else if ((licenseFromDB != null) && (licenseFromUser == null)) {
                getLicenseFromDatabase();
                return SUCCESS;
            } else if ((licenseFromDB == null) && (licenseFromUser != null)) {
                putLicenseInDatabase();
                WebUtils.setSessionAttribute(
                    this.getRequest(), "licenseContent", null);
                getLicenseFromDatabase();
                return SUCCESS;
            } else {
                return ERROR;
            }
        } catch (Exception e) {
           log.debug(e.getMessage());
           return ERROR;
       }
    }
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
    }
    
    /**
     * Method to put the XML License key in Database.
     * @param void
     */
    public void putLicenseInDatabase() {
        try {
            SystemLicense inputLicense = (SystemLicense) 
                WebUtils.getSessionAttribute(this.getRequest(), "licenseContent");
            licenseManager.saveCurrentLicense(inputLicense);
            
            VoiceLicense useMe = new VoiceLicense(inputLicense.getContents().getBytes(Charset.forName("utf-8")));
            Licensers.refreshLicense(useMe);
            this.setCurrentLicenseInfo(useMe);
            
            // Set the current license for this process
            VoiceLicense.setCurrentLicense(useMe);
            
        } catch (DataAccessException d) {
            d.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get License information from database.
     * @param void
     */
    public void getLicenseFromDatabase() {
        voiceLicense = VoiceLicense.getCurrentLicense();
    }
    
    /**
     * @return the license manager object.
     */
    @Override
    public LicenseManager getSystemLicenseManager() {
        return licenseManager;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseAction#setLicenseManager(com.vocollect.epp.service.LicenseManager)
     */
    @Override
    public void setSystemLicenseManager(LicenseManager licenseManagerParam) {
        this.licenseManager = licenseManagerParam;
    }

    /**
     * @return the license
     */
    public VoiceLicense getVoiceLicense() {
        return voiceLicense;
    }
    
    /**
     * @param voiceLicense the license
     */
    public void setVoiceLicense(VoiceLicense voiceLicense) {
        this.voiceLicense = voiceLicense;
    }
    
    /**
     * @return show agreement text
     */
    public String showAgreement() {
        return "showAgreement";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#getSingleSiteName()
     */
    @Override
    public String getSingleSiteName() throws DataAccessException {
        return getText("site.context.all");
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 