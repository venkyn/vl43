/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.MessageMap;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.User;
import com.vocollect.epp.plugin.PluginBase;
import com.vocollect.epp.plugin.PluginModule;
import com.vocollect.epp.security.RoleFeatureMap;
import com.vocollect.epp.security.SecurityErrorCode;
import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.service.PluginModuleManager;
import com.vocollect.epp.ui.Breadcrumb;
import com.vocollect.epp.ui.Navigation;
import com.vocollect.epp.util.ResourceUtil;

import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.HttpSessionContextIntegrationFilter;
import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Implementation of <code>ActionSupport</code> that contains convenience 
 * methods for subclasses. For example, getting the current user and saving 
 * messages/errors. This class is intended to be a base class for all Action
 * classes.
 * 
 * @author Dennis Doubleday
 */
public class BaseActionRoot extends LicenseAwareAction {

    private static final long   serialVersionUID = -3973839293224131297L;

    private static final Logger logger = new Logger(BaseAction.class);
    
    /**
     * The Javascript that is used to show the Modified Entity div.
     */
    public static final String SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT = 
        "javascript: YAHOO.example.resize.panel.show();";

    
    /**
     * Return status indicating forward to resource not found error page.
     */
    public static final String ERROR_404_RETURN = "error404";
    
    /**
     * Return status indicating forward to access control error page.
     */
    public static final String ERROR_ACCESS_RETURN = "errorAccess";
    
    /**
     * Return status indicating forward to entity not found error page.
     */
    public static final String ERROR_ENTITY_NOT_FOUND_RETURN = "errorEntityNotFound";
    
    /**
     * Return status indicating forward to application error page.
     */
    public static final String ERROR_APPLICATION_RETURN = "errorApplication";
    
    /**
     * Return status indicating forward to internal error page.
     */
    public static final String ERROR_INTERNAL_RETURN = "errorInternal";
 
    /**
     * This is the key under which messages are saved in the user session.
     */
    private static final String MESSAGES_ATTRIBUTE    = "actionMessages";

    /**
     * The dispatcher target when an action is cancelled.
     */
    public static final String CANCEL = "cancel";

    private static final String SESSION_COUNTER_KEY = "sessionCounter";

    private static final String SAVED_ENTITY_KEY_PREFIX = "savedEntity";

    // Static cache of available pluginCache.
    private static List<PluginModule> pluginCache;
    
   /**
     * This is the local copy of messages that are saved in the session.
     */
    private Map<String, List<String>> sessionActionMessages = null;

    // Spring-injected map of Role/Feature/FeatureGroup information.
    private RoleFeatureMap roleFeatureMap;

    // The key under which an entity associated with this action has
    // been saved in the session. The primary purposes of saving the
    // object in the session are to avoid going back to the database
    // to retrieve the object on each submission or validation call,
    // and to be able to take advantage of Hibernate's automatic
    // version-checking to detect stale data. 
    private String savedEntityKey;
    
    // If the entity that we are attempting to update was modified by
    // another thread between the time we initally retrieved it and the
    // time we submitted, then the current state of the entity will be
    // stored here for optional display to the end user.
    private Object modifiedEntity;
    
  
    /**
     * Provides access on every page to the list of installed & enabled
     * pluginCache.
     */
    private PluginModuleManager pluginModuleManager;
    
    // Usage of this attribute has increased so we are going to cache it
    private Navigation navigation;

    private DataTranslationManager dataTranslationManager;
    
    
    /**
     * Provides access to the Action's log for helper classes 
     * employing composition rather than inheritance.
     * 
     * @return The Logger instance.
     */
    public Logger getLogger() {
        return this.logger;
    }
        
    /**
     * @return the formatted current time 
     */
    public String getCurrentTime() {
        Format fmt = DateFormat.getTimeInstance(
                        DateFormat.MEDIUM, this.getLocale());
        return fmt.format(new Date());
    }
    
    /**
     * Add a <code>UserMessage</code> to the list of action errors associated
     * with this request.
     * @param msg the message to add
     */
    public void addActionError(UserMessage msg) {
        // Resolve the message according to the request Locale and store
        // in the usual String format.
        addActionError(getText(msg));
    }

    /**
     * Add a <code>UserMessage</code> to the list of action messages associated
     * with this request.
     * @param msg the message to add
     */
    public void addActionMessage(UserMessage msg) {
        // Resolve the message according to the request Locale and store
        // in the usual String format.
        addActionMessage(getText(msg));
    }

    /**
     * Add a <code>UserMessage</code> to the list of action errors associated
     * with this request.
     * @param fieldName the name of the field the error is for
     * @param msg the message to add
     */
    public void addFieldError(String fieldName, UserMessage msg) {
        // Resolve the message according to the request Locale and store
        // in the usual String format.
        addFieldError(fieldName, getText(msg));
    }

    /**
     * Add a <code>UserMessage</code> to the list of action messages that
     * are associated with the user HTTP session.
     * @param msg the message to add
     */
    @Override
    public void addSessionActionMessage(UserMessage msg) {
        createSessionActionMessage(msg, MessageMap.DEFAULT_CATEGORY);  
    }
    
    /**
     * Adds error message with additional details to be displayed in the View Details link.
     * @param msg - General Error message to be displayed.
     * @param detailMessages - List of details to be included in View More messages.
     */
    public void addErrorWithMoreInfoActionMessage(UserMessage msg, List<UserMessage> detailMessages) {
        createSessionActionMessageWithMoreInfo(msg, MessageMap.ERROR_CATEGORY,
            detailMessages);        
    }
    
    /**
     * Add a <code>UserMessage</code> and a list of detailMessages to the list of action messages that
     * are associated with the user HTTP session.
     * @param msg the general message to add
     * @param detailMessages the detail list of messages to be shown alongsode the general message
     */
    public void addPartialSuccessSessionActionMessage(UserMessage msg, 
                                      List<UserMessage> detailMessages) {        
        createSessionActionMessageWithMoreInfo(msg, MessageMap.ERROR_PARTIAL_SUCCESS,
            detailMessages);          
    }
    
    /**
     * Adds the <code>UserMessage</code> along with a list of detail messages
     * to the list of action messages for the specified category.
     * @param msg the message to add
     * @param category the category to add the message to
     * @param messages the list of detail messages that is to be shown along side the
     * general message
     */
    private void createSessionActionMessageWithMoreInfo(UserMessage msg,
                                                   String category,
                                                   List<UserMessage> messages) {
        // Create a MessageMap if none exists in this request.
        if (this.sessionActionMessages == null) {
            this.sessionActionMessages = new HashMap<String, List<String>>();
        }
        
        List<String> msgList = this.sessionActionMessages.get(category);
        if (msgList == null) {
            msgList = new ArrayList<String>();
            this.sessionActionMessages.put(category, msgList);
        }
        msgList.add(getText(msg));
        
        // Add the general message to also be displayed as the first message
        // in the more info bubble.
        createSessionActionMessage(msg, MessageMap.MORE_INFO_MESSAGES);
        
        // Add the detail messages to the MORE_INFO message map category
        for (int i = 0; i < messages.size(); i++) {
            createSessionActionMessage(messages.get(i), MessageMap.MORE_INFO_MESSAGES);
        }
               
        // Store the map in the session
        getSession().setAttribute(
                  MESSAGES_ATTRIBUTE, this.sessionActionMessages);        
    }
    
    /**
     * Add a <code>UserMessage</code> to the list of action messages that
     * are associated with the user HTTP session with error category.
     * @param msg the message to add
     */
    public void addSessionActionErrorMessage(UserMessage msg) {
        createSessionActionMessage(msg, MessageMap.ERROR_CATEGORY);    
    }
    
    /**
     * Adds the <code>UserMessage</code> to the list of action messages
     * for the specified category.
     * @param msg the message to add
     * @param category the category to add the message to
     */
    private void createSessionActionMessage(UserMessage msg, String category) {
        // Create a MessageMap if none exists in this request.
        if (this.sessionActionMessages == null) {
            this.sessionActionMessages = new HashMap<String, List<String>>();
        }
        // Add the message to the map after resolving it to this Locale.
        addMessageToSessionActionMessages(category, msg);
        
        // Store the map in the session
        getSession().setAttribute(
            MESSAGES_ATTRIBUTE, this.sessionActionMessages);        
    }

    /**
     * Convenience method to get the request object.
     * @return current request
     */
    @Override
    public HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();
    }

    /**
     * Convenience method to get the response object.
     * @return current response
     */
    public HttpServletResponse getResponse() {
        return ServletActionContext.getResponse();
    }

    /**
     * Convenience method to get the user's HTTP session.
     * @return the User session
     */
    @Override
    public HttpSession getSession() {
        if (getRequest() != null) {
            return getRequest().getSession();
        } else {
            return null;
        }
    }

    /**
     * Get the list of action messages (if any) that are either currently 
     * stored in the user's HTTP session, or have already been retrieved from 
     * the session.
     * <p>
     * Note that, when this method is called for the first time in a request,
     * it retrieves the messages from the session, stores them in this
     * object, <em>and deletes them from the session context</em>. This ensures
     * that the messages won't hang around and be displayed by later requests.
     * This method will usually be called only from FreeMarker templates, when
     * they attempt to display result messages, but there is no reason why
     * you cannot call it from your code as well.
     * @return the list of messages that were stored in the session.
     */
    @SuppressWarnings("unchecked")
    public Map<String, List<String>> getSessionActionMessages() {
        
        /* Do not remove the following line of code for ANY form of customization.  
         * The following line of code is mandatory for your software support contract! */ 
        checkForLicenseViolations();
        /* The preceding line of code needs to remain in all customizations. */
        
        // If the local copy is null, try to get it from the session.
        if (this.sessionActionMessages == null && getSession() != null) {
            Map<String, List<String>> sessionMessages = 
                (Map<String, List<String>>) getSession()
                    .getAttribute(MESSAGES_ATTRIBUTE);
            if (sessionMessages != null) {
                // We got it from the session, now remove it from the
                // session so no other request can get it.
                getSession().removeAttribute(MESSAGES_ATTRIBUTE);
                this.sessionActionMessages = sessionMessages;
            }
        }
        
        if (sessionActionMessages == null) {
            return new HashMap<String, List<String>>();
        }
        return this.sessionActionMessages;
    }

    /**
     * Get the current user of the system.
     * 
     * @return the User associated with this session.
     */
    public User getCurrentUser() {

        try {
            Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
            if (auth != null) {
                return (User) auth.getPrincipal();
            } else {
                // If the ContextHolder didn't get populated, which seems to
                // happen in error cases sometimes, we can try to
                // get the User from the session directly.
                SecurityContext context = (SecurityContext) getSession()
                    .getAttribute(
                        HttpSessionContextIntegrationFilter.ACEGI_SECURITY_CONTEXT_KEY);
                if (context != null) {
                    if (context.getAuthentication().getPrincipal() instanceof User) {
                        return (User) context.getAuthentication().getPrincipal();
                    } else {
                        // I have seen situations where we are the acegi "anonymous" user 
                        // but we don't have a User object representation for this.
                        return null;
                    }
                } else {
                    logger.error(
                        "Current user not found in context holder or session",
                        SecurityErrorCode.NO_CURRENT_USER);
                    return null;
                }
            }
        } catch (Throwable t) {
            // Make sure no exceptions are thrown out of this, to avoid
            // causing Freemarker to barf.
            logger.error(
                "Unable to retrieve current user",
                SecurityErrorCode.NO_CURRENT_USER, t);
            return null;
        }
    }
    
    /**
     * Get the navigation information for this action.
     * @return the <code>Navigation</code> object associated with this
     *         request, or null if there is no request (this appears to happen,
     *         rarely, under exceptional circumstances which aren't fully
     *         understood yet.)
     */
    public Navigation getNavigation() {
        if (getRequest() == null) {
            return null;
        } else {
            if (this.navigation == null) {
                this.navigation = new Navigation(getRequest().getRequestURI());
            }
            return this.navigation;
        }
    }

    /**
     * Get the Bread Crumb trail HTML that is displayed at the top of the
     * content area.
     * @return the Bread Crumb trail HTML for this action. This may be the
     * empty String.
     */
    public List<Breadcrumb> getBreadCrumbTrail() {
        Navigation nav = new Navigation(getRequest().getRequestURI());
        getSession().setAttribute(
               "SEARCH_BREADCRUMB" , getRequest().getRequestURI());
        
        return nav.getBreadcrumbTrail();
    }
    
    /**
     * Getter for the savedEntityKey property.
     * @return String value of the property
     */
    public String getSavedEntityKey() {
        return this.savedEntityKey;
    }

    
    /**
     * Setter for the savedEntityKey property.
     * @param savedEntityKey the new savedEntityKey value
     */
    public void setSavedEntityKey(String savedEntityKey) {
        this.savedEntityKey = savedEntityKey;
    }

    /**
     * Method that is called by Cancel buttons.
     * @return the CANCEL dispatcher target.
     */
    public String cancel() {
        cleanSession();
        return CANCEL;
    }

    /**
     * The input method just returns INPUT. This is useful for the
     * initial display of a page.
     * @return the INPUT constant
     */
    @Override
    public String input() {
        return INPUT;
    }

    /**
     * Do cleanup in session after CRUD success or cancel.
     */
    public void cleanSession() {
        if (getSavedEntityKey() != null) {
            getAndRemoveEntityFromSession(getSavedEntityKey());
        }
    }
    
    /**
     * Set a <code>MessageMap</code> of messages into the action errors
     * field.
     * <p>
     * Note that this method resolves the messages according to the request
     * Locale and adds them to the standard Struts Action Errors list, so
     * no message category information is retained.
     * @param msgs the messages to add
     */
    public void setActionErrors(MessageMap msgs) {
        ArrayList<String> resolvedList = new ArrayList<String>();
        for (UserMessage message : msgs) {
            // Resolve the message according to the request Locale and store
            // in the usual String format.
            resolvedList.add(getText(message));
        }
        setActionErrors(resolvedList);
    }

    /**
     * Set a <code>MessageMap</code> of messages into the action messages
     * field.
     * <p>
     * Note that this method resolves the messages according to the request
     * Locale and adds them to the standard Struts Action Messages list, so
     * no message category information is retained.
     * @param msgs the messages to add
     */
    public void setActionMessages(MessageMap msgs) {
        ArrayList<String> resolvedList = new ArrayList<String>();
        for (UserMessage message : msgs) {
            // Resolve the message according to the request Locale and store
            // in the usual String format.
            resolvedList.add(getText(message)); 
        }
        setActionMessages(resolvedList);
    }

    /**
     * Save the specified <code>MessageMap</code> of messages in the user's 
     * HTTP session.
     * @param msgs the map of messages to save.
     */
    public void setSessionActionMessages(MessageMap msgs) {
        
        this.sessionActionMessages = new HashMap<String, List<String>>();

        // For all categories of messages in the MessageMap, 
        // get the messages and convert them.
        for (String category : msgs.getCategoryNames()) {
            for (UserMessage message : msgs.getMessages(category)) {
                // Add the message to the map after resolving it to this Locale.
                addMessageToSessionActionMessages(category, message);        
            }
        }
        // Store the map in the session
        getSession().setAttribute(
            MESSAGES_ATTRIBUTE, this.sessionActionMessages);
    }

    /**
     * Return the Locale-translated text for the specified message. (This
     * includes the capability of a pre-translation step in which the
     * String value arguments are potentially translated as well.)
     * @param message the <code>UserMessage</code> to translate
     * @return the translated String.
     */
    protected String getText(UserMessage message) {
        return getText(message.getKey(), translateValues(message.getValues()));
    }
    
    /**
     * This method takes a List of Objects and, for each Object that is a
     * String, checks to see if the value is marked as a resource key that
     * needs to be evaluated in the current context Locale. If so, it translates
     * the value.
     * @param origValues the un-evaluated values
     * @return the translated List of values.
     */
    private List<Object> translateValues(List<Object> origValues) {

        if (origValues == null || origValues.isEmpty()) {
            return origValues;
        } else {
            ArrayList<Object> translatedValues = new ArrayList<Object>();
            for (Object value : origValues) {
                translatedValues.add(
                    ResourceUtil.translateArg(value, LocaleContextHolder.getLocale()));
            }
            return translatedValues;
        }
    }

    /**
     * Resolve and add a message to sessionActionMessages Map in the
     * specified category.
     * @param category the category (map key) to add to
     * @param msg the message to translate and add.
     */
    @Override
    protected void addMessageToSessionActionMessages(String      category,
                                                     UserMessage msg) {
        List<String> msgList = this.sessionActionMessages.get(category);
        if (msgList == null) {
            msgList = new ArrayList<String>();
            this.sessionActionMessages.put(category, msgList);
        }
        msgList.add(getText(msg));
    }

    /**
     * Make a URL that includes the context path from the specified URL,
     * which must be absolute (start with a '/').
     * @param absoluteURL the URL to prefix
     * @return the context-sensitive URL
     */
    protected String makeContextURL(String absoluteURL) {
        return getRequest().getContextPath() + absoluteURL;
    }
    
    /**
     * Creates a URL that includes both the context path and the application
     * portion of the URL.
     * @param absoluteURL The remaining portion of the URL to append. 
     * @return the full URL.
     */
    protected String makeGenericContextURL(String absoluteURL) {
        return getRequest().getContextPath() + "/"
            + getNavigation().getApplicationMenu() + absoluteURL;
    }

    /**
     * Getter for the roleFeatureMap property.
     * @return RoleFeatureMap value of the property
     */
    public RoleFeatureMap getRoleFeatureMap() {
        return this.roleFeatureMap;
    }

    /**
     * Setter for the roleFeatureMap property.
     * @param roleFeatureMap the new roleFeatureMap value
     */
    public void setRoleFeatureMap(RoleFeatureMap roleFeatureMap) {
        this.roleFeatureMap = roleFeatureMap;
    }
    
    /**
     * Getter for the static pluginCache property. It will lazily set the
     * cache if it hasn't been set.
     * @param pluginManager data source for plugin information.
     * @return List of enabled plugins
     */
    private static synchronized List<PluginModule> getPluginCache(PluginModuleManager pluginManager) {
        if (pluginCache == null) {
            // This never changes without app restart, so make a static
            // cache of the results to avoid querying on every page.
            pluginCache = pluginManager.listEnabledModules();
        }
    
          return pluginCache;
    }
    
    /**
     * @return the list of PluginModules that are enabled and
     * accessible by the current user.
     */
    public List<PluginModule> getAvailablePluginModules() {
        
        List<PluginModule> plugins = getPluginCache(getPluginModuleManager());
        ArrayList<PluginModule> userPlugins = new ArrayList<PluginModule>();
        User user = getCurrentUser();
        
        if (user != null) {       
            for (PluginModule module : plugins) {
                if (module.isAccessibleByUser(getCurrentUser())) {
                    userPlugins.add(module);
                }
            }
        }
        return userPlugins;
    }
    
    /**
     * Decide whether or not the user can see a particular plugin.
     * @param plugin the plugin to check.
     * @return true if the user has access, false if not.
     */
    public boolean getIsPluginAccessible(PluginBase plugin) {
        User user = getCurrentUser();
        if (user != null) {
            return plugin.isAccessibleByUser(user);
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("No user access to plugin " + plugin.getName());
        }
        return false;        
    }
    
    /**
     * Helper function for determining user privlidges.
     * @param group The feature group name in question.
     * @return true if user has access, otherwise false.
     */
    protected boolean hasGroupAccess(String group) {
        User user = getCurrentUser();
        return ((user != null) ? user.getHasAccessInFeatureGroup(group) : false);
    }
    
    /**
     * Save an entity in the session under a generated key.
     * @param o the Object to save in the session.
     * @return the key that the object was saved under. This is also stored
     * in the <code>savedEntityKey</code> field so Freemarker can save it
     * for resubmission.
     */
    protected String saveEntityInSession(Object o) {
        String key = getSessionObjectHandle();
        getSession().setAttribute(key, o);
        setSavedEntityKey(key);
        if (logger.isDebugEnabled()) {
            logger.debug("Saved object " + o + " under key " + key);
        }
        return key;
    }

    /**
     * Get an entity from the session.
     * @param key the key used to retrieve the entity.
     * @return the retrieved entity, or null if there was nothing under
     * the key.
     */
    protected Object getEntityFromSession(String key) {
        return getSession().getAttribute(key);
    }
    
    /**
     * Get an entity from the session and remove it, preventing further
     * access and freeing session memory.
     * @param key the used to retrieve the entity
     * @return the retrieved entity, or null if there was nothing under
     * the key.
     */
    protected Object getAndRemoveEntityFromSession(String key) {
        Object o = getSession().getAttribute(key);
        if (o != null) {
            getSession().removeAttribute(key);
        }
        return o;
    }
    
    /**
     * Get a unique (to this user session) handle for storing things
     * in the session.
     * @return the unique handle.
     */
    private String getSessionObjectHandle() {
        Integer sessionCounter = 
            (Integer) getSession().getAttribute(SESSION_COUNTER_KEY);
        if (sessionCounter == null) {
            // No counter for this session yet. Start one.
            sessionCounter = 1;
        }
        // Store the next value in the counter
        getSession().setAttribute(SESSION_COUNTER_KEY, sessionCounter + 1);
        
        return SAVED_ENTITY_KEY_PREFIX + sessionCounter;
    }

    
    /**
     * Getter for the modifiedEntity property.
     * @return Object value of the property
     */
    public Object getModifiedEntity() {
        return this.modifiedEntity;
    }

    
    /**
     * Setter for the modifiedEntity property.
     * @param modifiedEntity the new modifiedEntity value
     */
    public void setModifiedEntity(Object modifiedEntity) {
        this.modifiedEntity = modifiedEntity;
    }

    /**
     * This stub method provides Struts's message lookup functionality to
     * calling methods that do not need any additional logic.
     * @return the success code to Struts
     */
    public String defaultExecute() {
        return SUCCESS;
    }

    
    /**
     * Getter for the pluginModuleManager property.
     * @return PluginModuleManager value of the property
     */
    public PluginModuleManager getPluginModuleManager() {
        return this.pluginModuleManager;
    }

    
    /**
     * Setter for the pluginModuleManager property.
     * @param pluginModuleManager the new pluginModuleManager value
     */
    public void setPluginModuleManager(PluginModuleManager pluginModuleManager) {
        this.pluginModuleManager = pluginModuleManager;
    }
    
    /**
     * Getter for the dataTranslationManager property.
     * @return DataTranslationManager value of the property
     */
    public DataTranslationManager getDataTranslationManager() {
        return this.dataTranslationManager;
    }

    /**
     * Setter for the dataTranslationManager property.
     * @param dataTranslationManager the new dataTranslationManager value
     */
    public void setDataTranslationManager(DataTranslationManager dataTranslationManager) {
        this.dataTranslationManager = dataTranslationManager;
    }

    /**
     * Translate the specified key for the current Locale by querying
     * the DataTranslation table space.
     * @param key the key to translate
     * @return the translation, or the original value 
     * if the key isn't translated for this Locale.
     */
    public String getDataTranslation(String key) {
        try {
            if (key == null) {
                return null;
            } else {
                return getDataTranslationManager().translate(key);
            }
        } catch (DataAccessException e) {
            logger.error("Unable to get data translation", 
                      SystemErrorCode.DATA_PROVIDER_FAILURE, e);
            return key;
        }
    }

    /**
     * Translate the specified key for the current Locale by querying
     * the DataTranslation table space.
     * @param key the key to translate
     * @return the translation, or <code>null</code> if the key 
     * is null or it isn't translated for this Locale.
     */
    public String getDataTranslationOrNull(String key) {
        if (key == null) {
            return null;
        } else {
            String trans = getDataTranslation(key);
            if (trans.equals(key)) {
                return null;
            } else {
                return trans;
            }
        }
    }

    /**
     * Get a message describing a translated value and the Locale.
     * @param translation the translated value
     * @return the message containing the translated value and the
     * current Locale, or null if the translation is null.
     */
    public String getDataTranslationMessage(String translation) {
        if (translation != null) {
            return getText("translation.message", 
                new String[] {
                    LocaleContextHolder.getLocale().getDisplayName(
                        LocaleContextHolder.getLocale()),
                    translation
                });
        } else {
            return null;
        }
    }
      
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 