/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.ListObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.beanutils.BeanComparator;

/**
 *
 *
 * @author ??
 */
public class SiteEnabledActionRoot extends BaseAction {

    private static final long serialVersionUID = -1950335221679823347L;
    
    private static final Logger logger = new Logger(SiteEnabledActionRoot.class);
    
    private Long currentSiteID;

    private Long tempSiteID;

    private Long summarySiteID;

    // Site Navigation Menu Code
    private String[] homeActions = { "home" };

    private String[] textActions = {
        "viewProfile", "view", "edit", "create",
        "isEditable", "resequencelist"};

    private TagManager tagManager = null;

    // The Site management service.
    private SiteManager siteManager = null;


    /**
     * @return the Site ID for the summaries page.
     */
    public Long getSummarySiteID() {
        return summarySiteID;
    }


    /**
     * @param summarySiteID the Site ID for the summaries page
     */
    public void setSummarySiteID(Long summarySiteID) {
        this.summarySiteID = summarySiteID;
    }

    /**
     * Check if the array contains the value.
     * @param <T> the generic type of the array and value
     * @param arr the array to search in
     * @param val the value to look for
     * @return true if the array contains the value, false otherwise.
     */
    private <T> boolean containsValue(T[] arr, T val) {
        for (T a : arr) {
            if (a.equals(val)) {
                return true;
            }
        }
        return false;
    }

    /**
      * @return the name of this action, without the action piece.
     */
    private String getActionName() {
        // TODO: I think this can be done directly with the Struts
        // ActionContext. -- ddoubleday.
        String fullURI = getRequest().getRequestURI();
        int startIndex = fullURI.lastIndexOf('/') + 1;
        if (startIndex == -1) {
            return null;
        }

        int endIndex;
        if (fullURI.contains("!input")) {
            endIndex = fullURI.indexOf("!input", startIndex);
        } else {
            endIndex = fullURI.indexOf(".action", startIndex);
        }
        if (endIndex == -1) {
            return null;
        }

        return fullURI.substring(startIndex, endIndex);
    }

    /**
     * @return whether or not this is a Home action.
     */
    public boolean isHomeAction() {
        return containsValue(homeActions, getActionName());
    }

    /**
     * @return whether or not this is a text action.
     */
    public boolean isTextAction() {
        return containsValue(textActions, getActionName());
    }

    /**
     * @return success after clearing flow execution key from session.
     */
    public String cleanUpSessionElements() {
        getAndRemoveEntityFromSession("SessionFlowExecKeyInterceptor.SESSION_KEY");
        return SUCCESS;
    }

    /**
     * @return the list of ListObjects for the sites for the current user,
     * with each object containing the tag ID for the Site, and the Site name.
     * @throws DataAccessException on database failure.
     */
    public List<ListObject> getSiteList() throws DataAccessException {
        List<ListObject> objs = new ArrayList<ListObject>();

        for (Site s : getSitesForCurrentUser()) {
            Tag t = tagManager.findByTaggedObjectId(Tag.SITE, s.getId());
            ListObject obj = new ListObject(t.getId(), s.getName());
            objs.add(obj);
        }
        return objs;
    }

    /**
     * @return the list of sites for the current user.
     * @throws DataAccessException on database failure.
     */
    @SuppressWarnings("unchecked")
    public List<Site> getSitesForCurrentUser() throws DataAccessException {
        User u = getCurrentUser();
        List<Site> sites = null;
        if (u.getAllSitesAccess()) {
            sites = siteManager.getAll();
        } else {
            sites = new ArrayList<Site>(u.getSites());
        }
        Collections.sort(sites, new BeanComparator("name"));
        return sites;

    }

    /**
     * @return the number of sites the current user has access to
     * @throws DataAccessException on database failure.
     */
    public int getNumberOfSites() throws DataAccessException {
        return siteManager.getNumberOfSites(getCurrentUser());
    }

    /**
     * @return the site name of the current user's current site
     * @throws DataAccessException on database failure.
     */
    public String getSingleSiteName() throws DataAccessException {
        Long tagId = getCurrentUser().getCurrentSite();
        Tag tag = getTagManager().get(tagId);

        return this.siteManager.get(tag.getTaggableId()).getName();
    }
    
    /**
     * @return the last updated date/time in the current site's time zone
     */
    public String getCurrentSiteDateTime() {
        TimeZone tZ = TimeZone.getDefault();
        try {
            Tag currentTag = tagManager.get(getCurrentUser().getCurrentSite());
            tZ = siteManager.get(currentTag.getTaggableId()).getTimeZone();
        } catch (DataAccessException dae) {
            // We'll just log this error and swallow it.  We just won't be able 
            // to display the time in the time zone of the current site.
            logger.error("DataAccessException while retrieving the current site for timestamp generation.", 
                         SystemErrorCode.DATA_PROVIDER_FAILURE, 
                         dae);
        } catch (Throwable t) {
            // We'll just log this error and swallow it.  We just won't be able 
            // to display the time in the time zone of the current site.
            logger.error("Error while retrieving the current site for timestamp generation.", 
                         SystemErrorCode.UNHANDLED_EXCEPTION, 
                         t);
        }
        
        DateFormat dateFmt = DateFormat.getDateInstance(
                DateFormat.MEDIUM, this.getLocale());
        dateFmt.setTimeZone(tZ);
        
        DateFormat timeFmt = DateFormat.getTimeInstance(
                DateFormat.FULL, this.getLocale());
        timeFmt.setTimeZone(tZ);
        
        Date now = new Date();
        return dateFmt.format(now) + " " + timeFmt.format(now);
    }

    /** End Site Navigation code **/

    /**
     * Getter for the siteManager property.
     * @return siteManager value of the property
     */
    public SiteManager getSiteManager() {
        return this.siteManager;
    }

    /**
     * Setter for the siteManager property.
     * @param siteManager the new siteManager value
     */
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * Getter for the tagManager property.
     * @return TagManager value of the property
     */
    public TagManager getTagManager() {
        return tagManager;
    }

    /**
     * Setter for the tagManager property.
     * @param tagManager the new tagManager value
     */
    public void setTagManager(TagManager tagManager) {
        this.tagManager = tagManager;
    }

    /**
     * @return the currentSiteID property.
     */
    @Deprecated
    public Long getCurrentSiteID() {
        return currentSiteID;
    }

    /**
     * @param currentSiteID the currentSiteID property.
     */
    @Deprecated
    public void setCurrentSiteID(Long currentSiteID) {
        this.currentSiteID = currentSiteID;
    }
    /**
     * @param currentSiteTagID the currentSiteID property.
     */
    public void setCurrentSiteTagId(Long currentSiteTagID) {
        this.currentSiteID = currentSiteTagID;
    }

    /**
     * @return currentSiteID the currentSiteID property.
     */
    public Long getCurrentSiteTagId() {
        return this.currentSiteID;
    }

    /**
     * @return the tempSiteID property.
     */
    public Long getTempSiteID() {
        return tempSiteID;
    }

    /**
     * @param tempSiteID the tempSiteID property.
     */
    public void setTempSiteID(Long tempSiteID) {
        this.tempSiteID = tempSiteID;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 