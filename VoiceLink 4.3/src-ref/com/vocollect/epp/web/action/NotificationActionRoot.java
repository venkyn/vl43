/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.util.JSONResponseBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the Struts action class that handles operations on Notification
 * objects.
 *
 * @author Swathi Voruganti
 */

public class NotificationActionRoot extends DataProviderAction {

    private static final Logger log = new Logger(NotificationAction.class);

    // Serialization identifier.
    private static final long serialVersionUID = -3453400986168842667L;

    private static final long VIEW_ID = -6;

    private static final String SITE_COLUMN = "site";

    private long notificationId;

    private List<Column> columns;

    // Reference to the notification business service.
    private NotificationManager notificationManager;

    private boolean highlightByDefault;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    /**
     * Getter for the notificationManager property.
     *
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * Setter for the NotificationManager property.
     *
     * @param notificationManager the new manager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getNotificationManager();
    }

    /**
     * getter for notification table columns.
     * @return the list of columns that are displayable.
     */
    public List<Column> getNotificationColumns() {
        Iterator<Column> columnIter = this.columns.iterator();
        boolean found = false;
        while (columnIter.hasNext() && !found) {
            Column column = columnIter.next();
            if (column.getField().equals(SITE_COLUMN)) {
                found = true;

                if (SiteContextHolder.getSiteContext().isInAllSiteMode()) {
                    // We only want to display the site column when in the all
                    // display mode
                    column.setDisplayable(true);
                } else {
                    column.setDisplayable(false);
                }
            }
        }

        return this.columns;
    }

    /**
     *
     * @return a string Success on getting columns for notification table
     * @throws DataAccessException on database failure.
     */
    public String list() throws DataAccessException {
        View notificationView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            notificationView, getCurrentUser());
        for (int i = 0; i < columns.size(); i++) {
        }

        // Set the id of the row on the notification display page to be
        // preselected.
        String currentSiteID = getRequest().getParameter("currentSiteID");
        if (currentSiteID != null && isHighlightByDefault()) {
            Long l = notificationManager.getMaxNotificationIdForSiteId(
                NotificationPriority.CRITICAL, new Long(currentSiteID));

            if (l != null) {
                Map<String, HashSet<Long>> hm = new HashMap<String, HashSet<Long>>();
                HashSet<Long> hs = new HashSet<Long>();
                hs.add(l);
                hm.put(String.valueOf(VIEW_ID), hs);
                setSelectedIds(hm);
            }
        }
        this.restoreDefaultColumnsEnabled = this.getUserPreferencesManager()
            .isRestoreDefaultColumnsEnabled(getCurrentUser(), notificationView);

        return SUCCESS;
    }

    /**
     * Refuse to allow viewing of administrator role page. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "notification";
    }

    /**
     * getter for NotificationId Property value.
     *
     * @return notificationId
     */
    public long getNotificationId() {
        return notificationId;
    }

    /**
     * Setter for the NotificationId property.
     *
     * @param notificationId the ID value.
     */
    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    /**
     * Finds unacknowledged critical notifications.
     * @return String success or failure
     * @throws DataAccessException on database failure.
     */
    public String isThereUnAckNotification() throws DataAccessException {
        // Go ahead and call the getGroupedCountBySite...
        List<Object[]> unackCritNotificationsBySite = getNotificationManager()
            .getGroupedCountBySite(
                getCurrentUser(), NotificationPriority.CRITICAL);

        // if the list is size 0, then we have no unacknowledged critical
        // notifications and we should
        // simply return error_success, otherwise return ERROR_FAILURE
        if (unackCritNotificationsBySite.size() != 0) {
            setJsonMessage("", JSONResponseBuilder.ERROR_FAILURE);
        } else {
            setJsonMessage("", JSONResponseBuilder.ERROR_SUCCESS);
        }

        return SUCCESS;
    }

    /**
     * To get notification details for selected notification.
     *
     * @return String
     * @throws DataAccessException on database failure.
     */
    public String getDetailsForId() throws DataAccessException {
        Long id = new Long(getNotificationId());
        JSONObject json = null;
        JSONArray sliderItems = new JSONArray();
        JSONArray jsonary = null;

        Notification n = notificationManager.get(id);
        Set<NotificationDetail> nSet = n.getDetails();

        if (nSet.size() > 0) {
            jsonary = new JSONArray();
            Iterator<NotificationDetail> ndi = nSet.iterator();

            try {

                int i = 0;

                while (ndi.hasNext()) {
                    NotificationDetail ndl = ndi.next();
                    String value = toJSONDetail(ndl.getRenderType(), ndl.getValue());
                    sliderItems.put(value);
                    json = new JSONObject();
                    json.put("label", ResourceUtil.getLocalizedKeyValue(ndl.getKey()));
                    json.put("value", ResourceUtil.getLocalizedKeyValue(value));
                    jsonary.put(i + Integer.parseInt(ndl.getOrdering()), json);
                }

                json = new JSONObject();
                json.put("objects", jsonary);

            } catch (JSONException ex) {
                log.error("Error creating JSON representation",
                    SystemErrorCode.JAVASCRIPT_ERROR, ex);
            }
        }

        if (json == null || json.length() == 0) {
            setMessage("");
        } else {
            setMessage(json.toString());
        }
        return SUCCESS;
    }

    /**
     * To acknowledge critical notification.
     *
     * @return String
     * @throws Exception TODO: This should be improved. Right now
     * a BusinessRuleException can be thrown and nothing is done with it.
     * --ddoubleday.
     */
    public String ackNotification() throws Exception {
        Long[] theIds = getIds(); //Acknowledge multiple Critical Notifications
        for (int i = 0; i < theIds.length; i++) {
            Notification n = notificationManager.get(theIds[i]);
            try {
                if (StringUtil.isNullOrEmpty(n.getAcknowledgedUser())) {
                    n.setAcknowledgedUser(getCurrentUser().getUsername());
                    n.setAcknowledgedDateTime(new Date());
                    notificationManager.save(n);
                    setJsonMessage(getText(new UserMessage(
                        "notification.acknowledge.reply", theIds.length)), JSONResponseBuilder.ERROR_SUCCESS);
                } else {
                    setJsonMessage(getText(new UserMessage(
                        "notification.acknowledge.reply.done")), JSONResponseBuilder.ERROR_FAILURE);
                }
            } catch (OptimisticLockingFailureException e) {
                setJsonMessage(getText(new UserMessage(
                    "notification.acknowledge.reply.done")), JSONResponseBuilder.ERROR_FAILURE);
            }
        }

        return SUCCESS;
    }

    /**
     * @param renderType defines the way to render the detail
     * @param nDetailValue the detail string.
     * @return the detail, rendered according to the method.
     */
    public String toJSONDetail(String renderType, String nDetailValue) {

        String value = "";
        if (renderType.equals("1")) {
            value = nDetailValue;
        } else if (renderType.equals("2")) {
            value = ResourceUtil.getLocalizedKeyValue(nDetailValue);
        } else if (renderType.equals("3")) {
            HttpServletRequest request = getRequest();
            String appUrl = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getLocalPort()
                + request.getContextPath();
            value = appUrl + "/" + nDetailValue;
        } else {
            value = nDetailValue;
        }

        return value;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
         List<ListObject> list = new ArrayList<ListObject>();
         list.add(new ListObject(Tag.ALL, getText("notification.dropdown.site.All")));
         list.add(new ListObject(Tag.SYSTEM, getText("notification.dropdown.site.System")));
         for (ListObject lo : super.getSiteList()) {
             list.add(lo);
         }
         return list;
    }

     /**
      * Getter for the VIEW_ID property.
      * @return long value of the property
      */
     public static long getNotificationViewId() {
         return VIEW_ID;
     }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    /**
     * @return whether to highlight by default.
     */
    public boolean isHighlightByDefault() {
        return highlightByDefault;
    }

    /**
     * @param highlightByDefault whether to highlight by default.
     */
    public void setHighlightByDefault(boolean highlightByDefault) {
        this.highlightByDefault = highlightByDefault;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 