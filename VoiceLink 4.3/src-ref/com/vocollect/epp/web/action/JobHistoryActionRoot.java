/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnFilterType;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.JobManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * This is the Struts Action Class is implemented to handle operations on
 * <code>JobHistory</code> Objects.
 *
 * @author sdoddi
 */
public class JobHistoryActionRoot extends DataProviderAction {

    // Serialization identifier.
    private static final long serialVersionUID = -3453400986168842667L;

    //private static final Logger log = new Logger(JobHistoryAction.class);

    /**
     * The ID of the JobHistory table we need to get from the DD.
     */
    //private static final long VIEW_ID_HISTORY = -5;

    /**
     * Reference to the Job and JobHistory Service.
     */
    private JobManager jobManager;

    /**
     *
     */
    private List<Column> columns;

    /**
     * The ID of the Job.
     */
    private String jobID;

    /**
     * Getter for the jobManager property.
     * @return JobManager value of the property
     */
    public JobManager getJobManager() {
        return this.jobManager;
    }

    /**
     * Setter for the jobManager property.
     * @param jobManager the new jobManager value
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getJobManager();

    }

    /**
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     * @return the prefix.
     */
    @Override
    protected String getKeyPrefix() {
        return "history";
    }

    /**
     * Gets the columns.
     * @return the columns
     */
    public List<Column> getHistoryColumns() {
        return this.columns;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        ArrayList<Long> idList = new ArrayList<Long>();
        if (this.jobID != null) {
            // build an array from strings

            StringTokenizer st = new StringTokenizer(this.jobID, ",");
            while (st.hasMoreTokens()) {
                idList.add(new Long(st.nextToken().trim()));
            }
        }
        if (idList.size() == 0) {
            super.getTableData();
        } else {
            super.getTableData("jobId", ColumnFilterType.Database, idList);
        }

        return SUCCESS;
    }

    /**
     * Setter for the jobId property.
     * @param jobId the new jobId value
     */
    public void setJobId(String jobId) {
        this.jobID = jobId;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 