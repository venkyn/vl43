/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.model.User;


/**
 * Action associated with the Administration home page. It tries to 
 * be smart and show the user the first Administration view link he
 * has access to, if any.
 *
 * @author ddoubleday
 */
public class AdministrationHomeActionRoot extends BaseAction {

    private static final long serialVersionUID = 1730608416142573772L;

    private static final String VIEW_USERS_FEATURE 
        = "feature.userAdmin.user.view";
    private static final String VIEW_ROLES_FEATURE 
        = "feature.userAdmin.role.view";
    private static final String VIEW_LOGS_FEATURE 
        = "feature.appAdmin.log.view";
    private static final String VIEW_SCHEDULES_FEATURE
        = "feature.appAdmin.schedule.view";
    private static final String VIEW_NOTIFICATION_FEATURE
        = "feature.appAdmin.notification.view";
    private static final String VIEW_LICENSE_FEATURE
        = "feature.appAdmin.licensing.view";
    private static final String VIEW_SYSTEM_CONFIGUARTION_FEATURE
        = "feature.appAdmin.systemConfiguration.view";
    private static final String VIEW_SITES_FEATURE 
        = "feature.appAdmin.site.view";
    
    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        
        
        User user = getCurrentUser();
        // This determines which of the sub-pages of Administration
        // should be the user's default view, if any, based on his
        // roles.
        if (user.getHasAccessToFeature(VIEW_NOTIFICATION_FEATURE)) {
            return "viewNotification";
        } else if (user.getHasAccessToFeature(VIEW_USERS_FEATURE)) {
            return "viewUsers";
        } else if (user.getHasAccessToFeature(VIEW_ROLES_FEATURE)) {
            return "viewRoles";
        } else if (user.getHasAccessToFeature(VIEW_LOGS_FEATURE)) {
            return "viewLogs";
        } else if (user.getHasAccessToFeature(VIEW_SCHEDULES_FEATURE)) {
            return "viewSchedules";
        } else if (user.getHasAccessToFeature(VIEW_SITES_FEATURE)) {
            return "viewSites";
        } else if (user.getHasAccessToFeature(VIEW_LICENSE_FEATURE)) {
            return "viewLicense";
        } else if (user.getHasAccessToFeature(VIEW_SYSTEM_CONFIGUARTION_FEATURE)) {
            return "viewSystemConfiguration";
        } else {
          // Send the user to their homepage with a little message...
            addSessionActionErrorMessage(new UserMessage("app.admin.no.view.access"));
            return "goHome";
        }
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 