/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.Summary;
import com.vocollect.epp.model.TabularSummary;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.UpdatingSummary;
import com.vocollect.epp.model.UserHomepage;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.HomepageManager;
import com.vocollect.epp.service.SummaryManager;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.util.SiteContextHolder;


/**
 * This is the Struts action class that handles operations on
 * <code>Summary</code> objects.
 *
 * @author svoruganti
 */
public class ConfigurableHomepagesActionRoot extends DataProviderAction {

    private static final long serialVersionUID = 1730608416142573772L;

    private static final Logger log = new Logger(
        ConfigurableHomepagesAction.class);

    // map of summaries
    private Map<String, Summary> summaries = null;

    // map of summariesLocation
    private Map<String, List<String>> summariesLocation = null;

    // id for homepage
    private long homepageId = -1;

    // service for homepage
    private HomepageManager homepageManager;

    // service for summary
    private SummaryManager summaryManager;

    // The homepage object, which will either be newly created, or retrieved
    // via the homepageId.
    private Homepage homepage;

    // gets all summaries
    private Map<String, Long> allSummaries;

    // elasped time from last login
    private String lastLoginElapsedTime;

    // miliseconds in seconds
    private static final int SECOND_MILLIS = 1000;

    // seconds in minute
    private static final int MINUTE_SECONDS = 60;

    private static final String SITE_COLUMN = "site.name";

    // seconds in hour
    private static final int HOUR_SECONDS = 3600;

    // The ID of the summaryId
    private Long summaryId;

    // The location to be added
    private String location;

    // The list of locations for a summary to be removed
    private List<String> locationsOfSummaryToBeRemoved;

    // The list of locations for a summary to be removed
    private List<String> draggedFromLocation;

    // The list of locations for a summary to be removed
    private List<String> draggedToLocation;

    private String isTableComponent;

    private Long redirectSite;


    /**
     * @return redirectSite
     */
    public Long getRedirectSite() {
        return redirectSite;
    }


    /**
     * @param redirectSite .
     */
    public void setRedirectSite(Long redirectSite) {
        this.redirectSite = redirectSite;
    }

    /**
     * Returns the summaries.
     * @return Map
     */
    public Map<String, Summary> getSummaries() {
        return summaries;
    }

    /**
     * Sets the summaries.
     * @param summaries to set
     */
    public void setSummaries(Map<String, Summary> summaries) {
        this.summaries = summaries;
    }

    /**
     * Returns the homepage.
     * @return homepage
     */
    public Homepage getHomepage() {
        return homepage;
    }

    /**
     * Sets the homepage.
     * @param homepage to set
     */
    public void setHomepage(Homepage homepage) {
        this.homepage = homepage;
    }

    /**
     * Returns the homepageManager.
     * @return homepageManager
     */
    public HomepageManager getHomepageManager() {
        return homepageManager;
    }

    /**
     * Sets the homepageManager.
     * @param homepageManager to set
     */
    public void setHomepageManager(HomepageManager homepageManager) {
        this.homepageManager = homepageManager;
    }

    /**
     * Returns the summaryManager.
     * @return summaryManager
     */
    public SummaryManager getSummaryManager() {
        return summaryManager;
    }

    /**
     * Sets the summaryManager.
     * @param summaryManager to set
     */
    public void setSummaryManager(SummaryManager summaryManager) {
        this.summaryManager = summaryManager;
    }

    /**
     * Sets the summariesLocation.
     * @return map of summary id and list of location
     */
    public Map<String, List<String>> getSummariesLocation() {
        return summariesLocation;
    }

    /**
     * Sets the summaryiesLoaction.
     * @param summariesLocation - Map taht contains summary id and List of
     *            locations
     *
     */
    public void setSummariesLocation(Map<String, List<String>> summariesLocation) {
        this.summariesLocation = summariesLocation;
    }

    /**
     * gets the lastLoginElapsedTime.
     * @return lastLoginElapsedTime
     *
     */
    public String getLastLoginElapsedTime() {
        return lastLoginElapsedTime;
    }

    /**
     * Sets the lastLoginElapsedTime.
     * @param lastLoginElapsedTime - time since user has logged in
     */
    public void setLastLoginElapsedTime(String lastLoginElapsedTime) {
        this.lastLoginElapsedTime = lastLoginElapsedTime;
    }

    /**
     * gets the summaryId.
     * @return summaryId
     *
     */
    public Long getSummaryId() {
        return summaryId;
    }

    /**
     * Sets the summaryId.
     * @param summaryId - time since user has logged in
     */
    public void setSummaryId(Long summaryId) {
        this.summaryId = summaryId;
    }

    /**
     * gets the location.
     * @return location
     *
     */
    public String getLocation() {
        return location;
    }

    /**
     * Sets the location.
     * @param location - location summary needs to be added
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * gets the locations to be removed.
     * @return location
     *
     */
    public List<String> getLocationsOfSummaryToBeRemoved() {
        return locationsOfSummaryToBeRemoved;
    }

    /**
     * Sets the location.
     * @param locationsOfSummaryToBeRemoved - List of location summary needs to
     *            be removed
     */
    public void setLocationsOfSummaryToBeRemoved(List<String> locationsOfSummaryToBeRemoved) {
        this.locationsOfSummaryToBeRemoved = locationsOfSummaryToBeRemoved;
    }

    /**
     * gets the summary is dragged from.
     * @return draggedFromLocation
     *
     */
    public List<String> getDraggedFromLocation() {
        return draggedFromLocation;
    }

    /**
     * Sets the draggedFromLocation.
     * @param draggedFromLocation - location summary is dragged from
     */
    public void setDraggedFromLocation(List<String> draggedFromLocation) {
        this.draggedFromLocation = draggedFromLocation;
    }

    /**
     * gets the locations the summary is dragged to.
     * @return draggedToLocation
     *
     */
    public List<String> getDraggedToLocation() {
        return draggedToLocation;
    }

    /**
     * Sets the draggedToLocation.
     * @param draggedToLocation - location summary needs to be added
     */
    public void setDraggedToLocation(List<String> draggedToLocation) {
        this.draggedToLocation = draggedToLocation;
    }

    /**
     * Returns the List of summaries.
     * @return Long
     * @throws DataAccessException if unable to retrieve summaries
     */
    public String getAdminHomeSummaries() throws DataAccessException {


        Map<String, Long> summaryLocations = this.homepageManager.getSummaries(
            getCurrentUser(), getHomepageId());
        Map<String, Summary> locSummary = new HashMap<String, Summary>();

        // condition to take care that user doesnt have configurable homepages
        // and to get default summaries
        if (summaryLocations == null) {
            Homepage hm = this.homepageManager.getHomepageByUserAndHomepageId(
                getCurrentUser(), getHomepageId());
            if (hm == null) {
                hm = this.homepageManager.getHomepageById(getHomepageId());
                summaryLocations = hm.getSummaries();
            }
        }

        if (summaryLocations != null) {
            Set<String> locations = summaryLocations.keySet();

            Iterator<String> locIter = locations.iterator();
            while (locIter.hasNext()) {
                String loc = locIter.next();
                Long sumId = summaryLocations.get(loc);
                Summary summary = this.summaryManager.get(sumId);

                if (summary instanceof TabularSummary) {
                    TabularSummary tabSum = (TabularSummary) summary;
                    View userView = getUserPreferencesManager().getView(new Long(
                        tabSum.getViewId()));
                    List<Column> columnsForSummary = getUserPreferencesManager().getColumns(
                        userView, getCurrentUser());
                    Long timeWindow = getUserPreferencesManager().getTimeWindow(userView, getCurrentUser());
                    tabSum.setTimeWindow(timeWindow);
                    Iterator<Column> columnIter = columnsForSummary.iterator();
                    boolean found = false;
                    while (columnIter.hasNext() && !found) {
                        Column column = columnIter.next();
                        if (column.getField().equals(SITE_COLUMN)) {
                            found = true;

                            if (SiteContextHolder.getSiteContext().isInAllSiteMode()) {
                                // We only want to display the site column when in the all
                                // display mode
                                column.setDisplayable(true);
                            } else {
                                column.setDisplayable(false);
                            }
                        }
                    }
                    tabSum.setColumns(columnsForSummary);

                    boolean hasPermission = this.summaryManager
                        .checkUserToSummaryPermission(tabSum.getActionUrl()
                            + "/" + tabSum.getActionMethod() + ".action");
                    if (hasPermission) {
                        locSummary.put(loc, tabSum);
                    }

                } else if (summary instanceof UpdatingSummary) {
                    UpdatingSummary updatingSum = (UpdatingSummary) summary;
                    boolean hasPermission = this.summaryManager
                        .checkUserToSummaryPermission(updatingSum
                            .getActionUrl() + "/" + updatingSum.getActionMethod() + ".action");
                    if (hasPermission) {
                        locSummary.put(loc, updatingSum);
                    }
                } else {
                    locSummary.put(loc, summary);
                }
            }

            summariesLocation = getSummariesAndLocations(locSummary);
            setSummariesLocation(summariesLocation);
        }
        return SUCCESS;
    }

    /**
     * Utility method to get a hashmap of summaries and List of Locations that
     * Summary needs to be mapped to .
     * @param map of Summary Locations
     * @return Map
     *
     */
    public Map<String, List<String>> getSummariesAndLocations(Map<String, Summary> map) {

        Map<String, List<String>> result = new HashMap<String, List<String>>();
        summaries = new HashMap<String, Summary>();
        Set<Summary> mapValues = new HashSet<Summary>(map.values());
        Iterator<Summary> it = mapValues.iterator();
        while (it.hasNext()) {

            Summary sum = it.next();
            List<String> keysForVal = new ArrayList<String>();
            Iterator<String> iterator = (map.keySet()).iterator();
            while (iterator.hasNext()) {

                String key = iterator.next().toString();
                Summary value = map.get(key);

                if (value.equals(sum)) {
                    keysForVal.add(key);
                }
            }
            Collections.sort(keysForVal);
            result.put(sum.getId().toString(), keysForVal);
            summaries.put(sum.getId().toString(), sum);
        }

        setSummaries(summaries);

        return result;
    }

    /**
     *
     * @return user name who is currently logged in
     */
    public String getUser() {
        return getCurrentUser().getName();
    }

    /**
     * used to retrieve all summaries for homepage.
     * @return - determines the flow of currrent action
     * @throws DataAccessException - If summaries cannot be retrieved
     *
     */
    public String getAllSummariesForAdd() throws DataAccessException {
        return SUCCESS;
    }

    /**
     * used to retrieve all summaries for homepage.
     * @return map of summary id and location
     * @throws DataAccessException - If summaries cannot be retrieved
     *
     */
    public Map<String, Long> getAllSummaries() throws DataAccessException {
        allSummaries = new TreeMap<String, Long>();
        try {
            Map<String, Long> summaryLocations = this.homepageManager.getSummaries(
                getCurrentUser(), getHomepageId());
            Set<Summary> sum = this.homepageManager.getHomepageById(
                getHomepageId()).getAccessibleSummaries(); //this.getSummaryManager().getAll();
            Map<String, Long> defaultLocations = this.homepageManager
                .getHomepageById(getHomepageId()).getSummaries();

            for (Summary s : sum) {

                if (summaryLocations != null
                    && !summaryLocations.containsValue(s.getId())) {

                    // security check
                    Summary summary = this.summaryManager.get(s.getId());

                    if (summary instanceof TabularSummary
                        || summary instanceof UpdatingSummary) {
                        UpdatingSummary updatingSum = (UpdatingSummary) summary;
                        boolean hasPermission = this.summaryManager
                            .checkUserToSummaryPermission(updatingSum
                                .getActionUrl() + "/" + updatingSum.getActionMethod() + ".action");
                        if (hasPermission) {
                            allSummaries.put(getText(s.getTitle()), s.getId());
                        }
                    } else {
                        allSummaries.put(getText(s.getTitle()), s.getId());
                    }

                } else if (summaryLocations == null && defaultLocations != null
                    && !defaultLocations.containsValue(s.getId())) {

                    Summary summary = this.summaryManager.get(s.getId());

                    if (summary instanceof TabularSummary
                        || summary instanceof UpdatingSummary) {
                        UpdatingSummary updatingSum = (UpdatingSummary) summary;
                        boolean hasPermission = this.summaryManager
                            .checkUserToSummaryPermission(updatingSum
                                .getActionUrl() + "/" + updatingSum.getActionMethod() + ".action");
                        if (hasPermission) {
                            allSummaries.put(getText(s.getTitle()), s.getId());
                        }

                    } else {
                        allSummaries.put(getText(s.getTitle()), s.getId());
                    }
                }
            }
        } catch (Exception e) {
            log.warn("Unable to get all summaries: " + getText(e.getMessage()));
            setMessage(e.getMessage());
        }

        return allSummaries;
    }

    /**
     * Some data to test updating summary .
     *
     * @return String
     */
    public String getSumData() {
        long presentTime = System.currentTimeMillis();
        long lastLogin = getCurrentUser().getLastLoginTime().getTime();
        long timeLastLoggedIn = presentTime - lastLogin;
        long timeElapsed = timeLastLoggedIn / SECOND_MILLIS;

        int time = (int) timeElapsed;
        int hrs = time / HOUR_SECONDS;
        int left = time;
        if (hrs >= 1) {
            left = time % HOUR_SECONDS;
        }
        int mins = left / MINUTE_SECONDS;
        if (mins >= 1) {
            left = left % MINUTE_SECONDS;
        }
        int secs = left;

        lastLoginElapsedTime = hrs + " hrs " + mins + " mins " + secs
            + " secs ";
        setLastLoginElapsedTime(lastLoginElapsedTime);
        return SUCCESS;
    }

    private void tryHomepageSave(Homepage homepage) throws Exception {
    	try {
    		this.homepageManager.save(homepage);
    	} catch (DatabaseConstraintException dce) {
			if (homepage instanceof UserHomepage) {
				UserHomepage userHomepage = (UserHomepage)homepage;
				log.warn("A record already exists in the voc_homepage table with homepageId=" +
					  userHomepage.getHomepageId() + " user_id=" + userHomepage.getUser().getId() + 
					  " and caused a constraint violation - (homepageId, user_id) tuples must be unique.", dce);
			} else {
				log.warn("A constraint violation occurred on the voc_homepage table - (homepageId, user_id) tuples must be unique.", dce);
			}
    	}
    }
    
    /**
     * Adds summaries to db and returns.
     * @return - control flow of action
     * @throws DataAccessException - while trying to retrieve data
     * @throws BusinessRuleException - throws a business rule exception if
     *             unable to save
     */
    public String addSummaries() throws DataAccessException,
        BusinessRuleException {

        Map<String, Long> summaryLocations = null;

        try {
            Homepage hp = this.homepageManager.getHomepageByUserAndHomepageId(
                getCurrentUser(), getHomepageId());
            if (hp != null) {
                summaryLocations = hp.getSummaries();
                // condition to take care of there may be a homepage object but
                // all
                // summaries might have been removed and now being added
                if (hp.getSummaries() == null) {
                    summaryLocations = new HashMap<String, Long>();
                }
                summaryLocations.put(getLocation(), getSummaryId());
                tryHomepageSave(hp);
                
                
            } else {
                UserHomepage uhp = new UserHomepage();
                uhp.setHomepageId(getHomepageId());
                summaryLocations = new HashMap<String, Long>();
                summaryLocations.put(getLocation(), getSummaryId());

                // add default summaries . Assuming always there is atleast one
                // default summary
                Map<String, Long> defaultSummaries = this.homepageManager
                    .getHomepageById(getHomepageId()).getSummaries();
                if (defaultSummaries != null) {

                    Iterator<String> i = defaultSummaries.keySet().iterator();
                    while (i.hasNext()) {
                        Object key = i.next();
                        Object value = defaultSummaries.get(key);
                        summaryLocations.put(key.toString(), new Long(value
                            .toString()));
                    }
                }
                uhp.setSummaries(summaryLocations);
                uhp.setUser(getCurrentUser());
				tryHomepageSave(uhp);

            }

        } catch (Exception e) {
            log.warn("Unable to Add Summary: " + getText(e.getMessage()));
            setMessage(e.getMessage());
        }
        setMessage("Added Successfully");
        if (getRedirectSite().longValue() == Tag.ALL) {
            setJsonMessage("home.action?currentSiteID=" + ((int) Tag.ALL), ERROR_REDIRECT);
        } else {
            setJsonMessage("home.action", ERROR_REDIRECT);
        }
        return SUCCESS;
    }

    /**
     * Removes summaries from db and returns.
     * @return - control flow of action
     * @throws DataAccessException - while trying to retrieve data
     * @throws BusinessRuleException - throws a business rule exception if
     * @throws JSONException - if elements cannot be added
     *
     */
    public String removeSummaries() throws DataAccessException,
        BusinessRuleException, JSONException {
        Map<String, Long> summaryLocations = null;
        Homepage hp;
        try {
            hp = this.homepageManager.getHomepageByUserAndHomepageId(
                getCurrentUser(), getHomepageId());

            if (hp == null) {
                UserHomepage uhp = new UserHomepage();
                uhp.setHomepageId(getHomepageId());
                uhp.setUser(getCurrentUser());
                summaryLocations = new HashMap<String, Long>();

                // add default summaries . Assuming always there is atleast one
                // default summary
                Map<String, Long> defaultSummaries = this.homepageManager
                    .getHomepageById(getHomepageId()).getSummaries();
                if (defaultSummaries != null) {

                    Iterator<String> i = defaultSummaries.keySet().iterator();
                    while (i.hasNext()) {
                        String key = i.next();
                        Long value = defaultSummaries.get(key);
                        summaryLocations.put(key, value);
                    }
                }

                for (int i = 0; i < this.locationsOfSummaryToBeRemoved.size(); i++) {
                    summaryLocations.remove(locationsOfSummaryToBeRemoved
                        .get(i));
                }
                uhp.setSummaries(summaryLocations);
                tryHomepageSave(uhp);
                
            } else {
                summaryLocations = hp.getSummaries();

                for (int i = 0; i < this.locationsOfSummaryToBeRemoved.size(); i++) {
                    summaryLocations.remove(locationsOfSummaryToBeRemoved
                        .get(i));
                }
                tryHomepageSave(hp);
            }
        } catch (Exception e) {
            log.warn("Unable to remove Summary: " + getText(e.getMessage()));
            setMessage(e.getMessage());
        }
        JSONObject json = new JSONObject();
        json.put("isTableComponent", isTableComponent());
        setMessage(json.toString());
        return SUCCESS;
    }

    /**
     * Removes dragAndDrop from db and returns.
     * @return - control flow of action
     * @throws DataAccessException - while trying to retrieve data
     * @throws BusinessRuleException - throws a business rule exception if
     *             unable to save
     */
    public String dragAndDrop() throws DataAccessException,
        BusinessRuleException {

        Map<String, Long> summaryLocations = null;

        try {
            Homepage hp = this.homepageManager.getHomepageByUserAndHomepageId(
                getCurrentUser(), getHomepageId());

            if (hp == null) {
                UserHomepage uhp = new UserHomepage();
                uhp.setHomepageId(getHomepageId());
                uhp.setUser(getCurrentUser());
                summaryLocations = new HashMap<String, Long>();

                // add default summaries . Assuming always there is atleast one
                // default summary
                Map<String, Long> defaultSummaries = this.homepageManager
                    .getHomepageById(getHomepageId()).getSummaries();
                if (defaultSummaries != null) {

                    Iterator<String> i = defaultSummaries.keySet().iterator();
                    while (i.hasNext()) {
                        String key = i.next();
                        Long value = defaultSummaries.get(key);
                        summaryLocations.put(key, value);
                    }
                }

                if (draggedFromLocation != null) {
                    for (int i = 0; i < this.draggedFromLocation.size(); i++) {
                        summaryLocations.remove(draggedFromLocation.get(i)
                            .toString());
                    }
                }
                if (draggedToLocation != null) {
                    for (int j = 0; j < this.draggedToLocation.size(); j++) {
                        summaryLocations.put(draggedToLocation.get(j)
                            .toString(), summaryId);
                    }
                }
                uhp.setSummaries(summaryLocations);
                tryHomepageSave(uhp);

            } else {
                summaryLocations = hp.getSummaries();

                if (draggedFromLocation != null) {
                    for (int i = 0; i < this.draggedFromLocation.size(); i++) {
                        summaryLocations.remove(draggedFromLocation.get(i)
                            .toString());
                    }
                }
                if (draggedToLocation != null) {
                    for (int j = 0; j < this.draggedToLocation.size(); j++) {
                        summaryLocations.put(draggedToLocation.get(j)
                            .toString(), summaryId);
                    }
                }
                hp.setSummaries(summaryLocations);
                tryHomepageSave(hp);

            }

        } catch (Exception e) {
            log.warn("Unable to Update Summary: " + getText(e.getMessage()));
            setMessage(e.getMessage());
        }
        setMessage("Saved Changes Successfully");
        return SUCCESS;
    }


    /**
     * swap summaries from db and returns.
     * @return - control flow of action
     * @throws DataAccessException - while trying to retrieve data
     * @throws BusinessRuleException - throws a business rule exception if
     *             unable to save
     */
    public String swapSummaries() throws DataAccessException,
        BusinessRuleException {

        Map<String, Long> summaryLocations = null;
        Long key1 = null;
        Long key2 = null;

        try {
            Homepage hp = this.homepageManager.getHomepageByUserAndHomepageId(
                getCurrentUser(), getHomepageId());

            if (hp == null) {
                UserHomepage uhp = new UserHomepage();
                uhp.setHomepageId(getHomepageId());
                uhp.setUser(getCurrentUser());
                summaryLocations = new HashMap<String, Long>();

                // add default summaries . Assuming always there is atleast one
                // default summary
                Map<String, Long> defaultSummaries = this.homepageManager.getHomepageById(
                    getHomepageId()).getSummaries();
                if (defaultSummaries != null) {

                    Iterator<String> i = defaultSummaries.keySet().iterator();
                    while (i.hasNext()) {
                        String key = i.next();
                        Long value = defaultSummaries.get(key);
                        summaryLocations.put(key, value);
                    }
                }
                if (draggedFromLocation.size() > 0) {
                    key1 = summaryLocations.get(draggedFromLocation.get(0));
                }
                if (draggedToLocation.size() > 0) {
                    key2 = summaryLocations.get(draggedToLocation.get(0));
                }

                if (draggedFromLocation != null) {
                    for (int i = 0; i < this.draggedFromLocation.size(); i++) {
                        summaryLocations.put(draggedFromLocation.get(i)
                            .toString(), key2);

                    }
                }
                if (draggedToLocation != null) {
                    for (int j = 0; j < this.draggedToLocation.size(); j++) {
                        summaryLocations.put(draggedToLocation.get(j)
                            .toString(), key1);

                    }
                }
                uhp.setSummaries(summaryLocations);
                tryHomepageSave(uhp);

            } else {
                summaryLocations = hp.getSummaries();
                if (draggedFromLocation.size() > 0) {
                    key1 = summaryLocations.get(draggedFromLocation.get(0));
                }
                if (draggedToLocation.size() > 0) {
                    key2 = summaryLocations.get(draggedToLocation.get(0));
                }

                if (draggedFromLocation != null) {
                    for (int i = 0; i < this.draggedFromLocation.size(); i++) {
                        summaryLocations.put(draggedFromLocation.get(i)
                            .toString(), key2);

                    }
                }
                if (draggedToLocation != null) {
                    for (int j = 0; j < this.draggedToLocation.size(); j++) {
                        summaryLocations.put(draggedToLocation.get(j)
                            .toString(), key1);

                    }
                }
                hp.setSummaries(summaryLocations);
                tryHomepageSave(hp);

            }

        } catch (Exception e) {
            log.warn("Unable to Update Summary: " + getText(e.getMessage()));
            setMessage(e.getMessage());
        }
        setMessage("Saved Changes Successfully");
        return SUCCESS;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getHomepageManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "HomePageSummary";
    }


    /**
     * gets the isTableComponent.
     * @return boolean
     *
     */
    public boolean isTableComponent() {
       return this.isTableComponent.equals("true");
    }

    /**
     * Sets the isTableComponent.
     * @param isTableComponent - sets tabelcomponent
     */
    public void setIsTableComponent(String isTableComponent) {
        this.isTableComponent = isTableComponent;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        try {
        	Map<String, Long> summaries = getHomepageManager().getSummaries(getCurrentUser(), getHomepageId());
        	// condition to take care that user doesnt have configurable homepages
            // and to get default summaries
            if (summaries == null) {
                Homepage hm = this.homepageManager.getHomepageByUserAndHomepageId(
                    getCurrentUser(), getHomepageId());
                if (hm == null) {
                    hm = this.homepageManager.getHomepageById(getHomepageId());
                    summaries = hm.getSummaries();
                }
            }
	        Iterator it = summaries.entrySet().iterator();
	        while (it.hasNext()) {
	            Map.Entry pairs = (Map.Entry)it.next();
	            Summary summary = summaryManager.get((Long) pairs.getValue());
	            if (summary instanceof TabularSummary) {
	            	TabularSummary tabSum = (TabularSummary) summary;
	            	viewIds.add(tabSum.getViewId());
	            }
	        }
        } catch (DataAccessException e) {
        	e.printStackTrace();
        }
        return viewIds;
    }


    /**
     * Getter for the hOMEPAGE_ID property.
     * @return long value of the property
     */
    public long getHomepageId() {
        return homepageId;
    }


    /**
     * Setter for the homepageId property.
     * @param homepageId the new homepageId value
     */
    public void setHomepageId(long homepageId) {
        this.homepageId = homepageId;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
         List<ListObject> list = new ArrayList<ListObject>();
         list.add(new ListObject(Tag.ALL, getText("summary.dropdown.site.All")));
        // list.add(new ListObject(Tag.SYSTEM, getText("notification.dropdown.site.System")));
         for (ListObject lo : super.getSiteList()) {
             list.add(lo);
         }
         return list;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 