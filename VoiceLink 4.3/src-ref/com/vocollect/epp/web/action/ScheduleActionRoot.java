/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnFilterType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.ScheduleType;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.JobManager;
import com.vocollect.epp.util.DateUtil;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


/**
 * This is the Struts action class that handles operations on <code>Job</code>
 * objects.
 *
 * @author hulrich
 */
public class ScheduleActionRoot extends DataProviderAction
    implements Preparable {

    // Serialization identifier.
    private static final long serialVersionUID = -3453400986168842667L;

    private static final Logger log = new Logger(ScheduleAction.class);

    private static final long VIEW_ID = -4;

    private static final long VIEW_ID_HISTORY = -8;

    private static final int SECOND_MILLIS = 1000;

    private static final int MINUTE_SECONDS = 60;

    private static final int HOUR_SECONDS = 3600;

    private static final int MERIDIAN_HOURS = 12;

    private static final int MAX_INTERVAL_HOURS = 99;

    private static final int MAX_HOURS = 23;

    private static final int MAX_MINUTES = 59;

    private static final String JOB_INTERVAL = String
        .valueOf(ScheduleType.INTERVAL.getValue());

    private static final String JOB_DAILY = String.valueOf(ScheduleType.DAILY
        .getValue());

    private static final String PRE_ERROR_KEY = "error.scheduler.";

    // Reference to the schedule business service.
    private JobManager jobManager;

    /**
     * The Job Object retrieved by the jobId.
     */
    private Job job;

    private Long jobId;

    private String jobIds;

    private List<Column> columns;

    private List<Column> historyColumns;

    private String[] intervalJobTime = new String[] { "00", "00", "00" };

    private String[] cronJobTime = new String[] { "12", "00", Job.HOUR_24 };

    private String jobType;

    private String[] hrType = new String[] { Job.HOUR_24 };

    private String cronHrType = Job.HOUR_24;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    private boolean historyRestoreDefaultColumnsEnabled;

    /**
     * Getter for the jobManager property.
     *
     * @return JobManager value of the property
     */
    public JobManager getJobManager() {
        return this.jobManager;
    }

    /**
     * Setter for the jobManager property.
     *
     * @param jobManager the new roleManager value
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getJobManager();

    }

    /**
     * Gets the columns.
     * @return the columns
     */
    public List<Column> getSchedulerColumns() {
        return this.columns;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSingleSiteName()
     */
    @Override
    public String getSingleSiteName() throws DataAccessException {
        return getText("site.context.all");
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    public String getHistoryTableData() throws Exception {
        ArrayList<Long> idList = new ArrayList<Long>();
        if (this.jobIds != null) {
            // build an array from strings

            StringTokenizer st = new StringTokenizer(this.jobIds, ",");
            while (st.hasMoreTokens()) {
                idList.add(new Long(st.nextToken().trim()));
            }
        }
        if (idList.isEmpty()) {
            super.getTableData();
        } else {
            super.getTableData("jobId", ColumnFilterType.Database, idList);
        }

        return SUCCESS;
    }

    /**
     * Lists the scheduled jobs.
     * @return the scheduled jobs
     * @throws Exception if a problem occurs creating the JSON Objects.
     */
    public String list() throws Exception {
        this.clearErrorsAndMessages();
        View scheduleView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            scheduleView, getCurrentUser());
        this.restoreDefaultColumnsEnabled = this.getUserPreferencesManager()
            .isRestoreDefaultColumnsEnabled(getCurrentUser(), scheduleView);

        View historyView = getUserPreferencesManager().getView(VIEW_ID_HISTORY);
        this.historyColumns = this.getUserPreferencesManager().getColumns(
            historyView, getCurrentUser());
        this.historyRestoreDefaultColumnsEnabled = this
            .getUserPreferencesManager().isRestoreDefaultColumnsEnabled(
                getCurrentUser(), historyView);

        return SUCCESS;
    }

    /**
     * Refuse to allow viewing of administrator role page. {@inheritDoc}
     *
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    /**
     * Begins running the jobs with the ID's set in the <code>DataActionProvider
     * </code>.
     *
     * @return the control flow target name (SUCCESS on success, INPUT on
     *         constraint error.
     * @throws Exception on any unanticipated failure.
     */
    public String runNow() throws Exception {
        return performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.UNHANDLED_EXCEPTION;
            }

            public String getActionPrefix() {
                return "run";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                JobManager manager = (JobManager) getManager();
                manager.executeStartJob(obj.getId());
                return true;
            }
        });
    }

    /**
     * Stops the jobs with the ID's set in the <code>DataActionProvider
     * </code>.
     *
     * @return the control flow target name (SUCCESS on success, INPUT on
     *         constraint error.
     * @throws Exception on any unanticipated failure.
     */
    public String stopNow() throws Exception {
        return performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.UNHANDLED_EXCEPTION;
            }

            public String getActionPrefix() {
                return "stop";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                JobManager manager = (JobManager) getManager();
                manager.executeStopJob(obj.getId());

                return true;

            }

        });
    }

    /**
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     * @return the prefix.
     */
    @Override
    protected String getKeyPrefix() {
        return "schedule";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * Edits the job.
     * @return success or failure
     * @throws Exception on catastrophic failure
     */
    public String save() throws Exception {
        String ret = SUCCESS;
        if (JOB_INTERVAL.equals(this.getJobType())) {
            this.getJob().setType(ScheduleType.INTERVAL);
            this.getJob().setInterval(this.getInterval());

        } else {
            this.getJob().setType(ScheduleType.DAILY);
            this.getJob().setCronHrs(this.getCronHourType());
            this.getJob().setCronExpression(this.getCronExpression());
        }
        this.setIds(new Long[] { this.getJobId() });

        ret = performAction(new CustomActionImpl() {

            private SystemErrorCode errorCode = SystemErrorCode.NO_ERROR;

            public SystemErrorCode getSystemErrorCode() {
                return errorCode;
            }

            public String getActionPrefix() {
                return "edit";
            }

            public boolean execute(DataObject obj)
                throws VocollectException, DataAccessException,
                BusinessRuleException {
                try {
                    JobManager manager = (JobManager) getManager();
                    manager.saveJob(job);
                    addSessionActionMessage(new UserMessage(
                        "schedule.edit.message.success",
                        makeContextURL("/admin/schedule/view.action?jobId="
                            + getJobId()), getText(getJob().getDisplayName())));
                } catch (OptimisticLockingFailureException e) {
                    if (setModifiedJob()) {
                        addActionError(new UserMessage(
                            "entity.error.modified", UserMessage
                                .markForLocalization("entity.Job"),
                            SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
                    }

                    return false;
                }
                return true;
            }

            private boolean setModifiedJob() throws DataAccessException {
                try {
                    Job modifiedJob = getJobManager().updateAndGetJob(
                        job.getId());
                    job.setVersion(modifiedJob.getVersion());
                    modifiedJob.setDisplayName(getText(job.getDisplayName()));
                    setModifiedEntity(modifiedJob);
                    log.debug("Modified entity : " + modifiedJob.getEnabled());
                } catch (EntityNotFoundException ex) {
                    addSessionActionMessage(new UserMessage(
                        "entity.error.deleted", UserMessage
                            .markForLocalization(job.getName()), null));
                    return false;
                }
                return true;
            }

        });
        if (this.getActionErrors().size() > 0) {
            ret = INPUT;
        }
        log.debug("ON SAVE : " + ret);
        return ret;

    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        log.debug("preparing ....");
        if (this.getJobId() != null) {
            if (this.getSavedEntityKey() == null) {

                this.job = this.jobManager.updateAndGetJob(getJobId());
                this.prepareTimeComponent();
                this.saveEntityInSession(this.job);
            } else {
                this.job = (Job) this.getEntityFromSession(this
                    .getSavedEntityKey());
            }
            this.setJobType(String.valueOf(this.job.getType().getValue()));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#validate()
     */
    @Override
    public void validate() {
        log.debug("validating.........." + JOB_INTERVAL + " ............. "
            + getJobType());

        if ((getJobType()).equals(JOB_INTERVAL)) {
            if (this.getInterval() == 0) {
                addFieldError("intervalHours", new UserMessage(
                    "error.scheduler.edit.interval.nonzero"));
            } else {
                this.validateField(
                    "intervalHours", intervalJobTime[0], "hours", 0,
                    MAX_INTERVAL_HOURS);
                this.validateField(
                    "intervalHours", intervalJobTime[1], "minutes", 0,
                    MAX_MINUTES);
                this.validateField(
                    "intervalHours", intervalJobTime[2], "seconds", 0,
                    MAX_MINUTES);
            }
        } else {
            if ((cronJobTime[2].equals(Job.HOUR_AM))
                || (cronJobTime[2].equals(Job.HOUR_PM))) {
                this.validateField(
                    "cronHours", cronJobTime[0], "hours", 1, MERIDIAN_HOURS);
            } else {
                this.validateField(
                    "cronHours", cronJobTime[0], "hours", 0, MAX_HOURS);
            }
            this.validateField(
                "cronHours", cronJobTime[1], "minutes", 0, MAX_MINUTES);
        }
    }

    /**
     * @return the job
     */
    public Job getJob() {
        return job;
    }

    /**
     * @param job the job to set
     */
    public void setJob(Job job) {
        this.job = job;
    }

    /**
     * @return localized string representing last run date
     */
    public String getJobDisplayLastRun() {
        return DateUtil.formatTimestampForTableComponent(
            getJob().getLastRun());
    }

    /**
     * @return localized string representing next run date
     */
    public String getJobDisplayNextRun() {
        return DateUtil.formatTimestampForTableComponent(
            getJob().getLastRun());
    }

    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     * @throws DataAccessException Any DataBaseException
     */
    public String getDisplayLastStarted() throws DataAccessException {
        return DateUtil.formatTimestampForTableComponent(
            getJob().getLastStarted());
    }

    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     * @throws DataAccessException Any DataBaseException
     */
    public String getDisplayLastRun() throws DataAccessException {
        return DateUtil.formatTimestampForTableComponent(
            getJob().getLastRun());
    }

    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     * @throws DataAccessException Any DataBaseException
     */
    public String getDisplayNextRun() throws DataAccessException {
        return DateUtil.formatTimestampForTableComponent(
            getJob().getNextRun());

    }

    /**
     * Get the possible <code>Job Schedule</code> statuses for display.
     * @return the Map of defined <code>Job</code> status, where the map key
     *         is true/false, and the map value is the resource key for the
     *         status name.
     */
    public Map<Boolean, String> getStatusMap() {
        Map<Boolean, String> map = new LinkedHashMap<Boolean, String>();
        map.put(true, getText(Job.ENABLED));
        map.put(false, getText(Job.DISABLED));
        return map;
    }

    /**
     * Builds the job type map.
     * @return the job type map
     */
    public Map<String, String> getJobTypeMap() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        map.put(JOB_INTERVAL, this.getText("schedule.type.interval"));
        map.put(JOB_DAILY, this.getText("schedule.type.daily"));
        return map;
    }

    /**
     * Whether the job is of type interval, used for read only display.
     * @return whether the job is of interval
     */
    public boolean getIsInterval() {
        return JOB_INTERVAL.equals(job.getType());
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getIntervalHours() {
        return this.getIntervalJobTime()[0];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setIntervalHours(String val) {
        this.getIntervalJobTime()[0] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getIntervalJobTime()[0] = "00";
        }
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getIntervalMinutes() {
        return this.getIntervalJobTime()[1];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setIntervalMinutes(String val) {
        this.getIntervalJobTime()[1] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getIntervalJobTime()[1] = "00";
        }
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getIntervalSeconds() {
        return this.getIntervalJobTime()[2];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setIntervalSeconds(String val) {
        this.getIntervalJobTime()[2] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getIntervalJobTime()[2] = "00";
        }
    }

    /**
     * Gets this time component for the job time.
     * @return the time component
     */
    public String getCronHourType() {
        return cronJobTime[2];
    }

    /**
     * Sets this time component.
     * @param time the time component
     */
    public void setCronHourType(String time) {
        cronJobTime[2] = time;
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getCronHours() {
        return this.getCronJobTime()[0];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setCronHours(String val) {
        this.getCronJobTime()[0] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getCronJobTime()[0] = "00";
        }
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getCronMinutes() {
        return this.getCronJobTime()[1];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setCronMinutes(String val) {
        this.getCronJobTime()[1] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getCronJobTime()[1] = "00";
        }
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getCronSeconds() {
        return this.getCronJobTime()[2];
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setCronSeconds(String val) {
        this.getCronJobTime()[2] = val;
        if (val == null || val.trim().equalsIgnoreCase("")) {
            this.getCronJobTime()[2] = "00";
        }
    }

    /**
     * Retrieves the time component.
     * @return the time component
     */
    public String getCronHrType() {
        return this.cronHrType;
    }

    /**
     * Sets the time component.
     * @param val the time component
     */
    public void setCronHrType(String val) {
        this.cronHrType = val;
    }

    /**
     * Gets the columns.
     * @return the columns
     */
    public List<Column> getHistoryColumns() {
        return this.historyColumns;
    }

    /**
     * Populates the time component.
     */
    private void prepareTimeComponent() {
        if (ScheduleType.INTERVAL.equals(this.getJob().getType())) {
            prepareIntervalTime();
        } else {
            prepareCronTime();
        }
    }

    /**
     * Populates the interval component.
     */
    private void prepareIntervalTime() {
        this.intervalJobTime = prepareIntervalTime(getJob().getInterval());
    }

    /**
     * Helper method for calculating interval time.
     * @param t the interval
     * @return the time
     */
    private String[] prepareIntervalTime(double t) {
        String[] toks = new String[3];
        t = t / SECOND_MILLIS;
        int time = (int) t;
        int hrs = time / HOUR_SECONDS;
        int left = time;
        if (hrs >= 1) {
            left = time % HOUR_SECONDS;
        }
        int mins = left / MINUTE_SECONDS;
        if (mins >= 1) {
            left = left % MINUTE_SECONDS;
        }
        int secs = left;
        toks[0] = "" + hrs;
        toks[1] = "" + mins;
        toks[2] = "" + secs;
        for (int i = 0; i < toks.length; i++) {
            int temp = Integer.parseInt(toks[i]);
            if (temp < 10) {
                toks[i] = "0" + temp;
            }
        }
        return toks;
    }

    /**
     * Populates the interval component.
     */
    private void prepareCronTime() {
        this.setCronJobTime(prepareCronTime(
            getJob().getCronExpression(), getJob().getCronHrs()));
    }

    /**
     * Helper method for calculating cron time.
     * @param expression the cron expression
     * @param cronHours the hour type (AM, PM)
     * @return the time
     */
    private String[] prepareCronTime(String expression, String cronHours) {
        String[] exp = new String[3];
        String[] toks = expression.split("\\s");

        String mins = toks[1];
        exp[1] = String.format("%02d", Integer.parseInt(mins.trim()));

        if (cronHours.equals(Job.HOUR_24)) {
            String hours = toks[2];
            exp[0] = String.format("%02d", Integer.parseInt(hours.trim()));
            exp[2] = Job.HOUR_24;
        } else if (cronHours.equals(Job.HOUR_PM)) {
            int tm = Integer.parseInt(toks[2]);
            if (tm > MERIDIAN_HOURS) {
                tm = tm - MERIDIAN_HOURS;
            }
            exp[0] = "" + tm;
            exp[2] = Job.HOUR_PM;
        } else {
            exp[0] = toks[2];
            exp[2] = Job.HOUR_AM;
        }
        return exp;
    }

    /**
     * Getter for the cronJobTime property.
     * @return String[] value of the property
     */
    public String[] getCronJobTime() {
        return this.cronJobTime;
    }

    /**
     * Setter for the cronJobTime property.
     * @param cronJobTime the new cronJobTime value
     */
    public void setCronJobTime(String[] cronJobTime) {
        this.cronJobTime = cronJobTime;
    }

    /**
     * Getter for the intervalJobTime property.
     * @return String[] value of the property
     */
    public String[] getIntervalJobTime() {
        return this.intervalJobTime;
    }

    /**
     * Setter for the intervalJobTime property.
     * @param intervalJobTime the new intervalJobTime value
     */
    public void setIntervalJobTime(String[] intervalJobTime) {
        this.intervalJobTime = intervalJobTime;
    }

    /**
     * Getter for the hrType property.
     * @return String[] value of the property
     */
    public String[] getHrType() {
        return this.hrType;
    }

    /**
     * Returns a list of hour string and localized hour string.
     * @return Map
     */
    public HashMap<String, String> getHrTypeMap() {
        HashMap<String, String> hrTypeMap = new HashMap<String, String>();
        hrTypeMap.put(Job.HOUR_24, getText("schedule.twentyfourhour"));
        return hrTypeMap;
    }

    /**
     * Setter for the hrType property.
     * @param hrType the new hrType value
     */
    public void setHrType(String[] hrType) {
        this.hrType = hrType;
    }

    /**
     * Converts the edited interval values into an interval of ms.
     * @return long schedule interval value
     */
    public long getInterval() {
        try {
            int hr = Integer.parseInt(this.getIntervalJobTime()[0]);
            int min = Integer.parseInt(this.getIntervalJobTime()[1]);
            int secs = Integer.parseInt(this.getIntervalJobTime()[2]);

            long time = hr * HOUR_SECONDS * SECOND_MILLIS;
            time = time + (min * MINUTE_SECONDS * SECOND_MILLIS);
            time = time + (secs * SECOND_MILLIS);

            return time;
        } catch (NumberFormatException nfe) {

        }
        return 0;
    }

    /**
     * Converts the edited cron values into a cron expression.
     * @return String a Quartz version of a cron string.
     */
    public String getCronExpression() {
        String cron = "0 " + this.getCronJobTime()[1] + " ";
        int hr = Integer.parseInt(this.getCronJobTime()[0]);
        if ((this.getCronJobTime()[2].equals(Job.HOUR_PM)) && (hr < MERIDIAN_HOURS)) {
            cron = cron + (hr + MERIDIAN_HOURS);
        } else {
            cron = cron + hr;
        }

        cron = cron + " * * ?";
        return cron;

    }

    /**
     * Getter for the jobId property.
     * @return Long value of the property
     */
    public Long getJobId() {
        return this.jobId;
    }

    /**
     * Setter for the jobId property.
     * @param jobId the new jobId value
     */
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    /**
     * Getter for the jobType property display.
     * @return String value of the property
     */
    public String getJobTypeDisplay() {
        if (JOB_INTERVAL.equals(this.jobType)) {
            return this.getText("schedule.type.interval");
        } else {
            return this.getText("schedule.type.daily");
        }
    }

    /**
     * Getter for the jobType property.
     * @return String value of the property
     */
    public String getJobType() {
        return this.jobType;
    }

    /**
     * Setter for the jobType property.
     * @param jobType the new jobType value
     */
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    /**
     * For viewing schedule, helper display function because IE could not
     * display the text fields.
     * @return the display time
     */
    public String getTimeDisplay() {
        if (this.getJobType().equals(JOB_INTERVAL)) {
            return this.getIntervalHours() + this.getText("semicolon")
                + this.getIntervalMinutes() + this.getText("semicolon")
                + this.getIntervalSeconds() + " "
                + this.getText("schedule.edit.interval.timeformat");
        } else {
            return this.getCronHours() + this.getText("semicolon")
                + this.getCronMinutes() + this.getText("semicolon")
                + this.getCronHourType() + " "
                + this.getText("schedule.edit.daily.timeformat");
        }
    }

    /**
     * @param txt Time in text format
     */
    public void setTimeDisplay(String txt) {

    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getSchedulesViewId() {
        return VIEW_ID;
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getHistoryViewId() {
        return VIEW_ID_HISTORY;
    }

    /**
     * Getter for the historyRestoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isHistoryRestoreDefaultColumnsEnabled() {
        return historyRestoreDefaultColumnsEnabled;
    }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        viewIds.add(VIEW_ID_HISTORY);
        return viewIds;
    }

    /**
     * @return the ids selected in the table component
     */
    public String getJobIds() {
        return jobIds;
    }

    /**
     * @param jobIds comma separated list of job IDs
     */
    public void setJobIds(String jobIds) {
        this.jobIds = jobIds;
    }

    /**
     * Gets the type of the modified job.
     * @return the type
     */
    public String getModifiedType() {
        String ret = getJob().getType().getResourceKey();
        if (getModifiedEntity() != null) {
            ScheduleType t = ((Job) getModifiedEntity()).getType();
            if (t == ScheduleType.INTERVAL) {
                ret = getText("schedule.type.interval");
            } else {
                ret = getText("schedule.type.daily");
            }
        }
        return ret;
    }

    /**
     * Gets the modified time display.
     * @return the modified time display
     */
    public String getModifiedTimeDisplay() {

        String ret = this.getTimeDisplay();
        if ((getModifiedEntity() != null)
            && ((((Job) getModifiedEntity()).getType()) == ScheduleType.INTERVAL)) {
            String[] time = prepareIntervalTime(((Job) getModifiedEntity())
                .getInterval());
            ret = time[0] + this.getText("semicolon") + time[1]
                + this.getText("semicolon") + time[2] + " "
                + this.getText("schedule.edit.interval.timeformat");
        } else if ((getModifiedEntity() != null)
            && ((((Job) getModifiedEntity()).getType()) == ScheduleType.DAILY)) {
            String[] time = prepareCronTime(((Job) getModifiedEntity())
                .getCronExpression(), ((Job) getModifiedEntity()).getCronHrs());
            ret = time[0] + this.getText("semicolon") + time[1]
                + this.getText("semicolon") + time[2] + " "
                + this.getText("schedule.edit.daily.timeformat");
        }
        return ret;
    }

    /**
     * Gets the status of the modified job.
     * @return the status
     */
    public String getModifiedStatus() {
        String ret = getText(Job.ENABLED);
        if (!getJob().getEnabled()) {
            ret = getText(Job.DISABLED);
        }
        if ((getModifiedEntity() != null)
            && (((Job) getModifiedEntity()).getEnabled())) {
            ret = getText(Job.ENABLED);
        } else if ((getModifiedEntity() != null)
            && (!((Job) getModifiedEntity()).getEnabled())) {
            ret = getText(Job.DISABLED);
        }

        return ret;
    }

    /**
     * Validates a particular field.
     * @param field the field to validate
     * @param value the value of the field
     * @param fieldType the field type - "hours", "minutes"
     * @param min the min value for the field
     * @param max the max value for the field
     */
    private void validateField(String field,
                               String value,
                               String fieldType,
                               int min,
                               int max) {
        try {
            String fixedValue = value;
            int val = Integer.parseInt(fixedValue);
            if ((val < min) || (val > max)) {
                log.debug("Adding message for ==> " + field);
                log
                    .debug("Message is ==> "
                        + buildErrorKey(
                            PRE_ERROR_KEY, "edit", fieldType, min, max));
                addFieldError(field, new UserMessage(buildErrorKey(
                    PRE_ERROR_KEY, "edit", fieldType, min, max)));
            }
        } catch (NumberFormatException e) {
            addFieldError(field, new UserMessage(field, buildErrorKey(
                PRE_ERROR_KEY, "edit", fieldType, min, max)));
        }
    }

    /**
     * Simple method that builds an error message key.
     * @param errorMsg the first part of the key - "error.scheduler.edit."
     * @param actionType the action type - "edit"
     * @param fieldType the field type - "hours"
     * @param min - the min amount - "0"
     * @param max - the max amount - "23"
     * @return the built error key - "error.scheduler.edit.1.12.field"
     */
    private String buildErrorKey(String errorMsg,
                                 String actionType,
                                 String fieldType,
                                 int min,
                                 int max) {
        return errorMsg + actionType + "." + min + "." + max + "." + fieldType;
    }

    /**
     * Simple method that gets the job process name.
     *
     * @return the Process name - "*.Job.ProcessName"
     */
    public String getJobDisplayName() {
        if (getText(this.job.getDisplayName()) == null) {
            if (log.isDebugEnabled()) {
                log.debug("Job Process Name not found.");
            }
            return null;

        } else {
            return getText(this.job.getDisplayName());
        }
    }

    /**
     * @return the result of the last job run
     */
    public String getJobLastRunResult() {
        return getText(this.job.getLastRunResult().getResourceKey());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 