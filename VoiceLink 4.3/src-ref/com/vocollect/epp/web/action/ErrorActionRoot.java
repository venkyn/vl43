/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Date;

/**
 * Error handling Action.
 *
 * @author Dennis Doubleday
 */

public class ErrorActionRoot extends BaseAction {

    private static final long serialVersionUID = -6789434266454802728L;

    private static Logger log = new Logger(ErrorAction.class);

    /**
     * An enumeration of the valid error page types.
     */
    public enum ErrorPageType { error,
                                errorInternal,
                                error404,
                                errorEntityNotFound,
                                errorAccess
    }

    /**
     * This gets set to the type of error page to display.
     */
    private String error;

    /**
     * The error time is always assumed to be now. This works because the
     * ErrorAction is not a singleton--Struts creates it every time.
     */
    private Date errorTime = new Date();

    /**
     * Always returns success. The action is just called to set the error
     * information.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     * @return Always return SUCCESS
     * @throws Exception
     */
    @Override
    public String execute() throws Exception {

        // Output information about what was happening when the error occurred.
        log.error("Error request attribute 'javax.servlet.forward.request_uri': "
            + getRequest().getAttribute("javax.servlet.forward.request_uri")
            + ", errorType: " + getError(),
            SystemErrorCode.UNHANDLED_EXCEPTION);
        log.error("Error request attribute 'javax.servlet.error.request_uri': "
            + getRequest().getAttribute("javax.servlet.error.request_uri"),
            SystemErrorCode.UNHANDLED_EXCEPTION);
        // log the stack trace, if any.
        Object obj = ActionContext.getContext().getValueStack().findValue("exceptionStack");
        if (obj != null) {
            log.error(obj.toString(), SystemErrorCode.UNHANDLED_EXCEPTION);
        }
        return ActionSupport.SUCCESS;
    }

    /**
     * Getter for the error property.
     * @return the value of the error property, or a default value if the error
     *         is null or if the String isn't a valid <code>ErrorPageType</code>
     *         enum name
     */
    public String getError() {
        try {
            if (this.error == null || ErrorPageType.valueOf(this.error) == null) {

                log.warn("Invalid exception page type " + this.error
                    + " specified");

                return ErrorPageType.error.name();

            } else {
                return this.error;
            }
        } catch (RuntimeException e) {
            log.warn("Invalid exception page type " + this.error + " specified");
            return ErrorPageType.error.name();
        }
    }

    /**
     *
     * @param error Sets the type of error.
     */
    public void setError(String error) {
        this.error = error;

        if (log.isDebugEnabled()) {
            log.debug("Exception page type is " + this.error);
        }
    }

    /**
     * Getter for the errorTime property.
     * @return Error time
     */
    public Date getErrorTime() {
        return this.errorTime;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 