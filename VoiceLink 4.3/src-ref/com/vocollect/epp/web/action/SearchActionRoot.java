/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.search.SearchResult;
import com.vocollect.epp.search.SearchResultDisplayCollection;
import com.vocollect.epp.search.SearchResultList;
import com.vocollect.epp.service.SearchManager;
import com.vocollect.epp.ui.Breadcrumb;
import com.vocollect.epp.ui.Navigation;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hulrich
 */
public class SearchActionRoot extends SiteEnabledAction {

    private static final Logger log = new Logger(SearchAction.class);

    // Serialization identifier.
    private static final long serialVersionUID = -3453435986168842667L;

    private SearchManager searchManager;

    private String searchText;

    private SearchResultDisplayCollection results;

    private Map<String, Long> siteNameToId;

    /**
     * Action method to display the search results.
     * @return String the success value
     * @throws Exception if a problem occurs fetching search results.
     */
    public String search() throws Exception {

        if (log.isDebugEnabled()) {
            log.debug("Country locale == " + this.getLocale().getDisplayName()
                + " --" + this.getLocale());
        }

        SearchResultList<SearchResult> list = getSearchManager().searchAll(
            this.getSearchText(), getNavigation().getApplicationMenu());

        results = SearchResultDisplayCollection.
            buildDisplayCollection(list, this.getCurrentUser(),
                                   searchManager.getCategoryResultLimit());
        List<Site> sites = getSiteManager().getAll();
        this.siteNameToId = new HashMap<String, Long>();
        for (Site s : sites) {
            siteNameToId.put(s.getName(),
                             SiteContextHolder.getSiteContext().getTagBySiteId(s.getId()).getId());
        }

        return SUCCESS;
    }

    /**
     * @return Map The tag ids necessary for building sitespecific links.
     */
    public Map<String, Long> getSiteNameToId() {
        return this.siteNameToId;
    }

    /**
     * Gets the result collection.
     * @return the result collection
     */
    public SearchResultDisplayCollection getResultCollection() {
        return results;
    }

    /**
     * Getter for the searchManager property.
     * @return SearchManager value of the property
     */
    public SearchManager getSearchManager() {
        return this.searchManager;
    }

    /**
     * Setter for the searchManager property.
     * @param searchManager the new searchManager value
     */
    public void setSearchManager(SearchManager searchManager) {
        this.searchManager = searchManager;

    }

    /**
     * Getter for the searchText property.
     * @return String value of the property
     */
    public String getSearchText() {
        return this.searchText;
    }

    /**
     * Setter for the searchText property.
     * @param searchText the new searchText value
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    /**
     * Getter for the searchSuccessText property. This text will be displayed if
     * the search fetches one or more results
     * @return String value of the property
     */
    public String getSearchSuccessText() {
        if (results.getCount() > 1) {
            if (results.getHiddenCount() > 0) {
                return getText("nav.search.result.plural.limited",
                    new String[] {getSearchText(),
                                  "" + results.getCount(),
                                  "" + (results.getCount() - results.getHiddenCount())});
            } else {
                return getText("nav.search.result.plural",
                               new String[] {getSearchText(),
                                             results.getCount() + ""});
            }
        } else {
            return getText("nav.search.result.singular",
                           new String[] { getSearchText(),
                                          results.getCount() + ""});
        }

    }

    /**
     * Getter for the searchNoresultsText property. This text will be displayed
     * if the search fetches no results
     * @return String value of the display text
     */
    public String getSearchNoResultsText() {
        return getText("nav.search.noresult", new String[] {getSearchText()});

    }

    /**
     * Getter for the getResultSizeText property. This text contains the number
     * of results fetched for Area/Site based on what information is passed to
     * it.
     * @param name the name of the Area/Site to be displayed
     * @param size the size of the Area/Site to be displayed
     * @param hiddenCount the number of hidden hits in the Area/Site.
     * @return String value of the display text
     */
    public String getResultSizeText(String name, String size, String hiddenCount) {
        String locName = getText(name);
        int sizeNum = Integer.parseInt(size);
        int hiddenCountNum = Integer.parseInt(hiddenCount);
        if (sizeNum > 1) {
            if (hiddenCountNum > 0) {
                return getText("nav.search.sizetext.plural.limited",
                    new String[] {locName, "" + (sizeNum + hiddenCountNum), hiddenCount});
            } else {
                return getText("nav.search.sizetext.plural",
                               new String[] {locName, size});
            }
        } else {
            return getText("nav.search.sizetext.singular",
                           new String[] {locName, size});
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseAction#getBreadCrumbTrail()
     */
    @Override
    public List<Breadcrumb> getBreadCrumbTrail() {
        Navigation nav = new Navigation(getSession().getAttribute(
            "SEARCH_BREADCRUMB").toString());
        return nav.getBreadcrumbTrail();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 