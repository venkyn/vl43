/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.LicenseAuthenticationException;
import com.vocollect.epp.exceptions.LicenseFormatException;
import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemLicense;
import com.vocollect.epp.service.LicenseManager;
import com.vocollect.epp.web.licensing.enforcement.LicenseStatus;
import com.vocollect.epp.web.licensing.enforcement.Licensers;

import com.opensymphony.xwork2.ActionSupport;

import java.nio.charset.Charset;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 *
 *
 * @author mressler
 */
public abstract class LicenseAwareAction extends ActionSupport {

    private static int companyCount = 0;
    private static int siteCount = 0;
    private static int violationCount = 0;

    private static final Logger log = new Logger(LicenseAwareAction.class);

    /**
     * Everyone needs to have access to the LicenseManager object so
     * that we can pull Licenseinformation to be displayed in the header.
     * Auto-wire to the rescue.
     */
    private LicenseManager licenseManager;

    /**
     * Convenience method to get the request object.
     * @return current request
     */
    public abstract HttpServletRequest getRequest();

    /**
     * Convenience method to get the user's HTTP session.
     * @return the User session
     */
    public abstract HttpSession getSession();

    /**
     * Add a <code>UserMessage</code> to the list of action messages that
     * are associated with the user HTTP session.
     * @param msg the message to add
     */
    public abstract void addSessionActionMessage(UserMessage msg);

    /**
     * Resolve and add a message to sessionActionMessages Map in the
     * specified category.
     * @param category the category (map key) to add to
     * @param msg the message to translate and add.
     */
    protected abstract void addMessageToSessionActionMessages(String category,
                                                              UserMessage msg);

    /**
     * @param licenseInfo the license info
     */
    public final void setCurrentLicenseInfo(VoiceLicense licenseInfo) {
        getSession().setAttribute(LICENSE_COMPANY_NAME, licenseInfo.getLicenseeDisplayName());
        getSession().setAttribute(LICENSE_COMPANY_SITE, licenseInfo.getLicenseeDisplaySite());
    }

    /**
     * @return the company named in the current license.
     */
    public final String getCurrentLicenseCompany() {
        HttpSession session = getSession();
        String company = null;

        // sometimes we can have a null session.  If this is the case then we'll
        // be retrieving the info from the DB and short-circuiting the session

        try {
            if (session != null) {
                company = (String) session.getAttribute(LICENSE_COMPANY_NAME);
            } else {
                log.warn("getCurrentLicenseCompany called without an associated session");
            }

            companyCount++;

            if (company == null || session == null) {
                VoiceLicense currentLicense = getCurrentVoiceLicense();

                if (currentLicense != null) {
                    company = currentLicense.getLicenseeDisplayName();
                    if (company == null) {
                        company = currentLicense.getLicenseeName();
                    }

                    if (session != null) {
                        setCurrentLicenseInfo(currentLicense);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving license information!", null, e);
        }

        return company;
    }

    /**
     * @return the site named in the license
     */
    public final String getCurrentLicenseSite() {
        HttpSession session = getSession();
        String site = null;

        // sometimes we can have a null session.  If this is the case then we'll
        // be retrieving the info from the DB and short-circuiting the session

        try {
            if (session != null) {
                site = (String) session.getAttribute(LICENSE_COMPANY_SITE);
            } else {
                log.warn("getCurrentLicenseSite called without an associated session");
            }

            siteCount++;

            if (site == null || session == null) {
                VoiceLicense currentLicense = getCurrentVoiceLicense();

                if (currentLicense != null) {
                    site = currentLicense.getLicenseeDisplaySite();
                    if (site == null) {
                        site = currentLicense.getLicenseeSite();
                    }
                    if (session != null) {
                        setCurrentLicenseInfo(currentLicense);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error retrieving license information!", null, e);
        }

        return site;
    }

    /**
     * @return the manager
     */
    public LicenseManager getSystemLicenseManager() {
        return this.licenseManager;
    }

    /**
     * @param setLicenseManager the manager
     */
    public void setSystemLicenseManager(LicenseManager setLicenseManager) {
        this.licenseManager = setLicenseManager;
    }

    /**
     * This constant is used for storing and retrieving the company name from
     * the session.  It is private because all interaction with this data should
     * occur through the appropriate setters on this object, not through direct
     * manipulation of the session.
     */
    private static final String LICENSE_COMPANY_NAME = "licenseCompanyName";

    /**
     * This constant is used for storing and retrieving the company site from
     * the session.  It is private because all interaction with this data should
     * occur through the appropriate setters on this object, not through direct
     * manipulation of the session.
     */
    private static final String LICENSE_COMPANY_SITE = "licenseCompanySite";

    /**
     * The category used to display license overdraft messages.
     * This category should display as a warning.
     */
    private static final String LICENSE_OVERDRAFT_CATEGORY = "licenseOverdraft";

    /**
     * The category used to display license violation messages.
     * This category should display as an error.
     */
    private static final String LICENSE_VIOLATION_CATEGORY = "licenseViolation";

    private static final String CHECK_FOR_LICENSE_VIOLATION_FLAG = "licenseMethodFlag";

    /**
     * Gets the current license stored in runtime memory. If there is no
     * license in memory, the database is checked for a license.
     * @return A license object if one is in memory or stored in the database,
     * or null if no license data could be found.
     * @throws BusinessRuleException
     * @throws DataAccessException
     * @throws LicenseAuthenticationException
     * @throws LicenseFormatException
     */
    private final VoiceLicense getCurrentVoiceLicense()
    throws BusinessRuleException, DataAccessException, LicenseAuthenticationException, LicenseFormatException {
        VoiceLicense currentLicense = VoiceLicense.getCurrentLicense();
        if (currentLicense == null) {
            SystemLicense systemLicense = getSystemLicenseManager().getCurrentLicense();
            if (systemLicense != null) {
                currentLicense = new VoiceLicense(systemLicense.getContents().getBytes(Charset.forName("utf-8")));
            }
        }
        return currentLicense;
    }

    /**
     * This method will examine the current license usage of the system and will
     * determine whether a session action message needs to be added to show the
     * user that they are in overdraft.
     *
     */
    protected final void checkForLicenseViolations() {
        HttpServletRequest request = getRequest();

        // sometimes there can be a null request (very odd)
        if (request == null) {
            return;
        }

        // check if this method has already been called
        if (request.getAttribute(CHECK_FOR_LICENSE_VIOLATION_FLAG) != null) {
            // don't do anything as this has already been called at least once in this request
            return;
        }

        try {
            violationCount++;

            Collection<LicenseStatus> licenseUsage =
                Licensers.getLicenseUsage(getCurrentVoiceLicense(), Licensers.OVERDRAFT | Licensers.VIOLATIONS);

            for (LicenseStatus licenseStatus : licenseUsage) {
                addSessionActionMessage(new UserMessage(
                        "licenseViolation.overdraft",
                        licenseStatus.getUnits(),
                        licenseStatus.getUnitsLeased(),
                        licenseStatus.getUnits() + licenseStatus.getUnitsOfOverdraft()
                    ));
            }

        } catch (BusinessRuleException bre) {
            log.error("Multiple licenses found when checking for license violation", null, bre);
        } catch (DataAccessException e) {
            log.error("Unable to retrieve license when checking for license violation", null, e);
        } catch (LicenseAuthenticationException e) {
            addMessageToSessionActionMessages(
                LICENSE_VIOLATION_CATEGORY,
                new UserMessage("licenseViolation.authentication")
            );
        } catch (LicenseFormatException e) {
            addMessageToSessionActionMessages(
                LICENSE_VIOLATION_CATEGORY,
                new UserMessage("licenseViolation.format")
            );
        }

        request.setAttribute(CHECK_FOR_LICENSE_VIOLATION_FLAG, "true");
    }

    /**
     * @param toBeatOrNot whether or not to output hearbeat message.
     */
    public final void heartbeat(boolean toBeatOrNot) {
        if (toBeatOrNot) {
            log.warn("Heartbeat check returned true");
        }
    }

    /**
     * @return number of companies named in licensees section.
     */
    public static int getCompanyCount() {
        return companyCount;
    }

    /**
     * @return number of sites named in licensees section.
     */
    public static int getSiteCount() {
        return siteCount;
    }

    /**
     * @return license violation count.
     */
    public static int getViolationCount() {
        return violationCount;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 