/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnFilterType;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.DataProviderRoot;
import com.vocollect.epp.service.FilterManager;
import com.vocollect.epp.service.RemovableDataProvider;
import com.vocollect.epp.service.TableViewDataService;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;
import com.vocollect.epp.util.FilterTimeWindow;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.util.DataProviderPrintUtil;
import com.vocollect.epp.web.util.DataProviderTableUtil;
import com.vocollect.epp.web.util.DataProviderUtil;
import com.vocollect.epp.web.util.DisplayUtilities;
import com.vocollect.epp.web.util.JSONResponseBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.aop.framework.Advised;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * This class provides an abstract implementation for classes that provide data
 * to the table component. It is responsible for serializing business objects
 * (provided by a subclass) into a JSON-formatted string.
 * @author dkertis
 */
public abstract class DataProviderActionRoot extends SiteEnabledAction
    implements ApplicationContextAware {

    private static final long serialVersionUID = -3035392422616748094L;

    private static final Logger log = new Logger(BaseAction.class);

    public static final String ERROR_PARTIAL_SUCCESS = "2";

    public static final String ERROR_REDIRECT = "3";

    public static final String ERROR_SUCCESS_MORE_INFO = "4";

    public static final String GENERAL_MESSAGE = "generalMessage";

    public static final String RETURNMAP = "returnMap";

    public static final String PARTIAL_MESSAGES = "partialMessages";

    public static final String SUCCESS_IDS = "ids";

    // constant for default getData method nane
    public static final String GET_METHOD = "getAll";

    public static final String BLANK_RESPONSE = "{\"rowOffset\":-16,"
        + "\"sliderVals\":[],\"objects\":[],\"timestamp\":\"9:00:00 PM\","
        + "\"selection\":[],\"count\":0,\"previousRowCount\":16,\"errorCode\":\"0\"}";

    // Used to get column preferences for rendering JSON messages.
    private UserPreferencesManager userPreferencesManager;

    private TableViewDataService tableViewDataService;

    private Map<String, HashSet<Long>> selectIds;

    // The ID's of selected objects
    // Used by performAction
    private Long[] ids;

    // Class for special display needs in table component.
    private DisplayUtilities displayUtilities;

    // parameter to query for autocomplete
    private String query = null;

    // field to query against for autocomplete
    private String filterField = null;

    /**
     * Getter for the tableViewDataService property.
     * @return TableViewDataService value of the property
     */
    public TableViewDataService getTableViewDataService() {
        return this.tableViewDataService;
    }

    /**
     * Setter for the tableViewDataService property.
     * @param tableViewDataService the new tableViewDataService value
     */
    public void setTableViewDataService(TableViewDataService tableViewDataService) {
        this.tableViewDataService = tableViewDataService;
    }

    /**
     * Getter for the displayUtilities property. This also populates the object,
     * if it exists, with the current user and a reference back to the Action.
     * @return the value of the property
     */
    public DisplayUtilities getDisplayUtilities() {
        if (this.displayUtilities != null) {
            this.displayUtilities.setCurrentUser(getCurrentUser());
            this.displayUtilities.setAction((DataProviderAction) this);
        }
        return this.displayUtilities;
    }

    /**
     * Setter for the displayUtilities property.
     * @param displayUtilities the new displayUtilities value
     */
    public void setDisplayUtilities(DisplayUtilities displayUtilities) {
        this.displayUtilities = displayUtilities;
    }

    /**
     * Each subclass must provide access to DataProvider implementation.
     * @return the data provider
     */
    protected abstract DataProvider getManager();

    /** *************************** Table component parameters. ************* */
    private String sortColumn;

    private boolean sortAsc;

    // Identifies the business object at the top of the requested data window.
    private Long rowId;

    // Specifies the count of rows in the requested data window.
    private int rowCount;

    private int startIndex;

    private int numToolTips;

    private int rowsPerPage;

    private int offset;

    private boolean refreshRequest;

    private String tooltipColumn;

    private boolean firstTimeRun;

    private Long viewId;

    /** *************************** End table component parameters. ************* */
    // Parameter passed to the custom result type for getTableData calls.
    private String message;

    // The JSON Messages to be returned after an asynchronous action call for
    // custom action calls
    private String jsonMessage;

    // The filters for the page - set via a query string or via
    // the filter interceptor - used by the table component ftl to pass info
    // to getData call
    private List<Filter> filters = new LinkedList<Filter>();

    // This is the actual filter submitted via the getData call to filter on
    private String submittedFilterCriterion;

    // The filter manager to use to create filter from submitted serialized
    // filtering criteria
    private FilterManager filterManager;

    // used to inject the spring context so the action can call getBean
    private ApplicationContext ctx = null;

    /**
     * Overridden default handler for table component. Take parameters used to
     * filter the data.
     * @param columnToFilter the name of a column to filter the results
     * @param columnFilterType the type of filter - database or inmemory
     * @param columnValues a list of values used in the where clause
     * @return JSON message to render the table component
     * @throws Exception throws when an unexpected exception happens
     */
    public String getTableData(String columnToFilter,
                               ColumnFilterType columnFilterType,
                               List columnValues) throws Exception {
        ResultDataInfo rdi = makeResultDataInfo(columnToFilter,
            columnFilterType, columnValues);
        return doGetTableRequest(new DataProviderTableUtil(), rdi);
    }

    /**
     * Overridden default handler for table component. Take parameters used to
     * append where clause.
     * @param whereClause the string for where clause
     * @return JSON message to render the table component
     * @throws Exception throws when an unexpected exception happens
     */
    public String getTableData(String whereClause) throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        rdi.setWhereClause(whereClause);
        return doGetTableRequest(new DataProviderTableUtil(), rdi);
    }

    /**
     * Overridden default handler for table component. Take parameters used to
     * append where clause.
     * @param whereClause the string for where clause
     * @param queryArgs the queryArgs for the rdi
     * @return JSON message to render the table component
     * @throws Exception throws when an unexpected exception happens
     */
    public String getTableData(String whereClause, Object[] queryArgs)
        throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        rdi.setWhereClause(whereClause);
        rdi.setQueryArgs(queryArgs);
        return doGetTableRequest(new DataProviderTableUtil(), rdi);
    }

    /**
     * Default handler for table component. Will get the data and format a
     * response.
     * @return JSON-formatted table data
     * @throws Exception if an error occurs during JSON serialization.
     */
    public String getTableData() throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        return doGetTableRequest(new DataProviderTableUtil(), rdi);
    }
    
    /**
     * Hook to override the handler for table component. Will get the data and format a
     * response.
     * @param util - The data provider handler for the request 
     * @return JSON-formatted table data
     * @throws Exception if an error occurs during JSON serialization.
     */
    public String getTableData(DataProviderUtil util) throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        return doGetTableRequest(util, rdi);
    }

    /**
     * This method is used to handle print requests from the table component.
     * @return the success code to Struts
     * @throws Exception handles unexpected situations
     */
    public String getPrintData() throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        return doGetTableRequest(new DataProviderPrintUtil(), rdi);
    }

    /**
     * This method is used to handle print requests from the table component.
     * @param whereClause - where clause to send for query
     * @return the success code to Struts
     * @throws Exception handles unexpected situations
     */
    public String getPrintData(String whereClause) throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        rdi.setWhereClause(whereClause);
        return doGetTableRequest(new DataProviderPrintUtil(), rdi);
    }

    /**
     * This method is used to handle print requests from the table component.
     * @param whereClause - where clause to pass to query.
     * @param queryArgs - parameters for named query
     * @return the success code to Struts
     * @throws Exception handles unexpected situations
     */
    public String getPrintData(String whereClause, Object[] queryArgs)
        throws Exception {
        ResultDataInfo rdi = makeResultDataInfo();
        rdi.setQueryArgs(queryArgs);
        rdi.setWhereClause(whereClause);
        return doGetTableRequest(new DataProviderPrintUtil(), rdi);
    }

    /**
     * Gets all saved selections.
     * @return Map of viewIds to selections
     */
    @SuppressWarnings("unchecked")
    public Map getUserSelection() {
        return (HashMap) getSession().getAttribute("USER_SELECTION");
    }

    /**
     * The actual implmentation of the getTableData. Used to commonize the
     * overridden functions
     * @param util the DataProviderUtil instance to use.
     * @param rdi Table component parameters
     * @return the JSON string to return
     */
    @SuppressWarnings("unchecked")
    private String doGetTableRequest(DataProviderUtil util, ResultDataInfo rdi) {
        try {
            // the view contains the bean and method to call
            View view = getUserPreferencesManager().getView(this.getViewId());
            List<Column> columns = getUsersColumns(view);
            HashMap<String, HashSet> userSelection = (HashMap<String, HashSet>) getUserSelection();
            if (userSelection != null) {
                HashSet<Long> selectedIds = userSelection.get(this.getViewId()
                    .toString());
                rdi.setSelectedIds(selectedIds);
            }
            // set the time window query arg if it is defined
            insertTimeWindow(rdi, view);
            // get the bean to call the method on
            Object providerBean = getProviderBean(view);
            // get the method to call
            String providerMethod = getProviderMethod(view);
            String returnMsg = getTableViewDataService().getTableData(util,
                rdi, providerBean, providerMethod, getCurrentUser(), columns,
                getCurrentTime(), (DataProviderAction) this);
            // set the variable to be returned to the client with the JSON
            // message
            setMessage(returnMsg);
        } catch (Exception e) {
            log.error("Unexpected failure retrieving table component data",
                SystemErrorCode.DATA_PROVIDER_FAILURE, e);
            setMessage(JSONResponseBuilder.makeErrorResponse(e.getMessage()));
        }
        return SUCCESS;
    }

    /**
     * @param rdi the RDI to modify, if necessary
     * @param view the current view
     * @throws DataAccessException on failure to retrieve time window prefs.
     */
    private void insertTimeWindow(ResultDataInfo rdi, View view)
        throws DataAccessException {
        Long timeWindow = getUserPreferencesManager().getTimeWindow(view,
            getCurrentUser());
        if (timeWindow != 0) {
            FilterTimeWindow filterTimeWindow = FilterTimeWindow
                .getById(timeWindow.intValue());
            Date dateOffset = filterTimeWindow.applyOffset(new Date());
            // add new query arg
            Object[] queryArgs;
            if (rdi.getQueryArgs() == null) {
                queryArgs = new Object[1];
                queryArgs[0] = dateOffset;
            } else {
                Object[] oldQueryArgs = rdi.getQueryArgs();
                queryArgs = new Object[oldQueryArgs.length + 1];
                System.arraycopy(oldQueryArgs, 0, queryArgs, 0,
                    oldQueryArgs.length);
                queryArgs[oldQueryArgs.length] = dateOffset;
            }
            rdi.setQueryArgs(queryArgs);
        }
    }

    /**
     * Get the bean that will provide the data.
     * @param view current view
     * @return the bean instance.
     * @throws Exception if unable to get the target source bean from an AOP
     *             proxy.
     */
    public Object getProviderBean(View view) throws Exception {
        Object providerBean = null;
        if (view.getDataBeanName() == null) {
            // by default, it is the DAO wired though the service
            providerBean = this.getManager().getProviderDAO();
        } else {
            // otherwise, look it up through spring
            Object bean = this.ctx.getBean(view.getDataBeanName());
            providerBean = ((Advised) bean).getTargetSource().getTarget();
        }
        return providerBean;
    }

    /**
     * Get the name of the method that will provide the data.
     * @param view the current view
     * @return the method name.
     */
    private String getProviderMethod(View view) {
        // figure out the correct method to call
        String getMethod = view.getDataMethodName();
        if (getMethod == null) {
            getMethod = GET_METHOD;
        }
        return getMethod;
    }

    /**
     * Creates a RDI Struct with default filter info.
     * @return The ResultDataInfo class for the request
     * @throws DataAccessException Error accessing filter data
     * @throws JSONException Error building the JSON response
     */
    protected ResultDataInfo makeResultDataInfo() throws DataAccessException,
        JSONException {
        List<Filter> filterList = getFilterManager()
            .constructFilterFromSerializedJSON(getSubmittedFilterCriterion());
        // When we are within the get data - we are always only in the contact
        // of
        // a single filter - therefore it can be zero or one
        Filter filter = null;
        if (filterList.size() > 0) {
            filter = filterList.get(0);
        }
        return createRDIStruct(filter);
    }

    /**
     * Creates a RDI Struct with a filter created from the parameters.
     * @param columnToFilter name of field to apply filter to
     * @param filterType the type of filter to apply
     * @param columnValues list of values to be filtered
     * @return The ResultDataInfo class for the request
     * @throws DataAccessException Error accessing filter data
     * @throws JSONException Error building the JSON response
     */
    protected ResultDataInfo makeResultDataInfo(String columnToFilter,
                                                ColumnFilterType filterType,
                                                List columnValues)
        throws DataAccessException, JSONException {
        List<FilterCriterion> fcs = new ArrayList<FilterCriterion>();
        Column c = new Column(); // get the column for Job id here
        c.setField(columnToFilter);
        c.setFilterType(filterType);
        Operand o = OperandSetup.getOperandById(Operand.NUM_EQUALS); // get
                                                                        // the =
                                                                        // operand
        for (Object value : columnValues) {
            FilterCriterion fc = new FilterCriterion(c, o, value.toString(),
                null, true);
            fcs.add(fc);
        }
        List<Filter> filterList = getFilterManager()
            .constructFilterFromSerializedJSON(getSubmittedFilterCriterion());
        // When we are within the get data - we are always only in the contact
        // of
        // a single filter - therefore it can be zero or one
        Filter filter = null;
        if (filterList.size() > 0) {
            filter = filterList.get(0);
            filterList.get(0).getFilterCriteria().addAll(fcs);
        } else {
            filter = new Filter();
            filter.setFilterCriteria(fcs);
        }
        return createRDIStruct(filter);
    }

    /**
     * Creates the RDI based on request parameters.
     * @param filter filter value to add the the RDI
     * @return The ResultDataInfo class for the request
     */
    private ResultDataInfo createRDIStruct(Filter filter) {
        // build a structure from the query string
        ResultDataInfo rdi = new ResultDataInfo(this.tooltipColumn,
            this.sortColumn, this.sortAsc, this.rowId, this.offset,
            this.rowCount, this.startIndex, this.numToolTips, this.rowsPerPage,
            this.refreshRequest, filter, this.getDisplayUtilities(),
            this.firstTimeRun);
        if (log.isTraceEnabled()) {
            log.trace(rdi);
        }
        return rdi;
    }

    /**
     * Delete the object identified by the <code>List[] ids</code>.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified object wasn't found (with different messages to the
     *         end object)
     * @throws DataAccessException on unanticipated
     */
    public String delete() throws DataAccessException {
        return performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_DELETEABLE;
            }

            public String getActionPrefix() {
                return "delete";
            }

            public boolean execute(DataObject obj) throws VocollectException,
                DataAccessException, BusinessRuleException {
                RemovableDataProvider rdm = (RemovableDataProvider) getManager();
                postProcessDelete(rdm.delete(obj.getId()));
                return true;
            }
        });
    }

    /**
     * @return - The prefix key for the properties file
     */
    protected abstract String getKeyPrefix();

    /**
     * Does post processing for deletion.
     * @param deleteResult The return value of the manager's delete function.
     * @throws VocollectException if this method had anything in it.
     */
    public void postProcessDelete(Object deleteResult)
        throws VocollectException {
    }

    /**
     * Helper getter for the jsonMessage.
     * @param generalMessage The text for the general message.
     * @param statusCode The error code.
     * @return JSONObject built from the parameters.
     */
    public JSONObject getJsonMessage(String generalMessage, String statusCode) {
        JSONObject json = new JSONObject();
        try {
            json.put(GENERAL_MESSAGE, generalMessage);
            json.put(JSONResponseBuilder.ERROR_CODE, statusCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        logUsage(generalMessage);
        return json;
    }
    
    /**
     * Helper setter for the jsonMessage property.
     * @param generalMessage The text for the general message.
     * @param statusCode The error code.
     */    
    public void setJsonMessage(String generalMessage, String statusCode) {
        JSONObject json = getJsonMessage(generalMessage, statusCode);
        setJsonMessage(json.toString());       
    }    

    /**
     * Helper getter for the jsonMessage.
     * @param generalMessage The text for the general message.
     * @param statusCode The error code.
     * @param returnMap A jsonobject to return to the client.
     * 
     * @return JSONObject built from the parameters.
     */
    public JSONObject getJsonMessage(String generalMessage,
                               String statusCode,
                               JSONObject returnMap) {
        JSONObject json = new JSONObject();
        try {
            json.put(GENERAL_MESSAGE, generalMessage);
            json.put(JSONResponseBuilder.ERROR_CODE, statusCode);
            json.put(RETURNMAP, returnMap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        logUsage(generalMessage);
        return json;
    }
    
    /**
     * Helper setter for the jsonMessage property.
     * @param generalMessage The text for the general message.
     * @param statusCode The error code.
     * @param returnMap A jsonobject to return to the client.
     */    
    public void setJsonMessage(String generalMessage,
                                   String statusCode,
                                   JSONObject returnMap) {
        JSONObject json = getJsonMessage(generalMessage, statusCode, returnMap);
        setJsonMessage(json.toString());        
    }        
    

    /**
     * Helper getter for the jsonMessage.
     * @param generalMessage the new GENERAL_MESSAGE
     * @param statusCode the new ERROR_CODE
     * @param successIds the new SUCCESS_IDS
     * 
     * @return JSONObject built from the parameters.
     */
    public JSONObject getJsonMessage(String generalMessage,
                               String statusCode,
                               JSONArray successIds) {
        JSONObject json = new JSONObject();
        try {
            json.put(SUCCESS_IDS, successIds);
            json.put(GENERAL_MESSAGE, generalMessage);
            json.put(JSONResponseBuilder.ERROR_CODE, statusCode);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        logUsage(generalMessage);
        return json;
    }
    
    /**
     * Helper getter for the jsonMessage.
     * @param generalMessage the new GENERAL_MESSAGE
     * @param statusCode the new ERROR_CODE
     * @param successIds the new SUCCESS_IDS
     */    
    public void setJsonMessage(String generalMessage,
                               String statusCode,
                               JSONArray successIds) {
        JSONObject json = getJsonMessage(generalMessage, statusCode, successIds);
        setJsonMessage(json.toString());       
    }       

    /**
     * Helper getter for the jsonMessage.
     * @param generalMessage the new GENERAL_MESSAGE
     * @param statusCode the new ERROR_CODE
     * @param jsonMessages the new PARTIAL_MESSAGES
     * @param successIds the new SUCCESS_IDS
     * 
     * @return JSONObject built from the parameters.
     */
    public JSONObject getJsonMessage(String generalMessage,
                               String statusCode,
                               JSONArray jsonMessages,
                               JSONArray successIds) {
        JSONObject json = new JSONObject();
        try {
            json.put(SUCCESS_IDS, successIds);
            json.put(GENERAL_MESSAGE, generalMessage);
            json.put(JSONResponseBuilder.ERROR_CODE, statusCode);
            json.put(PARTIAL_MESSAGES, jsonMessages);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        logUsage(generalMessage);
        return json;
    }
    
    /**
     * Helper setter for the jsonMessage property.
     * @param generalMessage the new GENERAL_MESSAGE
     * @param statusCode the new ERROR_CODE
     * @param jsonMessages the new PARTIAL_MESSAGES
     * @param successIds the new SUCCESS_IDS
     */    
    public void setJsonMessage(String generalMessage,
                               String statusCode,
                               JSONArray jsonMessages,
                               JSONArray successIds) {
        JSONObject json = getJsonMessage(generalMessage, statusCode, jsonMessages, successIds);
        setJsonMessage(json.toString());       
    }     

    /**
     * Log end-user error messages for usage tracking purposes.
     * @param msg the end-user message
     */
    private void logUsage(String msg) {
        User user = getCurrentUser();
        String id = "";
        if (user != null) {
            id = user.getId().toString();
        }
        if (log.isTraceEnabled()) {
            log.trace("UT (uid=" + id + "): " + msg);
        }
    }

    /**
     * @return the list of columns according to the user's preferences for the
     *         current view.
     * @param currentView the user's current view
     * @throws DataAccessException on database failure
     */
    private List<Column> getUsersColumns(View currentView)
        throws DataAccessException {
        
        return this.getUserPreferencesManager().getColumns(currentView, getCurrentUser());
    }

    /**
     * Perform a custom action on the objects identified by the
     * <code>List[] ids</code>.
     * @param customAction the action to execute.
     * @return SUCCESS control flow target if the custom action was successful
     *         or if the specified object wasn't found (with different messages
     *         to the end object)

     */
 
    public String performAction(CustomAction customAction) {        

        Map<Long, VocollectException> errors = new java.util.HashMap<Long, VocollectException>();
        Long[] theIds = getIds();
        
        List<DataObject> successObjects = new java.util.ArrayList<DataObject>();
        JSONArray successIds = new JSONArray(); 
        JSONArray jsonMessages = new JSONArray();
        DataObject obj = null;
        JSONObject result = null;
        int entityNotFoundCount = 0;
        
        for (int i = 0; i < theIds.length; i++) {            
            try {
                RemovableDataProvider rdm = (RemovableDataProvider) getManager();
                obj = rdm.getDataObject(theIds[i]);
                if (customAction.execute(obj)) {
                    successIds.put(theIds[i]); 
                    successObjects.add(obj);
                }
            } catch (EntityNotFoundException e) {
                entityNotFoundCount++;                
            } catch (VocollectException e) {
                    errors.put(theIds[i], e);
                    jsonMessages.put(obj.getDescriptiveText() + ": " + getText(e.getUserMessage()));                    
            } catch (Exception e) {
                log.warn("Problem executing customAction method.", e);
                result = buildEntityErrorMsg(customAction, successIds);
                setJsonMessage(result.toString());
                return SUCCESS;                
            }
        }
        
        if (theIds.length == 1) {
            result = buildSingleEntityMsg(customAction, obj, errors, successIds);
        } else {
            result = buildEntitySuccessMsg(customAction, theIds, successIds, jsonMessages, entityNotFoundCount, errors);
        }
        
        customAction.postAction(result, successObjects, errors);
        setJsonMessage(result.toString());
        
        return SUCCESS;     
    }
    

    /**
     * A helper message to build the JSONObject for a success.
     * 
     * @param customAction - The CustomAction that was executed.
     * @param theIds - A Long[] of the IDs processed.
     * @param successIds - A JSONArray of successful IDs.
     * @param jsonMessages - A JSONArray of the messages to be sent back to the client.
     * @param entityNotFoundCount - A count of EntityNotFoundExceptions that occured.
     * @param errors - A Map of any VocollectException that occured.
     * @return JSONObject - The resulting JSONObject.
     */
    private JSONObject buildEntitySuccessMsg(CustomAction customAction, 
                                             Long[] theIds,
                                             JSONArray successIds,
                                             JSONArray jsonMessages,
                                             int entityNotFoundCount,
                                             Map<Long, VocollectException> errors) {
        JSONObject result = null;
                
        if (successIds.length() == theIds.length) {
            result  = getJsonMessage(getText(new UserMessage(getKeyPrefix() + "."
                + customAction.getActionPrefix() + ".message.multiple.success",
                true, successIds.length())), JSONResponseBuilder.ERROR_SUCCESS,
                successIds);
        } else {
            if (entityNotFoundCount == 1) {
                jsonMessages.put(getText(new UserMessage(getKeyPrefix() + "."
                    + customAction.getActionPrefix()
                    + ".message.entityNotFound")));
            } else if (entityNotFoundCount > 1) {
                jsonMessages.put(getText(new UserMessage(getKeyPrefix() + "."
                    + customAction.getActionPrefix()
                    + ".message.entitiesNotFound", entityNotFoundCount)));
            }
            result  = getJsonMessage(getText(new UserMessage(getKeyPrefix() + "."
                + customAction.getActionPrefix() + ".message.partialSuccess",
                successIds.length(), theIds.length)), ERROR_PARTIAL_SUCCESS,
                jsonMessages, successIds);
        }
        
        return result;
    }
    
    
    /**
     * A helper message to build the JSONObject for a error message.
     *  
     * @param customAction - The CustomAction that was executed.
     * @param successIds - A JSONArray of successful IDs.
     * 
     * @return JSONObject - The resulting JSONObject.
     */
    
    private JSONObject buildEntityErrorMsg(CustomAction customAction, JSONArray successIds) {
        //unknown exception has occured
        String msgKey = UserMessage.markForLocalization("entity." + StringUtils.capitalize(getKeyPrefix()));
        UserMessage userMessage = new UserMessage(customAction.getSystemErrorCode(), false, msgKey);
        return getJsonMessage(getText(userMessage), JSONResponseBuilder.ERROR_FAILURE, successIds);         
    }
    
    /**
     * A helper message to build the JSONObject for a single entity success message.
     * 
     * @param customAction - The CustomAction that was executed.
     * @param object - The successful DataObject
     * @param successIds - A JSONArray of successful IDs.
     * @param errors - A Map of any VocollectException that occured.
     * 
     * @return JSONObject - The resulting JSONObject.
     */    
    private JSONObject buildSingleEntityMsg(CustomAction customAction, DataObject object, 
                                            Map<Long, VocollectException> errors, JSONArray successIds) {       
        JSONObject result = null;
        
        if (successIds.length() == 1 && errors.isEmpty()) {
            //single success
            result = 
                getJsonMessage(createSuccessMessage(customAction, object), 
                    JSONResponseBuilder.ERROR_SUCCESS, successIds);            
        }        
        if (successIds.length() == 0 && errors.isEmpty()) {
            //single failure (execute return false)
            String descriptiveText = null;
            if (object != null) {
                descriptiveText = object.getDescriptiveText();
            }
            result = getJsonMessage(getText(new UserMessage(getKeyPrefix() + "." + customAction.getActionPrefix() 
                + ".message.failure", true, descriptiveText)), JSONResponseBuilder.ERROR_FAILURE);
        }        
        if (successIds.length() == 0 && !errors.isEmpty()) {
            //single failure with an exception
            VocollectException e = errors.values().iterator().next();
           
            result = getJsonMessage(getResponseText(e, "site.error.unexpected.timestamp"),
                    JSONResponseBuilder.ERROR_FAILURE, successIds);                           
        }
            
       return result;
    }        
    
    /**
     * Returns the translated text from the included exception if not null.
     * Otherwise, returns the translated text from the an alternateMessageKey which takes a timestamp.
     * @param ve - The exception which may include a UserMessage.
     * @param alternateMessageKey - An alternate message key which takes a timestamp as a parameter.
     * @return String response text.
     */
    private String getResponseText(VocollectException ve, String alternateMessageKey) {
        String responseText;
        if (ve.getUserMessage() != null) {
            responseText = getText(ve.getUserMessage());
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = new Date();
            String formatteddate = dateFormat.format(date);
            String formattedtime = timeFormat.format(date);
            responseText = getText(new UserMessage(alternateMessageKey, formatteddate, formattedtime));
        }
        return responseText;
    }
    
    /**
     * @param query parameter used for autocomplete query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * @return the paramter to use for the autocomplete query
     */
    public String getQuery() {
        return this.query;
    }

    /**
     * @return field to query against for autocomplete
     */
    public String getFilterField() {
        return this.filterField;
    }

    /**
     * @param filterField - field to query against for autocomplete
     */
    public void setFilterField(String filterField) {
        this.filterField = filterField;
    }

    /**
     * Gets the autocomplete data.
     * @param manager - the manager to use to get the auto complete data
     * @return The JSON message for the auto-complete ajax query
     * @throws Exception An error during JSON building
     */
    protected String getFilterAutoCompleteData(DataProviderRoot manager)
        throws Exception {
        try {
            // let the DAO filter get the results
            Collection c = manager.getProviderDAO().getDataByFieldAndValue(
                this.filterField, this.query);
            setMessage(JSONResponseBuilder.buildFilterResponse(c));
        } catch (Exception e) {
            log.error("Unexpected failure retrieving table component data",
                SystemErrorCode.DATA_PROVIDER_FAILURE, e);
            setMessage(JSONResponseBuilder.makeErrorResponse(e.getMessage()));
        }
        return SUCCESS;
    }

    /**
     * Web interface method to get the auto complete info, Defaults to using the
     * default manager for the data.
     * @return THe JSON message for the auto-complete ajax query
     * @throws Exception An error during JSON building
     */
    public String getFilterAutoCompleteData() throws Exception {
        return getFilterAutoCompleteData(getManager());
    }

    /**
     * @param customAction the action being executed.
     * @param obj the data object being processed.
     * @return the localized error message.
     */
    protected String createSuccessMessage(CustomAction customAction,
                                          DataObject obj) {
        return getText(new UserMessage(getSuccessMessageKey(customAction),
            false, obj.getDescriptiveText()));
    }

    /**
     * The default resource message key for success messages of AJAX actions.
     * @param customAction the action being executed.
     * @return the key
     */
    protected String getSuccessMessageKey(CustomAction customAction) {
        return getKeyPrefix() + "." + customAction.getActionPrefix()
            + ".message.success";
    }

    /** ********************* Getters and setters *************************** */
    /**
     * Getter for the jsonMessage property.
     * @return String value of the property.
     */
    public String getJsonMessage() {
        return this.jsonMessage;
    }

    /**
     * Setter for the jsonMessage property.
     * @param jsonMessage the new jsonMessage value
     */
    public void setJsonMessage(String jsonMessage) {
        this.jsonMessage = jsonMessage;
    }

    /**
     * @return the message attribute
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * @param message the message to be set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return <code>true</code> implies data is sorted in ascending order,
     *         otherwise <code>.false</code>.
     */
    public boolean isSortAsc() {
        return this.sortAsc;
    }

    /**
     * @param asc <code>true</code> implies data is sorted in ascending order,
     *            otherwise <code>false</code>.
     */
    public void setSortAsc(boolean asc) {
        this.sortAsc = asc;
    }

    /**
     * @return the sorted column
     */
    public String getSortColumn() {
        return this.sortColumn;
    }

    /**
     * @param sortColumn identifies the sorted column
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * @return the id of the business object in the first row of the requested
     *         data.
     */
    public Long getFirstRowId() {
        return this.rowId;
    }

    /**
     * @param firstRowId identifies the business object in the first row of the
     *            requested data.
     */
    public void setFirstRowId(Long firstRowId) {
        this.rowId = firstRowId;
    }

    /**
     * @return the count of rows in the requested data.
     */
    public int getRowCount() {
        return this.rowCount;
    }

    /**
     * @param rowCount specifies the count of rows in the requested data.
     */
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    /**
     * @return the row id to start the data at
     */
    public long getRowId() {
        return this.rowId;
    }

    /**
     * @param rowId - the row id to start the data at
     */
    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    /**
     * Getter for the viewId property.
     * @return Long value of the property
     */
    public Long getViewId() {
        return this.viewId;
    }

    /**
     * Setter for the viewId property.
     * @param viewId the new viewId value
     */
    public void setViewId(Long viewId) {
        this.viewId = viewId;
    }

    /**
     * @return the index into the offset to center the data around
     */
    public int getStartIndex() {
        return this.startIndex;
    }

    /**
     * @param startIndex - the index into the offset to center the data around
     */
    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    /**
     * @return the index into the data to return
     */
    public int getOffset() {
        return this.offset;
    }

    /**
     * @param offset - the index into the data to return
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return the numToolTips property.
     */
    public int getNumToolTips() {
        return this.numToolTips;
    }

    /**
     * @param numToolTips the numToolTips property.
     */
    public void setNumToolTips(int numToolTips) {
        this.numToolTips = numToolTips;
    }

    /**
     * @return Rows per page
     */
    public int getRowsPerPage() {
        return this.rowsPerPage;
    }

    /**
     * @param rowsPerPage - Rows per page
     */
    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    /**
     * @return whether data is refreshing or scrolling
     */
    public boolean getRefreshRequest() {
        return this.refreshRequest;
    }

    /**
     * @param refreshRequest - whether data is refreshing or scrolling
     */
    public void setRefreshRequest(boolean refreshRequest) {
        this.refreshRequest = refreshRequest;
    }

    /**
     * @param firstTimeRun - whether selection data should reset data start
     */
    public void setFirstTimeRun(boolean firstTimeRun) {
        this.firstTimeRun = firstTimeRun;
    }

    /**
     * @return column to use for tooltip
     */
    public String getTooltipColumn() {
        return this.tooltipColumn;
    }

    /**
     * @param tooltipColumn - column to use for tooltip
     */
    public void setTooltipColumn(String tooltipColumn) {
        this.tooltipColumn = tooltipColumn;
    }

    /**
     * @return the ids sent from client
     */
    public Long[] getIds() {
        return this.ids;
    }

    /**
     * @param ids - ids sent from the client
     */
    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    /**
     * Parses JSON to create map of selected id's per view.
     * @param newSelectIds Array of serialized selections.
     */
    public void setSelectIds(String[] newSelectIds) {
        if (newSelectIds != null && newSelectIds.length > 0) {
            try {
                Map<String, HashSet<Long>> hm = new HashMap<String, HashSet<Long>>();
                for (int i = 0; i < newSelectIds.length; i++) {
                    JSONObject jo = new JSONObject(newSelectIds[i]);
                    String vId = jo.getString("viewId");
                    Long selId = jo.getLong("id");
                    HashSet<Long> hs = hm.get(vId);
                    if (hs == null) {
                        hs = new HashSet<Long>();
                        hs.add(selId);
                        hm.put(vId, hs);
                    } else {
                        hs.add(selId);
                    }
                }
                this.selectIds = hm;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @param newSelectIds the new Map of selections.
     */
    public void setSelectedIds(Map<String, HashSet<Long>> newSelectIds) {
        this.selectIds = newSelectIds;
    }

    /**
     * Gets the selected ids in a map.
     * @return Map of selected ids per view.
     */
    public Map<String, HashSet<Long>> getSelectedIds() {
        return this.selectIds;
    }

    /**
     * Getter for the userPreferencesManager property.
     * @return UserPreferencesManager value of the property
     */
    public UserPreferencesManager getUserPreferencesManager() {
        return this.userPreferencesManager;
    }

    /**
     * Setter for the userPreferencesManager property.
     * @param userPreferencesManager the new userPreferencesManager value
     */
    public void setUserPreferencesManager(UserPreferencesManager userPreferencesManager) {
        this.userPreferencesManager = userPreferencesManager;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext newCtx) {
        this.ctx = newCtx;
    }

    /**
     * Getter for the viewIds property. Make this abstract so each instance
     * needs to provide an implementation
     * @return List&lt;Long&gt; value of the property
     */
    public abstract List<Long> getViewIds();

    /**
     * Getter for the submittedFilterCriterion property.
     * @return String value of the property
     */
    public String getSubmittedFilterCriterion() {
        return this.submittedFilterCriterion;
    }

    /**
     * Setter for the submittedFilterCriterion property.
     * @param submittedFilterCriterion the new submittedFilterCriterion value
     */
    public void setSubmittedFilterCriterion(String submittedFilterCriterion) {
        try {
            this.submittedFilterCriterion = URLDecoder.decode(
                submittedFilterCriterion, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for the filters property.
     * @return List &lt;Filter&gt; value of the property
     */
    public List<Filter> getFilters() {
        return this.filters;
    }

    /**
     * Setter for the filters property.
     * @param filters the new filters value
     */
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    /**
     * Getter for the filterManager property.
     * @return FilterManager value of the property
     */
    public FilterManager getFilterManager() {
        return this.filterManager;
    }

    /**
     * Setter for the filterManager property.
     * @param filterManager the new filterManager value
     */
    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }

    /**
     * Returns the cached list of all operands.
     * @return List of all operands.
     */
    public List<Operand> getAllOperands() {
        return OperandSetup.getOperands();
    }
    
    /**
     * Java comment.
     * @return JSONObject - something
     * @throws DataAccessException - something
     * @throws JSONException - something
     */
    public JSONObject getJSONColumns() throws DataAccessException, JSONException {
        JSONObject allViews = new JSONObject();
        for (Long tableViewId : getViewIds()) {
            final View theView = getUserPreferencesManager().getView(tableViewId);
            List<Column> columns = this.getUserPreferencesManager().getColumns(
                    theView, getCurrentUser());
            JSONArray jsonArr = new JSONArray();
            JSONArray jsonVisibleArr = new JSONArray();
            for (Column column : columns) {
                JSONObject jsonCol = new JSONObject();
                jsonCol.put("name", getText(column.getDisplay()));
                jsonCol.put("field", column.getField());
                jsonCol.put("id", column.getId());
                jsonCol.put("width", column.getWidth());
                jsonCol.put("defaultWidth", column.getDefaultWidth());
                jsonCol.put("displayable", column.isDisplayable());
                jsonCol.put("visible", column.isVisible());
                jsonCol.put("operandType", column.getOperandType());
                jsonCol.put("filterType", column.getFilterType());
                jsonCol.put("painterFunction", column.getPainterFunction());
                jsonCol.put("sortable", !column.getSortType().equals(ColumnSortType.NoSort));
                jsonCol.put("sorted", column.isSorted());
                jsonCol.put("sortDirection", column.isSortAsc());
                jsonCol.put("behavior", "select");
                jsonCol.put("filterAutoCompleteDisable", column.getFilterAutoCompleteDisable());
                if (column.isDisplayable()) {
                    jsonArr.put(jsonCol);
                }
                if (column.isVisible()) {
                    jsonVisibleArr.put(jsonCol);
                }
            }
            
            JSONObject allAndVisibleColumns = new JSONObject();
            allAndVisibleColumns.put("all", jsonArr);
            allAndVisibleColumns.put("visible", jsonVisibleArr);
            allViews.put(tableViewId.toString(), allAndVisibleColumns);
        }
        
        return allViews;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 