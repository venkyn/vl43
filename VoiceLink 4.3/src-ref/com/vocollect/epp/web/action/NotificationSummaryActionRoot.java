/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.NotificationSummaryManager;

import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on Notification
 * Summary objects.
 *
 * @author Swathi Voruganti
 */
public class NotificationSummaryActionRoot extends DataProviderAction {

    // Serialization identifier.
    private static final long serialVersionUID = -3453400776168842667L;

    // Reference to the notification business service.
    private NotificationSummaryManager notificationSummaryManager;

    private List<Column> columns;

    /**
     * Getter for the notificationSummaryManager property.
     *
     * @return NotificationSummaryManager value of the property
     */
    public NotificationSummaryManager getNotificationSummaryManager() {
        return this.notificationSummaryManager;
    }

    /**
     * Setter for the NotificationSummaryManager property.
     *
     * @param notificationSummaryManager the injected manager.
     *
     */
    public void setNotificationSummaryManager(NotificationSummaryManager notificationSummaryManager) {
        this.notificationSummaryManager = notificationSummaryManager;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    public DataProvider getManager() {
        this.notificationSummaryManager.setCurrUser(getCurrentUser());
        NotificationSummaryManager ns = getNotificationSummaryManager();
        return ns;
    }

    /**
     * getter for the notification table columns.
     *
     * @return the columns list
     *
     */
    public List<Column> getNotificationColumns() {
        return this.columns;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "notificationSummary";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        return viewIds;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getTableData()
     */
    @Override
    public String getTableData() throws Exception {
        this.notificationSummaryManager.setCurrUser(getCurrentUser());
        return super.getTableData();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 