/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.errors.ErrorCode;


/**
 * @author dgold
 *
 */
public class DataProviderErrorCodeRoot extends ErrorCode {

    /**
     * Lower boundary of the EPP logging error range.
     */
    public static final int EPP_LOWER_BOUND  = 3000;

    /**
     * Upper boundary of the EPP logging error range.
     */
    public static final int EPP_UPPER_BOUND  = 3999;

    /** No error, just the base initialization. */
    public static final DataProviderErrorCodeRoot NO_ERROR = new DataProviderErrorCodeRoot();

    public static final DataProviderErrorCode UNABLE_TO_OBTAIN_DATA = new DataProviderErrorCode(3000);
    public static final DataProviderErrorCode UNABLE_TO_OBTAIN_DAO_DATA = new DataProviderErrorCode(3001);
    public static final DataProviderErrorCode UNABLE_TO_OBTAIN_DAO_COUNT = new DataProviderErrorCode(3002);
    public static final DataProviderErrorCode PUTTING_DATA_TO_JSON_OBJECT = new DataProviderErrorCode(3003);
    public static final DataProviderErrorCode PUTTING_ERROR_DATA_TO_JSON_OBJECT = new DataProviderErrorCode(3004);
    public static final DataProviderErrorCode PUTTING_ERROR_DATA_TO_JSON_OBJECT_2 = new DataProviderErrorCode(3005);
    public static final DataProviderErrorCode GETTING_DATA_FOR_JSON_OBJECT = new DataProviderErrorCode(3006);
    
     

    
    /**
     * Constructor.
     */
    private DataProviderErrorCodeRoot() {
        super("EPP", EPP_LOWER_BOUND, EPP_UPPER_BOUND);
    }

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected DataProviderErrorCodeRoot(long err) {
        super(DataProviderErrorCodeRoot.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 