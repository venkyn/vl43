/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.DataObject;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;

/**
 * This interface is used to define an action that will occur on every
 * object in a list of selected objects sent from a table component.
 * @author bnichols
 */
public interface CustomActionRoot {
    
    /**
     * Retrieves the error code to use during system failures.
     * @return the error code to use for this action
     */
    public SystemErrorCode getSystemErrorCode();
    
    /**
     * Performs necessary actions on the object passed in.  
     * @param obj to perform the action on
     * @throws VocollectException on error.
     * @throws DataAccessException on database problem.
     * @throws BusinessRuleException on business rule violation.
     * @return boolean representing success of the action
     */
    public boolean execute(DataObject obj) throws VocollectException, 
        DataAccessException, BusinessRuleException;
    
    /**
     * Retrieves text to be used to construct the appropriate message key for 
     * success and failure messages. (e.g. actionPrefix = "markout" translates
     * into short.markout.message.success)
     * <p>
     * getKeyPrefix returns the object type (e.g. short)
     * @return the action name string
     */
    public String getActionPrefix();
    
    /**
     * A callback function once the CustomAction is executed that allows the CustomAction to implement
     * behavior after the task is completed.
     * 
     * The postAction function has access to the JSONObject that will be sent as a response, a list of
     * DataObjects that were successfully processed, and a map of VocollectExceptions of any errors that
     * may have occurred.
     * 
     * The resulting JSONOBject can be used to append any extra info to be sent back in the result.
     * 
     * The map of VocollectExceptions informs the CustomAction what VocollectExceptions that may have
     * occurred.  The key is the id of failed object and the value is the VocollectException that occurred.
     * The processAction function will continue processing the objects if a VocollectException occurs.
     * 
     * If a non VocollectException occurs, the processAction function stops and sends back an error message.  The
     * postAction method will not be called in this case.
     * 
     * @param jsonResult - The resulting JSONObject
     * @param successObjects - A List of successfully processed DataObjects
     * @param vocollectErrors - A Map of VocollectExceptions.  The key is the attempted object id and value is the exception.
     */
    public void postAction(JSONObject jsonResult, List<DataObject> successObjects, 
                           Map<Long, VocollectException> vocollectErrors);
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 