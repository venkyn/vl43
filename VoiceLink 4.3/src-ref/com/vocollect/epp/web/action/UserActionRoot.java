/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.util.MD5;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.ldap.DefaultInitialDirContextFactory;
import org.acegisecurity.ldap.search.FilterBasedLdapUserSearch;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.commons.beanutils.BeanComparator;

/**
 * This is the Struts action class that handles operations on <code>User</code>
 * objects.
 *
 * @author ddoubleday
 */
public class UserActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = -5144060775708246834L;

    private static final long VIEW_ID = -1;

    private static final Integer ALL_SITE_ACCESS = 1;

    private static final Integer SELECTIVE_SITE_ACCESS = 0;


    private static final Logger log = new Logger(UserAction.class);

    private static final String PASSWORD_MASK = "********";

    // The User management service.
    private UserManager userManager = null;

    // The Role management service.
    private RoleManager roleManager = null;

    // The SystemProperty management service
    private SystemPropertyManager systemPropertyManager = null;

    //The list of all the sites
    private List<Site> allSites = null;

    // The user object, which will either be newly created, or retrieved
    // via the userId.
    private User user;

    // The ID of the user.
    private Long userId;

    // The password entered by the user.
    private String password = "";

    // The confirmation password entered by the user.
    private String confirmPassword = "";

    // Cached copy of the map of possible user statuses.
    private Map<Boolean, String> userStatusMap;

    // The list of columns for the user table
    private List<Column> columns;

    private boolean profileEdit = false;

    /*This will indicate if logged on user has access to
    all sites or not*/
    private Integer loggedOnUserSiteAccess;

    /*This will indicate if user being edited has access to
    all sites or not*/
    private Integer userSiteAccess;

    //This array of site ids send by user screen
    private long[] inputSiteIds;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    // Authentication against directory service
    private boolean authenticateAgainstDirectoryService;

    // Authentication against directory service available
    private boolean authenticateAgainstDirectoryServiceAvailable;

    // Hidden attribute to store authentication against directory service
    private String hiddenAuthenticateAgainstDirectoryService = null;

    // Hidden attribute to store password
    private String hiddenPassword = null;

    // Hidden attribute to store password verified
    private String hiddenPasswordVerified = null;

    /**
     * Hidden attribute to check whether the user is self editing.
     */
    private boolean isSelfEditing = false;


    /**
     * Getter for the confirmPassword property.
     * @return String value of the property
     */
    public String getConfirmPassword() {
        return this.confirmPassword;
    }

    /**
     * Setter for the confirmPassword property.
     * @param confirmPassword the new confirmPassword value
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Getter for the password property.
     * @return String value of the property
     */
    public String getPassword() {
        return this.password;

    }

    /**
     * Setter for the password property.
     * @param password the new password value
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for the roleManager property.
     * @return RoleManager value of the property
     */
    public RoleManager getRoleManager() {

        return this.roleManager;
    }

    /**
     * Setter for the roleManager property.
     * @param roleManager the new roleManager value
     */
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }


    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return this.systemPropertyManager;
    }


    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the user property.
     * @return User value of the property
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Setter for the user property.
     * @param user the new user value
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Getter for the userId property.
     * @return Long value of the property
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * Setter for the userId property.
     * @param userId the new userId value
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return this.userManager;
    }

    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getUserColumns() {
        return this.columns;
    }

    /**
     * Getter for the loggedOnUserSiteAccess property.
     * @return Integer value of the property
     */
    public Integer getLoggedOnUserSiteAccess() {
        return this.loggedOnUserSiteAccess;
    }

    /**
     * Setter for the loggedOnUserSiteAccess property.
     * @param loggedOnUserSiteAccess the new loggedOnUserSiteAccess value
     */
    public void setLoggedOnUserSiteAccess(Integer loggedOnUserSiteAccess) {
        this.loggedOnUserSiteAccess = loggedOnUserSiteAccess;
    }

    /**
     * Getter for the userSiteAccess property.
     * @return Integer value of the property
     */
    public Integer getUserSiteAccess() {
        return this.userSiteAccess;
    }

    /**
     * Setter for the userSiteAccess property.
     * @param userSiteAccess the new userSiteAccess value
     */
    public void setUserSiteAccess(Integer userSiteAccess) {
        this.userSiteAccess = userSiteAccess;
    }

    /**
     * Getter for the string version of the userSiteAccess property.
     * This is used on the ftl so the radio button gets the value
     * correctly.
     * @return Integer value of the property
     */
    public String getUserSiteAccessString() {
        return String.valueOf(this.userSiteAccess);
    }

    /**
     * Setter for the string version of the userSiteAccess property.
     * This is used on the ftl so the radio button sets the value
     * correctly.
     * @param userSiteAccessParam the new userSiteAccess value
     */
    public void setUserSiteAccessString(String userSiteAccessParam) {
        this.userSiteAccess = Integer.valueOf(userSiteAccessParam);
    }

    /**
     * Gets the list of all sites.
     * @return List of all the sites
     */
    public List<Site> getAllSites() {
        return this.allSites;
    }

    /**
     * Sets the list of all sites.
     * @param allSites List of all the sites
     */
    public void setAllSites(List<Site> allSites) {
        this.allSites = allSites;
    }


    /**
     * Action for the user view page. Initializes the user table columns
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View userView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            userView, getCurrentUser());

        this.restoreDefaultColumnsEnabled = this.getUserPreferencesManager().isRestoreDefaultColumnsEnabled(
            getCurrentUser(), userView);
        return SUCCESS;
    }

    /**
     * Gets an array of Role Ids, one for each Role that the User has.
     * <p>
     * TODO: this should be done with a TypeConverter once I figure out how to
     * do one using Spring IOC instead of WW IOC. -- dday.
     * @return Returns the roleIds, or null if the User field is null or has no
     *         Roles.
     */
    public long[] getRoleIds() {
        if (this.user != null && this.user.getRoles() != null) {
            long[] roleIds = new long[this.user.getRoles().size()];
            int i = 0;
            for (Role role : this.user.getRoles()) {
                roleIds[i++] = role.getId();
            }
            return roleIds;
        } else {
            return null;
        }
    }

    /**
     * Gets an array of Site Ids, one for each Site that the User has.
     * @return Returns the siteIds, or null if the User field is null or has no
     *         Sites.
     */
    public long[] getSiteIds() {
        if (this.user != null && this.user.getSites() != null) {
            long[] siteIds = new long[this.user.getSites().size()];
            int i = 0;
            for (Site site : this.user.getSites()) {
                siteIds[i++] = site.getId();
            }
            return siteIds;
        } else {
            return null;
        }
    }


    /**
     * Set the Roles for the user object. Each roleId in the array will be
     * converted to a <code>Role</code> by retrieving it from the database.
     * The <code>Role</code> is then added to the member <code>User</code>
     * field. If no <code>User</code> exists, a new one is created.
     * @param roleIds The role IDs to set.
     * @throws DataAccessException on failure to convert a specified ID to a
     *             <code>Role</code>
     */
    public void setRoleIds(long[] roleIds) throws DataAccessException {
        if (this.user != null) {
            if (log.isDebugEnabled()) {
                log.debug("Setting " + roleIds.length
                    + " roles in existing user");
            }
            this.user.setRoles(null);
            for (int i = 0; i < roleIds.length; i++) {
                this.user.addRole(getRoleManager().get(roleIds[i]));
            }
        }
    }

    /**
     * Set the Sites for the user object. Each siteId in the array will be
     * converted to a <code>Site</code> by retrieving it from the database.
     * The <code>Site</code> is then added to the member <code>User</code>
     * field. If no <code>User</code> exists, a new one is created.
     * @param siteIds The site IDs to set.
     * @throws DataAccessException on failure to convert a specified ID to a
     *             <code>Site</code>
     */

    public void setSiteIds(long[] siteIds) throws DataAccessException {

        if (this.user != null) {
                if (log.isDebugEnabled()) {
                    log.debug("Setting " + siteIds.length
                        + " sites in existing user");
                }
                //if (user.getSites() == null) {
                this.user.setSites(null);
                    for (int i = 0; i < siteIds.length; i++) {
                        this.user.addSite(getSiteManager().get(siteIds[i]));
                    }
                /*
                } else {
                    for (int i = 0; i < siteIds.length; i++) {
                            if (!isInList(siteIds[i], user.getSites())) {
                                this.user.addSite(getSiteManager().get(siteIds[i]));
                            }
                      }
                }
                */
                //populate inputSiteIds property
                inputSiteIds = siteIds;
        }
    }

    /**
     * Get the possible <code>User</code> statuses for display.
     * @return the Map of defined <code>User</code> status, where the map key
     *         is true/false, and the map value is the resource key for the
     *         status name.
     */
    public Map<Boolean, String> getStatusMap() {
        if (this.userStatusMap == null) {
            this.userStatusMap = new LinkedHashMap<Boolean, String>();
            this.userStatusMap.put(true, getText("user.status.true"));
            this.userStatusMap.put(false, getText("user.status.false"));
        }
        return this.userStatusMap;
    }

    /**
     * Get the radio buttons for user site access.
     * @return the Map for the user site access
     * @throws DataAccessException on database failure.
     */
    public Map<Integer, String> getSiteRadioMap() throws DataAccessException {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(0, getText("user.create.label.selectiveSiteAccess"));
        map.put(1, getText("user.create.label.allSiteAccess"));
        return map;
    }


    /**
     * Return the localized String representing the status of the modified user
     * in the "Stale Data" scenario.
     * @return the status of the modified user in stale object scenario, or
     *         <code>null</code> if there is no modified user object.
     */
    public String getModifiedStatus() {
        if (getModifiedEntity() != null) {
            return getStatusMap().get(((User) getModifiedEntity()).isEnabled());
        }
        return null;
    }

    /**
     * Get the <code>Role</code>s for display.
     * @return the Map of defined <code>Role</code>s, where the map value is
     *         the ID of the role, and the map key is the (localized, if
     *         necessary) role name. The name is used as the key so the data
     *         will be sorted by name.
     * @throws DataAccessException on failure to retrieve.
     */
    public Map<String, Long> getRoleMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(StringUtil
            .getPrimaryCollator());
        List<Role> list = getRoleManager().getAll();
        for (Role role : list) {
            map.put(role.getName(), role.getId());

        }
        return map;
    }

    /**
     * Get the <code>Site</code>s for display.
     * @return the Map of defined <code>Site</code>s, where the map value is
     *         the ID of the site, and the map key site name.
     *         The name is used as the key so the data
     *         will be sorted by name.
     * @throws DataAccessException on failure to retrieve.
     */
    public Map<String, Long> getSiteMap() throws DataAccessException {
        Map<String, Long> map = new TreeMap<String, Long>(StringUtil
            .getPrimaryCollator());
         for (Site site : allSites) {
            map.put(site.getName(), site.getId());
        }
        return map;
    }


    /**
     * Get the last login time and location for the user specified by the
     * <code>user</code> member of this class.
     * @return a String that combines the lastLoginTime and LastLoginLocation
     *         for the user, or null if the user has not been defined or has
     *         never logged in.
     */
    public String getLastLoggedIn() {
        if (this.user == null || this.user.getLastLoginTime() == null) {
            return getText("user.edit.label.lastLogin.never");
        }

        SiteContext siteContext = SiteContextHolder.getSiteContext();
        Site site = null;

        if ((siteContext != null) && (this.getCurrentSiteTagId() != null)) {
          try {
              site = siteContext.getSiteByTagId(this.getCurrentSiteTagId());

          } catch (DataAccessException dae) {
            log.warn("Unable to get Site: "
                     + getText(dae.getUserMessage()));
          }
        }

        String correctTime = "";
        if (site == null) {

            correctTime = DateUtil.formatTimestampForTableComponent(
                this.user.getLastLoginTime(), getRequest().getLocale());

        } else {
            correctTime = DateUtil.formatTimestampForTableComponent(
                this.user.getLastLoginTime(), site.getTimeZone(),
                getRequest().getLocale());

        }
        return correctTime + " " + getText("user.create.label.from") + " "
            + this.user.getLastLoginLocation();
    }

    /**
     * Method called when viewing a profile. Make sure the current user is
     * loaded before viewing the profile.
     * @return INPUT the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String viewSelf() throws Exception {
        // Setting this ourselves means that the user doesn't get
        // a chance to cheat and try to edit somebody else's profile.
        User self = getCurrentUser();
        setUserId(self.getId());
        setProfileEdit(true);
        this.maskPasswords();
        //Get the latest from the DB, not just when they logged in.
        prepare();
        return INPUT;
    }

    /**
     * Method called when editing a profile. Ensures that the profile being
     * edited is the current user's profile before allowing save.
     * @return the control flow target name. This will be
     *         <code>ERROR_ACCESS_RETURN</code> if the user is trying to edit
     *         somebody else's profile, or the result of save() in all other
     *         cases.
     * @throws Exception on any unanticipated failure.
     */
    public String saveSelf() throws Exception {
        isSelfEditing = true;

        if (!this.user.getId().equals(getCurrentUser().getId())) {
            log.warn("Access Denied: User " + getCurrentUser().getName()
                + " attempted to edit profile of user " + this.user.getName());
            return ERROR_ACCESS_RETURN;
        } else {

            return save();
        }
    }

    /**
     * Create or update the user specified by the <code>user</code> member of
     * this class.
     * @return the control flow target name.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {

        boolean isNew = this.user.isNew();
        this.processPassword();

        try {

            String retStr = checkAndRemoveUserSite();
            if (retStr.equals(INPUT)) {
                addFieldError("siteIds", new UserMessage(
                "user.create.error.noSiteIds", this.user.getName()));
                 // Go back to the form to show the error message.
                 return INPUT;
            }

            if (Boolean.valueOf(hiddenAuthenticateAgainstDirectoryService)) {
                // First - let's make sure that the user entered exists
                // in the directory service
                if (!isUserFoundInDirectoryServer(this.user.getName())) {
                    // The user was not found...therefore return an error
                    addFieldError("user.name",
                        getText("user.create.error.notfoundindirectoryservice"));
                    return INPUT;
                }

                // If we have authenticate against the directory service
                // then make sure we set the password to null
                this.user.setPassword(null);
            }

            userManager.save(this.user);
            cleanSession();

        } catch (DatabaseConstraintException e) {
            log.warn("A User named '" + this.user.getName()
                + "' already exists");
            addFieldError("user.name", new UserMessage(
                "user.create.error.existing", this.user.getName()));
            // Go back to the form to show the error message.
            return INPUT;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified user "
                + this.user.getName());
            addActionError(new UserMessage(
                "entity.error.modified", UserMessage
                    .markForLocalization("entity.User"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                User modifiedUser = getUserManager().get(getUserId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.user.setVersion(modifiedUser.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedUser);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage(
                    "entity.error.deleted", UserMessage
                        .markForLocalization(this.user.getName()), null));
                return SUCCESS;
            }
            return INPUT;
        } catch (BusinessRuleException e) {
            log.warn("Unable to save user: "
                + getText(e.getUserMessage()));
            addActionError(e.getUserMessage());
            return INPUT;
        }

        // add success message
        String successKey = "user." + (isNew ? "create" : "edit")
            + ".message.success";
        if (isSelfEditing) {
            addSessionActionMessage(new UserMessage(
                "user.edit.self.message.success"));
        } else {
            addSessionActionMessage(new UserMessage(
                successKey, makeContextURL("/admin/user/view.action?userId="
                    + this.user.getId()), this.user.getName()));
        }
        // Go to the success target.
        return SUCCESS;
    }

    /**
     * Delete the user identified by the <code>userId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified user wasn't found (with different messages to the
     *         end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentUser() throws Exception {

        User userToDelete = null;

        try {
            userToDelete = userManager.get(this.userId);
            userManager.delete(this.userId);

            addSessionActionMessage(new UserMessage(
                "user.delete.message.success", userToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete user: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * This method sets up the <code>User</code> object by retrieving it from
     * the database when a userId is set by the form submission. {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {

        setupSiteAccess();

        if (this.userId != null) {
            // We have an ID, but not a User object yet.
            if (log.isDebugEnabled()) {
                log.debug("userId is " + this.userId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())
                && getEntityFromSession(getSavedEntityKey()) != null) {
                // This means the User is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved user from session");
                }

                this.user = (User) getEntityFromSession(getSavedEntityKey());

                // If we are not editing via profile, we need to remove
                // all roles.
                if (!getProfileEdit()) {
                    // The roles must be cleared, in case all are removed
                    // and no call to setRoleIds is made.
                    this.user.setRoles(null);
                }
                this.user.setSites(null);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting user from database");
                }

                this.user = this.userManager.get(this.userId);

                if (this.user.getAllSitesAccess()) {
                    this.setUserSiteAccess(ALL_SITE_ACCESS);
                } else {
                    this.setUserSiteAccess(SELECTIVE_SITE_ACCESS);
                }
                this.maskPasswords();
                saveEntityInSession(this.user);
                getSession().setAttribute("userSites", this.user.getSites());
            }
            if (log.isDebugEnabled()) {
                log.debug("User version is: " + this.user.getVersion());
            }
       } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new user object");
            }

            if (this.user != null) {
                this.user = null;
            }
            this.user = new User();
        }
        setHiddenAuthenticateAgainstDirectoryService(
            Boolean.toString(isAuthenticateAgainstDirectoryService()));
    }

    /**
     * This does the initial setup for sites and siteAccessProperties for logged
     * on user and user being modified.
     * @throws DataAccessException on database failure.
     */
    private void setupSiteAccess() throws DataAccessException {

        if (this.getCurrentUser() != null) {
             if (this.getCurrentUser().getAllSitesAccess()) {
                 this.setLoggedOnUserSiteAccess(ALL_SITE_ACCESS);
                 this.setUserSiteAccess(ALL_SITE_ACCESS);
                 this.setAllSites(getSiteManager().getAll());
             } else {
                 this.setLoggedOnUserSiteAccess(SELECTIVE_SITE_ACCESS);
                 this.setUserSiteAccess(SELECTIVE_SITE_ACCESS);
                 this.setAllSites(setupSites(getCurrentUser().getSites()));
             }
         }
    }

    /**
     * This is helper method to get the list of sites from the
     * set of sites.
     * @param sites the set of sites.
     * @return the list of sites.
     */
    private List<Site> setupSites(Set<Site> sites) {
        return new ArrayList<Site>(sites);
    }

    /**
     * @param ids the list of site IDs
     * @param s the Site to test.
     * @return true if Site ID is in the array, false otherwise.
     */
    private boolean isInSiteList(long[] ids, Site s) {
        // no sites were checked, so the array is null
        if (ids == null) {
            return false;
        }

        // loop through the ids and find the site id
        for (int i = 0; i < ids.length; i++) {
            if (s.getId() == ids[i]) {
                return true;
            }
        }
        // no site ids were checked
        return false;
    }

    /**
     * User must have the association
     * with at least one site also we need to remove sites which
     * aren't checked on the screen.
     * Also we need to make sure we aren't deleting the sites from
     * the user which logged on user doesn't have access to
     * @return the action target to forward to.
     */
    public String checkAndRemoveUserSite() {
        // if the user selected all site access, set the property and return
        if (0 == this.getUserSiteAccess().compareTo(ALL_SITE_ACCESS)) {
            user.setAllSitesAccess(true);
            return SUCCESS;
        } else {
            // clear the property
            user.setAllSitesAccess(false);
        }

        // if a new user has no sites checked, fail
        if (inputSiteIds == null && this.user.isNew()) {
            return INPUT;
        }
        User u = null;
        Set<Site> userSites = new HashSet<Site>();
        if (!this.user.isNew()) {
            // if editing the user.  We need to know the sites the creating user originally had access to
//            userSites = (Set<Site>) getSession().getAttribute("userSites");
            try {
                u = getUserManager().get(this.user.getId());
                userSites = u.getSites();
            } catch (DataAccessException e) {
                this.addActionMessage("errorPage.errorInternal.unknownDatabaseError");
                log.error("Error accessing user's site during creation/editing",
                    SystemErrorCode.DATA_PROVIDER_FAILURE, e);
            }
        }
        // loop through all the sites the creating user has access to
        for (Site s : this.getAllSites()) {
            // find out if the site has been checked
            if (isSelfEditing || isInSiteList(inputSiteIds, s)) {
                // if the site was checked, add it to the user being edited site list
                userSites.add(s);
            } else {
                // if the site was checked, remove it from the user being edited site list
                userSites.remove(s);
            }
        }
        if (u != null) {
            getUserManager().getPrimaryDAO().detach(u);
        }

        // the user must have at least 1 site left
        if (userSites.size() == 0) {
            return INPUT;
        } else {
            user.setSites(userSites);
            return SUCCESS;
        }


    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getUserManager();
    }

    /**
     * Get the list of localized Role names associated with the modified
     * <code>User</code> (stale data scenario) as a comma-separated list.
     * @return the comma-separated String, or the empty string if there are no
     *         roles or the <code>modifiedEntity</code> field has not been
     *         set.
     */
    public String getModifiedUserRoleNamesAsString() {
        return getUserRoleNamesAsString((User) getModifiedEntity());
    }

    /**
     * Get the list of localized Role names associated with the
     * <code>User</code> currently being modified as a comma-separated list.
     * @return the comma-separated String, or the empty string if there are no
     *         roles or the <code>user</code> field has not been set.
     */
    public String getUserRoleNamesAsString() {
        return getUserRoleNamesAsString(getUser());
    }

    /**
     * Get the list of localized Role names associated with the specified
     * <code>User</code> currently being modified as a comma-separated list.
     * @param forUser the User to supply the <code>Role</code> names
     * @return the comma-separated String, or the empty string if there are no
     *         roles or <code>forUser</code> is null.
     */
    private String getUserRoleNamesAsString(User forUser) {
        final String sepString = ", ";

        if (forUser == null || forUser.getRoles() == null
            || forUser.getRoles().isEmpty()) {

            return "";
        } else {
            StringBuilder buf = new StringBuilder();
            for (Role role : forUser.getRoles()) {
                buf.append(role.getName());
                buf.append(sepString);
            }
            // Leave off the final unneeded comma and space when returning
            return buf.subSequence(0, buf.length() - sepString.length())
                .toString();
        }
    }

    /**
     * Get the list of localized Site names associated with the modified
     * <code>User</code> (stale data scenario) as a comma-separated list.
     * @return the comma-separated String, or the empty string if there are no
     *         sites or the <code>modifiedEntity</code> field has not been
     *         set.
     */
    public String getModifiedUserSiteNamesAsString() {
        return getUserSiteNamesAsString((User) getModifiedEntity());
    }

    /**
     * /**
     * Get the list of localized Site names associated with the
     * <code>User</code> currently being modified as a comma-separated list.
     * @return the comma-separated String, or the empty string if there are no
     *         sites or the <code>user</code> field has not been set.
     */
     public String getUserSiteNamesAsString() {
        return getUserSiteNamesAsString(getUser());
    }

     /**
      * Get the list of localized Site names associated with the specified
      * <code>User</code> currently being modified as a comma-separated list.
      * @param forUser the User to supply the <code>Role</code> names
      * @return the comma-separated String, or the empty string if there are no
      *         Sites or <code>forUser</code> is null.
      */
    @SuppressWarnings("unchecked")
    private String getUserSiteNamesAsString(User forUser) {
        final String sepString = ", ";

        if (forUser == null || forUser.getSites() == null
            || forUser.getSites().isEmpty()) {
            if (forUser.getAllSitesAccess()) {
                return getText("site.context.all");
            } else {
               return "";
            }
        } else {
            StringBuilder buf = new StringBuilder();
            List<Site> sites = new ArrayList<Site>(forUser.getSites());
            Collections.sort(sites, new BeanComparator("name"));
            for (Site site : sites) {
                buf.append(site.getName());
                buf.append(sepString);
            }
            // Leave off the final unneeded comma and space when returning
            return buf.subSequence(0, buf.length() - sepString.length())
                .toString();
        }
    }

    /**
     * Simply sets the action's password/confirmPassword properties as the
     * <code>PASSWORD_MASK</code>.
     */
    private void maskPasswords() {
        this.setPassword(PASSWORD_MASK);
        this.setConfirmPassword(PASSWORD_MASK);
    }

    /**
     * Method determines if password needs to be encoded and updated or left as
     * is. A password should be encoded and updated when the user is being
     * created or an existing user has changed their password.
     */
    private void processPassword() {
        if (!StringUtil.isNullOrEmpty(getPassword())) {
            if (!getPassword().equals(PASSWORD_MASK)) {
                this.user.setPassword(MD5.encode(getPassword()));
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     * @return String value of the keyprefix
     */
    @Override
    protected String getKeyPrefix() {
        return "user";
    }

    /**
     * Setter for profile edit flag.
     * @param profileEdit - boolean representing whether this is a profile edit.
     */
    public void setProfileEdit(boolean profileEdit) {
        this.profileEdit = profileEdit;
    }

    /**
     * Getter for profile edit flag.
     * @return boolean representing whether this is a profile edit.
     */
    public boolean getProfileEdit() {
        return this.profileEdit;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
        List<ListObject> list = new ArrayList<ListObject>();
        for (ListObject lo : super.getSiteList()) {
            list.add(lo);
        }
        return list;
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getUserViewId() {
        return VIEW_ID;
    }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }


    /**
     * Getter for the authenticateAgainstDirectoryService property.
     * @return boolean value of the property
     * @throws Exception - any exception should be thrown - we can't eat any
     */
    public boolean isAuthenticateAgainstDirectoryService() throws Exception {

        if (getUserId() == null) {
            // We are in a create case - therefore whether or not we are using
            // directory authentication is solely based off of the system
            // property

            if (hiddenAuthenticateAgainstDirectoryService != null) {
                setAuthenticateAgainstDirectoryService(
                    Boolean.parseBoolean(hiddenAuthenticateAgainstDirectoryService));
            } else {
                if (!isAuthenticateAgainstDirectoryServiceAvailable()) {
                    setAuthenticateAgainstDirectoryService(false);
                } else {
                    setAuthenticateAgainstDirectoryService(true);
                }
            }
        } else {
            if (isAuthenticateAgainstDirectoryServiceAvailable()) {
                // If we have directory authentication defined and we are in an edit case - then
                // we always need to provide the checkbox
                if (getUser().getPassword() == null || getUser().getPassword().length() == 0) {
                    // Since we have no password defined - we must have authenticated against
                    // the directory service - this is the only way this can be null
                    setAuthenticateAgainstDirectoryService(true);
                } else {
                    setAuthenticateAgainstDirectoryService(false);
                }
            }
        }

        return this.authenticateAgainstDirectoryService;
    }


    /**
     * Setter for the authenticateAgainstDirectoryService property.
     * @param authenticateAgainstDirectoryService the new authenticateAgainstDirectoryService value
     */
    public void setAuthenticateAgainstDirectoryService(boolean authenticateAgainstDirectoryService) {
        this.authenticateAgainstDirectoryService = authenticateAgainstDirectoryService;
    }


    /**
     * Getter for the hiddenAuthenticateAgainstDirectoryService property.
     * @return String value of the property
     */
    public String getHiddenAuthenticateAgainstDirectoryService() {
        return this.hiddenAuthenticateAgainstDirectoryService;
    }


    /**
     * Setter for the hiddenAuthenticateAgainstDirectoryService property.
     * @param hiddenAuthenticateAgainstDirectoryService the new hiddenAuthenticateAgainstDirectoryService value
     */
    public void setHiddenAuthenticateAgainstDirectoryService(String hiddenAuthenticateAgainstDirectoryService) {
        this.hiddenAuthenticateAgainstDirectoryService = hiddenAuthenticateAgainstDirectoryService;
    }


    /**
     * Getter for the hiddenPassword property.
     * @return String value of the property
     */
    public String getHiddenPassword() {
        return this.hiddenPassword;
    }


    /**
     * Setter for the hiddenPassword property.
     * @param hiddenPassword the new hiddenPassword value
     */
    public void setHiddenPassword(String hiddenPassword) {
        this.hiddenPassword = hiddenPassword;
    }


    /**
     * Getter for the hiddenPasswordVerified property.
     * @return String value of the property
     */
    public String getHiddenPasswordVerified() {
        return this.hiddenPasswordVerified;
    }


    /**
     * Setter for the hiddenPasswordVerified property.
     * @param hiddenPasswordVerified the new hiddenPasswordVerified value
     */
    public void setHiddenPasswordVerified(String hiddenPasswordVerified) {
        this.hiddenPasswordVerified = hiddenPasswordVerified;
    }


    /**
     * Getter for the authenticateAgainstDirectoryServiceAvailable property.
     * @return boolean value of the property
     * @throws DataAccessException on failure to get property
     */
    public boolean isAuthenticateAgainstDirectoryServiceAvailable() throws DataAccessException {
        // Query for the directory service authentication
        SystemProperty systemProperty = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_AUTHENTICATION);

        if (systemProperty == null || !Boolean.valueOf(systemProperty.getValue())) {
            setAuthenticateAgainstDirectoryServiceAvailable(false);
        } else {
            setAuthenticateAgainstDirectoryServiceAvailable(true);
        }

        return this.authenticateAgainstDirectoryServiceAvailable;
    }


    /**
     * Setter for the authenticateAgainstDirectoryServiceAvailable property.
     * @param authenticateAgainstDirectoryServiceAvailable the new authenticateAgainstDirectoryServiceAvailable value
     */
    public void setAuthenticateAgainstDirectoryServiceAvailable(boolean authenticateAgainstDirectoryServiceAvailable) {
        this.authenticateAgainstDirectoryServiceAvailable = authenticateAgainstDirectoryServiceAvailable;
    }

    /**
     * @param userName the name to look for in the directory
     * @return true if found, false otherwise.
     * @throws DataAccessException on failure to get properties
     */
    private boolean isUserFoundInDirectoryServer(String userName) throws DataAccessException {
        String host;
        String port;
        String searchBase;
        String managerDN;
        String managerPassword;
        String userAttribute;

        host = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_HOST).getValue();
        port = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_PORT).getValue();
        managerDN = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_SEARCH_USERNAME).getValue();
        managerPassword = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_SEARCH_PASSWORD).getValue();
        searchBase = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_SEARCH_BASE).getValue();
        userAttribute = getSystemPropertyManager().
            findByName(SystemConfigurationAction.DIRSERV_SEARCHABLE_ATTRIBUTE).getValue();

        DefaultInitialDirContextFactory context = new DefaultInitialDirContextFactory(
            "ldap://" + host + ":" + port);

        if (managerDN != null && managerDN.length() > 0) {
            // We can support anonymous bind - so only set the manager DN
            // and password if they are defined
            context.setManagerDn(managerDN);
            context.setManagerPassword(managerPassword);
        }

        Map<String, String> vars = new HashMap<String, String>();
        vars.put("java.naming.referral", "follow");
        vars.put("com.sun.jndi.ldap.connect.timeout", "5000");
        context.setExtraEnvVars(vars);

        FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch(
            searchBase, "(" + userAttribute + "={0})", context);
        userSearch.setSearchSubtree(true);

        try {
            userSearch.searchForUser(userName);
            return true;
        } catch (UsernameNotFoundException unfe) {
            log.info("No user " + userName + " found at LDAP server");
            return false;
        } catch (Exception e) {
            log.error("Error contacting LDAP Server: ",
                SystemErrorCode.CONFIGURATION_ERROR, e);
            return false;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 