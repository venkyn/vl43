/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.FeatureGroup;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.util.StringUtil;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * This is the Struts action class that handles operations on
 * <code>Role</code> objects.
 *
 * @author ddoubleday
 */
public class RoleActionRoot extends DataProviderAction implements Preparable {

    private static final long   serialVersionUID = 8236745755840140495L;

    private static final Logger log              = new Logger(RoleAction.class);

    private static final long VIEW_ID = -2;

    private RoleManager         roleManager;

    private Role                role;

    private Long                roleId;

    // The list of columns for the user table
    private List<Column> columns;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    /**
     * @return the columns to display.
     */
    public List<Column> getRoleColumns() {
        return this.columns;
    }

    /**
     * Action for the user view page. Initializes the user table columns.
     * @return SUCCESS
     * @throws Exception on any failure.
     */
    public String list() throws Exception {
        View roleView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
                                                roleView, getCurrentUser());
        this.restoreDefaultColumnsEnabled = this.getUserPreferencesManager().isRestoreDefaultColumnsEnabled(
            getCurrentUser(), roleView);
        return SUCCESS;
    }


    /**
     * Gets an array of Feature Ids, one for each Feature
     * that the Role supports.
     * <p>
     * TODO: this should be done with a TypeConverter once I figure out how to
     * do one using Spring IOC instead of WW IOC. -- dday.
     * @return Returns the roleIds, or null if the role field is null or
     * has no features.
     */
    public long[] getFeatures() {
        if (this.role != null && this.role.getFeatures() != null) {
            long[] featureIds = new long[this.role.getFeatures().size()];
            int i = 0;
            for (Feature feature : role.getFeatures()) {
                featureIds[i++] = feature.getId();
            }
            return featureIds;
        } else {
            return null;
        }
    }

    /**
     * Gets an array of Feature ids, one for each Feature that is
     * supported by the newly-modified Role. This method supports display
     * of current data in case of stale data exception.
     * @return a List of feature Ids, or null if the modifiedEntity field
     * is null or has no features.
     */
    public List<Long> getModifiedFeatures() {
        Role modifiedRole = (Role) getModifiedEntity();
        if (modifiedRole != null && modifiedRole.getFeatures() != null) {
            ArrayList<Long> modifiedFeatures = new ArrayList<Long>();
            for (Feature feature : modifiedRole.getFeatures()) {
                 modifiedFeatures.add(feature.getId());
            }
            return modifiedFeatures;
        } else {
            return null;
        }
    }

    /**
     * Set the Features for the role object. Each featureId in the array will
     * be converted to a <code>Feature</code> by retrieving it from the
     * database. The <code>Feature</code> is then added to the member
     * <code>Role</code> field. If no <code>Role</code> exists, a new one
     * is created.
     * @param featureIds The feature IDs to set.
     * @throws DataAccessException on failure to convert a specified
     * ID to a <code>Feature</code>
     */
    public void setFeatures(long[] featureIds) throws DataAccessException {
        if (role != null) {
            if (log.isDebugEnabled()) {
                log.debug("Setting " + featureIds.length
                    + " features in existing role");
            }
            role.setFeatures(null);
            for (int i = 0; i < featureIds.length; i++) {
                role.addFeature(getRoleManager().getFeature(featureIds[i]));
            }
        }
    }

    /**
     * Get the <code>FeatureGroup</code>s for display.
     * @return the List of defined <code>FeatureGroup</code>s
     * @throws DataAccessException on failure to retrieve.
     */
    public List<FeatureGroup> getFeatureGroups() throws DataAccessException {
        return getRoleManager().getFeatureGroups();
    }


    /**
     * Delete the role identified by the <code>roleId</code> member.
     * @return SUCCESS control flow target if the deletion was successful or
     * if the specified role wasn't found (with different messages to the
     * end user)
     * @throws Exception on unanticipated exception condition
     */
    public String deleteCurrentRole() throws Exception {

        Role roleToDelete = null;

        try {
            roleToDelete = getRoleManager().get(this.roleId);
            getRoleManager().delete(roleToDelete);

            // The cached map of Role/Feature relationships must change.
            getRoleFeatureMap().reinitialize();

            addSessionActionMessage(new UserMessage(
                "role.delete.message.success", roleToDelete.getName()));
            return SUCCESS;
        } catch (BusinessRuleException e2) {
            log.warn("Unable to delete role: "
                + getText(e2.getUserMessage()));
            addSessionActionErrorMessage(e2.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
    }

    /**
     * Re-initializes the RoleFeatureMap after role deletion.
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#postProcessDelete()
     */
    @Override
    public void postProcessDelete(Object deleteResult) throws DataAccessException {
        getRoleFeatureMap().reinitialize();
    }

    /**
     * Create or update the role specified by the <code>role</code> member of
     * this class.
     * @return the control flow target name (SUCCESS on success,
     * INPUT on constraint error.
     * @throws Exception on any unanticipated failure.
     */
    public String save() throws Exception {
        boolean isNew = getRole().isNew();

        try {
            getRoleManager().save(getRole());

            // The cached map of Role/Feature relationships must change.
            getRoleFeatureMap().reinitialize();

            // add success message
            String successKey = "role." + (isNew ? "create" : "edit")
                + ".message.success";
            addSessionActionMessage(new UserMessage(
                successKey,
                makeContextURL(
                    "/admin/role/view.action?roleId=" + getRole().getId()),
                getRole().getName()));
            // Go to the success target.
            return SUCCESS;

        } catch (DatabaseConstraintException e) {
            log.warn("Unable to save or update role because of database "
                + "constraint violation: " + e.getMessage());
            addFieldError("role.name", new UserMessage(
                "role.create.error.existing", getRole().getName()));
            // Go back to the form to show the error message.
            return INPUT;
        } catch (OptimisticLockingFailureException e) {
            log.warn("Attempt to change already modified role "
                + this.role.getName());
            addActionError(
                new UserMessage("entity.error.modified",
                                UserMessage.markForLocalization("entity.Role"),
                                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT)
                    );
            // Get the modified data
            // If role has been deleted, this will throw EntityNotFoundException
            try {
                Role modifiedRole = getRoleManager().get(getRoleId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.role.setVersion(modifiedRole.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedRole);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(
                    new UserMessage("entity.error.deleted",
                                    UserMessage.markForLocalization(getRole().getName()), null)
                        );
               return SUCCESS;
            }
            return INPUT;
        } catch (BusinessRuleException e2) {
            log.warn("Unable to save or update role: "
                + getText(e2.getUserMessage()));
            addActionError(e2.getUserMessage());
            return INPUT;
        }

    }

    /**
     * Getter for the role property.
     * @return Role value of the property
     */
    public Role getRole() {
        return this.role;
    }

    /**
     * Setter for the role property.
     * @param role the new role value
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Getter for the roleId property.
     * @return Long value of the property
     */
    public Long getRoleId() {
        return this.roleId;
    }

    /**
     * Setter for the roleId property.
     * @param roleId the new roleId value
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    /**
     * Getter for the roleManager property.
     * @return RoleManager value of the property
     */
    public RoleManager getRoleManager() {
        return this.roleManager;
    }

    /**
     * Setter for the roleManager property.
     * @param roleManager the new roleManager value
     */
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    /**
     * @return the site name of the current user's current site
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }

    /**
     * This method sets up the <code>Role</code> object by retrieving it
     * from the database when a roleId is set by the form submission.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {
        if (this.roleId != null) {
            if (log.isDebugEnabled()) {
                log.debug("roleId is " + this.roleId);
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey()) &&
                getEntityFromSession(getSavedEntityKey()) != null) {
                // This means the Role is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved role from session");
                }
                this.role =
                    (Role) getEntityFromSession(getSavedEntityKey());
                // The features must be cleared, in case all are removed
                // and no call to setFeatures is made.
                this.role.setFeatures(null);
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting role from database");
                }
                this.role = this.roleManager.get(this.roleId);
                saveEntityInSession(this.role);
            }
            if (log.isDebugEnabled()) {
                log.debug("Role version is: " + this.role.getVersion());
            }
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Created new role object");
            }
            if (this.role != null) {
                this.role = null;
            }
            this.role = new Role();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseDataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return getRoleManager();

    }

    /**
     * Refuse to allow viewing of administrator role page.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        if (getRole() != null && getRole().getIsAdministrative()) {
            return ERROR_ACCESS_RETURN;
        } else {
            return SUCCESS;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "role";
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getView_Id() {
        return VIEW_ID;
    }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 