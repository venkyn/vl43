/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ExternalJob;
import com.vocollect.epp.model.ExternalJobType;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.ExternalJobManager;
import com.vocollect.epp.service.JobManager;
import com.vocollect.epp.util.StringUtil;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_SUCCESS;

import com.opensymphony.xwork2.Preparable;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This is the Struts action class that handles operations on
 * <code>External Job</code> objects.
 * @author khazra
 */
public class ExternalJobActionRoot extends DataProviderAction implements
    Preparable {

    //
    private static final long serialVersionUID = 8492857062211185965L;

    private static final Logger log = new Logger(ExternalJobActionRoot.class);

    // The ID of the view we need to grab from the DB.
    private static final long VIEW_ID = -9;

    // The list of columns for the operators table.
    private List<Column> columns;

    private Long externalJobId;

    private ExternalJob externalJob;

    private Integer jobType;

    // The External job management service.
    private ExternalJobManager externalJobManager;

    // The Job manager service
    private JobManager jobManager;

    /**
     * Getter for the externalJobManager property.
     * @return UserManager value of the property
     */
    public ExternalJobManager getExternalJobManager() {
        return this.externalJobManager;
    }

    /**
     * Setter for the externalJobManager property.
     * @param manager the new externalJobManager value
     */
    public void setExternalJobManager(ExternalJobManager manager) {
        this.externalJobManager = manager;
    }

    /**
     * Getter for the columns property.
     * @return List of Column value of the property.
     */
    public List<Column> getExternalColumns() {
        return this.columns;
    }

    /**
     * Method to get the ID of EPP job matching jobName
     * @return return ID of the EPP job
     * @throws DataAccessException
     */
    public String getAdminJobId() throws DataAccessException {
        ExternalJob exJob = getExternalJobManager().get(getExternalJobId());
        Job job = getJobManager().updateAndFindJobByName(exJob.getName());
        setJsonMessage(getRequest().getContextPath()
            + "/admin/schedule/edit!input.action?jobId="
            + job.getId().toString(), ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Action for the items view page. Initializes the item table columns.
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View externalJobView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(
            externalJobView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * This method sets up the <code>Item</code> object by retrieving it from
     * the database when a itemId is set by the form submission. {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        if (getExternalJobId() != null) {
            // We have an ID, but not a external job object yet.
            if (log.isDebugEnabled()) {
                log.debug("externalJobId is " + this.getExternalJobId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the safety check is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved external job from session");
                }
                this.externalJob = (ExternalJob) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting external job from database");
                }
                this.externalJob = this.getExternalJobManager().get(
                    this.externalJobId);
                saveEntityInSession(this.externalJob);
            }
            if (log.isDebugEnabled()) {
                log.debug("External Job version is: "
                    + this.externalJob.getVersion());
            }
        } else if (this.externalJob == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new external job object");
            }
            this.externalJob = new ExternalJob();
            this.externalJob.setJobType(ExternalJobType.Executable);

        } else {
            this.externalJob.setJobType(ExternalJobType.Executable);
        }
    }

    /**
     * @return SUCCESS or INPUT
     * @throws Exception When save fails
     */
    public String save() throws Exception {
        boolean isNew = this.externalJob.isNew();

        try {
            externalJob.setCreatedDate(new Date());
            externalJob.setJobType(ExternalJobType.Executable);
            this.getExternalJobManager().save(externalJob);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified external job "
                + this.externalJob.getName());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.externalJob"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                ExternalJob modifiedExternalJob = getExternalJobManager().get(
                    getExternalJobId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.externalJob.setVersion(modifiedExternalJob.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedExternalJob);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.externalJob"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("externalJob."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/externalJob/view.action?externalJobId="
                + this.externalJob.getId()), this.externalJob.getName()));

        return SUCCESS;
    }

    /**
     * Returns a Map of job type names and ids
     * @return name and id map
     * @throws DataAccessException
     */
    public Map<Integer, String> getJobTypeMap() throws DataAccessException {
        Map<Integer, String> map = new TreeMap<Integer, String>(
            StringUtil.getPrimaryCollator());

        for (ExternalJobType jobTypeObj : ExternalJobType.values()) {
            map.put(jobTypeObj.getValue(), jobTypeObj.getDisplayValue());
        }

        return map;
    }

    /**
     * Delete the External Job identified by the <code>externalJobId</code>
     * member.
     * @return SUCCESS control flow target if the deletion was successful or if
     *         the specified external job wasn't found (with different messages
     *         to the end user)
     * @throws Exception on unanticipated error condition
     */
    public String deleteCurrentExternalJob() throws Exception {
        ExternalJob externalJobToDelete = null;

        try {
            externalJobToDelete = externalJobManager.get(this.externalJobId);
            externalJobManager.delete(this.externalJobId);

            addSessionActionMessage(new UserMessage(
                "externalJob.delete.message.success",
                externalJobToDelete.getName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete external job: "
                + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return this.getExternalJobManager();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "externalJob";
    }

    /**
     * Getter for the ITEM_VIEW_ID property.
     * @return long value of the property
     */
    public static long getExternalJobViewId() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }

    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * @return the site name of the current user's current site
     */
    @Override
    public String getSingleSiteName() throws DataAccessException {
        return getText("site.context.all");
    }

    /**
     * @return - the job type
     */
    public Integer getJobType() {
        return this.externalJob.getJobType().toValue();
    }

    /**
     * @param jobType - the job type
     */
    public void setJobType(Integer jobType) {
        this.externalJob.setJobType(ExternalJobType.Executable);
    }

    /**
     * @return - the external job
     */
    public ExternalJob getExternalJob() {
        return externalJob;
    }

    /**
     * @param externalJob - the external job
     */
    public void setExternalJob(ExternalJob externalJob) {
        this.externalJob = externalJob;
    }

    /**
     * @return - the external job Id
     */
    public Long getExternalJobId() {
        return externalJobId;
    }

    /**
     * @param externalJobId - the external job Id
     */
    public void setExternalJobId(Long externalJobId) {
        this.externalJobId = externalJobId;
    }

    /**
     * @return the jobManager
     */
    public JobManager getJobManager() {
        return jobManager;
    }

    /**
     * @param jobManager the jobManager to set
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 