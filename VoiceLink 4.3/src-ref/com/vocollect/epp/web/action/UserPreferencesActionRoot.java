/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;


import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.UserChartPreference;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.ColumnManager;
import com.vocollect.epp.service.FilterCriterionManager;
import com.vocollect.epp.service.FilterManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.ui.ColumnDTO;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;
import com.vocollect.epp.ui.UserPreferencesErrorCode;
import com.vocollect.epp.util.FilterTimeWindow;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.web.util.JSONResponseBuilder;

import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

/**
 * Actions used solely for saving the user's preferences from an asynchronous
 * call from the table component.
 * 
 * 
 * @author jgeisler
 */
public class UserPreferencesActionRoot extends BaseAction implements
    ResourceLoaderAware {

    //
    private static final long serialVersionUID = 819973017695292898L;

    private static final String CACHE_TC = "tablecomponent_loctext";

    private static final String TABLE_LOC_ELEMENT = "_loc_map";

    private static final String CACHE_OPERAND = "operands_cache";

    private static final String OPERAND_ELEMENT = "operand_objects";

    private static final Logger log = new Logger(UserPreferencesActionRoot.class);

    public static final String ERROR_CODE = "errorCode";

    public static final String ERROR_SUCCESS = "0";

    public static final String ERROR_FAILURE = "1";

    private int sortedColumn;

    private boolean sortOrder;

    private String[] columns;

    private String filterCriterion;

    private Long viewId;

    private Long columnId;

    private Long[] ids;

    private List<String> filterCriterionId;

    private UserPreferencesManager userPreferencesManager;

    private ColumnManager columnManager;

    private FilterManager filterManager;

    private FilterCriterionManager filterCriterionManager;

    private Integer operandType;

    private Long timeWindowValue;

    private String message = null; // the string used to return JSON messages

    private CacheManager cacheManager;

    private UserManager userManager;

    private DashboardManager dashboardManager;

    private ChartManager chartManager;
    
    private DashboardDetailManager dashboardDetailManager;

    // Chart user preference variables

    private Long chartId;

    private Long dashboardId;

    private Float chartWidth;

    private Float chartHeight;

    private Float chartX;

    private Float chartY;

    /**
     * Returns the columns for a view specific to a user.
     * @return The columns in JSON format.
     * @throws DataAccessException when database error occurs.
     * @throws JSONException
     */
    public String getColumnsByUserAndView() throws DataAccessException,
        JSONException {
        View view = getUserPreferencesManager().getView(this.getViewId());
        List<Column> cols = this.userPreferencesManager.getColumns(view,
            getCurrentUser());

        JSONArray json = columnsToJson(cols);
        setJsonMessage(json.toString());
        return SUCCESS;

    }

    /**
     * Converts a list of columns to a JSON object.
     * @param cols the list of columns to convert.
     * @return JSON object representing a list of columns.
     * @throws JSONException
     */
    private static JSONArray columnsToJson(List<Column> cols)
        throws JSONException {
        JSONObject result = new JSONObject();
        JSONArray jsonColumns = new JSONArray();
        for (Column column : cols) {
            JSONObject jsonColumn = columnToJson(column);
            jsonColumns.put(jsonColumn);
        }
        // result.put("columns", jsonColumns);
        return jsonColumns;
    }

    /**
     * Convenience function to convert a column to a JSON object.
     * @param column - the column to convert
     * @return JSON column object
     * @throws JSONException
     */
    private static JSONObject columnToJson(Column column) throws JSONException {
        JSONObject json = new JSONObject();
        /*
         * json.put("field", column.getField()); json.put("display",
         * ResourceUtil.getLocalizedKeyValue(column.getDisplay()));
         * json.put("operandType", column.getOperandType());
         */
        json.put("id", column.getId());
        json.put("title",
            ResourceUtil.getLocalizedKeyValue(column.getDisplay()));
        json.put("field", column.getField());

        return json;
    }

    /**
     * Gets the timeWindowValue property.
     * @return Value of the property.
     */
    public Long getTimeWindowValue() {
        return timeWindowValue;
    }

    /**
     * Sets the timeWindowValue property.
     * @param timeWindowValue New property value.
     */
    public void setTimeWindowValue(Long timeWindowValue) {
        this.timeWindowValue = timeWindowValue;
    }

    /**
     * Setter for the sortedColumn parameter.
     * @param sortedColumn int of the current sorted column's id.
     */
    public void setSortedColumn(int sortedColumn) {
        this.sortedColumn = sortedColumn;
    }

    /**
     * Getter for the sortedColumn parameter.
     * @return int value of sortedColumn parameter.
     */
    public int getSortedColumn() {
        return this.sortedColumn;
    }

    /**
     * Setter for the sortOrder parameter.
     * @param sortOrder boolean of the current sort direction
     */
    public void setSortOrder(boolean sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * Getter for the sortOrder parameter.
     * @return boolean value of the sortOrder parameter.
     */
    public boolean getSortOrder() {
        return this.sortOrder;
    }

    /**
     * Setter for the columns parameter.
     * @param columns String[] of serialized column information.
     */
    public void setColumns(String[] columns) {
        this.columns = columns;
    }

    /**
     * Getter for the columns parameter.
     * @return String[] of serialized column information.
     */
    public String[] getColumns() {
        return this.columns;
    }

    /**
     * Setter for the filters parameter.
     * @param filter String of serialized filter information
     */
    public void setFilterCriterion(String filter) {
        this.filterCriterion = filter;
    }

    /**
     * Getter for the filters parameter.
     * @return String of filter
     */
    public String getFilterCriterion() {
        return this.filterCriterion;
    }

    /**
     * Getter for the userPreferencesManager property.
     * @return UserPreferencesManager value of the property
     */
    public UserPreferencesManager getUserPreferencesManager() {
        return this.userPreferencesManager;
    }

    /**
     * Setter for the userPreferencesManager property.
     * @param userPreferencesManager the new userPreferencesManager
     */
    public void setUserPreferencesManager(UserPreferencesManager userPreferencesManager) {
        this.userPreferencesManager = userPreferencesManager;
    }
    
    
    /**
     * @return the dashboardDetailManager
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    
    /**
     * @param dashboardDetailManager the dashboardDetailManager to set
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }



    // The JSON Messages to be returned after an asynchronous action call
    private String jsonMessage;

    /**
     * Getter for the JSONMessage property.
     * @return String value of the property.
     */
    public String getJsonMessage() {
        return this.jsonMessage;
    }

    /**
     * Setter for the JSONMessage property.
     * @param jsonMessage the new JSONMessage value
     */
    public void setJsonMessage(String jsonMessage) {
        this.jsonMessage = jsonMessage;
    }

    /**
     * Overridden setter for the setJsonMessage function.
     * @param key Key value for the json object
     * @param value Value attributed to key for the json object
     */
    public void setJsonMessage(String key, String value) {
        JSONObject json = new JSONObject();
        try {
            json.put(key, value);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        setJsonMessage(json.toString());
    }

    /**
     * Overridden setter for the setJsonMessage function.
     * @param statusKey Key value for the json object representing the status
     * @param statusValue Value attributed to key for the json object
     *            representing the status
     * @param returnKey Key value for the json object representing the return
     *            var
     * @param returnValue Value attributed to key for the json object
     *            representing the return var
     */
    public void setJsonMessage(String statusKey,
                               String statusValue,
                               String returnKey,
                               String returnValue) {
        JSONObject json = new JSONObject();
        try {
            json.put(statusKey, statusValue);
            json.put(returnKey, returnValue);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        setJsonMessage(json.toString());
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {

        List<ColumnDTO> columnDTOs = new ArrayList<ColumnDTO>();

        for (String column : this.columns) {
            ColumnDTO colDTO;
            try {
                colDTO = new ColumnDTO(column);
            } catch (JSONException e) {
                log.error(
                    "A problem occurred when de-serializing column information.",
                    UserPreferencesErrorCode.ERROR_SAVING_COLUMN_INFO, e);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);
                return SUCCESS;
            }
            if (colDTO.getColumnId() == this.sortedColumn) {
                colDTO.setSorted(true);
                colDTO.setSortAsc(this.sortOrder);
            }
            columnDTOs.add(colDTO);
        }

        // Save the preferences through the UserPreferencesManager
        this.userPreferencesManager.savePreferences(columnDTOs,
            getCurrentUser());

        if (log.isTraceEnabled()) {
            // Log column preferences for usage tracking.
            String uid = this.getCurrentUser().getId().toString();
            for (ColumnDTO dto : columnDTOs) {
                log.trace("UT (uid=" + uid + "): " + dto.toString());
            }
        }

        setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Returns user to default column settings.
     * @throws Exception if there's a problem getting the column information
     * @return String success is always returned.
     */
    public String restoreDefaultColumns() throws Exception {
        // Restore the default preferences through the UserPreferencesManager
        this.userPreferencesManager.restoreDefaultColumns(getCurrentUser(),
            viewId);
        setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Stores the user's time window.
     * @return Struts success code.
     * @throws Exception on error.
     */
    public String saveTimeWindow() throws Exception {
        this.userPreferencesManager.saveTimeWindow(viewId, getCurrentUser(),
            timeWindowValue);
        setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Adds a new filter criteria for a particular view and user.
     * @return String success is always returned.
     * @throws DataAccessException DB issue.
     */
    public String addFilterCriteria() throws DataAccessException {
        String serializedFilter = getFilterCriterion();

        if (serializedFilter != null) {
            try {

                JSONObject jsonFC = new JSONObject(serializedFilter);
                Long clientViewId = jsonFC.getLong("viewId");
                Long critNumber = jsonFC.getLong("critNum");
                String value2 = "";
                if (jsonFC.has("value2")) {
                    value2 = jsonFC.getString("value2");
                }
                FilterCriterion userFC = new FilterCriterion(getColumnManager()
                    .get(jsonFC.getLong("columnId")),
                    OperandSetup.getOperandById(jsonFC.getLong("operandId")),
                    jsonFC.getString("value1"), value2, false);

                Long critId = getFilterManager().addFilterCriterion(userFC,
                    getCurrentUser(), clientViewId);
                JSONObject jo = new JSONObject();
                jo.put("critNum", critNumber);
                jo.put("critId", critId);
                setJsonMessage(ERROR_CODE, ERROR_SUCCESS, "critId",
                    critId.toString());
                if (log.isTraceEnabled()) {
                    // Log for usage tracking purposes.
                    log.trace("UT (uid="
                        + this.getCurrentUser().getId().toString() + "): "
                        + userFC.toString());
                }
            } catch (JSONException ex) {
                log.error(
                    "A problem occurred when de-serializing filter information.",
                    UserPreferencesErrorCode.ERROR_ADDING_FILTER_CRITERIA, ex);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);
            }
        } else {
            setJsonMessage(ERROR_CODE, ERROR_FAILURE);
        }

        return SUCCESS;
    }

    /**
     * Edits a filter criteria for a particular view and user.
     * @return String success is always returned.
     * @throws DataAccessException if the column or operand can't be found.
     * @throws BusinessRuleException if a business rule is incorrect with the
     *             crit save.
     */
    public String editFilterCriteria() throws DataAccessException,
        BusinessRuleException {
        String serializedFilter = getFilterCriterion();

        if (serializedFilter != null) {
            try {
                JSONObject jsonFC = new JSONObject(serializedFilter);
                Long clientViewId = jsonFC.getLong("viewId");
                Long criterionId = jsonFC.getLong("id");
                FilterCriterion userFC = new FilterCriterion(getColumnManager()
                    .get(jsonFC.getLong("columnId")),
                    OperandSetup.getOperandById(jsonFC.getLong("operandId")),
                    jsonFC.getString("value1"), jsonFC.getString("value2"),
                    false);

                getFilterManager().updateFilterCriterion(userFC, criterionId,
                    getCurrentUser(), clientViewId);

                setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
                if (log.isTraceEnabled()) {
                    // Log for usage tracking purposes.
                    log.trace("UT (uid="
                        + this.getCurrentUser().getId().toString() + "): "
                        + userFC.toString());
                }
            } catch (JSONException ex) {
                log.error(
                    "A problem occurred when de-serializing filter information.",
                    UserPreferencesErrorCode.ERROR_EDITING_FILTER_CRITERIA, ex);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);
            }

        } else {
            setJsonMessage(ERROR_CODE, ERROR_FAILURE);
        }

        setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
        return SUCCESS;
    }

    /**
     * Deletes a new filter criteria for a particular view and user.
     * @return String success is always returned.
     * @throws DataAccessException if it can't find the criterion with the id.
     * @throws BusinessRuleException if the criterionId is null.
     */
    public String deleteFilterCriteria() throws BusinessRuleException,
        DataAccessException {

        List<String> critIds = getFilterCriterionId();
        if (critIds != null) {
            for (String critId : critIds) {
                getFilterCriterionManager().delete(Long.parseLong(critId));
            }
            setJsonMessage(ERROR_CODE, ERROR_SUCCESS);
        } else {
            setJsonMessage(ERROR_CODE, ERROR_FAILURE);
        }
        return SUCCESS;
    }

    /**
     * Saves the user's selection per view in the session.
     * @return String
     */
    @SuppressWarnings("unchecked")
    public String saveUserSelection() {
        HashMap<String, HashSet> userSelections = (HashMap<String, HashSet>) getSession()
            .getAttribute("USER_SELECTION");

        if (userSelections == null) {
            userSelections = new HashMap<String, HashSet>();
        }

        Long[] selectionIds = getIds();
        String currentViewId = getViewId().toString();

        HashSet<Long> idHashSet = new HashSet<Long>();

        if (selectionIds != null) {
            for (int i = 0; i < selectionIds.length; i++) {
                idHashSet.add(selectionIds[i]);
            }
        }
        userSelections.put(currentViewId, idHashSet);

        getSession().setAttribute("USER_SELECTION", userSelections);

        setJsonMessage(JSONResponseBuilder.makeSuccessResponse());
        return SUCCESS;
    }

    /**
     * Setter for the viewId property.
     * @param viewId the new viewId value
     */
    public void setViewId(Long viewId) {
        this.viewId = viewId;
    }

    /**
     * Getter for the viewId property.
     * @return Long value of the property
     */
    public Long getViewId() {
        return viewId;
    }

    public CacheManager getCacheManager() {
        if (this.cacheManager == null) {
            CacheManager.create();
            this.cacheManager = CacheManager.getInstance();
        }
        return this.cacheManager;
    }

    private Cache findOrCreateCacheByName(String cacheName) {
        Cache cache = getCacheManager().getCache(cacheName);
        if (cache == null) {
            cache = new Cache(cacheName, 100, false, true, 0, 0);
            getCacheManager().addCache(cache);
        }
        return cache;
    }

    /**
     * Get table localization text.
     * 
     * @return table component keys localized into a key map.
     * @throws IOException
     * 
     */
    public String getTableLocText() throws DataAccessException, IOException {

        Cache cache = findOrCreateCacheByName(CACHE_TC);

        // Element name is created using the locale's display name. New cache
        // per country_language_variant.
        String elementName = getLocale().getDisplayName() + TABLE_LOC_ELEMENT;
        Element localization_table = cache.get(elementName);

        String resultString = "";

        if (localization_table == null) {

            ResourcePatternResolver resourcePatternResolver = ResourcePatternUtils
                .getResourcePatternResolver(resourceLoader);
            Resource[] resources = resourcePatternResolver
                .getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
                    + "/**/*-table.keylist");

            List<String> keys = new ArrayList<String>();
            for (Resource resource : resources) {

                InputStream in = resource.getInputStream();
                BufferedReader br = new BufferedReader(
                    new InputStreamReader(in));
                String strLine;
                // Read File Line By Line
                while ((strLine = br.readLine()) != null) {
                    keys.add(strLine);
                }

                in.close();
            }

            try {
                JSONObject keyMap = new JSONObject();
                for (String key : keys) {
                    keyMap.put(key, ResourceUtil.getLocalizedKeyValue(key));
                }
                resultString = keyMap.toString();
            } catch (JSONException je) {
                log.error("Error generating JSON for table component keys.",
                    UserPreferencesErrorCode.ERROR_CREATING_OPERAND_LIST);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);

                return SUCCESS;
            }

            // Create localization element
            localization_table = new Element(elementName, resultString);
            cache.put(localization_table);

        } else {
            resultString = (String) localization_table.getObjectValue();
        }

        setJsonMessage(ERROR_CODE, ERROR_SUCCESS, "tableKeys", resultString);

        return SUCCESS;
    }

    /**
     * Action method for retrieving operands.
     * @return String for SUCCESS
     * @throws DataAccessException .
     */
    public String getOperands() throws DataAccessException {

        Cache cache = findOrCreateCacheByName(CACHE_OPERAND);
        Element operand_array = cache.get(OPERAND_ELEMENT);

        String resultString = "";

        if (operand_array == null) {

            try {
                List<Operand> operands;
                if (operandType == null) {
                    operands = OperandSetup.getOperands();
                } else {
                    operands = OperandSetup.getOperandByType(operandType
                        .intValue());
                }
                JSONArray ja = new JSONArray();
                for (Operand operand : operands) {
                    JSONObject jsonOperand = new JSONObject();
                    jsonOperand.put("label", operand.getLabel());
                    jsonOperand.put("id", operand.getId());
                    jsonOperand.put("type", operand.getType());
                    jsonOperand.put("inputFunction",
                        operand.getJsInputFunction());
                    ja.put(jsonOperand);
                }
                resultString = ja.toString();
            } catch (JSONException je) {
                log.error("Error generating JSON for operand list.",
                    UserPreferencesErrorCode.ERROR_CREATING_OPERAND_LIST);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);

                return SUCCESS;
            }

            // Create localization
            operand_array = new Element(OPERAND_ELEMENT, resultString);
            cache.put(operand_array);

        } else {
            resultString = (String) operand_array.getObjectValue();
        }

        setJsonMessage(ERROR_CODE, ERROR_SUCCESS, "operands", resultString);

        return SUCCESS;
    }

    /**
     * Gets all enumerations for the view and column.
     * @return String for success to always return a Json Message.
     * @throws DataAccessException db issues.
     */
    @SuppressWarnings("unchecked")
    public String getEnumerationValues() throws DataAccessException {

        if (this.viewId != null && this.columnId != null) {
            try {
                JSONArray ja = new JSONArray();
                View view = getUserPreferencesManager().getView(this.viewId);
                Column column = getColumnManager().get(this.columnId);

                Class theClass = Class.forName(view.getClassName());
                // check to see if there's an enum-override set on the column
                // (probably not)
                Class enumClass;

                if ((column.getFilterEnumType() != null)
                    && (column.getFilterEnumType().length() > 0)) {
                    enumClass = Class.forName(column.getFilterEnumType());
                } else {
                    enumClass = getPropertyType(theClass, column.getField());
                }

                Set<ValueBasedEnum> sbe = EnumSet.allOf(enumClass);
                for (ValueBasedEnum e : sbe) {
                    JSONObject jo = new JSONObject();
                    jo.put("value", e.toValue());
                    jo.put("display", ResourceUtil.getLocalizedEnumName(e));
                    ja.put(jo);
                }
                setJsonMessage(ERROR_CODE, ERROR_SUCCESS, "enums",
                    ja.toString());
            } catch (ClassNotFoundException e) {
                log.error("Error determining enumeration values.",
                    UserPreferencesErrorCode.ERROR_GETTING_ENUMERATIONS);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);
            } catch (JSONException e) {
                log.error("Error building JSON for enumeration values.",
                    UserPreferencesErrorCode.ERROR_GETTING_ENUMERATIONS);
                setJsonMessage(ERROR_CODE, ERROR_FAILURE);
            }
        } else {
            setJsonMessage(ERROR_CODE, ERROR_FAILURE);
        }
        return SUCCESS;
    }

    /**
     * @param theClass the class
     * @param field field
     * @return Class object
     */
    private Class getPropertyType(Class theClass, String field) {
        PropertyDescriptor[] properties = PropertyUtils
            .getPropertyDescriptors(theClass);
        for (PropertyDescriptor pd : properties) {
            if (0 == pd.getName().compareTo(field)) {
                return pd.getPropertyType();
            }
        }
        return null;
    }

    /**
     * Gets all enumerated values for dates.
     * @return String for success.
     */
    public String getDateValues() {
        FilterTimeWindow[] ftm = FilterTimeWindow.values();
        try {
            JSONArray ja = new JSONArray();
            for (int i = 0; i < ftm.length; i++) {
                JSONObject jo = new JSONObject();
                jo.put("value", ftm[i].toValue());
                jo.put("display", getText(ftm[i].getResourceKey()));
                ja.put(jo);
            }
            setJsonMessage(ERROR_CODE, ERROR_SUCCESS, "enums", ja.toString());
        } catch (JSONException e) {

        }
        return SUCCESS;
    }

    /**
     * Receives the chart change event.
     * @return SUCCESS if it can save information in preference
     * @exception Exception if accessing any data
     */
    public String chartSizeChanged() throws Exception {
        if (this.dashboardId != null && this.chartId != null
            && this.chartWidth != null && this.chartHeight != null
            && this.chartY != null && this.chartX != null) {

            User currentUser = getCurrentUser();
            Dashboard dashboard = null;
            Chart chart = null;
            try {
                dashboard = this.getDashboardManager().get(dashboardId);
                chart = this.getChartManager().get(chartId);
                if (!dashboardDetailManager.checkDashboardAndChartExist(
                    dashboard, chart)) {
                    throw new EntityNotFoundException(DashboardDetail.class, null);
                }
            } catch (EntityNotFoundException enfe) {
                log.warn(
                    "Error while updating chart size. Dashboard / chart may have removed from system: "
                + enfe.getLocalizedMessage());
                
                setJsonMessage("message", getText(new UserMessage("dashboard.components.delete.error")),
                    "errorCode", ERROR_FAILURE);
                return ERROR;
            }
            
            JSONObject chartPreferencesJSON = new JSONObject();
            chartPreferencesJSON.put("chartWidth", chartWidth);
            chartPreferencesJSON.put("chartHeight", chartHeight);
            chartPreferencesJSON.put("chartY", chartY);
            chartPreferencesJSON.put("chartX", chartX);

            this.getUserManager().updateUserChartPreferences(
                currentUser.getId(), chart, dashboard, chartPreferencesJSON);

            setJsonMessage("message", SUCCESS, "errorCode", ERROR_SUCCESS);
        }
        return SUCCESS;
    }

    /**
     * Returns chart size for given chart id and dashboard id.
     * @return SUCCESS is it can retrieve chart size, or Exception
     * @throws Exception if problem retrieving chart size
     */
    public String getChartPreferences() throws Exception {
        JSONObject chartPreferenceJSON = new JSONObject();
        if (this.dashboardId != null && this.chartId != null) {
            User currentUser = getCurrentUser();
            User user = this.getUserManager().get(currentUser.getId());

            Dashboard dashboard = null;
            Chart chart = null;
            try {
                dashboard = this.getDashboardManager().get(dashboardId);
                chart = this.getChartManager().get(chartId);
                if (!dashboardDetailManager.checkDashboardAndChartExist(
                    dashboard, chart)) {
                    throw new EntityNotFoundException(DashboardDetail.class, null);
                }
            } catch (EntityNotFoundException enfe) {
                log.warn(
                    "Error while fetching chart position and size. Dashboard / chart may have removed from system: "
                + enfe.getLocalizedMessage());
                
                setJsonMessage("message", getText(new UserMessage("dashboard.components.delete.error")),
                    "errorCode", ERROR_FAILURE);
                return ERROR;
            }

            
            UserChartPreference chartPreference = this.getUserManager()
                .getChartPreferenceByUserChartAndDashboard(user, chart, dashboard);

            if (chartPreference != null) {
                chartPreferenceJSON.put("chartWidth", chartPreference.getChartWidth());
                chartPreferenceJSON.put("chartHeight", chartPreference.getChartHeight());
                chartPreferenceJSON.put("chartX", chartPreference.getChartX());
                chartPreferenceJSON.put("chartY", chartPreference.getChartY());
            }

        }

        setJsonMessage(chartPreferenceJSON.toString());

        return SUCCESS;
    }

    /**
     * Receives the chart change event.
     * @return SUCCESS if it can save information in preference
     * @exception Exception if accessing any data
     */
    public String chartPositionChanged() throws Exception {
        if (this.dashboardId != null && this.chartId != null
            && this.chartY != null && this.chartX != null) {

            User currentUser = getCurrentUser();
            Dashboard dashboard = null;
            Chart chart = null;
            try {
                dashboard = this.getDashboardManager().get(dashboardId);
                chart = this.getChartManager().get(chartId);
                if (!dashboardDetailManager.checkDashboardAndChartExist(
                    dashboard, chart)) {
                    throw new EntityNotFoundException(DashboardDetail.class, null);
                }
            } catch (EntityNotFoundException enfe) {
                log.warn(
                    "Error while updating chart position. Dashboard / chart may have removed from system: "
                + enfe.getLocalizedMessage());
                
                setJsonMessage("message", getText(new UserMessage("dashboard.components.delete.error")),
                    "errorCode", ERROR_FAILURE);
                return ERROR;
            }

            JSONObject chartPreferencesJSON = new JSONObject();
            chartPreferencesJSON.put("chartY", chartY);
            chartPreferencesJSON.put("chartX", chartX);

            this.getUserManager().updateUserChartPreferences(
                currentUser.getId(), chart, dashboard, chartPreferencesJSON);

            setJsonMessage("message", SUCCESS, "errorCode", ERROR_SUCCESS);
        }
        return SUCCESS;
    }

    /**
     * Getter for the operandType.
     * @return Integer the operandType.
     */
    public Integer getOperandType() {
        return operandType;
    }

    /**
     * Setter for the operandType.
     * @param operandType type of operand set by Struts.
     */
    public void setOperandType(Integer operandType) {
        this.operandType = operandType;
    }

    /**
     * Sets the ColumnManager.
     * @param columnManager New ColumnManager.
     */
    public void setColumnManager(ColumnManager columnManager) {
        this.columnManager = columnManager;
    }

    /**
     * Sets the FilterManager.
     * @param filterManager New FilterManager.
     */
    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }

    /**
     * Gets the ColumnManager.
     * @return ColumnManager the CM.
     */
    public ColumnManager getColumnManager() {
        return columnManager;
    }

    /**
     * Gets the FilterManager.
     * @return FilterManager the FM.
     */
    public FilterManager getFilterManager() {
        return filterManager;
    }

    /**
     * Gets the filterCriterionId.
     * @return Long of filterCriterionId
     */
    public List<String> getFilterCriterionId() {
        return filterCriterionId;
    }

    /**
     * Sets the filterCriterionId.
     * @param filterCriterionId Long of fcId.
     */
    public void setFilterCriterionId(List<String> filterCriterionId) {
        this.filterCriterionId = filterCriterionId;
    }

    /**
     * Gets the FilterCriterionManager.
     * @return FilterCriterionManager the FCM.
     */
    public FilterCriterionManager getFilterCriterionManager() {
        return filterCriterionManager;
    }

    /**
     * Sets the FilterCriterionManager.
     * @param filterCriterionManager the new FCM.
     */
    public void setFilterCriterionManager(FilterCriterionManager filterCriterionManager) {
        this.filterCriterionManager = filterCriterionManager;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Getter for the dashboardManager property.
     * @return DashboardManager value of the property
     */
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * Setter for the dashboardManager property.
     * @param dashboardManager the new dashboardManager value
     */
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * Getter for the chartManager property.
     * @return ChartManager value of the property
     */
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * Setter for the chartManager property.
     * @param chartManager the new chartManager value
     */
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * @return Column id parameter.
     */
    public Long getColumnId() {
        return columnId;
    }

    /**
     * @param columnId Param from request.
     */
    public void setColumnId(Long columnId) {
        this.columnId = columnId;
    }

    /**
     * @return Long Array of ids.
     */
    public Long[] getIds() {
        return ids;
    }

    /**
     * @param ids The ids to be set locally.
     */
    public void setIds(Long[] ids) {
        this.ids = ids;
    }

    /**
     * Getter for the message string.
     * @return message string
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter for the message string.
     * @param message - the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Getter for the chartId property.
     * @return Integer value of the property
     */
    public Long getChartId() {
        return chartId;
    }

    /**
     * Setter for the chartId property.
     * @param chartId the new chartId value
     */
    public void setChartId(Long chartId) {
        this.chartId = chartId;
    }

    /**
     * Getter for the dashboardId property.
     * @return Integer value of the property
     */
    public Long getDashboardId() {
        return dashboardId;
    }

    /**
     * Setter for the dashboardId property.
     * @param dashboardId the new dashboardId value
     */
    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    /**
     * Getter for the chartWidth property.
     * @return Float value of the property
     */
    public Float getChartWidth() {
        return chartWidth;
    }

    /**
     * Setter for the chartWidth property.
     * @param chartWidth the new chartWidth value
     */
    public void setChartWidth(Float chartWidth) {
        this.chartWidth = chartWidth;
    }

    /**
     * Getter for the chartHeight property.
     * @return Float value of the property
     */
    public Float getChartHeight() {
        return chartHeight;
    }

    /**
     * Setter for the chartHeight property.
     * @param chartHeight the new chartHeight value
     */
    public void setChartHeight(Float chartHeight) {
        this.chartHeight = chartHeight;
    }

    /**
     * @return the chartX
     */
    public Float getChartX() {
        return chartX;
    }

    /**
     * @param chartX the chartX to set
     */
    public void setChartX(Float chartX) {
        this.chartX = chartX;
    }

    /**
     * @return the chartY
     */
    public Float getChartY() {
        return chartY;
    }

    /**
     * @param chartY the chartY to set
     */
    public void setChartY(Float chartY) {
        this.chartY = chartY;
    }

    /**
     * Getter for the resourceLoader property.
     * @return ResourceLoader value of the property
     */
    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }

    private ResourceLoader resourceLoader;

    @Override
    public void setResourceLoader(ResourceLoader arg0) {
        this.resourceLoader = arg0;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 