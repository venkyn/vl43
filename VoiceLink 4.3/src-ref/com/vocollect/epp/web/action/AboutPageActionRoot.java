/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;


/**
 * This class will be responsible for gathering information to display on the
 * About page.
 *
 * @author jstonebrook
 */
public class AboutPageActionRoot extends SiteEnabledAction {

    private static final long serialVersionUID = -7970702787680321062L;

    private static final String APPLICATION = "application";
    private static final String APPLICATION_NAME = "name";
    private static final String APPLICATION_VERSION = "version";
    private static final String APPLICATION_BUILD_NUMBER = "buildnumber";
    
    private static final String THIRD_PARTY_COMPONENTS = "third_party_components";
    private static final String THIRD_PARTY_COMPONENT = "third_party_component";
    private static final String PACKAGE_NAME = "package";
    private static final String LICENSE = "license";
    private static final String DOWNLOAD = "source_code_availability";
    private static final String LICENSE_URI = "uri";
    private static final String LICENSE_NAME = "name";
    
    // a list of application details
    private List<List<String>> applicationList = new ArrayList<List<String>>();
    
    // a list of third party component details
    private List<List<String>> thirdPartyComponents = new ArrayList<List<String>>();
    
    /**
     * Override the default execute method.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        // Each App in the WAR will have a Vocollect license copyright file - EPP and who
        // ever else.  We need to union this information and present a nice single About
        // page view
 
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();     
        Resource[] resourceList = resolver.getResources("classpath*:com/vocollect/**/vocollect-license-copyright.xml");
        
        for (Resource resource : resourceList) {
            try {
                SAXBuilder builder = new SAXBuilder();
                Document document = builder.build(resource.getInputStream());

                Element root = document.getRootElement();
                Element application = root.getChild(APPLICATION);
                if (application != null) {
                    // We need to pull out the application name, version and build # for the 
                    // application list details
                    String name = application.getChild(APPLICATION_NAME).getText();
                    String version = application.getChild(APPLICATION_VERSION).getText();
                    String buildNumber = application.getChild(APPLICATION_BUILD_NUMBER).getText();
                    
                    ArrayList<String> applicationDataList = new ArrayList<String>();
                    applicationDataList.add(name);
                    applicationDataList.add(version);
                    applicationDataList.add(buildNumber);
                    getApplicationList().add(applicationDataList);
                }
                
                Element thirdPartyComponentsElem = root.getChild(THIRD_PARTY_COMPONENTS);
                if (thirdPartyComponentsElem != null) {
                    List<Element> thirdPartyComponentsList = thirdPartyComponentsElem
                        .getChildren(THIRD_PARTY_COMPONENT);
                    for (Iterator<Element> iter = thirdPartyComponentsList.iterator(); iter.hasNext();) {
                        // For each 3rd party component - we need to pull out the
                        // pachage name, license and download location
                        Element thirdPartyComponent = iter.next();
                        String packageName = thirdPartyComponent.getChild(PACKAGE_NAME).getText();
                        String licenseURI = thirdPartyComponent.getChild(LICENSE).getChild(LICENSE_URI).getText();
                        String licenseName = thirdPartyComponent.getChild(LICENSE).getChild(LICENSE_NAME).getText();
                        String download = thirdPartyComponent.getChild(DOWNLOAD).getText();
                        String licenseURL = 
                            "<a target=\"_blank\" href=\"" 
                            + getRequest().getScheme() 
                            + "://"
                            + getRequest().getServerName() 
                            + ":" 
                            + getRequest().getServerPort() 
                            + getRequest().getContextPath() 
                            + "/"
                            + getText("nav.help.urls.licensebase") 
                            + licenseURI 
                            + "\">" 
                            + licenseName 
                            + "</a>";
                            
                        ArrayList<String> thirdPartyComponentList = new ArrayList<String>();
                        thirdPartyComponentList.add(packageName);
                        thirdPartyComponentList.add(licenseURL);
                        thirdPartyComponentList.add(download);
                        
                        getThirdPartyComponents().add(thirdPartyComponentList);
                    }
                }
            } catch (Exception e) {
                // If we encounter an error - go ahead and log it but don't do anything for it
                // this is only an about page...no need to stop the app
                e.printStackTrace();
            }
           
        }
        
        return "success";
    }

    /**
     * This method will return the application list.
     * @return List of application details, which is a list
     */
    public List<List<String>> getApplicationList() {
        return applicationList;
    }

    /**
     * This method will set the application list.
     * @param applicationList a list contained application details
     */
    public void setApplicationList(List<List<String>> applicationList) {
        this.applicationList = applicationList;
    }

    /**
     * This method will return all 3rd party component details.
     * @return the component details.
     */
    public List<List<String>> getThirdPartyComponents() {
        return thirdPartyComponents;
    }

    /**
     * This method will set the 3rd party component details list.
     * @param thirdPartyComponents a list of 3rd party component details
     */
    public void setThirdPartyComponents(List<List<String>> thirdPartyComponents) {
        this.thirdPartyComponents = thirdPartyComponents;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }
    
    /**
     * @return the site name of the current user's current site when in single
     * site mode.
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 