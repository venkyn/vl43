/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.logging.LoggingError;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.FileView;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.LogManager;
import com.vocollect.epp.service.ZipFileException;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.SystemUtil;
import com.vocollect.epp.util.ZipUtil;

import com.opensymphony.xwork2.Validateable;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;


/**
 * Public interface for the log browsing utility for the EPP. This will allow
 * the user to see all of the logs, select one for viewing, or one or more for
 * zipped download.
 * @author dgold --------------------------------------------------------- Begin
 *         Who Comments Complete Jan 24, 2006 - DHG - initial revision
 * @author astein modified August 9, 2006 - added convertfilesize method and fixed setRequestedFile method
 */
public class LogFilesActionRoot extends DataProviderAction implements Validateable {

    /**
     * Logger to log messages to.
     */
    private static final Logger log = new Logger(LogFilesAction.class);

    private String fileTooLarge = java.lang.System.getProperty("errors.invalid");

    /*
     * Long to support serializable.
     */
    private static final long   serialVersionUID = 1L;

    private static final long VIEW_ID = -3;

    private static final int ONE_K = 1024;

    /**
     * The extension for the zip file returned.
     */
    private static final String EXT               = ".zip";

    private static final String ZIPBASE           = "Vocollect.";

    private static final String TIMESTAMPFORMAT   = "yyyyMMdd.HHmmss";

    /**
     * Used by viewLog to iterate the lines of the file.
     */
    private List<String>        fileContents;

    /**
     * LogManager implementation.
     */
    private LogManager          logManager       = null;

    /**
     * Used by the viewLog.ftl template to display page header information.
     */
    private FileView            requestedFile;

    /**
     * Passed as a query string parameter to viewLog.ftl to specify which file
     * to display.
     */
    private String              fileName;

    /**
     * Contains a list of fileNames selected to download as a zip.
     */
    private List<String>        selectedFiles;

    // The list of columns for the log table
    private List<Column> columns;

    // Should restore default columns be enabled
    private boolean restoreDefaultColumnsEnabled;

    /**
     * Gets the log directory.
     * @return the log directory for system logs
     */
    private String getLogDirectory() {
        return SystemUtil.getSystemLogDirectory();
    }

    /**
     * Override constructor to set ordered HashMap for errors. NOTE: Currently
     * doesn't work.
     */
    public LogFilesActionRoot() {
        this.setViewId(VIEW_ID);
        super.setFieldErrors(new LinkedHashMap());
    }

    /**
     * @return Returns the logManager.
     */
    public LogManager getLogManager() {
        return this.logManager;
    }

    /**
     * @return Returns the requestedFile.
     */
    public FileView getRequestedFile() {
        return requestedFile;
    }

    /**
     * @param reqFile - The requestedFile to set.
     */
    public void setRequestedFile(FileView reqFile) {
        this.requestedFile = reqFile;
    }


    /**
     * Make the file size more readable.
     *
     * @param size file size.
     * @return long of converted file size.
     */
    public long convertFileSize(long size) {
        int divisor = 1;

        // Select the file size decoration
        if (size >= ONE_K * ONE_K) {
            divisor = ONE_K * ONE_K;
        } else if (size >= ONE_K) {
            divisor = ONE_K;
        } else {
            return size;
        }
        return (size / divisor);
    }

    /**
     * @return Returns the fileContents.
     */
    public List<String> getFileContents() {
        return this.fileContents;
    }

    /**
     * @param fContents - The fileContents to set.
     */
    public void setFileContents(List<String> fContents) {
        this.fileContents = fContents;
    }

    /**
     * @return Returns the fileName.
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * @param file - The fileName to set.
     */
    public void setFileName(String file) {
        this.fileName = file;
    }

    /**
     * Gets the last line number read from the LogManager.
     * @return - integer value of the last file line read by the LogManager
     */
    public int getFileLineNum() {
        return (getLogManager().getFileLineNum());
    }

    /**
     * Create the zip file name.
     * @return - returns the zip file name
     */
    public String getZipFileName() {
        String destination = "";
        destination += ZIPBASE + DateUtil.getTimestamp(new Date(), TIMESTAMPFORMAT) + EXT;
        return destination;
    }


    /**
     * Empty set to satisfy bean requirements.
     */
    public void setFileLineNum() {
    }

    /**
     * @return Returns the selectedFiles.
     */
    public List<String> getSelectedFiles() {
        return this.selectedFiles;
    }

    /**
     * @param files The selectedFiles to set.
     */
    public void setSelectedFiles(List<String> files) {
        this.selectedFiles = files;
    }

    /**
     * Getter for the columns property.
     * @return List of columns
     */
    public List<Column> getLogColumns() {
        return this.columns;
    }

    /**
     * @return the site name of the current user's current site
     */
    @Override
    public String getSingleSiteName() {
        return getText("site.context.all");
    }

    /**
     * Method called when the user views all log file info. Loading of the data
     * is deferred to the table component
     * @return - SUCCESS
     * @throws Exception in case of a DataAccess exception
     */
    public String getLogFiles() throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("Getting log files");
        }

        View logView = getUserPreferencesManager().getView(VIEW_ID);
        this.columns = this.getUserPreferencesManager().getColumns(logView, getCurrentUser());
        this.restoreDefaultColumnsEnabled = this.getUserPreferencesManager().isRestoreDefaultColumnsEnabled(
            getCurrentUser(), logView);

        return SUCCESS;
    }

    /**
     * Method called when the user chooses to view a log file. Sets the
     * fileContents parameter with the information in the log file selected. The
     * log file chosen to download is in the fileName parameter. the File
     * contents is received from the file manager service.
     * @throws VocollectException -
     * @return - SUCCESS or throw exception
     */
    public String getLogFile() throws VocollectException {
        if (log.isTraceEnabled()) {
            log.trace("entering getLogFile");
        }
        String logdir = getLogDirectory();

        if (log.isDebugEnabled()) {
            log.debug("Selected " + getFileName() + " to view");
        }
        String requestedFileName = logdir + getFileName();

        // set the contents of the file for viewing on the page
        try {
            setRequestedFile(new FileView(new File(requestedFileName)));
            setFileContents(getLogManager().getFileLines(requestedFileName));
        } catch (VocollectException e) {
            log.error("Error Setting File Contents"
                + requestedFileName, LoggingError.FILE_IO_ERROR);
            addActionError(new UserMessage(
                LoggingError.FILE_NOT_FOUND, new Object[] { fileName }));
            return ERROR;
        }
        if (log.isTraceEnabled()) {
            log.trace("exiting getLogFile");
        }
        return SUCCESS;
    }

    /**
     * Get the file - currently being displayed - as a zip file.
     * @return - SUCCESS or throw ZipFileException
     * @throws ZipFileException - occurs when zip file has errors
     */
    public String downloadFile() throws ZipFileException {
        LinkedList<String> tempFileList = new LinkedList<String>();
        tempFileList.add(getFileName());
        setSelectedFiles(tempFileList);
        return (downloadFiles());
    }

    /**
     * Get all selected files from the log file directory, and then use the
     * ZipUtil class to zip the data.
     * @return - SUCCESS or ERROR
     * @throws ZipFileException - occurs when zip file has an error
     */
    public String downloadFiles() throws ZipFileException {

        // make sure the user selected some files for download
        if (getSelectedFiles() == null) {
            log.error(
                "No files selected for download",
                LoggingError.SELECTED_FILES_LIST_IS_NULL);
            addActionError(new UserMessage(
                "logging.error.noFiles"));
            return ERROR;
        }
        if (getSelectedFiles().size() == 0) {
            log.error(
                "No files selected for download",
                LoggingError.NO_FILES_SELECTED);
            addActionError(new UserMessage("logging.error.noFiles"));
            return ERROR;
        }
        if (log.isDebugEnabled()) {
            for (String aFile : getSelectedFiles()) {
                log.debug("Selected " + aFile);
            }
        }

        Map<String, InputStream> logMap = getLogFilesDataForZipping();
        HttpServletResponse resp = ServletActionContext.getResponse();
        resp.setContentType("application/zip");
        resp.setHeader("Content-Disposition", "attachment;filename=\""
            + getZipFileName() + "\"");
        resp.setHeader("Content-Encoding", "x-compress");
        try {
            ServletOutputStream out = resp.getOutputStream();
            ZipUtil.zipData(logMap, out);
        } catch (IOException e) {
            addSessionActionMessage(new UserMessage(
                "logging.export.error.unableToSave"));
            log.error("Unable to get the client output stream. " + e.getMessage(),
                LoggingError.UNABLE_TO_OBTAIN_CLIENT_OUTPUT_STREAM);
            throw new ZipFileException(LoggingError.UNABLE_TO_OBTAIN_CLIENT_OUTPUT_STREAM,
                  new UserMessage(LoggingError.UNABLE_TO_OBTAIN_CLIENT_OUTPUT_STREAM), e);
        } catch (ZipFileException e) {
            addSessionActionMessage(new UserMessage(
                "logging.export.error.unableToSave"));
            log.error(
                "Cannot zip data. " + e.getMessage(), e.getErrorCode());
            throw (e);
        }
        return NONE;
    }



    /**
     * Get all selected files from the log file directory, and popluate the
     * the map object with each log file content and file name.
     * @return - Map - input data for Zipping
     * @throws ZipFileException - wrapper for IOException
     */
    private Map<String , InputStream> getLogFilesDataForZipping() throws ZipFileException {

        String logdir = getLogDirectory();

        //Input for the ZipUtil - zipData method
        Map<String, InputStream> logMap = new HashMap<String, InputStream>();

        for (String aFile : getSelectedFiles()) {
           try {
                BufferedInputStream bis = new BufferedInputStream(new FileInputStream(logdir + aFile));
                logMap.put(aFile, bis);

           } catch (FileNotFoundException e) {
               log.error("Unable to find the file"
                   + aFile, LoggingError.FILE_NOT_FOUND);
               addSessionActionMessage(new UserMessage(
                   LoggingError.FILE_NOT_FOUND, new Object[] { aFile }));
               throw new ZipFileException(LoggingError.FILE_NOT_FOUND,
                   new UserMessage(LoggingError.FILE_NOT_FOUND), e);
           }
        }
        return logMap;
    }

    /**
     * Sets the log manager.
     * @param manager - the log manager implementation
     */
    public void setLogManager(LogManager manager) {
        this.logManager = manager;
    }

    /**
     * Getter for the LogManager.
     * @see com.vocollect.epp.web.action.DataProviderAction#getManager()
     * @return DataProvider the LogManager
     */
    @Override
    protected DataProvider getManager() {
        return getLogManager();
    }

    /**
     * @return Returns the mAX_FILE_SIZE.
     */
    public static long getMaxFileSize() {
        ResourceBundle rb = null;
        rb = ResourceBundle.getBundle("log");
        String viewableSizeLimit = rb.getString("system.log.viewableFileSize");

        return Long.valueOf(viewableSizeLimit).longValue();
    }

    /**
     * @return Returns the fileTooLarge.
     */
    public String getFileTooLarge() {
        fileTooLarge = getText("errors.logs.logTooLarge");
        return fileTooLarge;
    }

    /**
     * Get the current system log directory - for display to the UI.
     * @return (String) the system log directory
     */
    public String getSystemLogDir() {
        return SystemUtil.getSystemLogDirectory();
    }

    /**
     * @see com.vocollect.epp.web.action.DataProviderAction#getKeyPrefix()
     * @return String the keyprefix
     */
    @Override
    protected String getKeyPrefix() {
        return "logging";
    }

    /**
     * Getter for the VIEW_ID property.
     * @return long value of the property
     */
    public static long getView_Id() {
        return VIEW_ID;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     * Getter for the restoreDefaultColumnsEnabled property.
     * @return boolean value of the property
     */
    public boolean isRestoreDefaultColumnsEnabled() {
        return restoreDefaultColumnsEnabled;
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.DataProviderAction#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(VIEW_ID);
        return viewIds;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 