/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.User;

/**
 * This is Struts Action for handling the Open Close Navigation Menu
 * functionality.
 * 
 * @author ypore
 */
public class OpenCloseNavMenuAction extends BaseAction {

    // serialzation
    private static final long serialVersionUID = 8898510778713224287L;

    private String lastRequestedURL = "/default.action";

    private String sideNavHeight = "414";

    private boolean navMenuOpen = true;

    /**
     * Getter for the navMenuOpen property.
     * @return boolean value of the property
     */
    public boolean getNavMenuOpen() {
        return navMenuOpen;
    }

    /**
     * Setter for the navMenuOpen property.
     * @param navMenuOpen the new navMenuOpen value
     */
    public void setNavMenuOpen(boolean navMenuOpen) {
        this.navMenuOpen = navMenuOpen;
    }

    /**
     * Getter for the lastRequestedURL property.
     * @return String value of the property
     */
    public String getLastRequestedURL() {
        return lastRequestedURL;
    }

    /**
     * Setter for the lastRequestedURL property.
     * @param lastRequestedURL the new lastRequestedURL value
     */
    public void setLastRequestedURL(String lastRequestedURL) {
        this.lastRequestedURL = lastRequestedURL;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {
        // get current user in session
        User currentUser = getCurrentUser();
        // set the current user's parameters for collapsable nav menu
        if (currentUser != null) {
            currentUser.setLastRequestedURL(getLastRequestedURL());
            currentUser.setSideNavHeight(getSideNavHeight());
            currentUser.setNavMenuOpen(getNavMenuOpen());
            return SUCCESS;
        } else {
            return ERROR;
        }
    }

    /**
     * Getter for the sideNavHeight property.
     * @return String value of the property
     */
    public String getSideNavHeight() {
        return sideNavHeight;
    }

    /**
     * Setter for the sideNavHeight property.
     * @param sideNavHeight the new sideNavHeight value
     */
    public void setSideNavHeight(String sideNavHeight) {
        this.sideNavHeight = sideNavHeight;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 