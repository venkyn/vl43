/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.DataObject;

import java.util.List;
import java.util.Map;

import org.json.JSONObject;



/**
 * 
 *
 * @author clofty
 */
public abstract class CustomActionRootImpl implements CustomActionRoot {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.CustomActionRoot#postAction(org.json.JSONObject, java.util.List, java.util.Map)
     */
    public void postAction(JSONObject jsonResult, List<DataObject> successObjects, 
                           Map<Long, VocollectException> vocollectErrors) {
        
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 