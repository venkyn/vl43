/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.SystemPropertyDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.licensing.LicensedProduct;
import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemLicense;
import com.vocollect.epp.plugin.PluginModule;
import com.vocollect.epp.service.EncryptionManager;

import com.opensymphony.xwork2.Preparable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import org.springframework.web.util.WebUtils;


/**
 * Action class to
 * 1) Get the user supplied license key from the webpage
 *    as enctype="multipart/form-data".
 * 2) Create a valid License object.
 * 3) Store it in session for later use.
 *
 * @author pmukhopadhyay
 */
public class DocLicenseActionRoot extends SiteEnabledAction implements Preparable {

    /**
     *    Serialization identifier.
     */
    private static final long serialVersionUID = -1160667176644207865L;

    private static final String ERROR = "error";

    /*
     * The multipart license key file
     */
    private File doc;

    /*
     * The Content Type of license file
     */
    private String docContentType;

    /*
     * The filename of the license file
     */
    private String docFileName;

    private String docFileNameForSelenium;

    private SystemPropertyDAO systemPropertyDAO;
    private EncryptionManager encryptionManager;

    protected static final Logger log = new Logger(DocLicenseAction.class
            .getPackage().toString());

    /**
     * Makes the license imported by <code>execute()</code> available for
     * subclasses to use for their app-specific checks.
     */
    private VoiceLicense importedVoiceLicense;

    /**
     * It creates the License object from the license file
     * and stores the object in the  session.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {

        SystemLicense inputLicense = new SystemLicense();

        if (getDocFileNameForSelenium() != null
            && getDocFileNameForSelenium().length() > 0) {
            setDoc(new File(getDocFileNameForSelenium()));
            setDocFileName(getDocFileNameForSelenium());
        }
        String theDocFileName = getDocFileName();
        if (theDocFileName == null || theDocFileName.length() == 0) {
            addFieldError("doc", new UserMessage("licensing.fileInput.fileError"));
            return ERROR;
        }

        byte[] dataInByteArray = putDataInByteArray(getDoc());

        if (dataInByteArray == null) {
            addFieldError("doc", getText(new UserMessage("license.fileInput.empty.file", docFileName)));
            return ERROR;
        }
        inputLicense.setContents(dataInByteArray);
        inputLicense.setDateEntered(new Date());

        try {
            VoiceLicense voiceLicense = new VoiceLicense(inputLicense
                .getContents().getBytes(Charset.forName("utf-8")));
            if (voiceLicense.getExpirationDate() != null) {
                if (voiceLicense.getExpirationDate().getTime() <= inputLicense.getDateEntered().getTime()) {
                    // We are about to import an expired license - so let's prevent this.
                    addFieldError("doc", getText("license.import.expired"));
                    return ERROR;
                }
            }

            boolean islicensedForApp = isLicenseValidForApp(voiceLicense.getLicensedProducts());
            if (!islicensedForApp) {
                // We are about to import license which isnt for an installed app.
                addFieldError("doc", getText("license.invalidApp"));
                return ERROR;
            }

            WebUtils.setSessionAttribute(
                this.getRequest(), "licenseContent", inputLicense);
            setImportedVoiceLicense(voiceLicense);
            return SUCCESS;
        } catch (Exception e) {
            addFieldError("doc", getText("licensing.fileInput.exception"));
            return ERROR;
        }

    }

    /**
     * This method is used to verify is the license is valid for this product.
     * the licensed products should match one of the installed plugin module
     * names to be a valid license.
     *
     * @param licensedProducts - the licensed products
     * @return boolean  - if the access is true
     */
    public boolean isLicenseValidForApp(List<LicensedProduct> licensedProducts) {

        List<PluginModule> plugins = getAvailablePluginModules();
        for (PluginModule module : plugins) {
            String moduleName = getText(module.getName());

             log.info("Looking for plugin module " + moduleName + " for verifying"
             + " if llicense id valid");

            for (LicensedProduct licenseProduct : licensedProducts) {
                if (licenseProduct.getName().equalsIgnoreCase(moduleName)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Method to put file data in byte array.
     * @param newDoc the file
     * @return the bytes
     */
    public byte[] putDataInByteArray(File newDoc) {
        byte[] bytes;
        try {
            InputStream is = new FileInputStream(getDoc());
            long length = newDoc.length();
            bytes = new byte[(int) length];
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)
            {
                offset += numRead;
            }
            if (offset < bytes.length) {
                throw new IOException("Cannot read file ");
            }
            is.close();
            return bytes;
        } catch (Exception e) {
            log.debug("couldn't create byte array from XML file");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param doc the license file
     */
    public void setDoc(File doc) {
        this.doc = doc;
    }

    /**
     * @param docContentType the content type of the file
     */
    public void setDocContentType(String docContentType) {
        this.docContentType = docContentType;
    }

    /**
     * @param docFileName the file name
     */
    public void setDocFileName(String docFileName) {
        this.docFileName = docFileName;
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    public void prepare() throws Exception {

    }

    /**
     * @return the license file
     */
    public File getDoc() {
        return doc;
    }

    /**
     * @return the content type of the file
     */
    public String getDocContentType() {
        return docContentType;
    }

    /**
     * @return the name of the license file
     */
    public String getDocFileName() {
        return docFileName;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#getSingleSiteName()
     */
    @Override
    public String getSingleSiteName() throws DataAccessException {
        return getText("site.context.all");
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#isHomeAction()
     */
    @Override
    public boolean isHomeAction() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledActionRoot#isTextAction()
     */
    @Override
    public boolean isTextAction() {
        return true;
    }

    /**
     *
     * @return docFileNameForSelenium
     */
    public String getDocFileNameForSelenium() {
        return docFileNameForSelenium;
    }

    /**
     *
     * @param docFileNameForSelenium .
     */
    public void setDocFileNameForSelenium(String docFileNameForSelenium) {
        this.docFileNameForSelenium = docFileNameForSelenium;
    }


    /**
     * @param vl .
     */
    protected final void setImportedVoiceLicense(VoiceLicense vl) {
        this.importedVoiceLicense = vl;
    }

    /**
     * @return importedVoiceLicense
     */
    protected final VoiceLicense getImportedVoiceLicense() {
        return importedVoiceLicense;
    }


    /**
     * @return systemPropertyDAO
     */
    public SystemPropertyDAO getSystemPropertyDAO() {
        return systemPropertyDAO;
    }


    /**
     * @param systemPropertyDAO .
     */
    public void setSystemPropertyDAO(SystemPropertyDAO systemPropertyDAO) {
        this.systemPropertyDAO = systemPropertyDAO;
    }


    /**
     * @return encryptionManager
     */
    public EncryptionManager getEncryptionManager() {
        return encryptionManager;
    }


    /**
     * @param encryptionManager .
     */
    public void setEncryptionManager(EncryptionManager encryptionManager) {
        this.encryptionManager = encryptionManager;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 