/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.validators.impl;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 *
 *
 * @author hulrich
 */
public class StringHourFieldValidator extends FieldValidatorSupport {

    private static final int MERIDIAN_HOURS = 12;

    private static final int MAX_HOURS = 23;

    private String hourTypeField;

    private String twelveHourMessage;

    private String twentyFourHourMessage;

    private String errorGroupName;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    public void validate(Object actionObject) throws ValidationException {
        try {
            log.debug("Validating string hour -- " + this.getFieldName()
                + " --- " + this.getErrorGroupName());
            int hours = Integer.parseInt(this.getFieldValue(
                getFieldName(), actionObject).toString());

            String hourType = this.getFieldValue(
                getHourTypeField(), actionObject).toString();

            log.debug("Hours are ==> " + hours);
            log.debug("Hour type is ==> " + hourType);

            if ((hourType.equals(this.getAM()))
                || (hourType.equals(this.getPM()))) {
                this.setMessageKey(this.getTwelveHourMessage());
                if ((hours < 1) || (hours > MERIDIAN_HOURS)) {
                    addFieldError(this.getErrorGroupName(), actionObject);
                }
            } else {
                this.setMessageKey(this.getTwentyFourHourMessage());
                if ((hours < 0) || (hours > MAX_HOURS)) {
                    addFieldError(this.getErrorGroupName(), actionObject);
                }
            }

        } catch (NumberFormatException e) {
            log.debug("Unable to get " + actionObject.toString()
                + " into an int");
            addFieldError(this.getErrorGroupName(), actionObject);
        }
    }

    /**
     * Getter for the hourType property.
     * @return String value of the property
     */
    public String getHourTypeField() {
        return this.hourTypeField;
    }

    /**
     * Setter for the hourType property.
     * @param hourType the new hourType value
     */
    public void setHourTypeField(String hourType) {
        this.hourTypeField = hourType;
    }

    /**
     * Getter for the twelveHourMessage property.
     * @return String value of the property
     */
    public String getTwelveHourMessage() {
        return this.twelveHourMessage;
    }

    /**
     * Setter for the twelveHourMessage property.
     * @param twelveHourMessage the new twelveHourMessage value
     */
    public void setTwelveHourMessage(String twelveHourMessage) {
        this.twelveHourMessage = twelveHourMessage;
    }

    /**
     * Getter for the twentyFourHourMessage property.
     * @return String value of the property
     */
    public String getTwentyFourHourMessage() {
        return this.twentyFourHourMessage;
    }

    /**
     * Setter for the twentyFourHourMessage property.
     * @param twentyFourHourMessage the new twentyFourHourMessage value
     */
    public void setTwentyFourHourMessage(String twentyFourHourMessage) {
        this.twentyFourHourMessage = twentyFourHourMessage;
    }

    /**
     * Gets the hour type.
     * @return the hour type
     * @throws ValidationException if the hour type is not set in the properties
     */
    private String getAM() throws ValidationException {
        try {
            return this.getValidatorContext().getText("schedule.antemeridian");
        } catch (Exception e) {
            throw new ValidationException(
                "Unable to find key for schedule.antemeridian");
        }
    }

    /**
     * Gets the hour type.
     * @return the hour type
     * @throws ValidationException if the hour type is not set in the properties
     */
    private String getPM() throws ValidationException {
        try {
            return this.getValidatorContext().getText("schedule.postmeridian");
        } catch (Exception e) {
            throw new ValidationException(
                "Unable to find key for schedule.postmeridian");
        }
    }

    /**
     * Gets the hour type.
     * @return the hour type
     * @throws ValidationException if the hour type is not set in the properties
     */
    private String getTwentyFourHour() throws ValidationException {
        try {
            return this.getValidatorContext()
                .getText("schedule.twentyfourhour");
        } catch (Exception e) {
            throw new ValidationException(
                "Unable to find key for schedule.twentyfourhour");
        }
    }

    /**
     * Getter for the errorGroupName property.
     * @return String value of the property
     */
    public String getErrorGroupName() {
        return this.errorGroupName;
    }

    /**
     * Setter for the errorGroupName property.
     * @param errorGroupName the new errorGroupName value
     */
    public void setErrorGroupName(String errorGroupName) {
        this.errorGroupName = errorGroupName;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 