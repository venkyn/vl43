/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.validators.impl;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.IntRangeFieldValidator;

/**
 * An implementation of XWorks <code>FieldValidatorSupport</code> to be used
 * for fields that are presented as strings, but must be validated as numbers.
 * This situation arises when a field's underlying property is type <code>String
 * </code>,
 * yet the user must enter numerical values.
 * <p>
 * An example of this is time fields. The values must be presented as two
 * characters, however, the user is limited to entering digits with a min and a
 * max.
 * <p>
 * This validator does not allow text, the string must parse to an integer.
 *
 * @author hulrich
 */
public class StringNumberFieldValidator extends IntRangeFieldValidator {

    private String errorGroupName;

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.validator.Validator#validate(java.lang.Object)
     */
    @Override
    public void validate(Object actionObject) throws ValidationException {
        try {
            log.debug("Validating string number -- " + this.getFieldName()
                + " --- " + this.getErrorGroupName());

            Object fieldObject = this.getFieldValue(
                getFieldName(), actionObject);
            int val = Integer.parseInt(fieldObject.toString());
            if (val < getMin()) {
                addFieldError(this.getErrorGroupName(), actionObject);
            } else if (val > getMax()) {
                addFieldError(this.getErrorGroupName(), actionObject);
            }
        } catch (NumberFormatException e) {
            log.debug("Unable to get " + actionObject.toString()
                + " into an int");
            addFieldError(this.getErrorGroupName(), actionObject);
        }
    }

    /**
     * Getter for the errorGroupName property.
     * @return String value of the property
     */
    public String getErrorGroupName() {
        return this.errorGroupName;
    }

    /**
     * Setter for the errorGroupName property.
     * @param errorGroupName the new errorGroupName value
     */
    public void setErrorGroupName(String errorGroupName) {
        this.errorGroupName = errorGroupName;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 