/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web;

import java.io.File;

import org.mortbay.jetty.Server;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.webapp.WebAppContext;

/**
 * Driver program for embedded Jetty web server.
 *
 * @author cwinters
 * @author ddoubleday
 */
public final class StandAloneJetty {

    private static final int DEFAULT_SERVER_PORT = 80;

    /**
     * Private Constructor.
     */
    private StandAloneJetty() {
    }
    
    /**
     * Main driver.
     * @param args command line args
     * @throws Exception on any failure.
     */
    public static void main(final String... args) throws Exception {
        if (args.length < 1) {
            System.out.println("Usage: java ... com.vocollect.epp.web.StandAloneJetty dir [port]");
            System.exit(1);
        }
        final File appDir = new File(args[0]);
        final String appPath = appDir.getAbsolutePath();
        if (!appDir.isDirectory()) {
            System.out.println("ERROR: [" + appPath + "] is not a valid directory");
            System.exit(1);
        }
        final int port = args.length >= 2 ? Integer.parseInt(args[1]) : DEFAULT_SERVER_PORT;
        final Server httpServer = new Server();
        final SelectChannelConnector connector = new SelectChannelConnector();
        connector.setPort(port);
        httpServer.addConnector(connector);

        final WebAppContext webapp = new WebAppContext();
        webapp.setContextPath("/" + appDir.getName());
        webapp.setResourceBase(appPath);
        webapp.setClassLoader(Thread.currentThread().getContextClassLoader());
        httpServer.addHandler(webapp);
        httpServer.start();
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 