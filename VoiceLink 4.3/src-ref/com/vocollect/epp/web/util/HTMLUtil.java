/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.vocollect.epp.logging.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * HTMLUtils contains utility methods related to HTML. All methods in this
 * class are static.
 *
 * @author Dennis Doubleday
 */
public final class HTMLUtil {

    private static final Logger log = new Logger(HTMLUtil.class);
    /**
     * Constructor. Private to prevent instances.
     */
    private HTMLUtil() {

    }

    /**
     * This is a wrapper for URLEncoder.encodeURL that supplies an
     * an appropriate charset encoding value. UTF-8 is always used.
     * @param url the URL to encode
     * @return the UTF-8 encoded URL.
     */
    public static String encodeURL(String url) {

        if (url == null) {
            return null;
        } else {
            try {
                return URLEncoder.encode(url, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // This will never happen because UTF-8 is guaranteed to
                // be supported.
                log.warn("Could not encode with UTF-8 encoding");
                return url;
            }
        }
    }

    /**
     * Convert the specified string to a form with HTML special characters
     * encoded as entity references.
     * @param s the String to escape
     * @return the escaped String.
     */
    public static String escapeHTML(String s) {

        if (s == null) {
            return null;
        } else {
            StringBuffer sb = new StringBuffer(s.length());
            int n = s.length();
            for (int i = 0; i < n; i++) {
                char c = s.charAt(i);
                switch (c) {
                case '<':
                    sb.append("&lt;"); break;
                case '>':
                    sb.append("&gt;"); break;
                case '&':
                    sb.append("&amp;"); break;
                case '"':
                    sb.append("&quot;"); break;
                case '\'':
                    sb.append("&#39;"); break;
                case ' ':
                    sb.append("&nbsp;"); break;
                default:
                    sb.append(c); break;
                }
            }
            return sb.toString();
        }

    }

    /**
     * Apply escapeHTML() and URLEncoder.encode() in sequence to the specified URL.
     * This gives you a URL that is encoded for transmission and has HTML
     * special characters pre-escaped so parameters can be pulled from
     * the request object and inserted into an HTML stream without worry.
     **/
    /**
     * @param url the URL to escape and encoded
     * @return the escaped and encoded URL
     */
    public static String escapeHTMLAndEncodeURL(String url) {

        return encodeURL(escapeHTML(url));
    }

    /**
     * Add a parameter to the specified URL. The value is encoded
     * automatically.
     * @param url the URL to add to.
     * @param param the parameter to add.
     * @param value the value of the parameter.
     * @return the URL with the new parameter appended.
     */
    public static String addParameterToURL(String url,
                                           String param,
                                           String value) {

        // If the URL already contains a query string, add another
        // parameter. Otherwise, start a query string.
        String newURL = url;
        if (newURL.indexOf('?') != -1) {
            newURL += "&";
        } else {
            newURL += "?";
        }

        return newURL + param + "=" + encodeURL(value);

    }

    /**
     * Returns true if the specified parameter name exists in
     * the request context.
     * @param request the request
     * @param parameterName the parameter to look for.
     * @return true if the request contains the parameter, false otherwise.
     */
    public static boolean parameterExists(HttpServletRequest request,
                                          String             parameterName) {
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String element = params.nextElement();
            if (element.equals(parameterName)) {
                return true;
            }
        }

        return false;
    }

}


*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 