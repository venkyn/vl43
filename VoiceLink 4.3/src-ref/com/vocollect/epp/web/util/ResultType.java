/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.StrutsResultSupport;

/**
 * A Struts ResultType for JSON results.
 *
 * @author dkertis
 */
public class ResultType extends StrutsResultSupport {

    private static final long serialVersionUID = -4848018200053929556L;

    private String dataSource;

    /**
     * @return Returns the inputName.
     */
    public String getDataSource() {
        return (dataSource);
    }

    /**
     * @param inputName The inputName to set.
     */
    public void setDataSource(String inputName) {
        this.dataSource = inputName;
    }

    /**
     * {@inheritDoc}
     * @see org.apache.struts2.dispatcher.StrutsResultSupport#doExecute(java.lang.String, com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    protected void doExecute(String finalLocation, ActionInvocation invocation)
        throws Exception {

        // get the result String, or default to success if no dataSource
        // defined.
        String sb;
        if (getDataSource() != null) {
            sb = (String) invocation.getStack().findValue(
                conditionalParse(dataSource, invocation));
        } else {
            sb = Action.SUCCESS;
        }

        HttpServletResponse oResponse = (HttpServletResponse) invocation
            .getInvocationContext().get(HTTP_RESPONSE);

        // Set the content type
        oResponse.setCharacterEncoding("UTF-8");
        oResponse.setContentType("application/x-json");
        oResponse.setHeader("Cache-Control", "no-cache");
        oResponse.setHeader("Pragma", "no-cache");

        // Get the printwriter
        PrintWriter oOutput = oResponse.getWriter();

        // write
        oOutput.write(sb);

        // Flush
        oOutput.flush();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 