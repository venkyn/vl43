/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.util;

import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.User;
import com.vocollect.epp.ui.ToolTipObject;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.ResultData;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Class responsible for building JSON objects to return on an AJAX call.
 * Specifically handles DataProvider calls right now
 *
 * @author dkertis
 */
public class JSONResponseBuilder {
    private static final int MAX_AUTOCOMPLETE_RESULTS = 200;
    
    /**
     * @param c the collection returned by the filter.
     * @return JSON representation of the filtered collection
     * @throws JSONException on failure to create JSON object.
     */
    public static String buildFilterResponse(Collection<?> c) throws JSONException {
        // data for contructing JSON messages
        JSONObject json = new JSONObject();
        JSONObject resultSet = new JSONObject();
        JSONArray result = new JSONArray();
        
        int index = 0;
        for (Object o : c) {
            // cap the results to the MAX
            if (index == MAX_AUTOCOMPLETE_RESULTS) {
                break;
            }
            
            JSONObject jo = new JSONObject();
            jo.put("value", o.toString());
            result.put(index++, jo);            
            
        }
        
        resultSet.put("Result", result);
        json.put("ResultSet", resultSet);
        return json.toString();
    }
    

    private static final Logger log = new Logger(JSONResponseBuilder.class);
    
    // Constants used for JSON objects.
    public static final String ERROR_CODE = "errorCode";
    public static final String ERROR_SUCCESS = "0";
    public static final String ERROR_FAILURE = "1";
    public static final String ERROR_MESSAGE = "errorMessage";
    
    /** Value used to insert into a JSON object when a property is null. */
    public static final String NULL_PROPERTY_VALUE = "";
    
    /**
     * Utility class should have a protected constructor.
     */
    protected JSONResponseBuilder() {
    }
    
    /**
     * This method builds the JSON response for a table component request.
     * @param rd - A structure holding result information
     * @param rdi - A structure holding request information
     * @param currentUser - Current User making the request
     * @param columns - The columns to be included in the result
     * @param currentTime - The current time to be returned
     * @param action - Requesting DataProviderAction
     * @return - The response in JSON format
     */
     public static String makeDataProviderResponse(ResultData rd, 
                                                  ResultDataInfo rdi,
                                                  User currentUser, 
                                                  List<Column> columns,
                                                  String currentTime,
                                                  DataProviderAction action) {
        // main json object to return to client
        JSONObject json = new JSONObject();
        try {
            makeJSONObjects(json, "objects", rd.getData(), currentUser, columns, action);
            makeJSONObjects(json, "selection", rd.getSelectedData(), currentUser, columns, action);
            
            makeSliderItems(json, rd.getSliderList());
            makeDataProviderMetaData(json, rdi, rd, currentTime);
            
            json.put("currentTimeStamp", action.getCurrentSiteDateTime());
            
            json.put(ERROR_CODE, ERROR_SUCCESS);
        } catch (Exception ex) {
            log.error("Failure building data provider response", 
                      SystemErrorCode.DATA_PROVIDER_FAILURE, ex);
            return makeErrorResponse(ex.getMessage());
        }
        return json.toString();
    }
    
    /**
     * This object creates JSON objects for all the slider's tooltip values and 
     *  places them in the main JSON object.
     * @param json - The return object
     * @param sliderObjects - a list of objects representing slider items
     */
    public static void makeSliderItems(JSONObject json, 
                                       List<ToolTipObject> sliderObjects) {
        JSONArray sliderItems = new JSONArray();
        
        try {
        // loop through slider objects and build JSON result
        for (ToolTipObject dobj : sliderObjects) {
            
              sliderItems.put(dobj.toJSONObject()); 
            
         }
              json.put("sliderVals", sliderItems);
        } catch (JSONException e) {
            log.warn("Failure creating json response for slider items" + e.getMessage());
        }
        
    }
    
    /**
     * Adds metadata information to the main JSON object.
     * @param json - Main JSON object to be returned to the client
     * @param rdi - Information pertaining to the request
     * @param rd - Information pertaining to the response
     * @param currentTime - the timestamp of the response
     * @throws JSONException occurrs on JSON building error
     */
    public static void makeDataProviderMetaData(JSONObject json, ResultDataInfo rdi,
                                                ResultData rd, String currentTime) 
                                                throws JSONException {
        json.put("previousRowCount", rd.getNumPreviousRows());
        json.put("count", rd.getTotalRows());
        json.put("timestamp", currentTime);
        json.put("rowOffset", rdi.getOffset());
    }
    
    /**
     * Loops through the data and builds a JSON response for each object.
     * @param json - The return object
     * @param jsonObjectName - The json object
     * @param objects - The data objects
     * @param currentUser - The current user
     * @param columns - The data columns
     * @param action - The action requested
     * @throws JSONException error building JSON object
     * @throws VocollectException error using reflection
     */
    public static void makeJSONObjects(JSONObject json,
                                         String jsonObjectName,
                                         List<DataObject> objects, 
                                         User currentUser, 
                                         List<Column> columns,
                                         DataProviderAction action) throws JSONException, VocollectException {
        JSONArray jsonItems = new JSONArray();
        
        // loop through objects and convert to JSON objects
        for (DataObject dobj : objects) {
            jsonItems.put(makeJSONObject(dobj, currentUser, columns, action));    
        }
        
        json.put(jsonObjectName, jsonItems);
    }
    
    /**
     * Turn the specified object into a JSON representation. This is a method so
     * subclasses can do something different, if need be.
     * @param dataObj the object to convert
     * @param column the column
     * @param obj that needs a translation
     * @param  du the DisplayUtilities 
     * @return the JSON representation
     * @throws VocollectException mainly a reflection issue
     */
    public static Object translateToDisplayValue(DataObject dataObj,
                                                 Column column,
                                                 DisplayUtilities du,
                                                 Object obj)
        throws VocollectException {
        
        Object val = null;
        
        try {
            
            // Check for special display handling
            if (!StringUtil.isNullOrBlank(column.getDisplayFunction())) {
                // A displayFunction was specified.
                Method m = du.getClass()
                    .getMethod(
                        column.getDisplayFunction(), Object.class,
                        DataObject.class);
             
                val = m.invoke(du, obj, dataObj);
                
            } else if (obj instanceof ValueBasedEnum) {
                // If it is an enum, resolve to the localized name.
                val = ResourceUtil.getLocalizedEnumName(((ValueBasedEnum<?>) obj));
                
            } else if (obj != null
                    && column.getSortType().equals(
                        ColumnSortType.SystemDataLocalized)) {
                // This is localized system data. Display the translation.
               val = ResourceUtil.getLocalizedKeyValue((String) obj);
                
            } else {
                if (obj != null
                    && (column.getSortType().equals(
                        ColumnSortType.UserDataLocalized) || column
                        .getSortType().equals(ColumnSortType.CodeOnlyLocalized))) {

                    // This is potentially translated user data.
                    // Display the translation, if it exists.
                    val = du.translateUserData(obj.toString());
                } else {
                    val = obj;
                }
            } 
        } catch (NestedNullException nne) {
              return NULL_PROPERTY_VALUE;
        } catch (IllegalAccessException e) {
            throw new VocollectException(e);
        } catch (InvocationTargetException e) {
             throw new VocollectException(e);
        } catch (NoSuchMethodException e) {
            throw new VocollectException(e);
        }
        return val;

    }
                                         

    
    /**
     * Turn the specified object into a JSON representation. This is a method so
     * subclasses can do something different, if need be.
     * @param dataObj the object to convert
     * @param currentUser the user
     * @param columns the list of columns the user wants.
     * @param action the requesting DataProviderAction class
     * @return the JSON representation
     * @throws JSONException error from malformed JSON Object
     * @throws VocollectException mainly a reflection issue
     */
    protected static JSONObject makeJSONObject(DataObject   dataObj,
                                               User         currentUser,
                                               List<Column> columns,
                                               DataProviderAction action)
                                        throws JSONException, VocollectException {
        JSONObject jo = new JSONObject();
        Object obj;
        
        try {
            for (Column column : columns) {
                // if (column.isVisible() || column.isRequired()) {
                    try {
                        obj = PropertyUtils.getNestedProperty(dataObj, column.getField());
                        Object val = translateToDisplayValue(dataObj, column, action.getDisplayUtilities(), obj);
                        buildNestedJSON(jo, column.getField(), val);
                     } catch (NestedNullException nne) {
                        buildNestedJSON(jo, column.getField(), NULL_PROPERTY_VALUE);
                    } 
                    String[] fieldSet = column.getExtraFieldSet(); 
                    if (fieldSet != null) {
                        for (int i = 0; i < fieldSet.length; i++) {
                            try {                                
                                obj = PropertyUtils.getNestedProperty(dataObj, fieldSet[i]);
                                buildNestedJSON(jo, fieldSet[i], obj);
                            } catch (NestedNullException nne) {
                                buildNestedJSON(jo, fieldSet[i], NULL_PROPERTY_VALUE);
                            }
                        }
                    }
                //}
            }
       } catch (IllegalAccessException e) {
            throw new VocollectException(e);
        } catch (InvocationTargetException e) {
            throw new VocollectException(e);
        } catch (NoSuchMethodException e) {
            throw new VocollectException(e);
        }

        return jo;
    }
    
    /**
     * Build a JSON representation of nested data.
     * @param jo the JSONObject to add stuff to.
     * @param fieldName the field to add
     * @param value the value to add
     */
    @SuppressWarnings("unchecked")
    protected static void buildNestedJSON(JSONObject jo, String fieldName, Object value) {
        try {
            if (fieldName.indexOf('.') > -1) {
                JSONObject newJo;
                if (jo.has(fieldName.substring(0, fieldName.indexOf('.')))) {
                    newJo = jo.getJSONObject(fieldName.substring(0, fieldName.indexOf('.')));
                } else {
                    newJo = new JSONObject();
                }
                buildNestedJSON(newJo, fieldName.substring(fieldName.indexOf('.') + 1), value);
                jo.put(fieldName.substring(0, fieldName.indexOf('.')), 
                        newJo);
            } else {
                if (value instanceof ValueBasedEnum) {
                    try {
                        jo.put(fieldName, 
                            ResourceUtil.getLocalizedEnumName((ValueBasedEnum) value));
                    } catch (Exception e) {
                        jo.put(fieldName, 
                            ((ValueBasedEnum) value).getResourceKey());                        
                    }
                } else {
                    jo.put(fieldName, value);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /** Creates a JSON response to return as an error.
     * returns a error message in JSON format
     * @param message to return
     * @return json error message
     */
    public static String makeErrorResponse(String message) {
        JSONObject json = new JSONObject();
        try {
            json.put(ERROR_CODE, ERROR_FAILURE);
            json.put(ERROR_MESSAGE, message);
        } catch (JSONException e) {
            e.printStackTrace();
        } 
        return json.toString();

    }
    
    /**
     * Creates a JSON response to return a generic success.
     * @return json success message.
     */
    public static String makeSuccessResponse() {
        JSONObject json = new JSONObject();
        try {
            json.put(ERROR_CODE, ERROR_SUCCESS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }
    
    /**
     * Returns a JSON response to return a generic success.
     * @param json JSON object to be used for response.
     * @return JSON Object with success response
     */
    public static String makeSuccessResponse(JSONObject json) {
        try {
            json.put(ERROR_CODE, ERROR_SUCCESS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    /**
     * This method builds the JSON response for a table component request.
     * @param objects Objects used to build the response
     * @param currentUser - Current User making the request
     * @param columns - The columns to be included in the result
     * @param action - Requesting DataProviderAction
     * @return - The response in JSON format
     */   
    public static String makePrintResponse(List<DataObject> objects, User currentUser,
                                             List<Column> columns, DataProviderAction action) {
        // main json object to return to client
        JSONObject json = new JSONObject();
        try {
            makeJSONObjects(json, "objects", objects, currentUser, columns, action);
            json.put(ERROR_CODE, ERROR_SUCCESS);
        } catch (Exception ex) {
            makeErrorResponse(ex.getMessage());
        }
        return json.toString();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 