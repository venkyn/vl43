/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.action.DataProviderAction;

import java.util.List;

/**
 * Utility class to house the Business logic of the Data Provider 
 * printing implementation.
 *
 * @author dkertis
 */
public class DataProviderPrintUtil extends DataProviderTableUtil {

    /**
     * {@inheritDoc}
     * @throws DataAccessException 
     * @see com.vocollect.epp.web.util.DataProviderTableUtil#makeJSONResponse(com.vocollect.epp.util.ResultDataInfo, java.util.List, com.vocollect.epp.model.User, java.util.List, java.lang.String, com.vocollect.epp.web.action.DataProviderAction)
     */
    @Override
    protected String makeJSONResponse(ResultDataInfo rdi,
                                      List<DataObject> data,
                                      User currentUser,
                                      List<Column> columns,
                                      String currentTime,
                                      DataProviderAction action) {
        try {
            data = postProcessData(rdi, data.size(), data, action);
        } catch (DataAccessException e) {
            return JSONResponseBuilder.makeErrorResponse(e.getMessage());
        }
        return JSONResponseBuilder.makePrintResponse(
            data, currentUser, columns, action);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 