/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.ui.UserPreferencesErrorCode;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.NotificationSummary;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.DataProviderAction;

import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * This is a utility class that is responsible for rendering the various table
 * components item specifically, due to any unique display requirements. The
 * app-specific subclass of this class is injected into all
 * <code>DataProviderAction</code> instances.
 *
 * @author Jim Geisler
 * @author Jeff Stonebrook
 * @author ddoubleday -- refactor into EPP and App-specific files, documented.
 */
public class DisplayUtilities {

    private static final Logger log = new Logger(DisplayUtilities.class);

    // This must be set before use
    private User currentUser;

    private DataProviderAction action;

    // Spring-injected manager.
    private DataTranslationManager dataTranslationManager;

    /**
     * Constructor.
     */
    public DisplayUtilities() {
    }


    /**
     * Getter for the currentUser property.
     * @return the value of the property
     */
    public User getCurrentUser() {
        return this.currentUser;
    }

    /**
     * Setter for the currentUser property.
     * @param currentUser the new currentUser value
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * Gets the Locale from the ThreadLocal, or returns the default Locale.
     * @return the Locale, or the default system locale if not set.
     */
    public Locale getLocale() {
        Locale locale = LocaleContextHolder.getLocale();
        if (locale != null) {
            return locale;
        } else {
            return Locale.getDefault();
        }
    }

    /**
     * Gets the SiteContext from the ThreadLocal.
     * @return the context, or null if not set.
     */
    public SiteContext getSiteContext() {
        return SiteContextHolder.getSiteContext();
    }

    /**
     * @return the current action.
     */
    public DataProviderAction getAction() {
        return action;
    }

    /**
     * Set the current action.
     * @param action the action.
     */
    public void setAction(DataProviderAction action) {
        this.action = action;
    }


    /**
     * Getter for the dataTranslationManager property.
     * @return the value of the property
     */
    protected DataTranslationManager getDataTranslationManager() {
        return this.dataTranslationManager;
    }



    /**
     * Setter for the dataTranslationManager property. Spring-injected.
     * @param dataTranslationManager the new dataTranslationManager value
     */
    public void setDataTranslationManager(DataTranslationManager dataTranslationManager) {
        this.dataTranslationManager = dataTranslationManager;
    }


    /**
     * Format a set of user roles.
     * @param obj the set of roles.
     * @param dataObj the containing object.
     * @return the JSONArray of formatted roles.
     * @throws JSONException on JSON error.
     */
    public JSONArray formatRoles(Object obj, DataObject dataObj)
        throws JSONException {

        Set<Role> roles = (Set<Role>) obj;
        JSONArray ja = new JSONArray();

        for (Iterator<Role> iter = roles.iterator(); iter.hasNext();) {
            JSONObject roleJO = new JSONObject();
            Role element = iter.next();
            roleJO.put("id", element.getId());
            roleJO.put("name", element.getName());
            roleJO.put("isAdministrative", element.getIsAdministrative());
            ja.put(roleJO);
        }
        return ja;
    }


    /**
     * Format a date.
     * @param obj the date.
     * @param dataObj the containing data object.
     * @return the formatted Date string.
     */
    public String formatTime(Object obj, DataObject dataObj) {
        Date timeStamp = (Date) obj;
        return DateUtil.formatTimestampForTableComponent(timeStamp);
    }

    /**
     * Format a date with time zone, according to the time zone of the
     * associated Site, if any, or by the server time zone otherwise.
     * @param obj the date.
     * @param dataObj the containing data object.
     * @return the formatted time.
     * @throws DataAccessException on database failure.
     */
    public String formatTimeWithTimeZone(Object obj, DataObject dataObj)
        throws DataAccessException {
        return formatDateTimeWithTimeZone(obj, dataObj, true);
    }

    /**
     * Format a date with time zone, according to the time zone of the
     * associated Site, if any, or by the server time zone otherwise.
     * @param obj the date.
     * @param dataObj the containing data object.
     * @return the formatted time.
     * @throws DataAccessException on database failure.
     */
    public String formatDateWithTimeZone(Object obj, DataObject dataObj)
        throws DataAccessException {
        return formatDateTimeWithTimeZone(obj, dataObj, false);
    }

    /**
     * Format a date with time zone, according to the time zone of the
     * associated Site, if any, or by the server time zone otherwise.
     * @param obj the date.
     * @param dataObj the containing data object.
     * @param includeTime if true, include the time in the return value,
     * otherwise just the date.
     * @return the formatted time.
     * @throws DataAccessException on database failure.
     */
    protected String formatDateTimeWithTimeZone(Object obj, DataObject dataObj, boolean includeTime)
        throws DataAccessException {

        Date timeStamp = (Date) obj;
        String formattedDate = null;
        SiteContext siteContext = getSiteContext();

        if (siteContext.isInCurrentSiteMode()) {
            // If we are in a current site context, i.e. not all or default,
            // then
            // we need to get the timezone of the current site, and use that to
            // display
            // the date
            // if we already looked up the site, it'll be in the site context
            // if not, look it up and put it in the site context
            Site site = null;
            if (siteContext.getCurrentSite() == null) {
                Long tagId = currentUser.getCurrentSite();
                site = siteContext.getSiteByTagId(tagId.longValue());
                siteContext.setCurrentSite(site);
            } else {
                site = siteContext.getCurrentSite();
            }
            if (includeTime) {
                formattedDate = DateUtil.formatTimestampForTableComponent(
                    timeStamp, site.getTimeZone(), getLocale());
            } else {
                formattedDate = DateUtil.formatDateForTableComponent(
                    timeStamp, site.getTimeZone(), getLocale());
            }
        } else if (siteContext.isInAllSiteMode()) {
            // else if we are in an all site context, get the timezone from the
            // site that
            // the object is related to
            if (dataObj instanceof Taggable) {
                // Make sure we are an instance of Taggable
                Taggable taggedObject = (Taggable) dataObj;

                // We need to grab a tag that belongs to a site. For now -
                // assume the first
                // site tag is the appropriate one. If this does not end up
                // being true - then
                // we will need to define a selection mechanism
                Set < Tag > tags = taggedObject.getTags();
                Long tagId = null;
                Iterator < Tag > tagIter = tags.iterator();
                while (tagIter.hasNext() && tagId == null) {
                    Tag tmpTag = tagIter.next();
                    if (tmpTag.getTagType().equals(Tag.SITE)) {
                        tagId = tmpTag.getId();
                    }
                }

                if (tagId != null && tagId != Tag.SYSTEM && tagId != Tag.ALL) {
                    Site site = siteContext.getSiteByTagId(tagId);
                    if (includeTime) {
                        formattedDate = DateUtil.formatTimestampForTableComponent(
                            timeStamp, site.getTimeZone(), getLocale());
                    } else {
                        formattedDate = DateUtil.formatDateForTableComponent(
                            timeStamp, site.getTimeZone(), getLocale());
                    }
                } else {
                    // We couldn't find a site - odd - so just use the default
                    // case
                    if (includeTime) {
                        formattedDate = DateUtil.formatTimestampForTableComponent(
                            timeStamp, TimeZone.getDefault(), getLocale());
                    } else {
                        formattedDate = DateUtil.formatDateForTableComponent(
                            timeStamp, TimeZone.getDefault(), getLocale());
                    }
                }
            }
        } else if (siteContext.isInSystemMode()) {
            // else if we are in a system context, get the default timezone from
            // the server
            if (includeTime) {
                formattedDate = DateUtil.formatTimestampForTableComponent(
                    timeStamp, TimeZone.getDefault(), getLocale());
            } else {
                formattedDate = DateUtil.formatDateForTableComponent(
                    timeStamp, TimeZone.getDefault(), getLocale());
            }
        }

        return formattedDate;
    }

    /**
     * Transforms the raw logType to a localized key.
     * @param obj Object value.
     * @param dataObj DataObject.
     * @return String of translated logType or raw logType.
     */
    public String displayLogType(Object obj, DataObject dataObj) {
        String logType = (String) obj;
        try {
            logType = interpretMessage("log.type." + logType, null);
        } catch (Exception e) {
            log.warn("This log type does not have a message key; instead the raw actual type will be shown.",
                UserPreferencesErrorCode.ERROR_NO_SUCH_FILE_TYPE);
        }
        return logType;
    }

    /**
     * Resolve a message key against our local resource bundle source.
     * @param obj assumed to be a resource property key value.
     * @param dataObj the data object we're working on.
     * @return the resolved String, or the unchanged key if nothing found.
     */
    public String interpretMessage(Object obj, DataObject dataObj) {
        return ResourceUtil.getLocalizedKeyValue((String) obj);
    }



    /**
     * Translate a String representing a piece of user data to the
     * current locale. If there is no translation for that locale,
     * return the original userData.
     * @param userData the untranslated field
     * @return the (possibly) translated field.
     * @throws DataAccessException on database failure.
     */
    public String translateUserData(String userData) throws DataAccessException {
        return getDataTranslationManager().translate(userData);
    }

    /**
     * Translate a String representing a piece of user data to the
     * current locale. If there is no translation for that locale,
     * return the original userData.
     * @param obj assumed to be a String representing the untranslated value
     * @param dataObj never used in this case.
     * @return the (possibly) translated field.
     * @throws DataAccessException on database failure.
     */
    public String translateUserData(Object obj, DataObject dataObj) throws DataAccessException {
        if (obj == null) {
            return null;
        } else {
            return translateUserData(obj.toString());
        }
    }

    /**
     * Construct a Site info object from the specified data, in which we
     * check for whether or not the dataObj is taggable.
     * @param obj the property being processed.
     * @param dataObj the owning data object
     * @return the site info in JSONObject form.
     * @throws JSONException on JSON error
     * @throws DataAccessException on data error
     */
    public JSONObject formatSite(Object obj, DataObject dataObj)
        throws JSONException, DataAccessException {

        // Note that this differs from formatSiteForSummary in that
        // the dataObj is passed as both arguments here.
        return doFormatSite(dataObj, dataObj);

    }

    /**
     * Construct a Site info object from the specified data, in which we
     * check for whether or not the obj is taggable.
     * @param obj the property being processed.
     * @param dataObj the owning data object
     * @return the site info in JSONObject form.
     * @throws JSONException on JSON error
     * @throws DataAccessException on data error
     */
    public JSONObject formatSiteForSummary(Object obj, DataObject dataObj)
        throws JSONException, DataAccessException {

        return doFormatSite(obj, dataObj);

    }

    /**
     * Format a NotificationSummary message.
     * @param obj assumed to be a resource key.
     * @param dataObj the containing data object.
     * @return the formatted message.
     * @throws DataAccessException on database failure.
     */
    public String displayNotificationSummaryMessage(Object obj,
                                                    DataObject dataObj)

        throws DataAccessException {

        String message = (String) obj;
        NotificationSummary notificationSummary = (NotificationSummary) dataObj;
        if (message.equals("NoUnacknowledged")) {
            return ResourceUtil.getLocalizedMessage(
                "notification.summary.noUnacknowledged", null, message);
        } else if (message.equals("AccessToOneSite")) {
            if (action.getNumberOfSites() == 1) {
                // We only have access to one site - so display the summary
                // message without site name
                return ResourceUtil.getLocalizedMessage(
                    "notification.summary.text",
                    new Object[] { notificationSummary.getNotificationCount()},
                    message);
            } else {
                // Even though we only have one site in the summary, we
                // still have access to more, therefore display the site name
                return ResourceUtil.getLocalizedMessage(
                    "notification.summary.site.text",
                    new Object[] {notificationSummary.getSiteName(),
                                  notificationSummary.getNotificationCount()},
                    message);
            }
        } else {
            return ResourceUtil.getLocalizedMessage(
                "notification.summary.site.text",
                new Object[] {notificationSummary.getSiteName(),
                              notificationSummary.getNotificationCount()},
                message);
        }

    }

    /**
     * Construct a Site info object from the specified data. This does
     * the real work of the previous two methods.
     * @param o the property being processed.
     * @param data the owning data object
     * @return the site info in JSONObject form.
     * @throws JSONException on JSON error
     * @throws DataAccessException on data error
     */
    protected JSONObject doFormatSite(Object o, DataObject data)
        throws JSONException, DataAccessException {

        JSONObject siteInfo = new JSONObject();

        if (o instanceof Taggable) {
            // Make sure we are an instance of Taggable
            Taggable taggedObject = (Taggable) o;

            // TODO ??
            // We need to grab a tag that belongs to a site. For now - assume
            // the first site tag is the appropriate one. If this does not end
            // up being true - then we will need to define a selection mechanism
            Set<Tag> tags = taggedObject.getTags();
            Long siteId = null;
            Iterator<Tag> tagIter = tags.iterator();
            while (tagIter.hasNext() && siteId == null) {
                Tag tmpTag = tagIter.next();
                if (tmpTag.getTagType().equals(Tag.SITE)) {
                    siteId = tmpTag.getTaggableId();
                }
            }

            Site site = null;
            if (siteId != null && siteId != Tag.SYSTEM && siteId != Tag.ALL) {
                site = getSiteContext().getSite(siteId);

                siteInfo.put("id", site.getId());
                siteInfo.put("name", site.getName());
            } else {
                siteInfo.put("name", interpretMessage(
                    "notification.dropdown.site.System", data));
            }
        }

        return siteInfo;
    }

    /**
     * Format a time zone.
     * @param obj the time zone id.
     * @param dataObj the containing data object.
     * @return the formatted Time Zone string.
     */
    public String formatTimeZone(Object obj, DataObject dataObj) {
        String timeZoneId = (String) obj;
        return ResourceUtil.getLocalizedKeyValue("timezone.id." + timeZoneId);
    }

    /**
     * Method to format dashboard isDefault status.
     * @param obj - the isDefault property
     * @param dataObj - the dashboard object
     * @return  true/false result
     */
    public Boolean formatisDefaultDashboard(Object obj, DataObject dataObj) {
        Site currentSite = SiteContextHolder.getSiteContext().getCurrentSite();
        Dashboard db = (Dashboard) dataObj;
        String propName = "default.dashboard.for.site." + currentSite.getId();
        
        Set<UserProperty> userProps = getCurrentUser().getUserProperties();
        for (UserProperty userProperty : userProps) {
            if (userProperty.getName().equals(propName)
                && Long.valueOf(userProperty.getValue()).equals(db.getId())) {
                return true;
            }
        }
        
        return false;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 