/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.FilterInvalidParameterException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.User;
import com.vocollect.epp.ui.ToolTipObject;
import com.vocollect.epp.util.PropertyComparator;
import com.vocollect.epp.util.ResultData;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.action.DataProviderAction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.beanutils.PropertyUtils;

/**
 * Utility class to house the Business logic of the Data Provider
 * implementation.
 * 
 * @author dkertis
 */
public class DataProviderTableUtil implements DataProviderUtil {

    private static final Logger log = new Logger(DataProviderTableUtil.class);

    /**
     * Constant used by the find function to indicate the item was not found.
     */
    public static final int ITEM_NOT_FOUND = -1;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.util.DataProviderUtil#getJSONData(com.vocollect.epp.util.ResultDataInfo,
     *      java.lang.Object, java.lang.String, com.vocollect.epp.model.User,
     *      java.util.List, java.lang.String,
     *      com.vocollect.epp.web.action.DataProviderAction, boolean)
     */
    public String getJSONData(ResultDataInfo rdi,
                              Object dataClass,
                              String getMethod,
                              User currentUser,
                              List<Column> columns,
                              String currentTime,
                              DataProviderAction action,
                              boolean useNullData) {

        // Add the list of columns to the RDI. Information in the sort
        // column object is needed for sorting.

        rdi.setSortColumnObjectFromColumns(columns);
        
        List<DataObject> objects = new LinkedList<DataObject>();
        if (!useNullData) {
            objects = callGetDataMethod(rdi, dataClass, getMethod);
        }

        // Apply any in-memory sorting and filtering required.
        objects = applyInMemorySort(applyInMemoryFilter(objects, rdi), rdi, action);

        return makeJSONResponse(
            rdi, objects, currentUser, columns, currentTime, action);
    }

    /**
     * @param rdi Information about the request.
     * @param data the data to include in the response (possibly windowed)
     * @param currentUser User making the request
     * @param columns Columns to render
     * @param currentTime timestamp of the request
     * @param action Request DataProviderAction class data set with the
     *            specified parameters, don't actually get the data.
     * @return The JSON response in String format
     */
    protected String makeJSONResponse(ResultDataInfo rdi,
                                      List<DataObject> data,
                                      User currentUser,
                                      List<Column> columns,
                                      String currentTime,
                                      DataProviderAction action) {
        // build selected data
        List<DataObject> selectedData = getSelectedData(rdi, data);
        if (selectedData.size() > 0 && rdi.isFirstTimeRun()) {
            rdi
                .setFirstRowId(selectedData.get(selectedData.size() - 1)
                    .getId());
        }
        ResultData rd = windowData(rdi, data, action.getDisplayUtilities());
        rd.setSelectedData(selectedData);
        try {           
             // allow a subclass to add data to the end
            rd.setData(postProcessData(rdi, rd.getTotalRows(), rd.getData(), action));
            
        } catch (DataAccessException e) {
            return JSONResponseBuilder.makeErrorResponse(e.toString());
        }

        if (log.isTraceEnabled()) {
            log.trace(rdi);
            log.trace(rd);
        }
        return JSONResponseBuilder.makeDataProviderResponse(
            rd, rdi, currentUser, columns, currentTime, action);
    }

    
    /**
     * This function allows for a sublcass to add data to request that exists outside the normal
     * table component stack.  One example is putting totals summary page.
     * @param rdi - ResultDataInfo for the request.
     * @param totalRows .
     * @param data - the data that fulfills the request from the client
     * @param action - the action for the request.
     * @return the data to be returned to the client
     * @throws DataAccessException exception
     */ 
    protected List<DataObject> postProcessData(ResultDataInfo rdi, long totalRows, List<DataObject> data, 
        DataProviderAction action) throws DataAccessException {
        return data;
    }

    /**
     * @param rdi Information about the request.
     * @param dataClass Spring bean to call method on
     * @param getMethod Method to call through reflection
     * @return A list of objects from the database to be windowed
     */
    @SuppressWarnings("unchecked")
    public static List<DataObject> callGetDataMethod(ResultDataInfo rdi,
                                                     Object dataClass,
                                                     String getMethod) {
        List<DataObject> objects = null;
        try {
            Class cls = dataClass.getClass();
            Class[] partypes = new Class[1];
            partypes[0] = rdi.getClass();
            Method meth = cls.getMethod(getMethod, partypes);
            Object[] arglist = new Object[1];
            arglist[0] = rdi;
            Object retobj = meth.invoke(dataClass, arglist);
            objects = (List<DataObject>) retobj;
            return objects;
        } catch (Exception e) {
            if (e.getCause() instanceof FilterInvalidParameterException) {
                throw (FilterInvalidParameterException) e.getCause();
            } else {
                log.error("Error occurred during reflection call "
                    + e.getMessage(), SystemErrorCode.DATA_PROVIDER_FAILURE, e);
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * @param rdi Information about the request.
     * @param allObjects A list of objects to be windowed
     * @param displayUtils the DisplayUtilities object.
     * @return a structure containing the objects to be used to render the
     *         response
     */
    public static ResultData windowData(ResultDataInfo rdi,
                                        List<DataObject> allObjects,
                                        DisplayUtilities displayUtils) {
        ResultData resultData = new ResultData();

        List<DataObject> data = null; // The list of objects to return
        int index = ITEM_NOT_FOUND;
        
     // save the count of records retrieved
        int dataCount = allObjects.size();

        if (rdi.getFirstRowId() != null) {
            // find the index of the starting row
            index = findById(allObjects, rdi.getFirstRowId());

            // We are building the resultset by ID. The value we want to
            // center the results around has an offset of index.
            if (index != ITEM_NOT_FOUND) {
                rdi.setOffset(index);
            } else {
                // if we can't find the item, and this is not a refresh
                // don't return anything (it would not be good to return
                // duplicate records while scrolling
                if (!rdi.getRefreshRequest()) {
                    rdi.setRowCount(0);
                }
            }
        }

        // if we are requesting an offset past the end of the dataset
        // return the last page of data
        if (rdi.getOffset() >= dataCount) {
            rdi.setOffset(dataCount - rdi.getRowsPerPage());
        }

        // we may need to adjust our result info if fulfilling
        // the request may not be possible (i.e., not enough rows to fill
        // a request.) So adjust the numbers accordingly
        updateResultInfo(resultData, rdi, dataCount);

        // build the list ourselves with the ID
        data = new ArrayList<DataObject>();
        long startIndex = rdi.getOffset() + rdi.getStartIndex();
        for (long i = startIndex; i < startIndex + rdi.getRowCount(); i++) {
            data.add(allObjects.get((int) i));
        }
        // build tooltips
        List<ToolTipObject> sliderList = getToolTips(rdi, allObjects, displayUtils);

        // build the result list to return to the client
        resultData.setTotalRows(dataCount);
        resultData.setData(data);
        resultData.setSliderList(sliderList);
        if (rdi.getStartIndex() > 0) {
            resultData.setNumPreviousRows(0);
        } else {
            resultData.setNumPreviousRows(-rdi.getStartIndex());
        }

        return resultData;
    }

    /**
     * Sort the specified list of objects by the specified property name.
     * @param sortList the list to sort
     * @param rdi the ResultDataInfo
     * @param action the DataProviderAction
     * @return the sorted list, or the original list if sortColumn was null or
     *         if the sortType of the column was not <code>CodeOnly</code>.
     */
    protected static List<DataObject> applyInMemorySort(List<DataObject> sortList,
                                                        ResultDataInfo rdi, DataProviderAction action) {
        Column sortColumn = rdi.getSortColumnObject();
        if (sortColumn != null && !sortList.isEmpty()) {
            if (sortColumn.getSortType().equals(ColumnSortType.CodeOnly)) {
                if (log.isTraceEnabled()) {
                    log.trace("Sorting results by property "
                        + sortColumn.getField());
                }
                Collections.sort(sortList, new PropertyComparator<DataObject>(
                    sortColumn.getField(), rdi.isSortAscending()));
            } else if (sortColumn.getSortType().equals(ColumnSortType.CodeOnlyLocalized)) {
                if (log.isTraceEnabled()) {
                    log.trace("Sorting results by localized property "
                        + sortColumn.getField());
                }
                DisplayUtilities du = action.getDisplayUtilities();
                
                // Localize the sort property of each row prior to sorting.
                for (int index = 0; index < sortList.size(); index++) {
                    DataObject dataObject = sortList.get(index);
                    try {
                        Object prop = PropertyUtils.getNestedProperty(dataObject, sortColumn.getField());
                        if (prop instanceof String) {
                            prop = du.translateUserData((String) prop);
                        }
                        
                        PropertyUtils.setNestedProperty(dataObject, sortColumn.getField(), prop);
                        sortList.set(index, dataObject);
                    } catch (Exception e) {
                        log.debug("Problem getting object property "
                            + sortColumn.getField());
                    }
                }
                
                // Sort the list that now contains the localized sort data.
                Collections.sort(sortList, new PropertyComparator<DataObject>(
                    sortColumn.getField(), rdi.isSortAscending()));
            }
        }

        return sortList;
    }

    /**
     * Filter the list in code if necessary.
     * @param filterList the list to filter (possibly)
     * @param rdi the ResultDataInfo
     * @return the list, filtered if the RDI specified an in-code filter
     */
    protected static List<DataObject> applyInMemoryFilter(List<DataObject> filterList,
                                                          ResultDataInfo rdi) {
        try {
            if (rdi != null) {
                if (rdi.getFilter() != null) {
                    rdi.getFilter().applyFilter(filterList, rdi.getDisplayUtilties());
                }
            }
        } catch (FilterInvalidParameterException fipe) {
            // TODO: consider either restricting invalid user filter entry from UI OR
            // throwing the exception to be displayed on UI as red box 
            log.warn(
                "FilterInvalidParameterException operand: "
                    + fipe.getOperandType() + " | value:" + fipe.getValue(),
                fipe);
        } catch (Exception e) {
            // Normally we would want to throw a data access exception
            // here - but since the people who are calling this method
            // can't handle the exception - we are going to go with the
            // default behavior of simply returning the return list
            log.warn("Exception", e);
        }
        return filterList;
    }

    /**
     * @param rd the result data
     * @param rdi (ouput) params to define window for data
     * @param dataCount total number of rows in the data source
     */
    private static void updateResultInfo(ResultData rd,
                                         ResultDataInfo rdi,
                                         long dataCount) {
        // updating by offset, so change the offset to start returning the
        // results
        // taking the startIndex into account
        long rowCount = rdi.getRowCount();
        // keep track of the num previous returns
        long startIndex = rdi.getStartIndex();

        // if we are fetching the beginning of a data set,
        // there may not be enough rows to satisfy the request,
        // so truncate the fetch size
        // and make sure we don't get more data than we are asking for

        if (rdi.getOffset() + startIndex < 0) {
            startIndex = -rdi.getOffset();
            rowCount -= Math.abs(rdi.getStartIndex()) - Math.abs(startIndex);
            // rowCount + rdi.getOffset() + startIndex;

        }

        // make sure we are not going off the end of the data
        if (rdi.getOffset() + startIndex + rowCount >= dataCount) {
            rowCount = dataCount - (rdi.getOffset() + startIndex);
        }

        // adjust the values accordingly
        rdi.setRowCount((int) rowCount);
        rdi.setStartIndex((int) startIndex);
    }

    /**
     * Calculates how many tooltips to return to client.
     * @param rowsPerPage number of rendered rows per window
     * @param dataCount total number of rows in datasource
     * @param numRequested max number of tooltips requested
     * @return the number of tooltips
     */
    private static int calculateNumTooltips(int rowsPerPage,
                                            int dataCount,
                                            int numRequested) {
        // take the lesser of number of pixels on the slider
        // or number of rows in the data source less the window size

        return Math.min(numRequested, dataCount - rowsPerPage + 1);
    }

    /**
     * Gets the data for the selected ids.
     * @param rdi selected id information.
     * @param allObjects the objects from the datasource.
     * @return the selected objects.
     */
    private static List<DataObject> getSelectedData(ResultDataInfo rdi,
                                                    List<DataObject> allObjects) {

        List<DataObject> selectedData = new ArrayList<DataObject>();
        HashSet<Long> selectedIds = rdi.getSelectedIds();

        if (selectedIds != null && selectedIds.size() > 0) {
            for (DataObject dd : allObjects) {
                if (selectedIds.contains(dd.getId())) {
                    selectedData.add(dd);
                }
            }
        }

        return selectedData;
    }

    /**
     * Function to build a list of tooltips.
     * @param rdi winodw param information
     * @param allObjects the objects from the datasource
     * @param displayUtils the DisplayUtilities object.
     * @return the tooltips
     */
    private static List<ToolTipObject> getToolTips(ResultDataInfo rdi,
                                                List<DataObject> allObjects,
                                                DisplayUtilities displayUtils) {
        List<ToolTipObject> sliderList = new ArrayList<ToolTipObject>();
        
        
        // 
        if (rdi.getTooltipColumn() == null) {
            return new ArrayList<ToolTipObject>(); 
        }
        
        
        try {
    
        if (rdi.getNumToolTips() == 0) {
            return sliderList;
        }

        int numToolTips = calculateNumTooltips(rdi.getRowsPerPage(), allObjects
            .size(), rdi.getNumToolTips());

        // if there is more data than can fit in the tooltips
        // get a slicing of the data which spans evenly
        // across the dataset
        int slice = (int) Math
            .ceil(((float) allObjects.size() - (float) rdi
                .getRowsPerPage()) / numToolTips);

        // loop through objects building tooltips
        for (int i = 0; i < allObjects.size()
            && sliderList.size() <= numToolTips; i += slice) {

            // we want a tooltip for every {slice} objects
            Object startValue = getProperty(
                allObjects.get(i), rdi, displayUtils);
            
            
            Column toolTipColumn = new Column();
            toolTipColumn = rdi.getSortColumnObject();
           
            Object start = JSONResponseBuilder.
                          translateToDisplayValue(allObjects.get(i), toolTipColumn, displayUtils, startValue);
          
            // the endValue tooltips should be the current tooltip value 
            // offsetted by the window size. Unless there is no more data left.
            // In which case just get the last value of the dataset
            int index = i + rdi.getRowsPerPage() - 1 <= allObjects.size() - 1
                ? i + rdi.getRowsPerPage() - 1 : allObjects.size() - 1;
                Object endValue = getProperty(
                allObjects.get(index), rdi,  displayUtils);
                Object end = JSONResponseBuilder.
                        translateToDisplayValue(allObjects.get(index), toolTipColumn, displayUtils, endValue);
            ToolTipObject tooltipObj = new ToolTipObject(allObjects.get(i)
                .getId(), start, end, i);

            sliderList.add(tooltipObj);
        }
      
        // get the last item as a end tooltip marker
        if (allObjects.size() > 0) {
            Object startValue = getProperty(allObjects
                .get(allObjects.size() - 1), rdi, displayUtils);
            Object start = JSONResponseBuilder.
            translateToDisplayValue(allObjects
                .get(allObjects.size() - 1), rdi.getSortColumnObject(), displayUtils, startValue);

            
            ToolTipObject tooltipObj = new ToolTipObject(
                allObjects.get(allObjects.size() - 1).getId(), start,
                start, allObjects.size() - 1);
            sliderList.add(tooltipObj);
        }  
        } catch (VocollectException e) {
            log.warn("Failure creating tooltip objects" + e.getMessage());
        }

        return sliderList;
    }

    /**
     * Convenience method to get the property from a bean.
     * @param dataObject - object to retreive
     * @param rdi - the ResultDataInfo
     * @param displayUtils the DisplayUtilities object.
     * @return the property, possibly translated.
     */
    private static Object getProperty(DataObject dataObject,
                                      ResultDataInfo rdi,
                                      DisplayUtilities displayUtils) {
        Object obj;
        try {
            obj = PropertyUtils.getNestedProperty(dataObject, rdi
                .getTooltipColumn());
        } catch (Exception e) {
            if (e instanceof NestedNullException
                || e.getCause() instanceof NullPointerException) {
                return JSONResponseBuilder.NULL_PROPERTY_VALUE;
            }
            log.error(
                "Error accessing tooltip property " + rdi.getTooltipColumn(),
                SystemErrorCode.CONFIGURATION_ERROR, e);
            throw new RuntimeException("Error accessing tooltip property");
        }

        // return an empty string in event of a null
        return obj;
    }

    /**
     * Returns the index of the object found when the id parameter matches the
     * objects id.
     * @param objects list of objects to search
     * @param id value to search for
     * @return index of the object that matches the value or ITEM_NOT_FOUND if
     *         no objects match
     */
    private static int findById(List<DataObject> objects, long id) {
        int index = 0;
        for (DataObject dataObject : objects) {
            if (dataObject.getId() == id) {
                return index;
            }
            index++;
        }
        return ITEM_NOT_FOUND;
    }

    /**
     * Returns all objects for set filter. Used to determine what values in
     * selection are still valid.
     * @param dataClass Spring bean to call method on
     * @param getMethod Method to call through reflection
     * @param filter Filter containing current selection values.
     * @param sortField the field to sort the results by
     * @param sortAscending the direction for the sort
     * @return DataObjects for set filter.
     */
    public static List<DataObject> checkSelectionData(Object dataClass,
                                                      String getMethod,
                                                      Filter filter,
                                                      String sortField,
                                                      boolean sortAscending) {
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setSortColumn(sortField);
        rdi.setSortAscending(sortAscending);
        rdi.setFilter(filter);
        return callGetDataMethod(rdi, dataClass, getMethod);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 