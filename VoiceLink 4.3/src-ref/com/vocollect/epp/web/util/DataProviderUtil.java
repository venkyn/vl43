/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.action.DataProviderAction;

import java.util.List;

/**
 * Utility interface to abstract the Business logic of the Data Provider 
 * implementation.
 *
 * @author dkertis
 */
public interface DataProviderUtil {
    /**
     * @param rdi Information about the request.
     * @param dataClass Spring bean to call method on
     * @param getMethod Method to call through reflection
     * @param currentUser User making the request
     * @param columns Columns to render
     * @param currentTime timestamp of the request
     * @param action Request DataProviderAction class
     * @param useNullData if true, just return JSON representing a null
     * data set with the specified parameters, don't actually get the data.
     * @return The JSON response in String format
     */
    public String getJSONData(ResultDataInfo rdi,
                                     Object dataClass,
                                     String getMethod,
                                     User currentUser,
                                     List<Column> columns,
                                     String currentTime,
                                     DataProviderAction action,
                                     boolean useNullData);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 