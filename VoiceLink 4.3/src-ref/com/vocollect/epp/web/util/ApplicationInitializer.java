/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.util;

import com.vocollect.epp.licensing.LicenseErrorCode;
import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemLicense;
import com.vocollect.epp.security.RoleFeatureChangeListener;
import com.vocollect.epp.service.LicenseManager;
import com.vocollect.epp.service.impl.LicenseManagerImpl;
import com.vocollect.epp.util.ApplicationContextHolder;
import com.vocollect.epp.web.licensing.enforcement.Licensers;

import java.nio.charset.Charset;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


/**
 * Bean that does post <code>BeanFactory</code> creation initialization
 * actions for the application.
 *
 * @author ddoubleday
 */
public class ApplicationInitializer implements ApplicationContextAware {

    private static Logger log = new Logger(ApplicationInitializer.class);

    // Avoid multiple initializations.
    private boolean isInitialized = false;
    
    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext ctx)
        throws BeansException {
        // Put the context in the static holder object.
        ApplicationContextHolder.setApplicationContext(ctx);
        if (!this.isInitialized) {
            initialize();
            this.isInitialized = true;
        }
        
    }

    /**
     * Do the initialization actions for the app. This will be called
     * automatically when the application context is set by Spring.
     */
    protected void initialize() {
        initializeLicense();
        startRoleFeatureChangeListener();        
    }
    
    /**
     * Initializes Licensers.
     */
    private void initializeLicense() {
        LicenseManager licenseManager = (LicenseManager) 
            ApplicationContextHolder.getApplicationContext()
                .getBean(LicenseManagerImpl.BEAN_NAME);

        try {
            SystemLicense sysLicense = licenseManager.getCurrentLicense();

            if (sysLicense != null) {
                VoiceLicense currentLicense = new VoiceLicense(sysLicense
                    .getContents().getBytes(Charset.forName("utf-8")));

                //Initialize the licensers
                Licensers.init(currentLicense);
                
                // Then set the current license for this process
                VoiceLicense.setCurrentLicense(currentLicense);
            }
        } catch (Exception e) {
            log.error("Problem while initializing license!", 
                LicenseErrorCode.CURRENT_LICENSE_FAILURE, e);
        }
    }

    /**
     * Starts the role feature chagnge listener.
     */
    private void startRoleFeatureChangeListener() {
        RoleFeatureChangeListener roleFeatureChangeListener = 
            (RoleFeatureChangeListener) 
                ApplicationContextHolder.getApplicationContext()
                    .getBean(RoleFeatureChangeListener.BEAN_NAME);

        roleFeatureChangeListener.start();
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 