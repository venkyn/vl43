/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;

import java.util.List;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * A Web Service interface that handles User inforamtion.
 *
 * @author ddoubleday
 */
@WebService
public interface UserWebService {

    /**
     * Sample web service that returns the list of the names of
     * all users of the system.
     * @return the list of names.
     * @throws DataAccessException on failure to retrieve data.
     */
    public List<String> getUserNames() throws DataAccessException;

    /**
     * @return the count of users of the system.
     * @throws DataAccessException on failure to retrieve data
     */
    public int getUserCount() throws DataAccessException;

    /**
     * Create a user from the specified data and return the ID of the new
     * user.
     * @param name the user name
     * @param roleNames the roles for the new user
     * @param emailAddress new user's email address.
     * @return the ID of the new user.
     * @throws BusinessRuleException on failure to create for business reasons.
     * @throws DataAccessException on failure to create due to database failure.
     */
    public long createUser(@WebParam(name = "name")         String       name,
                           @WebParam(name = "roleNames")    List<String> roleNames,
                           @WebParam(name = "emailAddress") String       emailAddress)
        throws DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 