/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.web.service.NotificationWebService;

import java.text.ParseException;
import java.util.Map;

import javax.jws.WebService;

/**
 * The implementation of the NotificationWebService.
 *
 * @author ??
 */
@WebService(endpointInterface = "com.vocollect.epp.web.service.NotificationWebService")
public class NotificationWebServiceImpl implements NotificationWebService {

    private NotificationManager notificationManager;

    /**
     * @return the notification manager.
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * @param notificationManager Spring-injected notification manager.
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.NotificationWebService#createdNotificationEvent(java.lang.Long)
     */
    public void createdNotificationEvent(Long notificationId) {
        // TODO : Not implemented yet.
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.NotificationWebService#getNotificationInfo(java.lang.Long, java.lang.String)
     */
    public Map<String, Object> getNotificationInfo(Long notificationId, String locale) {
        try {
            return notificationManager.getNotificationMap(notificationId);
        } catch (DataAccessException e) {
            // TODO: Bug! The error must be logged, and something other
            // than null should be returned. --ddoubleday
            e.printStackTrace();
            return null;
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.NotificationWebService#acknowledgeNotification(java.lang.Long, java.lang.String, java.lang.String)
     */
    public String acknowledgeNotification(Long notificationId,
                                          String acknowledgedUser,
                                          String acknowledgeDate) {

        String ack = "";
        try {
            ack = notificationManager.updateAck(
                notificationId, acknowledgedUser, acknowledgeDate);

        } catch (DataAccessException e) {

            e.printStackTrace();
        } catch (BusinessRuleException e) {

            e.printStackTrace();
        } catch (ParseException e) {

            e.printStackTrace();
        }
        return ack;

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 