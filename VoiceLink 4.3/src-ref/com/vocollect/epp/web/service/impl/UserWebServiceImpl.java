/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.web.service.UserWebService;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;


/**
 * Implements the UserService web service.
 *
 * @author ddoubleday
 */
@WebService(endpointInterface = "com.vocollect.epp.web.service.UserWebService")
public class UserWebServiceImpl implements UserWebService {

    private UserManager userManager;
    private RoleManager roleManager;


    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }


    /**
     * Setter for the roleManager property.
     * @param roleManager the new roleManager value
     */
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.UserWebService#getUserNames()
     */
    public List<String> getUserNames() throws DataAccessException {
        List<User> users = this.userManager.getAll();
        List<String> userNames = new ArrayList<String>();
        for (User user : users) {
            userNames.add(user.getName());
        }

        return userNames;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.UserWebService#getUserCount()
     */
    public int getUserCount() throws DataAccessException {
        return getUserNames().size();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.UserWebService#createUser(java.lang.String, java.util.List, java.lang.String)
     */
    public long createUser(String name, List<String> roleNames, String emailAddress)
        throws DataAccessException, BusinessRuleException {
        User newUser = new User();
        newUser.setName(name);
        newUser.setEmailAddress(emailAddress);
        for (String roleName : roleNames) {
            Role role = this.roleManager.findByName(roleName);
            newUser.addRole(role);
        }

        this.userManager.save(newUser);

        return newUser.getId();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 