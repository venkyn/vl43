/*
 * Copyright (c) 2009 Vocollect, Inc.
 * Pittsburgh, PA 15235
 * All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.web.service.SiteWebService;
import com.vocollect.epp.web.service.models.Site;

import java.util.ArrayList;
import java.util.List;

import javax.xml.ws.WebServiceException;

/**
 * Device web service implementation.
 * @author treed
 *
 */
public class SiteWebServiceImpl implements SiteWebService {
	private SiteManager siteManager;
	
	public SiteManager getSiteManager() {
		return siteManager;
	}

	public void setSiteManager(SiteManager siteManager) {
		this.siteManager = siteManager;
	}

	/**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.SiteWebService#fetchAllSites()
     */
    public List<Site> fetchAllSites() throws WebServiceException {
       
        List<Site> wsSites = new ArrayList<Site>();
        
        try {
            for (com.vocollect.epp.model.Site s:getSiteManager().getAll()) {
            	Site site = new Site();
            	site.setId(s.getId());
            	site.setName(s.getName());
            	site.setDescription(s.getDescription());
            	site.setTimeZoneId(s.getTimeZone().getID());
            	wsSites.add(site);
            }            
        } catch (DataAccessException e) {
            throw new WebServiceException(e.getMessage());
        }
        
        return wsSites;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 