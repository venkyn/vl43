/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service.impl;


import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.LicenseManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.licensing.enforcement.Licensers;
import com.vocollect.epp.web.service.IntegrityCheckWebService;

import javax.jws.WebService;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * The implementation of the IntegrityCheckWebService.
 *
 * @author emontgomery
 */
@WebService(endpointInterface = "com.vocollect.epp.web.service.IntegrityCheckWebService")
public class IntegrityCheckWebServiceImpl implements IntegrityCheckWebService, ApplicationContextAware {

    private static final Logger log = new Logger(IntegrityCheckWebServiceImpl.class);

    private LicenseManager licenseManager;

    private ApplicationContext ctx;

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext context)
        throws BeansException {
        this.ctx = context;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.IntegrityCheckWebService#obtainLicense(java.lang.String, java.lang.String)
     */
    public boolean integrityAssert(LicenseCheckInfo licenseCheckInfo) {

        //Default to All Site mode if current site information is missing
        setDefaultSite();

        // Use Licensers' checklease method which will iterate through
        // the checkLeases methods of the Licenser objects in memory
        // (VoiceLinkLicenser, VoiceConsoleLicenser,etc)
        return Licensers.checkLease(licenseCheckInfo.getProperties());
    }

    /**
     * Method to default to All Site mode if the current site information is not
     * available in the SiteContext
     */
    private void setDefaultSite() {
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        if (siteContext == null) {
            // Get a new instance of SiteContext bean.
            siteContext = (SiteContext) ctx.getBean("siteContext");
        }

        if (siteContext.getCurrentSite() == null) {
            siteContext.setToAllSiteMode();
            log.info("No currentSite set. Defaulting to All Site mode");
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.service.IntegrityCheckWebService#yieldLicense(java.lang.String, java.lang.String)
     */
    public boolean integrityRelax(LicenseCheckInfo licenseCheckInfo) {
        boolean results = Licensers.yieldLease(licenseCheckInfo.getProperties());
        if (log.isDebugEnabled()) {
            log.debug("Yield lease, result: " + results);
        }
        return results;
    }

    /**
     * @return the license manager
     */
    public LicenseManager getLicenseManager() {
        return licenseManager;
    }

    /**
     * @param licenseManager the manager
     */
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 