/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service.impl;

import java.util.Map;


/**
 * This is a wrapper class that abstracts the information needed to
 * do a license check. In the short term, it works around an issue
 * in JAXB with passing top level Map objects through JAX-WS. In the 
 * longer term, it allows for different kinds of licensing data.
 *
 * @author ddoubleday
 */
public class LicenseCheckInfo {

    private Map<String, String> properties;

    
    /**
     * Getter for the properties property.
     * @return the value of the property
     */
    public Map<String, String> getProperties() {
        return this.properties;
    }

    
    /**
     * Setter for the properties property.
     * @param properties the new properties value
     */
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 