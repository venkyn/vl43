/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service.impl;

import java.util.Map;

import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;


/**
 * A convenience class for creating a CXF Service Client Proxy object.
 * 
 * @param <T> the generic service interface parameter. 
 *
 * @author ddoubleday
 */
public class CXFServiceProxyCreator<T> {

    /**
     * Create a proxy client for the service with the specified parameters.
     * @param serviceInterface the interface class.
     * @param serviceURI the service URI (relative to the directory that the
     * CXFServlet maps services to.)
     * @return a proxy client of the specified service interface.
     */
    public T createProxy(Class<T> serviceInterface,
                         String   serviceURI) {
        return createProxy(serviceInterface, serviceURI, null);
    }

    /**
     * Create a proxy client for the service with the specified parameters.
     * @param serviceInterface the interface class.
     * @param serviceURI the service URI (relative to the directory that the
     * CXFServlet maps services to.)
     * @param properties additional properties (may be null)
     * @return a proxy client of the specified service interface.
     */
    public T createProxy(Class<T>            serviceInterface,
                         String              serviceURI,
                         Map<String, Object> properties) {
        JaxWsProxyFactoryBean fb = new JaxWsProxyFactoryBean();
        fb.setServiceClass(serviceInterface);
        fb.setAddress(serviceURI);
        if (properties != null) {
            fb.setProperties(properties);
        }

        return (T) fb.create();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 