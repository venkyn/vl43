/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;


/**
 * A WebService interface used to perform integrity checking on terminal requests
 * that will pass through the VoiceLink business layer.
 *
 * @author emontgomery
 * @author daich
 * @author ddoubleday
 */

import com.vocollect.epp.web.service.impl.LicenseCheckInfo;

import javax.jws.WebService;


/**
 * Integrity validation web service.
 */
@WebService
public interface IntegrityCheckWebService {
    
    /**
     * The transaction name extracted from client requests on the proxy side.  
     */
    String TRANSACTION_ID = "transactionId";

    /**
     * The serial number of the terminal of interest.
     */
    String TERMINAL_SERIAL_NUMBER = "terminalSerialNumber";

    /**
     * The id of the operator using the terminal.
     * 
     */
    String OPERATOR_ID = "operatorId";

  
    /**
     * Relaxes the integrity of the request.
     *
     * @param integrityCheckInfo - info about the request.
     * @return boolean - true if successful; false otherwise.
     */
    public boolean integrityRelax(LicenseCheckInfo integrityCheckInfo);

    
    /**
     * Asserts the integrity of the request.
     *
     * @param integrityCheckInfo - info about the request.
     * @return boolean - true if successful; false otherwise.
     */
    public boolean integrityAssert(LicenseCheckInfo integrityCheckInfo); 

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 