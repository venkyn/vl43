/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import java.util.Map;

import javax.jws.WebService;

/**
* A Web Service interface for Notification.
*/
@WebService
public interface NotificationWebService {

    /**
     * @param notificationId the ID of the notification
     */
    public void createdNotificationEvent(Long notificationId);

    /**
     * Get information about a notification.
     * @param notificationId the ID of the notification.
     * @param locale the String rep of the Locale that we want the info for.
     * @return a Map containing the information.
     */
    public Map<String, Object> getNotificationInfo(Long notificationId, String locale);

    /**
     * Acknowledge a notification event.
     * @param notificationId the ID of the notification.
     * @param acknowledgedUser the name of the user acknowledging
     * @param acknowledgeDate the String rep of the date of the acknowledgement.
     * @return an acknowledgment message.
     */
    public String acknowledgeNotification(Long notificationId,
                                          String acknowledgedUser,
                                          String acknowledgeDate);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 