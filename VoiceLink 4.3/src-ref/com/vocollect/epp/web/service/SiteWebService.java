/*
 * Copyright (c) 2006 Vocollect, Inc.
 * Pittsburgh, PA 15235
 * All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import com.vocollect.epp.web.service.models.Site;

/**
 * A Web Service interface that handles Site information.
 *
 * @author bnichols
 */
@WebService
public interface SiteWebService {

    /**
     * Web service that returns the list of the sites in the system.
     * @return the list of sites.
     * @throws WebServiceException in the event of a failure.
     */
    public List<Site> fetchAllSites() throws WebServiceException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 