/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.listener;

import com.vocollect.epp.dao.hibernate.HibernateStatsReporter;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ApplicationContextHolder;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Listener to capture the start and stop event of the application.
 * This will record Hibernate statistics, if enabled.
 *
 * @author ddoubleday
 */
public class ApplicationStateListener implements ServletContextListener {

    private static Logger log = new Logger(ApplicationStateListener.class);

    /**
     * Captures the Application Shutdown event. {@inheritDoc}
     * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent event) {
        dumpHibernateStats(event);
        logEvent(event, "stopped");
    }

    /**
     * Captures the Application Startup event. {@inheritDoc}
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event) {
        logEvent(event, "started");

    }

    /**
     * @param event the life cycle event
     * @param eventType description of the event.
     */
    private void logEvent(ServletContextEvent event,
                          String              eventType) {
        String applicationName = event.getServletContext()
            .getServletContextName();
        if (applicationName == null) {
            applicationName = "Unknown";
        }

        log.info("Application " + applicationName + ": " + eventType);
    }

    /**
     * Dump out Hibernate statistics, if enabled.
     * @param event shut down event.
     */
    private void dumpHibernateStats(ServletContextEvent event) {

        HibernateStatsReporter statsReporter = (HibernateStatsReporter)
            ApplicationContextHolder.getApplicationContext().
                getBean(HibernateStatsReporter.HIBERNATE_STATS_BEAN_NAME);
        if (statsReporter.getStatsEnabled()) {
            statsReporter.reportStats();
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 