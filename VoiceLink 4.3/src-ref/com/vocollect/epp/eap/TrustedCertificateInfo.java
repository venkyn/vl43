/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.eap;

/**
 * 
 * @author brianrupert
 */
public class TrustedCertificateInfo {
    private String subject;
    private String md5FingerPrint;
    private String sha1FingerPrint;
    
    /**
     * Constructor.
     * @param subject the certificate subject
     * @param md5FingerPrint MD5 certificate fingerprint
     * @param sha1FingerPrint SHA-1 certificate fingerprint
     */
    public TrustedCertificateInfo(String subject,
                                  String md5FingerPrint,
                                  String sha1FingerPrint) {
        this.subject = subject;
        this.md5FingerPrint = md5FingerPrint;
        this.sha1FingerPrint = sha1FingerPrint;        
    }
    
    /**
     * @return the MD5 certificate fingerpring
     */
    public String getMd5FingerPrint() {
        return md5FingerPrint;
    }
    
    /**
     * @return the SHA-1 certificate fingerprint
     */
    public String getSha1FingerPrint() {
        return sha1FingerPrint;
    }
    
    /**
     * @return the certificate subject
     */
    public String getSubject() {
        return subject;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (o != null && o instanceof TrustedCertificateInfo) {
            TrustedCertificateInfo rhs = (TrustedCertificateInfo) o;
            return md5FingerPrint.equals(rhs.getMd5FingerPrint())
                && sha1FingerPrint.equals(rhs.getSha1FingerPrint());
        }
        
        return false;
    }
      
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return this.md5FingerPrint.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        
        sb.append(subject);
        sb.append("\n MD5: ");
        sb.append(md5FingerPrint);
        sb.append("\n SHA1: ");
        sb.append(sha1FingerPrint);
        sb.append("\n");
        
        return sb.toString();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 