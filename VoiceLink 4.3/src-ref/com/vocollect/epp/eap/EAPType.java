/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.eap;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 *
 *
 * @author ???
 */
public enum EAPType implements ValueBasedEnum<EAPType> {
    EAPTLS(0, "TLS"),
    EAPTTLS(1, "TTLS"),
    PEAPV0(2, "PEAPv0"),
    PEAPV1(3, "PEAPv1"),
    LEAP(4, "LEAP"),
    NONE(-1, "");

    // Internal map of int values to enum values.
    private static ReverseEnumMap<EAPType> toValueMap = new ReverseEnumMap<EAPType>(
        EAPType.class);

    private int value;

    private String name;

    /**
     * Constructor.
     * @param value the associated int rep.
     * @param name the string associated with the type
     */
    private EAPType(int value, String name) {
        this.value = value;
        this.name = name;
    }

    /**
     * method to get name associated with the enum value.
     * @return - String for the name
     */
    public String getName() {
        return name;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public EAPType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.PickType#fromValue(Integer)
     */
    public EAPType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static EAPType toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 