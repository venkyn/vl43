/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.eap;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 *
 *
 * @author ???
 */
public enum EAPAssociation implements ValueBasedEnum<EAPAssociation> {
    SITE(0, CredentialType.SITE),
    DEVICE(1, CredentialType.DEVICE),
    OPERATOR(2, CredentialType.USER);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<EAPAssociation> toValueMap =
        new ReverseEnumMap<EAPAssociation>(EAPAssociation.class);

    private int value;
    private CredentialType credentialType;

    /**
     * Constructor.
     * @param value the associated int rep.
     * @param credentialType the credential type
     */
    private EAPAssociation(int value, CredentialType credentialType) {
        this.value = value;
        this.credentialType = credentialType;
    }

    /**
     * @return the credentialType
     */
    public CredentialType getCredentialType() {
        return credentialType;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public EAPAssociation fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.PickType#fromValue(Integer)
     */
    public EAPAssociation fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static EAPAssociation toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 