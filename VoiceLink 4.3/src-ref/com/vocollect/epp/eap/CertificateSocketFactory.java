/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.eap;

import com.vocollect.epp.model.NetworkCredential;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;

import org.apache.commons.ssl.KeyMaterial;
import org.apache.commons.ssl.SSLClient;
import org.apache.commons.ssl.TrustMaterial;
import org.apache.log4j.Logger;

/**
 * 
 *
 * @author
 */
public class CertificateSocketFactory extends SSLSocketFactory {
    
    private SSLClient delegate;

    private static final Logger log = Logger.getLogger(CertificateSocketFactory.class);
    private static Semaphore mutex = new Semaphore(1);
    private static NetworkCredential credential;

    private static List<NetworkCredential> trustMaterial;
    
    /**
     * Constructor.
     * @param credential .
     * @param trustMaterialToAdd .
     * @throws GeneralSecurityException
     * @throws IOException
     */
    protected CertificateSocketFactory(NetworkCredential credential, List<NetworkCredential> trustMaterialToAdd) 
        throws GeneralSecurityException, IOException {
        delegate = new SSLClient();
        
        // Add the usual suspects
        delegate.addTrustMaterial(TrustMaterial.CACERTS);
        
        // Add user defined trust entries
        for (NetworkCredential thisCred : trustMaterial) {
            delegate.addTrustMaterial(new TrustMaterial(thisCred.getCertificateFileContents()));
        }
        
        if (credential != null) {
            // Add the actual user to identify yourself as
            delegate.setKeyMaterial(new KeyMaterial(
                    credential.getCertificateFileContents(),
                    credential.getCertificateKeyFileContents(), 
                    credential.getCertificatePassword().toCharArray()
            ));
        }
    }
    
    /**
     * Only one thread should be using this CertificateSocketFactory at a time.
     * This will ensure only one thread touches this class.  
     * Call accept before the setInfo and after the getDefault is used.
     *
     */
    public static void acquire() {
        try {
            mutex.acquire();
        } catch (InterruptedException e) {
            log.error(e);
        }
    }
    
    /**
     * 
     */
    public static void release() {
        mutex.release();
    }
    
    /**
     * @param setCredential .
     * @param trustMaterialToAdd .
     */
    public static void setInfo(NetworkCredential setCredential, List<NetworkCredential> trustMaterialToAdd) {
        credential = setCredential;
        trustMaterial = trustMaterialToAdd;
    }
    
    /**
     * @return .
     */
    public static SocketFactory getDefault() {
        try {
            return new CertificateSocketFactory(credential, trustMaterial);
        } catch (GeneralSecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

    /**
     * {@inheritDoc}
     * @see javax.net.ssl.SSLSocketFactory#createSocket(java.net.Socket, java.lang.String, int, boolean)
     */
    public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
        return delegate.createSocket(s, host, port, autoClose);
    }

    /**
     * {@inheritDoc}
     * @see javax.net.ssl.SSLSocketFactory#getDefaultCipherSuites()
     */
    public String[] getDefaultCipherSuites() {
        return delegate.getDefaultCipherSuites();
    }

    /**
     * {@inheritDoc}
     * @see javax.net.ssl.SSLSocketFactory#getSupportedCipherSuites()
     */
    public String[] getSupportedCipherSuites() {
        return delegate.getSupportedCipherSuites();
    }

    /**
     * {@inheritDoc}
     * @see javax.net.SocketFactory#createSocket(java.lang.String, int)
     */
    public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
        return delegate.createSocket(host, port);
    }

    /**
     * {@inheritDoc}
     * @see javax.net.SocketFactory#createSocket(java.net.InetAddress, int)
     */
    public Socket createSocket(InetAddress host, int port) throws IOException {
        return delegate.createSocket(host, port);
    }

    /**
     * {@inheritDoc}
     * @see javax.net.SocketFactory#createSocket(java.lang.String, int, java.net.InetAddress, int)
     */
    public Socket createSocket(String host, int port, InetAddress localhost, int localport) 
        throws IOException, UnknownHostException {
        return delegate.createSocket(host, port, localhost, localport);
    }

    /**
     * {@inheritDoc}
     * @see javax.net.SocketFactory#createSocket(java.net.InetAddress, int, java.net.InetAddress, int)
     */
    public Socket createSocket(InetAddress host, int port, InetAddress localhost, int localport) throws IOException {
        return delegate.createSocket(host, port, localhost, localport);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 