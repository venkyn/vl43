/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.util;

import com.vocollect.epp.dataaggregator.model.DAFieldType;

import java.util.ArrayList;
import java.util.List;

/**
 * @author smittal
 * 
 */
public final class DAUtils {

    /**
     * Private Constructor.
     */
    private DAUtils() {

    }

    /**
     * . Utility method to get all field types
     * @return List of all DAFieldTypes
     */
    public static List<DAFieldType> getAllFieldTypes() {
        List<DAFieldType> daFieldTypes = new ArrayList<DAFieldType>();

        DAFieldType[] fieldTypes = DAFieldType.values();
        for (DAFieldType daFieldType : fieldTypes) {
            daFieldTypes.add(daFieldType);
        }

        return daFieldTypes;
    }

    /**
     * Utility method to identify field types needed based on usage.
     * @param isCategoryFields is the field types are required or category field
     * @return List of DA field types
     */
    public static List<DAFieldType> getFieldTypes(boolean isCategoryFields) {
        List<DAFieldType> daFieldTypes = new ArrayList<DAFieldType>();
        if (isCategoryFields) {
            daFieldTypes.add(DAFieldType.STRING);
            daFieldTypes.add(DAFieldType.TIME);
        } else {
            daFieldTypes.add(DAFieldType.INTEGER);
            daFieldTypes.add(DAFieldType.FLOAT);
            daFieldTypes.add(DAFieldType.DATE);
        }
        return daFieldTypes;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 