/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.exceptions.BusinessRuleException;

import java.util.Map;
import java.util.Set;

import org.json.JSONException;

/**
 * 
 * 
 * @author khazra
 */
public interface DataAggregatorHandlerRoot {

    /**
     * Returns all configured data aggregators.
     * @return List of configured concepts
     */
    public Map<String, GenericDataAggregator> getAll();

    /**
     * @param name the name of the aggregator to return
     * @return The data aggregator referred by name
     */
    public GenericDataAggregator getDataAggregatorByName(String name);

    /**
     * @param daName Data aggregator name
     * @param theDataAggregator the data aggregator
     * @throws JSONException when problem parsing json data from aggregator
     * @throws DataAccessException Accessing database
     * @throws BusinessRuleException and problem setting object
     */
    public void persistDA(String daName, GenericDataAggregator theDataAggregator)
        throws JSONException, BusinessRuleException, DataAccessException;
    
    /**
     * @param theDataAggregator the data aggregator
     * @param daInformation the daInformation
     * @throws JSONException when problem parsing json data from aggregator
     * @throws DataAccessException Accessing database
     * @throws BusinessRuleException and problem setting object
     */
    public void updateDA(GenericDataAggregator theDataAggregator, DAInformation daInformation)
        throws JSONException, BusinessRuleException, DataAccessException;

    /**
     * @param daNames the data aggregator names
     */
    public void deleteDA(Set<String> daNames);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 