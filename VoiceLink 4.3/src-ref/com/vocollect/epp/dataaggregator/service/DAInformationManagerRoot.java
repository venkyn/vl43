/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service;

import com.vocollect.epp.dataaggregator.dao.DAInformationDAO;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

/**
 * 
 *
 * @author khazra
 */
public interface DAInformationManagerRoot extends
    GenericManager<DAInformation, DAInformationDAO>, RemovableDataProvider {
    
    /**
     * @param name The name of the Data aggregator
     * @return The data aggregator information
     */
    public DAInformation findByName(String name);

    /**
     * Method to delete data aggregators which have been markedForDelete.  
     */
    public void updateDeleteMarked();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 