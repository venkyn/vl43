/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service.impl;

import com.vocollect.epp.dataaggregator.dao.DAInformationDAO;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;

/**
 * 
 *
 * @author khazra
 */
public abstract class DAInformationManagerImplRoot extends
    GenericManagerImpl<DAInformation, DAInformationDAO> implements
    DAInformationManager {

    /**
     * Constructor.
     * @param primaryDAO The Data Aggregator Information DAO
     */
    public DAInformationManagerImplRoot(DAInformationDAO primaryDAO) {
        super(primaryDAO);
    }

    @Override
    public DAInformation findByName(String name) {
        return this.getPrimaryDAO().findByName(name);
    }

   
    /* (non-Javadoc)
     * @see com.vocollect.epp.dataaggregator.service.DAInformationManagerRoot#deleteMarked()
     */
    @Override
    public void updateDeleteMarked() {
        this.getPrimaryDAO().updateDeleteMarked();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 