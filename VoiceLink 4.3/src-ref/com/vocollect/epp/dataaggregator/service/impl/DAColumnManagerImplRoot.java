/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service.impl;

import com.vocollect.epp.dataaggregator.dao.DAColumnDAO;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;

import java.util.List;

/**
 * @author mraj
 * 
 */
public abstract class DAColumnManagerImplRoot extends
    GenericManagerImpl<DAColumn, DAColumnDAO> implements DAColumnManager {

    /**
     * Constructor.
     * @param primaryDAO The data aggregator column DAO
     */
    public DAColumnManagerImplRoot(DAColumnDAO primaryDAO) {
        super(primaryDAO);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.dataaggregator.service.DAColumnManagerRoot#deleteByStatus
     * ()
     */
    @Override
    public void updateDeleteByState(DAColumnState state) {
        this.getPrimaryDAO().updateDeleteByState(state);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.dataaggregator.service.DAColumnManagerRoot#
     * updateColumnState(com.vocollect.epp.dataaggregator.model.DAColumnState)
     */
    @Override
    public void updateColumnState(DAColumnState state) {
        this.getPrimaryDAO().updateColumnState(state);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.service.DAColumnManagerRoot#listDAColumnByFieldType(com.vocollect.epp.dataaggregator.model.DAInformation,
     *      java.util.List)
     */
    public List<DAColumn> listDAColumnByFieldType(DAInformation daInformation,
                                                  List<DAFieldType> daFieldTypes) {
        return this.getPrimaryDAO().listDAColumnByFieldType(daInformation,
            daFieldTypes);
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.service.DAColumnManagerRoot#listSequencedDAColumnByFieldType(com.vocollect.epp.dataaggregator.model.DAInformation,
     *      java.util.List)
     */
    public List<DAColumn> listSequencedDAColumnByFieldType(DAInformation daInformation,
                                                           List<DAFieldType> daFieldTypes) {
        return this.getPrimaryDAO().listSequencedDAColumnByFieldType(
            daInformation, daFieldTypes);
    }
    
    /**
     * Method to find the DA column matching da and field Id.
     * @param daInformation - the DA Information
     * @param fieldId - the field ID
     * @return DAcolumn
     */
    public DAColumn findDAColumnByFieldId(DAInformation daInformation, String fieldId) { 
        return this.getPrimaryDAO().findDAColumnByFieldId(daInformation,
            fieldId);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 