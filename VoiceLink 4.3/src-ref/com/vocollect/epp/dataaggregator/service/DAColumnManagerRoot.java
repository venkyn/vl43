/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service;

import com.vocollect.epp.dataaggregator.dao.DAColumnDAO;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

import java.util.List;

/**
 * @author mraj
 *
 */
public interface DAColumnManagerRoot extends
    GenericManager<DAColumn, DAColumnDAO>, RemovableDataProvider {

    /**
     * Method to delete the columns having passed state.
     * @param state - the state of the column
     */
    public void updateDeleteByState(DAColumnState state);
    
    /**
     * Method to update the state the columns to the passed state.
     * @param state - the state of the column
     */
    public void updateColumnState(DAColumnState state);
    
    /**
     * Method to retrieve list of DAColumn based on DAInformation 
     * and DAFieldType.  sorted by fieldId
     *
     * @param daInformation DAInformation.
     * @param daFieldTypes List<DAFieldType>
     * @return List<DAColumn>
     */
    public List<DAColumn> listDAColumnByFieldType(DAInformation daInformation,
                                                  List<DAFieldType> daFieldTypes); 

    /**
     * Method to retrieve list of DAColumn based on DAInformation 
     * and DAFieldType. sorted on the basis of columnSequence number.
     *
     * @param daInformation DAInformation.
     * @param daFieldTypes List<DAFieldType>
     * @return List<DAColumn>
     */
    public List<DAColumn> listSequencedDAColumnByFieldType(DAInformation daInformation,
                                                  List<DAFieldType> daFieldTypes);   
    
    /**
     * Method to find the DA column matching da and field Id.
     * @param daInformation - the DA Information
     * @param fieldId - the field ID
     * @return DAcolumn
     */
    public DAColumn findDAColumnByFieldId(DAInformation daInformation, String fieldId);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 