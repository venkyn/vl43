/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.VersionedModelObject;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;

/**
 * 
 * 
 * @author khazra
 */
public class DAColumnRoot extends VersionedModelObject implements DataObject,
    Serializable {

    //
    private static final long serialVersionUID = 7779931983769042698L;

    private DAInformation daInformation;
    
    private String fieldId;

    private DAFieldType fieldType;

    private String displayName;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String daColumnDisplayName;

    private String uom;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String uomDisplayName;

    private Boolean identityColumn;
    
    private Integer columnSequence;
    
    private DAColumnState columnState = DAColumnState.UNCHANGED;

    /**
     * @return the daInformation
     */
    public DAInformation getDaInformation() {
        return daInformation;
    }

    
    /**
     * @param daInformation the daInformation to set
     */
    public void setDaInformation(DAInformation daInformation) {
        this.daInformation = daInformation;
    }

    /**
     * Getter for the fieldId property.
     * @return String value of the property
     */
    public String getFieldId() {
        return fieldId;
    }

    /**
     * Setter for the fieldId property.
     * @param fieldId the new fieldId value
     */
    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    /**
     * Getter for the fieldType property.
     * @return DAFieldType value of the property
     */
    public DAFieldType getFieldType() {
        return fieldType;
    }

    /**
     * Setter for the fieldType property.
     * @param fieldType the new fieldType value
     */
    public void setFieldType(DAFieldType fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * Getter for the displayName property.
     * @return String value of the property
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Setter for the displayName property.
     * @param displayName the new displayName value
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Getter for the daColumnDisplayName property.
     * @return String value of the property
     */
    public String getDaColumnDisplayName() {
        return ResourceUtil.getLocalizedKeyValue(this.getDisplayName());
    }

    /**
     * Getter for the uom property.
     * @return String value of the property
     */
    public String getUom() {
        return uom;
    }

    /**
     * Setter for the uom property.
     * @param uom the new uom value
     */
    public void setUom(String uom) {
        this.uom = uom;
    }

    /**
     * Getter for the uomDisplayName property.
     * @return String value of the property
     */
    public String getUomDisplayName() {
        return ResourceUtil.getLocalizedKeyValue(this.getUom());
    }

    /**
     * Getter for the identityColumn property.
     * @return Boolean value of the property
     */
    public Boolean getIdentityColumn() {
        return identityColumn;
    }

    /**
     * Setter for the identityColumn property.
     * @param identityColumn the new identityColumn value
     */
    public void setIdentityColumn(Boolean identityColumn) {
        this.identityColumn = identityColumn;
    }

    /**
     * Getter for the columnSequence property.
     * @return Integer value of the property
     */
    public Integer getColumnSequence() {
        return columnSequence;
    }


    /**
     * Setter for the columnSequence property.
     * @param columnSequence the new columnSequence value
     */
    public void setColumnSequence(Integer columnSequence) {
        this.columnSequence = columnSequence;
    }


    /**
     * Getter for the columnState property.
     * @return DaColumnState value of the property
     */
    public DAColumnState getColumnState() {
        return columnState;
    }

    /**
     * Setter for the columnState property.
     * @param columnState the new columnState value
     */
    public void setColumnState(DAColumnState columnState) {
        this.columnState = columnState;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DAColumn)) {
            return false;
        }
        final DAColumnRoot other = (DAColumn) obj;
        if (getFieldId() != other.getFieldId()) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return this.fieldId == null ? 0 : this.fieldId.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 