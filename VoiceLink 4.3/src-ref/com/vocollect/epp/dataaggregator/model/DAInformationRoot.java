/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.VersionedModelObject;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * 
 * @author khazra
 */
public class DAInformationRoot extends VersionedModelObject implements
    DataObject, Serializable {
 
    //
    private static final long serialVersionUID = 6589280056763010358L;

    private String name;

    private String displayName;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String daInformationDisplayName;

    private Set<DAColumn> columns;

    private boolean markedForDelete = false;

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the displayName property.
     * @return String value of the property
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Setter for the displayName property.
     * @param displayName the new displayName value
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Getter for the daInformationValue property.
     * @return String value of the property
     */
    public String getDaInformationDisplayName() {
        return ResourceUtil.getLocalizedKeyValue(this.getDisplayName());
    }

    /**
     * Getter for the columns property.
     * @return DAColumn value of the property
     */
    public Set<DAColumn> getColumns() {
        return columns;
    }

    /**
     * Setter for the columns property.
     * @param columns the new columns value
     */
    public void setColumns(Set<DAColumn> columns) {
        this.columns = columns;
    }

    /**
     * @return the markedForDelete
     */
    public boolean isMarkedForDelete() {
        return markedForDelete;
    }

    /**
     * @param markedForDelete the markedForDelete to set
     */
    public void setMarkedForDelete(boolean markedForDelete) {
        this.markedForDelete = markedForDelete;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DAInformation)) {
            return false;
        }
        final DAInformation other = (DAInformation) obj;
        if (getName() != other.getName()) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name == null ? 0 : this.name.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 