/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.impl;

import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.exceptions.BusinessRuleException;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 
 * @author khazra
 */
public abstract class GenericDataAggregatorImplRoot implements GenericDataAggregator {
    
    private JSONObject parameters;

        
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#getParameters()
     */
    @Override
    public JSONObject getParameters() {
        return parameters;
    }

    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#setParameters(org.json.JSONObject)
     */
    @Override
    public void setParameters(JSONObject parameters) {
        this.parameters = parameters;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#getIdentityColumnName()
     */
    @Override
    public String getIdentityColumnName() {
        return new String(this.toString());
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#getAllOutputColumns()
     */
    @Override
    public JSONArray getAllOutputColumns() {
        return new JSONArray();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#execute()
     */
    public abstract JSONArray execute() throws BusinessRuleException;
    
    /**
     * Method to get the JSON object from fields provided.
     * @param fieldId - the field Id
     * @param fieldType - the field type
     * @param displayName - the display name
     * @param uom - the UOM
     * @param columnSequence columnSequence
     * @return the JSON object created
     * @throws JSONException - the exception thrown
     */
    protected JSONObject getfieldJSON(String fieldId,
                                    String fieldType,
                                    String displayName,
                                    String uom,
                                    Integer columnSequence) throws JSONException {
        JSONObject fieldJSON = new JSONObject();
        fieldJSON.put(GenericDataAggregator.FIELD_ID, fieldId);
        fieldJSON.put(GenericDataAggregator.FIELD_TYPE, fieldType);
        fieldJSON.put(GenericDataAggregator.DISPLAY_NAME, displayName);
        fieldJSON.put(GenericDataAggregator.UOM, uom);
        fieldJSON.put(GenericDataAggregator.COLUMN_SEQUENCE, columnSequence);
        return fieldJSON;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#getStartTime(java.util.Date)
     */
    @Override
    public Date getStartTime(Date startTime) throws JSONException {
        JSONObject parameterJSONObject = getParameters();
        
        if (parameterJSONObject.has(START_TIME_PARAMETER)) {
            startTime = (Date) parameterJSONObject.get(START_TIME_PARAMETER);
        }
        
        return startTime;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.GenericDataAggregator#getEndTime(java.util.Date)
     */
    @Override
    public Date getEndTime(Date endTime) throws JSONException {
        JSONObject parameterJSONObject = getParameters();
        
        if (parameterJSONObject.has(END_TIME_PARAMETER)) {
            endTime = (Date) parameterJSONObject.get(END_TIME_PARAMETER);
        }
        
        return endTime;        
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 