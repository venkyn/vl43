/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.event.DAUpdateEvent;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAEventType;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * 
 * 
 * @author khazra
 */
public class DataAggregatorHandlerImplRoot implements DataAggregatorHandler,
    ApplicationContextAware, ApplicationListener<ApplicationContextEvent> {

    private static final Logger log = new Logger(
        DataAggregatorHandlerImplRoot.class);

    private NotificationManager notificationManager;
    
    private ApplicationContext ctx = null;

    private DAInformationManager daInformationManager = null;
    
    private DAColumnManager daColumnManager = null;

    private SiteContext siteContext;

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the daInformationManager property.
     * @return DAInformationManager value of the property
     */
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * Setter for the daInformationManager property.
     * @param daInformationManager the new daInformationManager value
     */
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * @return the daColumnManager
     */
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * @param daColumnManager the daColumnManager to set
     */
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.DataAggregatorHandlerRoot#getAll()
     */
    @Override
    public Map<String, GenericDataAggregator> getAll() {
        Map<String, GenericDataAggregator> conceptBeanMap = ctx
            .getBeansOfType(GenericDataAggregator.class);
        return conceptBeanMap;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.DataAggregatorHandlerRoot#getConceptByName(java.lang.String)
     */
    @Override
    public GenericDataAggregator getDataAggregatorByName(String name) {
        return ctx.getBean(name, GenericDataAggregator.class);
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    @Override
    public void setApplicationContext(ApplicationContext newCtx)
        throws BeansException {
        this.ctx = newCtx;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dataaggregator.DataAggregatorHandlerRoot#persistDA(java.lang.String,
     *      com.vocollect.epp.dataaggregator.GenericDataAggregator)
     */
    public void persistDA(String daName, GenericDataAggregator theDataAggregator)
        throws JSONException, BusinessRuleException, DataAccessException {

        DAInformation daInformation = new DAInformation();
        daInformation.setName(daName);
        daInformation.setDisplayName("com.vocollect.aggregators." + daName);
        
        String identityColumn = theDataAggregator.getIdentityColumnName();
        JSONArray columns = theDataAggregator.getAllOutputColumns();

        Set<DAColumn> daColumns = new HashSet<DAColumn>();
        for (int i = 0; i < columns.length(); i++) {
            JSONObject column = columns.getJSONObject(i);

            String fieldId = column.getString(GenericDataAggregator.FIELD_ID);
            String displayName = column
                .getString(GenericDataAggregator.DISPLAY_NAME);

            String fieldType = column
                .getString(GenericDataAggregator.FIELD_TYPE);
            column.has(GenericDataAggregator.UOM);

            String uom = (column.has(GenericDataAggregator.UOM)) ? column
                .getString(GenericDataAggregator.UOM) : null;
                
            Integer columnSequence = column.getInt(GenericDataAggregator.COLUMN_SEQUENCE);

            DAColumn daColumn = new DAColumn();
            daColumn.setDaInformation(daInformation);
            daColumn.setFieldId(fieldId);
            daColumn.setDisplayName(displayName);
            daColumn.setFieldType(DAFieldType.valueOf(fieldType));
            daColumn.setUom(uom);
            daColumn.setColumnSequence(columnSequence);
            daColumn.setIdentityColumn(fieldId.equals(identityColumn));

            daColumns.add(daColumn);
        }

        daInformation.setColumns(daColumns);
        
        this.daInformationManager.save(daInformation);
    }

    @Override
    public void updateDA(GenericDataAggregator theDataAggregator,
                         DAInformation daInformation) throws JSONException,
        BusinessRuleException, DataAccessException {

        boolean isChanged = false;
        // Create a map of already persisted fields
        Map<String, DAColumn> persistedFieldMap = new HashMap<String, DAColumn>(
            daInformation.getColumns().size());
        for (DAColumn column : daInformation.getColumns()) {
            persistedFieldMap.put(column.getFieldId(), column);

        }

        String identityColumn = theDataAggregator.getIdentityColumnName();
        JSONArray columns = theDataAggregator.getAllOutputColumns();

        Set<String> fieldSet = new HashSet<String>(theDataAggregator
            .getAllOutputColumns().length());

        // Identify the columns added/updated
        for (int i = 0; i < columns.length(); i++) {
            JSONObject column = columns.getJSONObject(i);
            String fieldId = column.getString(GenericDataAggregator.FIELD_ID);
            String displayName = column
                .getString(GenericDataAggregator.DISPLAY_NAME);
            String fieldType = column
                .getString(GenericDataAggregator.FIELD_TYPE);
            String uom = (column.has(GenericDataAggregator.UOM)) ? column
                .getString(GenericDataAggregator.UOM) : null;
            Integer columnSequence = column.getInt(GenericDataAggregator.COLUMN_SEQUENCE);
            DAColumn persistedDAColumn = persistedFieldMap.get(fieldId);

            // Check for new field
            if (persistedDAColumn == null) {
                DAColumn newColumn = new DAColumn();
                newColumn.setFieldId(fieldId);
                newColumn.setDisplayName(displayName);
                newColumn.setFieldType(DAFieldType.valueOf(fieldType));
                newColumn.setUom(uom);
                newColumn.setColumnSequence(columnSequence);
                newColumn.setIdentityColumn(fieldId.equals(identityColumn));
                newColumn.setColumnState(DAColumnState.UNCHANGED);
                newColumn.setDaInformation(daInformation);
                
                daInformation.getColumns().add(newColumn);
            } else {
                // Check the fields for which event has to be published

                String daActionKey = "notification.da.updated";
                String daName = daInformation.getDaInformationDisplayName();
                String processKey = "notification.updateAggregator.title";
                String messageKey = "notification.updateAggregator.notification";

                if (!persistedDAColumn.getColumnSequence().equals(
                    columnSequence)) {
                    persistedDAColumn
                        .setColumnState(DAColumnState.COLUMNSEQUENCEUPDATED);
                    isChanged = true;
                }

                if (!persistedDAColumn.getDisplayName().equals(displayName)) {
                    persistedDAColumn
                        .setColumnState(DAColumnState.DISPLAYNAMEUPDATED);
                    isChanged = true;

                    createNotification(daActionKey, daName,
                        persistedDAColumn.getDaColumnDisplayName(),
                        "notification.aggregator.update.field.displayName",
                        processKey, messageKey,
                        NotificationPriority.INFORMATION);
                }

                if ((StringUtil.isNullOrEmpty(persistedDAColumn.getUom()) != StringUtil
                    .isNullOrEmpty(uom))
                    || (uom != null && !uom.equals(persistedDAColumn.getUom()))) {
                    persistedDAColumn.setColumnState(DAColumnState.UOMUPDATED);
                    isChanged = true;

                    createNotification(daActionKey, daName,
                        persistedDAColumn.getDaColumnDisplayName(),
                        "notification.aggregator.update.field.uom", processKey,
                        messageKey, NotificationPriority.INFORMATION);
                }

                if (!persistedDAColumn.getFieldType().equals(
                    DAFieldType.valueOf(fieldType))) {
                    persistedDAColumn
                        .setColumnState(DAColumnState.FIELDTYPEUPDATED);
                    isChanged = true;
                }

                // update
                persistedDAColumn.setDisplayName(displayName);
                persistedDAColumn.setFieldType(DAFieldType.valueOf(fieldType));
                persistedDAColumn.setUom(uom);
                persistedDAColumn.setColumnSequence(columnSequence);
                persistedDAColumn.setIdentityColumn(fieldId
                    .equals(identityColumn));
            }

            fieldSet.add(fieldId);
        }

        // Remove field if has been removed from updated aggregator
        for (String persistedField : persistedFieldMap.keySet()) {
            if (!fieldSet.contains(persistedField)) {
                DAColumn col = persistedFieldMap.get(persistedField);
                col.setColumnState(DAColumnState.MARKEDFORDELETE);
                isChanged = true;
            }
        }

        this.daInformationManager.save(daInformation);
        log.info("DA " + daInformation.getDisplayName() + " modified");

        // Publish event
        if (isChanged) {
            DAUpdateEvent event = new DAUpdateEvent(DAEventType.UPDATED);
            event.setNewDAInformation(daInformation);
            this.ctx.publishEvent(event);

            log.info("DA update event published");
        }
    }

    @Override
    public void deleteDA(Set<String> daNames) {
        //identify the aggregators removed from the system
        try {
            List<DAInformation> daInfos = this.getDaInformationManager().getAll();
            for (DAInformation daInformation : daInfos) {
                if (!daNames.contains(daInformation.getName())) {
                    daInformation.setMarkedForDelete(true);
                    this.getDaInformationManager().save(daInformation);
                    
                    // Publish event
                    DAUpdateEvent event = new DAUpdateEvent(DAEventType.DELETED);
                    event.setNewDAInformation(daInformation);
                    this.ctx.publishEvent(event);

                    log.info("DA " + daInformation.getName()
                        + " marked for delete. Event published");
                }
            }
        } catch (DataAccessException e1) {
            log.warn("Error fetching DA Info. Removed aggregators, if any, won't be removed from database");
        } catch (BusinessRuleException e) {
            log.warn("Error persisting DA Info");
        }
        
    }
    
    @Override
    public void onApplicationEvent(ApplicationContextEvent event) {
        //Do only when context restarted
        if (!(event instanceof ContextRefreshedEvent)) {
            return;
        }
        
        cleanup();

        Map<String, GenericDataAggregator> daMap = getAll();

        Set<String> daNames = daMap.keySet();

        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());
        
        deleteDA(daNames);
        
        for (String daName : daNames) {
            DAInformation daInformation = this.getDaInformationManager()
                .findByName(daName);
            try {

                if (daInformation == null) {
                    persistDA(daName, daMap.get(daName));
                } else {                    
                    updateDA(daMap.get(daName), daInformation);
                }
            } catch (JSONException e) {
                log.warn("Error occured processing " + daName);
                e.printStackTrace();
            } catch (BusinessRuleException e) {
                log.warn("Error occured processing " + daName);
                e.printStackTrace();
            } catch (DataAccessException e) {
                log.warn("Error occured processing " + daName);
                e.printStackTrace();
            }
        }
    }

    /**
     * . clean up the system of data aggregators and their columns marked for
     * delete at every server start up
     */
    private void cleanup() {
        this.daInformationManager.updateDeleteMarked();
        this.daColumnManager.updateDeleteByState(DAColumnState.MARKEDFORDELETE);
        this.daColumnManager.updateColumnState(DAColumnState.UNCHANGED);
    }


    /**.
     * method to create notifications
     * @param daActionKey type of action that happened on da
     * @param daName name of da
     * @param fieldName name of field
     * @param updateTypeKey affect on field
     * @param processKey process key
     * @param messageKey message key
     * @param priority notification priority
     */
    private void createNotification(String daActionKey,
                                    String daName,
                                    String fieldName,
                                    String updateTypeKey,
                                    String processKey,
                                    String messageKey,
                                    NotificationPriority priority) {
        LOPArrayList lop = new LOPArrayList();

        lop.add(daActionKey, daName);
        lop.add("notification.da.field.name", fieldName);
        lop.add("notification.updateAggregator.field.update", updateTypeKey);
            
        try {
            getNotificationManager().createNotification(processKey, messageKey,
                priority, new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getLocalizedMessage());
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 