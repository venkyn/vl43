/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator;

import com.vocollect.epp.exceptions.BusinessRuleException;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 
 * @author khazra
 */
public interface GenericDataAggregatorRoot {

    // Key that are required for column definition.
    public static final String FIELD_ID = "fieldId";

    public static final String FIELD_TYPE = "fieldType";

    public static final String DISPLAY_NAME = "displayName";
    
    public static final String COLUMN_ID = "columnId";

    public static final String UOM = "uom";
    
    public static final String COLUMN_SEQUENCE = "columnSequence";

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String SITE_PARAMETER = "SITE";

    public static final String START_TIME_PARAMETER = "START_TIME";

    public static final String END_TIME_PARAMETER = "END_TIME";
    
    //Default value constants
    public static final String DEFAULT_STRING_VAL = " ";
    public static final int DEFAULT_TIME_VAL = -1;
    public static final long DEFAULT_LONG_VALUE = -1;

    /**
     * @return The column name that identify the record
     */
    public String getIdentityColumnName();

    /**
     * @return All properties that can be set in the concept
     */
    public JSONArray getAllOutputColumns();

    /**
     * @return The result after executing the code
     * @throws BusinessRuleException when the execute method encounters
     *             exception
     */
    public JSONArray execute() throws BusinessRuleException;
    
    /**
     * Getter for the parameters property.
     * @return JSONObject value of the property
     */
    public JSONObject getParameters();
    
    /**
     * Setter for the parameters property.
     * @param parameters the new parameters value
     */
    public void setParameters(JSONObject parameters);
    
    /**
     * method to get start time of time window for aggregator to consider for
     * fetching data during execution.
     * @param startTime default start time
     * @return start time of time window for aggregator execution
     * @throws JSONException je
     */
    public Date getStartTime(Date startTime) throws JSONException;
    
    /**
     * method to get end time of time window for aggregator to consider for
     * fetching data during execution.
     * @param endTime default start time
     * @return end time of time window for aggregator execution
     * @throws JSONException je
     */
    public Date getEndTime(Date endTime) throws JSONException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 