/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.event;

import com.vocollect.epp.dataaggregator.model.DAEventType;
import com.vocollect.epp.dataaggregator.model.DAInformation;

import org.springframework.context.ApplicationEvent;


/**
 * @author mraj
 *
 */
public abstract class DAUpdateEventRoot extends ApplicationEvent {

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    
    private DAInformation newDAInformation;
    
    /**
     * Constructor.
     * @param event - the event type
     */
    public DAUpdateEventRoot(DAEventType event) {
        super(event);
    }
    
    /**
     * @return the newDAInformation
     */
    public DAInformation getNewDAInformation() {
        return newDAInformation;
    }

    
    /**
     * @param newDAInformation the newDAInformation to set
     */
    public void setNewDAInformation(DAInformation newDAInformation) {
        this.newDAInformation = newDAInformation;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 