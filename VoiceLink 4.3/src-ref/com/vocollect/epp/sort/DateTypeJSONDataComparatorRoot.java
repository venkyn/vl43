/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.sort;

import com.vocollect.epp.logging.Logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author smittal
 * 
 */
public class DateTypeJSONDataComparatorRoot extends JSONDataComparator {
    
    private static final Logger log = new Logger(DateTypeJSONDataComparatorRoot.class);

    @Override
    public int compare(JSONObject json1, JSONObject json2) {
        try {
            
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss a z");
            
            Date jsonDateValue1 = sdf.parse(json1.get(getSortByFieldId()).toString());
            
            Date jsonDateValue2 =  sdf.parse(json2.get(getSortByFieldId()).toString());

            int compareToResultValue = jsonDateValue1.compareTo(jsonDateValue2);

            return getCompareToResultValueBySortType(compareToResultValue);
        } catch (JSONException je) {
            log.warn("json exception occurred");
            return 0;
        } catch (ParseException pe) {
            log.warn("parse exception occurred");
            return 0;
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 