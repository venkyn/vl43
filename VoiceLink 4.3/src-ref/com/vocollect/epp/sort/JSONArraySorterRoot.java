/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.sort;

import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.model.SortOrder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author smittal
 * 
 */
public class JSONArraySorterRoot {

    private JSONArray jsonArrayData;
    
    private DAColumn daColumn;
    
    private SortOrder sortOrder;
    
    private Map <DAFieldType, JSONDataComparator> comparators;

    /**
     * Getter for the jsonArrayData property.
     * @return JSONArray value of the property
     */
    public JSONArray getJsonArrayData() {
        return jsonArrayData;
    }

    /**
     * Setter for the jsonArrayData property.
     * @param jsonArrayData the new jsonArrayData value
     */
    public void setJsonArrayData(JSONArray jsonArrayData) {
        this.jsonArrayData = jsonArrayData;
    }

    /**
     * Getter for the daColumn property.
     * @return DAColumn value of the property
     */
    public DAColumn getDaColumn() {
        return daColumn;
    }

    /**
     * Setter for the daColumn property.
     * @param daColumn the new daColumn value
     */
    public void setDaColumn(DAColumn daColumn) {
        this.daColumn = daColumn;
    }
    
    /**
     * Getter for the sortOrder property.
     * @return SortOrder value of the property
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Setter for the sortOrder property.
     * @param sortOrder the new sortOrder value
     */
    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * Getter for the comparators property.
     * @return Map<DAFieldType,JSONDataComparator> value of the property
     */
    public Map <DAFieldType, JSONDataComparator> getComparators() {
        return comparators;
    }

    /**
     * Setter for the comparators property.
     * @param comparators the new comparators value
     */
    public void setComparators(Map <DAFieldType, JSONDataComparator> comparators) {
        this.comparators = comparators;
    }

    /**
     * method to sort jsonObjects of a jsonArray.
     * @return sorted jsonArray
     * @throws JSONException jse
     */
    public JSONArray sortData() throws JSONException {
        
        JSONArray jsonDataArray = getJsonArrayData();
        List<JSONObject> jsonDataObjects = new ArrayList<JSONObject>();

        for (int i = 0; i < jsonDataArray.length(); i++) {
            jsonDataObjects.add(jsonDataArray.getJSONObject(i));
        }
        
        String sortByFieldId = getDaColumn().getFieldId();
        DAFieldType sortByFieldType = getDaColumn().getFieldType();
        SortOrder dataSortOrder = getSortOrder();
        
        JSONDataComparator jsonComparator = getComparators().get(sortByFieldType);
        jsonComparator.setSortByFieldId(sortByFieldId);
        jsonComparator.setSortOrder(dataSortOrder);
        
        Collections.sort(jsonDataObjects, jsonComparator);
        
        JSONArray sortedData = new JSONArray();
        for (int i = 0; i < jsonDataObjects.size(); i++) {
            sortedData.put(jsonDataObjects.get(i));
        }

        return sortedData;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 