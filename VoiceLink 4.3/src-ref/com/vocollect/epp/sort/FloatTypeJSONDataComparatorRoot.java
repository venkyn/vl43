/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.sort;

import com.vocollect.epp.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author smittal
 * 
 */
public class FloatTypeJSONDataComparatorRoot extends JSONDataComparator {
    
    private static final Logger log = new Logger(FloatTypeJSONDataComparatorRoot.class);

    @Override
    public int compare(JSONObject json1, JSONObject json2) {
        try {
            Float jsonFloatValue1 = Float.parseFloat(json1.get(
                getSortByFieldId()).toString());
            Float jsonFloatValue2 = Float.parseFloat(json2.get(
                getSortByFieldId()).toString());

            int compareToResultValue = jsonFloatValue1
                .compareTo(jsonFloatValue2);

            return getCompareToResultValueBySortType(compareToResultValue);
        } catch (JSONException je) {
            log.warn("json exception occurred");
            return 0;
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 