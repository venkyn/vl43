/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.sort;

import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.TimeFormatter;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author smittal
 * 
 */
public class TimeTypeJSONDataComparatorRoot extends JSONDataComparator {

    private static final Logger log = new Logger(TimeTypeJSONDataComparatorRoot.class);
    
    @Override
    public int compare(JSONObject json1, JSONObject json2) {
        try {
            Integer jsonTimeValue1 = TimeFormatter.formatTimeStringToInteger(json1.get(
                getSortByFieldId()).toString(), ResourceUtil
                .getLocalizedKeyValue("aggregator.field.time.value.default"), GenericDataAggregator.DEFAULT_TIME_VAL);
            Integer jsonTimeValue2 = TimeFormatter.formatTimeStringToInteger(json2.get(
                getSortByFieldId()).toString(), ResourceUtil
                .getLocalizedKeyValue("aggregator.field.time.value.default"), GenericDataAggregator.DEFAULT_TIME_VAL);
            
            int compareToResultValue = jsonTimeValue1.compareTo(jsonTimeValue2);
            
            return getCompareToResultValueBySortType(compareToResultValue);
        } catch (JSONException je) {
            log.warn("json exception occurred");
            return 0;
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 