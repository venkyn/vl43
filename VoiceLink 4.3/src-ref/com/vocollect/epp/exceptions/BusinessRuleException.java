/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;


/**
 * This exception, or a subclass of it, should be thrown when you
 * want to communicate across layers a violation of a business rule.
 *
 * @author ddoubleday
 */
public class BusinessRuleException extends VocollectException {

    private static final long serialVersionUID = 4846213865543019564L;

    /**
     * Empty Constructor.
     */
    public BusinessRuleException() {
    }

    /**
     * Constructor.
     * @param t the exception that caused this exception.
     */
    public BusinessRuleException(Throwable t) {
        super(t);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     */
    public BusinessRuleException(ErrorCode code) {
        super(code);
    }

    /**
     * Constructor.
     * @param code the ErrorCode associated with the exception.
     * @param t the exception that caused this exception.
     */
    public BusinessRuleException(ErrorCode code, Throwable t) {
        super(code, t);
    }

    /**
     * Constructs an instance from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     */
    public BusinessRuleException(ErrorCode code, UserMessage msg) {
        super(code, msg);
    }

    /**
     * Constructs an instance from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user, and include the wrapped
     * exception.
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     * @param t the wrapped exception
     */
    public BusinessRuleException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 