/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;

/**
 * 
 * 
 * @author khazra
 */
public class ReportException extends VocollectException {

    private static final long serialVersionUID = -1318666243516892911L;

    private String reportMessage;
    
    /**
     * Getter for the reportMessage property.
     * @return String value of the property
     */
    public String getReportMessage() {
        return reportMessage;
    }

    /**
     * Setter for the reportMessage property.
     * @param reportMessage the new reportMessage value
     */
    public void setReportMessage(String reportMessage) {
        this.reportMessage = reportMessage;
    }

    /**
     * 
     * Constructor.
     */
    public ReportException() {
        super();
    }

    /**
     * 
     * Constructor.
     * @param code The error code
     * @param t The throwable exception
     */
    public ReportException(ErrorCode code, Throwable t) {
        super(code, t);
    }

    /**
     * 
     * Constructor.
     * @param code The error code
     * @param msg The user message to display
     * @param t The throwable exception
     */
    public ReportException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

    /**
     * 
     * Constructor.
     * @param code The error code
     * @param msg The message to display
     */
    public ReportException(ErrorCode code, UserMessage msg) {
        super(code, msg);
    }

    /**
     * 
     * Constructor.
     * @param code The error code
     * @param msg The message to display
     */
    public ReportException(ErrorCode code, String msg) {
        this.setReportMessage(msg);
    }
    
    /**
     * 
     * Constructor.
     * @param code The error code to display
     */
    public ReportException(ErrorCode code) {
        super(code);
    }

    /**
     * 
     * Constructor.
     * @param t The thorwable exception
     */
    public ReportException(Throwable t) {
        super(t);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.exceptions.VocollectException#toString()
     */
    @Override
    public String toString() {
        return getReportMessage();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 