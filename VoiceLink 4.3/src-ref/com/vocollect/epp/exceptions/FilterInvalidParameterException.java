/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;



/**
 * This exception will be thrown whenever invalid filter parameter.
 *
 * @author jstonebrook
 */
public class FilterInvalidParameterException extends RuntimeException {

    private static final long serialVersionUID = 6531542887019583605L;

    // This represents the operand type
    private int operandType;
    
    // This represents the value for the filter
    private String value;
    
    /**
     * Constructor.
     * @param operandType the operand type used in the filter.
     * @param value the filter value
     */
    public FilterInvalidParameterException(int operandType, String value) {
        this.operandType = operandType;
        this.value = value;
    }

    
    /**
     * Getter for the operandType property.
     * @return int value of the property
     */
    public int getOperandType() {
        return this.operandType;
    }

    
    /**
     * Setter for the operandType property.
     * @param operandType the new operandType value
     */
    public void setOperandType(int operandType) {
        this.operandType = operandType;
    }

    
    /**
     * Getter for the value property.
     * @return String value of the property
     */
    public String getValue() {
        return this.value;
    }

    
    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 