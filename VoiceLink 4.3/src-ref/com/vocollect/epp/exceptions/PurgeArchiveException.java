/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.scheduling.PurgeResult;

/**
 * Used when the purge archive cannot complete its purge attempt.
 *  (such as when all attempts in a batch fail.)
 * @author dkertis
 *
 */
public class PurgeArchiveException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -4776353012757215208L;

    PurgeResult result;

    /**
     * @param result the results of the purge attempt
     */
    public PurgeArchiveException(PurgeResult result) {
        super();
        this.result = result;
    }

    /**
     * @return - the results of the purge attempt
     */
    public PurgeResult getResult() {
        return result;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 