/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.errors.UserMessage;


/**
 * Base class for all Vocollect exceptions related to field validation.
 *
 * @author Brent Nichols
 */
public class FieldValidationException extends BusinessRuleException {

    private static final long serialVersionUID = -595595762483850705L;

    private String field = "";

    private String value = "";

    /**
     * Getter for the field property.
     * @return String value of the property
     */
    public String getField() {
        return this.field;
    }


    /**
     * Setter for the field property.
     * @param field the new field value
     */
    public void setField(String field) {
        this.field = field;
    }


    /**
     * Getter for the value property.
     * @return String value of the property
     */
    public String getValue() {
        return this.value;
    }


    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Constructor. Takes a field reference and a user message.
     * @param field - the field that is throwing the exception
     * @param value - value of the field that is causing the issue
     * @param msg - the localized message explaining the exception
     */
    public FieldValidationException(String field, String value, UserMessage msg) {
        super();
        this.setMessage(msg);
        setField(field);
        setValue(value);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 