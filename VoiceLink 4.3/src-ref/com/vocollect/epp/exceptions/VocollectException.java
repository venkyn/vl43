/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.MessageMap;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Base exception for Vocollect core J2EE framework. It supports a variety
 * of methods of passing information with the exception:
 * <ul>
 * <li>The standard text message
 * <li>The ability to carry an localized UserMessage
 * <li>The ability to carry a MessageMap of localized UserMessage objects.
 * <li>The ability to carry an ErrorCode object.
 * </ul>
 * @author Dennis Doubleday
 */
public class VocollectException extends Exception {

    private static final long serialVersionUID = -1241642848074074304L;

    private static Logger log = new Logger(VocollectException.class);

    /**
     * The error code associated with this exception.
     */
    private ErrorCode         errorCode;

    /**
     * Localized messages associated with the exception.
     */
    private MessageMap        messages;

    /**
     * Constructs an empty instance of <code>VocollectException</code>.
     */
    public VocollectException() {
    }

    /**
     * Constructor.
     * @param msg an unlocalized message associated with the exception.
     */
    public VocollectException(String msg) {
        super(msg);
    }

    /**
     * Constructor.
     * @param t the exception that caused this exception.
     */
    public VocollectException(Throwable t) {
        super(t);
    }

    /**
     * Constructor.
     * @param msg an unlocalized message associated with the exception.
     * @param t the exception that caused this exception.
     */
    public VocollectException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     */
    public VocollectException(ErrorCode code) {
        setCodeAndDefaultMessage(code);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     * @param t the exception that caused this exception.
     */
    public VocollectException(ErrorCode code, Throwable t) {
        super(t);
        setCodeAndDefaultMessage(code);
    }

    /**
     * Constructs an instance of <code>VocollectException</code> from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     */
    public VocollectException(ErrorCode code, UserMessage msg) {
        super();
        addMessage(msg);
        setErrorCode(code);
    }

    /**
     * Constructs an instance of <code>VocollectException</code> from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user, and include the wrapped
     * exception.
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     * @param t the wrapped exception
     */
    public VocollectException(ErrorCode code, UserMessage msg, Throwable t) {
        super(t);
        addMessage(msg);
        setErrorCode(code);
    }

    /**
     * Add the specified localized message to this exception. It will have
     * the default grouping key in the MessageMap.
     * @param msg the localized user message
     */
    public void addMessage(UserMessage msg) {
        if (this.messages == null) {
            this.messages = new MessageMap();
        }

        this.messages.add(msg);

    }

    /**
     * Getter for the errorCode property.
     * @return the code, or null if none set.
     */
    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    /**
     * Return the first localized UserMessage object added to this exception.
     * Note that this is intended for the normal use case where only one
     * message has been added to the <code>MessageMap</code> associated
     * with the exception. A warning will be logged if only one message is
     * retrieved when multiple messages are available.
     *
     * @return A localized UserMessage object, or null if none has been added.
     */
    public UserMessage getUserMessage() {
        if (this.messages == null || this.messages.isEmpty()) {
            return null;
        } else {
            int messageCount = this.messages.getSize();
            if (messageCount > 1) {
                log.warn("Only one message retrieved when " + messageCount
                    + " are available");
            }
            return this.messages.iterator().next();
        }
    }

    /**
     * This method ensures that the standard <code>Exception.getMessage()</code>
     * will never return <code>null</code>.
     * @see java.lang.Throwable#getMessage()
     * @return the value returned by the super-class, unless it is null, in
     * which case it returns the canonical name of the exception class.
     */
    @Override
    public String getMessage() {
        String out = super.getMessage();
        if (out == null) {
            return this.getClass().getCanonicalName();
        } else {
            return out;
        }
    }

    /**
     * @return Returns the localized messages map.
     */
    public MessageMap getMessages() {
        return this.messages;
    }

    /**
     * Setter for the errorCode property.
     * @param newErrorCode - the error code to be set
     */
    public void setErrorCode(ErrorCode newErrorCode) {
        this.errorCode = newErrorCode;
    }

    /**
     * Set the localized message for this exception. If any messages
     * were added prior to calling this, they will be removed and this
     * will be the only one.
     * @param newMessage a localized message.
     */
    public void setMessage(UserMessage newMessage) {
        setMessages(null);
        addMessage(newMessage);
    }

    /**
     * @param newMessages a map of localized messages.
     */
    public void setMessages(MessageMap newMessages) {
        this.messages = newMessages;
    }

    /**
     * Return a String representation of the exception suitable for debug
     * output. The format of the output is the standard
     * <code>ToStringBuilder</code> single line format.
     * @see java.lang.Object#toString()
     * @return the String representation of the exception
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * If code is non-null, store the code and an associated message based
     * off the default key for the code. Otherwise, do nothing.
     * @param code the error code
     */
    private void setCodeAndDefaultMessage(ErrorCode code) {
        if (code != null) {
            setErrorCode(code);
            addMessage(new UserMessage(code.getDefaultMessageKey()));
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 