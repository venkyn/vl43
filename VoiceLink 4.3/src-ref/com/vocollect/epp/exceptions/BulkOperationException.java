/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Base exception for buik operations that may return multiple exceptions, such
 * as partial success when deleting a range of objects. It wraps a list of
 * <code>VocollectException</code> objects, one for each failure in the bulk
 * opreation.
 *
 * @author Dennis Doubleday
 */
public class BulkOperationException extends Exception {

    static final long serialVersionUID = -5433352647132544717L;

    /**
     * The list of wrapped exceptions.
     */
    private List<VocollectException> exceptions =
        new ArrayList<VocollectException>();

    /**
     * Constructs an empty instance of <code>BulkOperationException</code>.
     */
    public BulkOperationException() {
        super();
    }

    /**
     * Add an exceptions to the list of wrapped exceptions.
     * @param e - the exception to be added
     */
    public void addException(VocollectException e) {
        this.exceptions.add(e);
    }

    /**
     * Getter for the exceptions property.
     * @return the list of <code>VocollectException</code>s, or an empty list
     * if there are no errors.
     */
    public List<VocollectException> getExceptions() {
        return this.exceptions;
    }


    /**
     * Setter for the exceptions property.
     * @param errors the list of exceptions to set. If this is null,
     * the list will be set to the empty list.
     */
    public void setExceptions(List<VocollectException> errors) {
        if (errors == null) {
            this.exceptions.clear();
        } else {
            this.exceptions = errors;
        }
    }


    /**
     * Return a String representation of the exception suitable for debug
     * output. The format of the output is the standard
     * <code>ToStringBuilder</code> single line format.
     * @see java.lang.Object#toString()
     * @return the String representation of this object.
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 