/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.search;

import java.util.Properties;

/**
 * This class maps the necessary data for search results.
 *
 * @author hulrich
 */
public class SearchResultDataMappings {

    /**
     * Code for the display name.
     */
    private static final String AREA_DISPLAY = ".displayArea";

    /**
     * Code for the URL.
     */
    private static final String URL = ".url";

    /**
     * Code for the filter params.
     */
    private static final String FILTER_PARAMS = ".filterParams";

    /**
     * Code for the display field.
     */
    private static final String DISPLAY_FIELD = ".displayField";

    /**
     * Code for the id to use in the url (if applicable).
     */
    private static final String DISPLAY_ID_FIELD = ".displayIdField";

    private static final String DO_NOT_DISPLAY_ID = ".doNotDisplayId";

    private static final String FILTER_PARAMETER = ".filterParameter";

    private static final String FILTER = ".filter";

    private static final String TRANSLATION = ".translation";

    private static Properties mappingProperties;

    private String className;

    /**
     * Constructor.
     */
    public SearchResultDataMappings() {
    }

    /**
     * Constructor.
     * @param className the className
     */
    public SearchResultDataMappings(String className) {
        this.className = className;
    }

    /**
     * Determines if there are any mappingProperties for this class.
     * @return if the result should be displayed
     */
    public boolean isDisplayable() {
        return (getUrl() != null);
    }

    /**
     * Gets the url.
     * @return the url
     */
    public String getUrl() {
        return getDataInternal(URL);
    }

    /**
     * Gets the params.
     * @return the params
     */
    public String getParams() {
        return getDataInternal(FILTER_PARAMS);
    }

    /**
     * The display area.
     * @return the display area
     */
    public String getDisplayArea() {
        return this.getDataInternal(AREA_DISPLAY);
    }

    /**
     * The display field.
     * @return the display field
     */
    public String getDisplayField() {
        return this.getDataInternal(DISPLAY_FIELD);
    }

    /**
     * The display id field.
     * @return the display id field
     */
    public String getDisplayFieldId() {
        return getDataInternal(DISPLAY_ID_FIELD);
    }

    /**
     * The display filter parameter to be placed in the Filter.
     * @return the filter parameter
     */
    public String getFilterParameter() {
        return getDataInternal(FILTER_PARAMETER);
    }

    /**
     * The display filter.
     * @return the filter.
     */
    public String getFilter() {
        return getDataInternal(FILTER);
    }

    /**
     * The display id field.
     * @return the display id field
     */
    public String getDoNotDisplayId() {
        return getDataInternal(DO_NOT_DISPLAY_ID);
    }

    /**
     * Creates the message code from appending the key to the class name given
     * on construction.
     * @return true if it is translatable
     */
    public boolean isTranslatable() {
        if (getDataInternal(TRANSLATION) != null) {
           return  Boolean.valueOf(getDataInternal(TRANSLATION)).booleanValue();
        } else {
            return false;
        }
     }

    /**
     * Creates the message code from appending the key to the class name given
     * on construction.
     * @param key the key to be appended
     * @return the message
     */
    public String getData(String key) {
        return mappingProperties.getProperty(key);
    }

    /**
     * Sets the mapping properties.
     * @param source the properties object.
     */
    public void setMappingProperties(Properties source) {
        mappingProperties = source;
    }

    /**
     * Gets the mapping propertes.
     * @return the mapping properties object.
     */
    public Properties getMappingProperties() {
        return mappingProperties;
    }

    /**
     * Return the data by prepending the key with class name.
     * @param key the key
     * @return the value
     */
    private String getDataInternal(String key) {
        try {
            return getData(className + key);
        } catch (Exception e) {
            return null;
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 