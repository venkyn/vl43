/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.search;

import com.vocollect.epp.errors.ErrorCode;


/**
 *
 *
 * @author hulrich
 */
public final class SearchErrorCode extends ErrorCode {
    /**
     * Lower boundary of the EPP scheduling error range.
     */
    public static final int LOWER_BOUND  = 4500;

    /**
     * Upper boundary of the EPP scheduling error range.
     */
    public static final int UPPER_BOUND  = 4999;

    /**
     * Constructor.
     */
    private SearchErrorCode() {
        super("SEARCH", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Create a custom error code using the given value.
     * @param err - error to be logged
     */
    private SearchErrorCode(long err) {
        super(SearchErrorCode.NO_ERROR, err);
    }

    /**
     * Perform base initialization.
     */
    public static final SearchErrorCode NO_ERROR
        = new SearchErrorCode();

    /**
     * Unable to access the default scheduler instance.
     */
    public static final SearchErrorCode COMPASS_DOES_NOT_EXIST
        = new SearchErrorCode(4500);

    /**
     * Unable to schedule job.
     */
    public static final SearchErrorCode UNABLE_TO_START_TRANSACTION
        = new SearchErrorCode(4501);

    /**
     * Job already running.
     */
    public static final SearchErrorCode UNABLE_TO_OPEN_SESSION
        = new SearchErrorCode(4502);

    public static final SearchErrorCode UNABLE_TO_MAP_RESULT
        = new SearchErrorCode(4503);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 