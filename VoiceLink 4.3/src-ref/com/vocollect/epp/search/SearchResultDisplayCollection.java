/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.search;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.security.RoleFeatureMap;
import com.vocollect.epp.security.RoleFeatureMapFactory;
import com.vocollect.epp.service.SearchManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;

/**
 * This class is used for organizing the display of search results. The results
 * are organized by site -> area -> result. The general usage should be to
 * retrieve the ordered keys with the current site as first and the "All Sites"
 * as last. Then iterate over the list, selecting each site result set.
 *
 * @author hulrich
 */
public final class SearchResultDisplayCollection {

    private static final Logger log = new Logger(
        SearchResultDisplayCollection.class);

    // Count of hits.
    private int count;

    // Count of hits not displayed due to limits
    private int hiddenCount;

    private Map<String, SearchResultList<SearchResultList<SearchResult>>> table;

    private List<String> siteList;

    /**
     * Builds the display collection with the current site id, if applicable.
     * @param results the search results
     * @param user the searching user
     * @param perCategoryLimit the limit to display per category
     * @return the collection
     * @throws DataAccessException if there is a problem building the collection
     */
     public static SearchResultDisplayCollection
        buildDisplayCollection(SearchResultList<SearchResult> results,
                               User user,
                               int  perCategoryLimit)
        throws DataAccessException {

        SearchResultDisplayCollection coll = null;
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        String currentSite = siteContext.getCurrentSiteName();
        if (((user.getAllSitesAccess()) || (user.getSites().size() > 1))
            && (siteContext.getAllSites().size() > 1)) {

            coll = SearchResultDisplayCollection.buildMultiSiteCollection(
                results, user, currentSite, SearchManager.ALL_SITES, perCategoryLimit);
        } else {
            coll = SearchResultDisplayCollection.buildSingleSiteCollection(
                results, currentSite, SearchManager.RESULTS, perCategoryLimit);
        }

        return coll;
    }

    /**
     * Builds the display collection for muti-sites.
     * @param results the results
     * @param user the user
     * @param currentSite the current site
     * @param multiSite the "ALL SITES" terminology
     * @param perCategoryLimit the limit to display per category
     * @return the collection
     */
    public static SearchResultDisplayCollection
        buildMultiSiteCollection(SearchResultList<SearchResult> results,
                                 User user,
                                 String currentSite,
                                 String multiSite,
                                 int perCategoryLimit) {
        log.debug("Building multi site collection..." + currentSite);
        SearchResultDisplayCollection coll = new SearchResultDisplayCollection(
            results.getData().size());

        for (SearchResult res : results.getData()) {
            coll.add(res, user.getSites(), user.getAllSitesAccess(), perCategoryLimit);
        }
        coll.orderKeys(currentSite, multiSite);

        return coll;
    }

    /**
     * Builds a collection for a single site.
     * @param results the results
     * @param currentSite the current (only) site of the user
     * @param displaySite the site name to display
     * @param perCategoryLimit the limit to display per category
     * @return the collection
     */
    public static SearchResultDisplayCollection
        buildSingleSiteCollection(SearchResultList<SearchResult> results,
                                  String currentSite,
                                  String displaySite,
                                  int perCategoryLimit) {
        log.debug("Building single site collection... " + displaySite);
        SearchResultDisplayCollection coll = new SearchResultDisplayCollection(
            results.getData().size());

        for (SearchResult res : results.getData()) {
            log.debug("All sites : "
                + res.getSites().contains(SearchManager.ALL_SITES));
            log.debug("current site : " + currentSite + " ---- ");
            for (String s : res.getSites()) {
                log.debug("Site: " + s);
            }
            log.debug(" ---- ");

            if ((res.getSites().contains(SearchManager.ALL_SITES))
                || (res.getSites().contains(currentSite))) {

                if (!(res.getResult() instanceof Site)) {
                    log.debug("Adding result " + res.getDisplayName()
                        + " to site " + displaySite);
                    coll.add(res, displaySite, perCategoryLimit);
                }
            }
        }
        coll.orderKeys(null, null);

        return coll;
    }

    /**
     * Constructor.
     * @param size the number of results
     */
    private SearchResultDisplayCollection(int size) {
        count = size;
        table = new TreeMap<String, SearchResultList<SearchResultList<SearchResult>>>();
    }

    /**
     * Retrieves the collection for the site.
     * @param key the site
     * @return the collection for that site
     */
    public SearchResultList<SearchResultList<SearchResult>> getSiteCollection(String key) {
        return table.get(key);
    }

    /**
     * The overall count of every result held in this collection.
     * @return the count
     */
    public int getCount() {
        return count;
    }


    /**
     * @return Count of hits not displayed due to limits
     */
    public int getHiddenCount() {
        return this.hiddenCount;
    }

    /**
     * Adds a result to the collection based upon its Sites, then its alias.
     * @param hit the result
     * @param userSites the sites the user has access to
     * @param allSites whether all sites are viewable
     * @param perCategoryLimit the limit to display per category
     */
    public void add(SearchResult hit,
                    Set<Site> userSites,
                    boolean allSites,
                    int perCategoryLimit) {
        for (String siteName : hit.getSites()) {
            if ((allSites)
                || (canView(siteName, userSites.iterator(), hit.getResult()))) {
                add(hit, siteName, perCategoryLimit);
            }
        }
    }

    /**
     * Gets the multi-leveled list for display.
     * @return the multi-level list of results
     */
    public List<SearchResultList<SearchResultList<SearchResult>>> getSiteList() {
        List<SearchResultList<SearchResultList<SearchResult>>> list =
            new LinkedList<SearchResultList<SearchResultList<SearchResult>>>();

        for (String key : siteList) {
            list.add(table.get(key));
        }

        return list;
    }

    /**
     * Determines the index of the site name.
     * @param siteName the site name
     * @return the index, -1 if it does not exist
     */
    public int getIndex(String siteName) {
        int i = 0;
        List<SearchResultList<SearchResultList<SearchResult>>> list = getSiteList();
        for (SearchResultList<SearchResultList<SearchResult>> srl : list) {
            if (srl.getName().equals(siteName)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    /**
     * Adds this result to the particular site. This is best used when the user
     * does not care about site information.
     * @param hit the result
     * @param siteName the site name
     * @param perCategoryLimit the limit to display per category
     */
    private void add(SearchResult hit, String siteName, int perCategoryLimit) {
        if (!this.canView(hit.getTargetUrl())) {
            return;
        }

        if ((hit.getResult() instanceof Role)
                && (((Role) hit.getResult()).getIsAdministrative())) {
            hit.setReadOnly(true);
        }

        SearchResultList<SearchResultList<SearchResult>> siteData = table
            .get(siteName);
        SearchResultList<SearchResult> areaData = null;

        if (siteData == null) {
            siteData = new SearchResultList<SearchResultList<SearchResult>>(
                siteName);
            areaData = new SearchResultList<SearchResult>(hit.getArea());
            siteData.add(areaData);
            table.put(siteName, siteData);
        } else {
            areaData = siteData.get(hit.getArea());
            if (areaData == null) {
                areaData = new SearchResultList<SearchResult>(hit.getArea());
                siteData.add(areaData);
            }
        }

        if (areaData.getSize() < perCategoryLimit) {
            areaData.add(hit);
            areaData.increment();
            siteData.increment();
        } else {
            areaData.incrementHidden();
            siteData.incrementHidden();
            hiddenCount++;
            if (log.isTraceEnabled()) {
                log.trace("Exceeded limit for area "
                    + areaData.getName() + ", data not displayed");
            }
        }
    }

    /**
     * Use this method to retrieve the properly ordered list of sites, which
     * starts with the current site and ends with the "All Sites". Use the
     * method return to pull the site collection.
     * @param first the current site name
     * @param last the "All Sites"
     */
    private void orderKeys(String first, String last) {
        siteList = new LinkedList<String>();
        Iterator<String> iter = table.keySet().iterator();
        while (iter.hasNext()) {
            siteList.add(iter.next());
        }
        Collections.sort(siteList, new Comparator<String>() {

            public int compare(String o1, String o2) {
                return o1.toLowerCase().compareTo(o2.toLowerCase());
            }
        });
        reorder(first, siteList, 0);
        reorder(last, siteList, -1);
    }

    /**
     * Simple convenience method that moves the element in the string.
     * @param key the element to be moved
     * @param list the list
     * @param index the new index, -1 if last
     */
    private void reorder(String key, List<String> list, int index) {
        if (key == null) {
            return;
        }

        if (list.remove(key)) {
            if (index < 0) {
                list.add(key);
            } else {
                list.add(index, key);
            }
        }
    }

    /**
     * Verifies that a user is able to view this url.
     * @param url the url to view
     * @return whether they are able to view the url
     */
    private boolean canView(String url) {
        Authentication user = SecurityContextHolder.getContext()
            .getAuthentication();
        RoleFeatureMap roleFeatureMap = RoleFeatureMapFactory
            .getRoleFeatureMap();
        // Need to lowercase the url before trying or it may not find it
        return roleFeatureMap.userHasURIAccess(user, url.toLowerCase());

    }

    /**
     * Verifies that a user can view the specified site.
     * @param siteName the site to be viewed
     * @param userSites iterator of the user's sites
     * @param o the retrieved object
     * @return whether the site is viewable
     */
    private boolean canView(String siteName, Iterator<Site> userSites, Object o) {
        if (SearchManager.ALL_SITES.equals(siteName)) {
            if (o instanceof Site) {
                Site s = (Site) o;
                while (userSites.hasNext()) {
                    if (s.getName().equals(userSites.next().getName())) {
                        return true;
                    }
                }
            } else {
                return true;
            }
        }

        while (userSites.hasNext()) {
            String site = userSites.next().getName();

            if ((site.equals(siteName))) {
                return true;
            }
        }
        return false;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 