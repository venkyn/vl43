/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.search;

import com.vocollect.epp.model.BaseModelObject;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * A result from a search through Compass.
 *
 * @author hulrich
 */
public class SearchResult implements SearchResultStorable {

    private String targetUrl;

    private String displayName;

    private String area;

    private List<String> sites;

    private String query;

    private BaseModelObject result;

    private boolean readOnly;

    /**
     * Constructor.
     * @param url the url to display
     * @param name the text to display over the url
     * @param area the business area (User, Role, Assignments)
     * @param site the site for the result
     * @param res the result object
     */
    public SearchResult(String url,
                        String name,
                        String area,
                        List<String> site,
                        BaseModelObject res) {
        targetUrl = url;
        displayName = name;
        this.area = area;
        this.sites = site;
        this.result = res;
    }

    /**
     * Getter for the displayName property.
     * @return String value of the property
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Getter for the query property.
     * @return String value of the property
     */
    public String getQuery() {
        return this.query;
    }

    /**
     * Getter for the area property.
     * @return String value of the property
     */
    public String getArea() {
        return this.area;
    }

    /**
     * Getter for the targetUrl property.
     * @return String value of the property
     */
    public String getTargetUrl() {
        return this.targetUrl;
    }

    /**
     * Getter for site name.
     * @return the site name
     */
    public List<String> getSites() {
        return this.sites;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.search.SearchResultStorable#getComparableKey()
     */
    public String getComparableKey() {
        return getDisplayName();
    }

    /**
     * Results are compared by display name. {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(SearchResultStorable res) {
        return getComparableKey().compareToIgnoreCase(res.getComparableKey());
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("area", this.getArea()).append(
                "displayName", this.getDisplayName()).append(
                "url", this.getTargetUrl()).toString();
    }


    /**
     * Getter for the result property.
     * @return Object value of the property
     */
    public Object getResult() {
        return this.result;
    }


    /**
     * Setter for the result property.
     * @param result the new result value
     */
    public void setResult(BaseModelObject result) {
        this.result = result;
    }

    /**
     * Whether this results is read only.
     * @return whether the result is read only
     */
    public boolean getIsReadOnly() {
        return readOnly;
    }

    /**
     * Sets whether the result is read only.
     * @param readOnly is read only
     */
    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 