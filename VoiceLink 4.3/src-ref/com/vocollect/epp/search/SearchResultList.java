/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This is a general storage class for search results. It can store anything
 * that implements the <code>SearchResultStorable</code> interface.
 * <p>
 * <bold>Note:</bold> the size indicated in the <code>getSize</code> does not
 * match the size of the underlying list object. This can be retrieved from the
 * actual list. The <code>getSize</code> method returns the number of
 * <code>SearchResult</code> objects held in the collection. This should be
 * incremented through the <code>increment</code> method.
 *
 * @param <T> the type to store
 * @author hulrich
 */
public class SearchResultList<T extends SearchResultStorable> implements
    SearchResultStorable {

    private List<T> data = new ArrayList<T>();

    // The count of displayable hits.
    private int count;

    // The count of hidden hits (too many hits - exceeded limit)
    private int hiddenCount;

    private String name;

    /**
     * Constructor.
     * @param name the key for this list.
     */
    public SearchResultList(String name) {
        this.name = name;
    }

    /**
     * The number of <code>SearchResult</code> objects held in this
     * collection. This is incremented through the <code>increment</code>
     * method.
     * @return the number of <code>SearchResult</code> objects
     */
    public int getSize() {
        return count;
    }


    /**
     * Getter for the hiddenCount property.
     * @return int value of the property
     */
    public int getHiddenCount() {
        return this.hiddenCount;
    }

    /**
     * Increments the number of <code>SearchResult</code> objects.
     */
    public void increment() {
        count++;
    }

    /**
     * Increments the number of <code>SearchResult</code> objects that
     * were not displayed because of query limits.
     */
    public void incrementHidden() {
        hiddenCount++;
    }

    /**
     * Returns the underlying list object. This is not a copy or clone, so be
     * careful of any changes made to this object. (This should probably be
     * changed)
     * @return the <code>List</code> object.
     */
    public List<T> getData() {
        Collections.sort(data);
        return data;
    }

    /**
     * The name of this collection. It is the key.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Adds an object to the list.
     * @param obj the object to add
     */
    public void add(T obj) {
        data.add(obj);
    }

    /**
     * Adds a collection of results.
     * @param coll the list
     */
    public void addAll(SearchResultList<T> coll) {
        data.addAll(coll.getData());
    }

    /**
     * Gets the element with this key.
     * @param key the identifier
     * @return the element, if it exists
     */
    public T get(String key) {
        for (int i = 0; i < getData().size(); i++) {
            T obj = getData().get(i);
            if (obj.getComparableKey().equals(key)) {
                return getData().get(i);
            }
        }
        return null;
    }

    /**
     * Gets the element with this index.
     * @param index the index
     * @return the element, if it exists
     */
    public T get(int index) {
        return data.get(index);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.search.SearchResultStorable#getComparableKey()
     */
    public String getComparableKey() {
        return this.getName();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(SearchResultStorable obj) {
        return getComparableKey().compareToIgnoreCase(obj.getComparableKey());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 