/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.DataProviderDAO;

/**
 * Interface to provide access to a List of DataObjects. This is used to provide
 * data to TableComponent views.
 *
 * @author dkertis
 */
public interface DataProviderRoot {

    /**
     * Returns the DAO to use for an operation.
     * @return - the DAO casted to a generic base class
     */
    DataProviderDAO getProviderDAO();

    /**
     * Specify which DAO should be provided.
     * @param name the name of the DAO to provide
     * @return the DAO casted to a generic base class
     */
    DataProviderDAO getProviderDAO(String name);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 