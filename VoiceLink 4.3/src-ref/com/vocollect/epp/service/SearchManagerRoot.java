/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.search.SearchResult;
import com.vocollect.epp.search.SearchResultList;

/**
 * This interface provides search functionality through Compass.
 *
 * @author hulrich
 */
public interface SearchManagerRoot {

    /**
     * Denotes results that are not associated with sites.
     */
    public static final String ALL_SITES = "search.allSites";

    /**
     * The key for general results, not associated with sites.
     * This should be used with single site searches.
     */
    public static final String RESULTS = "search.results";


    /**
     * Search all objects designated as searchable. Assumes the default
     * appMenuName.
     * @param searchTerm the term to search for.
     * @return the list of all objects that match in some field
     * designated as searchable.
     * @throws DataAccessException on any database failure.
     */
    SearchResultList<SearchResult> searchAll(String searchTerm)
                throws DataAccessException;

    /**
     * Search all objects designated as searchable.
     * @param searchTerm the term to search for.
     * @param appMenuName the name of the application menu to build search
     * hit URLs relative to.
     * @return the list of all objects that match in some field
     * designated as searchable.
     * @throws DataAccessException on any database failure.
     */
    SearchResultList<SearchResult> searchAll(String searchTerm, String appMenuName)
                throws DataAccessException;

    /**
     * Return the limit on results per category.
     * @return the limit.
     */
    int getCategoryResultLimit();


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 