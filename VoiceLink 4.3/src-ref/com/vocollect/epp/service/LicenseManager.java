/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.LicenseAuthenticationException;
import com.vocollect.epp.exceptions.LicenseFormatException;
import com.vocollect.epp.model.SystemLicense;


/**
 * 
 *
 * @author mressler
 */
public interface LicenseManager extends GenericManager<SystemLicense, GenericDAO<SystemLicense>> {

    /**
     * Returns an instance of License which wraps the currently enforced license
     * or returns null. 
     * @return License representing the license file being currently enforced
     * @throws DataAccessException if there is an error while trying to retrieve the license from the DB 
     * @throws BusinessRuleException if there is somehow more than one active 
     * license in the database
     */
    public SystemLicense getCurrentLicense() throws BusinessRuleException, DataAccessException;
    
    /**
     * Sets the current license for the system.  All other licenses will be 
     * deleted.
     * @param currentLicense The license that will be become the new system license
     * @throws DataAccessException on failure to save.
     */
    public void saveCurrentLicense(SystemLicense currentLicense) throws DataAccessException;
    
    /**
     * This method will check the license status and create a notification when appropriate.
     * @throws DataAccessException if there is some unexpected data access problem.
     * @throws BusinessRuleException if more than one license is detected
     * @throws LicenseAuthenticationException if we have an authentication problem
     * @throws LicenseFormatException if we have an invalid license
     */
    public void saveNotificationForLicenseStatus() throws DataAccessException, BusinessRuleException, 
        LicenseAuthenticationException, LicenseFormatException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 