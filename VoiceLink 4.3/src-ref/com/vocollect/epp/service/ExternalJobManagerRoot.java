/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.ExternalJobDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ExternalJob;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.GenericManager;

/**
 * 
 * 
 * @author khazra
 */
public interface ExternalJobManagerRoot extends
    GenericManager<ExternalJob, ExternalJobDAO>, DataProvider {

    /**
     * Finds an external job with the given name
     * @param name The name of the job
     * @return the external job if it is found, or null
     */
    public ExternalJob findExternalJobByName(String name)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 