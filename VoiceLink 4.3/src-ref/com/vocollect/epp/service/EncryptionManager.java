/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;


/**
 *
 *
 * @author ???
 */
public interface EncryptionManager {

    /**
     * Encrypts a string so that VoiceClient can decrypt it with its private key.
     * @param toEncrypt The string to encrypt
     * @return The encrypted string
     */
    public byte[] encrypt(byte[] toEncrypt);

    /**
     * @param toEncrypt .
     * @param pin .
     * @return .
     */
    public byte[] encrypt(byte[] toEncrypt, String pin);

    /**
     * @param encoded .
     * @param pin .
     * @return .
     */
    public byte[] decodeDecrypt(String encoded, String pin);

    /**
     * @param toEncode .
     * @return .
     */
    public String encode(byte[] toEncode);

    /**
     * @param toProcess .
     * @return .
     */
    public String encryptAndEncode(byte[] toProcess);

    /**
     * @param toEncrypt .
     * @param pin .
     * @return .
     */
    public String encryptAndEncode(byte[] toEncrypt, String pin);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 