/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.HomepageDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.User;
import java.util.Map;


/**
 * HomepageManager interface.
 *
 * @author svoruganti
 */
public interface HomepageManagerRoot  extends
GenericManager<Homepage, HomepageDAO>,  RemovableDataProvider {


    /**
     * To retrieve summaries with user and homepageId.
     * @param homepageId -  homepageID
     * @param  user - User currently logged in
     * @return Map - Summaries Map with Location and SummaryID
     * @throws DataAccessException - thrown when an exception occus while retrieving data
     */
    public Map < String , Long> getSummaries(User user, long homepageId) throws DataAccessException;

    /**
     * To retrieve summaries with user and homepageId.
     * @param homepageId -  homepageID
     * @param  user - User currently logged in
     * @return Homepage object
     * @throws DataAccessException - thrown when an exception occus while retrieving data
     */
    public Homepage getHomepageByUserAndHomepageId(User user, long homepageId) throws DataAccessException;


    /**
     * To retrieve summaries with  homepageId.
     * @param homepageId -  homepageID
     * @return Homepage object
     * @throws DataAccessException - thrown when an exception occus while retrieving data
     */
    public Homepage getHomepageById(long homepageId) throws DataAccessException;

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 