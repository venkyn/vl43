/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;



/**
 * The service interface for working with Site object.
 *
 * @author Kalpna T
 */
public interface SiteManagerRoot
    extends GenericManager<Site, SiteDAO>, RemovableDataProvider {

    /**
     * @param u the user
     * @return the Tag of the default site for the user.
     * @throws DataAccessException on database failure.
     */
    public Tag findDefaultSiteForUser(User u) throws DataAccessException;

    /**
     * @param u the User
     * @return the number of sites available to the user.
     * @throws DataAccessException on database failure.
     */
    public int getNumberOfSites(User u) throws DataAccessException;

    /**
     * Returns a site based on the name parameter.
     * @param name the site name
     * @return the site, or null if not found
     * @throws DataAccessException on database failure.
     */
    public Site findByName(String name) throws DataAccessException;


    /**
     * @return the Default Site.
     */
    public Site findDefaultSite();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 