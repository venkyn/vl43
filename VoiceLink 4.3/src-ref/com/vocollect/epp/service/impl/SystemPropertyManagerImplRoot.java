/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SystemPropertyDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.SystemPropertyManager;


/**
 * Implementation for system Property manager.
 *
 * @author
 */
public abstract class SystemPropertyManagerImplRoot 
    extends GenericManagerImpl<SystemProperty, SystemPropertyDAO> 
    implements SystemPropertyManager {

    /**
     * Constructor.
     * @param primaryDAO - primary data access object
     */
    public SystemPropertyManagerImplRoot(SystemPropertyDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SystemPropertyManager#findByName(java.lang.String)
     */
    public SystemProperty findByName(String propertyName) throws DataAccessException {
        return getPrimaryDAO().findByName(propertyName);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 