/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SystemPropertyDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.EncryptionManager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 *
 *
 * @author ??
 */
public class EncryptionManagerImpl implements EncryptionManager {

    private static final Logger log = new Logger(EncryptionManagerImpl.class);

    private SystemPropertyDAO propertyDAO;

    public static final String PUBLIC_KEY_PROPERTY = "PUBLIC_KEY";
    public static final String ASYMMETRIC_ALGORITHM = "RSA/NONE/PKCS1Padding";

    public static final String PRIVATE_PIN = "A%gi$^90()";

    {
        Security.addProvider(new BouncyCastleProvider());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#encrypt(byte[], java.lang.String)
     */
    public byte[] encrypt(byte[] toEncrypt, String pin) {

        if (pin == null) {
            return toEncrypt;
        }

        try {
            // Create the SecretKey we'll need
            String salt = "12NaCl34";
            byte[] pinData = (pin + salt).getBytes();

            byteShift(pinData);

            DESKeySpec desKeySpec = new DESKeySpec(pinData);

            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = keyFactory.generateSecret(desKeySpec);

            // Create the Cipher to be used for encryption
            Cipher encryptCipher = Cipher.getInstance("DES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);

            return encode(process(toEncrypt, encryptCipher)).getBytes();
        } catch (InvalidKeyException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            log.warn(e);
            throw new RuntimeException(e);
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#decodeDecrypt(java.lang.String, java.lang.String)
     */
    public byte[] decodeDecrypt(String encoded, String pin) {
        try {
            // Set up the cipher
            Cipher decryptCipher = Cipher.getInstance("DES/ECB/PKCS7Padding");
            decryptCipher.init(Cipher.DECRYPT_MODE, getSecretKey(pin));

            byte[] unencodedSecret = decode(encoded);
            return EncryptionManagerImpl.process(unencodedSecret, decryptCipher);
        } catch (NoSuchAlgorithmException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IllegalStateException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            log.warn(e);
            throw new RuntimeException(e);
        }
    }

    /**
     * @param toDecode .
     * @return .
     */
    public byte[] decode(String toDecode) {
        return Base64.decodeBase64(toDecode.getBytes());
    }

    /**
     * @param pin .
     * @return .
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    private SecretKey getSecretKey(String pin) throws InvalidKeyException,
        NoSuchAlgorithmException, InvalidKeySpecException {
        // Create the SecretKey we'll need
        String salt = "12NaCl34";
        byte[] pinData = (pin + salt).getBytes();

        byteShift(pinData);

        DESKeySpec desKeySpec = new DESKeySpec(pinData);

        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        return keyFactory.generateSecret(desKeySpec);
    }

    /**
     * @param toShift .
     * @return .
     */
    private byte[] byteShift(byte[] toShift) {

        for (int i = 0; i < toShift.length; i++) {
            toShift[i] <<= 4;
        }

        return toShift;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#encrypt(byte[])
     */
    public byte[] encrypt(byte[] toEncrypt) {

        try {
            SystemProperty publicKeyProp = propertyDAO.findByName(PUBLIC_KEY_PROPERTY);

            if (publicKeyProp == null) {
                String message = "Could not find a value for " + PUBLIC_KEY_PROPERTY + " in encrypt method.";
                log.warn(message);
                throw new RuntimeException(message);
            }

            // Recreate the Public Key from the property table
            X509EncodedKeySpec encodedKey = new X509EncodedKeySpec(Base64
                .decodeBase64(publicKeyProp.getValue().getBytes()));
            KeyFactory keyFact = KeyFactory.getInstance("RSA", "BC");
            PublicKey publicKey = keyFact.generatePublic(encodedKey);
            // Create the Cipher to be used for encryption
            Cipher encryptCipher = Cipher.getInstance(ASYMMETRIC_ALGORITHM, "BC");
            encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            return processChunked(toEncrypt, encryptCipher);

        } catch (InvalidKeySpecException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (DataAccessException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchProviderException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (NoSuchPaddingException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IOException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IllegalStateException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (IllegalBlockSizeException e) {
            log.warn(e);
            throw new RuntimeException(e);
        } catch (BadPaddingException e) {
            log.warn(e);
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#encode(byte[])
     */
    public String encode(byte[] toEncode) {
        return new String(Base64.encodeBase64(toEncode));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#encryptAndEncode(byte[])
     */
    public String encryptAndEncode(byte[] toProcess) {
        return encryptAndEncode(toProcess, null);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.EncryptionManager#encryptAndEncode(byte[], java.lang.String)
     */
    public String encryptAndEncode(byte[] toProcess, String pin) {

        return encode(encrypt(encrypt(toProcess, pin)));
    }

    /**
     * @param processMe .
     * @param cipher .
     * @return .
     * @throws IOException
     */
    public static byte[] process(byte[] processMe, Cipher cipher) throws IOException {
        // Create the input stream to be used for encryption
        ByteArrayInputStream in = new ByteArrayInputStream(processMe);

        // Now actually encrypt the data and put it into a
        // ByteArrayOutputStream so we can pull it out easily.
        CipherInputStream processStream = new CipherInputStream(in, cipher);
        ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
        int whatWasRead = 0;
        while ((whatWasRead = processStream.read()) != -1) {
            resultStream.write(whatWasRead);
        }

        return resultStream.toByteArray();
    }

    /**
     * @param processMe .
     * @param cipher .
     * @return .
     * @throws IOException
     * @throws IllegalStateException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    public static byte[] processChunked(byte[] processMe, Cipher cipher)
        throws IOException, IllegalStateException, IllegalBlockSizeException, BadPaddingException
    {
        ByteArrayOutputStream resultStream = new ByteArrayOutputStream();
        int blockSize = cipher.getBlockSize();

        byte[] processChunk = new byte[blockSize];
        for (int i = 0; i < processMe.length; i += blockSize) {
            if (i + blockSize - 1 >= processMe.length) {
                processChunk = new byte[processMe.length - i];
            }
            System.arraycopy(processMe, i, processChunk, 0, processChunk.length);
            resultStream.write(cipher.doFinal(processChunk));
        }

        return resultStream.toByteArray();
    }

    /**
     * @param propDAO .
     */
    public void setSystemPropertyDAO(SystemPropertyDAO propDAO) {
        propertyDAO = propDAO;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 