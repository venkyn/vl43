/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ColumnDAO;
import com.vocollect.epp.dao.FilterDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.FilterManager;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;
import com.vocollect.epp.util.FilterUtil;

import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Service implementation for Filter.
 * @author jgeisler
 */
public abstract class FilterManagerImplRoot
    extends GenericManagerImpl<Filter, FilterDAO>
    implements FilterManager {

    private UserPreferencesManager userPreferencesManager;
    private ColumnDAO columnDAO;

    /**
     * Constructor.
     * @param primaryDAO the PrimaryDAO
     */
    public FilterManagerImplRoot(FilterDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Adds Filter Crit.
     * @param criterion Newest criterion to be added to filter.
     * @param user The user the filter is for.
     * @param viewId The view id of the view the filter is on.
     * @throws DataAccessException if an error occurs getting
     * the view or saving the filter.
     * @return long of the criterion just created.
     */
    public long addFilterCriterion(FilterCriterion criterion, User user, long viewId) throws DataAccessException {

        Filter filter = getPrimaryDAO().findByUserIdAndViewId(user.getId(), viewId);

        if (filter != null) {
            filter.getFilterCriteria().add(criterion);
        } else {
            List<FilterCriterion> critList = new LinkedList<FilterCriterion>();
            critList.add(criterion);
            filter = new Filter(user,
                                userPreferencesManager.getView(viewId),
                                critList);
        }
        getPrimaryDAO().save(filter);
        return criterion.getId();
    }

    /**
     * Overrides current user's filters with new ones.
     * @param filters the new filters
     * @param user the user
     * @throws DataAccessException thrown.
     * @see com.vocollect.epp.service.FilterManagerRoot#addFilters(java.util.List)
     */
    public void addFilters(List<Filter> filters, User user) throws DataAccessException {
        for (Filter filter : filters) {
            List<FilterCriterion> newFCs = filter.getFilterCriteria();
            if (newFCs != null && newFCs.size() > 0) {
                View view = filter.getView();

                Filter currentFilter = findByUserIdAndViewId(user.getId(), view.getId());
                if (currentFilter != null) {
                    getPrimaryDAO().delete(currentFilter);
                }
                currentFilter = filter;
                currentFilter.setUser(user);
                getPrimaryDAO().save(currentFilter);
            }
        }
    }

    /**
     * Updates a specific filter criterion.
     * @param criterion The new criterion object.
     * @param critId The criterion's id.
     * @param user The user for which this criterion exists.
     * @param viewId The viewId of the view for this criterion.
     * @see com.vocollect.epp.service.FilterManager#updateFilterCriterion(com.vocollect.epp.model.FilterCriterion, java.lang.Long, com.vocollect.epp.model.User, long)
     * @throws DataAccessException if the Filter object cannot be found or updated.
     */
    public void updateFilterCriterion(FilterCriterion criterion, Long critId, User user, long viewId)
                                    throws DataAccessException {
        Filter filter = getPrimaryDAO().findByUserIdAndViewId(user.getId(), viewId);

        filter.updateFilterCriteria(criterion, critId);

        getPrimaryDAO().save(filter);
    }

    /**
     * Sets the UserPreferencesManager.
     * @param userPreferencesManager new userpreferencesmanager
     */
    public void setUserPreferencesManager(UserPreferencesManager userPreferencesManager) {
        this.userPreferencesManager = userPreferencesManager;
    }


    /**
     * Setter for the columnDAO property.
     * @param columnDAO the new columnDAO value
     */
    public void setColumnDAO(ColumnDAO columnDAO) {
        this.columnDAO = columnDAO;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.FilterManager#constructFilterFromSerializedJSONList<(String>)
     */
    public List<Filter> constructFilterFromSerializedJSON(String serializedJSONObjects)
        throws JSONException, DataAccessException {
        // We are only going to fill in the necessary aspects of the filter, filter criterion
        // and operand in order to properly produce the HQL for the filtering

        List<Filter> filterList = new LinkedList<Filter>();

        if (serializedJSONObjects != null && serializedJSONObjects.length() > 0) {
            JSONArray jsonArray = new JSONArray("[" + serializedJSONObjects + "]");
            for (int index = 0; index < jsonArray.length(); index++) {
                JSONObject jsonObject = jsonArray.getJSONObject(index);

                // We need to get the Column ID and Operand ID and query for the real
                // object - mostly to get the field name and operand type
                Long viewId = jsonObject.getLong(FilterCriterion.VIEW_ID);

                FilterCriterion filterCriterion = constructFilterCriterion(
                    viewId,
                    jsonObject.getLong(FilterCriterion.COLUMN_ID),
                    jsonObject.getLong(FilterCriterion.OPERAND_ID),
                    jsonObject.getString(FilterCriterion.VALUE1),
                    jsonObject.getString(FilterCriterion.VALUE2),
                    jsonObject.getBoolean(FilterCriterion.LOCKED));

                View view = userPreferencesManager.getView(viewId);
                FilterUtil.appendFilterToView(filterList, view, filterCriterion);
            }
        }
        return filterList;
    }

    /**
     * {@inheritDoc}
     */
    public FilterCriterion constructFilterCriterion(Long viewId,
                                               Long columnId,
                                               Long operandId,
                                               String val1,
                                               String val2,
                                               boolean locked)
    throws DataAccessException {
        Column column = columnDAO.get(columnId);
        Operand operand = OperandSetup.getOperandById(operandId);
        return new FilterCriterion(column, operand, val1, val2, locked);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.FilterManager#findByUserIdAndViewId(long, long)
     */
    public Filter findByUserIdAndViewId(long userId, long viewId) {
        return getPrimaryDAO().findByUserIdAndViewId(userId, viewId);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 