/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.util.ResultDataInfo;

import java.util.Collection;
import java.util.List;


/**
 * A GenericManager implementation that other service objects can inherit.
 * @param <T> The (primary) domain model type associated with this manager
 * @param <DAO> The (primary) DAO associated with this manager. It must be
 * a DAO for the primary domain model type.
 *
 * @author ddoubleday
 */
public abstract class GenericManagerImplRoot<T, DAO extends GenericDAO<T>>
    extends RemovableDataProviderImpl implements GenericManager<T, DAO> {

    private static final Logger log = new Logger(GenericManagerImpl.class);

    // The primary DAO, the one with which the GenericManager was instantiated.
    // Subclasses of this may include other DAOs.
    private DAO primaryDAO;

    /**
     * Constructor.
     * @param primaryDAO the primary DAO for this Manager. Subclasses can
     * add additional DAOs if needed.
     */
    public GenericManagerImplRoot(DAO primaryDAO) {
        this.primaryDAO = primaryDAO;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.RemovableDataProviderImpl#delete(java.lang.Long)
     * @return this implementation returns null in all cases, unless overridden.
     */
    @Override
    public Object delete(Long id) throws BusinessRuleException, DataAccessException {
        T instance = getPrimaryDAO().get(id);
        return delete(instance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#delete(java.lang.Object)
     * @return this implementation returns null in all cases, unless overridden.
     */
    public Object delete(T instance) throws BusinessRuleException, DataAccessException {
        this.primaryDAO.delete(instance);

        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#getCount()
     */
    public long getCount() throws DataAccessException {
        return this.primaryDAO.getCount();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#queryByExample(com.vocollect.epp.model.BaseModelObject)
     */
    public List<T> queryByExample(T exampleInstance) throws DataAccessException {
        return this.primaryDAO.queryByExample(exampleInstance);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#get(java.lang.Long)
     */
    public T get(Long id) throws DataAccessException {
        return this.primaryDAO.get(id);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#getAll()
     */
    public List<T> getAll() throws DataAccessException {
        return this.primaryDAO.getAll();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#getAll()
     */
    public List<? extends DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        return this.primaryDAO.getAll(rdi);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#save(java.lang.Object)
     * @return this implementation returns null in all cases, unless overridden.
    */
    public Object save(T instance) throws BusinessRuleException, DataAccessException {
        this.primaryDAO.save(instance);
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#saveAll(java.lang.Object)
     * @return this implementation returns null in all cases, unless overridden.
    */
    public Object save(List < T > instances) throws BusinessRuleException,
                                        DataAccessException {
        this.primaryDAO.save(instances);
        return null;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.GenericManager#getPrimaryDAO()
     */
    public DAO getPrimaryDAO() {
        return this.primaryDAO;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.DataProvider#getProviderDAO()
     */
    public DataProviderDAO getProviderDAO() {
        return getPrimaryDAO();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.RemovableDataProvider#getDataObject(java.lang.Long)
     */
    public DataObject getDataObject(Long id) throws DataAccessException {
        return (DataObject) get(id);
    }

    /**
     * Throws a BusinessRuleException with the message.
     * @param messageKey the resource key to put in the exception message.
     * @throws BusinessRuleException with the message.
     */
    protected void throwBusinessRuleException(String messageKey)
        throws BusinessRuleException {

        BusinessRuleException e = new BusinessRuleException();
        e.addMessage(new UserMessage(messageKey));
        throw e;
    }

    /**
     * Hibernate can return null from queries that count or sum things, so
     * this convenience function allows passing in an Object (that is assumed
     * to be a Number, for convenience) and getting back a 0 if the Object is
     * null, or the int representation otherwise.
     * @param o - generic object that should be instance of Number
     * @return - return int value of Number, or 0 if null.
     */
    protected int convertNumberToInt(Object o) {
        if (o == null) {
            return 0;
        } else {
            return ((Number) o).intValue();
        }
    }

    /**
     * Where clause object to facilitate adding lines to the where clause.
     * @author mnichols
     */
    public class WhereClause {
        StringBuilder whereClause = null;

        /**
         * Constructor.
         */
        public WhereClause() {
            whereClause = new StringBuilder("");
        }

        /**
         * Add a line to the where clause.
         * @param obj The object to add (must be a string, numeric or collection)
         * @param sql The string representing the object in the where clause
         */
        public void add(Object obj, String sql) {
            add(obj, sql, true, false);
        }

        /**
         * Overloaded method for complex queries
         * @param obj The object to add (must be a string, numeric or
         *            collection)
         * @param sql The string representing the object in the where clause
         * @param isAndCondition if true adds a <code>and</code> otherwise adds
         *            <code>or</code> to the query
         * @param isLastCondition if true add a closing bracket
         */
        public void add(Object obj,
                        String sql,
                        boolean isAndCondition,
                        boolean isLastCondition) {
            if (whereClause != null) {
                if (obj != null) {
                    if (whereClause.length() != 0) {
                        whereClause.append(isAndCondition ? " and " : " or ");
                    }
                    if (obj instanceof String) {
                        whereClause.append(" " + sql + " = '" + (String) obj
                            + "'");
                    } else if (obj instanceof Collection<?>) {
                        Collection<?> coll = (Collection<?>) obj;
                        whereClause.append(" " + sql + " in (");
                        for (Object object : coll) {
                            if (object != null) {
                                if (object instanceof Collection<?>) {
                                    log.warn("Collection member of collection "
                                        + "not handled. Object: "
                                        + object.toString()
                                        + " will not be processed");
                                } else if (object instanceof String) {
                                    whereClause.append("'" + (String) object
                                        + "'" + ",");
                                } else {
                                    whereClause.append(object.toString() + ",");
                                }
                            }
                        }
                        whereClause.setLength(whereClause.length() - 1);
                        whereClause.append(")");
                    } else {
                        whereClause.append(" " + sql + " = " + obj.toString());
                    }

                    if (isLastCondition) {
                        whereClause.append(" )");
                    }
                }
            }
        }

        /**
         * {@inheritDoc}
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return whereClause.toString();
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 