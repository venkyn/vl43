/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.LoggingError;
import com.vocollect.epp.model.FileView;
import com.vocollect.epp.service.LogManager;
import com.vocollect.epp.service.OtherFileException;
import com.vocollect.epp.service.UnreadableFileException;
import com.vocollect.epp.util.StringUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Class to implement the interface LogManager.
 * @see com.vocollect.epp.service.LogManager
 * @author dgold
 */
public abstract class LogManagerImplRoot extends DataProviderImpl implements LogManager {

    /**
     * Logger to send messages to.
     */
    private static com.vocollect.epp.logging.Logger theLog
        = new com.vocollect.epp.logging.Logger(LogManagerImpl.class.getName());

    /**
     * The current number of lines read in the file.
     */
    private int fileLinesNum;

    private DataProviderDAO dao;

    /**
     * Gets the last line number read by getFileLines.
     * @see com.vocollect.epp.service.LogManager#getFileLineNum()
     * @return - an integer representing the current line number that has been read
     */
    public int getFileLineNum() {
        return (fileLinesNum);
    }

    /**
     * Open the selected file, grab the contents and put them in a local string.
     * For reading in the browser.
     * @param fileName - The fileContents to set.
     * @return - string containing file contents
     * @see com.vocollect.epp.service.LogManager
     * @throws UnreadableFileException - occurs when file cannot be read
     * @throws OtherFileException - occurs when file has unknown exception
     */
    public List<String> getFileLines(String fileName)
        throws UnreadableFileException, OtherFileException {

        File fileSource = new File(fileName);
        try {
            //I18N Change: 11/09/2006 by mlashinsky for ability for logs to be properly displayed
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileSource), "UTF8"));
            List<String> contents = new LinkedList<String>();
            String temp = null;
            int ct = 0;
            while ((temp = reader.readLine()) != null) {
                ct++;
                fileLinesNum = ct;
                temp = StringUtil.dustForHTML(temp);
                contents.add(temp);
            }
            reader.close();
            return contents;
        } catch (FileNotFoundException e) {
            theLog.error("[LogManagerImpl.getFileLines] File Not Found '" + fileSource.getName() + "'."
                + e.getMessage(), LoggingError.FILE_NOT_FOUND, e);
            UnreadableFileException urf = new UnreadableFileException(LoggingError.FILE_NOT_FOUND,
                new UserMessage(LoggingError.FILE_NOT_FOUND, new Object[] { fileName }), e);
            throw urf;
        } catch (IOException e) {
            theLog.error("[LogManagerImpl.getFileLines] File IO Error '" + fileSource.getName() + "'."
                + e.getMessage(), LoggingError.FILE_IO_ERROR, e);
            UnreadableFileException ofe = new UnreadableFileException(LoggingError.FILE_IO_ERROR,
                new UserMessage(LoggingError.FILE_IO_ERROR, new Object[] { fileName }), e);
            throw ofe;
        }
    }

    /**
     * Get the list of files from the directory name passed in.
     * @param directory name of the directory to look in for the log files
     * @return List of FileView objects representing the pertinent info for each
     *         file in the directory
     * @see com.vocollect.epp.service.LogManager
     * @see com.vocollect.epp.model.FileView
     * @throws UnreadableFileException - occurs when the directory does not exist
     */
    public List<FileView> getLogFiles(String directory) throws UnreadableFileException {
        LinkedList<FileView> logs = new LinkedList<FileView>();
        File fileRoot = new File(directory);

        if (!(fileRoot.exists() && fileRoot.isDirectory())) {
            throw new UnreadableFileException(LoggingError.FILE_IO_ERROR, new UserMessage(
                LoggingError.FILE_IO_ERROR, new Object[] { directory }), null);
        }
        File[] logFiles = fileRoot.listFiles();

        // For every file in logFiles, do a conversion to a FileView
        for (int i = 0; i < logFiles.length; i++) {
            if (logFiles[i].isDirectory()) {
                continue;
            }
            logs.add(new FileView(logFiles[i]));
        }
        return logs;
    }

    /**
     * Sets the logging DAO.
     * @param logDAO the data access object to set
     * @see com.vocollect.epp.service.DataProvider#setProviderDAO()
     */
    public void setLoggingDAO(DataProviderDAO logDAO) {
        this.dao = logDAO;
    }

    /**
     * This method is not used since application logs are not backed
     * by DAOs.
     * @return (DataProviderDAO) the DAO
     * @see com.vocollect.epp.service.DataProvider#getProviderDAO()
     */
    public DataProviderDAO getProviderDAO() {
        return this.dao;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 