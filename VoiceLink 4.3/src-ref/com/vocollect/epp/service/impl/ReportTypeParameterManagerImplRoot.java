/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ReportTypeParameterDAO;
import com.vocollect.epp.model.ReportTypeParameter;
import com.vocollect.epp.service.ReportTypeParameterManager;


/**
 * 
 *
 * @author mnichols
 */
public class ReportTypeParameterManagerImplRoot extends GenericManagerImpl<ReportTypeParameter, ReportTypeParameterDAO>
    implements ReportTypeParameterManager {
    
    /**
     *
     * Constructor.
     * @param reportDAO The report DAO; constructed from Spring
     */
    public ReportTypeParameterManagerImplRoot(ReportTypeParameterDAO reportDAO) {
        super(reportDAO);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 