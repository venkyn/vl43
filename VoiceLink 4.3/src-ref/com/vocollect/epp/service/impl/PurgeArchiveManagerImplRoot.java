/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.PurgeArchiveException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.scheduling.PurgeResult;
import com.vocollect.epp.service.JobManager;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.PurgeArchiveManager;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Additional service methods for the Purge Archive Service.
 * 
 * @author mlashinsky
 */
public abstract class PurgeArchiveManagerImplRoot implements
    PurgeArchiveManager {

    /**
     * Definition of purge archive rule.
     */
    // CHECKSTYLE:OFF don't require strict to have getters
    protected class Rule {

        public String ruleName;

        public String modelObjectName;

        public String statusName;

        public String action;

        public Date purgeDate;

        public boolean archive = false;
    }
    // CHECKSTYLE:ON

    private static final int NUMBER_OF_RULE_TOKENS = 3;

    private static final int RULE_TOKEN_OBJECT = 0;

    private static final int RULE_TOKEN_STATUS = 1;

    private static final int RULE_TOKEN_ACTION = 2;

    protected static final String RULE_ACTION_PURGE = "Purge";

    protected static final String RULE_ACTION_ARCHIVE = "Archive";

    // **************************************************************
    // ********************TRICKLE PURGE SETTINGS********************
    // **************************************************************

    // Factor for milliseconds in a day
    protected static final long MILLISEC_CONVERSION_FACTOR = 86400000;

    // **********************************************************
    // ********************DAO's and MANAGERS********************
    // **********************************************************

    // System Properties Object used to get systemProperty rules
    private SystemPropertyManager systemPropertyManager;

    // Site Data Access Object
    private SiteManager siteManager;

    private Map<Long, Site> siteMap;

    // Declare and instantiate a logger
    private static final Logger log = new Logger(PurgeArchiveManagerImpl.class);

    // Notification manager
    private NotificationManager notificationManager;

    // Job Manager
    private JobManager jobManager;
    
    private int batchSize;
    
    private int batchInterval;
    
    /**
     * Getter for the batchInterval property.
     * @return int value of the property
     */
    public int getBatchInterval() {
        return batchInterval;
    }

    
    /**
     * Setter for the batchInterval property.
     * @param batchInterval the new batchInterval value
     */
    public void setBatchInterval(int batchInterval) {
        this.batchInterval = batchInterval;
    }

    
    /**
     * Getter for the batchSize property.
     * @return int value of the property
     */
    public int getBatchSize() {
        return batchSize;
    }

    
    /**
     * Setter for the batchSize property.
     * @param batchSize the new batchSize value
     */
    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    /**
     * Getter for the jobManager property.
     * @return JobManager value of the property
     */
    public JobManager getJobManager() {
        return jobManager;
    }

    /**
     * Setter for the jobManager property.
     * @param jobManager the new jobManager value
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException 
     * @see com.vocollect.epp.service.PurgeArchiveManagerRoot#purgeArchive()
     */
    public PurgeResult purgeArchive() throws PurgeArchiveException, DataAccessException {
        QueryDecorator queryDecorator = setupSiteAndDecorator();

        if (queryDecorator == null) {
            return null;
        }
        
        // Get a list of rules to process
        PurgeResult totals = new PurgeResult();
        List<Rule> rules = getPurgeArchiveRules();
        // process the rules
        for (Rule rule : rules) {
            PurgeResult result = null;
            try {
                result = processRule(queryDecorator, rule);
            } catch (DataAccessException e) {
                log.error("Error purging " + rule.modelObjectName, SystemErrorCode.PURGE_ARCHIVE_FAILURE, e);
                throw e;
            }
            // form a total of the results
            if (result != null) {
                totals.add(result);
            }
        }
        
        return totals;
    }

    /**
     * 
     * Sets the sitemap up and creates a query decorator holding the batch size.
     * @return QueryDecorator to be used in purge queries
     */
    protected QueryDecorator setupSiteAndDecorator() {
        // Grant all site access to Purge Archive Feature
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        //siteContext.setAllSiteAccess(true);
        siteContext.setFilterBySite(false);
        SiteContextHolder.setSiteContext(siteContext);

        // Map to hold Site Information using iterator
        Map<Long, Site> siteInfoMap = new HashMap<Long, Site>();
        List<Site> siteList = null;
        try {
            siteList = getSiteManager().getAll();
        } catch (DataAccessException e) {
            log.warn("Error getting site information: " + e + "\n");
            return null;
        }

        // Populate the map
        for (Site s : siteList) {
            siteInfoMap.put(s.getId(), s);
        }

        this.setSiteMap(siteInfoMap);

        // Instantiate query decorator and assign it default trickle purge factor
        // for the number of rows it retrieves
        QueryDecorator queryDecorator = new QueryDecorator();
        queryDecorator.setRowCount(getBatchSize());
        return queryDecorator;
    }

    /**
     * Check system properties for purge archive rules to process.
     * 
     * @return - a list of rules to process
     */
    protected List<Rule> getPurgeArchiveRules() {
        List<Rule> rules = new ArrayList<Rule>();

        // Get the Rules for Archiving and Purging from SystemProperty Table
        try {
            List<SystemProperty> systemPropertyList = systemPropertyManager
                .getAll();

            // Build list of rules
            for (SystemProperty sp : systemPropertyList) {
                String[] tokens = sp.getName().split("_");
                // Check for correct number of tokens
                if (tokens.length == NUMBER_OF_RULE_TOKENS) {
                    // check if last token is purge or archive
                    if (tokens[RULE_TOKEN_ACTION].equals(RULE_ACTION_PURGE)
                        || tokens[RULE_TOKEN_ACTION]
                            .equals(RULE_ACTION_ARCHIVE)) {
                        Rule rule = new Rule();

                        rule.ruleName = sp.getName();
                        rule.modelObjectName = tokens[RULE_TOKEN_OBJECT];
                        rule.statusName = tokens[RULE_TOKEN_STATUS];
                        rule.action = tokens[RULE_TOKEN_ACTION];
                        rule.purgeDate = calcDaysPast(Integer.parseInt(sp
                            .getValue()));
                        rules.add(rule);

                    }
                }
            }

            // Set archive flag for rule
            for (Rule r1 : rules) {
                if (r1.action.equals(RULE_ACTION_PURGE)) {
                    for (Rule r2 : rules) {
                        if (r1.modelObjectName.equals(r2.modelObjectName)
                            && r1.statusName.equals(r2.statusName)
                            && r2.action.equals(RULE_ACTION_ARCHIVE)) {
                            r1.archive = true;
                        }
                    }
                }
            }

        } catch (DataAccessException e) {
            log.warn("Error getting purge archive rules from database: " + e
                + "\n");
        }

        return rules;
    }

    /**
     * try to sleep to allow other process to continue.
     * 
     * @param listSize - size of list that was just processed
     */
    protected void purgeArchiveSleep(int listSize) {
        try {
            if (listSize > 0) {
                Thread.sleep(getBatchInterval());
            }
        } catch (InterruptedException e) {
            log
                .warn("Purge Archive process could not sleep for trickle purge interval, resuming purge.");
        }
    }

    /**
     * Calculate date that all items must be purged or purged and archived.
     * @param days number of days passed since object entered system
     * @return purge date anything equal to or older than this date is purged
     *         and or archived
     */
    private Date calcDaysPast(int days) {
        // For each rule construct a date that is today minus _ days
        // Meaning if it was created three days ago it goes today.

        // Get the date right now
        Date now = new Date();

        // Convert to milliseconds
        long nowMilli = now.getTime();

        // Get the created date in milliseconds
        long deleteOlderThanMilli = nowMilli
            - (days * MILLISEC_CONVERSION_FACTOR);

        // Date Object to mark records as purge-able
        Date purgeDate = new Date(deleteOlderThanMilli);

        return purgeDate;
    }

    /**
     * Getter for the siteManager property.
     * @return SiteManager value of the property
     */
    public SiteManager getSiteManager() {
        return siteManager;
    }

    /**
     * Setter for the siteManager property.
     * @param siteManager the new siteManager value
     */
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the siteMap property.
     * @return Map value of the property
     */
    public Map<Long, Site> getSiteMap() {
        return siteMap;
    }

    /**
     * Setter for the siteMap property.
     * @param siteMap the new siteMap value
     */
    public void setSiteMap(Map<Long, Site> siteMap) {
        this.siteMap = siteMap;
    }

    /**
     * method to process a rule. This methos should be overridden to add custom
     * rules. The super should still be called.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     * @return the result of the purge (may be null if no result needed.
     * @throws DataAccessException on failure to access database.
     * @throws PurgeArchiveException on other purge/archive failure.
     */
    protected PurgeResult processRule(QueryDecorator queryDecorator, Rule rule) 
            throws PurgeArchiveException, DataAccessException {

        // TODO: Change the True condition to be master application
        if (true) {
            if (rule.modelObjectName.equals("Notification")) {
                // Purge and archive(if needed) transaction data
                if (log.isDebugEnabled()) {
                    log.debug("### Starting Notification Purge Process :::");
                }
                if (rule.action.equals(RULE_ACTION_PURGE)) {
                    purgeNotificationData(queryDecorator, rule);
                }
                if (log.isDebugEnabled()) {
                    log.debug("### Notification Purge Process Complete :::");
                }
            } else if (rule.modelObjectName.equals("JobHistory")) {
                if (log.isDebugEnabled()) {
                    log.debug("### Starting Job History Purge Process :::");
                }
                // Purge and archive(if needed) transaction data
                if (rule.action.equals(RULE_ACTION_PURGE)) {
                    purgeJobHistoryData(queryDecorator, rule);
                }
                if (log.isDebugEnabled()) {
                    log.debug("### Job History Purge Process Complete :::");
                }
            }
            
        }
        return null;
    }

    /**
     * Purge Job History Data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeJobHistoryData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        do {
            records = getJobManager().executePurge(
                queryDecorator,
                rule.purgeDate);
            purgeArchiveSleep(records);
            
        } while (records == getBatchSize());
    }

    /**
     * Purge Notification Data.
     * 
     * @param queryDecorator - query decorator determining number of records to
     *            process
     * @param rule - rule to process
     */
    private void purgeNotificationData(QueryDecorator queryDecorator, Rule rule) {
        int records = 0;
        do {
            records = getNotificationManager().executePurge(
                queryDecorator,
                rule.purgeDate);
            purgeArchiveSleep(records);
            
        } while (records == getBatchSize());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 