/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ExternalJobDAO;
import com.vocollect.epp.dao.ExternalJobSystemTranslationDAO;
import com.vocollect.epp.dao.JobHistoryDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.model.ExternalJob;
import com.vocollect.epp.model.ExternalJobSystemTranslation;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.JobHistory;
import com.vocollect.epp.model.ScheduleType;
import com.vocollect.epp.scheduling.ExternalJobRunner;
import com.vocollect.epp.service.ExternalJobManager;
import com.vocollect.epp.service.JobManager;

import java.util.List;
import java.util.Locale;

import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;

/**
 * 
 * 
 * @author khazra
 */
public class ExternalJobManagerImplRoot extends
    GenericManagerImpl<ExternalJob, ExternalJobDAO> implements
    ExternalJobManager {

    private JobManager jobManager;

    private JobHistoryDAO jobHistoryDAO;

    private ExternalJobSystemTranslationDAO externalJobSystemTranslationDAO;

    /**
     * Constructor.
     * @param primaryDAO The ExternalJobDAO
     */
    public ExternalJobManagerImplRoot(ExternalJobDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * @return the jobManager
     */
    public JobManager getJobManager() {
        return jobManager;
    }

    /**
     * @param jobManager the jobManager to set
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * @return the jobHistoryDAO
     */
    public JobHistoryDAO getJobHistoryDAO() {
        return jobHistoryDAO;
    }

    /**
     * @param jobHistoryDAO the jobHistoryDAO to set
     */
    public void setJobHistoryDAO(JobHistoryDAO jobHistoryDAO) {
        this.jobHistoryDAO = jobHistoryDAO;
    }

    @Override
    public Object delete(Long id) throws BusinessRuleException,
        DataAccessException {

        ExternalJob ej = getPrimaryDAO().get(id);

        // Delete all the Job and Job History associated to the external job

        // We wanted to reduce changes to EPP, thats why we did not write a
        // getByName method in JobManager.
        List<Job> jobs = this.jobManager.getAll();

        for (Job job : jobs) {
            if (job.getName().equals(ej.getName())) {

                // Don't delete the job if it is enabled.
                if (job.getEnabled()) {
                    throwBusinessRuleException("externalJob.delete.error.jobEnabled");
                }

                // Try unscheduling the job before deleting
                try {
                    this.getJobManager().getPrimaryDAO().getScheduler()
                        .unscheduleJob(job.getName(), job.getGroup());
                } catch (SchedulerException e) {
                    throw new BusinessRuleException(e);
                }

                // Delete all job history
                List<JobHistory> jobHistorys = this.jobHistoryDAO
                    .getHistory(job.getId());

                for (JobHistory jobHistory : jobHistorys) {
                    this.jobHistoryDAO.delete(jobHistory);
                }

                // Delete the EPP half
                jobManager.delete(job);
                break;
            }
        }

        return super.delete(id);
    }

    @Override
    public Object save(ExternalJob instance) throws BusinessRuleException,
        DataAccessException {
        boolean isNew = instance.isNew();

        ExternalJob externalJob = this
            .findExternalJobByName(instance.getName());

        if (isNew && externalJob != null) {
            throw new FieldValidationException("externalJob.name",
                instance.getName(), new UserMessage(
                    "externalJob.error.duplicateName", instance.getName()
                        .toString()));
        }
        getPrimaryDAO().detach(externalJob);

        Object obj = super.save(instance);
        if (isNew) {
            // Create the EPP half and attach it to the scheduler
            Job job = createAndSynchronizeExternalJob(instance.getName());
            this.jobManager.saveJob(job);

            // Create the system translation entry, otherwise Admin > Schedule
            // page will not show the job
            List<ExternalJobSystemTranslation> vst = this
                .getExternalJobSystemTranslationDAO().listTranslationByKey(
                    instance.getName(), "en_US");

            if (vst == null || vst.size() == 0) {
                this.getExternalJobSystemTranslationDAO().save(
                    getJobTranslation(instance.getName()));
            }
        }
        return obj;
    }

    /**
     * method to create default job object
     * @param jobName - the job name
     * @return Job object
     */
    private Job createAndSynchronizeExternalJob(String jobName)
        throws DataAccessException, BusinessRuleException {

        // Set default interval for all jobs to 5 min
        final long interval = 5 * 60 * 1000;
        // Set ping interval for all jobs to 10 sec
        final long pingInterval = 10 * 1000;

        // Create the EPP job object for the external job
        Job job = new Job();
        job.setName(jobName);
        job.setDisplayName(jobName);
        job.setEnabled(Boolean.TRUE);
        job.setInterval(interval);
        job.setType(ScheduleType.INTERVAL);
        job.setJobClass(ExternalJobRunner.class.getCanonicalName());
        job.setCronHrs("24Hr");
        job.setPingInterval(pingInterval);

        // Create a trigger for the job
        Trigger trigger = new SimpleTrigger(job.getName(),
            Job.PUBLIC_GROUP_NAME, job.getRepeatCount(), job.getInterval());

        // Try to schedule the job
        try {
            this.getJobManager().getPrimaryDAO().getScheduler()
                .scheduleJob(job.getJobDetail(), trigger);
        } catch (SchedulerException e) {
            throw new BusinessRuleException(e);
        }

        return job;
    }

    /**
     * Method to create corresponding job translation
     * @param jobName - the job name
     * @return voicelink translation
     */
    private ExternalJobSystemTranslation getJobTranslation(String jobName) {
        ExternalJobSystemTranslation trans = new ExternalJobSystemTranslation();
        trans.setCode(0);
        trans.setTranslation(jobName);
        trans.setKey(jobName);
        trans.setLocale(Locale.US.toString());

        return trans;
    }

    /**
     * @return - the voicelink system translation dao
     */
    public ExternalJobSystemTranslationDAO getExternalJobSystemTranslationDAO() {
        return externalJobSystemTranslationDAO;
    }

    /**
     * 
     * @param externalJobSystemTranslationDAO - the external job system translation
     *            dao
     */
    public void setExternalJobSystemTranslationDAO(ExternalJobSystemTranslationDAO externalJobSystemTranslationDAO) {
        this.externalJobSystemTranslationDAO = externalJobSystemTranslationDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.voicelink.core.service.ExternalJobManagerRoot#
     * findExternalJobByName(java.lang.String)
     */
    @Override
    public ExternalJob findExternalJobByName(String name)
        throws DataAccessException {
        return this.getPrimaryDAO().findExternalJobByName(name);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 