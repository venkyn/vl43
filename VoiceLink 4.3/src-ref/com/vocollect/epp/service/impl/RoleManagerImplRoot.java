/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.RoleDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.FeatureGroup;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.service.RoleManager;

import java.util.List;

/**
 * Implementation of RoleManager interface.
 *
 * @author Dennis Doubleday
 */
public abstract class RoleManagerImplRoot extends GenericManagerImpl<Role, RoleDAO>
    implements RoleManager {

    private GenericDAO<Feature> featureDAO;
    private GenericDAO<FeatureGroup> featureGroupDAO;


    /**
     * Constructor.
     * @param primaryDAO the primary DAO
     * (the instantiation for the persistent class)
     */
    public RoleManagerImplRoot(RoleDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.RoleManager#getFeature(java.lang.Long)
     */
    public Feature getFeature(Long featureId) throws DataAccessException {
        return this.featureDAO.get(featureId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.RoleManager#getFeatureGroups()
     */
    public List<FeatureGroup> getFeatureGroups() throws DataAccessException {
        return this.featureGroupDAO.getAll();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.RoleManager#findByName(java.lang.String)
     */
    public Role findByName(String rolename) throws DataAccessException {
        return getPrimaryDAO().findByName(rolename);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Role role)
        throws BusinessRuleException, DataAccessException {

        throwExceptionOnAdminRole(role, "role.delete.error.administrative");
        throwExceptionOnInUse(role, "role.delete.error.inUse");
        return super.delete(role);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(Role role)
        throws BusinessRuleException, DataAccessException {

        throwExceptionOnAdminRole(role, "role.edit.error.administrative");
        return super.save(role);
    }

    /**
     * Set the FeatureDAO used by this service.
     * @param newDao - the data access object of the service
     */
    public void setFeatureDAO(GenericDAO<Feature> newDao) {
        this.featureDAO = newDao;
    }

    /**
     * Set the FeatureGroupDAO used by this service.
     * @param newDao - the data access object of the service
     */
    public void setFeatureGroupDAO(GenericDAO<FeatureGroup> newDao) {
        this.featureGroupDAO = newDao;
    }

    /**
     * Throws an exception if the Role being operated on is the admin role.
     * @param role the role being operated on.
     * @param messageKey the resource key to put in the exception message.
     * @throws BusinessRuleException if the role is Administrative.
     */
    protected void throwExceptionOnAdminRole(Role role, String messageKey)
            throws BusinessRuleException {
        if (role.getIsAdministrative()) {
            // Not allowed to modify the Administrative role
            throwBusinessRuleException(messageKey);
        }
    }

    /**
     * Throws an exception if the Role being deleted is in use.
     * @param role the role being operated on.
     * @param messageKey the resource key to put in the exception message.
     * @throws BusinessRuleException if the role is Administrative.
     */
    protected void throwExceptionOnInUse(Role role, String messageKey)
            throws BusinessRuleException {
        if (role.getUserCount() > 0) {
            // Not allowed to delete a role that's in use.
            throwBusinessRuleException(messageKey);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 