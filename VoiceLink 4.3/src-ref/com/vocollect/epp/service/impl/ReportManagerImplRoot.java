/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ReportDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Report;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.service.ReportManager;
import com.vocollect.epp.service.ReportParameterManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.exception.ConstraintViolationException;


/**
 * Additional service methods for the <code>Report</code> model object.
 *
 * @author mnichols
 */
public abstract class ReportManagerImplRoot extends
    GenericManagerImpl<Report, ReportDAO> implements ReportManager {

    private ReportParameterManager reportParameterManager = null;
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReportManagerImplRoot(ReportDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id)
        throws BusinessRuleException, DataAccessException {
        Report report = get(id);
        return delete(report);
    }     

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Report instance)
        throws BusinessRuleException, DataAccessException {
        try {            
            return super.delete(instance);
        } catch (DataAccessException ex) {
            if (ex.getCause() instanceof ConstraintViolationException) {
                throwBusinessRuleException("report.delete.error.inUse");
            } else {
                throw ex;
            }
        }

        return null;
     }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.util.List)
     */
    @Override
    public Object save(List<Report> instances) throws BusinessRuleException,
        DataAccessException {

        return super.save(instances);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImplRoot#save(java.lang.Object)
     */
    @Override
    public Object save(Report instance) throws BusinessRuleException,
        DataAccessException {
        // If the report exists, remove all of the parameters
        // before creating new ones.
        if (instance.getId() != null) {
            Iterator<ReportParameter> iter = instance.getOldReportParameters().iterator();
            while (iter.hasNext()) {
                ReportParameter param = iter.next();
                reportParameterManager.delete(param.getId());
            } 
        }
        if (instance.getReportParameters().size() > 0) {           
            reportParameterManager.save(new ArrayList<ReportParameter>(instance.getReportParameters()));
        }
        return super.save(instance);
    }

    
    /**
     * Getter for the reportParameterManager property.
     * @return the value of the property
     */
    public ReportParameterManager getReportParameterManager() {
        return this.reportParameterManager;
    }

    
    /**
     * Setter for the reportParameterManager property.
     * @param reportParameterManager the new reportParameterManager value
     */
    public void setReportParameterManager(ReportParameterManager reportParameterManager) {
        this.reportParameterManager = reportParameterManager;
    }
 
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 