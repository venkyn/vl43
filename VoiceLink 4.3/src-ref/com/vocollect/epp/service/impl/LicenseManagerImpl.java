/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.LicenseAuthenticationException;
import com.vocollect.epp.exceptions.LicenseFormatException;
import com.vocollect.epp.licensing.VoiceLicense;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.SystemLicense;
import com.vocollect.epp.service.LicenseManager;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.DocLicenseAction;

import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;


/**
 * 
 *
 * @author mressler
 */
public class LicenseManagerImpl 
    extends GenericManagerImpl<SystemLicense, GenericDAO<SystemLicense>> 
    implements LicenseManager {

    public static final String BEAN_NAME = "systemLicenseManager";
    
    private NotificationManager notificationManager;
    private Properties serverProperties;
    private SiteContext siteContext;
    
    // We start warning within this many days of expiration.
    private static final int WARNING_DAYS = -15;
    
    private static final long MILLISECS_PER_DAY = 24 * 60 * 60 * 1000;
    
    /**
     * Constructor.  Simply overrides the generic version to set the DAO.
     * @param primaryDAO
     */
    protected static final Logger log = new Logger(DocLicenseAction.class
            .getPackage().toString());
    
    /**
     * Constructor.
     * @param primaryDAO the DAO
     */
    public LicenseManagerImpl(GenericDAO<SystemLicense> primaryDAO) {
        super(primaryDAO);
    }

    
    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    
    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    
    /**
     * Getter for the serverProperties property.
     * @return Properties value of the property
     */
    public Properties getServerProperties() {
        return this.serverProperties;
    }


    
    /**
     * Setter for the serverProperties property.
     * @param serverProperties the new serverProperties value
     */
    public void setServerProperties(Properties serverProperties) {
        this.serverProperties = serverProperties;
    }


    
    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return this.siteContext;
    }


    
    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }


    /**
     * {@inheritDoc}
     * @throws DataAccessException 
     * @throws BusinessRuleException 
     * @see com.vocollect.epp.service.LicenseManager#getCurrentLicense()
     */
    @Override
    public SystemLicense getCurrentLicense()
    throws BusinessRuleException, DataAccessException {
        List<SystemLicense> allLicenses = getPrimaryDAO().getAll();
        
        if (!allLicenses.isEmpty()) {
            SystemLicense currentLicense = allLicenses.get(0);
        
            if (VoiceLicense.getCurrentLicense() == null) {
                try {
                    VoiceLicense.setCurrentLicense(new VoiceLicense(
                        currentLicense.getContents().getBytes(Charset.forName("utf-8"))));
                } catch (Exception e) {
                    log.error("The license being stored to the database is invalid!  This is bad!", 
                        null, e);
                }
            }
        
            return currentLicense;
        } else if (allLicenses.size() == 0) {
            return null; 
        } else {
            throw new BusinessRuleException(null, 
                new UserMessage("Multiple licenses were found in the database!"));
        }
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException 
     * @see com.vocollect.epp.service.LicenseManager#saveCurrentLicense(com.vocollect.epp.model.SystemLicense)
     */
    @Override
    public void saveCurrentLicense(SystemLicense currentLicense) 
    throws DataAccessException {
        log.debug("before getPrimaryDAO()");
        List<SystemLicense> allLicenses = getPrimaryDAO().getAll();
        //log.debug("after getPrimaryDAO()");
        // Delete all occurrences of other licenses (should only be one)
        log.debug("before iterating over all licenses");
        for (SystemLicense license : allLicenses) {
            getPrimaryDAO().delete(license);
        }
        log.info("****************************");
        log.info("before save");
        log.info("Type" + currentLicense.getType());
        log.info("DateEntered" + currentLicense.getDateEntered());
        log.info("****************************");
        
        getPrimaryDAO().save(currentLicense);
        log.info("after save");
        
        try {
            VoiceLicense.setCurrentLicense(new VoiceLicense(
                currentLicense.getContents().getBytes(Charset.forName("utf-8"))));
        } catch (Exception e) {
            log.error("the license that is being saved to the database is invalid!  This is bad!", null, e);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.LicenseManager#saveNotificationForLicenseStatus()
     */
    @Override
    public void saveNotificationForLicenseStatus() 
    throws DataAccessException, BusinessRuleException, 
        LicenseAuthenticationException, LicenseFormatException {
        Date currentDate = new Date();
        
        // Query for the license object
        SystemLicense license = getCurrentLicense();
        if (license != null) {
            VoiceLicense voiceLicense = new VoiceLicense(
                                        license.getContents().getBytes(Charset.forName("utf-8")));
            
            Notification notification = null;
            
            if (voiceLicense.getExpirationDate() != null) {
                Date expirationDate = voiceLicense.getExpirationDate();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(expirationDate);
                calendar.add(Calendar.DAY_OF_YEAR, WARNING_DAYS);
                
                // if the expirationDate is already later than the current date - 
                // we want to create a critical notification - but slightly different
                if (expirationDate.getTime() < currentDate.getTime()) {
                    long differenceInDays = 
                        (expirationDate.getTime() - currentDate.getTime()) 
                        / LicenseManagerImpl.MILLISECS_PER_DAY;
                    // Create a critical notification
                    notification = createLicenseNotification(
                        "notification.column.keyname.Process.2", 
                        "", 
                        "notification.column.keyname.Message.4", 
                        NotificationPriority.CRITICAL, 
                        "notification.detail.license.expired",
                        Long.toString(Math.abs(differenceInDays)));                    
                } else if (calendar.getTime().getTime() 
                            < currentDate.getTime()) {
                    // if the current date is within 15 days - we want to 
                    // create a critical notification
                    long differenceInDays = 
                        (expirationDate.getTime() - currentDate.getTime()) 
                        / LicenseManagerImpl.MILLISECS_PER_DAY;
                    // Create a critical notification
                    notification = createLicenseNotification(
                        "notification.column.keyname.Process.2", 
                        "", 
                        "notification.column.keyname.Message.3", 
                        NotificationPriority.CRITICAL, 
                        "notification.detail.license.expire",
                        Long.toString(differenceInDays));
                    
                } else {
                    // else if the current date is within 30 days - we want
                    // to create a warning notification
                    calendar.add(Calendar.DAY_OF_YEAR, WARNING_DAYS);
                    if (calendar.getTime().getTime() < currentDate.getTime()) {
                        long differenceInDays = 
                            (expirationDate.getTime() - currentDate.getTime())
                            / LicenseManagerImpl.MILLISECS_PER_DAY;
                        // Create a warning notification
                        notification = createLicenseNotification(
                            "notification.column.keyname.Process.2", 
                            "", 
                            "notification.column.keyname.Message.3", 
                            NotificationPriority.WARNING, 
                            "notification.detail.license.expire",
                            Long.toString(differenceInDays));
                    }
                }
            }
            
            if (notification != null) {
                getNotificationManager().save(notification);
            }
        }
    }
    
    /**
     * This method will create a notification object with the passed in information.
     * @param process the process message text attribute
     * @param errorNumber the error number, if any
     * @param message the message text attribute
     * @param notificationPriority the priority of the notification
     * @param detailMessage the detailed message of the notification
     * @param numberOfDaysUntilExpire days until expiration
     * @return Notification the actual created notification
     */
    private Notification createLicenseNotification(String process,
                                                   String errorNumber, 
                                                   String message, 
                                                   NotificationPriority notificationPriority, 
                                                   String detailMessage,
                                                   String numberOfDaysUntilExpire) {
        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());
        
        Notification notification = getNotificationManager()
                                        .systemNotification();
        
        notification.setProcess(process);
        notification.setErrorNumber(errorNumber);
        notification.setMessage(message);
        notification.setPriority(notificationPriority);
        notification.setCreationDateTime(new Date());

        NotificationDetail notificationDetail = new NotificationDetail();
        notificationDetail.setOrdering("0");
        notificationDetail.setKey(detailMessage);
        notificationDetail.setValue(numberOfDaysUntilExpire);
        notificationDetail.setRenderType("1");
        notification.getDetails().add(notificationDetail);

        return notification;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 