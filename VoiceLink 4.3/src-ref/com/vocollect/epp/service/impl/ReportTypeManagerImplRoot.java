/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ReportTypeDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportType;
import com.vocollect.epp.service.ReportTypeManager;

import java.util.List;


/**
 * 
 *
 * @author cblake
 */
public class ReportTypeManagerImplRoot extends GenericManagerImpl<ReportType, ReportTypeDAO>
    implements ReportTypeManager {
    
    /**
     *
     * Constructor.
     * @param reportDAO The report DAO; constructed from Spring
     */
    public ReportTypeManagerImplRoot(ReportTypeDAO reportDAO) {
        super(reportDAO);
    }
    
    /**
     * {@inheritDoc}
     * @param appType The substring of the applicatoin string
     */
    public List<ReportType> getAllByAppType(String appType) throws DataAccessException {
        return getPrimaryDAO().listByAppType(appType);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 