/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vocollect.epp.dao.TagDAO;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.service.TagManager;


/**
 *
 *
 * @author mlashinsky
 */
public abstract class TagManagerImplRoot extends GenericManagerImpl<Tag, TagDAO> implements TagManager {

    /**
     * Constructor.
     * @param primaryDAO the tag DAO.
     */
    public TagManagerImplRoot(TagDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.TagManager#findByTaggedObjectId(java.lang.Long,java.lang.Long)
     */
    public Tag findByTaggedObjectId(Long tagType, Long tagId) {
        return getPrimaryDAO().findByTaggedObjectId(tagType, tagId);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.TagManager#findSiteTags()
     */
    public List<Tag> findSiteTags() {
        return getPrimaryDAO().listSiteTags();
    }

    public void saveTags(Taggable taggedObj, List<Tag> tags) {
        Set<Tag> newTagsForObject = new HashSet<Tag>();

        for (Tag t : tags) {
            newTagsForObject.add(t);
        }
        taggedObj.setTags(newTagsForObject);
    }


    public void saveTag(Taggable taggedObj, Tag t) {
        if (null != taggedObj.getTags()) {
            taggedObj.getTags().clear();
        } else {
            taggedObj.setTags(new HashSet<Tag>());
        }

        taggedObj.getTags().add(t);
    }


    public void addTag(Taggable taggedObj, Tag t) {
        if (null == taggedObj.getTags()) {
            taggedObj.setTags(new HashSet<Tag>());
        }

        taggedObj.getTags().add(t);
    }

    public void deleteTag(Taggable taggedObj, Tag t) {
        if (null != taggedObj.getTags()) {
            taggedObj.getTags().remove(t);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 