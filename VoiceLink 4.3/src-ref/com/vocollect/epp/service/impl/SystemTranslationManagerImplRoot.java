/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SystemTranslationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemTranslation;
import com.vocollect.epp.service.SystemTranslationManager;
import com.vocollect.epp.util.LocaleAdapter;

import java.util.Collections;
import java.util.List;
import java.util.Locale;


/**
 * Additional service methods for the <code>SummaryPrompt</code> model object.
 *
 */
public abstract class SystemTranslationManagerImplRoot 
    extends GenericManagerImpl<SystemTranslation, SystemTranslationDAO> 
    implements SystemTranslationManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager.
     */
    public SystemTranslationManagerImplRoot(SystemTranslationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SystemTranslationManagerRoot#getAllSupportedLocales()
     */
    public List<Locale> getAllSupportedLocales() throws DataAccessException {
        List<Locale> locales = getPrimaryDAO().getSupportedLocales(); 
        Collections.sort(locales, LocaleAdapter.LOCALE_COMPARATOR);
        return locales;
    }    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 