/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.UserChartPreference;
import com.vocollect.epp.dao.FilterDAO;
import com.vocollect.epp.dao.HomepageDAO;
import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.dao.TagDAO;
import com.vocollect.epp.dao.TimeWindowDAO;
import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.TimeWindow;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.DataRetrievalFailureException;

/**
 * Implementation of UserManager interface.
 * 
 * @author Dennis Doubleday
 */
public abstract class UserManagerImplRoot extends
        GenericManagerImpl<User, UserDAO> implements UserManager {

    private static Logger log = new Logger(UserManagerImpl.class);

    private TagDAO tagDAO;

    private SiteDAO siteDAO;

    private FilterDAO filterDAO;
    
    private TimeWindowDAO timeWindowDAO;
    
    private HomepageDAO homepageDAO;
    
    /**
     * @param siteDAO
     *            injected site DAO
     */
    public void setSiteDAO(SiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * @param tagDAO
     *            injected tag DAO
     */
    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }
   
    /**
     * Setter for the filterDAO property.
     * @param filterDAO the new filterDAO value
     */
    public void setFilterDAO(FilterDAO filterDAO) {
        this.filterDAO = filterDAO;
    }

    /**
     * @param homepageDAO .
     */
    public void setHomepageDAO(HomepageDAO homepageDAO) {
        this.homepageDAO = homepageDAO;
    }
    
    /**
     * @param timeWindowDAO .
     */
    public void setTimeWindowDAO(TimeWindowDAO timeWindowDAO) {
        this.timeWindowDAO = timeWindowDAO;
    }
    
    /**
     * Constructor.
     * 
     * @param primaryDAO
     *            the DAO for this manager
     */
    public UserManagerImplRoot(UserDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#save(java.lang.Object)
     */
    @Override
    public Object save(User instance) throws BusinessRuleException,
            DataAccessException {

        this.throwExceptionOnRemoveLastAdminRole(instance,
                "user.delete.error.lastAdmin");

        this.throwExceptionOnChangeLastDBAuthenticatedAdmin(instance,
            "user.change.error.lastDBAuthenticatedAdmin");

        // create tags to give the user
        Set<Tag> tags = new HashSet<Tag>();
        Set<Site> newSites = new HashSet<Site>();
        Set<Site> sites = instance.getSites();
        if (sites != null) {
            boolean oldFilter = false;
            SiteContext sc = SiteContextHolder.getSiteContext();
            if (sc != null) {
                oldFilter = sc.isFilterBySite();
                SiteContextHolder.getSiteContext().setFilterBySite(false);
            }
            // loop through the sites and give the user 1 tag/site
            for (Site site : sites) {
                Tag t = tagDAO.findByTaggedObjectId(Tag.SITE, site.getId());
                if (t == null) {
                    throw new DataAccessException();
                }
                tags.add(t);
                newSites.add(siteDAO.get(site.getId()));
            }
            instance.setSites(newSites);
            instance.setTags(tags);
            
            if (sc != null) {
                SiteContextHolder.getSiteContext().setFilterBySite(oldFilter);
            }
        }

        return super.save(instance);
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.UserManager#findByName(java.lang.String)
     */
    public User findByName(String username)
            throws com.vocollect.epp.dao.exceptions.DataAccessException {
        return getPrimaryDAO().findByName(username);
    }

    /**
     * This implementation of the method from the Acegi UserDetailsService
     * allows Acegi to retrieve Vocollect users from our database. {@inheritDoc}
     * 
     * @see org.acegisecurity.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
     */
    public UserDetails loadUserByUsername(String name)
            throws UsernameNotFoundException,
            org.springframework.dao.DataAccessException {
        try {
            // Disable site context, as this is used by Acegi, outside
            // the site filter.
            SiteContext siteContext = SiteContextHolder.getSiteContext();
            if (siteContext != null) {
                siteContext.setFilterBySite(false);
            }
            // Find the user
            User user = getPrimaryDAO().findByName(name);

            // Re-enable site filtering.
            if (siteContext != null) {
                siteContext.setFilterBySite(true);
            }
            // TODO: this is a hack. We need feature group names in the Acegi
            // cached User object, but we get lazy evaluation errors
            // unless we force loading of those feature group names right here
            // by accessing them.
            if (user != null) {
                for (Role role : user.getRoles()) {
                    for (Feature feature : role.getFeatures()) {
                        feature.getFeatureGroup().getName();
                    }
                }
                for (Site site : user.getSites()) {
                    site.getName();
                }

            }
            return user;

        } catch (com.vocollect.epp.dao.exceptions.DataAccessException e) {
            // Acegi expects a Spring DataAccessException, not an EPP one.
            throw new DataRetrievalFailureException("User not found", e);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(User user) throws BusinessRuleException,
            com.vocollect.epp.dao.exceptions.DataAccessException {

        // Business rule: Can't delete yourself
        throwExceptionOnSelfDelete(user, "user.delete.error.selfDelete");

        // Business rule: last Administrator can't be deleted
        throwExceptionOnLastAdmin(user, "user.delete.error.lastAdmin");

        // Need to delete filters first
        List<Filter> filterList = filterDAO.listAllByUserId(user.getId());
        for (Filter filter : filterList) {
            filterDAO.delete(filter);
        }

        List<TimeWindow> twList = timeWindowDAO.listAllByUserId(user.getId());
        for (TimeWindow tw : twList) {
            timeWindowDAO.delete(tw);
        }

        List<Homepage> hpList = homepageDAO.listAllByUserId(user.getId());
        for (Homepage hp : hpList) {
            homepageDAO.delete(hp);
        }

        // do the deletion
        return super.delete(user);
    }

    /**
     * Throws an exception if the User being operated on is the last user with
     * the Administrator role and authenticates against the database.
     * 
     * @param user
     *            the user being operated on.
     * @param messageKey
     *            the resource key to put in the exception message.
     * @throws BusinessRuleException
     *             if the user is the last admin.
     * @throws com.vocollect.epp.dao.exceptions.DataAccessException
     *             on failure to retrieve admin list.
     */
    protected void throwExceptionOnLastAdmin(User user, String messageKey)
            throws BusinessRuleException,
            com.vocollect.epp.dao.exceptions.DataAccessException {

        if (user.isAdministrator() && user.getAllSitesAccess()
                && user.getPassword() != null
                && user.getPassword().length() > 0) {
            // We only want to perform this test if we are deleting an
            // administrator
            // who has a password, i.e. who authenticates against the db
            SiteContextHolder.getSiteContext().setFilterBySite(false);
            if (convertNumberToInt(
                    getPrimaryDAO().countDatabaseAuthenticatedAdministrators()) < 2) {
                // Not allowed to delete the last admin
                log
                        .warn("Attempt to delete the last Administrator with database authentication, user "
                                + user.getName());
                throwBusinessRuleException(messageKey);
            }
        }
    }

    /**
     * Ensures that the last admin cannot change their role to a non-admin.
     * 
     * @param user
     *            the user
     * @param messageKey
     *            the message key
     * @throws BusinessRuleException
     *             the exception thrown if they are the last admin
     * @throws com.vocollect.epp.dao.exceptions.DataAccessException
     *             if the user info cannot be retrieved
     */
    protected void throwExceptionOnRemoveLastAdminRole(User user,
            String messageKey) throws BusinessRuleException,
            DataAccessException {

        // test to see if the user is actually being updated to a non-admin
        if ((!user.isNew())
                && 
                (!user.isAdministrator()
                  || ((user.isAdministrator()) 
                      && (!user.getAllSitesAccess())))) {

            // get the ids of the current administrators
            List<Long> admins = this.getPrimaryDAO()
                    .getAllSiteAdministrators();
            // if there is more than one, we don't care
            if (admins.size() < 2) {
                for (Long id : admins) {
                    // test to see if the user being saved was an admin
                    if (user.getId().equals(id)) {
                        throwBusinessRuleException(messageKey);
                    }
                }
            }
        }
    }

    /**
     * Throws an exception if the User being deleted is the current user.
     * 
     * @param user
     *            the user being deleted
     * @param messageKey
     *            the resource key to put in the message
     * @throws BusinessRuleException
     *             if the user is deleting themselves
     */
    protected void throwExceptionOnSelfDelete(User user, String messageKey)
            throws BusinessRuleException {

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        if (auth != null && auth.getPrincipal() instanceof UserDetails) {
            User currentUser = (User) auth.getPrincipal();
            if (currentUser.getId().equals(user.getId())) {
                throwBusinessRuleException(messageKey);
            }
        }
    }

    /**
     * Retrieve the number of users who authenticate against the directory
     * server.
     * 
     * @return int - the number of users who authenticate against the directory
     *         server
     * @throws DataAccessException
     *             on any failure
     */
    public int countDirectoryServerAuthenticatingUsers()
            throws DataAccessException {
        return convertNumberToInt(
            getPrimaryDAO().countDirectoryServerAuthenticatingUsers());
    }

    /**
     * Ensures that we always have one administrator who can authenticate
     * against the database, instead of the directory server.  This protects us 
     * from the case where the directory server is down for whatever reason.
     * @param user
     *            the user
     * @param messageKey
     *            the message key
     * @throws BusinessRuleException
     *             the exception thrown if they are the last admin to authenticate
     *             against the db and the user attempts to change that
     * @throws com.vocollect.epp.dao.exceptions.DataAccessException
     *             if the user info cannot be retrieved
     */
    protected void throwExceptionOnChangeLastDBAuthenticatedAdmin(User user,
            String messageKey) throws BusinessRuleException,
            DataAccessException {

        // If the user is an Administrator
        if (user.isAdministrator()) {
            // If the user password is null or empty string, then we know this
            // is an directory server authenticated user
            if (StringUtil.isNullOrEmpty(user.getPassword())) {
                // If the number of database authenticated administrators 
                // is 0 - we can't let this go thru
                if (SiteContextHolder.getSiteContext() != null) {
                    SiteContextHolder.getSiteContext().setFilterBySite(false);
                }
                if (convertNumberToInt(
                        getPrimaryDAO().countDatabaseAuthenticatedAdministrators()) <= 1) {
                    List<User> users = getPrimaryDAO().listAllDatabaseAuthenticatedAdministrators();
                    if (users != null) {
                        if (users.contains(user)) {
                            // Throw the business rule exception.
                            throwBusinessRuleException(messageKey);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserManagerRoot#updateUserChartPreferencesByDashboard(java.lang.Long)
     */
    public void updateUserChartPreferencesByDashboard(Long dashboardId) {
        this.getPrimaryDAO().updateUserChartPreferencesByDashboard(dashboardId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserManagerRoot#updateUserPropertyByDashboard(java.lang.String, java.lang.Long)
     */
    public void updateUserPropertyByDashboard(String name, Long value) {
        this.getPrimaryDAO().updateUserPropertyByDashboard(name, value);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserManagerRoot#updateUserChartPreferencesByDashboardAndChart(java.lang.Long, java.lang.Long)
     */
    public void updateUserChartPreferencesByDashboardAndChart(Long dashboardId, Long chartId) {
        this.getPrimaryDAO().updateUserChartPreferencesByDashboardAndChart(dashboardId, chartId);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserManagerRoot#getChartPreferenceByUserChartAndDashboard(com.vocollect.epp.model.User,
     *      com.vocollect.epp.chart.model.Chart,
     *      com.vocollect.epp.dashboard.model.Dashboard)
     */
    @Override
    public UserChartPreference getChartPreferenceByUserChartAndDashboard(User user,
                                                  Chart chart,
                                                  Dashboard dashboard) {
        // fetching chart preferences through user object because user object
        // doesn't have reference in UserChartPreference to be able to write user
        // specific queries and its not recommended to do so either, any
        // interaction with properties of user object should be done using that
        // very object.
        Set<UserChartPreference> chartPreferences = user.getChartPreference();

        for (UserChartPreference chartPreference : chartPreferences) {
            if (chartPreference.getChart().getId() == chart.getId()
                && chartPreference.getDashboard().getId() == dashboard.getId()) {
                return chartPreference;
            }
        }

        return null;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserManagerRoot#updateUserChartPreferences()
     */
    @Override
    public synchronized void updateUserChartPreferences(Long currentUserId,
                                                        Chart chart,
                                                        Dashboard dashboard,
                                                        JSONObject chartPreferencesJSON)
        throws JSONException, DataAccessException, BusinessRuleException {

        User user = get(currentUserId);

        UserChartPreference chartPreference = getChartPreferenceByUserChartAndDashboard(user, chart,
            dashboard);

        if (chartPreference == null) {
            chartPreference = new UserChartPreference();
            user.getChartPreference().add(chartPreference);

            chartPreference.setChart(chart);
            chartPreference.setDashboard(dashboard);
        }
        
        if (chartPreferencesJSON.has("chartWidth")) {
            chartPreference.setChartWidth((Float) chartPreferencesJSON
                .get("chartWidth"));
        }
        if (chartPreferencesJSON.has("chartHeight")) {
            chartPreference.setChartHeight((Float) chartPreferencesJSON
                .get("chartHeight"));
        }
        if (chartPreferencesJSON.has("chartX")) {
            chartPreference.setChartX((Float) chartPreferencesJSON
                .get("chartX"));
        }
        if (chartPreferencesJSON.has("chartY")) {
            chartPreference.setChartY((Float) chartPreferencesJSON
                .get("chartY"));
        }

        save(user);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 