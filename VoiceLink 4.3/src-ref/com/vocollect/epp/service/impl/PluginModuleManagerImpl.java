/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.PluginModuleDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.plugin.PluginModule;
import com.vocollect.epp.service.PluginModuleManager;

import java.util.List;

/**
 * Used to manage registered plugin PluginModule objects.
 * 
 *
 * @author ddoubleday
 */
public class PluginModuleManagerImpl 
    extends GenericManagerImpl<PluginModule, PluginModuleDAO> 
    implements PluginModuleManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO.
     */
    public PluginModuleManagerImpl(PluginModuleDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.PluginModuleManager#findByName(java.lang.String)
     */
    public PluginModule findByName(String name) throws DataAccessException {
        return getPrimaryDAO().findByName(name);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.PluginModuleManager#listEnabledModules()
     */
    public List<PluginModule> listEnabledModules() {
        return getPrimaryDAO().listEnabledModules();
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 