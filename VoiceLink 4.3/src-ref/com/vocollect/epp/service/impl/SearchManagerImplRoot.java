/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SearchDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.search.SearchErrorCode;
import com.vocollect.epp.search.SearchResult;
import com.vocollect.epp.search.SearchResultDataMappings;
import com.vocollect.epp.search.SearchResultList;
import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.service.SearchManager;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Implements the <code>SearchManager</code>.
 *
 * @author hulrich
 */
public abstract class SearchManagerImplRoot implements SearchManager {

    private static final Logger log = new Logger(SearchManagerImpl.class);

    private static final String APP_MENU_PROPERTY = "appMenu";

    // Default application menu if no value is passed to the find method
    private static final String DEFAULT_APP_MENU = "admin";

    private TagManager tagManager;

    private SiteManager siteManager;

    private DataTranslationManager dataTranslationManager;

    private SearchDAO searchDAO;

    private static final String SELECT_MENU = "selection";

    private static final String PUTAWAY_MENU = "putaway";

    private static final String REASON_CODE_ALIAS = "ReasonCode";

    private static final String OPERATOR_ALIAS = "OperatorCommon";

    private static final String WORKGROUP_ALIAS = "Workgroup";

    private static final String LINELOAD_MENU = "lineloading";

    private static final String REPLENISHMENT_MENU = "replenishment";

    private static final String CYCLECOUNTING_MENU = "cyclecounting";
    
    private static final String LOADING_MENU = "loading";
    
    

    private String[] commonAliases = new String[] {
        "Location", "Item", OPERATOR_ALIAS, WORKGROUP_ALIAS,
        REASON_CODE_ALIAS, "BreakType", "UnitOfMeasure", "Vehicle",
        "VehicleSafetyCheck", "VehicleSafetyCheckResponse", "VehicleType",
        "ExternalJob"};

    // Limit on number of results to return in each category
    private int categoryResultLimit;

    /**
     * Getter for the categoryResultLimit property.
     * @return int value of the property
     */
    public int getCategoryResultLimit() {
        return this.categoryResultLimit;
    }


    /**
     * Setter for the categoryResultLimit property.
     * @param categoryResultLimitParam the new categoryResultLimit value
     */
    public void setCategoryResultLimit(int categoryResultLimitParam) {
        this.categoryResultLimit = categoryResultLimitParam;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SearchManagerRoot#searchAll(java.lang.String)
     */
    public SearchResultList<SearchResult> searchAll(String searchTerm)
        throws DataAccessException {

        return searchAll(searchTerm, DEFAULT_APP_MENU);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SearchManagerRoot#searchAll(java.lang.String, java.lang.String)
     */
    public SearchResultList<SearchResult> searchAll(String searchTerm, String appMenuName)
        throws DataAccessException {

        List<BaseModelObject> hits = new ArrayList<BaseModelObject>();
        hits = getSearchDAO().searchAll(searchTerm);

        return buildCollection(hits, searchTerm, appMenuName);
    }

    /**
     * Getter for the siteManager property.
     * @return SiteManager value of the property
     */
    public SiteManager getSiteManager() {
        return this.siteManager;
    }

    /**
     * Setter for the siteManager property.
     * @param siteManagerParam the new siteManager value
     */
    public void setSiteManager(SiteManager siteManagerParam) {
        this.siteManager = siteManagerParam;
    }

    /**
     * Getter for the tagManager property.
     * @return TagManager value of the property
     */
    public TagManager getTagManager() {
        return this.tagManager;
    }

    /**
     * Setter for the tagManager property.
     * @param tagManagerParam the new tagManager value
     */
    public void setTagManager(TagManager tagManagerParam) {
        this.tagManager = tagManagerParam;
    }

    /**
     * Getter for the dataTranslationManager property.
     * @return DataTranslationManager value of the property
     */
    public DataTranslationManager getDataTranslationManager() {
        return dataTranslationManager;
    }

    /**
     * Setter for the dataTranslationManager property.
     * @param dataTranslationManagerParam the new dataTranslationManager value
     */
    public void setDataTranslationManager(DataTranslationManager dataTranslationManagerParam) {
        this.dataTranslationManager = dataTranslationManagerParam;
    }

    /**
     * Converts the search hits to a list of search results.
     * @param hits the results
     * @param searchTerm the query
     * @param appMenuName the context appMenuName
     * @return the list of results
     * @throws DataAccessException if there is an error during conversion
     */
    private SearchResultList<SearchResult> buildCollection(List<BaseModelObject> hits,
                                                           String searchTerm,
                                                           String appMenuName)
        throws DataAccessException {

        SearchResultList<SearchResult> results = new SearchResultList<SearchResult>(
            ALL_SITES);

        for (BaseModelObject hit : hits) {
            SearchResult result = buildSearchResult(hit, appMenuName);
            if (result != null) {
                results.add(result);
                results.increment();
            }
        }
        return results;
    }

    /**
     * Builds a search result from the hit.
     * @param hit the result
     * @param appMenuName the context menu
     * @return the search result
     * @throws DataAccessException if there is a problem converting
     */
    private SearchResult buildSearchResult(BaseModelObject hit, String appMenuName)
        throws DataAccessException {

        SearchResult result = null;
        String className = hit.getClass().getName();
        String aliasName = hit.getClass().getSimpleName();
        String id = hit.getId().toString();

        if (log.isTraceEnabled()) {
            log.trace("Class - " + className + " -- " + id + " === ");
        }

        // Get the result mapping metadata for this class.
        SearchResultDataMappings mappings = new SearchResultDataMappings(
            className);

        boolean isDisplayable = mappings.isDisplayable();

        if (!isDisplayable) {
            log.error("Result : " + className + " is not displayable",
                      SystemErrorCode.CONFIGURATION_ERROR);
        }

        if (isDisplayable) {
            String url = mappings.getUrl().trim();
            if (isCommonFeature(aliasName)) {
                url = formURLForCommonFeature(url, aliasName, appMenuName);
            }
            if (log.isTraceEnabled()) {
                log.trace("url formed ----" + url);
            }

            String area = mappings.getDisplayArea();

            String displayName;

            // See if there is an override method for displayName
            String method = mappings.getDisplayField();
            if (method != null) {
                displayName = invokeMethod(hit, method);
            } else {
                // Assume getDescriptiveText() for the display name method.
                displayName = hit.getDescriptiveText();
            }

            // Used to translate the display value
            boolean translatable =  mappings.isTranslatable();
            if (translatable) {
                displayName = dataTranslationManager.translate(displayName);
            }


            // See if there is an override method for the display ID.
            method = mappings.getDisplayFieldId();
            if (method != null) {
                id = invokeMethod(hit, method).trim();
            }


            method = mappings.getDoNotDisplayId();
            if (method != null && method.equals("true")) {
                id = "";
            }

            // Add the ID to the URL
            url += id;

            // See if any extra URL params are specified.
            String params = mappings.getParams();
            if (params != null) {
                // Add them to the URL
                url += params;
            }

            //if there's a filter, let's add it to the url
            String filterParamMethod = mappings.getFilterParameter();
            String filter = mappings.getFilter();
            if (filterParamMethod != null && filter != null) {
                String paramValue = invokeMethod(hit, filterParamMethod);
                url += filter.replaceFirst("PARAM1", paramValue);
            }

            List<String> sites = getSiteList(hit);

            if (log.isTraceEnabled()) {
                log.trace(area + " is area, display name: " + displayName);
            }

            if (!StringUtil.isNullOrEmpty(url)) {
                result = new SearchResult(url, displayName, area, sites, hit);
            } else {
            	if (log.isDebugEnabled()) {
            		log.debug("Found class " + className + " but URL is null. Entry in searchResultMappings-voiceconsole.properties is wrong or missing.");
            	}
            }
        }

        return result;
    }
    /**
     * Builds the site list for this object, if it is not associated with a
     * site, assigns it to "All Sites".
     * @param hit the result
     * @return the list of associated sites
     * @throws DataAccessException if there is a problem retrieving the site
     */
    private List<String> getSiteList(BaseModelObject hit) throws DataAccessException {
        List<String> list = new LinkedList<String>();
        if (hit instanceof Taggable) {
            SiteContext sc = SiteContextHolder.getSiteContext();
            List<Site> sites = sc.getSites((Taggable) hit);
            // No sites, return ALL_SITES
            if (null == sites || sites.isEmpty()) {
                list.add(ALL_SITES);
                return list;
            }
            // get the names of the sites
            for (Site site : sites) {
                list.add(site.getName());
            }

        } else {
            list.add(ALL_SITES);
        }
        return list;
    }


    /**
     * Reflectively invokes the method.
     * @param o the object
     * @param method the object
     * @return the result
     * @throws DataAccessException if the method cannot be invoked
     */
    private String invokeMethod(Object o, String method)
        throws DataAccessException {

        String retString = null;
        try {
            Method m = o.getClass().getMethod(method, (Class[]) null);

            Object ret = m.invoke(o, (Object[]) null);
            if (ret != null) {
                retString = ret.toString();
            }
        } catch (Exception e) {
            UserMessage um = null;
            if (o != null) {
                um = new UserMessage("Unable to invoke method - " + method
                    + " - on object type - " + o.getClass().getName());
            } else {
                um = new UserMessage("Unable to invoke on null object");
            }
            throw new DataAccessException(
                SearchErrorCode.UNABLE_TO_MAP_RESULT, um, e);
        }
        return retString;
    }

    /**
     * Checks if the search result is a common features i.e, present across more
     * than one entity(classes in the core model and are searchable).
     * @param className containing the searchable class
     * @return boolean flag 'true'if the search result is a common feature false
     *         otherwise
     */
    private boolean isCommonFeature(String className) {
        for (String alias : commonAliases) {
            if (className.equals(alias)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Forms the URL for the common feature like Operator, Location etc, based
     * on the page the user currently in. The searched link will be re
     * redirected to this URL.
     * @param url containing the url obtained from the confirguration file
     * @param alias containing the searchable alias for the class
     * @param appMenuName the context menu.
     * @return newly formed URL for the common feature.
     */
    private String formURLForCommonFeature(String url, String alias, String appMenuName) {

        if (appMenuName.equals(DEFAULT_APP_MENU)) {
            if (!alias.equals(REASON_CODE_ALIAS)) {
                return url.replaceFirst(APP_MENU_PROPERTY, SELECT_MENU);
            }
            return url.replaceFirst(APP_MENU_PROPERTY, PUTAWAY_MENU);
        }

        if (alias.equals(REASON_CODE_ALIAS)) {

            if (appMenuName.equals(REPLENISHMENT_MENU)) {
                return url.replaceFirst(APP_MENU_PROPERTY, REPLENISHMENT_MENU);
            }
            return url.replaceFirst(APP_MENU_PROPERTY, PUTAWAY_MENU);
        }

        if (!appMenuName.equals(LINELOAD_MENU)) {
            return url.replaceFirst(APP_MENU_PROPERTY, appMenuName);
        }

        if (!appMenuName.equals(CYCLECOUNTING_MENU)) {
            return url.replaceFirst(APP_MENU_PROPERTY, appMenuName);
        }
        
        if (!appMenuName.equals(LOADING_MENU)) {
            return url.replaceFirst(APP_MENU_PROPERTY, appMenuName);
        }

        if ((alias.equals(OPERATOR_ALIAS) || (alias
            .equals(WORKGROUP_ALIAS)))) {

            return url.replaceFirst(APP_MENU_PROPERTY, appMenuName);
        }

        return url.replaceFirst(APP_MENU_PROPERTY, SELECT_MENU);
    }

    /**
     * Getter for the searchDAO property.
     * @return SearchDAO value of the property
     */
    public SearchDAO getSearchDAO() {
        return this.searchDAO;
    }

    /**
     * Setter for the searchDAO property.
     * @param searchDAOParam the new searchDAO value
     */
    public void setSearchDAO(SearchDAO searchDAOParam) {
        this.searchDAO = searchDAOParam;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 