/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ColumnDAO;
import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.TimeWindowDAO;
import com.vocollect.epp.dao.UserColumnDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.TimeWindow;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserColumn;
import com.vocollect.epp.model.UserTimeWindow;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.ui.ColumnDTO;

import java.util.List;

/**
 * Business Service implementation to handle the saving and retreiving of User
 * Preferences.
 * <p>
 *
 * @author dkertis
 */
public abstract class UserPreferencesManagerImplRoot implements UserPreferencesManager {

    private UserColumnDAO userColumnDAO;

    private GenericDAO<View> viewDAO;

    private ColumnDAO columnDAO;

    private TimeWindowDAO timeWindowDAO;

    /**
     * @return timeWindowDAO
     */
    public TimeWindowDAO getTimeWindowDAO() {
        return timeWindowDAO;
    }


    /**
     * @param timeWindowDAO .
     */
    public void setTimeWindowDAO(TimeWindowDAO timeWindowDAO) {
        this.timeWindowDAO = timeWindowDAO;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserPreferencesManagerRoot#getTimeWindow(com.vocollect.epp.model.View, com.vocollect.epp.model.User)
     */
    public Long getTimeWindow(View view, User user) throws DataAccessException {
        TimeWindow timeWindow = timeWindowDAO.getByViewIdAndUser(user, view.getId());
        if (timeWindow == null) {
            timeWindow = timeWindowDAO.getByViewId(view.getId());
            if (timeWindow == null) {
                return new Long(0);
            }
        }
        return timeWindow.getValue();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.UserPreferencesManagerRoot#saveTimeWindow(java.lang.Long, com.vocollect.epp.model.User, java.lang.Long)
     */
    public void saveTimeWindow(Long viewId, User user, Long newValue)
        throws DataAccessException {
        TimeWindow timeWindowObj = null;
        // must get the userTimeWindow and TimeWindow that we are trying to save
        timeWindowObj = timeWindowDAO.getByViewIdAndUser(user, viewId);
        if (timeWindowObj != null) {
            timeWindowObj.setValue(newValue);
            timeWindowDAO.save(timeWindowObj);
        } else {
            // create a UserTimeWindow
            UserTimeWindow utw = new UserTimeWindow();
            utw.setView(getView(viewId));
            utw.setValue(newValue);
            utw.setUser(user);
            timeWindowDAO.save(utw);
       }
    }

    /**
     * Attains the columns for a particular user and view.
     * @param view Describes what table
     * @param user Describes what user
     * @return List of columns
     * @throws DataAccessException in case a DB issue should occur.
     */
    @SuppressWarnings("unchecked")
    public List<Column> getColumns(View view, User user)
        throws DataAccessException {
        List<Column> cols = this.columnDAO.getColumns(view, user);
        //Collections.sort(cols, new BeanComparator("order"));
        return cols;
    }

    /**
     * Attains the visible columns for a particular user and view.
     * @param view Describes what table
     * @param user Describes what user
     * @return List of columns
     * @throws DataAccessException in case a DB issue should occur.
     */
    @SuppressWarnings("unchecked")
    public List<Column> getVisibleColumns(View view, User user)
        throws DataAccessException {
        List<Column> cols = this.columnDAO.getVisibleColumns(view, user);
        //Collections.sort(cols, new BeanComparator("order"));
        return cols;
    }


    /**
     * Saves the user's preferences.
     * @param columns DTO's with the column data
     * @param user which user to save for
     * @throws DataAccessException in case a DB issue should occur.
     */
    public void savePreferences(List<ColumnDTO> columns, User user)
        throws DataAccessException {
        for (ColumnDTO column : columns) {
            // save the column
            UserColumn userCol = null;


            // must get the userColumn and Column  that we are tryibng to save
            userCol = userColumnDAO.findByUserAndColumn(user.getId(), new Long(column
                .getColumnId()));


            if (userCol != null) {
                // update properties on the UserColumn object
                userCol.update(column);
            } else {
                // have to get the column object and add it to the userColumn while in
                //  a hibernate session
                Column c = null;
                c = this.columnDAO.get(column.getColumnId());

                // create a UserCol
                userCol = new UserColumn(user, c, column);
                // needed for hibernate to recognize the bi-directional association
                c.getUserColumn().add(userCol);
            }
            userColumnDAO.save(userCol);
        }
    }

    /**
     * Restores the user's default preferences.
     * @param user which user to restore
     * @param viewId of the view being restored.
     * @throws DataAccessException in case a DB issue should occur.
     */
    public void restoreDefaultColumns(User user, Long viewId)
        throws DataAccessException {
        // get list of Columns using view
        List<Column> columns = getColumns(getView(viewId), user);
        for (Column column : columns) {
            UserColumn userCol = userColumnDAO.findByUserAndColumn(user.getId(), column.getId());
            if (userCol != null) {
                this.userColumnDAO.delete(userCol);
            }
        }
    }

    /**
     * Searches for UserColumns.  If any exist, restore user default columns
     * preferences is enabled.
     * @param user User Object
     * @param view Screen view object
     * @return boolean of whether the restore columns should be enabled.
     * @throws DataAccessException in case a DB issue should occur.
     */
    public boolean isRestoreDefaultColumnsEnabled(User user, View view)
        throws DataAccessException {
        // get list of Columns using view
        List<Column> columns = getColumns(view, user);
        for (Column column : columns) {
            UserColumn userCol = userColumnDAO.findByUserAndColumn(user.getId(), column.getId());
            if (userCol != null) {
                return true;
            }
        }
        return false;
    }

    /**
     * Attains the view object.
     * @param id of the view
     * @return View object
     * @throws DataAccessException in case a DB issue should occur.
     */
    public View getView(Long id) throws DataAccessException {
        return this.viewDAO.get(id);
    }

    /**
     * Setter for the sserColumnDAO property.
     * @param userColumnDAO - injected by spring
     */
    public void setUserColumnDAO(UserColumnDAO userColumnDAO) {
        this.userColumnDAO = userColumnDAO;
    }

    /**
     * Setter for the viewDAO property.
     * @param viewDAO - injected by spring
     */
    public void setViewDAO(GenericDAO<View> viewDAO) {
        this.viewDAO = viewDAO;
    }


    /**
     * Setter for the columnDAO property.
     * @param columnDAO the new columnDAO value
     */
    public void setColumnDAO(ColumnDAO columnDAO) {
        this.columnDAO = columnDAO;
    }

    /**
     * @return columnDAO
     */
    protected ColumnDAO getColumnDAO() {
        return this.columnDAO;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 