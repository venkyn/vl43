/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ReportParameterDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.service.ReportParameterManager;

import java.util.List;

/**
 * 
 *
 * @author mnichols
 */
public class ReportParameterManagerImplRoot extends GenericManagerImpl<ReportParameter, ReportParameterDAO>
    implements ReportParameterManager {
    
    /**
     *
     * Constructor.
     * @param reportDAO The report DAO; constructed from Spring
     */
    public ReportParameterManagerImplRoot(ReportParameterDAO reportDAO) {
        super(reportDAO);
    }
    
    /**
     * {@inheritDoc}
     * @param id the id of the operator team
     */
    public List<ReportParameter> getTeamReferences(Long id) throws DataAccessException {
        return getPrimaryDAO().listTeamReferences(id.toString());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 