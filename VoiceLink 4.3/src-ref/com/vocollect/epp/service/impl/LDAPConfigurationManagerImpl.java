/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.LDAPConfigurationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.LDAPConfiguration;
import com.vocollect.epp.service.LDAPConfigurationManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author
 */
public class LDAPConfigurationManagerImpl extends GenericManagerImpl<LDAPConfiguration, LDAPConfigurationDAO>
    implements LDAPConfigurationManager {

    /**
     * Constructor.
     * @param primaryDAO .
     */
    public LDAPConfigurationManagerImpl(LDAPConfigurationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Removes LdapConfigurations identified by the given operator primary key
     * array.
     * @param ldapConfigurationIds Integer[] ldapConfiguration primary keys
     * @return array of value-objects containing the details of the
     *         ldapConfigurations which were just removed
     */
    public List<LDAPConfiguration> deleteLDAPConfigurations(Long[] ldapConfigurationIds)
        throws VocollectException, DataAccessException {
        List<LDAPConfiguration> ldapConfigurations = new ArrayList<LDAPConfiguration>();
        for (int i = 0; i < ldapConfigurationIds.length; i++) {
            ldapConfigurations
                .add(deleteLDAPConfiguration(ldapConfigurationIds[i]));
        }
        return ldapConfigurations;
    }

    /**
     * This method deletes the ldapConfiguration identified by the ldapConfiguration primary key.
     * @param ldapConfigurationId Integer ldapConfiguration primary key
     * @return ldapConfiguration vo representing the deleted ldapConfiguration, or an error VO
     * representing the failed attempt to delete.
     */
    public LDAPConfiguration deleteLDAPConfiguration(Long ldapConfigurationId) throws DataAccessException {
        LDAPConfiguration ldapConfiguration = getPrimaryDAO().get(ldapConfigurationId);
        if (ldapConfiguration == null) {
            // Create an exception VO and add the data.
            //TODO: Handle exception here
            return ldapConfiguration;
        }
        getPrimaryDAO().delete(ldapConfiguration);

        return ldapConfiguration;
    }

    /**
     * Retrieves value object for the specified LDAPConfiguration.
     * @param ldapConfigId Integer used to specify LDAPConfiguration
     * @throws EntityNotFoundException if the specified entity could not be
     *             found
     * @return the LDAPConfigurationVO (with simple properties only)
     */
    public LDAPConfiguration retrieveLDAPConfiguration(Long ldapConfigId)
        throws EntityNotFoundException, DataAccessException {
        LDAPConfiguration ldapConfiguration = getPrimaryDAO()
            .get(ldapConfigId);
        if (ldapConfiguration == null) {
            EntityNotFoundException nfe = new EntityNotFoundException(
                LDAPConfiguration.class, ldapConfigId);
            throw nfe;
        }
        return ldapConfiguration;
    }

    /**
     * Retrieves value objects for all LDAPConfigurations.
     * @return the VOResultSet of the current set of items
     * @throws DataAccessException
     */
    public List<LDAPConfiguration> retrieveAllLDAPConfigurations()
        throws DataAccessException {
        return getPrimaryDAO().getAll();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.LDAPConfigurationManager#uniqueness(com.vocollect.epp.model.LDAPConfiguration)
     */
    public Long uniqueness(LDAPConfiguration ldapConfig) throws DataAccessException {
        return getPrimaryDAO().uniqueness(ldapConfig.getUseSSL(),
                                          ldapConfig.getLdapHost(),
                                          ldapConfig.getLdapPort(),
                                          ldapConfig.getSearchUsername(),
                                          ldapConfig.getSearchPassword(),
                                          ldapConfig.getSearchBase(),
                                          ldapConfig.getSearchableAttribute(),
                                          ldapConfig.getPasswordAttribute());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 