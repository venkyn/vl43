/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.service.FilterCriterionManager;

/**
 * Implementation of the FilterCriterionManager.
 *
 *
 * @author jgeisler
 */
public abstract class FilterCriterionManagerImplRoot extends
    GenericManagerImpl<FilterCriterion, GenericDAO<FilterCriterion>> implements
    FilterCriterionManager {

    /**
     * Constructor.
     * @param primaryDAO The Primary DAO.
     */
    public FilterCriterionManagerImplRoot(GenericDAO<FilterCriterion> primaryDAO) {
        super(primaryDAO);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 