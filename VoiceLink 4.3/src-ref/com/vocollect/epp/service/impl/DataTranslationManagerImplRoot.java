/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.DataTranslationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataTranslation;
import com.vocollect.epp.service.DataTranslationManager;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Implementation of DataTranslationManager interface.
 *
 * @author Dennis Doubleday
 */
public abstract class DataTranslationManagerImplRoot
    extends GenericManagerImpl<DataTranslation, DataTranslationDAO>
    implements DataTranslationManager, BeanFactoryPostProcessor {

    private static final int QUERY_ROWS = 50000;
    private static final int LOCALE = 0;
    private static final int KEY = 1;
    private static final int TRANSLATION = 2;

    //Cached map of data translations so we do not have to hit the database
    //for every translation
    private Map<String, HashMap<String, String>> translations =
        new HashMap<String, HashMap<String, String>>();

    //This is the date time the cached map was loaded or updated. It is used to
    //   determine if the mapp should be updated with any changes that may have
    //   occured in the database. Note: Only records modified since last time
    //   cache was loaded are reloaded, not the entire map.
    //Date is intialized to 1970 so all translations should be loaded first time
    //   this is done during bean creation.
    private Date lastLoad = new Date(0);

    //How often the cached mapped should be updated, or how often in seconds
    //should the database be checked for changes
    private int refreshInterval;

    //Used to convert number entered for refresh interval to milli-seconds
    private static final long TIME_CONVERSION = 1000;

    //Logger for loging errors
    private static final Logger log = new Logger(JobManagerImpl.class);




    /**
     * Getter for the refreshInterval property.
     * @return int value of the property
     */
    public int getRefreshInterval() {
        return refreshInterval;
    }


    /**
     * Setter for the refreshInterval property.
     * @param refreshInterval the new refreshInterval value
     */
    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
    throws BeansException {
        // Initializes the mapped when bean is loaded to prevent a big hit on
        // the UI or task command that first hits it. All errors are caught and
        // logged to prevent spring from stopping bean creation.
        try {
            ((DataTranslationManager)
                factory.getBean("dataTranslationManager")).translate("fake");
        } catch (Throwable t) {
            log.error("Error loading user translation values",
                null,
                t);
        }
    }

    /**
     * Constructor.
     * @param primaryDAO the primary DAO
     * (the instantiation for the persistent class)
     */
    public DataTranslationManagerImplRoot(DataTranslationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.DataTranslationManagerRoot#findTranslation(java.lang.String, java.lang.String)
     */
    public DataTranslation findTranslation(String key, String locale)
    throws DataAccessException {
        return getPrimaryDAO().findTranslation(key, locale);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.DataTranslationManagerRoot#translate(java.lang.String)
     */
    public String translate(String key) throws DataAccessException {
        return translate(key, LocaleContextHolder.getLocale());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.DataTranslationManagerRoot#translate(java.lang.String, java.util.Locale)
     */
    public String translate(String key, Locale locale)
    throws DataAccessException {
        String tran = null;

        //call to load or update the cached translations if needed
        loadMap();

        //get translation from map if it exists
        Map<String, String> langauge = translations.get(locale.toString());
        if (langauge != null) {
            tran = langauge.get(key);
        }

        //if not null return translation, otherwise return Key.
        if (tran != null) {
            return tran;
        } else {
            return key;
        }
    }

    /**
     * Loads the map completely or refreshes it if needed.
     *
     * @throws DataAccessException - Database Exceptions
     */
    private synchronized void loadMap()
    throws DataAccessException {
        Date now = new Date();

        //if refresh interval is greater than zero
        // and was not recently refreshed (base on refresh interval)
        //then refresh map by loading any changed
        if ((refreshInterval != 0
                && now.getTime() - lastLoad.getTime()
                > (refreshInterval * TIME_CONVERSION))) {

            QueryDecorator decorator = new QueryDecorator();
            decorator.setRowCount(QUERY_ROWS);
            decorator.setStartRow(0);
            List<Object[]> trans = null;
            do {
                //Get All Translations since last loaded
                trans = getPrimaryDAO().listModified(decorator, lastLoad);

                //Load all translations retrieved if any.
                for (Object[] t : trans) {
                    HashMap<String, String> language =
                        translations.get(t[LOCALE]);
                    if (language == null) {
                        language = new HashMap<String, String>();
                        translations.put((String) t[LOCALE], language);
                    }

                    language.put((String) t[KEY], (String) t[TRANSLATION]);
                }

                //update the last loaded time
                decorator.setStartRow(decorator.getStartRow() + QUERY_ROWS);

            } while (trans.size() == QUERY_ROWS);

            lastLoad = now;
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 