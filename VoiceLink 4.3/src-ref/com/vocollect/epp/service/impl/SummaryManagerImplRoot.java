/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SummaryDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Summary;
import com.vocollect.epp.security.RoleFeatureMap;
import com.vocollect.epp.security.RoleFeatureMapFactory;
import com.vocollect.epp.service.SummaryManager;

import java.io.IOException;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;


/**
 * Implementation for SummaryManager.
 *
 * @author svoruganti
 */
public abstract class SummaryManagerImplRoot extends
    GenericManagerImpl<Summary, SummaryDAO> implements
    SummaryManager {


    private static final Logger log = new Logger(SummaryManagerImpl.class);

    /**
     * Constructor for SummaryManagerImpl.
     * @throws IOException  - on failure to find resource file.
     * @param primaryDAO - summaryDAO
     */
    public SummaryManagerImplRoot(SummaryDAO primaryDAO) throws IOException {
        super(primaryDAO);

    }

    /**
     * Returns a List of Summary objects .
     * @return List
     *
     * @throws DataAccessException if unable to retrieve summaries
     */
    public List<Summary> getAllSummaries() throws DataAccessException {
        return getPrimaryDAO().getAll();
    }


    /**
     * To check if a user has permission to given summary.
     * @return boolean
     *
     * @param url - Url for a summary
     */
    public boolean checkUserToSummaryPermission(String  url) {

        Authentication user = SecurityContextHolder.getContext().getAuthentication();
        RoleFeatureMap roleFeatureMap = RoleFeatureMapFactory.getRoleFeatureMap();
        if (log.isTraceEnabled()) {
        	log.trace("checking to see if " + user.getName()
        			  + " has permission for " + url);
        }
        url = url.toLowerCase();
        return  roleFeatureMap.userHasURIAccess(user, url);
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 