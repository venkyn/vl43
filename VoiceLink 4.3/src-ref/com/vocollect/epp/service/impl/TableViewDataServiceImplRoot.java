/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.exceptions.FilterInvalidParameterException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.TableViewDataService;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DataProviderTableUtil;
import com.vocollect.epp.web.util.DataProviderUtil;

import java.util.List;


/**
 * A service that provides data for Table Component views. This service
 * is used so Spring transactional properties will be applied to the
 * calls even though the DataProviderUtil is calling the methods directly
 * by reflection, avoiding the Spring transaction proxies.
 *
 * @author ddoubleday
 */
public abstract class TableViewDataServiceImplRoot 
    implements TableViewDataService {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.TableViewDataServiceRoot#getTableData(com.vocollect.epp.util.ResultDataInfo,
     *      java.lang.Object, java.lang.String, com.vocollect.epp.model.User,
     *      java.util.List, java.lang.String, com.vocollect.epp.web.action.DataProviderAction, boolean)
     */
    public String getTableData(DataProviderUtil util,
                               ResultDataInfo rdi,
                               Object dataClass,
                               String getMethod,
                               User currentUser,
                               List<Column> columns,
                               String currentTime,
                               DataProviderAction action) {
        try {
            // now get the json string
             return util.getJSONData(
                rdi, dataClass, getMethod, currentUser, columns,
                currentTime, action, false);
        } catch (FilterInvalidParameterException fipe) {
            // We hit a filter parameter exception - so do the 
            // same method call - but with 0 true data since we want
            // to return 0 rows in this case.
            
            // now get the json string
            return util.getJSONData(
                rdi, dataClass, getMethod, currentUser, columns,
                currentTime, action, true);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.TableViewDataServiceRoot#checkSelectionData(java.lang.Object,
     *      java.lang.String, java.lang.String, boolean, com.vocollect.epp.model.Filter)
     */
    public List<DataObject> checkSelectionData(Object dataClass,
                                               String getMethod,
                                               String sortField,
                                               boolean sortAscending,
                                               Filter filter) {
        return DataProviderTableUtil.checkSelectionData(
            dataClass, getMethod, filter, sortField, sortAscending);
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 