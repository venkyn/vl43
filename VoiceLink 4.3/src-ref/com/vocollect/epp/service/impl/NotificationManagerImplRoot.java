/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.LabelObjectPair;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * Implementation of NotificationManager interface.
 *
 * @author Aich Debasis
 */
public abstract class NotificationManagerImplRoot extends
    GenericManagerImpl<Notification, NotificationDAO> implements
    NotificationManager {

    /**
     * SMTP host system property.
     */
    private static final String HOST = "SMTP_Host";

    /**
     * SMTP port system property.
     */
    private static final String PORT = "SMTP_Port";

    /**
     * SMTP user system property.
     */
    private static final String USER = "SMTP_Username";

    /**
     * SMTP password system property.
     */
    private static final String PASS = "SMTP_Password";

    /**
     * SMTP auth system property.
     */
    private static final String AUTH = "SMTP_Authentication";

    /**
     * Message logger.
     */
    private static final Logger log = new Logger(NotificationManagerImpl.class);

    /**
     * Property to read system property.
     */
    private SystemPropertyManager systemPropertyManager;

    /**
     * Property to send email.
     */
    private JavaMailSenderImpl javaMailSender;

    /**
     * Property to read from server.properties.
     */
    private Properties serverProperties;

    /**
     * Placeholder for the application url for current installation.
     *
     */
    private String appUrl = null;

    /**
     * Constructor.
     * @param primaryDAO the notification DAO.
     */
    public NotificationManagerImplRoot(NotificationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#sendEmailNotification
     * (com.vocollect.epp.model.Notification, java.lang.Long)
     */
    public void sendEmailNotification(Notification notification, Long id)
        throws Exception {
        String host = "";
        String port = "";
        String user = "";
        String pass = "";
        String auth = "";

        if (appUrl == null) {
            appUrl = "http://" + serverProperties.getProperty("server.name")
                + ":" + serverProperties.getProperty("server.port") + "/"
                + serverProperties.getProperty("server.contextRoot");
        }

        // Get the list of user emails. Send email only if we have a
        // valid list of email Ids
        List<String> emails = getUserEmailsBySiteId(id);

        if (emails != null && emails.size() > 0) {

            String[] emailList = new String[emails.size()];
            emailList = emails.toArray(emailList);
            SimpleMailMessage mailMsg = new SimpleMailMessage();
            mailMsg.setTo(emailList);
            mailMsg.setSubject(ResourceUtil
                .getLocalizedKeyValue("notification.email.subject.text"));

            // Get the Mail configuration from SystemProperty
            List<SystemProperty> sProList = systemPropertyManager.getAll();

            Iterator<SystemProperty> lItr = sProList.iterator();
            while (lItr.hasNext()) {
                SystemProperty sp = lItr.next();
                if (HOST.equals(sp.getName())) {
                    host = sp.getValue();
                }
                if (PORT.equals(sp.getName())) {
                    port = sp.getValue();
                }
                if (USER.equals(sp.getName())) {
                    user = sp.getValue();
                }
                if (PASS.equals(sp.getName())) {
                    pass = sp.getValue();
                }
                if (AUTH.equals(sp.getName())) {
                    auth = sp.getValue();
                }
            }

            if (host != null && !host.equals("undefined")) {
                // Set up email configuration
                javaMailSender = new JavaMailSenderImpl();
                javaMailSender.setPort(Integer.parseInt(port));
                javaMailSender.setHost(host);
                if ("true".equalsIgnoreCase(auth)) {
                    // Do we need to check for not null user/pass here or
                    // it will be maintained in the database?
                    javaMailSender.setUsername(user);
                    javaMailSender.setPassword(pass);
                }

                // Retrieve all notifications/Details not yet acknowledged
                // and are critical
                Map<String, Object> nMap = new HashMap<String, Object>();
                String propertyLookupValue;

                nMap.put("AppUrl", appUrl);
                propertyLookupValue = null;
                propertyLookupValue = ResourceUtil
                    .getLocalizedKeyValue(notification.getApplication());
                nMap.put("Application", (propertyLookupValue != null)
                    ? propertyLookupValue : notification.getApplication());

                propertyLookupValue = null;
                propertyLookupValue = ResourceUtil
                    .getLocalizedKeyValue(notification.getProcess());
                nMap.put("Process", (propertyLookupValue != null)
                    ? propertyLookupValue : notification.getProcess());

                nMap.put("Priority", ResourceUtil.getLocalizedKeyValue(ResourceUtil.makeEnumResourceKey(
                                        notification.getPriority())));

                propertyLookupValue = null;
                propertyLookupValue = ResourceUtil
                    .getLocalizedKeyValue(notification.getMessage());
                nMap.put("Message", (propertyLookupValue != null)
                    ? propertyLookupValue : notification.getMessage());
                nMap.put("Date", notification.getCreationDateTime().toString());
                nMap.put("recordId", notification.getId().toString());

                Set<NotificationDetail> nSet = notification.getDetails();
                if (nSet != null && nSet.size() > 0) {
                    Iterator<NotificationDetail>
                        notificationDetailListIterator = nSet.iterator();

                    JSONObject json = null;
                    JSONArray jsonArray = new JSONArray();

                    while (notificationDetailListIterator.hasNext()) {
                        NotificationDetail notificationDetail =
                            notificationDetailListIterator.next();
                        json = new JSONObject();

                        propertyLookupValue = null;
                        propertyLookupValue = ResourceUtil
                            .getLocalizedKeyValue(notificationDetail.getKey());

                        if (notificationDetail.getRenderType().equals("3")) {
                            // we have a link rendering type - so we need
                            // to prepend the URL to it
                            json.put((propertyLookupValue != null)
                                ? propertyLookupValue : notificationDetail
                                    .getKey(), " " + appUrl + "/"
                                + notificationDetail.getValue());

                        } else {
                            json.put((propertyLookupValue != null)
                                ? propertyLookupValue : notificationDetail
                                    .getKey(), " "
                                + notificationDetail.getValue());
                        }
                        jsonArray.put(Integer.parseInt(notificationDetail
                            .getOrdering()), json);
                    }

                    String str = jsonArray.toString();
                    nMap.put("nDstr", str.substring(1, str.length() - 1));
                } else {
                    nMap.put("nDstr", "");
                }

                String msg = processFreeMarker(nMap);
                mailMsg.setText(msg);
                try {
                    javaMailSender.send(mailMsg);
                } catch (MailSendException mse) {
                    log.error("Unable to send email message : " + mse,
                        SystemErrorCode.EMAIL_FAILURE);
                } catch (MailAuthenticationException mae) {
                    log.error("Unable to send email due to authentication : "
                        + mae, SystemErrorCode.EMAIL_FAILURE);
                }
            }
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#getUserEmailsBySiteId(java.lang.Long)
     */
    public List<String> getUserEmailsBySiteId(Long id)
        throws DataAccessException {

        List<String> emailList = getPrimaryDAO().getUserEmailsBySiteId(id);

        if (emailList.size() == 0) {
            return null;
        }

        return emailList;

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#getGroupedCountBySite(com.vocollect.epp.model.User, com.vocollect.epp.util.NotificationPriority)
     */
    public List<Object[]> getGroupedCountBySite(User user, NotificationPriority np)
        throws DataAccessException {
        List<Object[]> summaryData = null;
        if (user.getAllSitesAccess()) {
            summaryData = getPrimaryDAO()
                .getGroupedCountBySiteForAllSiteAccess(np);

            // If the user has all site access and is an administrator -
            // go ahead and give them the System notification summary as well
            // Append the system critical notifications as well
            if (user.isAdministrator()) {
                int systemCount = convertNumberToInt(
                    getPrimaryDAO().getUnackedSystemNotificationsCount(np));
                if (systemCount > 0) {
                    // We need to add it to the list
                    Object[] systemEntry = new Object[3];
                    systemEntry[0] = ResourceUtil
                        .getLocalizedKeyValue("notification.dropdown.site.System");
                    systemEntry[1] = new Long(Tag.SYSTEM);
                    systemEntry[2] = new Integer(systemCount);
                    summaryData.add(systemEntry);
                }
            }
        } else {
            summaryData = getPrimaryDAO().getGroupedCountBySite(
                user.getName(), np);
        }

        return summaryData;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#getMaxNotificationIdForSiteId(com.vocollect.epp.util.NotificationPriority, java.lang.Long)
     */
    public Long getMaxNotificationIdForSiteId(NotificationPriority np,
                                              Long siteId)
        throws DataAccessException {
        if (siteId == Tag.SYSTEM) {
            return getPrimaryDAO().getMaxNotificationIdForSystemTag(np);
        } else {
            return getPrimaryDAO().getMaxNotificationIdForSiteId(np, siteId);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#getNotificationMap(java.lang.Long)
     */
    public Map<String, Object> getNotificationMap(Long notificationId)
        throws DataAccessException {
        Map<String, Object> nMap = new HashMap<String, Object>();

        Notification n = this.get(notificationId);
        nMap.put("Application", n.getApplication());
        nMap.put("Host", n.getHost());
        nMap.put("Process", n.getProcess());
        nMap.put("ErrorNumber", n.getErrorNumber());
        nMap.put("Message", n.getMessage());
        nMap.put("Priority", n.getPriority());
        nMap.put("CreationDateTime", n.getCreationDateTime().toString());

        if (!StringUtil.isNullOrEmpty(n.getAcknowledgedUser())) {
            nMap.put("AcknowledgedUser", n.getAcknowledgedUser());
        } else {
            nMap.put("AcknowledgedUser", "");
        }
        if ((n.getAcknowledgedDateTime() != null)
            && (!StringUtil.isNullOrEmpty(n.getAcknowledgedDateTime()
                .toString()))) {
            nMap.put("AcknowledgedDateTime", n.getAcknowledgedDateTime()
                .toString());
        } else {
            nMap.put("AcknowledgedDateTime", "");
        }

        return nMap;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#updateAck(java.lang.Long, java.lang.String, java.lang.String)
     */
    public String updateAck(Long id, String ackUser, String ackDate)
        throws DataAccessException, ParseException, BusinessRuleException {
        if (StringUtil.isNullOrEmpty(ackUser)) {
            return "No Username Supplied.";
        }

        if (StringUtil.isNullOrEmpty(ackDate)) {
            return "No Date Supplied.";
        }

        Notification n = this.get(id);
        if (!StringUtil.isNullOrEmpty(n.getAcknowledgedUser())) {
            return "Notification already acknowledged.";
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = dateFormat.parse(ackDate);
        n.setAcknowledgedDateTime(date);
        n.setAcknowledgedUser(ackUser);

        this.save(n);
        return "Notification acknowledged.";
    }

    /**
     * @param systemPropertyManager The systemPropertyManager to set.
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * @param serverProperties the server properties.
     */
    public void setServerProperties(Properties serverProperties) {
        this.serverProperties = serverProperties;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManager#createNotificationAndEmail(com.vocollect.epp.model.Notification)
     */
    public void createNotificationAndEmail(Notification n) throws Exception {
        this.save(n);

        // If it is a Critical notification, send and email
        if (n.getPriority().equals(NotificationPriority.CRITICAL)) {
            Iterator<Tag> iTe = n.getTags().iterator();
            while (iTe.hasNext()) {
                Tag t = iTe.next();
                if (t.getTagType() == 1) {
                    sendEmailNotification(n, t.getTaggableId());
                }
            }
        }
    }

    /**
     * Create a Notification.
     * @param processKey - resource key of the process
     * @param messageKey - resource key of the message
     * @param priority - notification priority level
     * @param timeOfAction - timestamp of the notification
     * @param errorNumber - String representation of an error number
     * @param application -
     * @param argDetails - LabelObjectPair of notificaiton detail information
     * @throws Exception - all exceptions
     */
    public void createNotification(String processKey,
                                   String messageKey,
                                   NotificationPriority priority,
                                   Date timeOfAction,
                                   String errorNumber,
                                   String application,
                                   List<LabelObjectPair> argDetails)
        throws Exception {

        Notification n =  new Notification();

        String host = serverProperties.getProperty("server.name");
        if (host == null) {
            host = "notification.column.Host.unknown";
        }
        n.setHost(host);

        if (application != null) {
            n.setApplication(application);
        } else {
            n.setApplication("notification.column.keyname.Application.2");
        }

        n.setProcess(processKey);
        n.setMessage(messageKey);
        n.setPriority(priority);
        n.setCreationDateTime(timeOfAction);
        n.setErrorNumber(errorNumber);

        // Save any parameters in the notificationDetails
        if (argDetails.size() > 0) {
            createDetails(n, argDetails);
        }

        // If failing to save the notification throws an error
        // we need to catch this, log it, and allow the task
        // to get the error message back it was processing.
       createNotificationAndEmail(n);

    }

    /**
     * Extract the message parameters from TaskMessageInfo and save each as a
     * NotificationDetail object of the Notification pass.
     * @param n Notification Object
     * @param argDetails - LabelObjectPair list of notification details
     */
    protected void createDetails(Notification n,
                                 List<LabelObjectPair> argDetails) {

        NotificationDetail detail = null;
        Integer order = 0;

        for (LabelObjectPair lop : argDetails) {
            detail = new NotificationDetail();
            //TODO - should change so that it works for all render types - sv
            //Dont use it if the renderType is not 1
            detail.setRenderType("1");
            detail.setOrdering(order.toString());
            detail.setKey(lop.getLabel());
            if (lop.getValue() != null) {
                detail.setValue(lop.getValue().toString());
            } else {
                detail.setValue("null");
            }
            n.getDetails().add(detail);

            // Increment Order
            order++;
        }

    }

    /**
     * Helper to build a system notificaiton object with the
     * server name set and the application set to System.
     * @return a basic epp notification object
     */
    public Notification systemNotification() {
        Notification n = new Notification();
        n.setHost(serverProperties.getProperty("server.name"));
        n.setApplication("notification.column.keyname.Application.2");
        return n;
    }


    /**
     * @param root map of variables to use when processing template.
     * @return the processed template file as a String.
     * @throws TemplateException on failure to process.
     * @throws IOException on failure to find template.
     */
    private String processFreeMarker(Map<?, ?> root) throws TemplateException,
        IOException {

        Configuration cfg = new Configuration();
        cfg.setClassForTemplateLoading(getClass(), "");
        // We have stored the email Notification ftl beside the
        // NotificationManagerImpl class file so the class loader can find it
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        Template temp = cfg.getTemplate("emailNotification.ftl");
        StringWriter swtr = new StringWriter();
        temp.process(root, swtr);
        return swtr.toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManagerRoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date)
     */
    public List<Notification> listOlderThan(QueryDecorator decorator, Date date)
    throws DataAccessException {
        return getPrimaryDAO().listOlderThan(decorator, date);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date)
     */
    public int executePurge(QueryDecorator decorator, Date olderThan) {
        int returnRecords = 0;
        List<Notification> notification = null;

        //Get List of notifications to purge
        if (log.isDebugEnabled()) {
            log.debug("### Finding Notification History to Purge :::");
        }
        try {
            notification = listOlderThan(decorator, olderThan);
            returnRecords = notification.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting NOTIFICATION "
                + "HISTORY from database to purge: " + e
                );
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords + " Notification Histroy records for purge :::");
        }
        for (Notification n : notification) {
            try {
                delete(n);
            } catch (Throwable t) {
                log.warn("!!! Error purging notification "
                    + n.getId()
                    + " from database: " + t
                    , t);
            }
        }

        getPrimaryDAO().clearSession();
        return returnRecords;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NotificationManagerRoot#getUnackedSystemNotificationsCount(java.lang.String, java.lang.String)
     */
    public int getUnackedSystemNotificationsCount(String processName,
                                                  String terminalNumber)
        throws DataAccessException {
        int notificationCount = getPrimaryDAO()
            .getUnackedSystemNotificationsCount(processName, terminalNumber);
        return notificationCount;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 