/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.SiteContextHolder;

/**
 * Implementation of SiteManager interface.
 *
 * @author KalpnaT
 */
public abstract class SiteManagerImplRoot extends GenericManagerImpl<Site, SiteDAO>
    implements SiteManager {

    private TagManager tagManager = null;

    /**
     * Message logger.
     */
    private static final Logger log = new Logger(SiteManagerImplRoot.class);


    /**
     * Constructor.
     * @param primaryDAO the primary DAO
     * (the instantiation for the persistent class)
     */
    public SiteManagerImplRoot(SiteDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Site site)
        throws BusinessRuleException, DataAccessException {
        try {
            // TODO, make sure there is an admin

            if (SiteContextHolder.getSiteContext().getCurrentSite().getId().equals(site.getId())) {
                throw new BusinessRuleException(null, new UserMessage("site.delete.message.error"));
            }

            Tag t = SiteContextHolder.getSiteContext().getTagBySiteId(site.getId());
            tagManager.delete(t);
            return super.delete(site);
        } catch (DatabaseConstraintException e) {
            throw new BusinessRuleException(null, new UserMessage("site.delete.message.references"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SiteManager#save(com.vocollect.epp.model.Site)
     */
    @Override
    public Object save(Site site)
        throws BusinessRuleException, DataAccessException {
        // only add a tag if we are creating
        boolean b = site.isNew();

         //Not sure if we have any business rules validation here.
         Object retVal = super.save(site);

         if (b) {

             Tag t = new Tag();
             t.setTaggableId(site.getId());
             t.setTagType(Tag.SITE);
             tagManager.save(t);
         }
          return retVal;
    }

    /**
     * {@inheritDoc}
     * @throws DataAccessException
     * @see com.vocollect.epp.service.SiteManager#findDefaultSiteForUser(com.vocollect.epp.model.User)
     */
    public Tag findDefaultSiteForUser(User u) throws DataAccessException {
       return getPrimaryDAO().getSiteByMinId(u);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SiteManager#getNumberOfSites(com.vocollect.epp.model.User)
     */
    public int getNumberOfSites(User u) throws DataAccessException {
        if (u.getAllSitesAccess()) {
            return  tagManager.findSiteTags().size() - 2;
        }
        return u.getTags().size();
    }

    /**
     * Setter for the tagManager property.
     * @param tagManagerParam the new tagManager value
     */
    public void setTagManager(TagManager tagManagerParam) {
        this.tagManager = tagManagerParam;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SiteManager#findByName(java.lang.String)
     */
    public Site findByName(String name) throws DataAccessException {
        return this.getPrimaryDAO().findByName(name);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.SiteManagerRoot#findDefaultSite()
     */
    public Site findDefaultSite() {
        return getPrimaryDAO().findDefaultSite();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 