/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.NetworkCredentialDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.eap.TrustedCertificateInfo;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NetworkCredential;
import com.vocollect.epp.service.EncryptionManager;
import com.vocollect.epp.service.NetworkCredentialManager;
import com.vocollect.epp.util.DataFileReferenceUtil;
import com.vocollect.epp.util.MD5;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.ssl.TrustMaterial;

/**
 * 
 * @author brupert
 */
public class NetworkCredentialManagerImpl extends GenericManagerImpl<NetworkCredential, NetworkCredentialDAO> 
    implements NetworkCredentialManager {

    private static final Logger log = new Logger(NetworkCredentialManagerImpl.class);
    private static final String TRUST_USER_NAME = "trustCertificate";

    /**
     * Constructor.
     * @param primaryDAO .
     */
    public NetworkCredentialManagerImpl(NetworkCredentialDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * Generate an MD5 of the network credential and store it to the MD5 field 
     * of the object.
     * 
     * @param netCred .
     * @param isCertCred .
     */
    public void generateMd5(NetworkCredential netCred, boolean isCertCred) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            
            if (hasContent(netCred.getUserName())) {
                md.update(netCred.getUserName().getBytes());
            }
            
            if (isCertCred) {
                if (hasContent(netCred.getCertificateFileName())) {
                    md.update(netCred.getCertificateFileContents());
                }
                
                if (hasContent(netCred.getCertificateKeyFileName())) {
                    md.update(netCred.getCertificateKeyFileContents());
                }
                
                if (hasContent(netCred.getCertificatePassword())) {
                    md.update(netCred.getCertificatePassword().getBytes());
                }
            } else {
                if (hasContent(netCred.getPassword())) {
                    md.update(netCred.getPassword().getBytes());
                }
            }

            netCred.setMd5(MD5.toHexString(md.digest()));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NetworkCredentialManager#encrypt(com.vocollect.epp.model.NetworkCredential, boolean, com.vocollect.epp.service.EncryptionManager)
     */
    public void encrypt(NetworkCredential netCred, boolean isCertCred, EncryptionManager em) {
        encrypt(netCred, isCertCred, em, null);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NetworkCredentialManager#encrypt(com.vocollect.epp.model.NetworkCredential, boolean, com.vocollect.epp.service.EncryptionManager, java.lang.String)
     */
    public void encrypt(NetworkCredential netCred, boolean isCertCred, EncryptionManager em, String pin) {
        
        if (hasContent(netCred.getPlainUserName())) {
            netCred.setPlainUserName(new String(em.encrypt(
                netCred.getPlainUserName().getBytes(), EncryptionManagerImpl.PRIVATE_PIN)));
        }
        
        if (hasContent(netCred.getUserName())) {
            netCred.setUserName(em.encryptAndEncode(netCred.getUserName().getBytes(), pin));
        }
        
        if (isCertCred) {
            if (hasContent(netCred.getCertificateFileName())) {
                netCred.setCertificateFileContents(em.encrypt(em.encrypt(netCred.getCertificateFileContents(), pin)));
            }
            
            if (hasContent(netCred.getCertificateKeyFileName())) {
                netCred.setCertificateKeyFileContents(em.encrypt(em.encrypt(
                    netCred.getCertificateKeyFileContents(), pin)));
            }
            
            if (hasContent(netCred.getCertificatePassword())) {
                netCred.setCertificatePassword(em.encryptAndEncode(netCred.getCertificatePassword().getBytes(), pin));
            }
        } else {
            if (hasContent(netCred.getPassword())) {
                netCred.setPassword(em.encryptAndEncode(netCred.getPassword().getBytes(), pin));
            }
        }
    }
    
    /**
     * @param toTest .
     * @return .
     */
    private boolean hasContent(String toTest) {
        return toTest != null && toTest != "";
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NetworkCredentialManager#isAlreadyTrusted(com.vocollect.epp.util.DataFileReferenceUtil)
     */
    public boolean isAlreadyTrusted(DataFileReferenceUtil dataVO) throws VocollectException {
        TrustMaterial trustMaterial = null;
        List<TrustedCertificateInfo> existingTrust = listTrust();
        
        // Attempt to create the trust material from the file we're given
        try {
            trustMaterial = new TrustMaterial(dataVO.getFileContents());
        } catch (Exception e) {
            throw new VocollectException(e);
        }
        
        // Grab the corresponding KeyStore
        KeyStore caCerts = trustMaterial.getKeyStore();
        
        // Grab the list of aliases
        Enumeration<String> caAliases;
        try {
            caAliases = caCerts.aliases();
        } catch (KeyStoreException e) {
            log.warn("Problems encountered while enumerating aliases", e);
            throw new VocollectException(e);
        }
        
        boolean found = false;
        // Iterate over the aliases
        while (caAliases.hasMoreElements()) {
            String alias = caAliases.nextElement();
    
            try {
                // Everything in this KeyStore should be an X.509 certificate.
                X509Certificate caCertificate = 
                    (X509Certificate) caCerts.getCertificate(alias);
            
                TrustedCertificateInfo tci = new TrustedCertificateInfo(
                        caCertificate.getSubjectDN().getName(),
                        md5Fingerprint(caCertificate),
                        sha1Fingerprint(caCertificate)
                );
                
                // See if the list contains this cert already
                found = found || existingTrust.contains(tci);
            } catch (NoSuchAlgorithmException e) {
                log.warn(e);
            } catch (CertificateEncodingException e) {
                log.warn(e);
            } catch (KeyStoreException e) {
                log.warn(e);
            }
        }
        
        return found;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NetworkCredentialManager#createTrustCertificate(com.vocollect.epp.util.DataFileReferenceUtil)
     */
    public void createTrustCertificate(DataFileReferenceUtil dataVO)
    throws VocollectException, DataAccessException {
        NetworkCredential creating = new NetworkCredential();
        
        try {
            new TrustMaterial(dataVO.getFileContents());
        } catch (Exception e) {
            throw new VocollectException(e);
        }
        
        creating.setUserName(TRUST_USER_NAME);
        creating.setCertificateFileName(dataVO.getName());
        
        dataVO.store();
        
        getPrimaryDAO().save(creating);
    }
        
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.NetworkCredentialManager#getAllTrustCertificates()
     */
    public List<NetworkCredential> getAllTrustCertificates() {
        return getPrimaryDAO().listByUserName(TRUST_USER_NAME);
    }
        
    /**
     * Get the list of trusted certificate authorities installed for this JVM.
     * The location of the keystore is $JAVA_HOME/jre/lib/security/cacerts.
     * @return An ArrayList of TrustedCertificateInfo
     * keystore
     */
    public List<TrustedCertificateInfo> listTrust() {
        ArrayList<TrustedCertificateInfo> trustInfo = new ArrayList<TrustedCertificateInfo>();
        
        // Iterate over all of the TrustMaterial the user has defined
        List<NetworkCredential> userDefinedTrust = getAllTrustCertificates();
        Iterator<NetworkCredential> i = userDefinedTrust.iterator();
        while (i.hasNext()) {
            NetworkCredential thisCred = i.next();
            DataFileReferenceUtil credentialFile = new DataFileReferenceUtil();
            credentialFile.setName(thisCred.getCertificateFileName());
            
            try {
                TrustMaterial thisTrust = new TrustMaterial(credentialFile.getFileContents());
                addTrustMaterial(thisTrust, trustInfo);
            } catch (Exception e) {
                log.warn("Error encountered while adding trust material from "
                    + thisCred.getCertificateFileName(), e);
            }
            
        }
        
        // Add the "usual suspects"
        addTrustMaterial(TrustMaterial.CACERTS, trustInfo);
        
        return trustInfo;
    }
        
    /**
     * @param trustMaterial .
     * @param trustInfo .
     * @return .
     */
    private ArrayList<TrustedCertificateInfo> 
        addTrustMaterial(TrustMaterial trustMaterial,
                         ArrayList<TrustedCertificateInfo> trustInfo) {
        KeyStore caCerts = trustMaterial.getKeyStore();
        
        Enumeration<String> caAliases;
        try {
            caAliases = caCerts.aliases();
        } catch (KeyStoreException e) {
            log.warn("Problems encountered while enumerating aliases", e);
            return trustInfo;
        }
        
        // Iterate over the aliases
        while (caAliases.hasMoreElements()) {
            String alias = caAliases.nextElement();
        
            try {
                // Everything in this KeyStore should be an X.509 certificate.
                X509Certificate caCertificate = 
                    (X509Certificate) caCerts.getCertificate(alias);
            
                TrustedCertificateInfo tci = new TrustedCertificateInfo(
                        caCertificate.getSubjectDN().getName(),
                        md5Fingerprint(caCertificate),
                        sha1Fingerprint(caCertificate)
                );
                
                trustInfo.add(tci);
            } catch (NoSuchAlgorithmException e) {
                log.warn(e);
            } catch (CertificateEncodingException e) {
                log.warn(e);
            } catch (KeyStoreException e) {
                log.warn(e);
            }
        }
        
        return trustInfo;
    }
    
    /**
     * Helper method to generate a md5 fingerprint from the X509Certificate.  
     * Nothing special here, it just grabs the encoded bytes of the certificate
     * and md5's them.
     * @param cert The certificate to generate the md5 fingerprint for
     * @return A HEX encoded string of the MD5 fingerprint in the form of 
     * AA:BB:CC:DD...
     * @throws CertificateEncodingException If the call to 
     * Certificate.getEncoded throws this exception.
     * @throws NoSuchAlgorithmException If MessageDigest.getInstance cannot 
     * find an MD5 algorithm
     */
    public static String md5Fingerprint(X509Certificate cert) 
        throws CertificateEncodingException, NoSuchAlgorithmException {
        byte[] encoded = cert.getEncoded();
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.reset();
        byte[] md5Result = md5.digest(encoded);
        return toHexString(md5Result);
    }
    
    /**
     * Helper method to generate a sha1 fingerprint from the X509Certificate.  
     * Nothing special here, it just grabs the encoded bytes of the certificate
     * and sha1's them.
     * @param cert The certificate to generate the sha1 fingerprint for
     * @return A HEX encoded string of the SHA1 fingerprint in the form of 
     * AA:BB:CC:DD...
     * @throws CertificateEncodingException If the call to 
     * Certificate.getEncoded throws this exception.
     * @throws NoSuchAlgorithmException If MessageDigest.getInstance cannot 
     * find a SHA1 algorithm
     */
    public static String sha1Fingerprint(X509Certificate cert) 
        throws CertificateEncodingException, NoSuchAlgorithmException {
        byte[] encoded = cert.getEncoded();
        MessageDigest sha1 = MessageDigest.getInstance("SHA1");
        sha1.reset();
        byte[] shaResult = sha1.digest(encoded);
        return toHexString(shaResult);
    }
    
    /**
     * Helper method to convert a byte[] into a hex String where each byte
     * is represented by two characters 0-F.  This is useful as the standard
     * representation of MD5 checksum data.
     *
     * @param data the byte[] to convert
     * @return a String hexidecimal representation of the byte array data
     */
    public static String toHexString(byte[] data) {
        StringBuffer hexString = new StringBuffer();
        String hex = null;
        for (int i = 0; i < data.length; i++) {
            // can't directly typecast a byte to an integer...have to preform
            // this bit of bit manipulation
            hex = Integer.toHexString(0xFF & data[i]);
            // make sure we spit out 2 characters for every byte of data
            if (hex.length() < 2) {
                hexString.append("0");
            }
            hexString.append(hex);
            
            if (i < data.length - 1) {
                hexString.append(":");
            }
        }

        return hexString.toString().toUpperCase();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 