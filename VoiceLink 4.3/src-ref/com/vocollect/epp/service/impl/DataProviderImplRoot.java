/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.service.DataProvider;

/**
 * Implements the DataProvider interface to provide a generic implementation
 * that other service objects can inherit.
 *
 *
 * @author dkertis
 */
public abstract class DataProviderImplRoot implements DataProvider {

    // private static final Logger log = new Logger(DataProviderImpl.class);

    /**
     * This method simply returns the <code>getProviderDAO()</code> object.
     * Override this method if needed. {@inheritDoc}
     * @see com.vocollect.epp.service.DataProvider#getProviderDAO(java.lang.String)
     */
    public DataProviderDAO getProviderDAO(String name) {
        return this.getProviderDAO();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 