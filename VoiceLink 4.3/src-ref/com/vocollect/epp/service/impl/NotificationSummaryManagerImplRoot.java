/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.NotificationSummaryManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.NotificationSummary;
import com.vocollect.epp.util.ResultDataInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of NotificationSummaryManager interface.
 *
 * @author Aich Debasis
 */
public abstract class NotificationSummaryManagerImplRoot extends
    GenericManagerImpl<Notification, NotificationDAO> implements
    NotificationSummaryManager {

    private static final String ACCESS_TO_ONE_SITE = "AccessToOneSite";

    private User currUser;

    private NotificationManager notificationManager;

    private TagManager tagManager;

    /**
     * Constructor for notificationSummaryManagerImpl.
     * @param primaryDAO the DAO primarily used.
     */
    public NotificationSummaryManagerImplRoot(NotificationDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.DataProviderImpl#getData(com.vocollect.epp.util.ResultDataInfo)
     */
    public List<DataObject> getData(ResultDataInfo rdi) throws DataAccessException {
        List result = notificationManager.getGroupedCountBySite(getCurrUser(), NotificationPriority.CRITICAL);
        List<DataObject> summaries = new ArrayList<DataObject>();

        if (result.size() == 1) {
            Object[] obj = (Object[]) result.get(0);
            String sName = (String) obj[0];
            Long sId = (Long) obj[1];
            Integer sCount = ((Number) obj[2]).intValue();
            NotificationSummary ns = new NotificationSummary();
            ns.setNotificationCount(new Long(sCount.toString()));
            ns.setSiteId(sId);
            ns.setTagId(this.getTagId(sId));
            ns.setSiteName(sName);
            ns.setFlag(ACCESS_TO_ONE_SITE);
            summaries.add(ns);
        } else {
            for (int i = 0; i < result.size(); i++) {
                Object[] obj = (Object[]) result.get(i);
                String sName = (String) obj[0];
                Long sId = (Long) obj[1];
                Integer sCount = ((Number) obj[2]).intValue();
                NotificationSummary ns = new NotificationSummary();
                ns.setNotificationCount(new Long(sCount.toString()));
                ns.setSiteId(sId);
                ns.setTagId(this.getTagId(sId));
                ns.setFlag("");
                ns.setSiteName(sName);
                summaries.add(ns);
                }
        }
        return summaries;
    }

    /**
     * Returns the notificationManager.
     * @return NotificationManager
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * Sets the notificationManager.
     * @param notificationManager to set
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Gets the user to execute query for.
     * @return User
     */
    public User getCurrUser() {
        return currUser;
    }

    /**
     * Sets the user to execute query for.
     * @param currUser to set
     */
    public void setCurrUser(User currUser) {
        this.currUser = currUser;
    }

    /**
     * Getter for the tagManager property.
     * @return TagManager value of the property
     */
    public TagManager getTagManager() {
        return this.tagManager;
    }


    /**
     * Setter for the tagManager property.
     * @param tagManager the new tagManager value
     */
    public void setTagManager(TagManager tagManager) {
        this.tagManager = tagManager;
    }

    /**
     * @param tagId the ID of the item
     * @return the ID of the tag for the tagged item
     * @throws DataAccessException on failure to retrieve.
     */
    private Long getTagId(Long tagId) throws DataAccessException {
        return this.tagManager.findByTaggedObjectId(Tag.SITE, tagId).getId();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 