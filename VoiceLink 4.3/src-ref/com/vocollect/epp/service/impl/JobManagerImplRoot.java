/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.JobDAO;
import com.vocollect.epp.dao.JobHistoryDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.JobHistory;
import com.vocollect.epp.model.ScheduleType;
import com.vocollect.epp.scheduling.SchedulingErrorCode;
import com.vocollect.epp.service.JobManager;
import com.vocollect.epp.util.ResultDataInfo;

import static com.vocollect.epp.model.ScheduleLastResult.SKIPPED;
import static com.vocollect.epp.model.ScheduleLastResult.STOPPED;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

/**
 * Implementation of JobManager interface.
 * 
 * @author jkercher
 */
public abstract class JobManagerImplRoot 
    extends GenericManagerImpl<Job, JobDAO> implements JobManager {

    private static final Logger log = new Logger(JobManagerImpl.class);

    private JobHistoryDAO jobHistoryDAO;

    /**
     * Constructor.
     * @param primaryDAO the primary DAO for this manager.
     */
    public JobManagerImplRoot(JobDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManager#updateAndGetAllJobs()
     */
    public List<Job> updateAndGetAllJobs() throws DataAccessException {
        List<Job> jobs = getPrimaryDAO().getAll();
        List<Job> syncJobs = new ArrayList<Job>(jobs.size());
        for (Job job : jobs) {
            syncJobs.add(synchronizeJob(job));
        }
        return syncJobs;

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManager#saveJob(com.vocollect.epp.model.Job)
     */
    public void saveJob(Job job) throws DataAccessException {
        if (ScheduleType.INTERVAL.equals(job.getType())) {
            job.setRepeatCount(Job.CONTINUOUS_RUN);
        }
        getPrimaryDAO().save(job);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManager#saveJobHistory(com.vocollect.epp.model.JobHistory)
     */
    public void saveJobHistory(JobHistory history) throws DataAccessException {
        this.jobHistoryDAO.save(history);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManagerRoot#executeStartJob(long)
     */
    public void executeStartJob(long id) throws DataAccessException {
        
        Job job = updateAndGetJob(id);
        String jobName = job.getName();
        
        Scheduler scheduler = this.getScheduler();
 
        try {
            if ((job.isRunning()) 
                && ((job.getLastPing() + job.getPingInterval()) > System.currentTimeMillis())) {
                this.throwAlreadyRunningException(job.getName());
            }
            
            List<JobExecutionContext> runningJobs = scheduler.getCurrentlyExecutingJobs();
            for (JobExecutionContext ctx : runningJobs) {
                if (ctx.getJobDetail().getName().equals(job.getName())) {
                    ctx.getJobDetail().getJobDataMap().put(
                        Job.LAST_RUN_RESULT, SKIPPED);
                    ctx.getJobDetail().getJobDataMap().put(
                        Job.LAST_RUN, new Date(System.currentTimeMillis()));

                    if (ctx.getNextFireTime() != null) {
                        ctx.getJobDetail().getJobDataMap().put(
                            Job.NEXT_RUN, ctx.getNextFireTime());
                    }

                    this.throwAlreadyRunningException(job.getName());
                }
            }
            if (log.isDebugEnabled()) {
                log.debug("This job : " + jobName + " is being forced to run...");
            }
            job.getJobDetail().getJobDataMap().put(Job.RUN_THIS_JOB, true);

            scheduler.triggerJob(job.getName(), job.getGroup());
       } catch (SchedulerException se) {
            log.warn("SchedulerException - " + se.getClass().getCanonicalName()
                + " - " + se.getMessage());
            SchedulingErrorCode code = SchedulingErrorCode.UNABLE_TO_SCHED_JOB;
            UserMessage msg = new UserMessage(
                "schedule.run.error.schedule", job.getDisplayName(), se
                    .getMessage());

            throw new DataAccessException(code, msg, se);
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.service.JobManager#executeStopJob(long)
     */
    public void executeStopJob(long id) throws DataAccessException {
        Job job = updateAndGetJob(id);
        Scheduler scheduler = this.getScheduler();

        try {
            List<JobExecutionContext> runningJobs = scheduler.getCurrentlyExecutingJobs();
            boolean running = false;

            for (JobExecutionContext ctx : runningJobs) {
                if (ctx.getJobDetail().getName().equals(job.getName())) {
                    running = true;
                    break;
                }
            }

            if (running) {
                scheduler.interrupt(job.getName(), job.getGroup());
                job.getJobDetail().getJobDataMap().put(
                    Job.LAST_RUN_RESULT, STOPPED);
                log.warn("Manually stopped the " + job.getName() + " job.");
            } else {
                this.throwNotRunningException(job.getName());
            }
        } catch (SchedulerException se) {
            log.warn("SchedulerException - " + se.getMessage());
            SchedulingErrorCode code = SchedulingErrorCode.UNABLE_TO_STOP_JOB;
            UserMessage msg = new UserMessage(
                "schedule.stop.error.schedule", job.getDisplayName(), se
                    .getMessage());

            throw new DataAccessException(code, msg, se);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManagerRoot#updateAndGetJob(long)
     */
    public Job updateAndGetJob(long id) throws DataAccessException {
        Job job = getPrimaryDAO().get(id);
        return synchronizeJob(job);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManager#updateAndFindJobByName(java.lang.String)
     */
    public Job updateAndFindJobByName(String name) throws DataAccessException {
        if (log.isDebugEnabled()) {
            log.debug("FINDING BY NAME.....");
        }
        Job job = getPrimaryDAO().retrieveByName(name);
        return synchronizeJob(job);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.RemovableDataProvider#getDataObject(java.lang.Long)
     */
    public DataObject getDataObject(Long id) throws DataAccessException {
        return updateAndGetJob(id);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.RemovableDataProviderImpl#delete(java.lang.Long)
     */
    public Object delete(Long id) throws DataAccessException,
        BusinessRuleException {
        // do nothing
        return null;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManagerRoot#executePing(java.lang.String)
     */
    public void executePing(String name) throws DataAccessException {
        getPrimaryDAO().ping(name);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManagerRoot#poll(java.lang.String)
     */
    public long executePoll(String name) throws DataAccessException {
        return getPrimaryDAO().poll(name);
    }

    /**
     * Getter for the jobHistoryDAO property.
     * @return JobHistoryDAO value of the property
     */
    public JobHistoryDAO getJobHistoryDAO() {
        return this.jobHistoryDAO;
    }

    /**
     * Setter for the jobHistoryDAO property.
     * @param jobHistoryDAO the new jobHistoryDAO value
     */
    public void setJobHistoryDAO(JobHistoryDAO jobHistoryDAO) {
        this.jobHistoryDAO = jobHistoryDAO;
    }
    
    /**
     * @param scheduler the scheduler to set.
     */
    public void setScheduler(Scheduler scheduler) {
        getPrimaryDAO().setScheduler(scheduler);
    }
    
    /**
     * @return the current scheduler.
     */
    public Scheduler getScheduler() {
        try {
            return getPrimaryDAO().getScheduler();
        } catch (Exception e) {
            log.warn("Could not get scheduler object", e);
        }
        return null;
    }
    
    /**
     * Throws the job already running exception.
     * @param jobName the name
     * @throws DataAccessException the exception
     */
    private void throwAlreadyRunningException(String jobName) 
        throws DataAccessException {
        
        SchedulingErrorCode code = SchedulingErrorCode.JOB_ALREADY_RUNNING;
        UserMessage msg = new UserMessage(
            "schedule.run.error.running", jobName);
        
        throw new DataAccessException(
            code, msg, new SchedulerException());
    }
    
    /**
     * Throws the job already stopped exception.
     * @param jobName the name
     * @throws DataAccessException the exception
     */
    private void throwNotRunningException(String jobName) 
        throws DataAccessException {
        
        SchedulingErrorCode code = SchedulingErrorCode.JOB_NOT_RUNNING;
        UserMessage msg = new UserMessage(
            "schedule.stop.error.stopped", jobName);

        throw new DataAccessException(
            code, msg, new SchedulerException());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.JobManagerRoot#executePurge(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date)
     */
    public int executePurge(QueryDecorator decorator, Date olderThan) {
        int returnRecords = 0;
        List<JobHistory> jobHistory = null;
        
        //Get List of jobHistory to purge
        if (log.isDebugEnabled()) {
            log.debug("### Finding Job History to Purge :::");
        }
        try {
            jobHistory = getJobHistoryDAO().listOlderThan(decorator, olderThan);
            returnRecords = jobHistory.size();
        } catch (DataAccessException e) {
            log.warn("!!! Error getting JOB "
                + "HISTORY from database to purge: " + e
                );
            return 0;
        }
        if (log.isDebugEnabled()) {
            log.debug("### Found " + returnRecords + " Job History records for purge :::");
        }
        for (JobHistory jh : jobHistory) {
            try {                
                getJobHistoryDAO().delete(jh);
            } catch (Throwable t) {
                log.warn("!!! Error purging transactional Job History " 
                    + jh.getId() 
                    + " from database: " + t
                    , t);
            }            
        }
        getJobHistoryDAO().clearSession();
        return returnRecords;
    }

    /**
     * @param rdi the ResultDataInfo
     * @return the list of objects, synchronized with Quartz.
     * @throws DataAccessException on database failure
     */
    @SuppressWarnings("unchecked")
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {

        // First get all jobs in the normal way.
        List<DataObject> unsyncedJobs = getPrimaryDAO().getAll(rdi);
        
        // Now build a list of jobs synced with quartz in-memory info.
        List<DataObject> jobs = new ArrayList<DataObject>();
        for (DataObject job : unsyncedJobs) {
            // Detach the Job from the session because we don't want to 
            // try to write back to the DB in this read-only transaction.
            getPrimaryDAO().detach((Job) job);
            // Synchronize with quartz.
            jobs.add(synchronizeJob(((Job) job)));
        }

        return jobs;
    }
    

    /**
     * Builds a synchronized Job.
     * @param job the persisted job
     * @return the synchronized job if it can
     */
    private Job synchronizeJob(Job job) {
        try {
            JobDetail det = 
                this.getScheduler()
                .getJobDetail(job.getName(), job.getGroup());
            if (det != null) {
                job.setDetail(det);
            }
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        return job;
    }

}

    



*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 