/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ColumnDAO;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.service.ColumnManager;

/**
 * Implementation of ColumnManager.
 *
 *
 * @author jgeisler
 */
public abstract class ColumnManagerImplRoot extends GenericManagerImpl<Column, ColumnDAO> implements ColumnManager {

    /**
     * Constructor.
     * @param primaryDAO The prim DAO.
     */
    public ColumnManagerImplRoot(ColumnDAO primaryDAO) {
        super(primaryDAO);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 