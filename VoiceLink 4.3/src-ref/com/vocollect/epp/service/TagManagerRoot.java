/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;
import java.util.List;

import com.vocollect.epp.dao.TagDAO;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;


/**
 * Interface to provide access to Get, Save, Delete Tag Objects.  This is used
 * to manipulate Site data.
 *
 * @author mlashinsky
 */
public interface TagManagerRoot extends GenericManager<Tag, TagDAO> {

    /**
     * @param tagType the type of the tag.
     * @param tagId the ID of the tagged object.
     * @return the Tag of the specified type associated with the object.
     */
    public Tag findByTaggedObjectId(Long tagType, Long tagId);

    /**
     * @return the list of all site tags
     */
    public List<Tag> findSiteTags();

    public void saveTags(Taggable taggedObj, List<Tag> tags);
    public void saveTag(Taggable taggedObj, Tag t);
    public void addTag(Taggable taggedObj, Tag t);
    public void deleteTag(Taggable taggedObj, Tag t);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 