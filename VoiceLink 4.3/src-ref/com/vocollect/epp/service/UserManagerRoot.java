/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.UserChartPreference;
import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.User;

import java.util.Set;

import org.acegisecurity.userdetails.UserDetailsService;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for User-related operations.
 * <p>
 * This interface extends UserDetailsService in order
 * to support Acegi authentication.
 *
 * @author ddoubleday
 */
public interface UserManagerRoot extends
    GenericManager<User, UserDAO>, UserDetailsService, RemovableDataProvider {

    /**
     * Retrieves a user by name, or returns null is there is no user
     * by the name.
     *
     * @param username - the name of the user
     * @return User - the requested User, or null
     * @throws DataAccessException on any failure.
     */
    User findByName(String username) throws DataAccessException;

    /**
     * Retrieve the number of users who authenticate against the directory server.
     * @return int - the number of users who authenticate against the directory server
     * @throws DataAccessException on any failure
     */
    int countDirectoryServerAuthenticatingUsers() throws DataAccessException;
    
    /**
     * Method to delete the Chart Preferences based on Dashboard ID.
     * @param dashboardId Long.
     */
    void updateUserChartPreferencesByDashboard(Long dashboardId);
    

    /**
     * Method to delete the Default dashboard user Property based on 
     * name value pair in voc_user_properties table.
     * @param name String.
     * @param value Long.
     */
    void updateUserPropertyByDashboard(String name, Long value);
    
    /**
     * Method to delete the Chart Preferences based on Dashboard ID.
     * @param dashboardId the dashboard Id.
     * @param chartId the chart id.
     */
    void updateUserChartPreferencesByDashboardAndChart(Long dashboardId, Long chartId);
    
    /**
     * Get chart preference for the user, dashboard and chart.
     * @param user the user
     * @param chart the chart
     * @param dashboard the dashboard
     * @return The chart preference or NULL
     */
    UserChartPreference getChartPreferenceByUserChartAndDashboard(User user,
                                                                  Chart chart,
                                                                  Dashboard dashboard);
    
     /**
      * method to update user's chart preferences.
      * @param userId userId of current user
      * @param chart selected chart
      * @param dashboard selected dashboard
      * @param chartPreferencesJSON jsonObject of chartPreferences
      * @throws JSONException je
      * @throws DataAccessException dae
      * @throws BusinessRuleException bre
      */
    void updateUserChartPreferences(Long userId,
                                    Chart chart,
                                    Dashboard dashboard,
                                    JSONObject chartPreferencesJSON)
        throws JSONException, DataAccessException, BusinessRuleException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 