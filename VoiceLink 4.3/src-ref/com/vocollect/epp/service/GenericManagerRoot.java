/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.util.ResultDataInfo;

import java.util.List;


/**
 * Generic ServiceManager interface that handles generic operations that
 * apply to most model objects.
 * @param <T> the parameterized type to manage
 * @param <DAO> the parameterized DAO to assist in management.
 *
 * @author ddoubleday
 */
public interface GenericManagerRoot<T, DAO extends GenericDAO<T>> {

    /**
     * Retrieves a model by key.  An exception is thrown if no associated model
     * is found.
     *
     * @param id - the id of the model object
     * @return the requested model object
     * @throws DataAccessException on any failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity
     * could not be found.
     */
    T get(Long id) throws DataAccessException;

    /**
     * Retrieves a list of all models of the generic type.
     * @return List - a list of all of the models of the type
     * @throws DataAccessException on any failure
     */
    List<T> getAll() throws DataAccessException;

    /**
     * Retrieves a list of all models of the generic type, potentially limited
     * and ordered by the RDI. This method is called by DataProviders.
     * @param rdi selection and ordering data for the query
     * @return List - a list of all of the models of the type
     * @throws DataAccessException on any failure
     */
    List<? extends DataObject> getAll(ResultDataInfo rdi) throws DataAccessException;

    /**
     * Find all instances that match the example object.
     * @param exampleInstance the instance that matching will be done against.
     * @return the list of matching instances, or the empty list if there are
     * none
     * @throws DataAccessException on any error
     */
    List<T> queryByExample(T exampleInstance) throws DataAccessException;

    /**
     * Delete an object from the database by its ID.
     *
     * @param id - the ID of the object to remove
     * @throws BusinessRuleException if the instance could not be removed
     * because of a business rule.
     * @throws DataAccessException on any other failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity
     * could not be found.
     * @return whatever data an implementing class may wish to return. You
     * can ignore the return value in most cases.
     */
    Object delete(Long id) throws BusinessRuleException, DataAccessException;

    /**
     * Removes a model from the database.
     *
     * @param instance - the instance to delete
     * @throws BusinessRuleException if the instance could not be removed
     * because of a business rule.
     * @throws DataAccessException on any other failure
     * @return whatever data an implementing class may wish to return. You
     * can ignore the return value in most cases.
     */
    Object delete(T instance) throws BusinessRuleException, DataAccessException;

    /**
     * Saves a model object's information.
     *
     * @param instance - the BaseModel
     * @throws BusinessRuleException if the instance could not be removed
     * because of a business rule.
     * @throws DataAccessException on any other failure
     * @return whatever data an implementing class may wish to return. You
     * can ignore the return value in most cases.
    */
    Object save(T instance) throws BusinessRuleException, DataAccessException;

    /**
     * Saves a list of these objects. A bulk-save.
     *
     * @param instance - the BaseModel
     * @throws BusinessRuleException if the instance could not be removed
     * because of a business rule.
     * @throws DataAccessException on any other failure
     * @return whatever data an implementing class may wish to return. You
     * can ignore the return value in most cases.
    */
    Object save(List<T> instance) throws BusinessRuleException,
                                         DataAccessException;

    /**
     * Count the number of instances of type T.
     * @return the count.
     * @throws DataAccessException on any error
     */
    long getCount() throws DataAccessException;

    /**
     * @return the primary DAO associated with the manager. This is the one
     * that the class is instantiated with.
     */
    DAO getPrimaryDAO();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 