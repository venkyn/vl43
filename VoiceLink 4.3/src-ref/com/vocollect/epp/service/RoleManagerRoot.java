/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.RoleDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.FeatureGroup;
import com.vocollect.epp.model.Role;

import java.util.List;

/**
 * The service interface for working with Role and Feature objects.
 *
 * @author Dennis Doubleday
 */
public interface RoleManagerRoot
    extends GenericManager<Role, RoleDAO>, RemovableDataProvider {


    /**
     * Gets the feature by ID.
     * @param featureId the ID of the requested feature
     * @return the requested Feature
     * @throws DataAccessException on any failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity
     * could not be found.
     */
    Feature getFeature(Long featureId) throws DataAccessException;

    /**
     * Return all FeatureGroups.
     * @return a List of available feature groups
     * @throws DataAccessException occurs when the feature list cannot be
     *             accessed
     */
    List<FeatureGroup> getFeatureGroups() throws DataAccessException;

    /**
     * Gets the <code>Role</code> by name. Returns null if no such
     * <code>Role</code> is found.
     * @param rolename - the name of the requested Role
     * @return the requested Role, or null.
     * @throws DataAccessException - occurs when role cannot be accessed
     * for unexpected reasons.
     */
    Role findByName(String rolename) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 