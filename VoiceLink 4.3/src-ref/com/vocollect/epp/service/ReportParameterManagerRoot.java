/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.ReportParameterDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportParameter;

import java.util.List;

/**
 * Manager for the reporting parameters framework.
 *
 * @author mnichols
 */
public interface ReportParameterManagerRoot 
    extends GenericManager<ReportParameter, ReportParameterDAO>, DataProvider {
  
    /**
     * Retrieves all report type parameters that match the operator team id
     *
     * @param id the operator team id
     * @return the requested ReportTypes, or null
     * @throws DataAccessException on any failure.
     */
    List<ReportParameter> getTeamReferences(Long id) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 