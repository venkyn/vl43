/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.PluginModuleDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.plugin.PluginModule;

import java.util.List;

/**
 * Used to manage registered plugin PluginModule objects.
 * 
 *
 * @author ddoubleday
 */
public interface PluginModuleManager 
    extends GenericManager<PluginModule, PluginModuleDAO> {

    /**
     * Gets a registered plugin application by name.
     * @param name the application name.
     * @return the application object, or null if not found.
     * @throws DataAccessException on database failure.
     */
    PluginModule findByName(String name) throws DataAccessException;

    /**
     * @return the List of PluginModules that are enabled, ordered by
     * sequence number.
     */
    List<PluginModule> listEnabledModules();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 