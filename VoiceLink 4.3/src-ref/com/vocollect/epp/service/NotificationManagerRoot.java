/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.LabelObjectPair;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *  NotificationManager interface.
 *
 * @author Aich Debasis
 */
public interface NotificationManagerRoot extends
    GenericManager<Notification, NotificationDAO>, RemovableDataProvider {

    /**
     * Sends email to all the users registered for this site id passed as
     * argument who has not null email ids.
     * @param n the notification
     * @param id the site ID
     * @throws Exception on any failure to send.
     */
    public void sendEmailNotification(Notification n, Long id) throws Exception;

    /**
     * Return a map of site vs count of un-acknowledged Critical notifications.
     * Used by the web service.
     * @param user the user
     * @param np the priority
     * @return List of object arrays containing the Site name, Site ID,
     * and the count of notifications of the specified priority for
     * all the Sites visible to the specified User.
     * @throws DataAccessException on database failure.
     */
    public List<Object[]> getGroupedCountBySite(User user, NotificationPriority np)
        throws DataAccessException;

    /**
     * Returns list of email addresses for the users who has view notification role
     * for the site id passed. Used internally by sendEmailNotification().
     * @param id the site ID.
     * @return the array of email addresses.
     * @throws DataAccessException on database failure.
     */
    public List<String> getUserEmailsBySiteId(Long id)
        throws DataAccessException;

    /**
     * Returns a Map of notification field name to values for a given id.
     * Used by the web service.
     * @param notificationId the ID of the notification.
     * @return the map
     * @throws DataAccessException on database failure.
     */
    public Map<String, Object> getNotificationMap(Long notificationId)
        throws DataAccessException;

    /**
     * Update the notification with supplied user and date time.
     * ackDate must be in this format: mm/dd/yyyy.
     * TODO: This date format probably needs to be localized. --ddoubleday
     * @param id notification ID
     * @param ackUser user string
     * @param ackDate date string
     * @return a message string (TODO: This MUST be localized -- ddoubleday)
     * @throws DataAccessException on database failure.
     * @throws ParseException on failure to parse date format.
     * @throws BusinessRuleException on business rule failure.
     */
    public String updateAck(Long id, String ackUser, String ackDate)
        throws DataAccessException, ParseException, BusinessRuleException;

    /**
     * @param np the priority
     * @param siteId the site
     * @return the notification id
     * @throws DataAccessException on database failure.
     */
    public Long getMaxNotificationIdForSiteId(NotificationPriority np,
                                              Long siteId)
        throws DataAccessException;

    /**
     * Creates a new notification and sends email.
     * @param n the notification.
     * @throws Exception on any failure to create or send.
     */
    public void createNotificationAndEmail(Notification n) throws Exception;

    /**
     * Creates a new notification l.
     *
     * @param processKey .
     * @param messageKey .
     * @param priority .
     * @param timeOfAction .
     * @param errorNumber .
     * @param application .
     * @param argDetails .
     * @throws Exception on any failure to create or send.
     */
    public void createNotification(String processKey,
                                   String messageKey,
                                   NotificationPriority priority,
                                   Date timeOfAction,
                                   String errorNumber,
                                   String application,
                                   List<LabelObjectPair> argDetails) throws Exception;


    /**
     * @return a Notification object
     */
    public Notification systemNotification();

    /**
     * Gets all Notifications older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @return a list of Notifications
     * @throws DataAccessException Database failure
     */
    public List<Notification> listOlderThan(QueryDecorator decorator,
        Date date) throws DataAccessException;

    /**
     * Purges Notifications based on date and status.
     *
     * @param decorator - query decorator
     * @param olderThan - date to purge by
     * @return - number of assignments purged
     */
    public int executePurge(QueryDecorator decorator,
                            Date olderThan);

    /**
     * @param processName the NotificationPriority
     * @param terminalNumber terminal number for which notifications are to be
     *            fetched
     * @return count of acknowledged notifications matching process name and
     *         terminal number.
     * @throws DataAccessException on database failure.
     */
    public int getUnackedSystemNotificationsCount(String processName,
                                                  String terminalNumber)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 