/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;

/**
 * 
 *
 * @author dkertis
 */
public interface RemovableDataProviderRoot extends DataProvider {
    
    /**
     * Retrieves an object by key.  An exception is thrown if the object
     * is not found.
     *
     * @param id - the id of the object
     * @return DataObject - the requested object
     * @throws DataAccessException on any failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity 
     * could not be found.
     */
    DataObject getDataObject(Long id) throws DataAccessException;
    
    /**
     * Removes an object from the database by the ID.
     *
     * @param id - the ID of the object to remove
     * @throws DataAccessException on any failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity 
     * could not be found.
     * @throws BusinessRuleException on business rule violation.
     * @return whatever data an implementing class may wish to return.
     */
    Object delete(Long id) throws DataAccessException, BusinessRuleException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 