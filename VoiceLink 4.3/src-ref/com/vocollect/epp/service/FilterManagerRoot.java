/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.FilterDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.User;

import java.util.List;

import org.json.JSONException;

/**
 * Service interface for Filter.
 *
 *
 * @author jgeisler
 */
public interface FilterManagerRoot extends GenericManager<Filter, FilterDAO> {

    /**
     * Add a FilterCriterion to Filter.
     * @param criterion new criterion to be saved
     * @param user The User whose filter is being modified/
     * @param viewId The View id the filter is on.
     * @throws DataAccessException thrown.
     * @return long the id of the criterion just added.
     */
    long addFilterCriterion(FilterCriterion criterion, User user, long viewId)
        throws DataAccessException;

    /**
     * Overrides a users filters with the new params.
     * @param filters new list of filters.
     * @param user the user
     * @throws DataAccessException thrown.
     */
    void addFilters(List<Filter> filters, User user) throws DataAccessException;

    /**
     * Updates a specific filter criterion.
     * @param criterion The new criterion object.
     * @param critId The criterion's id.
     * @param user The user for which this criterion exists.
     * @param viewId The viewId of the view for this criterion.
     * @throws DataAccessException In case we have a problem getting the filter.
     */
    void updateFilterCriterion(FilterCriterion criterion, Long critId, User user, long viewId)
        throws DataAccessException;

    /**
     * Accept a list of serialized JSON messages of filter criterion and
     * instantiate a transient model graph of Filter / Filter Criterion / Column
     * and Operand in order to produce the necesary HQL method for filtering.
     * @param serializedJSONObjects the serialized JSON objects all on the same line
     * @return List<Filter/> the transient filter object(s)
     * @throws DataAccessException thrown.
     * @throws JSONException thrown.
     */
    List<Filter> constructFilterFromSerializedJSON(String serializedJSONObjects)
        throws JSONException, DataAccessException;

    /**
     * For a given user id and view id, find, if any, the filter that applies to that
     * pair of data.
     * @param userId long representing the user id
     * @param viewId long representing the view id
     * @return Filter - the filter object, if any, for the given user id / view id pair
     */
    Filter findByUserIdAndViewId(long userId, long viewId);

    /**
     * Creates a new filter criterion in memory, but does not persist the
     * new criterion to the user preferences.
     * @param viewId The ID of the view to be filtered.
     * @param columnId The ID of the column to be filtered.
     * @param operandId The ID of the operand to filter with.
     * @param val1 The value the filter must meet.
     * @param val2 A second value the filter must meet.
     * @param locked If the filter is a locked filter.
     * @return A new <code>FilterCriterion</code> object.
     * @throws DataAccessException On database error.
     */
    public FilterCriterion constructFilterCriterion(Long viewId,
                                                    Long columnId,
                                                    Long operandId,
                                                    String val1,
                                                    String val2,
                                                    boolean locked)
    throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 