/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.LDAPConfigurationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.LDAPConfiguration;

import java.util.List;

/**
 * 
 *
 * @author 
 */
public interface LDAPConfigurationManager extends GenericManager<LDAPConfiguration, LDAPConfigurationDAO> {
    /**
     * Removes LdapConfigurations identified by the given operator primary key
     * array.
     * @param ldapConfigurationIds Integer[] ldapConfiguration primary keys
     * @return array of value-objects containing the details of the ldapConfigurations 
     * which were just removed
     */
    public List<LDAPConfiguration> deleteLDAPConfigurations(Long[] ldapConfigurationIds)
            throws VocollectException, DataAccessException;
    
    /**
     * This method deletes the ldapConfiguration identified by the ldapConfiguration primary key.
     * @param ldapConfigurationId Integer ldapConfiguration primary key
     * @return ldapConfiguration vo representing the deleted ldapConfiguration, or an error VO
     * representing the failed attempt to delete.
     */
    public LDAPConfiguration deleteLDAPConfiguration(Long ldapConfigurationId) throws DataAccessException;
     
    /**
     * Retrieves value object for the specified LDAPConfiguration.
     * @param ldapConfigId Integer used to specify LDAPConfiguration
     * @throws EntityNotFoundException if the specified entity could
     * not be found
     * @return the LDAPConfigurationVO (with simple properties only)
     */
    public LDAPConfiguration retrieveLDAPConfiguration(Long ldapConfigId)
            throws EntityNotFoundException, DataAccessException;
    
    /**
     * Retrieves value objects for all LDAPConfigurations.
     * @throws DataAccessException
     * not be found
     * @return the VOResultSet of the current set of items
     */
    public List<LDAPConfiguration> retrieveAllLDAPConfigurations()
            throws DataAccessException;
    
    /**
     * Query the uniqueness of a given ldap configuration
     * @param ldapConfig .
     * @return the id of the matching ldap configuration or null if none is found.
     * @throws DataAccessException
     */
    public Long uniqueness(LDAPConfiguration ldapConfig) throws DataAccessException;
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 