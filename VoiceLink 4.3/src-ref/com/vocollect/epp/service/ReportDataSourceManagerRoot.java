/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.service;

import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;


/**
 * Manager for implementing data sources for complex reports that need more
 * than just a hibernate query.
 */
public interface ReportDataSourceManagerRoot {

    /**
     * returns a list of objects to use in report.
     *
     * @param values - parameter values collected
     * @return - list of objects to use in report
     * @throws Exception - Any Exception
     */
    public JRDataSource getDataSource(Map<String, Object> values)
    throws Exception;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 