/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DataProviderUtil;

import java.util.List;


/**
 * A service that provides data for Table Component views.
 *
 * @author ddoubleday
 */
public interface TableViewDataServiceRoot {

    /**
     * @param providerUtil the data provider utility.
     * @param rdi Information about the request.
     * @param dataClass Spring bean to call method on
     * @param getMethod Method to call through reflection
     * @param currentUser User making the request
     * @param columns Columns to render
     * @param currentTime timestamp of the request
     * @param action Request DataProviderAction class
     * @return The JSON response in String format
     */
    public String getTableData(DataProviderUtil providerUtil,
                               ResultDataInfo rdi,
                               Object dataClass,
                               String getMethod,
                               User currentUser,
                               List<Column> columns,
                               String currentTime,
                               DataProviderAction action);
 
    /**
     * @param dataClass Spring bean to call method on
     * @param getMethod Method to call through reflection
     * @param sortField the field to sort on
     * @param sortAscending the sort direction
     * @param filter the Filter to apply
     * @return the list of DataObject returned by the query.
     */
    List<DataObject> checkSelectionData(Object dataClass,
                                        String getMethod,
                                        String sortField,
                                        boolean sortAscending,
                                        Filter filter);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 