/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.DataTranslationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.DataTranslation;

import java.util.Locale;

/**
 * The service interface for working with DataTranslation objects.
 *
 * @author ddoubleday
 */
public interface DataTranslationManagerRoot
    extends GenericManager<DataTranslation, DataTranslationDAO>, RemovableDataProvider {


    /**
     * Translate a data key for the current (LocaleContextHolder) Locale.
     * @param key the value to translate
     * @return the translation, or the original if nothing matched.
     * @throws DataAccessException on database failure
     */
    public String translate(String key) throws DataAccessException;

    /**
     * Translate a data key for the specified Locale. Most clients will want
     * to use the other overloaded version of this method, which uses the
     * current Locale context, but this allows for alternate contexts.
     * @param key the value to translate
     * @param locale the Locale to translate to
     * @return the translation, or the original if nothing matched.
     * @throws DataAccessException on database failure
     */
    public String translate(String key, Locale locale) throws DataAccessException;

    /**
     * Get the translation for a data key for the specified Locale. Most clients
     * will want to use one of the <code>translate()</code> methods, but this
     * lower-level call is useful if you want to get <code>null</code> back in
     * the case where no translation exists.
     * @param key the value to translate
     * @param locale the Locale to translate to
     * @return the translation, or null if nothing matched.
     * @throws DataAccessException on database failure.
     */
    DataTranslation findTranslation(String key, String locale) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 