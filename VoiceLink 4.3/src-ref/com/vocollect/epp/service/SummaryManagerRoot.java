/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.SummaryDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Summary;

import java.util.List;

/**
 * SummaryManager interface.
 *
 * @author svoruganti
 */
public interface SummaryManagerRoot  extends
GenericManager <Summary, SummaryDAO>, RemovableDataProvider {


    /**
     * Returns a List of Summary objects .
     * @return List
     *
     * @throws DataAccessException if unable to retrieve summaries
     */
    public List<Summary> getAllSummaries() throws DataAccessException;

    /**
     * To check if a user has permission to given summary.
     * @return boolean
     *
     * @param url - Url for a summary
     */
    public boolean checkUserToSummaryPermission(String  url);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 