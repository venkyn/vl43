/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.NetworkCredentialDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.eap.TrustedCertificateInfo;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.NetworkCredential;
import com.vocollect.epp.util.DataFileReferenceUtil;

import java.util.List;

/**
 * 
 * @author brupert
 */
public interface NetworkCredentialManager extends GenericManager<NetworkCredential, NetworkCredentialDAO> {
    
    /**
     * Generate an MD5 of the network credential and store it to the MD5 field 
     * of the object.
     * 
     * @param netCred .
     * @param isCertCred .
     */
    public void generateMd5(NetworkCredential netCred, boolean isCertCred);
    
    /**
     * Encrypt network credentials without a pin
     * @param netCred network credential to encrypt.
     * @param isCertCred whether or not it's a certificate credential.
     * @param em the encryption manager to use.
     */
    public void encrypt(NetworkCredential netCred, boolean isCertCred, EncryptionManager em);
    
    /**
     * Encrypt network credentials without a pin
     * @param netCred network credential to encrypt.
     * @param isCertCred whether or not it's a certificate credential.
     * @param em the encryption manager to use.
     * @param pin the pin to encrypt with.
     */
    public void encrypt(NetworkCredential netCred, boolean isCertCred, EncryptionManager em, String pin);
    
    /**
     * Creates a network credential based upon the properties passed to it in
     * the site value object.
     * @param  dataVO containing the credential's properties
     */
    public void createTrustCertificate(DataFileReferenceUtil dataVO) throws VocollectException, DataAccessException;
    
    /**
     * Retrieve all certificates from the database that are marked as 
     * trustCertificates.
     * @return A List of NetworkCredential objects
     */
    public List<NetworkCredential> getAllTrustCertificates();

    /**
     * @return .
     */
    public List<TrustedCertificateInfo> listTrust();
    
    /**
     * Determine whether a certificate is already being trusted by VoiceConsole. 
     * @param dataVO The DataFileReferenceVO that points to the certificate to 
     * add to VoiceConsole's trust list
     * @return Whether the dataVO is already trusted
     * create a TrustStore from the DataFileReferenceVO.
     */
    public boolean isAlreadyTrusted(DataFileReferenceUtil dataVO) throws VocollectException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 