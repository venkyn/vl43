/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.eap.CertificateSocketFactory;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.LDAPConfiguration;
import com.vocollect.epp.model.NetworkCredential;
import com.vocollect.epp.util.TestLDAPConnectionThread;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Hashtable;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.ssl.KeyStoreBuilder;
import org.apache.commons.ssl.ProbablyBadPasswordException;

/**
 *
 *
 * @author ???
 */
public class LDAPManagerRoot {

    private static final Logger log = new Logger(LDAPManagerRoot.class);
    private NetworkCredentialManager networkCredentialManager;

    private static final Long WAIT_TIME = new Long(100);
    private static final int TIMEOUT = 5 * 1000;

    /**
     * LDAP status codes.
     */
    public enum TestLDAPResult { SUCCESS, USER_NOT_FOUND, CONNECTION_FAILED }

    /**
     * Changes an objects credentials from oldCredentials to newCredentials.
     * @param ldapConfig .
     * @param oldCredentials The older credentials that will be used to login to the directory service
     * @param newCredentials The new credentials to change to
     * @throws NamingException
     * @throws VocollectException either EntityNotFoundException when VoiceConsole
     * is unable to loacte the specified user in the Directory Service, or
     * FalconInvalidParameterException when the ldapConfig that is passed in is invalid.
     */
    public void changeCredentials(LDAPConfiguration ldapConfig,
                                  NetworkCredential oldCredentials,
                                  NetworkCredential newCredentials)
        throws VocollectException {
        if (!oldCredentials.isCertificateCredentials()) {
            changePasswordCredentials(ldapConfig, oldCredentials, newCredentials);
        } else {
            changeCertificateCredentials(ldapConfig, oldCredentials, newCredentials);
        }
    }

    /**
     * @param ldapConfig .
     * @param oldCredentials .
     * @param newCredentials .
     * @throws VocollectException .
     */
    private void changeCertificateCredentials(
            LDAPConfiguration ldapConfig,
            NetworkCredential oldCredentials,
            NetworkCredential newCredentials)
        throws VocollectException {
        try {
            DirContext ctx = certificateSignOn(ldapConfig, oldCredentials);
            String distinguishedName = findUserCertificate(ldapConfig, ctx, oldCredentials);

            ModificationItem[] mods = new ModificationItem[1];
            mods[0] = new ModificationItem(
                    DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(ldapConfig.getPasswordAttribute(),
                                       newCredentials.getCertificateFileContents())
            );

            // Change the attributes of the user
            ctx.modifyAttributes(distinguishedName, mods);
        } catch (NamingException e) {
            log.error(oldCredentials.getUserName() + " not found.", SystemErrorCode.LDAP_EXCEPTION, e);
            throw new VocollectException(oldCredentials.getUserName() + " not found.");
        } catch (Exception e) {
            log.error("Problems with a user certificate", SystemErrorCode.LDAP_EXCEPTION, e);
            throw new VocollectException("Problems with a user certificate");
        }
    }

    /**
     * @param ldapConfig .
     * @param oldCredentials .
     * @param newCredentials .
     * @throws VocollectException .
     */
    private void changePasswordCredentials(
            LDAPConfiguration ldapConfig,
            NetworkCredential oldCredentials,
            NetworkCredential newCredentials)
        throws VocollectException {
        // Create the initial context of the search user so that we can find
        // the object we're looking for.
        DirContext currentContext;
        try {
            currentContext = signOn(ldapConfig, ldapConfig.getSearchUsername(), ldapConfig.getSearchPassword());
        } catch (NamingException e) {
            log.error(ldapConfig, SystemErrorCode.LDAP_EXCEPTION, e);
            throw new VocollectException(e + ": " + ldapConfig);
        }

        String distinguishedName;
        try {
            // Find the user
            distinguishedName = findUser(ldapConfig.getSearchableAttribute(),
                                         ldapConfig.getSearchBase(),
                                         currentContext,
                                         oldCredentials.getUserName());

            // Login as the user
            currentContext = signOn(ldapConfig, distinguishedName, oldCredentials.getPassword());

            ModificationItem[] mods = new ModificationItem[1];
            mods[0] = new ModificationItem(DirContext.REPLACE_ATTRIBUTE,
                    new BasicAttribute(ldapConfig.getPasswordAttribute(),
                                       newCredentials.getPassword()));

            // Change the attributes of the user
            currentContext.modifyAttributes(distinguishedName, mods);
        } catch (NamingException e) {
            log.error(oldCredentials.getUserName() + " not found.", SystemErrorCode.LDAP_EXCEPTION, e);
            throw new VocollectException(oldCredentials.getUserName() + " not found.");
        }
    }

    /**
     * Tests whether the credentials are valid in the LDAP server.
     * @param ldapConfig The LDAP configuration to use
     * @param testMe the Credentials to test
     * @return true if the credentials are valid.
     * @throws VocollectException either EntityNotFoundException when VoiceConsole
     * is unable to loacte the specified user in the Directory Service, or
     * FalconInvalidParameterException when the ldapConfig that is passed in is invalid.
     */
    public boolean testCredentials(LDAPConfiguration ldapConfig,
                                   NetworkCredential testMe)
        throws VocollectException  {
        if (!testMe.isCertificateCredentials()) {
            return testPasswordCredentials(ldapConfig, testMe);
        }
        return testCertificateCredentials(ldapConfig, testMe);
    }

    /**
     * @param ldapConfig .
     * @param testMe .
     * @return .
     */
    private boolean testCertificateCredentials(LDAPConfiguration ldapConfig,
                                               NetworkCredential testMe) {
        try {
            DirContext ctx = certificateSignOn(ldapConfig, testMe);

            String distinguishedName = findUserCertificate(ldapConfig, ctx, testMe);

            if (!verifyUserName(ctx, ldapConfig, distinguishedName, testMe.getUserName())) {
                ctx.close();
                return false;
            }

            nullAction(distinguishedName, ctx);

            ctx.close();

            return true;
        } catch (Exception e) {
            log.warn(e);
        }

        return false;
    }

    /**
     * @param ctx .
     * @param ldapConfig .
     * @param distinguishedName .
     * @param userName .
     * @return .
     * @throws NamingException
     */
    private boolean verifyUserName(DirContext ctx,
                                   LDAPConfiguration ldapConfig,
                                   String distinguishedName,
                                   String userName)
        throws NamingException {
        Attributes orig = ctx.getAttributes(distinguishedName, new String[]{ldapConfig.getSearchableAttribute()});

        String ldapUserName = (String) orig.get(ldapConfig.getSearchableAttribute()).get();

        return (new EqualsBuilder().append(userName, ldapUserName)).isEquals();
    }

    /**
     * @param distinguishedName .
     * @param ctx .
     * @throws NamingException .
     */
    private void nullAction(String distinguishedName,
                                   DirContext ctx)
        throws NamingException {

        Attributes orig = ctx.getAttributes(distinguishedName, new String[]{"mail"});

        ctx.modifyAttributes(distinguishedName, DirContext.REPLACE_ATTRIBUTE, orig);

    }

    /**
     * @param ldapConfig .
     * @param ctx .
     * @param credentials .
     * @return .
     * @throws InvalidKeyException
     * @throws CertificateException
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws ProbablyBadPasswordException
     * @throws IOException
     * @throws NamingException
     */
    private String findUserCertificate(
            LDAPConfiguration ldapConfig,
            DirContext ctx,
            NetworkCredential credentials)
        throws InvalidKeyException,
               CertificateException,
               KeyStoreException,
               NoSuchAlgorithmException,
               NoSuchProviderException,
               ProbablyBadPasswordException,
               IOException,
               NamingException {
        String password = credentials.getCertificatePassword();

        KeyStore tempKeyStore = KeyStoreBuilder.build(
                credentials.getCertificateFileContents(),
                credentials.getCertificateKeyFileContents(),
                password.toCharArray()
        );

        String alias = tempKeyStore.aliases().nextElement();
        X509Certificate userCert = (X509Certificate) tempKeyStore.getCertificate(alias);

        String distinguishedName = userCert.getSubjectDN().getName();
        int equalsIndex = distinguishedName.indexOf("=");
        String searchableAttribute = distinguishedName.substring(0, equalsIndex);
        int commaIndex = distinguishedName.indexOf(",");
        String searchValue = distinguishedName.substring(equalsIndex + 1, commaIndex);

        distinguishedName = findUser(searchableAttribute,
                                     ldapConfig.getSearchBase(),
                                     ctx,
                                     searchValue);
        return distinguishedName;
    }

    /**
     * @param ldapConfig .
     * @param testMe .
     * @return .
     * @throws VocollectException
     */
    private boolean testPasswordCredentials(LDAPConfiguration ldapConfig,
                                            NetworkCredential testMe)
        throws VocollectException {
        DirContext currentContext;
        try {
            currentContext = signOn(ldapConfig, ldapConfig.getSearchUsername(), ldapConfig.getSearchPassword());
        } catch (NamingException e) {
            log.error(ldapConfig, SystemErrorCode.LDAP_EXCEPTION, e);
            throw new VocollectException(e + ": " + ldapConfig);
        }

        String distinguishedName;
        try {
            distinguishedName = findUser(ldapConfig.getSearchableAttribute(),
                                         ldapConfig.getSearchBase(),
                                         currentContext,
                                         testMe.getUserName());

            DirContext userContext = signOn(ldapConfig, distinguishedName, testMe.getPassword());

            nullAction(distinguishedName, userContext);

            userContext.close();

            currentContext.close();

            return true;
        } catch (NamingException e) {
            log.warn(e);
            return false;
        }
    }

    /**
     * Tests a username against the LDAP server.
     * @param ldapConfig ldap configuration information
     * @param testUser username to login
     * @return TestLDAPResult
     */
    public TestLDAPResult testLdapConfiguration(LDAPConfiguration ldapConfig, String testUser)
    {
        TestLDAPResult result = TestLDAPResult.CONNECTION_FAILED;
        try {
            DirContext context = signOn(ldapConfig, ldapConfig.getSearchUsername(), ldapConfig.getSearchPassword());
            if (testUser != null && testUser.length() > 0) {
                result = TestLDAPResult.USER_NOT_FOUND;
                findUser(ldapConfig.getSearchableAttribute(), ldapConfig.getSearchBase(), context, testUser);
            }
            result = TestLDAPResult.SUCCESS;
        } catch (NamingException e) {
            log.error(ldapConfig, SystemErrorCode.LDAP_EXCEPTION, e);
        }

        return result;
    }

    /**
     * Tests a username and password against the LDAP server.
     * @param ldapConfig ldap configuration information
     * @param testUser username to login
     * @param password password to login
     * @return TestLDAPResult
     */
    public TestLDAPResult testLdapConfiguration(LDAPConfiguration ldapConfig, String testUser, String password)
    {
        TestLDAPResult result = TestLDAPResult.CONNECTION_FAILED;
        try {
            DirContext context = signOn(ldapConfig, ldapConfig.getSearchUsername(), ldapConfig.getSearchPassword());
            if (testUser != null && testUser.length() > 0) {
                result = TestLDAPResult.USER_NOT_FOUND;
                findUser(ldapConfig.getSearchableAttribute(), ldapConfig.getSearchBase(), context, testUser);
            }
            result = TestLDAPResult.SUCCESS;
        } catch (NamingException e) {
            log.error(ldapConfig, SystemErrorCode.LDAP_EXCEPTION, e);
        }

        return result;
    }

    /**
     * @param ldapConfig .
     * @param distinguishedName .
     * @param password .
     * @return .
     * @throws NamingException
     */
    private DirContext signOn(LDAPConfiguration ldapConfig,
                              String distinguishedName,
                              String password)
        throws NamingException {
        Hashtable<String, String> env = new Hashtable<String, String>();

        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapConfig.getProviderUrl());
        env.put(Context.SECURITY_PRINCIPAL, distinguishedName);
        env.put(Context.SECURITY_CREDENTIALS, password);
        if (ldapConfig.getUseSSL().booleanValue()) {
            env.put(Context.SECURITY_PROTOCOL, "ssl");
            env.put("java.naming.ldap.factory.socket",
                "com.vocollect.epp.eap.CertificateSocketFactory");
        }

        return socketFactorySignOn(env, null);
    }

    /**
     * @param ldapConfig .
     * @param credentials .
     * @return .
     * @throws NamingException
     */
    private DirContext certificateSignOn(LDAPConfiguration ldapConfig,
                                         NetworkCredential credentials)
        throws NamingException {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, ldapConfig.getProviderUrl());
        env.put(Context.SECURITY_AUTHENTICATION, "EXTERNAL");
        // EXTERNAL must be through a SSL connection
        // (since this is how it authenticates)
        env.put(Context.SECURITY_PROTOCOL, "ssl");
        env.put("java.naming.ldap.factory.socket", "com.vocollect.epp.eap.CertificateSocketFactory");

        return socketFactorySignOn(env, credentials);
    }

    /**
     * @param env .
     * @param credentials .
     * @return .
     * @throws NamingException
     */
    private DirContext socketFactorySignOn(Hashtable<String, String> env,
                                           NetworkCredential credentials) throws NamingException {
        List<NetworkCredential> trustMaterial = networkCredentialManager.getAllTrustCertificates();

        CertificateSocketFactory.acquire();
        CertificateSocketFactory.setInfo(credentials, trustMaterial);

        // We're using a seperate thread to test the connection because in the
        // case that the connection information is for an ldap server using ssl
        // but the security protocol isn't being used (because the user forgot to
        // check off the ssl box) the new InitialDirContext() call hangs forever.
        // This prevents the mutex lock from being released which prevents any
        // more connections from being made until the tomcat service is restarted.
        TestLDAPConnectionThread testConnThread = new TestLDAPConnectionThread(env);
        Thread attemptConnection = new Thread(testConnThread);
        attemptConnection.start();

        int totalTries = TIMEOUT / WAIT_TIME.intValue();
        boolean response = false;
        while (!response) {
            if (testConnThread.getCtx() != null) {
                response = true;
            } else if (testConnThread.getNamingException() != null) {
                throw testConnThread.getNamingException();
            } else if (testConnThread.getThrowable() != null) {
                throw new RuntimeException(testConnThread.getThrowable());
            } else if (totalTries == 0) {
                try {
                    attemptConnection.join(10);
                } catch (InterruptedException e) {
                    // Oh well, we tried.
                }
                CertificateSocketFactory.release();
                throw new NamingException("We waited a while and never got a connection.");
            } else {
                totalTries--;
                try {
                    Thread.sleep(WAIT_TIME);
                } catch (InterruptedException e) {
                    // Oh well we'll still check again, just sooner
                    // than expected.
                }
            }
        }

        return testConnThread.getCtx();
    }

    /**
     * @param searchAttr .
     * @param searchBase .
     * @param ctx .
     * @param searchValue .
     * @return .
     * @throws NamingException
     */
    private String findUser(String searchAttr,
                                   String searchBase,
                                   DirContext ctx,
                                   String searchValue)
         throws NamingException {
        // Specify the attributes to match
        // We want to match the searchable attribute to the username of the object.
        String filter = "(" + searchAttr + "=" + searchValue + ")";

        // Specify that we want to search all subtrees
        SearchControls ctls = new SearchControls();
        ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        // Search for objects under search base that have those matching attributes
        NamingEnumeration<SearchResult> answer = ctx.search(searchBase, filter, ctls);

        String distinguishedName = null;
        while (answer.hasMore()) {
            SearchResult sr = answer.next();

            if (distinguishedName != null) {
                log.warn("Too many users found in the directory service when looking for " + searchValue);
            }

            distinguishedName = sr.getName() + "," + searchBase;
        }

        if (distinguishedName == null) {
            log.warn("User " + searchValue + " not found.");
            throw new NamingException("User not found.");
        }
        return distinguishedName;
    }

    /**
     * @return networkCredentialManager
     */
    public NetworkCredentialManager getNetworkCredentialManager() {
        return networkCredentialManager;
    }

    /**
     * @param networkCredentialManager .
     */
    public void setNetworkCredentialManager(NetworkCredentialManager networkCredentialManager) {
        this.networkCredentialManager = networkCredentialManager;
    }

}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 