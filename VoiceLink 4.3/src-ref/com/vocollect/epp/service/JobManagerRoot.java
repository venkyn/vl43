/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.JobDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.JobHistory;

import java.util.Date;
import java.util.List;

/**
 * Business Service Interface to handle communication between web and business
 * logic layer for Job-related operations.
 *
 * @author jkercher
 */
public interface JobManagerRoot
    extends GenericManager<Job, JobDAO>, RemovableDataProvider  {

    /**
     * Gets the jobs from the scheduler. The scheduler should be kept
     * synchronized with the persisted values.
     * @return the scheduled jobs
     * @throws DataAccessException if the jobs cannot be retrieved
     */
    List<Job> updateAndGetAllJobs() throws DataAccessException;

    /**
     * Saves this job and updates the scheduler with the new trigger
     * information.
     * @param job the job to be saved
     * @throws DataAccessException if the job cannot be saved
     */
    void saveJob(Job job) throws DataAccessException;

    /**
     * Saves the job history.
     * @param history the history
     * @throws DataAccessException if the history cannot be saved
     */
    void saveJobHistory(JobHistory history) throws DataAccessException;

    /**
     * Starts the job with the corresponding id. A new trigger will be created
     * for the job to be started immediately, however, this trigger will not be
     * persisted in the Job.
     * @param id the <code>Job</code> id
     * @throws DataAccessException on any failure, and specifically
     *             <code>EntityNotFoundException</code> when the specified
     *             entity could not be found.
     */
    void executeStartJob(long id) throws DataAccessException;

    /**
     * Stops the job with the corresponding id. A new trigger will be created
     * for the job to be stopped immediately, however, this trigger will not be
     * persisted in the Job.
     * @param id the <code>Job</code> id
     * @throws DataAccessException on any failure, and specifically
     *             <code>EntityNotFoundException</code> when the specified
     *             entity could not be found.
     */
    void executeStopJob(long id) throws DataAccessException;

    /**
     * Retrieves the job with this id.
     * @param id the system id
     * @return the job
     * @throws DataAccessException on any failure, and specifically
     *             <code>EntityNotFoundException</code> when the specified
     *             entity could not be found.
     */
    Job updateAndGetJob(long id) throws DataAccessException;

    /**
     * Shows that this job is still alive (for clustered environments).
     * @param name the job name
     * @throws DataAccessException on exception
     */
    void executePing(String name) throws DataAccessException;

    /**
     * Gets the last ping time in milliseconds.
     * @param name the job name
     * @return the time in ms
     * @throws DataAccessException on exception
     */
    long executePoll(String name) throws DataAccessException;

    /**
     * Purges job history based on date and status.
     *
     * @param decorator - query decorator
     * @param olderThan - date to purge by
     * @return - number of assignments purged
     */
    int executePurge(QueryDecorator decorator,
                            Date olderThan);

    /**
     * Retrieves the job by name, and potentially updates it with
     * detail information from Quartz.
     * @param name the job name
     * @return the job
     * @throws DataAccessException on exception
     */
    Job updateAndFindJobByName(String name) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 