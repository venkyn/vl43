/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


/**
 * Exception to cover various errors that should never be encountered while
 * writing to (and closing) a zip file. This is thrown by the LogManager to the
 * LogFilesAction class, after the LogManager has recorded the error in the log.
 * The user gets a generalized error message.
 * @author Duff Gold
 */

package com.vocollect.epp.service;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;

/**
 * @author dgold
 *
 */
public class ZipFileException extends VocollectException {

    //
    private static final long serialVersionUID = 3994313262626177321L;

    /**
     * Constructs an empty instance of <code>ZipFileException</code>.
     */
    public ZipFileException() {
        super();
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     * @param t the exception that caused this exception.
     */
    public ZipFileException(ErrorCode code, Throwable t) {
        super(code, t);
    }

    /**
     * Constructs an instance of <code>ZipFileException</code> from a message
     * and a message key to be used for looking up an internationalized message
     * related to the specific issue with this exception. Use this constructor
     * when you want to add your own message, and specify a message that may be
     * useful for displaying to the user, and include the wrapped exception.
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     * @param t the wrapped exception
     */
    public ZipFileException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 