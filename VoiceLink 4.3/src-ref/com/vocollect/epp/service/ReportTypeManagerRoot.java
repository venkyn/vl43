/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.ReportTypeDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportType;


import java.util.List;


/**
 * Manager for the reporting framework.  This will give us access to
 * the report DAO and some related objects when querying report
 * parameters and running the report.
 *
 * @author mnichols
 */
public interface ReportTypeManagerRoot 
    extends GenericManager<ReportType, ReportTypeDAO>, DataProvider {
    
    /**
     * Retrieves all report types that match the application substring, 
     * or returns null if there are none.
     *
     * @param appType the application type
     * @return the requested ReportTypes, or null
     * @throws DataAccessException on any failure.
     */
    List<ReportType> getAllByAppType(String appType) throws DataAccessException;    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 