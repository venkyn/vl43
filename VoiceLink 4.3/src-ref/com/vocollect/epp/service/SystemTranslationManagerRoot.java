/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.SystemTranslationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemTranslation;

import java.util.List;
import java.util.Locale;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for SystemTranslation-related operations.
 * @author ddoubleday
 */
public interface SystemTranslationManagerRoot 
    extends GenericManager<SystemTranslation, SystemTranslationDAO> {

    /**
     * Returns a list of all support locales, sorted by the displayName
     * in the current Locale context.
     * 
     * @throws DataAccessException - Database exceptions 
     * @return - list of locales system supports
     */
    public List<Locale> getAllSupportedLocales() throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 