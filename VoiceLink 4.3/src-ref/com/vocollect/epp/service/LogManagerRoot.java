/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.model.FileView;

import java.util.List;

/**
 * Provides services supported by logging.
 * @author dgold
 */
public interface LogManagerRoot extends DataProvider {

    /**
     * Open the selected file, grab the contents and put them in a List of
     * Strings.
     * @param fileName The name of the file to read
     * @return List of strings containing file contents
     * @see com.vocollect.epp.service.LogManager
     * @throws UnreadableFileException - occurs when the file can't be read
     * @throws OtherFileException - occurs for any other file exception
     */
    List<String> getFileLines(String fileName) throws UnreadableFileException,
        OtherFileException;

    /**
     * Gets the list of files from the directory name passed in.
     * @param directory - name of the directory to look in for the log files
     * @return List of FileView objects representing the pertinent info for each
     *         file in the directory
     * @see com.vocollect.epp.service.LogManager
     * @see com.vocollect.epp.model.FileView
     * @throws UnreadableFileException if file does not exist or encounters IO error
     */
    List<FileView> getLogFiles(String directory) throws UnreadableFileException;

    /**
     * Get the number of lines read so far from the file.
     * @return the number of lines read from the file
     */
    int getFileLineNum();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 