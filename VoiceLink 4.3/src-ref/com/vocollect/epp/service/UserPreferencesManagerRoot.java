/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.ui.ColumnDTO;

import java.util.List;

/**
 * Business Service Interface to handle the saving and retreiving of User
 * Preferences.
 * <p>
 *
 * @author dkertis
 */
public interface UserPreferencesManagerRoot {

    /**
     * Get the time windows for the view/user.
     * @param view the view
     * @param user the user
     * @return the time window ID for this view/user combo
     * @throws DataAccessException on database failure
     */
    Long getTimeWindow(View view, User user) throws DataAccessException;

    /**
     * Save a time window preference for a view/user combo.
     * @param viewId the ID of the view
     * @param user the user
     * @param newValue the time window value.
     * @throws DataAccessException on database failure
     */
    void saveTimeWindow(Long viewId, User user, Long newValue)
        throws DataAccessException;

    /**
     * Attains the view object.
     * @param id of the view
     * @return View object
     * @throws DataAccessException in case a DB issue should occur.
     */
    View getView(Long id) throws DataAccessException;

    /**
     * Attains the columns for a particular user and view.
     * @param view Describes what table
     * @param user Describes what user
     * @return List of columns
     * @throws DataAccessException in case a DB issue should occur.
     */
    List<Column> getColumns(View view, User user) throws DataAccessException;


    /**
     * Attains the visible columns for a particular user and view.
     * @param view Describes what table
     * @param user Describes what user
     * @return List of columns
     * @throws DataAccessException in case a DB issue should occur.
     */
    List<Column> getVisibleColumns(View view, User user) throws DataAccessException;

    /**
     * Saves the user's preferences.
     * @param columns DTO's with the column data
     * @param user which user to save for
     * @throws DataAccessException in case a DB issue should occur.
     */
    void savePreferences(List<ColumnDTO> columns, User user)
        throws DataAccessException;

    /**
     * Restores the user's default preferences.
     * @param user which user to restore
     * @param viewId which view to restore
     * @throws DataAccessException in case a DB issue should occur.
     */
    void restoreDefaultColumns(User user, Long viewId)
        throws DataAccessException;

    /**
     * Searches for UserColumns.  If any exist, restore user default columns
     * preferences is enabled.
     * @param user User Object
     * @param view Screen view
     * @return true if enabled, false otherwise.
     * @throws DataAccessException in case a DB issue should occur.
     */
    public boolean isRestoreDefaultColumnsEnabled(User user, View view)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 