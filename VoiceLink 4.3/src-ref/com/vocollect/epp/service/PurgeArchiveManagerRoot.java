/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.PurgeArchiveException;
import com.vocollect.epp.scheduling.PurgeResult;

/**
 * Business Service Interface to handle communication between web and
 * persistence layer for Pick Archive-related operations.
 *
 * @author mlashinsky
 */
public interface PurgeArchiveManagerRoot {

    /**
     * Main Purge Archive function.
     * @return The result of the purge.
     * @throws PurgeArchiveException
     * @throws DataAccessException 
     */
    public PurgeResult purgeArchive() throws PurgeArchiveException, DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 