/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.dao;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dataaggregator.model.DAColumn;

import java.util.List;

/**
 * @author mraj
 * 
 */
public interface DashboardDetailDAORoot extends GenericDAO<DashboardDetail> {

    /**
     * . Method to find the linked drill down field
     * @param dashboard current dashboard
     * @param parentChart parent chart selected
     * @param drillDownChart child chart to draw
     * @return linked drill down field
     */
    public DAColumn findLinkedDrillDownField(Dashboard dashboard,
                                             Chart parentChart,
                                             Chart drillDownChart);
    
    /**.
     * Method to find list of detail records having references to chart
     * @param dashboard current dashboard
     * @param chart chart to be selected
     * @return list of dashboard details having reference to charts
     */
    public List<DashboardDetail> listDependentDetails(Dashboard dashboard,
                                                      Chart chart);
    
    /**.
     * Method to find if the given chart and dashboard combination exist
     * @param dashboard - the dashboard
     * @param chart - the chart
     * @return number of combinations
     */
    public Number countDashboardChartCombination(Dashboard dashboard,
                                                      Chart chart);
    
    /**
     * Method to return list of detail objects where chart object is either parent of drill-down.
     * @param chart the chart object to be searched
     * @return list of detail objects
     */
    public List<DashboardDetail> listDetailReferences(Chart chart);
    
    /**.
     * method to get list of detail ids by linked field
     * @param linkedDrillDownField linked field
     * @return list of dashboard detail ids
     */
    public List<Long> listDetailIdsByLinkedField(DAColumn linkedDrillDownField);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 