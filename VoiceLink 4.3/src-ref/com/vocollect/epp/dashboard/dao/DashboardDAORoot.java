/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.dashboard.dao;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.exceptions.BusinessRuleException;

import java.util.List;

/**
 * @author smittal
 *
 */
public interface DashboardDAORoot extends GenericDAO<Dashboard> {

    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the Dashboard to look for
     * @throws DataAccessException - indicates database error
     * @return alert id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;
    
    /**
     * 
     * @return list of site specific dashboards
     */
    public List<Dashboard> listDashboards();
    
    /**
     * Method to get most recently created dashboard in the site. 
     * @return recently created dashboard
     * @throws BusinessRuleException - bre
     * @throws DataAccessException - dae
     */
    public List<Dashboard> listRecentCreatedDashboard() throws BusinessRuleException,
    DataAccessException;    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 