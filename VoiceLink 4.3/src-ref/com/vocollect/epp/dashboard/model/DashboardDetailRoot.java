/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.model;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.model.VersionedModelObject;

/**
 * 
 * @author mraj
 *
 */
public class DashboardDetailRoot extends VersionedModelObject {

    /**
     * 
     */
    private static final long serialVersionUID = -687327777921611202L;

    private Dashboard dashboard;
    
    private Chart parentChart;
    
    private Chart drillDownChart;
    
    private DAColumn linkedDrillDownField;
    
    /**
     * @return the dashboard
     */
    public Dashboard getDashboard() {
        return dashboard;
    }


    /**
     * @return the parentChart
     */
    public Chart getParentChart() {
        return parentChart;
    }


    /**
     * @param parentChart the parentChart to set
     */
    public void setParentChart(Chart parentChart) {
        this.parentChart = parentChart;
    }


    /**
     * @param dashboard the dashboard to set
     */
    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    /**
     * @return the drillDownChart
     */
    public Chart getDrillDownChart() {
        return drillDownChart;
    }
    
    
    /**
     * @param drillDownChart the drillDownChart to set
     */
    public void setDrillDownChart(Chart drillDownChart) {
        this.drillDownChart = drillDownChart;
    }
    
    
    /**
     * @return the linkedDrillDownField
     */
    public DAColumn getLinkedDrillDownField() {
        return linkedDrillDownField;
    }

    
    /**
     * @param linkedDrillDownField the linkedDrillDownField to set
     */
    public void setLinkedDrillDownField(DAColumn linkedDrillDownField) {
        this.linkedDrillDownField = linkedDrillDownField;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        Chart chart = this.getDrillDownChart() != null ? this
            .getDrillDownChart() : this.getParentChart();
        
        return chart.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DashboardDetail)) {
            return false;
        }
        final DashboardDetail other = (DashboardDetail) obj;
        if ((getDashboard().equals(other.getDashboard()))
            && (getParentChart().equals(other.getParentChart()))
            && (((getDrillDownChart() == null)
                && (other.getDrillDownChart() == null)
                && (getLinkedDrillDownField() == null) && (other
                .getLinkedDrillDownField() == null)) || ((getDrillDownChart()
                .equals(other.getDrillDownChart())) && (getLinkedDrillDownField()
                .equals(other.getLinkedDrillDownField()))))) {
            return true;
        }
        return false;
    }

    
    @Override
    public int hashCode() {
        if (this.dashboard == null) {
            return 0;
        }

        int code = this.dashboard == null
            ? 0 : (this.dashboard.hashCode() * this.parentChart.hashCode());
        code *= 37;
        code += this.drillDownChart == null ? 0 : (this.drillDownChart
            .hashCode());
        return code;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 