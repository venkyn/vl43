/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.model.VersionedModelObject;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class defines the model of Dashboard.
 * 
 * @author kudupi
 */
public class DashboardRoot extends VersionedModelObject implements DataObject,
    Serializable, Taggable {

    // Serial Version ID.
    private static final long serialVersionUID = 1668990669024080602L;

    private String name;

    private String description;
    
    private TimeWindowFilterType timeFilter = TimeWindowFilterType.NO_FILTER;

    private Boolean isDefault = false;

    private Set<DashboardDetail> details = new HashSet<DashboardDetail>();
    
    private Set<Tag> tags;


    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return description;
    }

    
    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Getter for the timeFilter property.
     * @return TimeWindowFilterType value of the property
     */
    public TimeWindowFilterType getTimeFilter() {
        return timeFilter;
    }


    /**
     * Setter for the timeFilter property.
     * @param timeFilter the new timeFilter value
     */
    public void setTimeFilter(TimeWindowFilterType timeFilter) {
        this.timeFilter = timeFilter;
    }


    /**
     * Getter for the isDefault property.
     * @return Boolean value of the property
     */
    public Boolean getIsDefault() {
        return isDefault;
    }


    /**
     * Setter for the isDefault property.
     * @param isDefault the new isDefault value
     */
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }


    /**
     * @return the details
     */
    public Set<DashboardDetail> getDetails() {
        return details;
    }


    
    /**
     * @param details the details to set
     */
    public void setDetails(Set<DashboardDetail> details) {
        this.details = details;
    }


    /**
     * Getter for the tags property.
     * @return Set<Tag> value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    
    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getName();
    }    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Dashboard)) {
            return false;
        }
        
        final Dashboard other = (Dashboard) obj;
        if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }


    @Override
    public int hashCode() {
        return this.name == null ? 0 : this.name.hashCode();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 