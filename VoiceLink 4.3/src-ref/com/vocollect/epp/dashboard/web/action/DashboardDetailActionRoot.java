/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.web.action.DataProviderAction;

import com.opensymphony.xwork2.Preparable;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * Action class for Dashboard details.
 * 
 * @author mraj
 *
 */
public class DashboardDetailActionRoot extends DataProviderAction implements
    Preparable {

    /**
     * 
     */
    private static final long serialVersionUID = 7611357088440960470L;

    private static final long DASHBOARD_DETAILS_VIEW_ID = -2004;
    
    private DashboardDetailManager dashboardDetailManager;
    

    /**
     * @return the dashboardDetailManager
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    
    /**
     * @param dashboardDetailManager the dashboardDetailManager to set
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }

    /* (non-Javadoc)
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {

    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getDashboardDetailManager();
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "dashboard.detail";
    }

    
    /* (non-Javadoc)
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(DASHBOARD_DETAILS_VIEW_ID);
        return viewIds;
    }

    @Override
    public String delete() throws DataAccessException {
        Long[] ids = getIds();
        Arrays.sort(ids, Collections.reverseOrder());
        setIds(ids);
        return super.delete();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 