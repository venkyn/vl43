/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.web.action;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.CustomActionImpl;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DisplayUtilities;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Level;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Action class for Dashboards.
 * 
 * @author kudupi
 */
public class DashboardActionRoot extends DataProviderAction implements
    Preparable {

    // Serial Version ID
    private static final long serialVersionUID = -4996027919148354130L;

    private static final Logger log = new Logger(DashboardActionRoot.class);

    private static final long DASHBOARD_VIEW_ID = -2003;

    private static final long DASHBOARD_DETAILS_VIEW_ID = -2004;

    private DashboardManager dashboardManager;

    private ChartManager chartManager;

    private DashboardDetailManager dashboardDetailManager;

    private UserManager userManager;
    
    private DAColumnManager daColumnManager;

    // The list of columns for the dashboard table
    private List<Column> dashboardColumns;

    private List<Column> dashboardDetailsColumns;
    
    private DisplayUtilities displayUtilities;

    private Dashboard dashboard;

    private Long dashboardId;
    
    private Integer timeFilter = 0;

    private Long addChartId;

    private Chart newChart;

    private Long parentChartId;

    private Long drillDownChartId;
    
    private Long linkedDrillDownFieldId;

    private String dashboardIds;

    private Map<String, Long> parentChartsMap;
    
    private Map<String, Long> drillDownChartsMap;
    
    private Chart parentChartForDrillDown;


    /**
     * Getter for the dashboardManager property.
     * @return DashboardManager value of the property
     */
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * Setter for the dashboardManager property.
     * @param dashboardManager the new dashboardManager value
     */
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * Getter for the chartManager property.
     * @return ChartManager value of the property
     */
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * Setter for the chartManager property.
     * @param chartManager the new chartManager value
     */
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * @return the dashboardDetailManager
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    /**
     * @param dashboardDetailManager the dashboardDetailManager to set
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }

    /**
     * @return the userManager
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * @param userManager the userManager to set
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
    
    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    
    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }
    
    /**
     * Getter for the dashboardColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getDashboardColumns() {
        return dashboardColumns;
    }

    /**
     * @return the dashboardDetailsColumns
     */
    public List<Column> getDashboardDetailsColumns() {
        return dashboardDetailsColumns;
    }
    
    /**
     * Getter for the displayUtilities property.
     * @return DisplayUtilities value of the property
     */
    public DisplayUtilities getDisplayUtilities() {
        return displayUtilities;
    }
    
    /**
     * Setter for the displayUtilities property.
     * @param displayUtilities the new displayUtilities value
     */
    public void setDisplayUtilities(DisplayUtilities displayUtilities) {
        this.displayUtilities = displayUtilities;
    }

    /**
     * Method to set the properties of DisplayUtilities Bean.
     */
    private void setDisplayUtilitiesProperties() {
        try {
            getDisplayUtilities().setAction((DataProviderAction) this);
            getDisplayUtilities().setCurrentUser(
                getUserManager().get(getCurrentUser().getId()));
        } catch (DataAccessException e) {
            if (log.isDebugEnabled()) {
                log.debug(e.getLocalizedMessage());
            }
        }
    }

    /**
     * Getter for the dashboard property.
     * @return Dashboard value of the property
     */
    public Dashboard getDashboard() {
        return dashboard;
    }

    /**
     * Setter for the dashboard property.
     * @param dashboard the new dashboard value
     */
    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    /**
     * Getter for the dashboardId property.
     * @return Long value of the property
     */
    public Long getDashboardId() {
        return dashboardId;
    }

    /**
     * Setter for the dashboardId property.
     * @param dashboardId the new dashboardId value
     */
    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    /**
     * Getter for the timeFilter property.
     * @return Integer value of the property
     */
    public Integer getTimeFilter() {
        return timeFilter;
    }

    /**
     * Setter for the timeFilter property.
     * @param timeFilter the new timeFilter value
     */
    public void setTimeFilter(Integer timeFilter) {
        this.timeFilter = timeFilter;
    }

    /**
     * Getter for the addChartId property.
     * @return Long value of the property
     */
    public Long getAddChartId() {
        return addChartId;
    }

    /**
     * Setter for the addChartId property.
     * @param addChartId the new addChartId value
     */
    public void setAddChartId(Long addChartId) {
        this.addChartId = addChartId;
    }

    /**
     * @return the parentChartId
     */
    public Long getParentChartId() {
        return parentChartId;
    }

    /**
     * @param parentChartId the parentChartId to set
     */
    public void setParentChartId(Long parentChartId) {
        this.parentChartId = parentChartId;
    }

    /**
     * @return the drillDownChartId
     */
    public Long getDrillDownChartId() {
        return drillDownChartId;
    }

    /**
     * @param drillDownChartId the drillDownChartId to set
     */
    public void setDrillDownChartId(Long drillDownChartId) {
        this.drillDownChartId = drillDownChartId;
    }
    
    /**
     * Getter for the linkedDrillDownFieldId property.
     * @return Long value of the property
     */
    public Long getLinkedDrillDownFieldId() {
        return linkedDrillDownFieldId;
    }

    
    /**
     * Setter for the linkedDrillDownFieldId property.
     * @param linkedDrillDownFieldId the new linkedDrillDownFieldId value
     */
    public void setLinkedDrillDownFieldId(Long linkedDrillDownFieldId) {
        this.linkedDrillDownFieldId = linkedDrillDownFieldId;
    }

    /**
     * @return the dashboardIds
     */
    public String getDashboardIds() {
        return dashboardIds;
    }

    /**
     * @param dashboardIds the dashboardIds to set
     */
    public void setDashboardIds(String dashboardIds) {
        this.dashboardIds = dashboardIds;
    }
    
    /**
     * Getter for the parentChartForDrillDown property.
     * @return Chart value of the property
     */
    public Chart getParentChartForDrillDown() {
        return parentChartForDrillDown;
    }
    
    /**
     * Setter for the parentChartForDrillDown property.
     * @param parentChartForDrillDown the new parentChartForDrillDown value
     */
    public void setParentChartForDrillDown(Chart parentChartForDrillDown) {
        this.parentChartForDrillDown = parentChartForDrillDown;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        if (getViewId() == null || getViewId().equals(DASHBOARD_VIEW_ID)) {
            setDisplayUtilitiesProperties();
            return (DataProvider) this.getDashboardManager();
        } else {
            return (DataProvider) this.getDashboardDetailManager();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "dashboard";
    }

    /**
     * Getter for the dashboardViewId property.
     * @return long value of the property
     */
    public static long getDashboardViewId() {
        return DASHBOARD_VIEW_ID;
    }

    /**
     * Getter for the dashboardChartsViewId property.
     * @return long value of the property
     */
    public static long getDashboardDetailsViewId() {
        return DASHBOARD_DETAILS_VIEW_ID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(DASHBOARD_VIEW_ID);
        viewIds.add(DASHBOARD_DETAILS_VIEW_ID);
        return viewIds;
    }
    /**
     * Method to prepare details TC data.
     * @return success
     * @throws Exception - if unable to access TC data
     */
    public String getDashboardDetailsTableData() throws Exception {
        if (StringUtil.isNullOrEmpty(getDashboardIds())) {
            super.getTableData("obj.dashboard.id in ( -100)");
        } else {
            super.getTableData("obj.dashboard.id in ( " + this.dashboardIds
                + " )");
        }

        return SUCCESS;
    }
    
    /**
     * Action for the dashboard view page. Initializes the dashboard table
     * columns
     * 
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View dashboardView = getUserPreferencesManager().getView(
            DASHBOARD_VIEW_ID);
        this.dashboardColumns = this.getUserPreferencesManager().getColumns(
            dashboardView, getCurrentUser());
        View dashboardDetailsView = getUserPreferencesManager().getView(
            DASHBOARD_DETAILS_VIEW_ID);
        this.dashboardDetailsColumns = this.getUserPreferencesManager()
            .getColumns(dashboardDetailsView, getCurrentUser());
        return SUCCESS;
    }
    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);

        log.debug("Inside prepare method");
        log.debug("dashboardId = " + getDashboardId());
        if (getDashboardId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("dashboardId is " + getDashboardId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the dashboard is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved dashboard from session");
                }

                this.dashboard = (Dashboard) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting dashboard from database");
                }
                this.dashboard = this.dashboardManager.get(getDashboardId());
                saveEntityInSession(this.dashboard);
            }
               
            this.setTimeFilter(this.dashboard.getTimeFilter().toValue());
            
            if (log.isDebugEnabled()) {
                log.debug("Dashboard version is: "
                    + this.dashboard.getVersion());
            }
        } else if (this.dashboard == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new Dashboard object");
            }
            this.dashboard = new Dashboard();
        }
    }

    /**
     * @return SUCCESS or INPUT
     * @throws Exception When save fails
     */
    public String save() throws Exception {
        boolean isNew = this.dashboard.isNew();

        try {
            // Before saving the Dashboard, checking if already a Dashboard
            // exists with
            // the same name.
            this.getDashboardManager().verifyUniquenessByName(this.dashboard);

            this.dashboard.setTimeFilter(TimeWindowFilterType.toEnum(this.getTimeFilter()));
            
            this.getDashboardManager().save(this.dashboard);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified dashboard "
                + this.dashboard.getName());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.dashboard"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Dashboard modifiedDashboard = getDashboardManager().get(
                    getDashboardId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.dashboard.setVersion(modifiedDashboard.getVersion());
                // Store the modified data for display
                setModifiedEntity(modifiedDashboard);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.dashboard"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("dashboard."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/dashboards/view.action?dashboardId="
                + this.dashboard.getId()), this.dashboard.getName()));

        return SUCCESS;
    }

    /**
     * Deletes the currently viewed dashboard.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current dashboard
     */
    public String deleteCurrentDashboard() throws Exception {

        Dashboard dashboardToDelete = null;

        try {
            dashboardToDelete = dashboardManager.get(getDashboardId());

            dashboardManager.delete(dashboardToDelete);

            addSessionActionMessage(new UserMessage(
                "dashboard.delete.message.success", dashboardToDelete.getName()));

        } catch (BusinessRuleException e) {
            log.warn("Unable to delete Dashboard: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }
    
    /**
     * 
     * @return localised value of modified filter type
     */
    public String getModifiedTimeFilter() {
        return ResourceUtil
            .getLocalizedEnumName(((Dashboard) getModifiedEntity())
                .getTimeFilter());
    }

    /**
     * 
     * @return list of charts for the add chart drop down list
     * @throws DataAccessException dae
     */
    public Map<String, Long> getChartsToAddMap() throws DataAccessException {
        setDashboard(getDashboardManager().get(getDashboardId()));
        List<Chart> charts = getChartManager().listCharts();
        Set<Chart> chartsInDashboard = getChartManager()
            .getChartsInDashboard(getDashboard());
        charts.removeAll(chartsInDashboard);

        Map<String, Long> map = new TreeMap<String, Long>(
            StringUtil.getPrimaryCollator());
        for (Chart chart : charts) {
            map.put(getDataTranslation(chart.getName()), chart.getId());
        }

        return map;
    }
    
    /**
     * Method to get list of dashboard data time filters.
     * @return time filter map
     */
    public Map<Integer, String> getTimeFilterMap() {
        Map<Integer, String> timeFilterMap = new LinkedHashMap<Integer, String>();
        
        for (TimeWindowFilterType timeFilterValue : TimeWindowFilterType.values()) {
            timeFilterMap.put(timeFilterValue.toValue(), ResourceUtil.getLocalizedEnumName(timeFilterValue));
        }
        
        return timeFilterMap;
    }

    /**
     * Method to populate parent charts select box.
     * @return parentChartsMap Map<String, Long>.
     * @throws DataAccessException .
     */
    public Map<String, Long> getParentCharts() throws DataAccessException {
        parentChartsMap = new TreeMap<String, Long>(
            StringUtil.getPrimaryCollator());

        setDashboard(getDashboardManager().get(getDashboardId()));

        Set<Chart> chartsInDashboard = getChartManager()
            .getChartsInDashboard(getDashboard());

        for (Chart chart : chartsInDashboard) {
            parentChartsMap.put(getDataTranslation(chart.getName()),
                chart.getId());
        }

        return parentChartsMap;
    }

    /**
     * Getter for the linkingCategoryForDrillDown property.
     * @return String value of the chart category
     * @throws DataAccessException .
     */
    public String getLinkingCategoryForDrillDown() throws DataAccessException {
        String linkingChartCategory = "";
        if (getParentChartId() == null) {
            Set<String> chartNames = this.parentChartsMap.keySet();

            Iterator<String> chartItr = chartNames.iterator();
            if (chartItr.hasNext()) {
                linkingChartCategory = getChartManager()
                    .get(this.parentChartsMap.get(chartItr.next()))
                    .getCategory().getDaColumnDisplayName();
            }
        } else {
            linkingChartCategory = getChartManager().get(getParentChartId())
                .getCategory().getDaColumnDisplayName();
        }
        return linkingChartCategory;
    }

    /**
     * Method to populate parent charts select box. All those charts which are
     * not part of dashboard will be shown in this list.
     * @return child charts Map.
     * @throws DataAccessException .
     */
    public Map<String, Long> getDrillDownCharts() throws DataAccessException {
        setDashboard(getDashboardManager().get(getDashboardId()));

        Set<Chart> chartsInDashboard = getChartManager().getChartsInDashboard(
            getDashboard());
        List<Chart> drillCharts = new ArrayList<Chart>();

        Chart selectedChart = null;

        if (getParentChartId() == null) {
            Set<String> parentChartMapKeys = this.parentChartsMap.keySet();
            Iterator<String> keyItr = parentChartMapKeys.iterator();

            if (keyItr.hasNext()) {
                Long selectedParentChartId = parentChartsMap.get(keyItr.next());
                selectedChart = getChartManager().get(selectedParentChartId);
            }
        } else {
            selectedChart = getChartManager().get(getParentChartId());
        }

        setParentChartForDrillDown(selectedChart);

        drillCharts = getChartManager().listDrillDownChartsForDropdown(
            getDashboard(), selectedChart);

        drillCharts.removeAll(chartsInDashboard);

        drillDownChartsMap = new TreeMap<String, Long>(
            StringUtil.getPrimaryCollator());
        for (Chart chart : drillCharts) {
            drillDownChartsMap.put(getDataTranslation(chart.getName()),
                chart.getId());
        }
        return drillDownChartsMap;
    }

    /**
     * Method to populate drill down chart field select box. All the data fields
     * associated to the selected daInfo of the selected drill down chart are
     * returned
     * @return drill down chart field map
     * @throws DataAccessException .
     */
    public Map<Long, String> getLinkedDrillDownFields()
        throws DataAccessException {
        setDashboard(getDashboardManager().get(getDashboardId()));

        DAInformation daInfo = null;
        Chart parentChart = getParentChartForDrillDown();

        if (getDrillDownChartId() == null) {
            Set<String> drillChartMapKeys = this.drillDownChartsMap.keySet();
            Iterator<String> keyItr = drillChartMapKeys.iterator();

            if (keyItr.hasNext()) {
                Long selectedDrillChartId = drillDownChartsMap.get(keyItr
                    .next());
                daInfo = getChartManager().get(selectedDrillChartId)
                    .getDaInformation();
            }
        } else {
            daInfo = getChartManager().get(getDrillDownChartId())
                .getDaInformation();
        }
        List<DAFieldType> daFieldType = new ArrayList<DAFieldType>();
        daFieldType.add(parentChart.getCategory().getFieldType());
        List<DAColumn> linkedDrillDownFields = getDaColumnManager()
            .listDAColumnByFieldType(daInfo, daFieldType);

        Map<Long, String> linkedDrillDownFieldsMap = new LinkedHashMap<Long, String>();
        for (DAColumn field : linkedDrillDownFields) {
            linkedDrillDownFieldsMap.put(field.getId(),
                ResourceUtil.getLocalizedKeyValue(field.getDisplayName()));
        }
        return linkedDrillDownFieldsMap;
    }

    /**
     * 
     * @return list of charts for the add chart drop down list
     * @throws DataAccessException dae
     * @throws JSONException - jse
     */
    public String getDrillDownChartsActionSupport() throws DataAccessException,
        JSONException {
        setDashboard(getDashboardManager().get(getDashboardId()));

        Chart parentChart = getChartManager().get(this.getParentChartId());
        List<Chart> drillCharts = getChartManager().listDrillDownChartsForDropdown(
            getDashboard(), parentChart);
        Set<Chart> chartsInDashboard = getChartManager()
            .getChartsInDashboard(getDashboard());
        drillCharts.removeAll(chartsInDashboard);
        
        // Create JSON from daColumn
        JSONArray drillDownCharts = new JSONArray();
        for (Chart chart : drillCharts) {            
            JSONObject chartOption = new JSONObject();
            chartOption.put("id", chart.getId());
            chartOption.put("name", getDataTranslation(chart.getName()));
            drillDownCharts.put(chartOption);
        }

        JSONObject chartObj = new JSONObject();
        
        chartObj.put("category", parentChart.getCategory()
            .getDaColumnDisplayName());
        chartObj.put("drillDownCharts", drillDownCharts);
        

        DAInformation daInfo = drillCharts.get(0).getDaInformation();
        chartObj.put("linkedDrillDownFields",
            getLinkedDrillDownFieldsJSON(daInfo));
       
         setMessage(chartObj.toString());
        return SUCCESS;
    }

    /**
     * @return Success or failure after setting data fields json in message.
     * @throws DataAccessException dae
     * @throws JSONException jse
     */
    public String getLinkedDrillDownFieldsActionSupport() throws DataAccessException, JSONException {
        DAInformation daInfo = getChartManager().get(
            this.getDrillDownChartId()).getDaInformation();
        
        JSONObject linkedDrillDownFieldsObj = new JSONObject();
        linkedDrillDownFieldsObj.put("linkedDrillDownFields",
            getLinkedDrillDownFieldsJSON(daInfo));
        
        setMessage(linkedDrillDownFieldsObj.toString());
        return SUCCESS;
    }

    /**
     * Method to populate drill down chart field select box on change of drill
     * down chart selection. All the data fields associated to the selected
     * daInfo of the selected drill down chart are returned
     * @param daInformation daInfo
     * @return linked drill down field json
     */
    public JSONArray getLinkedDrillDownFieldsJSON(DAInformation daInformation) {
        try {

            Chart parentChart = getChartManager().get(this.getParentChartId());

            List<DAFieldType> daFieldType = new ArrayList<DAFieldType>();
            daFieldType.add(parentChart.getCategory().getFieldType());

            List<DAColumn> linkedDrillDownFields = getDaColumnManager()
                .listDAColumnByFieldType(daInformation, daFieldType);

            // Create JSON from daColumn
            JSONArray linkedDrillDownFieldsArray = new JSONArray();
            for (DAColumn field : linkedDrillDownFields) {
                JSONObject linkedDrillDownFieldOption = new JSONObject();
                linkedDrillDownFieldOption.put("id", field.getId());
                linkedDrillDownFieldOption.put("name",
                    ResourceUtil.getLocalizedKeyValue(field.getDisplayName()));
                linkedDrillDownFieldsArray.put(linkedDrillDownFieldOption);
            }

            return linkedDrillDownFieldsArray;
        } catch (Exception e) {
            log.warn("Excption occured preparing data for linked drill down fields : "
                + e.getLocalizedMessage());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 
     * @return success if selected dashboard successfully set as default
     * @throws DataAccessException dae
     * @throws BusinessRuleException bre
     */

    public String setAsDefault() throws DataAccessException,
        BusinessRuleException {
        String propName = getDashboardManager()
            .getDefaultDashboardUserPropertyName();
        Dashboard setToDefaultdashboard = getDashboardManager().get(
            getDashboardId());
        User user = this.getUserManager().get(getCurrentUser().getId());
        UserProperty prop = getUserManager().getPrimaryDAO().getPropertyByName(
            user.getName(), propName);
        if (prop == null) {
            prop = new UserProperty();
            prop.setName(propName);
            user.getUserProperties().add(prop);
        }

        prop.setValue(getDashboardId().toString());
        getUserManager().save(user);
        String[] values = { setToDefaultdashboard.getName() };
        setJsonMessage(
            getText("dashboard.setAsDefault.message.success", values), SUCCESS);

        return SUCCESS;

    }

    /**
     * 
     * @return String Input
     * @throws DataAccessException dae
     */
    public String showAddChartToDashboardDialog() throws DataAccessException {
        setDashboard(getDashboardManager().get(getDashboardId()));
        int numberOfChartsInDashboard = getChartManager()
            .getChartsInDashboard(getDashboard()).size();
        int numberOfChartsInSite = getChartManager().listCharts().size();

        if (getChartManager().listCharts().isEmpty()) {
            setJsonMessage(getText("dashboard.addChart.noCharts"),
                ERROR_FAILURE);
            return SUCCESS;
        } else {
            if (numberOfChartsInSite > numberOfChartsInDashboard) {
                return INPUT;
            } else {
                setJsonMessage(getText("dashboard.addChart.emptyList"),
                    ERROR_FAILURE);
                return SUCCESS;
            }
        }
    }

    /**
     * 
     * @return String key
     * @throws DataAccessException dae
     */
    public String addChartToDashboard() throws DataAccessException {
        newChart = getChartManager().get(addChartId);
        final Chart newChartObject = this.newChart;
        return super.performAction(new CustomActionImpl() {

            public SystemErrorCode getSystemErrorCode() {
                return SystemErrorCode.ENTITY_NOT_UPDATEABLE;
            }

            public String getActionPrefix() {
                return "addChartsToDashboard";
            }

            public boolean execute(DataObject obj) throws VocollectException,
                DataAccessException, BusinessRuleException {
                return dashboardManager.executeAddChartToDashboard(((Dashboard) obj),
                    newChartObject, null, null);
            }
        });
    }

    /**
     * Method to check if drill down of charts is possible or not. If there are
     * no charts in the system or dashboard doesnt have any charts added to it.
     * In both the cases drill down is not allowed.
     * 
     * @return struts response String.
     * @throws DataAccessException .
     */
    public String showChartsForDrillDown() throws DataAccessException {
        Long[] ids = getIds();
        setDashboard(getDashboardManager().get(ids[0]));

        int numberOfChartsInDashboard = this.getChartManager()
            .getChartsInDashboard(getDashboard()).size();

        if (numberOfChartsInDashboard < 1) {
            setJsonMessage(getText("dashboard.drilldown.noCharts"),
                ERROR_FAILURE);
            return SUCCESS;
        } else {
            String urlToRedirect = "addDrillDownChart!input.action?dashboardId="
                + ids[0];
            setJsonMessage(urlToRedirect, ERROR_REDIRECT);
            return SUCCESS;
        }
    }

    /**
     * Method to save the drill down configuration Information.
     * @return struts action response String.
     * @throws DataAccessException -dae
     * @throws BusinessRuleException - be
     */
    public String addDrillDownChart() throws DataAccessException,
        BusinessRuleException {
        Chart parentChart = getChartManager().get(this.getParentChartId());
        Chart drillDownChart = null;
        DAColumn drillDownField = null;
        if (this.getDrillDownChartId() != null) {
            drillDownChart = getChartManager().get(this.getDrillDownChartId());
        } else {
            FieldValidationException fve = new FieldValidationException(
                "drillDownChartId", "", new UserMessage(
                    "dashboard.drilldown.noChildChart"));
            addFieldError(fve.getField(), fve.getUserMessage());
            // Go back to the form to show the error message.
            return INPUT;
        }

        if (this.getLinkedDrillDownFieldId() != null) {
            drillDownField = getDaColumnManager().get(
                this.getLinkedDrillDownFieldId());
        } else {
            FieldValidationException fve = new FieldValidationException(
                "linkedDrillDownFieldId", "", new UserMessage(
                    "dashboard.drilldown.noLinkedDrillDownFieldId"));
            addFieldError(fve.getField(), fve.getUserMessage());
            // Go back to the form to show the error message.
            return INPUT;
        }
        
        Dashboard dashbaord = getDashboardManager().get(getDashboardId());
        String message = "";
        if (getDashboardManager().executeAddChartToDashboard(dashbaord,
            parentChart, drillDownChart, drillDownField)) {
            message = ResourceUtil.getLocalizedKeyValue("dashboard.addDrillDownCharts.dashboard.message.success");
            addSessionActionMessage(new UserMessage(message));
        } else {
            message = ResourceUtil.getLocalizedKeyValue("dashboard.addDrillDownCharts.dashboard.message.failure");
            addSessionActionErrorMessage(new UserMessage(message));
        }
        
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 