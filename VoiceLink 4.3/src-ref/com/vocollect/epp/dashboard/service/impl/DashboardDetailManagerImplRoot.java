/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service.impl;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.dao.DashboardDetailDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.List;

/**
 * @author mraj
 * 
 */
public abstract class DashboardDetailManagerImplRoot extends
    GenericManagerImpl<DashboardDetail, DashboardDetailDAO> implements
    DashboardDetailManager {
    
    private static final Logger LOG = new Logger(DashboardDetailManagerImplRoot.class);
    
    private UserManager userManager;
    
    /**
     * @return the userManager
     */
    public UserManager getUserManager() {
        return userManager;
    }

    
    /**
     * @param userManager the userManager to set
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }


    /**
     * @param primaryDAO - the primary DAO
     */
    public DashboardDetailManagerImplRoot(DashboardDetailDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dashboard.service.DashboardDetailManagerRoot#findLinkedDrillDownField(com.vocollect.epp.dashboard.model.Dashboard,
     *      com.vocollect.epp.chart.model.Chart,
     *      com.vocollect.epp.chart.model.Chart)
     */
    public DAColumn findLinkedDrillDownField(Dashboard dashboard,
                                             Chart parentChart,
                                             Chart drillDownChart) {
        return this.getPrimaryDAO().findLinkedDrillDownField(dashboard,
            parentChart, drillDownChart);
    }
    
    @Override
    public synchronized Object delete(DashboardDetail instance) throws BusinessRuleException,
    DataAccessException {
        Chart chartToDelete = instance.getDrillDownChart() != null ? instance
            .getDrillDownChart() : instance.getParentChart();
        
        if (countDependentDetails(instance.getDashboard(), chartToDelete)
            .longValue() == 0) {
            Object obj = super.delete(instance);
            
            // also remove the chart preferences if any
            SiteContextHolder.getSiteContext().setFilterBySite(false);
            getUserManager().updateUserChartPreferencesByDashboardAndChart(
                instance.getDashboard().getId(), chartToDelete.getId());
            SiteContextHolder.getSiteContext().setFilterBySite(true);

            return obj;
        } else {
            throw new DataAccessException(
                SystemErrorCode.ENTITY_NOT_DELETEABLE, new UserMessage(
                    "dashboard.detail.dependency.exist.error",
                    chartToDelete.getName()), null);
        }
    }
    
    @Override
    public Number countDependentDetails(Dashboard dashboard,
        Chart chart) {
        return this.getPrimaryDAO().listDependentDetails(dashboard, chart).size();
    }

    @Override
    public Boolean checkDashboardAndChartExist(Dashboard dashboard,
                                                 Chart chart) {
        return this.getPrimaryDAO()
            .countDashboardChartCombination(dashboard, chart).intValue() <= 0
            ? false : true;
    }
    
    @Override
    public void removeAllDependentDetails(Chart chart) throws BusinessRuleException, DataAccessException {
        List<DashboardDetail> refs = this.getPrimaryDAO().listDetailReferences(chart);
        for (DashboardDetail dashboardDetail : refs) {
            if (removeDetail(dashboardDetail) == null) {
                this.delete(dashboardDetail);
            }
        }
    }
    
    /**
     * Recursive method to incrementally remove child detail records first. 
     * @param detail - the dashboard detail object to be deleted
     * @return - list of dependent dashboard detail objects OR null if none found
     * @throws BusinessRuleException - bre
     * @throws DataAccessException - dae
     */
    private List<DashboardDetail> removeDetail(DashboardDetail detail)
        throws BusinessRuleException, DataAccessException {
        List<DashboardDetail> details = this.getPrimaryDAO()
            .listDependentDetails(
                detail.getDashboard(), detail.getDrillDownChart());
        if (details.isEmpty()) {
            return null;
        } else {
            for (DashboardDetail dashboardDetail : details) {
                List<DashboardDetail> details1 = removeDetail(dashboardDetail);
                if (details1 == null) {
                    this.delete(dashboardDetail);
                }
            }
            return null;
        }
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dashboard.service.DashboardDetailManagerRoot#listDetailIdsByLinkedField(com.vocollect.epp.dataaggregator.model.DAColumn)
     */
    public List<Long> listDetailIdsByLinkedField(DAColumn linkedDrillDownField) {
        return this.getPrimaryDAO().listDetailIdsByLinkedField(
            linkedDrillDownField);
    }    
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dashboard.service.DashboardDetailManagerRoot#forceDelete(java.lang.Long)
     */
    @Override
    public String forceDelete(Long detailId) throws BusinessRuleException, DataAccessException {
        
        SiteContextHolder.getSiteContext().setFilterBySite(false);
        DashboardDetail dd = this.get(detailId);
        SiteContextHolder.getSiteContext().setFilterBySite(true);

        String dashboardName = dd.getDashboard().getName();
        
        if (this.removeDetail(dd) == null) {
            this.delete(dd);
            return dashboardName;
        }

        LOG.warn("Some of the associated details couldn't be deleted, due to which forced delete didn't happen ");
        
        return null;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 