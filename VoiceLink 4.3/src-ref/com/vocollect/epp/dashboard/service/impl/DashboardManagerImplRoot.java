/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service.impl;

import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.dao.DashboardDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.List;
import java.util.Set;

/**
 * @author smittal
 * 
 */
public abstract class DashboardManagerImplRoot extends
    GenericManagerImpl<Dashboard, DashboardDAO> implements DashboardManager {

    private DashboardDetailManager dashboardDetailManager;
    
    private AlertManager alertManager;
    
    private UserManager userManager;
    
    /**
     * @param primaryDAO - the primary DAO
     */
    public DashboardManagerImplRoot(DashboardDAO primaryDAO) {
        super(primaryDAO);
    }
    
    /**
     * Getter for the dashboardDetailManager property.
     * @return DashboardDetailManager value of the property
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    
    /**
     * Setter for the dashboardDetailManager property.
     * @param dashboardDetailManager the new dashboardDetailManager value
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }
    
    /**
     * Getter for the alertManager property.
     * @return AlertManager value of the property
     */
    public AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * Setter for the alertManager property.
     * @param alertManager the new alertManager value
     */
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * Getter for the userManager property.
     * @return UserManager value of the property
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dashboard.service.DashboardManagerRoot#listDashboards()
     */
    public List<Dashboard> listDashboards() {
        return this.getPrimaryDAO().listDashboards();
    }

    /**
     * Implementation to check to see if the chart name is unique.
     * @param dashboard the Dashboard to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    public void verifyUniquenessByName(Dashboard dashboard)
        throws BusinessRuleException, DataAccessException {
        boolean isNew = dashboard.isNew();
        // Uniqueness check for name
        Long nameUniquenessId = getPrimaryDAO().uniquenessByName(
            dashboard.getName());
        if (nameUniquenessId != null
            && (isNew || (!isNew && nameUniquenessId.longValue() != dashboard
                .getId().longValue()))) {
            throw new FieldValidationException("dashboard.name",
                dashboard.getName(), new UserMessage(
                    "dashboard.error.duplicateName", dashboard.getName()));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Dashboard dashboard) throws BusinessRuleException,
        DataAccessException {

        if (getAlertManager().listAlertsByLinkedDashboard(dashboard).isEmpty()) {

            String userPropertyName = getDefaultDashboardUserPropertyName();
            Long userPropertyValue = dashboard.getId();
            synchronized (this) {
                SiteContextHolder.getSiteContext().setFilterBySite(false);
                getUserManager().updateUserChartPreferencesByDashboard(
                    dashboard.getId());

                getUserManager().updateUserPropertyByDashboard(userPropertyName,
                    userPropertyValue);
                SiteContextHolder.getSiteContext().setFilterBySite(true);
            }
            return super.delete(dashboard);
        } else {
            throw new DataAccessException(
                SystemErrorCode.ENTITY_NOT_DELETEABLE, new UserMessage(
                    "dashboard.delete.error.alert.inUse", dashboard.getName()),
                null);
        }
    }
    
    /***
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.dashboard.service.DashboardManagerRoot#executeAddChartToDashboard(com.vocollect.epp.dashboard.model.Dashboard,
     *      com.vocollect.epp.chart.model.Chart,
     *      com.vocollect.epp.chart.model.Chart,
     *      com.vocollect.epp.dataaggregator.model.DAColumn)
     */
    public Boolean executeAddChartToDashboard(Dashboard dashboard,
                                              Chart chart,
                                              Chart drillDownChart,
                                              DAColumn linkedDrillDownField)
        throws BusinessRuleException, DataAccessException {

        Set <DashboardDetail> details = dashboard.getDetails();

        DashboardDetail detail = new DashboardDetail();

        detail.setDashboard(dashboard);
        detail.setParentChart(chart);
        detail.setDrillDownChart(drillDownChart);
        detail.setLinkedDrillDownField(linkedDrillDownField);
        
        if (details.contains(detail)) {
            return false;
        } else {
            getDashboardDetailManager().save(detail);
            return true;
        }
    }


    @Override
    public Dashboard findDefaultDashboard(User user) throws BusinessRuleException,
        DataAccessException {
        String propName = getDefaultDashboardUserPropertyName();
       
        Set<UserProperty> userProps =  user.getUserProperties();
        for (UserProperty userProperty : userProps) {
            if (userProperty.getName().equals(propName)) {
                return get(Long.valueOf(userProperty.getValue()));
            }
        }
        return null;
    }
    
    @Override
    public String getDefaultDashboardUserPropertyName() {
        Site currentSite = SiteContextHolder.getSiteContext().getCurrentSite();
        String propName = "default.dashboard.for.site." + currentSite.getId();
        
        return propName;
    }
    @Override
    public List<Dashboard> listRecentCreatedDashboard() throws BusinessRuleException,
    DataAccessException {
        return this.getPrimaryDAO().listRecentCreatedDashboard();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 