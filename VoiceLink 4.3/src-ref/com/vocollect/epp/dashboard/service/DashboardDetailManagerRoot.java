/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.dao.DashboardDetailDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.GenericManager;

import java.util.List;

/**
 * @author mraj
 * 
 */
public interface DashboardDetailManagerRoot extends
    GenericManager<DashboardDetail, DashboardDetailDAO> {

    /**.
     * Method to find the linked drill down field
     * @param dashboard current dashboard
     * @param parentChart parent chart selected
     * @param drillDownChart child chart to draw
     * @return linked drill down field
     */
    public DAColumn findLinkedDrillDownField(Dashboard dashboard,
                                             Chart parentChart,
                                             Chart drillDownChart);
    
    /**.
     * Method to find count of dependent charts
     * @param dashboard current dashboard
     * @param chart chart to be selected
     * @return number of dependent charts
     */
    public Number countDependentDetails(Dashboard dashboard,
                                                      Chart chart);
    
    /**.
     * Method to find if the given chart and dashboard combination exist
     * @param dashboard - the dashboard
     * @param chart - the chart
     * @return true: if exist else false
     */
    public Boolean checkDashboardAndChartExist(Dashboard dashboard,
                                                      Chart chart);
    
    /**
     * Method to remove details corresponding to chart.
     * @param chart - chart corresponding to which details need to be removed
     * @throws BusinessRuleException - bre
     * @throws DataAccessException - dae
     */
    public void removeAllDependentDetails(Chart chart) throws BusinessRuleException, DataAccessException;
    
    /**.
     * method to get list of detail ids by linked field
     * @param linkedDrillDownField linked field
     * @return list of dashboard detail ids
     */
    public List<Long> listDetailIdsByLinkedField(DAColumn linkedDrillDownField);
    
    /**.
     * Method to delete dashboard detail, and all its drill down charts
     * @param detailId - id of detial to be deleted
     * @throws BusinessRuleException - thrown when business exception encountered
     * @throws DataAccessException - thrown when data exception encountered
     * @return name of affected dashboard
     */
    public String forceDelete(Long detailId) throws BusinessRuleException, DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 