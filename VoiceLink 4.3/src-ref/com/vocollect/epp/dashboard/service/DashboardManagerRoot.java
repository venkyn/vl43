/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.dao.DashboardDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

import java.util.List;

/**
 * @author smittal
 * 
 */
public interface DashboardManagerRoot extends
    GenericManager<Dashboard, DashboardDAO>, RemovableDataProvider {

    /**
     * Implementation to check to see if the name is unique.
     * @param dashboard the Dashboard to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    void verifyUniquenessByName(Dashboard dashboard)
        throws BusinessRuleException, DataAccessException;

    /**
     * 
     * @return list of site specific dashboards
     */
    public List<Dashboard> listDashboards();

    /**
     * 
     * @param dashboard dashboard to add chart to
     * @param chart chart to add
     * @param drillDownChart drill down to be added
     * @param linkedDrillDownField field used to filter data for drill down
     *            chart
     * @throws BusinessRuleException bre
     * @throws DataAccessException dae
     * @return true if chart successfully added else return false
     */
    Boolean executeAddChartToDashboard(Dashboard dashboard,
                                    Chart chart,
                                    Chart drillDownChart,
                                    DAColumn linkedDrillDownField)
        throws BusinessRuleException, DataAccessException;

    /**
     * @param user - current user 
     * @return The default dashboard
     * @throws BusinessRuleException if any data access exception
     * @throws DataAccessException if any data access exception
     */
    Dashboard findDefaultDashboard(User user) throws BusinessRuleException,
        DataAccessException;
    
    /**
     * method to generate user property name for setting user specific default dashboard. 
     * @return - property name
     */
    String getDefaultDashboardUserPropertyName();
    
    /**
     * Method to get most recently created dashboard in the site. 
     * @return recently created dashboard
     * @throws BusinessRuleException - bre
     * @throws DataAccessException - dae
     */
    List<Dashboard> listRecentCreatedDashboard() throws BusinessRuleException,
    DataAccessException;    

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 