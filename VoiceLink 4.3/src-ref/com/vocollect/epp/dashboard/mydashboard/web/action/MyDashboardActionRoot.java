/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.mydashboard.web.action;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseLockException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.ListObject;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.web.action.ConfigurableHomepagesAction;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 
 * @author khazra
 */
@SuppressWarnings("serial")
public class MyDashboardActionRoot extends ConfigurableHomepagesAction
    implements Preparable {

    private static final Logger log = new Logger(MyDashboardActionRoot.class);

    private Dashboard currentDashboard;

    private DashboardManager dashboardManager;

    private ChartManager chartManager;

    private UserManager userManager;

    private DashboardDetailManager dashboardDetailManager;

    private Long dashboardId;

    private Long chartId;

    private String parentChartsFilterFields;

    public static final String ERROR_SUCCESS = "0";

    public static final String ERROR_FAILURE = "1";
    
    public static final String ERROR_NO_REPORT_FAILURE = "2";

    /**
     * Getter for the currentDashboard property.
     * @return Dashboard value of the property
     */
    public Dashboard getCurrentDashboard() {
        return currentDashboard;
    }

    /**
     * Setter for the currentDashboard property.
     * @param currentDashboard the new currentDashboard value
     */
    public void setCurrentDashboard(Dashboard currentDashboard) {
        this.currentDashboard = currentDashboard;
    }

    /**
     * Getter for the dashboardManager property.
     * @return DashboardManager value of the property
     */
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * Setter for the dashboardManager property.
     * @param dashboardManager the new dashboardManager value
     */
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * Getter for the chartManager property.
     * @return ChartManager value of the property
     */
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * Setter for the chartManager property.
     * @param chartManager the new chartManager value
     */
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * @return the dashboardId
     */
    public Long getDashboardId() {
        return dashboardId;
    }

    /**
     * @param dashboardId the dashboardId to set
     */
    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    /**
     * @return the chartId
     */
    public Long getChartId() {
        return chartId;
    }

    /**
     * @param chartId the chartId to set
     */
    public void setChartId(Long chartId) {
        this.chartId = chartId;
    }

    /**
     * Getter for the parentChartsFilterFields property.
     * @return JSONArray value of the property
     */
    public String getParentChartsFilterFields() {
        return parentChartsFilterFields;
    }

    /**
     * Setter for the parentChartsFilterFields property.
     * @param parentChartsFilterFields the new parentChartsFilterFields value
     */
    public void setParentChartsFilterFields(String parentChartsFilterFields) {
        this.parentChartsFilterFields = parentChartsFilterFields;
    }

    /**
     * @return the userManager
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * @param userManager the userManager to set
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * @return the dashboardDetailManager
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    /**
     * @param dashboardDetailManager the dashboardDetailManager to set
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }

    @Override
    public void prepare() throws Exception {

    }

    /**
     * @return SUCCESS to redirect
     * @exception Exception If problem finding default dashboard
     */
    public String myDashboard() throws Exception {
        Dashboard dashboard = null;

        if (this.getDashboardId() != null) {
            dashboard = this.getDashboardManager().get(this.getDashboardId());
        } else {
            dashboard = this.getDashboardManager().findDefaultDashboard(
                getUserManager().get(getCurrentUser().getId()));
        }

        if (dashboard == null) {
            List<Dashboard> dashboards = this.getDashboardManager()
                .listRecentCreatedDashboard();
            if (dashboards == null || dashboards.isEmpty()) {
                dashboard = new Dashboard();
                dashboard.setName(ResourceUtil
                    .getLocalizedKeyValue("dashboard.default.none.name"));
                dashboard
                    .setDescription(ResourceUtil
                        .getLocalizedKeyValue("dashboard.default.none.description"));
            } else {
                dashboard = dashboards.get(0);
            }

        }

        this.setCurrentDashboard(dashboard);
        
        return SUCCESS;
    }

    /**
     * 
     * @return chart ids
     * @throws DataAccessException dae
     * @throws JSONException je
     */
    public String getChartIds() throws DataAccessException, JSONException {
        JSONArray data = new JSONArray();

        Dashboard dashboard = this.dashboardManager.get(this.getDashboardId());

        if (dashboard != null) {
            List<Chart> charts = this.getChartManager().listParentCharts(
                dashboard);

            for (Chart chart : charts) {
                JSONObject chartJSON = new JSONObject();

                chartJSON.put("id", chart.getId());

                data.put(chartJSON);
            }
        }

        JSONObject chartsConfig = new JSONObject();
        chartsConfig.put("chartIds", data);
        setMessage(chartsConfig.toString());
        return SUCCESS;
    }

    /**
     * This method returns data for all the charts in current dashboard. By
     * default the current dashboard is default dashboard.
     * 
     * @return A map of charts and its data in current dashboard
     * @throws DataAccessException if any data access exception
     * @throws BusinessRuleException if problem accessing chart data
     * @throws JSONException if accessing chart data
     */
    public String getChartData() throws DataAccessException,
        BusinessRuleException, JSONException {
        DateTime start = new DateTime();
        DateTime end = null;
        JSONObject chartData = new JSONObject();

        if (getDashboardId() != null && getChartId() != null) {
            JSONObject chartConfig = new JSONObject();
            JSONArray data = new JSONArray();

            JSONArray parentFilterFieldsArray = new JSONArray(getParentChartsFilterFields());

            Dashboard dashboard = null;
            Chart chart = null;
            try {
                dashboard = this.getDashboardManager().get(dashboardId);
                chart = this.getChartManager().get(chartId);
                if (!dashboardDetailManager.checkDashboardAndChartExist(
                    dashboard, chart)) {
                    throw new EntityNotFoundException(DashboardDetail.class,
                        null);
                }
            } catch (EntityNotFoundException enfe) {
                log.warn("Error while fetching chart data. Dashboard / chart may have been removed from system: "
                    + enfe.getLocalizedMessage());
                    
                setJsonMessage("message", getText(new UserMessage(
                    "dashboard.components.delete.error")), "errorCode",
                    ERROR_FAILURE);
                return ERROR;
            }
            JSONArray aggregatorData = null;
            try {
            aggregatorData = this.getChartManager()
                .executeChartDataAggregator(chart, dashboard.getTimeFilter());
            } catch (BusinessRuleException bre) {
                if (bre.getCause() instanceof DatabaseLockException) {
                    log.warn(
                        "Deadlock encountered when fetching chart data. Chart will not be refreshed.",
                        bre.getCause());
                    setJsonMessage("message", getText(new UserMessage(
                        "dashboard.components.delete.error")), "errorCode",
                        ERROR_NO_REPORT_FAILURE);
                    return ERROR;
                }
                throw bre;
            }
            DateTime date1 = new DateTime();
            chartConfig = this.getChartManager().getChartJSON(dashboard, chart, aggregatorData);
            DateTime date2 = new DateTime();
            log.trace("!!!" + chart.getName()
                + "!!!$$$Chart Config data time$$$@@@"
                + (date2.getMillis() - date1.getMillis()) + "@@@");

            chartData.put("config", chartConfig);

            if (parentFilterFieldsArray.length() > 0) {
                data = this.getChartManager().fetchDrillDownChartData(
                    dashboard, chart, parentFilterFieldsArray, aggregatorData);
            } else {
                data = this.getChartManager().getChartData(chart, aggregatorData);
            }

            chartData.put("data", data);
            end = new DateTime();
            log.trace("!!!" + chart.getName()
                + "!!!$$$Chart getData request calc time$$$@@@"
                + (end.getMillis() - start.getMillis()) + "@@@");
        }

        setJsonMessage("message", SUCCESS, "errorCode", ERROR_SUCCESS);
        setMessage(chartData.toString());
        return SUCCESS;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.SiteEnabledAction#getSiteList()
     */
    @Override
    public List<ListObject> getSiteList() throws DataAccessException {
        List<ListObject> list = new ArrayList<ListObject>();
        for (ListObject lo : super.getSiteList()) {
            if (!lo.getId().equals(Tag.ALL)) {
                list.add(lo);
            }
        }
        return list;
    }

    /**
     * Overridden setter for the setJsonMessage function.
     * @param statusKey Key value for the json object representing the status
     * @param statusValue Value attributed to key for the json object
     *            representing the status
     * @param returnKey Key value for the json object representing the return
     *            var
     * @param returnValue Value attributed to key for the json object
     *            representing the return var
     */
    public void setJsonMessage(String statusKey,
                               String statusValue,
                               String returnKey,
                               String returnValue) {
        JSONObject json = new JSONObject();
        try {
            json.put(statusKey, statusValue);
            json.put(returnKey, returnValue);
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        setJsonMessage(json.toString());
    }

    /**
     * . method to get jsonArray of all dashboards
     * @return json array of all the dashboards
     * @throws JSONException je
     */
    public JSONArray getDashboards() throws JSONException {
        JSONArray dashboardsJSONArray = new JSONArray();

        JSONObject dashboardJSONObj = new JSONObject();
        dashboardJSONObj.put("id", ResourceUtil
            .getLocalizedKeyValue("dashboard.dropdown.select.option.key"));
        dashboardJSONObj.put("name", ResourceUtil
            .getLocalizedKeyValue("dashboard.dropdown.select.option.value"));
        dashboardsJSONArray.put(dashboardJSONObj);

        for (Dashboard dashboardObj : getDashboardManager().listDashboards()) {
            JSONObject dashboardJSON = new JSONObject();
            dashboardJSON.put("id", dashboardObj.getId());
            dashboardJSON.put("name",
                getDataTranslation(dashboardObj.getName()));
            dashboardsJSONArray.put(dashboardJSON);
        }

        return dashboardsJSONArray;
    }

    /**
     * . method to see if the dashboard selected from the dashboard dropdown on
     * my dashboard exist in the system
     * @return error is dashboard has been deleted, success after redirecting to
     *         selected dashboard, if not
     * @throws DataAccessException dae
     */
    public String showDashboardForMyDashboard() throws DataAccessException {

        try {
            getDashboardManager().get(getDashboardId());
        } catch (EntityNotFoundException enfe) {
            log.warn("Error while fetching selected dashboard, it may have been removed from the system: "
                + enfe.getLocalizedMessage());
                
            setJsonMessage("message", getText(new UserMessage(
                "dashboard.selected.delete.error")), "errorCode", ERROR_FAILURE);
            return ERROR;
        }

        setJsonMessage("message", SUCCESS, "errorCode", ERROR_SUCCESS);
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 