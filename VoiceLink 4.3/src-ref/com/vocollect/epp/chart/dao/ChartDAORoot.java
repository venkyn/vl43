/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.chart.dao;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;

import java.util.List;

/**
 * @author mraj
 *
 */
public interface ChartDAORoot extends GenericDAO<Chart> {

    /**
     * Returns the Long of the id, or null if not found.
     * @param name - the name of the chart to look for
     * @throws DataAccessException - indicates database error
     * @return alert id or null
     */
    public Long uniquenessByName(String name)
        throws DataAccessException;
    
    /**
     * Method to count the number of associated dashbaords of the given chart.
     * @param chart - the chart
     * @return - number of dashbaords
     */
    public Long countAssociatedDashboards(Chart chart);
    
    /**
     * 
     * @return list of site specific charts
     */
    public List<Chart> listCharts();
    
    /**
     * Method to get the parent charts of the given dashboard.
     * @param dashboard - the dashboard
     * @return list of parent dashboards
     */
    public List<Chart> listParentCharts(Dashboard dashboard);
    
    /**.
     * Method to get child charts of a parent chart for a specific dashboard
     * @param dashboard dashboard selected
     * @param chart parent chart
     * @return list of child charts
     */    
    public List<Chart> listChildCharts(Dashboard dashboard, Chart chart);

    /**
     * @param dashboard - the dashboard
     * @return list of all charts which exist as parent in the dashboard
     */
    public List<Chart> listParentChartsForParentDropdown(Dashboard dashboard);
    
    /**
     * 
     * @param dashboard the dashboard
     * @return list of all charts which exist as child in the dashboard
     */
    public List<Chart> listChildChartsForParentDropdown(Dashboard dashboard);
    
    /**
     * Method to get the charts eligible to be drill down charts of the given dashboard and chart.
     * @param dashboard - the dashboard
     * @param chart - the selected parent chart
     * @return list of charts for drill down drop down
     */
    public List<Chart> listDrillDownChartsForDropdown(Dashboard dashboard, Chart chart);

    /**.
     * count the number of alerts by chart id
     * @param chartId id of chart
     * @return chart count
     * @throws DataAccessException dae
     */
    public Long countById(Long chartId) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 