/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.filter;

import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.util.TimeFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class provides implementation for Chart Drill down operation.
 * 
 * @author kudupi
 */
public abstract class ChartDrillDownFilterRoot implements DataAggregatorFilter {

    private JSONArray filterParameters;
    
    private static final Logger log = new Logger(ChartDrillDownFilterRoot.class);

    /**
     * Getter for the filterParameters property.
     * @return JSONArray value of the property
     */
    public JSONArray getFilterParameters() {
        return filterParameters;
    }

    /**
     * Setter for the filterParameters property.
     * @param filterParameters the new filterParameters value
     */
    public void setFilterParameters(JSONArray filterParameters) {
        this.filterParameters = filterParameters;
    }

    /**
     * {@inheritDoc}
     * @throws JSONException
     * @see com.vocollect.epp.evaluator.GenericFilterRoot#filter(java.lang.Object)
     */
    public JSONArray filter(JSONArray dataToFilter) throws JSONException {

        JSONArray filteredDrillDownChartData = new JSONArray();
        String filterKey = null;
        String filterValue = null;
        DAFieldType filterColumnFieldType = null;

        JSONArray filterParams = getFilterParameters();
        for (int i = 0; i < filterParams.length(); i++) {
            JSONObject filterJSONObject = new JSONObject(
                filterParams.getString(i));

            filterKey = (String) filterJSONObject.get("FILTER_KEY");
            filterValue = (String) filterJSONObject.get("FILTER_VALUE");
            filterColumnFieldType = DAFieldType
                .valueOf((String) filterJSONObject
                    .get("FILTER_COLUMN_DATATYPE"));
        }

        if ((dataToFilter.length() > 0)
            && (!StringUtil.isNullOrEmpty(filterKey))
            && (!StringUtil.isNullOrEmpty(filterValue))
            && (filterColumnFieldType != null)) {

            try {
                filteredDrillDownChartData = finderFunctionForDrillDown(
                    dataToFilter, filterKey, filterValue, filterColumnFieldType);
            } catch (BusinessRuleException bre) {
                log.warn(bre.getMessage());
            }

        }
        return filteredDrillDownChartData;
    }

    /**
     * Method to perform data filtering based on category key and category value
     * of Parent chart. This method will filter data of child chart using the
     * key and value provided by parent chart and returns the JSONArray response
     * of the child chart field which need to be drawn on Dashboard.
     * 
     * @param jsonArrayResponse JSONArray.
     * @param categoryKey String.
     * @param categoryValue String.
     * @param daFieldTypeChild DAFieldType.
     * @return child chart data JSONArray.
     * @throws BusinessRuleException .
     * @throws JSONException .
     */
    private JSONArray finderFunctionForDrillDown(JSONArray jsonArrayResponse,
                                                 String categoryKey,
                                                 String categoryValue,
                                                 DAFieldType daFieldTypeChild)
        throws JSONException, BusinessRuleException {

        JSONObject childJsonObject = null;
        JSONArray jsonArrayDrillDown = null;

        jsonArrayDrillDown = new JSONArray();
        try {
            for (int i = 0; i < jsonArrayResponse.length(); i++) {
                childJsonObject = new JSONObject(jsonArrayResponse.getString(i));

                // JSONObjects don't entertain null or empty string values, due
                // to which default string value records don't have the fields
                // for those values. This condition helps eliminate those
                // records for fetching values and comparing with the filter
                // field value because if not handled, the fetching of values
                // from such records throws exception.
                if (!childJsonObject.has(categoryKey)) {
                    continue;
                }
                
                switch (daFieldTypeChild) {
                case STRING:
                    String childValueString = childJsonObject.get(categoryKey)
                        .toString();
                    if (childValueString.equalsIgnoreCase(categoryValue)) {
                        jsonArrayDrillDown.put(childJsonObject);
                    }
                    break;
                case INTEGER:
                    Integer childValueInteger = Integer
                        .parseInt(childJsonObject.get(categoryKey).toString());
                    Integer parentValueInteger = Integer
                        .parseInt(categoryValue);
                    if (childValueInteger.equals(parentValueInteger)) {
                        jsonArrayDrillDown.put(childJsonObject);
                    }
                    break;
                case FLOAT:
                    Float childValueFloat = Float.parseFloat(childJsonObject
                        .get(categoryKey).toString());
                    Float parentValueFloat = Float.parseFloat(categoryValue);
                    if (childValueFloat.equals(parentValueFloat)) {
                        jsonArrayDrillDown.put(childJsonObject);
                    }
                    break;
                case DATE:
                    SimpleDateFormat sdf = new SimpleDateFormat(
                        "MM/dd/yy hh:mm:ss a z");

                    Date childValueDate = sdf.parse(childJsonObject.get(
                        categoryKey).toString());
                    Date parentValueDate = sdf.parse(categoryValue);

                    if (childValueDate.compareTo(parentValueDate) == 0) {
                        jsonArrayDrillDown.put(childJsonObject);
                    }
                    break;
                case TIME:
                    Integer childValueTime = TimeFormatter
                        .formatTimeStringToInteger(
                            childJsonObject.get(categoryKey).toString(),
                            ResourceUtil
                                .getLocalizedKeyValue("aggregator.field.time.value.default"),
                            GenericDataAggregator.DEFAULT_TIME_VAL);
                    Integer categoryValueInteger = TimeFormatter
                        .formatTimeStringToInteger(
                            categoryValue,
                            ResourceUtil
                                .getLocalizedKeyValue("aggregator.field.time.value.default"),
                            GenericDataAggregator.DEFAULT_TIME_VAL);
                    if (childValueTime.equals(categoryValueInteger)) {
                        jsonArrayDrillDown.put(childJsonObject);
                    }
                    break;
                default:
                    jsonArrayDrillDown = null;
                    break;
                }
            }
        } catch (Exception e) {
            throw new BusinessRuleException(e);
        }
        return jsonArrayDrillDown;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 