/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.model;

import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;
import java.util.Set;

/**
 * 
 * 
 * @author khazra
 */
public class UserChartPreferenceRoot extends BaseModelObject implements DataObject,
    Serializable, Taggable {

    /**
     * Serial Version ID.
     */
    private static final long serialVersionUID = 6309181399760028160L;

    private Dashboard dashboard;

    private Chart chart;

    private Float chartWidth;

    private Float chartHeight;
    
    private Float chartX;
    
    private Float chartY;
    
    private Set<Tag> tags;

    
    /**
     * Getter for the dashboard property.
     * @return Dashboard value of the property
     */
    public Dashboard getDashboard() {
        return dashboard;
    }

    /**
     * Setter for the dashboard property.
     * @param dashboard the new dashboard value
     */
    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    /**
     * Getter for the chart property.
     * @return Chart value of the property
     */
    public Chart getChart() {
        return chart;
    }

    /**
     * Setter for the chart property.
     * @param chart the new chart value
     */
    public void setChart(Chart chart) {
        this.chart = chart;
    }

    /**
     * Getter for the chartWidth property.
     * @return Float value of the property
     */
    public Float getChartWidth() {
        return chartWidth;
    }

    /**
     * Setter for the chartWidth property.
     * @param chartWidth the new chartWidth value
     */
    public void setChartWidth(Float chartWidth) {
        this.chartWidth = chartWidth;
    }

    /**
     * Getter for the chartHeight property.
     * @return Float value of the property
     */
    public Float getChartHeight() {
        return chartHeight;
    }

    /**
     * Setter for the chartHeight property.
     * @param chartHeight the new chartHeight value
     */
    public void setChartHeight(Float chartHeight) {
        this.chartHeight = chartHeight;
    }

    /**
     * @return the chartX
     */
    public Float getChartX() {
        return chartX;
    }

    
    /**
     * @param chartX the chartX to set
     */
    public void setChartX(Float chartX) {
        this.chartX = chartX;
    }

    
    /**
     * @return the chartY
     */
    public Float getChartY() {
        return chartY;
    }

    
    /**
     * @param chartY the chartY to set
     */
    public void setChartY(Float chartY) {
        this.chartY = chartY;
    }

    @Override
    public Set<Tag> getTags() {
        return this.tags;
    }
    
    @Override
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UserChartPreference)) {
            return false;
        }

        final UserChartPreferenceRoot other = (UserChartPreference) obj;

        if (this.dashboard.getId() == other.dashboard.getId()
            && this.chart.getId() == other.chart.getId()) {
            
            return true;
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.getId() == null ? 0 : this.getId().hashCode();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 