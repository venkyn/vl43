/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.model;

import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.model.VersionedModelObject;

/**
 * 
 * @author mraj
 *
 */
public class ChartDataFieldRoot extends VersionedModelObject  {

    /**
     * 
     */
    private static final long serialVersionUID = -81470488796067470L;

    private Chart chart;
    
    private DAColumn daColumn;
    
    
    /**
     * @return the chart
     */
    public Chart getChart() {
        return chart;
    }


    
    /**
     * @param chart the chart to set
     */
    public void setChart(Chart chart) {
        this.chart = chart;
    }


    /**
     * @return the daColumn
     */
    public DAColumn getDaColumn() {
        return daColumn;
    }

    
    /**
     * @param daColumn the daColumn to set
     */
    public void setDaColumn(DAColumn daColumn) {
        this.daColumn = daColumn;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChartDataField)) {
            return false;
        }
        final ChartDataField other = (ChartDataField) obj;
        if (getChart().equals(other.getChart()) && getDaColumn().equals(other.getDaColumn())) {
            return true;
        }
        return false;
    
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getChart().hashCode() + getDaColumn().hashCode());
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 