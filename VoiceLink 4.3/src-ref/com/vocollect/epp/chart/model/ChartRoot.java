/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.model;

import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.SortOrder;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.VersionedModelObject;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.Set;

/**
 * This class defines the Chart Model.
 * 
 * @author mraj
 */
public class ChartRoot extends VersionedModelObject implements DataObject,
    Serializable, Taggable {

    /**
     * Serial Version ID.
     */
    private static final long serialVersionUID = 889948190026496310L;

    private String name;

    private String description;

    private DAInformation daInformation;

    private DAColumn category;

    private Set<ChartDataField> chartDataFields;

    private ChartType type;

    private DAColumn pivotOn;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String pivotOnDisplayName;

    private DAColumn pivotFor;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String pivotForDisplayName;

    private DAColumn sortBy;

    // Used for displaying the Resolved name in Table Component.
    @SuppressWarnings("unused")
    private String sortByDisplayName;
    
    private SortOrder sortOrder;

    private Set<DashboardDetail> dashboardDetails;

    private Set<Tag> tags;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the daInformation property.
     * @return DAInformation value of the property
     */
    public DAInformation getDaInformation() {
        return daInformation;
    }

    /**
     * Setter for the daInformation property.
     * @param daInformation the new daInformation value
     */
    public void setDaInformation(DAInformation daInformation) {
        this.daInformation = daInformation;
    }

    /**
     * Getter for the daColumn property.
     * @return DAColumn value of the property
     */
    public DAColumn getCategory() {
        return category;
    }

    /**
     * Setter for the daColumn property.
     * @param category the new daColumn value
     */
    public void setCategory(DAColumn category) {
        this.category = category;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * @return the chartDataFields
     */
    public Set<ChartDataField> getChartDataFields() {
        return chartDataFields;
    }

    /**
     * @param chartDataFields the chartDataFields to set
     */
    public void setChartDataFields(Set<ChartDataField> chartDataFields) {
        this.chartDataFields = chartDataFields;
    }

    /**
     * Getter for the chartType property.
     * @return ChartType value of the property
     */
    public ChartType getType() {
        return type;
    }

    /**
     * Setter for the chartType property.
     * @param type the new chartType value
     */
    public void setType(ChartType type) {
        this.type = type;
    }

    /**
     * Getter for the pivotOn property.
     * @return DAColumn value of the property
     */
    public DAColumn getPivotOn() {
        return pivotOn;
    }

    /**
     * Setter for the pivotOn property.
     * @param pivotOn the new pivotOn value
     */
    public void setPivotOn(DAColumn pivotOn) {
        this.pivotOn = pivotOn;
    }

    /**
     * Getter for the pivotOn property.
     * @return String value of the property
     */
    public String getPivotOnDisplayName() {
        if (this.getPivotOn() != null) {
            return ResourceUtil.getLocalizedKeyValue(this.getPivotOn()
                .getDisplayName());
        } else {
            return ResourceUtil.getLocalizedKeyValue("chart.field.value.none");
        }
    }

    /**
     * Getter for the pivotFor property.
     * @return DAColumn value of the property
     */
    public DAColumn getPivotFor() {
        return pivotFor;
    }

    /**
     * Setter for the pivotFor property.
     * @param pivotFor the new pivotFor value
     */
    public void setPivotFor(DAColumn pivotFor) {
        this.pivotFor = pivotFor;
    }

    /**
     * Getter for the pivotFor property.
     * @return String value of the property
     */
    public String getPivotForDisplayName() {
        if (this.getPivotFor() != null) {
            return ResourceUtil.getLocalizedKeyValue(this.getPivotFor()
                .getDisplayName());
        } else {
            return ResourceUtil.getLocalizedKeyValue("chart.field.value.none");
        }
    }

    /**
     * Getter for the sortBy property.
     * @return DAColumn value of the property
     */
    public DAColumn getSortBy() {
        return sortBy;
    }

    /**
     * Setter for the sortBy property.
     * @param sortBy the new sortBy value
     */
    public void setSortBy(DAColumn sortBy) {
        this.sortBy = sortBy;
    }

    /**
     * Getter for the sortOrder property.
     * @return ChartSortOrder value of the property
     */
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Setter for the sortOrder property.
     * @param sortOrder the new sortOrder value
     */
    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    /**
     * Getter for the pivotFor property.
     * @return String value of the property
     */
    public String getSortByDisplayName() {
        if (this.getSortBy() != null) {
            return ResourceUtil.getLocalizedKeyValue(this.getSortBy()
                .getDisplayName());
        } else {
            return ResourceUtil.getLocalizedKeyValue("chart.field.value.none");
        }
    }

    /**
     * @return the dashboardDetails
     */
    public Set<DashboardDetail> getDashboardDetails() {
        return dashboardDetails;
    }

    /**
     * @param dashboardDetails the dashboardDetails to set
     */
    public void setDashboardDetails(Set<DashboardDetail> dashboardDetails) {
        this.dashboardDetails = dashboardDetails;
    }

    /**
     * Getter for the tags property.
     * @return Set<Tag> value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Chart)) {
            return false;
        }
        final ChartRoot other = (Chart) obj;
        if (getName() != other.getName()) {
            return false;
        }
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return this.name == null ? 0 : this.name.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 