/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.service;

import com.vocollect.epp.chart.dao.ChartDAO;
import com.vocollect.epp.chart.filter.ChartDrillDownFilter;
import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.ChartDataField;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.dataaggregator.util.DAUtils;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.sort.JSONArraySorter;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.util.DisplayUtilities;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public abstract class ChartManagerImplRoot extends
    GenericManagerImpl<Chart, ChartDAO> implements ChartManager {
    
    private static final Logger log = new Logger(ChartManagerImplRoot.class);

    private DataAggregatorHandler dataAggregatorHandler;

    private DAColumnManager daColumnManager;

    private DashboardDetailManager dashboardDetailManager;

    private JSONArraySorter jsonArraySorter;
    
    private static final String[] AGGREGATOR_WITH_NO_FILTER = {
        "OperatorInfo", "InfiniteRegionDataAggregator", "RouteOtherStatus",
        "ProxyDataAggregator" };

    /**
     * Getter for the dashboardDetailManager property.
     * @return DashboardDetailManager value of the property
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    /**
     * Setter for the dashboardDetailManager property.
     * @param dashboardDetailManager the new dashboardDetailManager value
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }

    private ChartDrillDownFilter chartDrillDownFilter;

    /**
     * @param primaryDAO - the primary DAO
     */
    public ChartManagerImplRoot(ChartDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * Getter for the dataAggregatorHandler property.
     * @return DataAggregatorHandler value of the property
     */
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * Setter for the dataAggregatorHandler property.
     * @param dataAggregatorHandler the new dataAggregatorHandler value
     */
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * Getter for the chartDrillDownFilter property.
     * @return ChartDrillDownFilter value of the property
     */
    public ChartDrillDownFilter getChartDrillDownFilter() {
        return chartDrillDownFilter;
    }

    /**
     * Setter for the chartDrillDownFilter property.
     * @param chartDrillDownFilter the new chartDrillDownFilter value
     */
    public void setChartDrillDownFilter(ChartDrillDownFilter chartDrillDownFilter) {
        this.chartDrillDownFilter = chartDrillDownFilter;
    }

    /**
     * Getter for the jsonArraySorter property.
     * @return JSONArraySorter value of the property
     */
    public JSONArraySorter getJsonArraySorter() {
        return jsonArraySorter;
    }

    /**
     * Setter for the jsonArraySorter property.
     * @param jsonArraySorter the new jsonArraySorter value
     */
    public void setJsonArraySorter(JSONArraySorter jsonArraySorter) {
        this.jsonArraySorter = jsonArraySorter;
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#listCharts()
     */
    public List<Chart> listCharts() {
        return this.getPrimaryDAO().listCharts();
    }

    @Override
    public List<Chart> listParentCharts(Dashboard dashboard) {
        return this.getPrimaryDAO().listParentCharts(dashboard);
    }

    @Override
    public List<Chart> listChildCharts(Dashboard dashboard, Chart chart) {
        return this.getPrimaryDAO().listChildCharts(dashboard, chart);
    }

    @Override
    public List<Chart> listParentChartsForParentDropdown(Dashboard dashboard) {
        return this.getPrimaryDAO()
            .listParentChartsForParentDropdown(dashboard);
    }

    @Override
    public List<Chart> listChildChartsForParentDropdown(Dashboard dashboard) {
        return this.getPrimaryDAO().listChildChartsForParentDropdown(dashboard);
    }

    @Override
    public List<Chart> listDrillDownChartsForDropdown(Dashboard dashboard,
                                                      Chart chart) {
        return this.getPrimaryDAO().listDrillDownChartsForDropdown(dashboard,
            chart);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#getChartsInDashboard(com.vocollect.epp.dashboard.model.Dashboard)
     */
    public Set<Chart> getChartsInDashboard(Dashboard dashboard) {
        Set<Chart> chartsInDashboard = new HashSet<Chart>();
        List<Chart> allParentCharts = this
            .listParentChartsForParentDropdown(dashboard);
        List<Chart> allChildCharts = this
            .listChildChartsForParentDropdown(dashboard);
        chartsInDashboard.addAll(allParentCharts);
        chartsInDashboard.addAll(allChildCharts);
        return chartsInDashboard;
    }

    /**
     * Implementation to check to see if the chart name is unique.
     * @param chart the chart to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    public void verifyUniquenessByName(Chart chart)
        throws BusinessRuleException, DataAccessException {
        boolean isNew = chart.isNew();
        // Uniqueness check for name
        Long nameUniquenessId = getPrimaryDAO().uniquenessByName(
            chart.getName());
        if (nameUniquenessId != null
            && (isNew || (!isNew && nameUniquenessId.longValue() != chart
                .getId().longValue()))) {
            throw new FieldValidationException("chart.name", chart.getName(),
                new UserMessage("chart.error.duplicateName", chart.getName()));
        }

    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.alert.service.ChartManagerRoot#countById(java.lang.Long)
     */
    @Override
    public Long countById(Long chartId) throws DataAccessException {
        return this.getPrimaryDAO().countById(chartId);
    }
    
    
    /**
     * Pass this call on to the delete(object) method. {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Long)
     */
    @Override
    public Object delete(Long id) throws BusinessRuleException,
        DataAccessException {
        Chart chart = get(id);
        return delete(chart);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.impl.GenericManagerImpl#delete(java.lang.Object)
     */
    @Override
    public Object delete(Chart chart) throws BusinessRuleException,
        DataAccessException {

        if (this.getPrimaryDAO().countAssociatedDashboards(chart) == 0) {
            return super.delete(chart);
        } else {
            throw new DataAccessException(
                SystemErrorCode.ENTITY_NOT_DELETEABLE, new UserMessage(
                    "chart.delete.error.dashboard.inUse", chart.getName()),
                null);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#getChartData()
     */
    @Override
    public JSONArray getChartData(Chart chart, JSONArray aggregatorData) throws BusinessRuleException,
        JSONException {
        JSONArray data = aggregatorData;
        DateTime date1 = new DateTime();
        data = sortData(chart, data);
        DateTime date2 = new DateTime();
        log.trace("!!!" + chart.getName()
            + "!!!$$$Parent Data Sorting time$$$@@@"
            + (date2.getMillis() - date1.getMillis()) + "@@@");
        data = pivotData(chart, data);
        DateTime date3 = new DateTime();
        log.trace("!!!" + chart.getName()
            + "!!!$$$Parent Data Pivoting time$$$@@@"
            + (date3.getMillis() - date2.getMillis()) + "@@@");
        return data;
    }

    /**
     * method to sort chart data based on sort by and sort type field values of
     * chart.
     * @param chart chart whose data is to be sorted
     * @param jsonData data to sort
     * @return JSONArray sorted jsonarray
     * @throws JSONException jse
     */
    protected JSONArray sortData(Chart chart, JSONArray jsonData)
        throws JSONException {

        if ((chart.getSortBy() == null) || (jsonData == null)) {
            return jsonData;
        }

        jsonArraySorter.setDaColumn(chart.getSortBy());
        jsonArraySorter.setSortOrder(chart.getSortOrder());
        jsonArraySorter.setJsonArrayData(jsonData);

        return jsonArraySorter.sortData();
    }

    /**
     * Formats any date and time value. After formatting the date and time
     * changes to string
     * @param data the json data to format
     * @param columns the column information of the data
     * @return formatted data
     * @exception JSONException if parsing json data
     * @exception BusinessRuleException if problem formatting the date
     */
    protected JSONArray formatDateTime(JSONArray data, JSONArray columns)
        throws JSONException, BusinessRuleException {

        List<String> dateColumns = new LinkedList<String>();
        List<String> timeColumns = new LinkedList<String>();
        DisplayUtilities du = new DisplayUtilities();

        // Get all the date columns
        for (int i = 0; i < columns.length(); i++) {
            JSONObject column = columns.getJSONObject(i);

            String columnType = column
                .getString(GenericDataAggregator.FIELD_TYPE);

            if (columnType.equals(DAFieldType.DATE.name())) {
                dateColumns.add(column
                    .getString(GenericDataAggregator.FIELD_ID));
            }

            if (columnType.equals(DAFieldType.TIME.name())) {
                timeColumns.add(column
                    .getString(GenericDataAggregator.FIELD_ID));
            }
        }

        // Format the date column
        for (int i = 0; i < data.length(); i++) {
            JSONObject record = data.getJSONObject(i);
            for (String dateColumn : dateColumns) {
                Date dateValue = (Date) record.get(dateColumn);
                try {
                    String strDateValue = du.formatTimeWithTimeZone(dateValue,
                        null);
                    record.put(dateColumn, strDateValue);
                } catch (DataAccessException e) {
                    throw new BusinessRuleException(e);
                }
            }

            for (String timeColumn : timeColumns) {
                Object chartTimeFieldValue = record.get(timeColumn);

                Integer chartTimeIntegerValue = (Integer) chartTimeFieldValue;

                if (chartTimeIntegerValue == GenericDataAggregator.DEFAULT_TIME_VAL) {
                    String strTimeValue = ResourceUtil.getLocalizedKeyValue("aggregator.field.time.value.default");
                    record.put(timeColumn, strTimeValue);
                } else {
                    String strTimeValue = String.format("%02d:%02d",
                        chartTimeIntegerValue / 100,
                        chartTimeIntegerValue % 100);
                    record.put(timeColumn, strTimeValue);
                }
            }
        }

        return data;
    }

    /**
     * Method to extract data to be plotted as part of drill down chart. This
     * method will return a JSONArray data of child chart in a filtered manner
     * based on the Parent Chart selected and the Parent chart category.
     * 
     * @param dashboard <code>Dashboard</code>.
     * @param childChartToDraw <code>Chart</code>.
     * @param drillDownFilterParameters JSONArray.
     * @param aggregatorData data returned by aggregator of selected chart
     * @return jsonArrayResponse JSONArray.
     * @throws JSONException .
     * @throws BusinessRuleException .
     * @throws DataAccessException .
     */
    public JSONArray fetchDrillDownChartData(Dashboard dashboard,
                                             Chart childChartToDraw,
                                             JSONArray drillDownFilterParameters, JSONArray aggregatorData)
        throws JSONException, BusinessRuleException, DataAccessException {

        DateTime date1 = new DateTime();
        JSONArray dataToFilter = aggregatorData;
        DAColumn daColumnChildChart = null;
        DAInformation daInformationChildChart = childChartToDraw
            .getDaInformation();

        int filterParametersLength = drillDownFilterParameters.length();

        for (int i = 0; i < filterParametersLength; i++) {
            String categoryKey = null;
            JSONObject drillDownFilterParam = (JSONObject) drillDownFilterParameters
                .get(i);

            if ((filterParametersLength >= 1)
                && (i == filterParametersLength - 1)) {
                // Fetching linking key for linked parent chart.
                Chart parentChart = this.get(drillDownFilterParam
                    .getLong("chartid"));
                daColumnChildChart = getDashboardDetailManager()
                    .findLinkedDrillDownField(dashboard, parentChart,
                        childChartToDraw);
                categoryKey = daColumnChildChart.getFieldId();
            } else {
                // Fetching linking key for non-linked parent charts.
                categoryKey = drillDownFilterParam.getString("fieldid");
                daColumnChildChart = getDaColumnManager()
                    .findDAColumnByFieldId(daInformationChildChart, categoryKey);
            }

            // If the value of the category field selected is empty then it
            // doesn't come as part of filterParameter JSONObject because
            // JSONObject doesn't allow null or empty strings. This condition
            // helps avoiding exceptions in that scenario. 
            String categoryValue = null;
            if (drillDownFilterParam.has("fieldvalue")) {
                categoryValue = drillDownFilterParam.getString("fieldvalue");
            } else {
                return new JSONArray();
            }
            
            // CategoryKey is not found. So returning empty array.
            if (daColumnChildChart == null) {
                return new JSONArray();
            }

            JSONArray filterParamJSONArray = new JSONArray();
            JSONObject filterParameters = new JSONObject();
            filterParameters.put("FILTER_KEY", categoryKey);
            filterParameters.put("FILTER_VALUE", categoryValue);
            filterParameters.put("FILTER_COLUMN_DATATYPE",
                daColumnChildChart.getFieldType());

            filterParamJSONArray.put(filterParameters);

            getChartDrillDownFilter().setFilterParameters(filterParamJSONArray);

            dataToFilter = getChartDrillDownFilter().filter(dataToFilter);
        }
        DateTime date2 = new DateTime();
        log.trace("!!!" + childChartToDraw.getName()
            + "!!!$$$Drill down filter time$$$@@@"
            + (date2.getMillis() - date1.getMillis()) + "@@@");
        dataToFilter = sortData(childChartToDraw, dataToFilter);
        DateTime date3 = new DateTime();
        log.trace("!!!" + childChartToDraw.getName()
            + "!!!$$$Drill down sorting time$$$@@@"
            + (date3.getMillis() - date2.getMillis()) + "@@@");
        dataToFilter = pivotData(childChartToDraw, dataToFilter);
        DateTime date4 = new DateTime();
        log.trace("!!!" + childChartToDraw.getName()
            + "!!!$$$Drill down pivoting time$$$@@@"
            + (date4.getMillis() - date3.getMillis()) + "@@@");

        return dataToFilter;
    }

    @Override
    public JSONObject getChartJSON(Dashboard dashboard, Chart chart, JSONArray aggregatorData)
        throws JSONException {
        JSONObject chartJSON = new JSONObject();
        chartJSON.put("id", chart.getId());
        chartJSON.put("name", chart.getName());
        chartJSON.put("description", chart.getDescription());
        chartJSON.put("type", chart.getType());

        JSONObject cat = new JSONObject();
        cat.put("name", chart.getCategory().getDaColumnDisplayName());
        cat.put("fieldId", chart.getCategory().getFieldId());
        chartJSON.put("category", cat);

        JSONArray fields = new JSONArray();

        Set<ChartDataField> chartFields = chart.getChartDataFields();
        Set<DAColumn> chartFieldColumns = new HashSet<DAColumn>();
        for (ChartDataField chartField : chartFields) {
            chartFieldColumns.add(chartField.getDaColumn());
        }

        List<DAColumn> sequencedColumns = getDaColumnManager()
            .listSequencedDAColumnByFieldType(chart.getDaInformation(),
                DAUtils.getFieldTypes(false));

        for (DAColumn sequencedColumn : sequencedColumns) {
            if (chartFieldColumns.contains(sequencedColumn)) {
                JSONObject obj = new JSONObject();
                obj.put("fieldId", sequencedColumn.getFieldId());
                obj.put("fieldDisplayname",
                    sequencedColumn.getDaColumnDisplayName());
                obj.put("fieldUom", sequencedColumn.getUomDisplayName());
                fields.put(obj);
            }
        }

        // If chart is pivoted get the pivot column information
        Object[] pivotColumns = {};
        if (chart.getPivotOn() != null) {
            pivotColumns = getPivotColumnNames(chart.getPivotOn(), aggregatorData);
        }

        for (Object pivotColumn : pivotColumns) {
            JSONObject obj = new JSONObject();
            obj.put("fieldId", pivotColumn + "");
            obj.put("fieldDisplayname", pivotColumn);
            obj.put("fieldUom", chart.getPivotOn().getUom());
            fields.put(obj);
        }

        chartJSON.put("fields", fields);

        List<Chart> childCharts = this.listChildCharts(dashboard, chart);
        JSONArray childChartIds = new JSONArray();

        if (!childCharts.isEmpty()) {
            for (Chart childChart : childCharts) {
                JSONObject childChartId = new JSONObject();

                childChartId.put("id", childChart.getId());

                childChartIds.put(childChartId);
            }
        }

        chartJSON.put("childChartIds", childChartIds);
        
        if ((dashboard != null)
            && (dashboard.getTimeFilter() != TimeWindowFilterType.NO_FILTER)) {
            String daName = chart.getDaInformation().getName();

            if (!Arrays.asList(AGGREGATOR_WITH_NO_FILTER).contains(daName)) {
                chartJSON.put("timeFilter", ResourceUtil
                    .getLocalizedEnumName(dashboard.getTimeFilter()));
            }

        }

        return chartJSON;
    }

    /**
     * Method to pivot the data depending on chart configuration.
     * @param chart the chart for configuration
     * @param jsonData the base json data
     * @return pivoted json data
     * @throws JSONException when problem pivoting the data provided
     */
    protected JSONArray pivotData(Chart chart, JSONArray jsonData)
        throws JSONException {

        // If the chart is not pivot chart, return the same json
        if (chart.getPivotOn() == null || jsonData == null) {
            return jsonData;
        }

        // Collect group by information
        List<String> groupBy = new LinkedList<String>();
        groupBy.add(chart.getCategory().getFieldId());

        for (ChartDataField dataField : chart.getChartDataFields()) {
            groupBy.add(dataField.getDaColumn().getFieldId());
        }

        // Build the pivot column names transforming row to column
        Object[] pivotColumnNames = getPivotColumnNames(chart.getPivotOn(),
            jsonData);
        Map<String, Map<Object, Object>> uniqueValueMaps = getUniqueValuesForGroupBy(
            groupBy, jsonData);

        // Build the data set depending on the group by and pivoted column
        for (String key : uniqueValueMaps.keySet()) {
            Map<Object, Object> uniueValueMap = uniqueValueMaps.get(key);
            for (Object pivotColumnName : pivotColumnNames) {
                Object value = getPivotColumnValue(key, groupBy,
                    pivotColumnName, chart.getPivotFor(), chart.getPivotOn(),
                    jsonData);

                uniueValueMap.put(pivotColumnName, value);
            }
        }

        // Map it to json
        JSONArray valueJSON = mapToJSON(uniqueValueMaps.values());

        return valueJSON;
    }

    /**
     * Transforms a map to a json.
     * @param records the collection of map
     * @return json array of the collection provided
     * @exception JSONException when there is a problem transforming the json
     */
    private JSONArray mapToJSON(Collection<Map<Object, Object>> records)
        throws JSONException {
        JSONArray jsonObjectArray = new JSONArray();

        for (Map<Object, Object> record : records) {
            JSONObject jsonObject = new JSONObject(true);
            for (Object key : record.keySet()) {
                jsonObject.put(key.toString(), record.get(key));
            }
            jsonObjectArray.put(jsonObject);
        }

        return jsonObjectArray;
    }

    /**
     * Builds the pivot column value from the provided information.
     * @param key index key build from the data of group by column
     * @param groupByColumns the group by columns to identify the right record
     * @param pivotColumnName the pivot column name after row to column rotation
     * @param pivotOn on which data aggregator column we are pivoting
     * @param pivotFor which is value we are going to display
     * @param jsonData The json data to parse
     * @exception JSONException when problem parsing json data
     * @return the value for the specified parameters
     */
    protected Object getPivotColumnValue(String key,
                                         List<String> groupByColumns,
                                         Object pivotColumnName,
                                         DAColumn pivotFor,
                                         DAColumn pivotOn,
                                         JSONArray jsonData)
        throws JSONException {

        for (int i = 0; i < jsonData.length(); i++) {
            String createdKey = "";
            for (String groupByColumn : groupByColumns) {
                createdKey = createdKey
                    + jsonData.getJSONObject(i).get(groupByColumn);
            }

            if (key.equals(createdKey)) {
                if (jsonData.getJSONObject(i).get(pivotOn.getFieldId())
                    .equals(pivotColumnName)) {
                    return jsonData.getJSONObject(i).get(pivotFor.getFieldId());
                }
            }
        }

        return new String();
    }

    /**
     * Creates a unique map of key and value for the group by columns.
     * @param groupByColumns the group by columns
     * @param jsonData json data to parse
     * @return the unique map
     * @throws JSONException when problem parsing data
     */
    protected Map<String, Map<Object, Object>> getUniqueValuesForGroupBy(List<String> groupByColumns,
                                                                         JSONArray jsonData)
        throws JSONException {

        Map<String, Map<Object, Object>> uniqueValueMaps = new LinkedHashMap<String, Map<Object, Object>>();

        for (int i = 0; i < jsonData.length(); i++) {

            String key = "";
            Map<Object, Object> uniqueValueMap = new LinkedHashMap<Object, Object>();
            for (String groupByColum : groupByColumns) {
                Object value = jsonData.getJSONObject(i).get(groupByColum);
                uniqueValueMap.put(groupByColum,
                    jsonData.getJSONObject(i).get(groupByColum));
                key = key + value.toString();
            }

            if (uniqueValueMap.get(key) == null) {
                uniqueValueMaps.put(key, uniqueValueMap);
            }
        }

        return uniqueValueMaps;
    }

    /**
     * Creates the pivot columns.
     * @param pivotOn the column name to consider
     * @param jsonData data to parse
     * @return the columns in array
     * @throws JSONException when problem parsing json data
     */
    protected Object[] getPivotColumnNames(final DAColumn pivotOn,
                                           JSONArray jsonData)
        throws JSONException {

        // Create sorted set with custom comparator
        Set<Object> columns = new LinkedHashSet<Object>(jsonData.length());

        for (int i = 0; i < jsonData.length(); i++) {
            columns.add(jsonData.getJSONObject(i).get(pivotOn.getFieldId()));
        }

        return columns.toArray();
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#executeChartDataAggregator(com.vocollect.epp.chart.model.Chart,
     *      com.vocollect.epp.model.TimeWindowFilterType)
     */
    @Override
    public JSONArray executeChartDataAggregator(Chart chart, TimeWindowFilterType timeFilter)
        throws JSONException, BusinessRuleException {
        DateTime date1 = new DateTime();
        DAInformation daInformation = chart.getDaInformation();

        GenericDataAggregator dataAggregator = this.getDataAggregatorHandler()
            .getDataAggregatorByName(daInformation.getName());

        
        Date startTime = null;
        Date endTime = null;
        
        if (daInformation.getName().equals("OperatorInfo")) {
            endTime = new Date();
            DateTime startTimeValue = new DateTime(endTime);
            startTime = startTimeValue.minusHours(24).toDate();
        } else if ((timeFilter != null)
            && (timeFilter != TimeWindowFilterType.NO_FILTER)) {
            int filterValue = timeFilter.toValue();
            
            startTime = DateUtil.convertTimeToSiteTime(new Date());
            DateTime endTimeValue = new DateTime(startTime);
            endTime = endTimeValue.plusHours(filterValue).toDate();
        }
        
        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        
        if (site == null) {
            log.warn("Site was not found for tag ID ");
            throw new RuntimeException();
        }

        JSONObject parameters = new JSONObject();
        parameters.put(
            GenericDataAggregator.START_TIME_PARAMETER,
            startTime);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER,
            endTime);
        parameters.put(GenericDataAggregator.SITE_PARAMETER, site);

        dataAggregator.setParameters(parameters);
        
        DateTime date2 = new DateTime();
        JSONArray data = dataAggregator.execute();
        DateTime date3 = new DateTime();
        log.trace("!!!" + chart.getName() + "!!!$$$Data aggregator time$$$@@@"
            + (date3.getMillis() - date2.getMillis()) + "@@@");
        
        data = formatDateTime(data, dataAggregator.getAllOutputColumns());
        DateTime date4 = new DateTime();
        log.trace("!!!" + chart.getName()
            + "!!!$$$Formatting Date Time field values time$$$@@@"
            + (date4.getMillis() - date3.getMillis()) + "@@@");
        log.trace("!!!" + chart.getName()
            + "!!!$$$Data aggregator executor method time$$$@@@"
            + (date4.getMillis() - date1.getMillis()) + "@@@");
        return data;
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#executeChartDataAggregator(com.vocollect.epp.chart.model.Chart)
     */
    @Override
    public JSONArray executeChartDataAggregator(Chart chart)
        throws JSONException, BusinessRuleException {
        return executeChartDataAggregator(chart, null);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.chart.service.ChartManagerRoot#forceDelete(com.vocollect.epp.chart.model.Chart)
     */
    @Override
    public void forceDelete(Chart chart) throws BusinessRuleException,
        DataAccessException {
        // first remove all dashboard details which has reference to the chart
        // to be deleted
        this.getDashboardDetailManager().removeAllDependentDetails(chart);

        // fresh retrieve chart and delete. All detail records should be deleted
        // by now.
        SiteContextHolder.getSiteContext().setFilterBySite(false);
        Chart ch = this.get(chart.getId());
        SiteContextHolder.getSiteContext().setFilterBySite(true);
        this.delete(ch);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 