/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.service;

import com.vocollect.epp.chart.dao.ChartDAO;
import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public interface ChartManagerRoot extends GenericManager<Chart, ChartDAO>,
    RemovableDataProvider {

    /**
     * Implementation to check to see if the name is unique.
     * @param chart the chart to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    void verifyUniquenessByName(Chart chart) throws BusinessRuleException,
        DataAccessException;

    /**
     * @param chart the chart for which data needs to be retrieved
     * @param aggregatorData data returned by aggregator of the selected chart
     * @return Data from the data aggregator associated to this chart
     * @throws BusinessRuleException if problem getting aggregator data
     * @throws JSONException If problem setting time window parameter
     */
    public JSONArray getChartData(Chart chart, JSONArray aggregatorData) throws BusinessRuleException,
        JSONException;
    
    /**
     * 
     * @return list of site specific charts
     */
    public List<Chart> listCharts();
    
    /**
     * Method to get the parent charts of the given dashboard.
     * @param dashboard - the dashboard
     * @return list of parent dashboards
     */
    public List<Chart> listParentCharts(Dashboard dashboard);
    
    /**.
     * Method to get child charts of a parent chart for a specific dashboard
     * @param dashboard dashboard selected
     * @param chart parent chart
     * @return list of child charts
     */
    public List<Chart> listChildCharts(Dashboard dashboard, Chart chart);

    /**
     * @param dashboard - the dashboard
     * @return list of all charts which exist as parent in the dashboard
     */
    public List<Chart> listParentChartsForParentDropdown(Dashboard dashboard);
    
    /**
     * 
     * @param dashboard the dashboard
     * @return list of all charts which exist as child in the dashboard
     */
    public List<Chart> listChildChartsForParentDropdown(Dashboard dashboard);
    
    /**
     * Method to get the charts eligible to be drill down charts of the given dashboard and chart.
     * @param dashboard - the dashboard
     * @param chart - the selected parent chart
     * @return list of charts for drill down drop down
     */
    public List<Chart> listDrillDownChartsForDropdown(Dashboard dashboard, Chart chart);
    
    /**
     * 
     * @param dashboard dashboard
     * @return set of all the charts in the dashboard including both parent and child charts
     */
    public Set<Chart> getChartsInDashboard(Dashboard dashboard);
    
    /**
     * Method to extract data to be plotted as part of drill down chart. This
     * method will return a JSONArray data of child chart.
     * 
     * @param dashboard Dashboard.
     * @param childChartToDraw Chart.
     * @param drillDownFilterParameters JSONArray.
     * @param aggregatorData data returned by aggregator of selected chart
     * @return jsonArrayResponse JSONArray.
     * @throws BusinessRuleException .
     * @throws JSONException .
     * @throws DataAccessException .
     */
    public JSONArray fetchDrillDownChartData(Dashboard dashboard, Chart childChartToDraw,
                                             JSONArray drillDownFilterParameters, JSONArray aggregatorData)
       throws BusinessRuleException, JSONException, DataAccessException;
    
    /**
     * Method to serialize chart object to JSON.
     * @param dashboard dashboard to which the chart belongs
     * @param chart - the chart to be serialized
     * @param aggregatorData data returned by aggregator of selected chart
     * @return - the serialized chart object
     * @throws JSONException - jse
     */
    public JSONObject getChartJSON(Dashboard dashboard, Chart chart, JSONArray aggregatorData) throws JSONException;
    
    
    /**
     * Method to delete chart, but first removing all its references in the
     * system. For now only the dashboard details are being removed prior to
     * attempting delete on chart. IF there are any more reference to chart
     * added in future, they need to be handled or documented here for exceptions.
     * @param chart - the chart to be deleted
     * @throws BusinessRuleException - thrown when business exception encountered
     * @throws DataAccessException - thrown when data exception encountered
     */
    public void forceDelete(Chart chart) throws BusinessRuleException, DataAccessException;

    /**
     * Executes the data aggregator specified in the chart.
     * @param chart the chart whose data aggregator needs to be executed
     * @param timeFilter time filter for chart data
     * @return the data retrieved from the aggregator
     * @throws JSONException when problem extracting data from aggregator
     * @throws BusinessRuleException when problem accessing chart information
     */
    public JSONArray executeChartDataAggregator(Chart chart, TimeWindowFilterType timeFilter)
        throws JSONException, BusinessRuleException;

    /**
     * Executes the data aggregator specified in the chart.
     * @param chart the chart whose data aggregator needs to be executed
     * @return the data retrieved from the aggregator
     * @throws JSONException when problem extracting data from aggregator
     * @throws BusinessRuleException when problem accessing chart information
     */
    public JSONArray executeChartDataAggregator(Chart chart)
        throws JSONException, BusinessRuleException;

    /**.
     * count the number of charts by chart id
     * @param chartId id of chart
     * @return chart count
     * @throws DataAccessException dae
     */
    public Long countById(Long chartId) throws DataAccessException;
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 