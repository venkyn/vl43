/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.event.listeners;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.ChartDataField;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dataaggregator.event.DAUpdateEvent;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAEventType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationListener;

/**
 * @author mraj
 * 
 */
public class ChartDAEventListenerRoot implements
    ApplicationListener<DAUpdateEvent> {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(ChartDAEventListenerRoot.class);

    private NotificationManager notificationManager;

    private DashboardDetailManager dashboardDetailManager;

    private ChartManager chartManager;

    private SiteContext siteContext;

    private static final DAColumnState[] COLUMN_STATES = {
        DAColumnState.MARKEDFORDELETE, DAColumnState.FIELDTYPEUPDATED };
        
        

    private List<DAColumnState> getDAColumnStates() {
        return Arrays.asList(COLUMN_STATES);
    }
    
    /**
     * @return the notificationManager
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * @param notificationManager the notificationManager to set
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the dashboardDetailManager property.
     * @return DashboardDetailManager value of the property
     */
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    /**
     * Setter for the dashboardDetailManager property.
     * @param dashboardDetailManager the new dashboardDetailManager value
     */
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }

    /**
     * @return the chartManager
     */
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * @param chartManager the chartManager to set
     */
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * @return the siteContext
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * @param siteContext the siteContext to set
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    @Override
    public void onApplicationEvent(DAUpdateEvent event) {
        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());

        log.info("Event received : " + event.getSource());
        try {
            if (((DAEventType) event.getSource()) == DAEventType.UPDATED) {
                handleChartDaInformationUpdate(event.getNewDAInformation());
            } else {
                handleChartDaInformationDelete(event.getNewDAInformation());
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (BusinessRuleException e) {
            e.printStackTrace();
        }

    }

    /**
     * Method to identify charts to delete configured using deleted DA.
     * @param daInformationMarkedForDelete da which has been marked for delete
     * @throws DataAccessException - dae
     * @throws BusinessRuleException - bre
     */
    private void handleChartDaInformationDelete(DAInformation daInformationMarkedForDelete)
        throws DataAccessException, BusinessRuleException {

        List<Chart> daMarkedForDeleteAffectedCharts = this.chartManager
            .getAll();

        for (Chart chart : daMarkedForDeleteAffectedCharts) {
            if (chart.getDaInformation().getId()
                .equals(daInformationMarkedForDelete.getId())) {
                log.debug("Deleting chart : " + chart.getName());
                executeDADeleteChartDelete(chart);
            }
        }

    }

    /**
     * Method to delete charts.
     * @param chart using the aggregator marked for delete.
     * @throws BusinessRuleException bre
     * @throws DataAccessException dae
     */
    private void executeDADeleteChartDelete(Chart chart)
        throws BusinessRuleException, DataAccessException {

        String actionKey = "notification.deleteAggregator.chart.delete";
        String fieldName = null;
        String chartName = chart.getName();
        String daActionKey = "notification.da.deleted";
        String daName = chart.getDaInformation().getDaInformationDisplayName();
        String objType = "notification.dataAggregator.chart.name";
        String actionOn = "notification.dataAggregator.chart.action";
        String processKey = "notification.deleteAggregator.title";
        String messageKey = "notification.deleteAggregator.notification";

        this.chartManager.forceDelete(chart);

        createNotification(objType, chartName, fieldName, daActionKey, daName,
            actionOn, actionKey, processKey, messageKey,
            NotificationPriority.CRITICAL);
    }

    /**
     * Method to identify charts and dashboard details affected by da update.
     * @param newDAInformation updated daInformation
     * @throws DataAccessException data access exception
     * @throws BusinessRuleException bre
     */
    private void handleChartDaInformationUpdate(DAInformation newDAInformation)
        throws DataAccessException, BusinessRuleException {

        List<Chart> daUpdateAffectedCharts = this.chartManager.getAll();

        for (Chart chart : daUpdateAffectedCharts) {
            if (chart.getDaInformation().getId()
                .equals(newDAInformation.getId())) {
                executeDAUpdateChartUpdateDelete(chart);
            }
        }

        Set<DAColumn> daColumns = newDAInformation.getColumns();
        Set<String> affectedDashboardNames = new HashSet<String>();

        for (DAColumn column : daColumns) {
            if (getDAColumnStates().contains(column.getColumnState())) {
                List<Long> affectedDetailIds = this.dashboardDetailManager
                    .listDetailIdsByLinkedField(column);
                for (Long detailId : affectedDetailIds) {
                    String dashboardName = this.dashboardDetailManager
                        .forceDelete(detailId);
                    affectedDashboardNames.add(dashboardName);
                }
            }
        }

        String actionKey = null;
        String daActionKey = "notification.da.updated";
        String daName = newDAInformation.getDaInformationDisplayName();
        String processKey = "notification.updateAggregator.title";
        String messageKey = "notification.updateAggregator.notification";
        String objType = "notification.dataAggregator.dashboard.name";
        String actionOn = "notification.dataAggregator.dashboard.action";
        for (String dName : affectedDashboardNames) {
            actionKey = "notification.updateAggregator.dashboard.detail.delete";
            createNotification(objType, dName, null, daActionKey, daName,
                actionOn, actionKey, processKey, messageKey,
                NotificationPriority.CRITICAL);
        }
    }

    /**
     * method to update/delete charts based on type of da update.
     * @param chart the chart which is getting affected
     * @throws BusinessRuleException if we could not delete the chart
     * @throws DataAccessException if we could not delete the chart
     */
    private void executeDAUpdateChartUpdateDelete(Chart chart)
        throws BusinessRuleException, DataAccessException {

        String actionKey = null;
        String fieldName = null;
        String daActionKey = "notification.da.updated";
        String chartName = chart.getName();
        String daName = chart.getDaInformation().getDaInformationDisplayName();
        String objType = "notification.dataAggregator.chart.name";
        String actionOn = "notification.dataAggregator.chart.action";
        String processKey = "notification.updateAggregator.title";
        String messageKey = "notification.updateAggregator.notification";
        Boolean chartConfigModified = false;

        if (getDAColumnStates().contains(chart.getCategory().getColumnState())) {
            actionKey = "notification.updateAggregator.chart.delete";
            fieldName = chart.getCategory().getDaColumnDisplayName();
            this.chartManager.forceDelete(chart);
            createNotification(objType, chartName, fieldName, daActionKey,
                daName, actionOn, actionKey, processKey, messageKey,
                NotificationPriority.CRITICAL);

            return;
        }

        if ((chart.getPivotOn() != null)
            && ((getDAColumnStates().contains(chart.getPivotOn().getColumnState())) || (getDAColumnStates()
                .contains(chart.getPivotFor().getColumnState())))) {
            chart.setPivotOn(null);
            chart.setPivotFor(null);
            chartConfigModified = true;

        }

        Set<ChartDataField> chartDataFields = chart.getChartDataFields();

        Set<ChartDataField> unaffectedFields = new HashSet<ChartDataField>();

        for (ChartDataField dataField : chartDataFields) {
            if (!getDAColumnStates().contains(dataField.getDaColumn()
                .getColumnState())) {
                unaffectedFields.add(dataField);
            }
        }

        if (unaffectedFields.size() != chartDataFields.size()) {
            chart.getChartDataFields().clear();
            chart.getChartDataFields().addAll(unaffectedFields);

            chartConfigModified = true;
        }

        if (chartConfigModified) {
            this.chartManager.save(chart);

            actionKey = "notification.updateAggregator.chart.modified";
            createNotification(objType, chartName, fieldName, daActionKey,
                daName, actionOn, actionKey, processKey, messageKey,
                NotificationPriority.WARNING);
        }
    }

    /**
     * Method to create a Notification for the Chart.
     * @param objTypeKey whether object is chart or dashboard
     * @param objectName Name of object
     * @param fieldName Field Name
     * @param daActionKey whether da has been updated or deleted
     * @param daName DaName
     * @param actionOnKey action on key
     * @param actionKey action key
     * @param processKey process key
     * @param messageKey message key
     * @param priority notification priority
     * 
     */
    private void createNotification(String objTypeKey,
                                    String objectName,
                                    String fieldName,
                                    String daActionKey,
                                    String daName,
                                    String actionOnKey,
                                    String actionKey,
                                    String processKey,
                                    String messageKey,
                                    NotificationPriority priority) {
        LOPArrayList lop = new LOPArrayList();

        lop.add(daActionKey, daName);
        lop.add(objTypeKey, objectName);
        lop.add(actionOnKey, actionKey);
        if (fieldName != null) {
            lop.add("notification.da.field.name", fieldName);
        }
        try {
            getNotificationManager().createNotification(processKey, messageKey,
                priority, new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getLocalizedMessage());
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 