/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.web.action;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.ChartDataField;
import com.vocollect.epp.chart.model.ChartType;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.dataaggregator.util.DAUtils;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.SortOrder;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;

import static com.vocollect.epp.web.util.JSONResponseBuilder.ERROR_FAILURE;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.log4j.Level;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author mraj
 * 
 */
public class ChartActionRoot extends DataProviderAction implements Preparable {

    /**
     * Serial Version ID.
     */
    private static final long serialVersionUID = 1L;

    private static final Logger log = new Logger(ChartActionRoot.class);

    private static final long CHART_VIEW_ID = -2002;

    private static final String FIELD_CHECKED = "fieldChecked";

    private static final String CHART_DATA_FIELDS = "chartDataFields";

    private static final String CHART_DATA_PROPERTIES = "chartDataProperties";

    private static final String CHART_PIVOT_FIELDS = "chartPivotFields";

    private static final String CHART_SORT_BY_FIELDS = "sortByFields";
    
    private static final String CHART_FIELDS_LIST_KEY_NONE = "chart.field.value.none";

    private Chart chart;

    private ChartManager chartManager;

    private DAInformationManager daInformationManager;

    private DAColumnManager daColumnManager;

    private Long chartId;

    private Long daInformationID;

    private Long categoryId;

    private String pivotOnId;

    private String pivotForId;

    private String sortById;

    private String sortOrderId;

    private String dataField;

    private List<Column> chartColumns;

    private TreeMap<String, Long> daInformationMap;

    private LinkedHashMap<Long, String> categoriesMap;

    private LinkedHashMap<String, String> pivotOnFieldsMap;

    private LinkedHashMap<String, String> pivotForFieldsMap;

    private DAInformation daInformation;

    /**
     * Getter for the chart property.
     * @return Chart value of the property
     */
    public Chart getChart() {
        return chart;
    }

    /**
     * Setter for the chart property.
     * @param chart the new chart value
     */
    public void setChart(Chart chart) {
        this.chart = chart;
    }

    /**
     * Getter for the chartManager property.
     * @return ChartManager value of the property
     */
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * Setter for the chartManager property.
     * @param chartManager the new chartManager value
     */
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * Getter for the daInformationManager property.
     * @return DAInformationManager value of the property
     */
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * Setter for the daInformationManager property.
     * @param daInformationManager the new daInformationManager value
     */
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * Getter for the chartId property.
     * @return Long value of the property
     */
    public Long getChartId() {
        return chartId;
    }

    /**
     * Setter for the chartId property.
     * @param chartId the new chartId value
     */
    public void setChartId(Long chartId) {
        this.chartId = chartId;
    }

    /**
     * Getter for the daInformationID property.
     * @return Long value of the property
     */
    public Long getDaInformationID() {
        return daInformationID;
    }

    /**
     * Setter for the daInformationID property.
     * @param daInformationID the new daInformationID value
     */
    public void setDaInformationID(Long daInformationID) {
        this.daInformationID = daInformationID;
    }

    /**
     * Getter for the categoryId property.
     * @return Long value of the property
     */
    public Long getCategoryId() {
        return categoryId;
    }

    /**
     * Setter for the categoryId property.
     * @param categoryId the new categoryId value
     */
    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Getter for the pivotOnnId property.
     * @return Long value of the property
     */
    public String getPivotOnId() {
        return pivotOnId;
    }

    /**
     * Setter for the pivotOnId property.
     * @param pivotOnId the new pivotOnId value
     */
    public void setPivotOnId(String pivotOnId) {
        this.pivotOnId = pivotOnId;
        this.chart.setPivotOn(getDaColumnManager().findDAColumnByFieldId(this.daInformation, pivotOnId));
    }

    /**
     * Getter for the pivotForId property.
     * @return Long value of the property
     */
    public String getPivotForId() {
        return pivotForId;
    }

    /**
     * Setter for the pivotForId property.
     * @param pivotForId the new pivotForId value
     */
    public void setPivotForId(String pivotForId) {
        this.pivotForId = pivotForId;
        this.chart.setPivotFor(getDaColumnManager().findDAColumnByFieldId(this.daInformation, pivotForId));
    }

    /**
     * Getter for the sortById property.
     * @return String value of the property
     */
    public String getSortById() {
        return sortById;
    }

    /**
     * Setter for the sortById property.
     * @param sortById the new sortById value
     */
    public void setSortById(String sortById) {
        this.sortById = sortById;
        this.chart.setSortBy(getDaColumnManager().findDAColumnByFieldId(this.daInformation, sortById));
    }

    /**
     * Getter for the sortOrderId property.
     * @return String value of the property
     */
    public String getSortOrderId() {
        return sortOrderId;
    }

    /**
     * Setter for the sortOrderId property.
     * @param sortOrderId the new sortOrderId value
     */
    public void setSortOrderId(String sortOrderId) {
        this.sortOrderId = sortOrderId;
        this.chart.setSortOrder(SortOrder.toEnum(Integer.parseInt(sortOrderId)));
    }

    /**
     * @return the dataField
     */
    public String getDataField() {
        return dataField;
    }

    /**
     * @param dataField the dataField to set
     */
    public void setDataField(String dataField) {
        this.dataField = dataField;
    }

    /**
     * Getter for the chartColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getChartColumns() {
        return chartColumns;
    }

    /**
     * Setter for the chartColumns property.
     * @param chartColumns the new chartColumns value
     */
    public void setChartColumns(List<Column> chartColumns) {
        this.chartColumns = chartColumns;
    }

    /**
     * Getter for Chart type.
     * @return enum value Integer.
     */
    public Integer getType() {
        return this.chart.getType().toValue();
    }

    /**
     * Setter for Chart Type.
     * @param value Integer.
     */
    public void setType(Integer value) {
        this.chart.setType(ChartType.toEnum(value));
    }

    /**
     * This method is used for view action of chart.
     * @return Data Aggregator display name.
     */
    public String getChartDataSourceValue() {
        return ResourceUtil.getLocalizedKeyValue(this.chart.getDaInformation()
            .getDisplayName());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getManager()
     */
    @Override
    protected DataProvider getManager() {
        return (DataProvider) this.getChartManager();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getKeyPrefix()
     */
    @Override
    protected String getKeyPrefix() {
        return "chart";
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vocollect.epp.web.action.DataProviderActionRoot#getViewIds()
     */
    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(CHART_VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the chartViewId property.
     * @return long value of the property
     */
    public static long getChartViewId() {
        return CHART_VIEW_ID;
    }

    /**
     * Action for the chart view page. Initializes the site table columns     * 
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View chartView = getUserPreferencesManager().getView(CHART_VIEW_ID);
        this.chartColumns = this.getUserPreferencesManager().getColumns(
            chartView, getCurrentUser());
        return SUCCESS;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.opensymphony.xwork2.Preparable#prepare()
     */
    @Override
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);
        log.debug("Inside prepare method");
        log.debug("chartId = " + getChartId());
        if (getChartId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("chartId is " + getChartId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the chart is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved chart from session");
                }

                this.chart = (Chart) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting chart from database");
                }
                this.chart = this.chartManager.get(getChartId());
                saveEntityInSession(this.chart);
            }

            // Set criteria, data aggregator information and alert frequence
            // after we retrieved the alert when editing

            this.setDaInformationID(this.chart.getDaInformation().getId());
            this.setCategoryId(this.chart.getCategory().getId());
            this.dataField = chartDataFieldsToJSON().toString();
            this.daInformation = daInformationManager.get(getDaInformationID());
            
            if ((this.chart.getPivotOn() != null)
                && (this.chart.getPivotFor() != null)) {

                this.setPivotOnId(this.chart.getPivotOn().getId().toString());
                this.setPivotForId(this.chart.getPivotFor().getId().toString());
            } else {

                this.setPivotOnId(ResourceUtil
                    .getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE));
                this.setPivotForId(ResourceUtil
                    .getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE));
            }

            if ((this.chart.getSortBy() != null)
                && (this.chart.getSortOrder() != null)) {

                this.setSortById(this.chart.getSortBy().getId().toString());
                this.setSortOrderId(String.valueOf(this.chart.getSortOrder()
                    .toValue()));
            } else {

                this.setSortById(ResourceUtil
                    .getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE));
                this.setSortOrderId(String.valueOf(SortOrder.ASCENDING.toValue()));
            }

            if (log.isDebugEnabled()) {
                log.debug("Chart version is: " + this.chart.getVersion());
            }
        } else if (this.chart == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new Chart object");
            }
            this.chart = new Chart();
        }
    }

    /**
     * @return SUCCESS or INPUT
     * @throws Exception When save fails
     */
    public String save() throws Exception {
        boolean isNew = this.chart.isNew();

        try {
            // Before saving the chart, checking if already an chart exists with
            // the same name.
            this.getChartManager().verifyUniquenessByName(this.chart);
            DAInformation daInfo = daInformationManager
                .get(getDaInformationID());
            this.chart.setDaInformation(daInfo);

            Set<ChartDataField> selectedDataFieds = populateDataField(daInfo);

            if (isNew) {
                this.chart.setChartDataFields(selectedDataFieds);
            } else {
                this.chart.getChartDataFields().clear();

                if (this.chart.getChartDataFields().isEmpty()) {
                    this.chart.getChartDataFields().addAll(selectedDataFieds);
                }
            }

            DAColumn category = daColumnManager.get(getCategoryId());
            this.chart.setCategory(category);

            if (selectedDataFieds.size() > 1
                && ((getType() == ChartType.PIE.toValue()))) {
                throw new FieldValidationException("type",
                    ResourceUtil.getLocalizedEnumName(ChartType
                        .toEnum(getType())), new UserMessage(
                        "chart.error.invalidChartType",
                        ResourceUtil.getLocalizedEnumName(ChartType
                            .toEnum(getType()))));
            }

            for (ChartDataField selectedChartDataField : selectedDataFieds) {
                if ((selectedChartDataField.getDaColumn().getFieldType() == DAFieldType.DATE)
                    && (getType() != ChartType.GRID.toValue())) {
                    throw new FieldValidationException("type",
                        ResourceUtil.getLocalizedEnumName(ChartType
                            .toEnum(getType())), new UserMessage(
                            "chart.error.invalidChartType.dateField",
                            ResourceUtil
                                .getLocalizedKeyValue(selectedChartDataField
                                    .getDaColumn().getDisplayName())));
                }
            }

            verifyAndSetChartPivotFields();
            verifyAndSetChartSortFields();

            if ((getChartManager().getPrimaryDAO().countById(
                this.chart.getId()) == 0) && (!isNew)) {
                this.chart.getChartDataFields().clear();
            }
            
            this.getChartManager().save(this.chart);
            cleanSession();
        } catch (BusinessRuleException bre) {
        
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified chart "
                + this.chart.getName());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.chart"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Chart modifiedChart = getChartManager().get(getChartId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.chart.setVersion(modifiedChart.getVersion());
                this.chart.setChartDataFields(modifiedChart.getChartDataFields());
                // Store the modified data for display
                setModifiedEntity(modifiedChart);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.chart"), null));
                return SUCCESS;
            }
            return INPUT;
        } 

        addSessionActionMessage(new UserMessage("chart."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/charts/view.action?chartId="
                + this.chart.getId()), this.chart.getName()));

        return SUCCESS;
    }
    
    /**
     * the method verifies pivot field selections and sets them to current chart.
     * @throws FieldValidationException fve
     * @throws NumberFormatException nfe
     * @throws DataAccessException dae
     */
    private void verifyAndSetChartPivotFields()
        throws FieldValidationException, NumberFormatException,
        DataAccessException {
        String pOnId = getPivotOnId();
        String pForId = getPivotForId();

        if (pOnId != null && pForId != null
            && getType() == ChartType.GRID.toValue()) {

            if ((pOnId.equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE))) != (pForId
                .equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE)))) {
                if (pOnId.equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE))) {
                    throw new FieldValidationException("pivotOnId", "",
                        new UserMessage("chart.error.pivotOn.none"));
                } else {
                    throw new FieldValidationException("pivotForId", "",
                        new UserMessage("chart.error.pivotFor.none"));
                }
            }

            if ((!pOnId.equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE)))
                && (!pForId.equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE)))) {
                DAColumn pivotOn = daColumnManager.get(Long.valueOf(
                    getPivotOnId()).longValue());
                this.chart.setPivotOn(pivotOn);

                DAColumn pivotFor = daColumnManager.get(Long.valueOf(
                    getPivotForId()).longValue());
                this.chart.setPivotFor(pivotFor);
            } else {
                this.chart.setPivotOn(null);
                this.chart.setPivotFor(null);
            }

        } else {
            this.chart.setPivotOn(null);
            this.chart.setPivotFor(null);
        }
    }
    
    /**
     * the method verifies sort field selections and sets them to current chart.
     * @throws FieldValidationException fve
     * @throws NumberFormatException nfe
     * @throws DataAccessException dae
     */
    private void verifyAndSetChartSortFields()
        throws FieldValidationException, NumberFormatException,
        DataAccessException {
        String sortByFieldId = getSortById();
        String sortOrderFieldId = getSortOrderId();

        if (sortByFieldId != null && sortOrderFieldId != null) {

            if ((!sortByFieldId.equals(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE)))) {
                DAColumn sortBy = daColumnManager.get(Long.valueOf(
                    sortByFieldId).longValue());
                this.chart.setSortBy(sortBy);
                
                SortOrder sortOrder = SortOrder.toEnum(Integer
                    .parseInt(sortOrderFieldId));
                this.chart.setSortOrder(sortOrder);
            } else {
                this.chart.setSortBy(null);
                this.chart.setSortOrder(null);
            }

        } else {
            this.chart.setSortBy(null);
            this.chart.setSortOrder(null);
        }
    }

    /**
     * 
     * @return list of modified data fields
     */
    public List<String> getModifiedDataFields() {        
        List<String> modifiedDataFields = new ArrayList<String>();
        Set <ChartDataField> modifiedChartDataFields = ((Chart) getModifiedEntity()).getChartDataFields();
        for (ChartDataField modifiedField : modifiedChartDataFields) {
            modifiedDataFields.add(modifiedField.getDaColumn().getDaColumnDisplayName());
        }
        Collections.sort(modifiedDataFields);
        return modifiedDataFields;
    }
    
    /**
     * 
     * @return localised value of modified chart type
     */
    public String getModifiedChartType() {
        return ResourceUtil.getLocalizedEnumName(((Chart) getModifiedEntity()).getType());
    }

    /**
     * 
     * @return localised value of modified chart sort Order
     */
    public String getModifiedSortOrder() {
        SortOrder sortOrder = ((Chart) getModifiedEntity()).getSortOrder();
        if (sortOrder != null) {
            return ResourceUtil.getLocalizedEnumName(sortOrder);
        } else {
            return "";
        }
    }
    
    /**
     * Deletes the currently viewed chart.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current chart
     */
    public String deleteCurrentChart() throws Exception {

        Chart chartToDelete = null;

        try {
            chartToDelete = chartManager.get(getChartId());

            chartManager.delete(getChartId());

            addSessionActionMessage(new UserMessage(
                "chart.delete.message.success", chartToDelete.getName()));

        } catch (BusinessRuleException e) {
            log.warn("Unable to delete Chart: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Method to populate chart display field from JSON.
     * @param daInfo daInfo
     * @return Set of chart display field
     * @throws JSONException jsonexception
     * @throws DataAccessException dataaccessexception
     */
    private Set<ChartDataField> populateDataField(DAInformation daInfo)
        throws JSONException, DataAccessException {
        JSONArray dataFieldJSONArray = new JSONArray(getDataField());

        Set<ChartDataField> dataFields = new HashSet<ChartDataField>();
        for (int i = 0; i < dataFieldJSONArray.length(); i++) {
            JSONObject dataFieldJSONObj = new JSONObject(dataFieldJSONArray
                .get(i).toString());
            if (!dataFieldJSONObj.getBoolean(FIELD_CHECKED)) {
                continue;
            }
            ChartDataField chartdataField = new ChartDataField();
            chartdataField.setChart(this.chart);

            DAColumn daColumn = daColumnManager.findDAColumnByFieldId(
                daInfo,
                dataFieldJSONObj.getString(GenericDataAggregator.FIELD_ID));
            chartdataField.setDaColumn(daColumn);
            dataFields.add(chartdataField);
        }

        return dataFields;
    }

    /**
     * 
     * @return jsonarray of fields associated to charts
     * @throws JSONException je
     * @throws DataAccessException dae
     */
    private JSONArray chartDataFieldsToJSON() throws JSONException,
        DataAccessException {
        Set<DAColumn> chartDaColumns = new HashSet<DAColumn>();

        Set <ChartDataField> dataFieldsInChart = this.chart.getChartDataFields();
        for (ChartDataField field : dataFieldsInChart) {
            chartDaColumns.add(field.getDaColumn());
        }

        DAInformation da = this.chart.getDaInformation();

        List<DAFieldType> daFieldTypes = new ArrayList<DAFieldType>();
        daFieldTypes.add(DAFieldType.INTEGER);
        daFieldTypes.add(DAFieldType.FLOAT);
        daFieldTypes.add(DAFieldType.DATE);
        List<DAColumn> columns = daColumnManager.listDAColumnByFieldType(da,
            daFieldTypes);

        Map<Long, DAColumn> daColumnMap = new LinkedHashMap<Long, DAColumn>();

        for (DAColumn daColumn : columns) {
            daColumnMap.put(daColumn.getId(), daColumn);
        }

        // Create JSON from daColumn
        JSONArray chartDataFieldsJSON = new JSONArray();

        Set<Long> daColumnKeys = daColumnMap.keySet();
        Iterator<Long> keyItr = daColumnKeys.iterator();

        while (keyItr.hasNext()) {
            DAColumn column = daColumnMap.get(keyItr.next());

            JSONObject chartDataField = new JSONObject();
            chartDataField.put(GenericDataAggregator.FIELD_ID,
                column.getFieldId());
            chartDataField.put(GenericDataAggregator.COLUMN_ID, column.getId());
            chartDataField.put(GenericDataAggregator.DISPLAY_NAME,
                ResourceUtil.getLocalizedKeyValue(column.getDisplayName()));
            chartDataField.put(FIELD_CHECKED, chartDaColumns.contains(column));
            chartDataFieldsJSON.put(chartDataField);
        }

        return chartDataFieldsJSON;
    }

    /**
     * Utility method to identify field types needed based on usage.
     * @param isCategory is the field types are required or category field
     * @return List of DA field types
     */
    private List<DAFieldType> getFieldTypes(boolean isCategory) {
        List<DAFieldType> daFieldTypes = new ArrayList<DAFieldType>();
        if (isCategory) {
            daFieldTypes.add(DAFieldType.STRING);
            daFieldTypes.add(DAFieldType.TIME);
        } else {
            daFieldTypes.add(DAFieldType.INTEGER);
            daFieldTypes.add(DAFieldType.FLOAT);
            daFieldTypes.add(DAFieldType.DATE);
        }
        return daFieldTypes;
    }

    /**
     * Getter method for Data Aggregation Information.
     * @return Map of Data Aggregation Information objects ID and String
     * @throws DataAccessException .
     */
    public Map<String, Long> getChartDAInformationMap()
        throws DataAccessException {
        daInformationMap = new TreeMap<String, Long>();

        List<DAInformation> daInfos = this.getDaInformationManager()
            .getAll();

        for (DAInformation daInfo : daInfos) {
            if (!daInfo.isMarkedForDelete()) {
                daInformationMap.put(
                    ResourceUtil.getLocalizedKeyValue(daInfo.getDisplayName()),
                    daInfo.getId());
            }
        }

        return daInformationMap;
    }

    /**
     * Getter method for returning all categories for the Chart. Chart
     * categories are all String and Time data type variables in DA Column for a
     * specific DA Information.
     * @return categoriesMap
     * @throws DataAccessException .
     */
    public Map<Long, String> getCategoriesMap() throws DataAccessException {
        categoriesMap = new LinkedHashMap<Long, String>();

        DAInformation da = null;

        if (this.getChartId() == null) {
            if (this.getDaInformationID() == null) {
                Set<String> daIDKeys = this.daInformationMap.keySet();
                Iterator<String> keyItr = daIDKeys.iterator();
                if (keyItr.hasNext()) {
                    Long id = daInformationMap.get(keyItr.next());
                    da = this.getDaInformationManager().get(id);
                }
            } else {
                da = this.getDaInformationManager().get(
                    this.getDaInformationID());
            }
        } else {
            da = this.chart.getDaInformation();
        }
        List<DAColumn> daColumnList = this.getDaColumnManager()
            .listDAColumnByFieldType(da, getFieldTypes(true));

        for (DAColumn daColumn : daColumnList) {
            categoriesMap.put(daColumn.getId(),
                ResourceUtil.getLocalizedKeyValue(daColumn.getDisplayName()));
        }

        return categoriesMap;
    }

    /**
     * Get the possible Chart Types for display.
     * @return the Map of defined Chart Types
     */
    public Map<Integer, String> getTypeMap() {
        Map<Integer, String> typeMap = new LinkedHashMap<Integer, String>();
        for (ChartType type : ChartType.values()) {
            typeMap
                .put(type.toValue(), ResourceUtil.getLocalizedEnumName(type));
        }
        return typeMap;
    }

    /**
     * . Method to get list of DAColumns which remain common for pivotOn and
     * pivotFor fields
     * @return list of DAColumns which remain common for pivotOn and pivotFor
     *         fields
     * @throws DataAccessException dataaccessexception
     */
    private List<DAColumn> getDaColumnAllFieldTypesList()
        throws DataAccessException {
        DAInformation da = null;

        if (this.getChartId() == null) {

            if (this.getDaInformationID() == null) {
                Set<String> daIDKeys = this.daInformationMap.keySet();
                Iterator<String> keyItr = daIDKeys.iterator();
                if (keyItr.hasNext()) {
                    Long id = daInformationMap.get(keyItr.next());
                    da = this.getDaInformationManager().get(id);
                }
            } else {
                da = this.getDaInformationManager().get(
                    this.getDaInformationID());
            }
        } else {
            da = this.chart.getDaInformation();
        }

        List<DAColumn> daColumnList = this.getDaColumnManager()
            .listDAColumnByFieldType(da, DAUtils.getAllFieldTypes());

        return daColumnList;
    }

    /**
     * Getter method for returning all pivotOn fields for the Chart. Chart
     * pivotOn fields are all DAColumns for a specific DA Information, including
     * a NONE option.
     * @return pivotOnFieldsMap
     * @throws DataAccessException dataaccessexception
     */
    public Map<String, String> getPivotOnFieldsMap()
        throws DataAccessException {
        pivotOnFieldsMap = new LinkedHashMap<String, String>();

        pivotOnFieldsMap.put(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE),
            ResourceUtil.getLocalizedKeyValue("chart.field.value.none"));

        List<DAColumn> daColumns = getDaColumnAllFieldTypesList();

        for (DAColumn daColumn : daColumns) {
            pivotOnFieldsMap.put(daColumn.getId().toString(),
                ResourceUtil.getLocalizedKeyValue(daColumn.getDisplayName()));
        }

        return pivotOnFieldsMap;
    }

    /**
     * Getter method for returning all pivotFor fields for the Chart. Chart
     * pivotFor fields are all DAColumns for a specific DA Information,
     * including a NONE option.
     * @return pivotOnFieldsMap
     * @throws DataAccessException dataaccessexception
     */
    public Map<String, String> getPivotForFieldsMap()
        throws DataAccessException {
        pivotForFieldsMap = new LinkedHashMap<String, String>();

        pivotForFieldsMap.put(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE),
            ResourceUtil.getLocalizedKeyValue("chart.field.value.none"));

        List<DAColumn> daColumns = getDaColumnAllFieldTypesList();

        for (DAColumn daColumn : daColumns) {
            pivotForFieldsMap.put(daColumn.getId().toString(),
                ResourceUtil.getLocalizedKeyValue(daColumn.getDisplayName()));
        }

        return pivotForFieldsMap;
    }

    /**
     * Getter method for returning all sortBy fields for the Chart. Chart
     * sortBy fields are all DAColumns for a specific DA Information,
     * including a NONE option.
     * @return sortByFieldsMap
     * @throws JSONException jsonexception
     * @throws DataAccessException dataaccessexception
     */
    public Map<String, String> getSortByFieldsMap()
        throws DataAccessException, JSONException {
        Map<String, String> sortByFieldsMap = new LinkedHashMap<String, String>();

        sortByFieldsMap.put(ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE),
            ResourceUtil.getLocalizedKeyValue("chart.field.value.none"));

        List<DAColumn> daColumns = getDaColumnAllFieldTypesList();

        for (DAColumn daColumn : daColumns) {
            sortByFieldsMap.put(daColumn.getId().toString(),
                ResourceUtil.getLocalizedKeyValue(daColumn.getDisplayName()));
        }

        return sortByFieldsMap;
    }

    /** getter for list of sort orders.
     * @return sortorderFieldsmap
     * @throws DataAccessException dataaccessexception
     */
    public Map<String, String> getSortOrderMap()
        throws DataAccessException {
        
        Map<String, String> sortOrderMap = new LinkedHashMap<String, String>();
        
        for (SortOrder sortOrder : SortOrder.values()) {
            sortOrderMap
                .put(String.valueOf(sortOrder.toValue()), ResourceUtil.getLocalizedEnumName(sortOrder));
        }
        return sortOrderMap;
    }

    /**
     * @return Success or failure after setting category fields for dropdown.
     */
    public String getChartConfigFieldsActionSupport() {
        setMessage(getChartFieldProperties(getFieldTypes(true)));
        return SUCCESS;
    }

    /**
     * @return Success or failure after setting data fields json in message.
     */
    public String getChartDataFieldsActionSupport() {
        setMessage(getChartFieldProperties(getFieldTypes(false)));
        return SUCCESS;
    }

    /**
     * Method to return the data fields for charts.
     * @return jsonArray in String.
     */
    public String getChartDataFieldProperties() {
        return getChartFieldProperties(getFieldTypes(false));
    }

    /**
     * Method to return the fields for charts based on passed field types.
     * @param fieldTypes - da field types
     * @return jsonArray in String.
     */
    private String getChartFieldProperties(List<DAFieldType> fieldTypes) {

        DAInformation da = null;
        JSONArray chartDataFields = null;
        JSONArray chartDataProperties = null;
        JSONArray chartPivotFields = null;
        JSONArray chartSortByFields = null;

        List<DAColumn> dataColumns = new ArrayList<DAColumn>();
        try {
            // If we have alert id, that means we are editing, else it is a new
            // page
            if (this.getChartId() == null) {
                if (getDaInformationID() == null) {
                    Set<String> daIDKeys = this.daInformationMap.keySet();
                    Iterator<String> keyItr = daIDKeys.iterator();
                    if (keyItr.hasNext()) {
                        Long id = daInformationMap.get(keyItr.next());
                        da = this.getDaInformationManager().get(id);
                    }
                    dataColumns = daColumnManager.listDAColumnByFieldType(da,
                        fieldTypes);
                    chartDataFields = daPropertyToJSON(dataColumns, false);
                } else {
                    da = this.daInformationManager.get(getDaInformationID());
                    if (this.dataField != null) {
                        chartDataFields = new JSONArray(this.dataField);
                    }
                }
            } else {
                da = this.chart.getDaInformation();
                chartDataFields = new JSONArray(this.dataField);
            }

            List<DAColumn> categoryColumns = daColumnManager
                .listDAColumnByFieldType(da, fieldTypes);
            chartDataProperties = daPropertyToJSON(categoryColumns, false);

            List<DAColumn> pivotColumns = daColumnManager
                .listDAColumnByFieldType(da, DAUtils.getAllFieldTypes());
            chartPivotFields = daPropertyToJSON(pivotColumns, true);

            List<DAColumn> sortByColumns = daColumnManager
                .listDAColumnByFieldType(da, DAUtils.getAllFieldTypes());
            chartSortByFields = daPropertyToJSON(sortByColumns, true);

            // Create final variable
            JSONObject chartDataFieldArray = new JSONObject();
            chartDataFieldArray.put(CHART_DATA_FIELDS, chartDataFields);
            chartDataFieldArray.put(CHART_DATA_PROPERTIES, chartDataProperties);
            chartDataFieldArray.put(CHART_PIVOT_FIELDS, chartPivotFields);
            chartDataFieldArray.put(CHART_SORT_BY_FIELDS, chartSortByFields);
            return chartDataFieldArray.toString();
        } catch (Exception e) {
            log.warn("Excption occured preparing chart on data fields : "
                + e.getLocalizedMessage());
        }
        return "";

    }

    /**
     * 
     * @param columns list of columns to be converted to json array
     * @param isNoneFieldRequired if the list of columns to be converted requires a none field
     * @return json array of columns
     * @throws JSONException je
     */
    private JSONArray daPropertyToJSON(List<DAColumn> columns, Boolean isNoneFieldRequired)
        throws JSONException {

        Map<Long, DAColumn> daColumnMap = new LinkedHashMap<Long, DAColumn>();

        JSONArray chartDataFields = new JSONArray();

        for (DAColumn column : columns) {
            daColumnMap.put(column.getId(), column);
        }

        if (isNoneFieldRequired) {
            JSONObject noneField = new JSONObject();
            noneField.put(GenericDataAggregator.FIELD_ID,
                ResourceUtil.getLocalizedKeyValue("chart.field.value.none"));
            noneField.put(GenericDataAggregator.COLUMN_ID,
                ResourceUtil.getLocalizedKeyValue(CHART_FIELDS_LIST_KEY_NONE));
            noneField.put(GenericDataAggregator.DISPLAY_NAME,
                ResourceUtil.getLocalizedKeyValue("chart.field.value.none"));

            chartDataFields.put(noneField);
        }

        Set<Long> daColumnKeys = daColumnMap.keySet();
        Iterator<Long> keyItr = daColumnKeys.iterator();

        while (keyItr.hasNext()) {
            DAColumn daColumn = daColumnMap.get(keyItr.next());

            JSONObject chartDataField = new JSONObject();
            chartDataField.put(GenericDataAggregator.FIELD_ID,
                daColumn.getFieldId());
            chartDataField.put(GenericDataAggregator.COLUMN_ID,
                daColumn.getId());
            chartDataField.put(GenericDataAggregator.DISPLAY_NAME,
                ResourceUtil.getLocalizedKeyValue(daColumn.getDisplayName()));
            chartDataField.put(FIELD_CHECKED, false);

            chartDataFields.put(chartDataField);
        }

        return chartDataFields;
    }
    
    /**.
     * Method to get data to preview a chart
     * @return data to draw preview of selected chart
     * @throws DataAccessException dae
     * @throws BusinessRuleException bre
     * @throws JSONException je
     */
    public String getChartPreviewData() throws DataAccessException,
        BusinessRuleException, JSONException {
        JSONObject chartData = new JSONObject();

        if (getChartId() != null) {
            JSONObject chartConfig = new JSONObject();
            JSONArray data = new JSONArray();

            Chart previewChart = this.getChartManager().get(getChartId());

            JSONArray aggregatorData = this.getChartManager().executeChartDataAggregator(previewChart);
            
            if (previewChart.getChartDataFields().isEmpty() && previewChart.getPivotOn() == null) {
                setJsonMessage(getText("chart.preview.error.noFields.selected"),
                    ERROR_FAILURE);
                return SUCCESS;
            }
            
            chartConfig = this.getChartManager().getChartJSON(null, previewChart, aggregatorData);
            chartData.put("config", chartConfig);

            data = this.getChartManager().getChartData(previewChart, aggregatorData);
            
            if (data.length() == 0) {
                setJsonMessage(getText("chart.preview.error.noData"),
                    ERROR_FAILURE);
                return SUCCESS;
            }
            
            chartData.put("data", data);
        }
        
        setJsonMessage(chartData.toString());
        return SUCCESS;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 