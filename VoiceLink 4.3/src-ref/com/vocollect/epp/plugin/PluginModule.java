/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.plugin;

import com.vocollect.epp.util.StringUtil;

import javax.servlet.http.HttpServletRequest;



/**
 * Model object for an Application Plugin Module. This is not
 * intended to be customizable. It must remain unchanged to allow
 * for plugin forward compatibility.
 *
 * @author ddoubleday
 */
public class PluginModule extends PluginBase {

    // The default port if no port is specified in the request URL.
    private static final int DEFAULT_HTTP_PORT = 80;

    private static final long serialVersionUID = 6956930558990173022L;
    
    // The module version.
    private String moduleVersion;
    // The host name (or IPAddress) at which the master module runs.
    private String masterHost;
    // The port number at which the master module runs.
    private String masterPort;
    // The webapp context at which the master module runs.
    private String masterContext;

    /**
     * Getter for the moduleVersion property.
     * @return the value of the property
     */
    public String getModuleVersion() {
        return this.moduleVersion;
    }

    
    /**
     * Setter for the moduleVersion property.
     * @param moduleVersion the new moduleVersion value
     */
    public void setModuleVersion(String moduleVersion) {
        this.moduleVersion = moduleVersion;
    }

    
    /**
     * Getter for the masterContext property.
     * @return the value of the property
     */
    public String getMasterContext() {
        return this.masterContext;
    }

    
    /**
     * Setter for the masterContext property.
     * @param masterContext the new masterContext value
     */
    public void setMasterContext(String masterContext) {
        this.masterContext = masterContext;
    }

    
    /**
     * Getter for the masterHost property.
     * @return the value of the property
     */
    public String getMasterHost() {
        return this.masterHost;
    }

    
    /**
     * Setter for the masterHost property.
     * @param masterHost the new masterHost value
     */
    public void setMasterHost(String masterHost) {
        this.masterHost = masterHost;
    }

    
    /**
     * Getter for the masterPort property.
     * @return the value of the property
     */
    public String getMasterPort() {
        return this.masterPort;
    }

    
    /**
     * Setter for the masterPort property.
     * @param masterPort the new masterPort value
     */
    public void setMasterPort(String masterPort) {
        this.masterPort = masterPort;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result 
            + ((getModuleVersion() == null) ? 0 : getModuleVersion().hashCode());
        return result;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof PluginModule)) {
            return false;
        }
        final PluginModule other = (PluginModule) obj;
        if (moduleVersion == null) {
            if (other.getModuleVersion() != null) {
                return false;
            }
        } else if (!moduleVersion.equals(other.getModuleVersion())) {
            return false;
        }
        return true;
    }
    
    /**
     * Get the URL associated with this module. This may be a
     * context-relative URI, or it may be a full URL pointing to
     * another module.
     * @param request the current request object
     * @return the URL for this module
     */
    public String getUrl(HttpServletRequest request) {

        if (!StringUtil.isNullOrBlank(getMasterContext())) {
            // The context for this module is potentially in another webapp.
            // Compare the context to the current context to know whether
            // or not we need to build a full URL or just use the
            // context-relative one.
            if (!request.getContextPath().equals(getMasterContext())) {
                // We need to go to another webapp for this module.
                return makeFullURL(request);
            }
        }
        // In the right webapp, just use the URI.
        return getUri();
    }


    /**
     * @param request the current request object
     * @return a full URL to get to the plugin module
     */
    private String makeFullURL(HttpServletRequest request) {

        String port;
        String host;
        
        if (StringUtil.isNullOrBlank(getMasterPort())) {
            if (request.getRemotePort() == DEFAULT_HTTP_PORT) {
                port = "";
            } else {
                port = ":" + String.valueOf(request.getLocalPort());
            }
        } else {
            port = ":" + getMasterPort().trim();
        }
        
        if (StringUtil.isNullOrBlank(getMasterHost())) {
            host = request.getServerName();
        } else {
            host = getMasterHost().trim();
        }
        
        String uri = getUri().trim();
        if (!uri.startsWith("/")) {
            uri = "/" + uri;
        }
        
        return request.getScheme() + "://" + host + port + "/"
            + getMasterContext().trim() + uri;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 