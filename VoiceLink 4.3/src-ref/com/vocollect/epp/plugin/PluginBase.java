/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.plugin;

import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.User;

import java.util.Date;
import java.util.List;


/**
 * Base object for Plugin modules and components. This is not
 * intended to be customizable. It must remain unchanged to allow
 * for plugin forward compatibility.
 *
 * @author ddoubleday
 */
public abstract class PluginBase extends BaseModelObject {

    private static final long serialVersionUID = 6956930558990173022L;
    
    // The module name. This will be a resource key.
    private String name;
    // The EPP framework version for the module.
    private String frameworkVersion;
    // Whether or not the plugin is enabled.
    private boolean isEnabled = false;
    // The order in which active modules are displayed.
    private int    sequenceNumber;
    // The name of the nav menu that is associated with the plugin
    private String navMenuName;
    // The URI to go to when the plugin menu button is clicked
    private String uri;
    // The components associated with this module.
    private List<PluginModuleComponent> components;
    // The list of feature group names that determine whether or not
    // a user has access to this module.
    private List<String> authNames;
    // The Date the plugin was installed.
    private Date createdDate;
    
    
    /**
     * Get the list of feature group names that determine whether or not
     * a user has access to this module.
     * @return the List of names. May be null.
     */
    public List<String> getAuthNames() {
        return this.authNames;
    }


    
    /**
     * Set the list of feature group names that determine whether or not
     * a user has access to this module.
     * @param authNames the new authNames value
     */
    public void setAuthNames(List<String> authNames) {
        this.authNames = authNames;
    }


    /**
     * Getter for the components property.
     * @return the value of the property
     */
    public List<PluginModuleComponent> getComponents() {
        return this.components;
    }

    
    /**
     * Setter for the components property.
     * @param components the new components value
     */
    public void setComponents(List<PluginModuleComponent> components) {
        this.components = components;
    }

    
    /**
     * Getter for the frameworkVersion property.
     * @return the value of the property
     */
    public String getFrameworkVersion() {
        return this.frameworkVersion;
    }

    
    /**
     * Setter for the frameworkVersion property.
     * @param frameworkVersion the new frameworkVersion value
     */
    public void setFrameworkVersion(String frameworkVersion) {
        this.frameworkVersion = frameworkVersion;
    }

    
    
    /**
     * Getter for the sequenceNumber property.
     * @return int value of the property
     */
    public int getSequenceNumber() {
        return this.sequenceNumber;
    }

    
    /**
     * Setter for the sequenceNumber property.
     * @param sequenceNumber the new sequenceNumber value
     */
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * Get whether or not the module is enabled.
     * @return the value of the property
     */
    public boolean getIsEnabled() {
        return this.isEnabled;
    }

    
    /**
     * Set whether or not the module is enabled.
     * @param enabled the new isEnabled value
     */
    public void setIsEnabled(boolean enabled) {
        this.isEnabled = enabled;
    }

    
    /**
     * Getter for the name property.
     * @return the value of the property
     */
    public String getName() {
        return this.name;
    }

    
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    
    /**
     * Get the URI associated with the plugin. If none was specified,
     * the conventional URI /navMenuName/default.action is assumed.
     * @return the URI, either specified or by convention
     */
    public String getUri() {
        if (this.uri != null) {
            return this.uri.trim();
        } else {
            return "/" + getNavMenuName() + "/default.action";
        }
    }

    
    /**
     * Setter for the uri property.
     * @param uri the new uri value
     */
    public void setUri(String uri) {
        this.uri = uri;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        return result;
    }



    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof PluginModuleComponent)) {
            return false;
        }
        final PluginBase other = (PluginBase) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        
        return true;
    }



    
    /**
     * Getter for the navMenuName property.
     * @return String value of the property
     */
    public String getNavMenuName() {
        return this.navMenuName;
    }



    
    /**
     * Setter for the navMenuName property.
     * @param navMenuName the new navMenuName value
     */
    public void setNavMenuName(String navMenuName) {
        this.navMenuName = navMenuName;
    }

    /**
     * Get whether or not the plugin is the currently active
     * one, given the menuContext.
     * @param menuContext the menu being viewed.
     * @return true if the plugin or any of its components navMenuNames
     * match the context.
     */
    public boolean getIsCurrent(String menuContext) {
        
        if (getNavMenuName().equals(menuContext)) {
            return true;
        } else if (getHasComponents()) {
            for (PluginBase component : getComponents()) {
                if (component.getNavMenuName().equals(menuContext)) {
                    return true;
                }
            }
        }
        
        return false;        
    }
    
    /**
     * @return true if the plugin has components, false otherwise.
     */
    public boolean getHasComponents() {
        return getComponents() != null && !getComponents().isEmpty();
    }



    /**
     * Determine whether or not the user has access to the plugin features.
     * @param user the current user
     * @return true if user has access, false otherwise.
     */
    public boolean isAccessibleByUser(User user) {
        
        if (getAuthNames() == null || getAuthNames().isEmpty()) {
            // No restrictions.
            return true;
        } else {
            for (String authName : getAuthNames()) {
                if (user.getHasAccessInFeatureGroup(authName)) {
                    return true;
                }
            }
            // No authorization.
            return false;
        }
    }



    
    /**
     * Getter for the createdDate property.
     * @return Date value of the property
     */
    public Date getCreatedDate() {
        return this.createdDate;
    }

    
    /**
     * Setter for the createdDate property.
     * @param createdDate the new createdDate value
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 