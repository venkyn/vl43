/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.util.SiteContextHolder;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.acegisecurity.Authentication;
import org.acegisecurity.AuthenticationException;
import org.acegisecurity.ui.AbstractProcessingFilter;
import org.acegisecurity.ui.savedrequest.SavedRequest;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;


/**
 * Extension of the Acegi AuthenticationProcessingFilter that does our
 * additional processing at login time.
 *
 * @author ddoubleday
 */
public class AuthenticationFilterRoot extends AuthenticationProcessingFilter {

    private static Logger log = new Logger(AuthenticationFilter.class);

    // The session key under which the login failure count is stored.
    private static final String FAILURE_COUNT_ATTRIBUTE = "loginFailureCount";

    // The number of failed login attempts before we start delaying
    private static final Integer FAILURE_DELAY_THRESHHOLD = 3;

    // The number of millisecs we delay after the threshhold is exceeded.
    private static final long FAILURE_DELAY_MILLIS = 500;

    // The service used to store User information.
    private UserManager userManager;

    /**
     * Getter for the userManager property.
     * @return the UserManager.
     */
    public UserManager getUserManager() {
        return this.userManager;
    }


    /**
     * Setter for the userManager property.
     * @param userManager the new manager.
     */
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * This extension of the Acegi filter updates the User's login information
     * after successful login.
     * {@inheritDoc}
     * @see org.acegisecurity.ui.AbstractProcessingFilter#onSuccessfulAuthentication
     * (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.acegisecurity.Authentication)
     */
    @Override
    protected void onSuccessfulAuthentication(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Authentication auth)
        throws IOException {

        super.onSuccessfulAuthentication(request, response, auth);

        // Works around a Tomcat issue. Because of Tomcat thread pooling,
        // a SiteContext can be leftover from a previous usage of the thread.
        // This makes sure there isn't one.
        SiteContextHolder.setSiteContext(null);

        if (auth.getPrincipal() instanceof User) {
            // Authentication was successful, get the User
            User currentUser = (User) auth.getPrincipal();
            try {
                // This is an attempted fix for EPP-89. When operating
                // on the Acegi principal directly, we were getting
                // a stale data exception sometimes. Not really sure
                // why, but this change (getting the User from the
                // DB) should fix it, I think. -ddoubleday
                User user = getUserManager().get(currentUser.getId());
                user.setLastLoginTime(new Date());
                user.setLastLoginLocation(request.getRemoteHost());
                getUserManager().save(user);
            } catch (Exception e) {
                // The login was successful, but we weren't able to
                // update the user object for some reason.
                log.error("", SecurityErrorCode.USER_UPDATE_FAILURE, e);
            }
        } else {
            // I don't know why this would happen, but just so it is
            // noticed if it does.
            log.warn("Authentication succeeded, but Acegi Principal "
                + auth + "is not an instance of User");
        }
    }


    /**
     * This extension of the Acegi filter, after unsuccessful login, increments
     * a failure counter and starts delaying briefly additional login attempts
     * after the <code>FAILURE_DELAY_THRESHHOLD</code> is exceeded.
     * {@inheritDoc}
     * @see org.acegisecurity.ui.AbstractProcessingFilter#onUnsuccessfulAuthentication
     * (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void onUnsuccessfulAuthentication(HttpServletRequest request,
                                                HttpServletResponse response,
                                                AuthenticationException authenticationException)
        throws IOException {

        super.onUnsuccessfulAuthentication(request, response, authenticationException);

        HttpSession session = request.getSession();
        Integer failureCount = 0;
        // Try to retrieve the failure count from the session.
        if (session.getAttribute(FAILURE_COUNT_ATTRIBUTE) != null) {
            failureCount = (Integer)
                session.getAttribute(FAILURE_COUNT_ATTRIBUTE);
        }
        // This is a new failure, so increment the count and store it back.
        failureCount++;
        session.setAttribute(FAILURE_COUNT_ATTRIBUTE, failureCount);

        if (log.isInfoEnabled()) {
            log.info("Login attempt failed, failure count is "
                + failureCount);
        }

        // If the count exceeds the threshhold, delay before allowing
        // another login attempt.
        if (failureCount > FAILURE_DELAY_THRESHHOLD) {
            if (log.isInfoEnabled()) {
                log.info("Delaying before allowing additional attempts");
            }
            try {
                Thread.sleep(FAILURE_DELAY_MILLIS);
            } catch (InterruptedException e1) {
                // No problem, this doesn't matter.
            }
        }
    }


    /**
     * {@inheritDoc}
     * @see org.acegisecurity.ui.AbstractProcessingFilter#successfulAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.acegisecurity.Authentication)
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            Authentication authResult) throws IOException {

        // Now check to see if the request was saved for later forwarding.
        // If it was, then remove the saved URL if it is a JSON-returning
        // request (to the best of our ability to determine (bugfix for VLINK-58)
        SavedRequest savedRequest = (SavedRequest)
            request.getSession().getAttribute(
                AbstractProcessingFilter.ACEGI_SAVED_REQUEST_KEY);

        if (savedRequest != null) {
            Map<String, ?> parameterMap = savedRequest.getParameterMap();
            String savedRequestURI = savedRequest.getRequestURI();
            if (parameterMap.get("refreshRequest") != null
                    // table component
                || savedRequestURI.toLowerCase().contains("acknotification")
                    // unacked notifications
                || savedRequestURI.toLowerCase().contains("userselection")
                || savedRequestURI.toLowerCase().contains("currentselection")) {
                    // user selection save/check

                if (log.isDebugEnabled()) {
                    log.debug("Ignoring saved URL '"
                        + savedRequest.getRequestURI()
                        + "' for login redirect purposes");
                }
                request.getSession().removeAttribute(
                    AbstractProcessingFilter.ACEGI_SAVED_REQUEST_KEY);
            }
        }
        // Now do the usual stuff.
        super.successfulAuthentication(request, response, authResult);
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 