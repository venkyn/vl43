/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;

import java.lang.reflect.Method;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.acegisecurity.providers.dao.UserCache;
import org.acegisecurity.userdetails.UserDetails;
import org.springframework.aop.AfterReturningAdvice;

/**
 * This interceptor runs after operations that change <code>User</code>
 * objects and evicts them from the Acegi cache, so we won't be working with
 * stale data.
 *
 * @author ddoubleday
 */
public class UserSecurityAdvice implements AfterReturningAdvice {


    private static final Logger log = new Logger(UserSecurityAdvice.class);

    // The Acegi user cache
    private UserCache userCache;

    /**
     * Setter for the user cache.
     * @param userCache the Acegi cache object
     */
    public void setUserCache(UserCache userCache) {
        this.userCache = userCache;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.aop.AfterReturningAdvice#afterReturning
     * (java.lang.Object, java.lang.reflect.Method, java.lang.Object[], java.lang.Object)
     */
    public void afterReturning(Object returnValue,
                               Method method,
                               Object[] args,
                               Object target) throws Throwable {

        // This should be called when the User save or delete method is
        // called. If the first arg is a User, then we can be specific in
        // clearing just one entry. If it is a Long, that is an ID and we
        // must clear the whole cache because we don't know the name that
        // goes with the ID.
        // TODO: This whole Acegi cache thing is kind of annoying.
        // think there might be a better way to do this with a Hibernate
        // second level cache. --ddoubleday
        User user;
        if (args[0] instanceof Long) {
            // This means that the delete method that takes an ID was called.
            ((com.vocollect.epp.security.UserCache) this.userCache).clear();
            if (log.isDebugEnabled()) {
                log.debug("Cleared user cache");
            }
        } else {
            user = (User) args[0];

            flushCache(user);

            // Reset the authentication object if the modified user was the
            // current one.
            Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();

            if (auth != null && auth.getPrincipal() instanceof UserDetails) {
                User currentUser = (User) auth.getPrincipal();
                if (currentUser.getId().equals(user.getId())) {
                    if (!currentUser.getUsername().equalsIgnoreCase(
                        user.getUsername())) {
                        // The name of the current user changed, so the
                        // previous flush won't have done anything. Flush the
                        // old name, too.
                        flushCache(currentUser);
                    }
                    // Replace the authentication token with a new one
                    // containing
                    // the updated User object. Make sure his
                    // roles/features/featuregroups have
                    // been loaded, to avoid lazy loading exception later.
                    // TODO: Is there a better way to do this? --ddoubleday
                    for (Role role : user.getRoles()) {
                        for (Feature feature : role.getFeatures()) {
                            feature.getFeatureGroup().getName();
                        }
                    }
                    if (user.getSites() != null) {
                        for (Site site : user.getSites()) {
                            site.getName();
                        }
                    }
                    if (user.getUserProperties() != null) {
                        for (UserProperty prop : user.getUserProperties()) {
                            prop.getName();
                        }
                    }
                    if (user.getTags() != null) {
                    	for (Tag t : user.getTags()) {
                    		t.getDescriptiveText();
                    	}
                    }
                    auth = new UsernamePasswordAuthenticationToken(
                        user, user.getPassword(), user.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(auth);
                }
            }
        }
    }

    /**
     * Flush the user out of the Acegi cache.
     * @param user the user
     */
    private void flushCache(User user) {
        if (log.isDebugEnabled()) {
            log.debug("Removing '" + user.getUsername() + "' from userCache");
        }

        // Remove the modified user from the cache.
        userCache.removeUserFromCache(user.getUsername());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 