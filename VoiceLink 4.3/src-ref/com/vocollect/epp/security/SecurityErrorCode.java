/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for EPP security-related errors.
 * @author Dennis Doubleday
 *
 */
public final class SecurityErrorCode extends ErrorCode {

    /**
     * Lower boundary of the EPP security error range.
     */
    public static final int LOWER_BOUND  = 1000;

    /**
     * Upper boundary of the EPP security error range.
     */
    public static final int UPPER_BOUND  = 1999;

    /**
     * No error, just the base initialization.
     */
    public static final SecurityErrorCode NO_ERROR
        = new SecurityErrorCode();

    /**
     * Unable to get current user.
     */
    public static final SecurityErrorCode NO_CURRENT_USER
        = new SecurityErrorCode(1000);

    /**
     * Unable to update user.
     */
    public static final SecurityErrorCode USER_UPDATE_FAILURE
        = new SecurityErrorCode(1001);

    /**
     * Unable to retrieve access control data.
     */
    public static final SecurityErrorCode NO_ACCESS_CONTROL_DATA
        = new SecurityErrorCode(1002);

    /**
     * Unable to update role.
     */
    public static final SecurityErrorCode ROLE_UPDATE_FAILURE
        = new SecurityErrorCode(1003);

    /**
     * Constructor.
     */
    private SecurityErrorCode() {
        super("EPP", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private SecurityErrorCode(long err) {
        // The error is defined in the SecurityErrorCode context.
        super(SecurityErrorCode.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 