/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import java.io.IOException;

import org.acegisecurity.providers.dao.cache.EhCacheBasedUserCache;


/**
 * Extension of Acegi user cache that allows for clearing all entries.
 *
 * @author ddoubleday
 */
public class UserCache extends EhCacheBasedUserCache {

    /**
     * Clear the User cache of all entries.
     * @throws IOException on failure to write to cache
     */
    public void clear() throws IOException {
        getCache().removeAll();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 