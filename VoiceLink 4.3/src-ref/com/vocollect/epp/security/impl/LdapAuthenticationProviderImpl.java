/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.security.LdapAuthenticationProvider;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.web.action.SystemConfigurationAction;

import java.util.HashMap;
import java.util.Map;

import org.acegisecurity.AuthenticationException;
import org.acegisecurity.AuthenticationServiceException;
import org.acegisecurity.ldap.DefaultInitialDirContextFactory;
import org.acegisecurity.ldap.search.FilterBasedLdapUserSearch;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.acegisecurity.providers.dao.AbstractUserDetailsAuthenticationProvider;
import org.acegisecurity.providers.ldap.authenticator.BindAuthenticator;
import org.acegisecurity.userdetails.UserDetails;


/**
 *
 *
 * @author jstonebrook
 */
public class LdapAuthenticationProviderImpl extends AbstractUserDetailsAuthenticationProvider
        implements LdapAuthenticationProvider {

    private UserManager userManager;
    private SystemPropertyManager systemPropertyManager;

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.providers.dao.AbstractUserDetailsAuthenticationProvider#additionalAuthenticationChecks(org.acegisecurity.userdetails.UserDetails, org.acegisecurity.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected void additionalAuthenticationChecks(UserDetails arg0,
                                                  UsernamePasswordAuthenticationToken arg1)
        throws AuthenticationException {
        // TODO Auto-generated method stub
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.providers.dao.AbstractUserDetailsAuthenticationProvider#retrieveUser(java.lang.String, org.acegisecurity.providers.UsernamePasswordAuthenticationToken)
     */
    @Override
    protected UserDetails retrieveUser(String arg0,
                                       UsernamePasswordAuthenticationToken arg1)
        throws AuthenticationException {

        String host;
        String port;
        String searchBase;
        String managerDN;
        String managerPassword;
        String userAttribute;
        String authenticateAgainstDirectoryServer;

        try {
            authenticateAgainstDirectoryServer = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_AUTHENTICATION).getValue();
            host = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_HOST).getValue();
            port = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_PORT).getValue();
            managerDN = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_SEARCH_USERNAME).getValue();
            managerPassword = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_SEARCH_PASSWORD).getValue();
            searchBase = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_SEARCH_BASE).getValue();
            userAttribute = getSystemPropertyManager().
                findByName(SystemConfigurationAction.DIRSERV_SEARCHABLE_ATTRIBUTE).getValue();
        } catch (DataAccessException dae) {
            // If we get a DAE - then go ahead and log it - but throw it as an
            // Authentication Service Exception as well
            throw new AuthenticationServiceException("Unable to access System Property", dae);
        } catch (NullPointerException npe) {
            // If we get a npe - then go ahead and log it - but throw it as an
            // Authentication Service Exception as well
            throw new AuthenticationServiceException("Unable to access System Property", npe);
        }

        if (authenticateAgainstDirectoryServer != null
            && authenticateAgainstDirectoryServer.equalsIgnoreCase("true")) {

            DefaultInitialDirContextFactory context = new DefaultInitialDirContextFactory(
                "ldap://" + host + ":" + port);

            if (managerDN != null && managerDN.length() > 0) {
                // We can support anonymous bind - so only set the manager DN
                // and password if they are defined
                context.setManagerDn(managerDN);
                context.setManagerPassword(managerPassword);
            }

            Map<String, String> vars = new HashMap<String, String>();
            vars.put("com.sun.jndi.ldap.connect.timeout", "5000");
            vars.put("java.naming.referral", "follow");
            context.setExtraEnvVars(vars);

            FilterBasedLdapUserSearch userSearch = new FilterBasedLdapUserSearch(
                searchBase, "(" + userAttribute + "={0})", context);
            userSearch.setSearchSubtree(true);

            BindAuthenticator bindAuthenticator = new BindAuthenticator(context);
            bindAuthenticator.setUserSearch(userSearch);

            bindAuthenticator.authenticate(arg0, (String) arg1.getCredentials());

            return getUserManager().loadUserByUsername(arg0);
        } else {
            throw new AuthenticationServiceException("Directory service is not configured.");
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.security.LdapAuthenticationProvider#setUserManager(com.vocollect.epp.service.UserManager)
     */
    public void setUserManager(UserManager newUserManager) {
        this.userManager = newUserManager;
    }

    /**
     * @return the User manager.
     */
    private UserManager getUserManager() {
        return this.userManager;
    }


    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return this.systemPropertyManager;
    }


    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 