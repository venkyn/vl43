/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security.taglibs.authzx;

import com.vocollect.epp.security.RoleFeatureMap;
import com.vocollect.epp.security.RoleFeatureMapFactory;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.Tag;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.taglibs.authz.AuthorizeTag;

/**
 * Custom JSP tag for checking permissions within FreeMarker based on the user's
 * feature-level access. In short, if the user has access to the specified
 * feature, the body of the JSP tag is output. If the user does not have access,
 * the body is skipped.
 * @author Brian Rupert
 */
@SuppressWarnings("serial")
public class AuthorizeByFeatureTag extends AuthorizeTag {

    /**
     * The feature name this tag is checking.
     */
    private String featureName;

    private static final String ALWAYS_ACCESSIBLE = "always";

    private static final String NEVER_ACCESSIBLE = "never";

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.taglibs.authz.AuthorizeTag#doStartTag()
     */
    @Override
    public int doStartTag() throws JspTagException {

        if (featureName.equals(ALWAYS_ACCESSIBLE)) {
            return Tag.EVAL_BODY_INCLUDE;
        }
        if (featureName.equals(NEVER_ACCESSIBLE)) {
            return Tag.SKIP_BODY;
        }

        Authentication user = SecurityContextHolder.getContext()
            .getAuthentication();
        RoleFeatureMap rfm = RoleFeatureMapFactory.getRoleFeatureMap();

        if (rfm.userHasFeatureAccess(user, featureName)) {
            return Tag.EVAL_BODY_INCLUDE;
        } else {
            return Tag.SKIP_BODY;
        }
    }

    /**
     * Retrieve the feature name to check against.
     * @return The feature name.
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * Set the feature name to check against.
     * @param featureName The feature name.
     */
    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 