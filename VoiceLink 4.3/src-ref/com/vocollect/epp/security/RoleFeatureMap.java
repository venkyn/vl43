/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.DateUtil;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.GrantedAuthority;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * This object maps between configured <code>Role</code>s and configured
 * application <code>Feature</code>s. If a role is not configured with a
 * feature, then users in that role have no access to the feature. (With the
 * exception of Administrator, which has access to everything.)
 * 
 * @author Dennis Doubleday
 */
public class RoleFeatureMap {

    private static Logger log = new Logger(RoleFeatureMap.class);

    // WW convention to identify an INPUT action
    private static final String INPUT_MARKER = "!input";

    // WW convention to identify a CANCEL action
    private static final String CANCEL_MARKER = "!cancel";

    // paths for security layer configuration
    private String[] appSecurityFileNames = null;

    // The XML tag name that contains feature info
    private static final String FEATURE_MAP_TAG = "feature-map";

    // The XML tag name that contains feature info
    private static final String FEATURE_TAG = "feature";

    // The XML attribute name that contains the action URI
    private static final String ACTION_ATTR = "action";

    // The XML attribute name that contains the SOAP action URN
    private static final String SOAP_URN_ATTR = "soap-urn";
    
    public static final String VOC_OPER_ROLE_NAME = "Voc_Operator";
    
    private static final String VOC_OPER_FEATURE = "feature.appAdmin.site.changeOperCred";

    // Internal map of role primary keys to logical feature names. Each entry
    // in the Map is another Map in which the keys are features (no values)
    private Map<String, Map<String, String>> rolesToFeatures = new HashMap<String, Map<String, String>>();

    // Internal map of action URIs to logical feature names
    private Map<String, String> actionURIsToFeatures = new HashMap<String, String>();

    // Internal map of SOAP action URNs to logical feature names
    private Map<String, String> soapURNsToFeatures = new HashMap<String, String>();

    // Cached list of UserRoles
    private List<Role> roleList;

    // Service to retrieve role/feature info.
    private RoleManager roleManager;

    private SystemPropertyManager systemPropertyManager;

    private boolean isInitialized = false;

    private Date reInitTime;

    /**
     * Set the paths for security layer configuration.
     * @param paths - The array of paths to load for the security layer.
     */
    public void setAppSecurityFileNames(String[] paths) {
        this.appSecurityFileNames = paths;
    }

    /**
     * Gets the paths for security layer configuration.
     * @return The paths array.
     */
    public String[] getAppSecurityFileNames() {
        return this.appSecurityFileNames;
    }

    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * @return Returns the roleManager.
     */
    public RoleManager getRoleManager() {
        return roleManager;
    }

    /**
     * This is Spring-loaded. Should not need to be set.
     * @param roleManager The roleManager to set.
     */
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    /**
     * Initialize the map if necessary.
     * @throws DataAccessException on failure to retrieve role and feature
     *             mapping data.
     */
    public synchronized void initialize() throws DataAccessException {
        if (this.isInitialized) {
            return;
        } else {
            reinitialize();
        }
    }

    /**
     * Calls reload and notification to load the role feature map and notify the
     * change.
     * @throws DataAccessException on failure to retrieve role and feature
     *             mapping data.
     */
    public synchronized void reinitialize() throws DataAccessException {
        reLoadRoleFeatureMap();
        notifyReinitialization();
    }

    /**
     * Construct the RoleFeatureMap. It pulls information from both the database
     * and the configuration file.
     * @throws DataAccessException if reloading role feature map fails
     */
    public synchronized void reLoadRoleFeatureMap() throws DataAccessException {
        this.isInitialized = false;
        this.rolesToFeatures.clear();

        // Get the list of roles with associated logical features from
        // the database.
        this.roleList = getRoleManager().getAll();

        for (Role role : this.roleList) {
            // Create an internal feature map for this role
            Map<String, String> roleMap = new HashMap<String, String>();
            // For each feature associated with a role, add it to the
            // cache map.
            if (role.getFeatures() != null) {
                for (Feature feature : role.getFeatures()) {
                    roleMap.put(feature.getName(), feature.getName());
                    if (log.isTraceEnabled()) {
                        log.trace("User role " + role.getName()
                            + " has access to feature " + feature.getName());
                    }
                }
            }
            // Add the map for this role to the outer map.
            this.rolesToFeatures.put(role.getName(), roleMap);
            if (log.isTraceEnabled()) {
                log.trace("Added user role " + role.getName() + " to map");
            }
        }

        addVocOperRoleToMap();
        
        // Get the list of logical features with optional URI mappings
        // (this comes from the app-security*.xml files) and
        // create the two-way maps of URIs to logical feature names

        buildFeatureMaps(appSecurityFileNames);

        this.isInitialized = true;

        this.reInitTime = new Date();
    }

    /**
     * This method adds the operator role to the roles to features
     * map.  This is done here because we don't want this role or
     * feature being in the database.  It's used for VoiceConsole 
     * operator login only.
     *
     */
    private void addVocOperRoleToMap() {
        Map<String, String> roleMap = new HashMap<String, String>();
        roleMap.put(VOC_OPER_FEATURE, VOC_OPER_FEATURE);
        this.rolesToFeatures.put(VOC_OPER_ROLE_NAME, roleMap);
    }
    
    /**
     * Saves the re-initialization time to system properties.
     * 
     */
    private void notifyReinitialization() {
        String roleFeatureReInitTimeString = "";

        if (this.getReInitTime() != null) {
            roleFeatureReInitTimeString = new String(DateUtil.getTimestamp(this
                .getReInitTime(), DateUtil.LONG_DATE_FORMAT));
        } else {
            roleFeatureReInitTimeString = new String(DateUtil.getTimestamp(
                new Date(), DateUtil.LONG_DATE_FORMAT));
        }

        SystemProperty lastInitTimeProp = null;
        try {
            lastInitTimeProp = this.getSystemPropertyManager().findByName(
                SystemPropertyManager.LAST_INIT_TIME);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        if (lastInitTimeProp == null) {
            lastInitTimeProp = new SystemProperty();
            lastInitTimeProp.setName(SystemPropertyManager.LAST_INIT_TIME);
            lastInitTimeProp.setValue(roleFeatureReInitTimeString);
        } else {
            lastInitTimeProp.setValue(roleFeatureReInitTimeString);
        }

        try {
            this.getSystemPropertyManager().save(lastInitTimeProp);
        } catch (BusinessRuleException e) {
            e.printStackTrace();
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        if (log.isInfoEnabled()) {
            log.info("Last init time updated");
        }
    }

    /**
     * Determine whether or not the specified user has access to the specified
     * URI.
     * @param user the authenticated user
     * @param uri the URI to test for access
     * @return true if access control is disabled or the user is non-null and
     *         has access, false otherwise.
     */
    public synchronized boolean userHasURIAccess(Authentication user, String uri) {

        if (user != null && user.isAuthenticated()) {
            for (GrantedAuthority role : user.getAuthorities()) {
                if (role instanceof Role) {
                    if (log.isTraceEnabled()) {
                        log.trace("Testing role: " + ((Role) role).getName()
                            + ", for access to " + uri);
                    }
                    if (roleHasURIAccess((Role) role, uri)) {
                        if (log.isTraceEnabled()) {
                            log.trace("Access permitted");
                        }
                        return true;
                    }
                } else {
                    if (log.isTraceEnabled()) {
                        log.trace("Skipping authority: " + role.getAuthority()
                            + ", it is not a Role");
                    }
                }
            }
        }
        return false;
    }

    /**
     * Determine whether or not the specified role has access to the specified
     * URI.
     * @param role the role being tested
     * @param uri the URI being checked. This must be a context-relative URI.
     * @return true if the role has access, false otherwise
     */
    public synchronized boolean roleHasURIAccess(Role role, String uri) {

        if (uri.indexOf(CANCEL_MARKER) != -1) {
            // The "!cancel" notation is a WW convention for canceling from
            // an action. Any role is allowed to cancel anything
            if (log.isTraceEnabled()) {
                log.trace("Access always granted for Cancel action");
            }
            return true;
        }
        if (role.getIsAdministrative()) {
            // If the role is an Administrator, just return true.
            if (log.isTraceEnabled()) {
                log.trace("Access granted due to Administrator role.");
            }
            return true;

        } else {
            String baseURI = uri;
            // If the URI has a query string portion, remove it.
            int queryIndex = uri.indexOf('?');
            if (queryIndex != -1) {
                baseURI = uri.substring(0, queryIndex);
            }
            // The "!input" notation is a Struts convention for displaying
            // a form page without submitting it. We ignore it when comparing
            // the URI to the configured URIs.
            baseURI = baseURI.replace(INPUT_MARKER, "");

            if (log.isTraceEnabled()) {
                log.trace("testing for access to URI: " + baseURI);
            }
            String feature = this.actionURIsToFeatures.get(baseURI);
            if (feature == null) {
                log.warn("No access defined for URI " + uri);
                return false;
            } else {
                boolean canAccess = roleHasFeatureAccess(role, feature);
                if (log.isTraceEnabled()) {
                    log.trace("Access to URI " + uri
                        + (canAccess ? " granted" : " denied") + " to role "
                        + role.getName() + " via feature " + feature);
                }
                return canAccess;
            }
        }
    }

    /**
     * Determine whether or not the specified user has access to the specified
     * SOAP URN.
     * @param user the authenticated user.
     * @param soapURN the SOAP URN to test for access.
     * @return true if access control is disabled or the user is non-null and
     *         has access, false otherwise.
     */
    public synchronized boolean userHasSOAPAccess(Authentication user,
                                                  String soapURN) {

        if (user != null) {
            for (GrantedAuthority role : user.getAuthorities()) {
                if (roleHasSOAPAccess((Role) role, soapURN)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine whether or not the specified role has access to the specified
     * SOAP action URN.
     * @param role the role being tested
     * @param soapURN the SOAP action URN being checked.
     * @return true if the role has access, false otherwise
     */
    public synchronized boolean roleHasSOAPAccess(Role role, String soapURN) {
        if (role.getIsAdministrative()) {
            // Administrators have access to everything
            return true;
        }
        String feature = this.soapURNsToFeatures.get(soapURN);
        if (feature == null) {
            log.warn("No access defined for URN " + soapURN);
            return false;
        }
        boolean canAccess = roleHasFeatureAccess(role, feature);
        if (log.isTraceEnabled()) {
            log.trace("Access to SOAP URN " + soapURN + " "
                + (canAccess ? "granted" : "denied") + " to role " + role
                + " via feature " + feature);
        }
        return canAccess;
    }

    /**
     * Determine whether or not the specified user has access to the specified
     * logical feature.
     * @param user the authenticated user
     * @param featureName the name of the logical feature to check.
     * @return true if access control is disabled or the user is non-null and
     *         has access, false otherwise.
     */
    public synchronized boolean userHasFeatureAccess(Authentication user,
                                                     String featureName) {

        if (user != null) {
            for (GrantedAuthority role : user.getAuthorities()) {
                if (roleHasFeatureAccess((Role) role, featureName)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine whether or not the specified role has access to the specified
     * logical feature.
     * @param role the role being tested
     * @param featureName the feature name being checked.
     * @return true if the role has access, false otherwise
     */
    public synchronized boolean roleHasFeatureAccess(Role role,
                                                     String featureName) {

        if (role.getIsAdministrative()) {
            return true;
        } else {
            Map<String, String> roleMap = this.rolesToFeatures.get(role.getName());
            if (roleMap == null) {
                log.warn("No feature map for user role " + role);
                return false;
            } else {
                // If there is a value for this role, it has access.
                String value = roleMap.get(featureName);
                if (value != null) {
                    if (log.isTraceEnabled()) {
                        log.trace("Access to feature " + featureName
                            + " granted to role " + role.getName());
                    }
                    return true;
                } else {
                    if (log.isTraceEnabled()) {
                        log.trace("Access to feature " + featureName
                            + " denied to role " + role.getName());
                    }
                    return false;
                }
            }
        }
    }

    /**
     * Build the URI-Feature Maps.
     * @param fromFileNames that will supply the app-security settings.
     */
    private void buildFeatureMaps(String[] fromFileNames) {
        // Make sure all maps are empty, in case we are reinitializing.
        this.actionURIsToFeatures.clear();
        this.soapURNsToFeatures.clear();

        for (String securityfile : fromFileNames) {
            buildFeatureMaps(this.getClass().getResourceAsStream(securityfile));
        }
    }

    /**
     * Initialize the internal URI-Feature Map.
     * @param stream an InputStream that will supply the XML. This is a stream
     *            off the appSecurityFileNames that defines the available
     *            Feature-URI mappings.
     */
    @SuppressWarnings("unchecked")
    private void buildFeatureMaps(InputStream stream) {

        SAXReader builder = new SAXReader(false);
        Document dom4jDoc = null;
        Element rootElement = null;
        List<Element> allFeatures = null;

        try {
            if (stream != null) {

                // Build document from XML stream
                dom4jDoc = builder.read(stream);
                rootElement = dom4jDoc.getRootElement();

                // All the children that define app features are tagged
                // as FEATURE_TAG.

                allFeatures = rootElement.element(
                    FEATURE_MAP_TAG).elements(FEATURE_TAG);

                // Now build the HashMap. Keys are actionURIs or SOAP URNs,
                // values are feature names.
                if (allFeatures != null) {
                    for (Element elem : allFeatures) {
                        String featureName = elem.getText();
                        Attribute action = elem.attribute(ACTION_ATTR);
                        Attribute soapURN = elem.attribute(SOAP_URN_ATTR);
                        if (action != null) {
                            // Add it to the map, with feature name as value.                            
                            addToFeatureMap(featureName, action.getValue(), 
                                            this.actionURIsToFeatures, "URI");
                        } else if (soapURN != null) {
                            // Add it to the map, with feature name as value.                            
                            addToFeatureMap(featureName, soapURN.getValue(), 
                                            this.soapURNsToFeatures, "Soap URN");
                        }
                    }
                }
            } else {
                log.error(
                    "Unable to load app feature map, " + appSecurityFileNames
                        + ", resource not found",
                    SecurityErrorCode.NO_ACCESS_CONTROL_DATA);
            }
        } catch (Throwable e) {
            log.error(
                "Unable to load app feature map, " + appSecurityFileNames,
                SecurityErrorCode.NO_ACCESS_CONTROL_DATA, e);
        }

    }

    /**
     * @param featureName the feature to add (as map value)
     * @param resourceName the resource to add (as map key)
     * @param featureMap the map to add to
     * @param resourceTypeName the name of the type of resource being added.
     */
    private void addToFeatureMap(String featureName,
                                 String resourceName,
                                 Map<String, String> featureMap,
                                 String resourceTypeName) {
        String previousValue = featureMap.put(resourceName, featureName);
        if (previousValue != null) {
            log.warn("Mapping of URI '" + resourceName + "' to feature '" 
                + featureName + "' replaced previous mapping to feature '" 
                + previousValue + "'");
        }
        if (log.isTraceEnabled()) {
            log.trace(resourceTypeName + "'" + resourceName
                + "' is tied to feature '" + featureName + "'");
        }
    }

    /**
     * Getter for the reInitTime property.
     * @return Date value of the property
     */
    public Date getReInitTime() {
        return reInitTime;
    }

    /**
     * Setter for the reInitTime property.
     * @param reInitTime the new reInitTime value
     */
    public void setReInitTime(Date reInitTime) {
        this.reInitTime = reInitTime;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 