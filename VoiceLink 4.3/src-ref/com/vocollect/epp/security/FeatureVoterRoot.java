/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.User;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.Authentication;
import org.acegisecurity.ConfigAttribute;
import org.acegisecurity.ConfigAttributeDefinition;
import org.acegisecurity.intercept.web.FilterInvocation;
import org.acegisecurity.vote.AccessDecisionVoter;

/**
 * Voter that takes into account feature-based security associated with
 * dynamic roles.
 * Votes if any {@link ConfigAttribute#getAttribute()} is
 * <ul>
 *   <li> ROLE_ANONYMOUS (any access OK)
 *   <li> ROLE_ANY (any user role is OK if the user is logged in), or
 *   <li> FEATURE_CHECK (the user roles are checked to see if they allow
 * access to the specified feature.)
 * </ul>
 * <p>
 * Access is always denied if none of the above attributes are specified.
 *
 * @author Dennis Doubleday
 *
 */
public class FeatureVoterRoot implements AccessDecisionVoter {

    private static Logger log = new Logger(FeatureVoter.class);

    // Config attribute that allows access for any logged in user
    private static final String ROLE_ANY_CONFIG = "ROLE_ANY";

    // Config attribute that allows access for any user, logged in or not
    private static final String ROLE_ANONYMOUS_CONFIG = "ROLE_ANONYMOUS";

    // Config attribute that forces a check of the users roles against
    // role/feature data.
    private static final String FEATURE_CHECK_CONFIG = "FEATURE_CHECK";

    // Printable view action name
    private static final String PRINTABLE_VIEW_ACTION = "getdataforprint.action";

    // Filter auto complete action name
    private static final String FILTER_AUTO_COMPLETE_ACTION = "asyncgetfilterautocompletedata.action";


    // Table component action name
    private static final String TABLE_COMPONENT_ACTION = "getdata.action";

    // The multi-way map of roles to features that are enabled by those roles.
    private RoleFeatureMap      roleFeatureMap;

    /**
     * @return Returns the roleFeatureMap.
     */
    public RoleFeatureMap getRoleFeatureMap() {
        return roleFeatureMap;
    }

    /**
     * @param roleFeatureMap The roleFeatureMap to set.
     */
    public void setRoleFeatureMap(RoleFeatureMap roleFeatureMap) {
        this.roleFeatureMap = roleFeatureMap;
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.vote.AccessDecisionVoter#supports(java.lang.Class)
     */
    public boolean supports(Class clazz) {
        return true;
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.vote.AccessDecisionVoter#supports(org.acegisecurity.ConfigAttribute)
     */
    public boolean supports(ConfigAttribute attr) {
        // These are the config attributes supported by this voter.
        return ROLE_ANONYMOUS_CONFIG.equals(attr.getAttribute())
            || ROLE_ANY_CONFIG.equals(attr.getAttribute())
            || FEATURE_CHECK_CONFIG.equals(attr.getAttribute());
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.vote.AccessDecisionVoter#vote
     * (org.acegisecurity.Authentication, java.lang.Object, org.acegisecurity.ConfigAttributeDefinition)
     */
    public int vote(Authentication            authentication,
                    Object                    object,
                    ConfigAttributeDefinition config) {

        if (log.isTraceEnabled()) {
            log.trace("Authentication object: " + authentication);
            log.trace("Principal: " + authentication.getPrincipal());
        }

        try {
            getRoleFeatureMap().initialize();
        } catch (DataAccessException e) {
            log.error("Unable to access role-feature mapping",
                      SecurityErrorCode.NO_ACCESS_CONTROL_DATA,
                      e);
            return ACCESS_DENIED;
        }

        // Loop through the configuration attributes for special cases
        Iterator<ConfigAttribute> iter = config.getConfigAttributes();
        while (iter.hasNext()) {
            ConfigAttribute configAttr = iter.next();
            if (configAttr.getAttribute().equals(FEATURE_CHECK_CONFIG)
                && authentication.isAuthenticated()) {
                // If the user is logged in, then we need to do a
                // check of feature access for the user's roles.

                // Get the URI without the context path.
                HttpServletRequest request =
                    ((FilterInvocation) object).getHttpRequest();
                String uri = mapURI(request.getRequestURI().
                    substring(request.getContextPath().length()).toLowerCase());


                if (getRoleFeatureMap().userHasURIAccess(authentication, uri)) {
                    if (log.isTraceEnabled()) {
                        log.trace("Access granted by feature check");
                    }
                    return AccessDecisionVoter.ACCESS_GRANTED;
                } else {
                    // Last ditch effort: check for special case in which
                    // the cancel button has been
                    // named according to a special Struts convention that
                    // maps to the Cancel action. We will always allow Cancel.
                    Iterator<String> parameterIter =
                        request.getParameterMap().keySet().iterator();
                    while (parameterIter.hasNext()) {
                        String key = parameterIter.next();
                        if (key.equals("method:cancel")) {
                            if (log.isDebugEnabled()) {
                                log.debug("Access granted for Cancel action "
                                    + "via method prefix");
                            }
                            return AccessDecisionVoter.ACCESS_GRANTED;
                        }
                    }

                    if (log.isDebugEnabled()) {
                        log.debug("Access denied by feature check");
                    }

                    return AccessDecisionVoter.ACCESS_DENIED;
                }

            } else if (configAttr.getAttribute().equals(ROLE_ANONYMOUS_CONFIG)) {
                // This allows access to anybody for this resource.
                if (log.isDebugEnabled()) {
                    log.debug("Access granted to any user.");
                }
                return ACCESS_GRANTED;

            } else if (configAttr.getAttribute().equals(ROLE_ANY_CONFIG)
                       && authentication.getPrincipal() instanceof User) {
                // Grant access if the resource is available to any role
                // and the user is logged in.
                if (log.isTraceEnabled()) {
                    log.trace("Access granted to any logged in user.");
                }
                return ACCESS_GRANTED;
            }
        }

        // If nothing was decided, assume no access
        return ACCESS_DENIED;
    }

    /**
     *  Apply special URI mappings for the purposes of feature access checking.
     *  For instance, printable view URIs are mapped to table component URIs.
     *  @param uri - the uri to be mapped
     *  @return String a mapped uri
     *
     */
    public String mapURI(String uri) {
        String mappedURI = uri;
        boolean isMapped = false;

        // map /getdataforprint.action to /getdata.action so that printable
        // views share the same access as their corresponding table component.
        if (mappedURI.indexOf(PRINTABLE_VIEW_ACTION) > -1) {
            mappedURI = mappedURI.replaceAll(PRINTABLE_VIEW_ACTION, TABLE_COMPONENT_ACTION);
            isMapped = true;
        }

        // map /asyncgetfilterautocompletedata.action to /getdata.action so that auto complete filter
        // views share the same access as their corresponding table component.

        if (mappedURI.indexOf(FILTER_AUTO_COMPLETE_ACTION) > -1) {
            mappedURI = mappedURI.replaceAll(FILTER_AUTO_COMPLETE_ACTION, TABLE_COMPONENT_ACTION);
            isMapped = true;
        }

        if (isMapped) {
            if (log.isDebugEnabled()) {
                log.debug("URI " + uri + " mapped to " + mappedURI + " for security purposes.");
            }
        }

        return mappedURI;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 