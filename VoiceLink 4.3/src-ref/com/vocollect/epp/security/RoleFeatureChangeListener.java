/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.DateUtil;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * This listener listens to the change of role feature map. When role feature
 * map gets updated this listener makes the role feature mapping to re-load.
 * This class is solely for the clustered environment.
 * 
 * @author khazra
 */
public final class RoleFeatureChangeListener extends Thread {

    public static final String BEAN_NAME = "roleFeatureListener";

    private static Logger log = new Logger(RoleFeatureChangeListener.class);

    private int delay;

    private RoleManager roleManager;

    private SystemPropertyManager systemPropertyManager;

    private RoleFeatureMap roleFeatureMap;

    private SessionFactory sessionFactory;

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Getter for the roleFeatureMapFactory property.
     * @return RoleFeatureMapFactory value of the property
     */
    public RoleFeatureMap getRoleFeatureMap() {
        return roleFeatureMap;
    }

    /**
     * Setter for the roleFeatureMapFactory property.
     * @param roleFeatureMap the new roleFeatureMapFactory value
     */
    public void setRoleFeatureMap(RoleFeatureMap roleFeatureMap) {
        this.roleFeatureMap = roleFeatureMap;
    }

    /**
     * Getter for the roleManager property.
     * @return RoleManager value of the property
     */
    public RoleManager getRoleManager() {
        return roleManager;
    }

    /**
     * Setter for the roleManager property.
     * @param roleManager the new roleManager value
     */
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the delay property.
     * @return int value of the property
     */
    public int getDelay() {
        return delay;
    }

    /**
     * Setter for the delay property.
     * @param delay the new delay value
     */
    public void setDelay(int delay) {
        this.delay = delay;
    }

    /**
     * 
     * {@inheritDoc}
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {

        // Checks if the application is clustered. If not clustered then the
        // thread will not start
        try {
            SystemProperty appClustered = this.getSystemPropertyManager()
                .findByName(SystemPropertyManager.CLUSTERED_PROP_NAME);

            if (appClustered == null
                || ("no".equalsIgnoreCase(appClustered.getValue()))) {
                if (log.isInfoEnabled()) {
                    log
                        .info("Application is not clustered. RoleFeatureChangeListener is exiting");
                }
                return;
            }
        } catch (DataAccessException e2) {
            e2.printStackTrace();
            return;
        }

        // Loop that checks the role feature map change
        while (!Thread.interrupted()) {

            if (log.isDebugEnabled()) {
                log.debug("Checking Role feature map ...");
            }

            if (isReInitializationRequired()) {
                try {
                    startSession();
                    reInitRoleFeatureMap();
                } catch (Exception ex) {
                } finally {
                    try {
                        stopSession();
                    } catch (Exception ex1) {
                        break;
                    }
                }
            }

            if (log.isDebugEnabled()) {
                log.debug("... role feature check done");
            }

            try {
                Thread.sleep(this.getDelay());
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

    /**
     * Re initializes the role feature map.
     * @return True if successfully saves the time, false if there is any error.
     */
    private boolean reInitRoleFeatureMap() {
        try {
            this.roleFeatureMap.reinitialize();
        } catch (DataAccessException e) {
            e.printStackTrace();
            return false;
        }

        if (log.isInfoEnabled()) {
            log.info("Role re-initialized");
        }

        return true;
    }

    /**
     * States if role feature map re-initialization required.
     * @return true if re-initialization require, otherwise false
     */
    private boolean isReInitializationRequired() {

        Date lastInitTime = null;

        try {
            SystemProperty lastInitTimeProp = this.getSystemPropertyManager()
                .findByName(SystemPropertyManager.LAST_INIT_TIME);
            if (lastInitTimeProp != null) {
                lastInitTime = DateUtil.getDate(
                    lastInitTimeProp.getValue(), DateUtil.LONG_DATE_FORMAT);
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        }

        if (log.isDebugEnabled()) {
            log.debug("Last init time : " + lastInitTime);
            log.debug("Re-init time : "
                + this.getRoleFeatureMap().getReInitTime());
        }

        if (lastInitTime != null
            && this.getRoleFeatureMap().getReInitTime() != null
            && (lastInitTime
                .compareTo(this.getRoleFeatureMap().getReInitTime()) > 0)) {

            return true;

        } else if (this.getRoleFeatureMap().getReInitTime() == null) {
            return true;
        }

        return false;

    }

    /**
     * Starts hibernate connection in separate thread.
     * @throws Exception if any exceptions occurs.
     */
    private void startSession() throws Exception {
        // The following code simulates what happens in the
        // OpenSessionInViewFilter in the real app.
        Session session = SessionFactoryUtils.getSession(
            this.sessionFactory, true);
        TransactionSynchronizationManager.bindResource(
            sessionFactory, new SessionHolder(session));
    }

    /**
     * Stops the started hibernate session.
     * @throws Exception if any exceptions occurs.
     */
    private void stopSession() throws Exception {
        // The following code simulates what happens in the
        // OpenSessionInViewFilter in the real app.
        Session session = ((SessionHolder) TransactionSynchronizationManager
            .getResource(sessionFactory)).getSession();
        TransactionSynchronizationManager.unbindResource(sessionFactory);
        SessionFactoryUtils.releaseSession(session, sessionFactory);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 