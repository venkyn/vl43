/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;


/**
 * Factory that allows retrieving the singleton RoleFeatureMap without
 * Spring dependency injection. (Needed for access from taglib.)
 *
 * @author ddoubleday
 */
public final class RoleFeatureMapFactory implements BeanFactoryPostProcessor {

    private static RoleFeatureMap rfMap;

    /**
     * Be sure this will be a singleton.
     */
    private RoleFeatureMapFactory() {
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(
        ConfigurableListableBeanFactory beanFactory) throws BeansException {
        rfMap = (RoleFeatureMap) beanFactory.getBean("roleFeatureMap");
    }

    /**
     * @return the RoleFeatureMap from the Spring configuration.
     */
    public static RoleFeatureMap getRoleFeatureMap() {
        return rfMap;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 