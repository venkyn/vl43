/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.helper;

import com.vocollect.epp.util.DataFileReferenceUtil;

import java.io.File;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author
 */
public class NetworkCredentialValidatorHelper extends Validator {
    
    private static final long serialVersionUID = 5619851027861142800L;
    
    public static final String UN_FLD = "USERNAMEFIELD";
    public static final String PW_FLD = "PASSWORDFIELD";
    public static final String VPW_FLD = "VERIFYPASSWORDFIELD";
    public static final String CERT_FLD = "CERTIFICATEFIELD";
    public static final String KEY_FLD = "KEYFIELD";
    public static final String PIN_FLD = "PINFIELD";
    public static final String VPIN_FLD = "VERIFYPINFIELD";
    
    /**
     * @param username .
     * @param password .
     * @param verifyPassword .
     * @param certificate .
     * @param certificateName .
     * @param key .
     * @param keyName .
     * @param isCertEapType .
     * @return .
     */
    public static Map<String, List<String>> validateCredential(String username,
                                                               String password,
                                                               String verifyPassword,
                                                               File certificate,
                                                               String certificateName,
                                                               File key,
                                                               String keyName,
                                                               boolean isCertEapType) {
        Map<String, List<String>> errors = new HashMap<String, List<String>>();
        errors.put(UN_FLD, new ArrayList<String>());
        errors.put(PW_FLD, new ArrayList<String>());
        errors.put(VPW_FLD, new ArrayList<String>());
        errors.put(CERT_FLD, new ArrayList<String>());
        errors.put(KEY_FLD, new ArrayList<String>());
        
        if (!isCertEapType) {
            errors.put(PW_FLD, validatePassword(password, true));
            errors.put(VPW_FLD, validatePassword(verifyPassword, true));
            
            if (errors.get(VPW_FLD).isEmpty() &&
                !password.equals(verifyPassword)) {
                List<String> mismatchError = new ArrayList<String>();
                mismatchError.add("networkCredential.error.password.mismatch");
                errors.put(VPW_FLD, mismatchError);
            }
        }
        
        errors.put(UN_FLD, validateUsername(username));
        
        if (isCertEapType) {
            errors.put(CERT_FLD, validateCertificate(certificate, certificateName));
            errors.put(KEY_FLD, validateKey(key, keyName));
            errors.put(PW_FLD, validatePassword(password, false));
        }
        
        return errors;
    }
    
    /**
     * @param username .
     * @return errors
     */
    private static List<String> validateUsername(String username) {
        List<String> errors = new ArrayList<String>();
        
        if (!exists(username)) {
            errors.add("networkCredential.error.required");
        }
        
        return errors;
    }
    
    /**
     * @param password .
     * @param required .
     * @return errors
     */
    private static List<String> validatePassword(String password, boolean required) {
        List<String> errors = new ArrayList<String>();
        
        if (required && !exists(password)) {
            errors.add("networkCredential.error.required");
        }
        
        return errors;
    }
    
    /**
     * @param certificate .
     * @param fileName .
     * @return .
     */
    private static List<String> validateCertificate(File certificate, String fileName) {
        List<String> errors = new ArrayList<String>();
        
        if (fileName == null ||
            fileName.length() == 0) {
            errors.add("networkCredential.error.required");
            return errors;
        } 
        
        DataFileReferenceUtil certificateData = DataFileReferenceUtil.fromFormFile(certificate);
        if (certificateData == null) {
            errors.add("networkCredential.error.fileNotExist");
        } else if (certificateData.getFileContents() == null ||
                certificateData.getFileContents().length == 0) {
            errors.add("networkCredential.error.noData");
        }
        
        return errors;
    }
    
    /**
     * @param key .
     * @param fileName .
     * @return errors
     */
    private static List<String> validateKey(File key, String fileName) {
        List<String> errors = new ArrayList<String>();
        
        if (fileName != null &&
            fileName.length() > 0) {
            DataFileReferenceUtil keyData = DataFileReferenceUtil.fromFormFile(key);
            if (keyData == null) {
                errors.add("networkCredential.error.fileNotExist");
            } else if (keyData.getFileContents() == null ||
                    keyData.getFileContents().length == 0) {
                errors.add("networkCredential.error.noData");
            }
        }
        
        return errors;
    }
    
    /**
     * @param pin .
     * @param verifyPin .
     * @return errors
     */
    public static Map<String, List<String>> validatePin(String pin, String verifyPin) {
        Map<String, List<String>> errors = new HashMap<String, List<String>>();
        errors.put(PIN_FLD, new ArrayList<String>());
        errors.put(VPIN_FLD, new ArrayList<String>());
        
        errors.put(PIN_FLD, validatePin(pin));
        errors.put(VPIN_FLD, validatePin(verifyPin));
        
        if (errors.get(VPIN_FLD).isEmpty() &&
            !pin.equals(verifyPin)) {
            List<String> mismatchError = new ArrayList<String>();
            mismatchError.add("networkCredential.error.pin.mismatch");
            errors.put(VPIN_FLD, mismatchError);
        }
        
        return errors;
    }
    
    /**
     * @param pin .
     * @return errors
     */
    private static List<String> validatePin(String pin) {
        final int pinMaxLength = 8;
        
        List<String> errors = new ArrayList<String>();

        if (!exists(pin)) {
            errors.add("networkCredential.error.required");
        } else {
            if (!validLength(pin, pinMaxLength)) {
                errors.add("networkCredential.error.maxlength");
            }
            
            // I18NM: Input Validation
            StringCharacterIterator iter = new StringCharacterIterator(pin);
            char ch;
            for (ch = iter.first(); ch != StringCharacterIterator.DONE; ch = iter.next()) {
                if (!Character.isDigit(ch)) {
                    errors.add("networkCredential.error.nondigit");
                    break;
                } 
            }
        }
        
        return errors;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 