/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.helper;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * This class provides basic validation methods for validating object
 * properties and form data.  This serves as the parent class for
 * specific validator objects.
 *
 * @author Gretchen Swecker
 */ 
public class Validator implements java.io.Serializable {

    /**
     * Determine if a string actually exists.
     *
     * @param str The string we are checking
     * @return true if string exists; false if it does not
     */
    public static boolean exists(String str) {
        if ((str == null) || (str.equals(""))) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Determine if a string exceeds a specified length.  It's assumed
     * that the string is not of zero length or null.
     *
     * @param str String we are checking
     * @param maxLength Integer representing the maximum length of the
     * string
     * @return true if string is a valid length; false if it is not
     */
    public static boolean validLength(String str, int maxLength) {
        return validLength(str, 1, maxLength);
    }

    /**
     * Determine if a string exceeds a specified length.  String is not
     * allowed to be null.
     *
     * @param str String we are checking
     * @param minLength Integer representing the minimum length of the
     * string
     * @param maxLength Integer representing the maximum length of the
     * string
     * @return true if string is a valid length; false if it is not
     */
    public static boolean validLength(String str,
                                      int minLength,
                                      int maxLength) {
        if (str == null) {
            return false;
        }
        if ((str.length() < minLength) || (str.length() > maxLength)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Determine if the IP address is valid.  The validity check here
     * is format only, not resolution to determine duplicate IP or
     * anything else of that nature.
     * @param ipAddress the String to verify as a valid ipAddress
     * @return String array containing error messages if any.
     */
    public static String[] validateIpAddress(String ipAddress) {
        final int ipAddressFields = 7; // 4 numbers, 3 decimal points
        final int ipMinValue = 0;
        final int ipMaxValue = 255;
        
        // a valid IP address looks like:
        // int.int.int.int where each int is 0 <= int <= 255
        HashSet<String> errorKeySet = new HashSet<String>();

        if (ipAddress != null) {
            StringTokenizer tokenizer = new StringTokenizer(ipAddress, ".", true);
            if (tokenizer.countTokens() != ipAddressFields) {
                errorKeySet.add("numfields");
            }
            String temp = null;
            while (tokenizer.hasMoreTokens()) {
                temp = tokenizer.nextToken();
                if (!(temp.equals("."))) {
                    try {
                        int num = Integer.parseInt(temp);
                        if ((num < ipMinValue) || (num > ipMaxValue)) {
                            errorKeySet.add("fieldrange");
                        }
                    } catch (NumberFormatException e) {
                        errorKeySet.add("fieldinteger");
                    }
                }

            }
        }
        // Return array of error keys
         String[] errorKeyArray = new String[errorKeySet.size()];
         errorKeySet.toArray(errorKeyArray);
         return errorKeyArray;
     }
     
    /**
     * Validate a String for alpha-numeric characters and additional
     * allowable characters.
     * @param test the String for validation
     * @param extraAllowableChars String of additional allowable characters
     * @return true if test String contains valid characters; false if it does not
     */
    public static boolean validChars(String test, String extraAllowableChars) {
        for (int i = 0; i < test.length(); i++) {
            char ch = test.charAt(i);
            if (!Character.isLetterOrDigit(ch) && (extraAllowableChars.indexOf(ch) == -1)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check a value to be contained within a range inclusive.
     * @param value int value to test
     * @param min int minimum value inclusive
     * @param max int maximum value inclusive
     * @return true if value is within min and max values inclusive, false if it is not
     */
    public static boolean validInRange(int value, int min, int max) {
        if (value >= min && value <= max) {
            return true;
        }
        return false;
    }

    /**
     * Convert an array to a set.
     * @param values String[] of values to convert
     * @return Set of values
     */
    public static Set<String> arrayToSet(String[] values) {
        Set<String> set = new HashSet<String>();
        if (values != null && values.length > 0) {
            for (int i = 0; i < values.length; i++) {
                set.add(values[i]);
            }
        }
        return set;
    }
}



*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 