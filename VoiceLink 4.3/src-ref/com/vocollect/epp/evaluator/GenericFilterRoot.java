/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.evaluator;

import org.json.JSONException;


/**
 * This is an interface for creating different filtering mechanisms. Implementor 
 * can choose a specific filtering technique and can also choose what object is
 * to be filtered. For example, implementor may want to implement a drill down filtering for 
 * Chart, the signature will be
 * 
 * <blockquote>
 * <code>public Class ChartDrillDownFilter<Chart,JSONArray>(){ public JSONArray filter(<code>Chart</code> chartObject){ ... } }
 * </code>
 * </blockquote>
 * 
 * @param <T> Any object that is getting filtered.
 * @param <O> The output object expected from the filtering.
 * 
 * @author kudupi
 */
public interface GenericFilterRoot<T, O> {
    
    /**
     * Filters the data and provides output. The filtering process will
     * depend on the implementation.
     * 
     * @param thisObject The object to be filtered
     * @throws JSONException 
     * @return Output in JSON array.
     */
    public O filter(T thisObject) throws  JSONException;
    
    /**
     * Filters the data based on the parameters provided. The filtering process will
     * depend on the implementation.
     * 
     * @param thisObject The criteria on which filtering will be done.
     * @throws JSONException 
     */
    public void setFilterParameters(T thisObject) throws  JSONException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 