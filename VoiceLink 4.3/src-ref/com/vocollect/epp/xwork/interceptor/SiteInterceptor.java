/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.xwork.interceptor;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.web.action.SiteEnabledAction;

import com.opensymphony.xwork2.ActionInvocation;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * This Interceptor sets the current site and does an access check for site
 * permissions.
 *
 *
 * @author dkertis
 */
public class SiteInterceptor extends AroundInterceptor
    implements ApplicationContextAware {

    private static final Logger log = new Logger(SiteInterceptor.class);

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.ctx = context;

    }

    // Support serialization
    private static final long serialVersionUID = 8716626878888061746L;

    // represents the url of the main home page
    private String homePageURL;

    private ApplicationContext ctx;

    /**
     * Site interceptor is responsible for setting the users current site
     * context.  it will fill a thread local class with the required variables
     * and set the currentSite on the current user object
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = null;
        before(invocation);

        try {

            if (invocation.getAction() instanceof SiteEnabledAction) {
                // get the action making the request
                SiteEnabledAction action = (SiteEnabledAction) invocation.getAction();

                // get the full URI of the request
                String fullURI = action.getRequest().getRequestURI();

                String contextURI = action.getRequest().getContextPath();

                String appHome = contextURI + "/home.action";

                // Get a new instance of SiteContext bean.
                SiteContext siteContext = (SiteContext) ctx.getBean("siteContext");

                // get the current user logged in
                User currentUser = action.getCurrentUser();

                // Assign the context to the ThreadLocal.
                SiteContextHolder.setSiteContext(siteContext);
                // make sure the DAO is set to filter objects
                siteContext.setFilterBySite(true);
                // set the thread local with user info
                siteContext.setContextSites(currentUser);

                // set a property to specify if the user has all site access
                siteContext.setHasAllSiteAccess(currentUser.getAllSitesAccess());

               // see if the table component is overriding the current site to view
                Long currentTagId;
                currentTagId = determineCurrentSite(action, fullURI, appHome, siteContext, currentUser);


                if ((currentTagId.longValue() != Tag.ALL)
                        && (currentTagId.longValue() != Tag.SYSTEM)) {
                    // Only do this check if changing sites
                    // this will reduce unnecessary db queries
                    if ((currentUser.getCurrentSite() != null)
                        && (currentTagId.longValue() != currentUser.getCurrentSite().longValue())) {
                        // first make sure that this is a valid site (the user may have typed
                        //  a random value in the query string
                        // temporarily turn off the filter to check if there is a site the user
                        //  doesn't have access to
                        siteContext.setFilterBySite(false);
                        if (!siteContext.validSiteByTagId(currentTagId)) {
                            log.error("User " + currentUser.getName() + " is accessing a site that "
                                + "does not exist with an ID of " + currentTagId,
                                SystemErrorCode.ENTITY_NOT_FOUND);
                            throw new EntityNotFoundException(Site.class, currentTagId);
                        }
                        siteContext.setFilterBySite(true);
                    }

                    // make sure user has permissions for this site
                    if (!currentUser.hasSiteAccess(currentTagId)) {
                        log.warn("User " + currentUser.getName()
                            + " is attempting invalid access" + "to "
                            + action.getRequest().getRequestURL()
                            + " with using the site ID " + currentTagId);
                        return "errorNoAccess";
                    }

                    // then check to see if the site has chanaged
                    if ((!currentTagId.equals(currentUser.getCurrentSite()))) {
                           // and the site is a valid one to save
                           // update the current user with the new site
                           currentUser.setCurrentSite(currentTagId);

                           // update the db also
                           siteContext.updateUser(currentUser, currentTagId);
                    }
                }
                if (log.isTraceEnabled()) {
                    log.trace("Current site is " + currentTagId);
                }

                // set it in the base action when rendering the combo box
                action.setCurrentSiteTagId(currentTagId);

                // now shove the tag in the siteContext object
                // if the user wants to limit by site

                if (currentTagId.longValue() == Tag.ALL) {
                    siteContext.setToAllSiteMode();
                } else if (currentTagId.longValue() == Tag.SYSTEM) {
                    siteContext.setToSystemMode();
                } else {
                    siteContext.setCurrentSite(siteContext.getSiteByTagId(currentTagId));
                }
            }
            result = invocation.invoke();
        } catch (Exception e) {
            log.error("Exception caught in SiteInterceptor: ", e);
            throw e;
        }

        after(invocation, result);

        return result;
    }

    /**
     * @param action .
     * @param fullURI .
     * @param appHome .
     * @param siteContext .
     * @param currentUser .
     * @return .
     * @throws DataAccessException
     */
    private Long determineCurrentSite(SiteEnabledAction action,
                                      String fullURI,
                                      String appHome,
                                      SiteContext siteContext,
                                      User currentUser) throws DataAccessException {
        Long currentTagId;
        currentTagId = action.getTempSiteID();


            // if the main home page is being requested then set currentSite to "All Site"
        if (fullURI.equals(appHome)) {
            currentTagId = Tag.ALL;
        }

        // if the table component is not forcing a site
        if (currentTagId == null) {
            // first check the query string to see if the user is changing the current site
            currentTagId = action.getCurrentSiteTagId();
        }

        // summaries use site Id instead of tag Id, handle this differently
        if (currentTagId == null) {
            Long currentSiteId = action.getSummarySiteID();
            if (currentSiteId != null) {
                currentTagId = siteContext.getTagBySiteId(currentSiteId).getId();
            }
        }

        // if the user did not specify a specific site to view
        if (currentTagId == null) {

            // the last site the user was viewing is the current site
            currentTagId = currentUser.getCurrentSite();

            // if the user no longer has access to the stored current site value,
            // let's clear it out and let it be treated as if the user has never
            // logged in before -- Jim Geisler 5/30/2008 VVC-1452 fix.
            if ((currentTagId != null) && !currentUser.hasSiteAccess(currentTagId)) {
                currentTagId = null;
            }

        }

        // This prevents a problem where the user's current site is a site that
        // was deleted in a different browser. -- Justin Volz 3/25/2009
        boolean siteExists = true;
        if (currentTagId != null &&
            currentTagId.longValue() != Tag.ALL) {
            try {
                SiteContextHolder.getSiteContext().getSiteByTagId(currentTagId);
            } catch (EntityNotFoundException e){
                siteExists = false;
            }
        }

        // if the user has not logged in before
        if (currentTagId == null ||
            !siteExists) {
            // get the first site in the users list of sites
//                    Tag t = siteContext.getDefaultSite(currentUser);
            Site site = siteContext.getDefaultSiteForUser(currentUser);
            //Site s = (Site) action.getSitesForCurrentUser().toArray()[0];
            currentTagId = siteContext.getTagBySiteId(site.getId()).getId();
        }
        return currentTagId;
    }

    /**
     *
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#after(com.opensymphony.xwork2.ActionInvocation, java.lang.String)
     */
    @Override
    protected void after(ActionInvocation arg0, String arg1) throws Exception {
    }

    /**
     *
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#before(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    protected void before(ActionInvocation arg0) throws Exception {
    }


    /**
     * Getter for the homePageURL property.
     * @return String value of the property
     */
    public String getHomePageURL() {
        return homePageURL;
    }


    /**
     * Setter for the homePageURL property.
     * @param homePageURL the new homePageURL value
     */
    public void setHomePageURL(String homePageURL) {
        this.homePageURL = homePageURL;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 