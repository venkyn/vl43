/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/*
 * Copyright (c) 2010 by OpenSymphony
 * All rights reserved.
 */

package com.vocollect.epp.xwork.interceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;


/**
 * An abstract interceptor that provides simple access to before/after callouts.
 *
 * @author Jason Carreira
 */
public abstract class AroundInterceptor implements Interceptor {

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#destroy()
     */
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#init()
     */
    public void init() {
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = null;

        before(invocation);
        result = invocation.invoke();
        after(invocation, result);

        return result;
    }

    /**
     * Called after the invocation has been executed.
     * @param dispatcher the invocation object.
     * @param result the result value returned by the invocation
     * @throws Exception on any uncaught failure.
     */
    protected abstract void after(ActionInvocation dispatcher, String result) throws Exception;

    /**
     * Called before the invocation has been executed.
     */
    /**
     * @param invocation the invocation object.
     * @throws Exception on any uncaught failure.
     */
    protected abstract void before(ActionInvocation invocation) throws Exception;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 