/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.xwork.interceptor;

import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.ActionInvocation;

/**
 * This Interceptor logs an exception thrown by action.
 *
 *
 * @author sseeram
 */
public class LogExceptionInterceptor extends AroundInterceptor {

    private static final Logger log = new Logger(LogExceptionInterceptor.class);

    // Support serialization
    private static final long serialVersionUID = 8716626878888061746L;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.xwork.interceptor.AroundInterceptor#after(com.opensymphony.xwork2.ActionInvocation, java.lang.String)
     */
    @Override
    protected void after(ActionInvocation arg0, String arg1) throws Exception {
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.xwork.interceptor.AroundInterceptor#before(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    protected void before(ActionInvocation arg0) throws Exception {
    }

    /**
     * This method logs an exception.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    /**
     * This method logs an exception.
     * {@inheritDoc}
     * @see com.vocollect.epp.xwork.interceptor.AroundInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        String result = null;
        before(invocation);
        try {
            result = invocation.invoke();
        } catch (Exception e) {
            log.error("Exception caught in LogExceptionInterceptor: ", e);
            throw e;
        }

        after(invocation, result);
        return result;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 