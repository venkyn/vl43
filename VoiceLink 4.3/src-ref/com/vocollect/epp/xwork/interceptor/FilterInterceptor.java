/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.xwork.interceptor;

import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.Summary;
import com.vocollect.epp.model.TabularSummary;
import com.vocollect.epp.service.FilterManager;
import com.vocollect.epp.web.action.ConfigurableHomepagesAction;
import com.vocollect.epp.web.action.DataProviderAction;

import com.opensymphony.xwork2.ActionInvocation;

import java.util.Collection;
import java.util.Map;


/**
 * 
 *
 * @author jstonebrook
 */
public class FilterInterceptor extends AroundInterceptor {

    // Support serialization
    private static final long serialVersionUID = -1583369459156374130L;

    private FilterManager filterManager;
    

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    public String intercept(ActionInvocation arg0) throws Exception {
        return super.intercept(arg0);
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#after(com.opensymphony.xwork2.ActionInvocation, java.lang.String)
     */
    @Override
    protected void after(ActionInvocation invocation, String arg1) throws Exception {
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.AroundInterceptor#before(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    protected void before(ActionInvocation invocation) throws Exception {
        if (invocation.getAction() instanceof ConfigurableHomepagesAction) {
            // We have a configurable home pages action - we need to figure out
            // what summaries are on this action and then get the view ids from the
            // summaries and use that to construct our filters properly
            ConfigurableHomepagesAction action = (ConfigurableHomepagesAction) invocation.getAction();
            action.getAdminHomeSummaries();

            Map<String, Summary> summaries = action.getSummaries();
            Collection<Summary> summaryInstances = summaries.values();
            for (Summary summary : summaryInstances) {
                if (summary instanceof TabularSummary) {
                    TabularSummary tabularSummary = (TabularSummary) summary;
                    // We have a Table Component - so we need to determine
                    // the view id
                    Filter filter = getFilterManager().findByUserIdAndViewId(
                        action.getCurrentUser().getId(),
                        tabularSummary.getViewId());
                    action.getFilters().add(filter);
                }
            }

        } else if (invocation.getAction() instanceof DataProviderAction) {
            DataProviderAction dataProviderAction = (DataProviderAction) invocation.getAction();
            
            if (dataProviderAction.getSubmittedFilterCriterion() != null
                && dataProviderAction.getSubmittedFilterCriterion().length() > 0) {
                // A filter has been bookmarked or smart filter passed into this page - 
                // so we need to use it
                
                dataProviderAction.setFilters(getFilterManager().
                    constructFilterFromSerializedJSON(dataProviderAction.getSubmittedFilterCriterion()));

                // We should also save it
                getFilterManager().addFilters(dataProviderAction.getFilters(),
                                              dataProviderAction.getCurrentUser());
                
            } else {
                // No passed in filter criterion - therefore we need to query the db for
                // the user / view / filter relationship and see if a filter exists
                for (Long tmpViewId : dataProviderAction.getViewIds()) {
                    Filter filter = getFilterManager().findByUserIdAndViewId(
                        dataProviderAction.getCurrentUser().getId(),
                        tmpViewId);
                    dataProviderAction.getFilters().add(filter);
                }
            }
        } 
    }

    
    /**
     * Getter for the filterManager property.
     * @return FilterManager value of the property
     */
    public FilterManager getFilterManager() {
        return this.filterManager;
    }

    
    /**
     * Setter for the filterManager property.
     * @param filterManager the new filterManager value
     */
    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 