/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.xwork.interceptor;

import com.vocollect.epp.logging.Logger;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.NoParameters;
import com.opensymphony.xwork2.interceptor.ParametersInterceptor;
import com.opensymphony.xwork2.util.InstantiatingNullHandler;
import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.XWorkConverter;
import com.opensymphony.xwork2.util.XWorkMethodAccessor;
import java.util.Iterator;
import java.util.Map;


/**
 * This extension of the base interceptor makes sure that all parameters
 * of type String or arrays of Strings are trimmed of leading/trailing
 * white space.
 *
 * @author ddoubleday
 */
public class TrimmingParametersInterceptor extends ParametersInterceptor {

    private static final long serialVersionUID = -4386945706980778523L;

    private static final Logger log = new Logger(TrimmingParametersInterceptor.class);

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.ParametersInterceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    @Override
    public String intercept(ActionInvocation arg0) throws Exception {
        // TODO Auto-generated method stub
        before(arg0);
        return super.intercept(arg0);
    }

    /**
     * The build method had been overridden from the ParametersInterceptor class to avoid
     * displaying the log.debug messages in the vocolloct.log file.
     * This was done while resolving EPP-242 that read "The confirmPassword field is captured
     * and displayed in Vocollect.log"
     * If there is a necessity where the other information related to the User form needs to be
     * appended in the debug list, it could be achieved by removing the password data from the
     * parameters variable in the method.
     * Date 15-Nov-06
     * Modified by - Sudip Acharya
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.ParametersInterceptor#before(com.opensymphony.xwork2.ActionInvocation)
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void before(ActionInvocation invocation) throws Exception {
        if (!(invocation.getAction() instanceof NoParameters)) {
            final Map parameters = ActionContext.getContext().getParameters();

            ActionContext invocationContext = invocation.getInvocationContext();

            try {
                invocationContext.put(
                    InstantiatingNullHandler.CREATE_NULL_OBJECTS, Boolean.TRUE);
                invocationContext.put(
                    XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.TRUE);
                invocationContext.put(
                    XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.TRUE);

                if (parameters != null) {
                    final ValueStack stack = ActionContext.getContext()
                        .getValueStack();

                    for (Iterator<Map.Entry> iterator = parameters.entrySet().iterator(); iterator
                        .hasNext();) {
                        Map.Entry entry = iterator.next();
                        String name = entry.getKey().toString();
                        if (acceptableName(name)) {
                            Object value = entry.getValue();
                            stack.setValue(name, value);
                        }
                    }
                }
            } finally {
                invocationContext
                    .put(
                        InstantiatingNullHandler.CREATE_NULL_OBJECTS,
                        Boolean.FALSE);
                invocationContext.put(
                    XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.FALSE);
                invocationContext.put(
                    XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.FALSE);
            }
        }
    }

    /**
     * Trim string parameters before calling the superclass method.
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.ParametersInterceptor#setParameters
     *      (java.lang.Object, com.opensymphony.xwork2.util.ValueStack,
     *      java.util.Map)
     */
    @Override
    @SuppressWarnings({ "unchecked", "rawtypes" })
    protected void setParameters(Object action,
                                 ValueStack stack,
                                 Map parameters) {

        for (Iterator paramIter = parameters.entrySet().iterator(); paramIter.hasNext();) {
            Map.Entry entry = (Map.Entry) paramIter.next();
            Object value = entry.getValue();
            if (value instanceof String) {
                if (log.isTraceEnabled()) {
                    log.trace("Setting parameter " + entry.getKey() + ": " + value);
                }
                entry.setValue(((String) value).trim());
            } else if (value instanceof String[]) {
                String[] values = (String[]) value;
                for (int i = 0; i < values.length; i++) {
                    if (log.isTraceEnabled()) {
                        log.trace("Setting parameter " + entry.getKey() + ": " + values[i]);
                    }
                    values[i] = values[i].trim();
                }
            } else {
                if (log.isTraceEnabled()) {
                    log.trace("Setting parameter " + entry.getKey() + ": " + value);
                }
            }
        }
        super.setParameters(action, stack, parameters);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 