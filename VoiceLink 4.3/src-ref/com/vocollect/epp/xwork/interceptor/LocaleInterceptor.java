/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.xwork.interceptor;


import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.LocaleAdapter;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * This interceptor places the current Locale in the ThreadLocal object
 * wrapped by {@link LocaleContextHolder}. Using the LocaleContextHolder
 * rather than the {@link ActionContext} means that code can look in the
 * same place for the Locale, regardless of whether it was called from a
 * Struts action or not.
 *
 * @author ddoubleday
 */
public class LocaleInterceptor implements Interceptor {

    private static final long serialVersionUID = 6394308999772062181L;

    private static final Logger log = new Logger(LocaleInterceptor.class);

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#init()
     */
    public void init() {
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#destroy()
     */
    public void destroy() {
    }

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.interceptor.Interceptor#intercept(com.opensymphony.xwork2.ActionInvocation)
     */
    public String intercept(ActionInvocation invocation) throws Exception {

        Locale locale = invocation.getInvocationContext().getLocale();
        Locale adjustedLocale = LocaleAdapter.adjustLocale(locale);

        if (locale == null) {
            // Use the adjusted Locale
            log.warn("Browser did not send Locale--using fallback " + adjustedLocale);
        }

        if (log.isTraceEnabled()) {
            log.trace("Saving locale " + adjustedLocale + " in LocaleContextHolder.");
        }

        // save it in a ThreadLocal and let child threads inherit
        LocaleContextHolder.setLocale(adjustedLocale, true);
        // Save it in Struts context, too
        invocation.getInvocationContext().setLocale(adjustedLocale);

        final String result = invocation.invoke();

        return result;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 