/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * View class - Represents a view in the GUI.
 * 
 */
public class ViewRoot extends BaseModelObject {

    private static final long serialVersionUID = -7698234690627067914L;

    private String name;
    
    private String className;

    /**
     * @return The class this view represents.
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className The name of the class this view represents.
     */
    public void setClassName(String className) {
        this.className = className;
    }
    private String dataBeanName;
    private String dataMethodName;

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof View)) {
            return false;
        }
        final View type = (View) o;
        if (name != null) {
            return name.equals(type.getName());
        } else {
            return (type.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.name).append(
            "id", getId()).toString();
    }

    /**
     * Getter for the name property.
     * @return the name property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name of the view
     */
    public void setName(String name) {
        this.name = name;
    }


    /** 
     * returns the name of the bean the DataProvider will use to get data.
     * @return the name
     */
    public String getDataBeanName() {
        return dataBeanName;
    }

    
    /**
     * Sets the bean the data provider will use to get the data.
     * @param dataBeanName - name of the bean
     */
    public void setDataBeanName(String dataBeanName) {
        this.dataBeanName = dataBeanName;
    }

    /** 
     * returns the name of the method the DataProvider will call to get data.
     * @return the name
     */
    public String getDataMethodName() {
        return dataMethodName;
    }

    
    /**
     * @param dataMethodName name of method that returns data to the DataProvider
     */
    public void setDataMethodName(String dataMethodName) {
        this.dataMethodName = dataMethodName;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 