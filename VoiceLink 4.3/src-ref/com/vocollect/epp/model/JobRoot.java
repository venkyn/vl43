/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;



/**
 * This class defines the model of a schedule.
 * 
 * @author jkercher
 * 
 */
public class JobRoot extends VersionedModelObject implements Serializable {

    private static final Logger log = new Logger(Job.class);

    /** JobDataMap key for the schedule name. */
    public static final String JOB_NAME = "name";

    /** JobDataMap key for the schedule type. */
    // public static final String SCHEDULE_TYPE = "type";

    /** JobDataMap key for the next execution timestamp. */
    public static final String NEXT_RUN = "nextRun";

    /** JobDataMap key for the last execution's ending timestamp. */
    public static final String LAST_RUN = "lastRun";
    
    /** JobDataMap key for the last execution timestamp. */
    public static final String LAST_STARTED = "lastStarted";

    /** JobDataMap key for the last run result. */
    public static final String LAST_RUN_RESULT = "lastRunResult";

    /** JobDataMap key for the last run message. */
    public static final String LAST_RUN_MESSAGE = "lastRunMessage";

    /** JobDataMap key for job failure message. */
    public static final String FAILURE_MESSAGE = "FailureMessage";

    /** JobDataMap key for job failure exception - DataAccessException. */
    public static final String FAILURE_EXCEPTION = "FailureException";

    /** JobDataMap key for job history - DataAccessException. */
    public static final String JOB_HISTORY = "JobHistory";

    public static final String ENABLED = "schedule.status.true";

    public static final String DISABLED = "schedule.status.false";

    public static final String CRON_JOB_HRS = "CronJobHrs";
    
    public static final String RUN_THIS_JOB = "Run This Job";

    public static final int CONTINUOUS_RUN = -1;
    
    public static final String PING_INTERVAL = "pingInterval";
    
    public static final long DEFAULT_PING = 10000L;
    
    public static final String HOUR_AM = "AM"; 

    public static final String HOUR_PM = "PM"; 

    public static final String HOUR_24 = "24Hr"; 

    // Unique ID for class serialization.
    private static final long serialVersionUID = -5003634036266454570L;

    // Job name; identifies the client process.
    private String name;

    // Identifies the schedule type; supported types are interval
    // and daily.
    private ScheduleType type;

    // Indicates whether the schedule is enabled (disabled schedules
    // never trigger their client process).
    private Boolean enabled = true;

    // Indicates the next time the client process wil be triggered.
    private Date nextRun;

    // Indicates the last time the client process terminated(execution ended).
    private Date lastRun;
    
    // Indicates the last time the client process was triggered.
    private Date lastStarted;

    // Indicates the result of the last client process execution.
    private ScheduleLastResult lastRunResult;

    // The result of the run.
    private String lastRunMessage;
    
    private boolean running;
    
    private long pingInterval;
    
    private long lastPing;

    // Unique identifier; this is a simple hashcode of the
    // class name until persistence is implemented.
    // private Long id = -1L;

    // The JobDetail instance.
    private JobDetail jobDetail;

    private int repeatCount;
    
    private long interval;
    private String cronExpression;
    private String cronHrs = "";
    private String displayName;
    private String jobClass;

    /** Group name for all visible schedules. */
    public static final String PUBLIC_GROUP_NAME = "PUBLIC";

    /** Group name for all invisible schedules. */
    public static final String PRIVATE_GROUP_NAME = "PRIVATE";
    
    /** Group name for all invisible schedules. */
    public static final String MONITOR_GROUP_NAME = "MONITOR";
    
    /**
     * Hibernate Constructor.
     */
    public JobRoot() {
    }

    /**
     * This constructor should only be used on startup from the configuration
     * file. Use the <code>(JobDetail, Job)</code> constructor whenever
     * possible.
     * 
     * @param jobDetail Quartz object that contains properties (both framework
     *            and custom) describing a given job.
     */
    public JobRoot(JobDetail jobDetail) {
        buildJob(jobDetail);

        this.setName(jobDetail.getName());
        this.setDisplayName(jobDetail.getJobDataMap().getString(JOB_NAME));
        this.setJobClass(jobDetail.getJobClass().getName());
        this.setEnabled(true);
    }

    /**
     * Helper method that updates the Job with Quartz information.
     * @param detail the job detail
     */
    private void buildJob(JobDetail detail) {
        this.jobDetail = detail;
        JobDataMap jobData = jobDetail.getJobDataMap();

        if (jobData.containsKey(NEXT_RUN)) {
            this.setNextRun((Date) jobData.get(NEXT_RUN));
        } else {
            this.setNextRun(null);
        }
        if (jobData.containsKey(LAST_RUN)) {
            this.setLastRun((Date) jobData.get(LAST_RUN));
        } else {
            this.setLastRun(null);
        }
        if (jobData.containsKey(LAST_STARTED)) {
            this.setLastStarted((Date) jobData.get(LAST_STARTED));
        } else {
            this.setLastStarted(null);
        }
        if (jobData.containsKey(LAST_RUN_RESULT)) {
            this.setLastRunResult((ScheduleLastResult) jobData.get(LAST_RUN_RESULT));
        } else {
            this.setLastRunResult(null);
        }
        if (jobData.containsKey(LAST_RUN_MESSAGE)) {
            this.setLastRunMessage(jobData.getString(LAST_RUN_MESSAGE));
        } else {
            this.setLastRunMessage(null);
        }
        if (jobData.containsKey(CRON_JOB_HRS)) {
            this.setCronHrs(jobData.getString(CRON_JOB_HRS));
        } else {
            this.setCronHrs(HOUR_24);
        }
        if (jobData.containsKey(PING_INTERVAL)) {
            this.setPingInterval(Long.parseLong(jobData.getString(PING_INTERVAL)));
        } else {
            this.setPingInterval(DEFAULT_PING);
        }
    }

    /**
     * Updates this instance with Quartz data.
     * @param detail the job detail
     */
    public void setDetail(JobDetail detail) {
        buildJob(detail);
    }

    /**
     * Getter method for the job name property.
     * 
     * @return the job name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter method for the job name property.
     * 
     * @param name the job name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for the last execution's ending timestamp.
     * 
     * @return the last run timestamp
     */
    public Date getLastRun() {
        return this.lastRun;
    }
    
    /**
     * Getter method for the last started timestamp.
     * 
     * @return the last run timestamp
     */
    public Date getLastStarted() {
        return this.lastStarted;
    }

    /**
     * Setter method for the last execution timestamp property.
     * 
     * @param lastRun sets the last run timestamp
     */
    public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }
    
    /**
     * Setter method for the last started timestamp property.
     * 
     * @param lastStarted sets the last run start timestamp
     */
    public void setLastStarted(Date lastStarted) {
        this.lastStarted = lastStarted;
    }

    /**
     * Getter method for the last run result.
     * 
     * @return the last run result
     */
    public ScheduleLastResult getLastRunResult() {
        return this.lastRunResult;
    }

    /**
     * Setter method for the last run result.
     * 
     * @param lastRunResult specifies the last run result
     */
    public void setLastRunResult(ScheduleLastResult lastRunResult) {
        this.lastRunResult = lastRunResult;
    }

    /**
     * Getter method for the next execution property.
     * 
     * @return the timestamp of the next scheduled execution
     */
    public Date getNextRun() {
        return this.nextRun;
    }

    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     */
    public String getDisplayNextRun() {
        return DateUtil.formatTimestampForTableComponent(this.getNextRun());
    }

    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     */
    public String getDisplayLastRun() {
        return DateUtil.formatTimestampForTableComponent(this.getLastRun());
    }

    
    /**
     * Returns a formatted string for display purposes.
     * @return the formatted string
     */
    public String getDisplayLastStarted() {
        return DateUtil.formatTimestampForTableComponent(this.getLastStarted());
    }
    
    /**
     * Setter method for the next execution property.
     * 
     * @param nextRun specifies the timestamp of the next scheduled execution
     */
    public void setNextRun(Date nextRun) {
        if (jobDetail != null) {
            jobDetail.getJobDataMap().put(NEXT_RUN, nextRun);
        }
        this.nextRun = nextRun;
    }

    /**
     * Getter method for the enabled property.
     * 
     * @return whether the schedule is enabled
     */
    public Boolean getEnabled() {
        return this.enabled;
    }

    /**
     * Setter method for the enabled property.
     * 
     * @param enabled specifies whether the schedule is enabled
     */
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Getter method for the schedule type property.
     * 
     * @return the schedule type
     */
    public ScheduleType getType() {
        return this.type;
    }

    /**
     * Setter method for the schedule type property.
     * 
     * @param type specifies a schedule type
     */
    public void setType(ScheduleType type) {
        this.type = type;
    }

    /**
     * Gets the group name for the job detail.
     * @return the job detail group
     */
    public String getGroup() {
        String group = "";
        try {
            group = getJobDetail().getGroup();
        } catch (DataAccessException dae) {
            dae.printStackTrace();
        }
        return group;
    }

    /**
     * Sets the trigger information for a new job.
     * @param triggers the triggers
     */
    public void setTriggerInformation(List<Trigger> triggers) {
        for (Trigger trigger : triggers) {
            setTriggerInformation(trigger);
        }
    }

    /**
     * Sets the trigger information for a new job.
     * @param trigger the trigger
     */
    public void setTriggerInformation(Trigger trigger) {
        setNextRun(trigger.getNextFireTime());
        if (trigger instanceof SimpleTrigger) {
            SimpleTrigger st = (SimpleTrigger) trigger;
            setInterval(st.getRepeatInterval());
            setType(ScheduleType.INTERVAL);
            setRepeatCount(st.getRepeatCount());
            // log.debug("Job - " + getName() + " -- interval - " +
            // st.getRepeatInterval());
        } else if (trigger instanceof CronTrigger) {
            CronTrigger ct = (CronTrigger) trigger;
            setCronExpression(ct.getCronExpression());
            setType(ScheduleType.DAILY);
        }
    }

    /**
     * Standard equals() implementation to compare object state.
     * 
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Job)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Job target = (Job) obj;

        return new EqualsBuilder().append(this.getName(), target.getName())
            .append(this.getType(), target.getType()).append(
                this.getId(), target.getId()).append(
                this.getEnabled(), target.getEnabled()).append(
                this.getDisplayName(), target.getDisplayName()).append(
                this.getJobClass(), target.getJobClass()).append(
                this.getInterval(), target.getInterval()).append(
                this.getCronExpression(), target.getCronExpression()).append(
                this.getCronHrs(), target.getCronHrs()).isEquals();
    }

    /**
     * Standard hashcode() implementation.
     * 
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        // All fields used in equal()s method should be used here
        // as well.
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getName())
            .append(this.getType()).append(this.getId()).append(
                this.getEnabled()).append(this.getDisplayName()).append(
                this.getJobClass()).append(this.getInterval()).append(
                this.getInterval()).append(this.getCronExpression()).append(
                this.getCronHrs()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     * 
     * @return String representation of the given schedule.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.getName()).append(
            "id", this.getId()).append("type", this.getType()).append(
            "enabled", this.getEnabled()).append("nextRun", this.getNextRun())
            .append("lastRun", this.getLastRun()).append(
                "lastRunResult", this.getLastRunResult()).toString();
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return ResourceUtil.getLocalizedKeyValue(this.getDisplayName());
    }

    /**
     * Gets the job detail.
     * @return the <code>JobDetail</code>
     * @throws DataAccessException if the <code>JobDetail</code> cannot be
     *             created.
     */
    public JobDetail getJobDetail() throws DataAccessException {
        try {
            if (jobDetail == null) {
                if (log.isTraceEnabled()) {
                    log.trace("Creating job detail from scratch");
                }
                jobDetail = new JobDetail(getName(), PUBLIC_GROUP_NAME, Class
                    .forName(this.getJobClass()));
            }
        } catch (ClassNotFoundException e) {
            
            throw new DataAccessException();
        }

        return jobDetail;
    }

    /**
     * Getter for the lastRunMessage property.
     * @return String value of the property
     */
    public String getLastRunMessage() {
        return this.lastRunMessage;
    }

    /**
     * Setter for the lastRunMessage property.
     * @param lastRunMessage the new lastRunMessage value
     */
    public void setLastRunMessage(String lastRunMessage) {
        this.lastRunMessage = lastRunMessage;
    }

    /**
     * Getter for the interval property.
     * @return long value of the property
     */
    public long getInterval() {
        return this.interval;
    }

    /**
     * Setter for the interval property.
     * @param interval the new interval value
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * Getter for the cronTime property.
     * @return Date value of the property
     */
    public String getCronExpression() {
        return this.cronExpression;
    }

    /**
     * Setter for the cronTime property.
     * @param cronExpression the new cronExpression
     */
    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    /**
     * Getter for the displayName property.
     * @return String value of the property
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Setter for the displayName property.
     * @param displayName the new displayName value
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * Getter for the jobClass property.
     * @return String value of the property
     */
    public String getJobClass() {
        return this.jobClass;
    }

    /**
     * Setter for the jobClass property.
     * @param jobClass the new jobClass value
     */
    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }

    /**
     * Getter for the cronHrs property.
     * @return String value of the property
     */
    public String getCronHrs() {
        return this.cronHrs;
    }

    /**
     * Setter for the cronHrs property.
     * @param cronHrs the new cronHrs value
     */
    public void setCronHrs(String cronHrs) {
        if (jobDetail != null) {
            jobDetail.getJobDataMap().put(CRON_JOB_HRS, cronHrs);
        }
        this.cronHrs = cronHrs;
    }

    
    /**
     * Getter for the running property.
     * @return boolean value of the property
     */
    public boolean isRunning() {
        return this.running;
    }

    
    /**
     * Setter for the running property.
     * @param running the new running value
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    
    /**
     * Getter for the pingInterval property.
     * @return long value of the property
     */
    public long getPingInterval() {
        return this.pingInterval;
    }

    
    /**
     * Setter for the pingInterval property.
     * @param pingInterval the new pingInterval value
     */
    public void setPingInterval(long pingInterval) {
        this.pingInterval = pingInterval;
    }

    
    /**
     * Getter for the lastPing property.
     * @return long value of the property
     */
    public long getLastPing() {
        return this.lastPing;
    }

    
    /**
     * Setter for the lastPing property.
     * @param lastPing the new lastPing value
     */
    public void setLastPing(long lastPing) {
        this.lastPing = lastPing;
    }

    
    /**
     * Getter for the repeatCount property.
     * @return int value of the property
     */
    public int getRepeatCount() {
        return this.repeatCount;
    }

    
    /**
     * Setter for the repeatCount property.
     * @param repeatCount the new repeatCount value
     */
    public void setRepeatCount(int repeatCount) {
        this.repeatCount = repeatCount;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 