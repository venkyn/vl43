/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Date;





/**
 * Model object representing a translation of user data.
 *
 * @author ddoubleday
 */
public class DataTranslationRoot extends BaseModelObject {

    private static final long serialVersionUID = -4954582194496884605L;

    private String  key;

    private String  locale;

    private String  translation;

    //Using a date for versioning instead of standard long. This is
    //being done so we can determine when a translation was last modified
    //so we can determine if it needs reloaded in cache
    private Date    modified;



    /**
     * This getter is a dummy getter for the model mapping to allow
     * us to put an index on the modified field.
     *
     * @return Date value of the property
     */
    private Date getModifiedIndex() {
        return modified;
    }


    /**
     * This setter is a dummy setter for the model mapping to allow
     * us to put an index on the modified field.
     *
     * @param modifiedIndex the new modified value
     */
    private void setModifiedIndex(Date modifiedIndex) {
        // DO NOTHING HERE, NOT A REAL SETTER;
    }

    /**
     * Getter for the modified property.
     * @return Date value of the property
     */
    public Date getModified() {
        return modified;
    }


    /**
     * Setter for the modified property.
     * @param modified the new modified value
     */
    public void setModified(Date modified) {
        this.modified = modified;
    }

    /**
     * Getter for the key property.
     * @return String value of the property
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Getter for the locale property.
     * @return String value of the property
     */
    public String getLocale() {
        return this.locale;
    }

    /**
     * Getter for the translation property.
     * @return String value of the property
     */
    public String getTranslation() {
        return this.translation;
    }

    /**
     * Setter for the key property.
     * @param key the new key value
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Setter for the locale property.
     * @param locale the new locale value
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Setter for the translation property.
     * @param translation the new translation value
     */
    public void setTranslation(String translation) {
        this.translation = translation;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getKey();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((locale == null) ? 0 : locale.hashCode());
        result = prime * result + ((translation == null) ? 0 : translation.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof DataTranslation)) {
            return false;
        }
        final DataTranslation other = (DataTranslation) obj;
        if (key == null) {
            if (other.getKey() != null) {
                return false;
            }
        } else if (!key.equals(other.getKey())) {
            return false;
        }
        if (locale == null) {
            if (other.getLocale() != null) {
                return false;
            }
        } else if (!locale.equals(other.getLocale())) {
            return false;
        }
        if (translation == null) {
            if (other.getTranslation() != null) {
                return false;
            }
        } else if (!translation.equals(other.getTranslation())) {
            return false;
        }

        return true;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 