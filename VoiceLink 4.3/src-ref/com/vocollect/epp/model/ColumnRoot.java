/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.util.StringUtil;

import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Column class - represents a Column in a view.
 * @author dkertis
 */
public class ColumnRoot extends BaseModelObject {

    private static final long serialVersionUID = -7698234690627067914L;

    private View view;
    
    private Set<UserColumn> userColumn;
    
    private String field;

    private String display;

    private ColumnSortType sortType;
    
    private Integer operandType;
    
    private boolean required;
    
    private boolean displayable;

    private String displayParam;
    
    private String displayFunction;
    
    private String painterFunction;

    private String toolTipFunction;

    private DataType dataType;

    private int defaultWidth;

    private boolean defaultVisible;

    private boolean defaultSorted;

    private boolean defaultSortAsc;

    private int defaultOrder;
    
    private String extraFields;
    
    private String extraFilterInfo;
    
    private ColumnFilterType filterType;
    
    //this was added as a possible solution to the filtering-on-a-List problem.
    private String filterEnumType;
    
    // This is the root of a key used to translate options for booleans. Could
    // be applied to enums.
    private String optionKeyPrefix;

    private Short filterAutoCompleteDisable;

    /**
     * Getter for the extra fields.
     * @return the extra fields, as a comma-separated String.
     */
    public String getExtraFields() {
        return extraFields;
    }
    
    /**
     * Alternate getter for the extra fields.
     * @return the extra fields as an array of Strings, or null if the
     * extraFields String is null or empty.
     */
    public String[] getExtraFieldSet() {
        if (!StringUtil.isNullOrEmpty(extraFields)) {
            return extraFields.split(",");
        } else {
            return null;
        }
    }
    
    /**
     * Setter for the extra fields. These are the extra fields that must be
     * retrieved in order to display this column (as a hyperlink, e.g.).
     * @param extraFields the comma-separated list of extra fields.
     */
    public void setExtraFields(String extraFields) {
        this.extraFields = extraFields;
    }

    
    /**
     * Getter for the extraFilterInfo property.
     * @return String value of the property
     */
    public String getExtraFilterInfo() {
        return this.extraFilterInfo;
    }

    
    /**
     * Setter for the extraFilterInfo property.
     * @param extraFilterInfo the new extraFilterInfo value
     */
    public void setExtraFilterInfo(String extraFilterInfo) {
        this.extraFilterInfo = extraFilterInfo;
    }

    /**
     * @return the view the column resides in 
     */
    public View getView() {
        return view;
    }

    /**
     * @param view - the view the column resides in 
     */
    public void setView(View view) {
        this.view = view;
    }
   
    /**
     * @return UserColumn to override column values
     */
    public Set<UserColumn> getUserColumn() {
        return userColumn;
    }

    /**
     * @param userColumn - UserColumn to override column values
     */
    public void setUserColumn(Set<UserColumn> userColumn) {
        this.userColumn = userColumn;
    }

    /**
     * @return - Java object field name of column
     */
    public String getField() {
        return field;
    }

    /**
     * @param field - Java object field name of column
     */
    public void setField(String field) {
        this.field = field;
    }

    /**
     * @return - Display text for a heading
     */
    public String getDisplay() {
        return display;
    }

    /**
     * @param display - Display text for a heading
     */
    public void setDisplay(String display) {
        this.display = display;
    }

    
    /**
     * Getter for the sortType property.
     * @return ColumnSortType value of the property
     */
    public ColumnSortType getSortType() {
        return this.sortType;
    }

    
    /**
     * Setter for the sortType property.
     * @param sortType the new sortType value
     */
    public void setSortType(ColumnSortType sortType) {
        this.sortType = sortType;
    }

    /**
     * @return - function name to display cell
     */
    public String getDisplayFunction() {
        return displayFunction;
    }

    /**
     * @param displayFunction - function name to display cell
     */
    public void setDisplayFunction(String displayFunction) {
        this.displayFunction = displayFunction;
    }

    /**
     * @return - function to render tooltip
     */
    public String getToolTipFunction() {
        return toolTipFunction;
    }

    /**
     * @param toolTipFunction - function to render tooltip
     */
    public void setToolTipFunction(String toolTipFunction) {
        this.toolTipFunction = toolTipFunction;
    }
   
    /**
     * @return type of data displayed in column
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * @param dataType - type of data displayed in column
     */
    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    /**
     * @return - order of the column in the view
     */
    public int getOrder() {
        // Return userColumn if they exist. otherwise, return the defaults.
        if (!userColumn.isEmpty()) {
            return ((UserColumn) (userColumn.toArray()[0])).getOrder();
        } else {
            return this.defaultOrder;
        }
    }

    /**
     * @return - the sort order if sorting is enabled
     */
    public boolean isSortAsc() {
        // Return userColumn if they exist. otherwise, return the defaults.
        if (!userColumn.isEmpty()) {
            return ((UserColumn) (userColumn.toArray()[0])).isSortAsc();
        } else {
            return this.defaultSortAsc;
        }
    }

    /**
     * @return specifies whether sorting is enabled
     */
    public boolean isSorted() {
        // Return userColumn if they exist. otherwise, return the defaults.
        if (!userColumn.isEmpty()) {
            return ((UserColumn) (userColumn.toArray()[0])).isSorted();
        } else {
            return this.defaultSorted;
        }
    }

    /**
     * @return specifies whether the column is visible
     */
    public boolean isVisible() {
        // Return userColumn if they exist. otherwise, return the defaults.
        if (!userColumn.isEmpty()) {
            return ((UserColumn) (userColumn.toArray()[0])).isVisible();
        } else {
            return this.defaultVisible;
        }
    }

    /**
     * @return specifies the width of the column
     */
    public int getWidth() {
        // Return userColumn if they exist. otherwise, return the defaults.
        if (!userColumn.isEmpty()) {
            return ((UserColumn) (userColumn.toArray()[0])).getWidth();
        } else {
            return this.defaultWidth;
        }
    }
   
    /**
     * @return Column ordering in view
     */
    public int getDefaultOrder() {
        return defaultOrder;
    }

    /**
     * @param colOrder - Column ordering in view
     */
    public void setDefaultOrder(int colOrder) {
        this.defaultOrder = colOrder;
    }

    /**
     * @return default sort order if sorted
     */
    public boolean isDefaultSortAsc() {
        return defaultSortAsc;
    }
    
    /**
     * @param colSortAsc - default sort order if sorted
     */
    public void setDefaultSortAsc(boolean colSortAsc) {
        this.defaultSortAsc = colSortAsc;
    }

    /**
     * @return the default of whether the column is sorted
     */
    public boolean isDefaultSorted() {
        return defaultSorted;
    }

    /**
     * @param colSorted - whether the column is sorted
     */
    public void setDefaultSorted(boolean colSorted) {
        this.defaultSorted = colSorted;
    }

    /**
     * Getter for the defaultVisible property.
     * @return visibility of column
     */
    public boolean isDefaultVisible() {
        return defaultVisible;
    }

    /**
     * Setter for the defaultVisible property.
     * @param colVisible visibility of column
     */
    public void setDefaultVisible(boolean colVisible) {
        this.defaultVisible = colVisible;
    }

    /**
     * Getter for the defaultWidth property.
     * @return default width of a column
     */
    public int getDefaultWidth() {
        return defaultWidth;
    }

    /**
     * Setter for the defaultWidth property.
     * @param colWidth - default width of a column
     */
    public void setDefaultWidth(int colWidth) {
        this.defaultWidth = colWidth;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Column)) {
            return false;
        }
        final Column col = (Column) o;
        if (field != null) {
            return field.equals(col.getField());
        } else {
            return (col.getField() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (field != null ? field.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("field", this.field).append(
            "id", getId()).toString();
    }

    /**
     * @return gets the painter function
     */
    public String getPainterFunction() {
        return painterFunction;
    }

    /**
     * @param painterFunction - new painter function value
     */
    public void setPainterFunction(String painterFunction) {
        this.painterFunction = painterFunction;
    }

    
    /**
     * @return whether or not the column is required.
     */
    public boolean isRequired() {
        return required;
    }

    
    /**
     * @param required whether or not the column is required.
     */
    public void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * Describes whether a column is able to be added or removed.
     * @return boolean value
     */
    public boolean isDisplayable() {
        return displayable;
    }

    /**
     * Sets whether a column is able to be added or removed.
     * @param displayable the flag
     */
    public void setDisplayable(boolean displayable) {
        this.displayable = displayable;
    }

    /**
     * Gets the operand type for this column.
     * @return int the type value
     */
    public Integer getOperandType() {
        return operandType;
    }

    /**
     * Sets the operand type for this column.
     * @param operandType type of operand
     */
    public void setOperandType(Integer operandType) {
        this.operandType = operandType;
    }

    
    /**
     * @return the displayParam property.
     */
    public String getDisplayParam() {
        return displayParam;
    }

    
    /**
     * Setter for the displayParam property.
     * @param displayParam the new value.
     */
    public void setDisplayParam(String displayParam) {
        this.displayParam = displayParam;
    }

    
    /**
     * Getter for the filterType property.
     * @return ColumnFilterType value of the property
     */
    public ColumnFilterType getFilterType() {
        return this.filterType;
    }

    
    /**
     * Setter for the filterType property.
     * @param filterType the new filterType value
     */
    public void setFilterType(ColumnFilterType filterType) {
        this.filterType = filterType;
    }
    
    //these were added as part of the filter-on-a-List solution; they may be useful for the final solution.
    //the idea is that when it's added to the column, userprefsactionroot will use this class to pull the
    //enum values from to filter by/sort on.
    /**
     * Getter for the enum type class name.
     * @return the enum type, as a String
     */
    public String getFilterEnumType() {
        return filterEnumType;
    }
    
    /**
     * Setter for the enum type class name. This is the class that 
     * filtering will look at for determining the values to filter on.
     * @param filterEnumType Enum type class name.
     */
    public void setFilterEnumType(String filterEnumType) {
        this.filterEnumType = filterEnumType;
    }

    /**
     * Gets the root of the key to use for translating boolean values. This is
     * appended with ".true" or ".false" to construct the full key. 
     * @return The root of the key.
     */
    public String getOptionKeyPrefix() {
        return optionKeyPrefix;
    }

    /**
     * 
     * @param optionKeyPrefix the new value
     */
    public void setOptionKeyPrefix(String optionKeyPrefix) {
        this.optionKeyPrefix = optionKeyPrefix;
    }

    /**
     * @return get whether or not auto-complete is enabled. 0 = false, 1 = true
     */
    public Short getFilterAutoCompleteDisable() {
        return filterAutoCompleteDisable;
    }

    /**
     * @param filterAutoCompleteDisable set whether or not auto-complete
     * is enabled.
     */
    public void setFilterAutoCompleteDisable(Short filterAutoCompleteDisable) {
        this.filterAutoCompleteDisable = filterAutoCompleteDisable;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 