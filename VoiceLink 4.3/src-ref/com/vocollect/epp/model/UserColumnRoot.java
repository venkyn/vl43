/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.ui.ColumnDTO;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * UserColumn class - represents a overridden column.
 * 
 */
public class UserColumnRoot extends BaseModelObject {

    private static final long serialVersionUID = -7698234690627067914L;

    private User user;

    private Column column;

    private boolean sorted;

    private boolean sortAsc;

    private int width;

    private boolean visible;

    private int order;

    /**
     * Default Constructor. (used by hibernate.)
     */
    public UserColumnRoot() {

    }

    /**
     * Constructor to create object from values.
     * @param user - User value of column
     * @param c - Column user is overriding
     * @param col - Values of column to override
     */
    public UserColumnRoot(User user, Column c, ColumnDTO col) {
        this.user = user;
        this.column = c;
        update(col);
    }

    /**
     * @param c values to be saved to the database
     */
    public void update(ColumnDTO c) {

        this.width = c.getWidth();

        this.order = c.getOrder();
        this.visible = c.isVisible();
        this.sortAsc = c.isSortAsc();
        this.sorted = c.isSorted();
    }

    /**
     * Getter for the order property.
     * @return order
     */
    public int getOrder() {
        return order;
    }

    /**
     * Setter for the order property.
     * @param order of column in table
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * Getter for the sortAsc property.
     * @return sort order property
     */
    public boolean isSortAsc() {
        return sortAsc;
    }

    /**
     * Setter for the sortAsc property.
     * @param sortAsc - sort order
     */
    public void setSortAsc(boolean sortAsc) {
        this.sortAsc = sortAsc;
    }

    /**
     * Getter for the sorted property.
     * @return whether the column is sorted
     */
    public boolean isSorted() {
        return sorted;
    }

    /**
     * Setter for the sorted property.
     * @param sorted - whether the column is sorted
     */
    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    /**
     * Getter for the visible property.
     * @return Visibility of column
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Setter for the visible property.
     * @param v - Visibility of column
     */
    public void setVisible(boolean v) {
        this.visible = v;
    }

    /**
     * Getter for the width property.
     * @return width of the column
     */
    public int getWidth() {
        return width;
    }

    /**
     * Setter for the width property.
     * @param width - Width of the column
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserColumn)) {
            return false;
        }
        final UserColumnRoot uc = (UserColumnRoot) o;

        return (this.width == uc.getWidth() && this.sortAsc == uc.sortAsc
            && this.sorted == uc.sorted && this.visible == uc.visible
            && this.order == uc.getOrder());

    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return 0;
    }

    /**
     * Getter for the column property.
     * @return the column the values are overriding
     */
    public Column getColumn() {
        return column;
    }

    /**
     * Setter for the column property.
     * @param col - the column the values are overriding
     */
    public void setColumn(Column col) {
        this.column = col;
    }

    /**
     * Getter for the user property.
     * @return User the column is for
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter for the user property. 
     * @param user - User the column is for
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getColumn().getField();
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 