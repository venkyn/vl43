/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class defines model for notification.
 * 
 * @author Vanitha Subramani
 */
public class NotificationRoot extends BaseModelObject implements Taggable {

    private static final long serialVersionUID = -1359004368520254241L;
    private String host;
    private String application;
    private String process;
    private String errorNumber;
    private String message;
    private NotificationPriority priority;
    private Date creationDateTime;
    private String acknowledgedUser;
    
    private Date acknowledgedDateTime;
    private Set<NotificationDetail> details = new HashSet<NotificationDetail>();
    private Set<Tag> tags = new HashSet<Tag>();

    /**
     * Returns the host.
     * @return String
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the host.
     * @param host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * Returns the application.
     * @return String
     */
    public String getApplication() {
        return application;
    }

    /**
     * Sets the application.
     * @param application to set
     */
    public void setApplication(String application) {
        this.application = application;
    }

    /**
     * Returns the errorNumber.
     * @return String
     */
    public String getErrorNumber() {
        return errorNumber;
    }

    /**
     * Sets the errorNumber.
     * @param errorNumber to set
     */
    public void setErrorNumber(String errorNumber) {
        this.errorNumber = errorNumber;
    }

    /**
     * Returns the message.
     * @return String
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     * @param message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns the priority.
     * @return notification priority
     */
    public NotificationPriority getPriority() {
        return priority;
    }

    /**
     * Sets the priority.
     * @param priority to set
     */
    public void setPriority(NotificationPriority priority) {
        this.priority = priority;
    }

    /**
     * Returns the process.
     * @return String
     */
    public String getProcess() {
        return process;
    }

    /**
     * Sets the process.
     * @param process to set
     */
    public void setProcess(String process) {
        this.process = process;
    }

    /**
     * Returns the creationDateTime.
     * @return String
     */
    public Date getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Sets the creationDateTime.
     * @param creationDateTime to set
     */
    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    /**
     * Returns the details.
     * @return String
     */
    public Set<NotificationDetail> getDetails() {
        return details;
    }

    /**
     * Sets the details.
     * @param details to set
     */
    public void setDetails(Set<NotificationDetail> details) {
        this.details = details;
    }

    /**
     * Returns the acknowledgedUser.
     * @return String
     */
    public String getAcknowledgedUser() {
        return acknowledgedUser;
    }

    /**
     * Sets the acknowledgedUser.
     * @param acknowledgedUser to set
     */
    public void setAcknowledgedUser(String acknowledgedUser) {
        this.acknowledgedUser = acknowledgedUser;
    }

    /**
     * Returns the acknowledgedDateTime.
     * @return String
     */
    public Date getAcknowledgedDateTime() {
        return acknowledgedDateTime;
    }

    /**
     * Sets the acknowledgedDateTime.
     * @param acknowledgedDateTime to set
     */
    public void setAcknowledgedDateTime(Date acknowledgedDateTime) {
        this.acknowledgedDateTime = acknowledgedDateTime;
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getMessage();
    }

    /**
     * Get method for a Site.  Merely a placeholder for the dynamic JSON building
     * to see the available property.  The actual display method will fill in this value.
     * @return null always
     */
    public Site getSite() {
        // TODO -- why is this here? ddoubleday
        return null;
    }
    
    /**
     * Standard equals() implementation to compare object state for Notification.
     * 
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Notification)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Notification target = (Notification) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getHost(), target.getHost()).append(
            this.getApplication(), target.getApplication()).append(
            this.getProcess(), target.getProcess()).append(
            this.getErrorNumber(), target.getErrorNumber()).append(
            this.getMessage(), target.getMessage()).append(
            this.getPriority(), target.getPriority()).append(
            this.getCreationDateTime(), target.getCreationDateTime())
            .isEquals();
    }

    /**
     * Standard hashcode() implementation.
     * 
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getHost()).append(this.getApplication()).append(
                this.getProcess()).append(this.getErrorNumber()).append(
                this.getMessage()).append(this.getPriority()).append(
                this.getCreationDateTime()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     * 
     * @return String representation of the given Notification.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "Host", this.getHost())
            .append("Application", this.getApplication()).append(
                "Process", this.getProcess()).append(
                "ErrorNumber", this.getErrorNumber()).append(
                "Message", this.getMessage()).append(
                "Priority", this.getPriority()).append(
                "CreationDate", this.getCreationDateTime()).toString();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 