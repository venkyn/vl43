/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class represents a Feature of the application. Feature definitions
 * are used for access control purposes.
 *
 * @author Dennis Doubleday
 *
 */
public class FeatureRoot extends BaseModelObject implements Serializable {

    static final long serialVersionUID = -8893394286122203288L;

    private String       name;

    private String       description;

    // Is this a read-only feature or not?
    private Boolean      isReadOnly;

    // To what FeatureGroup does this belong?
    private FeatureGroup featureGroup;

    /**
     * @return Returns the featureGroup.
     */
    public FeatureGroup getFeatureGroup() {
        return featureGroup;
    }

    /**
     * @param featureGroup The featureGroup to set.
     */
    public void setFeatureGroup(FeatureGroup featureGroup) {
        this.featureGroup = featureGroup;
    }

    /**
     * Get whether or not this is a read-only feature.
     * @return Returns the isReadOnly.
     */
    public Boolean getIsReadOnly() {
        return isReadOnly;
    }

    /**
     * Set whether or not this is a read-only feature.
     * @param isReadOnly The isReadOnly to set.
     */
    public void setIsReadOnly(Boolean isReadOnly) {
        this.isReadOnly = isReadOnly;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(Object)
     */
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Feature)) {
            return false;
        }
        final Feature f = (Feature) object;
        if (name != null) {
            return name.equals(f.getName());
        } else {
            return (f.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.name)
            .append("id", getId()).toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getName();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 