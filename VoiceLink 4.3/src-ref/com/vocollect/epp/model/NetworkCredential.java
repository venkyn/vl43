/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.util.DataFileReferenceUtil;

import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * @author brupert
 */
public class NetworkCredential extends BaseModelObject implements DataObject,
    Serializable {

    private static final long serialVersionUID = 4872653213888750209L;

    private String plainUserName;

    private String userName;

    private String password;

    private String certificateFileName;
    
    private byte[] certificateFileContents;

    private String certificateKeyFileName;
    
    private byte[] certificateKeyFileContents;

    private String certificatePassword;

    private String md5;

    private boolean valid;
    
    /**
     * Replaces getFormattedString on the DataFileReferenceUtility. Used by the network translator.
     * @return String representing the file contents, properly formatted & encoded.
     */
    public String getFormattedCertificateFileContents() {
        return getFormattedString(getCertificateFileContents());
    }
    
    /**
     * Replaces getFormattedString on the DataFileReferenceUtility. Used by the network translator.
     * @return String representing the file contents, properly formatted & encoded.
     */
    public String getFormattedCertificateKeyFileContents() {
        return getFormattedString(getCertificateKeyFileContents());
    }
    
    /**
     * On place for this code.
     * @param fileContents the stuff to format/encode
     * @return the formatted/encoded string
     */
    private String getFormattedString(byte[] fileContents) {
        if (fileContents.length > 0) {
            return new String(Base64.encodeBase64(fileContents));
        }
        return "";
    }

    /**
     * @return .
     */
    public boolean isCertificateCredentials() {
        return getCertificateFileName() != null;
    }
    
    /**
     * Getter for the plainUserName property.
     * @return the value of the property
     */
    public String getPlainUserName() {
        return this.plainUserName;
    }

    /**
     * Setter for the plainUserName property.
     * @param plainUserName the new plainUserName value
     */
    public void setPlainUserName(String plainUserName) {
        this.plainUserName = plainUserName;
    }

    /**
     * Getter for the userName property.
     * @return the value of the property
     */
    public String getUserName() {
        return this.userName;
    }

    /**
     * Setter for the userName property.
     * @param userName the new userName value
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Getter for the password property.
     * @return the value of the property
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Setter for the password property.
     * @param password the new password value
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for the certificateFileName property.
     * @return the value of the property
     */
    public String getCertificateFileName() {
        return this.certificateFileName;
    }

    /**
     * Setter for the certificateFileName property.
     * @param certificateFileName the new certificateFileName value
     */
    public void setCertificateFileName(String certificateFileName) {
        this.certificateFileName = certificateFileName;
    }

    /**
     * Getter for the certificateKeyFileName property.
     * @return the value of the property
     */
    public String getCertificateKeyFileName() {
        return this.certificateKeyFileName;
    }

    /**
     * Setter for the certificateKeyFileName property.
     * @param certificateKeyFileName the new certificateKeyFileName value
     */
    public void setCertificateKeyFileName(String certificateKeyFileName) {
        this.certificateKeyFileName = certificateKeyFileName;
    }

    /**
     * Getter for the certificatePassword property.
     * @return the value of the property
     */
    public String getCertificatePassword() {
        return this.certificatePassword;
    }

    /**
     * Setter for the certificatePassword property.
     * @param certificatePassword the new certificatePassword value
     */
    public void setCertificatePassword(String certificatePassword) {
        this.certificatePassword = certificatePassword;
    }
    

    /**
     * @return certificateFileContents
     */
    public byte[] getCertificateFileContents() {
        if (certificateFileContents == null) {
            return new DataFileReferenceUtil(certificateFileName).getFileContents();
        }
        return certificateFileContents;
    }

    /**
     * @param certificateFileContents .
     */
    public void setCertificateFileContents(byte[] certificateFileContents) {
        this.certificateFileContents = certificateFileContents;
    }

    /**
     * @return certificateKeyFileContents
     */
    public byte[] getCertificateKeyFileContents() {
        if (certificateKeyFileContents == null) {
            return new DataFileReferenceUtil(certificateKeyFileName).getFileContents();
        }
        return certificateKeyFileContents;
    }

    /**
     * @param certificateKeyFileContents .
     */
    public void setCertificateKeyFileContents(byte[] certificateKeyFileContents) {
        this.certificateKeyFileContents = certificateKeyFileContents;
    }

    /**
     * Getter for the md5 property.
     * @return the value of the property
     */
    public String getMd5() {
        return this.md5;
    }

    /**
     * Setter for the md5 property.
     * @param md5 the new md5 value
     */
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    /**
     * Getter for the valid property.
     * @return the value of the property
     */
    public boolean isValid() {
        return this.valid;
    }

    /**
     * Setter for the valid property.
     * @param valid the new valid value
     */
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    /**
     * Get a copy of this network credential.
     * @return NetworkCredential a copy of this credential
     */
    public NetworkCredential getCopyForComparison() {
        NetworkCredential copy = new NetworkCredential();
        copy.setPlainUserName(plainUserName);
        copy.setUserName(userName);
        copy.setPassword(password);
        copy.setCertificatePassword(certificatePassword);
        copy.setCertificateFileName(certificateFileName);
        copy.setCertificateKeyFileName(certificateKeyFileName);
        return copy;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if (o != null && (o instanceof NetworkCredential)) {
            NetworkCredential that = (NetworkCredential) o;
            equals = (new EqualsBuilder().append(
                this.plainUserName, that.plainUserName).append(
                this.userName, that.userName).append(
                this.password, that.password).append(
                this.certificatePassword, that.certificatePassword).append(
                this.certificateFileName, that.certificateFileName).append(
                this.certificateKeyFileName, that.certificateKeyFileName)).isEquals();
        }
        return equals;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(27, 37).append(this.plainUserName).append(
            this.userName).append(this.password).hashCode();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 