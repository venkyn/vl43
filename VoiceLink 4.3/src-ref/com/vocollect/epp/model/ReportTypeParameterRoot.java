/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Map;


/**
 * A Report parameter should be added for every input variable for the
 * given report.  This allows the report developer to specify what data
 * must be prompted before running the report.  This not only tracks info
 * from the database, but will also be filled with more information from
 * the report itself at runtime.
 *
 * @author cblake
 */
public class ReportTypeParameterRoot extends VersionedModelObject {
    
    // The standard model attributes are below
    private ReportType report;
    private String parameterName;
    private String defaultValue;
    private String validationFunction;
    private String description;
    private ReportParameterFieldType fieldType;
    private String serviceName;
    private String serviceMethod;
    private String displayMember;
    private String dataMember;
    private Short promptOrder;
    private Boolean isRequired;
    private String maxLength;
    
    // These attributes are not persisted in the database, but determined
    // at runtime.
    private Object value = null;
    private Map<String, String> dropDownData = null;
    private Class<?> parameterType = null;
    

    static final long serialVersionUID = 7454903888562155213L;
    
    /**
     * Getter for the dataMember property.  This is the service method
     * attribute which will be used to store the data value in a drop-down
     * list.  This data value will be passed into the report itself as
     * a JasperReports parameter.
     * @return String value of the property
     */
    public String getDataMember() {
        return this.dataMember;
    }

    
    /**
     * Setter for the dataMember property.
     * @param dataMember the new dataMember value
     */
    public void setDataMember(String dataMember) {
        this.dataMember = dataMember;
    }

    
    /**
     * Getter for the defaultValue property.  The default value for this
     * parameter; will be populated when the parameter screen is created.
     * @return String value of the property
     */
    public String getDefaultValue() {
        return this.defaultValue;
    }

    
    /**
     * Setter for the defaultValue property.
     * @param defaultValue the new defaultValue value
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    
    /**
     * Getter for the description property.  This will be interpreted as
     * a localization key, typically corresponding to the
     * ReportResources.properties file.  It will be displayed on
     * the screen when prompting for the parameter value.
     * @return String value of the property
     */
    public String getDescription() {
        return this.description;
    }

    
    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    
    /**
     * Getter for the displayMember property.  This is an attribute that
     * will be called on the service method; it should return a value
     * that can be displayed as a String in the drop-down list.
     * @return String value of the property
     */
    public String getDisplayMember() {
        return this.displayMember;
    }

    
    /**
     * Setter for the displayMember property.
     * @param displayMember the new displayMember value
     */
    public void setDisplayMember(String displayMember) {
        this.displayMember = displayMember;
    }

    
    /**
     * Getter for the parameterName property.  This is JasperReport
     * parameter name.  It should not have spaces in it, and will not
     * be displayed to the user.
     * @return String value of the property
     */
    public String getParameterName() {
        return this.parameterName;
    }

    
    /**
     * Setter for the parameterName property.
     * @param parameterName the new parameterName value
     */
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    
    /**
     * Getter for the promptOrder property.  This represents the order
     * in which parameters will be displayed on the screen.  Lowest
     * values are displayed first.
     * @return Short value of the property
     */
    public Short getPromptOrder() {
        return this.promptOrder;
    }

    
    /**
     * Setter for the promptOrder property.
     * @param promptOrder the new promptOrder value
     */
    public void setPromptOrder(Short promptOrder) {
        this.promptOrder = promptOrder;
    }

    
    /**
     * Getter for the report property.  The affiliated Report object,
     * retrieved through a foreign key in the database.
     * @return Report value of the property
     */
    public ReportType getReportType() {
        return this.report;
    }

    
    /**
     * Setter for the report type property.
     * @param reportType the new report type value
     */
    public void setReportType(ReportType reportType) {
        this.report = reportType;
    }

    
    /**
     * Getter for the serviceMethod property.  The name of the service
     * method to be called on the associated serviceName.  This is used
     * when a drop-list will be displayed for this parameter.
     * @return String value of the property
     */
    public String getServiceMethod() {
        return this.serviceMethod;
    }

    
    /**
     * Setter for the serviceMethod property.
     * @param serviceMethod the new serviceMethod value
     */
    public void setServiceMethod(String serviceMethod) {
        this.serviceMethod = serviceMethod;
    }

    
    /**
     * Getter for the serviceName property.  The name of the service
     * to be queried for the display of a drop-list parameter.  This
     * must correspond to a bean ID in one of the applicationContext*.xml
     * files.
     * @return String value of the property
     */
    public String getServiceName() {
        return this.serviceName;
    }

    
    /**
     * Setter for the serviceName property.
     * @param serviceName the new serviceName value
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    
    /**
     * Getter for the validationFunction property.  The name of the
     * JavaScript function to call in order to validate this parameter.
     * @return String value of the property
     */
    public String getValidationFunction() {
        return this.validationFunction;
    }

    
    /**
     * Setter for the validationFunction property.
     * @param validationFunction the new validationFunction value
     */
    public void setValidationFunction(String validationFunction) {
        this.validationFunction = validationFunction;
    }

    
    /**
     * Getter for the dropDownData property.  This is populated at run
     * time by calling the service method, if isFromList is true.
     * @return List LabelValuePair value of the property
     */
    public Map<String, String> getDropDownData() {
        return this.dropDownData;
    }


    
    /**
     * Setter for the dropDownData property.
     * @param dropDownData the new dropDownData value
     */
    public void setDropDownData(Map<String, String> dropDownData) {
        this.dropDownData = dropDownData;
    }


    
    /**
     * Getter for the value property.  Parameter value retrieved at
     * runtime from the parameter screen.  Will be passed in to the 
     * report.
     * @return Object value of the property
     */
    public Object getValue() {
        return this.value;
    }


    
    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(Object value) {
        this.value = value;
    }


    
    /**
     * Getter for the parameterType property.  This corresponds to the
     * JasperReports parameter data type.  Technically it can be any data
     * type, but we only support the following:
     * <ul>
     *  <li>String
     *  <li>Integer
     *  <li>Long
     *  <li>Double
     *  <li>Boolean
     *  <li>Date
     * </ul>
     *  
     * @return Class value of the property
     */
    public Class<?> getParameterType() {
        return this.parameterType;
    }


    
    /**
     * Setter for the parameterType property.
     * @param parameterType the new parameterType value
     */
    public void setParameterType(Class<?> parameterType) {
        this.parameterType = parameterType;
    }

    /**
     * Getter for isRequired property, which determines whether a user must
     * supply a value for this parameter.
     * @return Boolean value of the property
     */
    public Boolean getIsRequired() {
        return this.isRequired;
    }

    
    /**
     * Setter for the isRequired property.
     * @param isRequired specifies whether the user must supply a value for this
     * parameter.
     */
    public void setIsRequired(Boolean isRequired) {
        this.isRequired = isRequired;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }


    
    /**
     * Getter for the maxLength property.
     * @return String value of the property
     */
    public String getMaxLength() {
        return this.maxLength;
    }


    
    /**
     * Setter for the maxLength property.
     * @param maxLength the new maxLength value
     */
    public void setMaxLength(String maxLength) {
        this.maxLength = maxLength;
    }


    
    /**
     * Getter for the fieldType property.
     * @return ReportParameterFieldType value of the property
     */
    public ReportParameterFieldType getFieldType() {
        return fieldType;
    }


    
    /**
     * Setter for the fieldType property.
     * @param fieldType the new fieldType value
     */
    public void setFieldType(ReportParameterFieldType fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * Getter for the isFromList property.
     * @return Boolean value of the property
     */
    public Boolean getIsFromList() {
        return getFieldType().isInSet(ReportParameterFieldType.DropDownWithAll,
            ReportParameterFieldType.DropDownWithoutAll);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 