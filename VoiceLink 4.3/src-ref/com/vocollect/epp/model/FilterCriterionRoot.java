/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;


/**
 * Model object used for saving a user's preferred filter
 * criteria per view.
 *
 * @author jgeisler
 */
public class FilterCriterionRoot extends BaseModelObject {

    private static final long serialVersionUID = 4185423194548965368L;

    public static final String VIEW_ID = "viewId";
    public static final String COLUMN_ID = "columnId";
    public static final String OPERAND_ID = "operandId";
    public static final String VALUE1 = "value1";
    public static final String VALUE2 = "value2";
    public static final String LOCKED = "locked";

    private String value1;

    private String value2;

    private Operand operand;

    private Long operandId;
    
    private Column column;

    private int index;

    private boolean locked;

    /**
     * Empty constructor
     * Constructor.
     */
    public FilterCriterionRoot() {
    }

    /**
     * This is the major constructor which accepts all required fields
     * Constructor.
     * @param column the column associated with the filter criterion
     * @param operand the operand associated with the filter criterion
     * @param value1 the main filter value
     * @param value2 the secondary filter value
     * @param locked determines whether the user can remove.
     */
    public FilterCriterionRoot(Column column,
                           Operand operand,
                           String value1,
                           String value2,
                           boolean locked) {
        this.column = column;
        this.operand = operand;
        this.operandId = operand.getId();
        this.value1 = value1;
        this.value2 = value2;
        this.locked = locked;
    }

    /**
     * This returns the column associated with the filter criterion.
     * @return Column the column associated with the filter criterion
     */
    public Column getColumn() {
        return column;
    }

    /**
     * This sets the column for the filter criterion.
     * @param column the column to associate with the filter criterion
     */
    public void setColumn(Column column) {
        this.column = column;
    }

    /**
     * Required BaseModelObject function.
     * @see java.lang.Object#equals(java.lang.Object)
     * @param o Object
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * Required BaseModelObject function.
     * @see java.lang.Object#hashCode()
     * @return int
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Get the main filter criteria.
     * @return returns the main filter criteria
     */
    public String getValue1() {
        return value1;
    }

    /**
     * Set the main filter criterion.
     * @param value the main filter criteria
     */
    public void setValue1(String value) {
        this.value1 = value;
    }


    /**
     * Get the secondary filter criteria.
     * @return secondary filter criteria
     */
    public String getValue2() {
        if (value2 == null) {
            // We use this in JSON building and JSON doesn't like to have a null
            // value - so might as well return empty string
            return "";
        } else {
            return value2;
        }
    }


    /**
     * Set the secondary filter criteria.
     * @param value2 secondary filter criteria
     */
    public void setValue2(String value2) {
        this.value2 = value2;
    }


    /**
     * Get the operand associated with the filter criterion.
     * @return the operand associated with the filter criterion
     */
    public Operand getOperand() {
    	if (operand == null) {
    		operand = OperandSetup.getOperandById(this.operandId);
    	}
        return operand;
    }


    /**
     * Set the operand associated with the filter criterion.
     * @param operand to associate with the filter criterion
     */
    public void setOperand(Operand operand) {
        this.operand = operand;
        this.operandId = operand.getId();
    }



    public Long getOperandId() {
		return operandId;
	}

	public void setOperandId(Long operandId) {
		this.operandId = operandId;
	}

	/**
     * Getter for the index property.
     * @return int value of the property
     */
    public int getIndex() {
        return this.index;
    }


    /**
     * Setter for the index property.
     * @param index the new index value
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Gets HQL representation of the FilterCriterion.
     * @return String hql value for filter criterion
     */
    public String toHQL() {
        String returnHQL = null;
        
        String field = getFieldForFilter();
        StringBuilder hqlBuilder = new StringBuilder();

        int operandType = getOperand().getType();
        String operandHql = getOperand().getHql();

        if (operandType == Operand.TYPE_STRING) {
            //empty string, match against null or empty values
            if (getValue1() == null || getValue1().length() == 0) {
                hqlBuilder.append("upper(").append(field).append(") is null or ");
            }

            hqlBuilder.append("upper(").append(field).append(") ");
            hqlBuilder.append(operandHql);
            returnHQL = replacePositionalToken(hqlBuilder.toString(), "{0}",
                StringEscapeUtils.escapeSql(getValue1().toUpperCase()));

        } else if (operandType == Operand.TYPE_DATE) {
            
            hqlBuilder.append(field).append(" ").append(operandHql);
            // apply the current time stamp only for the within clause and not for not within
            if (operandHql.contains(">=")) {
            // Filtering the date in a range upto the current date
                hqlBuilder.append(" ").append("and").append(" ").append(field).append("<=").append("current_timestamp");
            // display the future dates for the not within filter clause
            } else if (operandHql.contains("<")) {
                hqlBuilder.append(" ").append("or").append(" ").append(field).append(">").append("current_timestamp");
            }
            returnHQL = replacePositionalToken(hqlBuilder.toString(), "{0}",
                ":" + getColumn().getField() + getIndex());

        } else {
            //enums and booleans and numbers
            if (getValue1() == null || getValue1().length() == 0) {
                
                operandHql = "is null";
            
            } else if (operandType == Operand.TYPE_NUMBER)  {
             
                // this is done to strip the filter value of the preceding zeroes
                this.setValue1(String.valueOf(Integer.parseInt(getValue1())));
                
                // this is done to strip the filter value of the preceding zeroes
                if (getValue2() != null && getValue2().length() > 0) {
                    this.setValue2(String.valueOf(Integer.parseInt(getValue2())));
                }
            }

            hqlBuilder.append(field).append(" ").append(operandHql);
            returnHQL = replacePositionalToken(hqlBuilder.toString(), "{0}", getValue1());
        }

        //replace any second values
        if (getValue2() != null && getValue2().length() > 0) {
            returnHQL = replacePositionalToken(returnHQL, "{1}", field);
            returnHQL = replacePositionalToken(returnHQL, "{2}", getValue2());
        }

        return returnHQL;

    }


    /**
     * @return .
     */
    private String getFieldForFilter() {
        String result = "obj." + getColumn().getField();
        if (getColumn().getExtraFilterInfo() != null
            && getColumn().getExtraFilterInfo().length() > 0) {
            result += "." + getColumn().getExtraFilterInfo();
        }

        return result;
    }


    /**
     * This method is responsible for taking the HQL from the
     * Operands - i.e. like '%{0}' and replace the {0} or {1}.
     * This method will actually modify the passed in masterString
     * @param masterString = the master string from the db to replace tokens
     * @param positionalToken - the replacement token - either {0} or {1}
     * @param valueToInsert - value to insert in replacement token
     * @return String the new string with inserted values
     */
    private String replacePositionalToken(String masterString,
                                        String positionalToken,
                                        String valueToInsert) {
        return masterString.replace(positionalToken, valueToInsert);
    }

    /**
     * @return a boolean of whether this is a locked filter.
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * @param locked Boolean representation of whether filter is locked.
     */
    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given schedule.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("viewId", this.getColumn().getView().getId().toString())
            .append("columnId", this.getColumn().getId().toString())
            .append("operandId", this.getOperand().getId().toString())
            .append("value1", this.getValue1())
            .append("value2", this.getValue2()).toString();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 