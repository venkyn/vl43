/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * This enum defines the types of table view sorting that is supported
 * for any given Column.
 * <p>
 * Note that new types MUST be added to the end of this list so as not
 * to invalidate the ordinal values (and hence database entries)
 * for the existing types.
 *
 * @author ddoubleday
 */
public enum ColumnSortType {
    NoSort,                 // 0: This column does not support sorting
    Normal,                 // 1: Sort in database with normal order by
    SystemDataLocalized,    // 2: Sort in database by joining with SystemTranslation
    SystemEnumLocalized,    // 3: Sort in database by joining with SystemTranslation
    UserDataLocalized,      // 4: Sort in database by joining with DataTranslation
    CodeOnly,               // 5: Only sortable in Java code
    NormalLeftJoin,         // 6: Sort in database with left join at property level 1
    NormalLeftJoinLevel2,   // 7: Sort in database with left join at property level 2
    CodeOnlyLocalized       // 8: Only sortable in Java code but needs to be localized.
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 