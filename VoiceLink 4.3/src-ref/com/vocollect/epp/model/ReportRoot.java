/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author mnichols
 */
public class ReportRoot extends VersionedModelObject implements Serializable, Taggable, DataObject {

    // Generated serial id
    private static final long serialVersionUID = 4117426486357779229L;

    // Descriptive name of report    
    private String name;

    // Type of report (i.e. Assignment Labor)
    private ReportType type;
    
    // Interval of information report spans
    private ReportInterval interval;
    
    // The report output format
    private ReportFormat format;
    
    // How often the report is scheduled to run
    private ReportFrequency frequency = ReportFrequency.Unscheduled;
    
    // The time of day the report is scheduled to run
    private Date time;
    
    // The last time the report was run
    private Date lastRun;
    
    // The locale for which the report was generated
    private String language;
    
    // The subject of the scheduled report email
    private String subject;
    
    // The list of email addresses sent by the scheduled report
    private String emails;
    
    // The list of saved report parameters
    private Set<ReportParameter> reportParameters;
    
    private Set<ReportParameter> oldReportParameters;
    
    // Tags for site association
    private Set<Tag> tags;
    
    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }
   
    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Report)) {
            return false;
        }
        final Report report = (Report) obj;
        if (name != null) {
            return name.equals(report.getName());
        } else {
            return (report.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }
   
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * Setter for the type property.
     * @param type the new type value
     */
    public void setType(ReportType type) {
        this.type = type;
        
    }

    /**
     * Getter for the type property.
     * @return the value of the property
     */
    public ReportType getType() {
        return this.type;
        
    }

    /**
     * Setter for the interval property.
     * @param interval the new interval value
     */
    public void setInterval(ReportInterval interval) {
        this.interval = interval;
        
    }

    /**
     * Getter for the interval property.
     * @return the value of the property
     */
    public ReportInterval getInterval() {
        return this.interval;
        
    }

    
    /**
     * Getter for the format property.
     * @return the value of the property
     */
    public ReportFormat getFormat() {
        return this.format;
    }

    
    /**
     * Setter for the format property.
     * @param format the new format value
     */
    public void setFormat(ReportFormat format) {
        this.format = format;
    }

    
    /**
     * Getter for the frequency property.
     * @return the value of the property
     */
    public ReportFrequency getFrequency() {
        return this.frequency;
    }

    
    /**
     * Setter for the frequency property.
     * @param frequency the new frequency value
     */
    public void setFrequency(ReportFrequency frequency) {
        this.frequency = frequency;
    }

    
    /**
     * Getter for the time property.
     * @return the value of the property
     */
    public Date getTime() {
        return this.time;
    }

    
    /**
     * Setter for the time property.
     * @param time the new time value
     */
    public void setTime(Date time) {
        this.time = time;
    }

    
    /**
     * Getter for the subject property.
     * @return the value of the property
     */
    public String getSubject() {
        return this.subject;
    }

    
    /**
     * Setter for the subject property.
     * @param subject the new subject value
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    
    /**
     * Getter for the emails property.
     * @return the value of the property
     */
    public String getEmails() {
        return this.emails;
    }

    
    /**
     * Setter for the emails property.
     * @param emails the new emails value
     */
    public void setEmails(String emails) {
        this.emails = emails;
    }

    
    /**
     * Getter for the reportParameters property.
     * @return the value of the property
     */
    public Set<ReportParameter> getReportParameters() {
        return this.reportParameters;
    }

    
    /**
     * Setter for the reportParameters property.
     * @param reportParameters the new reportParameters value
     */
    public void setReportParameters(Set<ReportParameter> reportParameters) {
        this.reportParameters = reportParameters;
    }

    
    /**
     * Getter for the tags property.
     * @return the value of the property
     */
    public Set<Tag> getTags() {
        return this.tags;
    }

    
    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    
    /**
     * Getter for the oldReportParameters property.
     * @return the value of the property
     */
    public Set<ReportParameter> getOldReportParameters() {
        return this.oldReportParameters;
    }

    
    /**
     * Setter for the newReportParameters property.
     * @param oldReportParameters the new newReportParameters value
     */
    public void setOldReportParameters(Set<ReportParameter> oldReportParameters) {
        this.oldReportParameters = oldReportParameters;
    }

    
    /**
     * Getter for the lastRun property.
     * @return the value of the property
     */
    public Date getLastRun() {
        return this.lastRun;
    }

    
    /**
     * Setter for the lastRun property.
     * @param lastRun the new lastRun value
     */
    public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }

    /**
     * @param reportLocale - string value for the language
     */
    public void setLanguage(String reportLocale) {
        this.language = reportLocale;
    }

    /**
     * @return String value for the language
     */
    public String getLanguage() {
        return language;
    }
       
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 