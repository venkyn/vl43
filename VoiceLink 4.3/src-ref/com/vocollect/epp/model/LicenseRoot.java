/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


import java.nio.charset.Charset;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;

/**
 * 
 */
public class LicenseRoot extends BaseModelObject {

    /**
     * 
     */
    private static final long serialVersionUID = -6187140106192646981L;
    private Integer type;
    private Date dateEntered;
    // private String contents;
    private String contents;
    
    
    /**
     * Constructor.
     */
    public LicenseRoot() {
        
    }

    /**
     * @return the contents of the license.
     */
    public String getContents() {
        return contents;
    }
    
//    public byte[] getContents() {
//        // return contents.getBytes();
//        return contents;
//    }

    /**
     * @param contents the contents of the license
     */
    public void setContents(String contents) {
        this.contents = contents;
    }
    
    /**
     * @param contents the byte array contents of the license
     */
    public void setContents(byte[] contents) {     
        this.contents = new String(contents, Charset.forName("utf-8"));
        // this.contents = contents.;
    }

    /**
     * @return the date the license was imported
     */
    public Date getDateEntered() {
        return dateEntered;
    }

    /**
     * @param dateEntered the date the license was imported
     */
    public void setDateEntered(Date dateEntered) {
        this.dateEntered = dateEntered;
    }

    /**
     * @return the license type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type the license type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        boolean equals = false;
        if (other != null && (other instanceof SystemLicense)) {
            LicenseRoot that = (LicenseRoot) other;
            equals = (new EqualsBuilder()
                      .append(this.type, that.type)
                      .append(this.dateEntered, that.dateEntered)
                      .append(this.contents, that.contents)
                      ).isEquals();
        }
        return equals;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((contents == null) ? 0 : contents.hashCode());
        result = prime * result + ((dateEntered == null) ? 0 : dateEntered.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }    
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 