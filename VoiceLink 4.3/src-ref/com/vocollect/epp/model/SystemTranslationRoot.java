/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.ToStringBuilder;




/**
 * Class representing localized translations of all system data that might
 * be displayed to an end user and require localization.
 *
 * @author ddoubleday
 */
public class SystemTranslationRoot extends VersionedModelObjectWithoutId {

    private static final long serialVersionUID = -1614320013755509359L;

    private Integer code;

    private String  key;

    private String  locale;

    private String  translation;

    /**
     * Getter for the code property.
     * @return Integer value of the property
     */
    public Integer getCode() {
        return this.code;
    }

    /**
     * Getter for the key property.
     * @return String value of the property
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Getter for the locale property.
     * @return String value of the property
     */
    public String getLocale() {
        return this.locale;
    }

    /**
     * Getter for the translation property.
     * @return String value of the property
     */
    public String getTranslation() {
        return this.translation;
    }

    /**
     * Setter for the code property.
     * @param code the new code value
     */
    protected void setCode(Integer code) {
        this.code = code;
    }

    /**
     * Setter for the key property.
     * @param key the new key value
     */
    protected void setKey(String key) {
        this.key = key;
    }

    /**
     * Setter for the locale property.
     * @param locale the new locale value
     */
    protected void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Setter for the translation property.
     * @param translation the new translation value
     */
    protected void setTranslation(String translation) {
        this.translation = translation;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getKey();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + ((locale == null) ? 0 : locale.hashCode());
        result = prime * result + ((translation == null) ? 0 : translation.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SystemTranslation)) {
            return false;
        }
        final SystemTranslation other = (SystemTranslation) obj;
        if (code == null) {
            if (other.getCode() != null) {
                return false;
            }
        } else if (!code.equals(other.getCode())) {
            return false;
        }
        if (key == null) {
            if (other.getKey() != null) {
                return false;
            }
        } else if (!key.equals(other.getKey())) {
            return false;
        }
        if (locale == null) {
            if (other.getLocale() != null) {
                return false;
            }
        } else if (!locale.equals(other.getLocale())) {
            return false;
        }
        if (translation == null) {
            if (other.getTranslation() != null) {
                return false;
            }
        } else if (!translation.equals(other.getTranslation())) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.VersionedModelObjectWithoutIdRoot#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", this.key).append(
            "code", this.code).append("locale", this.locale).toString();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 