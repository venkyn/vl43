/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * 
 */
public class TimeWindowRoot extends BaseModelObject {

    private static final long serialVersionUID = -7698234690627067914L;

    private View view;

    private Long value;

    /**
     * Getter for the value property.
     * @return Long value of the property
     */
    public Long getValue() {
        return this.value;
    }

    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(Long value) {
        this.value = value;
    }

    /**
     * Getter for the view property.
     * @return View value of the property
     */
    public View getView() {
        return this.view;
    }

    /**
     * Setter for the view property.
     * @param view the new view value
     */
    public void setView(View view) {
        this.view = view;
    }

    /**
     * Overridden.
     * @return int value
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * Overridden.
     * @param o Object.
     * @return boolean value.
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 