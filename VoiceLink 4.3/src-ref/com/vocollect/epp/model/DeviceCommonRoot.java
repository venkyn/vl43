/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Set;

/**
 * 
 * 
 * @author khazra
 */
public class DeviceCommonRoot extends VersionedModelObject
    implements java.io.Serializable, Taggable {

    private static final long serialVersionUID = -9148371684300218869L;

    // Terminals Serial Number
    private String serialNumber;

    private Set<Tag> tags;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof DeviceCommon) {
            final DeviceCommon other = (DeviceCommon) obj;
            if (getSerialNumber() == null) {
                if (other.getSerialNumber() != null) {
                    return false;
                }
            } else if (!getSerialNumber().equals(
                other.getSerialNumber())) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return this.getSerialNumber() == null ? 0 : this.getSerialNumber()
            .hashCode();
    }

    /**
     * Getter for the serialNumber property.
     * @return String value of the property
     */
    public String getSerialNumber() {
        return this.serialNumber;
    }

    /**
     * Setter for the serialNumber property.
     * @param serialNumber the new serialNumber value
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Returns the tags.
     * @return String
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * @param tags to set
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 