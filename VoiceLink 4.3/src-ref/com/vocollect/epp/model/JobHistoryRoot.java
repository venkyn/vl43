/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * The model definition of the job history.
 *
 * @author hulrich
 */
public class JobHistoryRoot extends VersionedModelObject implements Serializable {

    // Unique ID for class serialization.
    private static final long serialVersionUID = -4236784349066454570L;

    // The job's id
    private Long jobId;

    // The job's name
    private String jobName;

    // The job start
    private Date jobStarted;

    // The job finish
    private Date jobFinished;

    // The job result
    private ScheduleLastResult jobResult;

    // The job results details
    private String resultDetails;

    // The history id
    private Long id;

    /**
     * Hibernate constructor.
     */
    public JobHistoryRoot() {

    }

    /**
     * Constructor.
     * @param jobId the id of the <code>Job</code>
     * @param jobName the name of the job
     * @param started when the job started
     */
    public JobHistoryRoot(Long jobId, String jobName, Date started) {
        this.jobId = jobId;
        this.jobName = jobName;
        this.jobStarted = started;
    }

    /**
     * Getter for the id property.
     * @return Long value of the property
     */
    @Override
    public Long getId() {
        return this.id;
    }

    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for the jobFinished property.
     * @return Date value of the property
     */
    public Date getJobFinished() {
        return this.jobFinished;
    }

    /**
     * Setter for the jobFinished property.
     * @param jobFinished the new jobFinished value
     */
    public void setJobFinished(Date jobFinished) {
        this.jobFinished = jobFinished;
    }

    /**
     * Getter for the jobName property.
     * @return String value of the property
     */
    public String getJobName() {
        return this.jobName;
    }

    /**
     * Setter for the jobName property.
     * @param jobName the new jobName value
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * Getter for the jobResult property.
     * @return String value of the property
     */
    public ScheduleLastResult getJobResult() {
        return this.jobResult;
    }

    /**
     * Setter for the jobResult property.
     * @param jobResult the new jobResult value
     */
    public void setJobResult(ScheduleLastResult jobResult) {
        this.jobResult = jobResult;
    }

    /**
     * Getter for the jobStarted property.
     * @return Date value of the property
     */
    public Date getJobStarted() {
        return this.jobStarted;
    }

    /**
     * Setter for the jobStarted property.
     * @param jobStarted the new jobStarted value
     */
    public void setJobStarted(Date jobStarted) {
        this.jobStarted = jobStarted;
    }

    /**
     * Getter for the resultDetails property.
     * @return String value of the property
     */
    public String getResultDetails() {
        if (this.resultDetails == null) {
            this.resultDetails = "";
        }
        return this.resultDetails;
    }

    /**
     * Setter for the resultDetails property.
     * @param resultDetails the new resultDetails value
     */
    public void setResultDetails(String resultDetails) {
        this.resultDetails = resultDetails;
    }

    /**
     * Return a String representation of this property for use by the
     * BeanComparator (for sorting).
     *
     * @return String representation of the last run property.
     */
    public String getSortJobStarted() {
        if (this.getJobStarted() != null) {
            return String.valueOf(this.getJobStarted().getTime());
        } else {
            return "";
        }
    }

    /**
     * Return a String representation of this property for use by the
     * BeanComparator (for sorting).
     *
     * @return String representation of the last run property.
     */
    public String getSortJobFinished() {
        if (this.getJobFinished() != null) {
            return String.valueOf(this.getJobFinished().getTime());
        } else {
            return "";
        }
    }

    /**
     * Standard equals() implementation to compare object state.
     *
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof JobHistory)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        JobHistory target = (JobHistory) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getJobId(), target.getJobId()).append(
            this.getJobName(), target.getJobName()).append(
            this.getJobStarted(), target.getJobStarted()).append(
            this.getJobFinished(), target.getJobFinished()).append(
            this.getJobResult(), target.getJobResult()).append(
            this.getResultDetails(), target.getResultDetails()).isEquals();
    }

    /**
     * Standard hashcode() implementation.
     *
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        // All fields used in equal()s method should be used here
        // as well.
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getJobId()).append(this.getJobName()).append(
                this.getId()).append(this.getJobStarted()).append(
                this.getJobFinished()).append(this.getJobResult()).append(
                this.getResultDetails()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given schedule.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.getJobName())
            .append("id", this.getId()).append("jobId", this.getJobId())
            .append("started", this.getJobStarted()).append(
                "finished", this.getJobFinished()).append(
                "result", this.getJobResult()).append(
                "details", this.getResultDetails()).toString();
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getJobName();
    }

    /**
     * Getter for the jobId property.
     * @return Long value of the property
     */
    public Long getJobId() {
        return this.jobId;
    }

    /**
     * Setter for the jobId property.
     * @param jobId the new jobId value
     */
    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 