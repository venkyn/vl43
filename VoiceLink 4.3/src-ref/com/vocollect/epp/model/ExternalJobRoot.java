/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author khazra
 */
public class ExternalJobRoot extends VersionedModelObject implements
    Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -9218079598182240184L;

    private String name;

    private ExternalJobType jobType;

    private String command;

    private String workingDirectory;

    private Date createdDate;

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the type property.
     * @return ExternalJobType value of the property
     */
    public ExternalJobType getJobType() {
        return jobType;

    }

    /**
     * Setter for the type property.
     * @param jobType the new type value
     */
    public void setJobType(ExternalJobType jobType) {
        this.jobType = jobType;
    }

    /**
     * Getter for the command property.
     * @return String value of the property
     */
    public String getCommand() {
        return command;
    }

    /**
     * Setter for the command property.
     * @param command the new command value
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * Getter for the workingDirectory property.
     * @return String value of the property
     */
    public String getWorkingDirectory() {
        return workingDirectory;
    }

    /**
     * Setter for the workingDirectory property.
     * @param workingDirectory the new workingDirectory value
     */
    public void setWorkingDirectory(String workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    /**
     * Getter for the createdDate property.
     * @return Date value of the property
     */
    public Date getCreatedDate() {
        return this.createdDate;
    }

    /**
     * Setter for the createdDate property.
     * @param createdDate the new createdDate value
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String getDescriptiveText() {
        return this.getName();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ExternalJob)) {
            return false;
        }
        final ExternalJob other = (ExternalJob) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.name == null ? 0 : this.name.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 