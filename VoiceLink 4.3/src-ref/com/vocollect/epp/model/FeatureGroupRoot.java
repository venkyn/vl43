/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class represents a logical grouping of Feature objects.
 *
 * @author Dennis Doubleday
 *
 */
public class FeatureGroupRoot extends BaseModelObject implements Serializable {

    static final long serialVersionUID = -6809133511147293707L;

    /**
     * The name of the group that includes features for app administration.
     */
    public static final String APP_ADMIN_GROUP_NAME =
        "featureGroup.appAdmin.name";

    /**
     * The name of the group that includes features for app user administration.
     */
    public static final String USER_ADMIN_GROUP_NAME =
        "featureGroup.userAdmin.name";
    
    /**
     * The name of the group that includes features for dashboard and alert administration.
     */
    public static final String DNA_ADMIN_GROUP_NAME =
        "featureGroup.dnaAdmin.name";

    private String description;

    private Set<Feature> features;

    private String name;

    /**
     * @return Returns the description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return Returns the features.
     */
    public Set<Feature> getFeatures() {
        return features;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param features The features to set.
     */
    public void setFeatures(Set<Feature> features) {
        this.features = features;
    }

    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(Object)
     */
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof FeatureGroup)) {
            return false;
        }
        final FeatureGroup fg = (FeatureGroup) object;
        if (name != null) {
            return name.equals(fg.getName());
        } else {
            return (fg.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.name).append(
            "id", getId()).toString();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getName();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 