/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * The Report corresponds to a JasperReports report, which should
 * be found in the source folder as a .jrxml file.  Each report can
 * have zero to many parameters, which are defined in the ReportParameter
 * class.
 * 
 * @author cblake
 */
public class ReportTypeRoot extends VersionedModelObject {
    
    private Set<ReportTypeParameter> reportTypeParameters;
    private String reportTypeName;
    private String reportTypeNameKey;
    private String reportTypeDescription;
    // Identifies the app (epp, selection, etc.) that this report belongs to.
    private String appName;
    // Bean name of hibernate session factory to use as a data provider for
    // this report.
    private String sessionBeanId;

    private static final long serialVersionUID = -3391245683162459943L;
    
    
    
    /**
     * Getter for the reportParameters property.  This represents
     * the report's list of parameters to be displayed to the user.
     * @return Set of ReportParameter
     */
    public Set<ReportTypeParameter> getReportTypeParameters() {
        return this.reportTypeParameters;
    }


    /**
     * Setter for the reportTypeParameters property.
     * @param reportTypeParameters the new reportTypeParameters value
     */
    public void setReportTypeParameters(Set<ReportTypeParameter> reportTypeParameters) {
        this.reportTypeParameters = reportTypeParameters;
    }


    /**
     * Getter for the reportDescription property.  This is the descriptive
     * name of the report.
     * @return String value of the property
     */
    public String getReportTypeDescription() {
        return this.reportTypeDescription;
    }

    
    /**
     * Setter for the reportTypeDescription property.
     * @param reportTypeDescription the new reportTypeDescription value
     */
    public void setReportTypeDescription(String reportTypeDescription) {
        this.reportTypeDescription = reportTypeDescription;
    }

    
    /**
     * Getter for the reportName property.  This is the short report name,
     * which cannot have any spaces.  It will be used in the action URL.
     * @return String value of the property
     */
    public String getReportTypeName() {
        return this.reportTypeName;
    }

    
    /**
     * Setter for the reportTypeName property.
     * @param reportTypeName the new reportTypeName value
     */
    public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }
    
    /**
     * Getter for the report name message key.
     * @return String value of the property
     */
    public String getReportTypeNameKey() {
        return this.reportTypeNameKey;
    }

    
    /**
     * Setter for the report type name message key.
     * @param reportTypeNameKey the new report name message key value
     */
    public void setReportTypeNameKey(String reportTypeNameKey) {
        this.reportTypeNameKey = reportTypeNameKey;
    }
    
    /**
     * Getter for the name of the application that owns this report. The app
     * name is used to group the reports for display. If it is null, the
     * report is not a report that will be displayed in a summary view.
     * @return String value of the property
     */
    public String getAppName() {
        return this.appName;
    }
    
    /**
     * Setter for the owning application name.
     * @param appName application name
     */
    public void setAppName(String appName) {
        this.appName = appName;
    }    
    
    /**
     * Getter for the session bean id used to provide data to this report.
     * @return String value of the property
     */
    public String getSessionBeanId() {
        return this.sessionBeanId;
    }
    
    /**
     * Setter for the session bean id used to provide data to this report.
     * @param sessionBeanId session bean id
     */
    public void setSessionBeanId(String sessionBeanId) {
        this.sessionBeanId = sessionBeanId;
    }    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ReportType)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        ReportType target = (ReportType) obj;

        return new EqualsBuilder()
            .append(this.getReportTypeName(), target.getReportTypeName())
            .append(this.getReportTypeNameKey(), target.getReportTypeNameKey())
            .append(this.getReportTypeDescription(), target.getReportTypeDescription())
            .append(this.getSessionBeanId(), target.getSessionBeanId())
            .append(this.getAppName(), target.getAppName()).isEquals();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        // All fields used in equal()s method should be used here
        // as well.
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2)
            .append(this.getReportTypeName())
            .append(this.getReportTypeNameKey())
            .append(this.getReportTypeDescription())
            .append(this.getSessionBeanId())
            .append(this.getAppName()).toHashCode();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("reportTypeName", this.getReportTypeName())
            .append("reportTypeNameKey", this.getReportTypeNameKey())
            .append("reportTypeDescription", this.getReportTypeDescription())
            .append("sessionBeanId", this.getSessionBeanId())
            .append("appName", this.getAppName()).toString();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 