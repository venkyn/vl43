/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class represents the model for User Property.
 * 
 * @author vjayaram
 * @author ddoubleday fixed equals, hashCode, compareTo
 */
public class UserPropertyRoot extends VersionedModelObject implements
    DataObject, Serializable, Comparable<UserProperty> {

    //
    private static final long serialVersionUID = 5419450819456823138L;

    private String name = "default";

    private String value = "default";

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {        
        return this.name == null ? 0 : this.name.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof UserProperty)) {
            return false;
        }
        final UserProperty other = (UserProperty) obj;
        if (getName() == null) {
            if (other.getName() != null) {
                return false;
            }
        } else if (!getName().equals(other.getName())) {
            return false;
        }
        return true;
    }

    
    /**
     * {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(UserProperty other) {
        if (this.name != null) {
            return name.compareTo(other.getName());
        } else if (other.getName() != null) {
            return other.getName().compareTo(this.name);
        } else {
            return 0;
        }
    }

    /**
     * Standard toString() implementation.
     * 
     * @return String representation of the given Notification.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(
            "name", this.getName()).
            append("value", this.getValue()).toString();
    }

    /**
     * Getter for the propertyName property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the propertyValue property.
     * @return String value of the property
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter for the propertyValue property.
     * @param value the new propertyValue value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 