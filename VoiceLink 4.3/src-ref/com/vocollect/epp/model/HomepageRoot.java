/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.builder.ToStringBuilder;


/**
 *  This class defines model for Homepage.
 *
 * @author
 */
public class HomepageRoot extends BaseModelObject {

    //serialVerion id for Homepage class
    private static final long serialVersionUID = -7665234690627054431L;

    private Long homepageId;

   private Map< String , Long > summaries;

   private Set<Summary> accessibleSummaries;


   /**
    * Returns the accessible summaries.
    * @return Set
    */
   public Set<Summary> getAccessibleSummaries() {
       return accessibleSummaries;
   }

   /**
    * Sets the accessible summaries.
    * @param accessibleSummaries to set
    */
   public void setAccessibleSummaries(Set<Summary> accessibleSummaries) {
       this.accessibleSummaries = accessibleSummaries;
   }

    /**
     * Returns the summaries.
     * @return Map
     */
    public Map< String , Long > getSummaries() {
        return summaries;
    }

    /**
     * Sets the summaries.
     * @param summaries to set
     */
    public void setSummaries(Map< String , Long > summaries) {
        this.summaries = summaries;
    }

    /**
     * Returns the homepageId.
     * @return Long
     */
    public Long getHomepageId() {
        return homepageId;
    }

    /**
     * Sets the homepageId.
     * @param homepageId to set
     */
    public void setHomepageId(Long homepageId) {
        this.homepageId = homepageId;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        return false;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
         return 0;
    }
    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(
            "id", getId()).toString();
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 