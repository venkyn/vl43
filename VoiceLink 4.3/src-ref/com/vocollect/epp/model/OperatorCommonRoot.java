/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Set;

/**
 * @author khazra
 * 
 */
public class OperatorCommonRoot extends VersionedModelObject
    implements DataObject, Serializable, Comparable<OperatorCommon>, Taggable {

    private static final long serialVersionUID = 2090710070219263090L;

    // Operator ID from terminal/task
    private String operatorIdentifier;

    private String name;
    
    private String spokenName;
    
    private Set<Tag> tags;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof OperatorCommon) {
            final OperatorCommon other = (OperatorCommon) obj;
            if (getOperatorIdentifier() == null) {
                if (other.getOperatorIdentifier() != null) {
                    return false;
                }
            } else if (!getOperatorIdentifier().equals(
                other.getOperatorIdentifier())) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return getOperatorIdentifier() == null ? 0 : getOperatorIdentifier().hashCode();
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(OperatorCommon other) {
        if (getOperatorIdentifier() == null) {
            if (other.getOperatorIdentifier() == null) {
                return 0;
            } else {
                return other.getOperatorIdentifier().compareTo(null);
            }
        } else {
            return getOperatorIdentifier().compareTo(other.getOperatorIdentifier());
        }
    }

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the operatorIdentifier property.
     * @return String value of the property
     */
    public String getOperatorIdentifier() {
        return this.operatorIdentifier;
    }

    /**
     * Setter for the operatorIdentifier property.
     * @param operatorIdentifier the new operatorIdentifier value
     */
    public void setOperatorIdentifier(String operatorIdentifier) {
        this.operatorIdentifier = operatorIdentifier;
    }

    /**
     * Getter for the tags property.
     * @return the value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getOperatorIdentifier();
    }

    
    /**
     * Getter for the spokenName property.
     * @return the value of the property
     */
    public String getSpokenName() {
        return this.spokenName;
    }

    
    /**
     * Setter for the spokenName property.
     * @param spokenName the new spokenName value
     */
    public void setSpokenName(String spokenName) {
        this.spokenName = spokenName;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 