/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.TimeZone;

import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * Site class - represents a site of the EPP-based application.
 * 
 */
public class SiteRoot extends VersionedModelObject 
    implements DataObject, Serializable, Comparable<Site> {

    private static final long serialVersionUID = 5694781418898273277L;

    /**
     * The name of the Default Site in an out-of-the-box installation.
     */
    public static final String DEFAULT_SITE_NAME = "Default";

    private String name;

    private String description;

    private String notes;

    private TimeZone timeZone;

    private String timeZoneId;

    //  New for EAP
    // EAP Type
    private Integer eapType;
    // Association
    private Integer credentialAssociation;
    // Type
    private Integer credentialType;
    // Use PINS?
    private Boolean usePins;
    // Charger Disconnect
    private Boolean chargerDisconnect;
    // Server credentials
    private NetworkCredential serverCredentials;
    // Restricted User
    private NetworkCredential restrictedUser;
    // The SSID to use for the restricted user
    private String restrictedSSID;
    // The EAP Type to use for the restricted user
    private Integer restrictedEapType;
    // Site-wide User
    private NetworkCredential siteWideUser;
    // Site-wide PIN
    private String sitePIN;
    // LDAP config
    private LDAPConfiguration ldapConfig;

    
    /**
     * @return the chargerDisconnect
     */
    public Boolean getChargerDisconnect() {
        return chargerDisconnect;
    }

    /**
     * @param chargerDisconnect the chargerDisconnect to set
     */
    public void setChargerDisconnect(Boolean chargerDisconnect) {
        this.chargerDisconnect = chargerDisconnect;
    }

    /**
     * @return the credentialAssociation
     */
    public Integer getCredentialAssociation() {
        return credentialAssociation;
    }

    /**
     * @param credentialAssociation the credentialAssociation to set
     */
    public void setCredentialAssociation(Integer credentialAssociation) {
        this.credentialAssociation = credentialAssociation;
    }

    /**
     * @return the credentialType
     */
    public Integer getCredentialType() {
        return credentialType;
    }

    /**
     * @param credentialType the credentialType to set
     */
    public void setCredentialType(Integer credentialType) {
        this.credentialType = credentialType;
    }

    /**
     * @return the eapType
     */
    public Integer getEapType() {
        return eapType;
    }

    /**
     * @param eapType the eapType to set
     */
    public void setEapType(Integer eapType) {
        this.eapType = eapType;
    }

    /**
     * @return the ldapConfig
     */
    public LDAPConfiguration getLdapConfig() {
        return ldapConfig;
    }

    /**
     * @param ldapConfig the ldapConfig to set
     */
    public void setLdapConfig(LDAPConfiguration ldapConfig) {
        this.ldapConfig = ldapConfig;
    }

    /**
     * @return the restrictedEapType
     */
    public Integer getRestrictedEapType() {
        return restrictedEapType;
    }

    /**
     * @param restrictedEapType the restrictedEapType to set
     */
    public void setRestrictedEapType(Integer restrictedEapType) {
        this.restrictedEapType = restrictedEapType;
    }

    /**
     * @return the restrictedSSID
     */
    public String getRestrictedSSID() {
        return restrictedSSID;
    }

    /**
     * @param restrictedSSID the restrictedSSID to set
     */
    public void setRestrictedSSID(String restrictedSSID) {
        this.restrictedSSID = restrictedSSID;
    }

    /**
     * @return the restrictedUser
     */
    public NetworkCredential getRestrictedUser() {
        return restrictedUser;
    }

    /**
     * @param restrictedUser the restrictedUser to set
     */
    public void setRestrictedUser(NetworkCredential restrictedUser) {
        this.restrictedUser = restrictedUser;
    }

    /**
     * @return the serverCredentials
     */
    public NetworkCredential getServerCredentials() {
        return serverCredentials;
    }

    /**
     * @param serverCredentials the serverCredentials to set
     */
    public void setServerCredentials(NetworkCredential serverCredentials) {
        this.serverCredentials = serverCredentials;
    }

    /**
     * @return the sitePIN
     */
    public String getSitePIN() {
        return sitePIN;
    }

    /**
     * @param sitePIN the sitePIN to set
     */
    public void setSitePIN(String sitePIN) {
        this.sitePIN = sitePIN;
    }

    /**
     * @return the siteWideUser
     */
    public NetworkCredential getSiteWideUser() {
        return siteWideUser;
    }

    /**
     * @param siteWideUser the siteWideUser to set
     */
    public void setSiteWideUser(NetworkCredential siteWideUser) {
        this.siteWideUser = siteWideUser;
    }

    /**
     * @return the usePins
     */
    public Boolean getUsePins() {
        return usePins;
    }

    /**
     * @param usePins the usePins to set
     */
    public void setUsePins(Boolean usePins) {
        this.usePins = usePins;
    }
    
    /**
     * Returns the name.
     * @return String
     */

    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param username The name to set
     */
    public void setName(String username) {
        this.name = username;
    }

    /**
     * Returns the description.
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description.
     * @param description The description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Returns the notes.
     * @return String
     */
    public String getNotes() {
        return notes;
    }

    /**
     * Sets notes.
     * @param notes Notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * Returns the timeZone.
     * @return TimeZone
     */
    public TimeZone getTimeZone() {
        return timeZone;
    }

    /**
     * Sets timezone.
     * @param timeZone The timeZone to set
     */
    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * Returns the timeZoneId.
     * @return String
     */
    public String getTimeZoneId() {
        return this.timeZoneId;
    }

    /**
     * Sets timezone id.
     * @param timeZoneId The timeZoneId to set
     */
    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Site) {
            final Site other = (Site) obj;
            if (getName() == null) {
                if (other.getName() != null) {
                    return false;
                }
            } else if (!getName().equals(
                other.getName())) {
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.name).append(
            "id", getId()).toString();
    }

    /**
     * Sites are compared by name. {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Site site) {
        if (getName() != null) {
            return getName().compareToIgnoreCase(site.getName());
        } else if (site.getName() != null) {
            return -1;
        } else {
            return 0;
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 