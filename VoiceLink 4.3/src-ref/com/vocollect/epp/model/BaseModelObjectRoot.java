/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;




/**
 * Base class for Model objects.  Child objects must implement 
 * <code>equals()</code> and <code>hashCode()</code>. 
 * @author ddoubleday
 */
public abstract class BaseModelObjectRoot implements DataObject, Serializable {
    
    protected static final int HASH_CODE_PRIME = 31;

    // The persistent ID key for this object
    private Long id;
   
    /**
     * Getter for the id property.
     * @return the peristent ID for this object, which will be <code>null</code>
     * if the object has never be persisted.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Setter for the id property. This is provate because the ID should
     * only ever be set by Hibernate.
     * @param id the persistent ID.
     */
    @SuppressWarnings("unused")
    private void setId(Long id) {
        this.id = id;
    }

    /**
     * Default implementation of toString() for model objects. Builds a
     * String representation that includes only the ID of the object.
     * This should be overridden in subclasses if more information is wanted. 
     * @see java.lang.Object#toString()
     * @return the String representation
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", getId()).toString();
    }
    
    /**
     * Tells whether or not an object has been persisted yet.
     * @return true if the object has not been persisted, false otherwise.
     */
    public boolean isNew() {
        return getId() == null;
    }
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public abstract boolean equals(Object o);
    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public abstract int hashCode();

    /**
     * This allows subclasses to override this method or not. It should
     * be overridden if it makes sense for the particular model object.
     * This base implementation will just return the ID as a String.
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return (getId() == null ? null : getId().toString());
    }
   
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 