/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class defines model for Summary.
 *
 * @author svoruganti
 */
public class SummaryRoot extends BaseModelObject {

    //serialVerion id for Summary class
    private static final long serialVersionUID = -7698234690627054434L;

    // Message key translated wehn building the summary
    private String title;


    // A reference to an included FreeMarker File
    private String sourceFile;


   /**
     * Returns the sourceFile.
     * @return String
     */
    public String getSourceFile() {
        return sourceFile;
    }

    /**
     * Sets the sourceFile.
     * @param sourceFile to set
     */
    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    /**
     * Returns the title.
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     * @param title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return "Summary";
    }

    /**
     * Standard equals() implementation to compare object state for Summary.
     *
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Summary)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Summary target = (Summary) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getTitle(), target.getTitle()).append(
            this.getSourceFile(), target.getSourceFile())
            .isEquals();
    }

    /**
     * Standard hashcode() implementation.
     *
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getTitle()).append(this.getSourceFile()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given Summary.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "title", this.getTitle()).append(
                "sourceFile", this.getSourceFile()).toString();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 