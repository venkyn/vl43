/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * @author mraj
 * 
 */
public class ExternalJobSystemTranslationRoot extends SystemTranslation {

    /**
     * 
     */
    private static final long serialVersionUID = -4938753136117286433L;

    /**
     * Setter for the code property.
     * @param code the new code value
     */
    public void setCode(Integer code) {
        super.setCode(code);
    }

    /**
     * Setter for the key property.
     * @param key the new key value
     */
    public void setKey(String key) {
        super.setKey(key);
    }

    /**
     * Setter for the locale property.
     * @param locale the new locale value
     */
    public void setLocale(String locale) {
        super.setLocale(locale);
    }

    /**
     * Setter for the translation property.
     * @param translation the new translation value
     */
    public void setTranslation(String translation) {
        super.setTranslation(translation);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 