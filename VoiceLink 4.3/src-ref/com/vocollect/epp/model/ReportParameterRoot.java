/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;


/**
 *
 * @author mnichols
 */
public class ReportParameterRoot extends VersionedModelObject implements Serializable, DataObject {

    private ReportTypeParameter reportTypeParameter;
    
    private String value;
    
    //
    private static final long serialVersionUID = 3236801127848824178L;

    /**
     * Constructor.
     */
    public ReportParameterRoot() {
        super();
    }
    
    /**
     * Constructor.
     * @param repTypeParam
     * @param value
     */
    public ReportParameterRoot(ReportTypeParameter repParam, String value) {
        this.reportTypeParameter = repParam;
        this.value = value;
    }    
    
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        
        return 0;
    }

    
    /**
     * Getter for the value property.
     * @return the value of the property
     */
    public String getValue() {
        return this.value;
    }

    
    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(String value) {
        this.value = value;
    }

    
    /**
     * Getter for the reportTypeParameter property.
     * @return the value of the property
     */
    public ReportTypeParameter getReportTypeParameter() {
        return this.reportTypeParameter;
    }

    
    /**
     * Setter for the reportTypeParameter property.
     * @param reportTypeParameter the new reportTypeParameter value
     */
    public void setReportTypeParameter(ReportTypeParameter reportTypeParameter) {
        this.reportTypeParameter = reportTypeParameter;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 