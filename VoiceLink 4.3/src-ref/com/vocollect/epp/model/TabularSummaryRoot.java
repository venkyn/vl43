/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * This class defines model for TabularSummary.
 *
 * @author svoruganti
 */
public class TabularSummaryRoot extends UpdatingSummary {

    //serialVerion id for TabularSummary class
    private static final long serialVersionUID = -7698223690627054434L;

    //The viewId of the table component summary view to be displayed
    private Long viewId;

    //The viewId of the table component summary view to be displayed
    private String tableId;

    // map that has any extra params that must be passes into tablecomponent macro
    private Map extraParams;


    private List<Column> columns;

    private Long timeWindow;


    /**
     * @return the time window associated with the summary.
     */
    public Long getTimeWindow() {
        return timeWindow;
    }


    /**
     * @param timeWindow the time window associated with the summary.
     */
    public void setTimeWindow(Long timeWindow) {
        this.timeWindow = timeWindow;
    }

    /**
     * Returns the viewId.
     * @return int
     */
    public Long getViewId() {
        return viewId;
    }

    /**
     * Sets the viewId.
     * @param viewId to set
     */
    public void setViewId(Long viewId) {
        this.viewId = viewId;
    }

    /**
     * Returns the extraParams.
     * @return int
     */
    public Map getExtraParams() {
        return extraParams;
    }

    /**
     * Returns the columns  for table component.
     * @return list
     */
    public List<Column> getColumns() {
          return columns;
    }

    /**
     * Sets the columns of table component with given viewId.
     * @param columns to set
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * Sets the extraParams.
     * @param extraParams to set
     */
    public void setExtraParams(Map extraParams) {
        this.extraParams = extraParams;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return "TabularSummary";
    }

    /**
     * Standard equals() implementation to compare object state for TabularSummary.
     *
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TabularSummary)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        TabularSummary target = (TabularSummary) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
          this.getViewId(), target.getViewId()).isEquals();
    }

    /**
     * Standard hashcode() implementation.
     *
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getViewId()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given TabularLocation.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "viewId", this.getViewId())
            .toString();
    }


    /**
     * Getter for the tableId property.
     * @return String value of the property
     */
    public String getTableId() {
        return tableId;
    }


    /**
     * Setter for the tableId property.
     * @param tableId the new tableId value
     */
    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 