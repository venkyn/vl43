/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @author jvolz
 */
public class LDAPConfiguration extends BaseModelObject implements
        Serializable, DataObject {

    private static final long serialVersionUID = 4031664240745544940L;

    // Whether or not to use ssl
    private Boolean useSSL;
    
    // The hostname of the LDAP Host
    private String ldapHost;
    
    // The port the Directory Service is listening on
    private String ldapPort;
    
    // The username to use when searching in the DS
    private String searchUsername;
    
    // The password to use when searching in the DS
    private String searchPassword;
    
    // The base DN to begin the search from
    private String searchBase;
    
    // The attribute to search on 
    private String searchableAttribute;
    
    // The attribute name of the password attribute.  This will allow VC to change the user's password.
    private String passwordAttribute;

    /**
     * Return a diplayable name for the LDAP Configuration
     * @return String representing the display name
     */
    public String getDisplayName() {
        return getLdapHost() + ":" + getLdapPort() + ";" + getSearchableAttribute();
    }
    
    /**
     * @return .
     */
    public String getProviderUrl() {
        return "ldap://" + getLdapHost() + ":" + getLdapPort();
    }
    
    /**
     * @return the ldapHost
     */
    public String getLdapHost() {
        return ldapHost;
    }

    /**
     * @param ldapHost the ldapHost to set
     */
    public void setLdapHost(String ldapHost) {
        this.ldapHost = ldapHost;
    }

    /**
     * @return the ldapPort
     */
    public String getLdapPort() {
        return ldapPort;
    }

    /**
     * @param ldapPort the ldapPort to set
     */
    public void setLdapPort(String ldapPort) {
        this.ldapPort = ldapPort;
    }

    /**
     * @return the passwordAttribute
     */
    public String getPasswordAttribute() {
        return passwordAttribute;
    }

    /**
     * @param passwordAttribute the passwordAttribute to set
     */
    public void setPasswordAttribute(String passwordAttribute) {
        this.passwordAttribute = passwordAttribute;
    }

    /**
     * @return the searchableAttribute
     */
    public String getSearchableAttribute() {
        return searchableAttribute;
    }

    /**
     * @param searchableAttribute the searchableAttribute to set
     */
    public void setSearchableAttribute(String searchableAttribute) {
        this.searchableAttribute = searchableAttribute;
    }

    /**
     * @return the searchBase
     */
    public String getSearchBase() {
        return searchBase;
    }

    /**
     * @param searchBase the searchBase to set
     */
    public void setSearchBase(String searchBase) {
        this.searchBase = searchBase;
    }

    /**
     * @return the searchPassword
     */
    public String getSearchPassword() {
        return searchPassword;
    }

    /**
     * @param searchPassword the searchPassword to set
     */
    public void setSearchPassword(String searchPassword) {
        this.searchPassword = searchPassword;
    }

    /**
     * @return the searchUsername
     */
    public String getSearchUsername() {
        return searchUsername;
    }

    /**
     * @param searchUsername the searchUsername to set
     */
    public void setSearchUsername(String searchUsername) {
        this.searchUsername = searchUsername;
    }

    /**
     * @return the useSSL
     */
    public Boolean getUseSSL() {
        return useSSL;
    }

    /**
     * @param useSSL the useSSL to set
     */
    public void setUseSSL(Boolean useSSL) {
        this.useSSL = useSSL;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        boolean equals = false;
        if (other != null && (other instanceof LDAPConfiguration)) {
            LDAPConfiguration that = (LDAPConfiguration) other;
            equals = (new EqualsBuilder()
                      .append(this.useSSL, that.useSSL)
                      .append(this.ldapHost, that.ldapHost)
                      .append(this.ldapPort, that.ldapPort)
                      .append(this.searchUsername, that.searchUsername)
                      .append(this.searchPassword, that.searchPassword)
                      .append(this.searchBase, that.searchBase)
                      .append(this.searchableAttribute, that.searchableAttribute)
                      .append(this.passwordAttribute, that.passwordAttribute)
                      ).isEquals();
        }
        return equals;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#hashCode()
     */
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
            .append(this.useSSL)
            .append(this.ldapHost)
            .append(this.ldapPort)
            .append(this.searchUsername)
            .append(this.searchPassword)
            .append(this.searchBase)
            .append(this.searchableAttribute)
            .append(this.passwordAttribute).toHashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 