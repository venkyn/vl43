/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * Class representing a tagging of an object model.
 *
 * @author dkertis
 */
public class TagRoot extends BaseModelObject {
    //
    private static final long serialVersionUID = 5668116041299956360L;

    public static final Long SITE = new Long(1);

    public static final long SYSTEM = -463L;

    public static final long ALL = -927L;

    private Long taggableId;

    private Long tagType;


    /**
     * Getter for the tagType property.
     * @return  Long tagType
     */
    public Long getTagType() {
        return tagType;
    }


    /**
     * Setter for the tagType property.
     * @param tagType tagType
     */
    public void setTagType(Long tagType) {
        this.tagType = tagType;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tagType == null) ? 0 : tagType.hashCode());
        result = prime * result + ((taggableId == null) ? 0 : taggableId.hashCode());
        return result;
    }


    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        final Tag other = (Tag) obj;
        if (tagType == null) {
            if (other.getTagType() != null) {
                return false;
            }
        } else if (!tagType.equals(other.getTagType())) {
            return false;
        }
        if (taggableId == null) {
            if (other.getTaggableId() != null) {
                return false;
            }
        } else if (!taggableId.equals(other.getTaggableId())) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "id = " + getId() + ", taggedObjectID = " + getTaggableId();
    }


    /**
     * Getter for the taggableId property.
     * @return taggableId
     */
    public Long getTaggableId() {
        return taggableId;
    }


    /**
     * Setter for the taggableId property.
     * @param tagValue Long
     */
    public void setTaggableId(Long tagValue) {
        this.taggableId = tagValue;
    }


  }
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 