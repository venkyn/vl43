/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.acegisecurity.GrantedAuthority;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * This class is used to represent a user role, or capability, within the app.
 *
 * @author Dennis Doubleday
 */
public class RoleRoot extends VersionedModelObject
    implements DataObject, Serializable, GrantedAuthority, Comparable<Role> {
   
    private static final long serialVersionUID = 3690197650654049848L;

    // The name of this role.
    private String name;

    // A description of the role, e.g. who should have it.
    private String description;

    // Indicates whether or not the role is the Administrator role.
    private Boolean isAdministrative = false;

    // The set of features associated with the role. This will actually 
    // be null for the Administrative role.
    private Set<Feature> features;

    // The count of users that have this role. This is a computed property.
    private int userCount;

    /**
     * Returns the name.
     * @return String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the description of the role.
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }
    
    /**
     * Set the name.
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Set the description.
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Get the count of users who are in this role. This is a computed
     * property, so it cannot be set.
     * @return the count
     */
    public int getUserCount() {
        return this.userCount;
    }

    /**
     * Setter for the userCount property. This is private because only
     * Hibernate should use it.
     * @param userCount the new count.
     */
    @SuppressWarnings("unused")
    private void setUserCount(int userCount) {
        this.userCount = userCount;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }

        final Role role = (Role) o;
        if (name != null) {
            return name.equals(role.getName());
        } else {
            return (role.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("name", this.name).append("id", getId()).toString();
    }

    /**
     * @return whether or not this is an Administrative role
     */
    public Boolean getIsAdministrative() {
        return this.isAdministrative;
    }

    /**
     * @param isAdministrative whether or not this is Adminstrative.
     */
    public void setIsAdministrative(Boolean isAdministrative) {
        this.isAdministrative = isAdministrative;
    }

    /**
     * @return Returns the features.
     */
    public Set<Feature> getFeatures() {
        return features;
    }

    /**
     * @param features The features to set.
     */
    public void setFeatures(Set<Feature> features) {
        this.features = features;
    }

    /**
     * @param feature The feature to add.
     */
    public void addFeature(Feature feature) {
        if (this.features == null) {
            this.features = new HashSet<Feature>();
        }
        this.features.add(feature);
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.GrantedAuthority#getAuthority()
     */
    public String getAuthority() {
        return "ROLE_" + getName();
    }

    /**
     * Return whether or not this role provides access to the feature
     * specified by featureName.
     * @param featureName the name of the Feature.
     * @return true if this Role provides access, false otherwise.
     */
    public boolean getAllowsAccessToFeature(String featureName) {
        if (getIsAdministrative()) {
            return true;
        } else {
            if (getFeatures() != null) {
                for (Feature feature : getFeatures()) {
                    if (feature.getName().equals(featureName)) {
                        return true;
                    }
                }
            }
        }
        
        // No access to feature via this role
        return false;
    }

    /**
     * Determine whether or not the role allows access to any 
     * <code>Feature</code> in the <code>FeatureGroup</code> identified by
     * the name.
     * @param featureGroupName the name of the <code>FeatureGroup</code>
     * @return true if the role has access to any feature in the group, 
     * false otherwise
     */
    public boolean getAllowsAccessInFeatureGroup(String featureGroupName) {
        
        if (getIsAdministrative()) {
            // Administrative role has access to everything.
            return true;
        } else {
            if (getFeatures() != null) {
                for (Feature feature : getFeatures()) {
                    if (feature.getFeatureGroup().getName()
                        .equals(featureGroupName)) {
                        // The role provides access to a feature in the
                        // specified group.
                        return true;
                    }
                }
            }
        }
        
        // No access
        return false;
    }

    /**
     * Roles are compared by name.
     * {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Role role) {
        if (getName() != null) {
            return getName().compareToIgnoreCase(role.getName());
        } else if (role.getName() != null) {
            return -1;
        } else {
            return 0;
        }
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 