/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.chart.model.UserChartPreference;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.userdetails.UserDetails;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * User class - represents a user of the EPP-based application.
 * 
 */
public class UserRoot extends VersionedModelObject implements DataObject,
    Serializable, UserDetails, Comparable<User>, Taggable {

    private static final long serialVersionUID = 3832626162173359411L;

    private String name;

    private String password;

    private String emailAddress;

    private Date lastLoginTime;

    private String lastLoginLocation;

    private String notes;

    private boolean enabled = true;

    private boolean allSitesAccess = false;

    private Long currentSiteTagId;
    
    private Set<UserProperty> userProperties = new HashSet<UserProperty>();
    
    private Set<UserChartPreference> chartPreference = null;

    private Set<Role> roles;

    private Set<Tag> tags;

    private Set<Site> sites;

    // parameter to store Nav Menu status
    private boolean navMenuOpen = true;

    private String lastRequestedURL = "/home.action";

    private String sideNavHeight = "414";

    /**
     * Getter for the currentSite property.
     * WARNING: What is returned is the TAG ID 
     *          corresponding to the site, and not
     *          the site ID.
     * @return the tag ID corresponding to the User's current site.
     */
    public Long getCurrentSite() {
        return currentSiteTagId;
    }

    /**
     * Setter for the currentSite property.
     * WARNING: What is cached here is the TAG ID
     *          corresponding to the site of interest,
     *          and not the site ID itself.
     * @param tagId for the new site.
     */
    public void setCurrentSite(Long tagId) {
        this.currentSiteTagId = tagId;
    }

    /**
     * Returns the name.
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the password.
     * @return String
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns the emailAddress.
     * @return String
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Returns the user's roles.
     * @return Set
     */
    public Set<Role> getRoles() {
        return roles;
    }

    /**
     * Adds a role for the user.
     * 
     * @param role the Role to add.
     */
    public void addRole(Role role) {
        if (getRoles() == null) {
            setRoles(new HashSet<Role>());
        }
        getRoles().add(role);
    }

    /**
     * Return whether or not the <code>User</code> has access to
     * <code>Feature</code> specified by the name.
     * @param featureName the name of the Feature
     * @return true if the user has access, false otherwise.
     */
    public boolean getHasAccessToFeature(String featureName) {
        if (getRoles() != null) {
            for (Role role : getRoles()) {
                if (role.getAllowsAccessToFeature(featureName)) {
                    return true;
                }
            }
        }

        // No feature access allowed.
        return false;
    }

    /**
     * Determine whether or not the User has access to any <code>Feature</code>
     * in the <code>FeatureGroup</code> identified by the name.
     * @param featureGroupName the name of the <code>FeatureGroup</code>
     * @return true if the role has access to any feature in the group, false
     *         otherwise
     */
    public boolean getHasAccessInFeatureGroup(String featureGroupName) {

        if (getRoles() != null) {
            for (Role role : getRoles()) {
                if (role.getAllowsAccessInFeatureGroup(featureGroupName)) {
                    return true;
                }
            }
        }

        // No match
        return false;
    }

    /**
     * Sets the name.
     * @param username The name to set
     */
    public void setName(String username) {
        this.name = username;
    }

    /**
     * Sets the password.
     * @param password The password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Sets the emailAddress.
     * @param email The emailAddress to set
     */
    public void setEmailAddress(String email) {
        this.emailAddress = email;
    }

    /**
     * Sets the roles.
     * @param roles The roles to set
     */
    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * @return Returns if the user is enabled
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * @param enabled Enable/disable this user
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return Returns is user has access to all the sites
     */
    public boolean getAllSitesAccess() {
        return this.allSitesAccess;
    }

    /**
     * @param allSitesAccess set true/false for the all site access for this
     *            user
     */
    public void setAllSitesAccess(boolean allSitesAccess) {
        this.allSitesAccess = allSitesAccess;
    }

    /**
     * Returns the set of sites user has access to.
     * @return List
     */
    public Set<Site> getSites() {
        return sites;
    }

    /**
     * Sets the sites.
     * @param sites The sites to set
     */
    public void setSites(Set<Site> sites) {
        this.sites = sites;
    }

    /**
     * Adds a site for the user.
     * 
     * @param site the Site to add.
     */
    public void addSite(Site site) {
        if (getSites() == null) {
            setSites(new HashSet<Site>());
        }
        getSites().add(site);
    }

    /**
     * Removes a site for the user.
     * 
     * @param site the Site to remove.
     */
    public void removeSite(Site site) {
        if (getSites() != null) {
            getSites().remove(site);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User)) {
            return false;
        }
        final User user = (User) o;
        if (name != null) {
            return name.equals(user.getName());
        } else {
            return (user.getName() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (name != null ? name.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", this.name).append(
            "id", getId()).toString();
    }

    /**
     * Getter for the lastLoginLocation property.
     * @return String value of the property
     */
    public String getLastLoginLocation() {
        return this.lastLoginLocation;
    }

    /**
     * Setter for the lastLoginLocation property.
     * @param lastLoginLocation the new lastLoginLocation value
     */
    public void setLastLoginLocation(String lastLoginLocation) {
        this.lastLoginLocation = lastLoginLocation;
    }

    /**
     * Getter for the lastLoginTime property.
     * @return Date value of the property
     */
    public Date getLastLoginTime() {
        return this.lastLoginTime;
    }

    /**
     * Setter for the lastLoginTime property.
     * @param lastLoginTime the new lastLoginTime value
     */
    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    /**
     * Getter for the notes property.
     * @return String value of the property
     */
    public String getNotes() {
        return this.notes;
    }

    /**
     * Setter for the notes property.
     * @param notes the new notes value
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return true if the User possesses an Administrative role, false
     *         otherwise.
     */
    public boolean isAdministrator() {

        if (getRoles() != null) {
            for (Role role : getRoles()) {
                if (role.getIsAdministrative()) {
                    return true;
                }
            }
        }
        // No administrator role
        return false;
    }

    // The following methods are all implemented to satisfy the
    // Acegi Security UserDetails interface.

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.userdetails.UserDetails#isAccountNonExpired()
     */
    public boolean isAccountNonExpired() {
        // Our accounts never expire
        return true;
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.userdetails.UserDetails#isAccountNonLocked()
     */
    public boolean isAccountNonLocked() {
        // We don't do account lockouts
        return true;
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.userdetails.UserDetails#getAuthorities()
     */
    public GrantedAuthority[] getAuthorities() {
        if (getRoles() == null) {
            return null;
        } else {
            return getRoles().toArray(new GrantedAuthority[0]);
        }
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.userdetails.UserDetails#isCredentialsNonExpired()
     */
    public boolean isCredentialsNonExpired() {
        // We don't expire passwords
        return true;
    }

    /**
     * {@inheritDoc}
     * @see org.acegisecurity.userdetails.UserDetails#getUsername()
     */
    public String getUsername() {
        return getName();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return this.getName();
    }

    /**
     * Users are compared by name. {@inheritDoc}
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(User user) {
        if (getName() != null) {
            return getName().compareToIgnoreCase(user.getName());
        } else if (user.getName() != null) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * Getter for the tags property.
     * @return the tags associated with this user.
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags set the tags for the user.
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    /**
     * @param siteID the ID of the site to check access for.
     * @return whether or not the user has access to the specified site.
     */
    public boolean hasSiteAccess(Long siteID) {
        if (allSitesAccess || siteID.longValue() == Tag.ALL) {
            return true;
        }
        for (Tag t : getTags()) {
            if (t.getId().equals(siteID)) {
                return true;
            }
        }
        return false;
    }

    // Parameters for Open Close Nav Menu
    /**
     * Getter for the navMenuOpen property.
     * @return boolean value of the property
     */
    public boolean getNavMenuOpen() {
        return navMenuOpen;
    }

    /**
     * Setter for the navMenuOpen property.
     * @param navMenuOpen the new navMenuIsOpen value
     */
    public void setNavMenuOpen(boolean navMenuOpen) {
        this.navMenuOpen = navMenuOpen;
    }

    /**
     * Getter for the lastRequestedURL property.
     * @return String value of the property
     */
    public String getLastRequestedURL() {
        return lastRequestedURL;
    }

    /**
     * Setter for the lastRequestedURL property.
     * @param lastRequestedURL the new lastRequestedURL value
     */
    public void setLastRequestedURL(String lastRequestedURL) {
        this.lastRequestedURL = lastRequestedURL;
    }

    /**
     * Getter for the sideNavHeight property.
     * @return String value of the property
     */
    public String getSideNavHeight() {
        return sideNavHeight;
    }

    /**
     * Setter for the sideNavHeight property.
     * @param sideNavHeight the new sideNavHeight value
     */
    public void setSideNavHeight(String sideNavHeight) {
        this.sideNavHeight = sideNavHeight;
    }

    /**
     * Getter for the userProperties property.
     * @return the value of the property
     */
    public Set<UserProperty> getUserProperties() {
        return this.userProperties;
    }

    /**
     * Setter for the userProperties property.
     * @param userProperties the new userProperties value
     */
    public void setUserProperties(Set<UserProperty> userProperties) {
        this.userProperties = userProperties;
    }

    
    /**
     * Getter for the chartPreference property.
     * @return Set<UserProperty> value of the property
     */
    public Set<UserChartPreference> getChartPreference() {
        return chartPreference;
    }

    
    /**
     * Setter for the chartPreference property.
     * @param chartPreference the new chartPreference value
     */
    public void setChartPreference(Set<UserChartPreference> chartPreference) {
        this.chartPreference = chartPreference;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 