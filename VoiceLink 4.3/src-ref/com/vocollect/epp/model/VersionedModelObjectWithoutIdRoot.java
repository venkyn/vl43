/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;




/**
 * Base class for Model objects with a version number but no ID. This
 * is a bit of a hack because we are actually inheriting from a class
 * that does provide an ID, but it is not used. It is being done this way
 * backward compatibility reasons (we may revisit at some point.)
 * <p>
 * Unlike other instances of <code>BaseModelObject</code>, objects of
 * this type are determined to be persisted or not based on the value
 * of the <code>version</code> property, since we don't have use the ID.
 * @author ddoubleday
 */
public abstract class VersionedModelObjectWithoutIdRoot 
    extends VersionedModelObject implements DataObject, Serializable {

    /**
     * Default implementation of toString(). This just returns the class
     * name because we don't know much about it at this level.
     * This should be overridden in subclasses if more information is wanted. 
     * @see java.lang.Object#toString()
     * @return the String representation
     */
    @Override
    public String toString() {
        return this.getClass().getName();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObjectRoot#isNew()
     */
    @Override
    public boolean isNew() {
        return getVersion() == null;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 