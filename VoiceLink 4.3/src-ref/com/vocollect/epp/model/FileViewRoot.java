/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.util.DateUtil;

import java.io.File;
import java.io.Serializable;
import java.util.Date;


/** 
 * Class to hold the details of the file that we are interested in
 * for the front end.
 * @author dgold
 *
 */
public class FileViewRoot implements DataObject, Serializable {

    private static final long serialVersionUID = 7962938609761894018L;

    private static final int KBYTE_SIZE = 1024;

    private String            fileName         = "";

    // Name of file, sans directory

    private LogFileType     fileType            = null;

    // file size, in bytes
    
    private String            fileSuffix         = "";

    // file suffix, i.e., bytes, MB, KB

    private Long              actualFileSize;

    // file size, in bytes, as a long from the actual file.

    private String            selected;

    // Field for the checkbox...

    private long              actualModificationDate;

    // modification date as a long, from the actual file

    private String            fileModifiedDate = "";

    // Date on which the file was created

    private String            fileCreatedDate  = "";

    // Date on which the file was created

    private String            displayName      = " ";

    // The type of log as we show it to the user.

    private Long              id;

    /**
     * Constructor. A default constructor is necessary so that a class
     * can be instantiated to get the enumerations.
     */
    public FileViewRoot() {
    }
    
    /**
     * Constructor which takes a File to get info from.
     * @param source file we need to salvage for information
     */
    public FileViewRoot(File source) {
        this.setFileName(source.getName());
        this.setFileType(getFileType(getFileName()));
        this.setActualFileSize(source.length());
        this.setFileSuffix(source.length());
        this.setActualModificationDate(source.lastModified());
        this.setFileModifiedDate(
            DateUtil.formatTimestampForTableComponent(
                new Date(source.lastModified())
            )
        );
        this.setFileCreatedDate(
            DateUtil.formatTimestampForTableComponent(
                new Date(source.lastModified())
            )
        );
        this.setDisplayName(getFileName());
        this.setSelected("false");
        this.setId(new Long(source.hashCode()));
    }

    /**
     * @return Returns the fileCreatedDate.
     */
    public String getFileModifiedDate() {
        return this.fileModifiedDate;
    }

    /**
     * @param createdDate The fileCreatedDate to set.
     */
    private void setFileModifiedDate(String createdDate) {
        this.fileModifiedDate = createdDate;
    }

    /**
     * @return Returns the fileName.
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * @param fileName The fileName to set.
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return Returns the fileType.
     */
    public LogFileType getFileType() {
        return this.fileType;
    }

    /**
     * @param fileType The fileType to set.
     */
    private void setFileType(LogFileType fileType) {
        this.fileType = fileType;
    }

    /**
     * @return Returns the displayName.
     */
    public String getDisplayName() {
        return this.displayName;
    }

    /**
     * Change in requirements - the filename is the filename as shown on the OS.
     * @param name The displayName to set.
     */
    private void setDisplayName(String name) {
        this.displayName = name;
    }

    /**
     * Separate out the extension of the file in order to determine file 'type'.
     * @param sourceName name of file
     * @return final extension of the file, representing the 'type'
     */
    public static LogFileType getFileType(String sourceName) {
        LogFileType lft = null;
        if (sourceName.contains(".log")) {
            lft = LogFileType.LOG;
        } else if (sourceName.contains(".err")) {
            lft = LogFileType.ERROR;
        } else {
            lft = LogFileType.UNKNOWN;
        }
    
        return lft;
    }

    /**
     * @return Returns the fileCreatedDate.
     */
    public String getFileCreatedDate() {
        return this.fileCreatedDate;
    }

    /**
     * @param fileCreatedDate The fileCreatedDate to set.
     */
    private void setFileCreatedDate(String fileCreatedDate) {
        this.fileCreatedDate = fileCreatedDate;
    }

    /**
     * @return Returns the actualFileSize.
     */
    public Long getActualFileSize() {
        return this.actualFileSize;
    }

    /**
     * @param actualFileSize The actualFileSize to set.
     */
    public void setActualFileSize(Long actualFileSize) {
        this.actualFileSize = actualFileSize;
    }

    /**
     * @return Returns the selected.
     */
    public String getSelected() {
        return this.selected;
    }

    /**
     * @param selected The selected to set.
     */
    public void setSelected(String selected) {
        this.selected = selected;
    }

    /**
     * @return Returns the id.
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id The id to set.
     */
    public void setId(long id) {
        this.id = id;
    }

    
    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileView)) {
            return false;
        }
        final FileView file = (FileView) o;

        if (getFileName() != null
            ? !getFileName().equals(file.getFileName())
            : file.getFileName() != null) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getFileName();
    }

    /**
     * @return Returns the actualModificationDate.
     */
    public long getActualModificationDate() {
        return this.actualModificationDate;
    }

    /**
     * @param actualModificationDate The actualModificationDate to set.
     */
    private void setActualModificationDate(long actualModificationDate) {
        this.actualModificationDate = actualModificationDate;
    }
    
    /**
     * @return Returns a sortable modification date String.
     */
    public Date getModificationDate() {
        return new Date(this.getActualModificationDate());
        //return DateUtil.SORTABLE_DATE_FORMAT.format(
        //        new Date(this.actualModificationDate));
    }

    /**
     * @return Returns the fileSuffix.
     */
    public String getFileSuffix() {
        return this.fileSuffix;
    }

    /**
     * @param size The file size that determines the suffix to set.
     */
    public void setFileSuffix(long size) {
        String suffix = "";
        if (size >= KBYTE_SIZE * KBYTE_SIZE) {
            suffix = "MB";
        } else if (size >= KBYTE_SIZE) {
            suffix = "KB";
        } else {
            suffix = "bytes";
        }
        this.fileSuffix = suffix;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getFileName();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 