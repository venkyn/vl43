/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

/**
 * This is a class whose sole object is to parameterize the ID for the BaseModelObject according to type. A wrapper
 * for the anonymous Long we use as IDs in the database.
 *
 * Primarily intended to replace the passing-about of Long ids as parameters when dealing with Site and Tag.
 *
 * History:
 * We originally started with the concept of Sites. Then Tags were used to implement Sites. But the term Tag and Site
 * was used interchangeably, although a Site is not a Tag and vise versa. This led to a confusion of terminology. In
 * some cases, tags were referred to as sites, and sites were referred to as tags. This wasn't too hars to keep track
 * of, but a lot of these things were done with the IDs, which are simply Longs. It was difficult to tell whether
 * the authors intended Tags or Sites. Additionally, every time we needed to go in and manipulate the sites in the code,
 * we were invariably confused and either hosed the test data, or the code (sometimes more than once).
 *
 * We have changed the interface of the User and SiteContext to make use of these ObjectIds.
 * Currently, I've made using the ID class a royal pain in the butt.
 *
 * @author dgold
 *
 */
/**
 * @param <T>
 *
 * @author 
 */
public class ObjectId<T extends BaseModelObject> {

    Long internalId = null;

     /**
      * Constructor taking an object - grabs the id from the object.
     * @param bmo .
     */
    public ObjectId(T bmo) {
        internalId = bmo.getId();
    }

    /**
     * Constructor taking a class and an id. For when all you have is the id.
     * @param id .
     */
    @Deprecated
    public ObjectId(long id) {
        internalId = id;
    }

    /**
     * HashCode & equals - based on the internalId.
     * @return .
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((internalId == null) ? 0 : internalId.hashCode());
        return result;
    }

    /**
     * HashCode & equals - based on the internalId.
     * @param obj .
     * @return .
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ObjectId<T> other = (ObjectId<T>) obj;
        if (internalId == null) {
            if (other.internalId != null) {
                return false;
            }
        } else if (!internalId.equals(other.internalId)) {
            return false;
        }
        return true;
    }

    /**
     * Get the value of the ID
     * @return .
     */
    public Long getIdValue() {
        return internalId;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 