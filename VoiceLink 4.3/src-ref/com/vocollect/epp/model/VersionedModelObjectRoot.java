/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;



/**
 * Model objects extending from the class have a version field for
 * concurrent update detection.
 *
 * @author Dennis Doubleday
 */
public abstract class VersionedModelObjectRoot extends BaseModelObject {

    // The persistent version of the object.
    private Integer version;


    /**
     * Getter for the version property, the persistent version of the object.
     * @return the version, which will be <code>null</code> if the object
     * has never be persisted.
     */
     public Integer getVersion() {
        return this.version;
    }


    /**
     * Setter for the version property. It is not necessary to set the
     * version when creating a new object. Hibernate manages the version
     * attribute automatically.
     * @param version the new version.
     */
    public void setVersion(Integer version) {
        this.version = version;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 