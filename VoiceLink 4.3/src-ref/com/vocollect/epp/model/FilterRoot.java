/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.exceptions.FilterInvalidParameterException;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.util.FilterTimeWindow;
import com.vocollect.epp.util.OperandType;
import com.vocollect.epp.web.util.DisplayUtilities;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.json.JSONException;

/**
 * Container for criteria per view per user.
 *
 *
 * @author jgeisler
 */
public class FilterRoot extends BaseModelObject {

    //
    private static final long serialVersionUID = 3613699376942393790L;

    private User user;

    private View view;

    private List<FilterCriterion> filterCriteria = new LinkedList<FilterCriterion>();

    /**
     * Constructor.
     */
    public FilterRoot() {
    }

    /**
     *
     * Constructor.
     * @param user The User this filter is for.
     * @param view The View this filter is on.
     * @param filterCriteria The List of FilterCriterion.
     */
    public FilterRoot(User user, View view, List<FilterCriterion> filterCriteria) {
        this.user = user;
        this.view = view;
        this.filterCriteria = filterCriteria;
    }

    /**
     * When we get serialized JSON from the web tie
     * Constructor.
     * @param submittedSerializedJSON the serialized JSON filter to create from
     */
    public FilterRoot(String submittedSerializedJSON) {
    }

    /**
     * Update the filter criterion values by Id.
     * @param fc The new filter criterion object.
     * @param criterionId The criterion id to be updated.
     */
    public void updateFilterCriteria(FilterCriterion fc, Long criterionId) {
        for (FilterCriterion crit : getFilterCriteria()) {
            if (crit.getId().equals(criterionId)) {
                crit.setValue1(fc.getValue1());
                crit.setValue2(fc.getValue2());
            }
        }
    }

    /**
     * return the user object.
     * @return User the user object associated with the filter
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the user associated with the filter.
     * @param user the user to associate with the filter
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Return the view associated with the filter.
     * @return View the view associated with the filter
     */
    public View getView() {
        return view;
    }

    /**
     * Set the view with the filter.
     * @param view the view to associate to the filter
     */
    public void setView(View view) {
        this.view = view;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        // TODO Auto-generated method stub
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.BaseModelObject#hashCode()
     */
    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return 0;
    }


    /**
     * Return the filter criteria associated with the filter.
     * @return a list of filter criteria
     */
    public List<FilterCriterion> getFilterCriteria() {
        return filterCriteria;
    }


    /**
     * Set the filter criteria for the filter.
     * @param filterCriteria the list of filter criteria to associate with the
     * filter
     */
    public void setFilterCriteria(List<FilterCriterion> filterCriteria) {
        this.filterCriteria = filterCriteria;
    }

    /**
     * This method will take the filter criteria for the filter and produce
     * the necessary HQL to apply in the where clause.
     * @return String - the appropriate HQL to add to the where clause
     */
    public String toHQL() {
        // We need to organize the list of filter criteria into like column
        // buckets.  This is because we or the results between like columns
        // and and the results between columns
        HashMap<Column, List<FilterCriterion>> columnMapping =
            new HashMap<Column, List<FilterCriterion>>();

        int index = 0;
        for (FilterCriterion filterCriterion : getFilterCriteria()) {
            // We only want to include filter criterion for which a database filter
            // is appropriate.  We will do the inmemory filter later on
            if (filterCriterion.getColumn().getFilterType() == ColumnFilterType.Database) {
                // We want to give each filter criterion a unique number to be used
                // later on with named parameters
                filterCriterion.setIndex(index++);
                List<FilterCriterion> criterionList = columnMapping.get(filterCriterion.getColumn());
                if (criterionList == null) {
                    // We have never encountered this column before - therefore we
                    // have no list - go ahead and create the list and put it in the map
                    criterionList = new LinkedList<FilterCriterion>();
                    criterionList.add(filterCriterion);
                    columnMapping.put(filterCriterion.getColumn(), criterionList);
                } else {
                    // We have encountered this criterion before - therefore
                    // get the list and add this criterion
                    criterionList.add(filterCriterion);
                }
            }
        }

        // Lets throw a FilterException is the operand and parameters don't match
        // Today - the only time this can occur is on the Numeric operands.  They
        // could provide alpha or special characters that blow up the HQL
        // The inmemory sort should be able to handle this
        validateFilterParameter();

        // Iterate over the column buckets and do grouping for them.
        String returnHQL = "";

        // Only want to do this if we have valid criterion to filter on
        if (columnMapping.size() > 0) {
            Iterator<Column> columnIter = columnMapping.keySet().iterator();
            while (columnIter.hasNext()) {
                Column column = columnIter.next();
                returnHQL += " ( ";

                List<FilterCriterion> criterionList = columnMapping.get(column);
                Iterator<FilterCriterion> criterionIter = criterionList.iterator();
                while (criterionIter.hasNext()) {
                    FilterCriterion filterCriterion = criterionIter.next();
                    returnHQL += filterCriterion.toHQL();
                    if (criterionIter.hasNext()) {
                        returnHQL += " or ";
                    }
                }
                returnHQL += " ) ";
                if (columnIter.hasNext()) {
                    returnHQL += "and";
                }
            }

        }

        return returnHQL;
    }

    /**
     * This method will apply the filter to the passed in collection.  Objects
     * that don't pass the filter will be removed from the collection
     * @param objectsToFilter the complete object list to apply the filter toward
     * @param du the DisplayUtilities object.
     * @throws NoSuchMethodException standard reflection exception
     * @throws InvocationTargetException standard reflection exception
     * @throws IllegalAccessException standard reflection exception
     */
    public void applyFilter(List<DataObject> objectsToFilter, DisplayUtilities du)
        throws InvocationTargetException, IllegalAccessException, 
        NoSuchMethodException, JSONException {
        // We need to organize the list of filter criteria into like column
        // buckets.  This is because we or the results between like columns
        // and and the results between columns
        HashMap<Column, List<FilterCriterion>> columnMapping =
            new HashMap<Column, List<FilterCriterion>>();

        for (FilterCriterion filterCriterion : getFilterCriteria()) {
            // We only want to include filter criterion for which an inmemory filter
            // is appropriate.
            if (filterCriterion.getColumn().getFilterType() == ColumnFilterType.InMemory) {
                List<FilterCriterion> criterionList = columnMapping.get(filterCriterion.getColumn());
                if (criterionList == null) {
                    // We have never encountered this column before - therefore we
                    // have no list - go ahead and create the list and put it in the map
                    criterionList = new LinkedList<FilterCriterion>();
                    criterionList.add(filterCriterion);
                    columnMapping.put(filterCriterion.getColumn(), criterionList);
                } else {
                    // We have encountered this criterion before - therefore
                    // get the list and add this criterion
                    criterionList.add(filterCriterion);
                }
            }
        }

        // Lets throw a FilterException is the operand and parameters don't match
        // Today - the only time this can occur is on the Numeric operands.  They
        // could provide alpha or special characters that blow up the HQL
        // The inmemory sort should be able to handle this
        validateFilterParameter();

        if (columnMapping.size() > 0) {
            Iterator<DataObject> dataObjectIter = objectsToFilter.iterator();
            while (dataObjectIter.hasNext()) {
                // We need to now apply the filters - and we will do so with or-ing
                // like columns and and-ing different columns
                Iterator<Column> columnIter = columnMapping.keySet().iterator();
                boolean andResults = true;
                DataObject dataObject = dataObjectIter.next();
                while (columnIter.hasNext()) {
                    boolean orResults = false;
                    // We have a column - now get all of the FilterCriterion for this
                    // column.  We need to or the results of the filter criteria
                    Column column = columnIter.next();
                    List<FilterCriterion> criterionList = columnMapping.get(column);
                    Iterator<FilterCriterion> criterionIter = criterionList.iterator();
                    while (criterionIter.hasNext()) {
                        FilterCriterion filterCriterion = criterionIter.next();
                        OperandType operand = OperandType.getById(filterCriterion.getOperand().getId());
                        orResults |= operand.filter(dataObject, filterCriterion, du);
                    }
                    andResults &= orResults;
                }
                if (!andResults) {
                    dataObjectIter.remove();
                }
            }
        }
    }

    /**
     * This method will replace the query with all named parameters from
     * the filter.
     * @param query the query object constructed
     */
    public void fillInNamedParameters(Query query) {
        // Iterate over the filter criterion and if we encounter a date criterion
        // for a database filter - we have work to do
        for (FilterCriterion filterCriterion : getFilterCriteria()) {
            if (filterCriterion.getOperand().getType() == Operand.TYPE_DATE
                && filterCriterion.getColumn().getFilterType() == ColumnFilterType.Database
                && filterCriterion.getOperand().getId() != OperandType.EMPTY_DATE.getId()) {
                int filterTimeWindowId = Integer.parseInt(filterCriterion.getValue1());
                FilterTimeWindow filterTimeWindow =
                    FilterTimeWindow.getById(filterTimeWindowId);
                Date dateOffset = filterTimeWindow.applyOffset(new Date());
                query.setTimestamp(filterCriterion.getColumn().getField()
                                   + filterCriterion.getIndex(), dateOffset);
            }
        }
    }

    /**
     * This methos is responsible for detecting a filter parameter problem and
     * throwing the DataAccessException.
     * @throws FilterInvalidParameterException - this is throw when an invalid
     * value is submitted for filtering.
     */
    private void validateFilterParameter() throws FilterInvalidParameterException {
        // Iterate over the filter criterion and today, the only operand we
        // can screw up is the Numeric operands - ensure that the parameter
        // set to it can be a number
        for (FilterCriterion filterCriterion : getFilterCriteria()) {
            int filterCriterionOperandType = filterCriterion.getOperand().getId().intValue();
            if (filterCriterionOperandType == OperandType.EQUAL.getId()
                || filterCriterionOperandType == OperandType.GREATER_THAN.getId()
                || filterCriterionOperandType == OperandType.GREATHER_THAN_OR_EQUAL.getId()
                || filterCriterionOperandType == OperandType.LESS_THAN.getId()
                || filterCriterionOperandType == OperandType.LESS_THAN_OR_EQUAL.getId()
                || filterCriterionOperandType == OperandType.BETWEEN.getId()) {

                try {
                    // We now know we are expecting a number
                    Double.valueOf(filterCriterion.getValue1());
                } catch (NumberFormatException nfe) {
                    throw new FilterInvalidParameterException(
                        filterCriterion.getOperand().getType(), filterCriterion.getValue1());
                }

                if (filterCriterionOperandType == OperandType.BETWEEN.getId()) {
                    try {
                        // We now know we are expecting a number
                        Double.valueOf(filterCriterion.getValue2());
                    } catch (NumberFormatException nfe) {
                        throw new FilterInvalidParameterException(
                            filterCriterion.getOperand().getType(), filterCriterion.getValue2());
                    }
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 