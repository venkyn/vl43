/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 *
 *
 * @author svoruganti
 */
public class SummaryLocationRoot extends BaseModelObject {

    //serialVerion id for Summary class
    private static final long serialVersionUID = -7698234690627054434L;

    // Message key translated wehn building the summary
    private String position;


    // A reference to an included FreeMarker File
    private Summary summary;


   /**
     * Returns the Summary.
     * @return Summary
     */
    public Summary getSummary() {
        return summary;
    }

    /**
     * Sets the summary.
     * @param summary to set
     */
    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    /**
     * Returns the position.
     * @return String
     */
    public String getPosition() {
        return position;
    }



    /**
     * Sets the position.
     * @param position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }



    /**
     * Standard equals() implementation to compare object state for SummaryLocation.
     *
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SummaryLocation)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        SummaryLocation target = (SummaryLocation) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getPosition(), target.getPosition())
            .isEquals();
    }

    /**
     * Standard hashcode() implementation.
     *
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getPosition()).append(this.getPosition()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given SummaryLocation.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "position", this.getPosition()).append(
                "sourceFile", this.getPosition()).toString();
    }



}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 