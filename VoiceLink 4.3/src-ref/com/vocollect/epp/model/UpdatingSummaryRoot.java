/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


/**
 * This class defines model for UpdatingSummary.
 *
 * @author svoruganti
 */
public class UpdatingSummaryRoot extends Summary {

    //serialVerion id for UpdatingSummary class
    private static final long serialVersionUID = -7698223690627054434L;

    // action url used to access data asynchronously
    private String actionUrl;

    // action method used to access data asynchronously
    private String actionMethod;

    // The amount of seconds between asynchronous refreshes
    private int refreshRate;


    /**
     * Returns the actionMethod.
     * @return String
     */
    public String getActionMethod() {
        return actionMethod;
    }

    /**
     * Sets the actionMethod.
     * @param actionMethod to set
     */
    public void setActionMethod(String actionMethod) {
        this.actionMethod = actionMethod;
    }

    /**
     * Returns the actionUrl.
     * @return String
     */
    public String getActionUrl() {
        return actionUrl;
    }

    /**
     * Sets the actionUrl.
     * @param actionUrl to set
     */
    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    /**
     * Returns the refreshRate.
     * @return int
     */
    public int getRefreshRate() {
        return refreshRate;
    }

    /**
     * Sets the refreshRate.
     * @param refreshRate to set
     */
    public void setRefreshRate(int refreshRate) {
        this.refreshRate = refreshRate;
    }

    /**
     * {@inheritDoc}
     *
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return "UpdatingSummary";
    }


    /**
     * Standard equals() implementation to compare object state for UpdatingSummary.
     *
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UpdatingSummary)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        UpdatingSummary target = (UpdatingSummary) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getActionUrl(), target.getActionUrl()).append(this.getActionMethod(), this.getActionMethod())
            .append(this.getRefreshRate(), target.getRefreshRate())
            .isEquals();
    }

    /**
     * Standard hashcode() implementation.
     *
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getActionUrl())
            .append(this.getActionMethod())
            .append(this.getRefreshRate()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given UpdatingLocation.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "actionUrl", this.getActionUrl())
            .append("actionMethod", this.getActionMethod())
            .append("refreshRate", this.getRefreshRate()).toString();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 