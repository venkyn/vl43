/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * DataType class - represents a the data type of a table component
 * element. THIS CLASS IS NOT USED YET. 
 * 
 */
public class DataTypeRoot extends BaseModelObject {

    private static final long serialVersionUID = -7698234690627067914L;

    private String type;

    /**
     * {@inheritDoc}
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DataType)) {
            return false;
        }
        final DataType dtype = (DataType) o;
        if (type != null) {
            return type.equals(dtype.getType());
        } else {
            return (dtype.getType() == null);
        }
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return (type != null ? type.hashCode() : 0);
    }

    /**
     * {@inheritDoc}
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", this.type).append(
            "id", getId()).toString();
    }

    
    /**
     * Getter for the type property.
     * @return the type value.
     */
    public String getType() {
        return type;
    }

    
    /**
     * Setter for the type property.
     * @param type the type value.
     */
    public void setType(String type) {
        this.type = type;
    }

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 