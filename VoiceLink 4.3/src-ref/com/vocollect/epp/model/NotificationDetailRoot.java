/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * This class defines model for notification detail.
 * 
 * @author Vanitha Subramani
 */
public class NotificationDetailRoot extends BaseModelObject {

    private static final long serialVersionUID = -8351944436925732988L;

    private String key;
    private String value;
    private String renderType;
    private String ordering;

    /**
     * Returns the key.
     * @return String
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the key.
     * @param key to set
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Returns the ordering.
     * @return String
     */
    public String getOrdering() {
        return ordering;
    }

    /**
     * Sets the ordering.
     * @param ordering to set
     */
    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    /**
     * Returns the renderType.
     * @return String
     */
    public String getRenderType() {
        return renderType;
    }

    /**
     * Sets the renderType.
     * @param renderType to set
     */
    public void setRenderType(String renderType) {
        this.renderType = renderType;
    }

    /**
     * Returns the notification detail value.
     * @return String
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the notification detail value..
     * @param value notification detail value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    @Override
    public String getDescriptiveText() {
        return getKey();
    }

    /**
     * Standard equals() implementation to compare object state for Notification.
     * 
     * @param obj object to compare to the given object (this)
     * @return true implies object equality, false implies inequality
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NotificationDetail)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        NotificationDetail target = (NotificationDetail) obj;
        return new EqualsBuilder().append(this.getId(), target.getId()).append(
            this.getKey(), target.getKey()).append(
            this.getValue(), target.getValue()).append(
            this.getRenderType(), target.getRenderType()).append(
            this.getOrdering(), target.getOrdering()).isEquals();
    }

    /**
     * Standard hashcode() implementation.
     * 
     * @return this object's hashcode value {@inheritDoc}
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int hashCode1 = 17;
        final int hashCode2 = 37;
        return new HashCodeBuilder(hashCode1, hashCode2).append(this.getId())
            .append(this.getKey()).append(this.getValue()).append(
                this.getRenderType()).append(this.getOrdering()).toHashCode();
    }

    /**
     * Standard toString() implementation.
     * 
     * @return String representation of the given Notification.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("Id", this.getId()).append(
            "Key", this.getKey()).append("Value", this.getValue()).append(
            "RenderType", this.getRenderType()).append(
            "Ordering", this.getOrdering()).toString();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 