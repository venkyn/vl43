/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import com.vocollect.epp.logging.exceptions.DuplicateErrorCodeException;
import com.vocollect.epp.logging.exceptions.ErrorOutOfRangeException;

/**
 * Basis for an error code for the Vocollect EPP. This provides a simple
 * interface for the engineer, while ensuring that error codes are not
 * duplicated.
 *
 * <pre></pre>
 *
 * It is intended to be used as a base class for error codes.
 *
 * <pre></pre>
 *
 * The error code consists of a prefix, held by the Range, which becomes part of
 * the identification of the error code's string representation. Consequently,
 * each project or packace or whatever need only be concerned with the error
 * codes contained in its own space as defined by the error code prefix.
 *
 * <pre></pre>
 *
 * Each distinct class holds a list of static, final public variables which
 * represent the error codes. The base class will check that the error codes are
 * not duplicated within the class, nor across classes. It will check that the
 * ranges are discrete as well.
 *
 * <pre></pre>
 *
 * The base class will throw a runtime exception if any of the above turn out to
 * be false.
 *
 * <pre></pre>
 *
 * It is a requirement that the test case for the error ranges instantiate at
 * least one error of each class. This will ensure that the duplicates are
 * caught in unit testing and not later.
 *
 * <pre>
 *    Example:
 *    class AnError extends ErrorCode {
 *
 *      public static final AnError
 *        NO_ERROR = new AnError(), // Required to establish the range
 *        someError = new AnError( 100 )
 *        // New errors go here
 *        private AnError(){
 *        super(&quot;Ex&quot;, 100, 1000); // Specify prefix &amp; range in default constructor
 *      }
 *
 *      private AnError( long err ){
 *        // All errors of this class share the same range &amp; prefix
 *        super( AnError.noError, err );
 *      }
 *     }
 *
 * </pre>
 *
 * @author dgold
 *
 * @invariant getRangeMin() <= getErrorCode() <= getRangeMax()
 *
 */
public abstract class ErrorCode {

    // The error code this instance represents
    private long       errorCode;

    // Flag to stop you from setting the error code 2x
    private boolean    errorCodeIsSet = false;

    // One list of all errors registered for 'this' class.
    private ErrorList  errors         = null;

    // Ref to the instance of an error range.
    private ErrorRange range;

    /**
     * This constructor needs to be called first - It sets the range, and
     * registers the min/max errors.
     *
     * @param prefix - the text to appear before each error
     * @param low - the low range of the error code values
     * @param high - the upper range of the error code values
     */
    public ErrorCode(String prefix, long low, long high) {
        super();
        setRange(new ErrorRange(prefix, low, high));
        setErrors(new ErrorList());
    }

    /**
     * Copy constructor, which replaces the error code of the new instance with
     * the new code.
     * @param template ErrorCode instance to copy from
     * @param newCode The resulting error code for the new instance
     * @postcondition ( getRange() == template.getRange() )&& ( getErrorCode ==
     *                newCode ))
     */
    public ErrorCode(ErrorCode template, long newCode) {

        // Pay attention to the order in which these are run.
        // setprefix depends on setRange, register depends on setErrors
        setRange(template.getRange());
        setPrefix(template.getPrefix());
        setErrors(template.getErrors());
        setErrorCode(newCode);
        register(newCode);
    }

    /**
     * Use the range provided for self's range.
     * @param theRange - an range for the errors to exist in
     */
    private void setRange(ErrorRange theRange) {
        this.range = theRange;
    }

    /**
     * Get the range object.
     * @return the range object
     */
    protected ErrorRange getRange() {
        return this.range;
    }

    /**
     * Get the upper limit of the range of error codes this can represent.
     *
     * @return Returns the rangeMax.
     */
    public long getRangeMax() {
        return getRange().getHighEndOfRange();
    }

    /**
     * Get the lower end of the range this can represent.
     *
     * @return Returns the rangeMin.
     */
    public long getRangeMin() {
        return getRange().getLowEndOfRange();
    }

    /**
     * Gets the error code represented by this instance.
     *
     * @return Returns the errorCode.
     */
    public long getErrorCode() {
        return this.errorCode;
    }

    /**
     * Sets the error code.
     * @param errCode - The errorCode to set.
     * @precondition the error code has not been set
     * @postcondition ( getErrorCode() == errorCode ) && ( isErrorCodeSet() == true )
     */
    private void setErrorCode(long errCode) {
        // Need a guard against re-setting the error code? Only if this is
        // public.
        this.errorCode = errCode;
        setErrorCodeIsSet();
    }

    /**
     * @return Returns the errorCodeIsSet.
     */
    public boolean isErrorCodeSet() {
        return this.errorCodeIsSet;
    }

    /**
     * Set the flag to indicate that the error code ihas been set.
     */
    private void setErrorCodeIsSet() {
        this.errorCodeIsSet = true;
    }

    /**
     * Clear the flag indicating the error code is set. To enable reuse of the
     * ErrorCode instance
     *
     */
    public void clearErrorCodeIsSet() {
        this.errorCodeIsSet = false;
    }

    /**
     * Register the individual error. Delegates to ErrorList.register()
     *
     * If the error code err exists, throw an exception If the error code is out
     * of range, throw an exception
     *
     * @param err - the error to be registered
     * @precondition - err has not been registered
     * @postcondition - the error is contained in the error list
     */
    private void register(long err) {
        if (!getRange().isInRange(err)) {
            throw new ErrorOutOfRangeException(
                "Error code " + err + " out of range ["
                + getRange().getLowEndOfRange() + ","
                + getRange().getHighEndOfRange() + "] for " + toString());
        }
        if (errors.isRegistered(err)) {
            throw new DuplicateErrorCodeException(
                "Duplicate error code " + err + " for " + toString());
        }
        errors.register(err);
        this.errorCode = err;
    }

    /**
     * Get the string representation for this error.
     * @return String containing {prefix}-{errorCode}
     */
    @Override
    public String toString() {
        return new String(getPrefix() + "-" + getErrorCode());
    }

    /**
     * @return Returns the prefix.
     */
    public String getPrefix() {
        return this.range.getPrefix();
    }

    /**
     * @return the resource message key which is associated with this ErrorCode
     *         by default.
     */
    public String getDefaultMessageKey() {
        return "errorCode.message." + getPrefix() + getErrorCode();
    }

    /**
     * @param prefix The prefix to set.
     */
    private void setPrefix(String prefix) {
        this.range.setPrefix(prefix);
    }

    /**
     * Getter for the errors property.
     * @return - ErrorList of errors
     */
    private ErrorList getErrors() {
        return this.errors;
    }

    /**
     * Setter for the errors property.
     * @param errs - set of errors to be set
     */
    private void setErrors(ErrorList errs) {
        this.errors = errs;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 