/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/*
 * Copyright 2001-2004 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.vocollect.epp.errors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * A class that encapsulates messages. Messages can be added and associated with
 * different categories. If a category is not specified when a message is added,
 * it goes into the <code>DEFAULT_CATEGORY</code>.
 * <p>
 * Each individual message is described by an <code>UserMessage</code> object,
 * which contains a message key (to be looked up in an appropriate message
 * resources database), and placeholder arguments used for parametric
 * substitution in the resulting message.
 * <p>
 * <strong>IMPLEMENTATION NOTE</strong> - It is assumed that these objects are
 * created and manipulated only within the context of a single thread.
 * Therefore, no synchronization is required for access to internal collections.
 *
 * @author Dennis Doubleday (partially derived from Struts 1.2.4 ActionMessages)
 */
public class MessageMap implements Serializable, Iterable<UserMessage> {

    private static final long serialVersionUID = 1023829521489438873L;

    /**
     * The default category that messages are added to if no category is
     * specified.
     */
    public static final String             DEFAULT_CATEGORY = "global";

    /**
     * The error category that may be used to display the message in the error format.
     */
    public static final String             ERROR_CATEGORY = "error";

    /**
     * The error category that may be used to display the message in the partial success format.
     */
    public static final String             ERROR_PARTIAL_SUCCESS = "partial_success";

    /**
     * More information that is result type-agnostic which builds the View Details bubble in the UI.
     */
    public static final String             MORE_INFO_MESSAGES = "more_info";

    /**
     * The accumulated set of <code>UserMessage</code> objects (represented as
     * an ArrayList) for each category, keyed by category name.
     * @contained UserMessage
     */
    private Map<String, List<UserMessage>> messages
                                    = new HashMap<String, List<UserMessage>>();

    /**
     * Create an empty <code>MessageMap</code> object.
     */
    public MessageMap() {
    }

    /**
     * Add a message to the set of messages for the default category. An order
     * of the category/key is maintained based on the initial addition of the
     * category/key.
     *
     * @param message The message to be added
     * @return this object, to enable chain-style adding of messages.
     */
    public MessageMap add(UserMessage message) {

        return add(DEFAULT_CATEGORY, message);

    }

    /**
     * Add a message to the set of messages for the specified category. An order
     * of the category/key is maintained based on the initial addition of the
     * category/key.
     *
     * @param category The message category name. If this is null, the message
     *            will be added to the <code>DEFAULT_CATEGORY</code>
     * @param message The message to be added. This must not be null.
     * @return this object, to enable chain-style adding of messages.
     */
    public MessageMap add(String category, UserMessage message) {

        if (message == null) {
            // A null message cannot be added to the map.
            throw new IllegalArgumentException("Cannot add null message to map");
        }

        // Use DEFAULT_CATEGORY if the category is null.
        String cat = (category == null ? DEFAULT_CATEGORY : category);

        // Check to see if there is already a list for this category.
        List<UserMessage> list = this.messages.get(cat);

        if (list == null) {
            // Create a list for this category and add it to the map.
            list = new ArrayList<UserMessage>();
            this.messages.put(cat, list);
        }

        // Add the message to the list.
        list.add(message);

        return this;

    }

    /**
     * Adds the messages from the given <code>MessageMap</code> object to this
     * set of messages. The messages are added in the same order they are
     * returned from the source <code>MessageMap</code>. If a message's
     * category is already in the current <code>MessageMap</code> object, it
     * is added to the end of the list for that category.
     *
     * @param newMessages The <code>MessageMap</code> object to be added. This
     *            parameter can be <code>null</code>, in which case nothing
     *            is added.
     *
     * @return this object, to enable chain-style adding of messages.
     */
    public MessageMap add(MessageMap newMessages) {

        if (newMessages != null) {
            // Only add if the argument is non-null

            // loop over categories
            for (String category : newMessages.getCategoryNames()) {
                // loop over messages for each category
                for (UserMessage message : newMessages.getMessages(category)) {
                    // Add the message to this map.
                    this.add(category, message);
                }
            }
        }

        return this;
    }

    /**
     * Clear all messages recorded by this object.
     */
    public void clear() {

        this.messages.clear();

    }

    /**
     * Determines whether or not this <code>MessageMap</code> has messages.
     * @return <code>true</code> if there are no messages recorded in this
     *         collection, or <code>false</code> otherwise.
     */
    public boolean isEmpty() {

        return (this.messages.isEmpty());

    }

    /**
     * Return the set of all added messages, regardless of category. If there
     * are no messages recorded, an empty List is returned.
     * <p>
     * Note that this list does not contain the category information, just the
     * messages.
     * @return a List containing all messages, in any category, or an empty List
     *         if there are no messages.
     */
    public Iterator<UserMessage> iterator() {

        if (this.messages.isEmpty()) {
            return new ArrayList<UserMessage>().iterator();
        }

        ArrayList<UserMessage> results = new ArrayList<UserMessage>();

        // For each category entry in the Map
        for (String key : this.messages.keySet()) {
            // Get the list of messages
            List<UserMessage> list = this.messages.get(key);
            // For each message in the list
            for (UserMessage message : list) {
                // Add it to the overall list
                results.add(message);
            }
        }

        return results.iterator();
    }
    /**
     * Return the messages related to a specific category.
     *
     * @param category The category name
     * @return the Iterable over messages in the specified category, or an
     * empty Iterable there are none.
     */
    public Iterable<UserMessage> getMessages(String category) {

        List<UserMessage> catList = this.messages.get(category);
        if (catList == null) {
            return new ArrayList<UserMessage>();
        } else {
            return catList;
        }
    }

    /**
     * Return the list of category names for which at least one message has been
     * recorded. If there are no messages, an empty <code>List</code> is
     * returned. If you have recorded global messages, the <code>String</code>
     * value of <code>DEFAULT_CATEGORY</code> will be one of the returned
     * category names.
     * @return the List of category names.
     */
    public List<String> getCategoryNames() {

        return new ArrayList<String>(this.messages.keySet());
    }

    /**
     * Return the number of messages in the map, for all categories.
     * <p>
     * <strong>NOTE</strong> - it is more efficient to call
     * <code>isEmpty</code> if all you care about is whether or not there are
     * any messages at all.
     * @return the number of messages om the map.
     */
    public int getSize() {

        int total = 0;

        // For each entry in the Map, get the size of the associated List
        // and add to the total.
        for (String key : this.messages.keySet()) {
            total += this.messages.get(key).size();
        }
        return total;

    }

    /**
     * Getthe number of messages associated with the specified category.
     *
     * @param category The category name
     * @return the number of messages associated with the specified category
     */
    public int getSize(String category) {

        List<UserMessage> catList = this.messages.get(category);
        return (catList == null) ? 0 : catList.size();
    }

    /**
     * Get a String representation of this object. The representation is in the
     * following format:
     *
     * <pre>
     *   MessageMap(Category:&lt;catname&gt;(Messages:&lt;key&gt;,&lt;key&gt;,...),...)
     * </pre>
     *
     * @see java.lang.Object#toString()
     * @return the String representation of this object.
     */
    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 