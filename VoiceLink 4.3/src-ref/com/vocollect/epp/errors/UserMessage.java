/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Modifications Copyright (c) 2005 Vocollect, Inc.
 * Pittsburgh, PA 15235
 * All rights reserved.
 */

package com.vocollect.epp.errors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * An encapsulation of a localizable error message, consisting of a message key
 * (to be used to look up message text in an appropriate message resources
 * database) plus placeholder objects that can be used for parametric
 * replacement in the message text.
 *
 * @author Dennis Doubleday
 */
public class UserMessage implements Serializable {

    private static final long serialVersionUID = -3990123863221921241L;

    /**
     * If an argument to the UserMessage is prefixed with this value,
     * then it indicates that the argument should be evaluated aa a resource
     * key prior to evaluation of the entire message. This will only work
     * on messages displayed via the <code>BaseAction</code> methods that
     * add errors and messages to the request or session.
     */
    public static final String KEY_EVAL_PREFIX = "KEY%";

    /**
     * The message key for this message.
     */
    private String  key              = null;

    /**
     * The replacement values for this mesasge.
     */
    private List<Object>  values            = new ArrayList<Object>();

    /**
     * Construct an error message with no replacement values.
     *
     * @param key Message key for this message
     */
    public UserMessage(String key) {
        this.key = key;
    }

    /**
     * Construct an error message with the specified replacement values.
     *
     * @param key Message key for this message
     * @param escapeHTML whether to escape HTML in the text
     * @param args Varargs-type list of replacement values
     */
    @SuppressWarnings("unchecked")
    public UserMessage(String key, boolean escapeHTML, Object... args) {
        this.key = key;
        for (Object object : args) {
            if ((object instanceof String) && (escapeHTML)) {
                object = com.vocollect.epp.web.util.HTMLUtil.escapeHTML((String) object);
            }
            values.add(object);
        }
    }

    /**
     * Construct an error message with the specified replacement values.
     * This method DOES escape HTML.
     *
     * @param key Message key for this message
     * @param args Varargs-type list of replacement values
     */
    @SuppressWarnings("unchecked")
    public UserMessage(String key, Object... args) {
        this(key, true, args);
    }

    /**
     * Construct an error messge withe the specified ErrorCode.
     * @param code - the error code for this message
     */
    public UserMessage(ErrorCode code) {
        this(code.getDefaultMessageKey());
    }

    /**
     * Construct an error message with the specified replacement values.
     *
     * @param code ErrorCode for this message
     * @param args Varargs-type list of replacement values
     */
    public UserMessage(ErrorCode code, Object... args) {
        this(code.getDefaultMessageKey(), args);
    }

    /**
     * Get the message key for this message.
     * @return the resource key for this message.
     */
    public String getKey() {

        return this.key;

    }

    /**
     * Get the replacement values for this message.
     * @return the List of replacement values for this message, or the empty
     * List if there are none.
     */
    public List<Object> getValues() {

        return this.values;

    }

    /**
     * Returns a String in the standard <code>ToStringBuilder</code> single
     * line format.
     *
     * @see java.lang.Object#toString()
     * @return the String description of this object.
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Mark the specified arg as one that needs to go through an eval
     * step before the full message is translated.
     * @param arg the arg to mark
     * @return the original arg with a prefix that indicates the
     * arg needs to be localized before insertion into the message.
     */
    public static String markForLocalization(String arg) {
        return KEY_EVAL_PREFIX + arg;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 