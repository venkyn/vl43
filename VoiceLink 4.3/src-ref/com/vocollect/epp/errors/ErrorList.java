/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import java.util.TreeSet;

/**
 * Class representing a list of error codes. This list is to be static
 * (unchanging) at runtime after initialization, so no 'remove' functionality is
 * present. This is contained in the ErrorCode class to provide a list of ALL
 * errors registered in the system.
 * @author dgold
 *
 */
class ErrorList {

    /**
     * List of error codes.
     */
    private TreeSet<Long> errors = new TreeSet<Long>();

    /**
     * Default constructor.
     */
    public ErrorList() {
        super();
    }

    /**
     * Add an error to the list.
     * @param errorCode - the error code to be added
     * @return true if the error was added, false if it already exists
     */
    public boolean addError(long errorCode) {
        if (isRegistered(errorCode)) {
            return false;
        }
        register(errorCode);
        return true;
    }

    /**
     * Add the error code to the list.
     * @param errorCode - the error code to be registered
     */
    public void register(long errorCode) {
        this.errors.add(new Long(errorCode));
    }

    /**
     * Check to see if this error code was already registered.
     * @param errorCode - the error code to be checked
     * @return true if the error was previously registered
     */
    public boolean isRegistered(long errorCode) {
        return this.errors.contains(new Long(errorCode));
    }


    /**
     * Getter for the errors property.
     * @return - TreeSet of errors
     */
    public TreeSet<Long> getErrors() {
        return errors;
    }


    /**
     * Setter for the errors property.
     * @param errs - TreeSet of errors to assign
     */
    public void setErrors(TreeSet<Long> errs) {
        this.errors = errs;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 