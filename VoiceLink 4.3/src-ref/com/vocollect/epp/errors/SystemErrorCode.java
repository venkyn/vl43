/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

/**
 * Class to hold the instances of error codes for general EPP errors
 * that aren't related to a specific featuere.
 *
 * @author Dennis Doubleday
 */
public final class SystemErrorCode extends ErrorCode {

    /**
     * Lower boundary of the EPP security error range.
     */
    public static final int LOWER_BOUND = 100;

    /**
     * Upper boundary of the EPP security error range.
     */
    public static final int UPPER_BOUND = 999;

    /**
     * No error, just the base initialization.
     */
    public static final SystemErrorCode NO_ERROR
        = new SystemErrorCode();

    /**
     * A system entity was not found.
     */
    public static final SystemErrorCode ENTITY_NOT_FOUND
        = new SystemErrorCode(100);

    /**
     * A system entity could not be updated.
     */
    public static final SystemErrorCode ENTITY_NOT_UPDATEABLE
        = new SystemErrorCode(101);

    /**
     * A system entity could not be updated.
     */
    public static final SystemErrorCode ENTITY_NOT_DELETEABLE
        = new SystemErrorCode(102);

    /**
     * A system entity could not be updated.
     */
    public static final SystemErrorCode ENTITY_NOT_CREATEABLE
        = new SystemErrorCode(103);

    /**
     * An internal error occured.
     */
    public static final SystemErrorCode UNHANDLED_EXCEPTION
        = new SystemErrorCode(104);

    /**
     * Unable to copy the zip data to output stream.
     */
    public static final SystemErrorCode UNABLE_TO_COPY_DATA_TO_ZIP_OUTPUT_STREAM
        = new SystemErrorCode(105);

    /**
     * An internal error occured.
     */
    public static final SystemErrorCode FLUSH_OUTPUT_STREAM_FAILED
        = new SystemErrorCode(106);

    /**
     * An internal error occured.
     */
    public static final SystemErrorCode UNABLE_TO_READ_SOURCE_STREAM
        = new SystemErrorCode(107);


    /**
     * A JavaScript error has occurred.
     */
    public static final SystemErrorCode JAVASCRIPT_ERROR
        = new SystemErrorCode(108);

    /**
     * A JavaScript fatal error has occurred.
     */
    public static final SystemErrorCode JAVASCRIPT_FATAL
        = new SystemErrorCode(109);

    /**
     * An illigal attampt has been made to delete.
     */
    public static final SystemErrorCode UNAUTHORIZED_SITE_DELETE
        = new SystemErrorCode(110);

    /**
     * An error in application configuration.
     */
    public static final SystemErrorCode CONFIGURATION_ERROR
        = new SystemErrorCode(111);


    /**
     * An error in retrieving data for the table component.
     */
    public static final SystemErrorCode DATA_PROVIDER_FAILURE
        = new SystemErrorCode(112);

    /**
     * An error in sending email.
     */
    public static final SystemErrorCode EMAIL_FAILURE
        = new SystemErrorCode(113);

    /**
     * An error in creating a notification.
     */
    public static final SystemErrorCode NOTIFICATION_FAILURE
        = new SystemErrorCode(114);

    /**
     * An error while doing purge/archive.
     */
    public static final SystemErrorCode PURGE_ARCHIVE_FAILURE
        = new SystemErrorCode(115);

    /**
     * An error in input data.
     */
    public static final SystemErrorCode INVALID_DATA
        = new SystemErrorCode(116);

    /**
     * An error with ldap services.
     */
    public static final SystemErrorCode LDAP_EXCEPTION
        = new SystemErrorCode(117);

    /**
     * An error with hibernate data services.
     */
    public static final SystemErrorCode DATABASE_EXCEPTION
        = new SystemErrorCode(118);
    
    /**
     * An error with hibernate data services.
     */
    public static final SystemErrorCode ALERT_JOB_FAILURE
        = new SystemErrorCode(119);    


    /**
     * Constructor.
     */
    private SystemErrorCode() {
        super("EPP", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    private SystemErrorCode(long err) {
        // The error is defined in the SystemErrorCode context.
        super(SystemErrorCode.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 