/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import com.vocollect.epp.logging.exceptions.BadErrorRangeException;
import com.vocollect.epp.logging.exceptions.DuplicateErrorTypeException;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Class that represents a range of error codes. Range is inclusive, as is
 * implied by the condition that low <= high The intent is to have one range
 * object for a single class inheriting from ErrorCode. The subclass will not
 * allow any errors outside of this range. This range will self-check to make
 * sure that ranges are discrete (no overlap)
 * @invariant getlowEndOfRange() <= getHighEndOfRange()
 * @author dgold
 *
 */
public class ErrorRange {

    // Low bound of range
    private long                         lowEndOfRange;

    // High bound of range
    private long                         highEndOfRange;

    // Flag indicating that the range is (or not) set
    private boolean                      rangeSet  = false;

    // Part of the identification of the errors in this range.
    private String                       prefix;

    // List of all ranges
    private static LinkedList<ErrorRange> rangeList = new LinkedList<ErrorRange>();

    /**
     * Constructor. This is supplied so that you can create an ErrorRange as a
     * temp, or as a parameter to a search.
     */
    public ErrorRange() {
        super();
    }

    /**
     * Construct the object and set the range.
     * @precondition low <= high
     * @postcondition ((getHighEndOfRange() == high) && (getLowEndOfRange() ==
     *                low) && (isRangeSet() == true))
     * @param pre - the initial text describing the error
     * @param low - the lower boundary of the error range
     * @param high - the upper boundary of the error range
     */
    public ErrorRange(String pre, long low, long high) {
        super();
        setRange(pre, low, high);
        rangeList.add(this);
    }

    /**
     * Method to set the range. This is provided so that you could have a list
     * of ranges and use a range object to search for them. There are no
     * separate methods to set the hi/low of the range. Attempting to do so
     * leads to too many inconsistencies, so you set both at the same time with
     * the result that the only check is that low <= high
     * @param pre - the initial text describing the error
     * @param low Low end of rangre
     * @param high Upper end of range
     * @precondition low <= high
     * @postcondition ((getLowEndOfRange() == low ) && (getHighEndOfRange() ==
     *                high ) && (isRangeSet() == true))
     */
    public void setRange(String pre, long low, long high) {
        if (low > high) {
            throw new BadErrorRangeException(
                "Low range bound exceeds high bound [" + low + ", " + high
                    + "]");
        }
        setPrefix(pre);
        if (rangeOverlap(prefix, low, high)) {
            throw new DuplicateErrorTypeException(
                " Duplicate range [" + low + ", " + high + "]");
        }
        this.lowEndOfRange = low;
        this.highEndOfRange = high;
        setRangeSet();
    }

    /**
     * Check to see if the requested range [min,max] intersects with any other
     * range that is already registered.
     *
     * @precondition min < max
     * @param pre - the initial text describing the error
     * @param min - low end of requested range
     * @param max - high end of requested range
     * @return - true if the requested range overlaps any other registered range
     */
    public static boolean rangeOverlap(String pre, long min, long max) {

        // Loop through all of the existing ErrorRanges and see if we have
        // any ranges that overlap.
        // Range1.max < Range2.min <= Range2.max < Range3.min
        Iterator<ErrorRange> i = rangeList.iterator();
        while (i.hasNext()) {
            ErrorRange registeredError = i.next();
            if ((registeredError.getPrefix() == pre)
                && (registeredError.getLowEndOfRange() <= max)
                && (registeredError.getHighEndOfRange() >= min)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if a range with the given min,max has been registered. Supplied
     * primarily for use in searching a list of ranges.
     * @param min - Low end of range
     * @param max - High end of range
     * @return true if the range has already been registered
     */
    public static boolean rangeExists(long min, long max) {
        Iterator<ErrorRange> i = rangeList.iterator();
        while (i.hasNext()) {
            ErrorRange registeredError = i.next();
            if ((registeredError.getLowEndOfRange() == min)
                && (registeredError.getHighEndOfRange() == max)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determine if a range with matching min,max has been registered.
     * @param range Range to match
     * @return true if a matching range is found, false OW
     */
    public static boolean rangeExists(ErrorRange range) {
        return rangeExists(range.getLowEndOfRange(), range.getHighEndOfRange());
    }

    /**
     * If the range is not already registered, add it to the range registrations.
     * @precondition min < max
     * @param pre - the text describing the error class
     * @param min - Low end of range
     * @param max - High end of range
     * @return - true if the registered is added, false otherwise
     */
    public static boolean registerRange(String pre, long min, long max) {
        if (!rangeOverlap(pre, min, max)) {
            ErrorRange range = new ErrorRange(pre, min, max);
            rangeList.add(range);
            return true;
        }
        return false;
    }

    /**
     * If this range is not already registered, add it to the range
     * registrations.
     * @precondition min < max
     *
     * @return true if the range can be registered, false otherwise.
     */
    public boolean register() {
        return registerRange(
            getPrefix(), getLowEndOfRange(), getHighEndOfRange());
    }

    /**
     * Check to see if the candidate can be part of the range represented by
     * this.
     * @param candidate - value to be checked
     * @return true if getLowEndOfRange() <= candidate <= getHighEndOfRange()
     */
    public boolean isInRange(long candidate) {
        return ((getLowEndOfRange() <= candidate) && (getHighEndOfRange() >= candidate));
    }

    /**
     * @return Returns the highEndOfRange.
     */
    public long getHighEndOfRange() {
        return this.highEndOfRange;
    }

    /**
     * @return Returns the lowEndOfRange.
     */
    public long getLowEndOfRange() {
        return this.lowEndOfRange;
    }

    /**
     * @return Returns true if the range has been set
     */
    public boolean isRangeSet() {
        return this.rangeSet;
    }

    /**
     * Set the flag to say that the range has been set.
     * @postcondition isRangeSet() == true
     */
    private void setRangeSet() {
        this.rangeSet = true;
    }

    /**
     * Getter for the prefix property.
     * @return Returns the prefix.
     */
    public String getPrefix() {
        return this.prefix;
    }

    /**
     * Setter for the prefix property.
     * @param pre - The prefix to set.
     */
    public void setPrefix(String pre) {
        this.prefix = pre;
    }


    /**
     * Getter for the rangeList property.
     * @return - the LinkedList rangeList
     */
    public static LinkedList<ErrorRange> getRangeList() {
        return rangeList;
    }


    /**
     * Setter for the rangeList property.
     * @param range - the list to set to rangeList
     */
    public static void setRangeList(LinkedList<ErrorRange> range) {
        ErrorRange.rangeList = rangeList;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 