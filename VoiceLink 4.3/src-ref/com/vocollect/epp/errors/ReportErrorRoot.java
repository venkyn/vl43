/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;



/**
 * 
 *
 * @author cblake
 */
public class ReportErrorRoot extends ErrorCode {
    /**
     * Lower boundary of the EPP logging error range.
     */
    public static final int EPP_LOWER_BOUND  = 9000;

    /**
     * Upper boundary of the EPP logging error range.
     */
    public static final int EPP_UPPER_BOUND  = 9499;

    /** No error, just the base initialization. */
    public static final ReportErrorRoot NO_ERROR = new ReportErrorRoot();

    public static final ReportErrorRoot NO_REPORT_NAME =  new ReportErrorRoot(9000);
    public static final ReportErrorRoot BAD_REPORT_NAME = new ReportErrorRoot(9001);
    public static final ReportErrorRoot JASPER_LOAD_ERROR = new ReportErrorRoot(9002);
    public static final ReportErrorRoot SERVICE_BEAN_NOT_FOUND = new ReportErrorRoot(9003);
    public static final ReportErrorRoot SERVICE_BEAN_NULL = new ReportErrorRoot(9004);
    public static final ReportErrorRoot SERVICE_METHOD_NOT_FOUND = new ReportErrorRoot(9005);
    public static final ReportErrorRoot SERVICE_INVOCATION_ERROR = new ReportErrorRoot(9006);
    public static final ReportErrorRoot SERVICE_NO_COLLECTION = new ReportErrorRoot(9007);
    public static final ReportErrorRoot JASPER_COMPILE_ERROR = new ReportErrorRoot(9008);
    public static final ReportErrorRoot HIBERNATE_SESSION_ERROR = new ReportErrorRoot(9009);
    public static final ReportErrorRoot JASPER_FILL_ERROR = new ReportErrorRoot(9010);
    public static final ReportErrorRoot JASPER_EXPORT_ERROR = new ReportErrorRoot(9011);
    public static final ReportErrorRoot BAD_RENDER_FORMAT = new ReportErrorRoot(9012);
    public static final ReportErrorRoot REPORT_NOT_FOUND = new ReportErrorRoot(9013);
    public static final ReportErrorRoot JASPER_PRINTSERVICE_EXCEPTION = new ReportErrorRoot(9014);
    public static final ReportErrorRoot DATASOURCE_BEAN_NOTFOUND_ERROR = new ReportErrorRoot(9015);
    public static final ReportErrorRoot DATASOURCE_BEAN_UNKOWN_ERROR = new ReportErrorRoot(9016);

    
    /**
     * Constructor.
     */
    protected ReportErrorRoot() {
        super("EPP", EPP_LOWER_BOUND, EPP_UPPER_BOUND);
    }

    /**
     * @param err - error to be logged
     */
    protected ReportErrorRoot(long err) {
        super(ReportErrorRoot.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 