/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dto;

/**
 * Data transfer object used for data exchange between Report wrapper
 * implementations and client classes. This is required so that output of report
 * implementations is library neutral.
 *
 * @author mraj
 */
public class ReportParametersDTO {

    private String name;

    private Class<?> valueClass;

    private String fieldType;

    private String description;

    private String value;

    private String defaultValue;

    private boolean isRequired;

    private boolean isSystemDefined;

    /**
     * Getter for the name property.
     * @return String value of the property
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name property.
     * @param name the new name value
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the valueClass property.
     * @return Class value of the property
     */
    public Class<?> getValueClass() {
        return valueClass;
    }

    /**
     * Setter for the valueClass property.
     * @param valueClass the new valueClass value
     */
    public void setValueClass(Class<?> valueClass) {
        this.valueClass = valueClass;
    }

    /**
     * Getter for the fieldType property.
     * @return String value of the property
     */
    public String getFieldType() {
        return fieldType;
    }

    /**
     * Setter for the fieldType property.
     * @param fieldType the new fieldType value
     */
    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    /**
     * Getter for the description property.
     * @return String value of the property
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter for the description property.
     * @param description the new description value
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Getter for the defaultValue property.
     * @return String value of the property
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Setter for the defaultValue property.
     * @param defaultValue the new defaultValue value
     */
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Getter for the isRequired property.
     * @return boolean value of the property
     */
    public boolean isRequired() {
        return isRequired;
    }

    /**
     * Setter for the isRequired property.
     * @param isRequiredParam the new isRequired value
     */
    public void setRequired(boolean isRequiredParam) {
        this.isRequired = isRequiredParam;
    }

    /**
     * Getter for the isSystemDefined property.
     * @return boolean value of the property
     */
    public boolean isSystemDefined() {
        return isSystemDefined;
    }

    /**
     * Setter for the isSystemDefined property.
     * @param isSystemDefinedParam the new isSystemDefined value
     */
    public void setSystemDefined(boolean isSystemDefinedParam) {
        this.isSystemDefined = isSystemDefinedParam;
    }


    /**
     * Getter for the value property.
     * @return String value of the property
     */
    public String getValue() {
        return value;
    }


    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 