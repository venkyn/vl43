/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ui;

import com.vocollect.epp.model.DataObject;

import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 *
 * @author dkertis
 */
public class ToolTipObject implements DataObject {

    private long   id;
    private Object startValue;
    private Object endValue;
    private int offset;


    /**
     * Constructor.
     * @param id the tooltip ID
     * @param startValue the tooltip start value
     * @param endValue the tooltip end value
     * @param offset the offset to begin getting tooltips
     */
    public ToolTipObject(long id, Object startValue, Object endValue, int offset) {
        super();
        this.setId(id);
        this.startValue = startValue;
        this.endValue = endValue;
        this.offset = offset;
    }

    /**
     * @return the offset value
     */
    public int getOffset() {
        return offset;
    }

    /**
     * @param offset the new offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }

    /**
     * @return the startValue
     */
    public Object getStartValue() {
        return startValue;
    }

    /**
     * @param id the new id value
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getId()
     */
    public Long getId() {
        return this.id;
    }


    /**
     * @param value the startValue
     */
    public void setStartValue(Object value) {
        this.startValue = value;
    }

    /**
     * @return a JSON representation of the tooltip data.
     */
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        try {
            jo.put("startValue", this.getStartValue());
            jo.put("endValue", this.getEndValue());
            jo.put("offset", this.getOffset());
            jo.put("id", this.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jo;
    }

    /**
     * @return the endValue
     */
    public Object getEndValue() {
        return endValue;
    }



    /**
     * @param endValue the new end value
     */
    public void setEndValue(Object endValue) {
        this.endValue = endValue;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getStartValue() + " -  " + this.getEndValue();
     }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 