/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.epp.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * The Navigation class is used in order to calculate how the
 * menu and breadcrumbs are to be built in the freemarker templates
 * or the BaseAction class.
 *
 * @author jgeisler
 */
public class Navigation {

    private String contextRoot = null;

    static final String LEVEL_DIVIDER = "/";
    static final String ACTION_SUFFIX = ".action";
    static final String INPUT_SUFFIX = "!input.action";
    static final String DEFAULT_ACTION_SUFFIX = "/default.action";
    static final String MESSAGE_KEY_PREFIX = "breadcrumbs.";

    static final String DEFAULT_ACTION_NAME = "default";
    static final String PERIOD = ".";
    static final String WW_ACTION_DELIMITER = "!";
    static final String VIEW_PAGE_ACTION = "list.action";
    static final String HOME_PAGE_ACTION = "home.action";


    // String name of Application Menu for current URI
    private String    applicationMenu;
    // String name of Actions Menu for current URI
    private String    actionsMenu;
    // String name of the page name
    private String    pageName;

    // List of Breadcrumbs for current URI
    private List<Breadcrumb> breadcrumbTrail;

    /**
     * Constructor.
     * Calls a void parse routine which sets
     * the applicationMenu and actionsMenu vars.
     * Also calls a function to set the Breadcrumb
     * trail.
     *
     * @param uri String containing a URI
     */
    public Navigation(String uri) {
        if (uri == null) {
            throw new IllegalArgumentException();
        } else {
            parseURI(uri);
            initBreadcrumbTrail(uri);
        }
    }

    /**
     * Private setter for the breadcrumbTrail.
     * @param breadcrumbTrail ArrayList of breadcrumbs
     */
    private void setBreadcrumbTrail(List<Breadcrumb> breadcrumbTrail) {
        this.breadcrumbTrail = breadcrumbTrail;
    }

    /**
     * Public getter for the breadcrumbTrail.
     * @return ArrayList of breadcrumbs
     */
    public List<Breadcrumb> getBreadcrumbTrail() {
        return this.breadcrumbTrail;
    }

    /**
     * Private setter for the applicationMenu
     * Only set by the parseURI function.
     * @param applicationMenu Application Menu name
     */
    private void setApplicationMenu(String applicationMenu) {
        this.applicationMenu = applicationMenu;
    }

    /**
     * Public getter for the applicationMenu
     * private variable.
     * @return Application Menu name
     */
    public String getApplicationMenu() {
        return applicationMenu;
    }

    /**
     * Private setter for the actionsMenu
     * private variable.
     * Only set by the parseURI function.
     * @param actionsMenu Actions Menu name
     */
    private void setActionsMenu(String actionsMenu) {
        this.actionsMenu = actionsMenu;
    }

    /**
     * Public getter for the actionsMenu
     * private variable.
     * @return Actions Menu name
     */
    public String getActionsMenu() {
        return this.actionsMenu;
    }

    /**
     * Set the pageName property.
     * @param pageName The name of the page.
     */
    private void setPageName(String pageName) {
        this.pageName = pageName;
    }

    /**
     * Get the pageName property.
     * @return The page name.
     */
    public String getPageName() {
        return this.pageName;
    }

    /**
     * Parses the URI. Builds an ArrayList of Breadcrumbs
     * which is set to the breadcrumbsTrail attribute.
     * @param uri the URI to parse a breadcrumb from
     */
    private void initBreadcrumbTrail(String uri) {
        String[] tempTokens = uri.split(LEVEL_DIVIDER);
        String[] uriTokens = new String[tempTokens.length - 1];

        // When we split the uri, it comes up with "" for the first element.
        // This removes that first empty string in the uri tokens.
        System.arraycopy(tempTokens, 1, uriTokens, 0, uriTokens.length);

        ArrayList<Breadcrumb> bcTrail = new ArrayList<Breadcrumb>();
        String id = "";
        String trail = "";
        Breadcrumb crumb = new Breadcrumb();

        for (int i = 0; i < uriTokens.length; i++) {
            String rawCrumb = uriTokens[i];
            String cleanCrumb = cleanUpCrumb(rawCrumb);

            // Ignore the base of the URI and any 'list'.
            if (!cleanCrumb.equals(this.contextRoot) && !rawCrumb.equals(VIEW_PAGE_ACTION)) {
                id += cleanCrumb;
                trail += LEVEL_DIVIDER + cleanCrumb;

                // Create the appropriate message key for this crumb
                String crumbText = createMessageKey(crumb, cleanCrumb);
                crumb = new Breadcrumb();

                // set id and source so the breadcrumb has a link.
                // only set these if its not an action and its not a view page.
                if (!rawCrumb.endsWith(ACTION_SUFFIX) && !isViewPageNext(uriTokens, i)) {
                    crumb.setId(id);
                    crumb.setSource(trail + DEFAULT_ACTION_SUFFIX);
                }

                crumb.setText(crumbText);
                bcTrail.add(crumb);
            }

        }
        //Make sure we actually have something in the trail
        if (bcTrail.size() > 0) {
            setBreadcrumbTrail(bcTrail);
        }
    }

    /**
     * This method is meant to simplify the if logic in initBreadcrumbTrail().
     * @param tokens The tokenized URI.
     * @param position How far we are in the URI.
     * @return <code>true</code> if the next breadcrumb would be "View".
     */
    private boolean isViewPageNext(String[] tokens, int position) {
        return position < tokens.length - 1 && tokens[position + 1].equals(VIEW_PAGE_ACTION);
    }

    /**
     * Builds the message key that will be set as the text
     * of the crumb. If the action is the default action
     * then the actions menu needs to append the crumb
     * text. If the actions menu is null, then the
     * application menu should append the crumb text.
     *
     * @param last the last breadcrumb
     * @param crumbText current text of the crumb
     * @return String of the message key
     */
    private String createMessageKey(Breadcrumb last, String crumbText) {
        String prefix = MESSAGE_KEY_PREFIX;
        if ((last != null) && (last.getText() != null) && (!last.getText().equals(""))) {
            prefix = last.getText() + PERIOD;
        }

        return prefix + crumbText;
    }

    /**
     * Needs to strip out the ".action" or the
     * "!something.action". This code just cleans
     * it up for us.
     * @param oldCrumb the raw crumb
     * @return the new version of the crumb
     */
    private String cleanUpCrumb(String oldCrumb) {

        String newCrumb = oldCrumb;

        if (oldCrumb.indexOf(WW_ACTION_DELIMITER) != -1) {
            //Check for an exclamation point in the URI

            newCrumb = oldCrumb.substring(0,
                oldCrumb.indexOf(WW_ACTION_DELIMITER));
        } else if (oldCrumb.indexOf(PERIOD) != -1) {
            //If not, then check for a period

            newCrumb = oldCrumb.substring(0,
                oldCrumb.indexOf(PERIOD));
        }

        return newCrumb;
    }


    /**
     * Parsing subroutine which uses a stringtokenizer
     * to tokenize the URI and call the setters for the
     * applicationMenu variable and the objectName variable
     * if they exist in the URI.
     *
     * @param uri the URI
     */
    private void parseURI(String uri) {

        StringTokenizer stringToke = new StringTokenizer(uri, LEVEL_DIVIDER);

        // Remove the base from the URI
        if (stringToke.hasMoreElements()) {
            this.contextRoot = stringToke.nextToken(); // epp
        }

        // Strip out the application-level information
        if (stringToke.hasMoreElements()) {
            setApplicationMenu(stringToke.nextToken());
        }

        // Strip out the object level information
        if (stringToke.hasMoreElements()) {
            String actMenu = stringToke.nextToken();
            if (!actMenu.endsWith(ACTION_SUFFIX)) {
                setActionsMenu(actMenu);
            } else {
                // Not setting this variable for the report pages
                // causes a Freemarker error on line 20 of nav.ftl
                setActionsMenu("");
            }
        }
        if (stringToke.hasMoreElements()) {
            String thePageName = stringToke.nextToken();
            while (stringToke.hasMoreTokens()) {
                thePageName += PERIOD + stringToke.nextToken();
            }
            if (thePageName.endsWith(INPUT_SUFFIX)) {
                setPageName(thePageName.split(INPUT_SUFFIX)[0]);
            } else if (thePageName.endsWith(ACTION_SUFFIX)) {
                setPageName(thePageName.split(ACTION_SUFFIX)[0]);
            }
        } else {
            // check for home page
            if (uri.endsWith(HOME_PAGE_ACTION)) {
                setActionsMenu("home");
            }
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 