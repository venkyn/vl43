/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ui;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * DTO for containing column information for the saving of user preferences by
 * the UserPreferencesManager.
 *
 *
 * @author jgeisler
 */
public class ColumnDTO {

    private boolean sorted;

    private boolean sortAsc;

    private int width;

    private long columnId;

    private boolean visible;

    private int order;

    /**
     * used to support unit testing.
     *
     */
    public ColumnDTO() {

    }

    /**
     * Initialized with a serialized column String. Constructor.
     * @param serializedColumn String of a serialized column.
     * @throws JSONException when the serialized input cannot be de-serialized.
     */
    public ColumnDTO(String serializedColumn) throws JSONException {

        JSONObject jsonColumn = new JSONObject(serializedColumn);

        this.setVisible(((Boolean) jsonColumn.get("visible")).booleanValue());

        if (this.isVisible()) {
            this.setWidth(((Integer) jsonColumn.get("width")).intValue());
        } else {
            this.setWidth(0);
        }
        this.setColumnId(Integer.valueOf((String) jsonColumn.get("column_id")));
        this.setOrder(((Integer) jsonColumn.get("column_order")).intValue());

    }

    /**
     * Getter for the order property.
     * @return int value of the property.
     */
    public int getOrder() {
        return order;
    }

    /**
     * Setter for the order property.
     * @param order new value for property.
     */
    public void setOrder(int order) {
        this.order = order;
    }

    /**
     * Getter for the sortAsc property.
     * @return boolean value of the property.
     */
    public boolean isSortAsc() {
        return sortAsc;
    }

    /**
     * Setter for the sortAsc property.
     * @param sortAsc new value of the property.
     */
    public void setSortAsc(boolean sortAsc) {
        this.sortAsc = sortAsc;
    }

    /**
     * Getter for the sorted property.
     * @return boolean value of the property.
     */
    public boolean isSorted() {
        return sorted;
    }

    /**
     * Setter for the sorted property.
     * @param sorted new value for the property.
     */
    public void setSorted(boolean sorted) {
        this.sorted = sorted;
    }

    /**
     * Getter for the visible property.
     * @return boolean of the property.
     */
    public boolean isVisible() {
        return visible;
    }

    /**
     * Setter for the visible property.
     * @param visible new value for the property.
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Getter for the width property.
     * @return int value of the property.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Setter for the width property.
     * @param width new value of the property.
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * Getter for the columnId property.
     * @return int value of the property.
     */
    public long getColumnId() {
        return columnId;
    }

    /**
     * Setter for the columnId property.
     * @param columnId new value of the property.
     */
    public void setColumnId(long columnId) {
        this.columnId = columnId;
    }

    /**
     * Standard toString() implementation.
     *
     * @return String representation of the given schedule.
     */
    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append("id", this.getColumnId())
            .append("order", this.getOrder())
            .append("width", this.getWidth())
            .append("visible", this.isVisible())
            .append("sorted", this.isSorted())
            .append("sortAsc", this.isSortAsc()).toString();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 