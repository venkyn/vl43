/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ui;

/**
 * This is used by the Navigation object to
 * create a breadcrumb trail.
 *
 *
 * @author jgeisler
 */
public class Breadcrumb {

    // Anchor id attribute. Contains the id for the bread crumb anchor
    private String id;
    // Anchor source attribute. Contains the href for the bread crumb anchor
    private String source;
    // Anchor text attribute. Contains the text of the bread crumb anchor
    private String text;

    /**
     * Constructor.
     */
    public Breadcrumb() {
    }

    /**
     * Sets the id attribute.
     * @param id - new id value
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the id.
     * @return id as a String.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the source attribute.
     * @param source - new source value
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets the source attribute.
     * @return source as a String
     */
    public String getSource() {
        return this.source;
    }

    /**
     * Sets the text attribute.
     * @param text - new text value
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets the text attribute.
     * @return text as a String
     */
    public String getText() {
        return this.text;
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 