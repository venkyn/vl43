/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.epp.ui;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class containing instances of User Preferences error codes.
 *
 *
 * @author jgeisler
 */
public final class UserPreferencesErrorCode extends ErrorCode {

    /**
     * Upper boundary of the EPP scheduling error range.
     */
    public static final long UPPER_BOUND = 5999;

    /**
     * Lower boundary of the EPP scheduling error range.
     */
    public static final long LOWER_BOUND = 5000;

    /**
     * Constructor.
     */
    private UserPreferencesErrorCode() {
        super("EPP", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Perform base initialization.
     */
    public static final UserPreferencesErrorCode NO_ERROR
        = new UserPreferencesErrorCode();

    /**
     * When saving a filter criterion asynchronously has issues.
     */
    public static final UserPreferencesErrorCode ERROR_ADDING_FILTER_CRITERIA
        = new UserPreferencesErrorCode(5000);

    /**
     * When saving new column configuration asnychronously has issues.
     */
    public static final UserPreferencesErrorCode ERROR_SAVING_COLUMN_INFO
        = new UserPreferencesErrorCode(5001);

    /**
     * When saving a filter criterion and no filter information is specified.
     */
    public static final UserPreferencesErrorCode ERROR_NULL_FILTER_VALUE
        = new UserPreferencesErrorCode(5002);

    public static final UserPreferencesErrorCode ERROR_CREATING_OPERAND_LIST
        = new UserPreferencesErrorCode(5003);

    public static final UserPreferencesErrorCode ERROR_BUSINESS_RULE_VIOLATED
        = new UserPreferencesErrorCode(5004);

    public static final UserPreferencesErrorCode ERROR_EDITING_FILTER_CRITERIA
        = new UserPreferencesErrorCode(5005);

    public static final UserPreferencesErrorCode ERROR_GETTING_ENUMERATIONS
    = new UserPreferencesErrorCode(5006);

    public static final UserPreferencesErrorCode ERROR_NO_SUCH_FILE_TYPE
    = new UserPreferencesErrorCode(5007);

    /**
     * Create a custom error code using the given value.
     * @param err - error to be logged
     */
    private UserPreferencesErrorCode(long err) {
        super(UserPreferencesErrorCode.NO_ERROR, err);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 