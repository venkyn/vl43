/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.epp.ui;

/**
 * Operand referenced by filter criteria.
 *
 * @author jgeisler
 */
public class Operand {

    public static final int TYPE_STRING = 0;
    public static final int TYPE_NUMBER = 1;
    public static final int TYPE_DATE = 2;
    public static final int TYPE_ENUMERATION = 3;
    public static final int TYPE_BOOLEAN = 4;


    public static final long NUM_EQUALS = -11;

    private String label;
    private String jsInputFunction;
    private int type;
    private Long id;
    private String hql;


    /**
     * 
     * @param id - Id of operand.
     * @param type - The type of operand, String, Date, etc
     * @param label - The label for this operand
     * @param jsInputFunction - java script function
     * @param hql - the hql for this operand.
     */
    public Operand(Long id,
                   int type,
                   String label,
                   String jsInputFunction,
                   String hql) {

        this.id = id;
        this.label = label;
        this.jsInputFunction = jsInputFunction;
        this.type = type;
        this.hql = hql;
    }
    
    /**
     * Getter for the jsInputFunction.
     * @return String jsInputFunction value.
     */
    public String getJsInputFunction() {
        return jsInputFunction;
    }

    /**
     * Setter for the jsInputFunction.
     * @param jsInputFunction String value.
     */
    public void setJsInputFunction(String jsInputFunction) {
        this.jsInputFunction = jsInputFunction;
    }

    /**
     * Getter for label.
     * @return String label value.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Stter for label.
     * @param label String value.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Getter for type.
     * @return int type value.
     */
    public int getType() {
        return type;
    }

    /**
     * Setter for type.
     * @param type int value.
     */
    public void setType(int type) {
        this.type = type;
    }


    /**
     * Getter for the hql property.
     * @return String value of the property
     */
    public String getHql() {
        return this.hql;
    }


    /**
     * Setter for the hql property.
     * @param hql the new hql value
     */
    public void setHql(String hql) {
        this.hql = hql;
    }
    
    /**
     * 
     * @param id - the id of the operand.
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * 
     * @return the id of the operand.
     */
    public Long getId() {
        return this.id;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 