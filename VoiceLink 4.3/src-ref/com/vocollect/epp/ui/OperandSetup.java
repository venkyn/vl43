/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.epp.ui;

import java.util.ArrayList;
import java.util.List;

/**
 * The operands used by the filtering UI and building the hibernate queries.
 *
 * @author jgeisler
 */
public class OperandSetup {

	private static List<Operand> operands;
	
	private static List<Operand> createOperands() {
		List<Operand> operandList = new ArrayList<Operand>();

		// String operands
		operandList.add(new Operand(-1L, //id
						0, //type
						"operand.0.startsWith", //label
						"drawInputBox", //js input function,
						"like '{0}%'")); //hql
		operandList.add(new Operand(-2L, //id
						0, //type
						"operand.0.endsWith", //label
						"drawInputBox", //js input function,
						"like '%{0}'")); //hql
		operandList.add(new Operand(-3L, //id
						0, //type
						"operand.0.contains", //label
						"drawInputBox", //js input function,
						"like '%{0}%'")); //hql
		operandList.add(new Operand(-4L, //id
						0, //type
						"operand.0.equals", //label
						"drawInputBox", //js input function,
						"= '{0}'")); //hql
		
		// Number operands
		operandList.add(new Operand(-11L, //id
						1, //type
						"operand.1.equals", //label
						"drawInputBox", //js input function,
						"= {0}")); //hql
		operandList.add(new Operand(-12L, //id
						1, //type
						"operand.1.greaterThan", //label
						"drawInputBox", //js input function,
						"> {0}")); //hql
		operandList.add(new Operand(-13L, //id
						1, //type
						"operand.1.greaterThanOrEqualTo", //label
						"drawInputBox", //js input function,
						">= {0}")); //hql
		operandList.add(new Operand(-14L, //id
						1, //type
						"operand.1.lessThan", //label
						"drawInputBox", //js input function,
						"< {0}")); //hql
		operandList.add(new Operand(-15L, //id
						1, //type
						"operand.1.lessThanOrEqualTo", //label
						"drawInputBox", //js input function,
						"<= {0}")); //hql
		operandList.add(new Operand(-16L, //id
						1, //type
						"operand.1.between", //label
						"drawTwoInputBoxes", //js input function,
						">= {0} and {1} <= {2}")); //hql
		
		// Date operands
		operandList.add(new Operand(-23L, //id
						2, //type
						"operand.2.within", //label
						"drawDropDownWithDates", //js input function,
						">= {0}")); //hql
		operandList.add(new Operand(-22L, //id
						2, //type
						"operand.2.notWithin", //label
						"drawDropDownWithDates", //js input function,
						"< {0}")); //hql
		operandList.add(new Operand(-21L, //id
						2, //type
						"operand.2.emptyDate", //label
						"drawEmptyControl", //js input function,
						"is null")); //hql
		
		// Enumeration operands 
		operandList.add(new Operand(-31L, //id
						3, //type
						"operand.3.equalTo", //label
						"drawDropDownWithEnums", //js input function,
						"= {0}")); //hql
		operandList.add(new Operand(-32L, //id
						3, //type
						"operand.3.notEqualTo", //label
						"drawDropDownWithEnums", //js input function,
						"<> {0}")); //hql
		
		// Boolean operands
		operandList.add(new Operand(-41L, //id
						4, //type
						"operand.4.equalTo", //label
						"drawDropDown", //js input function,
						"= {0}")); //hql
		operandList.add(new Operand(-42L, //id
						4, //type
						"operand.4.notEqualTo", //label
						"drawDropDown", //js input function,
						"!= {0}")); //hql
		
		
		return operandList;
	}
	
	public static List<Operand> getOperandByType(int type) {
		List<Operand> operandList = getOperands();
		List<Operand> operandOfType = new ArrayList<Operand>();
		for (Operand o: operandList) {
			// primitive int comparison
			if (o.getType() == type) {
				operandOfType.add(o);
			}
		}
		return operandOfType;		
	}
	
	/**
	 * Get an operand by it's id.
	 * @param id - unique identifier of an operand
	 * @return - operand object identified by id
	 */
	public static Operand getOperandById(Long id) {
		List<Operand> operandList = getOperands();
		for (Operand o: operandList) {
			if (o.getId().equals(id)) {
				return o;
			}
		}
		return null;
	}
	
	public static List<Operand> getOperands() {
		if (operands == null) {
			operands = createOperands();
		}
		return operands;
	}
	
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 