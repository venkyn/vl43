/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Report;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.ReportManager;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.SiteContext;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class implements scheduling of Reports.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 * @author kudupi
 */
public class ReportSchedulerJob extends EPPSpringQuartzJob {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(ReportSchedulerJob.class);

    // Report object for Scheduler.
    private Report report;

    // Manager for setting Notifications.
    private NotificationManager notificationManager;

    // Manager for accessing Reports related information.
    private ReportManager reportManager;

    // SystemProperty Manager to access system properties.
    private SystemPropertyManager systemPropertyManager;

    // Context for Site.
    private SiteContext siteContext;

    /**
     * Getter for report property.
     * @return report - Report object.
     */
    public Report getReport() {
        return report;
    }

    /**
     * Setter for report property.
     * @param report the new Report value.
     */
    public void setReport(Report report) {
        this.report = report;
    }

    /**
     * Getter for systemPropertyManager property.
     * @return systemPropertyManager - SystemPropertyManager object.
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for systemPropertyManager property.
     * @param systemPropertyManager the new SystemPropertyManager value.
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return notificationManager - NotificationManager object.
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }


    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new NotificationManager value.
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the siteContext property.
     * @return siteContext - SiteContext Object.
     */
    @Override
    public SiteContext getSiteContext() {
        return this.siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    @Override
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * Getter for the reportManager property.
     * @return reportManager - ReportManager object.
     */
    public ReportManager getReportManager() {
        return reportManager;
    }

    /**
     * Setter for the reportManager property.
     * @param reportManager the new ReportManager value.
     */
    public void setReportManager(ReportManager reportManager) {
        this.reportManager = reportManager;
    }

    /**
     * This Quartz job activates the Report Scheduling Process.
     * @param context describes details corresponding to the current invocation.
     * @throws JobExecutionException if an error occurs during job execution
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     * @author kudupi
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        try {
            log.debug("####REPORT SCHEDULE: Starting Report Scheduling Process:::");
        } catch (Exception e) {
            e.printStackTrace();
            log.warn("Exception encountered during report scheduler "
                + "execution: " + e.getMessage());
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

/**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    @Override
    protected void internalInterrupt() {

        // Indicate Purge Archive is failing in the log file
        log.debug("####REPORT SCHEDULER: Stopping Report Scheduler Process:::");

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 