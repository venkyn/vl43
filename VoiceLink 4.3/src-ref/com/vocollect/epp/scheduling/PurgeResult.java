/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;


/**
 * This class holds the results of a purge attempt.
 *
 *
 * @author dkertis
 */
public class PurgeResult {
    // the total amount of objects attempted to purge
    public int total;
    // number of successful objects purged
    public int successful;
    // number of failed purge objects
    public int failure;

    /**
     * Constructor.
     * @param successful number of successful objects purged
     * @param failure number of failed purge objects
     * @param total the total amount of objects attempted to purge
     */
    public PurgeResult(int successful, int failure, int total) {
        super();
        this.total = total;
        this.successful = successful;
        this.failure = failure;
    }

    /**
     * Initialize to default values (0)
     * Constructor.
     */
    public PurgeResult() {
        super();
    }

    /**
     * @param result .
     */
    public void add(PurgeResult result) {
        this.total += result.total;
        this.successful += result.successful;
        this.failure += result.failure;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 