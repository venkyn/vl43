/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.model.Job;
import com.vocollect.epp.service.JobManager;

import org.quartz.JobExecutionContext;

/**
 * Super class to be used for jobs running in a clustered environment. Any
 * subclass of this job should either update the database with a new timestamp
 * on the specified interval, or poll the database to ensure a new timestamp has
 * been entered. These jobs ensure that a node that failed while a job was
 * executing gets completed by a monitoring node.
 * 
 * @author hulrich
 */
public abstract class ClusterMonitoringJob extends EPPSpringQuartzJob {
    
    public static final String MONITOR_JOB_NAME = "monitorJobName";
    public static final String BUSINESS_JOB_NAME = "businessJobName";

    /**
     * Flags whether the job should continue running.
     */
    private boolean running;

    /**
     * The id of the job being monitored.
     */
    private String jobName;

    /**
     * The job manager.
     */
    private JobManager jobManager;
    
    /**
     * The job being monitored.
     */
    private Job job;
    
    /**
     * The interval to monitor the job at.
     */
    private long interval;

    
    /**
     * Getter for the interval property.
     * @return long value of the property
     */
    public long getInterval() {
        return this.interval;
    }

    
    /**
     * Setter for the interval property.
     * @param interval the new interval value
     */
    public void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * The current time in milliseconds.
     * @return now
     */
    public long getNow() {
        return System.currentTimeMillis();
    }

    /**
     * Getter for the jobManager property.
     * @return JobManager value of the property
     */
    public JobManager getJobManager() {
        return this.jobManager;
    }

    /**
     * Setter for the jobManager property.
     * @param jobManager the new jobManager value
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * Getter for the jobId of the job being monitored.
     * @return String value of the property
     */
    public String getJobName() {
        return this.jobName;
    }

    /**
     * Setter for the jobId of the job being monitored.
     * @param jn the jobId
     */
    public void setJobName(String jn) {
        this.jobName = jn;
    }

    /**
     * Is this monitoring job running.
     * @return boolean value of the property
     */
    public boolean isRunning() {
        return this.running;
    }
    
    /**
     * Getter for the job property.
     * @return Job value of the property
     */
    public Job getJob() {
        return this.job;
    }
    
    /**
     * Setter for the job property.
     * @param job the new job value
     */
    public void setJob(Job job) {
        this.job = job;
    }

    /**
     * Setter for the monitoring job running property.
     * @param running the new running value
     */
    protected void setRunning(boolean running) {
        this.running = running;
    }
    
    /**
     * Populates common attributes for this job.
     * @param context the context
     */
    protected void populateJob(JobExecutionContext context) {
        this.setJobName(context.getJobDetail().getJobDataMap().getString(
            BUSINESS_JOB_NAME));
        this.setInterval(context.getJobDetail().getJobDataMap().getLong(Job.PING_INTERVAL));
        this.setJob((Job) context.getJobDetail().getJobDataMap().get(Job.class)); 
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 