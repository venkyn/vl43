/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.PurgeArchiveManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Date;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class implements a schedulable job for logging JVM memory usage.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 * @author mlashinsky
 */
public class PurgeArchiveJob extends EPPSpringQuartzJob {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(PurgeArchiveJob.class);

    private PurgeArchiveManager purgeArchiveManager;

    private NotificationManager notificationManager;

    private SiteContext siteContext;

    /**
     * This Quartz job activates the Purge Archive Process.
     * @param context describes details corresponding to the current invocation.
     * @throws JobExecutionException if an error occurs during job execution
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     * @author mlashinsky
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        try {
            log.debug("####PURGE: Starting Purge Archive Process:::");
            this.getPurgeArchiveManager().purgeArchive();
        } catch (Exception e) {
            log.error("Error during purge/archive process",
                SystemErrorCode.PURGE_ARCHIVE_FAILURE, e);
            try {
                //Modify the siteContext currentTagId to point to the system "site"
                getSiteContext().setToSystemMode();
                SiteContextHolder.setSiteContext(getSiteContext());

                // Create Purge/Archive Notification
                LOPArrayList lop = new LOPArrayList();
                getNotificationManager().createNotification("systemConfiguration.purgearchive.title",
                    "errorCode.message.EPP104",
                    NotificationPriority.CRITICAL,
                    new Date(),
                    "100",
                    "notification.column.keyname.Application.2",
                    lop);
            } catch (Exception exc) {
                log.error("Error creating Notification",
                    SystemErrorCode.NOTIFICATION_FAILURE, exc);
            }
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

   /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    @Override
    protected void internalInterrupt() {

        // Indicate Purge Archive is failing in the log file
        log.debug("####PURGE: Stopping Purge Archive Process:::");

    }

    /**
     * Gets the purge archive manager.
     * @return the manager
     */
    protected PurgeArchiveManager getPurgeArchiveManager() {
        return purgeArchiveManager;
    }

    /**
     * Sets the purge archive manager.
     * @param purgeArchiveManager the manager
     */
    public void setPurgeArchiveManager(
            PurgeArchiveManager purgeArchiveManager) {
        this.purgeArchiveManager = purgeArchiveManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }


    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    @Override
    public SiteContext getSiteContext() {
        return this.siteContext;
    }


    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    @Override
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 