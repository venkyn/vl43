/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.ExternalJob;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.ExternalJobManager;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * External job runner is a class responsible to run the external command
 * configured in External Job. This class acts as a wrapper plus interface of
 * that command to VoiceLink
 * 
 * @author mraj
 * 
 */
public class ExternalJobRunnerRoot extends EPPSpringQuartzJob {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(ExternalJobRunnerRoot.class);

    private ExternalJobManager externalJobManager;

    private NotificationManager notificationManager;

    private SiteContext siteContext;

    /**
     * @return the externalJobManager
     */
    public ExternalJobManager getExternalJobManager() {
        return externalJobManager;
    }

    /**
     * @param externalJobManager the externalJobManager to set
     */
    public void setExternalJobManager(ExternalJobManager externalJobManager) {
        this.externalJobManager = externalJobManager;
    }

    /**
     * @return the notificationManager
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * @param notificationManager the notificationManager to set
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * @return the siteContext
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * @param siteContext the siteContext to set
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalExecute(org
     * .quartz.JobExecutionContext)
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        String jobName = context.getJobDetail().getName();

        SiteContextHolder.setSiteContext(siteContext);

        ExternalJob externalJob = null;

        try {
            externalJob = this.externalJobManager
                .findExternalJobByName(jobName);
        } catch (DataAccessException e) {
            notifyOtherJobException(context.getJobDetail().getName(), e);
            throw new JobExecutionException(e);
        }

        // Print the job detail in log
        log.info("Job name              : " + externalJob.getName());
        log.info("Job type              : " + externalJob.getJobType());
        log.info("Job command           : " + externalJob.getCommand());
        log.info("Job working directory : " + externalJob.getWorkingDirectory());

        // Building the process builder from external job
        ProcessBuilder processBuilder = buildProcessBuilder(externalJob);

        // Starting and monitoring the job
        startAndMonitorExternalJob(context, processBuilder);

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalInterrupt()
     */
    @Override
    protected void internalInterrupt() {
        log.warn("External jobs are not interruptable");
    }

    /**
     * Builds the process builder for the external job command
     * @param externalJob The external job
     * @return ProcessBuilder for the external job
     */
    protected ProcessBuilder buildProcessBuilder(ExternalJob externalJob) {
        // Command line parsing pattern
        Pattern cmdPattern = Pattern.compile("(\"[^\"]*\")|([^\"\\s]+)");

        // Parse the external job command to get command string and arguments
        Matcher cmdMatcher = cmdPattern.matcher(externalJob.getCommand());
        List<String> cmdFragment = new LinkedList<String>();
        while (cmdMatcher.find()) {
            cmdFragment.add(externalJob.getCommand().substring(
                cmdMatcher.start(), cmdMatcher.end()));
        }

        // Build the process
        ProcessBuilder processBuilder = new ProcessBuilder(cmdFragment);
        processBuilder.redirectErrorStream(true);
        processBuilder.directory(new File(externalJob.getWorkingDirectory()));

        return processBuilder;
    }

    /**
     * Starts and monitors the external job
     * @param context Job context provided by quartz
     * @param processBuilder for external job
     * @throws JobExecutionException
     */
    protected void startAndMonitorExternalJob(JobExecutionContext context,
                                              ProcessBuilder processBuilder)
        throws JobExecutionException {

        Process externalJobProcess = null;

        // Try to start the job
        try {
            externalJobProcess = processBuilder.start();
        } catch (IOException e1) {
            notifyOtherJobException(context.getJobDetail().getName(), e1);
            
            log.error(e1.getLocalizedMessage(),
                SchedulingErrorCode.UNABLE_TO_START, e1);
            
            throw new JobExecutionException(e1);
        }

        // Print output to the log
        InputStream is = externalJobProcess.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        try {
            line = br.readLine();
            while (line != null) {
                log.info(line);
                line = br.readLine();
            }
        } catch (IOException e) {
            notifyOtherJobException(context.getJobDetail().getName(), e);
            
            log.error(e.getLocalizedMessage(),
                SchedulingErrorCode.JOB_NOT_RUNNING, e);
            
            throw new JobExecutionException(e);
        }
        
        //Wait for job to finish
        try {
            externalJobProcess.waitFor();
        } catch (InterruptedException e) {
            notifyOtherJobException(context.getJobDetail().getName(), e);
            
            log.error(e.getLocalizedMessage(),
                SchedulingErrorCode.UNABLE_TO_STOP_JOB, e);
            
            throw new JobExecutionException(e);
        }

        log.debug("Job : " + context.getJobDetail().getName()
            + " Exit value :  " + externalJobProcess.exitValue());

        // If code exited with non-zero value throw job exception
        if (externalJobProcess.exitValue() != 0) {
            notifyJobExitedWithError(context.getJobDetail().getName(),
                externalJobProcess.exitValue());
            throw new JobExecutionException("Exited with error code "
                + externalJobProcess.exitValue());
        }
    }

    /**
     * @param jobName name of the job
     * @param exitValue exit value of the job
     */
    protected void notifyJobExitedWithError(String jobName, int exitValue) {
        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());

        LOPArrayList lop = new LOPArrayList();
        lop.add("notification.externalJob.jobName", jobName);
        lop.add("notification.externalJob.exitCode", exitValue);

        try {
            getNotificationManager().createNotification(
                "notification.externalJob.title",
                "notification.externalJob.failed",
                NotificationPriority.CRITICAL, new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getLocalizedMessage());
        }
    }

    /**
     * @param jobName Job name
     * @param e exception to be reported
     */
    protected void notifyOtherJobException(String jobName, Exception e) {
        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());

        LOPArrayList lop = new LOPArrayList();
        lop.add("notification.externalJob.jobName", jobName);
        lop.add("notification.externalJob.exception", e.getLocalizedMessage());

        try {
            getNotificationManager().createNotification(
                "notification.externalJob.title",
                "notification.externalJob.failed",
                NotificationPriority.CRITICAL, new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e1) {
            log.warn(e1.getLocalizedMessage());
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 