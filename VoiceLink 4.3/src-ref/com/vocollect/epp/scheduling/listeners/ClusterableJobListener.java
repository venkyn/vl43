/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.listeners;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.ScheduleType;
import com.vocollect.epp.scheduling.ClusterMonitoringJob;
import com.vocollect.epp.scheduling.impl.PingingJob;
import com.vocollect.epp.scheduling.impl.PollingJob;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;

/**
 * 
 * 
 * @author hulrich
 */
public class ClusterableJobListener extends JobListener {

    private static final Logger log = new Logger(ClusterableJobListener.class);

    /**
     * The job that ensures the job is marked as running.
     */
    private PollingJob pollingJob;

    /**
     * The job that updates the timestamp.
     */
    private PingingJob pingingJob;

    /**
     * Getter for the pingingJob property.
     * @return PingingJob value of the property
     */
    public PingingJob getPingingJob() {
        return this.pingingJob;
    }

    /**
     * Setter for the pingingJob property.
     * @param pingingJob the new pingingJob value
     */
    public void setPingingJob(PingingJob pingingJob) {
        this.pingingJob = pingingJob;
    }

    /**
     * Getter for the pollingJob property.
     * @return PollingJob value of the property
     */
    public PollingJob getPollingJob() {
        return this.pollingJob;
    }

    /**
     * Setter for the pollingJob property.
     * @param pollingJob the new pollingJob value
     */
    public void setPollingJob(PollingJob pollingJob) {
        this.pollingJob = pollingJob;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#startMonitors(java.lang.String)
     */
    protected void startMonitor(boolean pingJob, Job firingJob,
                                JobExecutionContext context) {

        try {
            log.debug("Starting to monitor Job : " + context.getJobDetail().getName());
            
            ClusterMonitoringJob job = null;
            if (pingJob) {
                job = getPingingJob();
            } else {
                job = getPollingJob();
            }
            log.debug(pingJob + " -- setting the name of the job to monitor : " 
                + context.getJobDetail().getName());
            
            job.setJobName(context.getJobDetail().getName());
            JobDetail monitorDetail = buildJobDetail(job);
            context.getJobDetail().getJobDataMap()
                .put(ClusterMonitoringJob.MONITOR_JOB_NAME, 
                    monitorDetail.getName());
            monitorDetail.getJobDataMap().put(Job.RUN_THIS_JOB, true);
            monitorDetail.getJobDataMap()
                .put(ClusterMonitoringJob.BUSINESS_JOB_NAME, 
                    context.getJobDetail().getName());
            monitorDetail.getJobDataMap().put(Job.PING_INTERVAL, 
                firingJob.getPingInterval());
            monitorDetail.getJobDataMap().put(Job.class, 
                firingJob);
            
            context.getScheduler().addJob(monitorDetail, true);
            log.debug("Triggering job ... " + monitorDetail.getName());
            context.getScheduler().triggerJob(monitorDetail.getName(),
                monitorDetail.getGroup());
            log.debug("Done.");
            log.debug("This job should run now.");
        } catch (SchedulerException se) {
            log.warn("Unable to start monitoring job for Job : "
                + context.getJobDetail().getName() + " -- Is running : " + pingJob);
            se.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#stopMonitors(java.lang.String)
     */
    @Override
    protected void stopMonitor(JobExecutionContext context) {
        boolean saved = false;
        Job job = null;
        try {
            job = this.getJobManager().updateAndFindJobByName(context.getJobDetail().getName());
            job.setRunning(false);
            this.safeUpdateJob(job);
            saved = true;
            if (context.getJobDetail().getJobDataMap()
                .containsKey(ClusterMonitoringJob.MONITOR_JOB_NAME)) {
                String jobName = context.getJobDetail().getJobDataMap()
                    .getString(ClusterMonitoringJob.MONITOR_JOB_NAME);
                context.getScheduler().interrupt(jobName,
                    Job.MONITOR_GROUP_NAME);
                log.debug("deleting job : " + jobName);
                context.getScheduler().deleteJob(jobName,
                    Job.MONITOR_GROUP_NAME);
                log.debug("job deleted : " + jobName);
            }
        } catch (SchedulerException se) {
            log.warn("Unable to stop monitoring job for Job : "
                + context.getJobDetail().getName());
            se.printStackTrace();
        } catch (DataAccessException dae) {
            dae.printStackTrace();
        } finally {
            if ((!saved) && (job != null)) {
                log.debug("Finally saving...........");
                safeUpdateJob(job);
            }
        }
    }
    
    /**
     * Updates this job, does not throw exceptions.
     * @param job the job
     */
    protected void safeUpdateJob(Job job) {
        try {
            getJobManager().saveJob(job);
        } catch (DataAccessException dae) {
            dae.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#isEarlyRun(com.vocollect.epp.model.Job)
     */
    @Override
    protected boolean isEarlyRun(Job job) {
        boolean early = false;
        if ((job.getLastRun() != null)
            && (job.getType() == ScheduleType.INTERVAL)) {
            
            long next = job.getLastRun().getTime() + job.getInterval();
            log.debug("Last run : " + job.getLastRun().getTime());
            log.debug("Interval : " + job.getInterval());
            log.debug("NOW : " + System.currentTimeMillis());
            if (next > System.currentTimeMillis()) {
                early = true;
            }
        }
        return early;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#isForceRun(org.quartz.JobExecutionContext)
     */
    @Override
    protected boolean isForceRun(JobExecutionContext context) {
        boolean forceRun = false;
        if (context.getJobDetail().getJobDataMap()
            .containsKey(Job.RUN_THIS_JOB)) {
            forceRun = context.getJobDetail().getJobDataMap().getBoolean(
                Job.RUN_THIS_JOB);
        }
        log.debug("FORCE RUN >>>>>> " + forceRun);
        return forceRun;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#runThis(com.vocollect.epp.model.Job,
     *      boolean, boolean, org.quartz.JobExecutionContext)
     */
    @Override
    protected boolean runThis(Job job,
                              boolean early,
                              boolean force,
                              JobExecutionContext context) {
        boolean run = false;
        log.debug("ENABLED : " + job.getEnabled());
        log.debug("FORCE : " + force);
        log.debug("Running : " + job.isRunning());
        log.debug("Early : " + early);
        
        if (!force && (job.getEnabled())) {
            if (!job.isRunning() && !early) {
                run = true;
            } 
        } else if (force && !job.isRunning()) {
            run = true;
        } else if (force && job.isRunning()
            && (job.getLastPing() + job.getPingInterval()) < System.currentTimeMillis()) {
            run = true;
        }
        
        log.debug("RUN THIS >>>> " + run);
        context.getJobDetail().getJobDataMap().put(Job.RUN_THIS_JOB, run);
        return run;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.listeners.JobListener#updatePreJobStatus(com.vocollect.epp.model.Job,
     *      org.quartz.JobExecutionContext)
     */
    @Override
    protected void updatePreJobStatus(Job job, JobExecutionContext context)
        throws DataAccessException {
        job.setRunning(true);

        log.debug("Updating pre job status to true..");
        this.safeUpdateJob(job);
        super.updatePreJobStatus(job, context);
    }

    /**
     * Builds the job detail.
     * @param job the job
     * @return the detail
     */
    protected JobDetail buildJobDetail(ClusterMonitoringJob job) {
        String jobName = job.getJobName() + "_" + System.currentTimeMillis();
        JobDetail detail = new JobDetail(jobName, Job.MONITOR_GROUP_NAME, job
            .getClass());
        return detail;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 