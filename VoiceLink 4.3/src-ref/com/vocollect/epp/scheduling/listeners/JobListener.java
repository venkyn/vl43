/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.listeners;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.JobHistory;
import com.vocollect.epp.model.ScheduleLastResult;
import com.vocollect.epp.service.JobManager;

import static com.vocollect.epp.model.ScheduleLastResult.PENDING;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * This class implements a Quartz JobListener to monitor the execution of all
 * publicly schedulable (visible) jobs. It is responsible for tracking job
 * execution properties (start times, results, etc.).
 *
 * @author hulrich
 */
public class JobListener implements org.quartz.JobListener {

    // Instantiate a logger for this class.
    private static final Logger logger = new Logger(JobListener.class);

    private JobManager jobManager;

    private String name;

    private SessionFactory sessionFactory;

    /**
     * Return the listener name. This listener name is the value that public
     * (visible) jobs must use when registering itself with the scheduler.
     *
     * @return listener name for job registration
     * @see org.quartz.JobListener#getName()
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the listener name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
        logger.debug("Name is : " + name);
    }

    /**
     * This method is called prior to job execution.
     *
     * @param context contains detailed information describing the job that is
     *            being invoked
     * @see org.quartz.JobListener#jobToBeExecuted(org.quartz.JobExecutionContext)
     */
    public synchronized void jobToBeExecuted(JobExecutionContext context) {

        if (!isPublic(context)) {
            context.getJobDetail().getJobDataMap().put(Job.RUN_THIS_JOB, true);
            return;
        }
        try {
            bind();
            Job firingJob = getJob(context.getJobDetail()
                .getName());
            logger.debug("Firing job ... " + firingJob.getName() + " == "
                + firingJob.isRunning());
            logger.debug("FORCE RUN - "
                + context.getJobDetail().getJobDataMap().get(Job.RUN_THIS_JOB));
            boolean early = isEarlyRun(firingJob);
            boolean forceRun = isForceRun(context);
            boolean runThis = runThis(firingJob, early, forceRun, context);

            if (runThis) {
                this.updatePreJobStatus(firingJob, context);
            } else {
                firingJob.getJobDetail().getJobDataMap().put(
                    Job.RUN_THIS_JOB, runThis);
            }
            logger.debug("Starting monitor ... " + runThis);
            startMonitor(runThis, context);
        } catch (DataAccessException dae) {
            logger.warn("Could not save job in listener - "
                + context.getJobDetail().getName() + " - " + dae.getMessage());
        } finally {
            unbind();
        }

        logger.debug("-------------------------------------------------");
    }

    /**
     * This function is required by the interface; it is called when a
     * TriggerListener vetoes a job.
     *
     * @param context contains detailed information describing the job that is
     *            being invoked
     * @see org.quartz.JobListener#jobExecutionVetoed(org.quartz.JobExecutionContext)
     */
    public void jobExecutionVetoed(JobExecutionContext context) {
        logger.warn("Unhandled veto of job "
            + context.getJobDetail().getFullName());
    }

    /**
     * This method is called after job completion.
     *
     * @param context contains detailed information describing the job that is
     *            being invoked
     * @param jobException describes any exceptional condition that occured
     *            during job execution.
     * @see org.quartz.JobListener#jobWasExecuted(org.quartz.JobExecutionContext,
     *      org.quartz.JobExecutionException)
     */
    public synchronized void jobWasExecuted(JobExecutionContext context,
                               JobExecutionException jobException) {

        logger.debug("Done executing -- "
            + context.getJobDetail().getFullName());
        if (!isPublic(context)) {
            return;
        }

        logger.debug("Job : " + context.getJobDetail().getFullName() + " -- "
            + context.getJobDetail().getJobDataMap().get(Job.RUN_THIS_JOB));
        if (!context.getJobDetail().getJobDataMap()
            .getBoolean(Job.RUN_THIS_JOB)) {
            logger.debug("Not saving...");
            return;
        }

        try {
            bind();
            stopMonitor(context);
            JobHistory jh = (JobHistory) context.getJobDetail().getJobDataMap()
                .get(Job.JOB_HISTORY);

            context.getJobDetail().getJobDataMap().put(
                Job.LAST_RUN, new Date(System.currentTimeMillis()));

            if (context.getNextFireTime() != null) {
                context.getJobDetail().getJobDataMap().put(
                    Job.NEXT_RUN, context.getNextFireTime());
            }

            jh.setJobFinished(new Date(System.currentTimeMillis()));

            if (jobException != null) {
                jh.setJobResult(ScheduleLastResult.FAILURE);
                context.getJobDetail().getJobDataMap().put(
                    Job.LAST_RUN_RESULT, ScheduleLastResult.FAILURE);
                context.getJobDetail().getJobDataMap().put(
                    Job.FAILURE_MESSAGE, jobException.getMessage());
            } else {
                if (!ScheduleLastResult.STOPPED.equals(context.getJobDetail()
                    .getJobDataMap().get(Job.LAST_RUN_RESULT))) {

                    jh.setJobResult(ScheduleLastResult.SUCCESS);
                    context.getJobDetail().getJobDataMap().put(
                        Job.LAST_RUN_RESULT, ScheduleLastResult.SUCCESS);
                } else {
                    logger.debug("Last result is a stopped....");
                    jh.setJobResult(ScheduleLastResult.STOPPED);
                    context.getJobDetail().getJobDataMap().put(
                        Job.LAST_RUN_RESULT, ScheduleLastResult.STOPPED);
                }
            }

            this.jobManager.saveJobHistory(jh);
            String[] nms = context.getScheduler().getJobNames(
                Job.PRIVATE_GROUP_NAME);
            for (String nm : nms) {
                logger.debug("Name: " + nm);
            }
        } catch (DataAccessException dae) {
            logger.warn("Could not save post-job in listener - "
                + context.getJobDetail().getName() + " - " + dae.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unbind();
        }
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    protected SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Setter for the jobManager property.
     *
     * @param jobManager the new jobManager value
     */
    public void setJobManager(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    /**
     * Method used to start any monitoring jobs.
     * @param pingJob whether the job should be pinged
     * @param context the context
     */
    protected void startMonitor(boolean pingJob, JobExecutionContext context) {

    }

    /**
     * Used to stop any monitoring jobs.
     * @param context the context
     */
    protected void stopMonitor(JobExecutionContext context) {

    }

    /**
     * Gets the job manager.
     *
     * @return the job manager
     */
    protected JobManager getJobManager() {
        return this.jobManager;
    }

    /**
     * Determines if this job is running too early. Used for clustering.
     *
     * @param job the job
     * @return whether this job is trying to execute too early
     */
    protected boolean isEarlyRun(Job job) {
        return false;
    }

    /**
     * Determines if this job is being started manually. Used for clustering.
     *
     * @param context the context
     * @return whether this job is being run manually
     */
    protected boolean isForceRun(JobExecutionContext context) {
        boolean forceRun = false;
        if (context.getJobDetail().getJobDataMap()
            .containsKey(Job.RUN_THIS_JOB)) {
            forceRun = context.getJobDetail().getJobDataMap().getBoolean(
                Job.RUN_THIS_JOB);
        }
        return forceRun;
    }

    /**
     * Based upon the values of the parameters, determines whether this job
     * should be run.
     *
     * @param job the job
     * @param early whether the job is running early
     * @param force whether the job should be forced to run
     * @param context the execution context
     * @return whether this job should be run
     */
    protected boolean runThis(Job job,
                              boolean early,
                              boolean force,
                              JobExecutionContext context) {
        boolean run = false;
        if ((job.getEnabled()) || (force)) {
            run = true;
        }
        context.getJobDetail().getJobDataMap().put(Job.RUN_THIS_JOB, run);
        return run;
    }

    /**
     * Updates the status variables of this job before it runs.
     *
     * @param job the job
     * @param context the execution context
     * @throws DataAccessException on exception if a manager is used
     */
    protected void updatePreJobStatus(Job job, JobExecutionContext context)
        throws DataAccessException {
        logger.debug("UPDATING PRE-JOB STATUS...");

        if (context.getFireTime() != null) {
            job.setLastStarted(context.getFireTime());
            context.getJobDetail().getJobDataMap().put(
                Job.LAST_STARTED, context.getFireTime());
        }

        if (context.getNextFireTime() != null) {
            job.setLastRun(context.getNextFireTime());
            context.getJobDetail().getJobDataMap().put(
                Job.NEXT_RUN, context.getNextFireTime());
        }

        job.setLastRunResult(PENDING);
        context.getJobDetail().getJobDataMap()
            .put(Job.LAST_RUN_RESULT, PENDING);

        JobHistory jh = new JobHistory(
            job.getId(), job.getDisplayName(), new Date(System
                .currentTimeMillis()));
        logger.debug("Putting jh : " + context.getJobDetail().getFullName());
        context.getJobDetail().getJobDataMap().put(Job.JOB_HISTORY, jh);
    }

    /**
     * Creates and binds a session for this job.
     */
    protected void bind() {
        try {
            Session session = SessionFactoryUtils.getSession(
                getSessionFactory(), true);
            TransactionSynchronizationManager.bindResource(
                sessionFactory, new SessionHolder(session));
        } catch (IllegalStateException e) {
            logger.warn("Exception binding... " + e.getLocalizedMessage());
        }
    }

    /**
     * Releases the session for this job.
     */
    protected void unbind() {
        try {
            Session session = ((SessionHolder) TransactionSynchronizationManager
                .getResource(getSessionFactory())).getSession();
            TransactionSynchronizationManager
                .unbindResource(getSessionFactory());
            SessionFactoryUtils.releaseSession(session, getSessionFactory());
        } catch (IllegalStateException e) {
            logger.warn("Exception unbinding... " + e.getLocalizedMessage());

        }
    }

    /**
     * Gets the job.
     * @param nm the job name
     * @return the job
     * @throws DataAccessException on exception
     */
    protected Job getJob(String nm) throws DataAccessException {
        Job job = null;
        try {
            job = this.getJobManager().updateAndFindJobByName(nm);
        } catch (OptimisticLockingFailureException e) {
            e.printStackTrace();
        }

        return job;
    }

    /**
     * Determines if the job is public.
     *
     * @param context the context
     * @return whether the job is public
     */
    private boolean isPublic(JobExecutionContext context) {
        if (context.getJobDetail().getGroup().equals(Job.PUBLIC_GROUP_NAME)) {
            return true;
        }
        return false;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 