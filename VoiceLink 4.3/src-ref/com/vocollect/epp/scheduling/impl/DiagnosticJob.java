/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.impl;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.EPPAbstractQuartzJob;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class implements a schedulable job for logging JVM memory usage.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 * @author hulrich
 */
public class DiagnosticJob extends EPPAbstractQuartzJob {

    // Instantiate a logger for this class.
    private static final Logger logger = new Logger(DiagnosticJob.class);

    /**
     * This Quartz job logs JVM memory statistics.
     * @param context describes details corresponding to the current invocation.
     * @throws JobExecutionException if an error occurs during job execution
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     * @author jkercher
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        logger.debug("DIAGNOSTIC JOB:::");
        if (logger.isDebugEnabled()) {
            Runtime runtime = Runtime.getRuntime();
            logger.debug("-------- Diagnostics: Java VM Usage ---------------");
            logger.debug("JVM totalMemory():         " + runtime.totalMemory());
            logger.debug("JVM maxMemory():           " + runtime.maxMemory());
            logger.debug("JVM freeMemory():          " + runtime.freeMemory());
            logger.debug("JVM availableProcessors(): "
                + runtime.availableProcessors());
            logger.debug("-------- Diagnostics: Java VM Usage ---------------");
        }
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    @Override
    protected void internalInterrupt() {

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 