/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.impl;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.EPPSpringQuartzJob;
import com.vocollect.epp.service.LicenseManager;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


/**
 * This job will be programmatically added to Quartz and will run every night
 * at midnight and check the status of the license expiration date.
 *
 * @author jstonebrook
 */
public class LicenseExpirationJob extends EPPSpringQuartzJob {

    private static final Logger log = new Logger(LicenseExpirationJob.class);

    /**
     * LicenseManager is used to check the status of the license.
     */
    private LicenseManager systemLicenseManager;

    private boolean running;

    /**
     * {@inheritDoc}
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    protected void internalExecute(JobExecutionContext ctx) throws JobExecutionException {
        try {
            if (!running) {
                running = true;
                log.debug("Saving notification license...");
                getSystemLicenseManager().saveNotificationForLicenseStatus();
                log.debug("done saving...");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void internalInterrupt() {
        
    }

    
    /**
     * Getter for the systemLicenseManager property.
     * @return LicenseManager value of the property
     */
    public LicenseManager getSystemLicenseManager() {
        return this.systemLicenseManager;
    }

    
    /**
     * Setter for the systemLicenseManager property.
     * @param lm the new systemLicenseManager value
     */
    public void setSystemLicenseManager(LicenseManager lm) {
        this.systemLicenseManager = lm;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 