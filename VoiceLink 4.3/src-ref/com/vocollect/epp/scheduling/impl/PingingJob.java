/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.impl;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.ClusterMonitoringJob;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Job used to update the DB, and therefore other nodes, that the monitored job
 * is still running.
 * 
 * @author hulrich
 */
public class PingingJob extends ClusterMonitoringJob {

    private static final Logger log = new Logger(PingingJob.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalExecute(org.quartz.JobExecutionContext)
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        try {
            this.setRunning(true);
            log.debug("Pinging job : " + context.getJobDetail().getName());
            log.debug("Monitoring job : "
                + context.getJobDetail().getJobDataMap().containsKey(
                    BUSINESS_JOB_NAME));
            populateJob(context);
            
            while (isRunning()) {
                getJobManager().executePing(getJobName());
                Thread.sleep(getInterval());
            }
        } catch (InterruptedException ie) {
        } catch (Exception e) {
            e.printStackTrace();
            throw new JobExecutionException("Failure while executing : "
                + this.getClass().getCanonicalName() + ".  Further details: "
                + e.getClass().getCanonicalName() + " -> "
                + e.getLocalizedMessage());
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalInterrupt()
     */
    @Override
    protected void internalInterrupt() {
        this.setRunning(false);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 