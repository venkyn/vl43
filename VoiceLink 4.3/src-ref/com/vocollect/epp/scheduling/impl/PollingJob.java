/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.scheduling.ClusterMonitoringJob;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

/**
 * Job used to poll the manager at set interval, expecting the last ping value
 * to be less than the ping interval.
 * 
 * @author hulrich
 */
public class PollingJob extends ClusterMonitoringJob {

    private static final Logger log = new Logger(PollingJob.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalExecute(org.quartz.JobExecutionContext)
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {

        try {
            this.setRunning(true);
            log.debug("Name : "
                + context.getJobDetail().getJobDataMap().getString(
                    BUSINESS_JOB_NAME));
            log.debug("Manager : " + getJobManager());
            populateJob(context);
            
            while (isRunning()) {
                log.debug("Polling for job : " + getJobName());
                if (isPastDue()) {
                    log.debug("Overdue job : " + getJobName());
                    startJob(context);
                    setRunning(false);
                } else {
                    Thread.sleep(getInterval());
                }
            }
        } catch (InterruptedException ie) {
        } catch (Exception e) {
            e.printStackTrace();
            throw new JobExecutionException("Failure while executing : "
                + this.getClass().getCanonicalName() + ".  Further details: "
                + e.getClass().getCanonicalName() + " -> "
                + e.getLocalizedMessage());
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#internalInterrupt()
     */
    @Override
    protected void internalInterrupt() {
        setRunning(false);
    }

    /**
     * Determines if this job has not pinged the database within the given
     * interval.
     * @return whether this job is past due
     * @throws DataAccessException on exception
     */
    protected boolean isPastDue() throws DataAccessException {
        if ((getJob().isRunning())
            && (getJobManager().executePoll(getJob().getName()) + getJob()
                .getPingInterval()) < getNow()) {

            return true;
        }
        return false;
    }

    /**
     * Starts the job being monitored, assumes it is in failed state.
     * @param context the context
     * @throws DataAccessException on exception
     */
    protected void startJob(JobExecutionContext context)
        throws DataAccessException {
        try {
            getJob().setRunning(false);
            getJobManager().saveJob(getJob());
            context.getScheduler().triggerJob(getJob().getName(), getJob().getGroup());
        } catch (SchedulerException se) {
            log.warn("Unable to start the clustered, rescued job : "
                + getJob().getName());
            se.printStackTrace();
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 