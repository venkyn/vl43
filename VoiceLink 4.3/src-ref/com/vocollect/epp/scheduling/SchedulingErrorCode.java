/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.errors.ErrorCode;

/**
 * Class to hold the instances of error codes for EPP scheduling-related
 * errors.
 * @author J Kercher
 */
public final class SchedulingErrorCode extends ErrorCode {

    /**
     * Lower boundary of the EPP scheduling error range.
     */
    public static final int LOWER_BOUND  = 4000;

    /**
     * Upper boundary of the EPP scheduling error range.
     */
    public static final int UPPER_BOUND  = 4999;

    /**
     * Constructor.
     */
    private SchedulingErrorCode() {
        super("EPP", LOWER_BOUND, UPPER_BOUND);
    }

    /**
     * Perform base initialization.
     */
    public static final SchedulingErrorCode NO_ERROR
        = new SchedulingErrorCode();

    /**
     * Unable to access the default scheduler instance.
     */
    public static final SchedulingErrorCode NO_SCHEDULER_ACCESS
        = new SchedulingErrorCode(4000);

    /**
     * Unable to schedule job.
     */
    public static final SchedulingErrorCode UNABLE_TO_SCHED_JOB
        = new SchedulingErrorCode(4001);

    /**
     * Job already running.
     */
    public static final SchedulingErrorCode JOB_ALREADY_RUNNING
        = new SchedulingErrorCode(4002);

    /**
     * Job cannot be stopped, it is not running.
     */
    public static final SchedulingErrorCode JOB_NOT_RUNNING
        = new SchedulingErrorCode(4003);

    /**
     * Unable to stop job.
     */
    public static final SchedulingErrorCode UNABLE_TO_STOP_JOB
        = new SchedulingErrorCode(4004);

    /**
     * Unable to store trigger.
     */
    public static final SchedulingErrorCode UNABLE_TO_STORE_TRIGGER
        = new SchedulingErrorCode(4005);

    /**
     * Attempt to relaunch an alreaady running job.
     */
    public static final SchedulingErrorCode RELAUNCH_RUNNING_JOB
    = new SchedulingErrorCode(4006);

    /**
     * Job is null in job listener.
     */
    public static final SchedulingErrorCode POST_JOB_EXECUTION_IS_NULL
    = new SchedulingErrorCode(4007);


    /**
     * Unable to start the scheduler.
     */
    public static final SchedulingErrorCode UNABLE_TO_START
        = new SchedulingErrorCode(4008);

    /**
     * Create a custom error code using the given value.
     * @param err - error to be logged
     */
    private SchedulingErrorCode(long err) {
        super(SchedulingErrorCode.NO_ERROR, err);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 