/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.lang.reflect.Method;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Spring version of the <code>EPPAbstractQuartzJob</code>.
 * 
 * @author hulrich
 */
public abstract class EPPSpringQuartzJob extends EPPAbstractQuartzJob implements
    ApplicationContextAware {

    private static final Logger log = new Logger(EPPSpringQuartzJob.class);

    private static ApplicationContext context;

    private SiteContext siteContext;

    private SessionFactory sessionFactory;

    private boolean autoWire = true;

    /**
     * Constructor.
     */
    public EPPSpringQuartzJob() {
        if (context != null) {
            this.setProperties();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#bind()
     */
    @Override
    protected void bind() {
        try {
            Session session = SessionFactoryUtils.getSession(
                getSessionFactory(), true);
            TransactionSynchronizationManager.bindResource(
                getSessionFactory(), new SessionHolder(session));
        } catch (IllegalStateException e) {
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.scheduling.EPPAbstractQuartzJob#unbind()
     */
    @Override
    protected void unbind() {
        try {
            Session session = ((SessionHolder) TransactionSynchronizationManager
                .getResource(getSessionFactory())).getSession();
            TransactionSynchronizationManager
                .unbindResource(getSessionFactory());
            SessionFactoryUtils.releaseSession(session, getSessionFactory());
        } catch (IllegalStateException e) {
        }
    }


    /**
     * Getter for the autoWire property.
     * @return boolean value of the property
     */
    public boolean getAutoWire() {
        return this.autoWire;
    }

    /**
     * Setter for the autoWire property.
     * @param autoWire the new autoWire value
     */
    public void setAutoWire(boolean autoWire) {
        this.autoWire = autoWire;
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
     */
    public void setApplicationContext(ApplicationContext ctx) {
        if (log.isDebugEnabled()) {
            log.debug("App context -- " + ctx);
        }
        context = ctx;
        try {
            setProperties();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the <code>ApplicationContext</code>.
     * @return the application context
     */
    protected ApplicationContext getApplicationContext() {
        return context;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    protected SiteContext getSiteContext() {
        return this.siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
        this.siteContext.setFilterBySite(false);
        SiteContextHolder.setSiteContext(siteContext);
    }

    /**
     * Autowires the available properties.
     */
    private void setProperties() {
        if ((this.getApplicationContext() == null) && (getAutoWire())) {
            return;
        }

        Method[] methods = this.getClass().getMethods();
        for (Method m : methods) {
            if (m.getName().startsWith("set")) {
                setBean(m);
            }
        }
    }

    /**
     * Sets the bean, if applicable, on the object.
     * @param method the setter
     */
    private void setBean(Method method) {
        try {
            String bean = getBeanName(method.getName());
            Object o = this.getApplicationContext().getBean(bean);
            method.invoke(this, new Object[] { o });
        } catch (Exception e) {

        }
    }

    /**
     * Retrieves the bean name based upon autowire principle.
     * @param methodName the method name
     * @return the bean id
     */
    private String getBeanName(String methodName) {
        String beanName = methodName.substring("set".length());
        return beanName.substring(0, 1).toLowerCase() + beanName.substring(1);
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 