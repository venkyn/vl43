/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class should be subclassed by any implementing jobs that wish to cluster
 * the application, however, it is best to just subclass this for simplicity.<br>
 * Simply implement the internal methods.
 * 
 * @author hulrich
 */
public abstract class EPPAbstractQuartzJob implements EPPQuartzJob {

    private static final Logger log = new Logger(EPPAbstractQuartzJob.class);

    /**
     * {@inheritDoc}
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    public void execute(JobExecutionContext context)
        throws JobExecutionException {

        try {
            if (!context.getJobDetail().getJobDataMap().getBoolean(
                Job.RUN_THIS_JOB)) {
                return;
            }
            bind();
            internalExecute(context);
            unbind();
        } catch (Throwable t) {
            t.printStackTrace();
            throw new JobExecutionException();
        } 
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    public void interrupt() {
        internalInterrupt();
    }

    /**
     * Implements the functionality for this job.
     * @param context the job context
     * @throws JobExecutionException on failure
     */
    protected abstract void internalExecute(JobExecutionContext context)
        throws JobExecutionException;

    /**
     * Implements the functionality for interrupting the job.
     */
    protected abstract void internalInterrupt();
    
    /**
     * Creates and binds a session for this job.
     */
    protected void bind() {
        
    }
    
    /**
     * Releases the session for this job.
     */
    protected void unbind() {
        
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 