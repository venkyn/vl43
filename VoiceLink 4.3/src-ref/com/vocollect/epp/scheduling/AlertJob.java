/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertStatus;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class implements a schedulable job for logging JVM memory usage.
 * 
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 * @author smittal
 */

public class AlertJob extends EPPSpringQuartzJob {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(AlertJob.class);

    private AlertManager alertManager;

    private NotificationManager notificationManager;

    private SiteContext siteContext;

    /**
     * This Quartz job activates the Purge Archive Process.
     * @param context describes details corresponding to the current invocation.
     * @throws JobExecutionException if an error occurs during job execution
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     * @author smittal
     */
    @Override
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        try {
            log.debug("####ALERT: Starting Alert Process:::");
            List<Alert> alertsList = this.getAlertManager().getAll();
            int count = alertsList.size();
            log.info(count + " Alerts exist in the system");

            for (Alert activeAlert : alertsList) {
                if (activeAlert.getAlertStatus() == AlertStatus.ENABLED) {
                    SiteContext siteContextObj = SiteContextHolder
                        .getSiteContext();
                    Site siteValue = siteContextObj.getSite(activeAlert);
                    setSiteContext(siteContextObj);
                    log.info("##### Alert being executed is "
                        + activeAlert.getAlertName() + ": Site ID = "
                        + siteValue.getId() + " Site Name = "
                        + siteValue.getName());
                    siteContextObj.setCurrentSite(siteValue);
                    siteContextObj.setFilterBySite(true);
                    SiteContextHolder.setSiteContext(siteContextObj);
                    this.getAlertManager().executeAlerts(activeAlert);
                }
            }
        } catch (Exception e) {
            log.error("Error during Alert Checking process",
                SystemErrorCode.ALERT_JOB_FAILURE, e);
            try {

                // Create Alert Job Notification
                LOPArrayList lop = new LOPArrayList();
                getNotificationManager().createNotification(
                    "notification.alertJob.title",
                    "errorCode.message.EPP104", NotificationPriority.CRITICAL,
                    new Date(), "100",
                    "notification.column.keyname.Application.2", lop);
            } catch (Exception exc) {
                log.error("Error creating Notification",
                    SystemErrorCode.NOTIFICATION_FAILURE, exc);
            }
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    @Override
    protected void internalInterrupt() {

        // Indicate Alert Job is failing in the log file
        log.debug("####ALERT: Stopping Alert Process:::");

    }

    /**
     * Gets the alert manager.
     * @return the manager
     */
    protected AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * Sets the alert manager.
     * @param alertManager the manager
     */
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    @Override
    public SiteContext getSiteContext() {
        return this.siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    @Override
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 