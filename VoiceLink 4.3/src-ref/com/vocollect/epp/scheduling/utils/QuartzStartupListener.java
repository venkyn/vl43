/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.utils;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.service.JobManager;
import com.vocollect.epp.service.SystemPropertyManager;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerContext;
import org.quartz.ee.servlet.QuartzInitializerListener;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * This class handles the initialization and shutdown tasks for the Quartz
 * scheduler. It should be overridden for custom tasks.
 *
 * @author hulrich
 */
public class QuartzStartupListener extends QuartzInitializerListener {

    private static final Logger log = new Logger(QuartzStartupListener.class);

    /**
     * {@inheritDoc}
     * @see org.quartz.ee.servlet.QuartzInitializerListener#contextDestroyed(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.debug("Shutting down ****************************************");
        try {

            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            for (String nm : scheduler.getJobNames(Job.MONITOR_GROUP_NAME)) {
                log.debug("Monitor : " + nm);
            }
            scheduler.shutdown();
        } catch (Exception e) {
            log.warn("EPP Scheduler failed to shutdown.", e);
        }

    }

    /**
     * {@inheritDoc}
     * @see org.quartz.ee.servlet.QuartzInitializerListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            log.debug("got the scheduler... " + scheduler);
            scheduler.getContext().put(
                JobManager.class.getName(), getSpringBean(sce.getServletContext(),
                    "jobManager"));
            assignListener(scheduler.getContext(), sce.getServletContext());
            log.debug("scheduler is set ...");
            scheduler.start();

        } catch (Exception e) {
            log.warn("Cannot get the scheduler - this is bad..");
            log.warn(e.getClass().getName() + " - " + e.getMessage());
            StackTraceElement[] el = e.getStackTrace();
            for (StackTraceElement ste : el) {
                log.debug(ste);
            }
        }
    }

    /**
     * Sets the <code>JobManager</code> if it has not already been set.
     * @param se the servlet context
     * @param id the bean id
     * @return the job manager
     */
    private Object getSpringBean(ServletContext se, String id) {
        WebApplicationContext ctx = WebApplicationContextUtils
            .getRequiredWebApplicationContext(se);
        return ctx.getBean(id);
    }

    /**
     * Used to load the correct listener.
     * @param ctx the scheduler context
     * @param sc the servlet context
     * @throws DataAccessException if the clustered property cannot be found
     */
    private void assignListener(SchedulerContext ctx, ServletContext sc)
        throws DataAccessException {
        String listener = "";
        SystemPropertyManager mgr = (SystemPropertyManager)
            getSpringBean(sc, "systemPropertyManager");
        if ("yes".equals(mgr.findByName(SystemPropertyManager.CLUSTERED_PROP_NAME))) {
            listener = "clusterableJobListener";
        } else {
            listener = "jobListener";
        }
        ctx.put(JobListener.class.getName(), getSpringBean(sc, listener));
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 