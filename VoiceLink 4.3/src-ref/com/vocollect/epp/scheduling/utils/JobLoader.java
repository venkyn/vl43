/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling.utils;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.scheduling.SchedulingErrorCode;
import com.vocollect.epp.scheduling.impl.LicenseExpirationJob;
import com.vocollect.epp.service.JobManager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.spi.SchedulerPlugin;
import org.quartz.xml.JobSchedulingBundle;
import org.quartz.xml.JobSchedulingDataProcessor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * This class handles loading the jobs from the epp_Jobs.xml into the scheduler
 * and database.
 *
 * @author hulrich
 */
public class JobLoader implements SchedulerPlugin {

    private static final Logger log = new Logger(JobLoader.class);

    private static final String LIST_KEY = "List";

    private JobManager jobManager;

    private JobListener globalJobListener;

    private String[] fileNames;

    /**
     * {@inheritDoc}
     * @see org.quartz.spi.SchedulerPlugin#initialize(java.lang.String,
     *      org.quartz.Scheduler)
     */
    public void initialize(String name, Scheduler scheduler)
        throws SchedulerException {

    }

    /**
     * {@inheritDoc}
     * @see org.quartz.spi.SchedulerPlugin#shutdown()
     */
    public void shutdown() {
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.spi.SchedulerPlugin#start()
     */
    public void start() {
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            if (log.isDebugEnabled()) {
                log.debug("Starting ----------------- " + scheduler);
            }
            for (String s : scheduler.getContext().getKeys()) {
                if (log.isTraceEnabled()) {
                    log.trace("KEY : " + s);
                }
            }

            this.jobManager = (JobManager) scheduler.getContext().get(
                JobManager.class.getName());
            this.globalJobListener = (JobListener) scheduler.getContext().get(
                JobListener.class.getName());

            if (log.isDebugEnabled()) {
                log.debug("MANG: " + jobManager + " -- LIST: "
                    + this.globalJobListener);
            }

            this.scheduleJobs(scheduler);
        } catch (SchedulerException e) {
            log.error("Could not start scheduler",
                SchedulingErrorCode.UNABLE_TO_START, e);
        }
    }

    /**
     * Getter for the fileName property.
     * @return String[] value of the property
     */
    public String[] getFileName() {
        return fileNames;
    }

    /**
     * Sets the file names.
     * @param fileNames comma delimited file names
     */
    public void setFileNames(String fileNames) {
        if (log.isDebugEnabled()) {
            log.debug("Init: " + fileNames);
        }
        String[] names = fileNames.split(",");
        List<String> paths = this.getFilePaths(names);
        this.fileNames = new String[paths.size()];
        paths.toArray(this.fileNames);
    }

    /**
     * Loads jobs from DB and file and schedules them. The jobs in the DB have
     * precedence.
     * @param scheduler the scheduler
     */
    private void scheduleJobs(Scheduler scheduler) {
        if (log.isDebugEnabled()) {
            log.debug("Starting to load jobs....");
        }
        List<Job> scheduledJobs = new LinkedList<Job>();
        JobSchedulingDataProcessor processor = new JobSchedulingDataProcessor(
            true, false, false);
        try {
            if (log.isDebugEnabled()) {
                log.debug("Persisted.... - " + scheduler);
                log.debug("Listener -- " + this.globalJobListener);
            }

            scheduler.addGlobalJobListener(this.globalJobListener);
            scheduler.standby();

            for (String fileName : fileNames) {
                if (log.isDebugEnabled()) {
                    log.debug("Processing file... " + fileName);
                }
                processor
                    .processFileAndScheduleJobs(fileName, scheduler, false);
                if (log.isDebugEnabled()) {
                    log.debug("Processed jobs --- " + processor.getScheduledJobs());
                }

                scheduledJobs.addAll(this.getJobListFromMap(processor
                    .getScheduledJobs()));
            }
            if (log.isDebugEnabled()) {
                log.debug("Sked Job count --- " + scheduledJobs.size());
            }
            List<Job> persistedJobs = this.getPersistedJobs();
            saveJobs(scheduledJobs, persistedJobs);
        } catch (Exception e) {
            log.error("Error loading jobs -- " + e.getMessage(),
                SchedulingErrorCode.UNABLE_TO_SCHED_JOB, e);
        } finally {
            try {
                JobDetail licenseEnforcementJob = new JobDetail(
                    "License Enforcement Job", Job.PRIVATE_GROUP_NAME,
                    LicenseExpirationJob.class);
                Trigger trigger = TriggerUtils.makeDailyTrigger(0, 0);
                trigger.setName("midnightLicenseEnforcementTrigger");
                scheduler.scheduleJob(licenseEnforcementJob, trigger);

                // We need to check if there is any job persisted but did not
                // come from XML. This will happen when user schedules external
                // job introduced in VL4.2 release
                List<Job> persistedJobs = this.getPersistedJobs();
                for (Job job : persistedJobs) {
                    if (jobExists(scheduledJobs, job)) {
                        continue;
                    }

                    Trigger persistedJobTrigger = new SimpleTrigger(
                        job.getName(), Job.PUBLIC_GROUP_NAME,
                        job.getRepeatCount(), job.getInterval());

                    scheduler.scheduleJob(job.getJobDetail(),
                        persistedJobTrigger);
                }

                scheduler.start();
            } catch (Exception e) {
                log.error("Error loading license enforcement job -- "
                    + e.getMessage(), SchedulingErrorCode.UNABLE_TO_SCHED_JOB, e);
            }
        }
    }

    /**
     * Method to find out if the job exists in the list. Some how list.contains
     * didn't work. Hence the need for this method
     * @param jobs list of scheduled jobs
     * @param dbJob job to be added to the schedule
     * @return result of found from the list
     */
    private boolean jobExists(List<Job> jobs, Job dbJob) {
        for (Job job : jobs) {
            if (log.isDebugEnabled()) {
                log.debug("PERS: " + job.getName() + " -- " + job.getName());
            }
            if (job.getName().equals(dbJob.getName())) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Synchronizes the scheduled and persisted jobs.
     * @param scheduledJobs the jobs loaded from the XML file
     * @param persistedJobs the jobs in the data store
     * @throws DataAccessException if there is a problem storing the jobs
     */
    private void saveJobs(List<Job> scheduledJobs, List<Job> persistedJobs)
        throws DataAccessException {
        for (Job scheduledJob : scheduledJobs) {
            if (log.isDebugEnabled()) {
                log.debug("Scheduled job : " + scheduledJob.getName());
            }
            boolean found = false;
            for (Job persistedJob : persistedJobs) {
                if (log.isDebugEnabled()) {
                    log.debug("PERS: " + persistedJob.getName() + " -- "
                        + scheduledJob.getName());
                }
                if (persistedJob.getName().equals(scheduledJob.getName())) {
                    saveJob(persistedJob);
                    found = true;
                    break;
                }
            }

            if (!found) {
                if (log.isDebugEnabled()) {
                    log.debug("Adding new job ... " + scheduledJob.getName()
                        + " -- " + scheduledJob + " -- "
                        + scheduledJob.getInterval());
                }

                saveJob(scheduledJob);
            }
        }
    }

    /**
     * Builds a list from the JobSchedulingBundle job map.
     * @param jobMap the job map
     * @return a list of <code>Job</code>'s
     */
    @SuppressWarnings("unchecked")
    private List<Job> getJobListFromMap(Map jobMap) {
        List<Job> jobList = new ArrayList<Job>();
        Iterator keys = jobMap.keySet().iterator();
        while (keys.hasNext()) {
            JobSchedulingBundle bundle = (JobSchedulingBundle) jobMap.get(keys
                .next());
            JobDetail detail = this.buildLists(bundle.getJobDetail());

            Job job = new Job(detail);
            job.setTriggerInformation(bundle.getTriggers());
            jobList.add(job);
        }

        return jobList;
    }

    /**
     * Retrieves all persisted jobs.
     * @return the persisted jobs
     * @throws Exception if unable to get jobs.
     */
    private List<Job> getPersistedJobs() throws Exception {
        return this.jobManager.updateAndGetAllJobs();
    }

    /**
     * Saves public jobs.
     * @param job the job
     * @throws DataAccessException if unable to save
     */
    private void saveJob(Job job) throws DataAccessException {
        if (!job.getGroup().contains(Job.PRIVATE_GROUP_NAME)) {
            this.jobManager.saveJob(job);
        }
    }

    /**
     * Workaround method for converting comma-delimited strings into Lists. The
     * name of the parameter must include the word "List".
     * @param detail the job detail to check
     * @return a new job detail that has an actual list rather than a string
     */
    private JobDetail buildLists(JobDetail detail) {
        JobDataMap map = detail.getJobDataMap();
        String[] keys = map.getKeys();
        for (String key : keys) {
            if (log.isTraceEnabled()) {
                log.trace("KEY: " + key);
            }

            if ((key.indexOf(LIST_KEY)) != -1) {
                String data = map.getString(key);
                String[] vals = data.split(",");
                List<String> list = new ArrayList<String>();
                for (String v : vals) {
                    list.add(v.trim());
                }
                map.put(key, list);
            }
        }
        return detail;
    }

    /**
     * Gets the paths of the files for the processor.
     * @param files the files
     * @return the list of full paths
     */
    private List<String> getFilePaths(String... files) {
        List<String> paths = new LinkedList<String>();
        for (String name : files) {
            String path = getFileName(name);
            if (path != null) {
                paths.add(path);
            }
        }
        return paths;
    }

    /**
     * Gets the file name from the name.
     * @param name the name
     * @return the full path
     */
    private String getFileName(String name) {
        String fileName = null;
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource res = resolver.getResource("classpath:**/" + name.trim());
        if (res != null) {
            fileName = res.getFilename();
        }
        return fileName;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 