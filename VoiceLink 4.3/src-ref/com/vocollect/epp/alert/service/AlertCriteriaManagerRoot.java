/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.service;

import com.vocollect.epp.alert.dao.AlertCriteriaDAO;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

/**
 * @author smittal
 * 
 */
public interface AlertCriteriaManagerRoot extends
    GenericManager<AlertCriteria, AlertCriteriaDAO>, RemovableDataProvider {

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 