/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.service;


import com.vocollect.epp.alert.dao.AlertDAO;
import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.GenericManager;
import com.vocollect.epp.service.RemovableDataProvider;

import java.util.List;


/**
 * @author mraj
 * 
 */
public interface AlertManagerRoot extends GenericManager<Alert, AlertDAO>,
    RemovableDataProvider {

    /**
     * Implementation to check to see if the name is unique.
     * @param alert the alert to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    void verifyUniquenessByAlertName(Alert alert) throws BusinessRuleException,
        DataAccessException;

    /**.
     * returns list of alerts having selected dashboard linked to them
     * @param dashboard selected dashboard
     * @return associated alerts
     */
    public List<Alert> listAlertsByLinkedDashboard(Dashboard dashboard);

    /**
     * @param activeAlert Alert.
     * @throws DataAccessException any database exception
     * @throws BusinessRuleException any Business rule exception
     */
    void executeAlerts(Alert activeAlert) throws BusinessRuleException,
        DataAccessException;

    /**.
     * count the number of alerts by alert id
     * @param alertId id of alert
     * @return alert count
     * @throws DataAccessException dae
     */
    public Long countById(Long alertId) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 