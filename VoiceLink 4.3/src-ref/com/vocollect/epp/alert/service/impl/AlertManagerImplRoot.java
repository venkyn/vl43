/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.service.impl;

import com.vocollect.epp.alert.dao.AlertDAO;
import com.vocollect.epp.alert.evaluator.AlertEvaluator;
import com.vocollect.epp.alert.event.AlertNotificationEvent;
import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertStatus;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.impl.GenericManagerImpl;
import com.vocollect.epp.util.AlertUtil;
import com.vocollect.epp.util.ApplicationContextHolder;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * @author mraj
 * 
 */
public abstract class AlertManagerImplRoot extends
    GenericManagerImpl<Alert, AlertDAO> implements AlertManager {

    private NotificationManager notificationManager;

    private AlertEvaluator alertEvaluator;

    private static final Logger log = new Logger(AlertManagerImplRoot.class);

    /**
     * @param primaryDAO - the primary DAO
     */
    public AlertManagerImplRoot(AlertDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * @return the notificationManager
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * @param notificationManager the notificationManager to set
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the alertEvaluator property.
     * @return AlertEvaluator value of the property
     */
    public AlertEvaluator getAlertEvaluator() {
        return alertEvaluator;
    }

    /**
     * Setter for the alertEvaluator property.
     * @param alertEvaluator the new alertEvaluator value
     */
    public void setAlertEvaluator(AlertEvaluator alertEvaluator) {
        this.alertEvaluator = alertEvaluator;
    }

    /**
     * Implementation to check to see if the alert name is unique.
     * @param alert the alert to check
     * @throws BusinessRuleException when not unique
     * @throws DataAccessException any database exception
     */
    public void verifyUniquenessByAlertName(Alert alert)
        throws BusinessRuleException, DataAccessException {
        boolean isNew = alert.isNew();
        // Uniqueness check for name
        Long nameUniquenessId = getPrimaryDAO().uniquenessByAlertName(
            alert.getAlertName());
        if (nameUniquenessId != null
            && (isNew || (!isNew && nameUniquenessId.longValue() != alert
                .getId().longValue()))) {
            throw new FieldValidationException("alert.alertName",
                alert.getAlertName(), new UserMessage(
                    "alert.error.duplicateName", alert.getAlertName()));
        }

    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.alert.service.AlertManagerRoot#getAlertsByLinkedDashboard(com.vocollect.epp.dashboard.model.Dashboard)
     */
    @Override
    public List<Alert> listAlertsByLinkedDashboard(Dashboard dashboard) {
        return this.getPrimaryDAO().listAlertByLinkedDashboard(dashboard);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.alert.service.AlertManagerRoot#countById(java.lang.Long)
     */
    @Override
    public Long countById(Long alertId) throws DataAccessException {
        return this.getPrimaryDAO().countById(alertId);
    }
    
    @Override
    public Object delete(Long id) throws BusinessRuleException,
        DataAccessException {
        Alert alert = getPrimaryDAO().get(id);
        if (alert.getAlertStatus() == AlertStatus.ENABLED) {
            throwBusinessRuleException("alert.delete.error.enabled");
        }

        return super.delete(id);
    }

    /**
     * 
     * {@inheritDoc}
     * @see com.vocollect.epp.alert.service.AlertManagerRoot#executeAlerts()
     */
    public void executeAlerts(Alert activeAlert) throws BusinessRuleException,
        DataAccessException {

        int frequencyInMinutes = AlertUtil.getFrequencyInMin(activeAlert
            .getReNotificationFrequency());

        DateTime nextAlertEvaluationTime = null;

        DateTime lastAlertEvaluatedTime = new DateTime(
            activeAlert.getLastEvaluationTime());

        if (activeAlert.getLastEvaluationTime() != null) {
            nextAlertEvaluationTime = lastAlertEvaluatedTime
                .plusMinutes(frequencyInMinutes);
        }

        // If last execution time is not set or next evaluation time is
        // before than current time or equal to current time
        if (activeAlert.getLastAlertEventTime() == null
            || nextAlertEvaluationTime.isBeforeNow()
            || nextAlertEvaluationTime.isEqualNow()) {

            JSONArray alertedRecords = new JSONArray();

            try {
                alertedRecords = alertEvaluator.evaluate(activeAlert);
            } catch (JSONException e) {
                throw new DataAccessException(e.getLocalizedMessage());
            }

            activeAlert.setLastEvaluationTime(new Date());
            // If there are alertable record
            if (alertedRecords.length() > 0) {
                // Setting the last execution time
                activeAlert.setLastAlertEventTime(new Date());

                // Fire an Alert Notification event
                AlertNotificationEvent event = new AlertNotificationEvent();
                event.setAlert(activeAlert);
                event.setAlertRecords(alertedRecords);
                ApplicationContextHolder.getApplicationContext().publishEvent(
                    event);

            }

        } else {
            log.info("Alert " + activeAlert.getAlertName()
                + " will be evaluated at " + nextAlertEvaluationTime.toDate());
        }

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 