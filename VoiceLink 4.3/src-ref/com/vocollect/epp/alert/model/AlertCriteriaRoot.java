/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.model;

import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.model.VersionedModelObject;

/**
 * This class defines model for Alert.
 * 
 * @author smittal
 */
public class AlertCriteriaRoot extends VersionedModelObject {

    private static final long serialVersionUID = -5112940014342897301L;

    private Alert alert;

    private Integer sequence;
    
    private DAColumn daColumn;
    
    private AlertOperandType operandType;

    private String threshold;
    
    private AlertCriteriaRelation relation;

    /**
     * Getter for the alert property.
     * @return Alert value of the property
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     * Setter for the alert property.
     * @param alert the new alert value
     */
    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    
    /**
     * Getter for the sequence property.
     * @return Integer value of the property
     */
    public Integer getSequence() {
        return sequence;
    }

    
    /**
     * Setter for the sequence property.
     * @param sequence the new sequence value
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * Getter for the daColumn property.
     * @return DAColumn value of the property
     */
    public DAColumn getDaColumn() {
        return daColumn;
    }

    /**
     * Setter for the daColumn property.
     * @param daColumn the new daColumn value
     */
    public void setDaColumn(DAColumn daColumn) {
        this.daColumn = daColumn;
    }
    
    /**
     * Getter for the operandType property.
     * @return AlertOperandType value of the property
     */
    public AlertOperandType getOperandType() {
        return operandType;
    }

    /**
     * Setter for the operandType property.
     * @param operandType the new operandType value
     */
    public void setOperandType(AlertOperandType operandType) {
        this.operandType = operandType;
    }

    /**
     * Getter for the thresholdValue property.
     * @return String value of the property
     */
    public String getThreshold() {
        return threshold;
    }

    /**
     * Setter for the threshold property.
     * @param threshold the new threshold value
     */
    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    
    /**
     * Getter for the relation property.
     * @return AlertCriteriaRelation value of the property
     */
    public AlertCriteriaRelation getRelation() {
        return relation;
    }

    
    /**
     * Setter for the relation property.
     * @param relation the new relation value
     */
    public void setRelation(AlertCriteriaRelation relation) {
        this.relation = relation;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AlertRoot)) {
            return false;
        }
        final AlertCriteriaRoot other = (AlertCriteriaRoot) obj;
        if ((getDaColumn() != other.getDaColumn())
            || (getOperandType() != other.getOperandType())
            || (getThreshold() != other.getThreshold())) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = this.daColumn == null ? 0 : this.daColumn.hashCode();
        hashCode += this.operandType == null ? 0 : this.operandType.hashCode();
        hashCode += this.threshold == null ? 0 : this.threshold.hashCode();
        return hashCode;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 