/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.model;

import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.model.VersionedModelObject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * This class defines model for Alert.
 * 
 * @author kudupi
 */
public class AlertRoot extends VersionedModelObject implements DataObject,
    Serializable, Taggable {

    // Serial Version ID
    private static final long serialVersionUID = 9055356766587314280L;

    private String alertName;

    private DAInformation daInformation;
    
    private List<AlertCriteria> alertCriterias;

    private NotificationPriority alertNotificationPriority;

    private String emailAddress;

    private AlertReNotificationFrequency reNotificationFrequency;
    
    private Date lastAlertEventTime;
    
    private Date lastEvaluationTime;
    
    private AlertStatus alertStatus = AlertStatus.ENABLED;
    
    private Dashboard dashboard;
    
    private String serverURL;

    private Set<Tag> tags;

    /**
     * Getter for the alertName property.
     * @return String value of the property
     */
    public String getAlertName() {
        return alertName;
    }

    /**
     * Setter for the alertName property.
     * @param alertName the new alertName value
     */
    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    /**
     * Getter for the daInformation property.
     * @return DAInformation value of the property
     */
    public DAInformation getDaInformation() {
        return daInformation;
    }

    /**
     * Setter for the daInformation property.
     * @param daInformation the new daInformation value
     */
    public void setDaInformation(DAInformation daInformation) {
        this.daInformation = daInformation;
    }
    
    /**
     * Getter for the alertCriterias property.
     * @return List<AlertCriteria> value of the property
     */
    public List<AlertCriteria> getAlertCriterias() {
        return alertCriterias;
    }

    /**
     * Setter for the alertCriterias property.
     * @param alertCriterias the new alertCriterias value
     */
    public void setAlertCriterias(List<AlertCriteria> alertCriterias) {
        this.alertCriterias = alertCriterias;
    }

    
    /**
     * Getter for the alertNotificationPriority property.
     * @return NotificationPriority value of the property
     */
    public NotificationPriority getAlertNotificationPriority() {
        return alertNotificationPriority;
    }

    /**
     * Setter for the alertNotificationPriority property.
     * @param alertNotificationPriority the new alertNotificationPriority value
     */
    public void setAlertNotificationPriority(NotificationPriority alertNotificationPriority) {
        this.alertNotificationPriority = alertNotificationPriority;
    }

    /**
     * Getter for the emailAdress property.
     * @return String value of the property
     */
    public String getEmailAddress() {
        return this.emailAddress;
    }

    /**
     * Setter for the emailAdress property.
     * @param emailAddress the new emailAddress value
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * @return the reNotificationFrequency
     */
    public AlertReNotificationFrequency getReNotificationFrequency() {
        return reNotificationFrequency;
    }

    
    /**
     * @param reNotificationFrequency the reNotificationFrequency to set
     */
    public void setReNotificationFrequency(AlertReNotificationFrequency reNotificationFrequency) {
        this.reNotificationFrequency = reNotificationFrequency;
    }

    public Date getLastAlertEventTime() {
        return lastAlertEventTime;
    }

    public void setLastAlertEventTime(Date lastAlertEventTime) {
        this.lastAlertEventTime = lastAlertEventTime;
    }

    /**
     * Getter for the lastEvaluationTime property.
     * @return Date value of the property
     */
    public Date getLastEvaluationTime() {
        return lastEvaluationTime;
    }

    /**
     * Setter for the lastEvaluationTime property.
     * @param lastEvaluationTime the new lastEvaluationTime value
     */
    public void setLastEvaluationTime(Date lastEvaluationTime) {
        this.lastEvaluationTime = lastEvaluationTime;
    }
    
    /**
     * Getter for the alertStatus property.
     * @return AlertStatus value of the property
     */
    public AlertStatus getAlertStatus() {
        return alertStatus;
    }

    /**
     * Setter for the alertStatus property.
     * @param alertStatus the new alertStatus value
     */
    public void setAlertStatus(AlertStatus alertStatus) {
        this.alertStatus = alertStatus;
    }
    
    
    /**
     * Getter for the dashboard property.
     * @return Dashboard value of the property
     */
    public Dashboard getDashboard() {
        return dashboard;
    }

    
    /**
     * Setter for the dashboard property.
     * @param dashboard the new dashboard value
     */
    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }
    
    
    /**
     * Getter for the serverURL property.
     * @return String value of the property
     */
    public String getServerURL() {
        return serverURL;
    }

    
    /**
     * Setter for the serverURL property.
     * @param serverURL the new serverURL value
     */
    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.model.DataObject#getDescriptiveText()
     */
    public String getDescriptiveText() {
        return this.getAlertName();
    }    
    
    /**
     * Getter for the tags property.
     * @return Set<Tag> value of the property
     */
    public Set<Tag> getTags() {
        return tags;
    }

    /**
     * Setter for the tags property.
     * @param tags the new tags value
     */
    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Alert)) {
            return false;
        }
        final Alert other = (Alert) obj;
        if (getAlertName() != other.getAlertName()) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.alertName == null ? 0 : this.alertName.hashCode();
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 