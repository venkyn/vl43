/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.alert.ui;


/**
 * @author mraj
 *
 */
public class AlertOperand {
    public static final String PROPERTIES = "properties";
    public static final String ID = "id";
    public static final String TEXT = "text";
    public static final String OPERANDTYPE = "operandType";
    public static final String THRESHOLD = "threshold";
    
    public static final int TYPE_STRING = 0;
    public static final int TYPE_NUMBER = 1;
    public static final int TYPE_TIME = 2;
    
    private String label;
    private int type;
    private Long id;
    
    /**
     * 
     * @param id - Id of operand.
     * @param type - The type of operand, String, Date, etc
     * @param label - The label for this operand
     */
    public AlertOperand(Long id, int type, String label) {
        this.id = id;
        this.label = label;
        this.type = type;
    }

    
    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    
    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    
    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    
    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    
    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 