/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.dao;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;

import java.util.List;

/**
 * @author mraj
 * 
 */
public interface AlertDAORoot extends GenericDAO<Alert> {

    /**
     * Returns the Long of the id, or null if not found.
     * @param alertName - the name of the alert to look for
     * @throws DataAccessException - indicates database error
     * @return alert id or null
     */
    public Long uniquenessByAlertName(String alertName)
        throws DataAccessException;

    /**.
     * returns list of alerts having selected dashboard linked to them
     * @param dashboard selected dashboard
     * @return associated alerts
     */
    public List<Alert> listAlertByLinkedDashboard(Dashboard dashboard);
    
    /**.
     * count the number of alerts by alert id
     * @param alertId id of alert
     * @return alert count
     * @throws DataAccessException dae
     */
    public Long countById(Long alertId) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 