/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event.listeners;

import com.vocollect.epp.alert.event.AlertNotificationEvent;
import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.SystemPropertyManager;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.SystemConfigurationAction;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.context.ApplicationListener;

/**
 * 
 * 
 * @author khazra
 */
public class AlertEmailEventListenerRoot implements
    ApplicationListener<AlertNotificationEvent> {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(
        AlertEmailEventListenerRoot.class);

    private DataAggregatorHandler dataAggregatorHandler;

    // SystemProperty Manager to access system properties.
    private SystemPropertyManager systemPropertyManager;

    /**
     * Getter for the dataAggregatorHandler property.
     * @return DataAggregatorHandler value of the property
     */
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * Setter for the dataAggregatorHandler property.
     * @param dataAggregatorHandler the new dataAggregatorHandler value
     */
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the systemPropertyManager property.
     * @return SystemPropertyManager value of the property
     */
    public SystemPropertyManager getSystemPropertyManager() {
        return systemPropertyManager;
    }

    /**
     * Setter for the systemPropertyManager property.
     * @param systemPropertyManager the new systemPropertyManager value
     */
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    @Override
    public void onApplicationEvent(AlertNotificationEvent alertNotificationEvent) {
        try {
            if (!StringUtil.isNullOrEmpty(alertNotificationEvent.getAlert()
                .getEmailAddress())) {
                sendMail(alertNotificationEvent.getAlert(),
                    alertNotificationEvent.getAlertRecords());

            } else {
                log.debug("No email configured for this alert");
            }
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Send mail to subscribed user.
     * @param alert The alert needs to be email
     * @param alertedRecords The records which are filtered for alert
     * @throws JSONException when problem accessing data aggregator
     * @throws MessagingException when problem e-mailing
     * @throws AddressException when recipient email is wrong
     * @throws DataAccessException when problem reading system properties
     */
    private void sendMail(Alert alert, JSONArray alertedRecords)
        throws JSONException, AddressException, MessagingException,
        DataAccessException {
        Properties props = System.getProperties();
        String smtpHostname = "";
        String fromAddress = "";
        String blockBoundary = "=======================================\t\n";
        String blockSeparation = "--------------------------------------------------------------\t\n";
        String gapSeparation = "\t\n\t\n\t\n\t\n";

        // Read system property for SMTP host and from address
        smtpHostname = getSystemPropertyManager().findByName(
            SystemConfigurationAction.SMTP_HOST).getValue();
        fromAddress = getSystemPropertyManager().findByName(
            SystemConfigurationAction.SMTP_USERNAME).getValue();

        if (smtpHostname.isEmpty()) {
            log.info("No SMTP host configured in System Configuration, cannot send email");
            return;
        }
        if (fromAddress.isEmpty()) {
            log.info("No SMTP user is configured in System Configuration, cannot send email");
            return;
        }

        // Setting SMTP host
        props.put("mail.smtp.host", smtpHostname);
        Session session = Session.getInstance(props, null);

        // Getting email message
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(fromAddress));

        // Prepare email IDs
        String[] emailIDs = alert.getEmailAddress().split(",");
        InternetAddress[] address = new InternetAddress[emailIDs.length];

        for (int i = 0; i < emailIDs.length; i++) {
            address[i] = new InternetAddress(emailIDs[i]);
        }

        // Set recipient
        msg.setRecipients(Message.RecipientType.TO, address);

        // Build the detail message
        StringBuilder sb = new StringBuilder();

        sb.append(
            ResourceUtil.getLocalizedKeyValue("alert.email.greetingsText"))
            .append("\t\n");
        sb.append(blockBoundary);

        sb.append(
            ResourceUtil.getLocalizedMessage(
                "alert.email.alertDescriptionText",
                new Object[] { alert.getAlertName() }, "")).append("\t\n");

        sb.append(blockBoundary);
        sb.append(
            ResourceUtil.getLocalizedKeyValue("alert.email.alertCriteria"))
            .append("\t\n");

        for (AlertCriteria criteria : alert.getAlertCriterias()) {
            sb.append(
                ResourceUtil.getLocalizedKeyValue(criteria.getRelation()
                    .getResourceKey())).append(" ");
            sb.append(
                ResourceUtil.getLocalizedKeyValue(criteria.getDaColumn()
                    .getDisplayName())).append(" ");
            sb.append(
                ResourceUtil.getLocalizedKeyValue(criteria.getOperandType()
                    .getResourceKey())).append(" ");
            sb.append(
                ResourceUtil.getLocalizedKeyValue(criteria.getThreshold()))
                .append(" ");
            sb.append(
                ResourceUtil.getLocalizedKeyValue(criteria.getDaColumn()
                    .getUom())).append(" ");
            sb.append("\t\n");
        }
        sb.append("\t\n");
        sb.append(
            ResourceUtil.getLocalizedMessage(
                "alert.email.alertRecordCountText",
                new Object[] { alertedRecords.length() }, "")).append("\t\n\t\n");
        
        
        // Put the dashboard link if user chooses to link to a dashboard
        if (alert.getDashboard() != null) {
            Long siteId = SiteContextHolder.getSiteContext().getSite(alert)
                .getId();
            Long tagId = SiteContextHolder.getSiteContext()
                .getTagBySiteId(siteId).getId();
            
            String link = alert.getServerURL()
                + "/dashboardalert/mydashboard/myDashboard.action?dashboardId="
                + alert.getDashboard().getId() + "&tempSiteID=" + tagId;

            sb.append(
                ResourceUtil.getLocalizedMessage("alert.email.alertLinkGoto",
                    new Object[] { link }, "")).append("\t\n");
        }

        sb.append(blockSeparation);

        sb.append(
            ResourceUtil
                .getLocalizedKeyValue("alert.email.alertDetailHeadingText"))
            .append("\t\n\t\n");

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName(alert.getDaInformation().getName());
        String identityColumnName = da.getIdentityColumnName();

        for (int i = 0; i < alertedRecords.length(); i++) {

            String identityValue = alertedRecords.getJSONObject(i).getString(
                identityColumnName);

            String alertFieldDisplayName = null;
            Object alertFieldValue = null;
            String uom = null;
            int c = 0;

            Set<String> fieldsAddedToEmail = new HashSet<String>();

            for (AlertCriteria alertCriteria : alert.getAlertCriterias()) {

                if (fieldsAddedToEmail.contains(alertCriteria.getDaColumn()
                    .getFieldId())) {
                    continue;
                }
                fieldsAddedToEmail
                    .add(alertCriteria.getDaColumn().getFieldId());

                alertFieldDisplayName = ResourceUtil
                    .getLocalizedKeyValue(alertCriteria.getDaColumn()
                        .getDisplayName());
                alertFieldValue = alertedRecords.getJSONObject(i).get(
                    alertCriteria.getDaColumn().getFieldId());
                if (alertCriteria.getDaColumn().getFieldType() == DAFieldType.TIME) {
                    if ((Integer) alertFieldValue == GenericDataAggregator.DEFAULT_TIME_VAL) {
                        alertFieldValue = (Object) ResourceUtil
                            .getLocalizedKeyValue("aggregator.field.time.value.default");
                    } else {
                        alertFieldValue = (Object) String.format("%02d:%02d",
                            (Integer) alertFieldValue / 100,
                            (Integer) alertFieldValue % 100);
                    }
                }

                if (alertCriteria.getDaColumn().getFieldType() == DAFieldType.DATE) {
                    SimpleDateFormat sdf = new SimpleDateFormat(
                        GenericDataAggregator.DATE_FORMAT);
                    alertFieldValue = (Object) sdf
                        .format((Date) alertFieldValue);
                }

                uom = ResourceUtil.getLocalizedKeyValue(alertCriteria
                    .getDaColumn().getUom());

                c++;
                if (c != 1) {
                    identityValue = " ";
                }

                sb.append(
                    ResourceUtil.getLocalizedMessage(
                        "alert.email.alertDetailText", new Object[] {
                            identityValue, alertFieldDisplayName,
                            alertFieldValue.toString(), uom }, "")).append(
                    "\t\n");

                sb.append("\t\n");
            }
        }

        sb.append(blockBoundary);
        sb.append(gapSeparation);
        sb.append(blockSeparation);

        sb.append(
            ResourceUtil.getLocalizedKeyValue("alert.email.disclamerText"))
            .append("\t\n");

        // Set subject line
        msg.setSubject(ResourceUtil
            .getLocalizedMessage(
                "alert.email.subject",
                new Object[] {
                    alert.getDescriptiveText(), alertedRecords.length() }, ""),
            "UTF-8");

        // Set body content
        Multipart mp = new MimeMultipart();

        MimeBodyPart mbp1 = new MimeBodyPart();
        mbp1.setHeader("Content-Type", "text/plain");
        mbp1.setText(sb.toString(), "UTF-8");
        mp.addBodyPart(mbp1);

        msg.setContent(mp);
        msg.setSentDate(new Date());

        // Finally send the mail
        Transport.send(msg);

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 