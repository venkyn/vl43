/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event.listeners;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.model.AlertStatus;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.event.DAUpdateEvent;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAEventType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Date;
import java.util.List;

import org.springframework.context.ApplicationListener;

/**
 * @author smittal
 * 
 */
public class AlertDAEventListenerRoot implements
    ApplicationListener<DAUpdateEvent> {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(AlertDAEventListenerRoot.class);

    private NotificationManager notificationManager;

    private AlertManager alertManager;

    private SiteContext siteContext;

    @Override
    public void onApplicationEvent(DAUpdateEvent event) {
        getSiteContext().setToSystemMode();
        SiteContextHolder.setSiteContext(getSiteContext());
        
        log.info("Event received : " + event.getSource());
        try {
            if (((DAEventType) event.getSource()) == DAEventType.UPDATED) {
                handleAlertDaInformationUpdate(event.getNewDAInformation());
            } else {
                handleAlertDaInformationDelete(event.getNewDAInformation());
            }
        } catch (DataAccessException e) {
            e.printStackTrace();
        } catch (BusinessRuleException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the alertManager property.
     * @return AlertManager value of the property
     */
    public AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * Setter for the alertManager property.
     * @param alertManager the new alertManager value
     */
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    /**
     * 
     * @param daInformationMarkedForDelete aggregator which is marked for delete
     * @throws DataAccessException dae
     * @throws BusinessRuleException bre
     */
    private void handleAlertDaInformationDelete(DAInformation daInformationMarkedForDelete)
        throws DataAccessException, BusinessRuleException {

        List<Alert> daMarkedForDeleteAffectedAlerts = this.alertManager
            .getAll();

        for (Alert alert : daMarkedForDeleteAffectedAlerts) {
            if (alert.getDaInformation().getId()
                .equals(daInformationMarkedForDelete.getId())) {
                log.debug("Deleting alert : " + alert.getAlertName());
                executeDADeleteAlertDelete(alert);
            }
        }
    }

    /**
     * 
     * @param alert alert using the aggregator marked for delete
     * @throws BusinessRuleException bre
     * @throws DataAccessException dae
     */
    private void executeDADeleteAlertDelete(Alert alert)
        throws BusinessRuleException, DataAccessException {

        String actionKey = "notification.deleteAggregator.alert.delete";
        String fieldName = null;
        String alertName = alert.getAlertName();
        String daName = alert.getDaInformation().getDaInformationDisplayName();
        String processKey = "notification.deleteAggregator.title";
        String messageKey = "notification.deleteAggregator.notification";

        this.alertManager.delete(alert);

        createNotification(alertName, fieldName, daName, actionKey, processKey,
            messageKey, NotificationPriority.CRITICAL);
    }

    /**
     * 
     * @param newDAInformation updated daInformation
     * @throws DataAccessException data access exception
     * @throws BusinessRuleException bre
     */
    private void handleAlertDaInformationUpdate(DAInformation newDAInformation)
        throws DataAccessException, BusinessRuleException {

        List<Alert> daUpdateAffectedAlerts = this.alertManager.getAll();

        for (Alert alert : daUpdateAffectedAlerts) {
            if (alert.getDaInformation().getId()
                .equals(newDAInformation.getId())) {
                executeDAUpdateAlertUpdateDelete(alert);
            }
        }

    }

    /**
     * @param alert the alert which is getting affected
     * @throws BusinessRuleException if we could not delete the alert
     * @throws DataAccessException if we could not delete the alert
     */
    private void executeDAUpdateAlertUpdateDelete(Alert alert)
        throws BusinessRuleException, DataAccessException {
        List<AlertCriteria> alertCriterias = alert.getAlertCriterias();

        String actionKey = null;
        String fieldName = null;
        String alertName = alert.getAlertName();
        String daName = alert.getDaInformation().getDaInformationDisplayName();
        String processKey = "notification.updateAggregator.title";
        String messageKey = "notification.updateAggregator.notification";

        for (AlertCriteria alertCriteria : alertCriterias) {
            if (alertCriteria.getDaColumn().getColumnState() == DAColumnState.MARKEDFORDELETE) {
                actionKey = "notification.updateAggregator.alert.delete";
                fieldName = alertCriteria.getDaColumn()
                    .getDaColumnDisplayName();
                this.alertManager.delete(alert);
                createNotification(alertName, fieldName, daName, actionKey,
                    processKey, messageKey, NotificationPriority.CRITICAL);
                break;
            } else if ((alertCriteria.getDaColumn().getColumnState() == DAColumnState.FIELDTYPEUPDATED)) {
                alert.setAlertStatus(AlertStatus.DISABLED);
                this.alertManager.save(alert);
                actionKey = "notification.alert.disbaled.field.type";
                fieldName = alertCriteria.getDaColumn()
                    .getDaColumnDisplayName();
                createNotification(alertName, fieldName, daName, actionKey,
                    processKey, messageKey, NotificationPriority.CRITICAL);
            }
        }
    }

    /**
     * Method to create a Notification for the Alert.
     * @param alertName Alert Name
     * @param fieldName Field Name
     * @param daName DaName
     * @param actionKey action key
     * @param processKey process key
     * @param messageKey message key
     * @param priority notification priority
     * 
     */
    private void createNotification(String alertName,
                                    String fieldName,
                                    String daName,
                                    String actionKey,
                                    String processKey,
                                    String messageKey,
                                    NotificationPriority priority) {

        LOPArrayList lop = new LOPArrayList();

        if (fieldName == null) {
            lop.add("notification.da.deleted",
                daName);
        } else {
            lop.add("notification.da.updated",
                daName);
        }
        lop.add("notification.dataAggregator.alertName", alertName);
        lop.add("notification.dataAggregator.alert.action", actionKey);
        if (fieldName != null) {
            lop.add("notification.da.field.name", fieldName);
        }
        try {
            getNotificationManager().createNotification(processKey, messageKey,
                priority, new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getLocalizedMessage());
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 