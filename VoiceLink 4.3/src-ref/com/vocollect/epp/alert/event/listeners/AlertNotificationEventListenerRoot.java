/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event.listeners;

import com.vocollect.epp.alert.event.AlertNotificationEvent;
import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.context.ApplicationListener;

/**
 * This listener mainly listens to an Alert Event to be generated. And once an
 * AlertEvent got generated, it creates an Alert Notification and sends an Email
 * if Alert is configured to send an email.
 * 
 * @author kudupi
 */
public class AlertNotificationEventListenerRoot implements
    ApplicationListener<AlertNotificationEvent> {

    // Instantiate a logger for this class.
    private static final Logger log = new Logger(
        AlertNotificationEventListenerRoot.class);

    private NotificationManager notificationManager;

    private DataAggregatorHandler dataAggregatorHandler;

    private SiteContext siteContext;

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the dataAggregatorHandler property.
     * @return DataAggregatorHandler value of the property
     */
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * Setter for the dataAggregatorHandler property.
     * @param dataAggregatorHandler the new dataAggregatorHandler value
     */
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }

    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }

    @Override
    public void onApplicationEvent(AlertNotificationEvent alertNotificationEvent) {

        try {
            createNotification(alertNotificationEvent.getAlert(),
                alertNotificationEvent.getAlertRecords());
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (DataAccessException dae) {
            dae.printStackTrace();
        }
    }

    /**
     * Method to create a Notification for the Alert.
     * @param alert Alert
     * @param alertedRecords JSONArray
     * @throws DataAccessException .
     * @throws JSONException .
     */
    private void createNotification(Alert alert, JSONArray alertedRecords)
        throws DataAccessException, JSONException {
        Long currentSiteID = alert.getTags().iterator().next().getTaggableId();
        if (currentSiteID != null) {
            getSiteContext().setCurrentSite(currentSiteID);
            SiteContextHolder.setSiteContext(getSiteContext());
        } else {
            throw new DataAccessException(
                "Site Information could not be retrieved for Alert = "
                    + alert.getAlertName());
        }

        LOPArrayList lop = new LOPArrayList();

        lop.add("notification.alertJob.alertName", alert.getAlertName());

        StringBuilder criteriaString = new StringBuilder();

        for (AlertCriteria alertCriteria : alert.getAlertCriterias()) {
            criteriaString.append(
                ResourceUtil.getLocalizedKeyValue(alertCriteria.getRelation()
                    .getResourceKey())).append(" ");
            criteriaString.append(
                ResourceUtil.getLocalizedKeyValue(alertCriteria.getDaColumn()
                    .getDisplayName())).append(" ");
            criteriaString.append(
                ResourceUtil.getLocalizedKeyValue(alertCriteria
                    .getOperandType().getResourceKey())).append(" ");
            criteriaString
                .append(
                    ResourceUtil.getLocalizedKeyValue(alertCriteria
                        .getThreshold())).append(" ");
            criteriaString.append(
                ResourceUtil.getLocalizedKeyValue(alertCriteria.getDaColumn()
                    .getUom())).append(" ");
        }
        lop.add("notification.alertJob.criteria", criteriaString);

        lop.add("notification.alertJob.entries", alertedRecords.length());

        try {
            getNotificationManager().createNotification(
                "notification.alertJob.title",
                "notification.alertJob.notification",
                alert.getAlertNotificationPriority(), new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception e) {
            e.printStackTrace();
            log.warn(e.getLocalizedMessage());
        }

    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 