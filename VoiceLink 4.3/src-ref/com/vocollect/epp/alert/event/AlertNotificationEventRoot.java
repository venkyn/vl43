/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event;

import com.vocollect.epp.alert.model.Alert;

import org.json.JSONArray;
import org.springframework.context.ApplicationEvent;


/**
 * This class is responsible for capturing the Event when
 * an alertable situation arises.
 *
 * @author kudupi
 */
public abstract class AlertNotificationEventRoot extends ApplicationEvent {

    //
    private static final long serialVersionUID = 6176470037837344350L;
    
    private Alert alert;
    
    private JSONArray alertRecords;

    /**
     * Constructor.
     */
    public AlertNotificationEventRoot() {
        super("alertNotificationEvent");
    }
    
    /**
     * Getter for the alert property.
     * @return Alert value of the property
     */
    public Alert getAlert() {
        return alert;
    }

    
    /**
     * Setter for the alert property.
     * @param alert the new alert value
     */
    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    
    /**
     * Getter for the alertRecords property.
     * @return JSONArray value of the property
     */
    public JSONArray getAlertRecords() {
        return alertRecords;
    }

    
    /**
     * Setter for the alertRecords property.
     * @param alertRecords the new alertRecords value
     */
    public void setAlertRecords(JSONArray alertRecords) {
        this.alertRecords = alertRecords;
    }    
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 