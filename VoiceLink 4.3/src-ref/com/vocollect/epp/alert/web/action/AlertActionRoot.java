/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.web.action;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.model.AlertCriteriaRelation;
import com.vocollect.epp.alert.model.AlertOperandType;
import com.vocollect.epp.alert.model.AlertReNotificationFrequency;
import com.vocollect.epp.alert.model.AlertStatus;
import com.vocollect.epp.alert.service.AlertCriteriaManager;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.exceptions.OptimisticLockingFailureException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.DataProvider;
import com.vocollect.epp.util.AlertUtil;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.web.action.DataProviderAction;
import com.vocollect.epp.web.util.DisplayUtilities;

import com.opensymphony.xwork2.Preparable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Level;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the Struts action class that handles operations on <code>Alert</code>
 * objects.
 * 
 * @author kudupi
 */
public class AlertActionRoot extends DataProviderAction implements Preparable {

    private static final long serialVersionUID = 5247932223098960094L;

    private static final Logger log = new Logger(AlertActionRoot.class);

    private static final long ALERT_VIEW_ID = -2000;

    private static final long ALERT_CRITERIA_VIEW_ID = -2001;

    // The list of columns for the alert table
    private List<Column> alertColumns;

    private List<Column> alertCriteriaColumns;

    private AlertManager alertManager;

    private AlertCriteriaManager alertCriteriaManager;

    private Alert alert;

    private Long alertId;

    private Long daInformationID;
    
    private Long dashboardId;

    private Map<Integer, String> notificationPriorityMap;

    private DataAggregatorHandler dataAggregatorHandler;

    private DAInformationManager daInformationManager;
    
    private DashboardManager dashboardManager;

    private String criteria;
    
    private String criteriaErrors = "{}";

    private String alertFrequency = "";
    
    private TreeMap<String, Long> daInformationMap;

    // Comma seperated list of alert IDs, which are to be queried
    private String alertIdValueForCriteria;

    private boolean showAlertCriteria = false;

    /**
     * Getter for the alertIdValueForCriteria property.
     * @return String value of the property
     */
    public String getAlertIdValueForCriteria() {
        return alertIdValueForCriteria;
    }

    /**
     * Setter for the alertIdValueForCriteria property.
     * @param alertIdValueForCriteria the new alertIdValueForCriteria value
     */
    public void setAlertIdValueForCriteria(String alertIdValueForCriteria) {
        this.alertIdValueForCriteria = alertIdValueForCriteria;
    }

    /**
     * Getter for the criteria property.
     * @return String value of the property
     */
    public String getCriteria() {
        return criteria;
    }
    

    
    /**
     * Getter for the criteriaErrors property.
     * @return String value of the property
     */
    public String getCriteriaErrors() {
        return criteriaErrors;
    }

    /**
     * Setter for the criteria property.
     * @param criteria the new criteria value
     */
    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    @Override
    public void prepare() throws Exception {
        log.setLevel(Level.DEBUG);

        log.debug("Inside prepare method");
        log.debug("alertId = " + getAlertId());
        if (getAlertId() != null) {
            if (log.isDebugEnabled()) {
                log.debug("alertId is " + getAlertId());
            }
            if (!StringUtil.isNullOrEmpty(getSavedEntityKey())) {
                // This means the alert is being edited, so
                // get it from the session.
                if (log.isDebugEnabled()) {
                    log.debug("Getting saved alert from session");
                }

                this.alert = (Alert) getEntityFromSession(getSavedEntityKey());
            } else {
                if (log.isDebugEnabled()) {
                    log.debug("Getting alert from database");
                }
                this.alert = this.alertManager.get(getAlertId());
                saveEntityInSession(this.alert);
            }

            // Set criteria, data aggregator information and alert frequence
            // after we retrieved the alert when editing
            this.criteria = AlertUtil.alertCriteriaToJSON(this.alert)
                .toString();
            this.setDaInformationID(this.alert.getDaInformation().getId());
            
            if (this.alert.getDashboard() != null) {
                this.setDashboardId(this.alert.getDashboard().getId());
            }
            
            this.alertFrequency = getRenotificationFrequencyStr();

            if (log.isDebugEnabled()) {
                log.debug("Alert version is: " + this.alert.getVersion());
            }
        } else if (this.alert == null) {
            if (log.isDebugEnabled()) {
                log.debug("Created new Alert object");
            }
            this.alert = new Alert();
        }

    }

    /**
     * @return SUCCESS or INPUT
     * @throws Exception When save fails
     */
    public String save() throws Exception {
        boolean isNew = this.alert.isNew();

        try {
            // Before saving the alert, checking if already an alert exists with
            // the same name.
            this.getAlertManager().verifyUniquenessByAlertName(this.alert);
            DAInformation daInformation = daInformationManager
                .get(getDaInformationID());
            this.alert.setDaInformation(daInformation);
            
            if (this.getDashboardId() != null) {
                Dashboard dashboard = this.getDashboardManager().get(
                    this.getDashboardId());
                this.alert.setDashboard(dashboard);
            } else {
                this.alert.setDashboard(null);
            }
            
            
            HttpServletRequest request = getRequest();
            String serverURL = request.getScheme() + "://"
                + request.getServerName() + ":" + request.getLocalPort()
                + request.getContextPath();
            
            this.alert.setServerURL(serverURL);
            
            List<AlertCriteria> alertCriteriaList = populateAlertCriteria(daInformation);
            
            if (!verifyAlertCriteria(alertCriteriaList)) {
                return INPUT;
            }
            
            verifyReNotificationFrequency();
            
            if (isNew) {
                this.alert.setAlertCriterias(alertCriteriaList);
            } else {
                this.alert.getAlertCriterias().clear();
                if (this.alert.getAlertCriterias().isEmpty()) {
                    this.alert.getAlertCriterias().addAll(alertCriteriaList);
                }
            }
            
            this.alert.setReNotificationFrequency(getReNotificationFrequency());
            
            if ((getAlertManager().getPrimaryDAO().countById(
                this.alert.getId()) == 0) && (!isNew)) {
                this.alert.getAlertCriterias().clear();
            }
            
            this.getAlertManager().save(this.alert);
            cleanSession();
        } catch (BusinessRuleException bre) {
            if (bre instanceof FieldValidationException) {
                FieldValidationException fve = (FieldValidationException) bre;
                log.warn(fve.getField() + " " + fve.getValue()
                    + " already exists");
                addFieldError(fve.getField(), fve.getUserMessage());
                // Go back to the form to show the error message.
                return INPUT;
            }
        } catch (OptimisticLockingFailureException e) {
            // } catch (Exception e) {
            log.warn("Attempt to change already modified alert "
                + this.alert.getAlertName());
            addActionError(new UserMessage("entity.error.modified",
                UserMessage.markForLocalization("entity.alert"),
                SHOW_MODIFIED_ENTITY_DIV_JAVASCRIPT));
            // Get the modified data
            // If user has been deleted, this will throw EntityNotFoundException
            try {
                Alert modifiedAlert = getAlertManager().get(getAlertId());
                // Set the local object's version to match, so it will work
                // if the user resubmits.
                this.alert.setVersion(modifiedAlert.getVersion());
                this.alert.setAlertCriterias(modifiedAlert.getAlertCriterias());
                // Store the modified data for display
                setModifiedEntity(modifiedAlert);
            } catch (EntityNotFoundException ex) {
                addSessionActionMessage(new UserMessage("entity.error.deleted",
                    UserMessage.markForLocalization("entity.alert"), null));
                return SUCCESS;
            }
            return INPUT;
        }

        addSessionActionMessage(new UserMessage("alert."
            + (isNew ? "create" : "edit") + ".message.success",
            makeGenericContextURL("/alerts/view.action?alertId="
                + this.alert.getId()), this.alert.getAlertName()));

        return SUCCESS;
    }

    /**
     * 
     * @return list of modified alert criterias
     */
    public List<String> getModifiedCriterias() {        
        List<String> modifiedCriterias = new ArrayList<String>();
        List <AlertCriteria> modifiedAlertCriterias = ((Alert) getModifiedEntity()).getAlertCriterias();
        AlertUtil.sortCriteria(modifiedAlertCriterias);
        for (AlertCriteria modifiedCriteria : modifiedAlertCriterias) {
            modifiedCriterias.add(ResourceUtil
                .getLocalizedKeyValue(modifiedCriteria.getRelation()
                    .getResourceKey())
                + " "
                + modifiedCriteria.getDaColumn().getDaColumnDisplayName()
                + " "
                + ResourceUtil.getLocalizedKeyValue(modifiedCriteria
                    .getOperandType().getResourceKey())
                + modifiedCriteria.getThreshold()
                + ResourceUtil.getLocalizedKeyValue(modifiedCriteria
                    .getDaColumn().getUom()));
        }
        
        return modifiedCriterias;
    }
    
    /**
     * 
     * @return localised value of modified alert status
     */
    public String getModifiedAlertStatus() {
        return ResourceUtil.getLocalizedEnumName(((Alert) getModifiedEntity()).getAlertStatus());
    }
    
    /**
     * 
     * @return localised value of modified alert notification priority
     */
    public String getModifiedAlertNotificationPriority() {
        return ResourceUtil.getLocalizedEnumName(((Alert) getModifiedEntity()).getAlertNotificationPriority());
    }

    /**
     * @throws FieldValidationException 
     * 
     */
    private void verifyReNotificationFrequency() throws FieldValidationException {
        if (this.alert != null
            && this.alert.getAlertStatus() == AlertStatus.ENABLED) {
            
            String frequency = this.alertFrequency;
            
            if (StringUtils.isEmpty(frequency)
                || !frequency.matches("^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$") || frequency.matches("^00:00$")) {
                throw new FieldValidationException("alertFrequency", "",
                    new UserMessage("alert.frequency.error.time.invalid", ""));
            }
        }
    }

    /**
     * Validated the alert criteria.
     * @param alertCriteriaList the criteria list to validate
     * @return True if verification passed, or false
     */
    private boolean verifyAlertCriteria(List<AlertCriteria> alertCriteriaList) {
        boolean verificationPassed = true;
        JSONArray messagesJSON = new JSONArray();

        for (AlertCriteria alertCriteria : alertCriteriaList) {

            String[] fieldDisplayName = new String[] { ResourceUtil
                .getLocalizedKeyValue(alertCriteria.getDaColumn()
                    .getDisplayName()) };

            if (this.alert.getAlertStatus() == AlertStatus.ENABLED) {

                // Checks if string is empty
                if (StringUtil.isNullOrEmpty(alertCriteria.getThreshold())) {
                    JSONObject messageJSON = new JSONObject();
                    try {
                        messageJSON.put("message", ResourceUtil
                            .getLocalizedMessage("errors.alertCriteria.empty",
                                fieldDisplayName, " cannot be empty"));
                    } catch (JSONException e) {
                    }
                    messagesJSON.put(messageJSON);
                    verificationPassed = false;
                }
            }

            // Checks if string is more than 50 characters
            if (!StringUtil.isNullOrEmpty(alertCriteria.getThreshold())
                && alertCriteria.getThreshold().length() > 50) {

                JSONObject messageJSON = new JSONObject();
                try {
                    messageJSON.put("message", ResourceUtil
                        .getLocalizedMessage("errors.alertCriteria.maxLength",
                            fieldDisplayName, " more than 50 character"));
                } catch (JSONException e) {
                }
                messagesJSON.put(messageJSON);
                verificationPassed = false;
            }

            // Checks if string is a number
            if (!StringUtil.isNullOrEmpty(alertCriteria.getThreshold())
                && (alertCriteria.getDaColumn().getFieldType() == DAFieldType.INTEGER || alertCriteria
                    .getDaColumn().getFieldType() == DAFieldType.FLOAT)) {
                if (!NumberUtils.isNumber(alertCriteria.getThreshold())) {

                    JSONObject messageJSON = new JSONObject();
                    try {
                        messageJSON.put("message", ResourceUtil
                            .getLocalizedMessage(
                                "errors.alertCriteria.numeric",
                                fieldDisplayName, " is not numeric"));
                    } catch (JSONException e) {
                    }
                    messagesJSON.put(messageJSON);
                    verificationPassed = false;
                }
            }

            // Checks if string is a valid time string
            if (!StringUtil.isNullOrEmpty(alertCriteria.getThreshold())
                && alertCriteria.getDaColumn().getFieldType() == DAFieldType.TIME) {
                if (!alertCriteria.getThreshold().matches(
                    "^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$")) {

                    JSONObject messageJSON = new JSONObject();
                    try {
                        messageJSON.put("message", ResourceUtil
                            .getLocalizedMessage("errors.alertCriteria.time",
                                fieldDisplayName, " is not valid time"));
                    } catch (JSONException e) {
                    }
                    messagesJSON.put(messageJSON);
                    verificationPassed = false;
                }
            }
            
            // Checks if string is a valid date string
            if (!StringUtil.isNullOrEmpty(alertCriteria.getThreshold())
                && alertCriteria.getDaColumn().getFieldType() == DAFieldType.DATE) {
                Date date = DateUtil.getDateWithLenient(
                    alertCriteria.getThreshold(),
                    GenericDataAggregator.DATE_FORMAT, false);
                if (date == null) {
                    JSONObject messageJSON = new JSONObject();
                    String[] params =  new String[] {ResourceUtil
                        .getLocalizedKeyValue(alertCriteria.getDaColumn()
                            .getDisplayName()) , GenericDataAggregator.DATE_FORMAT.toLowerCase()};
                    try {
                        messageJSON.put("message", ResourceUtil
                            .getLocalizedMessage("errors.alertCriteria.date",
                                params , " is not valid date"));
                    } catch (JSONException e) {
                    }
                    
                    messagesJSON.put(messageJSON);
                    verificationPassed = false;
                    
                }
            }
        }

        JSONObject errorTexts = new JSONObject();
        try {
            errorTexts.put("errorTexts", messagesJSON);
        } catch (JSONException e) {
        }
        criteriaErrors = errorTexts.toString();

        return verificationPassed;
    }

    /**
     * Method to populate Alert Criteria list for Alert object.
     * @param daInformation DAInformation.
     * @return alertCriteriaList List<AlertCriteria>.
     * @throws DataAccessException dataAccessException.
     * @throws JSONException jsonException.
     */
    private List<AlertCriteria> populateAlertCriteria(DAInformation daInformation)
        throws DataAccessException, JSONException {
        Map<String, DAColumn> daColumnMap = new HashMap<String, DAColumn>();

        Set<DAColumn> daColumns = daInformation.getColumns();
        for (DAColumn daColumn : daColumns) {
            daColumnMap.put(daColumn.getFieldId(), daColumn);
        }

        List<AlertCriteria> alertCriteriaList = new ArrayList<AlertCriteria>();
        JSONArray criteriaJSONArray = new JSONArray(getCriteria());
        for (int i = 0; i < criteriaJSONArray.length(); i++) {
            JSONObject criteriaJSONObj = new JSONObject(criteriaJSONArray
                .get(i).toString());
            AlertCriteria alertCriteria = new AlertCriteria();
            alertCriteria.setAlert(this.alert);

            alertCriteria.setOperandType(AlertOperandType
                .toEnum(criteriaJSONObj.getInt(AlertUtil.OPERANDTYPE)));

            alertCriteria.setThreshold(criteriaJSONObj
                .getString(AlertUtil.THRESHOLD));
            alertCriteria.setDaColumn(daColumnMap.get(criteriaJSONObj
                .get(GenericDataAggregator.FIELD_ID)));
            alertCriteria.setRelation(AlertCriteriaRelation
                .toEnum(criteriaJSONObj.getInt(AlertUtil.RELATION)));
            alertCriteria.setSequence(criteriaJSONObj
                .getInt(AlertUtil.SEQUENCE));
            alertCriteriaList.add(alertCriteria);
        }

        return alertCriteriaList;
    }

    /**
     * Deletes the currently viewed Alert.
     * @return the control flow target name.
     * @throws Exception Throws Exception if error occurs deleting current alert
     */
    public String deleteCurrentAlert() throws Exception {

        Alert alertToDelete = null;

        try {
            alertToDelete = alertManager.get(getAlertId());
            alertManager.delete(getAlertId());

            addSessionActionMessage(new UserMessage(
                "alert.delete.message.success", alertToDelete.getAlertName()));
        } catch (BusinessRuleException e) {
            log.warn("Unable to delete Alert: " + getText(e.getUserMessage()));
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (EntityNotFoundException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        } catch (DataAccessException e) {
            addSessionActionErrorMessage(e.getUserMessage());
            return SUCCESS;
        }
        return SUCCESS;
    }

    /**
     * Method to return data for Alert Criteria TC.
     * @return message
     * @throws Exception any exception
     */
    public String getAlertCriteriaTableData() throws Exception {
        if (getAlertIdValueForCriteria() != null
            && getAlertIdValueForCriteria().length() > 0) {
            showAlertCriteria = true;
            super.getTableData("obj.alert.id in ( "
                + this.alertIdValueForCriteria + " )");
        } else {
            showAlertCriteria = false;
            setMessage(BLANK_RESPONSE);
        }
        return SUCCESS;

    }

    @Override
    protected DataProvider getManager() {
        if (showAlertCriteria) {
            return this.getAlertCriteriaManager();
        } else {
            return this.getAlertManager();
        }

    }

    @Override
    protected String getKeyPrefix() {
        return "alert";
    }

    /**
     * Getter for the alertId property.
     * @return Long value of the property
     */
    public Long getAlertId() {
        return alertId;
    }

    /**
     * Setter for the alertId property.
     * @param alertId the new alertId value
     */
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    /**
     * Getter for the daInformationID property.
     * @return Long value of the property
     */
    public Long getDaInformationID() {
        return daInformationID;
    }

    /**
     * Setter for the daInformationID property.
     * @param daInformationID the new daInformationID value
     */
    public void setDaInformationID(Long daInformationID) {
        this.daInformationID = daInformationID;
    }

    
    /**
     * Getter for the dashboardId property.
     * @return Long value of the property
     */
    public Long getDashboardId() {
        return dashboardId;
    }

    
    /**
     * Setter for the dashboardId property.
     * @param dashboardId the new dashboardId value
     */
    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    /**
     * Getter for the alertViewId property.
     * @return long value of the property
     */
    public static long getAlertViewId() {
        return ALERT_VIEW_ID;
    }

    /**
     * Getter for the alertCriteriaViewId property.
     * @return long value of the property
     */
    public static long getAlertCriteriaViewId() {
        return ALERT_CRITERIA_VIEW_ID;
    }

    /**
     * Getter for the columns property.
     * 
     * @return List of Column value of the property.
     */
    public List<Column> getAlertColumns() {
        return this.alertColumns;
    }

    /**
     * Getter for the alertCriteriaColumns property.
     * @return List<Column> value of the property
     */
    public List<Column> getAlertCriteriaColumns() {
        return alertCriteriaColumns;
    }

    /**
     * Setter for the alertCriteriaColumns property.
     * @param alertCriteriaColumns the new alertCriteriaColumns value
     */
    public void setAlertCriteriaColumns(List<Column> alertCriteriaColumns) {
        this.alertCriteriaColumns = alertCriteriaColumns;
    }

    @Override
    public List<Long> getViewIds() {
        List<Long> viewIds = new LinkedList<Long>();
        viewIds.add(ALERT_VIEW_ID);
        viewIds.add(ALERT_CRITERIA_VIEW_ID);
        return viewIds;
    }

    /**
     * Getter for the alertManager property.
     * @return AlertManager value of the property
     */
    public AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * Setter for the alertManager property.
     * @param alertManager the new alertManager value
     */
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * Getter for the alertCriteriaManager property.
     * @return AlertCriteriaManager value of the property
     */
    public AlertCriteriaManager getAlertCriteriaManager() {
        return alertCriteriaManager;
    }

    /**
     * Setter for the alertCriteriaManager property.
     * @param alertCriteriaManager the new alertCriteriaManager value
     */
    public void setAlertCriteriaManager(AlertCriteriaManager alertCriteriaManager) {
        this.alertCriteriaManager = alertCriteriaManager;
    }

    /**
     * Getter for the alert property.
     * @return Alert value of the property
     */
    public Alert getAlert() {
        return alert;
    }

    /**
     * Setter for the alert property.
     * @param alert the new alert value
     */
    public void setAlert(Alert alert) {
        this.alert = alert;
    }

    /**
     * Action for the alert view page. Initializes the site table columns
     * 
     * @return String value of outcome.
     * @throws Exception in case of DataAccess issues
     */
    public String list() throws Exception {
        View alertView = getUserPreferencesManager().getView(ALERT_VIEW_ID);
        this.alertColumns = this.getUserPreferencesManager().getColumns(
            alertView, getCurrentUser());
        View alertCriteriaView = getUserPreferencesManager().getView(
            ALERT_CRITERIA_VIEW_ID);
        this.alertCriteriaColumns = this.getUserPreferencesManager()
            .getColumns(alertCriteriaView, getCurrentUser());
        return SUCCESS;
    }

    /**
     * Getter for Alert Frequency.
     * @return enum value Integer.
     */
    public String getAlertFrequency() {

        return this.alertFrequency;
    }

    /**
     * Setter for Alert Notification Type.
     * @param value Integer.
     */
    public void setAlertFrequency(String value) {
        this.alertFrequency = value;
    }

    /**
     * method to get the re notification freq object to save from the user
     * submitted value.
     * @return AlertReNotificationFrequency object
     */
    private AlertReNotificationFrequency getReNotificationFrequency() {
        if (StringUtil.isNullOrEmpty(this.alertFrequency)) {
            return null;
        }

        String[] hm = this.alertFrequency.split(":");
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(Integer.valueOf(hm[0]));
        freq.setMinutes(Integer.valueOf(hm[1]));
        return freq;
    }

    /**
     * Method to get the displayble value of freq field.
     * @return re notification freq string.
     */
    private String getRenotificationFrequencyStr() {
        if (this.alert.getReNotificationFrequency() != null) {
            return this.alert.getReNotificationFrequency().getDescriptiveText();
        }

        return null;
    }

    /**
     * Returns a list of notification priorities.
     * 
     * @return list of notification priorities
     */
    public Map<Integer, String> getNotificationPriorityMap() {
        this.notificationPriorityMap = new LinkedHashMap<Integer, String>(
            NotificationPriority.values().length);

        for (NotificationPriority notitificationPriority : NotificationPriority
            .values()) {
            notificationPriorityMap.put(notitificationPriority.getValue(),
                ResourceUtil.getLocalizedEnumName(notitificationPriority));
        }

        return this.notificationPriorityMap;
    }

    /**
     * Getter for Alert Notification Priority.
     * @return enum value Integer.
     */
    public Integer getNotificationPriority() {
        return this.alert.getAlertNotificationPriority().toValue();
    }

    /**
     * Setter for Alert Notification Priority.
     * @param value Integer.
     */
    public void setNotificationPriority(Integer value) {
        this.alert.setAlertNotificationPriority(NotificationPriority
            .toEnum(value));
    }

    /**
     * @return the dataAggregatorHandler
     */
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * @return the daInformationManager
     */
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * @param daInformationManager the daInformationManager to set
     */
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }
    
    
    /**
     * Getter for the dashboardManager property.
     * @return DashboardManager value of the property
     */
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    
    /**
     * Setter for the dashboardManager property.
     * @param dashboardManager the new dashboardManager value
     */
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * Getter method for Data Aggregation Information.
     * @return Map of Data Aggregation Information objects ID and String
     * @throws DataAccessException .
     */
    public Map<String, Long> getAlertDAInformationMap()
        throws DataAccessException {

        List<DAInformation> daInformation = this.getDaInformationManager()
            .getAll();

        daInformationMap = new TreeMap<String, Long>(StringUtil.getPrimaryCollator());
        
        for (DAInformation daInfo : daInformation) {
            if (!daInfo.isMarkedForDelete()) {
                daInformationMap.put(ResourceUtil.getLocalizedKeyValue(daInfo.getDisplayName()), daInfo.getId());
            }
        }

        return daInformationMap;
    }

    /**
     * @return the map of dashboard
     * @throws DataAccessException when problem accessing dashboard data
     */
    public Map<String, Long> getDashboardMap() throws DataAccessException {
        Map<String, Long> dashboardMap = new TreeMap<String, Long>();
        
        List<Dashboard> dashboards = this.getDashboardManager().listDashboards();
        
        for (Dashboard dashboard : dashboards) {
            dashboardMap.put(dashboard.getName(), dashboard.getId());
        }
        
        return dashboardMap;
    }
    /**
     * @return Success or failure after setting criteria json in message.
     */
    public String getCriteriaPropertiesActionSupport() {
        setMessage(getCriteriaProperties());
        return SUCCESS;
    }
    
    /**
     * Method to get JSON of DA fields.
     * @return JSON
     */
    public String getCriteriaProperties() {
        try {
            DAInformation da = null;
            JSONArray criteriaObject = null;

            // If we have alert id, that means we are editing, else it is a new
            // page
            if (this.getAlertId() == null) {
                if (getDaInformationID() == null) {
                    Set<String> daIDKeys = (Set<String>) this.daInformationMap.keySet();
                    Iterator<String> keyItr = daIDKeys.iterator();
                    if (keyItr.hasNext()) {
                        da = this.getDaInformationManager().get(daInformationMap.get(keyItr.next()));
                    }
                } else {
                    da = this.daInformationManager.get(getDaInformationID());
                    if (this.criteria != null) {
                        criteriaObject = new JSONArray(this.criteria);
                    }
                }
            } else {
                da = this.alert.getDaInformation();
                criteriaObject = new JSONArray(this.criteria);
            }

            JSONArray propertiesObject = AlertUtil.daPropertyToJSON(da);

            JSONObject jsonArray = new JSONObject(); // root level array object
            jsonArray.put(AlertUtil.PROPERTIES, propertiesObject);
            jsonArray.put(AlertUtil.CRITERIAS, criteriaObject);
            return jsonArray.toString();
        } catch (Exception e) {
            log.warn("Excption occured preparing alert on criteria : "
                + e.getLocalizedMessage());
        }
        
        return "";
    }

    /**
     * Get the possible AlertStatus for display.
     * @return the Map of defined AlertStatus (1-Enabled/0-Disabled)
     */
    public Map<Integer, String> getAlertStatusMap() {
        Map<Integer, String> alertStatusMap = new LinkedHashMap<Integer, String>();
        for (AlertStatus alertStatus : AlertStatus.values()) {
            alertStatusMap.put(alertStatus.toValue(),
                ResourceUtil.getLocalizedEnumName(alertStatus));
        }
        return alertStatusMap;
    }

    /**
     * Getter for Alert Status.
     * @return enum value Integer.
     */
    public Integer getAlertStatus() {
        return this.alert.getAlertStatus().toValue();
    }

    /**
     * Setter for Alert Status.
     * @param value Integer.
     */
    public void setAlertStatus(Integer value) {
        this.alert.setAlertStatus(AlertStatus.toEnum(value));
    }

    /**
     * Getter for the lastExecutionTime, which will return the properly
     * formatted time.
     * @return String value of the property
     */

    public String getLastAlertEventTime() {
        String lastAlertEventTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            lastAlertEventTime = du.formatTimeWithTimeZone(
                this.alert.getLastAlertEventTime(), null);
        } catch (DataAccessException e) {
            log.warn(
                "error occurred on site retrieval. falling back to JVM default timezone.",
                e);
            lastAlertEventTime = this.alert.getLastAlertEventTime()
                .toString();
        }

        return lastAlertEventTime;
    }

    /**
     * Getter for the lastExecutionTime, which will return the properly
     * formatted time.
     * @return String value of the property
     */

    public String getLastEvaluationTime() {
        String lastEvaluationTime = null;
        DisplayUtilities du = getDisplayUtilities();

        try {
            lastEvaluationTime = du.formatTimeWithTimeZone(
                this.alert.getLastEvaluationTime(), null);
        } catch (DataAccessException e) {
            log.warn(
                "error occurred on site retrieval. falling back to JVM default timezone.",
                e);
            lastEvaluationTime = this.alert.getLastEvaluationTime().toString();
        }

        return lastEvaluationTime;
    }

    /**
     * This method is used for view action of Alert.
     * @return Data Aggregator display name.
     */
    public String getAlertOnValue() {
        return ResourceUtil.getLocalizedKeyValue(this.alert.getDaInformation()
            .getDisplayName());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 