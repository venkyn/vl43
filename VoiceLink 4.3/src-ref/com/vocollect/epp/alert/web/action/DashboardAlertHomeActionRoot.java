/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.web.action;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.model.User;
import com.vocollect.epp.web.action.BaseAction;

/**
 * Action associated with the Dashboards and Alerts home page. It tries to be
 * smart and show the user the first Dashboards and Alerts view link he has
 * access to, if any.
 * 
 * @author kudupi
 */
public class DashboardAlertHomeActionRoot extends BaseAction {

    // Serial Version ID
    private static final long serialVersionUID = -7650519333614845926L;

    private static final String VIEW_ALERTS_FEATURE = "feature.dnaAdmin.alert.view";
    
    private static final String VIEW_CHARTS_FEATURE = "feature.dnaAdmin.chart.view";
    
    private static final String VIEW_DASHBOARDS_FEATURE = "feature.dnaAdmin.dashboard.view";
    
    private static final String MY_DASHBOARDS_FEATURE = "feature.dnaAdmin.dashboard.myDashboard";

    /**
     * {@inheritDoc}
     * @see com.opensymphony.xwork2.ActionSupport#execute()
     */
    @Override
    public String execute() throws Exception {

        User user = getCurrentUser();
        // This determines which of the sub-pages of Dashboards and Alerts
        // should be the user's default view, if any, based on his
        // roles.
        if (user.getHasAccessToFeature(MY_DASHBOARDS_FEATURE)) {
            return "myDashboard";
        } else if (user.getHasAccessToFeature(VIEW_DASHBOARDS_FEATURE)) {
            return "viewDashboards";
        } else if (user.getHasAccessToFeature(VIEW_CHARTS_FEATURE)) {
            return "viewCharts";
        } else if (user.getHasAccessToFeature(VIEW_ALERTS_FEATURE)) {
            return "viewAlerts";
        } else {
          // Send the user to their homepage with a little message...
            addSessionActionErrorMessage(new UserMessage("app.dashboardAlert.no.view.access"));
            return "goHome";
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 