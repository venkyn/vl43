/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.evaluator;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.model.AlertCriteriaRelation;
import com.vocollect.epp.alert.model.AlertOperandType;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseLockException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.util.AlertUtil;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.jexl2.Expression;
import org.apache.commons.jexl2.JexlContext;
import org.apache.commons.jexl2.JexlEngine;
import org.apache.commons.jexl2.MapContext;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * 
 * @author khazra
 */
public abstract class JexlAlertEvaluatorRoot implements AlertEvaluator {
    
    private static final Logger log = new Logger(JexlAlertEvaluatorRoot.class);

    private DataAggregatorHandler dataAggregatorHandler;
    
    private NotificationManager notificationManager;

    @SuppressWarnings({ "serial" })
    private Map<AlertCriteriaRelation, String> operationRelationMap = new HashMap<AlertCriteriaRelation, String>() {

        {
            put(AlertCriteriaRelation.WHEN, "");
            put(AlertCriteriaRelation.AND, " && ");
            put(AlertCriteriaRelation.OR, " || ");
        }

    };

    @SuppressWarnings({ "serial" })
    private Map<AlertOperandType, String> operationMap = new HashMap<AlertOperandType, String>() {

        {
            // Numeric operations
            put(AlertOperandType.EQUALSTO, "==");
            put(AlertOperandType.GREATERTHAN, ">");
            put(AlertOperandType.GREATERTHANOREQUALSTO, ">=");
            put(AlertOperandType.LESSTHAN, "<");
            put(AlertOperandType.LESSTHANOREQUALTO, "<=");
            put(AlertOperandType.NOTEQUALSTO, "!=");

            // String operations
            put(AlertOperandType.STRINGEQUALS, "%s.equals('%s')");
            put(AlertOperandType.STRINGNOTEQUALS, "(! %s.equals('%s'))");
            put(AlertOperandType.STARTSWITH, "%s.startsWith('%s')");
            put(AlertOperandType.ENDSWITH, "%s.endsWith('%s')");
            put(AlertOperandType.CONTAINS, "%s.contains('%s')");

            // Date and Time operations
            put(AlertOperandType.AFTER, ">");
            put(AlertOperandType.BEFORE, "<");

        }

    };

    /**
     * Getter for the dataAggregatorHandler property.
     * @return DataAggregatorHandler value of the property
     */
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * Setter for the dataAggregatorHandler property.
     * @param dataAggregatorHandler the new dataAggregatorHandler value
     */
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }
    
    /**
     * @return the notificationManager
     */
    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

    /**
     * @param notificationManager the notificationManager to set
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * {@inheritDoc}
     * @throws JSONException
     * @see com.vocollect.epp.evaluator.GenericEvaluatorRoot#evaluate(java.lang.Object)
     */
    @Override
    public JSONArray evaluate(Alert alert) throws JSONException {
        JSONArray output = new JSONArray();

        JSONArray data = getData(alert);
        if (data == null) {
            return output;    
        }
        String expression = createExpressionString(alert);

        JexlEngine jexl = new JexlEngine();
        Expression ex = jexl.createExpression(expression);

        for (int i = 0; i < data.length(); i++) {
            try {
                JSONObject record = data.getJSONObject(i);

                if (evaluateRecord(ex, alert, record)) {
                    output.put(record);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return output;
    }

    /**
     * @param ex The expression
     * @param alert The alert which has criteria
     * @param record the record to evaluate
     * @return true if expression matches or flase
     * @throws JSONException when field not found in the record
     */
    private boolean evaluateRecord(Expression ex, Alert alert, JSONObject record)
        throws JSONException {
        
        JexlContext jc = new MapContext();

        for (AlertCriteria alertCriteria : alert.getAlertCriterias()) {
            String fieldId = alertCriteria.getDaColumn().getFieldId();
            Object value = record.get(alertCriteria.getDaColumn().getFieldId());

            switch (alertCriteria.getDaColumn().getFieldType()) {
            case STRING:
                jc.set(fieldId, value.toString());
                break;
            
            case DATE:
                jc.set("date1", value);
                Date date = DateUtil.getDateWithLenient(
                    alertCriteria.getThreshold(),
                    GenericDataAggregator.DATE_FORMAT, false);
                jc.set("date2", date);
                break;
            case TIME:
                if (alertCriteria.getOperandType() == AlertOperandType.BEFORE) {
                    if ((Integer) value == GenericDataAggregator.DEFAULT_TIME_VAL) {
                        value = 2400; // Value changed to 2400 because if
                                      // aggregator has returned default time
                                      // value for a record then without
                                      // changing the value to 2400 for alerts
                                      // with time field operator set as before
                                      // the user will get alerts even for records with
                                      // default value as -1 will always be less than whatever value user inputs.
                    }
                }
            default:
                jc.set(fieldId, value);
                break;                
            }
        }
        return (Boolean) ex.evaluate(jc);
    }

    /**
     * @param alert the alert
     * @return data in JSONObject
     * @throws JSONException json exception
     */
    
    public JSONObject createJSONParameter(Alert alert) throws JSONException {
        
        JSONObject parameters = new JSONObject();
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        Site alertSite = null;
        try {
            alertSite = siteContext.getSite(alert);
        } catch (DataAccessException e) {
            log.warn("Site not found for alert id=" + alert.getId());
        }

        String alertDAName = alert.getDaInformation().getName();

        Date startTime = null;
        Date endTime = null;
        
        if (alertDAName.equals("OperatorInfo")) {
            startTime = alert.getLastEvaluationTime();
            if (startTime == null) {
                DateTime startTimeValue = new DateTime();
                int frequencyInMinutes = AlertUtil.getFrequencyInMin(alert
                    .getReNotificationFrequency());
                startTime = startTimeValue.minusMinutes(frequencyInMinutes)
                    .toDate();
            }

            endTime = new Date();

        }
        
        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);
        parameters.put("SITE", alertSite);
        
        if (log.isDebugEnabled()) {
            log.debug("Time window start Time = " + startTime + " End Time = " + endTime);
        }
        return parameters;
        
    }
    
    /**
     * @param alert the alert
     * @return data in JSONArray
     * @throws JSONException json exception
     */
    private JSONArray getData(Alert alert) throws JSONException  {
        JSONArray data = null;

        DAInformation daInfo = alert.getDaInformation();
        String daName = daInfo.getName();

        GenericDataAggregator dataAggregator = dataAggregatorHandler
            .getDataAggregatorByName(daName);

        dataAggregator.setParameters(createJSONParameter(alert));

        try {
            data = dataAggregator.execute();
        } catch (BusinessRuleException e) {
            if (e.getCause() instanceof DatabaseLockException) {
                log.error(
                    "Error evaluating alert: "
                        + alert.getAlertName()
                        + ". Evaluation will be reattempted on next Alert job run,",
                    SystemErrorCode.DATABASE_EXCEPTION, e);
                createNotification(alert);
            }
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Creates the criteria expression string from alert.
     * @param alert The alert from which expression is built
     * @return The criteria expression
     */
    private String createExpressionString(Alert alert) {
        StringBuilder criteriaJexlExpression = new StringBuilder();

        // Get alert criteria and create JEXL expression
        List<AlertCriteria> criteriaList = alert.getAlertCriterias();

        AlertUtil.sortCriteria(criteriaList);

        for (AlertCriteria criteria : criteriaList) {
            DAColumn daColumn = criteria.getDaColumn();

            AlertOperandType alertOperandtype = criteria.getOperandType();

            String threshold = criteria.getThreshold();

            String operation = "";
            criteriaJexlExpression.append(operationRelationMap.get(criteria
                .getRelation()));

            int dotIndex = -1;

            switch (daColumn.getFieldType()) {
            case INTEGER:
                operation = operationMap.get(alertOperandtype);
                criteriaJexlExpression.append(daColumn.getFieldId())
                    .append(" ");
                criteriaJexlExpression.append(operation).append(" ");

                dotIndex = threshold.indexOf('.');
                if (dotIndex == -1) {
                    criteriaJexlExpression.append(Integer.parseInt(threshold));
                } else {
                    criteriaJexlExpression.append(Integer.parseInt(threshold
                        .substring(0, dotIndex)));
                }

                break;
            case TIME:
                operation = operationMap.get(alertOperandtype);
                criteriaJexlExpression.append(daColumn.getFieldId())
                    .append(" ");
                criteriaJexlExpression.append(operation).append(" ");
                threshold = threshold.replaceAll(":", "");
                criteriaJexlExpression.append(Integer.parseInt(threshold));
                break;
            case FLOAT:
                operation = operationMap.get(alertOperandtype);
                criteriaJexlExpression.append(daColumn.getFieldId())
                    .append(" ");
                criteriaJexlExpression.append(operation).append(" ");

                dotIndex = threshold.indexOf('.');
                if (dotIndex == -1) {
                    criteriaJexlExpression.append(Integer.parseInt(threshold));
                    break;
                } else if (dotIndex == threshold.length() - 1) {
                    criteriaJexlExpression.append(Integer.parseInt(threshold
                        .substring(0, dotIndex)));
                    break;
                }

                criteriaJexlExpression.append(threshold);
                break;

            case STRING:
                operation = operationMap.get(alertOperandtype);
                operation = String.format(operation, daColumn.getFieldId(),
                    threshold.toString());
                criteriaJexlExpression.append(operation);
                break;
            
            case DATE:
                operation = operationMap.get(alertOperandtype);
                String exp = "%s  " + operation + " %s";
                exp = String.format(exp, "date1", "date2");
                criteriaJexlExpression.append(" ").append(exp).append(" ");
                break;

            default:
                break;
            }

        }

        return criteriaJexlExpression.toString();
    }

    /**
     * Method to create a notification in case of error evaluating this alert.
     * Following alerts will continue to be evalaued without being impacted.
     * @param alert - alert failed evaluation
     */
    private void createNotification(Alert alert) {
        try {
            // Create missed Alert Notification
            LOPArrayList lop = new LOPArrayList();
            getNotificationManager().createNotification(
                "notification.alertJob.title",
                "errorCode.message.EPP118", NotificationPriority.CRITICAL,
                new Date(), "100",
                "notification.column.keyname.Application.2", lop);
        } catch (Exception exc) {
            log.error("Error creating Notification",
                SystemErrorCode.NOTIFICATION_FAILURE, exc);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 