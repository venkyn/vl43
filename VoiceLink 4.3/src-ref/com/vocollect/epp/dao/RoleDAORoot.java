/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Role;

/**
 * Role Data Access Object (DAO) interface.
 *
 * @author Dennis Doubleday
 */
public interface RoleDAORoot extends GenericDAO<Role> {

    /**
     * Gets role information by the role name.
     * @param name the current name
     * @return the specified <code>Role</code>, if it exists; null otherwise.
     * @throws DataAccessException on any failure
     */
    Role findByName(String name) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 