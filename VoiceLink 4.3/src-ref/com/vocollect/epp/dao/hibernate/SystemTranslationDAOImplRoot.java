/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.SystemTranslationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemTranslation;
import com.vocollect.epp.util.LocaleAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * DAO implemetation for SystemTranslations.
 *
 * @author mkoenig
 */
public abstract class SystemTranslationDAOImplRoot 
    extends GenericDAOImpl<SystemTranslation>
     implements SystemTranslationDAO {

    /**
     * Constructor.
     */
    public SystemTranslationDAOImplRoot() {
        super(SystemTranslation.class);
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.SystemTranslationDAORoot#listSupportedLocales()
     */
    @SuppressWarnings("unchecked")
    public List<Locale> getSupportedLocales() throws DataAccessException {
        List<Locale> locales = new ArrayList<Locale>();
        
        List<String> data = getCurrentSession().getNamedQuery(
        "SystemTranslation_findSupportedLocales").list();
        
        for (String locale : data) {
            locales.add(LocaleAdapter.makeLocaleFromString(locale));
        }
        
        return locales;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 