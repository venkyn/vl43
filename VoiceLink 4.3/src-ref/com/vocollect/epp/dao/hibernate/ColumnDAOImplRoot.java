/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.ColumnDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;

import java.util.List;

/**
 * Hibernate implementation of ColumnDAO.
 *
 * @author ddoubleday
 */
public abstract class ColumnDAOImplRoot extends GenericDAOImpl<Column>
    implements ColumnDAO {

    /**
     * Constructor.
     */
    public ColumnDAOImplRoot() {
        super(Column.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.ColumnDAO#getColumns(com.vocollect.epp.model.View, com.vocollect.epp.model.User)
     */
    @SuppressWarnings("unchecked")
    public List<Column> getColumns(View view, User user)
        throws DataAccessException {

        getCurrentSession().enableFilter("Column_userColumnFilter")
            .setParameter("userId", user.getId());
        return getCurrentSession().getNamedQuery(
            "Column_findByViewIdWithUserColumns")
            .setLong("viewId", view.getId()).list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.ColumnDAO#getVisibleColumns(com.vocollect.epp.model.View, com.vocollect.epp.model.User)
     */
    @SuppressWarnings("unchecked")
    public List<Column> getVisibleColumns(View view, User user)
        throws DataAccessException {

        getCurrentSession().enableFilter("Column_userColumnFilter")
            .setParameter("userId", user.getId());
        return getCurrentSession().getNamedQuery(
            "Column_findVisibleByViewIdWithUserColumns")
            .setLong("viewId", view.getId()).list();
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 