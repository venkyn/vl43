/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;

import java.lang.reflect.Method;

/**
 * Used to locate a named query based on the called finder method.
 * @author ddoubleday (adapted from Per Mellqvist)
 */
public interface FinderNamingStrategy {

    /**
     * @param findTargetType the domain class the finder is associated with.
     * @param finderMethod the method being called.
     * @return the name of the Hibernate named query to find with.
     */
    public String queryNameFromMethod(Class<?> findTargetType, Method finderMethod);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 