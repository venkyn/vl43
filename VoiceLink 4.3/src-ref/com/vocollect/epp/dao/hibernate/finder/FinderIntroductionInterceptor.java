/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;


import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.IntroductionInterceptor;

/**
 * Interceptor that implements all GenericDAO finder methods from named queries.
 * in any class that is intercepted by this, for any method beginning with
 * "find", "list", or "iterate" this interceptor will use the FinderExecutor to
 * call a Hibernate named query.
 * @author ddoubleday (adapted from Per Mellqvist original)
 */
public class FinderIntroductionInterceptor implements IntroductionInterceptor {

    /**
     * {@inheritDoc}
     * @see org.aopalliance.intercept.MethodInterceptor#invoke(org.aopalliance.intercept.MethodInvocation)
     */
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        FinderExecutor<?> executor = (FinderExecutor<?>) methodInvocation.getThis();

        Method method = methodInvocation.getMethod();
        String methodName = method.getName();

        if (methodName.startsWith("find")) {
            return executor.executeUniqueFinder(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("list")) {
            return executor.executeListFinder(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("iterate")) {
            return executor.executeIterateFinder(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("report")) {
            return executor.executeReportQuery(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("uniqueness")) {
            return executor.executeIdFinder(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("count")
            || methodName.startsWith("sum")
            || methodName.startsWith("max")
            || methodName.startsWith("min")
            || methodName.startsWith("avg")) {
            return executor.executeAggregateFunction(
                method, methodInvocation.getArguments());
        } else if (methodName.startsWith("update")) {
            if (methodName.startsWith("updateCustom")) {
                return executor.executeUpdateWithTextualParamReplacement(
                    method, methodInvocation.getArguments());
            } else if (methodName.startsWith("updateSQL")) {
                return executor.executeSQLUpdate(
                    method, methodInvocation.getArguments());
            } else {
                // Normal update interception
                return executor.executeUpdate(
                    method, methodInvocation.getArguments());
            }
        } else {
            // No interception of this method
            return methodInvocation.proceed();
        }
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.aop.DynamicIntroductionAdvice#implementsInterface(java.lang.Class)
     */
    public boolean implementsInterface(Class intf) {
        return intf.isInterface()
            && FinderExecutor.class.isAssignableFrom(intf);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 