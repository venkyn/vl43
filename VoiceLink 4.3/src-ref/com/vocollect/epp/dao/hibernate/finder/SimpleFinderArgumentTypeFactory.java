/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;


import java.util.Properties;

import org.hibernate.type.Type;
import org.hibernate.type.TypeFactory;

/**
 * Maps Enums to a custom Hibernate user type.
 * @author ddoubleday (adapted from Per Mellqvist original)
 */
public class SimpleFinderArgumentTypeFactory implements
    FinderArgumentTypeFactory {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderArgumentTypeFactory#getArgumentType(java.lang.Object)
     */
    public Type getArgumentType(Object arg) {
        if (arg instanceof Enum) {
            return getEnumType(arg.getClass());
        } else {
            return null;
        }
    }

    /**
     * @param argClass The enum class we want to get a Hibernate type for.
     * @return the Hibernate custom type that implements persistence for the
     * enum.
     */
    private Type getEnumType(Class<? extends Object> argClass) {
        Properties p = new Properties();
        p.setProperty("enumClassName", argClass.getName());
        Type enumType = TypeFactory.heuristicType(
            "com.vocollect.epp.dao.hibernate.EnumUserType", p);
        return enumType;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 