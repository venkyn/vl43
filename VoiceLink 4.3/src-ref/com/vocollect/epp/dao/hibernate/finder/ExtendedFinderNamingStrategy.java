/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;


import java.lang.reflect.Method;


/**
 * Looks up Hibernate named queries based on the simple name of the invoked
 * class and the method name of the invocation.
 * @author ddoubleday (adapted from Per Mellqvist original)
 */
public class ExtendedFinderNamingStrategy implements FinderNamingStrategy {

    /**
     * Always look for queries that start with find (even if method is
     * iterate... or list...)
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderNamingStrategy#queryNameFromMethod(java.lang.Class, java.lang.reflect.Method)
     */
    public String queryNameFromMethod(Class<?> findTargetType, Method finderMethod) {
        String methodName = finderMethod.getName();
        // Assume method name is the finder name.
        String methodPart = methodName;
        if (methodName.startsWith("list")) {
            methodPart = "find" + methodName.substring("list".length());
        } else if (methodName.startsWith("iterate")) {
            methodPart = "find" + methodName.substring("iterate".length());
        }
        return findTargetType.getSimpleName() + "_" + methodPart;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 