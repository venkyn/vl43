/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.util.ResultDataInfo;


/**
 *
 *
 * @author ddoubleday
 */
public class QueryDecorator {

    // The max number of rows to return
    private int     rowCount;

    // The current sort direction; true implies ascending, false
    // implies descending.
    private boolean sortAscending;

    // The name of the current sort field. No sort is wanted if no column
    // is specified.
    private String  sortColumn;

    // The row index to start at (1-based)
    private int    startRow;

    private Filter filter;

    private String whereClause;

    // The sort column object. Needed for additional information about
    // type of sorting to do.
    private Column sortColumnObject;


    /**
     * Constructor.
     */
    public QueryDecorator() {

    }

    /**
     * Constructor that builds from a ResultDataInfo and copies selected
     * properties (sortColumn and sortAscending, currently).
     * @param rdi the ResultDataInfo
     */
    public QueryDecorator(ResultDataInfo rdi) {
        setSortColumn(rdi.getSortColumn());
        setSortAscending(rdi.isSortAscending());
        setFilter(rdi.getFilter());
        setWhereClause(rdi.getWhereClause());
        setSortColumnObject(rdi.getSortColumnObject());
    }

    /**
     * Get the max number of rows to fetch.
     * @return the max number of rows. Zero implies all rows.
     */
    public int getRowCount() {
        return this.rowCount;
    }

    /**
     * Getter for the sortColumn property.
     * @return String value of the property
     */
    public String getSortColumn() {
        return this.sortColumn;
    }

    /**
     * Getter for the startRow property.
     * @return the row to start fetching results at. If zero, fetch all results.
     */
    public int getStartRow() {
        return this.startRow;
    }

    /**
     * Getter for the sortAscending property.
     * @return boolean value of the property
     */
    public boolean isSortAscending() {
        return this.sortAscending;
    }

    /**
     * Set the max number of rows to fetch. The default is zero, which implies
     * all rows.
     * @param rowCount the new rowCount value
     */
    public void setRowCount(int rowCount) {
        this.rowCount = rowCount;
    }

    /**
     * Setter for the sortAscending property.
     * @param sortAscending the new sortAscending value
     */
    public void setSortAscending(boolean sortAscending) {
        this.sortAscending = sortAscending;
    }

    /**
     * Setter for the sortColumn property.
     * @param sortColumn the new sortColumn value
     */
    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    /**
     * Setter for the startRow property.
     * @param startRow the new startRow value (the default of zero
     * indicates all rows).
     */
    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    /**
     * @return the Filter associated with the query.
     */
    public Filter getFilter() {
        return filter;
    }

    /**
     * @param filter the Filter to associate with the query.
     */
    public void setFilter(Filter filter) {
        this.filter = filter;
    }

    /**
     * @return the where clause for the query.
     */
    public String getWhereClause() {
        return whereClause;
    }

    /**
     * @param whereClause the where clause for the query.
     */
    public void setWhereClause(String whereClause) {
        this.whereClause = whereClause;
    }

    /**
     * Getter for the sortColumnObject property.
     * @return the value of the property
     */
    public Column getSortColumnObject() {
        return this.sortColumnObject;
    }

    /**
     * Setter for the sortColumnObject property.
     * @param sortColumnObject the new sortColumnObject value
     */
    public void setSortColumnObject(Column sortColumnObject) {
        this.sortColumnObject = sortColumnObject;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 