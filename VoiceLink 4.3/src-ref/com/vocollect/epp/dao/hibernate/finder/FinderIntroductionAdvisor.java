/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;

import org.springframework.aop.support.DefaultIntroductionAdvisor;

/**
 * The introduction advisor for proxying DAO finder methods.
 *
 * @author ddoubleday (adapted from Per Mellqvist original)
 */
public class FinderIntroductionAdvisor extends DefaultIntroductionAdvisor {

    private static final long serialVersionUID = -7917533073189348471L;

    /**
     * Constructor.
     */
    public FinderIntroductionAdvisor() {
        super(new FinderIntroductionInterceptor());
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 