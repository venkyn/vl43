/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate.finder;

import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

/**
 * Interface that defines the means of executing Hibernate queries generically.
 * @param <T>
 *
 * @author ddoubleday
 */
public interface FinderExecutor<T> {

    /**
     * Execute the specified method. This method will be called via an AOP
     * interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The list returned by the query, which may be empty.
     */
    List<T> executeListFinder(Method method, Object[] queryArgs);
    
    /**
     * Execute the list finder and filter out duplicate references in the result set.  Duplicate references can happen
     * when forced fetch joins are utilized in a query - Hibernate developers consider this normal behavior
     * 
     * Execute the specified method. This method will be called via an AOP
     * interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The list returned by the query, which may be empty.
     */
    List<T> executeDistinctListFinder(Method method, Object[] queryArgs);

    /**
     * Execute the specified method. This method will be called via an AOP
     * interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The list returned by the query, which may be empty.
     */
    Iterator<T> executeIterateFinder(Method method, Object[] queryArgs);

    /**
     * Execute the specified method. This method will be called via an AOP
     * interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The item of type T returned by the query, which may be null.
     */
    T executeUniqueFinder(Method method, Object[] queryArgs);

    /**
     * Execute the specified function, which must return a single value.
     * The type of the returned value depends on the query, and the caller
     * will need to cast the returned Object to the right type. Generally
     * this will be some numeric type.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The (generally numeric) Object returned by the query.
     */
    Object executeAggregateFunction(Method method, Object[] queryArgs);

    /**
     * Execute the specified function, which returns a List of Object arrays.
     * This is the form in which Hibernate returns the results of report
     * queries, i.e. queries that don't return model objects representing rows.
     * For example, projection queries return individual fields that may
     * represent pieces of an object, or summation values, etc.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The List of Object arrays, one per result row.
     */
    public List<Object[]> executeReportQuery(Method method, Object[] queryArgs);

    /**
     * Execute the specified function, which is expected to be an update
     * query.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The number of rows affected.
     */
    public int executeUpdate(Method method, Object[] queryArgs);

    /**
     * Execute the specified function, which is expected to be a SQL update
     * query.
     * <p>
     * Note that this does none of
     * dynamic tagging and filtering that the prepareQuery() method does.
     * All it will do is parameter replacement. SQL queries should only be
     * used in very limited situations, and only if the code is standard
     * SQL that is database-independent.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The number of rows affected.
     */
    public int executeSQLUpdate(Method method, Object[] queryArgs);

    /**
     * Execute the specified function, which is expected to be an update
     * query.
     * <p>
     * Normally, executeUpdate() would be used. However, we have found that
     * Hibernate will not do parameter replacement in some cases, such as in
     * the middle of an expression used in an update. This method gets around
     * this by doing textual replacement of positional parameters directly in
     * the query String. This should be used only as needed because it is not
     * so safe as the normal Hibernate method, and is limited to parameters
     * that have a suitable toString() method.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The number of rows affected.
     */
    public int executeUpdateWithTextualParamReplacement(Method method,
                                                        Object[] queryArgs);

    /**
     * Execute the specified function, which returns a Long of null or the
     * id of the entity found.
     * <p>
     * This method will be called via an AOP interceptor.
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return the Id of the row found or null.
     */
    public Long executeIdFinder(Method method, Object[] queryArgs);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 