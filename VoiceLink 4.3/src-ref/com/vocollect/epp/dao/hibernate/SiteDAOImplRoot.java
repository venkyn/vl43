/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.util.OperandType;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;


/**
 *
 *
 * @author dkertis
 */
public abstract class SiteDAOImplRoot extends GenericDAOImpl<Site> implements SiteDAO {

    private static final long TIME_ZONE_COLUMN_ID = -405;

    /**
     * Constructor.
     */
    public SiteDAOImplRoot() {
        super(Site.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImpl#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // get all users from the DB
        //TODO modify this query to only return sites user has access to
        String hql = "select obj from Site obj";
        boolean needWhereClause = true;
        if (siteContext.isFilterBySite() && !siteContext.isHasAllSiteAccess()) {
            hql +=  " , Tag t where ( t.taggableId=obj.id and t.tagType=1 and t.id in (:tagId) )";
            needWhereClause = false;
        }

        if (rdi.getFilter() != null) {
            String filterHQL = rdi.getFilter().toHQL();
            if (needWhereClause) {
                if (filterHQL != null && filterHQL.length() > 0) {
                    hql += " where " + rdi.getFilter().toHQL();
                }
            } else {
                if (filterHQL != null && filterHQL.length() > 0) {
                    hql += " and " + rdi.getFilter().toHQL();
                }
            }
        }

        hql += getOrderClause(rdi);
        Query query = getCurrentSession().createQuery(hql);

        if (rdi.getFilter() != null) {
            // We need to replace any named parameters
            rdi.getFilter().fillInNamedParameters(query);
        }

        if (siteContext.isFilterBySite() && !siteContext.isHasAllSiteAccess()) {
            query.setParameterList("tagId", siteContext.getTagIdsForQuery());
        }

        List<DataObject> siteList = query.list();
        // Since TimeZone is slightly different and doesn't fit into our
        // filter model - I have to do it here separately - if the time zone
        // is being filtered
        Filter filter = rdi.getFilter();
        if (filter != null) {
            // Iterate over the filter criterion and pick out the timezone column
            for (FilterCriterion filterCriterion : filter.getFilterCriteria()) {
                if (filterCriterion.getColumn().getId() == TIME_ZONE_COLUMN_ID) {
                    // We have a filter case with time zone involved
                    OperandType operand = OperandType.getById(filterCriterion.getOperand().getId());
                    // We have the operand type - now filter the list
                    Iterator<DataObject> siteListIter = siteList.iterator();
                    while (siteListIter.hasNext()) {
                        Site site = (Site) siteListIter.next();
                        try {
                            if (!operand.filter(site, filterCriterion, rdi.getDisplayUtilties())) {
                                siteListIter.remove();
                            }
                        } catch (Exception e) {
                            siteListIter.remove();
                        }
                    }
                }
            }
        }

        return siteList;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.SiteDAO#getSiteByMinId(com.vocollect.epp.model.User)
     */
    public Tag getSiteByMinId(User u) throws DataAccessException {
        String hql = null;
        if (u.getAllSitesAccess()) {
            hql = "select t from Tag t where t.tagType=" + Tag.SITE
            + " and t.id !=" + Tag.SYSTEM + " and t.id !=" + Tag.ALL + " order by t.id asc";
        } else {
            hql = "select u.tags from User u where u.id=:userId order by u.tags.id asc";
        }
        Query query = getCurrentSession().createQuery(hql);
        query.setMaxResults(1);
        if (!u.getAllSitesAccess()) {
            query.setParameter("userId", u.getId());
        }

        Object obj = query.uniqueResult();
        return (Tag) obj;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#get(java.io.Serializable)
     */
    @Override
    public Site get(Long id) throws DataAccessException {

         SiteContext siteContext = SiteContextHolder.getSiteContext();

         // get all users from the DB
         String hql = null;

         // start with a simple select
         hql = "select obj from " + getPersistentClassName() + " obj where obj.id=:id";

         // if the object is taggable, make sure the user has access to it
         if (siteContext != null && siteContext.isFilterBySite()) {
             // bind the query to only the tags the user has acccess to
             if (!siteContext.isHasAllSiteAccess()) {
                 hql = "select obj from Site obj,Tag t "
                       + "where obj.id=:id and ( t.taggableId=obj.id and t.tagType=1 and t.id in (:tagId) )";
             }
         }
         Query query = getCurrentSession().createQuery(hql);

         if (siteContext != null && siteContext.isFilterBySite()) {
             // bind the query to only the tags the user has acccess to
             if (!siteContext.isHasAllSiteAccess()) {
                 query.setParameterList("tagId", siteContext.getTagIdsForQuery());
             }
         }
         // bind the id parameter
         query.setParameter("id", id);
         // now do the query and make sure there is only one result
         Site t = (Site) query.uniqueResult();

         // if the object is not found, throw an exception
         if (t == null) {
             throw new EntityNotFoundException(getPersistentClass(), id);
         } else {
             return t;
         }

     }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.SiteDAO#findByName(java.lang.String)
     */
    public Site findByName(String name) throws DataAccessException {
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.SiteDAO#findDefaultSite()
     */
    public Site findDefaultSite() {
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImplRoot#getDataByFieldAndValue(java.lang.String, java.lang.String)
     */
    @Override
    public List getDataByFieldAndValue(String fieldName, String value) throws DataAccessException {
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // get all users from the DB
        String hql = null;

        // start with a simple select
        hql = "select obj." + fieldName + " from " + getPersistentClassName()
            + " obj where obj." + fieldName + " like :value";

        // if the object is taggable, make sure the user has access to it
        if (siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                hql = "select obj." + fieldName + " from Site obj, Tag t "
                      + "where obj." + fieldName + " like :value "
                      + "and ( t.taggableId=obj.id and t.tagType=1 and t.id in (:tagId) )";
            }
        }
        Query query = getCurrentSession().createQuery(hql);

        if (siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                query.setParameterList("tagId", siteContext.getTagIdsForQuery());
            }
        }
        // bind the id parameter
        query.setParameter("value", value + "%");
        // now do the query and make sure there is only one result

        return query.list();


    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 