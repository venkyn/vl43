/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.ResultDataInfo;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.json.JSONException;

/**
 * Hibernate implementation of NotificationDAO.
 *
 * @author Aich Debasis
 */

public abstract class NotificationDAOImplRoot extends GenericDAOImpl<Notification> implements
    NotificationDAO {

    private static final String VIEW_NOTIFICATION_FTR_NAME = "feature.appAdmin.notification.view";
    private static final String ADMINISTRATOR_ROLE_NAME = "Administrator";

    /**
     * Constructor.
     */
    public NotificationDAOImplRoot() {
        super(Notification.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getGroupedCountBySite(java.lang.String, com.vocollect.epp.model.NotificationPriority)
     */
    public List<Object[]> getGroupedCountBySite(String userName, NotificationPriority np) throws DataAccessException {
        Query qry = getCurrentSession().getNamedQuery("Notification_getGroupedCountBySite");
        qry.setParameter(0, np);
        qry.setParameter(1, userName);
        return qry.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getGroupedCountBySiteForAllSiteAccess(com.vocollect.epp.model.NotificationPriority)
     */
    public List<Object[]> getGroupedCountBySiteForAllSiteAccess(NotificationPriority np) throws DataAccessException {
        Query qry = getCurrentSession().getNamedQuery("Notification_getGroupedCountBySiteForAllSiteAccess");
        qry.setParameter(0, np);
        return qry.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getUnackedSystemNotificationsCount(com.vocollect.epp.model.NotificationPriority)
     */
    public int getUnackedSystemNotificationsCount(NotificationPriority np) throws DataAccessException {
        Query qry = getCurrentSession().getNamedQuery("Notification_getUnackedSystemNotificationsCount");
        qry.setParameter(0, np);
        Number result = (Number) qry.uniqueResult();
        if (result == null) {
            return 0;
        } else {
            return result.intValue();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getUserEmailsBySiteId(java.lang.Long)
     */
    public List<String> getUserEmailsBySiteId(Long id) throws DataAccessException {
        Query q = getCurrentSession().getNamedQuery("Notification_getUserEmailsBySiteId");
        q.setParameter("siteId", id);
        q.setParameter("administratorRole", ADMINISTRATOR_ROLE_NAME);
        q.setParameter("viewNotificationFeature", VIEW_NOTIFICATION_FTR_NAME);
        return q.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getUnackNotifications(com.vocollect.epp.model.NotificationPriority)
     */
    public List<Notification> getUnackNotifications(NotificationPriority np) throws DataAccessException {
        Query q = getCurrentSession().getNamedQuery("Notification_getUnackNotifications");
        q.setParameter(0, np);
        return q.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getMaxNotificationIdForSiteId(com.vocollect.epp.model.NotificationPriority, java.lang.Long)
     */
    public Long getMaxNotificationIdForSiteId(NotificationPriority np, Long siteId) throws DataAccessException {
        Query qry = getCurrentSession().getNamedQuery("Notification_getMaxNotificationIdForSiteId");
        qry.setParameter(0, siteId);
        qry.setParameter(1, np);
        List list = qry.list();
        if (list != null && list.size() > 0) {
            return (Long) list.get(0);
        } else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getMaxNotificationIdForSystemTag(com.vocollect.epp.model.NotificationPriority)
     */
    public Long getMaxNotificationIdForSystemTag(NotificationPriority np) throws DataAccessException {
        Query qry = getCurrentSession().getNamedQuery("Notification_getMaxNotificationIdForSystemTag");
        qry.setParameter(0, np);
        List list = qry.list();
        if (list != null && list.size() > 0) {
            return (Long) list.get(0);
        } else {
            return null;
        }
    }

    /**
     * We need to override this method due to filtering.  We are going to have to replace
     * all resource keys with values and do filtering based on values
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImpl#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        // I want to retrieve all notifications since I need to do the filter in
        // memory - so get the Filter from the rdi and set the rdi filter to null
        Filter filter = rdi.getFilter();
        rdi.setFilter(null);

        List<DataObject> returnList = super.getAll(rdi);

        // For each notification data - we need to translate the lookup keys to
        // values so we can do the filtering
        for (int index = 0; index < returnList.size(); index++) {
            Notification notification = (Notification) returnList.get(index);
            notification.setApplication(
                ResourceUtil.getLocalizedKeyValue(notification.getApplication()));
            notification.setProcess(
                ResourceUtil.getLocalizedKeyValue(notification.getProcess()));
            notification.setMessage(
                ResourceUtil.getLocalizedKeyValue(notification.getMessage()));
            returnList.set(index, notification);
        }

        if (filter != null) {
            try {
                // If this fails - for what ever reason related to
                // reflection - just go ahead and throw a DataAccessException
                // since the getDataObjectsAsJSON will catch it and send
                // a failure with the exception info
                filter.applyFilter(returnList, rdi.getDisplayUtilties());
            } catch (IllegalAccessException iae) {
                throw new DataAccessException(iae);
            } catch (NoSuchMethodException nsme) {
                throw new DataAccessException(nsme);
            } catch (InvocationTargetException ite) {
                throw new DataAccessException(ite);
            } catch (JSONException ite) {
                throw new DataAccessException(ite);
            }
        }

        return returnList;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date)
     */
    public List<Notification> listOlderThan(QueryDecorator decorator, Date date) throws DataAccessException {
        // this method isn't actually called, implemented by dynamic finder
        return null;
    }



    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.NotificationDAORoot#getUnackedSystemNotificationsCount(java.lang.String, java.lang.String)
     */
    public int getUnackedSystemNotificationsCount(String processName,
                                                  String terminalNumber)
        throws DataAccessException {

        Query qry = getCurrentSession().getNamedQuery("Notification_getAckTerminalLicenseNotificationsCount");
        qry.setParameter("processName", processName);
        qry.setParameter("terminalNumber", terminalNumber);
        Number result = (Number) qry.uniqueResult();
        if (result == null) {
            return 0;
        } else {
            return result.intValue();
        }

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 