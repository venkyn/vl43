/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.ParameterizedType;

/**
 * Hibernate UserType for persisting Enum objects.
 * @author ddoubleday (adapted from Gavin King)
 */
@SuppressWarnings("unchecked")
public class EnumUserType implements EnhancedUserType, ParameterizedType {

    private Class<? extends Enum> enumClass;

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.ParameterizedType#setParameterValues(java.util.Properties)
     */
    public void setParameterValues(Properties parameters) {
        String enumClassName = parameters.getProperty("enumClassName");
        try {
            enumClass = Class.forName(enumClassName).asSubclass(Enum.class);
        } catch (ClassNotFoundException cnfe) {
            throw new HibernateException("Enum class not found", cnfe);
        }
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#assemble(java.io.Serializable, java.lang.Object)
     */
    public Object assemble(Serializable cached, Object owner)
        throws HibernateException {
        return cached;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#deepCopy(java.lang.Object)
     */
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#disassemble(java.lang.Object)
     */
    public Serializable disassemble(Object value) throws HibernateException {
        return (Enum) value;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#equals(java.lang.Object, java.lang.Object)
     */
    public boolean equals(Object x, Object y) throws HibernateException {
        return x == y;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#hashCode(java.lang.Object)
     */
    public int hashCode(Object x) throws HibernateException {
        return x.hashCode();
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#isMutable()
     */
    public boolean isMutable() {
        return false;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#nullSafeGet(java.sql.ResultSet, java.lang.String[], java.lang.Object)
     */
    public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
        throws HibernateException, SQLException {

        Integer number = rs.getInt(names[0]);
        if (rs.wasNull() || (number == null)) {
            return null;
        } else {
            if (ValueBasedEnum.class.isAssignableFrom(enumClass)) {
                // This is a value-based enum, so treat the number as
                // the value, not the ordinal.
                // We need a sample enum value to call the fromValue()
                // method on. The 0th one is guaranteed to exist, so use it.
                Object sample = enumClass.getEnumConstants()[0];
                // Now get the real enum value we want to return.
                return ((ValueBasedEnum) sample).fromValue(number);
            } else {
                // This is not one of our value-based enums, so just use the
                // number as the ordinal value.
                return enumClass.getEnumConstants()[number];
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#nullSafeSet(java.sql.PreparedStatement, java.lang.Object, int)
     */
    public void nullSafeSet(PreparedStatement st, Object value, int index)
        throws HibernateException, SQLException {
        if (value == null) {
            st.setNull(index, Types.INTEGER);
        } else {
            if (ValueBasedEnum.class.isAssignableFrom(enumClass)) {
                // This is a value-based enum, save the value.
                st.setInt(index, ((ValueBasedEnum) value).toValue());
            } else {
                // This is not one of our value-based enums, so just
                // save the ordinal.
                st.setInt(index, ((Enum) value).ordinal());
            }
        }

    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#replace(java.lang.Object, java.lang.Object, java.lang.Object)
     */
    public Object replace(Object original, Object target, Object owner)
        throws HibernateException {
        return original;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#returnedClass()
     */
    public Class returnedClass() {
        return enumClass;
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.UserType#sqlTypes()
     */
    public int[] sqlTypes() {
        return new int[] { Types.INTEGER };
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.EnhancedUserType#fromXMLString(java.lang.String)
     */
    public Object fromXMLString(String xmlValue) {
        int ordinal = Integer.parseInt(xmlValue);
        return enumClass.getEnumConstants()[ordinal];
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.EnhancedUserType#objectToSQLString(java.lang.Object)
     */
    public String objectToSQLString(Object value) {
        return String.valueOf(((Enum) value).ordinal());
    }

    /**
     * {@inheritDoc}
     * @see org.hibernate.usertype.EnhancedUserType#toXMLString(java.lang.Object)
     */
    public String toXMLString(Object value) {
        return String.valueOf(((Enum) value).ordinal());
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 