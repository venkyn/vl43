/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.SearchDAO;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.hibernate.MappingException;
import org.hibernate.Query;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Hibernate implementation of SearchDAO, the DAO for searching all 
 * searchable objects.
 *
 * @author ddoubleday
 */
public abstract class SearchDAOImplRoot extends GenericDAOImpl<BaseModelObject> 
    implements SearchDAO {

    // Wild card token used in HQL
    private static final char HQL_WILD_CARD = '%';

    // Wild card token used in the GUI
    private static final char GUI_WILD_CARD = '*';

    // The absolute limit on the number of rows any given search query
    // will return.
    private static final int QUERY_ABSOLUTE_LIMIT = 10000;

    // The name of the named parameter for the Locale
    private static final String LOCALE_PARAM = "locale_2";

    // The name of the named parameter for the search term.
    private static final String SEARCH_TERM_PARAM = "searchTerm_1";

    private static final Logger log = new Logger(SearchDAOImpl.class);

    // The array of named queries to be run during search. These
    // are Spring-injected.
    private static String[] searchQueryNames;

    /**
     * Constructor.
     */
    public SearchDAOImplRoot() {
        super(BaseModelObject.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.SearchDAORoot#searchAll(java.lang.String)
     */
    @SuppressWarnings("unchecked")
    public List<BaseModelObject> searchAll(String searchTerm) {

        if (log.isDebugEnabled()) {
            log.debug("Start search");
        }
        
        List<BaseModelObject> hits = new ArrayList<BaseModelObject>(); 
        
        if (!StringUtil.isNullOrBlank(searchTerm)) {
            // Only search if some non-whitespace is specified.
            String fixedSearchTerm = fixSearchTerm(searchTerm);
            // Get the list of queries to run
            List<Query> searchQueries = getSearchQueries();
            
            // Now run each query.
            for (Query query : searchQueries) {
                String[] namedParameters = query.getNamedParameters();
                // There is a presumption here that, if the query only
                // takes one named parameter, it is the search term. If
                // it takes more than one, then the second one is the Locale.
                query.setParameter(SEARCH_TERM_PARAM, fixedSearchTerm);
                if (namedParameters.length > 1) {
                    query.setParameter(LOCALE_PARAM, LocaleContextHolder
                        .getLocale().toString());
                }

                // Set a fallback limit so the results don't get too big. We can't
                // apply the real limit at this point because we haven't separated
                // things into sites yet.
                query.setMaxResults(QUERY_ABSOLUTE_LIMIT);

                List<BaseModelObject> queryHits = query.list();
                if (queryHits != null) {
                    hits.addAll(queryHits);
                }
            }
        }        
        if (log.isDebugEnabled()) {
            log.debug("Search complete, " + hits.size() + " hits");
        }
        
        return hits;
    }

    /**
     * Trim, lower and decorate the search term with wild-cards as appropriate.
     * @param searchTerm the original term
     * @return the fixed term
     */
    private String fixSearchTerm(String searchTerm) {
        Locale locale = LocaleContextHolder.getLocale();
        if (searchTerm.indexOf(GUI_WILD_CARD) != -1) {
            // User specified wild cards, just use them
            return searchTerm.trim().toLowerCase(locale).
                                        replace(GUI_WILD_CARD, HQL_WILD_CARD);
        } else {
            // Supply default wild cards.
            return HQL_WILD_CARD + searchTerm.trim().toLowerCase(locale) + HQL_WILD_CARD;
        }
    }

    /**
     * @return the List of Queries to run when searching.
     */
    private List<Query> getSearchQueries() {
        List<Query> queryList = new ArrayList<Query>();
        for (String queryName : getSearchQueryNames()) {
            Query query;
            try {
                query = getCurrentSession().getNamedQuery(queryName);
                queryList.add(query);
            } catch (MappingException e) {
                log.warn("Query " + queryName + " not defined");
            }
        }
        return queryList;
    }

    /**
     * Override this to customize the list of search queries.
     * @return the array of query names to run when searching.
     */
    protected String[] getSearchQueryNames() {
        return searchQueryNames;
    }
    
    /**
     * Spring-injected list of search query names to be run.
     * @param queryNames the array of query names.
     */
    public void setSearchQueryNames(String[] queryNames) {
        searchQueryNames = queryNames;
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 