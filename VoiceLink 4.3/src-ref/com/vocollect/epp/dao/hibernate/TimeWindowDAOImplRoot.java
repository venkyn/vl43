/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.TimeWindowDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.TimeWindow;
import com.vocollect.epp.model.User;

import java.util.List;

import org.hibernate.Query;

/**
 * Hibernate implementation of TimeWindowDAO.
 *
 * @author bnichols
 */
public abstract class TimeWindowDAOImplRoot extends GenericDAOImpl<TimeWindow>
    implements TimeWindowDAO {

    /**
     * Constructor.
     */
    public TimeWindowDAOImplRoot() {
        super(TimeWindow.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.TimeWindowDAO#findByViewIdAndUser(com.vocollect.epp.model.User, long)
     */
    public TimeWindow getByViewIdAndUser(User user, long viewId) throws DataAccessException {

        Query qry = getCurrentSession().getNamedQuery("TimeWindow_findByViewIdAndUser");
        qry.setParameter(0 , viewId);
        qry.setParameter(1 , user.getId());
        TimeWindow tw = (TimeWindow) qry.uniqueResult();
       return tw;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.TimeWindowDAO#findByIdAndUser(com.vocollect.epp.model.User, long)
     */
    public TimeWindow getByViewId(long viewId) throws DataAccessException {

        Query qry = getCurrentSession().getNamedQuery("TimeWindow_findByViewIdForDefault");
        qry.setParameter(0 , viewId);
        TimeWindow tw = (TimeWindow) qry.uniqueResult();
        return tw;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.TimeWindowDAORoot#listAllByUserId(long)
     */
    public List<TimeWindow> listAllByUserId(long userId) {
        Query qry = getCurrentSession().getNamedQuery("TimeWindow_findAllByUserId");
        qry.setParameter(0, userId);
        return qry.list();
    }
}




*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 