/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Maps an enum value to a specified property of the enum type.
 * @param <E> The enum type to reverse map
 *
 * @author jvolz
 */
public class DynamicReverseEnumMap<E extends Enum<E>> {
    private Map<Object, E> map = new HashMap<Object, E>();

    /**
     * Constructor.
     * @param enumType the type to map.
     * @param variableName the property to map.
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    public DynamicReverseEnumMap(Class<E> enumType, String variableName) {
        String expectedMethodName = "get"
            + Character.toUpperCase(variableName.charAt(0));

        if (variableName.length() > 1) {
            expectedMethodName += variableName.substring(1);
        }

        try {
            for (E e : enumType.getEnumConstants()) {
                map.put(e.getClass().getMethod(expectedMethodName).invoke(e, (Object[]) null), e);
            }
        } catch (NoSuchMethodException e2) {
            throw new RuntimeException("Expected the public method "
                + expectedMethodName
                + " with no arguments but it doesn't exist.", e2);
        } catch (SecurityException e1) {
            // Don't know what happened.
            throw e1;
        } catch (IllegalArgumentException e3) {
            // This shouldn't happen.  We make sure the method doesn't require and arguments.
            throw e3;
        } catch (IllegalAccessException e4) {
            // This shouldn't happen.  We check that the method is public.
            throw new RuntimeException(e4);
        } catch (InvocationTargetException e5) {
            // Don't know what happened.
            throw new RuntimeException(e5);
        }
    }

    /**
     * @param obj the key
     * @return the associated Enum constant.
     * @throws IllegalArgumentException if the num doesn't map to a constant.
     */
    public E get(Object obj) throws IllegalArgumentException {
        E value = map.get(obj);
        if (value ==  null) {
            // Get a sample enum constant to work with.
            E sampleEnum = map.values().iterator().next();
            throw new IllegalArgumentException(
                "Obj " + obj + " does not map to value in "
                + sampleEnum.getClass());
        } else {
            return map.get(obj);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 