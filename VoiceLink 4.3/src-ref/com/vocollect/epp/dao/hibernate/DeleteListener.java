/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.model.User;

import org.hibernate.HibernateException;
import org.hibernate.event.DeleteEvent;
import org.hibernate.event.def.DefaultDeleteEventListener;

/**
 * A Hibernate event class to run before deleting of objects.
 *
 * @author dkertis
 */
public class DeleteListener extends DefaultDeleteEventListener {

    //
    private static final long serialVersionUID = 8614092112952529757L;

    /**
     * Called when hibernate is about to delete an object.  This method deletes all
     *  userColumn associated with the user about to be deleted.
     *  @param e contains info about the delete
     *  @throws HibernateException - when a hibernate error occurs
     */
    @Override
    public void onDelete(DeleteEvent e) throws HibernateException {
        if (e.getObject() instanceof User) {
            User u = (User) e.getObject();

            e.getSession().getNamedQuery("UserColumn_deleteByUserId").setLong(
                "userId", u.getId()).executeUpdate();
            e.getSession().flush();
        }
        super.onDelete(e);
    }


}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 