/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.logging.Logger;

import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;


/**
 * A class to allow for logging of detailed Hibernate activity reporting.
 * If enabled, it can dump the information to the log files at the INFO
 * level.
 *
 * @author ddoubleday
 */
public class HibernateStatsReporter implements BeanFactoryPostProcessor {

    private static final Logger log = new Logger(HibernateStatsReporter.class);

    private SessionFactory sessionFactory;

    private boolean statsEnabled = false;

    // Bean name associated with this object.
    public static final String HIBERNATE_STATS_BEAN_NAME = "hibernateStats";

    /**
     * Getter for the statsEnabled property.
     * @return boolean value of the property
     */
    public boolean getStatsEnabled() {
        return this.statsEnabled;
    }

    /**
     * Setter for the statsEnabled property.
     * @param statsEnabled the new statsEnabled value
     */
    public void setStatsEnabled(boolean statsEnabled) {
        this.statsEnabled = statsEnabled;
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Report the statistics associated with the session factory.
     */
    public void reportStats() {

        Statistics stats = getSessionFactory().getStatistics();
        // Number of flushes done on the session (either by client code or
        // by hibernate).
        log.info("Flush count: " + stats.getFlushCount());
        // The number of completed transactions (failed and successful).
        log.info("Transaction count: " + stats.getTransactionCount());
        // The number of transactions completed without failure
        log.info("Successful transaction count: "
            + stats.getSuccessfulTransactionCount());
        // The number of sessions your code has opened.
        log.info("Session open count: " + stats.getSessionOpenCount());
        // The number of sessions your code has closed.
        log.info("Session close count: " + stats.getSessionCloseCount());
        // All of the queries that have executed.
        // log.info("Queries: " + stats.getQueries());
        // Total number of queries executed.
        log.info("Query execution count: " + stats.getQueryExecutionCount());
        // Time of the slowest query executed.
        log.info("Longest query time: " + stats.getQueryExecutionMaxTime());

        // the number of collections fetched from the DB.
        log.info("Collection fetch count: " + stats.getCollectionFetchCount());
        // The number of collections loaded from the DB.
        log.info("Collection load count: " + stats.getCollectionLoadCount());
        // The number of collections that were rebuilt
        log.info("Collection recreate count: "
            + stats.getCollectionRecreateCount());
        // The number of collections that were 'deleted' batch.
        log.info("Collection deletion count: "
            + stats.getCollectionRemoveCount());
        // The number of collections that were updated batch.
        log.info("Collection update count: " + stats.getCollectionUpdateCount());

        // The number of your objects deleted.
        log.info("Entity delete count: " + stats.getEntityDeleteCount());
        // The number of your objects fetched.
        log.info("Entity fetch count: " + stats.getEntityFetchCount());
        // The number of your objects actually loaded (fully populated).
        log.info("Entity load count: " + stats.getEntityLoadCount());
        // The number of your objects inserted.
        log.info("Entity insert count: " + stats.getEntityInsertCount());
        // The number of your object updated.
        log.info("Entity update count: " + stats.getEntityUpdateCount());

        long cacheHitCount = stats.getSecondLevelCacheHitCount();
        long cacheMissCount = stats.getSecondLevelCacheMissCount();
        long cachePutCount = stats.getSecondLevelCachePutCount();

        log.info("Cache hit count: " + cacheHitCount);
        log.info("Cache miss count: " + cacheMissCount);
        log.info("Cache put count: " + cachePutCount);
        if (cacheHitCount + cacheMissCount > 0) {
            log.info("Cache hit ratio: "
                + (cacheHitCount / (cacheHitCount + cacheMissCount)));
        }

        long queryCacheHitCount = stats.getQueryCacheHitCount();
        long queryCacheMissCount = stats.getQueryCacheMissCount();

        log.info("Query cache hit count: " + queryCacheHitCount);
        log.info("Query cache miss count: " + queryCacheMissCount);
        if (queryCacheHitCount + queryCacheMissCount > 0) {
            log.info("Query cache hit ratio: "
                + (queryCacheHitCount / (queryCacheHitCount + queryCacheMissCount)));
        }
    }

    /**
     * {@inheritDoc}
     * @see org.springframework.beans.factory.config.BeanFactoryPostProcessor#postProcessBeanFactory(org.springframework.beans.factory.config.ConfigurableListableBeanFactory)
     */
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory)
        throws BeansException {

        if (getStatsEnabled()) {
            getSessionFactory().getStatistics().setStatisticsEnabled(true);
        }
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 