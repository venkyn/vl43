/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Sharable;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.QueryUtil;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

/**
 * A class to support the table component. Returns the data necessary for
 * rendering.
 * @param <T> the generic type we are providing data for.
 * @author dkertis
 * @author ddoubleday -- mods to use GenericDAO pattern.
 */
public abstract class DataProviderDaoImplRoot<T extends BaseModelObject>
    implements DataProviderDAO {

    private static final Logger log = new Logger(DataProviderDaoImpl.class);

    /**
     * Return the Class of the generic object for a particular instance of the
     * DAO.
     * @return the Class object
     */
    abstract Class<T> getPersistentClass();

    /**
     * @return the generic persistent class name (used in constructing
     *         queries)
     */
    protected abstract String getPersistentClassName();

    /**
     * @return the current Hibernate session.
     */
    protected abstract Session getCurrentSession();

    /**
     * @return whether or not the persistent class is a Taggable.
     */
    protected boolean isTaggable() {
        return Taggable.class.isAssignableFrom(getPersistentClass());
    }

    /**
     * @return whether or not the persistent class is a Sharable.
     */
    protected boolean isSharable() {
        return Sharable.class.isAssignableFrom(getPersistentClass());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.DataProviderDAO#getAll(com.vocollect.epp.util.ResultDataInfo)
     * @return (long) the data count
     * @throws DataAccessException for any error
     */
    @SuppressWarnings("unchecked")
    public List getDataByFieldAndValue(String fieldName, String value)
        throws DataAccessException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // Construct base query
        String hql = "select distinct obj." + fieldName  + " from " + getPersistentClassName()
            + " obj where obj." + fieldName + " like '" + value + "%' order by obj." + fieldName +  " asc";

        Query query = getCurrentSession()
            .createQuery(hql);

        if (siteContext != null && isTaggable() && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            query = QueryUtil.addQuerySiteRestriction(
                siteContext, getCurrentSession(), query.getQueryString(),
                isSharable());
        }

        if (log.isTraceEnabled()) {
            log.trace("Running query: " + query.getQueryString());
        }

        return query.list();

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.DataProviderDAO#getAll(com.vocollect.epp.util.ResultDataInfo)
     * @return (long) the data count
     * @throws DataAccessException for any error
     */
    @SuppressWarnings("unchecked")
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // Construct base query
        String hql = "select obj from " + getPersistentClassName()
            + " obj";

        hql = QueryUtil.addSortingTransform(getPersistentClass(), rdi
            .getSortColumnObject(), rdi.isSortAscending(), hql);

        // Apply any specified where clause transformation.
        hql = QueryUtil.addQueryRestriction(rdi.getWhereClause(), hql);

        hql = QueryUtil.addFilterRestriction(
            rdi.getFilter(), getCurrentSession(), hql);

        Query query = getCurrentSession()
            .createQuery(hql + getOrderClause(rdi));

        if (siteContext != null && isTaggable() && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            query = QueryUtil.addQuerySiteRestriction(
                siteContext, getCurrentSession(), query.getQueryString(),
                isSharable());
        }

        // Because the Query for the order clause create a new Query object -
        // the
        // named parameters that we set in the addFilterRestriction are no
        // longer
        // set - therefore we need to set the parameters explictly
        if (rdi.getFilter() != null) {
            rdi.getFilter().fillInNamedParameters(query);
        }

        if (log.isTraceEnabled()) {
            log.trace("Running query: " + query.getQueryString());
        }

        return query.list();

    }

    /**
     * @param rdi specifies the sort column and sort order
     * @return an "order by" clause based upon sort column and order in the rdi,
     *         or the empty String if no sort is set.
     */
    protected String getOrderClause(ResultDataInfo rdi) {
        final String issuanceOrder = "issuanceOrder";
        if (rdi.getSortColumn() != null) {
            String orderClause = " order by ";
            // TODO: Possibly re-work this solution
            // After many discussions, this was the solution for the descending
            // sort issue with the component object issuanceOrder
            if (rdi.getSortColumn().endsWith(issuanceOrder)) {
                String prefix = "";
                if (rdi.getSortColumn().indexOf(issuanceOrder) != 0) {
                    prefix = rdi.getSortColumn().substring(0,
                        rdi.getSortColumn().indexOf(issuanceOrder));
                }
                if (rdi.isSortAscending()) {
                    orderClause += prefix + issuanceOrder + ".priority desc, "
                                + prefix + issuanceOrder + ".sequenceNumber";
                    orderClause += " asc";
                } else {
                    orderClause += prefix + issuanceOrder + ".priority asc, "
                                + prefix + issuanceOrder + ".sequenceNumber";
                    orderClause += " desc";
                }
                return orderClause;
            }
        }
        return QueryUtil.getOrderClause(rdi);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 