/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.RemovableDataProviderDAO;
import com.vocollect.epp.dao.exceptions.BatchDataAccessException;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dao.hibernate.finder.ExtendedFinderNamingStrategy;
import com.vocollect.epp.dao.hibernate.finder.FinderArgumentTypeFactory;
import com.vocollect.epp.dao.hibernate.finder.FinderExecutor;
import com.vocollect.epp.dao.hibernate.finder.FinderNamingStrategy;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.dao.hibernate.finder.SimpleFinderArgumentTypeFactory;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.BaseModelObject;
import com.vocollect.epp.model.Taggable;
import com.vocollect.epp.util.QueryUtil;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.type.Type;

/**
 * This is generic DAO interface that handles basic functions that most DAO
 * instances will need.
 * @param <T> the type to operate on
 *            <p>
 *            This class uses code and ideas from two sources:
 *            <ul>
 *            <li>{@see <a href="http://www-128.ibm.com/developerworks/java/library/j-genericdao.html?ca=drs">
 *            Don't Repeat the DAO! by Per Mellqvist</a> }
 *            <li>{@see <a href="http://blog.hibernate.org/cgi-bin/blosxom.cgi/2005/09/08#genericdao">
 *            Generic DAO pattern with JDK 5.0 by Christian Bauer</a>}
 *            </ul>
 *
 * @author ddoubleday
 */
public abstract class GenericDAOImplRoot<T extends BaseModelObject>
    extends DataProviderDaoImpl<T>
    implements RemovableDataProviderDAO, GenericDAO<T>, FinderExecutor<T> {

    private static Logger log = new Logger(GenericDAOImpl.class);

    // private static final int DEFAULT_SAVE_FLUSH_COUNT = 1;

    // Default value--can be injected if need be.
    private FinderArgumentTypeFactory argumentTypeFactory = new SimpleFinderArgumentTypeFactory();

    // Default value--can be injected if need be.
    private FinderNamingStrategy namingStrategy = new ExtendedFinderNamingStrategy();

    private Class<T> persistentClass;

    private SessionFactory sessionFactory;

    // Number of elements to save before calling flush. Used in save(List<T>)
    //private int bulkSaveBeforeFlushCount = DEFAULT_SAVE_FLUSH_COUNT;

    /**
     * Constructor that determines the type of object the generic DAO will work
     * with.
     * @param type the generic class associated with the generic T
     */
    public GenericDAOImplRoot(Class<T> type) {
        this.persistentClass = type;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(Long id) throws DataAccessException {
        delete(get(id));

    }

    /**
     * Determine if the current user has access to the instance.
     * @param persistentInstance instance to check against.
     * @return true if the user has access, false otherwise.
     */
    protected boolean hasAccess(T persistentInstance) {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        if (siteContext.isHasAllSiteAccess()
            || (siteContext.getSiteCountInContext() == 0)) {
            // If the user has all site access or this is not being
            // run from UI space, allow access.
            return true;
        }
        Taggable taggable = (Taggable) persistentInstance;
        return siteContext.userHasAccess(taggable);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#delete(java.io.Serializable)
     */
    @Override
    public void delete(T persistentInstance) throws DataAccessException {
        if (isTaggable()) {
            if (!hasAccess(persistentInstance)) {
                log.warn("Unauthorized site access to delete object of type "
                    + getPersistentClassName() + "with id "
                    + persistentInstance.getId());
                throw new DataAccessException(
                    SystemErrorCode.UNAUTHORIZED_SITE_DELETE, new UserMessage(
                        "site.delete.unauthorized"), null);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Deleting " + getPersistentClass().getSimpleName() + ": "
                + persistentInstance.getDescriptiveText());
        }
        getCurrentSession().delete(persistentInstance);
        getCurrentSession().flush();
        if (log.isDebugEnabled()) {
            log.debug("Deleted " + getPersistentClass().getSimpleName() + ": "
                + persistentInstance.getDescriptiveText());
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeAggregateFunction(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    public Object executeAggregateFunction(Method method,
                                           final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return namedQuery.uniqueResult();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeIterateFinder(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public Iterator<T> executeIterateFinder(Method method,
                                            final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return namedQuery.iterate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeIdFinder(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public Long executeIdFinder(Method method, final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return (Long) namedQuery.uniqueResult();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeListFinder(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> executeListFinder(Method method, final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return namedQuery.list();
    }

    /**
     * Execute the list finder and filter out duplicate references in the result set.  Duplicate references can happen when
     * forced fetch joins are utilized in a query - Hibernate developers consider this normal behavior
     * <p/>
     * Execute the specified method. This method will be called via an AOP interceptor.
     *
     * @param method the method to execute
     * @param queryArgs the arguments to plug in to the query
     * @return The list returned by the query, which may be empty.
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> executeDistinctListFinder(Method method, Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        namedQuery.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return namedQuery.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeReportQuery(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<Object[]> executeReportQuery(Method method,
                                             final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return namedQuery.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeUniqueFinder(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    @SuppressWarnings("unchecked")
    public T executeUniqueFinder(Method method, final Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return (T) namedQuery.uniqueResult();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeUpdate(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    public int executeUpdate(Method method, Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, false);
        return namedQuery.executeUpdate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeSQLUpdate(java.lang.reflect.Method, java.lang.Object[])
     */
    @Override
    public int executeSQLUpdate(Method method, Object[] queryArgs) {
        final SQLQuery namedQuery = prepareSQLQuery(method, queryArgs);
        return namedQuery.executeUpdate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.finder.FinderExecutor#executeUpdate(java.lang.reflect.Method,
     *      java.lang.Object[])
     */
    @Override
    public int executeUpdateWithTextualParamReplacement(Method method,
                                                        Object[] queryArgs) {
        final Query namedQuery = prepareQuery(method, queryArgs, true);
        return namedQuery.executeUpdate();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#get(java.io.Serializable)
     */
    @Override
    @SuppressWarnings("unchecked")
    public T get(Long id) throws DataAccessException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        if (siteContext == null) {
            log.warn("SiteContext is not set");
        }

        // get all users from the DB
        String hql = null;

        // start with a simple select
        hql = "select obj from " + getPersistentClassName()
            + " obj where obj.id=:id";

        // if the object is taggable, make sure the user has access to it
        if ((siteContext != null) && isTaggable() && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                if (isSharable()) {
                    hql = "select distinct obj from "
                        + getPersistentClassName() + " obj";
                    hql += " where obj.common.tags.id in (:tagId) and id=:id";
                } else {
                    hql = "select distinct obj from "
                        + getPersistentClassName() + " obj";
                    hql += " where obj.tags.id in (:tagId) and id=:id";
                }
            }
        }
        Query query = getCurrentSession().createQuery(hql);

        if ((siteContext != null) && isTaggable() && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                if (siteContext.getSiteCountInContext() == 0) {
                    log.warn("SiteContext has no site tag IDs!");
                }
                query.setParameterList("tagId", siteContext.getTagIdsForQuery());
            }
        }
        // bind the id parameter
        query.setParameter("id", id);
        // now do the query and make sure there is only one result
        T t = (T) query.uniqueResult();

        // if the object is not found, throw an exception
        if (t == null) {
            throw new EntityNotFoundException(getPersistentClass(), id);
        }
        return t;

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#getAll()
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> getAll() throws DataAccessException {
        return queryByCriteria();
    }

    /**
     * Return a factory that potentially associates Hibernate custom types with
     * Finder method arguments.
     * @return the factory for argument types
     */
    public FinderArgumentTypeFactory getArgumentTypeFactory() {
        return argumentTypeFactory;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#getCount()
     */
    @Override
    public long getCount() throws DataAccessException {
        String hql = "select count(*) from " + getPersistentClassName();
        Long count = (Long) getCurrentSession().createQuery(hql).uniqueResult();
        return count.longValue();
    }

    /**
     * Convenience method to access the current session.
     * @return current Hibernate Session object
     */
    @Override
    protected Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
    }

    /**
     * Returns a naming strategy object that is used to map called method names
     * to Hibernate named queries.
     * @return the naming strategy object
     */
    public FinderNamingStrategy getNamingStrategy() {
        return namingStrategy;
    }

    /**
     * Return the Class of the generic object for a particular instance of the
     * DAO.
     * @return the Class object
     */
    @Override
    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    /**
     * Return the fully qualified name of the class of the generic object for
     * a particular instance of the DAO.
     * @return the Class name
     */
    @Override
    protected String getPersistentClassName() {
        return this.persistentClass.getName();
    }

    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Get a Hibernate Query object from the named query associated by
     * convention with the called method.
     * @param method the interface method that was called
     * @param queryArgs the arguments passed in the call
     * @param textualParameterReplacement true if parameters should be replaced
     *            via textual substitution, false if normal Hibernate positional
     *            or named parameter substitution should be used.
     * @return the Query object that implements the method.
     */
    private Query prepareQuery(Method method,
                               Object[] queryArgs,
                               boolean textualParameterReplacement) {

        final String queryName = getNamingStrategy().queryNameFromMethod(
            this.persistentClass, method);

        Query namedQuery = getCurrentSession().getNamedQuery(queryName);
        String[] namedParameters = namedQuery.getNamedParameters();

        if (textualParameterReplacement && (namedParameters.length == 0)) {
            namedQuery = replaceParameters(namedQuery, queryArgs);
        }

        Object[] newArgs = queryArgs;

        // Check for QueryDecorator specifier as first argument, and modify
        // the query as needed.
        if ((queryArgs != null) && (queryArgs.length > 0)) {
            if (queryArgs[0] instanceof QueryDecorator) {

                // The first argument is query decorator information. Use it
                // and remove it from the list of args.
                QueryDecorator decorator = (QueryDecorator) queryArgs[0];
                if (queryArgs.length == 1) {
                    // There are no additional arguments.
                    newArgs = null;
                } else {
                    // Move all args up one slot.
                    newArgs = new Object[queryArgs.length - 1];
                    System.arraycopy(queryArgs, 1, newArgs, 0, newArgs.length);
                }

                // Get the HQL for the base query.
                String newHQL = namedQuery.getQueryString();

                // Apply any sorting transformation
                newHQL = QueryUtil.addSortingTransform(
                    getPersistentClass(), decorator.getSortColumnObject(),
                    decorator.isSortAscending(), newHQL);

                // Apply any specified where clause transformation.
                newHQL = QueryUtil.addQueryRestriction(decorator
                    .getWhereClause(), newHQL);

                // Apply filter restriction transformation
                newHQL = QueryUtil.addFilterRestriction(
                    decorator.getFilter(), getCurrentSession(), newHQL);

                // Now create a new Query from the (possibly) modified HQL.
                namedQuery = getCurrentSession().createQuery(
                    newHQL.trim() + getOrderClause(decorator));

            }
        }

        SiteContext siteContext = SiteContextHolder.getSiteContext();
        // Apply site restriction transformation
        if ((siteContext != null) && isTaggable() && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            namedQuery = QueryUtil.addQuerySiteRestriction(
                siteContext, getCurrentSession(), namedQuery.getQueryString(),
                isSharable());
        }

        // Because the Query for the order clause create a new Query object -
        // the
        // named parameters that we set in the addFilterRestriction are no
        // longer
        // set - therefore we need to set the parameters explictly
        if ((queryArgs != null) && (queryArgs.length > 0)) {
            if (queryArgs[0] instanceof QueryDecorator) {

                // The first argument is query decorator information. Use it
                // and remove it from the list of args.
                QueryDecorator decorator = (QueryDecorator) queryArgs[0];
                if (decorator.getFilter() != null) {
                    decorator.getFilter().fillInNamedParameters(namedQuery);
                }

                // Now see if there row restrictions requested.
                if (decorator.getStartRow() != 0) {
                    namedQuery.setFirstResult(decorator.getStartRow());
                }
                if (decorator.getRowCount() != 0) {
                    namedQuery.setMaxResults(decorator.getRowCount());
                }

            }

        }

        if (!textualParameterReplacement) {

            // Textual replacement would have been done above. If not, then
            // normal positional or named parameter replacement is done here.
            if (namedParameters.length == 0) {
                setPositionalParams(newArgs, namedQuery);
            } else {
                setNamedParams(namedParameters, newArgs, namedQuery);
            }
        }

        if (log.isTraceEnabled()) {
            log.trace("Running query: " + namedQuery.getQueryString());
        }

        return namedQuery;
    }

    /**
     * Get a Hibernate SQLQuery object from the named query associated by
     * convention with the called method. Note that this does none of
     * dynamic tagging and filtering that the prepareQuery() method does.
     * All it will do is parameter replacement. SQL queries should only be
     * used in very limited situations, and only if the code is standard
     * SQL that is database-independent.
     * @param method the interface method that was called
     * @param queryArgs the arguments passed in the call
     * @return the SQLQuery object that implements the method.
     */
    private SQLQuery prepareSQLQuery(Method method,
                                     Object[] queryArgs) {

        final String queryName = getNamingStrategy().queryNameFromMethod(
            this.persistentClass, method);

        SQLQuery namedQuery = (SQLQuery) getCurrentSession().getNamedQuery(queryName);
        String[] namedParameters = namedQuery.getNamedParameters();

        // Textual replacement would have been done above. If not, then
        // normal positional or named parameter replacement is done here.
        if (namedParameters.length == 0) {
            setPositionalParams(queryArgs, namedQuery);
        } else {
            setNamedParams(namedParameters, queryArgs, namedQuery);
        }

        if (log.isTraceEnabled()) {
            log.trace("Running query: " + namedQuery.getQueryString());
        }

        return namedQuery;
    }
    /**
     * Do a textual replacement of the positional parameters in the in the named
     * query.
     * @param namedQuery the named query we're running
     * @param queryArgs the arguments to the query
     * @return a new Query, with the positional parameters of the original query
     *         replaced, via textual replacement only, by the queryArgs
     */
    private Query replaceParameters(Query namedQuery, Object[] queryArgs) {

        String queryString = namedQuery.getQueryString();
        StringBuilder buf = new StringBuilder();
        int startIndex = 0;
        int paramIndex = 0;
        for (Object arg : queryArgs) {
            if (!(arg instanceof QueryDecorator)) {
                // Don't do anything with the QueryDecorator
                paramIndex = queryString.indexOf('?', startIndex);
                if (paramIndex == -1) {
                    throw new IllegalArgumentException(
                        "Invalid parameter count");
                }
                buf.append(queryString.substring(startIndex, paramIndex - 1));
                buf.append(arg.toString());
                startIndex = paramIndex + 1;
            }
        }

        if (startIndex <= queryString.length()) {
            buf.append(queryString.substring(startIndex));
        }

        return getCurrentSession().createQuery(buf.toString());
    }

    /**
     * Use this inside subclasses as a convenience method.
     * @param criterion list of Criteria to query by
     * @return the list of matching instances of T
     */
    @SuppressWarnings("unchecked")
    protected List<T> queryByCriteria(Criterion... criterion) {
        Criteria crit = getCurrentSession()
            .createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#queryByExample(java.lang.Object)
     */
    @Override
    @SuppressWarnings("unchecked")
    public List<T> queryByExample(T exampleInstance) throws DataAccessException {
        return queryByCriteria(Example.create(exampleInstance));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)
     */
    @Override
    public void save(T object) throws DataAccessException {
        boolean isNew = object.isNew();

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // if the object is being created and is a taggable object
        // get the current site from the context and add it to the object

        if (object.isNew() && (object instanceof Taggable)
            && ((((Taggable) object).getTags() == null)
                || ((Taggable) object).getTags().isEmpty())) {

            if (log.isDebugEnabled()) {
                log.debug("Adding a taggable site");
            }

            // TODO Cannot get spring to inject the tagDAO into generic
            // so I am working around this by using siteContext
            Taggable taggable = (Taggable) object;
            siteContext.setSiteToCurrentSite(taggable);
        }
        getCurrentSession().saveOrUpdate(object);

        getCurrentSession().flush();
        if (log.isDebugEnabled()) {
            log.debug(((isNew ? "Created " : "Updated ")
                + getPersistentClass().getSimpleName() + " '"
                + object.getDescriptiveText() + "'"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#saveAll(java.lang.Object)
     */
    @Override
    public void save(List<T> object) throws DataAccessException {
        boolean isNew = false;
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        Session currentSession = getCurrentSession();

        // Get this here once.
//        Long tagId = siteContext.getSiteTagId();
//        Tag siteTag = siteContext.getTagById(tagId);

        for (int i = 0; i < object.size(); i++) {
            try {
                isNew = object.get(i).isNew();
                // if the object is being created and is a taggable object
                // add the current Site tag to the object
                if (object.get(i).isNew()
                    && (object.get(i) instanceof Taggable)) {
                    Taggable taggable = (Taggable) object.get(i);
                    siteContext.setSiteToCurrentSite(taggable);
//                    Set<Tag> tags = new HashSet<Tag>();
//                    tags.add(siteTag);
//                    taggable.setTags(tags);
                }

                currentSession.saveOrUpdate(object.get(i));
                // if ((i % bulkSaveBeforeFlushCount) == 0) {
                currentSession.flush();
                // }
                if (log.isDebugEnabled()) {
                    log.debug(((isNew ? "Created " : "Updated ")
                        + getPersistentClass().getSimpleName() + "'"
                        + object.get(i).getDescriptiveText() + "'"));
                }
                currentSession.evict(object.get(i));
            } catch (Exception e) {
                throw new BatchDataAccessException(e, i);
            }
        }

        // Not necessary if it is being done every time above.
        // currentSession.flush();
    }

    /**
     * Set a factory that potentially associates Hibernate custom types with
     * Finder method arguments.
     * @param argumentTypeFactoryParam the factory object.
     */
    public void setArgumentTypeFactory(FinderArgumentTypeFactory argumentTypeFactoryParam) {
        this.argumentTypeFactory = argumentTypeFactoryParam;
    }

    /**
     * Bind data to the Query using the named parameters.
     * @param namedParameters the named parameters defined in the Query
     * @param queryArgs the data passed in the method call
     * @param namedQuery the Query object
     */
    private void setNamedParams(String[] namedParameters,
                                Object[] queryArgs,
                                Query namedQuery) {

        if (namedParameters != null) {
            for (String param : namedParameters) {

                // Extract the argument index from the parameter name. If
                // there is more than one named parameter, then the named
                // parameters must follow our naming convention that allows
                // for mapping multiple args to the named parameters in a
                // reliable way. If there is only one, it doesn't matter how
                // the parameter is named.
                int argIndex;
                if (namedParameters.length > 1) {
                    try {
                        argIndex = Integer.parseInt(param.substring(param
                            .length() - 1)) - 1;

                    } catch (NumberFormatException e) {
                        // Our convention is the named parameter in the query
                        // must start with a number to support auto-mapping
                        // method arguments.
                        throw new IllegalArgumentException(
                            "Named query parameter " + param
                                + " must end with a number");
                    }
                } else {
                    argIndex = 0;
                }

                Object arg = queryArgs[argIndex];
                Type argType = getArgumentTypeFactory().getArgumentType(arg);
                if (argType != null) {
                    // A custom Hibernate type was specified for this argument,
                    // so use it.
                    namedQuery.setParameter(param, arg, argType);
                } else {
                    if (arg instanceof Collection) {
                        namedQuery.setParameterList(param, (Collection<?>) arg);
                    } else {
                        namedQuery.setParameter(param, arg);
                    }
                }
            }
        }
    }

    /**
     * Set a naming strategy object that is used to map called method names to
     * Hibernate named queries.
     * @param namingStrategyParam the strategy object
     */
    public void setNamingStrategy(FinderNamingStrategy namingStrategyParam) {
        this.namingStrategy = namingStrategyParam;
    }

    /**
     * Bind data to the Query using positional parameters.
     * @param queryArgs the data passed in the method call
     * @param namedQuery the Query object
     */
    private void setPositionalParams(Object[] queryArgs, Query namedQuery) {
        // Set parameter. Use custom Hibernate Type if necessary
        if (queryArgs != null) {
            for (int i = 0; i < queryArgs.length; i++) {
                Object arg = queryArgs[i];
                Type argType = getArgumentTypeFactory().getArgumentType(arg);
                if (argType != null) {
                    namedQuery.setParameter(i, arg, argType);
                } else {
                    namedQuery.setParameter(i, arg);
                }
            }
        }
    }

    /**
     * Setter for the sessionFactory property.
     * @param sessionFactoryParam the new sessionFactory value.
     */
    public void setSessionFactory(SessionFactory sessionFactoryParam) {
        this.sessionFactory = sessionFactoryParam;
    }

    /**
     * @param decorator specifies the sort column and sort order
     * @return an "order by" clause based upon sort column and order in the rdi,
     *         or the empty String if no sort is set.
     */
    protected String getOrderClause(QueryDecorator decorator) {
        String locationDescription = "location.description";
        String issuanceOrder = "issuanceOrder";
        if (decorator.getSortColumn() != null) {
            String orderClause = " order by ";
            // TODO: Possibly re-work this solution
            // After many discussions, this was the solution for the descending
            // sort issue with the component object locationDescription
            // TODO: Don't know who put this code here, but this won't
            // work at all when preAisle and postAisle use localized translations.
            // -- ddoubleday
            if (decorator.getSortColumn().endsWith(locationDescription)
                && !decorator.isSortAscending()) {
                String prefix = "";
                if (decorator.getSortColumn().indexOf(locationDescription) != 0) {
                    prefix = decorator.getSortColumn().substring(0,
                        decorator.getSortColumn().indexOf(locationDescription));
                }
                orderClause += prefix + locationDescription + ".preAisle desc, "
                            + prefix + locationDescription + ".aisle desc, "
                            + prefix + locationDescription + ".postAisle desc, "
                            + prefix + locationDescription + ".slot";
                orderClause += " desc";
                return orderClause;
            }
            if (decorator.getSortColumn().endsWith(issuanceOrder)
                && !decorator.isSortAscending()) {
                String prefix = "";
                if (decorator.getSortColumn().indexOf(issuanceOrder) != 0) {
                    prefix = decorator.getSortColumn().substring(0,
                        decorator.getSortColumn().indexOf(issuanceOrder));
                }
                orderClause += prefix + issuanceOrder + ".priority desc, "
                            + prefix + issuanceOrder + ".sequenceNumber";
                orderClause += " desc";
                return orderClause;
            }
        }
        return QueryUtil.getOrderClause(decorator);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#refresh(java.lang.Object)
     */
    @Override
    public void refresh(BaseModelObject persistentObject) throws DataAccessException {
        getCurrentSession().refresh(persistentObject);
        getCurrentSession().flush();
        if (log.isDebugEnabled()) {
            log.debug(("Refreshed " + getPersistentClass().getSimpleName()
                + "'" + persistentObject.getDescriptiveText() + "'"));
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAORoot#clearSession()
     */
    @Override
    public void clearSession() {
        getCurrentSession().clear();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAORoot#flushSession()
     */
    @Override
    public void flushSession() {
        getCurrentSession().flush();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAORoot#reattach(java.lang.Object)
     */
    @Override
    public void reattach(T persistentObject) {
        getCurrentSession().update(persistentObject);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAORoot#detach(java.lang.Object)
     */
    @Override
    public void detach(T persistentObject) {
        getCurrentSession().evict(persistentObject);
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 