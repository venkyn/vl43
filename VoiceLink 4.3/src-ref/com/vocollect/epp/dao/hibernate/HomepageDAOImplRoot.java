/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.HomepageDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.User;

import java.util.List;

import org.hibernate.Query;


/**
 * Implementation of HomepageDAO.
 *
 * @author svoruganti
 */

public abstract class HomepageDAOImplRoot extends GenericDAOImpl<Homepage> implements HomepageDAO {
    // private static Logger log = new Logger(HomepageDAOImpl.class);

    /**
     * Constructor.
     */
    public HomepageDAOImplRoot() {
        super(Homepage.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.HomepageDAO#findByIdAndUser(com.vocollect.epp.model.User, long)
     */
    public Homepage getByIdAndUser(User user, long homepageId) throws DataAccessException {

        Query qry = getCurrentSession().getNamedQuery("Homepage_findByIdAndUser");
        qry.setParameter(0 , homepageId);
        qry.setParameter(1 , user.getId());
        Homepage hm = (Homepage) qry.uniqueResult();
       return hm;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.HomepageDAO#findByIdAndUser(com.vocollect.epp.model.User, long)
     */
    public Homepage getByHomepageId(long homepageId) throws DataAccessException {

        Query qry = getCurrentSession().getNamedQuery("Homepage_findByHomepageIdForDefault");
        qry.setParameter(0 , homepageId);
        Homepage hm = (Homepage) qry.uniqueResult();
        return hm;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.HomepageDAORoot#listAllByUserId(long)
     */
    public List<Homepage> listAllByUserId(long userId) {
        Query qry = getCurrentSession().getNamedQuery("Homepage_findAllByUserId");
        qry.setParameter(0, userId);
        return qry.list();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 