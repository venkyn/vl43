/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import java.util.HashMap;
import java.util.Map;


/**
 * A map object that holds Enum instances keyed by their Integer value
 * representation. The "value" is different from the Enum ordinal, so
 * only Enums that implement
 * {@link com.vocollect.epp.dao.hibernate.ValueBasedEnum} can be mapped.
 *
 * @param <E> the Enum type to put in the map.
 *
 * @author ddoubleday
 */
public class ReverseEnumMap<E extends Enum<E> & ValueBasedEnum<E>> {

    private Map<Integer, E> map = new HashMap<Integer, E>();

    /**
     * Constructor.
     * @param enumType the type to map.
     */
    public ReverseEnumMap(Class<E> enumType) {
        for (E e : enumType.getEnumConstants()) {
            map.put(e.toValue(), e);
        }
    }

    /**
     * @param num the key
     * @return the associated Enum constant.
     * @throws IllegalArgumentException if the num doesn't map to a constant.
     */
    public E get(int num) throws IllegalArgumentException {
        E value = map.get(num);
        if (value ==  null) {
            // Get a sample enum constant to work with.
            E sampleEnum = map.values().iterator().next();
            throw new IllegalArgumentException(
                "Number " + num + " does not map to value in "
                + sampleEnum.getClass());
        } else {
            return map.get(num);
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 