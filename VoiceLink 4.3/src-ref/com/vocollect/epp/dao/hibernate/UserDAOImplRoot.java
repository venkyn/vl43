/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;

/**
 *
 *
 * @author dkertis
 */
public abstract class UserDAOImplRoot extends GenericDAOImpl<User> implements
    UserDAO {

    private static Logger log = new Logger(UserDAOImpl.class);

    /**
     * Constructor.
     */
    public UserDAOImplRoot() {
        super(User.class);
    }

    /**
     * Implemented by dynamic finder query. {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAO#findByName(java.lang.String)
     */
    public User findByName(String username) throws DataAccessException {
        return null;
    }

    /**
     * Implemented by dynamic finder query. {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAO#listBySite()
     */
    public List<User> listBySite() throws DataAccessException {
        return null;
    }

    /**
     * Implemented by dynamic finder query. {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAO#countDatabaseAuthenticatedAdministrators()
     */
    public Number countDatabaseAuthenticatedAdministrators()
        throws DataAccessException {
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.GenericDAOImpl#get(java.lang.Long)
     */
    @Override
    public User get(Long id) throws DataAccessException {

        // The context could be null, if this is called during login.
        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // get all users from the DB
        String hql = null;

        // start with a simple select
        hql = "select obj from " + getPersistentClassName()
            + " obj where obj.id=:id";

        // if the object is taggable, make sure the user has access to it
        if (isTaggable() && siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                hql = "select distinct obj from "
                    + getPersistentClassName()
                    + " obj "
                    + " left join obj.tags t where (t.id in (:tagId) or obj.allSitesAccess=1) and obj.id=:id";
            }
        }
        Query query = getCurrentSession().createQuery(hql);

        if (isTaggable() && siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                query.setParameterList("tagId", siteContext.getTagIdsForQuery());
            }
        }
        // bind the id parameter
        query.setParameter("id", id);
        // now do the query and make sure there is only one result
        User t = (User) query.uniqueResult();

        // if the object is not found, throw an exception
        if (t == null) {
            throw new EntityNotFoundException(getPersistentClass(), id);
        } else {
            return t;
        }

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImpl#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {

        SiteContext siteContext = SiteContextHolder.getSiteContext();

        // get all users from the DB restricted by the site that is in the
        // thread local

        String hql = "select distinct obj from "
            + getPersistentClassName() + " obj "
            + " left join obj.tags t where ( t.id = :tagId"
            + " or obj.allSitesAccess=true )";

        if (rdi.getFilter() != null) {
            hql += " and " + rdi.getFilter().toHQL();
        }

        hql += getOrderClause(rdi);
        Query query = getCurrentSession().createQuery(hql);
        if (rdi.getFilter() != null) {
            // We need to replace any named parameters
            rdi.getFilter().fillInNamedParameters(query);
        }

        // set the tag parameter from the thread local
//        Long tagId = siteContext.getSiteTagId();
        Tag tag = siteContext.getCurrentTagForQuery();
        if (tag == null) {
            // TODO throw Exception
            // DHG This should never occur
        }
        query.setParameter("tagId", tag.getId());
        return query.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.GenericDAOImpl#save(com.vocollect.epp.model.BaseModelObject)
     */
    @Override
    public void save(User object) throws DataAccessException {
        boolean isNew = object.isNew();

        getCurrentSession().saveOrUpdate(object);

        getCurrentSession().flush();
        if (log.isInfoEnabled()) {
            log.info(((isNew ? "Created " : "Updated ")
                + getPersistentClass().getSimpleName() + "'" + object + "'"));
        }
    }

    /**
     * Implemented by dynamic finder query. {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAO#countDirectoryServerAuthenticatingUsers()
     */
    public Number countDirectoryServerAuthenticatingUsers()
        throws DataAccessException {
        return null;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAORoot#getPropertyByName(java.lang.String,
     *      java.lang.String)
     */
    public UserProperty getPropertyByName(String username, String pname)
        throws DataAccessException {

        // Check for null parameters ??????????

        Query q = getCurrentSession().getNamedQuery("User_findByName");
        q.setParameter("name", username);
        User user = (User) q.uniqueResult();
        Set<UserProperty> userProperties = user.getUserProperties();
        Iterator<UserProperty> property = userProperties.iterator();
        while (property.hasNext()) {
            UserProperty up = property.next();
            if (pname.equals(up.getName())) {
                return up;
            }
        }

        return null;
    }

    /**
     * Automatically tied to the query. <br> {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAORoot#findAllSiteAdministrators()
     */
    public List<Long> getAllSiteAdministrators() throws DataAccessException {
        Query q = getCurrentSession().getNamedQuery("User_findAllSiteAdministrators");
        return q.list();
    }

    /**
     * Automatically tied to the query. <br> {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAORoot#listAllDatabaseAuthenticatedAdministrators()
     */
    public List<User> listAllDatabaseAuthenticatedAdministrators() throws DataAccessException {
        Query q = getCurrentSession().getNamedQuery("User_findAllDatabaseAuthenticatedAdministrators");
        return q.list();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImplRoot#getDataByFieldAndValue(java.lang.String, java.lang.String)
     */
    @Override
    public List getDataByFieldAndValue(String fieldName, String value) throws DataAccessException {
        // The context could be null, if this is called during login.
        SiteContext siteContext = SiteContextHolder.getSiteContext();
        // get all users from the DB
        String hql = null;

        // start with a simple select
        hql = "select distinct obj." +  fieldName + " from " + getPersistentClassName()
            + " obj where obj." + fieldName + " like :value";

        // if the object is taggable, make sure the user has access to it
        if (isTaggable() && siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                hql = "select distinct obj." +  fieldName + "from "
                    + getPersistentClassName()
                    + " obj "
                    + " left join obj.tags t where (t.id in (:tagId)"
                    + " or obj.allSitesAccess=1) and obj." + fieldName + " like :value";
            }
        }
        Query query = getCurrentSession().createQuery(hql);

        if (isTaggable() && siteContext != null && siteContext.isFilterBySite()) {
            // bind the query to only the tags the user has acccess to
            if (!siteContext.isHasAllSiteAccess()) {
                query.setParameterList("tagId", siteContext.getTagIdsForQuery());
            }
        }
        // bind the id parameter
        query.setParameter("value", value + '%');

       return query.list();
    }
    

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAORoot#updateUserChartPreferencesByDashboard(java.lang.Long)
     */
    public void updateUserChartPreferencesByDashboard(Long dashboard) {
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.UserDAORoot#updateUserPropertyByDashboard(java.lang.String, java.lang.Long)
     */
    public void updateUserPropertyByDashboard(String name, Long value) {
    }

    @Override
    public void updateUserChartPreferencesByDashboardAndChart(Long dashboardId,
                                                              Long chartId) {
    }
    
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 