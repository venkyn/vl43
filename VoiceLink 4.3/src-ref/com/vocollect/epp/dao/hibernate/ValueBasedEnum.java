/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;


/**
 * Interface that must be implemented by Enum types that we want to
 * persist to a specific int value rather to the ordinal value that the
 * enum constant gets by reason of its location in the enum definition list.
 * <p>
 * The reason we want to do this is to control the values that are stored in
 * the database, which standard enums don't let you do. This is particularly
 * important for customizations, to avoid value conflicts between customized
 * enum values and new values introduced by Vocollect at upgrade time.
 * @param <E> the generic Enum type.
 *
 * @author ddoubleday
 */
public interface ValueBasedEnum<E extends Enum<E> & ValueBasedEnum<E>> {

    /**
     * @return the int value of the Enum
     */
    int toValue();


    /**
     * Preferably, this would be a static method, but you can't specify
     * a static method in an interface. This method is needed for Hibernate
     * persistence, but in most cases the static version should be used.
     * An additional static version of
     * this method should be added Enum classes that require it.
     * @param val the int value to convert to an Enum
     * @return the matching Enum
     */
    E fromValue(int val);

    /**
     * Set the enum from an Integer, or use autoboxing to convert from some other Object .
     * Autoboxing will not work to convert between primitives and Objects, only between Objects
     * The assumption most packages (ognl, Castor, etc) is that all members & member functions
     * are expressed in terms of Objects. This became a problem WRT importing data.
     * @param valObject the Integer object to use to convert to an Enum.
     * @return the matching Enum
     */
    E fromValue(Integer valObject);

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey();

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 