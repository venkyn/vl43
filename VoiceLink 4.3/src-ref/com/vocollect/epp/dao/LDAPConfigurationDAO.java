/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.LDAPConfiguration;

/**
 * @author jvolz
 */
public interface LDAPConfigurationDAO extends GenericDAO<LDAPConfiguration> {

    /**
     * Returns the Long of the id, or null if not found.
     * @param useSSL .
     * @param host .
     * @param port .
     * @param searchUsername .
     * @param searchPassword .
     * @param searchBase .
     * @param searchableAttribute .
     * @param passwordAttribute .
     * @return .
     * @throws DataAccessException
     */
    public Long uniqueness(Boolean useSSL,
                           String host,
                           String port,
                           String searchUsername,
                           String searchPassword,
                           String searchBase,
                           String searchableAttribute,
                           String passwordAttribute)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 