/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;

import java.util.Date;
import java.util.List;

/**
 * Interface of NotificationDAO.
 *
 * @author Aich Debasis
 * @author ddoubleday
 */
public interface NotificationDAORoot extends GenericDAO<Notification> {

    /**
     * Note: this method only returns results for users that are associated
     * with specific sites. For users with all site access, use the
     * {@link getGroupedCountBySiteForAllSiteAccess} method.
     * @param userName the User name
     * @param np the NotificationPriority
     * @return List of object arrays containing the Site name, Site ID,
     * and the count of notifications of the specified priority for
     * all the Sites visible to the specified User.
     * @throws DataAccessException on database failure.
     */
    public List<Object[]> getGroupedCountBySite(String userName, NotificationPriority np)
        throws DataAccessException;

    /**
     * @param np the NotificationPriority
     * @return List of object arrays containing the Site name, Site ID,
     * and the count of notifications of the specified priority for
     * all the Sites.
     * @throws DataAccessException on database failure.
     */
    public List<Object[]> getGroupedCountBySiteForAllSiteAccess(NotificationPriority np)
        throws DataAccessException;

    /**
     * @param np the NotificationPriority
     * @return count of unacked notifications for the system site.
     * @throws DataAccessException on database failure.
     */
    public int getUnackedSystemNotificationsCount(NotificationPriority np)
        throws DataAccessException;

    /**
     * @param id the site ID.
     * @return list of user email addresses.
     * @throws DataAccessException on database failure.
     */
    public List<String> getUserEmailsBySiteId(Long id) throws DataAccessException;

    /**
     * @param np the NotificationPriority
     * @return the list of unacked notifications.
     * @throws DataAccessException on database failure.
     */
    public List<Notification> getUnackNotifications(NotificationPriority np)
        throws DataAccessException;

    /**
     * @param np the NotificationPriority
     * @param siteId the site ID.
     * @return the max unacknowledged notification ID for the site and priority.
     * @throws DataAccessException on database failure.
     */
    public Long getMaxNotificationIdForSiteId(NotificationPriority np,
                                              Long siteId)
        throws DataAccessException;

    /**
     * @param np the NotificationPriority
     * @return the max unacknowledged notification ID for the System tag and priority.
     * @throws DataAccessException on database failure.
     */
    public Long getMaxNotificationIdForSystemTag(NotificationPriority np)
        throws DataAccessException;

    /**
     * Gets all Notifications older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @return a list of Notifications
     * @throws DataAccessException Database failure
     */
    List<Notification> listOlderThan(QueryDecorator decorator,
                                     Date date) throws DataAccessException;

    /**
     * @param processName the NotificationPriority
     * @param terminalNumber terminal number for which notifications are to be
     *            fetched
     * @return count of acknowledged notifications matching process name and
     *         terminal number.
     * @throws DataAccessException on database failure.
     */
    public int getUnackedSystemNotificationsCount(String processName,
                                                  String terminalNumber)
        throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 