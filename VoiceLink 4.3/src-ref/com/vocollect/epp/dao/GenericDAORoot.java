/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.BaseModelObject;

import java.util.List;


/**
 * This is generic DAO interface that handles basic functions that most
 * DAO instances will need.
 * @param <T> the type to operate on
 * <p>
 * This class uses code and ideas from two sources:
 * <ul>
 *   <li><a href="http://www-128.ibm.com/developerworks/java/library/j-genericdao.html?ca=drs">
 *             Don't Repeat the DAO! by Per Mellqvist</a>
 *   <li><a href="http://blog.hibernate.org/cgi-bin/blosxom.cgi/2005/09/08#genericdao">
 *             Generic DAO pattern with JDK 5.0 by Christian Bauer</a>
 * </ul>
 *
 * @author ddoubleday
 */
public interface GenericDAORoot<T> extends DataProviderDAO {

    /** Retrieve an object from the datbase by the specified id.
     * @param id the ID
     * @return The object of type T
     * @throws DataAccessException on any error
     */
    T get(Long id) throws DataAccessException;

    /**
     * Retrieve all instances of the type T.
     * @return the list of instances, or the empty list if there are none.
     * @throws DataAccessException on any error
     */
    List<T> getAll() throws DataAccessException;

    /**
     * Find all instances that match the example object.
     * @param exampleInstance the instance that matching will be done against.
     * @return the list of matching instances, or the empty list if there are
     * none
     * @throws DataAccessException on any error
     */
    List<T> queryByExample(T exampleInstance) throws DataAccessException;

    /**
     * Save or update the specified object.
     * @param object to save
     * @throws DataAccessException on any error
     */
    void save(T object) throws DataAccessException;

    /**
     * Save the List of specified objects.
     * @param object to saveAll
     * @throws DataAccessException on any error
     */
    void save(List < T > object) throws DataAccessException;


    /**
     * Remove an object from persistent storage in the database.
     * @param persistentObject to delete
     * @throws DataAccessException on any error
     */
    void delete(T persistentObject) throws DataAccessException;

    /**
     * Remove the object with the specified ID from
     * persistent storage in the database.
     * @param id the ID of the object to delete
     * @throws DataAccessException on any error
    */
    void delete(Long id) throws DataAccessException;

    /**
     * Count the number of instances of type T.
     * @return the count.
     * @throws DataAccessException on any error
     */
    long getCount() throws DataAccessException;

    /**
     * Refresh the loaded object with the database infromation.
     *
     * @param persistentObject - object to refresh
     * @throws DataAccessException on any error
     */
    void refresh(BaseModelObject persistentObject) throws DataAccessException;

    /**
     * Method to clear a session of old objects.
     */
    void clearSession();

    /**
     * Method to flush a session. This should be used sparingly, if at all, and
     * only to handle specific issues. In general, it is best to flush only at
     * transaction boundaries (which Hibernate will do automatically) to reduce
     * database activity.
     */
    void flushSession();

    /**
     * Reattaches an object to the hibernate session.
     *
     * @param persistentObject - object to reattach
     */
    public void reattach(T persistentObject);

    /**
     * Detaches an object from the hibernate session.
     *
     * @param persistentObject - object to reattach
     */
    public void detach(T persistentObject);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 