/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Job;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
/**
 * The DAO for job data (run information).
 *
 * @author hulrich
 */

public interface JobDAORoot extends GenericDAO<Job> {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)
     */
    void save(Job job) throws DataAccessException;

    /**
     * Finds the job by its identifying name.
     * @param name the id name
     * @return the Job
     * @throws DataAccessException if the job cannot be found
     */
    Job retrieveByName(String name) throws DataAccessException;

    /**
     * Sets the last ping value to now.
     * @param name the job name
     * @throws DataAccessException on exception
     */
    void ping(String name) throws DataAccessException;

    /**
     * Retrieves the last ping value for this job.
     * @param name the job name
     * @return the last ping
     * @throws DataAccessException on exception
     */
    long poll(String name) throws DataAccessException;


    /**
     * @param scheduler the current scheduler object.
     */
    void setScheduler(Scheduler scheduler);

    /**
     * @return the current Scheduler object
     * @throws SchedulerException if unable to get it.
     */
    Scheduler getScheduler() throws SchedulerException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 