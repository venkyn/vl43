/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.JobHistory;

import java.util.Date;
import java.util.List;

/**
 * DAO for JobHistory. Create and read behavior only.
 *
 * @author hulrich
 */
public interface JobHistoryDAORoot extends GenericDAO<JobHistory> {

    /**
     * Retrieves the history for the job with this id.
     * @param jobId the job id
     * @return the list of job histories
     * @throws DataAccessException if unable to retrieve history for this job
     *             name
     */
    List<JobHistory> getHistory(Long jobId) throws DataAccessException;

    /**
     * Gets all JobHistories older than the date specified.
     * @param decorator extra instructions for the query
     * @param date the date to use
     * @return a list of JobHistories
     * @throws DataAccessException Database failure
     */
    List<JobHistory> listOlderThan(QueryDecorator decorator,
        Date date) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 