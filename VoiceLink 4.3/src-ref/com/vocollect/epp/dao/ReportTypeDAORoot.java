/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportType;

import java.util.List;


/**
 * This provides access to a simple select query defined in the Hibernate
 * mapping file.
 *
 * @author cblake
 * @author ddoubleday
 */
public interface ReportTypeDAORoot extends GenericDAO<ReportType> {
    
    /**
     * Select a Report object by name.
     * @param reportName the name of the report.
     * @return  A matching Report object, or NULL if not found
     * @throws DataAccessException  On database error
     */
    ReportType findByName(String reportName) throws DataAccessException;
 
    /**
     * Select a list of ReportType objects given the appName associated
     * with the Reports.
     * @param appName the name of the app the Report belongs to.
     * @return  the List of reports that belong to the specified appName.
     * @throws DataAccessException  On database error
     */
    List<ReportType> listByAppName(String appName) throws DataAccessException;
    
    /**
     * Select a list of ReportType objects given a substring of the appName associated
     * with the Reports.
     * @param appType the name of the app the Report belongs to.
     * @return  the List of reports that belong to the specified appName.
     * @throws DataAccessException  On database error
     */
    List<ReportType> listByAppType(String appType) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 