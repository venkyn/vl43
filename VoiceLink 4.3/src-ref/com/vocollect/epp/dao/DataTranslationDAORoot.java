/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataTranslation;

import java.util.Date;
import java.util.List;


/**
 * DAO Interface for DataTranslation objects.
 *
 * @author ddoubleday
 */
public interface DataTranslationDAORoot extends GenericDAO<DataTranslation> {

    /**
     * Get the translation for a data key for the specified Locale.
     * @param key the value to translate
     * @param locale the Locale to translate to
     * @return the translation, or null if nothing matched.
     * @throws DataAccessException on database failure.
     */
    DataTranslation findTranslation(String key, String locale) 
    throws DataAccessException;

    /**
     * Finds all translations that have been modified since the date specified.
     * 
     * @param decorator - query defintions
     * @param modifiedSince - date to search on
     * @return - list of translations
     * @throws DataAccessException - database exceptions
     */
    List<Object[]> listModified(QueryDecorator decorator, 
        Date modifiedSince) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 