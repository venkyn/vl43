/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.util.ResultDataInfo;

import java.util.Collection;
import java.util.List;

/**
 * Interface to abstract DAO objects for the table component (or anything that needs data).
 *
 *
 * @author dkertis
 */
public interface DataProviderDAORoot {

    /**
     * @param rdi - Information (such as sorting) regarding how to retreive the data
     * @return A list of all objects
     * @throws DataAccessException on any failure
     */
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException;


    /**
     * @param fieldName .
     * @param value .
     * @return .
     * @throws DataAccessException
     */
    public Collection getDataByFieldAndValue(String fieldName, String value) throws DataAccessException;

    /**
     * Returns the total number of objects.
     * @return a count of objects in the persistent store
     * @throws DataAccessException on any failure
     */
    public long getCount() throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 