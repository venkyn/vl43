/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.filesystem;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.FileView;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.StringUtil;
import com.vocollect.epp.util.SystemUtil;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.PropertyUtils;
import org.json.JSONException;

/**
 *
 *
 * @author dgold/dkertis
 */
public class LoggingDaoImpl implements DataProviderDAO {

    /**
     * Return a windowed list of log files as defined by an offset value
     * given by the rdi parameter.  In this case, getFirstRowId() should
     *  store a relative position rather than an Id.
     * @param rdi specifies data window constraints
     * @return windowed list of log files
     * @throws DataAccessException for errors accessing the DataObject
     */
    public List<DataObject> getDataByOffset(ResultDataInfo rdi) throws DataAccessException {
        List<DataObject> allLogs = getAll(rdi);
        List<DataObject> windowedLogs = new ArrayList<DataObject>();
        for (int i = (int) rdi.getOffset(); i < rdi.getOffset() + rdi.getRowCount(); i++) {
            DataObject data = allLogs.get(i);
            if (data == null) {
                continue;
            }
            windowedLogs.add(data);
        }
        return windowedLogs;

    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.DataProviderDAO#getCount()
     */
    public long getCount() throws DataAccessException {
        return new File(SystemUtil.getSystemLogDirectory())
                        .listFiles().length;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.DataProviderDAO#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @SuppressWarnings("unchecked")
    public List<DataObject> getAll(ResultDataInfo rdi)
        throws DataAccessException {
        LinkedList<DataObject> logs = getLogFiles();

        if (rdi.getFilter() != null) {
            try {
                // If this fails - for what ever reason related to
                // reflection - just go ahead and throw a DataAccessException
                // since the getDataObjectsAsJSON will catch it and send
                // a failure with the exception info
                rdi.getFilter().applyFilter(logs, rdi.getDisplayUtilties());
            } catch (IllegalAccessException iae) {
                throw new DataAccessException(iae);
            } catch (NoSuchMethodException nsme) {
                throw new DataAccessException(nsme);
            } catch (InvocationTargetException ite) {
                throw new DataAccessException(ite);
            } catch (JSONException ite) {
                throw new DataAccessException(ite);
            }
        }
        if (logs != null && logs.size() > 0) {

            try {

                if (PropertyUtils.getNestedProperty(logs.get(0), rdi.getSortColumn()) instanceof String) {
                    Collections.sort(logs, new BeanComparator(
                        rdi.getSortColumn(), StringUtil.getPrimaryCollator()));
                } else {
                    Collections.sort(logs, new BeanComparator(rdi.getSortColumn()));
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }

        // If sort is descending, reverse the results.
        if (!rdi.isSortAscending()) {
            Collections.reverse(logs);
        }
        return logs;
    }

    /**
     * Get all of the files from the system log directory and returns them in a list.
     * @return The log files as a linked list.
     */
    protected LinkedList<DataObject> getLogFiles() {
        // Get an array of file objects corresponding to the
        // application logs.
        String directory = SystemUtil.getSystemLogDirectory();
        File fileRoot = new File(directory);
        File[] logFiles = fileRoot.listFiles();

        // For every file in logFiles, do a conversion to a FileView
        // and addit to our log file list.
        LinkedList<DataObject> logs = new LinkedList<DataObject>();
        if (logFiles != null) {
            for (int i = 0; i < logFiles.length; i++) {
                if (logFiles[i].isDirectory()) {
                    continue;
                }
                logs.add(new FileView(logFiles[i]));
            }
        }
        return logs;
    }

    /**
     * Convenience method to get the property from a bean.
     * @param dataObject - object to retreive
     * @param prop - String representation of the property to get
     * @return the property, possibly translated.
     */
    private static Object getProperty(DataObject dataObject,
                                      String prop) {
        Object obj;
        try {
            obj = PropertyUtils.getNestedProperty(dataObject, prop);
        } catch (Exception e) {
            throw new RuntimeException("Error accessing auto-complete property");
        }

        // return an empty string in event of a null
        return obj;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.DataProviderDAORoot#getDataByFieldAndValue(java.lang.String, java.lang.String)
     */
    public Collection getDataByFieldAndValue(String fieldName, String value) throws DataAccessException {

        // GET All the log files
        List<DataObject> logFiles = getLogFiles();


        // extract the property you are currently filtering on
        Set<String> values = new HashSet<String>(0);

        for (DataObject data : logFiles) {
            String val = getProperty(data, fieldName).toString();
            if (val.startsWith(value)) {
                values.add(val);
            }
        }

        // sort the list

        // return it
        return values;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 