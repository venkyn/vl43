/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;

/**
 * Site Data Access Object (DAO) interface.
 *
 * @author Kalpna T
 */
public interface SiteDAORoot extends GenericDAO<Site> {

    /**
     * @return the default Site.
     */
    public Site findDefaultSite();

    /**
     * @param u the User
     * @return the Tag for the Site.
     * @throws DataAccessException on database failure.
     */
    public Tag getSiteByMinId(User u) throws DataAccessException;

    /**
     * Returns a site based on the name parameter.
     * @param name the name to look for.
     * @return the Site with the specified name, or null.
     * @throws DataAccessException on database failure
     */
    public Site findByName(String name) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 