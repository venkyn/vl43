/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;

import java.util.List;


/**
 * DAO Interface for Column objects.
 *
 * @author ddoubleday
 */
public interface ColumnDAORoot extends GenericDAO<Column> {

    /**
     * Gets the column information for a specific view, with the
     * user column information for the specified user populated.
     * @param view the view being rendered
     * @param user the user requesting the view
     * @return A List of columns for the user's view
     * @throws DataAccessException on any failure
     */
    List<Column> getColumns(View view, User user) throws DataAccessException;

    /**
     * Gets the column information for a specific view, with the
     * user column information for the specified user populated.
     * @param view the view being rendered
     * @param user the user requesting the view
     * @return A List of columns for the user's view
     * @throws DataAccessException on any failure
     */
    List<Column> getVisibleColumns(View view, User user) throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 