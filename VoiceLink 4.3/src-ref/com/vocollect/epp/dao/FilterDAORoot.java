/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.Filter;

import java.util.List;

/**
 * DAO interface for Filter.
 *
 *
 * @author jgeisler
 */
public interface FilterDAORoot extends GenericDAO<Filter> {

    /**
     * Retrieves the necessary Filter for the specified user
     * and view id's.
     * @param userId id of the user
     * @param viewId id of the view
     * @return Filter for the specific user and view
     */
    Filter findByUserIdAndViewId(long userId, long viewId);

    /**
     * Retrieves the necessary Filters for the specified user.
     * @param userId id of the user
     * @return Collection for the specific user
     */
    List<Filter> listAllByUserId(long userId);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 