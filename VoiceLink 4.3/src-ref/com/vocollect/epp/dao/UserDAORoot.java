/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;

import java.util.List;

/**
 * User Data Access Object (DAO) interface.
 *
 * @author Dennis Doubleday
 */
public interface UserDAORoot extends GenericDAO<User> {

    /**
     * Gets user information based on login name.
     * @param username the current name
     * @return the specified <code>User</code>, if it exists; null otherwise.
     * @throws DataAccessException on any failure
     */
    User findByName(String username) throws DataAccessException;

    /**
     * Count the Users with Administrative authority who authenticate against
     * the database and not a directory server.
     * @return the count
     * @throws DataAccessException on any failure
     */
    Number countDatabaseAuthenticatedAdministrators() throws DataAccessException;

    /**
     * Count the Users without a password defined, i.e. authenticating against
     * the directory server
     * @return the count.
     * @throws DataAccessException on any failure
     */
    Number countDirectoryServerAuthenticatingUsers() throws DataAccessException;

    /**
     * Finds all administrators that belong have access to all sites and are
     * authenticated against the database.
     * @return the admins who have all sites access and authenticate against
     * the database
     * @throws DataAccessException on any failure
     */
    List<Long> getAllSiteAdministrators() throws DataAccessException;

    /**
     * Returns all administrators who authenticate against the database and
     * not the directory server.
     * @return A list of users
     * @throws DataAccessException on any failure
     */
    List<User> listAllDatabaseAuthenticatedAdministrators() throws DataAccessException;

    /**
     * Gest property information based on the user and the property name.
     * @param username the current name
     * @param pname the property name
     * @return the UserProperty
     * @throws DataAccessException on any failure
     */
    UserProperty getPropertyByName(String username, String pname)
        throws DataAccessException;
    
    /**
     * Method to delete the Chart Preferences based on Dashboard ID.
     * @param dashboardId Long.
     */
    void updateUserChartPreferencesByDashboard(Long dashboardId);
    
    /**
     * Method to delete the Default dashboard user Property based on 
     * name value pair in voc_user_properties table.
     * @param name String.
     * @param value Long.
     */
    void updateUserPropertyByDashboard(String name, Long value);
    
    /**
     * Method to delete the Chart Preferences based on Dashboard ID and chart Id.
     * @param dashboardId the dashboard id.
     * @param chartId the chart id .
     */
    void updateUserChartPreferencesByDashboardAndChart(Long dashboardId, Long chartId);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 