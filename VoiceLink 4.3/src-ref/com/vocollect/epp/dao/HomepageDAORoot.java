/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.User;

import java.util.List;

/**
 * Interface of HomepageDAO.
 *
 * @author svoruganti
 */
public interface HomepageDAORoot extends GenericDAO<Homepage> {


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.HomepageManager#getSummaries(com.vocollect.epp.model.User, long)
     */
    public Homepage getByIdAndUser(User user, long homepageId) throws DataAccessException;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.service.HomepageManager#getByHomepageId( long)
     */
    public Homepage getByHomepageId(long homepageId) throws DataAccessException;

    /**
     * @param userId of the user
     * @return the homepage for this user
     */
    List<Homepage> listAllByUserId(long userId);

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 