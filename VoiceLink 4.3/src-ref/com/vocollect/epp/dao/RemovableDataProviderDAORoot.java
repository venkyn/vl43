/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;



/**
 * Interface that indicates the DataProvider supports deletion
 * of the objects being provided.
 *
 * @author dkertis
 */
public interface RemovableDataProviderDAORoot extends DataProviderDAO {

    /**
     * Removes <code>DataProvider</code> object by ID.
     * @param id the key of the user
     * @throws DataAccessException on any failure, and specifically
     * <code>EntityNotFoundException</code> when the specified entity
     * could not be found.
     */
    void delete(Long id) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 