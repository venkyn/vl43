/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemTranslation;

import java.util.List;
import java.util.Locale;


/**
 * DAO Interface for System Translations objects.
 *
 * @author mkoenig
 */
public interface SystemTranslationDAORoot extends GenericDAO<SystemTranslation> {

    /**
     * Find All locales installed in the system.
     * 
     * @return - list of DataObject containing Disntinct Locales.
     * @throws DataAccessException - database exceptions
     */
    List<Locale> getSupportedLocales()  throws DataAccessException;

    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 