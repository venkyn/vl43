/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.ExternalJobSystemTranslation;

import java.util.List;

/**
 * @author mraj
 * 
 */
public interface ExternalJobSystemTranslationDAORoot extends
    GenericDAO<ExternalJobSystemTranslation> {


    /**
     * Returns the list of translation for the given key
     * @param key the key
     * @param locale the locale
     * @return System translation or null
     */
    public List<ExternalJobSystemTranslation> listTranslationByKey(String key,
                                                                   String locale);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 