/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;


/**
 * Exception class for representing failures caused by invalid
 * Hibernate queries. This can be a recoverable exception when
 * the query is constructed dynamically with user guidance.
 *
 * @author Dennis Doubleday
 */
public class InvalidQueryException extends DataAccessException {

    static final long serialVersionUID = -8663323432664466553L;

    /**
     * Constructor.
     */
    public InvalidQueryException() {
        super();
    }

    /**
     * Constructor.
     * @param t the exception that caused this exception.
     */
    public InvalidQueryException(Throwable t) {
        super(t);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     * @param t the exception that caused this exception.
     */
    public InvalidQueryException(ErrorCode code, Throwable t) {
        super(code, t);
    }


    /**
     * Constructs an instance of <code>VocollectException</code> from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user, and include the wrapped
     * exception.
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     * @param t the wrapped exception
     */
   public InvalidQueryException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 