/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.errors.UserMessage;


/**
 * Exception class for representing failure to find an entity that
 * was specifically requested. This exception does not permit
 * supplying alternate messages or codes; this makes generic handling
 * of the error possible.
 *
 * @author Dennis Doubleday
 */
public class EntityNotFoundException extends DataAccessException {

    private static final long serialVersionUID = 6447037249816511073L;

    // The class of entity that was not found.
    private Class<?> type;

    // The ID of the unfound entity.
    private Object entityId;

    /**
     * Constructor that wraps <code>Throwable</code>. This will usually be
     * a <code>HibernateExeption</code>.
     * @param t the Throwable.
     */
    public EntityNotFoundException(Throwable t) {
        super();
        initCause(t);
        setErrorCode(SystemErrorCode.ENTITY_NOT_FOUND);
        addMessage(new UserMessage(getErrorCode().getDefaultMessageKey(),
            getEntityResourceKey(), getEntityId()));
    }

    /**
     * Constructor that specifies the type of entity that was not found.
     * The exception has a default <code>ErrorCode</code> of
     * <code>SecurityErrorCode.ENTITY_NOT_FOUND</code>.
     * @param type the <code>Class</code> of the unfound entity.
     * @param id the ID of the unfound entity.
     */
    public EntityNotFoundException(Class<?> type, Object id) {
        this.type = type;
        this.entityId = id;
        setErrorCode(SystemErrorCode.ENTITY_NOT_FOUND);
        addMessage(new UserMessage(
            getErrorCode(),
            UserMessage.markForLocalization(getEntityResourceKey()),
            getEntityId()));

    }
    /**
     * Constructor that specifies the type of entity that was not found.
     * The exception has a default <code>ErrorCode</code> of
     * <code>SecurityErrorCode.ENTITY_NOT_FOUND</code>.
     * @param msg a non-localized exception message.
     * @param type the <code>Class</code> of the unfound entity.
     * @param id the ID of the unfound entity.
     */
    public EntityNotFoundException(String msg, Class<?> type, Object id) {
        super(msg);
        this.type = type;
        this.entityId = id;
        setErrorCode(SystemErrorCode.ENTITY_NOT_FOUND);
        addMessage(new UserMessage(
            getErrorCode(),
            UserMessage.markForLocalization(getEntityResourceKey()),
            getEntityId()));

    }

    /**
     * Getter for the type property.
     * @return Class value of the property
     */
    public Class<?> getType() {
        return this.type;
    }

    /**
     * Getter for the entityId property.
     * @return Object value of the property
     */
    public Object getEntityId() {
        return this.entityId;
    }


    /**
     * Construct a resource key from type information stored in the exception.
     * @return the resource key
     */
    private String getEntityResourceKey() {
        if (getType() == null) {
            return "entity.unknown";
        } else {
            return "entity." + getType().getSimpleName();
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 