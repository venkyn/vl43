/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;


import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;

import org.hibernate.HibernateException;
import org.springframework.aop.ThrowsAdvice;

/**
 * Advice to catch HibernateException and throw DataAccessException.
 * @author ddoubleday
 */
public class ExceptionConverterAdvice implements ThrowsAdvice {

    private static Logger log = new Logger(ExceptionConverterAdvice.class);

    /**
     * @param ex the HibernateException that was thrown.
     * @throws DataAccessException always
     */
    public void afterThrowing(HibernateException ex) throws DataAccessException {
        if (log.isTraceEnabled()) {
            log.trace("Caught exception: " + ex.getMessage());
        }
        throw ExceptionUtils.convertHibernateException(ex, SystemErrorCode.DATABASE_EXCEPTION);
    }

    /**
     * @param ex the DataAccessException that was thrown.
     * @throws DataAccessException always
     */
    public void afterThrowing(org.springframework.dao.DataAccessException ex) throws DataAccessException {
        if (log.isTraceEnabled()) {
            log.trace("Caught exception: " + ex.getMessage());
        }
        throw new DataAccessException(ex);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 