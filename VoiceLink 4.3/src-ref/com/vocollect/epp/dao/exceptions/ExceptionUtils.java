/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;

import org.hibernate.HibernateException;
import org.hibernate.JDBCException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.QueryException;
import org.hibernate.StaleStateException;
import org.hibernate.UnresolvableObjectException;
import org.hibernate.WrongClassException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.LockAcquisitionException;


/**
 * Utility methods for converting unchecked Hibernate exceptions to checked
 * Vocollect <code>DataAccessException</code>.
 *
 * @author ddoubleday
 */
public final class ExceptionUtils {

    /**
     * Private Constructor.
     */
    private ExceptionUtils() {
        // Private constructor prevents instance creation.
    }

    /**
     * Convert an unchecked HibernateException into a checked Vocollect
     * DataAccessException.
     * @param ex the original Hibernate exception
     * @return DataAccessException wrapping the original
     */
    public static DataAccessException convertHibernateException(HibernateException ex) {
        return convertHibernateException(ex, null);
    }

    /**
     * Convert an unchecked HibernateException into a checked Vocollect
     * DataAccessException.
     * @param ex the original Hibernate exception
     * @param code the ErrorCode to associate with the wrapping exception.
     * @return DataAccessException wrapping the original
     */
    public static DataAccessException convertHibernateException(HibernateException ex,
                                                                ErrorCode code) {
        if (ex instanceof LockAcquisitionException) {
            return new DatabaseLockException(
                code, new UserMessage("exception.database.lock"), ex);
        }
        if (ex instanceof ConstraintViolationException) {
            return new DatabaseConstraintException(
                code, new UserMessage("exception.database.constraint"), ex);
        }
        if (ex instanceof UnresolvableObjectException
            || ex instanceof WrongClassException) {
            // This exception doesn't take the other arguments.
            return new EntityNotFoundException(ex);
        }
        if (ex instanceof NonUniqueResultException) {
            return new NotUniqueResultException(
                code, new UserMessage("exception.database.nonunique"), ex);
        }
        if (ex instanceof StaleStateException) {
            return new OptimisticLockingFailureException(
                code, new UserMessage("exception.database.optimisticlock"), ex);
        }
        if (ex instanceof QueryException) {
            return new InvalidQueryException(
                code, new UserMessage("exception.database.invalidquery"), ex);
        }
        if (ex instanceof JDBCException) {
            return new JdbcException(
                code, new UserMessage("exception.database.jdbc"), ex);
        }

        // fallback, for all other exceptions that likely can never be
        // recovered from.
        return new DataAccessException(
            code, new UserMessage("exception.database.generic"), ex);

    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 