/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.ErrorCode;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.VocollectException;


/**
 * Base class for all Vocollect exceptions related to data access.
 *
 * @author Dennis Doubleday
 */
public class DataAccessException extends VocollectException {

    private static final long serialVersionUID = 2163122438728147723L;

    /**
     * Constructor.
     */
    public DataAccessException() {
        super();
    }

    /**
     * Constructor.
     * @param t the exception that caused this exception.
     */
    public DataAccessException(Throwable t) {
        super(t);
    }

    /**
     * Constructor.
     * @param msg a non-localized exception message.
     * @param t the exception that caused this exception.
     */
    public DataAccessException(String msg, Throwable t) {
        super(msg, t);
    }

    /**
     * Constructor.
     * @param msg a non-localized exception message.
     */
    public DataAccessException(String msg) {
        super(msg);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     */
    public DataAccessException(ErrorCode code) {
        super(code);
    }

    /**
     * Constructor. The default message key associated with the error
     * code is added to the message map.
     * @param code the ErrorCode associated with the exception.
     * @param t the exception that caused this exception.
     */
    public DataAccessException(ErrorCode code, Throwable t) {
        super(code, t);
    }


    /**
     * Constructs an instance of <code>DataAccessException</code> from a
     * message and a message key to be used for looking up an internationalized
     * message related to the specific issue with this exception. Use this
     * constructor when you want to add your own message, and specify a message
     * that may be useful for displaying to the user, and include the wrapped
     * exception.
     * @param code - the ErrorCode associated with the exception
     * @param msg - the localized message explaining the exception
     * @param t the wrapped exception
     */
    public DataAccessException(ErrorCode code, UserMessage msg, Throwable t) {
        super(code, msg, t);
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 