/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.TimeWindow;
import com.vocollect.epp.model.User;

import java.util.List;


/**
 * DAO Interface for TimeWindow objects.
 *
 * @author bnichols
 */
public interface TimeWindowDAORoot extends GenericDAO<TimeWindow> {

    /**
     * @param user the current user
     * @param viewId the ID of the current view
     * @return the TimeWindow associated with the View and the User
     * @throws DataAccessException on any failure.
     */
    public TimeWindow getByViewIdAndUser(User user, long viewId) throws DataAccessException;

    /**
     * @param viewId the ID of the current view
     * @return the TimeWindow associated with the view.
     * @throws DataAccessException on any failure
     */
    public TimeWindow getByViewId(long viewId) throws DataAccessException;

    /**
     * @param userId of the user.
     * @return a list of TimeWindow objects
     */
    List<TimeWindow> listAllByUserId(long userId);
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 