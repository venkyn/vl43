/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.scheduling;

import com.vocollect.epp.dao.JobDAORoot;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.GenericDAOImpl;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Job;
import com.vocollect.epp.model.ScheduleType;
import com.vocollect.epp.scheduling.SchedulingErrorCode;

import java.text.ParseException;
import java.util.Date;

import org.hibernate.Query;
import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Job Data Access Object (DAO) implementation.
 *
 * @author hulrich
 */
public class JobDAOImplRoot extends GenericDAOImpl<Job> implements JobDAORoot {

    private static final Logger log = new Logger(JobDAOImplRoot.class);

    private Scheduler scheduler;

    /**
     * Constructor.
     */
    public JobDAOImplRoot() {
        super(Job.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobDAO#findByName(java.lang.String)
     */
    public Job retrieveByName(String name) throws DataAccessException {
        // TODO: should be dynamic finder method.
        try {
            if (log.isTraceEnabled()) {
                log.trace("FIND BY NAME......................... " + name);
            }
            Query query = this.getCurrentSession().getNamedQuery(
                "findThruName");
            query.setString("name", name);
            return ((Job) query.uniqueResult());
        } catch (Exception e) {
            throw new DataAccessException();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)
     */
    @Override
    public void save(Job job) throws DataAccessException {
        if (log.isTraceEnabled()) {
            log.trace("Saving .... : " + job.getName());
        }
        scheduleJobAndTrigger(job);
        if (getCurrentSession().contains(job)) {
            super.save(job);
        } else {
            getCurrentSession().merge(job);
            getCurrentSession().flush();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobDAORoot#ping(java.lang.String)
     */
    public void ping(String name) throws DataAccessException {
        Query q = getCurrentSession().getNamedQuery("pingJob");
        q.setLong("ping", System.currentTimeMillis());
        q.setString("name", name);
        if (q.executeUpdate() != 1) {
            if (log.isDebugEnabled()) {
                log.debug("Did not update the job correctly - " + name);
            }
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobDAORoot#poll(java.lang.String)
     */
    public long poll(String name) throws DataAccessException {
        // TODO: should be dynamic finder method.
        Query q = getCurrentSession().getNamedQuery("pollJob");
        q.setString("name", name);
        return ((Long) q.uniqueResult()).longValue();
    }

    /**
     * Builds a trigger based upon persisted values and reschedules the job.
     * @param job the job
     * @throws DataAccessException if the job cannot be saved
     */
    private void scheduleJobAndTrigger(Job job) throws DataAccessException {

        try {
            //scheduler = StdSchedulerFactory.getDefaultScheduler();
            //sched = this.getScheduler();
            Trigger[] triggers = this.getScheduler().getTriggersOfJob(job.getName(), job
                .getGroup());
            log.debug("Got the triggers ... " + triggers.length);
            for (Trigger trigger : triggers) {
                Trigger newTrigger = null;

                if (ScheduleType.INTERVAL.equals(job.getType())) {
                    newTrigger = new SimpleTrigger(
                        trigger.getName(), trigger.getGroup(), job.getName(),
                        job.getGroup(), new Date((System.currentTimeMillis() + job.getInterval())),
                        null, job.getRepeatCount(), job.getInterval());
                } else if (ScheduleType.DAILY.equals(job.getType())) {
                    newTrigger = new CronTrigger(trigger.getName(), trigger
                        .getGroup(), job.getName(), job.getJobDetail()
                        .getGroup(), job.getCronExpression());
                }

                this.getScheduler().rescheduleJob(newTrigger.getName(), newTrigger
                    .getGroup(), newTrigger);

                if (!job.getEnabled()) {
                    this.getScheduler().pauseTrigger(newTrigger.getName(), newTrigger
                        .getGroup());
                }
                job.setTriggerInformation(newTrigger);
            }
        } catch (SchedulerException ex) {
            throw new DataAccessException(
                SchedulingErrorCode.NO_SCHEDULER_ACCESS, new UserMessage(
                    SchedulingErrorCode.NO_SCHEDULER_ACCESS), ex);
        } catch (ParseException px) {
            throw new DataAccessException(
                SchedulingErrorCode.UNABLE_TO_STORE_TRIGGER, new UserMessage(
                    SchedulingErrorCode.UNABLE_TO_STORE_TRIGGER), px);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobDAORoot#getScheduler()
     */
    public Scheduler getScheduler() throws SchedulerException {
        if (this.scheduler == null) {
            this.scheduler = StdSchedulerFactory.getDefaultScheduler();
        }

        return this.scheduler;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobDAORoot#setScheduler(org.quartz.Scheduler)
     */
    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 