/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.scheduling;

import com.vocollect.epp.dao.JobHistoryDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.GenericDAOImpl;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.JobHistory;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.util.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanComparator;
import org.hibernate.Query;
import org.json.JSONException;

/**
 * The DAO implementation.
 *
 * @author hulrich
 */
public class JobHistoryDAOImplRoot extends GenericDAOImpl<JobHistory> implements
    JobHistoryDAO {

    //private static final Logger log = new Logger(JobHistoryDAOImpl.class);

    /**
     * Constructor.
     */
    public JobHistoryDAOImplRoot() {
        super(JobHistory.class);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobHistoryDAO#getHistory(java.lang.String)
     */
    public List<JobHistory> getHistory(Long jobId) throws DataAccessException {
        List<JobHistory> jobH = null;
        try {
            if (jobId != null) {
                Query query = this.getCurrentSession().getNamedQuery(
                    "JobHistory_findByJobId");
                query.setLong("jobId", jobId);

                jobH = query.list();
            } else {
                jobH = new ArrayList<JobHistory>();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new DataAccessException();
        }

        return jobH;
    }

    /**
     * This method will take a list of hitory objects and apply a filter to limit the
     *   objects display on the GUI and also sort the objects according to the metadata.
     * @param history - a list of results to apply a filter on and sort
     * @param rdi - metadata for sorting and filtering
     * @return - the list filtered and sorted
     * @throws DataAccessException on any db failure.
     */
    public static List<DataObject> prepareResults(List<JobHistory> history,
                                                  ResultDataInfo rdi)
        throws DataAccessException {
        List<DataObject> data = buildDataObjectList(history);

        if (rdi.getFilter() != null) {
            try {
                // If this fails - for what ever reason related to
                // reflection - just go ahead and throw a DataAccessException
                // since the getDataObjectsAsJSON will catch it and send
                // a failure with the exception info
                rdi.getFilter().applyFilter(data, rdi.getDisplayUtilties());
            } catch (IllegalAccessException iae) {
                throw new DataAccessException(iae);
            } catch (NoSuchMethodException nsme) {
                throw new DataAccessException(nsme);
            } catch (InvocationTargetException ite) {
                throw new DataAccessException(ite);
            } catch (JSONException ite) {
                throw new DataAccessException(ite);
            }
        }

        sortData(data, rdi);
        return data;
    }

    /**
     * Builds the list of data objects from job histories.
     * @param history the list of history
     * @return the list of data objects
     */
    private static List<DataObject> buildDataObjectList(List<JobHistory> history) {
        List<DataObject> data = new ArrayList<DataObject>();
        for (JobHistory h : history) {
            data.add(h);
        }
        return data;
    }

    /**
     * Sorts the retrieved data.
     * @param data the data objects
     * @param rdi the data info
     */
    @SuppressWarnings("unchecked")
    private static void sortData(List<DataObject> data, ResultDataInfo rdi) {
        // Sort the file list based on the specified sort column (or
        // its equivalent).
        String sortColumn = rdi.getSortColumn();
        if (sortColumn.equals("jobName")) {
            sortColumn = "jobName";
        } else if (sortColumn.equals("jobResult")) {
            sortColumn = "jobResult";
        } else if (sortColumn.equals("resultDetails")) {
            sortColumn = "resultDetails";
        } else if (sortColumn.equals("jobStarted")) {
            sortColumn = "sortJobStarted";
        } else if (sortColumn.equals("jobFinished")) {
            sortColumn = "sortJobFinished";
        }

         Collections.sort(data, new BeanComparator(
            sortColumn, StringUtil.getPrimaryCollator()));

        // If sort is descending, reverse the results.
        if (!rdi.isSortAscending()) {
            Collections.reverse(data);
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.DataProviderDaoImplRoot#getAll(com.vocollect.epp.util.ResultDataInfo)
     */
    @Override
    public List<DataObject> getAll(ResultDataInfo rdi) throws DataAccessException {
        if (rdi.getFilter() != null) {
            return super.getAll(rdi);
        } else {
            return new ArrayList<DataObject>();
        }
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.JobHistoryDAORoot#listOlderThan(com.vocollect.epp.dao.hibernate.finder.QueryDecorator, java.util.Date)
     */
    public List<JobHistory> listOlderThan(QueryDecorator decorator, Date date) throws DataAccessException {
        //   this mehtid isn't actually called, implemented by dynamic finder
        return null;
    }



}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 