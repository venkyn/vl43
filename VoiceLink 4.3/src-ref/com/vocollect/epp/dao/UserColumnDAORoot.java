/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.UserColumn;

/**
 * UserColumn Data Access Object (DAO) interface.
 *
 * @author Dennis Kertis
 */
public interface UserColumnDAORoot extends GenericDAO<UserColumn> {
    /**
     * Gets users information based on login name.
     * @param userId the user object the column is for
     * @param columnId the column the user is overriding
     * @return the specified <code>UserColumn</code>, if it exists; null otherwise.
     * @throws DataAccessException on any failure
     */
    UserColumn findByUserAndColumn(Long userId, Long columnId)
        throws DataAccessException;

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 