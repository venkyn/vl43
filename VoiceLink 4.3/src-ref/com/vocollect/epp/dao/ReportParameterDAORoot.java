/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ReportParameter;

import java.util.List;

/**
 * This provides access to a simple select query defined in the Hibernate
 * mapping file.
 *
 * @author mnichols
 */
public interface ReportParameterDAORoot extends GenericDAO<ReportParameter> {
 
    /**
     * Select a list of ReportParameter objects given the operator team id associated
     * with the Reports.
     * @param id the id of the operator team
     * @return  the List of report parameters that belong to the specified operator team.
     * @throws DataAccessException  On database error
     */
    List<ReportParameter> listTeamReferences(String id) throws DataAccessException;
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 