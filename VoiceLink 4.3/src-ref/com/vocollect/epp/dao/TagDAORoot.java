/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;

import java.util.List;

/**
 * Tags Data Access Object (DAO) interface.  Supports Deleting, Getting, and Saving Tags.
 *
 * @author mlashinsky
 */
public interface TagDAORoot extends GenericDAO<Tag> {

    /**
     * @param tagType the tag type to look for.
     * @param objectId the ID of the tagged object.
     * @return the Tag that the object is tagged with.
     */
    public Tag findByTaggedObjectId(Long tagType, Long objectId);

    /**
     * TODO: why isn't this just a get? --ddoubleday
     * @param tagId the Tag id
     * @return the Tag
     */
    public Tag findObjectByTagId(Long tagId);

    /**
     * @return the default Site
     */
    public Site getDefaultSite();

    /**
     * @return the list of Tags of type SITE.
     */
    public List<Tag> listSiteTags();
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 