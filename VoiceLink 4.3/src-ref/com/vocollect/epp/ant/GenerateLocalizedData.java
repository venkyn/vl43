/**
 * 
 */
package com.vocollect.epp.ant;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.MatchingTask;

import com.vocollect.epp.util.LocaleAdapter;

/**
 * @author treed
 *
 */
public abstract class GenerateLocalizedData extends MatchingTask {

    /**
     * Comparator that compares Locales by locale string.
     */
    protected static final Comparator<Locale> LOCALE_COMPARATOR =
        new Comparator<Locale>() {
            public int compare(Locale l1, Locale l2) {
                 return l1.toString().compareTo(l2.toString());
            }
        };

    // This is a map of fallback property files, keyed by the base name
    // with which the fallbacks are associated.
    protected Map<String, Properties> baseNameFallbacks = 
        new TreeMap<String, Properties>();
    
    // This is a map, keyed by the base name, of maps of property files
    // (keyed by Locale). There should eventually be an entry for each base name
    // and each supported Locale for that base name. Some will be missing 
    // until all localizations are done, so the fallbacks will be used for
    // those, because all Locales must be represented in the DB in order for
    // sort joins to work.
    protected Map<String, Map<Locale, Properties>> baseNamePropertyEntries =
        new TreeMap<String, Map<Locale, Properties>>(); 
    
    // This is a map, keyed by Locale, of Maps (keyed by property file base name)
    // of Sets containing resource property keys that are present in the fallback
    // file but not in the translation file. This is almost surely an error.
    protected Map<Locale, Map<String, Set<String>>> missingProperties =
        new TreeMap<Locale, Map<String, Set<String>>>(LOCALE_COMPARATOR); 

    // This is a map, keyed by Locale, of Maps (keyed by property file base name)
    // of Maps (keyed by property name, value is the property value) that are 
    // present in the translation file
    // but the value of the property is the same as the fallback. This is
    // suspect--it may be OK or not.
    protected Map<Locale, Map<String, Map<String, String>>> untranslatedProperties =
        new TreeMap<Locale, Map<String, Map<String, String>>>(LOCALE_COMPARATOR); 

    // This is a map, keyed by Locale, of Maps (keyed by property file base name)
    // of Sets containing resource property keys that are present in the translation
    // file but not in the fallback file. This is almost surely an error.
    protected Map<Locale, Map<String, Set<String>>> noFallbackProperties =
        new TreeMap<Locale, Map<String, Set<String>>>(LOCALE_COMPARATOR);
    
    protected String currentFileName = null;


    // The src directory for resource files
    protected File srcDir;

    // Comma-separated list of locales that are supported
    protected List<Locale> supportedLocales;
    
    // The target file name for dbunit data
    protected File dataFile;
    
    // The file name for the resource key report
    protected File reportFile;
    
    // The Writer associated with the target file.
    protected PrintWriter dataWriter;
    
    // The Writer associated with the report file.
    protected PrintWriter reportWriter;
    
    // This is the map of which enum types have been processed. It is needed
    // so we know when to generate a base null value database row for each
    // enum type (in order to make inner joins work on potentially null columns).
    protected Map<String, String> processedEnumsMap = new HashMap<String, String>();
    
    /**
     * {@inheritDoc}
     * @see org.apache.tools.ant.Task#execute()
     */
    @Override
    public void execute() throws BuildException {
        
        checkInputs();
        
        // Get the list of files to operate on.
        DirectoryScanner ds = getDirectoryScanner(this.srcDir);
        String[] resourceFiles = ds.getIncludedFiles();
        
        // Split them into fallback files (which contain the US English
        // words) and translation files
        List<String> fallbackFiles = new ArrayList<String>();
        List<String> translationFiles = new ArrayList<String>();
        
        for (String file : resourceFiles) {
            if (file.contains("_")) {
                translationFiles.add(file);
            } else {
                fallbackFiles.add(file);
            }
        }
        
        // Open writer to report file.
        openReportWriter();
        this.reportWriter.println("***********************************************");
        this.reportWriter.println("RESOURCE PROPERTY VALIDATION REPORT, " + new Date());
        this.reportWriter.println("***********************************************");
        this.reportWriter.println();
        this.reportWriter.println("Processing the following resource files:");
        
        
        // Process all fallback files first. Important!
        for (String file : fallbackFiles) {
            processResourceFile(file, true);
        }
        
        // Now process all localization files.
        for (String file : translationFiles) {
            processResourceFile(file, false);
        }

        // Write data file, if specified.        
        writeDataFile();
         
        // create report
        createResourceReport();
    }
    
    /**
     * Setter for the srcDir property.
     * @param srcDir the new srcDir value
     */
    public void setSrcDir(File srcDir) {
        this.srcDir = srcDir;
    }

    
    /**
     * Setter for the supportedLocales property.
     * @param supportedLocales the new supportedLocales value
     */
    public void setSupportedLocales(String supportedLocales) {
        String[] localeStrings = supportedLocales.split(",");
        this.supportedLocales = new ArrayList<Locale>();
        for (String localeString : localeStrings) {
            this.supportedLocales.add(LocaleAdapter.makeLocaleFromString(localeString));
        }
    }

    /**
     * Setter for the targetDir property.
     * @param dataFile the new targetDir value
     */
    public void setDataFile(File dataFile) {
        this.dataFile = dataFile;
    }
    
    /**
     * Setter for the reportFile property.
     * @param reportFile the new reportFile value
     */
    public void setReportFile(File reportFile) {
        this.reportFile = reportFile;
    }

    /**
     * Check input values.
     */
    protected void checkInputs() {
        if (this.srcDir == null) {
            throw new BuildException("srcDir must be specified");
        }
        if (this.reportFile == null) {
            throw new BuildException("reportFile must be specified");
        }
        if (this.dataFile == null) {
            log("No data file specified, no data will be generated", Project.MSG_DEBUG);            
        }
        if (this.supportedLocales == null) {
            throw new BuildException("supportedLocales must be specified");
        }
        
        log("srcDir = " + this.srcDir, Project.MSG_DEBUG);
    }

    /**
     * @param property the resource property to examine.
     * @return true if the property represents an enum value translation, false
     * otherwise.
     */
    protected boolean isEnumProperty(String property) {
        if (property.startsWith("com.vocollect")) {
            return true;
        }
        return false;
    }

    /**
     * Open the Writer to the target data file.
     */
    protected void openDataWriter() {
        this.dataWriter = openWriter(this.dataFile);
    }

    /**
     * Open the Writer to the report file.
     */
    protected void openReportWriter() {
        // Overwrite existing report
        this.reportFile.delete();
        this.reportWriter = openWriter(this.reportFile);
    }

    /**
     * Open the Writer to the specified file.
     * @param targetFile the file to open the writer on.
     * @return the opened writer.
     */
    protected PrintWriter openWriter(File targetFile) {
        try {
            // Get the output stream for the dbunit file
            FileOutputStream fos = new FileOutputStream(targetFile);
            // Wrap it in an OutputStreamWriter to force UTF-8 encoding, and in
            // a BufferedWriter for performance.
            return new PrintWriter(
                new BufferedWriter(new OutputStreamWriter(fos, "UTF-8")));
        } catch (FileNotFoundException e) {
            throw new BuildException(
                "Unable to create file " + targetFile.getPath() + ": " + e);
        } catch (UnsupportedEncodingException e1) {
            // Should never happen, since UTF-8 is guaranteed.
            throw new BuildException("Unsupported encoding UTF-8: " + e1);
        }
    }
    
    /**
     * Make sure that the properties for the specified Locale include all those
     * in the associated fallback file.
     * @param props the localized property set
     * @param locale the Locale being processed
     * @param baseName the baseName that properties are associated with.
     */
    protected void validatePropertiesForLocale(Properties props, 
                                             Locale     locale, 
                                             String     baseName) {
        
        Properties fallbacks = this.baseNameFallbacks.get(baseName);
        Map<String, Set<String>> missing = this.missingProperties.get(locale);
        Map<String, Map<String, String>> untranslated = this.untranslatedProperties.get(locale);
        Map<String, Set<String>> noFallback = this.noFallbackProperties.get(locale);

        // Check for properties that are in the translation file but
        // not in the fallbacks.
        for (Object propName : props.keySet()) {
            if (fallbacks.get(propName) == null) {
                // No fallback. 
                // Add the noFallback property to the set of noFallback ones.
                log("No fallback for property '" + propName + "' defined in file '" 
                    + baseName +  "_" + locale + "'");
                // If no entry for this locale, create it.
                if (noFallback == null) {
                    noFallback = new TreeMap<String, Set<String>>();
                    this.noFallbackProperties.put(locale, noFallback);
                }
                // If no entry for this basename, create it.
                if (noFallback.get(baseName) == null) {
                    noFallback.put(baseName, new TreeSet<String>());
                }
                noFallback.get(baseName).add((String) propName);                
            }
            
        }
        
        // Check for properties that are in the fallbacks but not the
        // translation file.
        for (Object propName : fallbacks.keySet()) {
            if (props.get(propName) == null) {
                // Missing one from the fallback set. 
                // Add the missing property to the set of missing ones.
                log("Missing property '" + propName + "' in file '" 
                    + baseName +  "_" + locale + "'");
                // If no entry for this locale, create it.
                if (missing == null) {
                    missing = new TreeMap<String, Set<String>>();
                    this.missingProperties.put(locale, missing);
                }
                // If no entry for this basename, create it.
                if (missing.get(baseName) == null) {
                    missing.put(baseName, new TreeSet<String>());
                }
                missing.get(baseName).add((String) propName);
                
                // Put the fallback value into the property set that will be
                // written to the database.
                props.put(propName, fallbacks.get(propName));
            } else if (fallbacks.get(propName).equals(props.get(propName))) {
                // There is a property, but it is the same in the fallback
                // and the translation file. Add it to the untranslated list.
                log("Untranslated property '" + propName + "' in file '" 
                    + baseName + "_" + locale + "'");
                // If no entry for this locale, create it.
                if (untranslated == null) {
                    untranslated = new TreeMap<String, Map<String, String>>();
                    this.untranslatedProperties.put(locale, untranslated);
                }
                // If no entry for this basename, create it.
                if (untranslated.get(baseName) == null) {
                    untranslated.put(baseName, new TreeMap<String, String>());
                }
                untranslated.get(baseName).put((String) propName, (String) props.get(propName));
                
            }
        }       
    }

    /**
     * If a data file was specified, write dbunit data for all processed
     * properties to the file.
     */
    protected void writeDataFile() {
        
        if (this.dataFile != null) {
            // Make sure not to append to an existing file.
            this.dataFile.delete();
            
            // Open writer to target file.
            openDataWriter();
            
            // Output XML file header and open dbunit dataset
            dataWriter.println("<?xml version='1.0' encoding='UTF-8'?>\n<dataset>");
            writeAllProperties();
            
            // Close dbunit dataset and close file.
            this.dataWriter.println("</dataset>");
            
            this.dataWriter.close();
        }        
    }
    
    /**
     * Make dbunit entries for all resources in the file.
     * @param fileName the relative name of the file to process (prefix
     * with srcDir to get full path name.)
     * @param isFallback true if the file is a fallback resource file, false
     * otherwise.
     * @throws BuildException on failure to find or read file.
     */
    protected void processResourceFile(String fileName, boolean isFallback) 
        throws BuildException {

        // The base name of the properties file
        String baseName;
        // The locale part of the properties file name
        String localeString;
        // The Locale object created from the localeString
        Locale locale;
        
        currentFileName = fileName;

        if (isFallback) {
            // The fallback properties file has the US translations
            reportWriter.println("Processing fallback file " + fileName);            
            locale = Locale.US;
            baseName = fileName.substring(0, fileName.indexOf(".properties"));
        } else {
            reportWriter.println("Processing localization file " + fileName);            
            // Extract the Locale portion of the file name
            int startIndex = fileName.indexOf('_');
            localeString = fileName.substring(
                startIndex + 1, fileName.indexOf('.', startIndex));
            locale = LocaleAdapter.makeLocaleFromString(localeString);
            baseName = fileName.substring(0, startIndex);
        }
        
        log("Locale is " + locale);
        log("Basename is " + baseName);
        
        // Get properties from file
        Properties props = new Properties();
        String pathName = this.srcDir + File.separator + fileName;
        try {
            props.load(new FileInputStream(pathName));
        } catch (FileNotFoundException e) {
            throw new BuildException("Cannot find file: " + pathName);
        } catch (IOException e) {
            throw new BuildException("Cannot read file: " + pathName);
        }
        // Determine which baseName the fallback file is for and
        // remember the fallbacks for that baseName.
        if (isFallback) {
            this.baseNameFallbacks.put(baseName, props);
            // Create an associated baseName map for localizations.
            this.baseNamePropertyEntries.put(
                baseName, new TreeMap<Locale, Properties>(LOCALE_COMPARATOR));
        } else {
            // Create a non-fallback entry, keyed by baseName and Locale.
            Map<Locale, Properties> baseNameLocalePropertiesMap = this.baseNamePropertyEntries
                .get(baseName);
            baseNameLocalePropertiesMap.put(locale, props);
            // Make sure the non-fallback file contains all the entries
            // of the associated fallback file. ALL LANGUAGES MUST HAVE THE
            // SAME SET OF PROPERTIES. (This ignores "en" which is empty
            // because fallbacks are the same.)
            if (!locale.toString().equals("en")) {
                validatePropertiesForLocale(props, locale, baseName);
            }
        }
    }
    
    /**
     * Write all properties to the target dbunit file.
     */
    protected abstract void writeAllProperties();
    
    /**
     * Write the report of possible problems in the translations.
     */
    protected void createResourceReport() {
        
        // Write out entire missing property files
        this.reportWriter.println();
        this.reportWriter.println();
        this.reportWriter.println("*****************************");
        this.reportWriter.println("MISSING PROPERTY FILES REPORT");
        this.reportWriter.println("*****************************");
        for (String baseName : baseNamePropertyEntries.keySet()) {
            for (Locale locale : this.supportedLocales) {
                if (!locale.toString().startsWith("en")) {
                    // Ignore the "en" locales, these are just false positives.
                    Properties baseNamePropsForLocale = 
                        baseNamePropertyEntries.get(baseName).get(locale);
                    if (baseNamePropsForLocale == null) {
                        this.reportWriter.println(
                            "Missing translation file '" + baseName + "_" + locale + "'"); 
                    }
                }
            }
        }        

        // Write out missing properties
        this.reportWriter.println();
        this.reportWriter.println();
        this.reportWriter.println("*************************");
        this.reportWriter.println("MISSING PROPERTIES REPORT");
        this.reportWriter.println("*************************");
        
        if (!missingProperties.keySet().isEmpty()) {
            log("There are missing resource key translations, " 
                + "see the translation report at " + reportFile.getPath()
                + " for more info", Project.MSG_WARN);            
        }
        
        for (Locale localeOfMissing : missingProperties.keySet()) {
            if (missingProperties.get(localeOfMissing) != null) {
                this.reportWriter.println();
                this.reportWriter.println("*************************************************");
                this.reportWriter.println("Missing properties for Locale " + localeOfMissing);
                this.reportWriter.println("*************************************************");
                for (String baseName : missingProperties.get(localeOfMissing).keySet()) {
                    this.reportWriter.println();
                    this.reportWriter.println(
                        "    ***********************************************************");
                    this.reportWriter.println(
                        "    Missing properties for property file " + baseName + "_" + localeOfMissing);
                    this.reportWriter.println(
                        "    ***********************************************************");
                    for (String propName : missingProperties.get(localeOfMissing).get(baseName)) {
                        this.reportWriter.println("    " + propName + ", fallback value is '" 
                            + this.baseNameFallbacks.get(baseName).getProperty(propName) + "'");                        
                    }
                }
            }
        }

        // Write out noFallback properties
        this.reportWriter.println();
        this.reportWriter.println();
        this.reportWriter.println("**********************************");
        this.reportWriter.println("MISSING FALLBACK PROPERTIES REPORT");
        this.reportWriter.println("**********************************");
        
        if (!noFallbackProperties.keySet().isEmpty()) {
            log("There are resource key translations without fallbacks, " 
                + "see the translation report at " + reportFile.getPath()
                + " for more info", Project.MSG_WARN);
        }
        
        for (Locale localeOfNoFallback : noFallbackProperties.keySet()) {
            if (noFallbackProperties.get(localeOfNoFallback) != null) {
                this.reportWriter.println();
                this.reportWriter.println("***************************************************");
                this.reportWriter.println("Missing fallback properties for Locale " + localeOfNoFallback);
                this.reportWriter.println("***************************************************");
                for (String baseName : noFallbackProperties.get(localeOfNoFallback).keySet()) {
                    this.reportWriter.println();
                    this.reportWriter.println(
                        "    ***********************************************************");
                    this.reportWriter.println(
                        "    Missing fallback properties for property file " + baseName + "_" + localeOfNoFallback);
                    this.reportWriter.println(
                        "    ***********************************************************");
                    for (String propName : noFallbackProperties.get(localeOfNoFallback).get(baseName)) {
                        this.reportWriter.println("    " + propName);                        
                    }
                }
            }
        }
        
        // Write out untranslated properties
        this.reportWriter.println();
        this.reportWriter.println();
        this.reportWriter.println("******************************************");
        this.reportWriter.println("PRESENT BUT UNTRANSLATED PROPERTIES REPORT");
        this.reportWriter.println("******************************************");
        
        if (!untranslatedProperties.keySet().isEmpty()) {
            log("There are resource key translations that are untranslated from the fallbacks, " 
                + "see the translation report at " + reportFile.getPath()
                + " for more info", Project.MSG_WARN);
         }
        
        for (Locale localeOfUntranslated : untranslatedProperties.keySet()) {
           if (untranslatedProperties.get(localeOfUntranslated) != null) {
                this.reportWriter.println();
                this.reportWriter.println("**********************************************");
                this.reportWriter.println("Untranslated properties for Locale " + localeOfUntranslated);
                this.reportWriter.println("**********************************************");
                for (String baseName : untranslatedProperties.get(localeOfUntranslated).keySet()) {
                    this.reportWriter.println();
                    this.reportWriter.println(
                        "    ***********************************************************");
                    this.reportWriter.println(
                        "    Untranslated properties in property file " 
                        + baseName + "_" + localeOfUntranslated);
                    this.reportWriter.println(
                        "    ***********************************************************");
                    for (Object propName : untranslatedProperties.get(localeOfUntranslated).get(baseName).keySet()) {
                        this.reportWriter.println(
                            "    " + propName + ", untranslated value is '" 
                            + untranslatedProperties.get(localeOfUntranslated).
                                get(baseName).get(propName) + "'");                        
                    }
                }
            }
        }
        
        this.reportWriter.close();
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 