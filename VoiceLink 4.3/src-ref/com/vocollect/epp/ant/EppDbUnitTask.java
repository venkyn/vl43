/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ant;

import org.dbunit.ant.DbUnitTask;

/**
 * <code>EppDbUnitTask</code> is an extension of <code>DbUnitTask</code>
 * that allows for use of a nested <code>EppDbUnitOperation</code>
 * tag.
 *
 * @author ddoubleday
 */
public class EppDbUnitTask extends DbUnitTask {
    
    /**
     * @param o the special EPP DbUnitOperation that allows for
     * token replacement.
     */
    public void addEppDbUnitOperation(EppDbUnitOperation o) {
        addOperation(o);
    }
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 