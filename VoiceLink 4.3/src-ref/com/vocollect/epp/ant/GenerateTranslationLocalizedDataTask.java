/**
 * 
 */
package com.vocollect.epp.ant;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import org.apache.tools.ant.Project;

/**
 * @author treed
 *
 */
public class GenerateTranslationLocalizedDataTask extends GenerateLocalizedData {

    private static final Long APP_ID = -10000L;
    private static final Long BASE_ID = -100L;
    
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Date currentDate = new Date();
    private Long nextId = null;
    
    /**
     * Initializes the database ID to an appropriate value.
     */
    private void initId() {
        if (currentFileName.toLowerCase().contains("voiceconsole")
                || currentFileName.toLowerCase().contains("voicelink")) {
            //If we're put in application data, start at the APP_ID
            nextId = APP_ID;
        } else {
            // If this is base EPP data, start at the base ID.
            nextId = BASE_ID;
        }
    }
    
    /**
     * Gets the next available negative ID
     * @return the next available id;
     */
    private long getNextId() {
        if (nextId == null) {
            initId();
        }
        long theId = nextId;
        nextId--;
        return theId;
    }
    
    /**
     * Write a dbunit entry for this resource property.
     * @param property the resource property to process
     * @param props the Properties set from which the property comes
     * @param locale the Locale the resource is associated with.
     */
    private void processProperty(String property, Properties props, Locale locale) {
        String keyName = property;
        printRow(props.getProperty(property), keyName, locale);
    }


    /**
     * @param translation the translation to set
     * @param keyName the enum key name
     * @param locale current Locale
     */
    private void printRow(String translation,
                              String keyName,
                              Locale locale) {
        
        // Common piece of every entry.
        this.dataWriter.print("  <voc_data_translations dataTranslationId=\"" + getNextId()
                + "\" modified=\"" + dateFormat.format(currentDate) + "\" locale=\"" 
            + locale + "\" keyValue=\"" + keyName + "\" ");
        
        this.dataWriter.println("translation=\"" + translation + "\"/>");
    }

    /**
     * Write all properties to the target dbunit file.
     */
    protected void writeAllProperties() {

        for (String baseName : baseNamePropertyEntries.keySet()) {
            for (Locale locale : this.supportedLocales) {
                Properties baseNamePropsForLocale = 
                    baseNamePropertyEntries.get(baseName).get(locale);
                if (baseNamePropsForLocale == null) {
                    baseNamePropsForLocale = 
                        this.baseNameFallbacks.get(baseName);
                    log("Using fallback properties for base name '" 
                        + baseName + "' and locale '" + locale + "'", 
                        Project.MSG_WARN);
                }
                // Iterate through the properties, creating appropriate dbunit
                // records for each one.
                for (Object propName : baseNamePropsForLocale.keySet()) {
                    log("Prop: " + propName, Project.MSG_DEBUG);
                    processProperty(
                        (String) propName, baseNamePropsForLocale, locale);
                }
            }
        }        
    }
    
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 