/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ant;

import java.io.File;

import org.dbunit.DatabaseUnitException;
import org.dbunit.ant.Operation;
import org.dbunit.dataset.IDataSet;

/**
 * The <code>EppDbUnitOperation</code> class is an extension of the
 * Ant DbUnit Operation that, for input operations, wraps the source
 * XML data set in a <code>ReplacementDataSet</code> that allows for
 * token replacement.
 *
 * @author ddoubleday
 */
public class EppDbUnitOperation extends Operation {

    /**
     * Use a ReplacementDataSet so we can use tokens to represent
     * dynamically calculated values to be inserted in the database.
     * {@inheritDoc}
     * @see org.dbunit.ant.AbstractStep#getSrcDataSet(java.io.File, java.lang.String, boolean)
     */
    @Override
    protected IDataSet getSrcDataSet(File arg0, String arg1, boolean arg2) throws DatabaseUnitException {
        IDataSet dataSet = super.getSrcDataSet(arg0, arg1, arg2);
        return DbUnitUtil.createReplacementDataSet(dataSet);
    }
    
}

*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 