/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ant;

import java.util.Date;

import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;


/**
 * This centralizes creation of a ReplacementDataSet so the
 * same thing is always done from the Ant task and DbUnitAdapter.
 *
 * @author ddoubleday
 */
public final class DbUnitUtil {

    // The token used to represent null in FlatXML data sets.
    public static final String NULL_REPLACEMENT_TOKEN = "[null]";
    // The token used to represent the current time in FlatXML data sets.
    public static final String TODAY_REPLACEMENT_TOKEN = "@DATE_TODAY@";
    // The token used to represent the time 24 hours ago in FlatXML data sets.
    public static final String YESTERDAY_REPLACEMENT_TOKEN = "@DATE_YESTERDAY@";
    // The token used to represent the time 9 hours ago in FlatXML data sets.
    public static final String NINE_HOURS_AGO_REPLACEMENT_TOKEN = "@DATE_NINE_HOURS_AGO@";
    // The token used to represent the time 11 hours ago in FlatXML data sets.
    public static final String ELEVEN_HOURS_AGO_REPLACEMENT_TOKEN = "@DATE_ELEVEN_HOURS_AGO@";
    // The token used to represent the time 48 hours ago in FlatXML data sets.
    public static final String TWO_DAYS_AGO_REPLACEMENT_TOKEN = "@DATE_TWO_DAYS_AGO@";
    // The token used to represent the time 72 hours ago in FlatXML data sets.
    public static final String THREE_DAYS_AGO_REPLACEMENT_TOKEN = "@DATE_THREE_DAYS_AGO@";
    // The token used to represent the time 7 days ago in FlatXML data sets.
    public static final String SEVEN_DAYS_AGO_REPLACEMENT_TOKEN = "@DATE_SEVEN_DAYS_AGO@";
    // The token used to represent the time 10 days ago in FlatXML data sets.
    public static final String TEN_DAYS_AGO_REPLACEMENT_TOKEN = "@DATE_TEN_DAYS_AGO@";
    
    private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;
    
    private static final long MILLIS_PER_HOUR = 60 * 60 * 1000;
    
    // Day/Hour Multipliers
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int SEVEN = 7;
    private static final int NINE = 9;
    private static final int TEN = 10;
    private static final int ELEVEN = 11;

    /**
     * Constructor.
     */
    private DbUnitUtil() {
        
    }
    
    /**
     * Create the DataSet for the operation.
     * <p>
     * Important! Any replacement token changes made here must also be
     * made to <code>EppDbUnitOperation</code>, and, if the data is 
     * installation data, to <code>DbUnitInstallerAdapter.</code>
     * @param dataSet the base data set.
     * @return a ReplacementDataSet that converts certain tokens in the input
     * stream.
     * @throws DataSetException on failure to create data set
     */
    public static ReplacementDataSet createReplacementDataSet(IDataSet dataSet)
        throws DataSetException {
        
        ReplacementDataSet repDataSet = new ReplacementDataSet(dataSet);
        repDataSet.addReplacementObject(NULL_REPLACEMENT_TOKEN, null);
        repDataSet.addReplacementObject(TODAY_REPLACEMENT_TOKEN, new Date());
        repDataSet.addReplacementObject(YESTERDAY_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - MILLIS_PER_DAY));
        repDataSet.addReplacementObject(NINE_HOURS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - NINE * MILLIS_PER_HOUR));
        repDataSet.addReplacementObject(ELEVEN_HOURS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - ELEVEN * MILLIS_PER_HOUR));
        repDataSet.addReplacementObject(TWO_DAYS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - TWO * MILLIS_PER_DAY));
        repDataSet.addReplacementObject(THREE_DAYS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - THREE * MILLIS_PER_DAY));
        repDataSet.addReplacementObject(SEVEN_DAYS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - SEVEN * MILLIS_PER_DAY));
        repDataSet.addReplacementObject(TEN_DAYS_AGO_REPLACEMENT_TOKEN, 
            new Date(System.currentTimeMillis() - TEN * MILLIS_PER_DAY));
        return repDataSet;
    }

}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 