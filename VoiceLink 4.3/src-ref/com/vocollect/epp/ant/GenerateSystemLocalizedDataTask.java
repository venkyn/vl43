/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.ant;

import com.vocollect.epp.util.LocaleAdapter;
import com.vocollect.epp.util.QueryUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.MatchingTask;


/**
 * This Ant task copies localized system resources that need to be in the
 * database for sorting purposes into a dbunit file suitable for insert.
 * <p>
 * Since entries need to be made for all supported Locales in order for sorting
 * table joins to work properly, the task also make pseudo-entries for languages
 * that haven't been translated yet, simply inserting the English translation.
 * <p>
 * The original US English resources are assumed to be only in the "fallback"
 * resource files (i.e., those with no Locale specified in the name.)
 *
 * @author ddoubleday
 */
public class GenerateSystemLocalizedDataTask extends GenerateLocalizedData {

    /**
     * Write a dbunit entry for this resource property.
     * @param property the resource property to process
     * @param props the Properties set from which the property comes
     * @param locale the Locale the resource is associated with.
     */
    private void processProperty(String property, Properties props, Locale locale) {

        Integer enumCode = null;
        String keyName;

        if (isEnumProperty(property)) {
            // Enum property must have an int code key and the Enum class
            // name will be used for the String key portion.

            // Pull off the number part, which is the last piece of the resource
            // name.
            int lastDot = property.lastIndexOf('.');
            enumCode = Integer.valueOf(property.substring(lastDot + 1));
            keyName = property.substring(0, lastDot);

            // One fake row is created in the database for each enum type to support
            // queries that might have null columns. Having an entry for null
            // the SystemTranslation table allows inner joins to work correctly.
            if (processedEnumsMap.get(keyName) == null) {
                // Remember we have generated the fake entry for this one.
                processedEnumsMap.put(keyName, keyName);
                printEnumRow(DbUnitUtil.NULL_REPLACEMENT_TOKEN,
                    QueryUtil.ENUM_NULL_MATCH_CODE, keyName, locale);
            }

        } else {
            // Ordinary property. The property name itself is the database key
            keyName = property;
        }

        printEnumRow(props.getProperty(property), enumCode, keyName, locale);
    }


    /**
     * @param translation the translation to set
     * @param enumCode the enum code
     * @param keyName the enum key name
     * @param locale current Locale
     */
    private void printEnumRow(String translation,
                              Integer enumCode,
                              String keyName,
                              Locale locale) {

        // Common piece of every entry.
        this.dataWriter.print("  <voc_system_translations version=\"0\" locale=\""
            + locale + "\" keyValue=\"");

        this.dataWriter.print(keyName + "\" code=\"");

        // There is a code only for enum entries.
        if (enumCode != null) {
            this.dataWriter.print(enumCode + "\" ");
        } else {
            this.dataWriter.print("0\" ");
        }

        this.dataWriter.println("translation=\"" + translation + "\"/>");
    }

    /**
     * Write all properties to the target dbunit file.
     */
    protected void writeAllProperties() {

        for (String baseName : baseNamePropertyEntries.keySet()) {
            for (Locale locale : this.supportedLocales) {
                Properties baseNamePropsForLocale =
                    baseNamePropertyEntries.get(baseName).get(locale);
                if (baseNamePropsForLocale == null) {
                    baseNamePropsForLocale =
                        this.baseNameFallbacks.get(baseName);
                    log("Using fallback properties for base name '"
                        + baseName + "' and locale '" + locale + "'",
                        Project.MSG_WARN);
                }
                // Iterate through the properties, creating appropriate dbunit
                // records for each one.
                for (Object propName : baseNamePropsForLocale.keySet()) {
                    log("Prop: " + propName, Project.MSG_DEBUG);
                    processProperty(
                        (String) propName, baseNamePropsForLocale, locale);
                }
            }
        }
    }
}
*******************************************************************************
WARNING: THIS IS A CORE VOICELINK CLASS. IF YOU NEED TO CHANGE THE BEHAVIOR
OF THIS CLASS, THE RECOMMENDED APPROACH IS TO MAKE A SUBCLASS EXTENSION 
FROM THIS CLASS (OR USE THE PRE-EXTENDED SUBCLASS IN "src-custom", IF PROVIDED)
AND OVERRIDE THE METHOD THAT YOU NEED TO CHANGE. SEE THE CUSTOMIZATION GUIDE.
*******************************************************************************

 