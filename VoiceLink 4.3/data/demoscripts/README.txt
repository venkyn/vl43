

The problem we are trying to solve with the scripts contained in the demoscripts folders is the ability to automate 
the generation of a demo data file, or files, which can be used by the Senior Business Consultants (SBC's) to 
demonstrate the various applications and features of VoiceLink.
Historically, creating the demo data has been an extremely time consuming and painful process.   
The scripts in the underlying folders were created with the goal of using them to automate the process of creating 
a demo data file that can be used in conjunction with the demo data tool.
Please see the readme files in the various folders to understand how to run the scripts in each folder 
and what they are being used for.

The overall concept for creating the demo data file(s) is as follows.
1. Install VoiceLink
2. Import a 10 user demo license into VoiceLink that restricts the number of devices that can connect to the VoiceLink system.
3. Run the region scripts to create regions for the VoiceLink applications.
4. Import the data into VoiceLink using VoiceLink’s standard import process.
5. Run various VirtualWarehouse scenarios for all applications to get the data in a state that will be usable by the SBC's 
   to demo the VoiceLink system.
6. Use the demo tool to generate a new ResetDb.xml and DemoData.xml file for a particular release of VoiceLink.


