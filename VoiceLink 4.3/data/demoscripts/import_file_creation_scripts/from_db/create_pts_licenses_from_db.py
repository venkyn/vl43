'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a pts license/put import file for VoiceLink 
# by reading the current set of license id's from the data base and generating a 
# new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of distinct ids of the pts licenses to include in the file.
pts_ids_cursor = cnxn.cursor()

# select the distinct license id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.

pts_ids_cursor.execute("select distinct licenseId from pts_licenses order by licenseId")
licenses = pts_ids_cursor.fetchall()
if not licenses:
    print "No pts licenses were found in the database" 
    print "Please check that the pts_licenses table in your database contains rows" 
    print "and verify that the query being run by this script to select the licenses is actually returning data"    
else:
    puts_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('ptslic_demo_data_from_db.dat', 'w')

    # build the import file 
    for license in licenses:
        print "pts license id = " + str(license.licenseId)
        query = "select l.licenseNumber, cr.regionNumber, ptsLoc.customerNumber, cl.scannedVerification, \
                 ci.itemNumber, p.quantityToPut, p.unitOfMeasure, p.sequenceNumber, p.allowOverPack, p.residualQuantity \
                 from pts_licenses l \
                 join pts_puts p on l.licenseId = p.licenseId \
                 join core_regions cr on cr.regionId = l.regionId \
                 join core_items ci on ci.itemId = p.itemId \
                 left outer join pts_customer_locations ptsloc on ptsloc.customerLocationId = p.customerLocationId \
                 left outer join core_locations cl on cl.locationId =  ptsLoc.locationId \
                 where l.licenseId = " + str(license.licenseId)

        puts_cursor.execute(query)
        puts = puts_cursor.fetchall()
                
        for put in puts:
            file.writelines(str(put.licenseNumber).ljust(50)
                + str(put.regionNumber).ljust(9)                
                + str(put.customerNumber).ljust(50)
                + str(put.scannedVerification).ljust(50)  
                + str(put.itemNumber).ljust(50)                  
                + str(put.quantityToPut).ljust(9)
                + str(put.unitOfMeasure).ljust(50)         
                + str(put.sequenceNumber).ljust(9)
                + str(put.allowOverPack)    
                + str(put.residualQuantity).ljust(9)
                + "\n" )
    file.close()         
