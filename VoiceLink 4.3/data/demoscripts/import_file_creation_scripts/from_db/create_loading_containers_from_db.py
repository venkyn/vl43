'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a loading containers file for VoiceLink by reading 
# the current set of container id's from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of distinct ids of the containers to include in the file.
container_ids_cursor = cnxn.cursor()

# select the distinct container id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.

container_ids_cursor.execute("select distinct containerId from loading_containers order by containerId")
rows = container_ids_cursor.fetchall()
if not rows:
    print "No containers were found in the database" 
    print "Please check that the loading_containers table in your database contains rows" 
    print "and verify that the query being run by this script to select the routes is actually returning data"    
else:
    container_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('loadcontainer_demo_data_from_db.dat', 'w')
    # build the import file 
    for row in rows:
        print "containerId = " + str(row.containerId)
        query = "select lc.containerId, lc.containerNumber, lr.routeNumber, ls.stopNumber, lr.departureDateTime, \
                cr.regionNumber, lc.expectedCube, lc.expectedWeight \
                from loading_containers lc \
                join loading_stops ls on ls.stopId = lc.stopId \
                join loading_routes lr on lr.routeId = ls.routeId \
                join core_regions cr on cr.regionId = lr.regionId \
                where lc.containerId = " + str(row.containerId)
        
        container_cursor.execute(query)
        container = container_cursor.fetchone()
        # The following fields are NOT required and therefore can be empty.
        # If the field is empty then pyodbc returns "NONE" as the field value and we do 
        # not want this in the data file so if the value returned is "None" then 
        # set the field to an empty string.
        expectedCube = ""
        expectedWeight = ""
        if container.expectedCube is not None: 
            expectedCube = str(container.expectedCube)
        if container.expectedWeight is not None: expectedWeight = str(container.expectedWeight)
            
        # remove '-' and ':' from departure date and time    
        temp = str(container.departureDateTime).split(" ")
        departure_date_parts = temp[0].split("-")
        departure_time_parts = temp[1].split(":")
        departure_time_more_parts = str(departure_time_parts[2]).split(".")
        departureDateTime = str(departure_date_parts[0]) + str(departure_date_parts[1]) + str(departure_date_parts[2] + \
                                departure_time_parts[0]) + str(departure_time_parts[1]) + str(departure_time_more_parts[0])    
           
        file.writelines(str(container.containerNumber).ljust(50)
            + str(container.routeNumber).ljust(50)
            + str(container.stopNumber).ljust(9)
            + str(departureDateTime).ljust(14)
            + str(container.regionNumber).ljust(9)
            + str(expectedCube).ljust(9)
            + str(expectedWeight).ljust(9)
            + "\n")
    file.close()