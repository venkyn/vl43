'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a cycle counting assignment import file for VoiceLink 
# by reading the current set of items from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of distinct ids of the cycle counting assignments to include in the file.
assignment_ids_cursor = cnxn.cursor()
# select the distinct location id's currently being used in the database
# You can change this query to select different assignments and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
assignment_ids_cursor.execute("select locationid, * from cc_assignments ca order by ca.locationid")
rows = assignment_ids_cursor.fetchall()
if not rows:
    print "No cycle counting assignments were found in the database." 
    print "Please check that the cc_assignments table in your database contains rows" 
    print "and verify that the query being run by this script to select data is actually returning data"
else:
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('cc_demo_data_from_db.dat', 'w')
    cc_details_cursor = cnxn.cursor()
    # build the import file 
    for row in rows:
        print "cc location id = " + str(row.locationId)
        query = "select cl.scannedVerification, cr.regionNumber, ci.itemNumber, cd.unitOfMeasure, cd.expectedQuantity \
                from cc_assignments ca \
                join core_locations cl on ca.locationId = cl.locationId \
                join core_regions cr on ca.regionId = cr.regionid \
                join cc_details cd on cd.cycleCountId = ca.cycleCountId \
                join core_items ci on ci.itemId = cd.itemId \
                where ca.locationId = " + str(row.locationid)

        cc_details_cursor.execute(query)
        cc_details = cc_details_cursor.fetchone()
        if cc_details:
            file.writelines(str(cc_details.scannedVerification).ljust(50)
                + str(cc_details.regionNumber).ljust(9)
                + str(cc_details.itemNumber).ljust(50)
                + str(cc_details.unitOfMeasure).ljust(50)    
                + str(cc_details.expectedQuantity).ljust(9)
                + "\n" )
    file.close()         
