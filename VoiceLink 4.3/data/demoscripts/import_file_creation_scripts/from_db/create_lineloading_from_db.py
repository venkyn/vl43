'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a lineloading import file for VoiceLink by reading 
# the current set of lineloading routes and stops from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')
# this cursor will contain the list of distinct ids of the lineloading to include in the file.
lineloading_ids_cursor = cnxn.cursor()

# select the distinct lineloading id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
lineloading_ids_cursor.execute("select distinct cartonId from lineload_cartons order by cartonId")
rows = lineloading_ids_cursor.fetchall()
if not rows:
    print "No lineloading information was found in the database." 
    print "Please check that the lineloading cartons table in your database contains data and verify " 
    print "that the query being run by this script to select the cartions is actually returning data"    
else:
    carton_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('ctn_demo_data_from_db.dat', 'w')
    # build the import file 
    for row in rows:
        print "route stop  id = " + str(row.cartonId)
        query = "select c.cartonNumber, rs.spur, rs.route, rs.stop, rs.wave, c.cartonType, cr.regionNumber \
                from lineload_route_stops rs \
                join lineload_cartons c on c.routeStopId = rs.routeStopId \
                join core_regions cr on cr.regionid = rs.regionid \
                where c.cartonId = " + str(row.cartonId)
        carton_cursor.execute(query)
        carton = carton_cursor.fetchone()

        file.writelines(str(carton.cartonNumber).ljust(50) 
            + str(carton.spur).ljust(10)
            + str(carton.route).ljust(50)
            + str(carton.stop).ljust(50)
            + str(carton.wave).ljust(50) 
            + str(carton.cartonType)
            + str(carton.regionNumber).ljust(9)
            + str(" ")
            + "\n" )
    file.close()        
