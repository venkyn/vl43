'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a loading route/stop file for VoiceLink by reading 
# the current set of route id's from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of distinct ids of the routes to include in the file.
route_ids_cursor = cnxn.cursor()

# select the distinct route id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.

route_ids_cursor.execute("select distinct routeId  from loading_routes r order by r.routeId")
routes = route_ids_cursor.fetchall()
if not routes:
    print "No routes were found in the database" 
    print "Please check that the loading_routes table in your database contains rows" 
    print "and verify that the query being run by this script to select the routes is actually returning data"    
else:
    stops_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('loadrs_demo_data_from_db.dat', 'w')

    # build the mport file 
    for route in routes:
        print "route id = " + str(route.routeId)
        query = "select lr.routeId, lr.routeNumber, ls.stopNumber, cl.scannedVerification, lr.departureDateTime, \
                 lr.trailer, ls.expectedNumberOfContainers, cr.regionNumber, ls.customerNumber, \
                 lr.captureLoadPosition, lr.note as routeNote, ls.note as stopNote \
                 from loading_routes lr join loading_stops ls on lr.routeId = ls.routeId \
                 join core_regions cr on lr.regionId = cr.regionid \
                 join core_locations cl on cl.locationId = lr.dockDoorLocationID \
                 where lr.routeId = " + str(route.routeId) + " order by lr.routeId"

        stops_cursor.execute(query)
        stops = stops_cursor.fetchall()
        for stop in stops:
            # The following fields are NOT required and therefore can be empty.
            # If the field is empty then pyodbc returns "NONE" as the field value and we do 
            # not want this in the data file so if the value returned is "None" then 
            # set the field to an empty string.
            trailer = ""
            routeNote = ""
            stopNote = ""
            if stop.trailer is not None: trailer = str(stop.trailer)
            if stop.routeNote is not None: routeNote = str(stop.routeNote)
            if stop.stopNote is not None: stopNote = str(stop.stopNote)
            
            # remove '-' and ':' from departure date and time    
            temp = str(stop.departureDateTime).split(" ")
            departure_date_parts = temp[0].split("-")
            departure_time_parts = temp[1].split(":")
            departure_time_more_parts = str(departure_time_parts[2]).split(".")
            departureDateTime = str(departure_date_parts[0]) + str(departure_date_parts[1]) + str(departure_date_parts[2] + \
                                    departure_time_parts[0]) + str(departure_time_parts[1]) + str(departure_time_more_parts[0])    
            
            file.writelines(str(stop.routeNumber).ljust(50)
                + str(stop.stopNumber).ljust(9)
                + str(stop.scannedVerification).ljust(50)
                + str(departureDateTime)
                + str(trailer).ljust(9)
                + str(stop.expectedNumberOfContainers).ljust(9)
                + str(stop.regionNumber).ljust(9)
                + str(stop.customerNumber).ljust(50)
                + str(stop.captureLoadPosition)
                + str(routeNote).ljust(255)
                + str(stopNote).ljust(255)
                + "\n")
    file.close()