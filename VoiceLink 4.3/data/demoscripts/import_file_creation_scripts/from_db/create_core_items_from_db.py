'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a core items import file for VoiceLink 
# by reading the current set of items from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')
# this cursor will contain the list of distinct ids of the locations to include in the file.
item_ids_cursor = cnxn.cursor()
# select the distinct item id's currently being used in the picks 
# You can change this query to select different items and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
item_ids_cursor.execute("select distinct (i.itemId) from sel_picks p join core_items i on p.itemId = i.itemid")
#item_ids_cursor.execute("select distinct (itemId) from core_items")
rows = item_ids_cursor.fetchall()
if not rows:
    print "No Items were found in the database." 
    print "Please check that the core_items table in your database contains rows" 
    print "and verify that the query being run by this script to select item data is actually returning data"
else:
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('itm_demo_data_from_db.dat', 'w')
    item_cursor = cnxn.cursor()
    # build the import file 
    for row in rows:
        query = "select itemNumber, itemDescription, phoneticDescription, itemSize, pack, weight, cube, upc, \
                isVariableWeightItem, variableWeightTolerance, scanVerificationCode, spokenVerificationCode, isSerialNumberItem \
                from core_items where itemid = " + str(row.itemId) 
        item_cursor.execute(query)
        item = item_cursor.fetchone()
        if item:
            print "item number = " + str(item.itemNumber)
            # The following fields are NOT required and therefore can be empty.
            # If the field is empty then pyodbc returns "NONE" as the field value and we do 
            # not want this in the data file so if the value returned is "None" then 
            # set the field to an empty string.
            phoneticDescription = ""
            cube = ""
            itemSize = ""
            upc = ""
            pack = ""
            variableWeightTolerance = ""
            scanVerificationCode =  ""
            spokenVerificationCode = ""
            isSerialNumberItem = ""
            if item.phoneticDescription is not None:     phonetic_description = str(item.phoneticDescription)
            if item.cube is not None:                    cube = str(item.cube)            
            if item.itemSize is not None:                itemSize = str(item.itemSize)            
            if item.upc is not None:                     upc = str(item.upc)            
            if item.pack is not None:                    pack = str(item.pack)            
            if item.variableWeightTolerance is not None: variableWeightTolerance = str(item.variableWeightTolerance)        
            if item.scanVerificationCode is not None:    scanVerificationCode = str(item.scanVerificationCode)            
            if item.spokenVerificationCode is not None:  spokenVerificationCode = str(item.spokenVerificationCode)            
            if item.isSerialNumberItem is not None:      isSerialNumberItem = str(item.isSerialNumberItem)            

            file.writelines(str(item.itemNumber).ljust(50) # Item Number
                + str(item.itemDescription).ljust(50)      # Item Description
                + str(phoneticDescription).ljust(255)      # Phonetic Description
                + str(item.weight).ljust(10)               # Weight
                + str(cube).ljust(10)                      # Cube
                + str(itemSize).ljust(50)                  # itemSize
                + str(upc).ljust(50)                       # UPC
                + str(pack).ljust(50)                      # pack
                + str(item.isVariableWeightItem)           # variable weight item
                + str(variableWeightTolerance).ljust(3)    # variable weight tolerance
                + str(scanVerificationCode).ljust(50)      # scanned product verification id
                + str(spokenVerificationCode).ljust(5)     # spoken product verification id
                + str(isSerialNumberItem)                  # capture serial number            
                + "\n" )
    file.close()         
