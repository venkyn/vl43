'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a assignment\pick import file for VoiceLink 
# by reading the current set of assignment id's from the data base and generating a 
# new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')
# this cursor will contain the list of distinct ids of the assignments to include in the file.
assignment_ids_cursor = cnxn.cursor()
# select the distinct assignments id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
assignment_ids_cursor.execute("select distinct assignmentid from sel_assignments")
assignments = assignment_ids_cursor.fetchall()
if not assignments:
    print "No Assignments were found in the database" 
    print "Please check that the sel_assignments table in your database contains rows" 
    print "and verify that the query being run by this script to select the assignments is actually returning data"    
else:
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('ap_demo_data_from_db.dat', 'w')
    picks_cursor = cnxn.cursor()
    # build the import file 
    for assignment in assignments:
        print "Assignment id = " + str(assignment.assignmentid)
        query = "select a.assignmentid, a.assignmentNumber, a.deliveryDate, a.deliveryLocation, a.route, \
            a.customerNumber, a.customerName, a.customerAddress, a.goalTime, p.sequenceNumber, \
            p.quantityToPick, i.itemNumber, p.baseItemOverride, l.scannedVerification, p.triggerReplenish, \
            r.regionNumber, p.caseLabelCheckDigit, p.cartonNumber, p.unitOfMeasure, a.workIdentifierValue, \
            p.targetContainerIndicator, p.promptMessage, a.departureDateTime, lr.regionNumber as loadingRegionNumber\
            from sel_assignments a \
            join sel_picks p on a.assignmentid = p.assignmentId \
            join core_locations l on p.locationId = l.locationId \
            left outer join core_regions r on a.regionId = r.regionId \
            left outer join core_regions lr on lr.regionId = a.loadingRegionId \
            join core_items i on i.itemid = p.itemid \
            where a.assignmentid = " + str(assignment.assignmentid) 

        picks_cursor.execute(query)
        picks = picks_cursor.fetchall()
        for pick in picks:
            # The following fields are NOT required and therefore can be empty.
            # If the field is empty then pyodbc returns "NONE" as the field value and we do 
            # not want this in the data file so if the value returned is "None" then 
            # set the field to an empty string.
            deliveryLocation = ""
            customerName = "Customer " + pick.customerNumber + " name"
            customerAddress = "Customer " + pick.customerNumber + " address"
            baseItemOverride = ""
            triggerReplenish = " "
            caseLabelCheckDigit = ""
            cartonNumber = ""
            unitOfMeasure = ""
            targetContainerIndicator = ""
            promptMessage = ""
            departureDateTime = ""
            loadingRegionNumber = "" 
            if pick.deliveryLocation is not None:           deliveryLocation = str(pick.deliveryLocation)
            if pick.customerName is not None:               customerName = str(pick.customerName)
            if pick.customerAddress is not None:            customerAddress = str(pick.customerAddress)  
            if pick.triggerReplenish != 0:                  triggerReplenish = str(pick.triggerReplenish)  
            if pick.caseLabelCheckDigit is not None:        caseLabelCheckDigit = str(pick.caseLabelCheckDigit)  
            if pick.caseLabelCheckDigit is not None:        caseLabelCheckDigit = str(pick.caseLabelCheckDigit) 
            if pick.cartonNumber is not None:               cartonNumber = str(pick.cartonNumber) 
            if pick.unitOfMeasure is not None:              unitOfMeasure = str(pick.unitOfMeasure) 
            if pick.targetContainerIndicator is not None:   targetContainerIndicator = str(pick.targetContainerIndicator) 
            if pick.promptMessage is not None:              promptMessage = str(pick.promptMessage) 
            if pick.loadingRegionNumber is not None:        loadingRegionNumber = str(pick.loadingRegionNumber)                 
             
            # convert deliver date from date time to a string with just the date portion.
            temp = str(pick.deliveryDate).split(" ")
            date_parts = temp[0].split("-")
            deliveryDate = str(date_parts[0]) + str(date_parts[1]) + str(date_parts[2])

            # remove '-' and ':' from departure date and time    
            if pick.departureDateTime is not None:
                temp = str(pick.departureDateTime).split(" ")
                departure_date_parts = temp[0].split("-")
                departure_time_parts = temp[1].split(":")
                departure_time_more_parts = str(departure_time_parts[2]).split(".")
                departureDateTime = str(departure_date_parts[0]) + str(departure_date_parts[1]) + str(departure_date_parts[2] + \
                                        departure_time_parts[0]) + str(departure_time_parts[1]) + str(departure_time_more_parts[0])       
              
            file.writelines(str(pick.assignmentNumber).ljust(18) # Assignment number
                + str(deliveryDate).ljust(8)                
                + str(deliveryLocation).ljust(9)
                + str(pick.route).ljust(50)                 
                + str(pick.customerNumber).ljust(50)
                + str(customerName).ljust(50)               
                + str(customerAddress).ljust(50)
                + str(int(pick.goalTime)).ljust(10)         
                + str(pick.sequenceNumber).ljust(9)
                + str(pick.quantityToPick).ljust(9)         
                + str(pick.itemNumber).ljust(50)
                + str(pick.baseItemOverride)                
                + str(" ")
                + str(pick.scannedVerification).ljust(50)   
                + str(triggerReplenish)
                + str(pick.regionNumber).ljust(9)           
                + str(" ") # filler not not used.
                + str(caseLabelCheckDigit).ljust(5)         
                + str(cartonNumber).ljust(50)
                + str(unitOfMeasure).ljust(50)  
                + str(pick.workIdentifierValue).ljust(18)
                + str(targetContainerIndicator).ljust(2)
                + str(promptMessage).ljust(250)
                + str(departureDateTime).ljust(14)
                + str(loadingRegionNumber).ljust(9)
                + "\n" )
    file.close()         
