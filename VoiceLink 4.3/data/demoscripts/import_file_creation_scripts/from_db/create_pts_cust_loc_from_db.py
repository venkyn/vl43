'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a put to store customer-location import file for VoiceLink 
# by reading the current set of PTS customer locations from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of PTS Customer Locations include in the file.
pts_ids_cursor = cnxn.cursor()

# select the distinct pts customer location id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
pts_ids_cursor.execute("select distinct customerLocationId from pts_customer_locations")
rows = pts_ids_cursor.fetchall()
if not rows:
    print "No pts customer location  information was found in the database." 
    print "Please check that the pts customer location table in your database contains data and verify " 
    print "that the query being run by this script to select the assighments is actually returning data"    
else:
    customer_location_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('ptsCustLoc_demo_data_from_db.dat', 'w')

    # build the import file 
    for row in rows:
        print "customer_location id = " + str(row.customerLocationId)
        
        query = "select customerNumber, cl.scannedVerification, route, deliveryLocation, \
                    customerName, customerAddress \
                    from pts_customer_locations pts \
                    join core_locations cl on cl.locationId = pts.locationId \
                    where pts.customerLocationId = " + str(row.customerLocationId)
        
                   
        customer_location_cursor.execute(query)
        customerLocation = customer_location_cursor.fetchone()
         
        file.writelines(str(customerLocation.customerNumber).ljust(50) 
            + str(customerLocation.scannedVerification).ljust(50)
            + str(customerLocation.scannedVerification).ljust(50)
            + str(customerLocation.route).ljust(50)
            + str(customerLocation.deliveryLocation).ljust(9)
            + str(customerLocation.customerName).ljust(50) 
            + str(customerLocation.customerAddress).ljust(50)
            + "\n" )
          
    file.close()        
