'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a replenishment import file for VoiceLink by reading 
# the current set of replenishment assignments from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')
# this cursor will contain the list of distinct ids of the assignments to include in the file.
replen_ids_cursor = cnxn.cursor()
# select the distinct replenishment assignment id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
replen_ids_cursor.execute("select distinct(replenishId) from replen_replenishments order by replenishId")
rows = replen_ids_cursor.fetchall()
if not rows:
    print "No replenishment assignments were found in the database" 
    print "Please check that the replenishment table in your database contains data and verify " 
    print "that the query being run by this script to select the assighments is actually returning data"    
else:
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('replen_demo_data_from_db.dat', 'w')
    replenishment_cursor = cnxn.cursor()
    # build the import file 
    for row in rows:
        print "Assignment id = " + str(row.replenishId)
        query = "select r.replenishNumber, r.licenseNumber, r.status, r.replenishType, \
                    r.goalTime, r.promptLicense, r.scannedValidation, r.spokenValidation, \
                    cr.regionNumber, ci.itemNumber, r.quantityToReplenish, \
                    clFrom.scannedVerification as reservedScannedVerification, \
                    clTo.ScannedVerification as destinationScannedVerification \
                    from replen_replenishments r \
                    join core_items ci on ci.itemId = r.itemId \
                    join core_regions cr on cr.regionId = r.regionId \
                    left outer join core_locations clTo on clTo.locationId = r.toLocationId \
                    left outer join core_locations clFrom on clFrom.locationId = r.fromLocationId \
                    where r.replenishId = " + str(row.replenishId)
        replenishment_cursor.execute(query)
        replenishment = replenishment_cursor.fetchone()
        
        # The following fields are NOT required and therefore can be empty.
        # If the field is empty then pyodbc returns "NONE" as the field value and we do 
        # not want this in the data file so if the value returned is "None" then 
        # set the field to an empty string.
        goalTime = ""
        scannedValidation = ""
        spokenValidation = ""
        quantityToReplenish = "" 
        if replenishment.goalTime is not None:              goalTime = str(replenishment.goalTime)
        if replenishment.scannedValidation is not None:     scannedValidation = str(replenishment.scannedValidation)
        if replenishment.spokenValidation is not None:      spokenValidation = str(replenishment.spokenValidation)
        if replenishment.quantityToReplenish is not None:   quantityToReplenish = str(replenishment.quantityToReplenish)
            
        # NOTE:  validation values for replenihsment are a bit confusing so I will attempt to explain how they work.
        #   The scannedValidation and spokenValidation are optional when importing replenishment assignments.
        #   If the scannedValidation and/or spokenValidation values are provided in the import file then the operator is 
        #   required to speak or scan the value to confirm the pickup location (from location).
        #   If these values are NOT imported, then the system will use the scannedValidation or the spokenValidation
        #   values from the Location record. This is why the scannedValidation and spokenValidation values in the 
        #   replenishment record can be empty.
        #
         
        file.writelines(str(replenishment.replenishNumber).ljust(50) 
            + str(replenishment.licenseNumber).ljust(50)
            + str(replenishment.status)
            + str(replenishment.replenishType)
            + str(goalTime).ljust(10)
            + str(replenishment.promptLicense) 
            + str(scannedValidation).ljust(50)           # this does NOT have to match a location
            + str(spokenValidation).ljust(5)             # this does NOT have to match a location  
            + str(replenishment.regionNumber).ljust(9)
            + str(" ")
            + str(replenishment.itemNumber).ljust(50)
            + str(quantityToReplenish).ljust(9)     
            + str(replenishment.reservedScannedVerification).ljust(50)      # this MUST match a location
            + str(replenishment.destinationScannedVerification).ljust(50)   # this MUST match a location            
            + "\n" )
          
    file.close()        
