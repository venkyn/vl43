'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a putaway import file for VoiceLink 
# by reading the current set of putaway licenses from the data base and generating a 
# new file.

import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')

# this cursor will contain the list of distinct ids of the assignments to include in the file.
putaway_ids_cursor = cnxn.cursor()

# select the distinct putawya license id's that are currently in the database
# You can change this query to select different set of id's and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.

putaway_ids_cursor.execute("select distinct licenseId from putaway_licenses order by licenseId")
rows = putaway_ids_cursor.fetchall()
if not rows:
    print "No putaway licenses were found in the database" 
    print "Please check that the licenses table in your database contains data" 
    print "and verify that the query being run by this script to select the licenses is actually returning data"    
else:
    license_cursor = cnxn.cursor()
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('palic_demo_data_from_db.dat', 'w')

    # build the import file 
    for row in rows:
        print "License id = " + str(row.licenseId)
        query = "select pl.licenseNumber, pl.dateReceived, pl.expirationFlag, pl.quantityReceived, \
                    cl.scannedVerification, ci.itemNumber, cr.regionNumber, cr.goalRate  \
                    from putaway_licenses pl \
                    join core_locations cl on pl.locationid = cl.locationId \
                    join core_items ci on pl.itemId = ci.itemId \
                    join core_regions cr on pl.regionId = cr.regionId \
                    where pl.licenseId = " + str(row.licenseId)
        license_cursor.execute(query)
        license = license_cursor.fetchone()
        
        # The following fields are NOT required and therefore can be empty.
        # If the field is empty then pyodbc returns "NONE" as the field value and we do 
        # not want this in the data file so if the value returned is "None" then 
        # set the field to an empty string.
        expirationFlag = " ";
        dateReceived = ""
        quantityReceived = ""
        locationIdentifier = ""
        itemNumber = ""
        goalTime = ""
        if license.dateReceived is not None:
            temp = str(license.dateReceived).split(" ")
            # remove '-' from dateReceived
            dateReceived_date_parts = temp[0].split("-")
            dateReceived = str(dateReceived_date_parts[0]) + str(dateReceived_date_parts[1]) + str(dateReceived_date_parts[2]) 
         
        if license.expirationFlag is not None: expirationFlag = str(license.expirationFlag)
        if license.quantityReceived is not None: quantityReceived = str(license.quantityReceived)
        if license.scannedVerification is not None: locationIdentifier = str(license.scannedVerification)
        if license.itemNumber is not None: itemNumber = str(license.itemNumber)
        if license.goalRate is not None: goalTime = str(license.goalRate)
                
        file.writelines(str(license.licenseNumber).ljust(50) # License Number
            + str(dateReceived).ljust(8) 
            + str(expirationFlag)
            + str(quantityReceived).ljust(9)
            + str(locationIdentifier).ljust(50)
            + str(itemNumber).ljust(50)
            + str(license.regionNumber).ljust(9)
            + str(" ")
            + str(goalTime).ljust(10)
            + "\n" )
    file.close()        
