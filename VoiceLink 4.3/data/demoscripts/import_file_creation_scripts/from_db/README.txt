

The scripts in this folder are designed to re-create import files that can be 
imported into VoiceLink from data that is contained in a VoiceLink Database.


The idea is that if you  need to change the demo data in anyway for a particular
release of VoiceLink, you can load the DemoData.xml file.  Modify the data in the database
and then re-generate the import files that can be imported into VoiceLink and then dumped back 
out using the demo tool to create a new DemoData.xml file.
