'''
Created on August 20, 2014

@author: pfunyak
'''
# This script can be used to generate a core locations import file for VoiceLink 
# by reading the current set of locations from the data base and generating a new file.
import pyodbc

#connect to the database
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=localhost;DATABASE=VoiceLink;UID=sa;PWD=Talkman1')
# this cursor will contain the list of distinct ids of the locations to include in the file.
location_ids_cursor = cnxn.cursor()
# select the distinct location id's currently being used in the picks 
# You can change this query to select different locations and you my have to adujst the 
# following for loop to build the file correctly if you changed the query.
location_ids_cursor.execute("select distinct (l.locationid) from sel_picks p join core_locations l on p.locationId = l.locationId")
#location_ids_cursor.execute("select distinct (locationid) from core_locations")
rows = location_ids_cursor.fetchall()
if not rows:
    print "No Locations were found in the database." 
    print "Please check that the core_locations table in your database contains rows" 
    print "and verify that the query being run by this script to select item data is actually returning data"
else:
    # open a file that will contain the data that can be imported into VoiceLink.
    file = open('coreloc_demo_data_from_db.dat', 'w')
    location_cursor = cnxn.cursor()
    # build the import file 
    for row in rows:
        query = "select locationid, preaisle, aisle, postaisle, slot, checkdigits, spokenverification, scannedverification,\
            status from core_locations where locationid = " + str(row.locationid) 
        location_cursor.execute(query)
        location = location_cursor.fetchone()
        if location:
            print "location id = " + str(location.locationid)
            # The following fields are NOT required and therefore can be empty.
            # If the field is empty then pyodbc returns "NONE" as the field value and we do 
            # not want this in the data file so if the value returned is "None" then 
            # set the field to an empty string.
            preaisle = ""
            postaisle = ""
            aisle = ""
            checkdigits = ""
            if location.preaisle is not None:       preasile = str(location.preaisle)
            if location.postaisle is not None:      postaisle = str(location.postaisle)
            if location.aisle is not None:          aisle = str(location.aisle)
            if location.checkdigits is not None:    checkdigits = str(location.checkdigits)
            
            file.writelines(str(preaisle).ljust(50)             # Pre-aisle direction
                + str(aisle).ljust(50)                          # Aisle
                + str(location.slot).ljust(50)                  # Slot
                + str(location.scannedverification).ljust(50)   # Location Identifier (scanned verfification) (Location ID)
                + str(location.spokenverification).ljust(50)    # Spoken Location (Spoken Verification)
                + str(checkdigits).ljust(5)                     # Check Digits
                + str(postaisle).ljust(50)                      # Post-Aisle Direction
                + "\n") 
    file.close()