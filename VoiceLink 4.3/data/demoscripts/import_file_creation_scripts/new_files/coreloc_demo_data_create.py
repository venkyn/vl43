# This script can be used to generate locations for VoiceLink
# Number of aisle in warehouse.
# NOTE: aisle zero will NOT be created.
aisles = 100
# Number of slots per aisle
slots_per_aisle = 100
# Setting slot increment to 100 will generate 3 digit slot numbers.
slot_increment = 100

# create some general picking locations
# Scanned and Spoken verification are created by combining the Aisle and Slot values
# check digit values are the last two characters of the slot.
file = open('coreloc_picking_for_demo_data.dat', 'w')

for aisle in range(aisles):
    if slot_increment > 900:
        slot_increment = 100	
    for slot in range(slots_per_aisle):
        file.writelines(str("").ljust(50)                                # Pre-aisle direction
            + str(aisle + 1).ljust(50)                                   # Aisle
            + str(slot + slot_increment).ljust(50)                       # Slot
            + str(str(aisle + 1) + str(slot + slot_increment)).ljust(50) # Location Identifier (scanned verfification) (Location ID)
            + str(str(aisle + 1) + str(slot + slot_increment)).ljust(50) # Spoken Location (Spoken Verification)
            + str(slot + slot_increment)[-2:].ljust(5)                   # Check Digits
            + str("").ljust(50)                                          # Post-Aisle Direction
			+ "\n")   
    slot_increment += 100
file.close()


#create some loading dock door locations
# for "door" locations, we only populate the "slot"
# The scanned and spoken verification values are equal to the door number
# The check digits are equal to the door number

file = open('coreloc_dock_doors_for_demo_data.dat', 'w')
loadingDockDoors = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']

for doorNumber in loadingDockDoors:
    file.writelines(str("").ljust(50)                        # Pre-aisle direction
        + str("").ljust(50)                                  # Aisle
        + str(str("Door ") + str(doorNumber)[-2:]).ljust(50) # Slot
        + str(doorNumber).ljust(50)                          # Location Identifier (scanned verfification) (Location ID)
        + str(doorNumber).ljust(50)                          # Spoken Location (Spoken Verification)
        + str(doorNumber)[-2:].ljust(5)                      # Check Digits
        + str("").ljust(50)                                  # Post-Aisle Direction
        + "\n")
    
file.close()
