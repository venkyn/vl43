
The scripts in this folder are designed to generat import files that can be imported into VoiceLink.
In general, these scripts need modified BEFORE they are run.
They are designed in a way that when modified they can create various import files for VoiceLink.

For example, you can use the "items_demo_data_create.py" script to create a generic items import 
file with any number of items.

