'''
Created on Apr 26, 2012

@author: khazra
'''
#Routes and stops information
routes = 60
stops = 3
region_num = 1
capture_load_pos = 1
departure_date_time = 20111201120000
expected_number_of_containers = 45
route_note = 'Route created by automated script'
stop_note = 'Stop created by automated script'


#Dock door to be used aligning with core location data
dock_doors = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']


file = open('loadrs.dat', 'w')

dock_doors_count = 0;
for route in range(1, routes + 1):
    
    #Getting dock door
    current_dock_door = dock_doors[dock_doors_count]
    if dock_doors_count < len(dock_doors) - 1:
        dock_doors_count = dock_doors_count + 1
    else:
        dock_doors_count = 0
    
    for stop in range(1, stops + 1):
        file.writelines(("R" + str(route).zfill(3).ljust(49),
                         str(stop).zfill(2).ljust(9),
                         str(current_dock_door).ljust(50),
                         str(departure_date_time),
                         str(route).zfill(9),
                         str(expected_number_of_containers).ljust(9),
                         str(region_num).ljust(9),
                         str(stop).ljust(50),
                         str(capture_load_pos).ljust(1),
                         route_note.ljust(255),
                         stop_note.ljust(255) + "\n"))
        
