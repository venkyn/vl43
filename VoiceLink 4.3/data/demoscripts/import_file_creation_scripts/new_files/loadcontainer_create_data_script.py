'''
Created on Apr 26, 2012

@author: khazra
'''
#Routes and stops information
routes = 60
stops = 3
containers = 8
region_num = 1
capture_load_pos = 1
container_start_number = 481
departure_date_time = 20111201120000

file = open('loadcontainer.dat', 'w')


dock_doors_count = 0;
for route in range(1, routes + 1):
    for stop in range(1, stops + 1):
        for container in range(1,containers+1):
            file.writelines(("C" + str(container_start_number).zfill(14).ljust(49), 
                             "R" + str(route).zfill(3).ljust(49),
                             str(stop).zfill(2).ljust(9), 
                             str(departure_date_time),
                             str(region_num).ljust(9), 
                             str(1000).ljust(9), 
                             str(1000).ljust(9) + "\n"))
            
            container_start_number = container_start_number + 1
        
