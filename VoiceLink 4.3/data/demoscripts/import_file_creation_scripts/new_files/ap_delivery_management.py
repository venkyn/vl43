'''
Script to generate assignment for delivery management testing

assignment_number [18]
delivery_date [14]
delivery_location [9]
route [50]
customer_number [50]
customer_name [50]
customer_address [50]
goal_time [10]
pick_sequence_number [9]
quantity_to_pick [9]
item_number [50]
base_item_override [1]
not_used [1]
location_identifier [50]
trigger_a_replenishment [1]
region [9]
filler [1]
case_label_check_digit [5]
carton_id [50]
uom [50]
work_id [18]
target_container [2]
pick_message [250]
loading_departure_date_time [14]
loading_region [9]
'''
import random

assignment_start_number = 1
assignment_per_region = 150
picks_per_assignment = 5
number_of_route_to_use = 5
item_number_start = 701
location_number_start = 701
require_target_container = False



regions = ['109'] #['101','102','103','104','105','106','107','108','109','110','111','112','113','114','115']
routes = ['R001','R002','R003','R004','R005','R006','R007','R008','R009','R010'] #['R001','R002','R003','R004','R005','R006','R007','R008','R009','R010']

delivery_dates = {
                  'R001': 20131017203015,
                  'R002': 20131017203015,
                  'R003': 20131017203015, 
                  'R004': 20131017203015,
                  'R005': 20131017203015,
                  'R006': 20131017203015,
                  'R007': 20131008190000,
                  'R008': 20131008190000,
                  'R009': 20131008190000,
                  'R010': 20131008190000
                  }



current_route_index = 0

for region in regions:
    for assignment_number in range(assignment_start_number,assignment_start_number + assignment_per_region):
            customer_number = 'n' + routes[current_route_index]
            #customer_number = '1'
            customer_name = 'c' + routes[current_route_index]
            customer_address = 'a' + routes[current_route_index]
            goal_time = random.randint(1,10)
            route = routes[current_route_index]
            delivery_date = delivery_dates[route]
            delivery_location = route[2:]
            base_item_override = 0
            trigger_a_replenishment = ' '
            filler = ' '
            case_label_check_digit = ' '
            carton_id = ' '
            uom = ' '
            target_container = random.randint(10,99) if require_target_container else ' '
            pick_message = ' '
            loading_departure_date_time = delivery_dates[route]
            loading_region = ' '
            #loading_region = '11'
            
            for pick_sequence_number in range(1,picks_per_assignment):
                quantity_to_pick = random.randint(1,10)
                item_number = random.randint(item_number_start, item_number_start + picks_per_assignment)
                base_item = 0
                not_used = ' '
                location_identifier = random.randint(location_number_start, location_number_start + picks_per_assignment)
                
                data_line = region + str(assignment_number).zfill(4).ljust(15)
                data_line += str(delivery_date).ljust(14)
                data_line += str(delivery_location).ljust(9)
                data_line += str(route).ljust(50)
                data_line += str(customer_number).ljust(50)
                data_line += str(customer_name).ljust(50)
                data_line += str(customer_address).ljust(50)
                data_line += str(goal_time).ljust(10)
                data_line += str(pick_sequence_number).ljust(9)
                data_line += str(quantity_to_pick).ljust(9)
                data_line += str(item_number).ljust(50)
                data_line += str(base_item_override).ljust(1)
                data_line += str(not_used).ljust(1)
                data_line += str(location_identifier).ljust(50)
                data_line += str(trigger_a_replenishment).ljust(1)
                data_line += str(region).ljust(9)
                data_line += str(filler).ljust(1)
                data_line += str(case_label_check_digit).ljust(5)
                data_line += str(carton_id).ljust(50)
                data_line += str(uom).ljust(50)
                data_line += region + str(assignment_number).zfill(4).ljust(15)
                data_line += str(target_container).ljust(2)
                data_line += str(pick_message).ljust(250)
                data_line += str(loading_departure_date_time).ljust(14)
                data_line += str(loading_region).ljust(9)
                
                print(data_line)
            
            
            if current_route_index >= number_of_route_to_use :
                current_route_index = 0
            else:
                current_route_index += 1
        
    
    
    
    
    
    
    
    
    
    
    
