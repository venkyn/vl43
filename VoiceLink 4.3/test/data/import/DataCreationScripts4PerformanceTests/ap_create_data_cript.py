from datetime import date
from random import randrange

#Number of records (minus dedicated routes to be added)
records = 100000		
picks_per_assignment = 10
#assignments per region is figured out by taking records and devide it by 15 (the normal amount of regions created)
assignments_per_region = 6667
picks_counter = 1
assignments_per_region_counter = 1

#Region variables
region = 101

#Assignment variables
assignment_counter = 1
assignment_number = str(region) + str(assignment_counter).zfill(4)

#Delivery date variables
delivery_date = 20060221

#Delivery location variables
delivery_location = assignment_counter

#Route variables
route = assignment_counter

#Customer number variables
customer_number = assignment_counter

#Customer name variables
customer_name = str(" ")

#Customer address variables
customer_address = str(" ")

#Goal time variables
goal_time = assignment_counter

#Pick Sequence Number variables
pick_sequence_number = (picks_counter -1)


#Quantity to pick variables
quantity_number = randrange(1, 15)
quantity_to_pick = quantity_number

#Item Number variables
item_number = 1

#Base Item Override variables
base_item_override = 0

#Location Identifier variables
location_ID = item_number

#Trigger a Replenishment variables
trigger_a_replenishment = 0

#Case Label Check Digit variables
check_digit_number = 0
case_label_check_digit = str(" ")

#Carton ID variables
carton_ID = str(" ")

#UOM variables
uom = str(" ")

#Work ID variables
work_id = assignment_number

#Target Container variables
container_number = 00
target_container = str(" ")

#Pick Message variables
pick_message = str("Pick Message Site 1")

file = open('selectionTest.dat', 'w')

for rec in range(records):

	file.writelines((str(assignment_number).ljust(18),
		str(delivery_date).ljust(8),
		str(delivery_location).ljust(9),
		str(route).ljust(50),
		str(customer_number).ljust(50),
		str(customer_name).ljust(50),
		str(customer_address).ljust(50),
		str(goal_time).ljust(10),
		str(pick_sequence_number).ljust(9),
		str(quantity_to_pick).ljust(9),
		str(item_number).ljust(50),
		str(base_item_override).ljust(1),
		str("0").ljust(1),
		str(location_ID).ljust(50),
		str(trigger_a_replenishment).ljust(1),
		str(region).ljust(9),
		str(" ").ljust(1),
		str(case_label_check_digit).ljust(5),
		str(carton_ID).ljust(50),
		str(uom).ljust(50),
		str(work_id).ljust(18),
		str(target_container).ljust(2),
		str(pick_message).ljust(250),
		str(" ").ljust(23) + "\n"))
	
	if (assignments_per_region_counter >= assignments_per_region):
		assignment_counter = 1
		delivery_location = assignment_counter
		route = assignment_counter
		customer_number = assignment_counter
		goal_time = assignment_counter
		picks_counter = 1
		pick_sequence_number = (picks_counter -1)
		region += 1
		assignment_number = str(region) + str(assignment_counter).zfill(4)
		item_number += 1
		location_ID = item_number
		assignments_per_region_counter = 1
		work_id = assignment_number
		quantity_number = randrange(1, 15)
		quantity_to_pick = quantity_number
		
	else:
		if (picks_counter >= picks_per_assignment):
			assignment_counter += 1
			delivery_location = assignment_counter
			route = assignment_counter
			customer_number = assignment_counter
			goal_time = assignment_counter
			picks_counter = 1
			pick_sequence_number = (picks_counter -1)
			assignment_number = str(region) + str(assignment_counter).zfill(4)
			item_number += 1
			location_ID = item_number
			assignments_per_region_counter += 1
			work_id = assignment_number
			quantity_number = randrange(1, 15)
			quantity_to_pick = quantity_number
				
		else:
			picks_counter += 1
			pick_sequence_number = (picks_counter -1)
			item_number += 1
			location_ID = item_number
			assignments_per_region_counter += 1
			quantity_number = randrange(1, 15)
			quantity_to_pick = quantity_number
			
	#region 109 requires containers
	if (region == 109):
		container_number = randrange(0, 99)
		target_container = str(container_number).zfill(2)
	
	else:
		target_container = str(" ")	
		
	#region 112 requires check digits
	if (region == 112):
		check_digit_number += 1
		case_label_check_digit = str(check_digit_number)
		quantity_to_pick = str("1")
		
	else:
		case_label_check_digit = str(" ")
		
	
	
file.close()
