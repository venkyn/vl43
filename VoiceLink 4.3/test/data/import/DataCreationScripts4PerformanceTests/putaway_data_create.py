#number of records
licenses = 100000

#Item Number
license = 1

file = open('palic_10k.dat', 'w')

for item in range(licenses):

	file.writelines(str(license).ljust(50)             # Licence Plate
		+("20120824").ljust(8)                         # Date received
		+("0")                                         # Expiration Flag
		+("5").ljust(9)                                # Received QTY
		+ str(license).ljust(50)                       # Location ID
		+ str(license).ljust(50)                       # Item ID
		+("1").ljust(9)                                # Region
		+(" ")                                         # Filler
		+("100").ljust(10)                             # Goal Time
		+ "\n")

	license += 1
	print license
file.close()