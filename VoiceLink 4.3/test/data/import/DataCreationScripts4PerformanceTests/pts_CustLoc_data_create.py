#number of records
records = 100000

#Carton
customer_num = 1

file = open('ptsCustLoc_10k.dat', 'w')

for item in range(records):

	file.writelines(str(customer_num).ljust(50)          # Customer Number
		+ str(customer_num).ljust(50)                    # Location ID
		+ str(str("R") + str(customer_num)).ljust(50)    # Route
		+("D1").ljust(9)                                 # Delivery Location 
		+("Customer Name here").ljust(50)                # Customer Name
		+("Customer Address here").ljust(50)             # Customer Address
		+ "\n")

	customer_num += 1
	print customer_num
file.close()