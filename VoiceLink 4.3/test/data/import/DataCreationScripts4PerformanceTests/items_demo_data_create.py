#number of records
items = 100000

#Item Number
item_number = 1

file = open('itm_10k.dat', 'w')

for item in range(items):

	file.writelines(str(item_number).ljust(50)             # Item Number
		+ str(str("Item ") + str(item_number)).ljust(50)   # Item Description
		+ str(str("Item ") + str(item_number)).ljust(255)  # Phonetic Description
		+ str("0").ljust(10)                               # Weight
		+ str(0).ljust(10)                                 # Cube
		+ str(0).ljust(50)                                 # Size
		+ str(0).ljust(50)                                 # UPC
		+ str("0").ljust(50)                               # pack
		+ str("0")                                         # variable weight item
		+ str("0").ljust(3)                                # variable weight tolerance
		+ str("").ljust(50)                                # scanned product verification id
		+ str("").ljust(5)                                 # spoken product verification id
		+ str("0")                                         # capture serial number
		+ "\n")
		
	item_number += 1
	print item_number
file.close()