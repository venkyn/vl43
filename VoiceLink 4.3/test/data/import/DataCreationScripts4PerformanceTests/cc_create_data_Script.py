#number of records (minus dedicated routes to be added)
records = 100000

#location variables
location_ID = 1
location_ID_counter = 1

#region number variables
region_number = 101
items_per_region_counter = 1

#item number variables
item_number = 1

#uom variables
uom = "case"

#expected quantity variables
expected_quantity = 1
expected_quantity_counter = 1


file = open('cycleCountTest.dat', 'w')

for rec in range(records):
	

	file.writelines(str(location_ID).ljust(50) + str(region_number).ljust(9) + str(item_number).ljust(50) + str(uom).ljust(50) + str(expected_quantity).ljust(9) + "\n")
	location_ID += 1
	location_ID_counter += 1
	
	#Region Number
	#This is used to put a certain amount of items into a region
	if (items_per_region_counter >= 33600):
		region_number += 100
		items_per_region_counter = 1
	
	else:
		items_per_region_counter += 1
	
	#Item Number
	item_number = item_number + 1
	
	#expected quantity
	if (expected_quantity_counter >= 5):
		expected_quantity = 1
		expected_quantity_counter = 1
		
	else:
		expected_quantity += 1
		expected_quantity_counter += 1
		
		
	
	
	
	
file.close()	
	

