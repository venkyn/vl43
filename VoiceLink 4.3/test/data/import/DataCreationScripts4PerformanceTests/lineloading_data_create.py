#number of records
records = 100000

#Carton
carton_id = 1

file = open('ctn_10k.dat', 'w')

for item in range(records):

	file.writelines(str(carton_id).ljust(50)             # Carton ID
		+("01").ljust(10)                                # SPUR 
		+ str(str("R") + str(carton_id)).ljust(50)       # Route
		+("DD").ljust(50)                                # Stop
		+("001").ljust(50)                               # Wave
		+("1")                                           # Carton Type
		+("101").ljust(9)                                  # Region
		+(" ")                                           # Filler
		+ "\n")

	carton_id += 1
	print carton_id
file.close()