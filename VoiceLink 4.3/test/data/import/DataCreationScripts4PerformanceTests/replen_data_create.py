#number of records
records = 100000

#Replen Number
replenhisment_number = 1

file = open('replen_10k.dat', 'w')

for item in range(records):

	file.writelines(str(replenhisment_number).ljust(50)             # Replenishment Number
		+ str(replenhisment_number).ljust(50)                       # License Plate
		+("1")                                                      # Status Flag
		+("0")                                                      # Type Flag
		+("100").ljust(10)                                          # Goal Time
		+("0")                                                      # Specific License ID Flag
		+ str(replenhisment_number).ljust(50)                       # Scanned Validation 
		+("").ljust(5)                                              # Spoken Validation
		+("1").ljust(9)                                             # Region
		+(" ")                                                      # Filler
		+ str(replenhisment_number).ljust(50)                       # Item Number
		+("5").ljust(9)                                             # QTY
		+ str(replenhisment_number).ljust(50)                       # Reserve Location ID
		+ str(replenhisment_number).ljust(50)                       # Destination Location ID
		+ "\n")

	replenhisment_number += 1
	print replenhisment_number
file.close()