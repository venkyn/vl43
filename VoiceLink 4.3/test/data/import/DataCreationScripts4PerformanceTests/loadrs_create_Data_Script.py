#number of records (minus dedicated routes to be added)
records = 100000

create_dedicated = False
dedicated_route_num = 99999999
dedicated_per_route = 1

number_of_stops = 10
departure_date_time = 20111201120000
number_of_trailers = 2
expected_number_of_containers = 45 
number_of_regions = 1
route_note = "Route Note "
stop_note = "Stop Note "

#stop variables
stop_count = 0

#dockdoor variables
dd_num = 1
dd_count = 1
num_of_dd = 10

#trailer variables
trailer_num = 123456789
trailer_count = 0
num_of_trailers = records/number_of_trailers

#route variables
region_num = 1
region_count = 0
#num_of_regions = records/number_of_regions

#customer variables
customer_count = 0

#capture load position
capture_load_pos = 0

#route/stop_note variables
route_note_count = 0
stop_note_count = 0

print_dedicated = False


#route variables
route_num = 1
route_count = 0
num_of_routes = num_of_dd

file = open('loadrs.dat', 'w')


for rec in range(records):
    
    if (create_dedicated and print_dedicated):
        for i in range(dedicated_per_route):
            file.writelines(("R" + str(dedicated_route_num).zfill(3).ljust(49), str(stop_count).zfill(2).ljust(9),
                         str(dd_count).ljust(50), str(departure_date_time) + str(dedicated_route_num).zfill(9), 
                         str(expected_number_of_containers).ljust(9), str(region_num).ljust(9), str(dedicated_route_num).ljust(50), 
                         str(capture_load_pos).ljust(1), route_note + str(route_note_count).ljust(244), stop_note + str(stop_note_count).ljust(245) + "\n"))
            dedicated_route_num = dedicated_route_num - 1
        print_dedicated = False
    
    else:
        #figure out what route to print
        if route_count == num_of_routes:
            route_num = route_num + 1
            route_count = 1
            customer_count = 1     
            print_dedicated = True
        else:
            route_count = route_count + 1
            customer_count = customer_count + 1 
  
        
        #figure out what stop to print
        if stop_count == number_of_stops:
            stop_count = 1
                    #figure out what dd to print
            if dd_count == num_of_dd:
                dd_num = dd_num + 1
                dd_count = 1
            else:
                dd_count = dd_count + 1
        else:
            stop_count = stop_count + 1
        

        
        #figure out what trailers to print
        if trailer_count == num_of_trailers:
            trailer_num = trailer_num + 1
            trailer_count = 1
        else:
            trailer_count = trailer_count + 1
    
        #figure out what regions to print
        #if region_count == num_of_regions:
            #region_num = region_num + 1
            #region_count = 1
       # else:
           # region_count = region_count + 1
    
        

        capture_load_pos = 1
        
    
        route_note_count = route_note_count + 1
        stop_note_count = stop_note_count + 1
        
      
        file.writelines(("R" + str(route_num).zfill(3).ljust(49), str(stop_count).zfill(2).ljust(9),
                 str(dd_count).ljust(50), str(departure_date_time) + str(route_num).zfill(9), 
                 str(expected_number_of_containers).ljust(9), str(region_num).ljust(9), str(customer_count).ljust(50), 
                 str(capture_load_pos).ljust(1), route_note + str(route_note_count).ljust(244), stop_note + str(stop_note_count).ljust(245) + "\n"))





file.close()
    
