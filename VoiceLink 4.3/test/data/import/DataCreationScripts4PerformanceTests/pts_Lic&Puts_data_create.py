#number of records
records = 100000

#Carton
license_num = 1

file = open('ptslic_10k.dat', 'w')

for item in range(records):

	file.writelines(str(license_num).ljust(50)          # License Number
		+("1").ljust(9)                                 # Delivery Location 
		+ str(license_num).ljust(50)                    # Customer Number
		+ str(license_num).ljust(50)                    # Location ID
		+ str(license_num).ljust(50)                    # Item
		+("1").ljust(9)                                 # QTY
		+(" ").ljust(50)                                # UOM
		+ str(license_num).ljust(9)                     # Sequence
		+("1")                                          # Allow overpack
		+("1").ljust(9)                                 # Residual
		+ "\n")

	license_num += 1
	print license_num
file.close()