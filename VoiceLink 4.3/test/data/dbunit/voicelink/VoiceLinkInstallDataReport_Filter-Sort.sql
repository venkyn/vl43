-- This is a report for Filtering / Sorting / Operands / Type
-- It has been verified to work on SQLServer or Oracle 10g
SELECT v.name as View_Name,
c.Column_Order,
c.Field as Field_Name,
(CASE Operand_Type
	WHEN 0 THEN 'String'
	WHEN 1 THEN 'Number'
	WHEN 2 THEN 'Date'
	WHEN 3 THEN 'Enum'
	WHEN 4 THEN 'Boolean'
	ELSE 'not defined'
END) Operand_Type,
(CASE FilterType
	WHEN 0 THEN 'No Filter'
	WHEN 2 THEN 'Database'
	WHEN 3 THEN 'In Memory'
	ELSE 'not defined'
END) Filter_Type,
(CASE SortType
	WHEN 0 THEN 'NoSort - This column does not support sorting'
	WHEN 1 THEN 'Normal - In Database with normal order'
	WHEN 2 THEN 'SystemDataLocalized - Sort in database by joining with SystemTranslation'
	WHEN 3 THEN 'SystemEnumLocalized - Sort in database by joining with SystemTranslation'
	WHEN 4 THEN 'UserDataLocalized - Sort in database by joining with DataTranslation'
	WHEN 5 THEN 'CodeOnly - Only sortable in Java code'
	WHEN 6 THEN 'NormalLeftJoin - Sort in database with left join at property level 1'
	WHEN 7 THEN 'NormalLeftJoinLevel2 - Sort in database with left join at property level 2'
	WHEN 8 THEN 'CodeOnlyLocalized - Only sortable in Java code but needs to be localized.'
	ELSE 'not defined'
END) Sort_Type,
c.width,
c.id AS Column_ID,
v.id View_ID
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
Order by v.name, c.column_order;
