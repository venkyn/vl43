-- This script is for oracle to double the size of
-- assignments and picks each time it is ran.

--Insert assignment records, setting the resevered by field 
--to the original assignment records assignment ID to be used later
INSERT INTO sel_assignments(assignmentid, 
                                                        version, 
							sequenceNumber, 
							originalSequenceNumber, 
							assignmentNumber, 
							requestedOrderNo, 
							groupPosition, 
							groupCount, 
							route, 
							deliveryDate, 
							deliveryLocation, 
							customerNumber, 
							customerName, 
							customerAddress, 
							regionId, 
							workIdentifierValue, 
							partialWorkIdentifier, 
							totalItemQuantity, 
							totalWeight, 
							totalCube, 
							goalTime, 
							calculatedBaseItems, 
							assignmentType, 
							status, 
							exportStatus, 
							containerCount, 
							pickDetailCount, 
							createdDate, 
							reservedBy)
SELECT	voc_assignment_sequence.nextval,
                0, 
		voc_assignment_sequence.currval, 
		voc_assignment_sequence.currval, 
		voc_assignment_sequence.currval, 
                1, 
		1, 
		1, 
		route, 
		deliveryDate, 
		deliveryLocation, 
		customerNumber, 
		customerName, 
		customerAddress, 
		regionId, 
                workIdentifierValue, 
		partialWorkIdentifier, 
		totalItemQuantity, 
		totalWeight, 
		totalCube, 
		goalTime, 
		calculatedBaseItems, 
                assignmentType, 
		3, 
		exportStatus, 
		0, 
		0, 
		createdDate, 
		null
FROM sel_assignments;

--Copy pick records from original assignments to new assignments
INSERT INTO sel_picks(pickId,           
                                        version, 
					assignmentId, 
					locationId, 
					itemId, 
					sequenceNumber, 
					caseLabelCheckDigit, 
					cartonNumber, 
					triggerReplenish, 
					unitOfMeasure, 
					quantityToPick, 
					baseItemOverride, 
					targetContainerIndicator, 
					promptMessage, 
					quantityPicked, 
					quantityAdjusted, 
					isBaseItem, 
					pickTime, 
					pickType, 
					status, 
					pickDetailCount, 
					originalPickId, 
					createdDate)
SELECT voc_pick_sequence.nextval,
                version, 
                assignmentId + (voc_assignment_sequence.currval / 2), 
                locationId, 
		itemId, 
		sequenceNumber, 
		caseLabelCheckDigit, 
		cartonNumber, 
		triggerReplenish, 
                unitOfMeasure, 
		quantityToPick, 
		baseItemOverride, 
		targetContainerIndicator, 
                promptMessage, 
		0, 
		0, 
		isBaseItem, 
                pickTime, 
		pickType, 
		2, 
		0, 
		null, 
		createdDate
FROM sel_picks;


--Insert tag records for new records based on regions tag
INSERT INTO sel_assignment_tag (tagged_id, tag_id)
SELECT a.assignmentId, t.tag_id
FROM sel_assignments a 
    INNER JOIN sel_regions r ON r.regionId=a.regionId
    INNER JOIN core_region_tag t ON r.regionId=t.tagged_id
WHERE a.assignmentid not in (select tagged_id from sel_assignment_tag);

-- insert tag records for picks based on assignments
INSERT INTO sel_pick_tag (tagged_id, tag_id)
SELECT p.pickId, t.tag_id
FROM sel_picks p 
    INNER JOIN sel_assignments a ON p.assignmentId=a.assignmentID
    INNER JOIN sel_assignment_tag t ON a.assignmentId=t.tagged_id
WHERE p.pickid not in (select tagged_id from sel_pick_tag);

