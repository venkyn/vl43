DECLARE 
	dnow DATE;
	dmax DATE;
	dmax1 DATE;
	dmax2 DATE;
	dmax3 DATE;
	nDiff Number(10,3);
	nDiff1 Number(10,3);
	nDiff2 Number(10,3);
	nDiff3 Number(10,3);

BEGIN

--
--This will force the import and export to regenerate
--the site directories upon service restart.
--
UPDATE 
    VOC_SYSTEM_PROPERTIES
    SET VALUE = '1' 
WHERE 
    SYSTEMPROPERTYID = '-1005';
--
--Start of adjust dates
--

dnow := SysDate;

SELECT MAX(createdDate) INTO dmax
FROM arch_pts_container_details;
nDiff := dnow - dmax;

UPDATE arch_pts_container_details
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM arch_pts_containers;
nDiff := dnow - dmax;

UPDATE arch_pts_containers
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM arch_pts_license_labor;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE arch_pts_license_labor
	SET createdDate = createdDate + nDiff;
UPDATE arch_pts_license_labor
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE arch_pts_license_labor
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM arch_pts_licenses;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE arch_pts_licenses
	SET createdDate = createdDate + nDiff;
UPDATE arch_pts_licenses
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE arch_pts_licenses
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(putTime) INTO dmax, dmax1
FROM arch_pts_put_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE arch_pts_put_details
	SET createdDate = createdDate + nDiff;
UPDATE arch_pts_put_details
	SET putTime = putTime + nDiff1
	WHERE putTime IS NOT NULL;


SELECT MAX(createdDate), MAX(putTime) INTO dmax, dmax1
FROM arch_pts_puts;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE arch_pts_puts
	SET createdDate = createdDate + nDiff;
UPDATE arch_pts_puts
	SET putTime = putTime + nDiff1
	WHERE putTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM arch_sel_assignment_labor;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE arch_sel_assignment_labor
	SET createdDate = createdDate + nDiff;
UPDATE arch_sel_assignment_labor
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE arch_sel_assignment_labor
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(deliveryDate) INTO dmax, dmax1
FROM arch_sel_assignments;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE arch_sel_assignments
	SET createdDate = createdDate + nDiff;
UPDATE arch_sel_assignments
	SET deliveryDate = deliveryDate + nDiff1
	WHERE deliveryDate IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM arch_sel_containers;
nDiff := dnow - dmax;

UPDATE arch_sel_containers
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(pickTime) INTO dmax, dmax1
FROM arch_sel_pick_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE arch_sel_pick_details
	SET createdDate = createdDate + nDiff;
UPDATE arch_sel_pick_details
	SET pickTime = pickTime + nDiff1
	WHERE pickTime IS NOT NULL;


SELECT MAX(createdDate), MAX(pickTime), MAX(shortedDate) INTO dmax, dmax1, dmax2
FROM arch_sel_picks;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE arch_sel_picks
	SET createdDate = createdDate + nDiff;
UPDATE arch_sel_picks
	SET pickTime = pickTime + nDiff1
	WHERE pickTime IS NOT NULL;
UPDATE arch_sel_picks
	SET shortedDate = shortedDate + nDiff2
	WHERE shortedDate IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM core_break_types;
nDiff := dnow - dmax;

UPDATE core_break_types
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_items;
nDiff := dnow - dmax;

UPDATE core_items
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_location_items;
nDiff := dnow - dmax;

UPDATE core_location_items
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_locations;
nDiff := dnow - dmax;

UPDATE core_locations
	SET createdDate = createdDate + nDiff;


SELECT  MAX(expirationDate) INTO dmax
FROM core_lot_import_data;
nDiff := dnow - dmax;

UPDATE core_lot_import_data
	SET expirationDate = expirationDate + nDiff;


SELECT MAX(createdDate) INTO dmax 
FROM core_lots;
nDiff := dnow - dmax;

UPDATE core_lots
	SET createdDate = createdDate + nDiff;
UPDATE core_lots
	SET expirationDate =dnow +7;



SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM core_operator_labor;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE core_operator_labor
	SET createdDate = createdDate + nDiff;
UPDATE core_operator_labor
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE core_operator_labor
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(signOnTime), MAX(signOffTime) INTO dmax, dmax1, dmax2
FROM core_operators;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE core_operators
	SET createdDate = createdDate + nDiff;
UPDATE core_operators
	SET signOnTime = signOnTime + nDiff1
	WHERE signOnTime IS NOT NULL;
UPDATE core_operators
	SET signOffTime = signOffTime + nDiff2
	WHERE signOffTime IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM core_printers;
nDiff := dnow - dmax;

UPDATE core_printers
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_reason_codes;
nDiff := dnow - dmax;

UPDATE core_reason_codes
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_regions;
nDiff := dnow - dmax;

UPDATE core_regions
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_work_group_functions;
nDiff := dnow - dmax;

UPDATE core_work_group_functions
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM core_work_groups;
nDiff := dnow - dmax;

UPDATE core_work_groups
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(actionTime) INTO dmax, dmax1
FROM lineload_carton_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE lineload_carton_details
	SET createdDate = createdDate + nDiff;
UPDATE lineload_carton_details
	SET actionTime = actionTime + nDiff1
	WHERE actionTime IS NOT NULL;


SELECT MAX(createdDate), MAX(loadTime) INTO dmax, dmax1
FROM lineload_cartons;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE lineload_cartons
	SET createdDate = createdDate + nDiff;
UPDATE lineload_cartons
	SET loadTime = loadTime + nDiff1
	WHERE loadTime IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM lineload_pallets;
nDiff := dnow - dmax;

UPDATE lineload_pallets
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(dateOpened), MAX(dateClosed) INTO dmax, dmax1, dmax2
FROM lineload_route_stops;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE lineload_route_stops
	SET createdDate = createdDate + nDiff;
UPDATE lineload_route_stops
	SET dateOpened = dateOpened + nDiff1
	WHERE dateOpened IS NOT NULL;
UPDATE lineload_route_stops
	SET dateClosed = dateClosed + nDiff2
	WHERE dateClosed IS NOT NULL;


SELECT MAX(createdDate), MAX(putTime) INTO dmax, dmax1
FROM pts_container_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE pts_container_details
	SET createdDate = createdDate + nDiff;
UPDATE pts_container_details
	SET putTime = putTime + nDiff1
	WHERE putTime IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM pts_containers;
nDiff := dnow - dmax;

UPDATE pts_containers
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM pts_license_labor;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE pts_license_labor
	SET createdDate = createdDate + nDiff;
UPDATE pts_license_labor
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE pts_license_labor
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime),  MAX(endTime) INTO dmax, dmax1, dmax2
FROM pts_licenses;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE pts_licenses
	SET createdDate = createdDate + nDiff;
UPDATE pts_licenses
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE pts_licenses
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(putTime) INTO dmax, dmax1
FROM pts_put_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE pts_put_details
	SET createdDate = createdDate + nDiff;
UPDATE pts_put_details
	SET putTime = putTime + nDiff1
	WHERE putTime IS NOT NULL;


SELECT MAX(createdDate), MAX(putTime) INTO dmax, dmax1
FROM pts_puts;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE pts_puts
	SET createdDate = createdDate + nDiff;
UPDATE pts_puts
	SET putTime = putTime + nDiff1
	WHERE putTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(putTime) INTO dmax, dmax1, dmax2
FROM putaway_license_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE putaway_license_details
	SET createdDate = createdDate + nDiff;
UPDATE putaway_license_details
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE putaway_license_details
	SET putTime = putTime + nDiff2
	WHERE putTime IS NOT NULL;

SELECT MAX(dateReceived) INTO dmax
FROM putaway_license_import_data;
nDiff := dnow - dmax;

UPDATE putaway_license_import_data
	SET dateReceived = dateReceived + nDiff
	WHERE dateReceived IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(putTime), MAX(dateReceived) INTO dmax, dmax1, dmax2, dmax3
FROM putaway_licenses;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;
nDiff3 := dnow - dmax3;

UPDATE putaway_licenses
	SET createdDate = createdDate + nDiff;
UPDATE putaway_licenses
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE putaway_licenses
	SET putTime = putTime + nDiff2
	WHERE putTime IS NOT NULL;
UPDATE putaway_licenses
	SET dateReceived = dateReceived + nDiff3
	WHERE dateReceived IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(replenishTime) INTO dmax, dmax1, dmax2
FROM replen_replenishment_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE replen_replenishment_details
	SET createdDate = createdDate + nDiff;
UPDATE replen_replenishment_details
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE replen_replenishment_details
	SET replenishTime = replenishTime + nDiff2
	WHERE replenishTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime), MAX(dateReceived) INTO dmax, dmax1, dmax2, dmax3
FROM replen_replenishments;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;
nDiff3 := dnow - dmax3;

UPDATE replen_replenishments
	SET createdDate = createdDate + nDiff;
UPDATE replen_replenishments
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE replen_replenishments
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;
UPDATE replen_replenishments
	SET dateReceived = dateReceived + nDiff3
	WHERE dateReceived IS NOT NULL;


SELECT MAX(deliveryDate) INTO dmax
FROM sel_assignment_import_data;
nDiff := dnow - dmax;

UPDATE sel_assignment_import_data
	SET deliveryDate = deliveryDate + nDiff
	WHERE deliveryDate IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(endTime) INTO dmax, dmax1, dmax2
FROM sel_assignment_labor;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE sel_assignment_labor
	SET createdDate = createdDate + nDiff;
UPDATE sel_assignment_labor
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE sel_assignment_labor
	SET endTime = endTime + nDiff2
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate), MAX(startTime), MAX(deliveryDate), MAX(endTime) INTO dmax, dmax1, dmax2, dmax3
FROM sel_assignments;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;
nDiff3 := dnow - dmax3;

UPDATE sel_assignments
	SET createdDate = createdDate + nDiff;
UPDATE sel_assignments
	SET startTime = startTime + nDiff1
	WHERE startTime IS NOT NULL;
UPDATE sel_assignments
	SET deliveryDate = deliveryDate + nDiff2
	WHERE deliveryDate IS NOT NULL;
UPDATE sel_assignments
	SET endTime = endTime + nDiff3
	WHERE endTime IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM sel_containers;
nDiff := dnow - dmax;

UPDATE sel_containers
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate), MAX(pickTime) INTO dmax, dmax1
FROM sel_pick_details;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE sel_pick_details
	SET createdDate = createdDate + nDiff;
UPDATE sel_pick_details
	SET picktime = pickTime + nDiff1
	WHERE pickTime IS NOT NULL;


SELECT MAX(createdDate), MAX(pickTime), MAX(shortedDate) INTO dmax, dmax1, dmax2
FROM sel_picks;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;
nDiff2 := dnow - dmax2;

UPDATE sel_picks
	SET createdDate = createdDate + nDiff;
UPDATE sel_picks
	SET picktime = pickTime + nDiff1
	WHERE pickTime IS NOT NULL;
UPDATE sel_picks
	SET shortedDate = shortedDate + nDiff1
	WHERE shortedDate IS NOT NULL;


SELECT MAX(createdDate) INTO dmax
FROM sel_sum_prompt_def;
nDiff := dnow - dmax;

UPDATE sel_sum_prompt_def
	SET createdDate = createdDate + nDiff;


SELECT MAX(createdDate) INTO dmax
FROM sel_sum_prompt_loc;
nDiff := dnow - dmax;

UPDATE sel_sum_prompt_loc
	SET createdDate = createdDate + nDiff;


SELECT MAX(creationDateTime) INTO dmax
FROM voc_notification;
nDiff := dnow - dmax;

UPDATE voc_notification
	SET creationDateTime = creationDateTime + nDiff;


SELECT MAX(start_time), MAX(finish_time) INTO dmax, dmax1
FROM voc_job_history;
nDiff := dnow - dmax;
nDiff1 := dnow - dmax1;

UPDATE voc_job_history
	SET start_time = start_time + nDiff
	WHERE start_time IS NOT NULL;
UPDATE voc_job_history
	SET finish_time = finish_time + nDiff1
	WHERE finish_time IS NOT NULL;


SELECT MAX(last_run) INTO dmax
FROM voc_jobs;
nDiff := dnow - dmax;

UPDATE voc_jobs
	SET last_run = last_run + nDiff
	WHERE last_run IS NOT NULL;


SELECT MAX(last_login) INTO dmax
FROM voc_user;
nDiff := dnow - dmax;

UPDATE voc_user
	SET last_login = last_login + nDiff
	WHERE last_login IS NOT NULL;

END;
