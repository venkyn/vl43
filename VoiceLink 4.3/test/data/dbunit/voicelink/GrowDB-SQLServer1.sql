--Script is to prepare an oracle database to increase the size of picks
--update sel_picks set promptMessage = null
DELETE FROM sel_assignment_containers
DELETE FROM sel_container_tag
DELETE FROM sel_pick_details
DELETE FROM sel_containers
DELETE FROM sel_assignment_labor
UPDATE sel_assignments SET createdDate = getDate(), workidentifiervalue = '1234', partialWorkidentifier = '1234';
UPDATE sel_picks SET promptMessage = null

