--Script is to prepare an oracle database to increase the size of picks
--update sel_picks set promptMessage = null
create sequence voc_assignment_sequence;
create sequence voc_pick_sequence START WITH 1000000;

alter table sel_picks drop constraint FK_Assignment_Picks;


BEGIN
DELETE FROM sel_assignment_containers;
DELETE FROM sel_container_tag;
DELETE FROM sel_pick_details;
DELETE FROM sel_containers;
DELETE FROM sel_assignment_labor;

DELETE FROM sel_assignment_tag;

UPDATE sel_assignments SET reservedBy = assignmentId;
UPDATE sel_assignments SET assignmentId = voc_assignment_sequence.nextval;
UPDATE sel_picks 
SET assignmentId = (SELECT a.assignmentId FROM sel_assignments a WHERE a.reservedBy = sel_picks.assignmentId); 
UPDATE sel_assignments SET reservedBy = NULL;
UPDATE sel_assignments SET createdDate = sysdate, workidentifiervalue = '1234', partialWorkidentifier = '1234';

END;
