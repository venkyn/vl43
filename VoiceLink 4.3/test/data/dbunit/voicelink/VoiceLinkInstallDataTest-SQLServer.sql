-- This test was designed to run in SQL Server 2000 or greater.
-- The ant target Test-Install-Data will execute this test.


-- This tests that each view contains an ID column.
SELECT DISTINCT
v.name as WARNING__View_With_No_ID_Field,
c.view_id
FROM voc_Column c
LEFT OUTER JOIN (Select * from Voc_Column
	WHERE field = 'id') as h ON h.view_id = c.view_id
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE h.view_id is null;


-- This tests that each ID column is not displayable.
-- a displayable ID, can be removed.
SELECT DISTINCT
v.name as ERROR_ID_Field_that_is_displayable,
c.view_id
FROM voc_Column c
LEFT OUTER JOIN (Select * from Voc_Column
	WHERE field = 'id') as h ON h.view_id = c.view_id
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE h.displayable = 1;


--This tests that each view only has one column marked (initially) sorted
SELECT
MAX(v.name) AS ERROR__View_has_more_than_1_initial_sort_column,
COUNT(c.view_id) AS Num_fields_marked_sorted,
MAX(c.view_id)
FROM voc_column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.sorted=1
GROUP BY c.view_id
HAVING COUNT(c.view_id) > 1;


-- This test sees if a view has duplicated column_orders.
SELECT
MAX(v.name) AS ERROR__View_with_multiple_columns_with_same_column_order,
c.view_id,
c.column_order,
COUNT(column_order) AS Num_columns_with_same_column_order
FROM voc_column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
GROUP BY c.view_id, c.column_order
HAVING COUNT(column_order) > 1;


-- This tests if a feature is not mapped to any roles.
-- A feature (found in voc_feature) is a unit that can be assigned
-- to a role in the security model.  It is usually a screen, or an action.
-- A feature may not be mapped to any roles if its security is controlled
-- by another feature on the same page... For instance, in selection, picks
-- appear on the assignments page.  The assignment feature is mapped to a role,
-- but the picks may not be.
SELECT
f.name as Warn_Feature_is_not_mapped_to_any_roles
FROM voc_feature f
Left Outer Join voc_role_to_feature rtf ON f.id = rtf.feature_id
WHERE rtf.feature_id is null;


-- This ensures that features marked as read only are only assigned to Read-Only
-- role(-3 ).
SELECT
f.name as ERROR_ReadOnly_Feature_Mapped_to_Non_ReadOnly_Role,
f.id 
FROM voc_feature f
Left Outer Join voc_role_to_feature rtf ON f.id = rtf.feature_id
WHERE f.read_only = 1 AND rtf.role_id != -3;


-- This ensures that features marked as NOT read only are NOT assigned to
-- two the Read-Only role (-3).
SELECT f.id, f.name as ERROR_ReadWrite_Feature_Mapped_to_ReadOnly_Role
FROM voc_feature f
Left Outer Join voc_role_to_feature rtf ON f.id = rtf.feature_id
WHERE f.read_only = 0 AND(rtf.role_id = -3);


-- This warns if a Voc_Column field is displayable and not sortable.
SELECT DISTINCT
c.field as WARNING__Voc_Column_Field_Displayble_but_not_Sortable,
v.name as view_name,
v.id as view_id,
c.id as column_id
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.sortType = 0 AND c.displayable = 1;


-- This warns if a Voc_Column field is sorted in memory.
SELECT DISTINCT
c.field as WARNING__Voc_Column_Field_Sorted_In_Memory,
v.name as view_name, 
c.id as column_id,
v.id as view_id
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.sortType in (5,8);


-- This warns if a Voc_Column field is filtered in memory.
SELECT DISTINCT
c.field as WARNING__Voc_Column_Field_Filtered_In_Memory,
v.name as view_name, 
c.id as column_id,
v.id as view_id
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.filterType = 2;


-- This warns if a displayable Voc_Column field is not able to be filtered.
SELECT DISTINCT
c.field as WARNING__voc_column_field_displayable_but_not_filterable,
v.name as view_name,
c.id as column_id,
v.id as view_id
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.filterType = 0 AND c.displayable=1;


-- This warns if a Voc_Column field has a displayfunction, it should be sorted/filtered in memory.
-- A voc_column field with a DisplayFunction, indicates that it is probably some sort of calculated
-- field.  Calculated fields require memory sort/filtering.
SELECT
c.field as WARNING__Voc_Column_Field_has_display_function_but_not_Sorted_or_Filtered_In_Memory,
v.name as view_name, 
c.id as column_id,
v.id as view_id
FROM voc_Column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE c.display_function is not null
AND c.display_function <> ''
AND (c.sortType not in (5,8,0) OR c.filterType not in (0,2));


-- If there is a filter_enum_type, then the operand should be 3 (enum)
SELECT
c.field AS ERROR__Field_with_Filter_Enum_Type_whose_Operand_Type_is_not_3_enum,
v.name AS View_Name,
v.id AS View_ID,
c.id AS Field_ID
FROM voc_column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE (filter_enum_type is not null AND filter_enum_type <> '')
AND Operand_Type <> 3;


-- If there is an optionKeyPrefix, then the operand should be 4 (boolean)
--
-- The Option_Key_Prefix is used for displaying and for filtering boolean values.
-- The value shown for a boolean filed is defined using the Option_Key_Prefix and
-- a matching key in a .properties file.
-- When we have a boolean field being filtered, there will be a dropdown of operands
-- (like equal to, not equal to) and then an option dropdown.  These lists can say any
-- two words (eg. true/false or yes/no) depending on the situatuion.  An example pair of keys
--- is item.isCaptureSerialNumbers.true=Yes and item.isCaptureSerialNumbers.false=No
-- The Option_Key_Prefix for these is "item.isCaptureSerialNumbers"
SELECT
c.field AS ERROR__Field_with_Option_Key_Prefix_whose_Operand_Type_is_not_4_Boolean,
v.name AS View_Name,
v.id AS View_ID,
c.id AS Field_ID
FROM voc_column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE (option_key_prefix is not null AND option_key_prefix <> '')
AND Operand_Type <> 4;


-- If operand type is 4 (Boolean), warn that may need Option_Key_Prefix
--
-- The Option_Key_Prefix is used for displaying and for filtering boolean values.
-- The value shown for a boolean filed is defined using the Option_Key_Prefix and
-- a matching key in a .properties file.
-- When we have a boolean field being filtered, there will be a dropdown of operands
-- (like equal to, not equal to) and then an option dropdown.  These lists can say any
-- two words (eg. true/false or yes/no) depending on the situatuion.  An example pair of keys
--- is item.isCaptureSerialNumbers.true=Yes and item.isCaptureSerialNumbers.false=No
-- The Option_Key_Prefix for these is "item.isCaptureSerialNumbers"
SELECT
c.field AS WARNING__Boolean_field_may_need_Option_Key_Prefix,
v.name AS View_Name,
v.id AS View_ID,
c.id AS Field_ID
FROM voc_column c
LEFT OUTER JOIN voc_view v ON v.id = c.view_id
WHERE  Operand_Type = 4
AND (option_key_prefix is null OR option_key_prefix <> '');


-- This tests the System Translation table to ensure that some EPP data was installed.
SELECT     (CASE WHEN (COUNT(*) = 0) 
THEN 'No Data Found - EPP SystemTranlations Data Missing' ELSE 'Data Found - No Problem' END) EPP_System_Translation_Check 
FROM         voc_system_translations
WHERE     (keyValue LIKE '%.epp.%');


-- This tests the System Translation table to ensure that some VoiceLink data was installed.
SELECT     (CASE WHEN (COUNT(*) = 0) 
THEN 'No Data Found - VL SystemTranlations Data Missing' ELSE 'Data Found - No Problem' END) VL_System_Translation_Check 
FROM         voc_system_translations
WHERE     (keyValue LIKE '%.voicelink.%');
