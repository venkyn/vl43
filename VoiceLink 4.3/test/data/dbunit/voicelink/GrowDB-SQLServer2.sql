--Insert assignment records, setting the resevered by field 
--to the original assignment records assignment ID to be used later
INSERT INTO sel_assignments(version, 
			sequenceNumber, 
			originalSequenceNumber, 
			assignmentNumber, 
			requestedOrderNo, 
			groupPosition, 
			groupCount, 
			route, 
			deliveryDate, 
			deliveryLocation, 
			customerNumber, 
			customerName, 
			customerAddress, 
			regionId, 
			workIdentifierValue, 
			partialWorkIdentifier, 
			totalItemQuantity, 
			totalWeight, 
			totalCube, 
			goalTime, 
			calculatedBaseItems, 
			assignmentType, 
			assignmentPriority,
			status, 
			exportStatus, 
			containerCount, 
			pickDetailCount, 
			createdDate, 
			reservedBy)
SELECT	0, 
	sequenceNumber, 
	originalSequenceNumber, 
    assignmentNumber, 
    1, 
	1, 
	1, 
	route, 
	deliveryDate, 
	deliveryLocation, 
	customerNumber, 
	customerName, 
	customerAddress, 
	regionId, 
    workIdentifierValue, 
	partialWorkIdentifier, 
	totalItemQuantity, 
	totalWeight, 
	totalCube, 
	goalTime, 
	calculatedBaseItems, 
    assignmentType, 
	assignmentPriority,
	3, 
	exportStatus, 
	0, 
	0, 
	createdDate, 
	assignmentId
FROM sel_assignments

--Update the assignment number with the original assignment ID
-- plus 1000000000000. This should create a unique assignment number
UPDATE sel_assignments 
SET assignmentNumber = assignmentId + 1000000000000 
WHERE reservedBy IS NOT NULL

--Set the PromptMessage to the new assignmentID of the new records
-- before coping the pick records
UPDATE sel_picks 
SET PromptMessage = a.assignmentId
FROM sel_assignments a 
WHERE a.reservedBy = sel_picks.assignmentId

--Copy pick records from original assignments to new assignments
INSERT INTO sel_picks(version, 
		assignmentId, 
		locationId, 
		itemId, 
		sequenceNumber, 
		caseLabelCheckDigit, 
		cartonNumber, 
		triggerReplenish, 
		unitOfMeasure, 
		quantityToPick, 
		baseItemOverride, 
		targetContainerIndicator, 
		promptMessage, 
		quantityPicked, 
		quantityAdjusted, 
		isBaseItem, 
		pickTime, 
		pickType, 
		status, 
		pickDetailCount, 
		originalPickId, 
		createdDate)
SELECT version, 
        PromptMessage, 
        locationId, 
	itemId, 
	sequenceNumber, 
	caseLabelCheckDigit, 
	cartonNumber, 
	triggerReplenish, 
        unitOfMeasure, 
	quantityToPick, 
	baseItemOverride, 
	targetContainerIndicator, 
        promptMessage, 
	0, 
	0, 
	isBaseItem, 
        pickTime, 
	pickType, 
	2, 
	0, 
	null, 
	createdDate
FROM sel_picks

--Clean up temporary wroking fields
UPDATE sel_assignments SET reservedBy = NULL where reservedBy is not null
UPDATE sel_picks SET PromptMessage = NULL where PromptMessage is not null


--Insert tag records for new records based on regions tag
INSERT INTO sel_assignment_tag (tagged_id, tag_id)
SELECT a.assignmentId, t.tag_id
FROM sel_assignments a 
    INNER JOIN sel_regions r ON r.regionId=a.regionId
    INNER JOIN core_region_tag t ON r.regionId=t.tagged_id
WHERE a.assignmentid not in (select tagged_id from sel_assignment_tag)

-- insert tag records for picks based on assignments
INSERT INTO sel_pick_tag (tagged_id, tag_id)
SELECT p.pickId, t.tag_id
FROM sel_picks p 
    INNER JOIN sel_assignments a ON p.assignmentId=a.assignmentID
    INNER JOIN sel_assignment_tag t ON a.assignmentId=t.tagged_id
WHERE p.pickid not in (select tagged_id from sel_pick_tag)

