Basic operation of the oracle grow scripts:


STEP BY STEP
1.  Import base data including Assignments & Picks to grow (script currently supports up to 1 million picks)
2.  Run "GrowDB-Oracle1.sql" and commit results.
3.  Run "GrowDB-Oracle2.sql" and commit results to double size of data.
4.  To double size again Go to step 3, to finish go to step 5.
5.  When done growing, run "GrowDB-Oracle3.sql" and commit results.


Note that to support more than 1M picks, modify "GrowDB-Oracle1.sql" and change "START WITH" amount for voc_pick_sequence.

