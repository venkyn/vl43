--clean up script for after growing database (assignment and picks) to desired size.

drop sequence voc_assignment_sequence;
drop sequence voc_pick_sequence;
drop sequence voc_primary_key_sequence;
create sequence voc_primary_key_sequence START WITH 100000000;

alter table sel_picks add constraint FK_Assignment_Picks foreign key (assignmentId) references sel_assignments on delete cascade;
