/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.aggregators;

import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.dataaggregator.impl.GenericDataAggregatorImpl;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.exceptions.BusinessRuleException;

import java.math.BigDecimal;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

/**
 * 
 * 
 * @author khazra
 */
@Component("testDataAggregator")
public class TestDataAggregator extends GenericDataAggregatorImpl {

    private String identityColumnName = "column0";

    private String[] columns = {
        "column0", "column1", "column2", "column3", "column4", "column5",
        "column7", "column8" };

    private DAFieldType[] columnType = {
        DAFieldType.INTEGER, DAFieldType.INTEGER, DAFieldType.INTEGER,
        DAFieldType.INTEGER, DAFieldType.STRING, DAFieldType.TIME,
        DAFieldType.DATE, DAFieldType.FLOAT };

    @Override
    public String getIdentityColumnName() {
        return identityColumnName;
    }

    @Override
    public JSONArray getAllOutputColumns() {

        JSONArray columnsArray = new JSONArray();

        try {

            int i = 0;
            for (String column : columns) {
                JSONObject columnJSON = new JSONObject();
                columnJSON.put(GenericDataAggregator.FIELD_ID, column);
                columnJSON.put(GenericDataAggregator.DISPLAY_NAME, column);
                columnJSON.put(GenericDataAggregator.FIELD_TYPE,
                    columnType[i++]);
                columnJSON.put(GenericDataAggregator.UOM, column);
                columnJSON.put(GenericDataAggregator.COLUMN_SEQUENCE, i);
                columnsArray.put(columnJSON);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return columnsArray;
    }

    @Override
    public JSONArray execute() throws BusinessRuleException {
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            for (int i = 0; i < 3; i++) {
                jsonObject.put(columns[i], i);
            }
            jsonObject.put(columns[4], "Test column 5");
            jsonObject.put(columns[5], 700);
            jsonObject.put(columns[6], new Date(137785275000L));

            BigDecimal bd = new BigDecimal((float) 22.24);
            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            Float floatValue = bd.floatValue();

            jsonObject.put(columns[7], floatValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        jsonArray.put(jsonObject);
        return jsonArray;
    }

}
