/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.lineloading.service.impl;


import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.lineloading.service.CartonManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the Line Loading Carton mananger.
 * 
 */
@Test(groups = { FAST, LINELOADING })

public class CartonManagerImplTest extends  BaseServiceManagerTestCase {

    private CartonManager cartonManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setCartonManager(CartonManager impl) {
        this.cartonManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        
        adapter.resetInstallationData();
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
           + "VoiceLinkDataSmall.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testAverageGoalRate() throws Exception {
        Object[] queryArgs = {new Date()};
        
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(queryArgs);
        List<DataObject> summary = cartonManager.listSummaryByWave(rdi);
        assertEquals(summary.size(), 2, "2 summary records");
        
        summary = cartonManager.listSummaryByWaveAndRegion(rdi);
        assertEquals(summary.size(), 4, "2 summary records");
        
    }
}
