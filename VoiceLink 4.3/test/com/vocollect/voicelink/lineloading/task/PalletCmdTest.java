/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.PalletStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for Pallet Command test.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, LINELOADING })
public class PalletCmdTest extends TaskCommandTestCase {

    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    private PalletCmd cmd;
    private String operId = "PalletOper";
    private String sn = "PalletSerial";

    /**
     * @return a bean for testing.
     */
    private PalletCmd getCmdBean() {
        return (PalletCmd) getBean("cmdPrTaskLUTLineLoadingPallet");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test for Carton Getter and Setter.
     */
    public void testGetSetCartonNumber() {
        this.cmd.setCartonNumber("13");
        assertEquals(this.cmd.getCartonNumber(), "13", "Carton Getter/Setter");
    }

    /**
     * Test for Pallet Getter and Setter.
     */
    public void testGetSetPalletNumber() {
        this.cmd.setPalletNumber("11");
        assertEquals(this.cmd.getPalletNumber(), "11", "Pallet Getter/Setter");
    }

    /**
     * Test for Set Aside Getter and Setter. 
     */
    public void testGetSetNewPallet() {
        this.cmd.setNewPallet(true);
        assertEquals(this.cmd.isNewPallet(), true, "Set Aside Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test carton not found error
        Response response = executeCommand(this.getCmdDateSec(), "01327", "001W14003", "0");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_NOT_FOUND.getErrorCode(), "errorCode");

        //test route stop closed
        response = executeCommand(this.getCmdDateSec(), "01327082", "001W14003", "0");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_ROUTE_STOP_CLOSED.getErrorCode(), "errorCode");


        //Test pallet stop not match 
        response = executeCommand(this.getCmdDateSec(), "01327325", "001W1400x", "1");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.PALLET_STOP_NOT_MATCH.getErrorCode(), "errorCode");

        //Test pallet route not match
        response = executeCommand(this.getCmdDateSec(), "01327325", "001W1x003", "1");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.PALLET_ROUTE_NOT_MATCH.getErrorCode(), "errorCode");

        //Test pallet not found
        response = executeCommand(this.getCmdDateSec(), "01327325", "001W14003", "0");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.PALLET_NOT_FOUND.getErrorCode(), "errorCode");


        //Test successful command
        Thread.sleep(1200);
        response = executeCommand(this.getCmdDateSec(), "01327325", "001W14003", "1");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0L, "errorCode");

        //Test pallet closed
        Pallet pallet = cmd.getPalletManager().findPalletByNumber("001W14003");
        pallet.setStatus(PalletStatus.Closed);
        cmd.getPalletManager().save(pallet);

        response = executeCommand(this.getCmdDateSec(), "01327325", "001W14003", "0");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.PALLET_NOT_OPEN.getErrorCode(), "errorCode");
       
        
        //All Cartons Loaded
        response = executeCommand(this.getCmdDateSec(), "01326914", "001W09001", "1");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 99L, "errorCode");

        //Verify Labor Record
        List<OperatorLabor> olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(
                this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        
        assertEquals(olList.get(1).getActionType(),
                     OperatorLaborActionType.LineLoading, "Invalid Action Type: ");
        assertEquals(olList.get(1).getOperator().getId(),
                     olList.get(0).getOperator().getId(), "Invalid Operator Id: ");
        assertNotNull(olList.get(1).getStartTime().getTime(), "Start Time is Null: ");
        assertNull(olList.get(1).getEndTime(), "End Time is Not Null: ");
        assertTrue(olList.get(1).getDuration() > new Long(0), "Invalid Duration: ");
        assertNotNull(olList.get(1).getCreatedDate(), "Created Date is Null: ");
        assertEquals(olList.get(1).getCount(), new Integer(2), "Invalid Count: ");
        assertTrue(olList.get(1).getActualRate() > new Double(0), "Invalid Actual Rate: ");
        assertTrue(olList.get(1).getPercentOfGoal() > new Double(0), "Invalid Percent of Goal: ");
        assertEquals(olList.get(1).getRegion().getId(), new Long(-41), "Invalid Region Id: ");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteBreak() throws Exception {

        initialize();
        //Test successful command
        Thread.sleep(1000);
        executeCommand(this.getCmdDateSec(), "01327325", "001W14003", "1");
        
        takeABreak("1", "0", "0");
        Thread.sleep(1000);
        List<OperatorLabor> olList = this.cmd.getLaborManager()
        .getOperatorLaborManager().listAllRecordsByOperatorId(
            this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorBreakLabor blRec = (OperatorBreakLabor) olList.get(2);
        assertEquals(olList.size(), 3, "Record Count ");
        assertEquals(olList.get(2).getActionType(), OperatorLaborActionType.Break, "Action Type ");
        assertEquals(olList.get(2).getOperator().getId(), olList.get(1).getOperator().getId(), "Operator Id ");
        assertNotNull(olList.get(2).getStartTime().getTime(), "Start Time ");
        assertNull(olList.get(2).getEndTime(), "End Time");
        assertNotNull(olList.get(2).getCreatedDate().getTime(), "Created Date ");
        assertEquals(blRec.getPreviousOperatorLabor().getId(), olList.get(1).getId(), "Previous Operator Labor Id ");
        assertEquals(blRec.getBreakType().getId(), new Long(-1), "Break Id ");
        
        takeABreak("1", "1", "0");
        olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        assertEquals(olList.size(), 4, "Number of Records ");
    
        assertTrue(olList.get(2).getDuration() > new Long(0), "Duration ");
        assertNotNull(olList.get(2).getEndTime().getTime(), "End Time");
        assertEquals(olList.get(3).getActionType(), OperatorLaborActionType.LineLoading, "Action Type ");
        assertEquals(olList.get(3).getOperator().getId(), olList.get(1).getOperator().getId(), "Operator Id ");
        assertNotNull(olList.get(3).getStartTime().getTime(), "Start Time ");
        assertNull(olList.get(3).getEndTime(), "End Time ");
        assertEquals(olList.get(3).getRegion().getId(), new Long(-41), "Region Id ");
    }
    
    
    /**
     * 
     * @param breakType -  type of break, battery change, lunch, etc
     * @param startEndFlag - 0 to start break.  1 to end break
     * @param description - not used
     * @throws Exception
     */
    private void takeABreak(String breakType, String startEndFlag, String description)
                                                              throws Exception {

        executeLutCmd("cmdPrTaskODRCoreSendBreakInfo", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            breakType, startEndFlag, description});
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLineLoadingRegions();
        classSetupLineLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTLineLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "41"});
        executeLutCmd("cmdPrTaskLUTLineLoadingRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTLineLoadingCarton", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "01326914", "0", "01"});
        executeLutCmd("cmdPrTaskLUTLineLoadingCarton", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "01327325", "0", "01"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param carton - carton
     * @param pallet - should carton be set aside
     * @param newPallet - spur
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, 
                                    String carton, 
                                    String pallet, 
                                    String newPallet) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                pallet, carton, newPallet}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
