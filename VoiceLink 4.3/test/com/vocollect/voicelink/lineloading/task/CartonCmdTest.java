/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.voicelink.lineloading.model.Carton;
import com.vocollect.voicelink.lineloading.model.CartonStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for LineLoadingCartonCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, LINELOADING })

public class CartonCmdTest extends TaskCommandTestCase {

    private CartonCmd cmd;
    private String operId = "CartonOper";
    private String sn = "CartonSerial";

    /**
     * @return a bean for testing.
     */
    private CartonCmd getCmdBean() {
        return (CartonCmd) getBean("cmdPrTaskLUTLineLoadingCarton");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test for Spur Getter and Setter.
     */
    public void testGetSetSpur() {
        this.cmd.setSpur("12");
        assertEquals(this.cmd.getSpur(), "12", "Spur Getter/Setter");
    }

    /**
     * Test for Carton Getter and Setter.
     */
    public void testGetSetCartonNumber() {
        this.cmd.setCartonNumber("13");
        assertEquals(this.cmd.getCartonNumber(), "13", "Carton Getter/Setter");
    }

    /**
     * Test for Set Aside Getter and Setter. 
     */
    public void testGetSetSetAside() {
        this.cmd.setSetAside(true);
        assertEquals(this.cmd.getSetAside(), true, "Set Aside Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();

        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "01327325", "0", "01");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0L, "errorCode");
        assertEquals(record.get("route"), "W14", "route");
        assertEquals(record.get("stop"), "003", "stop");

        Carton carton = cmd.getCartonManager().get(-497L);
        assertEquals(carton.getStatus(), CartonStatus.InProgress, "CartonStatus");

        //Test carton not found error
        response = executeCommand(this.getCmdDateSec(), "01327", "0", "01");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_NOT_FOUND.getErrorCode(), "errorCode");

        //test route stop closed
        response = executeCommand(this.getCmdDateSec(), "01327082", "0", "01");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_ROUTE_STOP_CLOSED.getErrorCode(), "errorCode");

        //test Carton already loaded
        response = executeCommand(this.getCmdDateSec(), "01327198", "0", "01");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 2L, "errorCode");
        
        //test Carton already loaded wrong spur
        response = executeCommand(this.getCmdDateSec(), "01327198", "0", "02");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_LOADED_ON_WRONG_SPUR.getErrorCode(), "errorCode");
        
        //Test Available wrong spur response
        response = executeCommand(this.getCmdDateSec(), "01327059", "0", "02");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CARTON_AVAILABLE_ON_WRONG_SPUR.getErrorCode(), "errorCode");
        carton = cmd.getCartonManager().get(-488L);
        assertEquals(carton.getStatus(), CartonStatus.SetAside, "CartonStatus");
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLineLoadingRegions();
        classSetupLineLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTLineLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "41"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param carton - carton
     * @param setAside - should carton be set aside
     * @param spur - spur
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, 
                                    String carton, 
                                    String setAside, 
                                    String spur) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                carton, setAside, spur}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
