/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.voicelink.lineloading.model.Pallet;
import com.vocollect.voicelink.lineloading.model.PalletStatus;
import com.vocollect.voicelink.lineloading.model.RouteStop;
import com.vocollect.voicelink.lineloading.model.RouteStopStatus;
import com.vocollect.voicelink.task.command.BaseTaskCommand;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PutawayRegionConfigurationCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, LINELOADING })
public class CloseStopCmdTest extends TaskCommandTestCase {

    private CloseStopCmd cmd;
    private String operId = "CloseStopOper";
    private String sn = "CloseStopSerial";

    /**
     * @return a bean for testing.
     */
    private CloseStopCmd getCmdBean() {
        return (CloseStopCmd) getBean("cmdPrTaskLUTLineLoadingCloseStop");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test for Carton Getter and Setter.
     */
    public void testGetSetPrinterNumber() {
        this.cmd.setPrinterNumber("1");
        assertEquals(this.cmd.getPrinterNumber(), "1", "Printer Getter/Setter");
        assertEquals(this.cmd.getPrinterNumberInt(), new Integer(1), 
                     "Printer Integer Getter/Setter");
        
        this.cmd.setPrinterNumber("");
        assertNull(this.cmd.getPrinterNumberInt(), "Printer Integer Getter/Setter");
    }

    /**
     * Test for Pallet Getter and Setter.
     */
    public void testGetSetPalletNumber() {
        this.cmd.setPalletNumber("11");
        assertEquals(this.cmd.getPalletNumber(), "11", "Pallet Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();

        //Test pallet found
        Response response = executeCommand(this.getCmdDateSec(), "001W09001", "1");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);

        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.PALLET_NOT_FOUND.getErrorCode(), "errorCode");
        //Test not all loaded
        loadCarton("01326952", "001W09001");
        
        response = executeCommand(this.getCmdDateSec(), "001W09001", "1");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);

        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.ROUTE_STOP_NOT_LOADED.getErrorCode(), "errorCode");

        //Test route stop closed
        loadCarton("01326914", "001W09001");
        response = executeCommand(this.getCmdDateSec(), "001W09001", "");

        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0L, "errorCode");
        RouteStop routeStop = cmd.getRouteStopManager().get(-53L);
        assertEquals(RouteStopStatus.Closed,
            routeStop.getStatus(), "Route Stop Not Closed");
        
        Pallet pallet = cmd.getPalletManager().findPalletByNumber("001W09001");
        assertEquals(pallet.getStatus(), PalletStatus.Closed, "Pallet not Closed");
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLineLoadingRegions();
        classSetupLineLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTLineLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "41"});
    }

    /**
     * Load carton command.
     * 
     * @param carton - carton
     * @param pallet - pallet
     * @throws Exception - any exception
     */
    private void loadCarton(String carton, String pallet) throws Exception {
        executePallet("cmdPrTaskLUTLineLoadingPallet", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            pallet, carton, "1"});

    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param pallet - should carton be set aside
     * @param printer - printer number
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, 
                                    String pallet, 
                                    String printer) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                pallet, printer}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    /**
     * Execute a generic command object.
     * 
     * @param cmdString - Command to execute
     * @param args - arguments for commands
     * @return - response from command
     * @throws Exception - exception
     */
    private Response executePallet(String cmdString, String[] args)
        throws Exception {

        BaseTaskCommand newCmd = (BaseTaskCommand) getBean(cmdString);

        getCommandDispatcher().mapArgumentsToCommand(args, newCmd);
        Response response = getTaskCommandService().executeCommand(newCmd);
        ResponseRecord record = response.getRecords().iterator().next();
        if (!(record.getErrorCode().equals("0") 
            || record.getErrorCode().equals("99"))) {
            assertEquals(record.getErrorCode(), record.getErrorMessage(), "0");
        }

        return response;

    }

}
