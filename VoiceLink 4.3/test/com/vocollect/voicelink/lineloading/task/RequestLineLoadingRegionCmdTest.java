/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for RequestLineLoadingRegionCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, LINELOADING })
public class RequestLineLoadingRegionCmdTest extends TaskCommandTestCase {

    private RequestRegionCmd cmd;
    private String operId = "RequestLineLoadingRegionOper";
    private String sn = "RequestLineLoadingRegionSerial";

    /**
     * @return a bean for testing.
     */
    private RequestRegionCmd getCmdBean() {
        return (RequestRegionCmd) 
            getBean("cmdPrTaskLUTLineLoadingRequestRegion");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.RequestPutawayRegionCmd#getRegionNumber}.
     */
    public void testGetSetRegionNumber() {
        this.cmd.setRegionNumber("");
        assertEquals(this.cmd.getRegionNumber(), "", 
                     "Region Number Getter/Setter Null");
        assertNull(this.cmd.getRegionNumberInt(),
                   "Region Number Getter/Setter Null");

        this.cmd.setRegionNumber("1");
        assertEquals(this.cmd.getRegionNumber(), "1", 
                     "Region Number Getter/Setter Not Null");
        assertEquals((Integer) 1, this.cmd.getRegionNumberInt(),
                     "Region Number Getter/Setter Not Null");
    }
  
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecute() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "41");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteValidations() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "99");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode(), "errorCode");

        //Test standard response
        response = executeCommand(this.getCmdDateSec(), "42");
        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION.getErrorCode(), "errorCode");
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLineLoadingRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param regionNumber - region Number
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String regionNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                regionNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
