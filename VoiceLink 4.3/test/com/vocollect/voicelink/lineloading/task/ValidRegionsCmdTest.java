/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LINELOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ValidLineLoadingRegionsCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, LINELOADING })
public class ValidRegionsCmdTest extends TaskCommandTestCase {

    private ValidRegionsCmd cmd;
    private String operId = "LineValidRegionsOper";
    private String sn = "LineValidRegionsSerial";

    /**
     * @return a bean for testing.
     */
    private ValidRegionsCmd getCmdBean() {
        return (ValidRegionsCmd) getBean("cmdPrTaskLUTLineLoadingValidRegions");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNoRegions() throws Exception {

        initializeCore(false);
        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), 
                Long.toString(TaskErrorCode.NOT_AUTHORIZED_ANY_REGIONS.getErrorCode()),
                "errorCode");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteRegions() throws Exception {

        initializeCore(true);
        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(Long.parseLong(record.getErrorCode()), 0L, "errorCode");
        assertEquals(record.get("regionNumber").toString(), "41", "RegionNumber");
    }
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param includeRegions - should region data be included
     * @throws Exception - Exception
     */
    private void initializeCore(boolean includeRegions) throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        if (includeRegions) {
            classSetupLineLoadingRegions();
        }

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
