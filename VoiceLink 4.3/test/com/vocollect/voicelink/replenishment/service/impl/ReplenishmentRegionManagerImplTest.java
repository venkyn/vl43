/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.replenishment.ReplenishmentErrorCode;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the ReplenishmentManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, REPLENISHMENT })
public class ReplenishmentRegionManagerImplTest extends  BaseServiceManagerTestCase {

    private ReplenishmentRegionManager replenishmentRegionManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setReplenishmentRegionManager(ReplenishmentRegionManager impl) {
        this.replenishmentRegionManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        // populate db with base data.
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_ForkAppWorkGroups.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
           + "VoiceLinkDataUnitTest_ReplenRegions.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
           + "VoiceLinkDataUnitTest_ReplenOperators.xml", DatabaseOperation.INSERT);
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testAverageGoalRage() throws Exception {
        
      Double rate = this.replenishmentRegionManager.avgGoalRate();
      assertEquals(rate, new Double(175.00), "Average goal rate is incorrect");
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
      
        Long theValue =  this.replenishmentRegionManager.uniquenessByNumber(new Integer(31));
        assertEquals(theValue, new Long(-31), "Should have found region 31");
        
        theValue = this.replenishmentRegionManager.uniquenessByNumber(15);
        assertNull(theValue, "Value should be null");
    }
    
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        
        Long theValue =  this.replenishmentRegionManager.uniquenessByName("Replenishment Region 33");
        assertEquals(theValue, new Long(-33), "Should have found region 33");
        
        theValue = this.replenishmentRegionManager.uniquenessByName("Does Not Exist");
        assertNull(theValue, "Value should be null");
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testexecuteValidateEditRegion() throws Exception {
        
        // verify that we throw an exception when attempting to edit a region
        // that has an operator signed on.
        ReplenishmentRegion region = this.replenishmentRegionManager.findRegionByNumber(31);
        try {
            this.replenishmentRegionManager.executeValidateEditRegion(region);
            fail("Did not catch business Rule Exception");
        } catch (BusinessRuleException bre) {
              assertEquals(bre.getErrorCode(), ReplenishmentErrorCode.REGION_OPERATORS_SIGNED_IN);
        }
        
        // verify that we do not throw an exception when attempting to edit a
        // region that does not have an operator signed on.
        region = this.replenishmentRegionManager.findRegionByNumber(34);
        try {
            this.replenishmentRegionManager.executeValidateEditRegion(region);
        } catch (BusinessRuleException bre) {
             fail("Should not have caught execption");           
        }
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testexecuteValidateDeleteRegion() throws Exception {
        
        // verify that we throw an exception when attempting to delete a region
        // that has an operator signed on.
        ReplenishmentRegion region = this.replenishmentRegionManager.findRegionByNumber(31);
        try {
            this.replenishmentRegionManager.executeValidateDeleteRegion(region);
            fail("Did not catch business Rule Exception");
        } catch (BusinessRuleException bre) {
              assertEquals(bre.getErrorCode(), ReplenishmentErrorCode.REGION_OPERATORS_SIGNED_IN);
        }
        // it would be good to verify that we can actually delete a region but
        // if we delete a region, the other tests would break because they run in 
        // a random order.
    }

    
    
    
    
    
    
    
    
}
