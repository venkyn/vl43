/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.replenishment.ReplenishmentErrorCode;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentAssignmentSummary;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the ReplenishmentManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, REPLENISHMENT })

public class ReplenishmentManagerImplTest extends  BaseServiceManagerTestCase {

    private ReplenishmentManager replenishmentManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setReplenishmentManager(ReplenishmentManager impl) {
        this.replenishmentManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
      
        adapter.resetInstallationData();
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataSmall.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test
    public void testAssignmentSummaryCounts() throws Exception {
        Object[] queryArgs = {new Date(0)};
        
        List<DataObject> theList = new ArrayList<DataObject>();
        
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(queryArgs);
        
        rdi.setSortColumn("region.name");
        theList = this.replenishmentManager.listAssignmentCountsByRegionAndStatus(rdi);
        assertEquals(theList.size(), 3, "List size should be 3");
        ReplenishmentAssignmentSummary theObject = (ReplenishmentAssignmentSummary) theList.get(0);
        Integer intExpectedValue = new Integer(25);
        Integer intZero = new Integer(0);
        assertNotNull(theObject.getSite(), "Site should not be null");
        assertNotNull(theObject.getRegion(), "Region should not be null");
        assertEquals(theObject.getAvailableAssignments(), intExpectedValue,
            "Value should be " + intExpectedValue.toString());
        assertEquals(theObject.getTotalAssignments(), intExpectedValue, 
            "Total should be "  + intExpectedValue.toString());
        assertEquals(theObject.getNonCompletedAssignments(), intExpectedValue, 
            "Not Complete should be "  + intExpectedValue.toString());
        assertEquals(theObject.getInProgressAssignments(), intZero, "In progress should be zero");
        assertEquals(theObject.getCompletedAssignments(), intZero, "Completed should be zero");
    }
    
    /**
     * @throws  Exception - any
    * 
     */
    @Test
    public void testExecuteUpdateStatus() throws Exception {
        
        ReplenishmentManager rm = this.replenishmentManager;
        
        // verify that we can only change assignments whose status is 
        // available and unavailable
        Replenishment r =  rm.findReplenishmentByNumber("21365");
        // change status to unavailable and verify change
        rm.executeUpdateStatus(r, ReplenishStatus.Unavailable);
        r = rm.findReplenishmentByNumber("21365");
        assertEquals(r.getStatus(), ReplenishStatus.Unavailable, "Status should be unavailable");
        // change back to available.  should succeed
        rm.executeUpdateStatus(r, ReplenishStatus.Available);
        r = rm.findReplenishmentByNumber("21365");
        assertEquals(r.getStatus(), ReplenishStatus.Available, "Status should be available");
        // set status to complete
        r.setComplete();
        rm.save(r);
        // try to change the status of a completed assignment
        try {
            rm.executeUpdateStatus(r, ReplenishStatus.Available);
            fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
        } catch (BusinessRuleException bre) {
            if (bre.getErrorCode() != ReplenishmentErrorCode.REPLENISHMENT_CANNOT_CHANGE_STATUS) {
                fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
            }
        }

        r = rm.findReplenishmentByNumber("21364");
        r.setInProgress();
        r = rm.findReplenishmentByNumber("21364");
        assertEquals(r.getStatus(), ReplenishStatus.InProgress, "Status should be In progress");
        // try to change the status of an in progress assignment
        try {
            rm.executeUpdateStatus(r, ReplenishStatus.Available);
            fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
        } catch (BusinessRuleException bre) {
            if (bre.getErrorCode() != ReplenishmentErrorCode.REPLENISHMENT_CANNOT_CHANGE_STATUS) {
                fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
            }
        }
        
        r = rm.findReplenishmentByNumber("21363");
        r.setComplete();
        r = rm.findReplenishmentByNumber("21363");
        assertEquals(r.getStatus(), ReplenishStatus.Complete, "Status should be Complete");
        // try to change the status of an in progress assignment
        try {
            rm.executeUpdateStatus(r, ReplenishStatus.Available);
            fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
        } catch (BusinessRuleException bre) {
            if (bre.getErrorCode() != ReplenishmentErrorCode.REPLENISHMENT_CANNOT_CHANGE_STATUS) {
                fail("did not catch exception REPLENISHMENT_CANNOT_CHANGE_STATUS");
            }
        }
    }
}
