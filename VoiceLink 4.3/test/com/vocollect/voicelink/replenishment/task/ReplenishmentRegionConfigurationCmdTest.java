/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.Test;


/**
 * 
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, REPLENISHMENT })

public class ReplenishmentRegionConfigurationCmdTest extends TaskCommandTestCase {
    
    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    private ReplenishmentRegionConfigurationCmd cmd;
    private String operId = "ReplenishmentRegionConfigurationOper";
    private String sn = "ReplenishmentRegionConfigurationSerial";

    /**
     * @return a bean for testing.
     */
    private ReplenishmentRegionConfigurationCmd getCmdBean() {
        return (ReplenishmentRegionConfigurationCmd) 
            getBean("cmdPrTaskLUTForkReplenishmentRegionConfiguration");
    }

       
    /**
     * Test method for {@link com.vocollect.voicelink.replenishment.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteChangeRegion() throws Exception {

        initialize();
        validReplenishmentRegions();
        requestRegion("32", "0");
        
        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        Response response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("regionNumber").toString(), "32", "regionNumber");
        assertEquals(record.get("regionName").toString(), "Replenishment Region 32", "regionName");
        
        // change regions
        validReplenishmentRegions();
        requestRegion("31", "0");
        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get("regionNumber").toString(), "31", "regionNumber");
        assertEquals(record.get("regionName").toString(), "Replenishment Region 31", "regionName");
        
        // change regions
        validReplenishmentRegions();
        requestRegion("32", "0");
        requestRegion("31", "0");
        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 2, "recordSize");
    }
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        validReplenishmentRegions();
        requestRegion("32", "0");

        //Test that we successfully signed on to a region and the proper configuration is returned 
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("regionNumber").toString(), "32", "regionNumber");
        assertEquals(record.get("regionName").toString(), "Replenishment Region 32", "regionName");
        assertEquals(record.get("allowCancelLicense"), true, "allowCancelLicense");
        assertEquals(record.get("allowOverrideLocation"), false, "allowOverrideLocation");
        assertEquals(record.get("allowOverridePickUpQty"), false, "allowOverridePickUpQty");
        assertEquals(record.get("allowPartialPut"), false, "allowPartialPut");
        assertEquals(record.get("capturePickUpQty"), true, "capturePickUpQty");
        assertEquals(record.get("capturePutQty"), false, "capturePutQty");
        assertEquals(record.get("licDigitsTaskSpeaks").toString(), "99", "licDigitsTaskSpeaks");
        assertEquals(record.get("locDigitsOperSpeak").toString(), "0", "locDigitsOperSpeak");
        assertEquals(record.get("checkDigitsOperSpeak").toString(), "2", "checkDigitsOperSpeak");
        assertEquals(record.get("exceptionLocation").toString(), "Exception Location", "exceptionLocation");
        assertEquals(record.get("verifySpokenLicenseLocation"), false, "verifySpokenLicenseLocation");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
        
        
        //  Verify that we got 2 labor records. The first is a sign on record. 
        //  The second is a replenish. 
        List<OperatorLabor> olList =
            this.cmd.getLaborManager().getOperatorLaborManager()
                .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        
        assertEquals(olList.size(), 2, "Invalid Record Count ");
        assertEquals(olList.get(1).getActionType(), OperatorLaborActionType.Replenishment, "Invalid Action Type ");
        assertEquals(olList.get(1).getOperator().getId(), olList.get(0).getOperator().getId(), "Invalid Operator ID ");
        assertNotNull(olList.get(1).getStartTime(), "Start Time is Null ");
        assertNull(olList.get(1).getEndTime(), "End Time is Not Null ");
        assertEquals(olList.get(1).getDuration(), new Long(0), "Invalid Duration ");
        assertEquals(olList.get(1).getExportStatus(), ExportStatus.NotExported, "Invalid Export Status ");
        assertNotNull(olList.get(1).getCreatedDate(), "Created Date is Null ");
        assertEquals(olList.get(1).getCount(), new Integer(0), "Count is Not Zero ");
        assertEquals(olList.get(1).getActualRate(), new Double(0), "Actual Rate is Not Zero ");
        assertEquals(olList.get(1).getPercentOfGoal(), new Double(0), "Percent of Goal is Not Zero ");
        assertNotNull(olList.get(1).getRegion(), "Region ID is Null ");
        assertNull(olList.get(1).getBreakType(), "Break ID is Not Null ");
        
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNoRegions() throws Exception {

        initializeNoRegions();
        //Test - No regions are signed into but a request for 
        //region configuration has been made
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(1, response.getRecords().size(), "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD), 
                Long.toString(TaskErrorCode.REGIONS_NO_LONGER_VALID.getErrorCode()), 
                "errorCode");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteAllRegions() throws Exception {

        initializeAllRegions();
        //Test that after an operator signs on to all available regions the appropriate
        //configuration response is returned
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(response.getRecords().size(), 3, "recordSize");
        assertEquals(response.getRecords().get(0).get("errorCode"), "0", "errorCode");
        assertEquals(response.getRecords().get(0).get("errorMessage"), "", "errorMessage");
    }
    

    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});

    }
    

    /**
     * @throws Exception
     */
    private void validReplenishmentRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * @param regionNumber - region number
     * @param allRegions - all regions string
     * @throws Exception
     */
    private void requestRegion(String regionNumber, String allRegions) throws Exception {
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber, allRegions});
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeAllRegions() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "", "1"});
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeNoRegions() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
