/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * 
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, REPLENISHMENT })
public class ValidReplenishmentRegionsCmdTest extends TaskCommandTestCase {

    private ValidReplenishmentRegionsCmd cmd;
    private String operId = "ValidReplenishmentRegionsOper";
    private String sn = "ValidReplenishmentRegionsSerial";

    /**
     * @return a bean for testing.
     */
    private ValidReplenishmentRegionsCmd getCmdBean() {
        return (ValidReplenishmentRegionsCmd) 
            getBean("cmdPrTaskLUTForkValidReplenishmentRegions");
    }
    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
    }
    
    /**
     * wrapper method to ensure tests all run back to back.
     * @throws Exception
     */
    @Test()
    public void testExecuteRegionAuthorization() throws Exception {
        testExecuteNoAuthorizedRegions();
        testExecuteGetAuthorizedRegions();
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    private void testExecuteNoAuthorizedRegions() throws Exception {

        // Sign operator on the system.
        initialize();
        // Attempt to get replenishment regions for an operator who is not authorized for ANY Regions.
        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD),
                Long.toString(TaskErrorCode.NOT_AUTHORIZED_ANY_REGIONS.getErrorCode()), 
                "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains(operId),
                   "error message should contain Operator Id");
    }
    
    /**
     * 
     * @throws Exception any
     */
    // So we don't have to execute classSetup and classSetupCore for each test,
    // I've made this test dependent upon testExecuteNoAuthorizedRegions.  When 
    // testExecuteNoAuthorizedRegions runs there should not be any Replenishment regions
    // in the database.  
    // testExecuteGetAuthorizedRegions needs replenishment regions and loads the regions 
    // into the database.
    private void testExecuteGetAuthorizedRegions() throws Exception {

        // Load replenishment regions
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();
        // Sign Operator onto the system
        initialize();

        //Test for all authorized regions 
        Response response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 3, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
        
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
