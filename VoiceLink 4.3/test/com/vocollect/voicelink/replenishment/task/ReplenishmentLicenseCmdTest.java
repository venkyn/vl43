/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.replenishment.model.ReplenishDetailStatus;
import com.vocollect.voicelink.replenishment.model.ReplenishStatus;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * 
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, REPLENISHMENT })
public class ReplenishmentLicenseCmdTest extends TaskCommandTestCase {

    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    private ReplenishmentLicenseCmd cmd;
    private String operId = "ReplenishmentLicenseOper";
    private String sn = "ReplenishmentLicenseSerial";
    private Date startDate;

    /**
     * @return a bean for testing.
     */
    private ReplenishmentLicenseCmd getCmdBean() {
        return (ReplenishmentLicenseCmd) 
            getBean("cmdPrTaskLUTForkReplenishmentLicense");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
        this.startDate = this.getCmdDateSec();
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getLicenseNumber}.
     */
    @Test()
    public void testGetSetReplenishmentNumber() {
        this.cmd.setReplenishmentNumber("1");
        assertEquals(this.cmd.getReplenishmentNumber(), "1", "Replenishment Number Getter/Setter");
    }
    
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteReplenNoExist() throws Exception {

        initialize();
        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior.
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        // The requested replenishment does not exist
        Response response = executeCommand(endDate, "9999", "30", "-916", "1", "", this.startDate);

        assertEquals(1, response.getRecords().size(), "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.REPLEN_NOT_FOUND.getErrorCode(), 
                "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains("9999"),
                   "error message should contain Replenishment Number 9999");
    }
    
    
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteCanceled() throws Exception {

        initialize();
        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior.
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        // Cancel a replenishment
        String replenNumber = "21340";
        Response response =   executeCommand(endDate, replenNumber, "0", "", "2", "2", this.startDate);
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        // Verify replenishment assignment was updated correctly.
        Replenishment replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber(replenNumber);
        assertEquals(ReplenishStatus.Canceled, replen.getStatus(), "Replenishment Canceled");
        assertEquals(makeStringFromDate(replen.getStartTime()),
                     makeStringFromDate(this.startDate), "Replenishment startTime");
        assertEquals(makeStringFromDate(replen.getEndTime()), 
                     makeStringFromDate(endDate), "Replenishment endTime");
        assertEquals(replen.getTotalReplenishedQuantity(), 0, "Replenished quantity");
        assertEquals(replen.getDetailCount(), 1, "Detail Count");
        assertEquals(replen.getOperator().getOperatorIdentifier(), operId, "Operator");
 
        // verify replenishment detail record
        List<ReplenishmentDetail> details = this.cmd.getReplenishmentDetailManager().getAll();
        assertEquals(1, details.size(), "DetailCount");
        ReplenishmentDetail theDetails = details.get(0);
        assertEquals(ReplenishDetailStatus.Canceled, theDetails.getStatus(), "Detail Status");
        assertEquals(replen.getToLocation().getId(), 
                     theDetails.getReplenishLocation().getId(), "Detail Replen Location");
        assertEquals(theDetails.getQuantity(), 0, "Detail Quantity replenished");
        assertEquals(theDetails.getOperator().getId(), this.cmd.getOperator().getId(), "Detail Operator");
        assertEquals(makeStringFromDate(theDetails.getStartTime()),
                     makeStringFromDate(this.startDate), "Detail startTime");
        assertEquals(makeStringFromDate(theDetails.getReplenishTime()),
                     makeStringFromDate(endDate), "Detail replenishTime");
        assertNotNull(theDetails.getReason(), "Reason code");
        assertEquals(replen.getId(), theDetails.getReplenishment().getId(), "Detail replenishment");
        assertEquals(theDetails.getExportStatus(), ExportStatus.NotExported, "Export Status");

        // When we cancel a replenishment, the location status should be set to replenished.
        // This seems strange but it is by design.  The worst thing that will happen is
        // we will send a selector to an empty slot which they will short and another
        // replenishment will be triggered.
        Location replenishedLocation = 
            this.cmd.getLocationManager().findLocationFromID(replen.getToLocation().getId());
        assertEquals(replenishedLocation.getStatus(), LocationStatus.Replenished, "Location replenishment status");
        
        List<OperatorLabor> olList = 
            this.cmd.getLaborManager() .getOperatorLaborManager()
                 .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec = olList.get(1);
        
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Replenishment, "Invalid Action Type: ");
        assertEquals(olRec.getFilterType(), OperatorLaborFilterType.Replenishment, "Invalid Filter Type");
        assertNotNull(olRec.getStartTime(), "Start Time is Null: ");
        assertNull(olRec.getEndTime(), "End Time is Not Null: ");
        assertEquals(olRec.getDuration(), new Long(0), "Invalid Duration: ");
        assertEquals(olRec.getCount(), new Integer(0), "Invalid Count: ");
        assertEquals(olRec.getActualRate(),  new Double(0), "Invalid Actual Rate: ");
        assertEquals(olRec.getPercentOfGoal(), new Double(0), "Invalid Percent of Goal: ");
        assertNull(olRec.getBreakType(), "Break type is not null");
        assertNotNull(olRec.getRegion(), "Region Id is Null: ");
    }
    
    
    
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteComplete() throws Exception {

        initialize();
        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior.
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        //Test completed replenishment
        Response response = executeCommand(endDate, "21335", "30", "-916", "1", "", this.startDate);

        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        Replenishment replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21335");
        assertEquals(ReplenishStatus.Complete, replen.getStatus(), "Replenishment Complete");
 
        List<ReplenishmentDetail> details = this.cmd.getReplenishmentDetailManager().getAll();
        assertEquals(details.size(), 1, "DetailCount");
        ReplenishmentDetail theDetails = details.get(0);
        assertEquals(ReplenishDetailStatus.Replenished, theDetails.getStatus(), "DetailStatus");
        assertNull(theDetails.getReason(), "Reason code");
        
        List<OperatorLabor> olList = 
            this.cmd.getLaborManager() .getOperatorLaborManager()
                 .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec = olList.get(1);
        
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Replenishment, "Invalid Action Type: ");
        assertNotNull(olRec.getStartTime(), "Start Time is Null: ");
        assertNull(olRec.getEndTime(), "End Time is Not Null: ");
        assertTrue(olRec.getDuration() > new Long(0), "Invalid Duration: ");
        assertEquals(olRec.getCount(), new Integer(1), "Invalid Count: ");
        assertTrue(olRec.getActualRate() > new Double(0), "Invalid Actual Rate: ");
        assertTrue(olRec.getPercentOfGoal() > new Double(0), "Invalid Percent of Goal: ");
        assertNotNull(olRec.getRegion(), "Region Id is Null: ");
    }
    
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testMultiRegions() throws Exception {

        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();
        classSetupReplenData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "31", "1"});
        executeLutCmd("cmdPrTaskLUTForkReplenishmentRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        
        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior..
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        // Complete a replenihsment while signed on to multiple regions.
        // This is testing that the region on the labor record is null since we can not
        // track multiple regions.
        Response response = executeCommand(endDate, "21363", "50", "-1107", "1", "", this.startDate);

        assertEquals(1, response.getRecords().size(), "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        Replenishment replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21363");
        assertEquals(ReplenishStatus.Complete, replen.getStatus(), "Replenishment Complete");
        
        List<ReplenishmentDetail> details  
                = this.cmd.getReplenishmentDetailManager().getAll();
        assertEquals(details.size(), 1, "DetailCount");
        ReplenishmentDetail theDetails = details.get(0);
        assertEquals(ReplenishDetailStatus.Replenished, theDetails.getStatus(), "DetailStatus");
        assertNull(theDetails.getReason(), "Reason code");
        
        
        List<OperatorLabor> olList = this.cmd.getLaborManager().getOperatorLaborManager()
                            .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec = olList.get(1);

        assertEquals(olRec.getActionType(), OperatorLaborActionType.Replenishment, "Invalid Action Type: ");
        assertNotNull(olRec.getStartTime(), "Start Time is Null: ");
        assertNull(olRec.getEndTime(), "End Time is Not Null: ");
        assertTrue(olRec.getDuration() > new Long(0), "Invalid Duration: ");
        assertEquals(olRec.getCount(), new Integer(1), "Invalid Count: ");
        assertTrue(olRec.getActualRate() > new Double(0), "Invalid Actual Rate: ");
        assertTrue(olRec.getPercentOfGoal() > new Double(0), "Invalid Percent of Goal: ");
        assertNull(olRec.getRegion(), "Region Id is Not Null: ");
    }
    


    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteReplenAlreadyCompleted() throws Exception {

        initialize();
        // Set Replenishment 21339 to Status Completed
        String replenNumber = "21339";
        Replenishment replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber(replenNumber);
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);

        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior.
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        //Attempt to get a replenihsment that is already completed

        Response response = executeCommand(endDate, replenNumber, "20", "-247", "1", "", this.startDate);

        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.REPLEN_ALREADY_COMPLETE.getErrorCode(),
                "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains(replenNumber),
                   "error message should contain Replenishment Number " + replenNumber);
    }
    
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.replenishment.task.ReplenishmentLicenseCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteTakeABreak() throws Exception {

        initialize();
        // NOTE: The task captures and stores the start time of the replenishment.
        //       The startDate needs to be less than the endDate.
        //       Add 1 minute to the endDate to simulate this behavior.
        Date endDate = this.getCmdDateSec();
        endDate.setTime(this.startDate.getTime() + 60000);
        
        // Test Taking a break
        String replenNumber = "21338";
        Response response = executeCommand(endDate, replenNumber, "50", "-250", "1", "", this.startDate);
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        Replenishment replenishment = this.cmd.getReplenishmentManager().findReplenishmentByNumber(replenNumber);
        assertEquals(ReplenishStatus.Complete, replenishment.getStatus(), "Replenishment Complete");
        
        takeABreak("1", "0", "0");
       // Thread.sleep(1000);
        List<OperatorLabor> olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec1 = olList.get(0);
        assertEquals(olRec1.getActionType(), OperatorLaborActionType.SignOn, "Action Type ");
        OperatorLabor olRec2 = olList.get(1);
        assertEquals(olRec2.getActionType(), OperatorLaborActionType.Replenishment, "Action Type ");
        OperatorLabor olRec3 = olList.get(2);
        assertEquals(olRec3.getActionType(), OperatorLaborActionType.Break, "Action Type ");
        OperatorBreakLabor blRec = (OperatorBreakLabor) olRec3;
        assertNotNull(olRec3.getStartTime().getTime(), "Start Time ");
        assertNull(olRec3.getEndTime(), "End Time ");
        assertNotNull(olRec3.getCreatedDate().getTime(), "Created Date ");
        assertEquals(blRec.getPreviousOperatorLabor().getId(), olRec2.getId(), "Previous Operator Labor Id ");
        assertNotNull(blRec.getBreakType(), "Break Id ");
        // return from the break
        takeABreak("1", "1", "0");
        olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        assertEquals(olList.size(), 4, "Number of Records ");
        olRec2 = olList.get(1);
        OperatorLabor olRec4 = olList.get(3);
        assertEquals(olRec4.getActionType(), OperatorLaborActionType.Replenishment, "Action Type ");
        assertNotNull(olRec4.getStartTime().getTime(), "Start Time ");
        assertEquals(olRec4.getDuration(), new Long(0), "Duration ");
        assertNull(olRec4.getEndTime(), "End Time");
        assertEquals(olRec4.getOperator().getId(), olRec2.getOperator().getId(), "Operator Id ");
        assertEquals(olRec4.getRegion().getId(), new Long(-31), "Region Id ");
    }
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();
        classSetupReplenData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.startDate), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        this.bumpStartDate();
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.startDate), sn, operId});
        this.bumpStartDate();
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.startDate), sn, operId, "1234"});
        this.bumpStartDate();
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.startDate), sn, operId});
        this.bumpStartDate();
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.startDate), sn, operId, "31", "0"});
        this.bumpStartDate();
        executeLutCmd("cmdPrTaskLUTForkReplenishmentRegionConfiguration", 
            new String[] {makeStringFromDate(this.startDate), sn, operId});
        this.bumpStartDate();
    }
    
    
    /**
     * 
     * @param breakType -  type of break, battery change, lunch, etc
     * @param startEndFlag - 0 to start break.  1 to end break
     * @param description - not used
     * @throws Exception
     */
    private void takeABreak(String breakType, String startEndFlag, String description)
                                                              throws Exception {

        executeLutCmd("cmdPrTaskODRCoreSendBreakInfo", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            breakType, startEndFlag, description});
        this.bumpStartDate();
    }
    
    /**
     * This method is used to increment the start time by 1 minute.
     * This is necessary so we can validate labor records.
     *
     */
    private void bumpStartDate() {
        this.startDate.setTime(this.startDate.getTime() + 60000);
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param replenishNumber - number of replenishment
     * @param quantityPut - quantity to put
     * @param locationPk - location to replenish
     * @param replenishIndicator - replenishment indicator
     * @param reasonCode - reason code
     * @param startTime - start time
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, 
                                    String replenishNumber, 
                                    String quantityPut,
                                    String locationPk,
                                    String replenishIndicator,
                                    String reasonCode,
                                    Date startTime) throws Exception {
        this.cmd = getCmdBean();
       
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                replenishNumber, 
                quantityPut, 
                locationPk, 
                replenishIndicator, 
                reasonCode, 
                makeStringFromDate(startTime)},  this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
