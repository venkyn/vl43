/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.Test;


/**
 * 
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, REPLENISHMENT })

public class GetReplenishmentCmdTest extends TaskCommandTestCase {

    private GetReplenishmentCmd cmd;
    private String operId = "GetReplenishmentOper";
    private String sn = "GetReplenishmentSerial";

    /**
     * @return a bean for testing.
     */
    private GetReplenishmentCmd getCmdBean() {
        return (GetReplenishmentCmd) getBean("cmdPrTaskLUTForkGetReplenishment");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecute() throws Exception {

        initialize();

        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(1, response.getRecords().size(), "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("replenishmentNumber"), "21335", "replenishmentNumber");
        assertEquals(record.get("license"), "10916", "license");
        assertEquals(record.get("promptLicense"), true, "promptLicense");
        assertEquals(record.get("regionNumber").toString(), "31", "regionNumber");
        assertEquals(record.get("itemNumber"), "146795", "itemNumber");
        assertEquals(record.get("itemDescription"), "TWIZZLER STRAWBERRY", "itemDescription");
        assertEquals(record.get("quantity").toString(), "30", "quantity");
        assertEquals(record.get("fromLocationPreAisle"), null, "fromLocationPreAisle");
        assertEquals(record.get("fromLocationAisle"), "47", "fromLocationAisle");
        assertEquals(record.get("fromLocationPostAisle"), null, "fromLocationPostAisle");
        assertEquals(record.get("fromLocationSlot"), "425", "fromLocationSlot");
        assertEquals(record.get("replenSpokenValidation"), "20", "replenSpokenValidation");
        assertEquals(record.get("replenScannedValidation"), "10916", "replenScannedValidation");
        assertEquals(record.get("toLocationPreAisle"), null, "toLocationPreAisle");
        assertEquals(record.get("toLocationAisle"), "54", "toLocationAisle");
        assertEquals(record.get("toLocationPostAisle"), null, "toLocationPostAisle");
        assertEquals(record.get("toLocationSlot"), "001", "toLocationSlot");
        assertEquals(record.get("toLocationCheckDigits"), "48", "toLocationCheckDigits");
        assertEquals(record.get("toLocationScannedLocation"), "0754001", "toLocationScannedLocation");
        assertEquals(record.get("toLocationPk").toString(), "-259", "toLocationPk");
        assertEquals(record.get("goalTime"), "5", "goalTime");
        assertEquals(record.get("errorCode"), "0", "errorCode");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNoWork() throws Exception {

        initializeNoWork();
        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.NO_WORK_AVAILABLE.getErrorCode(),
                "errorCode");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteWorkInOtherRegion() throws Exception {

        initialize();
        //Complete all license in signed in region 31
        this.cmd = getCmdBean();
        Replenishment replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21335");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21336");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21337");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21338");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21339");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21340");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21341");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21342");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21343");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);
        replen = this.cmd.getReplenishmentManager().findReplenishmentByNumber("21344");
        replen.setComplete();
        this.cmd.getReplenishmentManager().save(replen);

        //Test No authorized regions error
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.WORK_IN_OTHER_REGION.getErrorCode(),
                "errorCode");
    }

    
    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();
        classSetupReplenData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "31", "0"});
    }
    
    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeNoWork() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidReplenishmentRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTForkRequestReplenishmentRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "31", "0"});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
