/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.REPLENISHMENT;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.Test;


/**
 * 
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, REPLENISHMENT })
public class RequestReplenishmentRegionCmdTest extends TaskCommandTestCase {

    private RequestReplenishmentRegionCmd cmd;
    private String operId = "RequestReplenishmentRegionOper";
    private String sn = "RequestReplenishmentRegionSerial";

    /**
     * @return a bean for testing.
     */
    private RequestReplenishmentRegionCmd getCmdBean() {
        return (RequestReplenishmentRegionCmd) 
            getBean("cmdPrTaskLUTForkRequestReplenishmentRegion");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteSingleRegion() throws Exception {

        initialize();
        //Test signing on to a single region
        Response response = executeCommand(this.getCmdDateSec(), "31", "0");
        //Verify Successful result
        assertEquals(1, response.getRecords().size(), "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteAllRegions() throws Exception {

        initialize();
        //Test requesting all regions
        Response response = executeCommand(this.getCmdDateSec(), "", "1");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteRegionNotFound() throws Exception {

        initialize();

        //Test Region not found error
        Response response = executeCommand(this.getCmdDateSec(), "123", "0");
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD), 
                Long.toString(TaskErrorCode.REGION_NOT_FOUND.getErrorCode()), 
                "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("123"),
                   "error message should contain region 123");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteRegionNotAuthorized() throws Exception {

        initialize();
        //Test Region not found error
        Response response = executeCommand(this.getCmdDateSec(), "34", "0");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertNotNull(record.get("errorMessage"), "error message should not be null ");
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD), 
                Long.toString(TaskErrorCode.NOT_AUTH_FOR_REGION.getErrorCode()),
                "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("34"),
                   "error message should contain region 34");
    }
    
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupReplenRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param regionNumber - the region being requested
     * @param allRegions - to request all regions
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String regionNumber, 
                                    String allRegions) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, 
                                             regionNumber, allRegions}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
