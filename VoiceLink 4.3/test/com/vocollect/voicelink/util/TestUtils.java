/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.util;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import org.testng.Assert;



/**
 * @author amracna/jtauberg
 * 
 * This class provides utilities to ensure certain setters or getters are included in overridden
 * methods in partner code.
 *
 */
public class TestUtils {

    //Define constants - test values for each primitive and Object type.
    private static final int TEST_INT = 9;
    private static final byte TEST_BYTE = 9;
    private static final short TEST_SHORT = 9;
    private static final long TEST_LONG = 12345L;
    private static final double TEST_DOUBLE = 12345D;
    private static final float TEST_FLOAT = 123.45F;
    private static final char TEST_CHAR = 'y';
    private static final String TEST_STRING_OBJ = "testing";
    private static final Integer TEST_INTEGER_OBJ = 9;
    private static final Byte TEST_BYTE_OBJ = 9;
    private static final Short TEST_SHORT_OBJ = 9;
    private static final Long TEST_LONG_OBJ = 12345L;
    private static final Double TEST_DOUBLE_OBJ = 12345D;
    private static final Float TEST_FLOAT_OBJ = 123.45F;
    private static final Character TEST_CHAR_OBJ = 'y';
    
    
    /**
     * Builds a list of getters and a list of setters for fields that have both.
     * Does not include any inherited methods.
     * @param clazz - the Class to get the list of getters and setters from.
     * @param setters - the List of setter methods to fill
     * @param getters - the List of getter methods to fill
     */
    public void getSettersAndGettersToExecute(Class<?> clazz, List<Method> setters, List<Method> getters) {
        
        // get the fields
        Field[] fields = clazz.getDeclaredFields();
        
        for (Field field : fields) {
            // see if there is a getter for this field
            String getterMethodName = getGetterMethodName(field);
            Method getterMethod = null;
            
            try {
                getterMethod = clazz.getMethod(getterMethodName, (Class[]) null);
            } catch (NoSuchMethodException e) {
                // get method doesn't exist, try is in case it's a boolean
                try {
                    getterMethod = clazz.getMethod(getterMethodName.replaceFirst("get", "is"), (Class[]) null);
                } catch (NoSuchMethodException ex) {
                    // no valid JavaBean getter for this method
                    continue;
                }
            }
            
            // see if there is a setter for this field
            String setterMethodName = getSetterMethodName(field);
            Method setterMethod;
            
            try {
                setterMethod = clazz.getMethod(setterMethodName, new Class[] {getterMethod.getReturnType()});
            } catch (NoSuchMethodException e) {
                // method doesn't exist, skip to the next field
                continue;
            }
            
            getters.add(getterMethod);
            setters.add(setterMethod);
        } 
    }
    
    /**
     * Invokes the list of setters. Assumes list contains only valid JavaBean setters.
     * @param object - the Object to invoke the setters on.
     * @param setters - the List of setter methods to invoke on the object.
     * @throws Exception - Various.
     * @throws RuntimeException - if object contains a member of an unsupported type.
     */
    public void invokeSetters(Object object, List<Method> setters)
            throws Exception, RuntimeException {
        
        for (Method method : setters) {
            
            Class<?>[] parameters = method.getParameterTypes();
                
            // get the type of the parameter
            String typeName = parameters[0].getName();

            // look for primitives
            if (typeName.equals("byte")) {
                method.invoke(object, TEST_BYTE);
            } else if (typeName.equals("short")) {
                method.invoke(object, TEST_SHORT);
            } else if (typeName.equals("int")) {
                method.invoke(object, TEST_INT);
            } else if (typeName.equals("long")) {
                method.invoke(object, TEST_LONG);
            } else if (typeName.equals("float")) {
                method.invoke(object, TEST_FLOAT);
            } else if (typeName.equals("double")) {
                method.invoke(object, TEST_DOUBLE);
            } else if (typeName.equals("boolean")) {
                method.invoke(object, true);
            } else if (typeName.equals("char")) {
                method.invoke(object, TEST_CHAR);
            } else {

                // the type is not a primitive
                // get the Class and instantiate a new object of that type
                Class<?> parameterClass = Class.forName(typeName);
                Object newObject = parameterClass.newInstance();
                
                // look for String and wrapped primitives
                if (newObject instanceof String) {
                    newObject = TEST_STRING_OBJ;
                } else if (newObject instanceof Byte) {
                    newObject = TEST_BYTE_OBJ;
                } else if (newObject instanceof Short) {
                    newObject = TEST_SHORT_OBJ;
                } else if (newObject instanceof Integer) {
                    newObject = TEST_INTEGER_OBJ;
                } else if (newObject instanceof Long) {
                    newObject = TEST_LONG_OBJ;
                } else if (newObject instanceof Float) {
                    newObject = TEST_FLOAT_OBJ;
                } else if (newObject instanceof Double) {
                    newObject = TEST_DOUBLE_OBJ;
                } else if (newObject instanceof Boolean) {
                    newObject = true;
                } else if (newObject instanceof Character) {
                    newObject = TEST_CHAR_OBJ;
                } else {
                    throw new RuntimeException("InvokeSetters() Unsupported type:" + typeName);
                }
                
                // invoke the method to set the value on the pick
                method.invoke(object, newObject);
                
            }
        }        
    }

    
    /**
     * Invokes a list of getters on the supplied Objects and compares the returned values.
     * Assumes the supplied list of getter methods only contains valid JavaBean getters.
     * Also assumes that the supplied objects are of the same type.
     * @param object1 - the first Object to compare
     * @param object2 - the second Object to compare
     * @param getters - a List of getter methods
     * @throws Exception - any
     */
    public void invokeGettersAndCompare(Object object1, Object object2, List<Method> getters) throws Exception {
        
        for (Method method : getters) {
         
            try {
                // execute the getters and make sure the results match
                Object object1ReturnObj = method.invoke(object1, (Object[]) null);
                Object object2ReturnObj = method.invoke(object2, (Object[]) null);

                if (object1ReturnObj == null && object2ReturnObj == null) {
                    Assert.fail("For method " 
                            + method.getName()
                            + "both objects returned null.");
                } else if (object1ReturnObj == null && object2ReturnObj != null) {
                    Assert.fail("For method " + method.getName()
                            + " object1 was null but object2 wasn't!");
                } else if (object1ReturnObj != null && object2ReturnObj == null) {
                    Assert.fail("For method " + method.getName()
                            + " object2 was null but object1 wasn't!");
                } else if (!object1ReturnObj.equals(object2ReturnObj)) {
                    // fail!
                    Assert.fail("For method "
                            + method.getName()
                            + " , object1 and object2 did not return the same value. "
                            + "object1 returned "
                            + object1ReturnObj.toString()
                            + " and object2 returned "
                            + object2ReturnObj.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } 
        } 
    }
    
    /**
     * Utility method to build a setter method name from a field.
     * @param field - the Field to build the setter method name from.
     * @return - the setter method name ("sett"+field after uppercasing the first letter of field.)
     */
    public String getSetterMethodName(Field field) {
        
        StringBuilder setterMethodName = new StringBuilder();
        setterMethodName.append("set");
        setterMethodName.append(field.getName().substring(0, 1).toUpperCase());
        setterMethodName.append(field.getName().substring(1));
        return setterMethodName.toString();
    }

    /**
     * Utility method to build a getter method name from a field.
     
     * @param field - the Field to build the getter method name from.
     * @return - the getter method name ("get"+field after uppercasing the first letter of field.)
     */
    public String getGetterMethodName(Field field) {
        
        StringBuilder getterMethodName = new StringBuilder();
        getterMethodName.append("get");
        getterMethodName.append(field.getName().substring(0, 1).toUpperCase());
        getterMethodName.append(field.getName().substring(1));
        return getterMethodName.toString();
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
