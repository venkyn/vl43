/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.util;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dto.ReportParametersDTO;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.Report;
import com.vocollect.epp.model.ReportFormat;
import com.vocollect.epp.model.ReportInterval;
import com.vocollect.epp.model.ReportParameter;
import com.vocollect.epp.model.ReportTypeParameter;
import com.vocollect.epp.service.ReportManager;
import com.vocollect.epp.service.ReportTypeManager;
import com.vocollect.epp.service.ReportTypeParameterManager;
import com.vocollect.epp.util.JasperReportWrapperRoot;
import com.vocollect.epp.util.ReportUtilities;
import com.vocollect.epp.util.ReportWrapperRoot;
import com.vocollect.epp.util.ReportWrapperRoot.RenderFormat;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Test class to test the functionality of JasperReportWarapper implementation
 * of ReportWrapper. This class tests physical existence of report definition
 * file on the file system, its compatibility with japser framework and report
 * generation functionality
 *
 * @author mraj
 */

public class ReportWrapperTest extends VoiceLinkDAOTestCase {
    //
    // private static final String ASSIGNMENT_REPORT_NAME = "Assignment";
    //
    // private static final String SLOT_VISIT_REPORT_NAME = "SlotVisit";
    //
    // private Report assignmentReport;
    //
    // private Report slotVisitReport;
    //
    // private static final String JRXML_FILE_EXT =
    // ReportWrapperRoot.JASPER_REPORT_SOURCE_EXTENSION;
    //
    // private ReportManager reportManager;
    //
    // private ReportTypeManager reportTypeManager;
    //
    // private ReportTypeParameterManager reportTypeParameterManager;
    //
    // private ReportWrapperRoot reportWrapper;
    //
    // private String reportsPath;
    //
    // /**
    // * {@inheritDoc}
    // * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
    // */
    // @Override
    // protected void onSetUp() throws Exception {
    // super.onSetUp();
    // String reportsBaseDir = "src-custom/reports/voicelink";
    // reportsPath = System.getProperty("test.input.dir");
    // if (null == reportsPath) {
    // reportsPath = reportsBaseDir;
    // } else {
    // reportsPath = reportsPath + "../../" + reportsBaseDir;
    // }
    //
    // // TODO Spring injected
    // this.reportWrapper = new JasperReportWrapperRoot(
    // reportsPath, ASSIGNMENT_REPORT_NAME + JRXML_FILE_EXT);
    // loadContextLocations(getApplicationContext());
    //
    // // create reports required for tests.
    // createReports();
    // }
    //
    // /**
    // * @see BaseDAOTestCase#onTearDown()
    // */
    // @Override
    // protected void onTearDown() throws Exception {
    // deleteReport();
    // super.onTearDown();
    // }
    //
    // /**
    // * Method to test report exist with all legal values
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testReportExistsWorksGood() throws VocollectException {
    //
    // // test un-initialized
    // assertNull(((JasperReportWrapperRoot)
    // this.reportWrapper).getReportDesign());
    //
    // try {
    // assertTrue(
    // this.reportWrapper.reportExists(
    // reportsPath, ASSIGNMENT_REPORT_NAME + JRXML_FILE_EXT),
    // "Report wrapper did not find exisiting report." + reportsPath);
    // assertTrue(
    // this.reportWrapper.reportExists(
    // reportsPath, SLOT_VISIT_REPORT_NAME + JRXML_FILE_EXT),
    // "Report wrapper did not find exisiting report." + reportsPath);
    //
    // assertNotNull(((JasperReportWrapperRoot) this.reportWrapper)
    // .getReportDesign(), "Did not returned correct report object");
    // assertEquals(
    // ((JasperReportWrapperRoot) this.reportWrapper).getReportDesign()
    // .getName().toLowerCase(), SLOT_VISIT_REPORT_NAME
    // .toLowerCase(),
    // "Report name is different, when should have been same");
    //
    // // Wrong inputs
    // assertFalse(
    // this.reportWrapper.reportExists(
    // reportsPath, ASSIGNMENT_REPORT_NAME + "JUNK"
    // + JRXML_FILE_EXT), "Report found, when does not exist");
    // assertFalse(
    // this.reportWrapper.reportExists("JUNK", ASSIGNMENT_REPORT_NAME
    // + JRXML_FILE_EXT), "Report found, when does not exist");
    //
    // } catch (Exception e) {
    // // Test exceptions
    // fail("No exception was expected..got one");
    // }
    // }
    //
    // /**
    // * Method to test getParameters of report definition
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGetParameters() throws VocollectException {
    // try {
    //
    // assertNotNull(reportWrapper.getParameters()); // Before
    //
    // assertTrue(reportWrapper.reportExists(
    // reportsPath, ASSIGNMENT_REPORT_NAME + JRXML_FILE_EXT)); // Initialize
    // assertNotNull(reportWrapper.getParameters()); // After
    // assertNotSame(reportWrapper.getParameters().size(), 0);
    // assertNotNull(reportWrapper.getParameters().get(0).getName());
    // assertNotNull(reportWrapper.getParameters().get(0).getClass());
    // assertNotNull(reportWrapper.getParameters().get(0).getFieldType());
    // assertNotNull(reportWrapper.getParameters().get(0).getValueClass());
    // assertNotNull(reportWrapper.getParameters().get(
    // this.reportWrapper.getParameters().size() - 1).getName());
    //
    // } catch (Exception e) {
    // // Test exceptions
    // fail("No exception was expected..got one", e);
    // }
    // }
    //
    // /**
    // * Method to test report exist with non-existent definition file
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGetParametersForBadReport() throws VocollectException {
    //
    // try {
    //
    // assertNotNull(reportWrapper.getParameters()); // Before
    //
    // assertFalse(reportWrapper.reportExists(
    // reportsPath, ASSIGNMENT_REPORT_NAME + "JUNK" + JRXML_FILE_EXT)); //
    // Initialize
    // reportWrapper.getParameters(); // After
    // } catch (Exception e) {
    // // Test exceptions
    // assertTrue(e instanceof VocollectException);
    // }
    // }
    //
    // /**
    // * Method to test report generation with all correct values
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGenerateReportWorksGoodForCorrectValues()
    // throws VocollectException {
    //
    // try {
    //
    // assertNotNull(reportWrapper.getParameters()); // Before
    //
    // assertTrue(reportWrapper.reportExists(
    // reportsPath, SLOT_VISIT_REPORT_NAME + JRXML_FILE_EXT)); // Initialize
    // ((JasperReportWrapperRoot) this.reportWrapper).getReportDesign()
    // .setTitle(null);
    // Report report = this.reportManager
    // .get(this.slotVisitReport.getId());
    // assertNotNull(this.reportWrapper.getParameters()); // After
    // Map<String, Object> values = ReportUtilities.getReportValuesMap(
    // this.reportWrapper.getParameters(), convertToDTO(report
    // .getReportParameters()), getReportValues());
    //
    // InputStream reportStream = this.reportWrapper
    // .generateReport(values);
    // assertNotNull(report);
    // assertTrue(reportStream.available() > 0);
    // } catch (Exception e) {
    // e.printStackTrace();
    // // Test exceptions
    // fail("No exception was expected..got one", e);
    // }
    // }
    //
    // /**
    // * Method to test report generation with invalid date parameter value
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGenerateReportForInCorrectValues()
    // throws VocollectException {
    //
    // try {
    // this.reportWrapper.reportExists(reportsPath, SLOT_VISIT_REPORT_NAME
    // + JRXML_FILE_EXT);
    // ((JasperReportWrapperRoot) this.reportWrapper).getReportDesign()
    // .setTitle(null);
    // Map<String, Object> values = getReportValues();
    // values.put("START_DATE", "JUNK");
    // this.reportWrapper.generateReport(values);
    // fail("Exception expected");
    // } catch (Exception e) {
    // assertTrue(e instanceof VocollectException);
    // }
    // }
    //
    // /**
    // * Method to test overloaded report generation method with all parameters
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGenerateReportForAllParams() throws VocollectException {
    //
    // try {
    // this.reportWrapper.reportExists(reportsPath, SLOT_VISIT_REPORT_NAME
    // + JRXML_FILE_EXT);
    // ((JasperReportWrapperRoot) this.reportWrapper).getReportDesign()
    // .setTitle(null);
    // loadContextLocations(getApplicationContext());
    // Report report = this.reportManager
    // .get(this.slotVisitReport.getId());
    // Set<ReportParametersDTO> reportDefinitionParams = convertToDTO(report
    // .getReportParameters());
    // Map<String, Object> values = ReportUtilities.getReportValuesMap(
    // this.reportWrapper.getParameters(), reportDefinitionParams,
    // getReportValues());
    // Object ds = null;
    //
    // InputStream reportStream = this.reportWrapper.generateReport(
    // values, RenderFormat.HTML.toString(), "fr", ds);
    // assertNotNull(report);
    // assertTrue(reportStream.available() > 0);
    //
    // } catch (Exception e) {
    // fail("No exception was expected..got one", e);
    // }
    // }
    //
    // /**
    // *Method to test report generation with Hibernate datasource
    // * @throws VocollectException
    // */
    // @Test(enabled = false)
    // public void testGenerateReportForHibernateDataSource()
    // throws VocollectException {
    //
    // try {
    //
    // assertNotNull(reportWrapper.getParameters()); // Before
    // assertTrue(reportWrapper.reportExists(
    // reportsPath, this.assignmentReport.getType()
    // .getReportTypeName()
    // + JRXML_FILE_EXT)); // Initialize
    // // This was required to avoid filling title requirements
    // ((JasperReportWrapperRoot) this.reportWrapper).getReportDesign()
    // .setTitle(null);
    //
    // assertNotNull(reportWrapper.getParameters()); // After
    //
    // Report report = this.reportManager.get(this.assignmentReport
    // .getId());
    // Set<ReportParametersDTO> reportDefinitionParams = convertToDTO(report
    // .getReportParameters());
    // Map<String, Object> values = ReportUtilities.getReportValuesMap(
    // this.reportWrapper.getParameters(), reportDefinitionParams,
    // getReportValues());
    // values.put(
    // ReportWrapperRoot.JASPER_HIBERNATE_SESSION_PARAM,
    // getSessionFactory().openSession());
    // InputStream reportStream = this.reportWrapper
    // .generateReport(values);
    // assertNotNull(report);
    // assertTrue(reportStream.available() > 0);
    // } catch (Exception e) {
    // e.printStackTrace();
    // fail("No exception was expected..got one", e);
    // }
    //
    // }
    //
    // /**
    // * Utility method to collect all user submitted parameters values in a map
    // * of values to be used for invocation of report generation
    // * @return Map of user submitted values. <code>key</code> Parameter name
    // <br>
    // * <code>value</code> Parameter value
    // */
    // private Map<String, Object> getReportValues() {
    // Map<String, Object> values = new HashMap<String, Object>();
    // values.put(ReportUtilities.PARAM_CURRENT_SITE_CONTEXT, "Default");
    // values.put(ReportUtilities.PARAM_CURRENT_USER, "admin");
    // values.put(ReportUtilities.SITE, "-1");
    // SimpleDateFormat format = new SimpleDateFormat(
    // ReportUtilities.DATE_FORMAT);
    // format.setLenient(false);
    // values.put(ReportUtilities.START_DATE, "Thu Jul 15 00:00:00 EDT 2010");
    // values.put(ReportUtilities.END_DATE, "Thu Jul 15 23:59:59 EDT 2010");
    // values.put(ReportUtilities.REPORT_NAME, ASSIGNMENT_REPORT_NAME);
    // values.put(ReportWrapperRoot.JASPER_REPORT_LOCALE, new Locale("en",
    // "US"));
    // return values;
    // }
    //
    // /**
    // * Method to convert ReportParameter object to ReportParametersDTO object,
    // so
    // * that it can be used in util classes
    // * @param reportParameters Set of ReportParameter objects associated with
    // * the report object
    // * @return Set of ReportParametersDTO objects adapted from ReportParameter
    // * objects
    // */
    // private Set<ReportParametersDTO> convertToDTO(Set<ReportParameter>
    // reportParameters) {
    // Set<ReportParametersDTO> paramList = new HashSet<ReportParametersDTO>(
    // reportParameters.size());
    // ReportParametersDTO param;
    // for (ReportParameter reportParam : reportParameters) {
    //
    // param = new ReportParametersDTO();
    // param.setDescription(reportParam.getDescriptiveText());
    // param.setName(reportParam.getReportTypeParameter()
    // .getParameterName());
    // param.setValue(reportParam.getValue());
    // param.setValueClass(reportParam.getClass());
    // paramList.add(param);
    // }
    //
    // return paramList;
    // }
    //
    // /**
    // *
    // * @return array of file locations to be in classpath
    // */
    // private String[] getApplicationContext() {
    // final String[] eppSpringTestConfigs = new String[] {
    // "classpath*:/applicationContext-epp*.xml" };
    // return eppSpringTestConfigs;
    // }
    //
    // /**
    // * Getter for the reportManager property.
    // * @return ReportManager value of the property
    // */
    // @Test()
    // public ReportManager getReportManager() {
    // return reportManager;
    // }
    //
    // /**
    // * Setter for the reportManager property.
    // * @param reportManager the new reportManager value
    // */
    // public void setReportManager(ReportManager reportManager) {
    // this.reportManager = reportManager;
    // }
    //
    // /**
    // * Method to create reports
    // * @throws DataAccessException
    // * @throws BusinessRuleException
    // */
    //
    // private void createReports() throws DataAccessException,
    // BusinessRuleException {
    // createAssignmentReport();
    // createSlotVisitReport();
    // }
    //
    // /**
    // * Method to create Assignment report
    // * @throws DataAccessException
    // * @throws BusinessRuleException
    // */
    // private void createAssignmentReport() throws DataAccessException,
    // BusinessRuleException {
    // String reportName = "TestCreated_Assignment_Report_9512357"; // random
    // // name
    // Report newAssignmentReport = new Report();
    // Set<ReportParameter> reportParams = new HashSet<ReportParameter>();
    // newAssignmentReport.setName(reportName);
    // // Consider that this could change
    // newAssignmentReport.setType(this.reportTypeManager.get(-200L));
    // newAssignmentReport.setFormat(ReportFormat.HTML);
    // newAssignmentReport.setInterval(ReportInterval.Yesterday);
    //
    // ReportTypeParameter repTypeParam = this.reportTypeParameterManager
    // .get(-201L);
    // reportParams.add(new ReportParameter(repTypeParam, "12"));
    //
    // repTypeParam = this.reportTypeParameterManager.get(-202L);
    // reportParams.add(new ReportParameter(repTypeParam, "_ALL_"));
    //
    // repTypeParam = this.reportTypeParameterManager.get(-203L);
    // reportParams.add(new ReportParameter(repTypeParam, "123"));
    //
    // repTypeParam = this.reportTypeParameterManager.get(-204L);
    // reportParams.add(new ReportParameter(repTypeParam, "12"));
    //
    // repTypeParam = this.reportTypeParameterManager.get(-205L);
    // reportParams.add(new ReportParameter(repTypeParam, "12"));
    // newAssignmentReport.setReportParameters(reportParams);
    //
    // this.reportManager.save(newAssignmentReport);
    // this.assignmentReport = newAssignmentReport;
    //
    // }
    //
    // /**
    // * Method to create Slot visit report
    // * @throws DataAccessException
    // * @throws BusinessRuleException
    // */
    // private void createSlotVisitReport() throws DataAccessException,
    // BusinessRuleException {
    // String reportName = "TestCreated_Slot_Visit_Report_9512357"; // random
    // // name
    // Report newSlotVistReport = new Report();
    // Set<ReportParameter> repParams = new HashSet<ReportParameter>();
    // newSlotVistReport.setName(reportName);
    // newSlotVistReport.setType(this.reportTypeManager.get(-1000L));
    // newSlotVistReport.setFormat(ReportFormat.HTML);
    // newSlotVistReport.setInterval(ReportInterval.Yesterday);
    //
    // ReportTypeParameter repTypeParam = this.reportTypeParameterManager
    // .get(-1001L);
    // repParams.add(new ReportParameter(repTypeParam, "10"));
    //
    // newSlotVistReport.setReportParameters(repParams);
    // this.reportManager.save(newSlotVistReport);
    // this.slotVisitReport = newSlotVistReport;
    // }
    //
    // /**
    // *
    // * @throws BusinessRuleException
    // * @throws DataAccessException
    // */
    // private void deleteReport() throws BusinessRuleException,
    // DataAccessException {
    // reportManager.delete(this.assignmentReport.getId());
    // reportManager.delete(this.slotVisitReport.getId());
    // }
    //
    // /**
    // * Getter for the reportTypeManager property.
    // * @return ReportTypeManager value of the property
    // */
    //
    // public ReportTypeManager getReportTypeManager() {
    // return reportTypeManager;
    // }
    //
    // /**
    // * Setter for the reportTypeManager property.
    // * @param reportTypeManager the new reportTypeManager value
    // */
    //
    // public void setReportTypeManager(ReportTypeManager reportTypeManager) {
    // this.reportTypeManager = reportTypeManager;
    // }
    //
    // /**
    // * Getter for the reportTypeParameterManager property.
    // * @return ReportTypeParameterManager value of the property
    // */
    //
    // public ReportTypeParameterManager getReportTypeParameterManager() {
    // return reportTypeParameterManager;
    // }
    //
    // /**
    // * Setter for the reportTypeParameterManager property.
    // * @param reportTypeParameterManager the new reportTypeParameterManager
    // * value
    // */
    //
    // public void setReportTypeParameterManager(ReportTypeParameterManager
    // reportTypeParameterManager) {
    // this.reportTypeParameterManager = reportTypeParameterManager;
    // }

}
