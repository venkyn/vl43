/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service;

import com.vocollect.epp.web.service.WebServiceTestCase;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;

/**
 * Base test class for VoiceLink CXF-based web services tests.
 * @param <T> the generic web services interface under test
 *
 * @author ddoubleday
 */
public class VoiceLinkWebServiceTestCase<T> extends WebServiceTestCase<T> {

    /**
     * @return the Spring config locations required for VL Web Services tests.
     */
    public static final String[] getVLWebServicesTestConfigLocations() {
        String[] eppWebConfigs = getWebServicesTestConfigLocations();
        String[] vlDAOConfigs = VoiceLinkDAOTestCase.getDAOTestConfigLocations();
        String[] config = new String[eppWebConfigs.length + vlDAOConfigs.length + 4];
        System.arraycopy(eppWebConfigs, 0, config, 0, eppWebConfigs.length);
        System.arraycopy(vlDAOConfigs, 0, config, eppWebConfigs.length,
                         vlDAOConfigs.length);
        config[config.length - 4] = 
                "classpath:/applicationContext-epp-events.xml";
        config[config.length - 3] = 
                "classpath:/applicationContext-voicelink-events.xml";
        config[config.length - 2] = 
            "classpath:/applicationContext-voicelink-task*.xml";
        config[config.length - 1] = 
            "classpath:/applicationContext-voicelink-webServices.xml";
        return config;

    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLWebServicesTestConfigLocations();
    }
    
}
