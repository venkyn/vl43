/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.CoreErrorCode;

import static com.vocollect.epp.test.TestGroups.WEBSERVICE;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.Test;

/**
 * Unit Tests for Lot Import web services.
 *
 * @author mkoenig
 */
@Test(groups = { WEBSERVICE })
public class LotWebServiceTest extends VoiceLinkWebServiceTestCase<LotWebService> {

    private static final long MAX_TIME = 60000;
    
    /**
     * @return get client handle
     */
    public LotWebService getClient() {
        return getClient(LotWebService.class, "LotService", null);
    }
    
    /**
     * test validations.
     * 
     * @throws Exception - any exception
     */
    public void testValidations() throws Exception {
        LotWebService service = getClient();
        long errorCode;
        
        //test delete validatations
        errorCode = service.deleteLot("invalid", "123", "456", "L789");
        assertEquals(CoreErrorCode.SITE_NOT_FOUND.getErrorCode(), errorCode,
            "Expected Site not found error code");
        
        errorCode = service.deleteLot("Default", "", "456", "L789");
        assertEquals(CoreErrorCode.LOT_NUMBER_EMTPY.getErrorCode(), errorCode,
            "Expected Empty Lot Number error code");
        
        errorCode = service.deleteLot("Default", 
            "123456789012345678901234567890123456789012345678901", "456", "L789");
        assertEquals(CoreErrorCode.LOT_NUMBER_TO_LONG.getErrorCode(), errorCode,
            "Expected Lot to long  error code");
        
        errorCode = service.deleteLot("Default", "123", "456", "L789");
        assertEquals(CoreErrorCode.LOT_NOT_FOUND.getErrorCode(), errorCode,
            "Expected lot not found error code");
        
        //test create validatations
        errorCode = service.createUpdateLot("invalid", "lotNumber", "itemNumber", 
            "locationId", "speakableLotNumber", new Date());
        assertEquals(CoreErrorCode.SITE_NOT_FOUND.getErrorCode(), errorCode,
            "Expected Site not found error code");

        errorCode = service.createUpdateLot("Default", "", "itemNumber", 
            "locationId", "speakableLotNumber", new Date());
        assertEquals(CoreErrorCode.LOT_NUMBER_EMTPY.getErrorCode(), errorCode,
            "Expected Empty Lot Number error code");

        errorCode = service.createUpdateLot("Default", 
            "123456789012345678901234567890123456789012345678901", "itemNumber", 
            "locationId", "speakableLotNumber", new Date());
        assertEquals(CoreErrorCode.LOT_NUMBER_TO_LONG.getErrorCode(), errorCode,
            "Expected Lot to long  error code");

        errorCode = service.createUpdateLot("Default", "lotNumber", "itemNumber", 
            "locationId", "", new Date());
        assertEquals(CoreErrorCode.SPEAKABLE_LOT_NUMBER_EMTPY.getErrorCode(), 
            errorCode, "Expected Empty Speakable Lot Number error code");

        errorCode = service.createUpdateLot("Default", "lotNumber", "itemNumber", 
            "locationId", "123456789012345678901234567890123456789012345678901", 
            new Date());
        assertEquals(CoreErrorCode.SPEAKABLE_LOT_NUMBER_TO_LONG.getErrorCode(), 
            errorCode, "Expected Speakable Lot to long  error code");
        
        errorCode = service.createUpdateLot("Default", "lotNumber", "itemNumber", 
            "locationId", "speakableLotNumber", new Date());
        assertEquals(CoreErrorCode.ITEM_NOT_FOUND.getErrorCode(), errorCode,
            "Expected Item not found error code");

        errorCode = service.createUpdateLot("Default", "lotNumber", "99838", 
            "locationId", "speakableLotNumber", new Date());
        assertEquals(CoreErrorCode.LOCATION_NOT_FOUND.getErrorCode(), errorCode,
            "Expected Location not found error code");
    }
    
    /**
     * Test successful calls to web service.
     * 
     * @throws Exception - any exceptions.
     */
    public void testCreateAndDelete() throws Exception {
        LotWebService client = getClient();
        String lotNumber = new Date().toString();
        long errorCode;
        long currentCount = 0L;

        //reset data
        resetCoredata();
        // Check number of lots in database at start of test.
        long startCount = client.countLots("Default");
        
        //Create with a location
        errorCode = client.createUpdateLot("Default", lotNumber, "99838", 
            "0814207", "speakableLotNumber", new Date());
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(++startCount, currentCount, 
            "Expected new lot count with location");
        
        //create without a location
        errorCode = client.createUpdateLot("Default", lotNumber, "99838", 
            null, "speakableLotNumber", new Date());
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(++startCount, currentCount, 
            "Expected new lot count without location");
        
        //update with location
        errorCode = client.createUpdateLot("Default", lotNumber, "99838", 
            "0814207", "new speakableLotNumber", new Date());
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(startCount, currentCount, 
            "Expected same lot count after update");
        
        //update with location
        errorCode = client.createUpdateLot("Default", lotNumber, "99838", 
            null, "new speakableLotNumber", new Date());
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(startCount, currentCount, 
            "Expected same lot count after update");
        
        //delete with location
        errorCode = client.deleteLot("Default", lotNumber, "99838", "0814207");
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(--startCount, currentCount, 
            "Expected new lot count with location");

        //delete without location
        errorCode = client.deleteLot("Default", lotNumber, "99838", null);
        assertEquals(0L, errorCode, "Expected success error code");
        currentCount = client.countLots("Default");
        assertEquals(--startCount, currentCount, 
            "Expected new lot count without location");
    }
    
    /**
     * Test performance of adding lots.
     * 
     * @throws Exception - any exception
     */
    public void testPerformance() throws Exception {
        LotWebService client = getClient();
        List<String> items = buildItems();
        List<String> locations = buildLocations();
        int lotCount = 10;

        //reset data
        resetCoredata();

        // check initial lot count.
        long startCount = client.countLots("Default");

        //=====================================================================
        //Test Adding lots
        Long lotNumber = 0L;
        Date start = new Date();
        for (String item : items) {
            for (String location : locations) {
                for (int i = 1; i <= lotCount; i++) {
                    lotNumber++;
                    client.createUpdateLot("Default", lotNumber.toString(), 
                        item, location, "speakableLotNumber", new Date());
                }
            }
        }
        Date finished = new Date();
        
        //Check all were created
        long currentCount = client.countLots("Default");
        assertEquals(currentCount, items.size() * locations.size() * lotCount + startCount, 
            "Insert Expected Count");
        
        //check max time
        assertTrue(finished.getTime() - start.getTime() < MAX_TIME,
            "Insert Exceeded Time of " + MAX_TIME);
        System.out.println("Time to insert 1000 = " + (finished.getTime() - start.getTime()));

        

        
    }
    
    /**
     * Builds list of items.
     * 
     * @return - list of items
     */
    private List<String> buildItems() {
        List<String> items = new ArrayList<String>();
        items.add("99838");
        items.add("955153");
        items.add("952754");
        items.add("952721");
        items.add("952507");
        items.add("951145");
        items.add("951004");
        items.add("950030");
        items.add("94110");
        items.add("919902");
        return items;
    }
    
    /**
     * Builds list of locations.
     * 
     * @return - list of locations
     */
    private List<String> buildLocations() {
        List<String> locations = new ArrayList<String>();
        locations.add("0814207");
        locations.add("0814189");
        locations.add("0814127");
        locations.add("0814056");
        locations.add("0814055");
        locations.add("081405B");
        locations.add("0813289");
        locations.add("0813270");
        locations.add("0813278");
        locations.add("0813099");
        return locations;
    }
    
    /**
     * @throws Exception
     */
    private void resetCoredata() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml");
    }
    
}
