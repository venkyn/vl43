/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetAlternatePutLocationCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })

public class GetAlternatePutLocationCmdTest extends TaskCommandTestCase {

    private GetAlternatePutLocationCmd cmd;
    private String operId = "operGetAlternatePutLocationCmd";
    private String sn = "serialGetAlternatePutLocationCmd";

    /**
     * @return a bean for testing.
     */
    private GetAlternatePutLocationCmd getCmdBean() {
        return (GetAlternatePutLocationCmd) 
            getBean("cmdPrTaskLUTForkGetAlternatePutLocation");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.GetAlternatePutLocationCmd#getFunction}.
     */
    @Test()
    public void testGetSetFunction() {
        this.cmd.setFunction("Function");
        assertEquals(this.cmd.getFunction(), "Function", "Function Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.GetAlternatePutLocationCmd#getLicenseNumber}.
     */
    @Test()
    public void testGetSetLicenseNumber() {
        this.cmd.setLicenseNumber("LicenseNumber");
        assertEquals(this.cmd.getLicenseNumber(), "LicenseNumber", 
                     "LicenseNumber Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.GetAlternatePutLocationCmd#getLocationId}.
     */
    @Test()
    public void testGetSetLocationId() {
        this.cmd.setLocationId("LocationId");
        assertEquals(this.cmd.getLocationId(), "LocationId", 
                     "LocationId Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.GetAlternatePutLocationCmd#getQuantityRemaining}.
     */
    @Test()
    public void testGetSetQuantityRemaining() {
        this.cmd.setQuantityRemaining("QuantityRemaining");
        assertEquals(this.cmd.getQuantityRemaining(), "QuantityRemaining", 
                     "QuantityRemaining Getter/Setter");
    }
     
    
     /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "1", "0", "1", "5");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0L, "errorcode");
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param function - function
     * @param licenseNumber - license number
     * @param locationId - Location ID
     * @param quantityRemaining - Quantity Remaining
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String function,
                                    String licenseNumber,
                                    String locationId,
                                    String quantityRemaining) 
    throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                function, licenseNumber, locationId, quantityRemaining}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
