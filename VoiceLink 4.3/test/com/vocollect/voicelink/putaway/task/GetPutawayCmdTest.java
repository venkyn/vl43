/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.Test;


/**
 * Test for GetPutawayCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class GetPutawayCmdTest extends TaskCommandTestCase {

    private GetPutawayCmd cmd;
    private String operId = "operGetPutaway";
    private String sn = "serialGetPutaway";

    /**
     * @return a bean for testing.
     */
    private GetPutawayCmd getCmdBean() {
        return (GetPutawayCmd) getBean("cmdPrTaskLUTForkGetPutAway");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec());

        //Verify no license error
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                     TaskErrorCode.NO_LICENSE_FOUND_FOR_OPERATOR.getErrorCode(),
                     "errorCode");

        //Test single license
        reserveLicense("11243");
        response = executeCommand(this.getCmdDateSec());
        
        //Verify Successful results
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "1 Record");
        assertEquals(record.get("licenseNumber"), "11243", "licenseNumber");
        assertEquals(record.get("regionNumber"), 21, "regionNumber");
        assertEquals(record.get("locationId"), -1243L, "locationId");
        assertEquals(record.get("scannedLocation"), "0582461", "scannedLocation");
        assertNull(record.get("preAisleDirection"), "preAisleDirection");
        assertEquals(record.get("aisle"), "82", "aisle");
        assertNull(record.get("postAisleDirection"), "postAisleDirection");
        assertEquals(record.get("slot"), "461", "slot");
        assertEquals(record.get("checkDigits"), "49", "checkDigits");
        assertEquals(record.get("itemNumber"), "20222", "itemNumber");
        assertEquals(record.get("itemDescription"), "RUBBERBAND", "itemDescription");
        assertEquals(record.get("quantity"), 20, "quantity");
        assertEquals(record.get("goalTime"), "4", "goalTime");
        assertEquals(record.get("errorCode"), "0", "errorCode");

        //Test multiple licenses
        reserveLicense("11240");
        response = executeCommand(this.getCmdDateSec());
        
        //Verify Successful results
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 2, "2 Records");
                
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();
        classSetupPutAwayData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidPutAwayRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTForkRequestPutAwayRegion", 
            new String[] {makeStringFromDate(
                this.getCmdDateSec()), sn, operId, "21", "0"});
        executeLutCmd("cmdPrTaskLUTForkPutAwayRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * Reserve a license.
     * @param license - license to reserve
     * @throws Exception - Any Exception
     */
    private void reserveLicense(String license) throws Exception {
        executeLutCmd("cmdPrTaskLUTForkVerifyLicense", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId,
                license, "0"});
    }
    

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
