/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for ValidPutawayRegionsCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class ValidPutawayRegionsCmdTest extends TaskCommandTestCase {

    private ValidPutawayRegionsCmd cmd;
    private String putAwayOperId = "putAwayOperator";
    private String putAwaySerial = "putAwayValidRegionsSerial";
    
    private String replenishmentOperId = "replenishmentOperator";
    private String replenishmentSerial = "replenishmentPutawayValidSerial";

    /**
     * @return a bean for testing.
     */
    private ValidPutawayRegionsCmd getCmdBean() {
        return (ValidPutawayRegionsCmd) 
            getBean("cmdPrTaskLUTForkValidPutAwayRegions");
    }
    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        // load the 
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppOperators.xml");
        // Load the regions
        classSetupPutAwayRegions();
        classSetupReplenRegions();
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteNoAuthorizedPutAwayRegions() throws Exception {

        // Sign on an operator who is not authorized for putaway.
        initialize(replenishmentSerial, replenishmentOperId);
        // We should receive an error.
        Response response = executeCommand(this.getCmdDateSec(), replenishmentSerial, replenishmentOperId);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD),
            Long.toString(TaskErrorCode.NOT_AUTHORIZED_ANY_REGIONS.getErrorCode()), "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains(replenishmentOperId),
                   "error message should contain Operator Id");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    // So we don't have to load data for every test, I've made these tests dependent upon each other.
    // The tests need to run in a specific order.
    @Test()
    public void testExecuteRegions() throws Exception {

        // Sign on an operator who is authorized for putaway.
        initialize(this.putAwaySerial, this.putAwayOperId);
        //Test for all authorized regions 
        Response response = executeCommand(this.getCmdDateSec(), putAwaySerial, putAwayOperId);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 3, "recordCount");
        assertEquals(response.getRecords().get(0).get("errorCode"), "0", "ErrorCode");
        assertEquals(response.getRecords().get(0).get("errorMessage"), "", "ErrorMessage");
    }
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * @param serial - the serial number
     * @param operator - the operator
     * @throws Exception - Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param sn - the serial number
     * @param operId - the operator ID
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String sn, String operId) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
