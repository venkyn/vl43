/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PutawayLicenseCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class RequestPutawayRegionCmdTest extends TaskCommandTestCase {

    private RequestPutawayRegionCmd cmd;
    private String operId = "operRequestPutawayRegionCmd";
    private String sn = "serialRequestPutawayRegionCmd";

    /**
     * @return a bean for testing.
     */
    private RequestPutawayRegionCmd getCmdBean() {
        return (RequestPutawayRegionCmd) 
            getBean("cmdPrTaskLUTForkRequestPutAwayRegion");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();        
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.RequestPutawayRegionCmd#getLicenseNumber}.
     */
    @Test()
    public void testGetSetAllRegions() {
        this.cmd.setAllRegions(true);
        assertEquals(this.cmd.getAllRegions(), true, "All Regions Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.RequestPutawayRegionCmd#getRegionNumber}.
     */
    @Test()
    public void testGetSetQuantity() {
        this.cmd.setRegionNumber("");
        assertEquals(this.cmd.getRegionNumber(), "", "Region Number Getter/Setter Null");
        assertNull(this.cmd.getRegionNumberInt(), "Region Number Getter/Setter Null");

        this.cmd.setRegionNumber("1");
        assertEquals(this.cmd.getRegionNumber(), "1", "Region Number Getter/Setter Not Null");
        assertEquals(this.cmd.getRegionNumberInt(), (Integer) 1, 
                     "Region Number Getter/Setter Not Null");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidRegion() throws Exception {

        initialize();
        //Test standard response for signing into one authorized Region
        Response response = executeCommand(this.getCmdDateSec(), "21", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        assertEquals(record.get("errorMessage"), "", "ErrorMessage");        
    }

    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteInvalidRegion() throws Exception {

        initialize();
        //Test response for attempting to sign onto a non existent region
        Response response = executeCommand(this.getCmdDateSec(), "99", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.REGION_NOT_FOUND.getErrorCode(), "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains("99"),
                   "error message should contain region 99");
    }
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteUnAuthorizedRegion() throws Exception {

        initialize();
        //Test response for attempting to sign onto an unauthorized region
        Response response = executeCommand(this.getCmdDateSec(), "24", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.NOT_AUTH_FOR_REGION.getErrorCode(), 
                     "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains("24"),
                   "error message should contain region 24");
    }
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteAllRegions() throws Exception {

        initialize();
        //Test standard response for signing into all authorized regions
        Response response = executeCommand(this.getCmdDateSec(), "99", "1");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode().toString(), "0", "errorCode");
        assertEquals(record.getErrorMessage(), "", "errorMessage");
       
        //===============================================================
        // Test all regions with conflict
        //===============================================================
        // modify the region setting to set up the conflict
        PutawayRegion r = (PutawayRegion) this.cmd.getRegionManager().findRegionByNumber(21);
        r.setVerifyLicense(true);
        this.cmd.getRegionManager().save(r);
        r = (PutawayRegion) this.cmd.getRegionManager().findRegionByNumber(22);
        r.setVerifyLicense(false);
        this.cmd.getRegionManager().save(r);

        // Test the response for signing into conflicting regions when requesting all regions
        response = executeCommand(this.getCmdDateSec(), "99", "1");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.ALL_REGIONS_NOT_ALLOWED.getErrorCode(), 
                     "errorCode");
         //===============================================================
        // Test 2 regions with conflict
        //===============================================================
        // Test response for signing into conflicting regions when requesting only two regions. 
        executeCommand(this.getCmdDateSec(), "21", "0");
        response = executeCommand(this.getCmdDateSec(), "22", "0");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.NOT_COMP_WITH_PREV_REGION.getErrorCode(), 
                     "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains("22"),
                   "error message should contain Region 22");
        
        // undo the conflict
        r = (PutawayRegion) this.cmd.getRegionManager().findRegionByNumber(22);
        r.setVerifyLicense(false);
        this.cmd.getRegionManager().save(r);
    }    
    
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidPutAwayRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param regionNumber - region Number
     * @param allRegions - all Regions Flag
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String regionNumber,
                                    String allRegions) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                regionNumber, allRegions}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
