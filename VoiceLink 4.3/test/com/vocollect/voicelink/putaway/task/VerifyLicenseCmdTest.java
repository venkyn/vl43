/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for VerifyLicenseCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class VerifyLicenseCmdTest extends TaskCommandTestCase {

    private VerifyLicenseCmd cmd;
    private String operId = "operVerifyLicenseCmd";
    private String sn = "serialVerifyLicenseCmd";

    /**
     * @return a bean for testing.
     */
    private VerifyLicenseCmd getCmdBean() {
        return (VerifyLicenseCmd) getBean("cmdPrTaskLUTForkVerifyLicense");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();
        classSetupPutAwayData();
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.VerifyLicenseCmd#getLicenseNumber}.
     */
    @Test()
    public void testGetSetLicenseNumber() {
        this.cmd.setLicenseNumber("License");
        assertEquals(this.cmd.getLicenseNumber(), "License", "LicenseNumber Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.VerifyLicenseCmd#getScanned}.
     */
    @Test()
    public void testGetSetScanned() {
        this.cmd.setScanned(true);
        assertEquals(this.cmd.getScanned(), true, "Region Number Getter/Setter");
    }
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        getValidRegions();
        requestRegion("21", "0");
        getRegionConfiguration();
        //===============================================================
        // pick up a license in region 21 and verify that it goes in progress
        Response response = executeCommand(this.getCmdDateSec(), "11243", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        //verify license is in-progress with correct operator
        License l = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(l.getStatus(), LicenseStatus.InProgress, "LicenseStatus");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteMultiple() throws Exception {

        initialize();
        getValidRegions();
        requestRegion("21", "0");
        getRegionConfiguration();

        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "2", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.MULTIPLE_LICENSE_FOUND.getErrorCode(),
                "errorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("2"),
                   "error message should contain 2");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteFatalException() throws Exception {

        initialize();
        // Do not sign into any regions. Verify that we throw a not signed in
        // to any regions exception.   This really should never happen.
        Response response = executeCommand(this.getCmdDateSec(), "999999", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 98L, "errorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("operVerifyLicenseCmd"),
                   "error message should contain operator operVerifyLicenseCmd");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNotFoundException() throws Exception {

        initialize();
        getValidRegions();
        requestRegion("21", "0");
        getRegionConfiguration();

        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "999999", "0");

        //Verify Successful result
        List <ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.LICENSE_NOT_FOUND.getErrorCode(),
                "errorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("999999"),
                   "error message should contain Licence 999999");
        
    }   
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteAlreadySelectedException() throws Exception {

        initialize();
        getValidRegions();
        requestRegion("21", "0");
        getRegionConfiguration();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "0");
        response = executeCommand(this.getCmdDateSec(), "11243", "0");

        //Verify Successful result
        List <ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 99L, "errorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("11243"),
                   "error message should contain Licence 11243");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNotAvailableException() throws Exception {

        initialize();
        getValidRegions();
        requestRegion("21", "0");
        getRegionConfiguration();
        //===============================================================
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11218", "0");

        //Verify Successful result
        List <ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.LICENSE_NOT_AVAILABLE.getErrorCode(), 
                "errorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertTrue(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("11218"),
                   "error message should contain Licence 11218");
        
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    
    /**
    * Execute a command to get the region configuration for putaway.
    * 
    * @throws Exception - all exceptions
    */
   private void getValidRegions() throws Exception {
       executeLutCmd("cmdPrTaskLUTForkValidPutAwayRegions", 
           new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
   }
   
    /**
     * Execute a command to request a region for putaway.
     * 
     * @param regionId - region Id
     * @param allRegions - boolean for choosing all regions
     * @throws Exception - all exceptions
     */
    private void requestRegion(String regionId, String allRegions) throws Exception {
        executeLutCmd("cmdPrTaskLUTForkRequestPutAwayRegion", 
            new String[] {makeStringFromDate(
                this.getCmdDateSec()), sn, operId, regionId, allRegions});
    }
    
    /**
    * Execute a command to get the region configuration for putaway.
    * 
    * @throws Exception - all exceptions
    */
   private void getRegionConfiguration() throws Exception {
       executeLutCmd("cmdPrTaskLUTForkPutAwayRegionConfiguration", 
           new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
   }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param license - region Number
     * @param scanned - all Regions Flag
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String license,
                                    String scanned) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, license, scanned}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
