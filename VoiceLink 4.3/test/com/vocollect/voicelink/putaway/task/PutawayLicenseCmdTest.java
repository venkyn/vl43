/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.putaway.model.License;
import com.vocollect.voicelink.putaway.model.LicenseDetail;
import com.vocollect.voicelink.putaway.model.LicenseDetailStatus;
import com.vocollect.voicelink.putaway.model.LicenseStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PutawayLicenseCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class PutawayLicenseCmdTest extends TaskCommandTestCase {
    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    
    private PutawayLicenseCmd cmd;
    private String operId = "operPutawayLicenseCmdTest";
    private String sn = "serialPutawayLicenseCmdTest";

    /**
     * @return a bean for testing.
     */
    private PutawayLicenseCmd getCmdBean() {
        return (PutawayLicenseCmd) getBean("cmdPrTaskLUTForkPutAwayLicense");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
   
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getLicenseNumber}.
     */
    @Test()
    public void testGetSetLicenseNumber() {
        this.cmd.setLicenseNumber("License");
        assertEquals(this.cmd.getLicenseNumber(), "License", "License Number Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getQuantityPut}.
     */
    @Test()
    public void testGetSetQuantity() {
        this.cmd.setQuantityPut("");
        assertEquals(this.cmd.getQuantityPut(), "", "Quantity Put Getter/Setter Null");
        assertNull(this.cmd.getQuantityPutInt(), "Quantity Put Getter/Setter Null");

        this.cmd.setQuantityPut("1");
        assertEquals(this.cmd.getQuantityPut(), "1", "Quantity Put Getter/Setter Not Null");
        assertEquals(this.cmd.getQuantityPutInt(), (Integer) 1, "Quantity Put Getter/Setter Not Null");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getLocationId}.
     */
    @Test()
    public void testGetSetLocationId() {
        this.cmd.setLocationId("");
        assertEquals(this.cmd.getLocationId(), "", "Location Id Getter/Setter Null");
        assertNull(this.cmd.getLocationIdLong(), "Location Id Getter/Setter Null");

        this.cmd.setLocationId("1");
        assertEquals(this.cmd.getLocationId(), "1", "Location Id Getter/Setter Not Null");
        assertEquals(this.cmd.getLocationIdLong(), (Long) 1L, "Location Id Getter/Setter Not Null");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getPutIndicator}.
     */
    @Test()
    public void testGetSetPutIndicator() {
        this.cmd.setPutIndicator(1);
        assertEquals(this.cmd.getPutIndicator(), (Integer) 1, "Put Indicator Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getReasonCode}.
     */
    @Test()
    public void testGetSetReasonCode() {
        this.cmd.setReasonCode("");
        assertEquals(this.cmd.getReasonCode(), "", "Reason Code Getter/Setter Null");

        this.cmd.setReasonCode("1");
        assertEquals(this.cmd.getReasonCode(), "1", "Reason Code Getter/Setter Not Null");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.putaway.task.PutawayLicenseCmd#getStartTime}.
     */
    @Test()
    public void testGetSetStartTime() {
        this.cmd.setStartTime("");
        assertEquals(this.cmd.getStartTime(), "", "StartTime Getter/Setter Null");
        assertNull(this.cmd.getStartTimeAsDate(), "StartTime Getter/Setter Null");

        Date startDate = this.getCmdDateSec();
        this.cmd.setStartTime(makeStringFromDate(startDate));
        assertEquals(this.cmd.getStartTime(), makeStringFromDate(startDate), 
                     "StartTime Getter/Setter Not Null");
        assertEquals(makeStringFromDate(this.cmd.getStartTimeAsDate()),
                     makeStringFromDate(startDate),
                     "StartTime Getter/Setter Not Null");
    }
    
 
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteCancel() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "0", "-1243", "2", "3");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        License license = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(license.getStatus(), LicenseStatus.Complete, "LicenseComplete");
        
        List<LicenseDetail> detailsList = this.cmd.getLicenseDetailManager().getAll();
        assertEquals(detailsList.size(), 1, "DetailCount");
        LicenseDetail theDetails = detailsList.get(0);
        
        assertEquals(theDetails.getStatus(), LicenseDetailStatus.Canceled, "DetailStatus");
        assertNull(theDetails.getStartLocation(), "StartLocation");
        assertNotNull(theDetails.getReason(), "Reason Code");
               
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecutePartial() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "1", "-1243", "0", "");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");

        License license = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(license.getStatus(), LicenseStatus.InProgress, "LicenseComplete");
        
        List<LicenseDetail> details = this.cmd.getLicenseDetailManager().getAll();
        assertEquals(details.size(), 1, "DetailCount");
        assertEquals(details.get(0).getStatus(), LicenseDetailStatus.Put, "DetailStatus");
        assertNull(details.get(0).getReason(), "Reason code");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteComplete() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "20", "-1243", "1", "");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        License license = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(license.getStatus(), LicenseStatus.Complete, "LicenseComplete");
        assertNotNull(license.getOperator(), "LicenseOperator");
        assertEquals(license.getQuantityPutaway(), 20, "Quantity put");
        assertNotNull(license.getStartTime(), "Start time should no be null");
        assertNotNull(license.getPutTime(), "Put time should not be null");
        
        List<LicenseDetail> details = this.cmd.getLicenseDetailManager().getAll();
        LicenseDetail detail = details.get(0);
        assertEquals(1, details.size(), "DetailCount");
        assertEquals(LicenseDetailStatus.Put,  detail.getStatus(), "DetailStatus");
        assertEquals(detail.getPutLocation().getId(), new Long(-1243), "Detail put location");
        assertEquals(detail.getQuantity(), new Integer(20), "Detail Put quantity");
        assertNotNull(detail.getOperator(), "Detail operator");
        assertNotNull(detail.getStartTime(), "Detail start time");
        assertNotNull(detail.getPutTime(), "Detail put time");
        
        //Verify Operator Labor Record results
        List<OperatorLabor> olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec0 = olList.get(0);
        OperatorLabor olRec1 = olList.get(1);
        assertEquals(olRec1.getActionType(), OperatorLaborActionType.PutAway, "Invalid Action Type: ");
        assertEquals(olRec0.getOperator().getId(), olRec1.getOperator().getId(), "Invalid OperatorID: ");
        assertNotNull(olRec1.getStartTime(), "Start Time is Null: ");
        assertNull(olRec1.getEndTime(), "End Time is not Null: ");
        assertTrue(olRec1.getDuration() > new Long(0), "Invalid Duration: ");
        assertNotNull(olRec1.getCreatedDate(), "Created Date is Null: ");
        assertEquals(olRec1.getCount(), new Integer(1), "Invalid Count :");
        assertTrue(olRec1.getActualRate() > new Double(0), "Invalid Actual Rate: ");
        assertTrue(olRec1.getPercentOfGoal() > new Double(0), "Invalid Percent of Goal: ");
        assertNotNull(olRec1.getRegion(), "Region is Null: ");
        assertNull(olRec1.getBreakType(), "BreakID not Null: ");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteMultiRegionPutaway() throws Exception {

        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();
        classSetupPutAwayData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        requestRegion("21", "0");
        requestRegion("22", "0");
        getRegionConfiguration();
       
        requestLicense("11228");
        executeCommand(this.getCmdDateSec(), "11228", "24", "-1228", "1", "");
        requestLicense("11226");
        executeCommand(this.getCmdDateSec(), "11226", "50", "-1226", "1", "");
        requestLicense("11240");
        executeCommand(this.getCmdDateSec(), "11240", "50", "-1240", "1", "");
        
        //verify the labor record results
        List<OperatorLabor> olList = this.cmd.getLaborManager()
            .getOperatorLaborManager()
                .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        Long longZero = new Long(0);
        Double dblZero = new Double(0);
        OperatorLabor olRec = olList.get(1);
        assertEquals(olList.size(), 2, "Invalid number of records: ");
        assertTrue(olRec.getDuration() > longZero, "Invalid Duration: ");
        assertEquals(olRec.getCount(), new Integer(3), "Invalid Count: ");
        assertTrue(olRec.getActualRate() > dblZero, "Invalid Actual Rate: ");
        assertTrue(olRec.getPercentOfGoal() > dblZero, "Invalid Percent of Goal: ");
        assertNull(olRec.getRegion(), "Region ID Not Null: ");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteReleased() throws Exception {

        initialize();
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "0",
            "-1243", "3", "");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        License license = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(license.getStatus(), LicenseStatus.Available, "LicneseComplete");
        assertNull(license.getOperator(), "OperatorNull");
        
        List<LicenseDetail> details 
            = this.cmd.getLicenseDetailManager().getAll();
        assertEquals(0, details.size(), "DetailCount");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteOnCompleted() throws Exception {

        initialize();
        License license = this.cmd.getLicenseManager().findByNumber("11243");
        license.setStatus(LicenseStatus.Complete);
        this.cmd.getLicenseManager().save(license);
        
        //Test that attempt to putaway a license that has already been 
        //given a completed status throws appropriate error message.
        Response response = executeCommand(this.getCmdDateSec(), "11243", "0",
            "-1243", "3", "");
        
        //Verify correct results
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.LICENSE_ALREADY_COMPLETE.getErrorCode(), 
                     "errorCode");
        assertFalse(record.getErrorMessage().contains("{"),
                    "error message should not contain curly braces");
        assertTrue(record.getErrorMessage().contains("11243"),
                   "error message should contain License Number 11243");
        
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.forkapps.putaway.task.ValidPutawayCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteTakeABreak() throws Exception {

        initialize();
        // Test breaks for Put away.  Verify the following sequence of events.
        // Sign on to the system, request a put away region, and get a license 
        // and execute the put.
        // Verify that we get an open put away labor record with updated counts.
        // Take a break.  Verify that we get an open break labor record.
        // Return from break.  Verify that we get an open put away labor record.
        //===============================================================
        //Test standard response
        Response response = executeCommand(this.getCmdDateSec(), "11243", "20", "-1243", "1", "");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        
        License license = this.cmd.getLicenseManager().findByNumber("11243");
        assertEquals(LicenseStatus.Complete, license.getStatus(), "LicenseComplete");
        
        //Verify Operator Labor Record results
        List<OperatorLabor> olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec0 = olList.get(0);
        OperatorLabor olRec1 = olList.get(1);
        assertEquals(olRec1.getActionType(), OperatorLaborActionType.PutAway, "Invalid Action Type: ");
        assertEquals(olRec0.getOperator().getId(), olRec1.getOperator().getId(), "Invalid OperatorID: ");
        assertNotNull(olRec1.getStartTime(), "Start Time is Null: ");
        assertNull(olRec1.getEndTime(), "End Time is not Null: ");
        assertTrue(olRec1.getDuration() > new Long(0), "Invalid Duration: ");
        assertNotNull(olRec1.getCreatedDate(), "Created Date is Null: ");
        assertEquals(olRec1.getCount(), new Integer(1), "Invalid Count :");
        assertTrue(olRec1.getActualRate() > new Double(0), "Invalid Actual Rate: ");
        assertTrue(olRec1.getPercentOfGoal() > new Double(0), "Invalid Percent of Goal: ");
        assertNotNull(olRec1.getRegion(), "Region is Null: ");
        assertNull(olRec1.getBreakType(), "BreakID not Null: ");
        
        takeABreak("1", "0", "0");
        olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRec2 = olList.get(2);
        OperatorBreakLabor blRec = (OperatorBreakLabor) olRec2;
        assertEquals(olRec2.getActionType(), OperatorLaborActionType.Break, "Action Type ");
        assertNotNull(olRec2.getStartTime().getTime(), "Start Time ");
        assertNotNull(olRec2.getCreatedDate().getTime(), "Created Date ");
        assertEquals(blRec.getPreviousOperatorLabor().getId(), olRec1.getId(), "Previous Operator Labor Id ");
        assertEquals(blRec.getBreakType().getId(), new Long(-1), "Break Id ");
        takeABreak("1", "1", "0");
        olList = this.cmd.getLaborManager()
            .getOperatorLaborManager().listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        Collections.sort(olList, OPERATOR_LABOR_ID);
        assertEquals(olList.size(), 4, "Number of Records ");
        olRec2 = olList.get(2);
        OperatorLabor olRec3 = olList.get(3);
        blRec = (OperatorBreakLabor) olRec2;
        assertTrue(olRec2.getDuration() > new Long(0), "Duration ");
        assertNotNull(olRec2.getEndTime().getTime(), "End Time");
        assertEquals(olRec3.getActionType(), OperatorLaborActionType.PutAway, "Action Type ");
        assertEquals(olRec3.getOperator().getId(), olRec2.getOperator().getId(), "Operator Id ");
        assertNotNull(olRec3.getStartTime().getTime(), "Start Time ");
        assertEquals(olRec3.getRegion().getId(), new Long(-21), "Region Id ");
    }
    


    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();
        classSetupPutAwayData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTForkValidPutAwayRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        requestRegion("21", "0");
        getRegionConfiguration();
        requestLicense("11243");
    }
    
    /**
     * Execute a command to request a region for putaway.
     * 
     * @param regionId - region Id
     * @param allRegions - boolean for choosing all regions
     * @throws Exception - all exceptions
     */
    private void requestRegion(String regionId, String allRegions) throws Exception {
        executeLutCmd("cmdPrTaskLUTForkRequestPutAwayRegion", 
            new String[] {makeStringFromDate(
                this.getCmdDateSec()), sn, operId, regionId, allRegions});
    }
    
    /**
     * Execute a command to get the region configuration for putaway.
     * 
     * @throws Exception - all exceptions
     */
    private void getRegionConfiguration() throws Exception {
        executeLutCmd("cmdPrTaskLUTForkPutAwayRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * Execute a command to request a license for putaway.
     * 
     * @param licenseNum - license number
     * @throws Exception - all exceptions
     */
    private void requestLicense(String licenseNum) throws Exception {
        executeLutCmd("cmdPrTaskLUTForkVerifyLicense", 
            new String[] {makeStringFromDate(
                this.getCmdDateSec()), sn, operId, licenseNum, "0"});
    }
    
    /**
     * 
     * @param breakType -  type of break, battery change, lunch, etc
     * @param startEndFlag - 0 to start break.  1 to end break
     * @param description - not used
     * @throws Exception
     */
    private void takeABreak(String breakType, String startEndFlag, String description)
                                                              throws Exception {

        executeLutCmd("cmdPrTaskODRCoreSendBreakInfo", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            breakType, startEndFlag, description});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param licenseNumber - license put
     * @param quantityPut - quantity that was put
     * @param locationId - location Id
     * @param putIndicator - put indicator
     * @param reasonCode - reason code
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String licenseNumber,
                                    String quantityPut,
                                    String locationId,
                                    String putIndicator,
                                    String reasonCode) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                licenseNumber, quantityPut, locationId,
                putIndicator, reasonCode, 
                makeStringFromDate(cmdDate)}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
