/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.task;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for PutawayRegionConfigurationCmd.
 *
 * @author mkoenig
 */
@Test(groups = { FAST, TASK, PUTAWAY })
public class PutawayRegionConfigurationCmdTest extends TaskCommandTestCase {

    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    
    private PutawayRegionConfigurationCmd cmd;
    private String operId = "operPutawayRegionConfiguration";
    private String sn = "serialPutawayRegionConfiguration";

    /**
     * @return a bean for testing.
     */
    private PutawayRegionConfigurationCmd getCmdBean() {
        return (PutawayRegionConfigurationCmd) 
            getBean("cmdPrTaskLUTForkPutAwayRegionConfiguration");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
     
    /**
     * Test method for {@link com.vocollect.voicelink.replenishment.task.ValidReplenishmentRegionsCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteChangeRegion() throws Exception {

        initialize();
        validPutawayRegions();
        requestSingleRegion("22");

        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        Response response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("regionNumber").toString(), "22", "regionNumber");
        assertEquals(record.get("regionName").toString(), "Put away Region 22", "regionName");
        
        // change regions
        validPutawayRegions();
        requestSingleRegion("21");
        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get("regionNumber").toString(), "21", "regionNumber");
        assertEquals(record.get("regionName").toString(), "Put away Region 21", "regionName");
 
        // change regions
        validPutawayRegions();
        requestSingleRegion("22");
        requestSingleRegion("21");
        //Test that we successfully signed on to region 32 and the proper configuration is returned 
        response = executeCommand(this.getCmdDateSec());
        //Verify Successful result
        assertEquals(response.getRecords().size(), 2, "recordSize");
    }
    
    
    
    /**
     * request a single region and verify result.
     * @throws Exception any
     */
    @Test()
    public void testRequestSingleRegionExecute() throws Exception {

        initialize();
        validPutawayRegions();
        requestSingleRegion("23");
        //Test standard response for a single region sign-on
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "regions returned");
        ResponseRecord record = records.get(0);
        
        assertEquals(record.get("regionNumber"), 23, "regionNumber");
        assertEquals(record.get("regionName"), "Put away Region 23", "regionName");
        assertEquals(record.get("captureStartLocation"), 1, "captureStartLocation");
        assertEquals(record.get("allowSkipLicense"), true, "allowSkipLicense");
        assertEquals(record.get("allowCancelLicense"), true, "allowCancelLicense");
        assertEquals(record.get("allowOverrideLocation"), true, "allowOverrideLocation");
        assertEquals(record.get("allowOverrideQuantity"), false, "allowOverrideQuantity");
        assertEquals(record.get("allowPartialPut"), true, "allowPartialPut");
        assertEquals(record.get("capturePickUpQuantity"), false, "capturePickUpQuantity");
        assertEquals(record.get("capturePutQuantity"), true, "capturePutQuantity");
        assertEquals(record.get("licenseDigitsTaskSpeaks"), 2, "licenseDigitsTaskSpeaks");
        assertEquals(record.get("licenseDigitsOperatorSpeaks"), 4, "licenseDigitsOperatorSpeaks");
        assertEquals(record.get("locationDigitsOperatorSpeaks"), 5, "locationDigitsOperatorSpeaks");
        assertEquals(record.get("numberCheckDigitsOperatorSpeaks"), 2, "numberCheckDigitsOperatorSpeaks");
        assertEquals(record.get("numberLicenseAllowed"), 4, "numberLicenseAllowed");
        assertEquals(record.get("exceptionLocation"), "Exception Location", "exceptionLocation");
        assertEquals(record.get("verifySpokenLicenseLocation"), false, "verifySpokenLicenseLocation");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
        
        //  Verify that we got 2 labor records. The first is a sign on record. 
        // The second is a putaway. 
        List<OperatorLabor> olList =
            this.cmd.getLaborManager().getOperatorLaborManager()
                .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(olList.size(), 2, "Record Count ");
        Collections.sort(olList, OPERATOR_LABOR_ID);
        
        assertEquals(OperatorLaborActionType.SignOn,
                     olList.get(0).getActionType(), "Invalid Action Type ");
        
        // Get the putaway record
        OperatorLabor olRecord = olList.get(1);
        
        assertEquals(OperatorLaborActionType.PutAway, olRecord.getActionType(), "Invalid Action Type ");
        assertNotNull(olRecord.getStartTime(), "Invalid Start Time ");
        assertNull(olRecord.getEndTime(), "Invalid End Time ");
        Integer integerZero = new Integer(0);
        Double doubleZero = new Double(0);
        Long longZero = new Long(0);
        assertEquals(olRecord.getCount(), integerZero, "Count not zero ");
        assertEquals(olRecord.getActualRate(), doubleZero, "ActualRate not zero ");
        assertEquals(olRecord.getPercentOfGoal(), doubleZero, "PercentOfGoal not zero ");
        assertEquals(olRecord.getOperator().getId(), olList.get(0).getOperator().getId(),
                     "Invalid Operator ID ");
        assertEquals(olRecord.getDuration(), longZero, "Invalid Duration ");
        assertEquals(olRecord.getExportStatus(), ExportStatus.NotExported, "Invalid ExportStatus ");
        assertNotNull(olRecord.getRegion(), "RegionID Null ");
        assertNull(olRecord.getBreakType(), "BreakId not Null ");
    }
    
    
    /**
     * request two regions.
     * @throws Exception any
     */
    @Test()
    public void testRequestTwoRegionsExecute() throws Exception {

        initialize();
        validPutawayRegions();
        requestSingleRegion("21");
        requestSingleRegion("23");

        //Test standard response for two region sign-on
        Response response = executeCommand(this.getCmdDateSec());

        //Verify that we got all 2 regions.
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 2, "regions returned"); 
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
                
        // verify that the region field is null since we are signed on to two regions.
        OperatorLabor olRec =
            this.cmd.getLaborManager().getOperatorLaborManager()
                    .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        
        assertNull(olRec.getRegion(), "RegionID Null ");
    }
    
    
    
     /**
     * request all regions.
     * @throws Exception any
     */
    @Test()
    public void testRequestAllRegionsExecute() throws Exception {

        initialize();
        validPutawayRegions();
        requestAllRegions();

        //Test standard response
        Response response = executeCommand(this.getCmdDateSec());

        //Verify that we got all 3 regions.
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 3, "regions returned"); 
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMesssage");
         
        // verify that the region field is null since we are signed on to all regions.
        OperatorLabor olRec =
            this.cmd.getLaborManager().getOperatorLaborManager()
                    .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        assertNull(olRec.getRegion(), "RegionID Null ");
    }
    
    
    /**
     * No regions are signed into but a request for 
     * region configuration has been made.
     * @throws Exception any
     */
    public void testRequestNoRegions() throws Exception {

        initialize();
        validPutawayRegions();
        //Test error is thrown for regions no longer available
        Response response = executeCommand(this.getCmdDateSec());
        
        //Verify Results
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "records returned");
        ResponseRecord record = records.get(0);
        
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.REGIONS_NO_LONGER_VALID.getErrorCode(), 
                     "errorCode");
    }
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        classSetupPutAwayRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }

    /**
     * 
     * @throws Exception
     */
    private void validPutawayRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTForkValidPutAwayRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * 
     * @param region - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        
        executeLutCmd("cmdPrTaskLUTForkRequestPutAwayRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, region, "0"});
    }

    /**
     * 
     * @throws Exception
     */
    private void requestAllRegions() throws Exception {
        
        executeLutCmd("cmdPrTaskLUTForkRequestPutAwayRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0", "1"});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
