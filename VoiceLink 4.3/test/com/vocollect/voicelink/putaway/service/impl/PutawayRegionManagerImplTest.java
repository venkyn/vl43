/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.putaway.PutawayErrorCode;
import com.vocollect.voicelink.putaway.model.PutawayRegion;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the ReplenishmentManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, PUTAWAY })
public class PutawayRegionManagerImplTest extends  BaseServiceManagerTestCase {

    private PutawayRegionManager putawayRegionManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setPutawayRegionManager(PutawayRegionManager impl) {
        this.putawayRegionManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        // populate db with base data.
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_ForkAppWorkGroups.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
           + "VoiceLinkDataUnitTest_PutawayRegions.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PutAwayOperators.xml", DatabaseOperation.INSERT);
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testAverageGoalRate() throws Exception {
        
      Double rate = this.putawayRegionManager.avgGoalRate();
      assertEquals(rate, new Double(175.00), "Average goal rate is incorrect");
    }
    
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
      
        Long theValue =  this.putawayRegionManager.uniquenessByNumber(new Integer(21));
        assertEquals(theValue, new Long(-21), "Should have found region 21");
        
        theValue = this.putawayRegionManager.uniquenessByNumber(15);
        assertNull(theValue, "Value should be null");
    }
    
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        
        Long theValue =  this.putawayRegionManager.uniquenessByName("Put away Region 23");
        assertEquals(theValue, new Long(-23), "Should have found region 23");
        
        theValue = this.putawayRegionManager.uniquenessByName("Does Not Exist");
        assertNull(theValue, "Value should be null");
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testexecuteValidateEditRegion() throws Exception {
        
        // verify that we throw an exception when attempting to edit a region
        // that has an operator signed on.
        PutawayRegion region = this.putawayRegionManager.findRegionByNumber(21);
        try {
            this.putawayRegionManager.executeValidateEditRegion(region);
            fail("Did not catch business Rule Exception");
        } catch (BusinessRuleException bre) {
              assertEquals(bre.getErrorCode(), PutawayErrorCode.REGION_OPERATORS_SIGNED_IN);
        }
        
        // verify that we do not throw an exception when attempting to edit a
        // region that does not have an operator signed on.
        region = this.putawayRegionManager.findRegionByNumber(24);
        try {
            this.putawayRegionManager.executeValidateEditRegion(region);
        } catch (BusinessRuleException bre) {
             fail("Should not have caught execption");           
        }
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testexecuteValidateDeleteRegion() throws Exception {
        
        // verify that we throw an exception when attempting to delete a region
        // that has an operator signed on.
        PutawayRegion region = this.putawayRegionManager.findRegionByNumber(21);
        try {
            this.putawayRegionManager.executeValidateDeleteRegion(region);
            fail("Did not catch business Rule Exception");
        } catch (BusinessRuleException bre) {
              assertEquals(bre.getErrorCode(), PutawayErrorCode.REGION_OPERATORS_SIGNED_IN);
        }
        // it would be good to verify that we can actually delete a region but
        // if we delete a region, the other tests would break because they run in 
        // a random order.
    }
 }
