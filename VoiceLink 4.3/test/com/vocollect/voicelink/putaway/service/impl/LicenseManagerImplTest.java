/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.putaway.model.PutawayLicenseSummary;
import com.vocollect.voicelink.putaway.service.LicenseManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTAWAY;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the ReplenishmentManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, PUTAWAY })

public class LicenseManagerImplTest  extends  BaseServiceManagerTestCase {
 
    private LicenseManager licenseManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setLicenseManager(LicenseManager impl) {
        this.licenseManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        
        adapter.resetInstallationData();
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataSmall.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testAssignmentSummaryCounts() throws Exception {
        Object[] queryArgs = {new Date(0)};
        
        List<DataObject> theList = new ArrayList<DataObject>();
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(queryArgs);
        
        rdi.setSortColumn("region.name");
        theList = this.licenseManager.listLicenseCountsByRegionAndStatus(rdi);
        assertEquals(theList.size(), 3, "List size should be 3");
        PutawayLicenseSummary theObject = (PutawayLicenseSummary) theList.get(0);
        Integer intExpectedValue = new Integer(25);
        Integer intZero = new Integer(0);
        assertNotNull(theObject.getSite(), "Site should not be null");
        assertNotNull(theObject.getRegion(), "Region should not be null");
        assertEquals(theObject.getAvailableLicenses(), intExpectedValue,
            "Value should be " + intExpectedValue.toString());
        assertEquals(theObject.getTotalLicenses(), intExpectedValue, 
            "Total should be "  + intExpectedValue.toString());
        assertEquals(theObject.getNonCompletedLicenses(), intExpectedValue, 
            "Not Complete should be "  + intExpectedValue.toString());
        assertEquals(theObject.getInProgressLicenses(), intZero, "In progress should be zero");
        assertEquals(theObject.getCompletedLicenses(), intZero, "Completed should be zero");
    }
}
